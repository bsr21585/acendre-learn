﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using Asentia.Common;

namespace Asentia.UMS.Library
{
    /// <summary>
    /// Methods for loading and saving a site's home page.
    /// </summary>
    public class HomePage
    {
        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HomePage()
        {
            this.LanguageSpecificProperties = new ArrayList();
            this._LoadHomePageContent();
        }
        #endregion

        #region Properties
        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;
        #endregion

        #region Private Properties
        /// <summary>
        /// The name of the XML file that contains the home page content.
        /// </summary>
        private const string _HOME_PAGE_FILE_NAME = "HomePage.xml";
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;            
            public string Html;
            public LanguageSpecificProperty(string langString, string html)
            {
                this.LangString = langString;                
                this.Html = html;
            }
        }
        #endregion

        #region Methods
        #region SaveHomePageContent
        /// <summary>
        /// Saves the home page content for a site in a specific language,
        /// wile retaining the rest of the saved language versions in the file.
        /// </summary>
        public void SaveHomePageContent()
        {
            try
            {
                string fullFilePath = SitePathConstants.SITE_CONFIG_ROOT + _HOME_PAGE_FILE_NAME;
                XmlDocument homePageXml = new XmlDocument();

                // BEGIN BUILDING XML

                // homePages node (document element)
                XmlNode newHomePagesNode = homePageXml.CreateNode(XmlNodeType.Element, "homePages", null);

                // loop through all language specific properties and create nodes for each
                foreach (LanguageSpecificProperty homePageLanguageSpecificProperty in this.LanguageSpecificProperties)
                {
                    // homePage node and lang attribute
                    XmlNode newHomePageNode = homePageXml.CreateNode(XmlNodeType.Element, "homePage", null);
                    XmlAttribute newLangAttribute = homePageXml.CreateAttribute("lang");
                    newLangAttribute.Value = homePageLanguageSpecificProperty.LangString;
                    newHomePageNode.Attributes.Append(newLangAttribute);

                    // html node
                    XmlNode newHtmlNode = homePageXml.CreateNode(XmlNodeType.Element, "html", null);
                    XmlCDataSection newHtmlNodeCData = homePageXml.CreateCDataSection(HttpUtility.HtmlDecode(homePageLanguageSpecificProperty.Html));
                    newHtmlNode.AppendChild(newHtmlNodeCData);
                    newHomePageNode.AppendChild(newHtmlNode);

                    newHomePagesNode.AppendChild(newHomePageNode);
                }

                // append homePages to document
                homePageXml.AppendChild(newHomePagesNode);

                // END BUILDING XML

                // BEGIN WRITE FILE

                // if the directory does not exist, create it
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT)))
                { Directory.CreateDirectory(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT)); }

                // write the new file
                using (XmlTextWriter xmlWriter = new XmlTextWriter(HttpContext.Current.Server.MapPath(fullFilePath + ".new"), Encoding.UTF8))
                {
                    xmlWriter.Formatting = Formatting.Indented;
                    homePageXml.Save(xmlWriter);
                    xmlWriter.Close();
                }

                // copy new file into the old
                File.Copy(HttpContext.Current.Server.MapPath(fullFilePath + ".new"),
                          HttpContext.Current.Server.MapPath(fullFilePath), true);

                // delete the placeholder file
                File.Delete(HttpContext.Current.Server.MapPath(fullFilePath + ".new"));

                // END WRITE FILE
            }
            catch (Exception ex)
            {
                throw new AsentiaException(ex.Message + "<br /><br />" + ex.StackTrace);
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _LoadHomePageContent
        /// <summary>
        /// Loads the home page content for a site in the specified language.
        /// </summary>
        private void _LoadHomePageContent()
        {
            try
            {
                string fullFilePath = SitePathConstants.SITE_CONFIG_ROOT + _HOME_PAGE_FILE_NAME;

                if (File.Exists(HttpContext.Current.Server.MapPath(fullFilePath)))
                {
                    XmlDocument homePageXml = new XmlDocument();
                    homePageXml.Load(HttpContext.Current.Server.MapPath(fullFilePath));

                    XmlNodeList homePageNodes = homePageXml.GetElementsByTagName("homePage");

                    foreach (XmlNode node in homePageNodes)
                    {
                        this.LanguageSpecificProperties.Add(new LanguageSpecificProperty(node.Attributes["lang"].Value,
                                                                                         node.ChildNodes[0].InnerText));
                    }
                }
                else // else, load the default home page, if it exists
                {
                    string defaultFilePath = SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME + _HOME_PAGE_FILE_NAME;

                    if (AsentiaSessionState.IsThemePreviewMode)
                    { defaultFilePath = SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW + _HOME_PAGE_FILE_NAME; }

                    if (File.Exists(HttpContext.Current.Server.MapPath(defaultFilePath)))
                    {
                        XmlDocument homePageXml = new XmlDocument();
                        homePageXml.Load(HttpContext.Current.Server.MapPath(defaultFilePath));

                        XmlNodeList homePageNodes = homePageXml.GetElementsByTagName("homePage");

                        foreach (XmlNode node in homePageNodes)
                        {
                            this.LanguageSpecificProperties.Add(new LanguageSpecificProperty(node.Attributes["lang"].Value,
                                                                                             node.ChildNodes[0].InnerText));
                        }
                    }
                }
            }
            catch
            {
                throw new AsentiaException(_GlobalResources.UnableToLoadHomePageContent);
            }
        }
        #endregion
        #endregion
    }
}
