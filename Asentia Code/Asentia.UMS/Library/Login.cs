﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using Asentia.Common;

namespace Asentia.UMS.Library
{
    public class Login
    {
        #region AuthenticateUser
        public static void AuthenticateUser(string username, string password, out string lastActiveSessionId)
        {
            //AsentiaSite object to fetch site param only, passed 0 as idUser
            //as no user id is available at this time.
            AsentiaSite siteParam = new AsentiaSite(0, AsentiaSessionState.IdSite);
            bool simultaniousLogin = (bool)siteParam.ParamBool(SiteParamConstants.SYSTEM_SIMULTANEOUSLOGIN_ENABLED);
            string loginPriority = siteParam.ParamString(SiteParamConstants.SYSTEM_LOGINPRIORITY);

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            // RETURN CODE / ERROR DESCRIPTION CODE
            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@callerLangString", null, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", null, SqlDbType.Int, 4, ParameterDirection.Input);

            // INPUTS
            databaseObject.AddParameter("@username", username, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@password", password, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@loginPriority", loginPriority, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@simultaniousLogin", simultaniousLogin, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtSessionExpires", AsentiaSessionState.DtExpires, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@sessionId", AsentiaSessionState.SessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);

            // get and send the user's ip address to the procedure so we can check against "login" ip restrictions
            string clientIPAddress = String.Empty;

            if (!String.IsNullOrWhiteSpace(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"])) // client's ip address if application is behind load balancer
            { clientIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]; }
            else
            { clientIPAddress = HttpContext.Current.Request.UserHostAddress; }

            databaseObject.AddParameter("@clientIPAddress", clientIPAddress, SqlDbType.NVarChar, 15, ParameterDirection.Input);
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_LOGOUT_ENABLED))
            {
                databaseObject.AddParameter("@lockoutMinutes", AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_LOGOUT_MINUTES), SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@lockoutAttempts", AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_LOGOUT_ATTEMPTS), SqlDbType.Int, 4, ParameterDirection.Input);
            }
            else
            {
                databaseObject.AddParameter("@lockoutMinutes", 0, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@lockoutAttempts", 0, SqlDbType.Int, 4, ParameterDirection.Input);
            }
            // OUTPUTS
            databaseObject.AddParameter("@idSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSiteUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@userFirstName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userLastName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userAvatar", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userGender", null, SqlDbType.NVarChar, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@userCulture", null, SqlDbType.NVarChar, 20, ParameterDirection.Output);
            databaseObject.AddParameter("@isUserACourseExpert", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isUserACourseApprover", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isUserASupervisor", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isUserAWallModerator", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isUserAnILTInstructor", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@lastActiveSessionId", null, SqlDbType.NVarChar, 80, ParameterDirection.Output);
            databaseObject.AddParameter("@userMustChangePassword", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@mustExecuteLicenseAgreement", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtUserAgreementAgreed", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[System.AuthenticateLogin]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                AsentiaSessionState.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                AsentiaSessionState.IdSiteUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSiteUser"].Value);
                AsentiaSessionState.UserFirstName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@userFirstName"].Value);
                AsentiaSessionState.UserLastName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@userLastName"].Value);
                AsentiaSessionState.UserAvatar = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@userAvatar"].Value);
                AsentiaSessionState.UserGender = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@userGender"].Value);
                AsentiaSessionState.UserCulture = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@userCulture"].Value);
                AsentiaSessionState.IsUserACourseExpert = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserACourseExpert"].Value);
                AsentiaSessionState.IsUserACourseApprover = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserACourseApprover"].Value);
                AsentiaSessionState.IsUserASupervisor = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserASupervisor"].Value);
                AsentiaSessionState.IsUserAWallModerator = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserAWallModerator"].Value);
                AsentiaSessionState.IsUserAnILTInstructor = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserAnILTInstructor"].Value);
                AsentiaSessionState.UserMustChangePassword = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@userMustChangePassword"].Value);
                AsentiaSessionState.MustExecuteLicenseAgreement = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@mustExecuteLicenseAgreement"].Value);
                AsentiaSessionState.MustExecuteUserAgreement = AsentiaSessionState.GlobalSiteObject.DtUserAgreementModified > AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtUserAgreementAgreed"].Value);
                lastActiveSessionId = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@lastActiveSessionId"].Value);

                // GET USER'S EFFECTIVE PERMISSIONS - UNLESS USER ID IS 1

                if (AsentiaSessionState.IdSiteUser > 1)
                {
                    DataTable effectivePermissions = User.GetEffectivePermissions();
                    List<AsentiaPermissionWithScope> effectivePermissionsList = new List<AsentiaPermissionWithScope>();

                    foreach (DataRow row in effectivePermissions.Rows)
                    {
                        AsentiaPermissionWithScope permissionWithScope = new AsentiaPermissionWithScope((AsentiaPermission)Convert.ToInt32(row["idPermission"]), row["scope"].ToString());
                        effectivePermissionsList.Add(permissionWithScope);
                    }

                    AsentiaSessionState.UserEffectivePermissions = effectivePermissionsList;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ResetPassword
        public static string ResetPassword(string username)
        {
            // generate a new password
            string newPassword = RandomPassword.Generate();

            // RESET THE USER'S PASSWORD

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            // RETURN CODE / ERROR DESCRIPTION CODE
            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", null, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", null, SqlDbType.Int, 4, ParameterDirection.Input);

            // INPUTS
            databaseObject.AddParameter("@username", username, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@idSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@newPassword", newPassword, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            // OUTPUTS
            databaseObject.AddParameter("@userFirstName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userLastName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userEmail", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userCulture", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 20, ParameterDirection.InputOutput);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[User.ResetPassword]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                string firstName = databaseObject.Command.Parameters["@userFirstName"].Value.ToString();
                string lastName = databaseObject.Command.Parameters["@userLastName"].Value.ToString();
                string email = databaseObject.Command.Parameters["@userEmail"].Value.ToString();
                AsentiaSessionState.UserCulture = databaseObject.Command.Parameters["@userCulture"].Value.ToString();

                int systemSmtpServerPortOverride = 0;

                if (!String.IsNullOrWhiteSpace(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_EMAIL_SMTPSERVERPORTOVERRIDE)))
                { Int32.TryParse(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_EMAIL_SMTPSERVERPORTOVERRIDE), out systemSmtpServerPortOverride); }

                bool systemSmtpServerUseSslOverride = false;

                if (!String.IsNullOrWhiteSpace(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_EMAIL_SMTPSERVERUSESSLOVERRIDE)))
                { Boolean.TryParse(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_EMAIL_SMTPSERVERUSESSLOVERRIDE), out systemSmtpServerUseSslOverride); }

                // SEND EMAIL TO USER CONTAINING PASSWORD INFORMATION
                PasswordResetEmail passwordResetEmail = new PasswordResetEmail(AsentiaSessionState.UserCulture, 
                    AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_EMAIL_FROMADDRESSOVERRIDE),
                    AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_EMAIL_SMTPSERVERNAMEOVERRIDE),
                    systemSmtpServerPortOverride,
                    systemSmtpServerUseSslOverride,
                    AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_EMAIL_SMTPSERVERUSERNAMEOVERRIDE),
                    AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_EMAIL_SMTPSERVERPASSWORDOVERRIDE));

                // lms information
                passwordResetEmail.LmsHostname = AsentiaSessionState.SiteHostname;
                passwordResetEmail.LmsUrl = "http://" + AsentiaSessionState.SiteHostname + "." + Config.AccountSettings.BaseDomain;

                // user information
                passwordResetEmail.UserFullName = firstName + " " + lastName;
                passwordResetEmail.UserFirstName = firstName;
                passwordResetEmail.UserLogin = username;
                passwordResetEmail.UserPassword = newPassword;
                passwordResetEmail.UserEmail = email;

                // recipient information
                passwordResetEmail.RecipientFullName = firstName + " " + lastName;
                passwordResetEmail.RecipientFirstName = firstName;
                passwordResetEmail.RecipientLogin = username;
                passwordResetEmail.RecipientEmail = email;

                // send mail
                passwordResetEmail.Send();

                return email;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DeleteBySessionId
        public static void DeleteBySessionId(string sessionId)
        {
            AsentiaDatabase DatabaseObject = new AsentiaDatabase(DatabaseType.SessionState);
            DatabaseObject.ClearParameters();

            // RETURN CODE / ERROR DESCRIPTION CODE
            DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

            // INPUTS
            DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);

            // execute the procedure
            try
            {
                DatabaseObject.ExecuteNonQuery("[SessionState.Remove]", true);
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(DatabaseObject.Command.Parameters["@Return_Code"].Value);               
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, string.Empty);
            }
            catch
            {
                throw;
            }
            finally
            {
                DatabaseObject.Dispose();
            }
        }
        #endregion
    }
}
