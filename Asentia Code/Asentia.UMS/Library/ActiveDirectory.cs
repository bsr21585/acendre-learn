﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.DirectoryServices;
using System.Transactions;
using System.Data;
using System.Web;
using Asentia.Common;

namespace Asentia.UMS.Library
{
    public class ActiveDirectory
    {
        #region Properties
        /// <summary>
        /// Name or IP Address of the Active Directory Server
        /// </summary>
        public string ServerAddress;

        /// <summary>
        /// Port to be used in the LDAP Query
        /// </summary>
        public string ServerPort;

        /// <summary>
        /// Distinguished Name of the Server
        /// </summary>
        public string DistinguishedName;

        /// <summary>
        /// Enable/Disable the import of users accounts information
        /// </summary>
        public bool UserAccountInformation;

        /// <summary>
        /// Security Groups And Memberships
        /// </summary>
        public bool SecurityGpsAndMemberships;

        /// <summary>
        /// Distribution Groups And Memberships
        /// </summary>
        public bool DistributionGpsAndMemberships;

        /// <summary>
        /// LDAP Users Query i.e. (&(objectClass=user))
        /// </summary>
        public string LDAPUserQuery;

        /// <summary>
        /// LDAP Groups Query, i.e. (&(objectClass=group))
        /// </summary>
        public string LDAPGroupQuery;

        /// <summary>
        /// Active Directory Admin Username
        /// </summary>
        public string ADUsername;

        /// <summary>
        /// Password for Active Directory Admin
        /// </summary>
        public string ADPassword;

        #endregion Properties

        #region _IsIncludedGroup
        private bool _IsIncludedGroup(string groupType)
        {
            switch(groupType)
            {
                //-2147483646 ~ Global Security Group
                case "-2147483646": return SecurityGpsAndMemberships;
                //-2147483644 ~ Local Security Group
                case "-2147483644": return SecurityGpsAndMemberships;
                //-2147483643 ~ BuiltIn Group
                case "-2147483643": return SecurityGpsAndMemberships;
                //-2147483640 ~ Universal Security Group
                case "-2147483640": return SecurityGpsAndMemberships;
                //2 ~ Global Distribution Group
                case "2": return DistributionGpsAndMemberships;
                //4 ~ Local Distribution Group
                case "4": return DistributionGpsAndMemberships;
                //8 ~ Universal Distribution Group
                case "8": return DistributionGpsAndMemberships;
                default: return false;
            }
        }
        #endregion

        #region _GetProperty
        private static string _GetProperty(SearchResult searchResult, string PropertyName)
        {
            if (searchResult.Properties.Contains(PropertyName))
            {
                return searchResult.Properties[PropertyName][0].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion

        #region SyncUsers
        public void SyncUsers()
        {
            
            #region ADSync
            if (string.IsNullOrWhiteSpace(this.ServerPort))
                this.ServerPort = "389";

            DirectoryEntry dirEntry = new DirectoryEntry("LDAP://" + this.ServerAddress + ":" + this.ServerPort + "/" + this.DistinguishedName, this.ADUsername, this.ADPassword);

            DirectorySearcher groupsSearch = new DirectorySearcher(dirEntry);

            groupsSearch.Filter = "(&(objectClass=group))";
            if(string.IsNullOrWhiteSpace(this.LDAPGroupQuery))
                groupsSearch.Filter = "(&(objectClass=group))";
            else
                groupsSearch.Filter = this.LDAPGroupQuery;

            DirectorySearcher usersSearch = new DirectorySearcher(dirEntry);
            if (string.IsNullOrWhiteSpace(this.LDAPUserQuery))
                usersSearch.Filter = "(&(&(objectClass=user)(objectClass=person)))";
            else
                usersSearch.Filter = this.LDAPUserQuery;

            //return;
            int currentGroupUser = 0;
            string currentGroupUserName = String.Empty;
            //string filename = System.IO.Path.GetTempPath() + "ADUsers.txt";
            string filename = HttpContext.Current.Server.MapPath("~") + "ADUsers.txt";
            try
            {
                int totalGroupsUsers = groupsSearch.FindAll().Count + usersSearch.FindAll().Count;
            using (TransactionScope scope = new TransactionScope())
            {
                    DataTable usersDataTable = new DataTable();
                    usersDataTable = Asentia.UMS.Library.User.GetGUIDs();

                    #region GroupDetails
                List<Group> groupsList = new List<Group>();
                foreach (SearchResult groupResult in groupsSearch.FindAll())
                {
                        currentGroupUser++;
                        using (global::System.IO.StreamWriter file = new global::System.IO.StreamWriter(filename, true))
                        {
                            file.WriteLine("Group###" + totalGroupsUsers.ToString() + "###" + _GetProperty(groupResult, "name") + "###" + currentGroupUser.ToString());
                            file.Close();
                        }
                    if (_IsIncludedGroup(_GetProperty(groupResult, "groupType")))
                    {
                        Group groupObject = new Group();
                        groupObject.Name = _GetProperty(groupResult, "name");
                            currentGroupUserName = groupObject.Name;
                        groupObject.ObjectGUID = groupResult.GetDirectoryEntry().Guid.ToString();
                        groupObject.DistinguishedName = _GetProperty(groupResult, "distinguishedName");
                        //groupObject.Save();
                        groupsList.Add(groupObject);
                    }
                }
                    #endregion                    
                    
                    #region UsersAll
                // get all entries from the active directory.
                // Last Name, name, initial, homepostaladdress, title, company etc..
                foreach (SearchResult userResult in usersSearch.FindAll())
                {
                        bool isUserAlreadyExist = false;

                        foreach(DataRow row in usersDataTable.Rows)
                        {
                            if (row["objectGUID"].ToString() == userResult.GetDirectoryEntry().Guid.ToString())
                            {
                                isUserAlreadyExist = true;
                                break;
                            }
                        }

                        if(isUserAlreadyExist == false){
                    User userObject = new User();
                    #region User Details
                        currentGroupUser++;

                        using (global::System.IO.StreamWriter file = new global::System.IO.StreamWriter(filename, true))
                        {
                            file.WriteLine("User###" + totalGroupsUsers.ToString() + "###" + _GetProperty(userResult, "distinguishedName") + "###" + currentGroupUser.ToString());
                            file.Close();
                        }

                        try
                        {
                            #region UserObject
                            

                    // Distinguished Name
                    userObject.DistinguishedName = _GetProperty(userResult, "distinguishedName");
                    // Login Name
                    userObject.Username = _GetProperty(userResult, "cn");
                            currentGroupUserName = userObject.Username;
                    // First Name
                    userObject.FirstName = _GetProperty(userResult, "givenName");
                    // Middle Initials
                    userObject.MiddleName = _GetProperty(userResult, "initials");
                    // Last Name
                    userObject.LastName = _GetProperty(userResult, "sn");
                    // Address
                    userObject.Address = _GetProperty(userResult, "homePostalAddress");
                    // company
                    userObject.Company = _GetProperty(userResult, "company");
                    //state
                    userObject.Province = _GetProperty(userResult, "st");
                    //city
                    userObject.City = _GetProperty(userResult, "l");
                    //country
                    userObject.Country = _GetProperty(userResult, "co");
                    //mobile phone
                    userObject.PhoneMobile = _GetProperty(userResult, "mobilePhone");
                    //work phone
                    userObject.PhoneWork = _GetProperty(userResult, "workPhone");
                    //home phone
                    userObject.PhoneHome = _GetProperty(userResult, "homePhone");
                    //pager phone
                    userObject.PhonePager = _GetProperty(userResult, "pagerPhone");
                    //postal code
                    userObject.PostalCode = _GetProperty(userResult, "postalCode");
                    // telephonenumber
                    userObject.PhonePrimary = _GetProperty(userResult, "telephoneNumber");
                    //extention
                    userObject.PhoneOther = _GetProperty(userResult, "otherTelephone");
                    //fax
                    userObject.PhoneFax = _GetProperty(userResult, "facsimileTelephoneNumber");
                    // email address
                    userObject.Email = _GetProperty(userResult, "mail");
                    // Field 00
                    userObject.Field00 = _GetProperty(userResult, "extensionAttribute0");
                    // Challenge Question
                    userObject.Field01 = _GetProperty(userResult, "extensionAttribute1");
                    // Challenge Response
                    userObject.Field02 = _GetProperty(userResult, "extensionAttribute2");
                    //Member Company
                    userObject.Field03 = _GetProperty(userResult, "extensionAttribute3");
                    // Company Relation ship Exits
                    userObject.Field04 = _GetProperty(userResult, "extensionAttribute4");
                    //status
                    userObject.Field05 = _GetProperty(userResult, "extensionAttribute5");
                    // Assigned Sales Person
                    userObject.Field06 = _GetProperty(userResult, "extensionAttribute6");
                    // Accept T and C
                    userObject.Field07 = _GetProperty(userResult, "extensionAttribute7");
                    // jobs
                    userObject.Field08 = _GetProperty(userResult, "extensionAttribute8");
                    // email over night
                    userObject.Field09 = _GetProperty(userResult, "extensionAttribute9");
                    
                    // date of account created
                    userObject.DtCreated = Convert.ToDateTime(_GetProperty(userResult, "whenCreated"));
                    // date of account changed
                    userObject.DtModified = Convert.ToDateTime(_GetProperty(userResult, "whenChanged"));
                    //object GUID
                    userObject.ObjectGUID = userResult.GetDirectoryEntry().Guid.ToString();
                    
                    // Temporary Values
                    //activation GUID
                    userObject.ActivationGUID = userResult.GetDirectoryEntry().Guid.ToString();
                    userObject.Password = RandomPassword.Generate(8, 20);
                    //account expiry
                    string strExpire = _GetProperty(userResult, "accountExpires");
                    if (!string.IsNullOrWhiteSpace(strExpire))
                    {
                        // special value for never expiring accounts
                        if (strExpire == "9223372036854775807")
                            userObject.DtExpires = null;
                        else
                        {
                            bool isExpiryTrue = false;
                            long dtExpiry = 0;
                            isExpiryTrue = Int64.TryParse(strExpire, out dtExpiry);
                            if (isExpiryTrue && dtExpiry != 0)
                            {
                                userObject.DtExpires = DateTime.FromFileTime(dtExpiry);
                            }
                            else
                            {
                                userObject.DtExpires = null;
                            }
                        }
                    }
                    //employee id
                    userObject.EmployeeId = _GetProperty(userResult, "employeeId");
                    //department
                    userObject.Department = _GetProperty(userResult, "department");
                    //division
                    userObject.Division = _GetProperty(userResult, "division");
                    //region
                    userObject.Region = _GetProperty(userResult, "region");
                    //job title
                    userObject.JobTitle = _GetProperty(userResult, "jobTitle");
                    //job class
                    userObject.JobClass = _GetProperty(userResult, "jobClass");
                    //gender
                    userObject.Gender = _GetProperty(userResult, "gender");
                    //race
                    userObject.Race = _GetProperty(userResult, "race");
                    //dob
                    string strDOB = _GetProperty(userResult, "dateOfBirth");
                    bool isDOBPresent = false;
                    long dtDOB = 0;
                    isDOBPresent = Int64.TryParse(strDOB, out dtDOB);
                    if (isDOBPresent && dtDOB != 0)
                    {
                        userObject.DtDOB = DateTime.FromFileTime(dtDOB);
                    }
                    else
                    {
                        userObject.DtDOB = null;
                    }
                    //hiring date
                    string strHiring = _GetProperty(userResult, "hiringDate");
                    bool isHiringPresent = false;
                    long dtHiring = 0;
                    isHiringPresent = Int64.TryParse(strHiring, out dtHiring);
                    if (isHiringPresent && dtHiring != 0)
                    {
                        userObject.DtHire = DateTime.FromFileTime(dtHiring);
                    }
                    else
                    {
                        userObject.DtHire = null;
                    }
                    //term date
                    string strTerm = _GetProperty(userResult, "termDate");
                    bool isTermPresent = false;
                    long dtTerm = 0;
                    isTermPresent = Int64.TryParse(strTerm, out dtTerm);
                    if (isTermPresent)
                    {
                        userObject.DtTerm = DateTime.FromFileTime(dtTerm);
                    }
                    else
                    {
                        userObject.DtTerm = null;
                    }
                    //timezone
                    userObject.IdTimezone = 60;
                    userObject.Save();
                    
                            #endregion
                        }
                        catch (Exception ex)
                        {                            
                            using (global::System.IO.StreamWriter file = new global::System.IO.StreamWriter(filename, true))
                            {
                                file.WriteLine("The User/Group: " + userObject.Username + " already exist in the system.");
                                file.Close();
                            }                           
                        }
                    #endregion User Details

                    #region User Groups
                    DataTable groupsToAssign = new DataTable();
                    groupsToAssign.Columns.Add("id", typeof(int));

                    int propertyCount = userResult.Properties["memberOf"].Count;
                    String dn;
                    int equalsIndex, commaIndex;

                    for (int propertyCounter = 0; propertyCounter < propertyCount; propertyCounter++)
                    {
                        dn = (string)userResult.Properties["memberOf"][propertyCounter];

                        equalsIndex = dn.IndexOf("=", 1);
                        commaIndex = dn.IndexOf(",", 1);
                        if (-1 == equalsIndex)
                        {
                            continue;
                        }
                        //userGroups.Add(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1));
                        if (groupsList.Any(x => x.Name == dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1)))
                            groupsToAssign.Rows.Add(groupsList.FirstOrDefault(x => x.Name == dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1)).Id);
                    }
                    if (groupsToAssign.Rows.Count > 0)
                        userObject.JoinGroups(groupsToAssign);

                    #endregion User Groups
                }
                    }
                    #endregion

                    try
                    {
                scope.Complete();
                        using (global::System.IO.StreamWriter file = new global::System.IO.StreamWriter(filename, true))
                        {
                            file.WriteLine("###ENDOFWEBSERVICE###");
                            file.Close();
                        }
                    }
                    catch (TransactionAbortedException ex)
                    {
                        using (global::System.IO.StreamWriter file = new global::System.IO.StreamWriter(filename, true))
                        {
                            file.WriteLine("###EXCEPTION###" + ex.Message);
                            file.Close();
                        }
                    }
                    catch (ApplicationException ex)
                    {
                        using (global::System.IO.StreamWriter file = new global::System.IO.StreamWriter(filename, true))
                        {
                            file.WriteLine("###EXCEPTION###" + ex.Message);
                            file.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                using (global::System.IO.StreamWriter file = new global::System.IO.StreamWriter(filename, true))
                {
                    file.WriteLine("###EXCEPTION###");
                    file.Close();
                }
            }
            #endregion

        }
        #endregion
    }
}
