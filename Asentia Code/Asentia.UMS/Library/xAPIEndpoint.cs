﻿using System;
using Asentia.Common;
using System.Data;
namespace Asentia.UMS.Library
{
    public class xAPIEndpoint
    {
         #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public xAPIEndpoint()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idxAPIoAuthConsumer">xAPI oAuth Consumer ID</param>
        public xAPIEndpoint(int idxAPIoAuthConsumer)
        {
            _Details(idxAPIoAuthConsumer);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[xAPIoAuthConsumer.GetGrid]";

        /// <summary>
        /// xAPI oAuth Consumer Id.
        /// </summary>
        public int IdxAPIoAuthConsumer;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Label.
        /// </summary>
        public string Label;

        /// <summary>
        /// Key
        /// </summary>
        public string Key;

        /// <summary>
        /// Secret
        /// </summary>
        public string Secret;

        /// <summary>
        /// Is token active or not.
        /// </summary>
        public bool? IsActive;    

        #endregion

        #region Methods

        #region Save
        /// <summary>
        /// Saves object data to the database.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseFieldNotUniqueException">
        /// Thrown when a field must be unique.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <exception cref="DatabaseFieldConstraintException">
        /// Thrown when a value passed into a stored procedure violates a business rule.
        /// </exception>
        /// <exception cref="DatabaseSpecifiedLanguageNotDefaultException">
        /// Thrown when a Database Specified Language Not Default Exception occurs in a stored procedure.
        /// </exception>
        /// <param name=""></param>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idxAPIoAuthConsumer", this.IdxAPIoAuthConsumer, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@label", this.Label, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@oAuthKey", this.Key, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@oAuthSecret", this.Secret, SqlDbType.NVarChar, 255, ParameterDirection.Input);           
            databaseObject.AddParameter("@isActive", this.IsActive, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[xAPIoAuthConsumer.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdxAPIoAuthConsumer = Convert.ToInt32(databaseObject.Command.Parameters["@idxAPIoAuthConsumer"].Value);

                return this.IdxAPIoAuthConsumer;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes an xAPI oAuth Consumer.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@OAuthTokens", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[xAPIoAuthConsumer.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idxAPIoAuthConsumer">xAPI oAuth Consumer Id</param>
        private void _Details(int idxAPIoAuthConsumer)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idxAPIoAuthConsumer", idxAPIoAuthConsumer, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@label", this.Label, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@oAuthKey", this.Key, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@oAuthSecret", this.Secret, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@isActive", this.IsActive, SqlDbType.Bit, 1, ParameterDirection.Output);
            
            try
            {
                databaseObject.ExecuteNonQuery("[xAPIoAuthConsumer.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdxAPIoAuthConsumer = idxAPIoAuthConsumer;
                this.IdSite = Convert.ToInt32(databaseObject.Command.Parameters["@idSite"].Value);
                this.Label = databaseObject.Command.Parameters["@label"].Value.ToString();
                this.Key = databaseObject.Command.Parameters["@oAuthKey"].Value.ToString();
                this.Secret =  databaseObject.Command.Parameters["@oAuthSecret"].Value.ToString();
                this.IsActive = databaseObject.Command.Parameters["@isActive"].Value == DBNull.Value ? (bool?)null : Convert.ToBoolean(databaseObject.Command.Parameters["@isActive"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
