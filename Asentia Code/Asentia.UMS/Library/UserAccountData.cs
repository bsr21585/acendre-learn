﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.UMS.Library
{
    #region UserAccountDataFileType ENUM
    /// <summary>
    /// The type of User Account Data file to be loaded/saved.
    /// </summary>
    public enum UserAccountDataFileType
    {
        Site,
        SiteTemp,
        DefaultOnly,
    }
    #endregion

    public class UserAccountData
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileType">the type of file to be loaded or saved</param>
        /// <param name="loadFile">load the User Account Data file?</param>
        /// <param name="forSaving">is this being instansiated for saving?</param>
        public UserAccountData(UserAccountDataFileType fileType, bool loadFile, bool forSaving)
        {
            this.FileType = fileType;
            this._ForSaving = forSaving;
            this._LoadFile = loadFile;
            this._SiteDefaultLanguageString = AsentiaSessionState.GlobalSiteObject.LanguageString;

            // if this is for saving, the file type cannot be the default file
            if (this._ForSaving)
            {
                if (this.FileType == UserAccountDataFileType.DefaultOnly)
                { throw new AsentiaException(); }
            }

            // load global rules
            this.GlobalUserFieldRules = UserFieldGlobalRules.GetAllRules();

            // load user account data from file
            if (this._LoadFile)
            { this._LoadDataFromFile(); }
        }

        /// <summary>
        /// Constructor - Called when this is being called from JobProcessor code. 
        /// </summary>
        /// <param name="fileType">the type of file to be loaded or saved</param>
        /// <param name="loadFile">load the User Account Data file?</param>
        /// <param name="forSaving">is this being instansiated for saving?</param>
        /// <param name="defaultDataFilePath">the path to the default user account data file</param>
        /// <param name="siteDataFilePath">the path to the site-configured user account data file</param>
        public UserAccountData(UserAccountDataFileType fileType, bool loadFile, bool forSaving, string defaultDataFilePath, string siteDataFilePath, string defaultLanguageString)
        {
            this.FileType = fileType;
            this._ForSaving = forSaving;
            this._LoadFile = loadFile;
            this._SiteDefaultLanguageString = defaultLanguageString;

            // if this is for saving, the file type cannot be the default file
            if (this._ForSaving)
            {
                if (this.FileType == UserAccountDataFileType.DefaultOnly)
                { throw new AsentiaException(); }
            }

            // load global rules
            this.GlobalUserFieldRules = UserFieldGlobalRules.GetAllRules();

            // load user account data from file
            if (this._LoadFile)
            { this._LoadDataFromFile(defaultDataFilePath, siteDataFilePath); }
        }
        #endregion

        #region Properties
        /// <summary>
        /// The file type to load from or save to.
        /// </summary>
        public UserAccountDataFileType FileType;

        /// <summary>
        /// Collection of language specific registration instructions.
        /// </summary>
        public List<RegistrationInstructionsLanguageSpecific> RegistrationInstructions;

        /// <summary>
        /// Collection of tabs.
        /// </summary>
        public List<Tab> Tabs;

        /// <summary>
        /// Collection of global rules for each field.
        /// These are the system imposed constraints on each field.
        /// </summary>
        public List<UserFieldGlobalRules> GlobalUserFieldRules;

        /// <summary>
        /// Collection of language specific user agreement.
        /// </summary>
        public List<UserAgreementLanguageSpecific> UserAgreement;


        /// <summary>
        /// Allow the user to upload files?
        /// </summary>
        public bool AllowUserToUploadFiles;
        #endregion

        #region Private Properties
        /// <summary>
        /// Should the class be instansiated with data from the User Account Data file?
        /// </summary>
        private bool _LoadFile;

        /// <summary>
        /// Was this class instansiated for saving a User Account Data file?
        /// </summary>
        private bool _ForSaving;

        /// <summary>
        /// The site's default language string.
        /// </summary>
        private string _SiteDefaultLanguageString;

        /// <summary>
        /// The path to the default User Account Data file.
        /// </summary>
        private string _DefaultUserAccountDataFile { get { return SitePathConstants.DEFAULT_SITE_CONFIG_ROOT + "UserAccountData.xml"; } }

        /// <summary>
        /// The path to the site's User Account Data file.
        /// </summary>
        private string _SiteUserAccountDataFile { get { return SitePathConstants.SITE_CONFIG_ROOT + "UserAccountData.xml"; } }

        /// <summary>
        /// The path to the site's temporary User Account Data file.
        /// </summary>
        private string _SiteTempUserAccountDataFile { get { return SitePathConstants.SITE_CONFIG_ROOT + "UserAccountData.temp.xml"; } }
        #endregion

        #region Classes
        #region RegistrationInstructionsLanguageSpecific
        /// <summary>
        /// Registration instructions - language specific.
        /// </summary>
        public class RegistrationInstructionsLanguageSpecific
        {
            public string LangString;
            public bool IsDefaultLanguage;
            public string HTML;
        }
        #endregion

        #region Tab
        /// <summary>
        /// Class that represents a "tab" in the User Account Data.
        /// Tabs have a label and contain user fields.
        /// </summary>
        public class Tab
        {
            public Tab()
            {
                this.LanguageSpecificProperties = new List<TabLanguageSpecificProperties>();
                this.UserFields = new List<UserField>();
            }

            public string Identifier;
            public List<TabLanguageSpecificProperties> LanguageSpecificProperties;
            public List<UserField> UserFields;
        }
        #endregion

        #region TabLanguageSpecificProperties
        /// <summary>
        /// Language specific properties of a tab.
        /// </summary>
        public class TabLanguageSpecificProperties
        {
            public string LangString;
            public bool IsDefaultLanguage;
            public string Label;
        }
        #endregion

        #region UserFieldForLabel
        public class UserFieldForLabel
        {
            public string Identifier;
            public string UserObjectPropertyName;
            public string Label;
            public InputType? InputType;
        }
        #endregion

        #region UserField
        /// <summary>
        /// Class that represents a "user field"  in the User Account Data.
        /// User fields belong to a tab.
        /// </summary>
        public class UserField
        {
            public UserField()
            {
                this.LanguageSpecificProperties = new List<UserFieldLanguageSpecificProperties>();
            }

            public string Identifier;
            public string UserObjectPropertyName;
            public bool IsRequired;
            public bool IsUnique;
            public bool RuleSetEligible;
            public bool LeaderboardRelativeToEligible;
            public bool RegistrationPageView;
            public bool RegistrationPageModify;
            public bool UserEditPageView;
            public bool UserEditPageModify;
            public bool AdministratorEditPageView;
            public bool AdministratorEditPageModify;
            public string RegularExpression;
            public InputType? InputType;
            public int MaxLength;
            public List<UserFieldLanguageSpecificProperties> LanguageSpecificProperties;
        }
        #endregion

        #region UserFieldLanguageSpecificProperties
        /// <summary>
        /// Language specific properties of a user field.
        /// </summary>
        public class UserFieldLanguageSpecificProperties
        {
            public string LangString;
            public bool IsDefaultLanguage;
            public bool AlphabetizeChoiceValues;
            public string ChoiceValues;
            public string Label;
            public string Description;
        }
        #endregion

        #region UserAgreementLanguageSpecific
        /// <summary>
        /// User agreement - language specific.
        /// </summary>
        public class UserAgreementLanguageSpecific
        {
            public string LangString;
            public bool IsDefaultLanguage;
            public string HTML;
        }
        #endregion
        #endregion

        #region Enums
        /// <summary>
        /// Represents data type of a user field.
        /// </summary>
        public enum DataType
        {
            String,
            Integer,
            Date,
            Boolean
        }

        /// <summary>
        /// Represents type of a user field.
        /// </summary>
        public enum InputType
        {
            TextField = 1,
            Date = 2,
            RadioButtons = 3,
            CheckBoxes = 4,
            SelectBoxSingleSelect = 5,
            SelectBoxMultipleSelect = 6,
            DropDownList = 7
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves this User Account Data object to file.
        /// </summary>
        public void Save(string[] fieldWithDateType)
        {
            if (!this._ForSaving)
            { throw new AsentiaException(_GlobalResources.ThisInstanceWasInstansiatedAsReadOnlyThereforeSaveCannotBeCalled); }

            if (this.FileType == UserAccountDataFileType.DefaultOnly)
            { throw new AsentiaException(_GlobalResources.TheDefaultUserAccountDataFileCannotBeOverwritten); }

            // build xml document
            XDocument userAccountDataDocument = new XDocument();

            // userAccountData node - document element
            XElement userAccountDataElement = new XElement("userAccountData");

            // loop through registration instructions and build xml for each one
            foreach (RegistrationInstructionsLanguageSpecific registrationInstructionsObject in this.RegistrationInstructions)
            {
                // registration instructions element and attribute
                XElement registrationInstructionsElement = new XElement("registrationInstructions", new XCData(registrationInstructionsObject.HTML));
                registrationInstructionsElement.Add(new XAttribute("lang", registrationInstructionsObject.LangString));

                // add registration instructions element to document element
                userAccountDataElement.Add(registrationInstructionsElement);
            }

            string columnForCleaning = string.Empty;

            // loop through tabs and build the xml for each one
            foreach (Tab tab in this.Tabs)
            {
                // tab element and identifier attribute
                XElement tabElement = new XElement("tab");
                tabElement.Add(new XAttribute("identifier", tab.Identifier));

                // tab labels
                foreach (TabLanguageSpecificProperties tabLanguageSpecificProperty in tab.LanguageSpecificProperties)
                {
                    XElement tabLabelElement = new XElement("label", new XCData(tabLanguageSpecificProperty.Label));
                    tabLabelElement.Add(new XAttribute("lang", tabLanguageSpecificProperty.LangString));
                    tabElement.Add(tabLabelElement);
                }

                // user fields
                foreach (UserAccountData.UserField userField in tab.UserFields)
                {
                    // attributes
                    XElement userFieldElement = new XElement("userField");
                    userFieldElement.Add(new XAttribute("identifier", userField.Identifier));
                    userFieldElement.Add(new XAttribute("isRequired", userField.IsRequired.ToString().ToLower()));
                    userFieldElement.Add(new XAttribute("registrationPageView", userField.RegistrationPageView.ToString().ToLower()));
                    userFieldElement.Add(new XAttribute("registrationPageModify", userField.RegistrationPageModify.ToString().ToLower()));
                    userFieldElement.Add(new XAttribute("userEditPageView", userField.UserEditPageView.ToString().ToLower()));
                    userFieldElement.Add(new XAttribute("userEditPageModify", userField.UserEditPageModify.ToString().ToLower()));
                    userFieldElement.Add(new XAttribute("administratorEditPageView", userField.AdministratorEditPageView.ToString().ToLower()));
                    userFieldElement.Add(new XAttribute("administratorEditPageModify", userField.AdministratorEditPageModify.ToString().ToLower()));

                    // regular expression
                    XElement regularExpressionElement = new XElement("regularExpression", new XCData(userField.RegularExpression));
                    userFieldElement.Add(regularExpressionElement);

                    // type properties
                    XElement typePropertiesElement = new XElement("typeProperties");

                    // for cleaning up date type column, compare the userField with the array fieldWithDateType to check whether it is pre-existing date field
                    if (userField.InputType == UserAccountData.InputType.Date && Array.IndexOf(fieldWithDateType, userField.Identifier) < 0)
                    {
                        columnForCleaning += userField.Identifier.ToString() + ",";
                    }

                    // input type
                    XElement typeElement = new XElement("type", new XCData(userField.InputType.ToString()));
                    typePropertiesElement.Add(typeElement);

                    // choice values
                    foreach (UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty in userField.LanguageSpecificProperties)
                    {
                        if (userFieldLanguageSpecificProperty.ChoiceValues != null)
                        {
                            XElement choiceValuesElement = new XElement("choiceValues", new XCData(userFieldLanguageSpecificProperty.ChoiceValues));
                            choiceValuesElement.Add(new XAttribute("alphabetize", userFieldLanguageSpecificProperty.AlphabetizeChoiceValues.ToString().ToLower()));
                            choiceValuesElement.Add(new XAttribute("lang", userFieldLanguageSpecificProperty.LangString));
                            typePropertiesElement.Add(choiceValuesElement);
                        }
                    }

                    // add type properties element
                    userFieldElement.Add(typePropertiesElement);

                    // labels
                    foreach (UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty in userField.LanguageSpecificProperties)
                    {
                        if (userFieldLanguageSpecificProperty.Label != null)
                        {
                            XElement userFieldLabelElement = new XElement("label", new XCData(userFieldLanguageSpecificProperty.Label));
                            userFieldLabelElement.Add(new XAttribute("lang", userFieldLanguageSpecificProperty.LangString));
                            userFieldElement.Add(userFieldLabelElement);
                        }
                    }

                    // descriptions
                    foreach (UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty in userField.LanguageSpecificProperties)
                    {
                        if (userFieldLanguageSpecificProperty.Description != null)
                        {
                            XElement userFieldLabelElement = new XElement("description", new XCData(userFieldLanguageSpecificProperty.Description));
                            userFieldLabelElement.Add(new XAttribute("lang", userFieldLanguageSpecificProperty.LangString));
                            userFieldElement.Add(userFieldLabelElement);
                        }
                    }

                    // add user field to tab element
                    tabElement.Add(userFieldElement);
                }

                // add tab element to document element
                userAccountDataElement.Add(tabElement);
            }

            if (!string.IsNullOrEmpty(columnForCleaning))
            {
                columnForCleaning = columnForCleaning.Substring(0, columnForCleaning.Length - 1);
                User.ClearTableColumns(columnForCleaning);
            }

            // add user account data element to document
            userAccountDataDocument.Add(userAccountDataElement);

            // loop through user agreement and build xml for each one
            foreach (UserAgreementLanguageSpecific userAgreementObject in this.UserAgreement)
            {
                // registration instructions element and attribute
                XElement userAgreementElement = new XElement("userAgreement", new XCData(userAgreementObject.HTML));
                userAgreementElement.Add(new XAttribute("lang", userAgreementObject.LangString));

                // add user agreement element to document element
                userAccountDataElement.Add(userAgreementElement);
            }

            // FILES CONFIGURATION

            // add allow user to upload files element to xml
            XElement allowUserToUploadFilesElement = new XElement("allowUserToUploadFiles", new XCData(this.AllowUserToUploadFiles.ToString().ToLower()));

            // add allow user to upload files element to document element
            userAccountDataElement.Add(allowUserToUploadFilesElement);

            // save the xml to file
            if (this.FileType == UserAccountDataFileType.Site)
            { userAccountDataDocument.Save(HttpContext.Current.Server.MapPath(this._SiteUserAccountDataFile)); }
            else if (this.FileType == UserAccountDataFileType.SiteTemp)
            { userAccountDataDocument.Save(HttpContext.Current.Server.MapPath(this._SiteTempUserAccountDataFile)); }
            else
            { }
        }
        #endregion

        #region GetRegistrationInstructionsHTMLInLanguage
        /// <summary>
        /// Gets the registration instructions HTML in the specified language.
        /// </summary>
        /// <param name="languageString">language code</param>
        /// <returns>string</returns>
        public string GetRegistrationInstructionsHTMLInLanguage(string languageString)
        {
            string defaultHTML = null;
            string html = null;

            // get the default language html and html for specified language
            foreach (RegistrationInstructionsLanguageSpecific registrationInstructionObject in this.RegistrationInstructions)
            {
                if (registrationInstructionObject.IsDefaultLanguage)
                { defaultHTML = registrationInstructionObject.HTML; }

                if (registrationInstructionObject.LangString == languageString)
                { html = registrationInstructionObject.HTML; }
            }

            // if we found a label in the language, return it
            if (html != null)
            { return html; }

            // if the default label is null, throw an exception
            if (defaultHTML == null)
            { throw new AsentiaException(_GlobalResources.DefaultLanguagePropertyDoesNotExistForRegistrationInstructions); }

            return defaultHTML;
        }
        #endregion

        #region GetTabLabelInLanguage
        /// <summary>
        /// Gets a tab label in the specified language.
        /// </summary>
        /// <param name="tab">tab object</param>
        /// <param name="languageString">language code</param>
        /// <returns>string</returns>
        public string GetTabLabelInLanguage(Tab tab, string languageString)
        {
            string defaultLabel = null;
            string label = null;

            // get the default language label and label for specified language
            foreach (TabLanguageSpecificProperties tabLanguageSpecificProperty in tab.LanguageSpecificProperties)
            {
                if (tabLanguageSpecificProperty.IsDefaultLanguage)
                { defaultLabel = tabLanguageSpecificProperty.Label; }

                if (tabLanguageSpecificProperty.LangString == languageString)
                { label = tabLanguageSpecificProperty.Label; }
            }

            // if we found a label in the language, return it
            if (label != null)
            { return label; }

            // if the default label is null, throw an exception
            if (defaultLabel == null)
            { throw new AsentiaException(_GlobalResources.DefaultLanguagePropertyDoesNotExistForTab + " \"" + tab.Identifier + "\""); }

            return defaultLabel;
        }
        #endregion

        #region GetUserFieldLabelInLanguage
        /// <summary>
        /// Gets a user field label in the specified language.
        /// </summary>
        /// <param name="userField">user field object</param>
        /// <param name="languageString">language code</param>
        /// <returns>string</returns>
        public string GetUserFieldLabelInLanguage(UserField userField, string languageString)
        {
            string defaultLabel = null;
            string label = null;

            // get the default language label and label for specified language
            foreach (UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty in userField.LanguageSpecificProperties)
            {
                if (userFieldLanguageSpecificProperty.IsDefaultLanguage)
                { defaultLabel = userFieldLanguageSpecificProperty.Label; }

                if (userFieldLanguageSpecificProperty.LangString == languageString)
                { label = userFieldLanguageSpecificProperty.Label; }
            }

            // if we found a label in the language, return it
            if (label != null)
            { return label; }

            // if the default label is null, throw an exception
            if (defaultLabel == null)
            { throw new AsentiaException(_GlobalResources.DefaultLanguagePropertyDoesNotExistForUserField + " \"" + userField.Identifier + "\""); }

            return defaultLabel;
        }
        #endregion

        #region GetUserFieldDescriptionInLanguage
        /// <summary>
        /// Gets a user field description in the specified language.
        /// </summary>
        /// <param name="userField">user field object</param>
        /// <param name="languageString">language code</param>
        /// <returns>string</returns>
        public string GetUserFieldDescriptionInLanguage(UserField userField, string languageString)
        {
            string defaultDescription = null;
            string description = null;

            // get the default language label and label for specified language
            foreach (UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty in userField.LanguageSpecificProperties)
            {
                if (userFieldLanguageSpecificProperty.IsDefaultLanguage)
                { defaultDescription = userFieldLanguageSpecificProperty.Description; }

                if (userFieldLanguageSpecificProperty.LangString == languageString)
                { description = userFieldLanguageSpecificProperty.Description; }
            }

            // if we found a label in the language, return it
            if (description != null)
            { return description; }

            // if the default label is null, throw an exception
            if (defaultDescription == null)
            { throw new AsentiaException(_GlobalResources.DefaultLanguagePropertyDoesNotExistForUserField + " \"" + userField.Identifier + "\""); }

            return defaultDescription;
        }
        #endregion

        #region GetUserFieldChoiceValuesInLanguage
        /// <summary>
        /// Gets a user field's choice values in the specified language.
        /// </summary>
        /// <param name="userField">user field object</param>
        /// <param name="languageString">language code</param>
        /// <returns>string</returns>
        public string GetUserFieldChoiceValuesInLanguage(UserField userField, string languageString)
        {
            string defaultChoiceValues = null;
            string choiceValues = null;

            // get the default language label and label for specified language
            foreach (UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty in userField.LanguageSpecificProperties)
            {
                if (userFieldLanguageSpecificProperty.IsDefaultLanguage)
                { defaultChoiceValues = userFieldLanguageSpecificProperty.ChoiceValues; }

                if (userFieldLanguageSpecificProperty.LangString == languageString)
                { choiceValues = userFieldLanguageSpecificProperty.ChoiceValues; }
            }

            // if we found a label in the language, return it
            if (choiceValues != null)
            { return choiceValues; }

            // if the default label is null, throw an exception
            if (defaultChoiceValues == null)
            { throw new AsentiaException(_GlobalResources.DefaultLanguagePropertyDoesNotExistForUserField + " \"" + userField.Identifier + "\""); }

            return defaultChoiceValues;
        }
        #endregion

        #region GetUserFieldChoiceValuesAlphabetizeSettingInLanguage
        /// <summary>
        /// Gets a user field's alphabetize choice values for the specified language.
        /// </summary>
        /// <param name="userField">user field object</param>
        /// <param name="languageString">language code</param>
        /// <returns>boolean</returns>
        public bool GetUserFieldChoiceValuesAlphabetizeSettingInLanguage(UserField userField, string languageString)
        {
            bool choiceValuesAlphabetizeSetting = false;

            foreach (UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty in userField.LanguageSpecificProperties)
            {
                if (userFieldLanguageSpecificProperty.LangString == languageString)
                { choiceValuesAlphabetizeSetting = userFieldLanguageSpecificProperty.AlphabetizeChoiceValues; }
            }

            return choiceValuesAlphabetizeSetting;
        }
        #endregion

        #region GetUserAgreementHTMLInLanguage
        /// <summary>
        /// Gets the user agreement HTML in the specified language.
        /// </summary>
        /// <param name="languageString">language code</param>
        /// <returns>string</returns>
        public string GetUserAgreementHTMLInLanguage(string languageString)
        {
            string defaultHTML = null;
            string html = null;

            // get the default language html and html for specified language
            foreach (UserAgreementLanguageSpecific userAgreementObject in this.UserAgreement)
            {
                if (userAgreementObject.IsDefaultLanguage)
                { defaultHTML = userAgreementObject.HTML; }

                if (userAgreementObject.LangString == languageString)
                { html = userAgreementObject.HTML; }
            }

            // if we found a label in the language, return it
            if (html != null)
            { return html; }

            // if the default label is null, throw an exception
            if (defaultHTML == null)
            { throw new AsentiaException(_GlobalResources.DefaultLanguagePropertyDoesNotExistForUserAgreement); }

            return defaultHTML;
        }
        #endregion

        #region GetAdminViewableFieldLabelsInLanguage
        /// <summary>
        /// Gets a List of all admin viewable fields and labels.
        /// Used for user field selections in reporting, and other places where an admin viewable
        /// list of user fields is needed.
        /// </summary>
        /// <param name="languageString">the language for the labels</param>
        /// <returns>List of user fields and labels that are admin viewable</returns>
        public List<UserFieldForLabel> GetAdminViewableFieldLabelsInLanguage(string languageString)
        {
            List<UserFieldForLabel> userFieldsForLabels = new List<UserFieldForLabel>();

            foreach (Tab tab in this.Tabs)
            {
                foreach (UserField userField in tab.UserFields)
                {
                    if (userField.AdministratorEditPageView)
                    {
                        UserFieldForLabel userFieldForLabel = new UserFieldForLabel();
                        userFieldForLabel.Identifier = userField.Identifier;
                        userFieldForLabel.UserObjectPropertyName = userField.UserObjectPropertyName;
                        userFieldForLabel.Label = this.GetUserFieldLabelInLanguage(userField, languageString);
                        userFieldForLabel.InputType = userField.InputType;

                        userFieldsForLabels.Add(userFieldForLabel);
                    }
                }
            }

            return userFieldsForLabels;
        }
        #endregion

        #region GetUserViewableFieldLabelsInLanguage
        /// <summary>
        /// Gets a List of all user viewable fields and labels.
        /// Used for user field selections in certificates, and other places where a user viewable
        /// list of user fields is needed.
        /// </summary>
        /// <param name="languageString">the language for the labels</param>
        /// <returns>List of user fields and labels that are user viewable</returns>
        public List<UserFieldForLabel> GetUserViewableFieldLabelsInLanguage(string languageString)
        {
            List<UserFieldForLabel> userFieldsForLabels = new List<UserFieldForLabel>();

            foreach (Tab tab in this.Tabs)
            {
                foreach (UserField userField in tab.UserFields)
                {
                    if (userField.UserEditPageView)
                    {
                        UserFieldForLabel userFieldForLabel = new UserFieldForLabel();
                        userFieldForLabel.Identifier = userField.Identifier;
                        userFieldForLabel.UserObjectPropertyName = userField.UserObjectPropertyName;
                        userFieldForLabel.Label = this.GetUserFieldLabelInLanguage(userField, languageString);
                        userFieldForLabel.InputType = userField.InputType;

                        userFieldsForLabels.Add(userFieldForLabel);
                    }
                }
            }

            return userFieldsForLabels;
        }
        #endregion

        #region GetRuleSetEligibleFieldLabelsInLanguage
        /// <summary>
        /// Gets a List of all ruleset eligible and admin viewable fields and labels.
        /// Used for user field selections in auto-join rulesets.
        /// </summary>
        /// <param name="languageString">the language for the labels</param>
        /// <returns>List of user fields and labels that are ruleset eligible and admin viewable</returns>
        public List<UserFieldForLabel> GetRuleSetEligibleFieldLabelsInLanguage(string languageString)
        {
            List<UserFieldForLabel> ruleSetEligibleFields = new List<UserFieldForLabel>();

            foreach (Tab tab in this.Tabs)
            {
                foreach (UserField userField in tab.UserFields)
                {
                    if (userField.AdministratorEditPageView && userField.RuleSetEligible)
                    {
                        UserFieldForLabel userFieldForLabel = new UserFieldForLabel();
                        userFieldForLabel.Identifier = userField.Identifier;
                        userFieldForLabel.UserObjectPropertyName = userField.UserObjectPropertyName;
                        userFieldForLabel.Label = this.GetUserFieldLabelInLanguage(userField, languageString);
                        userFieldForLabel.InputType = userField.InputType;

                        ruleSetEligibleFields.Add(userFieldForLabel);
                    }
                }
            }

            return ruleSetEligibleFields;
        }
        #endregion

        #region GetLeaderboardRelativeToEligibleFieldLabelsInLanguage
        /// <summary>
        /// Gets a List of all leaderboard "relative to" eligible and user viewable fields and labels.
        /// Used for user field selections in the leaderboard "relative to" filter.
        /// </summary>
        /// <param name="languageString">the language for the labels</param>
        /// <returns>List of user fields and labels that are leaderboard "relative to" eligible and user viewable</returns>
        public List<UserFieldForLabel> GetLeaderboardRelativeToEligibleFieldLabelsInLanguage(string languageString)
        {
            List<UserFieldForLabel> leaderboardRelativeToEligibleFields = new List<UserFieldForLabel>();

            foreach (Tab tab in this.Tabs)
            {
                foreach (UserField userField in tab.UserFields)
                {
                    if (userField.UserEditPageView && userField.LeaderboardRelativeToEligible)
                    {
                        UserFieldForLabel userFieldForLabel = new UserFieldForLabel();
                        userFieldForLabel.Identifier = userField.Identifier;
                        userFieldForLabel.UserObjectPropertyName = userField.UserObjectPropertyName;
                        userFieldForLabel.Label = this.GetUserFieldLabelInLanguage(userField, languageString);
                        userFieldForLabel.InputType = userField.InputType;

                        leaderboardRelativeToEligibleFields.Add(userFieldForLabel);
                    }
                }
            }

            return leaderboardRelativeToEligibleFields;
        }
        #endregion
        #endregion

        #region Private Methods
        #region _LoadDataFromFile
        /// <summary>
        /// Loads User Account Data from file into the object.
        /// </summary>
        private void _LoadDataFromFile(string defaultDataFilePath = null, string siteDataFilePath = null)
        {
            // before doing anything, make sure the global rules have been loaded
            // we need them to load the user account data as validations are done
            // against global constraints
            this._VerifyGlobalRulesAreLoaded();

            // declare userAccountData XmlDocument
            XmlDocument userAccountDataFile = new XmlDocument();

            // load XML based on the file type to be loaded
            if (String.IsNullOrWhiteSpace(defaultDataFilePath) && String.IsNullOrWhiteSpace(siteDataFilePath))
            {
                switch (this.FileType)
                {
                    case UserAccountDataFileType.DefaultOnly:
                        userAccountDataFile.Load(HttpContext.Current.Server.MapPath(this._DefaultUserAccountDataFile));
                        break;
                    case UserAccountDataFileType.Site:
                        if (File.Exists(HttpContext.Current.Server.MapPath(this._SiteUserAccountDataFile)))
                        { userAccountDataFile.Load(HttpContext.Current.Server.MapPath(this._SiteUserAccountDataFile)); }
                        else
                        { userAccountDataFile.Load(HttpContext.Current.Server.MapPath(this._DefaultUserAccountDataFile)); }
                        break;
                    case UserAccountDataFileType.SiteTemp:
                        if (File.Exists(HttpContext.Current.Server.MapPath(this._SiteTempUserAccountDataFile)))
                        { userAccountDataFile.Load(HttpContext.Current.Server.MapPath(this._SiteTempUserAccountDataFile)); }
                        else
                        {
                            if (File.Exists(HttpContext.Current.Server.MapPath(this._SiteUserAccountDataFile)))
                            { userAccountDataFile.Load(HttpContext.Current.Server.MapPath(this._SiteUserAccountDataFile)); }
                            else
                            { userAccountDataFile.Load(HttpContext.Current.Server.MapPath(this._DefaultUserAccountDataFile)); }
                        }
                        break;
                }
            }
            else
            {
                switch (this.FileType)
                {
                    case UserAccountDataFileType.DefaultOnly:
                        userAccountDataFile.Load(defaultDataFilePath);
                        break;
                    case UserAccountDataFileType.Site:
                        if (File.Exists(siteDataFilePath))
                        { userAccountDataFile.Load(siteDataFilePath); }
                        else
                        { userAccountDataFile.Load(defaultDataFilePath); }
                        break;
                    case UserAccountDataFileType.SiteTemp:
                        if (File.Exists(siteDataFilePath))
                        { userAccountDataFile.Load(siteDataFilePath); }
                        else
                        {
                            if (File.Exists(siteDataFilePath))
                            { userAccountDataFile.Load(siteDataFilePath); }
                            else
                            { userAccountDataFile.Load(defaultDataFilePath); }
                        }
                        break;
                }
            }

            // instansiate the registration instructions list
            this.RegistrationInstructions = new List<RegistrationInstructionsLanguageSpecific>();

            // get the registration instruction nodes
            XmlNodeList registrationInstructionsNodes = userAccountDataFile.SelectNodes("userAccountData/registrationInstructions");

            foreach (XmlNode registrationInstructionsNode in registrationInstructionsNodes)
            {
                RegistrationInstructionsLanguageSpecific registrationInstructionsObject = new RegistrationInstructionsLanguageSpecific();

                // get the lang attribute
                registrationInstructionsObject.LangString = registrationInstructionsNode.Attributes["lang"].Value;

                // set whether or not this lang is the default site's language
                if (registrationInstructionsObject.LangString == this._SiteDefaultLanguageString)
                { registrationInstructionsObject.IsDefaultLanguage = true; }
                else
                { registrationInstructionsObject.IsDefaultLanguage = false; }

                // get the html value
                registrationInstructionsObject.HTML = registrationInstructionsNode.InnerText;

                // add this to the collection of registration instructions
                this.RegistrationInstructions.Add(registrationInstructionsObject);
            }

            // instansiate the tabs list
            this.Tabs = new List<Tab>();

            // get the tab nodes
            XmlNodeList tabNodes = userAccountDataFile.SelectNodes("userAccountData/tab");

            foreach (XmlNode tabNode in tabNodes)
            {
                // create a new Tab object and set its identifier
                Tab tabObject = new Tab();
                tabObject.Identifier = tabNode.Attributes["identifier"].Value;

                // get label nodes and populate TabLanguageSpecificProperties object
                XmlNodeList tabLabelNodes = tabNode.SelectNodes("label");

                foreach (XmlNode tabLabelNode in tabLabelNodes)
                {
                    TabLanguageSpecificProperties tabLanguageSpecificPropertiesObject = new TabLanguageSpecificProperties();

                    // get the lang attribute
                    tabLanguageSpecificPropertiesObject.LangString = tabLabelNode.Attributes["lang"].Value;

                    // set whether or not this lang is the default site's language
                    if (tabLanguageSpecificPropertiesObject.LangString == this._SiteDefaultLanguageString)
                    { tabLanguageSpecificPropertiesObject.IsDefaultLanguage = true; }
                    else
                    { tabLanguageSpecificPropertiesObject.IsDefaultLanguage = false; }

                    // get the label value
                    tabLanguageSpecificPropertiesObject.Label = tabLabelNode.InnerText;

                    // add this language specific property to the LanguageSpecificProperties collection
                    tabObject.LanguageSpecificProperties.Add(tabLanguageSpecificPropertiesObject);
                }

                // get userField nodes and populate UserFields
                XmlNodeList userFieldNodes = tabNode.SelectNodes("userField");

                foreach (XmlNode userFieldNode in userFieldNodes)
                {
                    UserField userField = new UserField();
                    userField.Identifier = userFieldNode.Attributes["identifier"].Value;

                    // get global ruleset for field
                    UserFieldGlobalRules globalRuleSetForField = this._GetGlobalRulesForField(userField.Identifier);

                    // set the user object property name from the global field information
                    // this will then be cascaded down into UserField object(s)
                    userField.UserObjectPropertyName = globalRuleSetForField.UserObjectPropertyName;

                    if (globalRuleSetForField == null)
                    { throw new AsentiaException(_GlobalResources.CouldNotFintGlobalRulesForUserField + " \"" + userField.Identifier + "\""); }

                    // get user field attributes
                    userField.IsRequired = this._ProcessUserFieldIsRequired(globalRuleSetForField, userFieldNode.Attributes["isRequired"].Value);
                    userField.IsUnique = this._ProcessUserFieldIsUnique(globalRuleSetForField);
                    userField.RuleSetEligible = this._ProcessUserFieldRuleSetEligible(globalRuleSetForField);
                    userField.LeaderboardRelativeToEligible = this._ProcessUserFieldLeaderboardRelativeToEligible(globalRuleSetForField);
                    userField.RegistrationPageView = this._ProcessUserFieldRegistrationPageView(globalRuleSetForField, userFieldNode.Attributes["registrationPageView"].Value);
                    userField.RegistrationPageModify = this._ProcessUserFieldRegistrationPageModify(globalRuleSetForField, userFieldNode.Attributes["registrationPageModify"].Value);
                    userField.UserEditPageView = this._ProcessUserFieldUserEditPageView(globalRuleSetForField, userFieldNode.Attributes["userEditPageView"].Value);
                    userField.UserEditPageModify = this._ProcessUserFieldUserEditPageModify(globalRuleSetForField, userFieldNode.Attributes["userEditPageModify"].Value);
                    userField.AdministratorEditPageView = this._ProcessUserFieldAdministratorEditPageView(globalRuleSetForField, userFieldNode.Attributes["administratorEditPageView"].Value);
                    userField.AdministratorEditPageModify = this._ProcessUserFieldAdministratorEditPageModify(globalRuleSetForField, userFieldNode.Attributes["administratorEditPageModify"].Value);

                    // get user field regular expression
                    userField.RegularExpression = this._ProcessUserFieldRegularExpression(globalRuleSetForField, userFieldNode.SelectSingleNode("regularExpression").InnerText);

                    // get user field input type
                    userField.InputType = this._ProcessUserFieldInputType(globalRuleSetForField, userFieldNode.SelectSingleNode("typeProperties/type").InnerText);

                    // get user field max length
                    userField.MaxLength = this._ProcessUserFieldMaxLength(globalRuleSetForField);

                    // get and process language specific properties
                    XmlNodeList userFieldLabelNodes = userFieldNode.SelectNodes("label");
                    XmlNodeList userFieldDescriptionNodes = userFieldNode.SelectNodes("description");
                    XmlNodeList choiceValuesNodes = userFieldNode.SelectNodes("typeProperties/choiceValues");

                    this._ProcessUserFieldLabels(ref userField.LanguageSpecificProperties, userFieldLabelNodes);
                    this._ProcessUserFieldDescriptions(ref userField.LanguageSpecificProperties, userFieldDescriptionNodes);
                    this._ProcessUserFieldChoiceValues(ref userField.LanguageSpecificProperties, choiceValuesNodes);

                    // add the user field to the tab
                    tabObject.UserFields.Add(userField);
                }

                // add the tab to the Tabs collection
                this.Tabs.Add(tabObject);
            }

            // reconcile user fields to default user account data
            if (String.IsNullOrWhiteSpace(defaultDataFilePath) && String.IsNullOrWhiteSpace(siteDataFilePath) && (this.FileType == UserAccountDataFileType.Site || this.FileType == UserAccountDataFileType.SiteTemp))
            { this._ReconcileMissingFieldsFromDefault(); }

            // instansiate the user agreement list
            this.UserAgreement = new List<UserAgreementLanguageSpecific>();

            // get the user agreement nodes
            XmlNodeList userAgreementNodes = userAccountDataFile.SelectNodes("userAccountData/userAgreement");

            foreach (XmlNode userAgreementNode in userAgreementNodes)
            {
                UserAgreementLanguageSpecific userAgreementObject = new UserAgreementLanguageSpecific();

                // get the lang attribute
                userAgreementObject.LangString = userAgreementNode.Attributes["lang"].Value;

                // set whether or not this lang is the default site's language
                if (userAgreementObject.LangString == this._SiteDefaultLanguageString)
                { userAgreementObject.IsDefaultLanguage = true; }
                else
                { userAgreementObject.IsDefaultLanguage = false; }

                // get the html value
                userAgreementObject.HTML = userAgreementNode.InnerText;

                // add this to the collection of user agreement text
                this.UserAgreement.Add(userAgreementObject);
            }

            // GET THE FILES CONFIGURATION

            // get the allow user to upload files node
            XmlNodeList allowUserToUploadFilesNodes = userAccountDataFile.SelectNodes("userAccountData/allowUserToUploadFiles");

            foreach (XmlNode allowUserToUploadFilesNode in allowUserToUploadFilesNodes)
            {
                // get the value
                this.AllowUserToUploadFiles = Convert.ToBoolean(allowUserToUploadFilesNode.InnerText);
            }
        }
        #endregion

        #region _ReconcileMissingFieldsFromDefault
        /// <summary>
        /// Reconciles missing data fields from default user field configuration. 
        /// This is for when fields are added to the system, but the portal's user account data does not reflect them.
        /// </summary>
        private void _ReconcileMissingFieldsFromDefault()
        {
            // load the default user account data file
            XmlDocument userAccountDataFile = new XmlDocument();
            userAccountDataFile.Load(HttpContext.Current.Server.MapPath(this._DefaultUserAccountDataFile));

            // get the tab nodes
            XmlNodeList tabNodes = userAccountDataFile.SelectNodes("userAccountData/tab");

            foreach (XmlNode tabNode in tabNodes)
            {
                // get userField nodes and populate UserFields
                XmlNodeList userFieldNodes = tabNode.SelectNodes("userField");

                foreach (XmlNode userFieldNode in userFieldNodes)
                {
                    // get the user field's identifier so we can see if it exists
                    string userFieldIdentifier = userFieldNode.Attributes["identifier"].Value;

                    // if the user field identifier is not found in the user object, add it to the user fields in the last tab
                    // this will effectively put the field at the bottom of the form
                    if (!this._CheckUserFieldExistsInLoadedData(userFieldIdentifier))
                    {
                        // declare a UserField object
                        UserField userField = new UserField();
                        userField.Identifier = userFieldIdentifier;

                        // get global ruleset for field
                        UserFieldGlobalRules globalRuleSetForField = this._GetGlobalRulesForField(userField.Identifier);

                        // set the user object property name from the global field information
                        // this will then be cascaded down into UserField object(s)
                        userField.UserObjectPropertyName = globalRuleSetForField.UserObjectPropertyName;

                        if (globalRuleSetForField == null)
                        { throw new AsentiaException(_GlobalResources.CouldNotFintGlobalRulesForUserField + " \"" + userField.Identifier + "\""); }

                        // get user field attributes
                        userField.IsRequired = this._ProcessUserFieldIsRequired(globalRuleSetForField, userFieldNode.Attributes["isRequired"].Value);
                        userField.IsUnique = this._ProcessUserFieldIsUnique(globalRuleSetForField);
                        userField.RuleSetEligible = this._ProcessUserFieldRuleSetEligible(globalRuleSetForField);
                        userField.LeaderboardRelativeToEligible = this._ProcessUserFieldLeaderboardRelativeToEligible(globalRuleSetForField);
                        userField.RegistrationPageView = this._ProcessUserFieldRegistrationPageView(globalRuleSetForField, userFieldNode.Attributes["registrationPageView"].Value);
                        userField.RegistrationPageModify = this._ProcessUserFieldRegistrationPageModify(globalRuleSetForField, userFieldNode.Attributes["registrationPageModify"].Value);
                        userField.UserEditPageView = this._ProcessUserFieldUserEditPageView(globalRuleSetForField, userFieldNode.Attributes["userEditPageView"].Value);
                        userField.UserEditPageModify = this._ProcessUserFieldUserEditPageModify(globalRuleSetForField, userFieldNode.Attributes["userEditPageModify"].Value);
                        userField.AdministratorEditPageView = this._ProcessUserFieldAdministratorEditPageView(globalRuleSetForField, userFieldNode.Attributes["administratorEditPageView"].Value);
                        userField.AdministratorEditPageModify = this._ProcessUserFieldAdministratorEditPageModify(globalRuleSetForField, userFieldNode.Attributes["administratorEditPageModify"].Value);

                        // get user field regular expression
                        userField.RegularExpression = this._ProcessUserFieldRegularExpression(globalRuleSetForField, userFieldNode.SelectSingleNode("regularExpression").InnerText);

                        // get user field input type
                        userField.InputType = this._ProcessUserFieldInputType(globalRuleSetForField, userFieldNode.SelectSingleNode("typeProperties/type").InnerText);

                        // get user field max length
                        userField.MaxLength = this._ProcessUserFieldMaxLength(globalRuleSetForField);

                        // get and process language specific properties
                        XmlNodeList userFieldLabelNodes = userFieldNode.SelectNodes("label");
                        XmlNodeList userFieldDescriptionNodes = userFieldNode.SelectNodes("description");
                        XmlNodeList choiceValuesNodes = userFieldNode.SelectNodes("typeProperties/choiceValues");

                        this._ProcessUserFieldLabels(ref userField.LanguageSpecificProperties, userFieldLabelNodes);
                        this._ProcessUserFieldDescriptions(ref userField.LanguageSpecificProperties, userFieldDescriptionNodes);
                        this._ProcessUserFieldChoiceValues(ref userField.LanguageSpecificProperties, choiceValuesNodes);

                        // add the user field to the last tab
                        this.Tabs[this.Tabs.Count - 1].UserFields.Add(userField);
                    }
                }
            }
        }
        #endregion

        #region _CheckUserFieldExistsInLoadedData
        /// <summary>
        /// Checks if a user field exists in the loaded user account data.
        /// </summary>
        private bool _CheckUserFieldExistsInLoadedData(string userFieldIdentifier)
        {
            bool doesUserFieldExist = false;

            foreach (Tab tab in this.Tabs)
            {
                foreach (UserField userField in tab.UserFields)
                {
                    if (userField.Identifier == userFieldIdentifier)
                    { return true; }
                }
            }

            return doesUserFieldExist;
        }
        #endregion

        #region _VerifyGlobalRulesAreLoaded
        /// <summary>
        /// Provides verification that we have loaded the "global rules."
        /// An exception is thrown if they haven't been loaded.
        /// </summary>
        private void _VerifyGlobalRulesAreLoaded()
        {
            if (this.GlobalUserFieldRules == null || this.GlobalUserFieldRules.Count <= 0)
            { throw new AsentiaException(); }
        }
        #endregion

        #region _GetGlobalRulesForField
        /// <summary>
        /// Gets the "global rules" for a user field from the GlobalUserFieldRules collection.
        /// </summary>
        /// <param name="fieldIdentifier">identifier of the user field</param>
        /// <returns>UserFieldGlobalRules for field</returns>
        private UserFieldGlobalRules _GetGlobalRulesForField(string fieldIdentifier)
        {
            UserFieldGlobalRules globalRuleSetForField = null;

            foreach (UserFieldGlobalRules ruleSet in this.GlobalUserFieldRules)
            {
                if (ruleSet.Identifier == fieldIdentifier)
                { globalRuleSetForField = ruleSet; }
            }

            return globalRuleSetForField;
        }
        #endregion

        #region _ProcessUserFieldIsRequired
        /// <summary>
        /// Processes the "IsRequired" flag for a user field based on global rules and the attribute
        /// value for the user field's node.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <param name="attributeValue">the value of the isRequired attribute</param>
        /// <returns>true/false</returns>
        private bool _ProcessUserFieldIsRequired(UserFieldGlobalRules globalRuleSetForField, string attributeValue)
        {
            if (globalRuleSetForField.AlwaysRequired)
            { return true; }

            return Convert.ToBoolean(attributeValue);
        }
        #endregion

        #region _ProcessUserFieldIsUnique
        /// <summary>
        /// Processes the "IsUnique" flag for a user field based on global rules.
        /// IsUnique is only set at the global rule level, it is not configurable
        /// per a site's user account data.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <returns>true/false</returns>
        private bool _ProcessUserFieldIsUnique(UserFieldGlobalRules globalRuleSetForField)
        {
            return globalRuleSetForField.IsUnique;
        }
        #endregion

        #region _ProcessUserFieldRuleSetEligible
        /// <summary>
        /// Processes the "RuleSetEligible" flag for a user field based on global rules.
        /// RuleSetEligible is only set at the global rule level, it is not configurable
        /// per a site's user account data.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <returns>true/false</returns>
        private bool _ProcessUserFieldRuleSetEligible(UserFieldGlobalRules globalRuleSetForField)
        {
            return globalRuleSetForField.RuleSetEligible;
        }
        #endregion

        #region _ProcessUserFieldLeaderboardRelativeToEligible
        /// <summary>
        /// Processes the "LeaderboardRelativeToEligible" flag for a user field based on global rules.
        /// LeaderboardRelativeToEligible is only set at the global rule level, it is not configurable
        /// per a site's user account data.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <returns>true/false</returns>
        private bool _ProcessUserFieldLeaderboardRelativeToEligible(UserFieldGlobalRules globalRuleSetForField)
        {
            return globalRuleSetForField.LeaderboardRelativeToEligible;
        }
        #endregion

        #region _ProcessUserFieldRegistrationPageView
        /// <summary>
        /// Processes the "UserRegistrationPageView" flag for a user field based on global rules and the attribute
        /// value for the user field's node.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <param name="attributeValue">the value of the userRegistrationPageView attribute</param>
        /// <returns>true/false</returns>
        private bool _ProcessUserFieldRegistrationPageView(UserFieldGlobalRules globalRuleSetForField, string attributeValue)
        {
            if (!globalRuleSetForField.RegistrationPageAllowed)
            { return false; }

            if (globalRuleSetForField.AlwaysVisible)
            { return true; }

            return Convert.ToBoolean(attributeValue);
        }
        #endregion

        #region _ProcessUserFieldRegistrationPageModify
        /// <summary>
        /// Processes the "UserRegistrationPageModify" flag for a user field based on global rules and the attribute
        /// value for the user field's node.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <param name="attributeValue">the value of the userRegistrationPageModify attribute</param>
        /// <returns>true/false</returns>
        private bool _ProcessUserFieldRegistrationPageModify(UserFieldGlobalRules globalRuleSetForField, string attributeValue)
        {
            if (!globalRuleSetForField.RegistrationPageAllowed)
            { return false; }

            return Convert.ToBoolean(attributeValue);
        }
        #endregion

        #region _ProcessUserFieldUserEditPageView
        /// <summary>
        /// Processes the "UserEditPageView" flag for a user field based on global rules and the attribute
        /// value for the user field's node.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <param name="attributeValue">the value of the userEditPageView attribute</param>
        /// <returns>true/false</returns>
        private bool _ProcessUserFieldUserEditPageView(UserFieldGlobalRules globalRuleSetForField, string attributeValue)
        {
            if (!globalRuleSetForField.UserViewAllowed)
            { return false; }

            if (globalRuleSetForField.AlwaysVisible)
            { return true; }

            return Convert.ToBoolean(attributeValue);
        }
        #endregion

        #region _ProcessUserFieldUserEditPageModify
        /// <summary>
        /// Processes the "UserEditPageModify" flag for a user field based on global rules and the attribute
        /// value for the user field's node.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <param name="attributeValue">the value of the userEditPageModify attribute</param>
        /// <returns>true/false</returns>
        private bool _ProcessUserFieldUserEditPageModify(UserFieldGlobalRules globalRuleSetForField, string attributeValue)
        {
            if (!globalRuleSetForField.UserViewAllowed || !globalRuleSetForField.UserModifyAllowed)
            { return false; }

            return Convert.ToBoolean(attributeValue);
        }
        #endregion

        #region _ProcessUserFieldAdministratorEditPageView
        /// <summary>
        /// Processes the "AdministratorEditPageView" flag for a user field based on global rules and the attribute
        /// value for the user field's node.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <param name="attributeValue">the value of the administratorEditPageView attribute</param>
        /// <returns>true/false</returns>
        private bool _ProcessUserFieldAdministratorEditPageView(UserFieldGlobalRules globalRuleSetForField, string attributeValue)
        {
            if (!globalRuleSetForField.AdminViewAllowed)
            { return false; }

            if (globalRuleSetForField.AlwaysVisible)
            { return true; }

            return Convert.ToBoolean(attributeValue);
        }
        #endregion

        #region _ProcessUserFieldAdministratorEditPageModify
        /// <summary>
        /// Processes the "AdministratorEditPageModify" flag for a user field based on global rules and the attribute
        /// value for the user field's node.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <param name="attributeValue">the value of the administratorEditPageModify attribute</param>
        /// <returns>true/false</returns>
        private bool _ProcessUserFieldAdministratorEditPageModify(UserFieldGlobalRules globalRuleSetForField, string attributeValue)
        {
            if (!globalRuleSetForField.AdminViewAllowed || !globalRuleSetForField.AdminModifyAllowed)
            { return false; }

            return Convert.ToBoolean(attributeValue);
        }
        #endregion

        #region _ProcessUserFieldRegularExpression
        /// <summary>
        /// Processes the "RegularExpression" for a user field based on global rules.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <param name="attributeValue">the value of the regularExpression node</param>
        /// <returns>regular expression string</returns>
        private string _ProcessUserFieldRegularExpression(UserFieldGlobalRules globalRuleSetForField, string attributeValue)
        {
            if (globalRuleSetForField.DataType != DataType.String || globalRuleSetForField.Identifier == "avatar") // !globalRuleSetForField.IsTypeable, do not check against is typable anymore
            { return null; }

            return attributeValue;
        }
        #endregion

        #region _ProcessUserFieldInputType
        /// <summary>
        /// Processes the "InputType" for a user field based on global rules.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <param name="attributeValue">the value of the typeProperties/type node</param>
        /// <returns>UserFields InputType</returns>
        private InputType? _ProcessUserFieldInputType(UserFieldGlobalRules globalRuleSetForField, string attributeValue)
        {
            if (!globalRuleSetForField.IsTypeable || globalRuleSetForField.DataType != DataType.String)
            { return null; }

            if (attributeValue == InputType.TextField.ToString())
            { return InputType.TextField; }
            else if (attributeValue == InputType.Date.ToString())
            { return InputType.Date; }
            else if (attributeValue == InputType.RadioButtons.ToString())
            { return InputType.RadioButtons; }
            else if (attributeValue == InputType.CheckBoxes.ToString())
            { return InputType.CheckBoxes; }
            else if (attributeValue == InputType.SelectBoxSingleSelect.ToString())
            { return InputType.SelectBoxSingleSelect; }
            else if (attributeValue == InputType.SelectBoxMultipleSelect.ToString())
            { return InputType.SelectBoxMultipleSelect; }
            else if (attributeValue == InputType.DropDownList.ToString())
            { return InputType.DropDownList; }
            else
            { return null; }
        }
        #endregion

        #region _ProcessUserFieldMaxLength
        /// <summary>
        /// Processes the maximum length for a user field based on global rules.
        /// </summary>
        /// <param name="globalRuleSetForField">global rules for the user field</param>
        /// <returns>Int</returns>
        private int _ProcessUserFieldMaxLength(UserFieldGlobalRules globalRuleSetForField)
        {
            return globalRuleSetForField.MaxLength;
        }
        #endregion

        #region _ProcessUserFieldLabels
        /// <summary>
        /// Processes user field labels into appropriate language specific properties object for the user field.
        /// </summary>
        /// <param name="userFieldLanguageSpecificProperties">language specific properties object for user field</param>
        /// <param name="userFieldLabelNodes">label nodes for user field</param>
        private void _ProcessUserFieldLabels(ref List<UserFieldLanguageSpecificProperties> userFieldLanguageSpecificProperties, XmlNodeList userFieldLabelNodes)
        {
            foreach (XmlNode userFieldLabelNode in userFieldLabelNodes)
            {
                string language = userFieldLabelNode.Attributes["lang"].Value;
                bool foundLanguageSpecificProperty = false;

                foreach (UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty in userFieldLanguageSpecificProperties)
                {
                    if (language == userFieldLanguageSpecificProperty.LangString)
                    {
                        foundLanguageSpecificProperty = true;
                        userFieldLanguageSpecificProperty.Label = userFieldLabelNode.InnerText;
                    }
                }

                if (!foundLanguageSpecificProperty)
                {
                    UserFieldLanguageSpecificProperties newUserFieldLanguageSpecificProperty = new UserFieldLanguageSpecificProperties();
                    newUserFieldLanguageSpecificProperty.LangString = language;
                    newUserFieldLanguageSpecificProperty.Label = userFieldLabelNode.InnerText;

                    // set whether or not this lang is the default site's language
                    if (language == this._SiteDefaultLanguageString)
                    { newUserFieldLanguageSpecificProperty.IsDefaultLanguage = true; }
                    else
                    { newUserFieldLanguageSpecificProperty.IsDefaultLanguage = false; }

                    userFieldLanguageSpecificProperties.Add(newUserFieldLanguageSpecificProperty);
                }
            }
        }
        #endregion

        #region _ProcessUserFieldDescriptions
        /// <summary>
        /// Processes user field descriptions into appropriate language specific properties object for the user field.
        /// </summary>
        /// <param name="userFieldLanguageSpecificProperties">language specific properties object for user field</param>
        /// <param name="userFieldLabelNodes">description nodes for user field</param>
        private void _ProcessUserFieldDescriptions(ref List<UserFieldLanguageSpecificProperties> userFieldLanguageSpecificProperties, XmlNodeList userFieldDescriptionNodes)
        {
            foreach (XmlNode userFieldDescriptionNode in userFieldDescriptionNodes)
            {
                string language = userFieldDescriptionNode.Attributes["lang"].Value;
                bool foundLanguageSpecificProperty = false;

                foreach (UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty in userFieldLanguageSpecificProperties)
                {
                    if (language == userFieldLanguageSpecificProperty.LangString)
                    {
                        foundLanguageSpecificProperty = true;
                        userFieldLanguageSpecificProperty.Description = userFieldDescriptionNode.InnerText;
                    }
                }

                if (!foundLanguageSpecificProperty)
                {
                    UserFieldLanguageSpecificProperties newUserFieldLanguageSpecificProperty = new UserFieldLanguageSpecificProperties();
                    newUserFieldLanguageSpecificProperty.LangString = language;
                    newUserFieldLanguageSpecificProperty.Description = userFieldDescriptionNode.InnerText;

                    // set whether or not this lang is the default site's language
                    if (language == this._SiteDefaultLanguageString)
                    { newUserFieldLanguageSpecificProperty.IsDefaultLanguage = true; }
                    else
                    { newUserFieldLanguageSpecificProperty.IsDefaultLanguage = false; }

                    userFieldLanguageSpecificProperties.Add(newUserFieldLanguageSpecificProperty);
                }
            }
        }
        #endregion

        #region _ProcessUserFieldChoiceValues
        /// <summary>
        /// Processes user field choice values into appropriate language specific properties object for the user field.
        /// </summary>
        /// <param name="userFieldLanguageSpecificProperties">language specific properties object for user field</param>
        /// <param name="userFieldLabelNodes">choice values nodes for user field</param>
        private void _ProcessUserFieldChoiceValues(ref List<UserFieldLanguageSpecificProperties> userFieldLanguageSpecificProperties, XmlNodeList choiceValuesNodes)
        {
            foreach (XmlNode choiceValuesNode in choiceValuesNodes)
            {
                string language = choiceValuesNode.Attributes["lang"].Value;
                bool foundLanguageSpecificProperty = false;

                foreach (UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty in userFieldLanguageSpecificProperties)
                {
                    if (language == userFieldLanguageSpecificProperty.LangString)
                    {
                        foundLanguageSpecificProperty = true;
                        userFieldLanguageSpecificProperty.AlphabetizeChoiceValues = Convert.ToBoolean(choiceValuesNode.Attributes["alphabetize"].Value);
                        userFieldLanguageSpecificProperty.ChoiceValues = choiceValuesNode.InnerText;
                    }
                }

                if (!foundLanguageSpecificProperty)
                {
                    UserFieldLanguageSpecificProperties newUserFieldLanguageSpecificProperty = new UserFieldLanguageSpecificProperties();
                    newUserFieldLanguageSpecificProperty.LangString = language;
                    newUserFieldLanguageSpecificProperty.AlphabetizeChoiceValues = Convert.ToBoolean(choiceValuesNode.Attributes["alphabetize"].Value);
                    newUserFieldLanguageSpecificProperty.ChoiceValues = choiceValuesNode.InnerText;

                    // set whether or not this lang is the default site's language
                    if (language == this._SiteDefaultLanguageString)
                    { newUserFieldLanguageSpecificProperty.IsDefaultLanguage = true; }
                    else
                    { newUserFieldLanguageSpecificProperty.IsDefaultLanguage = false; }

                    userFieldLanguageSpecificProperties.Add(newUserFieldLanguageSpecificProperty);
                }
            }
        }
        #endregion
        #endregion
    }
}
