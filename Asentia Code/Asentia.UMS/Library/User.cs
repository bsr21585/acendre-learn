﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Asentia.Common;
using Asentia.UMS.Controls;
using Newtonsoft.Json;

namespace Asentia.UMS.Library
{
    [Serializable]
    public class User
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public User()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idUser">User Id</param>
        public User(int idUser)
        {
            this._Details(idUser);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[User.GetGrid]";

        /// <summary>
        /// Id
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName;

        /// <summary>
        /// Middle Name
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string MiddleName;

        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName;

        /// <summary>
        /// Display Name
        /// </summary>
        public string DisplayName;

        /// <summary>
        /// Avatar
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Avatar;

        /// <summary>
        /// Email
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Email;

        /// <summary>
        /// Username
        /// </summary>
        public string Username;

        /// <summary>
        /// Password
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public string Password;

        /// <summary>
        /// Site Id
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Timezone Id
        /// </summary>
        public int IdTimezone;

        /// <summary>
        /// Object GUID
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string ObjectGUID;

        /// <summary>
        /// Activation GUID
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string ActivationGUID;

        /// <summary>
        /// Distinguished Name
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string DistinguishedName;

        /// <summary>
        /// Is Active
        /// </summary>
        public bool IsActive;

        /// <summary>
        /// Must Change Password
        /// </summary>
        public bool MustChangePassword;

        /// <summary>
        /// Date Created
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Date Expires
        /// </summary>
        public DateTime? DtExpires;

        /// <summary>
        /// Is Deleted
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool IsDeleted;

        /// <summary>
        /// Date Deleted
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public DateTime? DtDeleted;

        /// <summary>
        /// Language Id
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public int IdLanguage;

        /// <summary>
        /// Language String
        /// Language code that is representatibe of a "culture code," i.e. 'en-US'.
        /// </summary>
        public string LanguageString;

        /// <summary>
        /// Date Modified
        /// </summary>
        public DateTime? DtModified;

        /// <summary>
        /// Date Last Login
        /// </summary>
        public DateTime? DtLastLogin;

        /// <summary>
        /// Employee Id
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string EmployeeId;

        /// <summary>
        /// Company
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Company;

        /// <summary>
        /// Address
        /// </summary>
        public string Address;

        /// <summary>
        /// City
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string City;

        /// <summary>
        /// Province/State
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Province;

        /// <summary>
        /// Postal Code
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string PostalCode;

        /// <summary>
        /// Country
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Country;

        /// <summary>
        /// Primary Phone
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string PhonePrimary;

        /// <summary>
        /// Work Phone
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string PhoneWork;

        /// <summary>
        /// Fax Phone
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string PhoneFax;

        /// <summary>
        /// Home Phone
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string PhoneHome;

        /// <summary>
        /// Mobile Phone
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string PhoneMobile;

        /// <summary>
        /// Pager Phone
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string PhonePager;

        /// <summary>
        /// Other Phone
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string PhoneOther;

        /// <summary>
        /// Department
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Department;

        /// <summary>
        /// Division
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Division;

        /// <summary>
        /// Region
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Region;

        /// <summary>
        /// Job Title
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string JobTitle;

        /// <summary>
        /// Job Class
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string JobClass;

        /// <summary>
        /// Gender
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Gender;

        /// <summary>
        /// Race
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Race;

        /// <summary>
        /// Date of Birth
        /// </summary>
        public DateTime? DtDOB;

        /// <summary>
        /// Hire Date
        /// </summary>
        public DateTime? DtHire;

        /// <summary>
        /// Terminated Date
        /// </summary>
        public DateTime? DtTerm;

        /// <summary>
        /// Extra Field 00
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field00;

        /// <summary>
        /// Extra Field 01
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field01;

        /// <summary>
        /// Extra Field 02
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field02;

        /// <summary>
        /// Extra Field 03
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field03;

        /// <summary>
        /// Extra Field 04
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field04;

        /// <summary>
        /// Extra Field 05
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field05;

        /// <summary>
        /// Extra Field 06
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field06;

        /// <summary>
        /// Extra Field 07
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field07;

        /// <summary>
        /// Extra Field 08
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field08;

        /// <summary>
        /// Extra Field 09
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field09;

        /// <summary>
        /// Extra Field 10
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field10;

        /// <summary>
        /// Extra Field 11
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field11;

        /// <summary>
        /// Extra Field 12
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field12;

        /// <summary>
        /// Extra Field 13
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field13;

        /// <summary>
        /// Extra Field 14
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field14;

        /// <summary>
        /// Extra Field 15
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field15;

        /// <summary>
        /// Extra Field 16
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field16;

        /// <summary>
        /// Extra Field 17
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field17;

        /// <summary>
        /// Extra Field 18
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field18;

        /// <summary>
        /// Extra Field 19
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Field19;

        /// <summary>
        /// Is this user a course expert?
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool IsUserACourseExpert;

        /// <summary>
        /// Is this user a course approver?
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool IsUserACourseApprover;

        /// <summary>
        /// Is this user the supervisor of another user?
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool IsUserASupervisor;

        /// <summary>
        /// Is this user a moderator for a course or group wall?
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool IsUserAWallModerator;

        /// <summary>
        /// Is this user an instructor for an ILT session?
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool IsUserAnILTInstructor;

        /// <summary>
        /// A listing of ids of groups the user is a member of.
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public List<int> GroupMemberships;

        /// <summary>
        /// Has this user's registration been approved by the administrator?
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool? IsRegistrationApproved;

        /// <summary>
        /// The date that the user's registration was approved.
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public DateTime? DtApproved;

        /// <summary>
        /// The date that the user's registration was rejected.
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public DateTime? DtDenied;

        /// <summary>
        /// The ID of the user that approved or rejected the user's registration.
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public int? IdApprover;

        /// <summary>
        /// Description of why the user's registration was rejected.
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public string RejectionComments;

        /// <summary>
        /// Is the user excluded from logging in via the user login form (Must login with in-house SAML link)
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool? DisableLoginFromLoginForm;

        /// <summary>
        /// Does the user opt out of email notifications
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool? OptOutOfEmailNotifications;

        /// <summary>
        /// Exclude From Leaderboards
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public bool? ExcludeFromLeaderboards;
        #endregion

        #region Classes
        #region FieldError
        public class FieldError
        {
            public string FieldIdentifier;
            public string ErrorMessage;
            public string TabIdentifier;

            public FieldError(string fieldIdentifier, string errorMessage, string tabIdentifier = null)
            {
                this.FieldIdentifier = fieldIdentifier;
                this.ErrorMessage = errorMessage;
                this.TabIdentifier = tabIdentifier;
            }
        }
        #endregion
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves user data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves user data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session, i.e.
        /// on user self-registration. Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveAvatar
        /// <summary>
        /// Saves user's avatar path to database as the current session user.
        /// </summary>
        public void SaveAvatar()
        {
            this._SaveAvatar(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves user's avatar path to database with a caller specified.
        /// This would be used when SaveAvatar needs to be called outside of a user session, i.e.
        /// on user self-registration. Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public void SaveAvatar(int idCaller)
        {
            this._SaveAvatar(idCaller);
        }
        #endregion

        #region ChangePassword
        /// <summary>
        /// Changes the calling user's password.
        /// </summary>
        public void ChangePassword(string newPassword)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@password", newPassword, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.ChangePassword]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ValidateDataFromForm
        /// <summary>
        /// Validates data coming from a user form.
        /// </summary>
        /// <param name="userForm">the user form</param>
        /// <returns>list of field errors</returns>
        public List<FieldError> ValidateDataFromForm(UserForm userForm)
        {
            List<FieldError> fieldErrors = new List<FieldError>();
            UserAccountData userAccountData = userForm.UserAccountData;

            foreach (UserAccountData.Tab tab in userAccountData.Tabs)
            {
                foreach (UserAccountData.UserField userField in tab.UserFields)
                {
                    // get whether or not the field is modifyable for the view type
                    bool isModifyableForViewType = false;

                    switch (userForm.ViewType)
                    {
                        case UserFormViewType.AdminView:
                            isModifyableForViewType = userField.AdministratorEditPageModify;
                            break;
                        case UserFormViewType.UserView:
                            isModifyableForViewType = userField.UserEditPageModify;
                            break;
                        case UserFormViewType.RegistrationPage:
                            isModifyableForViewType = userField.RegistrationPageModify;
                            break;
                    }

                    // get the value for the field
                    bool foundField = false;
                    object value = null;

                    switch (userField.Identifier)
                    {
                        case "firstName":
                            foundField = true;
                            value = userForm.FirstName;
                            break;
                        case "middleName":
                            foundField = true;
                            value = userForm.MiddleName;
                            break;
                        case "lastName":
                            foundField = true;
                            value = userForm.LastName;
                            break;
                        case "avatar":
                            foundField = true;
                            value = userForm.Avatar;
                            break;
                        case "email":
                            foundField = true;
                            value = userForm.Email;
                            break;
                        case "username":
                            foundField = true;
                            value = userForm.Username;
                            break;
                        case "password":
                            foundField = true;
                            value = userForm.Password;
                            break;
                        case "idTimezone":
                            foundField = true;
                            value = userForm.Timezone;
                            break;
                        case "idLanguage":
                            foundField = true;
                            value = userForm.Language;
                            break;
                        case "dtExpires":
                            foundField = true;
                            value = userForm.DtExpires;
                            break;
                        case "isActive":
                            foundField = true;
                            value = userForm.IsActive;
                            break;
                        case "company":
                            foundField = true;
                            value = userForm.Company;
                            break;
                        case "address":
                            foundField = true;
                            value = userForm.Address;
                            break;
                        case "city":
                            foundField = true;
                            value = userForm.City;
                            break;
                        case "province":
                            foundField = true;
                            value = userForm.Province;
                            break;
                        case "postalcode":
                            foundField = true;
                            value = userForm.PostalCode;
                            break;
                        case "country":
                            foundField = true;
                            value = userForm.Country;
                            break;
                        case "phonePrimary":
                            foundField = true;
                            value = userForm.PhonePrimary;
                            break;
                        case "phoneHome":
                            foundField = true;
                            value = userForm.PhoneHome;
                            break;
                        case "phoneWork":
                            foundField = true;
                            value = userForm.PhoneWork;
                            break;
                        case "phoneFax":
                            foundField = true;
                            value = userForm.PhoneFax;
                            break;
                        case "phoneMobile":
                            foundField = true;
                            value = userForm.PhoneMobile;
                            break;
                        case "phonePager":
                            foundField = true;
                            value = userForm.PhonePager;
                            break;
                        case "phoneOther":
                            foundField = true;
                            value = userForm.PhoneOther;
                            break;
                        case "employeeID":
                            foundField = true;
                            value = userForm.EmployeeID;
                            break;
                        case "jobTitle":
                            foundField = true;
                            value = userForm.JobTitle;
                            break;
                        case "jobClass":
                            foundField = true;
                            value = userForm.JobClass;
                            break;
                        case "division":
                            foundField = true;
                            value = userForm.Division;
                            break;
                        case "region":
                            foundField = true;
                            value = userForm.Region;
                            break;
                        case "department":
                            foundField = true;
                            value = userForm.Department;
                            break;
                        case "dtHire":
                            foundField = true;
                            value = userForm.DtHire;
                            break;
                        case "dtTerm":
                            foundField = true;
                            value = userForm.DtTerm;
                            break;
                        case "gender":
                            foundField = true;
                            value = userForm.Gender;
                            break;
                        case "race":
                            foundField = true;
                            value = userForm.Race;
                            break;
                        case "dtDOB":
                            foundField = true;
                            value = userForm.DtDOB;
                            break;
                        case "field00":
                            foundField = true;
                            value = userForm.Field00;
                            break;
                        case "field01":
                            foundField = true;
                            value = userForm.Field01;
                            break;
                        case "field02":
                            foundField = true;
                            value = userForm.Field02;
                            break;
                        case "field03":
                            foundField = true;
                            value = userForm.Field03;
                            break;
                        case "field04":
                            foundField = true;
                            value = userForm.Field04;
                            break;
                        case "field05":
                            foundField = true;
                            value = userForm.Field05;
                            break;
                        case "field06":
                            foundField = true;
                            value = userForm.Field06;
                            break;
                        case "field07":
                            foundField = true;
                            value = userForm.Field07;
                            break;
                        case "field08":
                            foundField = true;
                            value = userForm.Field08;
                            break;
                        case "field09":
                            foundField = true;
                            value = userForm.Field09;
                            break;
                        case "field10":
                            foundField = true;
                            value = userForm.Field10;
                            break;
                        case "field11":
                            foundField = true;
                            value = userForm.Field11;
                            break;
                        case "field12":
                            foundField = true;
                            value = userForm.Field12;
                            break;
                        case "field13":
                            foundField = true;
                            value = userForm.Field13;
                            break;
                        case "field14":
                            foundField = true;
                            value = userForm.Field14;
                            break;
                        case "field15":
                            foundField = true;
                            value = userForm.Field15;
                            break;
                        case "field16":
                            foundField = true;
                            value = userForm.Field16;
                            break;
                        case "field17":
                            foundField = true;
                            value = userForm.Field17;
                            break;
                        case "field18":
                            foundField = true;
                            value = userForm.Field18;
                            break;
                        case "field19":
                            foundField = true;
                            value = userForm.Field19;
                            break;
                    }

                    // if the field is modifyable for the view type and the field was found in the form, validate
                    if (isModifyableForViewType && foundField)
                    {
                        // username field gets special validation
                        if (userField.Identifier == "username")
                        {
                            // validate required
                            if (String.IsNullOrWhiteSpace((string)value))
                            {
                                fieldErrors.Add(new FieldError(userField.Identifier,
                                                                userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsRequired,
                                                                tab.Identifier
                                                                )
                                                );
                            }

                            // validate against regular expression minimums and exclusions for the system - if they exist
                            if (
                                (!String.IsNullOrWhiteSpace((string)value) && !String.IsNullOrWhiteSpace(Config.ApplicationSettings.MinimumUsernameRequirementsRegEx))
                                ||
                                (!String.IsNullOrWhiteSpace((string)value) && !String.IsNullOrWhiteSpace(Config.ApplicationSettings.UsernameExclusionsRegEx))
                               )
                            {
                                Match minimumMatch = Regex.Match((string)value, Config.ApplicationSettings.MinimumUsernameRequirementsRegEx, RegexOptions.IgnoreCase);
                                Match exclusionMatch = Regex.Match((string)value, Config.ApplicationSettings.UsernameExclusionsRegEx, RegexOptions.IgnoreCase);

                                if (!minimumMatch.Success || exclusionMatch.Success)
                                {
                                    fieldErrors.Add(new FieldError(userField.Identifier,
                                                                   _GlobalResources.ResourceManager.GetString(Config.ApplicationSettings.UsernameRequirementsErrorResourceKey, new CultureInfo(AsentiaSessionState.UserCulture)),
                                                                   tab.Identifier
                                                                   )
                                                   );
                                }
                            }

                            // validate against regular expression for the field - if there is one
                            if (!String.IsNullOrWhiteSpace(userField.RegularExpression) && !String.IsNullOrWhiteSpace((string)value))
                            {
                                Match match = Regex.Match((string)value, userField.RegularExpression, RegexOptions.IgnoreCase);

                                if (!match.Success)
                                {
                                    fieldErrors.Add(new FieldError(userField.Identifier,
                                                                   userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsInvalid,
                                                                   tab.Identifier
                                                                   )
                                                   );
                                }
                            }
                            
                        }
                        // password field gets special validation
                        else if (userField.Identifier == "password")
                        {
                            // if there is no existing user object (new user), validate required
                            if (userForm.UserObject == null)
                            {
                                if (userField.IsRequired)
                                {
                                    if (String.IsNullOrWhiteSpace((string)value))
                                    {
                                        fieldErrors.Add(new FieldError(userField.Identifier,
                                                                       userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsRequired,
                                                                       tab.Identifier
                                                                       )
                                                       );
                                    }
                                }
                            }
                            
                            // ensure password and confirmation match
                            if ((string)value != userForm.PasswordConfirm)
                            {
                                fieldErrors.Add(new FieldError(userField.Identifier,
                                                                userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.MustMatchConfirmationField,
                                                                tab.Identifier
                                                                )
                                                );
                            }

                            // validate against regular expression minimums and exclusions for the system - if they exist
                            if (
                                (!String.IsNullOrWhiteSpace((string)value) && !String.IsNullOrWhiteSpace(Config.ApplicationSettings.MinimumPasswordRequirementsRegEx))
                                ||
                                (!String.IsNullOrWhiteSpace((string)value) && !String.IsNullOrWhiteSpace(Config.ApplicationSettings.PasswordExclusionsRegEx))
                               )
                            {
                                Match minimumMatch = Regex.Match((string)value, Config.ApplicationSettings.MinimumPasswordRequirementsRegEx, RegexOptions.IgnoreCase);
                                Match exclusionMatch = Regex.Match((string)value, Config.ApplicationSettings.PasswordExclusionsRegEx, RegexOptions.IgnoreCase);

                                if (!minimumMatch.Success || exclusionMatch.Success || (string)value == userForm.Username)
                                {
                                    fieldErrors.Add(new FieldError(userField.Identifier,
                                                                   _GlobalResources.ResourceManager.GetString(Config.ApplicationSettings.PasswordRequirementsErrorResourceKey, new CultureInfo(AsentiaSessionState.UserCulture)),
                                                                   tab.Identifier
                                                                   )
                                                   );
                                }
                            }

                            // validate against regular expression for the field - if there is one
                            if (!String.IsNullOrWhiteSpace(userField.RegularExpression) && !String.IsNullOrWhiteSpace((string)value))
                            {
                                Match match = Regex.Match((string)value, userField.RegularExpression, RegexOptions.IgnoreCase);

                                if (!match.Success)
                                {
                                    fieldErrors.Add(new FieldError(userField.Identifier,
                                                                   userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsInvalid,
                                                                   tab.Identifier
                                                                   )
                                                   );
                                }
                            }
                        }
                        // avatar field also gets special validation
                        else if (userField.Identifier == "avatar")
                        {
                            // validate required
                            if (userForm.UserObject == null)
                            {
                                if (userField.IsRequired)
                                {
                                    if (String.IsNullOrWhiteSpace((string)value))
                                    {
                                        fieldErrors.Add(new FieldError(userField.Identifier,
                                                                       userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsRequired,
                                                                       tab.Identifier
                                                                       )
                                                       );
                                    }
                                }
                            }
                            else
                            {
                                if (userField.IsRequired)
                                {
                                    if (String.IsNullOrWhiteSpace((string)value) && String.IsNullOrWhiteSpace(userForm.UserObject.Avatar))
                                    {
                                        fieldErrors.Add(new FieldError(userField.Identifier,
                                                                       userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsRequired,
                                                                       tab.Identifier
                                                                       )
                                                       );
                                    }
                                }
                            }
                        }
                        // all other fields
                        else
                        {
                            // validate required
                            if (userField.IsRequired)
                            {
                                if (String.IsNullOrWhiteSpace((string)value))
                                {
                                    fieldErrors.Add(new FieldError(userField.Identifier,
                                                                   userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsRequired,
                                                                   tab.Identifier
                                                                   )
                                                   );
                                }
                            }

                            // validate against regular expression for the field - email gets some built-in validation
                            if (!String.IsNullOrWhiteSpace(userField.RegularExpression) && !String.IsNullOrWhiteSpace((string)value))
                            {
                                Match match = Regex.Match((string)value, userField.RegularExpression, RegexOptions.IgnoreCase);

                                if (!match.Success)
                                {
                                    fieldErrors.Add(new FieldError(userField.Identifier,
                                                                   userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsInvalid,
                                                                   tab.Identifier
                                                                   )
                                                   );
                                }
                            }
                            else // email
                            {
                                if (userField.Identifier == "email")
                                {
                                    // built-in regex validation for email addresses
                                    if (!String.IsNullOrWhiteSpace((string)value) && !RegExValidation.ValidateEmailAddress((string)value))
                                    {
                                        fieldErrors.Add(new FieldError(userField.Identifier,
                                                                       userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsInvalid,
                                                                       tab.Identifier
                                                                      )
                                                   );
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // validate if the user agreement has been agreed to if it is required
            if (userForm.IsUserAgreementRequired)
            {
                if (!userForm.UserAgreesToUserAgreement)
                {
                    fieldErrors.Add(new FieldError("UserAgreement",
                                                   _GlobalResources.YouMustAgreeToTheTermsOfTheUserAgreement,
                                                   null
                                                   )
                                   );
                }
            }

            // return the list of errors
            return fieldErrors;
        }
        #endregion

        #region ValidateDataFromSelf
        /// <summary>
        /// Validates the data self.
        /// </summary>
        /// <param name="userAccountData">The user account data.</param>
        /// <returns></returns>
        public List<FieldError> ValidateDataSelf(UserAccountData userAccountData)
        {
            List<FieldError> fieldErrors = new List<FieldError>();

            foreach (UserAccountData.Tab tab in userAccountData.Tabs)
            {
                foreach (UserAccountData.UserField userField in tab.UserFields)
                {
                    // get whether or not the field is modifyable for the admin view type
                    bool isModifyableForViewType = userField.AdministratorEditPageModify;

                    // get the value for the field
                    bool foundField = false;
                    object value = null;

                    switch (userField.Identifier)
                    {
                        case "firstName":
                            foundField = true;
                            value = this.FirstName;
                            break;
                        case "middleName":
                            foundField = true;
                            value = this.MiddleName;
                            break;
                        case "lastName":
                            foundField = true;
                            value = this.LastName;
                            break;
                        case "avatar":
                            foundField = true;
                            value = this.Avatar;
                            break;
                        case "email":
                            foundField = true;
                            value = this.Email;
                            break;
                        case "username":
                            foundField = true;
                            value = this.Username;
                            break;
                        case "password":
                            foundField = true;
                            value = this.Password;
                            break;
                        case "idTimezone":
                            foundField = true;
                            value = this.IdTimezone;
                            break;
                        case "idLanguage":
                            foundField = true;
                            value = this.LanguageString;
                            break;
                        case "dtExpires":
                            foundField = true;
                            value = this.DtExpires;
                            break;
                        case "isActive":
                            foundField = true;
                            value = this.IsActive;
                            break;
                        case "company":
                            foundField = true;
                            value = this.Company;
                            break;
                        case "address":
                            foundField = true;
                            value = this.Address;
                            break;
                        case "city":
                            foundField = true;
                            value = this.City;
                            break;
                        case "province":
                            foundField = true;
                            value = this.Province;
                            break;
                        case "postalcode":
                            foundField = true;
                            value = this.PostalCode;
                            break;
                        case "country":
                            foundField = true;
                            value = this.Country;
                            break;
                        case "phonePrimary":
                            foundField = true;
                            value = this.PhonePrimary;
                            break;
                        case "phoneHome":
                            foundField = true;
                            value = this.PhoneHome;
                            break;
                        case "phoneWork":
                            foundField = true;
                            value = this.PhoneWork;
                            break;
                        case "phoneFax":
                            foundField = true;
                            value = this.PhoneFax;
                            break;
                        case "phoneMobile":
                            foundField = true;
                            value = this.PhoneMobile;
                            break;
                        case "phonePager":
                            foundField = true;
                            value = this.PhonePager;
                            break;
                        case "phoneOther":
                            foundField = true;
                            value = this.PhoneOther;
                            break;
                        case "employeeID":
                            foundField = true;
                            value = this.EmployeeId;
                            break;
                        case "jobTitle":
                            foundField = true;
                            value = this.JobTitle;
                            break;
                        case "jobClass":
                            foundField = true;
                            value = this.JobClass;
                            break;
                        case "division":
                            foundField = true;
                            value = this.Division;
                            break;
                        case "region":
                            foundField = true;
                            value = this.Region;
                            break;
                        case "department":
                            foundField = true;
                            value = this.Department;
                            break;
                        case "dtHire":
                            foundField = true;
                            value = this.DtHire;
                            break;
                        case "dtTerm":
                            foundField = true;
                            value = this.DtTerm;
                            break;
                        case "gender":
                            foundField = true;
                            value = this.Gender;
                            break;
                        case "race":
                            foundField = true;
                            value = this.Race;
                            break;
                        case "dtDOB":
                            foundField = true;
                            value = this.DtDOB;
                            break;
                        case "field00":
                            foundField = true;
                            value = this.Field00;
                            break;
                        case "field01":
                            foundField = true;
                            value = this.Field01;
                            break;
                        case "field02":
                            foundField = true;
                            value = this.Field02;
                            break;
                        case "field03":
                            foundField = true;
                            value = this.Field03;
                            break;
                        case "field04":
                            foundField = true;
                            value = this.Field04;
                            break;
                        case "field05":
                            foundField = true;
                            value = this.Field05;
                            break;
                        case "field06":
                            foundField = true;
                            value = this.Field06;
                            break;
                        case "field07":
                            foundField = true;
                            value = this.Field07;
                            break;
                        case "field08":
                            foundField = true;
                            value = this.Field08;
                            break;
                        case "field09":
                            foundField = true;
                            value = this.Field09;
                            break;
                        case "field10":
                            foundField = true;
                            value = this.Field10;
                            break;
                        case "field11":
                            foundField = true;
                            value = this.Field11;
                            break;
                        case "field12":
                            foundField = true;
                            value = this.Field12;
                            break;
                        case "field13":
                            foundField = true;
                            value = this.Field13;
                            break;
                        case "field14":
                            foundField = true;
                            value = this.Field14;
                            break;
                        case "field15":
                            foundField = true;
                            value = this.Field15;
                            break;
                        case "field16":
                            foundField = true;
                            value = this.Field16;
                            break;
                        case "field17":
                            foundField = true;
                            value = this.Field17;
                            break;
                        case "field18":
                            foundField = true;
                            value = this.Field18;
                            break;
                        case "field19":
                            foundField = true;
                            value = this.Field19;
                            break;
                    }

                    // if the field is modifyable for the view type and the field was found in the form, validate
                    if (isModifyableForViewType && foundField)
                    {
                        // validate required
                        if (userField.IsRequired)
                        {
                            if (String.IsNullOrWhiteSpace((string)value))
                            {
                                fieldErrors.Add(new FieldError(userField.Identifier,
                                                                userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsRequired
                                                                )
                                                );
                            }
                        }

                        // validate against regular expression for the field
                        if (!String.IsNullOrWhiteSpace(userField.RegularExpression))
                        {
                            Match match = Regex.Match((string)value, userField.RegularExpression, RegexOptions.IgnoreCase);

                            if (!match.Success)
                            {
                                fieldErrors.Add(new FieldError(userField.Identifier,
                                                                userAccountData.GetUserFieldLabelInLanguage(userField, AsentiaSessionState.UserCulture) + " " + _GlobalResources.IsInvalid
                                                                )
                                                );
                            }
                        }
                    }
                }
            }

            // return the list of errors
            return fieldErrors;
        }
        #endregion

        #region GetGroups
        /// <summary>
        /// Gets a listing of group ids and names that this user is a member of.
        /// </summary>
        /// <returns>DataTable of group ids and names the user is a member of</returns>
        public DataTable GetGroups(bool excludeAutoJoinedUsers, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@excludeAutoJoined", excludeAutoJoinedUsers, SqlDbType.Bit, 1, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.GetGroups]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetRoles
        /// <summary>
        /// Gets a listing of role ids and names that this user is a member of.
        /// </summary>
        /// <returns>DataTable of role ids and names that this user is a member of.</returns>
        public DataTable GetRoles(bool excludeAutoJoinedUsers, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@excludeAutoJoined", excludeAutoJoinedUsers, SqlDbType.Bit, 1, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.GetRoles]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetUsersAPI
        /// <summary>
        /// Return a list of users for APIquery
        /// </summary>
        public static List<int> GetUsersAPI(string type, string searchParam)
        {
            List<int> returnIDs = new List<int>();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@dataType", type, SqlDbType.NVarChar, 20, ParameterDirection.Input);
            // SEARCH PARAMETER
            // build the full text query
            FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
            string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

            // if it's not empty, use it; else make it a wildcard
            SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

            databaseObject.AddParameter("@searchParam", (type == "fts") ? formattedSearchQuery : searchParam, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsForAPI]", true);
                while (sdr.Read())
                {
                    returnIDs.Add(Convert.ToInt32(sdr[0]));
                }
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return returnIDs;
        }
        #endregion

        #region JoinGroups
        /// <summary>
        /// Joins this user to one or more groups.
        /// </summary>
        /// <param name="groups">DataTable of group ids</param>
        public void JoinGroups(DataTable groups)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Groups", groups, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.JoinGroups]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region RemoveGroups
        /// <summary>
        /// Removes user from group(s).
        /// </summary>
        /// <param name="users">DataTable of group ids</param>
        public void RemoveGroups(DataTable groups)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Groups", groups, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.RemoveGroups]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region JoinRoles
        /// <summary>
        /// Joins this user to one or more roles.
        /// </summary>
        /// <param name="roles">DataTable of role ids</param>
        public void JoinRoles(DataTable roles)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Roles", roles, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.JoinRoles]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region RemoveRoles
        /// <summary>
        /// Removes user from role(s).
        /// </summary>
        /// <param name="users">DataTable of role ids</param>
        public void RemoveRoles(DataTable roles)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Roles", roles, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.RemoveRoles]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region EnrollToStandupTraining
        /// <summary>
        /// Joins this user to one or more sessions of a standup training .
        /// </summary>
        /// <param name="groups">DataTable of group ids</param>
        public void EnrollToStandupTraining(DataTable sessions)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sessions", sessions, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@isWaitingList", 0, SqlDbType.Bit, 1, ParameterDirection.Input);
            try
            {
                databaseObject.ExecuteNonQuery("[User.EnrollStandupTraining]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region RemoveUserFromStandupTrainingInstance
        /// <summary>
        /// Removes this user from this sessions .
        /// </summary>
        /// <param name="idSession">int</param>
        public void RemoveUserFromStandupTrainingInstance(int idSession)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSession", idSession, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.RemoveUserFromStandupTrainingInstance]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetSupervisors
        /// <summary>
        /// Gets a listing of user ids and names that are supervisors of this user.
        /// </summary>
        /// <returns>DataTable of user ids and names that are supervisors of this user</returns>
        public DataTable GetSupervisors(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.GetSupervisors]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveSupervisors
        /// <summary>
        /// Saves Supervisors for this User.
        /// </summary>
        /// <param name="experts">DataTable of supervisors to save</param>
        public void SaveSupervisors(DataTable supervisors, int iduser)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", iduser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Supervisors", supervisors, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.SaveSupervisors]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes User(s).
        /// </summary>
        /// <param name="deletees">DataTable of user ids</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Users", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ClearTableColumns
        /// <summary>
        /// Clean Table when user configuration fields been saved to date type.
        /// </summary>
        /// <returns>Clean up columns</returns>
        public static void ClearTableColumns(string columns)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@columns", columns, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.ClearTableColumns]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region OptOutEmailNotifications
        /// <summary>
        /// Clean Table when user configuration fields been saved to date type.
        /// </summary>
        /// <returns>Clean up columns</returns>
        public static void OptOutEmailNotifications(int idUser)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.OptOutEmailNotifications]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetEffectivePermissions
        /// <summary>
        /// Returns effective permissions for a user.
        /// </summary>        
        /// <returns>DataTable of effective permissions for a user.</returns>
        public static DataTable GetEffectivePermissions()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@dtLastPermissionCheck", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.GetEffectivePermissions]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // store the last permission check timestamp in session
                DateTime dtLastPermissionCheck = DateTime.Parse(databaseObject.Command.Parameters["@dtLastPermissionCheck"].Value.ToString());
                AsentiaSessionState.UserLastPermissionCheckUtc = dtLastPermissionCheck;

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetEffectivePermissionsDetails
        /// <summary>
        /// Returns effective permissions for a user for specific use id.
        /// </summary>        
        /// <returns>DataTable of effective permissions for a user.</returns>
        public static DataTable GetEffectivePermissionDetails(int idUser)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.GetEffectivePermissionsDetail]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForUserSupervisorSelectList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <returns>DataTable of user ids and names not currently supervisors of the specified user.</returns>
        public static DataTable IdsAndNamesForUserSupervisorSelectList(int idUser, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsAndNamesForUserSupervisorSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForGroupSelectList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <param name="idGroup">group id</param>
        /// <returns>DataTable of user ids and names not currently in the specified group.</returns>
        public static DataTable IdsAndNamesForGroupSelectList(int idGroup, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idGroup", idGroup, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsAndNamesForGroupSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForCourseExpertSelectList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <returns>DataTable of user ids and names not currently experts of the specified course.</returns>
        public static DataTable IdsAndNamesForCourseExpertSelectList(int idCourse, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsAndNamesForCourseExpertSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForManageRosterList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <returns>DataTable of user ids and names not currently experts of the specified course.</returns>
        public static DataTable IdsAndNamesForManageRosterList(int idStandupTrainingInstance, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsAndNamesForManageRosterList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region EnrolledUsersIdsAndNamesForManageRosterList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list for users enrolled in the course with the ILT attached.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <returns>DataTable of user ids and names not currently experts of the specified course.</returns>
        public static DataTable EnrolledUsersIdsAndNamesForManageRosterList(int idStandupTraining, int idStandupTrainingInstance, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idStandupTraining", idStandupTraining, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.EnrolledUsersIdsAndNamesForManageRosterList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForInstructorSelectList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <returns>DataTable of user ids and displayNames not currently attached to the specified session.</returns>
        public static DataTable IdsAndNamesForInstructorSelectList(int idStandupTrainingInstance, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsAndNamesForInstructorSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForMergeUsersSelectList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <returns>DataTable of user ids and names.</returns>
        public static DataTable IdsAndNamesForMergeUsersSelectList(int idUserSource, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUserSource", idUserSource, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsAndNamesForMergeUsersSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForGroupWallModeratorsSelectList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <returns>DataTable of user ids and names not currently moderators of the specified group wall.</returns>
        public static DataTable IdsAndNamesForGroupWallModeratorsSelectList(int idGroup, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idGroup", idGroup, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsAndNamesForGroupWallModeratorsSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForCourseWallModeratorsSelectList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <returns>DataTable of user ids and names not currently moderators of the specified course wall.</returns>
        public static DataTable IdsAndNamesForCourseWallModeratorsSelectList(int idCourse, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsAndNamesForCourseWallModeratorsSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForEnrollmentApproversSelectList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <returns>DataTable of user ids and names of enrollment approvers of the specified course.</returns>
        public static DataTable IdsAndNamesForEnrollmentApproversSelectList(int idCourse, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsAndNamesForEnrollmentApproversSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetAllUsernamesForSite
        /// <summary>
        /// Return List of All Usernames in the current site
        /// </summary>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        public static List<string> GetAllUsernamesForSite()
        {
            List<string> siteUsernamesList = new List<string>();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader reader = databaseObject.ExecuteDataReader("[User.GetAllUserNamesForSite]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = (databaseObject.Command.Parameters["@Error_Description_Code"].Value == DBNull.Value || databaseObject.Command.Parameters["@Error_Description_Code"].Value == null) ? null : databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                DataTable dt = new DataTable();
                dt.Load(reader);

                siteUsernamesList = dt.AsEnumerable()
                           .Select(r => r.Field<string>("username"))
                           .ToList();

                return siteUsernamesList;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }            
        }
        #endregion

        #region UserListForCurrentSite
        /// <summary>
        /// Gets a listing of user ids, usernames,  names.
        /// </summary>
        public static DataTable UserListNotExistForCurrentSite(DataTable UserNameTable)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@UserNameFile", UserNameTable, SqlDbType.Structured, null, ParameterDirection.Input);


                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.UserListNotExistForCurrentSite]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SynchronizeUserBatch
        /// <summary>
        /// Synchronizes a DataTable of "batched" users to the user table.
        /// </summary>
        /// <param name="users">DataTable of users and their properties</param>
        public static void SynchronizeUserBatch(DataTable userBatch)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@UserBatch", userBatch, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.SynchronizeUserBatch]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetGUIDs
        /// <summary>
        /// Gets a listing of GUIDs for all users in the site.
        /// Used in AD synchronization.
        /// </summary>
        /// <returns>DataTable of user ids and GUIDs</returns>
        public static DataTable GetGUIDs()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {

                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.GetGUIDs]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IsMemberOfGroup
        /// <summary>
        /// Determines if the current session user is a member of a specified group.
        /// </summary>
        /// <param name="idGroup">Group Id</param>
        public static bool IsMemberOfGroup(int idGroup)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idGroup", idGroup, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isUserMemberOfGroup", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[User.IsMemberOfGroup]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserMemberOfGroup"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IsCourseEnrolled
        /// <summary>
        /// Determines if the current session user has a current enrollment in the specified course.
        /// </summary>
        /// <param name="idCourse">Course Id</param>
        public static bool IsCourseEnrolled(int idCourse)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isUserEnrolledInCourse", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[User.IsCourseEnrolled]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserEnrolledInCourse"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IsLearningPathEnrolled
        /// <summary>
        /// Determines if the current session user has a current enrollment in the specified learning path.
        /// </summary>
        /// <param name="idLearningPath">LearningPath Id</param>
        public static bool IsLearningPathEnrolled(int idLearningPath)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPath", idLearningPath, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isUserEnrolled", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[User.IsLearningPathEnrolled]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserEnrolled"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IsJoinedToTheStandupTrainingSession
        /// <summary>
        /// Determines if the current session user has a current enrollment in the specified standup training session.
        /// </summary>
        /// <param name="idLearningPath">LearningPath Id</param>
        public static bool IsJoinedToTheStandupTrainingSession(int idStandupTraining)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTraining", idStandupTraining, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isUserEnrolled", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[User.IsStandupTrainingEnrolled]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserEnrolled"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForResourceOwnersSelectList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <param name="idResource">resource id</param>
        /// <returns>DataTable of user (owner) ids and names for specified resource excluding current user (owner).</returns>
        public static DataTable IdsAndNamesForResourceOwnersSelectList(int idResource, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idResource", idResource, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.IdsAndNamesForResourceOwnersSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion                

        #region DoUserAccountsNeedApproval
        /// <summary>
        /// Determines if there are user accounts that need approval.  This is needed in the
        /// case of user approval being turned off in the system, but there are still accounts
        /// that require approval.
        /// </summary>
        /// <returns>do user accounts need approval?</returns>
        public static bool DoUserAccountsNeedApproval()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.Account);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@searchParam", null, SqlDbType.NVarChar, 4000, ParameterDirection.Input);
            databaseObject.AddParameter("@pageNum", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@pageSize", 10, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@orderColumn", null, SqlDbType.NVarChar, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@orderAsc", null, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                DataSet ds = databaseObject.ExecuteDataSet("[Widget.UserRegistrationApproval]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                int numAccountsNeedingApproval = Convert.ToInt32(ds.Tables[0].Rows[0]["row_count"]);

                if (numAccountsNeedingApproval == 0)
                { return false; }
                else
                { return true; }
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region MergeUserAccounts
        /// <summary>
        /// Merges the source user account into the destination user account
        /// </summary>
        /// <param name="idSourceUser"></param>
        /// <param name="idDestinationUser"></param>
        public static void MergeUserAccounts(int idUserSource, int idUserDestination)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.Account);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUserSource", idUserSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUserDestination", idUserDestination, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.MergeUserAccounts]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves user data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved user</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@firstName", this.FirstName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@middleName", this.MiddleName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@lastName", this.LastName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@avatar", this.Avatar, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@email", this.Email, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@username", this.Username, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@password", this.Password, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@objectGUID", (this.ObjectGUID == null) ? (object)DBNull.Value : Guid.Parse(this.ObjectGUID), SqlDbType.UniqueIdentifier, 36, ParameterDirection.Input);
            databaseObject.AddParameter("@activationGUID", (this.ActivationGUID == null) ? (object)DBNull.Value : Guid.Parse(this.ActivationGUID), SqlDbType.UniqueIdentifier, 36, ParameterDirection.Input);
            databaseObject.AddParameter("@distinguishedName", this.DistinguishedName, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@isActive", this.IsActive, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@mustChangePassword", this.MustChangePassword, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtExpires", this.DtExpires, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", this.LanguageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@employeeId", this.EmployeeId, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@company", this.Company, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@address", this.Address, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@city", this.City, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@province", this.Province, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@postalCode", this.PostalCode, SqlDbType.NVarChar, 25, ParameterDirection.Input);
            databaseObject.AddParameter("@country", this.Country, SqlDbType.NVarChar, 50, ParameterDirection.Input);
            databaseObject.AddParameter("@phonePrimary", this.PhonePrimary, SqlDbType.NVarChar, 25, ParameterDirection.Input);
            databaseObject.AddParameter("@phoneWork", this.PhoneWork, SqlDbType.NVarChar, 25, ParameterDirection.Input);
            databaseObject.AddParameter("@phoneFax", this.PhoneFax, SqlDbType.NVarChar, 25, ParameterDirection.Input);
            databaseObject.AddParameter("@phoneHome", this.PhoneHome, SqlDbType.NVarChar, 25, ParameterDirection.Input);
            databaseObject.AddParameter("@phoneMobile", this.PhoneMobile, SqlDbType.NVarChar, 25, ParameterDirection.Input);
            databaseObject.AddParameter("@phonePager", this.PhonePager, SqlDbType.NVarChar, 25, ParameterDirection.Input);
            databaseObject.AddParameter("@phoneOther", this.PhoneOther, SqlDbType.NVarChar, 25, ParameterDirection.Input);
            databaseObject.AddParameter("@department", this.Department, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@division", this.Division, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@region", this.Region, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@jobTitle", this.JobTitle, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@jobClass", this.JobClass, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@gender", this.Gender, SqlDbType.NVarChar, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@race", this.Race, SqlDbType.NVarChar, 64, ParameterDirection.Input);
            databaseObject.AddParameter("@dtDOB", this.DtDOB, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@dtHire", this.DtHire, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@dtTerm", this.DtTerm, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@field00", this.Field00, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field01", this.Field01, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field02", this.Field02, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field03", this.Field03, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field04", this.Field04, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field05", this.Field05, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field06", this.Field06, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field07", this.Field07, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field08", this.Field08, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field09", this.Field09, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field10", this.Field10, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field11", this.Field11, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field12", this.Field12, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field13", this.Field13, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field14", this.Field14, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field15", this.Field15, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field16", this.Field16, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field17", this.Field17, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field18", this.Field18, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@field19", this.Field19, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            if (this.IsRegistrationApproved == null)
            { databaseObject.AddParameter("@isRegistrationApproved", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isRegistrationApproved", this.IsRegistrationApproved, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.DtApproved == null)
            { databaseObject.AddParameter("@dtApproved", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dtApproved", this.DtApproved, SqlDbType.DateTime, 8, ParameterDirection.Input); }

            if (this.DtDenied == null)
            { databaseObject.AddParameter("@dtDenied", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dtDenied", this.DtDenied, SqlDbType.DateTime, 8, ParameterDirection.Input); }

            if (this.IdApprover == null)
            { databaseObject.AddParameter("@idApprover", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idApprover", this.IdApprover, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@rejectionComments", this.RejectionComments, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            databaseObject.AddParameter("@disableLoginFromLoginForm", this.DisableLoginFromLoginForm, SqlDbType.Bit, 1, ParameterDirection.Input);

            databaseObject.AddParameter("@optOutOfEmailNotifications", this.OptOutOfEmailNotifications, SqlDbType.Bit, 1, ParameterDirection.Input); 

            databaseObject.AddParameter("@excludeFromLeaderboards", this.ExcludeFromLeaderboards, SqlDbType.Bit, 1, ParameterDirection.Input); 

            try
            {
                databaseObject.ExecuteNonQuery("[User.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved user
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idUser"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SaveAvatar
        /// <summary>
        /// Saves user's avatar path to database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved user</returns>
        private void _SaveAvatar(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@avatar", this.Avatar, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.SaveAvatar]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveUserAgreementAgreed
        /// <summary>
        /// Saves user agreement agreed
        /// </summary>
        public void SaveUserAgreementAgreed(int idCaller, int idUser)
        {
            this._SaveUserAgreementAgreed(idCaller, idUser);
        }
        #endregion

        #region _SaveUserAgreementAgreed
        /// <summary>
        /// Saves user's avatar path to database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved user</returns>
        private void _SaveUserAgreementAgreed(int idCaller, int idUser)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[User.SaveUserAgreementAgreed]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified user's properties.
        /// </summary>
        /// <param name="idUser">User Id</param>
        private void _Details(int idUser)
        {
            if (idUser == 0)
            { return; }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@firstName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@middleName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@lastName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@displayName", null, SqlDbType.NVarChar, 768, ParameterDirection.Output);
            databaseObject.AddParameter("@avatar", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@email", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@username", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idTimezone", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@objectGUID", null, SqlDbType.UniqueIdentifier, 36, ParameterDirection.Output);
            databaseObject.AddParameter("@activationGUID", null, SqlDbType.UniqueIdentifier, 36, ParameterDirection.Output);
            databaseObject.AddParameter("@distinguishedName", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@isActive", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@mustChangePassword", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtExpires", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@idLanguage", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@languageString", null, SqlDbType.NVarChar, 10, ParameterDirection.Output);
            databaseObject.AddParameter("@dtModified", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtLastLogin", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@employeeId", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@company", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@address", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@city", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@province", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@postalCode", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@country", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@phonePrimary", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@phoneWork", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@phoneFax", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@phoneHome", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@phoneMobile", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@phonePager", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@phoneOther", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@department", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@division", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@region", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@jobTitle", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@jobClass", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@gender", null, SqlDbType.NVarChar, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@race", null, SqlDbType.NVarChar, 64, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDOB", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtHire", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtTerm", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@field00", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field01", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field02", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field03", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field04", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field05", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field06", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field07", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field08", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field09", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field10", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field11", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field12", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field13", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field14", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field15", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field16", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field17", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field18", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@field19", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@isUserACourseExpert", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isUserACourseApprover", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isUserASupervisor", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isUserAWallModerator", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isUserAnILTInstructor", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@groupMemberships", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@isRegistrationApproved", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtApproved", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDenied", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@idApprover", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@rejectionComments", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@disableLoginFromLoginForm", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@optOutOfEmailNotifications", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@excludeFromLeaderboards", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[User.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);
                this.FirstName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@firstName"].Value);
                this.MiddleName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@middleName"].Value);
                this.LastName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@lastName"].Value);
                this.DisplayName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@displayName"].Value);
                this.Avatar = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@avatar"].Value);
                this.Email = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@email"].Value);
                this.Username = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@username"].Value);
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdTimezone = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idTimezone"].Value);
                this.ObjectGUID = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@objectGUID"].Value);
                this.ActivationGUID = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@activationGUID"].Value);
                this.DistinguishedName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@distinguishedName"].Value);
                this.IsActive = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isActive"].Value);
                this.MustChangePassword = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@mustChangePassword"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtExpires = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtExpires"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
                this.IdLanguage = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idLanguage"].Value);
                this.LanguageString = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@languageString"].Value);
                this.DtModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
                this.DtLastLogin = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtLastLogin"].Value);
                this.EmployeeId = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@employeeId"].Value);
                this.Company = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@company"].Value);
                this.Address = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@address"].Value);
                this.City = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@city"].Value);
                this.Province = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@province"].Value);
                this.PostalCode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@postalCode"].Value);
                this.Country = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@country"].Value);
                this.PhonePrimary = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@phonePrimary"].Value);
                this.PhoneWork = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@phoneWork"].Value);
                this.PhoneFax = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@phoneFax"].Value);
                this.PhoneHome = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@phoneHome"].Value);
                this.PhoneMobile = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@phoneMobile"].Value);
                this.PhonePager = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@phonePager"].Value);
                this.PhoneOther = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@phoneOther"].Value);
                this.Department = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@department"].Value);
                this.Division = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@division"].Value);
                this.Region = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@region"].Value);
                this.JobTitle = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@jobTitle"].Value);
                this.JobClass = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@jobClass"].Value);
                this.Gender = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@gender"].Value);
                this.Race = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@race"].Value);
                this.DtDOB = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDOB"].Value);
                this.DtHire = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtHire"].Value);
                this.DtTerm = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtTerm"].Value);
                this.Field00 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field00"].Value);
                this.Field01 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field01"].Value);
                this.Field02 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field02"].Value);
                this.Field03 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field03"].Value);
                this.Field04 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field04"].Value);
                this.Field05 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field05"].Value);
                this.Field06 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field06"].Value);
                this.Field07 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field07"].Value);
                this.Field08 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field08"].Value);
                this.Field09 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field09"].Value);
                this.Field10 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field10"].Value);
                this.Field11 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field11"].Value);
                this.Field12 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field12"].Value);
                this.Field13 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field13"].Value);
                this.Field14 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field14"].Value);
                this.Field15 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field15"].Value);
                this.Field16 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field16"].Value);
                this.Field17 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field17"].Value);
                this.Field18 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field18"].Value);
                this.Field19 = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@field19"].Value);
                this.IsUserACourseExpert = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserACourseExpert"].Value);
                this.IsUserACourseApprover = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserACourseApprover"].Value);
                this.IsUserASupervisor = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserASupervisor"].Value);
                this.IsUserAWallModerator = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserAWallModerator"].Value);
                this.IsUserAnILTInstructor = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isUserAnILTInstructor"].Value);
                this.IsRegistrationApproved = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isRegistrationApproved"].Value);
                this.DtApproved = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtApproved"].Value);
                this.DtDenied = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDenied"].Value);
                this.IdApprover = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idApprover"].Value);
                this.RejectionComments = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@rejectionComments"].Value);
                this.DisableLoginFromLoginForm = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@disableLoginFromLoginForm"].Value);
                this.OptOutOfEmailNotifications = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@optOutOfEmailNotifications"].Value);
                this.ExcludeFromLeaderboards = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@excludeFromLeaderboards"].Value);

                // get the user's group memberships
                string groupMemberships = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@groupMemberships"].Value);
                if (!String.IsNullOrWhiteSpace(groupMemberships))
                {
                    this.GroupMemberships = new List<int>();
                    string[] groupMembershipsArray = groupMemberships.Split(',');

                    int idGroup;
                    foreach (string str in groupMembershipsArray)
                    {
                        if (Int32.TryParse(str, out idGroup))
                        { this.GroupMemberships.Add(idGroup); }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
