﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;
using Asentia.Common;
using Newtonsoft.Json;
using System.Collections;
using System.IO;
using System.Web;

namespace Asentia.UMS.Library
{
    #region DocumentRepositoryObjectType ENUM
    public enum DocumentRepositoryObjectType
    {
        Group = 1,
        Course = 2,
        LearningPath = 3,
        Profile = 4,
    }
    #endregion

    [Serializable]
    public class DocumentRepositoryItem
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public DocumentRepositoryItem()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idDocumentRepositoryItem">Document Repository Item Id</param>
        public DocumentRepositoryItem(int idDocumentRepositoryItem)
        {
            this.LanguageSpecificProperties = new ArrayList();
            this._GetPropertiesInLanguages(idDocumentRepositoryItem);
            this._Details(idDocumentRepositoryItem);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[DocumentRepositoryItem.GetGrid]";

        /// <summary>
        /// The stored procedure used to populate My Documents widget
        /// </summary>
        public static readonly string GridProcedureForUserDashboard = "[Widget.Documents]";

        /// <summary>
        /// Id
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Document Repository Object Type
        /// <seealso cref="DocumentRepositoryObjectType" />
        /// </summary>
        public DocumentRepositoryObjectType IdDocumentRepositoryObjectType;

        /// <summary>
        /// The object id this item belongs to.
        /// <seealso cref="DocumentRepositoryObjectType" />
        /// </summary>
        public int IdObject;

        /// <summary>
        /// The Document Repository Folder this item belongs to.
        /// <seealso cref="DocumentRepositoryFolder" />
        /// </summary>
        public int? IdDocumentRepositoryFolder;

        /// <summary>
        /// The owner (creator) of this item.
        /// <seealso cref="User" />
        /// </summary>
        public int IdOwner;

        /// <summary>
        /// Title
        /// </summary>
        public string Label;

        /// <summary>
        /// File Name
        /// </summary>
        public string FileName;

        /// <summary>
        /// Size in KB
        /// </summary>
        public int Kb;

        /// <summary>
        /// Search Tags.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string SearchTags;

        /// <summary>
        /// Is Private
        /// </summary>
        public bool? IsPrivate;

        /// <summary>
        /// Id Language
        /// </summary>
        public int IdLanguage;

        /// <summary>
        /// Language String
        /// Language code that is representatibe of a "culture code," i.e. 'en-US'.
        /// </summary>
        public string LanguageString;

        /// <summary>
        /// Is this item valid for all languages?
        /// </summary>
        public bool? IsAllLanguages;

        /// <summary>
        /// Date Created
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Is Deleted
        /// </summary>
        public bool? IsDeleted;

        /// <summary>
        /// Date Deleted
        /// </summary>
        public DateTime? DtDeleted;

        /// <summary>
        /// Is this item visible to user (only necessary for profile files)
        /// </summary>
        public bool? IsVisibleToUser;

        /// <summary>
        /// Is this item uploaded by the user? (only necessary for profile files)
        /// </summary>
        public bool? IsUploadedByUser;

        /// ArrayList of language specific properties.
        /// </summary>
        [NonSerialized]
        [JsonIgnore]
        public ArrayList LanguageSpecificProperties;
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Label;
            public string SearchTags;

            /// <summary>
            /// parameterless constructor for serialization purpose
            /// </summary>
            public LanguageSpecificProperty()
            { ;}

            public LanguageSpecificProperty(string langString, string label, string searchTags)
            {
                this.LangString = langString;
                this.Label = label;
                this.SearchTags = searchTags;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves document data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves document repository item data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this item.
        /// </summary>
        /// <param name="languageString">The language string.</param>
        /// <param name="label">The label.</param>
        /// <exception cref="AsentiaException"></exception>
        public void SaveLang(string languageString, string label, string searchTags)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureDocumentRepositoryItemSaveLangCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDocumentRepositoryItem", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@label", label, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", searchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[DocumentRepositoryItem.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a document repository item into a collection.
        /// </summary>
        /// <param name="idDocumentRepositoryItem">The identifier document repository item.</param>
        private void _GetPropertiesInLanguages(int idDocumentRepositoryItem)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDocumentRepositoryItem", idDocumentRepositoryItem, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[DocumentRepositoryItem.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["label"].ToString(),
                            sdr["searchTags"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Document Repository Item(s).
        /// </summary>
        /// <param name="deletees">DataTable of document repository items to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@DocumentRepositoryItems", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[DocumentRepositoryItem.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetObjectDocumentsInFolder
        /// <summary>
        /// Gets a listing of documents as per their object type
        /// </summary>
        /// <param name="idObject"></param>
        /// <param name="idObjectType"></param>
        /// <param name="idDocumentRepositoryFolder">if passed then gives documents within that folder only else gives documents which don't belong to any folder</param>
        /// <returns></returns>
        public static DataTable GetObjectDocumentsInFolder(int idObject, int idObjectType, int? idDocumentRepositoryFolder = null)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idObject", idObject, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idObjectType", idObjectType, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idDocumentRepositoryFolder", idDocumentRepositoryFolder, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[DocumentRepositoryItem.GetObjectDocumentsInFolder]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCorrectFileName
        /// <summary>
        /// Gets the correct name  of file.
        /// </summary>
        /// <param name="fullSavedFilePath">The full saved file path.</param>
        /// <param name="fileOriginalname">The file originalname.</param>
        /// <returns></returns>
        public static string GetCorrectFileName(string fullSavedFilePath, out string fileOriginalname)
        {
            int count = 1;
            string modifiedfullPath = fullSavedFilePath;
            string fileNameOnly = Path.GetFileNameWithoutExtension(fullSavedFilePath);
            string extension = Path.GetExtension(fullSavedFilePath);
            fileOriginalname = fileNameOnly + extension;
            
            //file location
            string path = Path.GetDirectoryName(fullSavedFilePath);

            //file name with incremented suffix if its already wxist with same name
            while (File.Exists(HttpContext.Current.Server.MapPath((modifiedfullPath))))
            {
                string tempFileName = string.Format("{0}({1})", fileNameOnly, count++);
                modifiedfullPath = Path.Combine(path, tempFileName + extension);
                fileOriginalname = tempFileName + extension;
            }

            return modifiedfullPath;
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves document item data to the database.
        /// </summary>
        /// <param name="idCaller"></param>
        /// <returns></returns>
        public int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDocumentRepositoryItem", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idDocumentRepositoryObjectType", this.IdDocumentRepositoryObjectType, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idObject", this.IdObject, SqlDbType.Int, 4, ParameterDirection.Input);
            
            if (this.IdDocumentRepositoryFolder == null)
            { databaseObject.AddParameter("@idDocumentRepositoryFolder", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idDocumentRepositoryFolder", this.IdDocumentRepositoryFolder, SqlDbType.Int, 4, ParameterDirection.Input); }
            
            databaseObject.AddParameter("@idOwner", this.IdOwner, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@label", this.Label, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@fileName", this.FileName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@kb", this.Kb, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", this.SearchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            
            if (this.IsPrivate == null)
            { databaseObject.AddParameter("@isPrivate", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isPrivate", this.IsPrivate, SqlDbType.Bit, 1, ParameterDirection.Input); }

            databaseObject.AddParameter("@languageString", this.LanguageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            
            if (this.IsAllLanguages == null)
            { databaseObject.AddParameter("@isAllLanguages", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isAllLanguages", this.IsAllLanguages, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if(this.IsVisibleToUser == null)
            { databaseObject.AddParameter("@isVisibleToUser", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isVisibleToUser", this.IsVisibleToUser, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if(this.IsUploadedByUser == null)
            { databaseObject.AddParameter("@isUploadedByUser", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isUploadedByUser", this.IsUploadedByUser, SqlDbType.Bit, 1, ParameterDirection.Input); }
            
            try
            {
                databaseObject.ExecuteNonQuery("[DocumentRepositoryItem.Save]", true);
                
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                
                // set the id of the saved course
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idDocumentRepositoryItem"].Value);
                
                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified document repository item's properties.
        /// </summary>
        /// <param name="idDocumentRepositoryItem">Document Repositiry Item Id</param>
        private void _Details(int idDocumentRepositoryItem)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDocumentRepositoryItem", idDocumentRepositoryItem, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idDocumentRepositoryObjectType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idObject", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idDocumentRepositoryFolder", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idOwner", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@fileName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@label", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@kb", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@searchTags", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@isPrivate", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@idLanguage", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@languageString", null, SqlDbType.NVarChar, 10, ParameterDirection.Output);
            databaseObject.AddParameter("@isAllLanguages", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isVisibleToUser", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isUploadedByUser", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[DocumentRepositoryItem.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idDocumentRepositoryItem;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdDocumentRepositoryObjectType = (DocumentRepositoryObjectType)AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idDocumentRepositoryObjectType"].Value);
                this.IdObject = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idObject"].Value);
                this.IdDocumentRepositoryFolder = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idDocumentRepositoryFolder"].Value);
                this.IdOwner = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idOwner"].Value);
                this.FileName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@fileName"].Value);
                this.Label = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@label"].Value);
                this.Kb = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@kb"].Value);
                this.SearchTags = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@searchTags"].Value);
                this.IsPrivate = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isPrivate"].Value);
                this.IdLanguage = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idLanguage"].Value);
                this.LanguageString = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@languageString"].Value);
                this.IsAllLanguages = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isAllLanguages"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
                this.IsVisibleToUser = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isVisibleToUser"].Value);
                this.IsUploadedByUser = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isUploadedByUser"].Value);
                
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
