﻿using System;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;

namespace Asentia.UMS.Library
{
    public class WebMeetingOrganizer
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public WebMeetingOrganizer()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="WebMeetingOrganizer" /> are not found.
        /// </exception>
        /// <param name="idWebMeetingOrganizer">WebMeetingOrganizer Id</param>
        public WebMeetingOrganizer(int idWebMeetingOrganizer)
        {
            this._Details(idWebMeetingOrganizer);
        }
        #endregion

        #region Properties
        /// <summary>
        /// WebMeetingOrganizer Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        public int IdSite;

        /// <summary>
        /// User Id of the user this organizer credential set is keyed to. 
        /// </summary>
        public int? IdUser;

        /// <summary>
        /// The type of web meeting organizer this is. Keyed to "MeetingType" enum values,
        /// GoToMeeting (2), GoToWebinar (3), GoToTraining (4), WebEx (5)
        /// </summary>
        public int OrganizerType;

        /// <summary>
        /// The organizer's username.
        /// </summary>
        public string Username;

        /// <summary>
        /// The organizer's password. Encrypted in database.
        /// </summary>
        public string Password;
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@WebMeetingOrganizers", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[WebMeetingOrganizer.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndUsernamesForSelectList
        /// <summary>
        /// Gets a listing of web meeting organizer ids, types, and usernames that are tied to the calling
        /// user, for population into a select list.
        /// </summary>       
        /// <returns>DataTable</returns>
        public static DataTable IdsAndUsernamesForSelectList()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[WebMeetingOrganizer.IdsAndUsernamesForSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved web meeting organizer</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idWebMeetingOrganizer", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@organizerType", this.OrganizerType, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@username", this.Username, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@password", this.Password, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[WebMeetingOrganizer.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idWebMeetingOrganizer"].Value);

                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idWebMeetingOrganizer">WebMeetingOrganizer Id</param>
        private void _Details(int idWebMeetingOrganizer)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idWebMeetingOrganizer", idWebMeetingOrganizer, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@organizerType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@username", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@password", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[WebMeetingOrganizer.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idWebMeetingOrganizer;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdUser = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idUser"].Value);
                this.OrganizerType = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@organizerType"].Value);
                this.Username = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@username"].Value);
                this.Password = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@password"].Value);                
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
