﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;

namespace Asentia.UMS.Library
{
    /// <summary>
    /// Represents user fields global rules to be loaded from XML file.
    /// </summary>
    public class UserFieldGlobalRules
    {
        #region Properties
        /// <summary>
        /// Field Identifier
        /// </summary>
        public string Identifier;

        /// <summary>
        /// User Object Property Name
        /// </summary>
        public string UserObjectPropertyName;

        /// <summary>
        /// Can a type be assigned to the field?
        /// </summary>
        public bool IsTypeable;

        /// <summary>
        /// Is the field always required?
        /// </summary>
        public bool AlwaysRequired;

        /// <summary>
        /// Is the field always visible?
        /// </summary>
        public bool AlwaysVisible;

        /// <summary>
        /// Must the field have a unique value?
        /// </summary>
        public bool IsUnique;

        /// <summary>
        /// Is the Admin allowed to modify the field?
        /// </summary>
        public bool AdminModifyAllowed;

        /// <summary>
        /// Is the Admin allowed to view the field?
        /// </summary>
        public bool AdminViewAllowed;

        /// <summary>
        /// Is the User allowed to modify the field?
        /// </summary>
        public bool UserModifyAllowed;

        /// <summary>
        /// Is the User allowed to view the field?
        /// </summary>
        public bool UserViewAllowed;

        /// <summary>
        /// Can the field be included on the registration form?
        /// </summary>
        public bool RegistrationPageAllowed;

        /// <summary>
        /// Can field be used in rule sets?
        /// </summary>
        public bool RuleSetEligible;

        /// <summary>
        /// Can field be used in the leaderboard "relative to" filter?
        /// </summary>
        public bool LeaderboardRelativeToEligible;

        /// <summary>
        /// The data type of the field.
        /// </summary>
        public UserAccountData.DataType DataType;

        /// <summary>
        /// The maximum length of the field, per database constraint.
        /// </summary>
        public int MaxLength;
        #endregion

        #region Private Properties
        /// <summary>
        /// Location/namespace of the "global user field rules" XML.
        /// </summary>
        private const string _USERFIELD_GLOBALRULES_FILE = "Asentia.UMS.Library.UserFields.xml";
        #endregion

        #region Methods
        #region GetAllRules
        /// <summary>
        /// Loads all global rules from XML file and returns them.
        /// </summary>
        /// <returns>Returns list of user fields rules.</returns>
        public static List<UserFieldGlobalRules> GetAllRules()
        {
            List<UserFieldGlobalRules> rulesList = new List<UserFieldGlobalRules>();

            Assembly assembly = Assembly.GetExecutingAssembly();
            XmlTextReader reader = new XmlTextReader(assembly.GetManifestResourceStream(_USERFIELD_GLOBALRULES_FILE));
            XmlDocument document = new XmlDocument();
            document.Load(reader);

            XmlNodeList nodeList = document.SelectNodes("userFields/field");
            foreach (XmlNode node in nodeList)
            {
                UserFieldGlobalRules rules = new UserFieldGlobalRules();

                rules.Identifier = node.Attributes["identifier"].Value;
                rules.UserObjectPropertyName = node.Attributes["userObjectPropertyName"].Value;
                rules.IsTypeable = Convert.ToBoolean(node.Attributes["isTypeable"].Value);
                rules.AlwaysRequired = Convert.ToBoolean(node.Attributes["alwaysRequired"].Value);
                rules.AlwaysVisible = Convert.ToBoolean(node.Attributes["alwaysVisible"].Value);
                rules.IsUnique = Convert.ToBoolean(node.Attributes["isUnique"].Value);
                rules.AdminModifyAllowed = Convert.ToBoolean(node.Attributes["adminModifyAllowed"].Value);
                rules.AdminViewAllowed = Convert.ToBoolean(node.Attributes["adminViewAllowed"].Value);
                rules.UserModifyAllowed = Convert.ToBoolean(node.Attributes["userModifyAllowed"].Value);
                rules.UserViewAllowed = Convert.ToBoolean(node.Attributes["userViewAllowed"].Value);
                rules.RegistrationPageAllowed = Convert.ToBoolean(node.Attributes["registrationPageAllowed"].Value);
                rules.RuleSetEligible = Convert.ToBoolean(node.Attributes["ruleSetEligible"].Value);
                rules.LeaderboardRelativeToEligible = Convert.ToBoolean(node.Attributes["leaderboardRelativeToEligible"].Value);
                rules.DataType = (UserAccountData.DataType)Enum.Parse(typeof(UserAccountData.DataType), node.Attributes["dataType"].Value);
                rules.MaxLength = Convert.ToInt32(node.Attributes["maxLength"].Value);

                rulesList.Add(rules);
            }

            return rulesList;
        }
        #endregion
        #endregion
    }
}
