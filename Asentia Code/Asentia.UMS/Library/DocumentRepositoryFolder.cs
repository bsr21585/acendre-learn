﻿using Asentia.Common;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Asentia.UMS.Library
{
    public class DocumentRepositoryFolder
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public DocumentRepositoryFolder()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idDocumentRepositoryfolder">Document Repository folder Id</param>
        public DocumentRepositoryFolder(int idDocumentRepositoryFolder)
        {
            this._Details(idDocumentRepositoryFolder);
        }
        #endregion

        #region properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[DocumentRepositoryFolder.GetGrid]";

        /// <summary>
        /// Id DocumentRepositoryFolder
        /// </summary>
        public int Id;

        /// <summary>
        /// Id Site
        /// </summary>
        public int IdSite;

        /// <summary>
        /// id Object 
        /// </summary>
        public int IdObject;

        /// <summary>
        /// Id DocumnetRepositoryFolder Type
        /// </summary>
        public DocumentRepositoryObjectType IdobjectType;

        /// <summary>
        /// DocumnetRepositoryFolder Name
        /// </summary>
        public string FolderNmae;

        /// <summary>
        /// Is Deleted
        /// </summary>
        public bool? IsDeleted;

        /// <summary>
        /// Date Deleted
        /// </summary>
        public DateTime? DtDeleted;

        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves course data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save();
        }

        #endregion
        #endregion

        #region Static Methods
        #region IdsAndNamesForDocumentRepositorySelectList
        /// <summary>
        /// Gets a listing of document repository folder ids nad names
        /// </summary>
        /// <param name="idObject"></param>
        /// <param name="IdObjectType"></param>
        /// <param name="searchParam"></param>
        /// <returns></returns>
        public static DataTable IdsAndNamesForDocumentRepositorySelectList(int idObject, int idObjectType)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idObject", idObject, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idObjectType", idObjectType, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[DocumentRepositoryFolder.IdsAndNamesObjectSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// Deletes Document Repository folder(s).
        /// </summary>
        /// <param name="deletees">DataTable of document repository folders to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@DocumentRepositoryfolders", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[DocumentRepositoryfolder.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves course data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved course</returns>
        public int _Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDocumentRepositoryFolder", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idDocumentRepositoryObjectType", this.IdobjectType, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idObject", this.IdObject, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@folderName", this.FolderNmae, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[DocumentRepositoryFolder.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved course
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idDocumentRepositoryFolder"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified document repository folder's properties.
        /// </summary>
        /// <param name="idDocumentRepositoryfolder">Document Repositiry folder Id</param>
        private void _Details(int idDocumentRepositoryFolder)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDocumentRepositoryfolder", idDocumentRepositoryFolder, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idObjectType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idObject", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@folderName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[DocumentRepositoryFolder.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idDocumentRepositoryFolder;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdobjectType = (DocumentRepositoryObjectType)AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idObjectType"].Value);
                this.IdObject = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idObject"].Value);
                this.FolderNmae = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@folderName"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
    public enum NodeSubType
    {
        Folder = 1,
        Document = 2
    }
}
