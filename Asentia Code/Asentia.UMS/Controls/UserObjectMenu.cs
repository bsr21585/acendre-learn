﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Controls
{
    public class UserObjectMenu : WebControl
    {
        #region Constructor
        public UserObjectMenu(User userObject)
            : base(HtmlTextWriterTag.Div)
        {
            this.UserObject = userObject;
            this.ID = "UserObjectMenu";
        }
        #endregion

        #region Properties
        /// <summary>
        /// The user that this menu is built for.
        /// </summary>
        public User UserObject;

        /// <summary>
        /// The selected menu item.
        /// </summary>
        public MenuObjectItem SelectedItem = MenuObjectItem.None;
        #endregion

        #region MenuObjectItem ENUM
        public enum MenuObjectItem
        {
            None = 0,
            UserDashboard = 1,
            UserProfile = 2,
            Enrollments = 3,
            Certifications = 4,
            Groups = 5,
            Certificates = 6,
            Transcript = 7,
            ImportActivityData = 8,
            ImportCertificateData = 9,
            Roles = 10,            
        }
        #endregion

        #region Private Methods
        #region _BuildUserObjectMenu
        private void _BuildUserObjectMenu()
        {
            this.CssClass = "ObjectMenu";

            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this.UserObject.GroupMemberships))
            {
                // USER DASHBOARD

                // user dashboard link container
                Panel userDashboardLinkContainer = new Panel();
                userDashboardLinkContainer.ID = "UserDashboardLinkContainer";
                userDashboardLinkContainer.CssClass = "ObjectMenuLinkContainer";
                userDashboardLinkContainer.ToolTip = _GlobalResources.UserDashboard;

                HyperLink userDashboardImageLink = new HyperLink();
                userDashboardImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD, ImageFiles.EXT_PNG);
                userDashboardImageLink.NavigateUrl = "/administrator/users/Dashboard.aspx?id=" + this.UserObject.Id.ToString();
                userDashboardLinkContainer.Controls.Add(userDashboardImageLink);

                this.Controls.Add(userDashboardLinkContainer);

                // USER PROFILE

                // user profile link container
                Panel userProfileLinkContainer = new Panel();
                userProfileLinkContainer.ID = "UserProfileLinkContainer";
                userProfileLinkContainer.CssClass = "ObjectMenuLinkContainer";
                userProfileLinkContainer.ToolTip = _GlobalResources.UserProfile;

                HyperLink userProfileImageLink = new HyperLink();
                userProfileImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);
                userProfileImageLink.NavigateUrl = "/administrator/users/Modify.aspx?id=" + this.UserObject.Id.ToString();
                userProfileLinkContainer.Controls.Add(userProfileImageLink);

                this.Controls.Add(userProfileLinkContainer);                

                // ENROLLMENTS

                // enrollments link container
                Panel enrollmentsLinkContainer = new Panel();
                enrollmentsLinkContainer.ID = "EnrollmentsLinkContainer";
                enrollmentsLinkContainer.CssClass = "ObjectMenuLinkContainer";
                enrollmentsLinkContainer.ToolTip = _GlobalResources.Enrollments;

                HyperLink enrollmentsImageLink = new HyperLink();
                enrollmentsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
                enrollmentsImageLink.NavigateUrl = "/administrator/users/enrollments/Default.aspx?uid=" + this.UserObject.Id.ToString();
                enrollmentsLinkContainer.Controls.Add(enrollmentsImageLink);

                this.Controls.Add(enrollmentsLinkContainer);

                // CERTIFICATIONS        

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                {
                    // certifications link container
                    Panel certificationsLinkContainer = new Panel();
                    certificationsLinkContainer.ID = "CertificationsLinkContainer";
                    certificationsLinkContainer.CssClass = "ObjectMenuLinkContainer";
                    certificationsLinkContainer.ToolTip = _GlobalResources.Certifications;

                    HyperLink certificationsImageLink = new HyperLink();
                    certificationsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
                    certificationsImageLink.NavigateUrl = "/administrator/users/certifications/Default.aspx?uid=" + this.UserObject.Id.ToString();
                    certificationsLinkContainer.Controls.Add(certificationsImageLink);

                    this.Controls.Add(certificationsLinkContainer);
                }

                // GROUPS

                // groups link container
                Panel groupsLinkContainer = new Panel();
                groupsLinkContainer.ID = "GroupsLinkContainer";
                groupsLinkContainer.CssClass = "ObjectMenuLinkContainer";
                groupsLinkContainer.ToolTip = _GlobalResources.Groups;

                HyperLink groupsImageLink = new HyperLink();
                groupsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
                groupsImageLink.NavigateUrl = "/administrator/users/Groups.aspx?uid=" + this.UserObject.Id.ToString();
                groupsLinkContainer.Controls.Add(groupsImageLink);

                this.Controls.Add(groupsLinkContainer);

                // CERTIFICATES

                // certificates link container
                Panel certificatesLinkContainer = new Panel();
                certificatesLinkContainer.ID = "CertificatesLinkContainer";
                certificatesLinkContainer.CssClass = "ObjectMenuLinkContainer";
                certificatesLinkContainer.ToolTip = _GlobalResources.Certificates;

                HyperLink certificatesImageLink = new HyperLink();
                certificatesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
                certificatesImageLink.NavigateUrl = "/administrator/users/Certificates.aspx?uid=" + this.UserObject.Id.ToString();
                certificatesLinkContainer.Controls.Add(certificatesImageLink);

                this.Controls.Add(certificatesLinkContainer);

                // TRANSCRIPT

                // transcript link container
                Panel transcriptLinkContainer = new Panel();
                transcriptLinkContainer.ID = "TranscriptLinkContainer";
                transcriptLinkContainer.CssClass = "ObjectMenuLinkContainer";
                transcriptLinkContainer.ToolTip = _GlobalResources.Transcript;

                HyperLink transcriptImageLink = new HyperLink();
                transcriptImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_TRANSCRIPT, ImageFiles.EXT_PNG);
                transcriptImageLink.NavigateUrl = "/administrator/users/Transcript.aspx?uid=" + this.UserObject.Id.ToString();
                transcriptLinkContainer.Controls.Add(transcriptImageLink);

                this.Controls.Add(transcriptLinkContainer);

                // IMPORT ACTIVITY DATA

                // import activity data link container
                Panel importActivityDataLinkContainer = new Panel();
                importActivityDataLinkContainer.ID = "ImportActivityDataLinkContainer";
                importActivityDataLinkContainer.CssClass = "ObjectMenuLinkContainer";
                importActivityDataLinkContainer.ToolTip = _GlobalResources.ImportActivityData;

                HyperLink importActivityDataImageLink = new HyperLink();
                importActivityDataImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_IMPORTACTIVITYDATA, ImageFiles.EXT_PNG);
                importActivityDataImageLink.NavigateUrl = "/administrator/activityimport/Default.aspx?uid=" + this.UserObject.Id.ToString();
                importActivityDataLinkContainer.Controls.Add(importActivityDataImageLink);

                this.Controls.Add(importActivityDataLinkContainer);

                // IMPORT CERTIFICATE DATA

                // import certificate data link container
                Panel importCertificateDataLinkContainer = new Panel();
                importCertificateDataLinkContainer.ID = "ImportCertificateDataLinkContainer";
                importCertificateDataLinkContainer.CssClass = "ObjectMenuLinkContainer";
                importCertificateDataLinkContainer.ToolTip = _GlobalResources.ImportCertificateData;

                HyperLink importCertificateDataImageLink = new HyperLink();
                importCertificateDataImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_IMPORTCERTIFICATEDATA, ImageFiles.EXT_PNG);
                importCertificateDataImageLink.NavigateUrl = "/administrator/certificateimport/Default.aspx?uid=" + this.UserObject.Id.ToString();
                importCertificateDataLinkContainer.Controls.Add(importCertificateDataImageLink);

                this.Controls.Add(importCertificateDataLinkContainer);
            }

            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
            {
                // ROLES

                // roles link container
                Panel rolesLinkContainer = new Panel();
                rolesLinkContainer.ID = "RolesLinkContainer";
                rolesLinkContainer.CssClass = "ObjectMenuLinkContainer";
                rolesLinkContainer.ToolTip = _GlobalResources.Roles;

                HyperLink rolesImageLink = new HyperLink();
                rolesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG);
                rolesImageLink.NavigateUrl = "/administrator/users/Roles.aspx?uid=" + this.UserObject.Id.ToString();
                rolesLinkContainer.Controls.Add(rolesImageLink);

                this.Controls.Add(rolesLinkContainer);
            }

            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this.UserObject.GroupMemberships))
            {                
                // CALENDAR

                // calendar link container
                Panel calendarLinkContainer = new Panel();
                calendarLinkContainer.ID = "CalendarLinkContainer";
                calendarLinkContainer.CssClass = "ObjectMenuLinkContainer";
                calendarLinkContainer.ToolTip = _GlobalResources.Calendar;

                Image calendarImageForLink = new Image();
                calendarImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR, ImageFiles.EXT_PNG);

                LinkButton calendarImageLink = new LinkButton();
                calendarImageLink.ID = "LaunchCalendarModal";
                calendarImageLink.Controls.Add(calendarImageForLink);
                calendarLinkContainer.Controls.Add(calendarImageLink);

                this.Controls.Add(calendarLinkContainer);

                if (
                    Asentia.Controls.AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserImpersonator, this.UserObject.GroupMemberships)
                    && this.UserObject.Id != AsentiaSessionState.IdSiteUser
                   )
                {
                    // IMPERSONATE

                    // impersonate link container
                    Panel impersonateLinkContainer = new Panel();
                    impersonateLinkContainer.ID = "ImpersonateLinkContainer";
                    impersonateLinkContainer.CssClass = "ObjectMenuLinkContainer";
                    impersonateLinkContainer.ToolTip = _GlobalResources.Impersonate;

                    HyperLink impersonateImageLink = new HyperLink();
                    impersonateImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN_IMPERSONATE, ImageFiles.EXT_PNG);
                    impersonateImageLink.NavigateUrl = "/administrator/users/Impersonate.aspx?uid=" + this.UserObject.Id.ToString();
                    impersonateLinkContainer.Controls.Add(impersonateImageLink);

                    this.Controls.Add(impersonateLinkContainer);
                }

                // attach a "modal only" calendar object for the "Calendar" link
                Asentia.Controls.Calendar userCalendar = new Asentia.Controls.Calendar(AsentiaSessionState.UserTimezoneCurrentLocalTime, this.UserObject.Id, true, calendarImageLink.ID);
                this.Controls.Add(userCalendar);
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnLoad
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.CreateControls();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        #endregion

        #region CreateControls
        /// <summary>
        /// Creates the controls for rendering to page.
        /// </summary>
        private void CreateControls()
        {
            // build the user object menu
            this._BuildUserObjectMenu();            

            // apply selected class to item
            switch (this.SelectedItem)
            {
                case MenuObjectItem.None:
                    // nothing to do
                    break;
                case MenuObjectItem.UserDashboard:
                    // select the user dashboard link
                    Panel userDashboardLinkContainer = (Panel)this.FindControl("UserDashboardLinkContainer");

                    if (userDashboardLinkContainer != null)
                    { userDashboardLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.UserProfile:
                    // select the user profile link
                    Panel userProfileLinkContainer = (Panel)this.FindControl("UserProfileLinkContainer");

                    if (userProfileLinkContainer != null)
                    { userProfileLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;                
                case MenuObjectItem.Enrollments:
                    // select the enrollments link
                    Panel enrollmentsLinkContainer = (Panel)this.FindControl("EnrollmentsLinkContainer");

                    if (enrollmentsLinkContainer != null)
                    { enrollmentsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Certifications:
                    // select the certifications link
                    Panel certificationsLinkContainer = (Panel)this.FindControl("CertificationsLinkContainer");

                    if (certificationsLinkContainer != null)
                    { certificationsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Groups:
                    // select the groups link
                    Panel groupsLinkContainer = (Panel)this.FindControl("GroupsLinkContainer");

                    if (groupsLinkContainer != null)
                    { groupsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Certificates:
                    // select the certificates link
                    Panel certificatesLinkContainer = (Panel)this.FindControl("CertificatesLinkContainer");

                    if (certificatesLinkContainer != null)
                    { certificatesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Transcript:
                    // select the transcript link
                    Panel transcriptLinkContainer = (Panel)this.FindControl("TranscriptLinkContainer");

                    if (transcriptLinkContainer != null)
                    { transcriptLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.ImportActivityData:
                    // select the import activity data link
                    Panel importActivityDataLinkContainer = (Panel)this.FindControl("ImportActivityDataLinkContainer");

                    if (importActivityDataLinkContainer != null)
                    { importActivityDataLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.ImportCertificateData:
                    // select the import certificate data link
                    Panel importCertificateDataLinkContainer = (Panel)this.FindControl("ImportCertificateDataLinkContainer");

                    if (importCertificateDataLinkContainer != null)
                    { importCertificateDataLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Roles:
                    // select the roles link
                    Panel rolesLinkContainer = (Panel)this.FindControl("RolesLinkContainer");

                    if (rolesLinkContainer != null)
                    { rolesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
            }
        }
        #endregion
        #endregion
    }
}
