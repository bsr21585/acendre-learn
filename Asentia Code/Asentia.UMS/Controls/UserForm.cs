﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
/* TODO: Agree portion */
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;

namespace Asentia.UMS.Controls
{
    #region UserFormViewType ENUM
    /// <summary>
    /// Represents view type of the user form.
    /// </summary>
    public enum UserFormViewType
    {
        AdminView = 1,
        UserView = 2,
        RegistrationPage = 3
    }
    #endregion

    public class UserForm : WebControl, INamingContainer
    {
        #region Constructors
        /// <summary>
        /// Constructor for building a blank user form.
        /// </summary>
        /// <param name="id">the id of the form</param>
        /// <param name="viewType">the view type of the form</param>
        /// <param name="fileType">the user account data file type</param>
        /// <param name="languageString">the language of the form</param>
        public UserForm(string id, UserFormViewType viewType, UserAccountDataFileType fileType, string languageString)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.UserAccountData = new UserAccountData(fileType, true, false);
            this.ViewType = viewType;
            this.LanguageString = languageString;

            // build the form
            this._Initialize();
        }

        /// <summary>
        /// Constructor for building a user form pre-populated with user data.
        /// </summary>
        /// <param name="id">the id of the form</param>
        /// <param name="viewType">the view type of the form</param>
        /// <param name="fileType">the user account data file type</param>
        /// <param name="languageString">the language of the form</param>
        /// <param name="userObject">user object</param>
        public UserForm(string id, UserFormViewType viewType, UserAccountDataFileType fileType, string languageString, User userObject)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.UserAccountData = new UserAccountData(fileType, true, false);
            this.ViewType = viewType;
            this.LanguageString = languageString;
            this.UserObject = userObject;

            // build the form
            this._Initialize();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Container for the user form tab panels.
        /// </summary>
        public Panel TabPanelsContainer;

        /// <summary>
        /// User Account Data object.
        /// </summary>
        public UserAccountData UserAccountData;

        /// <summary>
        /// View Type object.
        /// </summary>
        public UserFormViewType ViewType;

        /// <summary>
        /// The language of this form.
        /// </summary>
        public string LanguageString;

        /// <summary>
        /// User object.
        /// </summary>
        public User UserObject;

        /// <summary>
        /// Is the User Agreement diplayed on the registration form
        /// </summary>
        public bool DisplayUserAgreement = false;

        /// <summary>
        /// Is the User Agreement required to be accepted?
        /// </summary>
        public bool IsUserAgreementRequired = false;

        /// <summary>
        /// Clear the user's avatar?
        /// </summary>
        public HiddenField ClearAvatar;

        // VARIABLES FOR USER FORM FIELDS

        public string FirstName
        { get { return this._GetInputValue("firstName"); } }

        public string MiddleName
        { get { return this._GetInputValue("middleName"); } }

        public string LastName
        { get { return this._GetInputValue("lastName"); } }

        public string Avatar
        { get { return this._GetInputValue("avatar"); } }

        public string Email
        { get { return this._GetInputValue("email"); } }

        public string Username
        { get { return this._GetInputValue("username"); } }

        public string Password
        { get { return this._GetInputValue("password"); } }

        public string PasswordConfirm
        { get { return this._GetInputValue("passwordConfirm"); } }

        public string MustChangePassword
        { get { return this._GetInputValue("mustChangePassword"); } }

        public string DisableLoginFromLoginForm
        { get { return this._GetInputValue("disableLoginFromLoginForm"); } }

        public string OptOutOfEmailNotifications
        { get { return this._GetInputValue("optOutOfEmailNotifications"); } }

        public string ExcludeFromLeaderboards
        { get { return this._GetInputValue("excludeFromLeaderboards"); } }

        public string Timezone
        { get { return this._GetInputValue("idTimezone"); } }

        public string Language
        { get { return this._GetInputValue("idLanguage"); } }

        public string DtCreated
        { get { return this._GetInputValue("dtCreated"); } }

        public string DtExpires
        { get { return this._GetInputValue("dtExpires"); } }

        public string DtLastLogin
        { get { return this._GetInputValue("dtLastLogin"); } }

        public string IsActive
        { get { return this._GetInputValue("isActive"); } }

        public string Company
        { get { return this._GetInputValue("company"); } }

        public string Address
        { get { return this._GetInputValue("address"); } }

        public string City
        { get { return this._GetInputValue("city"); } }

        public string Province
        { get { return this._GetInputValue("province"); } }

        public string PostalCode
        { get { return this._GetInputValue("postalcode"); } }

        public string Country
        { get { return this._GetInputValue("country"); } }

        public string PhonePrimary
        { get { return this._GetInputValue("phonePrimary"); } }

        public string PhoneHome
        { get { return this._GetInputValue("phoneHome"); } }

        public string PhoneWork
        { get { return this._GetInputValue("phoneWork"); } }

        public string PhoneFax
        { get { return this._GetInputValue("phoneFax"); } }

        public string PhoneMobile
        { get { return this._GetInputValue("phoneMobile"); } }

        public string PhonePager
        { get { return this._GetInputValue("phonePager"); } }

        public string PhoneOther
        { get { return this._GetInputValue("phoneOther"); } }

        public string EmployeeID
        { get { return this._GetInputValue("employeeID"); } }

        public string JobTitle
        { get { return this._GetInputValue("jobTitle"); } }

        public string JobClass
        { get { return this._GetInputValue("jobClass"); } }

        public string Division
        { get { return this._GetInputValue("division"); } }

        public string Region
        { get { return this._GetInputValue("region"); } }

        public string Department
        { get { return this._GetInputValue("department"); } }

        public string DtHire
        { get { return this._GetInputValue("dtHire"); } }

        public string DtTerm
        { get { return this._GetInputValue("dtTerm"); } }

        public string Gender
        { get { return this._GetInputValue("gender"); } }

        public string Race
        { get { return this._GetInputValue("race"); } }

        public string DtDOB
        { get { return this._GetInputValue("dtDOB"); } }

        public string Field00
        { get { return this._GetInputValue("field00"); } }

        public string Field01
        { get { return this._GetInputValue("field01"); } }

        public string Field02
        { get { return this._GetInputValue("field02"); } }

        public string Field03
        { get { return this._GetInputValue("field03"); } }

        public string Field04
        { get { return this._GetInputValue("field04"); } }

        public string Field05
        { get { return this._GetInputValue("field05"); } }

        public string Field06
        { get { return this._GetInputValue("field06"); } }

        public string Field07
        { get { return this._GetInputValue("field07"); } }

        public string Field08
        { get { return this._GetInputValue("field08"); } }

        public string Field09
        { get { return this._GetInputValue("field09"); } }

        public string Field10
        { get { return this._GetInputValue("field10"); } }

        public string Field11
        { get { return this._GetInputValue("field11"); } }

        public string Field12
        { get { return this._GetInputValue("field12"); } }

        public string Field13
        { get { return this._GetInputValue("field13"); } }

        public string Field14
        { get { return this._GetInputValue("field14"); } }

        public string Field15
        { get { return this._GetInputValue("field15"); } }

        public string Field16
        { get { return this._GetInputValue("field16"); } }

        public string Field17
        { get { return this._GetInputValue("field17"); } }

        public string Field18
        { get { return this._GetInputValue("field18"); } }

        public string Field19
        { get { return this._GetInputValue("field19"); } }

        public bool UserAgreesToUserAgreement
        { get { return Convert.ToBoolean(this._GetInputValue("UserAgreementAgree")); } }
        #endregion

        #region Private Properties
        private DataTable _EligibleSupervisorsForSelectList;
        private ModalPopup _SelectSupervisorsForUser;
        private DynamicListBox _SelectEligibleSupervisors;
        private DataTable _UserSupervisors;
        public HiddenField _SelectedUserSupervisors;
        private Panel _UserSupervisorsContainer;
        private ModalPopup _UploadFilesModal;
        private ModalPopup _ModifyFilesModal;
        private Panel _ModifyFilesModalBodyPanel;
        private DocumentRepositoryItem _FilesObject;
        private HiddenField _IdDocumentRepositoryItemForModify;
        private TextBox _ModifyFileTitleTextBox;
        private CheckBox _IsVisibleToUserCheckBox;
        #endregion

        #region Methods
        #region ProcessFormErrors
        /// <summary>
        /// Proccess form validation errors and puts the messages into the appropriate container.
        /// </summary>
        /// <param name="fieldErrors">list of field errors</param>
        public void ProcessFormErrors(List<User.FieldError> fieldErrors)
        {
            foreach (User.FieldError fieldError in fieldErrors)
            {
                if (fieldError.FieldIdentifier == "UserAgreement") // validation for user agreeing to user agreement
                {
                    Panel errorPanel = (Panel)this.FindControl(this.ID + "_UserAgreementAgreeCheckBoxErrorContainer");

                    if (errorPanel != null)
                    {
                        HtmlGenericControl errorP = new HtmlGenericControl("p");
                        errorP.InnerText = fieldError.ErrorMessage;
                        errorPanel.Controls.Add(errorP);
                    }
                }
                else // validation for all form fields
                {
                    Panel containerPanel = (Panel)this.FindControl(this.ID + "_" + fieldError.FieldIdentifier + "_Container");
                    Panel errorPanel = (Panel)this.FindControl(this.ID + "_" + fieldError.FieldIdentifier + "_ErrorContainer");

                    if (containerPanel != null && errorPanel != null)
                    {
                        // add class to container
                        if (!containerPanel.CssClass.Contains("ErrorOn"))
                        { containerPanel.CssClass += " ErrorOn"; }

                        // attach error message
                        HtmlGenericControl errorP = new HtmlGenericControl("p");
                        errorP.InnerText = fieldError.ErrorMessage;
                        errorPanel.Controls.Add(errorP);

                        // find the tab li element and set the error css
                        HtmlGenericControl tabLI = (HtmlGenericControl)this.FindControl(this.ID + "_" + fieldError.TabIdentifier + "_TabLI");
                        Image tabLIErrorImage = (Image)this.FindControl(this.ID + "_" + fieldError.TabIdentifier + "_TabLIErrorImage");

                        if (tabLI != null && tabLIErrorImage == null)
                        {
                            // instansiate a string builder
                            Image errorImage = new Image();
                            errorImage.ID = this.ID + "_" + fieldError.TabIdentifier + "_TabLIErrorImage";
                            errorImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW,
                                                                         ImageFiles.EXT_PNG);
                            errorImage.CssClass = "SmallIcon";
                            tabLI.Controls.Add(errorImage);

                            // instansiate a string builder
                            StringBuilder sb = new StringBuilder();

                            // beginning of function
                            sb.AppendLine("$(\"#" + tabLI.ID + "\").addClass(\"TabbedListLIError\");");

                            // register a script to add the error class to the tab
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Toggle" + tabLI.ID, sb.ToString(), true);

                            if (this.ViewType == UserFormViewType.AdminView)
                            {
                                /* SUPERVISORS */
                                if (!String.IsNullOrWhiteSpace(this._SelectedUserSupervisors.Value))
                                {
                                    // split the "value" of the hidden field to get an array of supervisors ids
                                    string[] selectedSupervisorsIds = this._SelectedUserSupervisors.Value.Split(',');

                                    // loop through the array and add each supervisor to the listing container
                                    foreach (string supervisorId in selectedSupervisorsIds)
                                    {
                                        if (this._UserSupervisorsContainer.FindControl("Supervisor_" + supervisorId) == null)
                                        {
                                            // user object
                                            Library.User userObject = new Library.User(Convert.ToInt32(supervisorId));

                                            // container
                                            Panel userNameContainer = new Panel();
                                            userNameContainer.ID = "Supervisor_" + userObject.Id.ToString();

                                            // remove user button
                                            Image removeUserImage = new Image();
                                            removeUserImage.ID = "Supervisor_" + userObject.Id.ToString() + "_RemoveImage";
                                            removeUserImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                                ImageFiles.EXT_PNG);
                                            removeUserImage.CssClass = "SmallIcon";
                                            removeUserImage.Attributes.Add("onClick", "javascript:RemoveSupervisorFromUser('" + userObject.Id.ToString() + "');");
                                            removeUserImage.Style.Add("cursor", "pointer");

                                            // user name
                                            Literal userName = new Literal();
                                            userName.Text = userObject.DisplayName + "(" + userObject.Username + ")";

                                            // add controls to container
                                            userNameContainer.Controls.Add(removeUserImage);
                                            userNameContainer.Controls.Add(userName);
                                            this._UserSupervisorsContainer.Controls.Add(userNameContainer);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region SaveSupervisors

        /// <summary>
        /// Saves the supervisors.
        ///this method will do the saving of supervisors for a User(only for admin view)
        /// </summary>
        /// <param name="idUser">The identifier user.</param>
        public void SaveSupervisors(int idUser)
        {
            // delcare data table
            DataTable supervisorsToSave = new DataTable(); ;
            supervisorsToSave.Columns.Add("id", typeof(int));
            if (!String.IsNullOrWhiteSpace(this._SelectedUserSupervisors.Value))
            {
                // split the "value" of the hidden field to get an array of course expert ids
                string[] selectedSupervisorsIds = this._SelectedUserSupervisors.Value.Split(',');

                // put ids into datatable 
                foreach (string supervisorId in selectedSupervisorsIds)
                { supervisorsToSave.Rows.Add(Convert.ToInt32(supervisorId)); }
            }
            // save the course experts
            if (this.UserObject == null)
            {
                this.UserObject = new User();
            }
            this.UserObject.SaveSupervisors(supervisorsToSave, idUser);
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Initialize
        /// <summary>
        /// Builds the user form containers and fields.
        /// </summary>
        private void _Initialize()
        {
            // if this is a registration page, build the registration instructions
            if (this.ViewType == UserFormViewType.RegistrationPage)
            { this._BuildInstructionsPanel(); }

            // build tabs and user fields
            this._BuildTabsList();

            this.TabPanelsContainer = new Panel();
            this.TabPanelsContainer.ID = this.ID + "_TabPanelsContainer";
            this.TabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.Controls.Add(this.TabPanelsContainer);

            this._BuildUserForm();

            if (this.ViewType == UserFormViewType.AdminView) //builds the supervisor and files form & Tab on admin view
            {
                this._BuildSupervisorForm();

                if (this.UserObject != null && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERPROFILEFILEATTACHMENTS_ENABLE))
                { this._BuildFilesForm(); }
            }
            else if (this.ViewType == UserFormViewType.UserView) //builds the files form & tab on the user view
            {
                if (this.UserObject != null && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERPROFILEFILEATTACHMENTS_ENABLE))
                { this._BuildFilesForm(); }
            }

            // if this is a registration page, build the user agreement
            if (this.ViewType == UserFormViewType.RegistrationPage)
            { this._BuildUserAgreementPanel(); }
        }
        #endregion

        #region _BuildInstructionsPanel
        /// <summary>
        /// Builds the registration instructions panel.
        /// </summary>
        private void _BuildInstructionsPanel()
        {
            Panel registrationInstructionsPanel = new Panel();
            registrationInstructionsPanel.ID = this.ID + "_" + "RegistrationInstructionsPanel";
            registrationInstructionsPanel.CssClass = "RegistrationInstructionsPanel";

            Literal registrationInstructionsPanelHTML = new Literal();
            registrationInstructionsPanelHTML.Text = this.UserAccountData.GetRegistrationInstructionsHTMLInLanguage(this.LanguageString);

            registrationInstructionsPanel.Controls.Add(registrationInstructionsPanelHTML);
            this.Controls.Add(registrationInstructionsPanel);
        }
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for the form.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            // loop through each tab and add it to the tabs queue if there are user fields visible under that tab
            /*foreach (UserAccountData.Tab tab in this.UserAccountData.Tabs)
            {                
                // attach controls if there are user fields visible in the tab
                var j = 0;
                foreach (UserAccountData.UserField userField in tab.UserFields)
                {
                    switch (this.ViewType)
                    {
                        case UserFormViewType.AdminView:
                            if (userField.AdministratorEditPageView)
                            { j++; }
                            break;
                        case UserFormViewType.UserView:
                            if (userField.UserEditPageView)
                            { j++; }
                            break;
                        case UserFormViewType.RegistrationPage:
                            if (userField.RegistrationPageView)
                            { j++; }
                            break;
                    }
                }

                if (j > 0)
                { tabs.Enqueue(new KeyValuePair<string, string>(tab.Identifier, this.UserAccountData.GetTabLabelInLanguage(tab, this.LanguageString))); }

            }*/

            // Builds the properties tab to show account, contact, employee, and other sections
            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));

            if (this.ViewType == UserFormViewType.AdminView) // builds the supervisor and files tabs on admin view
            {
                tabs.Enqueue(new KeyValuePair<string, string>("Supervisors", _GlobalResources.Supervisors));

                if (this.UserObject != null && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERPROFILEFILEATTACHMENTS_ENABLE))
                { tabs.Enqueue(new KeyValuePair<string, string>("Files", _GlobalResources.Files)); }
            }
            else if (this.ViewType == UserFormViewType.UserView) // build the files tab on the user view
            {
                if (this.UserObject != null && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERPROFILEFILEATTACHMENTS_ENABLE))
                { tabs.Enqueue(new KeyValuePair<string, string>("Files", _GlobalResources.Files)); }
            }

            // build and attach the tabs
            this.Controls.Add(AsentiaPage.BuildTabListPanel(this.ID, tabs));

        }
        #endregion

        #region _BuildUserForm
        /// <summary>
        /// Builds the tab content container and userfields for each tab.
        /// </summary>
        private void _BuildUserForm()
        {

            Panel tabContentPanel = new Panel();
            tabContentPanel.ID = this.ID + "_Properties_TabPanel";

            // loop through each tab and build the content panel for it
            foreach (UserAccountData.Tab tab in this.UserAccountData.Tabs)
            {

                // if there are fields under this section, build the secion header
                var j = 0;
                foreach (UserAccountData.UserField userField in tab.UserFields)
                {
                    switch (this.ViewType)
                    {
                        case UserFormViewType.AdminView:
                            if (userField.AdministratorEditPageView)
                            { j++; }
                            break;
                        case UserFormViewType.UserView:
                            if (userField.UserEditPageView)
                            { j++; }
                            break;
                        case UserFormViewType.RegistrationPage:
                            if (userField.RegistrationPageView)
                            { j++; }
                            break;
                    }
                }

                if (j > 0)
                { tabContentPanel.Controls.Add(_BuildSectionHeader(this.UserAccountData.GetTabLabelInLanguage(tab, this.LanguageString), tab.Identifier)); }

                // build the user fields
                foreach (UserAccountData.UserField userField in tab.UserFields)
                {
                    Panel userFieldContainer = null;

                    switch (this.ViewType)
                    {
                        case UserFormViewType.AdminView:
                            userFieldContainer = this._BuildUserFieldContainer(userField, userField.AdministratorEditPageView, userField.AdministratorEditPageModify);
                            break;
                        case UserFormViewType.UserView:
                            userFieldContainer = this._BuildUserFieldContainer(userField, userField.UserEditPageView, userField.UserEditPageModify);
                            break;
                        case UserFormViewType.RegistrationPage:
                            userFieldContainer = this._BuildUserFieldContainer(userField, userField.RegistrationPageView, userField.RegistrationPageModify);
                            break;
                    }

                    // add user field container to tab panel
                    if (userFieldContainer != null && !(userField.Identifier == "optOutOfEmailNotifications" && !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.PRIVACY_ALLOWUSEREMAILOPTOUT)))
                    { tabContentPanel.Controls.Add(userFieldContainer); }

                }

            }
            this.TabPanelsContainer.Controls.Add(tabContentPanel);
        }
        #endregion

        #region _BuildSectionHeader
        /// <summary>
        /// Builds the header of each section of the page
        /// </summary>
        /// <param name="sectionTitle">The section title</param>
        /// <returns></returns>
        private Panel _BuildSectionHeader(string sectionTitle, string tabIdentifier)
        {

            // container
            Panel sectionHeaderContainer = new Panel();
            sectionHeaderContainer.ID = this.ID + "_" + tabIdentifier + "_SectionHeaderContainer";
            sectionHeaderContainer.CssClass = "UserFormSectionHeaderContainer";

            Label sectionLabel = new Label();
            sectionLabel.Text = sectionTitle;
            sectionLabel.ID = this.ID + "_" + tabIdentifier + "_SectionHeader";

            sectionHeaderContainer.Controls.Add(sectionLabel);

            return sectionHeaderContainer;
        }
        #endregion

        #region _BuildSupervisorForm
        /// <summary>
        /// Builds supervisor form.
        /// </summary>
        private void _BuildSupervisorForm()
        {
            // populate datatables with lists of users who are course experts and who are not
            if (this.UserObject != null)
            {
                this._UserSupervisors = this.UserObject.GetSupervisors(null);
                this._EligibleSupervisorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForUserSupervisorSelectList(this.UserObject.Id, null);
            }
            else
            {
                this._EligibleSupervisorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForUserSupervisorSelectList(0, null);
            }

            Panel supervisorsPanel = new Panel();
            supervisorsPanel.ID = this.ID + "_Supervisors" + "_TabPanel";
            supervisorsPanel.CssClass = "TabPanelContainer";
            supervisorsPanel.Attributes.Add("style", "display: none;");

            // course experts field
            List<Control> courseSupervisorsInputControls = new List<Control>();

            // course selected experts hidden field
            this._SelectedUserSupervisors = new HiddenField();
            this._SelectedUserSupervisors.ID = "SelectedUserSupervisorsHidden";
            courseSupervisorsInputControls.Add(this._SelectedUserSupervisors);

            // build a container for the user membership listing
            this._UserSupervisorsContainer = new Panel();
            this._UserSupervisorsContainer.ID = "UserSupervisorsContainer";
            this._UserSupervisorsContainer.CssClass = "ItemListingContainer";

            courseSupervisorsInputControls.Add(this._UserSupervisorsContainer);

            Panel supervisorsButtonsPanel = new Panel();
            supervisorsButtonsPanel.ID = "SupervisorsButtonsPanel";

            // select supervisor(s) button

            // link
            Image selectSupervisorsImageForLink = new Image();
            selectSupervisorsImageForLink.ID = "LaunchSelectSupervisorsModalImage";
            selectSupervisorsImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR,
                                                                        ImageFiles.EXT_PNG);
            selectSupervisorsImageForLink.CssClass = "MediumIcon";

            Localize selectSupervisorsTextForLink = new Localize();
            selectSupervisorsTextForLink.Text = _GlobalResources.SelectSupervisor_s;

            LinkButton selectSupervisorsLink = new LinkButton();
            selectSupervisorsLink.ID = "LaunchSelectSupervisorsModal";
            selectSupervisorsLink.CssClass = "ImageLink";
            selectSupervisorsLink.Controls.Add(selectSupervisorsImageForLink);
            selectSupervisorsLink.Controls.Add(selectSupervisorsTextForLink);
            supervisorsButtonsPanel.Controls.Add(selectSupervisorsLink);

            // attach the buttons panel to the container
            courseSupervisorsInputControls.Add(supervisorsButtonsPanel);

            // build modals for adding and removing supervisor to/from the user
            this._BuildSelectSupervisorsModal(selectSupervisorsLink.ID);

            supervisorsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Supervisors",
                                                                                     String.Empty,
                                                                                     courseSupervisorsInputControls,
                                                                                     false,
                                                                                     true));

            // attach panel to container
            this.TabPanelsContainer.Controls.Add(supervisorsPanel);

            if (this.UserObject != null && this.UserObject.Id > 0)
            {
                this._PopulateSupervisors();
            }
        }
        #endregion

        #region _BuildFilesForm
        /// <summary>
        /// Builds form for uploading profile files.
        /// </summary>
        private void _BuildFilesForm()
        {
            // files panel
            Panel filesPanel = new Panel();
            filesPanel.ID = this.ID + "_Files_TabPanel";
            filesPanel.CssClass = "TabPanelContainer";
            filesPanel.Attributes.Add("style", "display: none;");

            List<Control> filesInputControls = new List<Control>();

            // in the user view, only show an upload button and build upload modal if users are allowed to upload files
            if ((this.UserAccountData.AllowUserToUploadFiles && this.ViewType == UserFormViewType.UserView) || (this.ViewType == UserFormViewType.AdminView))
            {
                // upload file actions panel
                Panel uploadFileActionsPanel = new Panel();
                uploadFileActionsPanel.ID = "UploadFileActionsPanel";

                // upload file link button
                LinkButton uploadFileLinkButton = new LinkButton();
                uploadFileLinkButton.ID = "UploadFileLinkButton";
                uploadFileLinkButton.CssClass = "ImageLink";
                uploadFileLinkButton.OnClientClick = "ClearUploadModalFeedback(); return false;";

                // upload file button image
                Image uploadFileImage = new Image();
                uploadFileImage.ID = "UploadFilesLinkButtonImage";
                uploadFileImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG);
                uploadFileImage.CssClass = "SmallIcon";
                uploadFileImage.AlternateText = _GlobalResources.Upload;
                uploadFileLinkButton.Controls.Add(uploadFileImage);

                // upload file button text
                Literal uploadFileText = new Literal();
                uploadFileText.Text = _GlobalResources.UploadFile;
                uploadFileLinkButton.Controls.Add(uploadFileText);

                uploadFileActionsPanel.Controls.Add(uploadFileLinkButton);

                filesInputControls.Add(uploadFileActionsPanel);

                this._BuildUploadFilesModal(uploadFileLinkButton.ID, this.TabPanelsContainer);
            }

            UpdatePanel filesGridUpdatePanel = new UpdatePanel();
            filesGridUpdatePanel.ID = "FilesGridUpdatePanel";

            // Grid to show all files uploaded for this user
            Grid filesGrid = new Grid();

            filesGrid.ID = "FilesGrid";
            filesGrid.StoredProcedure = DocumentRepositoryItem.GridProcedure;
            filesGrid.DefaultRecordsPerPage = 10;
            filesGrid.EmptyDataText = _GlobalResources.NoDocumentsFound;
            filesGrid.ShowSearchBox = false;
            filesGrid.AddCheckboxColumn = true;
            filesGrid.ShowRecordsPerPageSelectbox = false;
            filesGrid.PagerSettings.Position = PagerPosition.Top;

            filesGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            filesGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            filesGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);

            filesGrid.AddFilter("@idDocumentRepositoryObjectType", SqlDbType.Int, 4, 4);
            filesGrid.AddFilter("@idObject", SqlDbType.Int, 4, this.UserObject.Id);

            // Set the isUserView parameter to 1 if this is the user view
            if (this.ViewType == UserFormViewType.UserView)
            { filesGrid.AddFilter("@isUserView", SqlDbType.Bit, 1, 1); }
            else
            { filesGrid.AddFilter("@isUserView", SqlDbType.Bit, 1, 0); }

            filesGrid.IdentifierField = "idDocumentRepositoryItem";
            filesGrid.DefaultSortColumn = "fileName";

            filesGrid.DataKeyNames = new string[] { "idDocumentRepositoryItem", "fileName", "label", "isVisibleToUser", "isUploadedByUser" };

            // columns
            GridColumn filename = new GridColumn(_GlobalResources.File, "fileName", "fileName");
            filename.AddProperty(new GridColumnProperty("True", "<a href=\"/warehouse/" + AsentiaSessionState.SiteHostname + "/documents/user/" + this.UserObject.Id.ToString() + "/##fileName##\" target=\"_blank\">"
                                                            + "##label##"
                                                            + "</a>"));
            GridColumn modify = new GridColumn(_GlobalResources.Modify, null, true);

            // add columns to data grid
            filesGrid.AddColumn(filename);
            filesGrid.AddColumn(modify);

            filesGrid.RowDataBound += new GridViewRowEventHandler(this._FilesGrid_RowDataBound);

            filesGridUpdatePanel.ContentTemplateContainer.Controls.Add(filesGrid);

            filesInputControls.Add(filesGridUpdatePanel);

            // delete button actions panel
            Panel deleteButtonActionsPanel = new Panel();
            deleteButtonActionsPanel.ID = "DeleteButtonActionPanel";
            deleteButtonActionsPanel.CssClass = "ActionsPanel";

            // delete button
            LinkButton deleteLinkButton = new LinkButton();
            deleteLinkButton.ID = "FilesGridDeleteLinkButton";
            deleteLinkButton.CssClass = "GridDeleteButton";
            deleteLinkButton.Command += this._FilesGridDeleteButton_Command;

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "FilesGridDeleteLinkButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "SmallIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            deleteLinkButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedFile_s;
            deleteLinkButton.Controls.Add(deleteText);

            deleteButtonActionsPanel.Controls.Add(deleteLinkButton);

            filesInputControls.Add(deleteLinkButton);

            // Hidden button used to launch the modal on modify
            Button modifyFilesModalHiddenLaunchButton = new Button();
            modifyFilesModalHiddenLaunchButton.ID = "ModifyFilesModalHiddenLaunchButton";
            modifyFilesModalHiddenLaunchButton.Style.Add("display", "none");
            filesInputControls.Add(modifyFilesModalHiddenLaunchButton);

            this._IdDocumentRepositoryItemForModify = new HiddenField();
            this._IdDocumentRepositoryItemForModify.ID = "IdDocumentRepositoryItemHiddenField";
            filesInputControls.Add(this._IdDocumentRepositoryItemForModify);

            filesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Files",
                                                                                     String.Empty,
                                                                                     filesInputControls,
                                                                                     false,
                                                                                     true));


            // attach panel to container
            this.TabPanelsContainer.Controls.Add(filesPanel);

            this._BuildModifyFilesModal(modifyFilesModalHiddenLaunchButton.ID, this.TabPanelsContainer);

            filesGrid.BindData();

        }
        #endregion

        #region ModifyFilesModalHiddenLoadButton_Click

        /// <summary>
        /// Used to load the modify files modal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ModifyFilesModalHiddenLoadButton_Click(object sender, EventArgs e)
        {
            DocumentRepositoryItem profileFile = new DocumentRepositoryItem(Convert.ToInt32(this._IdDocumentRepositoryItemForModify.Value));

            this._ModifyFileTitleTextBox.Text = profileFile.Label;

            // isVisibleToUser checkbox determines if the user can see this file (this option only available on admin view)
            if (this.ViewType == UserFormViewType.AdminView && profileFile.IsVisibleToUser != null)
            {
                    this._IsVisibleToUserCheckBox.Checked = (bool)profileFile.IsVisibleToUser;                
            }
        }
        #endregion

        #region _FilesGrid_RowDataBound
        /// <summary>
        /// Row data bound event handler for the files grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _FilesGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idDocumentRepositoryItem = Convert.ToInt32(rowView["idDocumentRepositoryItem"]);
                string documentRepositoryItemLabel = (string)rowView["label"];

                // is the file visible to the user?
                bool isVisibleToUser = true;
                if (!rowView["isVisibleToUser"].GetType().Equals(typeof(DBNull)))
                { isVisibleToUser = (bool)rowView["isVisibleToUser"]; }

                // is the file uploaded by the user?
                bool isUploadedByUser = false;
                if (!rowView["isUploadedByUser"].GetType().Equals(typeof(DBNull)))
                { isUploadedByUser = (bool)rowView["isUploadedByUser"]; }

                // create modify link button
                LinkButton modifyLinkButton = new LinkButton();
                modifyLinkButton.ID = "ModifyFile_" + idDocumentRepositoryItem.ToString();
                modifyLinkButton.ToolTip = _GlobalResources.Modify;

                Image modifyLinkImage = new Image();
                modifyLinkImage.ID = "ModifyFileImage_" + idDocumentRepositoryItem.ToString();
                modifyLinkImage.AlternateText = _GlobalResources.Modify;
                modifyLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                               ImageFiles.EXT_PNG);

                // Dim the icon and disable the button in the user view if the file was not uploaded by the user
                if ((this.ViewType == UserFormViewType.UserView && isUploadedByUser) || (this.ViewType == UserFormViewType.AdminView))
                {
                    modifyLinkImage.CssClass = "SmallIcon";
                    modifyLinkButton.OnClientClick = "LoadFilesUploadModalContent(" + idDocumentRepositoryItem + "); return false;";
                }
                else
                {
                    // don't allow user to modify this file, since it was uploaded by the admin
                    modifyLinkImage.CssClass = "SmallIcon DimIcon";
                    modifyLinkButton.Enabled = false;

                    // don't allow user to delete this file, since it was uploaded by the admin
                    CheckBox deleteCheckBox = (CheckBox)e.Row.Cells[0].FindControl("FilesGrid_GridSelectRecord_" + e.Row.RowIndex.ToString());
                    deleteCheckBox.Enabled = false;
                }

                modifyLinkButton.Controls.Add(modifyLinkImage);

                e.Row.Cells[2].Controls.Add(modifyLinkButton);

            }

        }
        #endregion

        #region _BuildUploadFilesModal
        /// <summary>
        /// Builds the modals for uploading files
        /// </summary>
        /// <param name="targetControlId">ID of control that launches modal</param>
        /// <param name="modalContainer">container that holds the modal</param>
        private void _BuildUploadFilesModal(string targetControlId, Control modalContainer)
        {

            // Upload New File Modal
            this._UploadFilesModal = new ModalPopup("UploadFilesModal");
            this._UploadFilesModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG);
            this._UploadFilesModal.HeaderIconAlt = _GlobalResources.UploadFile;
            this._UploadFilesModal.HeaderText = _GlobalResources.UploadFile;
            this._UploadFilesModal.Type = ModalPopupType.Form;
            this._UploadFilesModal.TargetControlID = targetControlId;
            this._UploadFilesModal.ReloadPageOnClose = false;
            this._UploadFilesModal.SubmitButtonCustomText = _GlobalResources.SaveChanges;
            this._UploadFilesModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._UploadFilesModal.SubmitButton.Command += this._UploadFileSubmit_Command;

            // file title text box
            TextBox uploadFileTitleTextBox = new TextBox();
            uploadFileTitleTextBox.ID = "Upload_FileTitle_Field";
            uploadFileTitleTextBox.CssClass = "InputLong";

            this._UploadFilesModal.AddControlToBody(AsentiaPage.BuildFormField("UploadFileModal_TitleTextbox",
                                                                        _GlobalResources.FileTitle,
                                                                        uploadFileTitleTextBox.ID,
                                                                        uploadFileTitleTextBox,
                                                                        false,
                                                                        false,
                                                                        false));


            // file uploader

            UploaderAsync uploadFileUploader = new UploaderAsync("Upload_FileUpload_Field", UploadType.ProfileFile, this.ID + "_Upload_FileUpload_ErrorContainer");
            uploadFileUploader.CssClass = "FileUpload_Field";

            this._UploadFilesModal.AddControlToBody(AsentiaPage.BuildFormField("UploadFileModal_FileUploader",
                                                                        _GlobalResources.UploadFile,
                                                                        uploadFileUploader.ID,
                                                                        uploadFileUploader,
                                                                        false,
                                                                        false,
                                                                        false));

            // isVisibleToUser checkbox determines if the user can see this file (this option only available on admin view)
            if (this.ViewType == UserFormViewType.AdminView)
            {
                CheckBox isVisibleToUserCheckBox = new CheckBox();
                isVisibleToUserCheckBox.ID = "Upload_IsVisibleToUser_Field";
                isVisibleToUserCheckBox.Text = _GlobalResources.MakeThisFileVisibleToTheUser;

                this._UploadFilesModal.AddControlToBody(AsentiaPage.BuildFormField("UploadFileModal_IsVisibleToUserCheckBox",
                                                                            _GlobalResources.Options,
                                                                            isVisibleToUserCheckBox.ID,
                                                                            isVisibleToUserCheckBox,
                                                                            false,
                                                                            false,
                                                                            false));
            }

            modalContainer.Controls.Add(this._UploadFilesModal);

        }
        #endregion

        #region _BuildModifyFilesModal
        /// <summary>
        /// Builds the modals for modifying files
        /// </summary>
        /// <param name="targetControlId">ID of control that launches modal</param>
        /// <param name="modalContainer">container that holds the modal</param>
        private void _BuildModifyFilesModal(string targetControlId, Control modalContainer)
        {
            // Modify File Modal
            this._ModifyFilesModal = new ModalPopup("ModifyFilesModal");
            this._ModifyFilesModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
            this._ModifyFilesModal.HeaderIconAlt = _GlobalResources.ModifyFile;
            this._ModifyFilesModal.HeaderText = _GlobalResources.ModifyFile;
            this._ModifyFilesModal.Type = ModalPopupType.Form;
            this._ModifyFilesModal.TargetControlID = targetControlId;
            this._ModifyFilesModal.ReloadPageOnClose = false;
            this._ModifyFilesModal.SubmitButtonCustomText = _GlobalResources.SaveChanges;
            this._ModifyFilesModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._ModifyFilesModal.SubmitButton.Command += this._ModifyFileSubmit_Command;
            this._ModifyFilesModal.ShowLoadingPlaceholder = true;

            this._ModifyFilesModalBodyPanel = new Panel();

            // file title text box
            this._ModifyFileTitleTextBox = new TextBox();
            this._ModifyFileTitleTextBox.ID = "Modify_FileTitle_Field";
            this._ModifyFileTitleTextBox.CssClass = "InputLong";

            this._ModifyFilesModalBodyPanel.Controls.Add(AsentiaPage.BuildFormField("ModifyFileModal_TitleTextbox",
                                                                        _GlobalResources.FileTitle,
                                                                        this._ModifyFileTitleTextBox.ID,
                                                                        this._ModifyFileTitleTextBox,
                                                                        false,
                                                                        false,
                                                                        false));

            // isVisibleToUser checkbox determines if the user can see this file (this option only available on admin view)
            if (this.ViewType == UserFormViewType.AdminView)
            {
                this._IsVisibleToUserCheckBox = new CheckBox();
                this._IsVisibleToUserCheckBox.ID = "Modify_IsVisibleToUser_Field";
                this._IsVisibleToUserCheckBox.Text = _GlobalResources.MakeThisFileVisibleToTheUser;

                this._ModifyFilesModalBodyPanel.Controls.Add(AsentiaPage.BuildFormField("ModifyFileModal_IsVisibleToUserCheckBox",
                                                                            _GlobalResources.Options,
                                                                            this._IsVisibleToUserCheckBox.ID,
                                                                            this._IsVisibleToUserCheckBox,
                                                                            false,
                                                                            false,
                                                                            false));
            }

            // Hidden button to load the file data in the modal
            Button modifyFilesModalHiddenLoadButton = new Button();
            modifyFilesModalHiddenLoadButton.ID = "ModifyFilesModalHiddenLoadButton";
            modifyFilesModalHiddenLoadButton.Style.Add("display", "none");
            modifyFilesModalHiddenLoadButton.Click += this.ModifyFilesModalHiddenLoadButton_Click;
            this._ModifyFilesModalBodyPanel.Controls.Add(modifyFilesModalHiddenLoadButton);

            this._ModifyFilesModal.AddControlToBody(this._ModifyFilesModalBodyPanel);

            modalContainer.Controls.Add(this._ModifyFilesModal);

        }
        #endregion

        #region _UploadFileSubmit_Command
        /// <summary>
        /// Command event handler for upload file modal submit button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _UploadFileSubmit_Command(Object sender, CommandEventArgs e)
        {

            UploaderAsync fileUploader = (UploaderAsync)this._UploadFilesModal.FindControl("Upload_FileUpload_Field");

            // Upload Profile Files
            if (fileUploader.SavedFilePath != null)
            {

                this._FilesObject = new DocumentRepositoryItem();

                // Get the user ID
                int userId = this.UserObject.Id;

                // check user folder existence and create if necessary
                if (!Directory.Exists(Page.Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_USER + userId)))
                { Directory.CreateDirectory(Page.Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_USER + userId)); }

                string fileUploadFilePath = null;

                if (File.Exists(Page.Server.MapPath(fileUploader.SavedFilePath)))
                {

                    try
                    {
                        fileUploadFilePath = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_USER + userId + "/";
                        TextBox fileTitleTextBox = (TextBox)this._UploadFilesModal.FindControl("Upload_FileTitle_Field");
                        CheckBox isVisibleToUserCheckBox = (CheckBox)this._UploadFilesModal.FindControl("Upload_IsVisibleToUser_Field");

                        string fileOriginalname = string.Empty;
                        string updatedFilePath = DocumentRepositoryItem.GetCorrectFileName(fileUploadFilePath + fileUploader.FileOriginalName, out fileOriginalname);

                        // Copy file from upload location to final location
                        File.Copy(Page.Server.MapPath(fileUploader.SavedFilePath), Page.Server.MapPath(updatedFilePath), true);
                        File.Delete(Page.Server.MapPath(fileUploader.SavedFilePath));

                        // Update the Files Object with appropriate properties
                        this._FilesObject.IdSite = AsentiaSessionState.IdSite;
                        this._FilesObject.IdDocumentRepositoryObjectType = DocumentRepositoryObjectType.Profile;
                        this._FilesObject.IdObject = userId;
                        this._FilesObject.IdOwner = AsentiaSessionState.IdSiteUser;
                        this._FilesObject.FileName = fileOriginalname;
                        this._FilesObject.Kb = Convert.ToInt32(fileUploader.FileSize) / 1000;
                        // If the title text box is empty, then set the label to the filename
                        if (fileTitleTextBox.Text != null & fileTitleTextBox.Text != "")
                        { this._FilesObject.Label = fileTitleTextBox.Text; }
                        else
                        { this._FilesObject.Label = fileOriginalname; }

                        // check the view type
                        if (this.ViewType == UserFormViewType.AdminView)
                        {
                            // For admin view type, isVisibleToUser comes from checkbox and isUploadedByUser is false
                            this._FilesObject.IsVisibleToUser = isVisibleToUserCheckBox.Checked;
                            this._FilesObject.IsUploadedByUser = false;
                        }
                        else
                        {
                            // For user view, file is automatically visible to user and isUploadedByUser is true
                            this._FilesObject.IsVisibleToUser = true;
                            this._FilesObject.IsUploadedByUser = true;
                        }

                        // save the file to the database
                        this._FilesObject.Save();
                        // show the feedback
                        this._UploadFilesModal.DisplayFeedback(_GlobalResources.TheFileWasSuccessfullyUploaded, false);
                        fileTitleTextBox.Text = "";
                        if (isVisibleToUserCheckBox != null)
                        {
                            isVisibleToUserCheckBox.Checked = false;
                        }
                        // refresh the grid
                        Grid filesGrid = (Grid)this.FindControl("FilesGrid");
                        filesGrid.BindData();
                    }
                    catch (AsentiaException ex)
                    {
                        // display the failure message
                        this._UploadFilesModal.DisplayFeedback(ex.Message, true);
                    }
                }
            }
        }
        #endregion

        #region _ModifyFileSubmit_Command
        /// <summary>
        /// Command event handler for modify file modal submit button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ModifyFileSubmit_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                // clear the modal feedback
                this._ModifyFilesModal.ClearFeedback();

                // instansiate file object for new and existing file
                int idFile = Convert.ToInt32(this._IdDocumentRepositoryItemForModify.Value);

                if (idFile > 0)
                { this._FilesObject = new DocumentRepositoryItem(idFile); }
                else
                { this._FilesObject = new DocumentRepositoryItem(); }

                // populate the object
                this._FilesObject.IdDocumentRepositoryObjectType = DocumentRepositoryObjectType.Profile;


                string fileOriginalname = this._FilesObject.Label;

                // Only set the isVisibleToUser if this is the admin view
                if (this.ViewType == UserFormViewType.AdminView)
                { this._FilesObject.IsVisibleToUser = this._IsVisibleToUserCheckBox.Checked; }

                // If the title text box is empty, then set the label to the filename
                if (String.IsNullOrEmpty(this._ModifyFileTitleTextBox.Text))
                {
                    this._FilesObject.Label = fileOriginalname;
                }
                else
                {
                    this._FilesObject.Label = this._ModifyFileTitleTextBox.Text;
                }

                // save the file to the database
                this._FilesObject.Save();

                // show the feedback
                this._ModifyFilesModal.DisplayFeedback(_GlobalResources.TheFileWasSuccessfullyModified, false);

                // refresh the grid
                Grid filesGrid = (Grid)this.FindControl("FilesGrid");
                filesGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ModifyFilesModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _FilesGridDeleteButton_Command
        /// <summary>
        /// Performs the delete action on the files grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _FilesGridDeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // Find the grid control
                Grid filesGrid = (Grid)this.FindControl("FilesGrid");

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < filesGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)filesGrid.Rows[i].FindControl(filesGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                DocumentRepositoryItem.Delete(recordsToDelete);

                // display the success message
                AsentiaAuthenticatedPage page = (AsentiaAuthenticatedPage)this.Page;
                page.DisplayFeedback(_GlobalResources.TheSelectedFile_sHaveBeenDeletedSuccessfully, false);

                // rebind the grid
                filesGrid.BindData();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                AsentiaAuthenticatedPage page = (AsentiaAuthenticatedPage)this.Page;
                page.DisplayFeedback(dnfEx.Message, true);

                // rebind the grid
                Grid filesGrid = (Grid)this.FindControl("FilesGrid");
                filesGrid.BindData();
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                AsentiaAuthenticatedPage page = (AsentiaAuthenticatedPage)this.Page;
                page.DisplayFeedback(fnuEx.Message, true);

                // rebind the grid
                Grid filesGrid = (Grid)this.FindControl("FilesGrid");
                filesGrid.BindData();
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                AsentiaAuthenticatedPage page = (AsentiaAuthenticatedPage)this.Page;
                page.DisplayFeedback(cpeEx.Message, true);

                // rebind the grid
                Grid filesGrid = (Grid)this.FindControl("FilesGrid");
                filesGrid.BindData();
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                AsentiaAuthenticatedPage page = (AsentiaAuthenticatedPage)this.Page;
                page.DisplayFeedback(dEx.Message, true);

                // rebind the grid
                Grid filesGrid = (Grid)this.FindControl("FilesGrid");
                filesGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                AsentiaAuthenticatedPage page = (AsentiaAuthenticatedPage)this.Page;
                page.DisplayFeedback(ex.Message, true);

                // rebind the grid
                Grid filesGrid = (Grid)this.FindControl("FilesGrid");
                filesGrid.BindData();
            }
        }
        #endregion

        #region PopulateSupervisors
        private void _PopulateSupervisors()
        {
            // loop through the datatable and add each member to the listing container
            foreach (DataRow row in this._UserSupervisors.Rows)
            {
                // container
                Panel userNameContainer = new Panel();
                userNameContainer.ID = "Supervisor_" + row["idUser"].ToString();

                // remove expert button
                Image removeSupervisorImage = new Image();
                removeSupervisorImage.ID = "Supervisor_" + row["idUser"].ToString() + "_RemoveImage";
                removeSupervisorImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                    ImageFiles.EXT_PNG);
                removeSupervisorImage.CssClass = "SmallIcon";
                removeSupervisorImage.Attributes.Add("onClick", "javascript:RemoveSupervisorFromUser('" + row["idUser"].ToString() + "');");
                removeSupervisorImage.Style.Add("cursor", "pointer");

                // supervisor name
                Literal userName = new Literal();
                userName.Text = row["displayName"].ToString();

                // add controls to container
                userNameContainer.Controls.Add(removeSupervisorImage);
                userNameContainer.Controls.Add(userName);
                this._UserSupervisorsContainer.Controls.Add(userNameContainer);
            }
        }
        #endregion

        #region _BuildSelectSupervisorsModal
        /// <summary>
        /// Builds the modal for selecting supervisors to add to the users.
        /// </summary>
        private void _BuildSelectSupervisorsModal(string targetControlId)
        {
            // set modal properties
            this._SelectSupervisorsForUser = new ModalPopup("SelectSupervisorsForUser");
            this._SelectSupervisorsForUser.Type = ModalPopupType.Form;
            this._SelectSupervisorsForUser.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR,
                                                                                 ImageFiles.EXT_PNG);
            this._SelectSupervisorsForUser.HeaderIconAlt = _GlobalResources.SelectSupervisor_s;
            this._SelectSupervisorsForUser.HeaderText = _GlobalResources.SelectSupervisor_s;
            this._SelectSupervisorsForUser.TargetControlID = targetControlId;
            this._SelectSupervisorsForUser.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectSupervisorsForUser.SubmitButtonCustomText = _GlobalResources.AddSupervisor_s;
            this._SelectSupervisorsForUser.SubmitButton.OnClientClick = "javascript:AddSupervisorsToUser(); return false;";
            this._SelectSupervisorsForUser.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectSupervisorsForUser.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectSupervisorsForUser.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleSupervisors = new DynamicListBox("SelectEligibleSupervisors");
            this._SelectEligibleSupervisors.NoRecordsFoundMessage = _GlobalResources.NoUsersFound;
            this._SelectEligibleSupervisors.SearchButton.Command += new CommandEventHandler(this._SearchSelectSupervisorsButton_Command);
            this._SelectEligibleSupervisors.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectSupervisorsButton_Command);
            this._SelectEligibleSupervisors.ListBoxControl.DataSource = this._EligibleSupervisorsForSelectList;
            this._SelectEligibleSupervisors.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleSupervisors.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleSupervisors.ListBoxControl.DataBind();

            // add controls to body
            this._SelectSupervisorsForUser.AddControlToBody(this._SelectEligibleSupervisors);

            // add modal to container
            this.TabPanelsContainer.Controls.Add(this._SelectSupervisorsForUser);
        }
        #endregion

        #region _SearchSelectSupervisorsButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Supervisor(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectSupervisorsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectSupervisorsForUser.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleSupervisors.ListBoxControl.Items.Clear();

            // do the search
            if (this.UserObject != null)
            { this._EligibleSupervisorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForUserSupervisorSelectList(this.UserObject.Id, this._SelectEligibleSupervisors.SearchTextBox.Text); }
            else
            { this._EligibleSupervisorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForUserSupervisorSelectList(0, this._SelectEligibleSupervisors.SearchTextBox.Text); }

            _SelectEligibleSupervisorsDataBind();
        }
        #endregion

        #region _ClearSearchSelectSupervisorsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Supervisor(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectSupervisorsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectSupervisorsForUser.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleSupervisors.ListBoxControl.Items.Clear();
            this._SelectEligibleSupervisors.SearchTextBox.Text = "";

            // clear the search
            if (this.UserObject != null)
            { this._EligibleSupervisorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForUserSupervisorSelectList(this.UserObject.Id, null); }
            else
            { this._EligibleSupervisorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForUserSupervisorSelectList(0, null); }

            _SelectEligibleSupervisorsDataBind();
        }
        #endregion

        #region _SelectEligibleSupervisorsDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectEligibleSupervisorsDataBind()
        {
            this._SelectEligibleSupervisors.ListBoxControl.DataSource = this._EligibleSupervisorsForSelectList;
            this._SelectEligibleSupervisors.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleSupervisors.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleSupervisors.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._SelectEligibleSupervisors.ListBoxControl.Items.Count == 0)
            {
                this._SelectSupervisorsForUser.SubmitButton.Enabled = false;
                this._SelectSupervisorsForUser.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectSupervisorsForUser.SubmitButton.Enabled = true;
                this._SelectSupervisorsForUser.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _BuildUserFieldContainer
        /// <summary>
        /// Builds the container and input for a user field.
        /// </summary>
        /// <param name="userField">user field object</param>
        /// <param name="view">is the field viewable?</param>
        /// <param name="modify">is the field modifyable?</param>
        /// <returns>Panel</returns>
        private Panel _BuildUserFieldContainer(UserAccountData.UserField userField, bool view, bool modify)
        {
            if (view)
            {
                // container
                Panel userFieldContainer = new Panel();
                userFieldContainer.ID = this.ID + "_" + userField.Identifier + "_Container";
                userFieldContainer.CssClass = "FormFieldContainer";

                // input
                Panel userFieldInputContainer = new Panel();
                userFieldInputContainer.ID = this.ID + "_" + userField.Identifier + "_InputContainer";
                userFieldInputContainer.CssClass = "FormFieldInputContainer";

                switch (userField.Identifier)
                {
                    case "avatar": // image and upload control

                        // set the user avatar image control, if avatar is exists for the user.
                        if (this.UserObject != null)
                        {
                            if (this.UserObject.Avatar != null)
                            {
                                // user avatar image panel
                                Panel avatarImagePanel = new Panel();
                                avatarImagePanel.ID = this.ID + "_" + userField.Identifier + "_ImageContainer";
                                avatarImagePanel.CssClass = "AvatarImageContainer";

                                // user avatar image
                                Image avatarImage = new Image();
                                avatarImage.ImageUrl = SitePathConstants.SITE_USERS_ROOT + this.UserObject.Id + "/" + this._SetInputValueString(userField.Identifier);
                                avatarImagePanel.Controls.Add(avatarImage);
                                userFieldInputContainer.Controls.Add(avatarImagePanel);

                                Panel avatarImageDeleteButtonContainer = new Panel();
                                avatarImageDeleteButtonContainer.ID = "AvatarImageDeleteButtonContainer";
                                avatarImageDeleteButtonContainer.CssClass = "AvatarDeleteButtonContainer";
                                avatarImagePanel.Controls.Add(avatarImageDeleteButtonContainer);

                                // delete course avatar image
                                Image deleteAvatarImage = new Image();
                                deleteAvatarImage.ID = "AvatarImageDeleteButton";
                                deleteAvatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                                deleteAvatarImage.CssClass = "SmallIcon";
                                deleteAvatarImage.Attributes.Add("onClick", "javascript:DeleteUserAvatar();");
                                deleteAvatarImage.Style.Add("cursor", "pointer");

                                avatarImageDeleteButtonContainer.Controls.Add(deleteAvatarImage);

                                // clear avatar hidden field
                                this.ClearAvatar = new HiddenField();
                                this.ClearAvatar.ID = "ClearAvatar_Field";
                                this.ClearAvatar.Value = "false";
                                userFieldInputContainer.Controls.Add(this.ClearAvatar);
                            }
                        }

                        // upload control
                        UploaderAsync fileUpload = new UploaderAsync(this.ID + "_" + userField.Identifier + "_Field", UploadType.Avatar, this.ID + "_" + userField.Identifier + "_ErrorContainer");
                        fileUpload.CssClass = userField.Identifier + "_Field";

                        // set params to resize the avatar upon upload
                        fileUpload.ResizeOnUpload = true;
                        fileUpload.ResizeMaxWidth = 256;
                        fileUpload.ResizeMaxHeight = 256;

                        if (!modify)
                        { fileUpload.Enabled = false; }

                        userFieldInputContainer.Controls.Add(fileUpload);

                        break;
                    case "password": // password textbox
                        TextBox passwordTextBox = new TextBox();
                        passwordTextBox.ID = this.ID + "_" + userField.Identifier + "_Field";
                        passwordTextBox.TextMode = TextBoxMode.Password;
                        passwordTextBox.CssClass = userField.Identifier + "_Field";

                        if (!modify)
                        { passwordTextBox.Enabled = false; }

                        userFieldInputContainer.Controls.Add(passwordTextBox);
                        break;
                    case "mustChangePassword": // checkbox
                        CheckBox checkBox = new CheckBox();
                        checkBox.ID = this.ID + "_" + userField.Identifier + "_Field";
                        checkBox.CssClass = userField.Identifier + "_Field";

                        checkBox.Text = _GlobalResources.ResourceManager.GetString("ForceUserToChangePasswordAtNextLogin", new CultureInfo(this.LanguageString));

                        if (!modify)
                        { checkBox.Enabled = false; }

                        userFieldInputContainer.Controls.Add(checkBox);
                        break;
                    case "disableLoginFromLoginForm": // checkbox
                        CheckBox disableLoginCheckBox = new CheckBox();
                        disableLoginCheckBox.ID = this.ID + "_" + userField.Identifier + "_Field";
                        disableLoginCheckBox.CssClass = userField.Identifier + "_Field";

                        disableLoginCheckBox.Text = _GlobalResources.ResourceManager.GetString("PreventTheUserFromLoggingInViaThePortal_sLoginForm", new CultureInfo(this.LanguageString));

                        if (this.UserObject != null)
                        { disableLoginCheckBox.Checked = (bool)this._SetInputValue(userField.Identifier); }

                        userFieldInputContainer.Controls.Add(disableLoginCheckBox);
                        break;
                    case "optOutOfEmailNotifications": // checkbox
                        CheckBox optOutOfEmailNotifications = new CheckBox();
                        optOutOfEmailNotifications.ID = this.ID + "_" + userField.Identifier + "_Field";
                        optOutOfEmailNotifications.CssClass = userField.Identifier + "_Field";

                        optOutOfEmailNotifications.Text = _GlobalResources.ResourceManager.GetString("OptOutOfEmailNotifications", new CultureInfo(this.LanguageString));

                        if (this.UserObject != null)
                        { optOutOfEmailNotifications.Checked = (bool)this._SetInputValue(userField.Identifier); }

                        userFieldInputContainer.Controls.Add(optOutOfEmailNotifications);
                        break;
                    case "excludeFromLeaderboards": // checkbox
                        CheckBox excludeFromLeaderboards = new CheckBox();
                        excludeFromLeaderboards.ID = this.ID + "_" + userField.Identifier + "_Field";
                        excludeFromLeaderboards.CssClass = userField.Identifier + "_Field";

                        excludeFromLeaderboards.Text = _GlobalResources.ResourceManager.GetString("ExcludeFromLeaderboards", new CultureInfo(this.LanguageString));

                        if (this.UserObject != null)
                        { excludeFromLeaderboards.Checked = (bool)this._SetInputValue(userField.Identifier); }

                        userFieldInputContainer.Controls.Add(excludeFromLeaderboards);
                        break;
                    case "idTimezone": // timezone selector
                        TimeZoneSelector timezoneSelector = new TimeZoneSelector();
                        timezoneSelector.ID = this.ID + "_" + userField.Identifier + "_Field";
                        timezoneSelector.CssClass = userField.Identifier + "_Field";

                        // set the value if there is a user object, else default it to the site's timezone
                        if (this.UserObject != null)
                        { timezoneSelector.SelectedValue = this._SetInputValueString(userField.Identifier); }
                        else
                        { timezoneSelector.SelectedValue = AsentiaSessionState.GlobalSiteObject.IdTimezone.ToString(); }

                        if (!modify)
                        { timezoneSelector.Enabled = false; }

                        userFieldInputContainer.Controls.Add(timezoneSelector);
                        break;
                    case "idLanguage": // language selector
                        LanguageSelector languageSelector = new LanguageSelector(LanguageDropDownType.DataOnly);
                        languageSelector.ID = this.ID + "_" + userField.Identifier + "_Field";
                        languageSelector.CssClass = userField.Identifier + "_Field";

                        // set the value if there is a user object
                        if (this.UserObject != null)
                        { languageSelector.SelectedValue = this._SetInputValueString(userField.Identifier); }

                        if (!modify)
                        { languageSelector.Enabled = false; }

                        userFieldInputContainer.Controls.Add(languageSelector);
                        break;
                    case "address": // multi-line text for address
                        TextBox userFieldAddressTextBox = new TextBox();
                        userFieldAddressTextBox.ID = this.ID + "_" + userField.Identifier + "_Field";
                        userFieldAddressTextBox.TextMode = TextBoxMode.MultiLine;
                        userFieldAddressTextBox.CssClass = userField.Identifier + "_Field";
                        userFieldAddressTextBox.Rows = 4;

                        // set the value if there is a user object
                        if (this.UserObject != null)
                        { userFieldAddressTextBox.Text = this._SetInputValueString(userField.Identifier); }

                        if (!modify)
                        { userFieldAddressTextBox.Enabled = false; }

                        userFieldInputContainer.Controls.Add(userFieldAddressTextBox);
                        break;
                    case "dtCreated": // label
                        Label staticFieldDtCreated = new Label();
                        staticFieldDtCreated.ID = this.ID + "_" + userField.Identifier + "_Field";
                        staticFieldDtCreated.CssClass = userField.Identifier + "_Field";

                        // set the value if there is a user object
                        if (this.UserObject != null)
                        {
                            if (this._SetInputValueDateTime(userField.Identifier) == null)
                            { staticFieldDtCreated.Text = "-"; }
                            else
                            {
                                DateTime dateTimeValue = (DateTime)this._SetInputValueDateTime(userField.Identifier);
                                staticFieldDtCreated.Text = dateTimeValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.LongDatePattern);
                            }
                        }
                        else
                        { staticFieldDtCreated.Text = "-"; }

                        userFieldInputContainer.Controls.Add(staticFieldDtCreated);
                        break;
                    case "dtLastLogin": // label
                        Label staticFieldDtLastLogin = new Label();
                        staticFieldDtLastLogin.ID = this.ID + "_" + userField.Identifier + "_Field";
                        staticFieldDtLastLogin.CssClass = userField.Identifier + "_Field";

                        // set the value if there is a user object
                        if (this.UserObject != null)
                        {
                            if (this._SetInputValueDateTime(userField.Identifier) == null)
                            { staticFieldDtLastLogin.Text = _GlobalResources.Never; }
                            else
                            {
                                DateTime dateTimeValue = (DateTime)this._SetInputValueDateTime(userField.Identifier);
                                staticFieldDtLastLogin.Text = dateTimeValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.LongDatePattern);
                            }
                        }
                        else
                        { staticFieldDtLastLogin.Text = _GlobalResources.Never; }

                        userFieldInputContainer.Controls.Add(staticFieldDtLastLogin);
                        break;
                    case "dtExpires": // date picker w/ none option
                        DatePicker datePickerWithNone = new DatePicker(this.ID + "_" + userField.Identifier + "_Field", true, false, false);
                        datePickerWithNone.LowYear = DateTime.UtcNow.Year;
                        datePickerWithNone.HighYear = DateTime.UtcNow.Year + 5;
                        datePickerWithNone.NoneCheckboxText = _GlobalResources.NeverExpires;
                        datePickerWithNone.CssClass = userField.Identifier + "_Field";

                        // set the value if there is a user object
                        if (this.UserObject != null)
                        { datePickerWithNone.Value = this._SetInputValueDateTime(userField.Identifier); }
                        else
                        { datePickerWithNone.Value = null; }

                        if (!modify)
                        { datePickerWithNone.Enabled = false; }

                        userFieldInputContainer.Controls.Add(datePickerWithNone);
                        break;
                    case "dtHire":
                    case "dtTerm":
                    case "dtDOB": // date picker
                        DatePicker datePicker = new DatePicker(this.ID + "_" + userField.Identifier + "_Field", false, false, false);
                        datePicker.LowYear = 1900;
                        datePicker.CssClass = userField.Identifier + "_Field";

                        // set the value if there is a user object
                        if (this.UserObject != null)
                        { datePicker.Value = this._SetInputValueDateTime(userField.Identifier); }

                        if (!modify)
                        { datePicker.Enabled = false; }

                        userFieldInputContainer.Controls.Add(datePicker);
                        break;
                    case "isActive": // drop down
                        DropDownList dropDownListIsActive = new DropDownList();
                        dropDownListIsActive.ID = this.ID + "_" + userField.Identifier + "_Field";
                        dropDownListIsActive.CssClass = userField.Identifier + "_Field";

                        dropDownListIsActive.Items.Add(new ListItem(_GlobalResources.Enabled, "true"));
                        dropDownListIsActive.Items.Add(new ListItem(_GlobalResources.Disabled, "false"));

                        // set the value if there is a user object
                        if (this.UserObject != null)
                        { dropDownListIsActive.SelectedValue = this._SetInputValueString(userField.Identifier); }

                        if (!modify)
                        { dropDownListIsActive.Enabled = false; }

                        userFieldInputContainer.Controls.Add(dropDownListIsActive);
                        break;
                    case "gender": // radio button list
                        RadioButtonList radioButtonListGender = new RadioButtonList();
                        radioButtonListGender.ID = this.ID + "_" + userField.Identifier + "_Field";
                        radioButtonListGender.CssClass = userField.Identifier + "_Field";
                        radioButtonListGender.RepeatDirection = RepeatDirection.Horizontal;

                        radioButtonListGender.Items.Add(new ListItem(" " + _GlobalResources.Male, "m"));
                        radioButtonListGender.Items.Add(new ListItem(" " + _GlobalResources.Female, "f"));

                        // set the value if there is a user object
                        if (this.UserObject != null)
                        { radioButtonListGender.SelectedValue = this._SetInputValueString(userField.Identifier); }

                        if (!modify)
                        { radioButtonListGender.Enabled = false; }

                        userFieldInputContainer.Controls.Add(radioButtonListGender);
                        break;
                    default:
                        switch (userField.InputType)
                        {
                            case UserAccountData.InputType.CheckBoxes:
                                CheckBoxList checkBoxList = new CheckBoxList();
                                checkBoxList.ID = this.ID + "_" + userField.Identifier + "_Field";
                                checkBoxList.CssClass = userField.Identifier + "_Field";

                                string[] checkBoxListItems = this.UserAccountData.GetUserFieldChoiceValuesInLanguage(userField, this.LanguageString).Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                                if (this.UserAccountData.GetUserFieldChoiceValuesAlphabetizeSettingInLanguage(userField, this.LanguageString))
                                { Array.Sort(checkBoxListItems, StringComparer.InvariantCultureIgnoreCase); }

                                foreach (string checkBoxListItem in checkBoxListItems)
                                {
                                    checkBoxList.Items.Add(new ListItem(" " + checkBoxListItem, checkBoxListItem));
                                }

                                // set the value if there is a user object
                                if (this.UserObject != null)
                                {
                                    string values = this._SetInputValueString(userField.Identifier);
                                    string[] valuesArray = values.Split('|');

                                    foreach (string value in valuesArray)
                                    {
                                        for (int i = 0; i < checkBoxList.Items.Count; i++)
                                        {
                                            if (value == checkBoxList.Items[i].Value)
                                            { checkBoxList.Items[i].Selected = true; }
                                        }
                                    }
                                }

                                if (!modify)
                                { checkBoxList.Enabled = false; }

                                userFieldInputContainer.Controls.Add(checkBoxList);
                                break;
                            case UserAccountData.InputType.Date:
                                DatePicker userFieldDatePickerType = new DatePicker(this.ID + "_" + userField.Identifier + "_Field", false, false, false);
                                userFieldDatePickerType.CssClass = userField.Identifier + "_Field";

                                // set the value if there is a user object
                                if (this.UserObject != null)
                                { userFieldDatePickerType.Value = this._SetInputValueDateTime(userField.Identifier); }

                                if (!modify)
                                { userFieldDatePickerType.Enabled = false; }

                                userFieldInputContainer.Controls.Add(userFieldDatePickerType);
                                break;
                            case UserAccountData.InputType.RadioButtons:
                                RadioButtonList radioButtonList = new RadioButtonList();
                                radioButtonList.ID = this.ID + "_" + userField.Identifier + "_Field";
                                radioButtonList.CssClass = userField.Identifier + "_Field";

                                string[] radioButtonListItems = this.UserAccountData.GetUserFieldChoiceValuesInLanguage(userField, this.LanguageString).Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                                if (this.UserAccountData.GetUserFieldChoiceValuesAlphabetizeSettingInLanguage(userField, this.LanguageString))
                                { Array.Sort(radioButtonListItems, StringComparer.InvariantCultureIgnoreCase); }

                                foreach (string radioButtonListItem in radioButtonListItems)
                                {
                                    radioButtonList.Items.Add(new ListItem(" " + radioButtonListItem, radioButtonListItem));
                                }

                                // set the value if there is a user object
                                if (this.UserObject != null)
                                { radioButtonList.SelectedValue = this._SetInputValueString(userField.Identifier); }

                                if (!modify)
                                { radioButtonList.Enabled = false; }

                                userFieldInputContainer.Controls.Add(radioButtonList);
                                break;
                            case UserAccountData.InputType.SelectBoxSingleSelect:
                                ListBox listBox = new ListBox();
                                listBox.ID = this.ID + "_" + userField.Identifier + "_Field";
                                listBox.SelectionMode = ListSelectionMode.Single;
                                listBox.CssClass = userField.Identifier + "_Field";

                                string[] listBoxItems = this.UserAccountData.GetUserFieldChoiceValuesInLanguage(userField, this.LanguageString).Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                                if (this.UserAccountData.GetUserFieldChoiceValuesAlphabetizeSettingInLanguage(userField, this.LanguageString))
                                { Array.Sort(listBoxItems, StringComparer.InvariantCultureIgnoreCase); }

                                foreach (string listBoxItem in listBoxItems)
                                {
                                    listBox.Items.Add(new ListItem(listBoxItem));
                                }

                                // set the value if there is a user object
                                if (this.UserObject != null)
                                { listBox.SelectedValue = this._SetInputValueString(userField.Identifier); }

                                if (!modify)
                                { listBox.Enabled = false; }

                                userFieldInputContainer.Controls.Add(listBox);
                                break;
                            case UserAccountData.InputType.SelectBoxMultipleSelect:
                                ListBox listBoxMulti = new ListBox();
                                listBoxMulti.ID = this.ID + "_" + userField.Identifier + "_Field";
                                listBoxMulti.SelectionMode = ListSelectionMode.Multiple;
                                listBoxMulti.CssClass = userField.Identifier + "_Field";

                                string[] listBoxMultiItems = this.UserAccountData.GetUserFieldChoiceValuesInLanguage(userField, this.LanguageString).Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                                if (this.UserAccountData.GetUserFieldChoiceValuesAlphabetizeSettingInLanguage(userField, this.LanguageString))
                                { Array.Sort(listBoxMultiItems, StringComparer.InvariantCultureIgnoreCase); }

                                foreach (string listBoxMultiItem in listBoxMultiItems)
                                {
                                    listBoxMulti.Items.Add(new ListItem(listBoxMultiItem));
                                }

                                // set the value if there is a user object
                                if (this.UserObject != null)
                                {
                                    string values = this._SetInputValueString(userField.Identifier);
                                    string[] valuesArray = values.Split('|');

                                    foreach (string value in valuesArray)
                                    {
                                        for (int i = 0; i < listBoxMulti.Items.Count; i++)
                                        {
                                            if (value == listBoxMulti.Items[i].Value)
                                            { listBoxMulti.Items[i].Selected = true; }
                                        }
                                    }
                                }

                                if (!modify)
                                { listBoxMulti.Enabled = false; }

                                userFieldInputContainer.Controls.Add(listBoxMulti);
                                break;
                            case UserAccountData.InputType.DropDownList:
                                DropDownList dropDownList = new DropDownList();
                                dropDownList.ID = this.ID + "_" + userField.Identifier + "_Field";
                                dropDownList.CssClass = userField.Identifier + "_Field";

                                string[] dropDownListItems = this.UserAccountData.GetUserFieldChoiceValuesInLanguage(userField, this.LanguageString).Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                                if (this.UserAccountData.GetUserFieldChoiceValuesAlphabetizeSettingInLanguage(userField, this.LanguageString))
                                { Array.Sort(dropDownListItems, StringComparer.InvariantCultureIgnoreCase); }

                                foreach (string dropDownListItem in dropDownListItems)
                                {
                                    dropDownList.Items.Add(new ListItem(dropDownListItem));
                                }

                                // set the value if there is a user object
                                if (this.UserObject != null)
                                { dropDownList.SelectedValue = this._SetInputValueString(userField.Identifier); }

                                if (!modify)
                                { dropDownList.Enabled = false; }

                                userFieldInputContainer.Controls.Add(dropDownList);
                                break;
                            default:
                                TextBox userFieldTextBox = new TextBox();
                                userFieldTextBox.ID = this.ID + "_" + userField.Identifier + "_Field";
                                userFieldTextBox.CssClass = userField.Identifier + "_Field";

                                // set the value if there is a user object
                                if (this.UserObject != null)
                                { userFieldTextBox.Text = this._SetInputValueString(userField.Identifier); }

                                if (!modify)
                                { userFieldTextBox.Enabled = false; }

                                userFieldInputContainer.Controls.Add(userFieldTextBox);
                                break;
                        }
                        break;
                }

                // label
                Panel userFieldLabelContainer = new Panel();
                userFieldLabelContainer.ID = this.ID + "_" + userField.Identifier + "_LabelContainer";
                userFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                Label userFieldLabel = new Label();
                userFieldLabel.Text = HttpUtility.HtmlEncode(this.UserAccountData.GetUserFieldLabelInLanguage(userField, this.LanguageString)) + ":";
                userFieldLabel.AssociatedControlID = this.ID + "_" + userField.Identifier + "_Field";

                userFieldLabelContainer.Controls.Add(userFieldLabel);

                if ((this.UserObject == null && userField.Identifier == "password") || (userField.IsRequired && userField.Identifier != "password"))
                {
                    Label requiredAsterisk = new Label();
                    requiredAsterisk.Text = " * ";
                    requiredAsterisk.CssClass = "RequiredAsterisk";
                    userFieldLabelContainer.Controls.Add(requiredAsterisk);
                }

                // error panel
                Panel userFieldErrorContainer = new Panel();
                userFieldErrorContainer.ID = this.ID + "_" + userField.Identifier + "_ErrorContainer";
                userFieldErrorContainer.CssClass = "FormFieldErrorContainer";

                // description
                Panel userFieldDescriptionContainer = new Panel();
                userFieldDescriptionContainer.ID = this.ID + "_" + userField.Identifier + "_DescriptionContainer";
                userFieldDescriptionContainer.CssClass = "FormFieldDescriptionContainer";

                Literal userFieldDescription = new Literal();
                userFieldDescription.Text = HttpUtility.HtmlEncode(this.UserAccountData.GetUserFieldDescriptionInLanguage(userField, this.LanguageString));
                userFieldDescriptionContainer.Controls.Add(userFieldDescription);

                // add controls to container
                userFieldContainer.Controls.Add(userFieldLabelContainer);
                userFieldContainer.Controls.Add(userFieldErrorContainer);
                userFieldContainer.Controls.Add(userFieldDescriptionContainer);
                userFieldContainer.Controls.Add(userFieldInputContainer);

                // if we're building the "password" field, add contriners for "confirm password"
                if (userField.Identifier == "password")
                {
                    // field
                    Panel confirmPasswordInputContainer = new Panel();
                    confirmPasswordInputContainer.ID = this.ID + "_" + userField.Identifier + "Confirm_InputContainer";
                    confirmPasswordInputContainer.CssClass = "FormFieldInputContainer";

                    TextBox confirmPasswordTextBox = new TextBox();
                    confirmPasswordTextBox.ID = this.ID + "_" + userField.Identifier + "Confirm_Field";
                    confirmPasswordTextBox.TextMode = TextBoxMode.Password;
                    confirmPasswordTextBox.CssClass = userField.Identifier + "_Field";

                    if (!modify)
                    { confirmPasswordTextBox.Enabled = false; }

                    confirmPasswordInputContainer.Controls.Add(confirmPasswordTextBox);

                    // label
                    Panel confirmPasswordLabelContainer = new Panel();
                    confirmPasswordLabelContainer.ID = this.ID + "_" + userField.Identifier + "Confirm_LabelContainer";
                    confirmPasswordLabelContainer.CssClass = "FormFieldLabelContainer";

                    Label confirmPasswordLabel = new Label();
                    confirmPasswordLabel.Text = _GlobalResources.ResourceManager.GetString("ConfirmByEnteringAgain", new CultureInfo(this.LanguageString)) + ":";
                    confirmPasswordLabel.AssociatedControlID = this.ID + "_" + userField.Identifier + "Confirm_Field";
                    confirmPasswordLabelContainer.Controls.Add(confirmPasswordLabel);

                    // add controls to container
                    userFieldContainer.Controls.Add(confirmPasswordLabelContainer);
                    userFieldContainer.Controls.Add(confirmPasswordInputContainer);
                }

                // return the built user field container
                return userFieldContainer;
            }

            return null;
        }
        #endregion

        #region _BuildUserAgreementPanel
        /// <summary>
        /// Builds the user agreement panel.
        /// </summary>
        private void _BuildUserAgreementPanel()
        {
            //set whether or not the user agreement need to be displayed in the registration
            if (this.ViewType == UserFormViewType.RegistrationPage && AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERAGREEMENT_DISPLAY) == true)
            { this.DisplayUserAgreement = true; }

            // user agreement panel
            Panel userAgreementPanel = new Panel();
            userAgreementPanel.ID = this.ID + "_UserAgreementContainer";
            userAgreementPanel.CssClass = "UserAgreementContainer";
            userAgreementPanel.Visible = this.DisplayUserAgreement;

            // add user agreement html to panel
            Literal userAgreementPanelHTML = new Literal();
            userAgreementPanelHTML.Text = this.UserAccountData.GetUserAgreementHTMLInLanguage(this.LanguageString);
            userAgreementPanel.Controls.Add(userAgreementPanelHTML);

            // user agreement label
            Panel userAgreementLabelContainer = new Panel();
            userAgreementLabelContainer.ID = this.ID + "_UserAgreementLabelContainer";
            userAgreementLabelContainer.CssClass = "FormFieldLabelContainer";
            userAgreementLabelContainer.Visible = this.DisplayUserAgreement;

            // add user agreement label to panel
            Label userAgreementLabel = new Label();
            userAgreementLabel.Text = _GlobalResources.UserAgreement + ":";

            userAgreementLabelContainer.Controls.Add(userAgreementLabel);

            // add asterisk if the user agreement is required
            if (!String.IsNullOrWhiteSpace(userAgreementPanelHTML.Text) && AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERAGREEMENT_REQUIRED) == true)
            {
                Label requiredAsterisk = new Label();
                requiredAsterisk.Text = " * ";
                requiredAsterisk.CssClass = "RequiredAsterisk";
                userAgreementLabelContainer.Controls.Add(requiredAsterisk);
            }

            // attach controls
            this.Controls.Add(userAgreementLabelContainer);
            this.Controls.Add(userAgreementPanel);

            // if there is a user agreement in place, add the checkbox
            if (!String.IsNullOrWhiteSpace(userAgreementPanelHTML.Text))
            {
                Panel userAgreementAgreeCheckBoxPanel = new Panel();
                userAgreementAgreeCheckBoxPanel.ID = this.ID + "_UserAgreementAgreeCheckBoxContainer";

                // error panel
                Panel userAgreementAgreeCheckBoxErrorPanel = new Panel();
                userAgreementAgreeCheckBoxErrorPanel.ID = this.ID + "_UserAgreementAgreeCheckBoxErrorContainer";
                userAgreementAgreeCheckBoxErrorPanel.CssClass = "FormFieldErrorContainer";
                userAgreementAgreeCheckBoxPanel.Controls.Add(userAgreementAgreeCheckBoxErrorPanel);

                // set whether or not the user is required to agree to the user agreement
                if (this.ViewType == UserFormViewType.RegistrationPage && AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERAGREEMENT_REQUIRED) == true)
                { this.IsUserAgreementRequired = true; }

                // add user agreement check box
                CheckBox checkBox = new CheckBox();
                checkBox.ID = this.ID + "_UserAgreementAgree_Field";
                checkBox.Text = _GlobalResources.ResourceManager.GetString("IAcceptTheAboveTermsAndConditions", new CultureInfo(this.LanguageString));
                checkBox.Visible = this.IsUserAgreementRequired;
                userAgreementAgreeCheckBoxPanel.Controls.Add(checkBox);

                this.Controls.Add(userAgreementAgreeCheckBoxPanel);

            }
        }
        #endregion

        #region _SetInputValueString
        /// <summary>
        /// Used to set the value of an input that takes a string (most inputs).
        /// </summary>
        /// <param name="fieldIdentifier">the id of the user field</param>
        /// <returns>string value</returns>
        private string _SetInputValueString(string fieldIdentifier)
        {
            object inputValue = this._SetInputValue(fieldIdentifier);

            if (inputValue == null)
            { return String.Empty; }

            return inputValue.ToString();
        }
        #endregion

        #region _SetInputValueDateTime
        /// <summary>
        /// Used to set the value of an input that takes a DateTime.
        /// </summary>
        /// <param name="fieldIdentifier">the id of the user field</param>
        /// <returns>DateTime value</returns>
        private DateTime? _SetInputValueDateTime(string fieldIdentifier)
        {
            object inputValue = this._SetInputValue(fieldIdentifier);

            if (inputValue == null)
            { return null; }

            DateTime inputValueFormatted;

            if (DateTime.TryParse(inputValue.ToString(), out inputValueFormatted))
            { return inputValueFormatted; }

            return null;
        }
        #endregion

        #region _SetInputValue
        /// <summary>
        /// Used to set the value of an input field from a user object property.
        /// </summary>
        /// <param name="fieldIdentifier">the id of the user field</param>
        /// <returns>object containing the value of the user object property</returns>
        private object _SetInputValue(string fieldIdentifier)
        {
            switch (fieldIdentifier)
            {
                case "firstName":
                    return this.UserObject.FirstName;
                case "middleName":
                    return this.UserObject.MiddleName;
                case "lastName":
                    return this.UserObject.LastName;
                case "email":
                    return this.UserObject.Email;
                case "username":
                    return this.UserObject.Username;
                case "disableLoginFromLoginForm":
                    return this.UserObject.DisableLoginFromLoginForm;
                case "optOutOfEmailNotifications":
                    return this.UserObject.OptOutOfEmailNotifications;
                case "excludeFromLeaderboards":
                    return this.UserObject.ExcludeFromLeaderboards;
                case "idTimezone":
                    return this.UserObject.IdTimezone;
                case "idLanguage":
                    return this.UserObject.LanguageString;
                case "dtCreated":
                    return this.UserObject.DtCreated;
                case "dtExpires":
                    return this.UserObject.DtExpires;
                case "dtLastLogin":
                    return this.UserObject.DtLastLogin;
                case "isActive":
                    if (this.UserObject.IsActive)
                    { return "true"; }
                    else
                    { return "false"; }
                case "company":
                    return this.UserObject.Company;
                case "address":
                    return this.UserObject.Address;
                case "city":
                    return this.UserObject.City;
                case "province":
                    return this.UserObject.Province;
                case "postalcode":
                    return this.UserObject.PostalCode;
                case "country":
                    return this.UserObject.Country;
                case "phonePrimary":
                    return this.UserObject.PhonePrimary;
                case "phoneHome":
                    return this.UserObject.PhoneHome;
                case "phoneWork":
                    return this.UserObject.PhoneWork;
                case "phoneFax":
                    return this.UserObject.PhoneFax;
                case "phoneMobile":
                    return this.UserObject.PhoneMobile;
                case "phonePager":
                    return this.UserObject.PhonePager;
                case "phoneOther":
                    return this.UserObject.PhoneOther;
                case "employeeID":
                    return this.UserObject.EmployeeId;
                case "jobTitle":
                    return this.UserObject.JobTitle;
                case "jobClass":
                    return this.UserObject.JobClass;
                case "division":
                    return this.UserObject.Division;
                case "region":
                    return this.UserObject.Region;
                case "department":
                    return this.UserObject.Department;
                case "dtHire":
                    return this.UserObject.DtHire;
                case "dtTerm":
                    return this.UserObject.DtTerm;
                case "gender":
                    return this.UserObject.Gender;
                case "race":
                    return this.UserObject.Race;
                case "dtDOB":
                    return this.UserObject.DtDOB;
                case "avatar":
                    return this.UserObject.Avatar;
                case "field00":
                    return this.UserObject.Field00;
                case "field01":
                    return this.UserObject.Field01;
                case "field02":
                    return this.UserObject.Field02;
                case "field03":
                    return this.UserObject.Field03;
                case "field04":
                    return this.UserObject.Field04;
                case "field05":
                    return this.UserObject.Field05;
                case "field06":
                    return this.UserObject.Field06;
                case "field07":
                    return this.UserObject.Field07;
                case "field08":
                    return this.UserObject.Field08;
                case "field09":
                    return this.UserObject.Field09;
                case "field10":
                    return this.UserObject.Field10;
                case "field11":
                    return this.UserObject.Field11;
                case "field12":
                    return this.UserObject.Field12;
                case "field13":
                    return this.UserObject.Field13;
                case "field14":
                    return this.UserObject.Field14;
                case "field15":
                    return this.UserObject.Field15;
                case "field16":
                    return this.UserObject.Field16;
                case "field17":
                    return this.UserObject.Field17;
                case "field18":
                    return this.UserObject.Field18;
                case "field19":
                    return this.UserObject.Field19;
            }

            return null;
        }
        #endregion

        #region _GetInputValue
        /// <summary>
        /// Gets the properly formatted string value of an input control,
        /// i.e. blanks get turned into nulls.
        /// </summary>
        /// <param name="fieldIdentifier">the id of the user field</param>
        /// <returns>string value</returns>
        public string _GetInputValue(string fieldIdentifier)
        {
            string value = this._GetInputValueString(fieldIdentifier);

            if (String.IsNullOrWhiteSpace(value))
            { return null; }
            else
            { return value; }
        }
        #endregion

        #region _GetInputValueString
        /// <summary>
        /// Gets the string value of an input from the input control.
        /// 
        /// Note, this method returns null when the input is not found, any "blank string"
        /// values are returned as a blank string. This is important to remember as it has
        /// an effect on the saving of data, i.e. some fields are not editable in certain
        /// views, so they wont exist in this form.
        /// </summary>
        /// <param name="fieldIdentifier">the id of the user field</param>
        /// <returns>string value</returns>
        public string _GetInputValueString(string fieldIdentifier)
        {
            if (this.FindControl(this.ID + "_" + fieldIdentifier + "_Field") != null)
            {
                Control control = this.FindControl(this.ID + "_" + fieldIdentifier + "_Field");

                if (control.GetType() == typeof(TextBox)) // textbox control
                {
                    TextBox textBoxControl = (TextBox)control;
                    return textBoxControl.Text;
                }
                if (control.GetType() == typeof(CheckBox)) // checkbox control
                {
                    CheckBox checkBoxControl = (CheckBox)control;
                    return checkBoxControl.Checked.ToString();
                }
                if (control.GetType() == typeof(CheckBoxList)) // checkboxlist control
                {
                    CheckBoxList checkBoxListControl = (CheckBoxList)control;
                    int i;
                    string checkedValues = String.Empty;

                    for (i = 0; i < checkBoxListControl.Items.Count; i++)
                    {
                        if (checkBoxListControl.Items[i].Selected)
                        {
                            checkedValues += checkBoxListControl.Items[i].Value;

                            if (i < checkBoxListControl.Items.Count - 1)
                            { checkedValues += "|"; }
                        }
                    }

                    if (checkedValues.Length > 0)
                    {
                        if (checkedValues.Substring(checkedValues.Length - 1, 1) == "|")
                        { checkedValues = checkedValues.Substring(0, checkedValues.Length - 1); }
                    }

                    return checkedValues;
                }
                if (control.GetType() == typeof(RadioButtonList)) // radiobuttonlist control
                {
                    RadioButtonList radioButtonListControl = (RadioButtonList)control;
                    return radioButtonListControl.SelectedValue;
                }
                if (control.GetType() == typeof(ListBox)) // listbox control
                {
                    ListBox listBoxControl = (ListBox)control;

                    if (listBoxControl.SelectionMode == ListSelectionMode.Single) // single select
                    { return listBoxControl.SelectedValue; }
                    else // multi-select
                    {
                        int i;
                        string checkedValues = String.Empty;

                        for (i = 0; i < listBoxControl.Items.Count; i++)
                        {
                            if (listBoxControl.Items[i].Selected)
                            {
                                checkedValues += listBoxControl.Items[i].Value;

                                if (i < listBoxControl.Items.Count - 1)
                                { checkedValues += "|"; }
                            }
                        }

                        if (checkedValues.Length > 0)
                        {
                            if (checkedValues.Substring(checkedValues.Length - 1, 1) == "|")
                            { checkedValues = checkedValues.Substring(0, checkedValues.Length - 1); }
                        }

                        return checkedValues;
                    }
                }
                if (control.GetType() == typeof(DropDownList)) // dropdownlist control
                {
                    DropDownList dropDownListControl = (DropDownList)control;
                    return dropDownListControl.SelectedValue;
                }
                if (control.GetType() == typeof(LanguageSelector)) // language selector control
                {
                    LanguageSelector languageSelectorControl = (LanguageSelector)control;
                    return languageSelectorControl.SelectedValue;
                }
                if (control.GetType() == typeof(TimeZoneSelector)) // timezoneselector control
                {
                    TimeZoneSelector timezoneSelectorControl = (TimeZoneSelector)control;
                    return timezoneSelectorControl.SelectedValue;
                }
                if (control.GetType() == typeof(DatePicker)) // datepicker control
                {
                    DatePicker datePickerControl = (DatePicker)control;
                    return datePickerControl.ValueString;
                }
                if (control.GetType() == typeof(UploaderAsync)) // uploaderasync control
                {
                    UploaderAsync uploaderAsyncControl = (UploaderAsync)control;
                    return uploaderAsyncControl.SavedFilePath;
                }
                // input type not matched, if there is a user object set the value,
                // otherwise return null
                else
                {
                    if (this.UserObject != null)
                    { return this._SetInputValueString(fieldIdentifier); }

                    return null;
                }
            }
            // input identifier not found, if there is a user object set the value,
            // otherwise return null
            else
            {
                if (this.UserObject != null)
                { return this._SetInputValueString(fieldIdentifier); }

                return null;
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(this.GetType(), "Asentia.UMS.Controls.UserForm.js");

            // build global JS variables for Social Media elements
            StringBuilder userGlobalJS = new StringBuilder();
            userGlobalJS.AppendLine("DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            csm.RegisterClientScriptBlock(typeof(UserForm), "userGlobalJS", userGlobalJS.ToString(), true);

        }
        #endregion
        #endregion
    }
}