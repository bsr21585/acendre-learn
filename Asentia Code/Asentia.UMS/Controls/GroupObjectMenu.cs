﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Controls
{
    public class GroupObjectMenu : WebControl
    {
        #region Constructor
        public GroupObjectMenu(Group groupObject)
            : base(HtmlTextWriterTag.Div)
        {
            this.GroupObject = groupObject;
            this.ID = "GroupObjectMenu";
        }
        #endregion

        #region Properties
        /// <summary>
        /// The group that this menu is built for.
        /// </summary>
        public Group GroupObject;

        /// <summary>
        /// The selected menu item.
        /// </summary>
        public MenuObjectItem SelectedItem = MenuObjectItem.None;
        #endregion

        #region MenuObjectItem ENUM
        public enum MenuObjectItem
        {
            None = 0,
            GroupDashboard = 1,
            GroupProperties = 2,
            AutoJoinRules = 3,
            Enrollments = 4,
            PrivateCatalogAccess = 5,
            Roles = 6,
            Wall = 7,
            Documents = 8,
        }
        #endregion

        #region Private Methods
        #region _BuildGroupObjectMenu
        private void _BuildGroupObjectMenu()
        {
            this.CssClass = "ObjectMenu";

            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this.GroupObject.Id))
            {
                // GROUP DASHBOARD

                // group dashboard link container
                Panel groupDashboardLinkContainer = new Panel();
                groupDashboardLinkContainer.ID = "GroupDashboardLinkContainer";
                groupDashboardLinkContainer.CssClass = "ObjectMenuLinkContainer";
                groupDashboardLinkContainer.ToolTip = _GlobalResources.GroupDashboard;

                HyperLink groupDashboardImageLink = new HyperLink();
                groupDashboardImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD, ImageFiles.EXT_PNG);
                groupDashboardImageLink.NavigateUrl = "/administrator/groups/Dashboard.aspx?id=" + this.GroupObject.Id.ToString();
                groupDashboardLinkContainer.Controls.Add(groupDashboardImageLink);

                this.Controls.Add(groupDashboardLinkContainer);

                // GROUP PROPERTIES

                // group properties link container
                Panel groupPropertiesLinkContainer = new Panel();
                groupPropertiesLinkContainer.ID = "GroupPropertiesLinkContainer";
                groupPropertiesLinkContainer.CssClass = "ObjectMenuLinkContainer";
                groupPropertiesLinkContainer.ToolTip = _GlobalResources.GroupProperties;

                HyperLink groupPropertiesImageLink = new HyperLink();
                groupPropertiesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
                groupPropertiesImageLink.NavigateUrl = "/administrator/groups/Modify.aspx?id=" + this.GroupObject.Id.ToString();
                groupPropertiesLinkContainer.Controls.Add(groupPropertiesImageLink);

                this.Controls.Add(groupPropertiesLinkContainer);

                // AUTO-JOIN RULES            

                // auto-join rules link container
                Panel autoJoinRulesLinkContainer = new Panel();
                autoJoinRulesLinkContainer.ID = "AutoJoinRulesLinkContainer";
                autoJoinRulesLinkContainer.CssClass = "ObjectMenuLinkContainer";
                autoJoinRulesLinkContainer.ToolTip = _GlobalResources.AutoJoinRules;

                HyperLink autoJoinRulesImageLink = new HyperLink();
                autoJoinRulesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYNCHRONIZE, ImageFiles.EXT_PNG);
                autoJoinRulesImageLink.NavigateUrl = "/administrator/groups/AutoJoinRules.aspx?gid=" + this.GroupObject.Id.ToString();
                autoJoinRulesLinkContainer.Controls.Add(autoJoinRulesImageLink);

                this.Controls.Add(autoJoinRulesLinkContainer);

                // ENROLLMENTS

                // enrollments link container
                Panel enrollmentsLinkContainer = new Panel();
                enrollmentsLinkContainer.ID = "EnrollmentsLinkContainer";
                enrollmentsLinkContainer.CssClass = "ObjectMenuLinkContainer";
                enrollmentsLinkContainer.ToolTip = _GlobalResources.Enrollments;

                HyperLink enrollmentsImageLink = new HyperLink();
                enrollmentsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
                enrollmentsImageLink.NavigateUrl = "/administrator/groups/enrollments/Default.aspx?gid=" + this.GroupObject.Id.ToString();
                enrollmentsLinkContainer.Controls.Add(enrollmentsImageLink);

                this.Controls.Add(enrollmentsLinkContainer);

                // PRIVATE CATALOG ACCESS          

                // private catalog access link container
                Panel privateCatalogAccessLinkContainer = new Panel();
                privateCatalogAccessLinkContainer.ID = "PrivateCatalogAccessLinkContainer";
                privateCatalogAccessLinkContainer.CssClass = "ObjectMenuLinkContainer";
                privateCatalogAccessLinkContainer.ToolTip = _GlobalResources.PrivateCatalogAccess;

                HyperLink privateCatalogAccessImageLink = new HyperLink();
                privateCatalogAccessImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG_PRIVATE, ImageFiles.EXT_PNG);
                privateCatalogAccessImageLink.NavigateUrl = "/administrator/groups/PrivateCatalogAccess.aspx?gid=" + this.GroupObject.Id.ToString();
                privateCatalogAccessLinkContainer.Controls.Add(privateCatalogAccessImageLink);

                this.Controls.Add(privateCatalogAccessLinkContainer);
            }

            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
            {
                // ROLES

                // roles link container
                Panel rolesLinkContainer = new Panel();
                rolesLinkContainer.ID = "RolesLinkContainer";
                rolesLinkContainer.CssClass = "ObjectMenuLinkContainer";
                rolesLinkContainer.ToolTip = _GlobalResources.Roles;

                HyperLink rolesImageLink = new HyperLink();
                rolesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG);
                rolesImageLink.NavigateUrl = "/administrator/groups/Roles.aspx?gid=" + this.GroupObject.Id.ToString();
                rolesLinkContainer.Controls.Add(rolesImageLink);

                this.Controls.Add(rolesLinkContainer);
            }

            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this.GroupObject.Id))
            {
                // WALL
                if (GroupObject.IsFeedActive == true && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
                {
                    // wall link container
                    Panel wallLinkContainer = new Panel();
                    wallLinkContainer.ID = "WallLinkContainer";
                    wallLinkContainer.CssClass = "ObjectMenuLinkContainer";
                    wallLinkContainer.ToolTip = _GlobalResources.Discussion;

                    HyperLink wallImageLink = new HyperLink();
                    wallImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG);
                    wallImageLink.NavigateUrl = "/administrator/groups/ManageWall.aspx?id=" + this.GroupObject.Id.ToString();
                    wallLinkContainer.Controls.Add(wallImageLink);

                    this.Controls.Add(wallLinkContainer);
                }

                // DOCUMENTS - only if group documents feature is enabled
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE))
                {
                    // documents link container
                    Panel documentsLinkContainer = new Panel();
                    documentsLinkContainer.ID = "DocumentsLinkContainer";
                    documentsLinkContainer.CssClass = "ObjectMenuLinkContainer";
                    documentsLinkContainer.ToolTip = _GlobalResources.Documents;

                    HyperLink documentsImageLink = new HyperLink();
                    documentsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MYACCOUNT, ImageFiles.EXT_PNG);
                    documentsImageLink.NavigateUrl = "/administrator/groups/documents/Default.aspx?gid=" + this.GroupObject.Id.ToString();
                    documentsLinkContainer.Controls.Add(documentsImageLink);

                    this.Controls.Add(documentsLinkContainer);
                }
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnLoad
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.CreateControls();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        #endregion

        #region CreateControls
        /// <summary>
        /// Creates the controls for rendering to page.
        /// </summary>
        private void CreateControls()
        {
            // build the group object menu
            this._BuildGroupObjectMenu();            

            // apply selected class to item
            switch (this.SelectedItem)
            {
                case MenuObjectItem.None:
                    // nothing to do
                    break;
                case MenuObjectItem.GroupDashboard:
                    // select the group dashboard link
                    Panel groupDashboardLinkContainer = (Panel)this.FindControl("GroupDashboardLinkContainer");

                    if (groupDashboardLinkContainer != null)
                    { groupDashboardLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.GroupProperties:
                    // select the group properties link
                    Panel groupPropertiesLinkContainer = (Panel)this.FindControl("GroupPropertiesLinkContainer");

                    if (groupPropertiesLinkContainer != null)
                    { groupPropertiesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.AutoJoinRules:
                    // select the auto-join rules link
                    Panel autoJoinRulesLinkContainer = (Panel)this.FindControl("AutoJoinRulesLinkContainer");

                    if (autoJoinRulesLinkContainer != null)
                    { autoJoinRulesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Enrollments:
                    // select the enrollments link
                    Panel enrollmentsLinkContainer = (Panel)this.FindControl("EnrollmentsLinkContainer");

                    if (enrollmentsLinkContainer != null)
                    { enrollmentsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.PrivateCatalogAccess:
                    // select the private catalog access link
                    Panel privateCatalogAccessLinkContainer = (Panel)this.FindControl("PrivateCatalogAccessLinkContainer");

                    if (privateCatalogAccessLinkContainer != null)
                    { privateCatalogAccessLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Roles:
                    // select the roles link
                    Panel rolesLinkContainer = (Panel)this.FindControl("RolesLinkContainer");

                    if (rolesLinkContainer != null)
                    { rolesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Wall:
                    // select the wall link
                    Panel wallLinkContainer = (Panel)this.FindControl("WallLinkContainer");

                    if (wallLinkContainer != null)
                    { wallLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Documents:
                    // select the documents link
                    Panel documentsLinkContainer = (Panel)this.FindControl("DocumentsLinkContainer");

                    if (documentsLinkContainer != null)
                    { documentsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
            }
        }
        #endregion
        #endregion
    }
}
