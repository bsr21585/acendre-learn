﻿//Method to add supervisors from modal popup to supervisor listing container
function AddSupervisorsToUser() {
    var userSupervisorsContainer = $("#UserSupervisorsContainer");
    var selectedUsers = $('select#SelectEligibleSupervisorsListBox').val();

    if (selectedUsers != null) {
        for (var i = 0; i < selectedUsers.length; i++) {
            if (!$("#Supervisor_" + selectedUsers[i]).length) {
                // add the selected user to the course expert list container
                var itemContainerDiv = $("<div id=\"Supervisor_" + selectedUsers[i] + "\">" + "<img class='SmallIcon' onclick=\"javascript:RemoveSupervisorFromUser('" + selectedUsers[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" />" + $("#SelectEligibleSupervisorsListBox option[value='" + selectedUsers[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(userSupervisorsContainer);
            }

            // remove the user from the select list
            $("#SelectEligibleSupervisorsListBox option[value='" + selectedUsers[i] + "']").remove();
        }
    }
}

//Method to remove supervisor from modal popup 
function RemoveSupervisorFromUser(supervisorId) {
    $("#Supervisor_" + supervisorId).remove();
}

//Gets the selected list of supervisors from the modal popup
function GetSelectedSupervisorsHiddenField() {
    var userSupervisorsContainer = $("#UserSupervisorsContainer");
    var selectedSupervisorsField = $("#SelectedUserSupervisorsHidden");
    var selectedSupervisors = "";

    userSupervisorsContainer.children().each(function () {
        selectedSupervisors = selectedSupervisors + $(this).prop("id").replace("Supervisor_", "") + ",";
    });

    if (selectedSupervisors.length > 0)
    { selectedSupervisors = selectedSupervisors.substring(0, selectedSupervisors.length - 1); }

    selectedSupervisorsField.val(selectedSupervisors);
}

// Launch Files Upload Modal for modifying file
function LoadFilesUploadModalContent(idDocumentRepositoryItem) {
    $("#IdDocumentRepositoryItemHiddenField").val(idDocumentRepositoryItem);
    
    $("#ModifyFilesModalModalPopupFeedbackContainer").empty();

    $("#ModifyFilesModalHiddenLaunchButton").click();

    $("#ModifyFilesModalHiddenLoadButton").click();

}

// Clears the upload modal feedback panel
function ClearUploadModalFeedback() {
    $("#UploadFilesModalModalPopupFeedbackContainer").empty();
}

// Clears the user's avatar
function DeleteUserAvatar() {
    $("#UserModify_avatar_ImageContainer").remove();
    $("#ClearAvatar_Field").val("true");
}