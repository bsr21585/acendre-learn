﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;  // This should use the Library of the namespace it's under, i.e. UMS, LMS, LRS.
                            // Remove this comment upon implementation.

namespace Asentia.UMS.Pages.__Template
{
    // This should inherit AsentiaPage or AsentiaAuthenticatedPage depending on page type.
    // Remove this comment upon implementation.
    public class _Template : AsentiaAuthenticatedPage
    {
        #region Properties
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink("REPLACE WITH TITLE FROM RESX", "REPLACE WITH LINK IF NECESSARY"));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, "REPLACE WITH TITLE FROM RESX", ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY,
                                                                                                                    ImageFiles.EXT_PNG));
        }
        #endregion
    }
}
