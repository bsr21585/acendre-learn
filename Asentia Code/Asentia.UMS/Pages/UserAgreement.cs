﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages
{
    public class UserAgreement : AsentiaAuthenticatedPage
    {
            #region Properties
            public Panel UserFormContentWrapperContainer;
            public Panel PageInstructionsPanel;
            public Panel ExecuteUserAgreementFormWrapperContainer;
            public Panel ExecuteUserAgreementFormContainer;
            public Panel ActionsPanel;

            public LoginForm LoginFormControl;
            public ModalPopup LoginFormModal;
            #endregion

            #region Private Properties
            private User _UserObject;
            private UserAccountData _UserAccountDataObject;
            private CheckBox _ExecuteUserAgreementCheckBox;
            private HtmlGenericControl _UserAgreementHtml;
            private Button _SaveButton;
            private Button _CancelButton;

            #endregion

            #region Page_Load
            /// <summary>
            /// Page_Load event
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            public void Page_Load(object sender, EventArgs e)
            {
                // bounce if the caller does not need to execute user agreement
                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERAGREEMENT_REQUIRED) == false)
                { Response.Redirect("~/dashboard/"); }

                // build the page controls
                this._BuildControls();
            }
            #endregion

            #region _BuildBreadcrumbAndPageTitle
            /// <summary>
            /// Builds the breadcrumb and page title.
            /// </summary>
            private void _BuildBreadcrumbAndPageTitle()
            {
                // evaluate for breadcrumb and page title information
                string breadCrumbPageTitle;
                string userImagePath;
                string imageCssClass = null;
                string pageTitle;

                breadCrumbPageTitle = _GlobalResources.LicenseAgreement;

                this._UserObject = new User(AsentiaSessionState.IdSiteUser);

                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }

                pageTitle = _GlobalResources.UserAgreement;

                // build the breadcrumb
                ArrayList breadCrumbLinks = new ArrayList();
                breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
                breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
                breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
                this.BuildBreadcrumb(breadCrumbLinks);

                // build the page title
                this.BuildPageTitle(PageCategoryForTitle.System, pageTitle, userImagePath, imageCssClass);
            }
            #endregion

            #region _BuildControls
            /// <summary>
            /// Builds the controls for the page.
            /// </summary>
            private void _BuildControls()
            {
                // build the breadcrumb and page title
                this._BuildBreadcrumbAndPageTitle();

                // format a page information panel with page instructions
                this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.YouMustAgreeToTheFollowingLicenseAgreementBeforeYouMayProceed, true);

                // build user agreement form
                this._BuildUserAgreementForm();

                // build the actions panel
                this._BuildActionsPanel();
            }
            #endregion

            #region _BuildUserAgreementForm
            /// <summary>
            /// Builds the user agreement form.
            /// </summary>
            private void _BuildUserAgreementForm()
            {
                this.ExecuteUserAgreementFormContainer.Controls.Clear();

                // user object
                this._UserAccountDataObject = new UserAccountData(UserAccountDataFileType.Site, true, false);
                
                this._UserAgreementHtml = new HtmlGenericControl();
                this._UserAgreementHtml.ID = "UserAgreementHtml_Field";
                this._UserAgreementHtml.InnerHtml = this._UserAccountDataObject.GetUserAgreementHTMLInLanguage(AsentiaSessionState.UserCulture);

                // user agreement label
                Panel userAgreementLabelContainer = new Panel();
                userAgreementLabelContainer.ID = this.ID + "_UserAgreementLabelContainer";
                userAgreementLabelContainer.CssClass = "FormFieldLabelContainer";

                // add asterisk if the user agreement is required
                if (!String.IsNullOrWhiteSpace(this._UserAgreementHtml.InnerHtml) && AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERAGREEMENT_REQUIRED) == true)
                {
                    Label requiredAsterisk = new Label();
                    requiredAsterisk.Text = " * ";
                    requiredAsterisk.CssClass = "RequiredAsterisk";
                    userAgreementLabelContainer.Controls.Add(requiredAsterisk);
                }

                this.ExecuteUserAgreementFormContainer.Controls.Add(AsentiaPage.BuildFormField("UserAgreementHtml",
                                                        null,
                                                        this._UserAgreementHtml.ID,
                                                        this._UserAgreementHtml,
                                                        false,
                                                        false,
                                                        false));

                this._ExecuteUserAgreementCheckBox = new CheckBox();
                this._ExecuteUserAgreementCheckBox.ID = "ExecuteUserAgreementCheckBox_Field";
                this._ExecuteUserAgreementCheckBox.Text = _GlobalResources.IAcceptTheAboveTermsAndConditions;

                this.ExecuteUserAgreementFormContainer.Controls.Add(AsentiaPage.BuildFormField("ExecuteUserAgreementCheckBox",
                                                        null,
                                                        this._ExecuteUserAgreementCheckBox.ID,
                                                        this._ExecuteUserAgreementCheckBox,
                                                        true,
                                                        true,
                                                        false));
            }
            #endregion

            #region _ValidateExecuteUserAgreementForm
            /// <summary>
            /// Validates the form.
            /// </summary>
            /// <returns>true/false</returns>
            private bool _ValidateExecuteUserAgreementForm()
            {
                bool isValid = true;

                // validate required
                if (!this._ExecuteUserAgreementCheckBox.Checked)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.ExecuteUserAgreementFormContainer, "ExecuteUserAgreementCheckBox", _GlobalResources.PleaseCheckTheBoxToAgreeToTheTermsOfTheUserAgreement);
                }

                return isValid;
            }
            #endregion


            #region _BuildActionsPanel
            /// <summary>
            /// Builds the container and buttons for form actions.
            /// </summary>
            private void _BuildActionsPanel()
            {
                this.ActionsPanel.Controls.Clear();

                // style actions panel
                this.ActionsPanel.CssClass = "ActionsPanel";

                // save button
                this._SaveButton = new Button();
                this._SaveButton.ID = "SaveButton";
                this._SaveButton.CssClass = "Button ActionButton";
                this._SaveButton.Text = _GlobalResources.Accept;
                this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
                this.ActionsPanel.Controls.Add(this._SaveButton);

                // cancel button
                this._CancelButton = new Button();
                this._CancelButton.ID = "CancelButton";
                this._CancelButton.CssClass = "Button NonActionButton";
                this._CancelButton.Text = _GlobalResources.DoNotAccept;
                this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
                this.ActionsPanel.Controls.Add(this._CancelButton);
            }
            #endregion

            #region _SaveButton_Command
            /// <summary>
            /// Handles the "Save Changes" button click.
            /// </summary>
            /// <param name="sender">sender</param>
            /// <param name="e">event args</param>
            private void _SaveButton_Command(object sender, CommandEventArgs e)
            {
                try
                {
                    // validate the form
                    if (!this._ValidateExecuteUserAgreementForm())
                    { throw new AsentiaException(); }

                    // create a user object
                    this._UserObject.SaveUserAgreementAgreed(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSiteUser);
                    AsentiaSessionState.MustExecuteUserAgreement = false;

                    // disable the "execute license aggreement" button
                    this._SaveButton.Enabled = false;

                    // display the saved feedback and register script to redirect on click of feedback ok button
                    this.DisplayFeedback(_GlobalResources.YouHaveAgreedToTheUserAgreementPleaseClickOKToContinue, false);
                    ScriptManager.RegisterStartupScript(this.ExecuteUserAgreementFormContainer, typeof(UserAgreement), "RedirectToDashboard", "$(\"#FeedbackInformationStatusPanelCloseButton\").click(function () { window.location.href = \"/dashboard\"; });", true);
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    // display the failure message
                    this.DisplayFeedback(dnfEx.Message, true);
                }
                catch (DatabaseFieldNotUniqueException fnuEx)
                {
                    // display the failure message
                    this.DisplayFeedback(fnuEx.Message, true);
                }
                catch (DatabaseCallerPermissionException cpeEx)
                {
                    // display the failure message
                    this.DisplayFeedback(cpeEx.Message, true);
                }
                catch (DatabaseException dEx)
                {
                    // display the failure message
                    this.DisplayFeedback(dEx.Message, true);
                }
                catch (AsentiaException ex)
                {
                    // display the failure message
                    this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                }
            }
            #endregion

            #region _CancelButton_Command
            /// <summary>
            /// Handles the "Cancel" button click.
            /// </summary>
            /// <param name="sender">sender</param>
            /// <param name="e">event args</param>
            private void _CancelButton_Command(object sender, CommandEventArgs e)
            {
                //kill user sessions
                AsentiaAuthenticatedPage.UpdateUserSessionExpiration(null);
                AsentiaSessionState.EndSession(true);
                Response.Redirect("~/");
            }
            #endregion
    }
}
