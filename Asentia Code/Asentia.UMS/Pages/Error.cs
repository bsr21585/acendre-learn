﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages
{
    public class Error : AsentiaPage
    {
        #region Properties
        public Panel ErrorDetailsContainer;
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // sanity check, make sure there is actually an error
            if (!HttpContext.Current.Items.Contains("IdExceptionLog") &&
                !HttpContext.Current.Items.Contains("RequestedPage") &&
                !HttpContext.Current.Items.Contains("Exception"))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("page-specific/Error.css");            

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.Error, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_ERROR_RED, ImageFiles.EXT_PNG));
            
            string idExceptionLog = HttpContext.Current.Items["IdExceptionLog"].ToString();
            string requestedPage = HttpContext.Current.Items["RequestedPage"].ToString();
            Exception ex = (Exception)HttpContext.Current.Items["Exception"];

            // build the container panels
            Panel errorInstructionsContainer = new Panel();
            errorInstructionsContainer.ID = "ErrorInstructionsContainer";
            errorInstructionsContainer.CssClass = "ErrorDetailsItemContainer";
            this.ErrorDetailsContainer.Controls.Add(errorInstructionsContainer);

            Panel errorIdentifierContainer = new Panel();
            errorIdentifierContainer.ID = "ErrorIdentifierContainer";
            errorIdentifierContainer.CssClass = "ErrorDetailsItemContainer";
            this.ErrorDetailsContainer.Controls.Add(errorIdentifierContainer);

            Panel errorPageContainer = new Panel();
            errorPageContainer.ID = "ErrorPageContainer";
            errorPageContainer.CssClass = "ErrorDetailsItemContainer";
            this.ErrorDetailsContainer.Controls.Add(errorPageContainer);

            Panel errorMessageContainer = new Panel();
            errorMessageContainer.ID = "ErrorMessageContainer";
            errorMessageContainer.CssClass = "ErrorDetailsItemContainer";
            this.ErrorDetailsContainer.Controls.Add(errorMessageContainer);            

            // error instructions
            Literal errorInstructions = new Literal();
            errorInstructions.Text = _GlobalResources.PleaseContactAnAdministratorWithTheFollowingDetails;
            errorInstructionsContainer.Controls.Add(errorInstructions);

            // error identifier
            Panel errorIdentifierLabelContainer = new Panel();
            errorIdentifierLabelContainer.ID = "ErrorIdentifierLabelContainer";
            errorIdentifierLabelContainer.CssClass = "ErrorDetailsItemLabel";
            errorIdentifierContainer.Controls.Add(errorIdentifierLabelContainer);

            Literal errorIdentifierLabel = new Literal();
            errorIdentifierLabel.Text = _GlobalResources.Identifier + ":";
            errorIdentifierLabelContainer.Controls.Add(errorIdentifierLabel);

            Panel errorIdentifierValueContainer = new Panel();
            errorIdentifierValueContainer.ID = "ErrorIdentifierValueContainer";
            errorIdentifierValueContainer.CssClass = "ErrorDetailsItemValue";
            errorIdentifierContainer.Controls.Add(errorIdentifierValueContainer);

            Literal errorIdentifierValue = new Literal();
            errorIdentifierValue.Text = idExceptionLog;
            errorIdentifierValueContainer.Controls.Add(errorIdentifierValue);

            // error page
            Panel errorPageLabelContainer = new Panel();
            errorPageLabelContainer.ID = "ErrorPageLabelContainer";
            errorPageLabelContainer.CssClass = "ErrorDetailsItemLabel";
            errorPageContainer.Controls.Add(errorPageLabelContainer);

            Literal errorPageLabel = new Literal();
            errorPageLabel.Text = _GlobalResources.Page + ":";
            errorPageLabelContainer.Controls.Add(errorPageLabel);

            Panel errorPageValueContainer = new Panel();
            errorPageValueContainer.ID = "ErrorPageValueContainer";
            errorPageValueContainer.CssClass = "ErrorDetailsItemValue";
            errorPageContainer.Controls.Add(errorPageValueContainer);

            Literal errorPageValue = new Literal();
            errorPageValue.Text = requestedPage;
            errorPageValueContainer.Controls.Add(errorPageValue);

            // error message
            Panel errorMessageLabelContainer = new Panel();
            errorMessageLabelContainer.ID = "ErrorMessageLabelContainer";
            errorMessageLabelContainer.CssClass = "ErrorDetailsItemLabel";
            errorMessageContainer.Controls.Add(errorMessageLabelContainer);

            Literal errorMessageLabel = new Literal();
            errorMessageLabel.Text = _GlobalResources.Message + ":";
            errorMessageLabelContainer.Controls.Add(errorMessageLabel);

            Panel errorMessageValueContainer = new Panel();
            errorMessageValueContainer.ID = "ErrorMessageValueContainer";
            errorMessageValueContainer.CssClass = "ErrorDetailsItemValue";
            errorMessageContainer.Controls.Add(errorMessageValueContainer);

            Literal errorMessageValue = new Literal();
            errorMessageValue.Text = ex.Message;
            errorMessageValueContainer.Controls.Add(errorMessageValue);
        }
        #endregion
    }
}
