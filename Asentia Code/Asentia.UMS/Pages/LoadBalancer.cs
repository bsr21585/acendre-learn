﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.UMS.Pages
{
    public class LoadBalancer : Page
    {
        #region Properties
        public Literal CurrentUtcTimestamp;        
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get and display the current utc timestamp
            this.CurrentUtcTimestamp.Text = AsentiaSessionState.UtcNow.ToString("o");
        }
        #endregion
    }
}
