﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages.Offline
{
    public class Default : AsentiaPage
    {
        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/offline/Default.css");

            // redirect to home page if site is active
            if (AsentiaSessionState.GlobalSiteObject.Id > 1 
                && AsentiaSessionState.GlobalSiteObject.IsActive 
                && (AsentiaSessionState.GlobalSiteObject.Expires == null || AsentiaSessionState.GlobalSiteObject.Expires >= AsentiaSessionState.UtcNow))
            { Response.Redirect("/"); }

            this.DisplayFeedback(_GlobalResources.ThisPortalIsCurrentlyOffline, true, false);
        }
        #endregion
    }
}