﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using System.Collections.Generic;

namespace Asentia.UMS.Pages.Administrator.System.API
{
    /// <summary>
    /// Class to manage API setting
    /// </summary>
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel APIFormContentWrapperContainer;
        public Panel APIWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel ApiFormContainer;
        public Panel ActionsPanel;
        #endregion

        #region PrivateProperties
        private RadioButtonList _ApiAccessOnOff;
        private TextBox _AuthenticationKey;
        private CheckBox _RequireHttps;
        private Button _SaveApi;
        private Button _CancelApi;
        private Label _HttpUrlAccessOff;
        private Label _HttpUrlAccessOn;
        private Label _HttpsUrlAccessOn;
        #endregion

        #region Page_Load
        /// <summary>
        /// Handles the Page Load Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_API))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.API));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.API, ImageFiles.GetIconPath(ImageFiles.ICON_API,
                                                                                                          ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.APIFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.APIWrapperContainer.CssClass = "FormContentContainer";

            // setup page level controls
            this._LoadPageControls();
            this._BuildActionsPanel();

            // if not postback
            if (!IsPostBack)
            {
                // load data
                _LoadData();
            }
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.UMS.Pages.Administrator.System.API.Default.js");

            // register start up action
            StringBuilder jsActions = new StringBuilder();

            jsActions.AppendLine("ToggleApiAccess();");
            jsActions.AppendLine("Sys.WebForms.PageRequestManager.getInstance().add_endRequest(ToggleApiAccess);");
            
            csm.RegisterStartupScript(typeof(Default), "JsActions", jsActions.ToString(), true);
        }
        #endregion

        #region LoadPageControls
        /// <summary>
        /// Loads the Tags/Controls on the page
        /// </summary>
        private void _LoadPageControls()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.ConfigureAccessToAPIUsingFormBelow, true);

            #region Api access feild container
            this._ApiAccessOnOff = new RadioButtonList();
            this._ApiAccessOnOff.ID = "ApiAccessOnOff";

            ListItem apiAccessOn = new ListItem();
            apiAccessOn.Text = _GlobalResources.On;
            apiAccessOn.Value = "1";
            apiAccessOn.Attributes.Add("onclick", "ToggleApiAccess();");
            this._ApiAccessOnOff.Items.Add(apiAccessOn);

            ListItem apiAccessOff = new ListItem();
            apiAccessOff.Text = _GlobalResources.Off;
            apiAccessOff.Value = "2";
            apiAccessOff.Attributes.Add("onclick", "ToggleApiAccess();");
            this._ApiAccessOnOff.Items.Add(apiAccessOff);

            this.ApiFormContainer.Controls.Add(AsentiaPage.BuildFormField("ApiAccess",
                                                            _GlobalResources.APIAccess,
                                                            this._ApiAccessOnOff.ID,
                                                            this._ApiAccessOnOff,
                                                            true,
                                                            true,
                                                            false));
            #endregion

            #region Authentication key container
            List<Control> authenticationKeyInputControls = new List<Control>();

            this._AuthenticationKey = new TextBox();
            this._AuthenticationKey.ID = "AuthenticationKey";
            this._AuthenticationKey.MaxLength = 255;
            authenticationKeyInputControls.Add(this._AuthenticationKey);

            Label minCharacterLabel = new Label();
            minCharacterLabel.Text = _GlobalResources.TenCharactersMinimum;
            authenticationKeyInputControls.Add(minCharacterLabel);

            this.ApiFormContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ApiKey",
                                                                                     _GlobalResources.AuthenticationKey,
                                                                                     authenticationKeyInputControls,
                                                                                     true,
                                                                                     true));

                
            //Enabling the control as after the save click it 
            //get disabled after adding to main container
            this._AuthenticationKey.Enabled = true;
            #endregion

            #region Reqired http container
            Panel httpContainer = new Panel();
            httpContainer.CssClass = "FormFieldContainer";

            this._RequireHttps = new CheckBox();
            this._RequireHttps.ID = "RequireHttps";
            this._RequireHttps.Text = _GlobalResources.WhenCheckedOnlyAPIRequestsMadeOverSSlWillBeAccepted;
            this._RequireHttps.Attributes.Add("onClick", "ToggleHttps();");

            this.ApiFormContainer.Controls.Add(AsentiaPage.BuildFormField("Http",
                                                            _GlobalResources.RequireHTTPS,
                                                            this._RequireHttps.ID,
                                                            this._RequireHttps,
                                                            false,
                                                            true,
                                                            false));

            //Enabling the control as after the save click it 
            //get disabled after adding to main container
            this._RequireHttps.Enabled = true;

            #endregion

            #region Url location container
            List<Control> httpUrlLabelControls = new List<Control>();

            Label httpUrlTextLabel = new Label();
            httpUrlTextLabel.ID = "HttpUrlTextLabel";
            httpUrlTextLabel.Text = _GlobalResources.APIRequestsShouldBeDirectedToThisLocation + ":";
            httpUrlLabelControls.Add(httpUrlTextLabel);

            this._HttpUrlAccessOn = new Label();
            this._HttpUrlAccessOn.ID = "HttpUrl_AccessOn";
            this._HttpUrlAccessOn.CssClass = "Urllink";
            string httpUrl = "http://" + AsentiaSessionState.SiteHostname + "." + Config.AccountSettings.BaseDomain + "/_util/API/Default.asmx/[MethodName]";
            this._HttpUrlAccessOn.Text = httpUrl;

            this._HttpsUrlAccessOn = new Label();
            this._HttpsUrlAccessOn.ID = "HttpsUrl_AccessOn";
            this._HttpsUrlAccessOn.CssClass = "Urllink";
            string httpsUrl = "https://" + AsentiaSessionState.SiteHostname + "." + Config.AccountSettings.BaseDomain + "/_util/API/Default.asmx/[MethodName]";
            this._HttpsUrlAccessOn.Text = httpsUrl;

            this._HttpUrlAccessOff = new Label();
            this._HttpUrlAccessOff.ID = "HttpUrl_AccessOff";
            this._HttpUrlAccessOff.Text = "[" + _GlobalResources.Disabled + "]";

            httpUrlLabelControls.Add(_HttpUrlAccessOn);
            httpUrlLabelControls.Add(_HttpsUrlAccessOn);
            httpUrlLabelControls.Add(_HttpUrlAccessOff);

            this.ApiFormContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("HttpUrl",
                                                                                     _GlobalResources.URL,
                                                                                     httpUrlLabelControls,
                                                                                     false,
                                                                                     false));
            #endregion
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Method to initialize action panel controls
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            Panel buttonsContainer = new Panel();
            buttonsContainer.CssClass = "Buttons";
            this._SaveApi = new Button();
            this._SaveApi.CssClass = "Button ActionButton SaveButton";
            this._SaveApi.ID = "SaveApi";
            this._SaveApi.Text = _GlobalResources.SaveChanges;
            this._SaveApi.Click += new EventHandler(SaveApi_Click);

            this._CancelApi = new Button();
            this._CancelApi.ID = "CancelApi";
            this._CancelApi.CssClass = "Button NonActionButton";
            this._CancelApi.Text = _GlobalResources.Cancel;
            this._CancelApi.Click += new EventHandler(CancelApi_Click);
            buttonsContainer.Controls.Add(this._SaveApi);
            buttonsContainer.Controls.Add(this._CancelApi);

            this.ActionsPanel.Controls.Add(buttonsContainer);
        }
        #endregion

        #region LoadData
        /// <summary>
        /// Loads API settings from the database and populates the controls
        /// </summary>
        private void _LoadData()
        {
            AsentiaSite site = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite);
            this._RequireHttps.Checked = site.ParamBool(SiteParamConstants.SYSTEM_API_REQUIREHTTPS) ?? false;
            this._AuthenticationKey.Text = site.ParamString(SiteParamConstants.SYSTEM_API_KEY);

            bool isApiAccess = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_API_ENABLED);
            if (isApiAccess)
            {
                this._ApiAccessOnOff.SelectedIndex = 0;

                //Enable fields
                this._AuthenticationKey.Enabled = true;
                this._RequireHttps.Enabled = true;
            }
            else
            {
                this._ApiAccessOnOff.SelectedIndex = 1;

                //Disable fields
                this._AuthenticationKey.Enabled = false;
                this._RequireHttps.Enabled = false;
            }
            if (isApiAccess && !this._RequireHttps.Checked)
            {
                this._HttpUrlAccessOn.Attributes.Add("style", "display: block;");
                this._HttpUrlAccessOff.Attributes.Add("style", "display: none;");
                this._HttpsUrlAccessOn.Attributes.Add("style", "display: none;");
            }
            else if (isApiAccess && this._RequireHttps.Checked)
            {
                this._HttpUrlAccessOn.Attributes.Add("style", "display: none;");
                this._HttpUrlAccessOff.Attributes.Add("style", "display: none;");
                this._HttpsUrlAccessOn.Attributes.Add("style", "display: block;");
            }
            else if (!isApiAccess)
            {
                this._HttpUrlAccessOn.Attributes.Add("style", "display: none;");
                this._HttpUrlAccessOff.Attributes.Add("style", "display: block;");
                this._HttpsUrlAccessOn.Attributes.Add("style", "display: none;");
            }
        }
        #endregion

        #region Controls Events

        #region CancelApi_Click
        /// <summary>
        /// Performs the cancel button action on the page.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        protected void CancelApi_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dashboard");
        }
        #endregion

        #region SaveApi_Click
        /// <summary>
        /// Handles Save Button Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void SaveApi_Click(object sender, EventArgs e)
        {
            // check if provided information is complete and valid
            if (this._ValidateForm())
            {
                try
                {
                    // create datable to save site parameters
                    DataTable siteParams = new DataTable();
                    siteParams.Columns.Add("key");
                    siteParams.Columns.Add("value");

                    bool apiAccess = false;
                    if (Convert.ToInt32(this._ApiAccessOnOff.SelectedItem.Value) == 1)
                    {
                        apiAccess = true;
                    }

                    String authKey = String.Empty;
                    if (Convert.ToInt32(this._ApiAccessOnOff.SelectedItem.Value) == 1)
                    {
                        authKey = this._AuthenticationKey.Text;
                    }

                    bool requireHttps = false;
                    if ((Convert.ToInt32(this._ApiAccessOnOff.SelectedItem.Value) == 1 && this._RequireHttps.Checked))
                    {
                        requireHttps = true;
                    }

                    DataRow rowApiAccess = siteParams.NewRow();
                    rowApiAccess["key"] = SiteParamConstants.SYSTEM_API_ENABLED;
                    rowApiAccess["value"] = apiAccess;
                    siteParams.Rows.Add(rowApiAccess);

                    DataRow rowAuthKey = siteParams.NewRow();
                    rowAuthKey["key"] = SiteParamConstants.SYSTEM_API_KEY;
                    rowAuthKey["value"] = authKey;
                    siteParams.Rows.Add(rowAuthKey);

                    DataRow rowRequireHttps = siteParams.NewRow();
                    rowRequireHttps["key"] = SiteParamConstants.SYSTEM_API_REQUIREHTTPS;
                    rowRequireHttps["value"] = Convert.ToString(requireHttps);
                    siteParams.Rows.Add(rowRequireHttps);

                    // save site parameters
                    AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, siteParams);

                    if ((Convert.ToInt32(this._ApiAccessOnOff.SelectedItem.Value) == 2))
                    {
                        this._AuthenticationKey.Enabled = false;
                        this._RequireHttps.Enabled = false;
                    }
                    //this._LoadData();

                    // display success message to user                    
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.APISettingsHaveBeenSavedSuccessfully, false);
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    // display the failure message
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dnfEx.Message, true);
                }
                catch (DatabaseFieldNotUniqueException fnuEx)
                {
                    // display the failure message
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, fnuEx.Message, true);
                }
                catch (DatabaseCallerPermissionException cpeEx)
                {
                    // display the failure message
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, cpeEx.Message, true);
                }
                catch (DatabaseException dEx)
                {
                    // display the failure message
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dEx.Message, true);
                }
                catch (AsentiaException ex)
                {
                    // display the failure message
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                }
                catch (Exception ex)
                {
                    // display failure message to user
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, ex.Message, true);
                }
            }
            else
            {
                // display error message to user
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;

            // api key field
            if (Convert.ToInt32(this._ApiAccessOnOff.SelectedItem.Value) == 1)
            {
                if (String.IsNullOrWhiteSpace(this._AuthenticationKey.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.ApiFormContainer, "ApiKey", _GlobalResources.AuthenticationKey + " " + _GlobalResources.IsRequired);
                }
                else
                {
                    if (this._AuthenticationKey.Text.Length < 10)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.ApiFormContainer, "ApiKey", _GlobalResources.AuthenticationKeyMustBeAtLeast10Characters);
                    }
                }
            }

            return isValid;
        }
        #endregion
    }
}
