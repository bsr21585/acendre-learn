﻿//used for user and group snap shot datset
function ToggleApiAccess() {
    if ($("#ApiAccessOnOff_1").is(":checked")) { // OFF
        $("#AuthenticationKey").val("");
        $("#AuthenticationKey").attr("disabled", "disabled");
        $("#RequireHttps").attr('checked', false);
        $("#RequireHttps").attr("disabled", true);
        $("#HttpUrl_AccessOff").css("display", "block");
        $("#HttpUrl_AccessOn").css("display", "none");
        $("#HttpsUrl_AccessOn").css("display", "none");
    }
    else { // ON
        $("#AuthenticationKey").removeAttr("disabled");
        $("#RequireHttps").removeAttr("disabled");
        $("#HttpUrl_AccessOff").css("display", "none");

        if ($("#RequireHttps").is(":checked")) {
            $("#HttpUrl_AccessOn").css("display", "none");
            $("#HttpsUrl_AccessOn").css("display", "block");
        }
        else {
            $("#HttpUrl_AccessOn").css("display", "block");
            $("#HttpsUrl_AccessOn").css("display", "none");
        }
    }
}

function ToggleHttps() {
    $("#HttpUrl_AccessOff").css("display", "none");

    if ($("#RequireHttps").is(":checked")) {
        $("#HttpUrl_AccessOn").css("display", "none");
        $("#HttpsUrl_AccessOn").css("display", "block");
    }
    else {
        $("#HttpUrl_AccessOn").css("display", "block");
        $("#HttpsUrl_AccessOn").css("display", "none");
    }
}