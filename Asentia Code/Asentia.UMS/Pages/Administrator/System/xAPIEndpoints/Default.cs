﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.UMS.Pages.Administrator.System.xAPIEndpoints
{
    /// <summary>
    /// class that is used to9 generate the xAPIEndpoing Keys.
    /// </summary>
    public class Default : AsentiaAuthenticatedPage
    {
        #region Public Properties
        public Panel xAPIEndpointsFormContentWrapperContainer;
        public Panel xAPIEndpointsWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel xAPIEndpointUpdatePanel;
        public Grid xAPIEndpointGrid;
        public Panel GridActionsPanel;
        
        public ModalPopup GridConfirmAction;
        public LinkButton DeleteButton = new LinkButton();
        public Panel xApiFormContainer = new Panel();
        #endregion

        #region Private Properties
        private xAPIEndpoint _xAPIEndpointObject;

        private LinkButton _GenerateKeyButton;
        private LinkButton _GenerateKeyHiddenButton;
        private LinkButton _ModifyKeyModalTargetControlButton;
        private LinkButton _ModifyKeyHiddenLink;
        private LinkButton _GenerateKeyHiddenLink;

        private ModalPopup _GenerateKeyModal;
        private ModalPopup _ModifyKeyModal;

        private TextBox _TokenLabel;
        private TextBox _TokenLabelGenerate;

        private CheckBox _TokenIsActive;

        private Localize _KeyGenerate;
        private Localize _KeyModify;
        private Localize _SecretGenerate;
        private Localize _SecretModify;

        private Panel _ModalPropertiesContainer;
        private Panel _ModalPropertiesContainerGenerate;

        private HiddenField _KeyIdHidden;

        #endregion

        #region Page_Load
        /// <summary>
        /// Handles the Page Load Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check to ensure xAPI endpoints is enabled on the portal
            if (!AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.XAPIENDPOINTS_ENABLE) ?? false)
            { Response.Redirect("/"); }

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_xAPIEndpoints))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.xAPIEndpoints));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.xAPIEndpoints, ImageFiles.GetIconPath(ImageFiles.ICON_API,
                                                                                                                    ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.xAPIEndpointsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.xAPIEndpointsWrapperContainer.CssClass = "FormContentContainer";

            // build the grid, actions panel, and modal
            this._BuildGrid();
            this._BuildObjectOptionsPanel();
            this._BuildGridActionsPanel();
            this._BuildGenerateKeyModal();
            this._BuildGridActionsModal();
            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.xAPIEndpointGrid.BindData();
            }
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.UMS.Pages.Administrator.System.xAPIEndpoints.Default.js");

            StringBuilder globalJS = new StringBuilder();
            globalJS.AppendLine("ModifyKeyModalTargetControlButton = \"" + this._ModifyKeyModalTargetControlButton.ClientID + "\";");
            globalJS.AppendLine("ModifyKeyHiddenLink = \"" + this._ModifyKeyHiddenLink.ClientID + "\";");
            globalJS.AppendLine("ModifyKeyModal = \"" + this._ModifyKeyModal.ClientID + "\";");
            globalJS.AppendLine("KeyIdHidden = \"" + this._KeyIdHidden.ClientID + "\";");
            globalJS.AppendLine("GenerateKeyHiddenButton = \"" + this._GenerateKeyHiddenButton.ClientID + "\";");
            globalJS.AppendLine("GenerateKeyHiddenLink = \"" + this._GenerateKeyHiddenLink.ClientID + "\";");
            globalJS.AppendLine("GenerateKeyModal = \"" + this._GenerateKeyModal.ClientID + "\";");

            csm.RegisterClientScriptBlock(typeof(Default), "GlobalJS", globalJS.ToString(), true);

        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // GENERATE KEY
            this._GenerateKeyButton = new LinkButton();
            this._GenerateKeyButton.ID = "GenerateKeyButton";

            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("GenerateKeyLink",
                                                this._GenerateKeyButton,
                                                null,
                                                "OnGenerateXApiIKey(); return false;",
                                                _GlobalResources.GenerateKey,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_API, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // apply css class to container
            this.xAPIEndpointUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.xAPIEndpointGrid.StoredProcedure = Library.xAPIEndpoint.GridProcedure;
            this.xAPIEndpointGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.xAPIEndpointGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.xAPIEndpointGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.xAPIEndpointGrid.IdentifierField = "idxAPIoAuthConsumer";
            this.xAPIEndpointGrid.DefaultSortColumn = "label";

            // data key names
            this.xAPIEndpointGrid.DataKeyNames = new string[] { "idxAPIoAuthConsumer" };

            // columns
            GridColumn label = new GridColumn(_GlobalResources.Label, "Label", "Label");
            GridColumn key = new GridColumn(_GlobalResources.Key, "oAuthKey");
            GridColumn secret = new GridColumn(_GlobalResources.Secret, "oAuthSecret");
            GridColumn isActive = new GridColumn(_GlobalResources.Status, "isActive", "isActive");

            GridColumn modify = new GridColumn(_GlobalResources.Modify, "isModifyOn", true);
            modify.AddProperty(new GridColumnProperty("True", "<a class=\"CursorPointer\" href=\"javascript:void(0);\" onclick=\"OnModifyXApiIKey('##idxAPIoAuthConsumer##'); return false;\" >"
                                                                + "<img class=\"SmallIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.Modify + "\" />"
                                                                + "</a>"));

            modify.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.ModifyDisabled + "\" />"));

            // add columns to data grid
            this.xAPIEndpointGrid.AddColumn(label);
            this.xAPIEndpointGrid.AddColumn(key);
            this.xAPIEndpointGrid.AddColumn(secret);
            this.xAPIEndpointGrid.AddColumn(isActive);
            this.xAPIEndpointGrid.AddColumn(modify);
        }
        #endregion

        #region _BuildGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsPanel()
        {
            this.GridActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedKey_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.GridActionsPanel.Controls.Add(this.DeleteButton);

            this._ModifyKeyModalTargetControlButton = new LinkButton();
            this._ModifyKeyModalTargetControlButton.ClientIDMode = ClientIDMode.Static;
            this._ModifyKeyModalTargetControlButton.ID = "ModifyKeyModalTargetControlButton";
            this._ModifyKeyModalTargetControlButton.Style.Add("display", "none");
            this.GridActionsPanel.Controls.Add(this._ModifyKeyModalTargetControlButton);

            this._KeyIdHidden = new HiddenField();
            this._KeyIdHidden.ID = "KeyIdHidden";
            this._KeyIdHidden.ClientIDMode = ClientIDMode.Static;
            this.GridActionsPanel.Controls.Add(this._KeyIdHidden);

            this._BuildModifyKeyModal(this._ModifyKeyModalTargetControlButton.ID);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedKey_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            body1Wrapper.ID = "GridConfirmActionModalBody1";

            Literal body1 = new Literal();
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseKey_s;

            //add body to body container
            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.GridActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.xAPIEndpointGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.xAPIEndpointGrid.Rows[i].FindControl(this.xAPIEndpointGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.xAPIEndpoint.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedback(_GlobalResources.TheSelectedKey_sHaveBeenSuccessfullyDeleted, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            catch (Exception ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.xAPIEndpointGrid.BindData();
            }

        }
        #endregion

        #region _GenerateKey_Click
        /// <summary>
        /// Handles GenerateKey Button Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _GenerateKey_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this._ValidateModifyModal(true))
                { throw new AsentiaException(); }

                this._xAPIEndpointObject = new xAPIEndpoint();

                this._xAPIEndpointObject.Label = this._TokenLabelGenerate.Text;
                this._xAPIEndpointObject.Key = this._KeyGenerate.Text;
                this._xAPIEndpointObject.Secret = this._SecretGenerate.Text;
                this._xAPIEndpointObject.IsActive = true;

                this._xAPIEndpointObject.Save();

                // rebind the grid
                this.xAPIEndpointGrid.BindData();

                this._GenerateKeyModal.SubmitButton.Enabled = false;
                this._GenerateKeyModal.SubmitButton.CssClass = "DisabledButton";
                this._GenerateKeyModal.DisplayFeedback(_GlobalResources.KeySuccessfullyGenerated, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._GenerateKeyModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._GenerateKeyModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._GenerateKeyModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._GenerateKeyModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException)
            {
                // display the failure message
                this._GenerateKeyModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            catch (Exception)
            {
                // display the failure message
                this._GenerateKeyModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _BuildGenerateKeyModal
        /// <summary>
        /// build key generation modal
        /// </summary>
        private void _BuildGenerateKeyModal()
        {
            this._GenerateKeyHiddenButton = new LinkButton();
            this._GenerateKeyHiddenButton.ClientIDMode = ClientIDMode.Static;
            this._GenerateKeyHiddenButton.ID = "GenerateKeyHiddenButton";
            this._GenerateKeyHiddenButton.Style.Add("display", "none");
            this.PageContentContainer.Controls.Add(this._GenerateKeyHiddenButton);

            this._GenerateKeyModal = new ModalPopup("GenerateKeyModal");
            this._GenerateKeyModal.Type = ModalPopupType.Form;
            this._GenerateKeyModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG,
                                                                   ImageFiles.EXT_PNG);
            this._GenerateKeyModal.HeaderIconAlt = _GlobalResources.GenerateKey;
            this._GenerateKeyModal.HeaderText = _GlobalResources.GenerateKey;
            this._GenerateKeyModal.TargetControlID = this._GenerateKeyHiddenButton.ID;
            this._GenerateKeyModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._GenerateKeyModal.SubmitButtonCustomText = _GlobalResources.Save;
            this._GenerateKeyModal.ReloadPageOnClose = false;
            this._GenerateKeyModal.SubmitButton.Command += new CommandEventHandler(this._GenerateKey_Click);

            //hidden button to simulate the action on click to show the modal
            this._GenerateKeyHiddenLink = new LinkButton();
            this._GenerateKeyHiddenLink.ClientIDMode = ClientIDMode.Static;
            this._GenerateKeyHiddenLink.ID = "GenerateKeyHiddenLink";
            this._GenerateKeyHiddenLink.Style.Add("display", "none");
            this._GenerateKeyHiddenLink.Command += new CommandEventHandler(this._GenerateKeyHiddenLink_Command);
            this._GenerateKeyModal.AddControlToBody(this._GenerateKeyHiddenLink);


            // build the modal body
            // course title field
            this._ModalPropertiesContainerGenerate = new Panel();
            this._ModalPropertiesContainerGenerate.ID = "ModalPropertiesGenerate_Container";

            this._TokenLabelGenerate = new TextBox();
            this._TokenLabelGenerate.ID = "TokenLabelGenerate";
            this._TokenLabelGenerate.CssClass = "InputLong";

            //add control to panel
            this._ModalPropertiesContainerGenerate.Controls.Add(AsentiaPage.BuildFormField("TokenLabelGenerate",
                                                             _GlobalResources.Label,
                                                             this._TokenLabelGenerate.ID,
                                                             this._TokenLabelGenerate,
                                                             true,
                                                             true,
                                                             false));


            this._KeyGenerate = new Localize();
            this._KeyGenerate.ID = "KeyGenerate";

            //add control to panel
            this._ModalPropertiesContainerGenerate.Controls.Add(AsentiaPage.BuildFormField("KeyGenerate",
                                                             _GlobalResources.Key,
                                                             this._KeyGenerate.ID,
                                                             this._KeyGenerate,
                                                             false,
                                                             true,
                                                             false));


            this._SecretGenerate = new Localize();
            this._SecretGenerate.ID = "SecretGenerate";

            //add control to panel
            this._ModalPropertiesContainerGenerate.Controls.Add(AsentiaPage.BuildFormField("SecretGenerate",
                                                             _GlobalResources.Secret,
                                                             this._SecretGenerate.ID,
                                                             this._SecretGenerate,
                                                             false,
                                                             true,
                                                             false));


            //add controls to modal
            this._GenerateKeyModal.AddControlToBody(this._ModalPropertiesContainerGenerate);

            //// add modal to container
            this.PageContentContainer.Controls.Add(this._GenerateKeyModal);
        }
        #endregion

        #region _BuildModifyKeyModal
        /// <summary>
        /// Builds the modal for modifying XAPI key.
        /// </summary>
        private void _BuildModifyKeyModal(string targetControlId)
        {
            // set modal properties
            this._ModifyKeyModal = new ModalPopup("ModifyKeyModal");
            this._ModifyKeyModal.Type = ModalPopupType.Form;
            this._ModifyKeyModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_API, ImageFiles.EXT_PNG);
            this._ModifyKeyModal.HeaderIconAlt = _GlobalResources.ModifyKey;
            this._ModifyKeyModal.HeaderText = _GlobalResources.ModifyKey;
            this._ModifyKeyModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._ModifyKeyModal.SubmitButtonCustomText = _GlobalResources.SaveChanges;

            this._ModifyKeyModal.TargetControlID = targetControlId;
            this._ModifyKeyModal.SubmitButton.Command += new CommandEventHandler(this._ModifyKeyModalSubmit_Command);
            this._ModifyKeyModal.CloseButtonTextType = ModalPopupButtonText.Close;
            this._ModifyKeyModal.ReloadPageOnClose = false;

            //hidden button to simulate the action on click to show the modal
            this._ModifyKeyHiddenLink = new LinkButton();
            this._ModifyKeyHiddenLink.ClientIDMode = ClientIDMode.Static;
            this._ModifyKeyHiddenLink.ID = "ModifyKeyHiddenLink";
            this._ModifyKeyHiddenLink.Style.Add("display", "none");
            this._ModifyKeyHiddenLink.Command += new CommandEventHandler(this._ModifyKeyHiddenLink_Command);
            this._ModifyKeyModal.AddControlToBody(this._ModifyKeyHiddenLink);

            // build the modal body
            // course title field
            this._ModalPropertiesContainer = new Panel();
            this._ModalPropertiesContainer.ID = "ModalProperties_Container";

            this._TokenLabel = new TextBox();
            this._TokenLabel.ID = "TokenLabel";
            this._TokenLabel.CssClass = "InputLong";

            //add control to panel
            this._ModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("TokenLabel",
                                                             _GlobalResources.Label,
                                                             this._TokenLabel.ID,
                                                             this._TokenLabel,
                                                             true,
                                                             true,
                                                             false));


            this._TokenIsActive = new CheckBox();
            this._TokenIsActive.ID = "TokenIsActive";
            this._TokenIsActive.Text = _GlobalResources.Active;

            //add control to panel
            this._ModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("TokenIsActive",
                                                             _GlobalResources.Active,
                                                             this._TokenIsActive.ID,
                                                             this._TokenIsActive,
                                                             false,
                                                             true,
                                                             false));

            this._KeyModify = new Localize();
            this._KeyModify.ID = "Key";

            //add control to panel
            this._ModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Key",
                                                             _GlobalResources.Key,
                                                             this._KeyModify.ID,
                                                             this._KeyModify,
                                                             false,
                                                             true,
                                                             false));


            this._SecretModify = new Localize();
            this._SecretModify.ID = "Secret";

            //add control to panel
            this._ModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Secret",
                                                             _GlobalResources.Secret,
                                                             this._SecretModify.ID,
                                                             this._SecretModify,
                                                             false,
                                                             true,
                                                             false));

            //add controls to modal
            this._ModifyKeyModal.AddControlToBody(this._ModalPropertiesContainer);

            //// add modal to container
            this.PageContentContainer.Controls.Add(this._ModifyKeyModal);

        }
        #endregion

        #region _ModifyKeyModalSubmit_Command
        /// <summary>
        /// _ModifyKeyModalSubmit_Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _ModifyKeyModalSubmit_Command(object sender, EventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateModifyModal(false))
                { throw new AsentiaException(); }

                if (!String.IsNullOrWhiteSpace(this._KeyIdHidden.Value))
                { this._xAPIEndpointObject = new xAPIEndpoint(Convert.ToInt32(this._KeyIdHidden.Value)); }
                else
                {
                    this._xAPIEndpointObject = new xAPIEndpoint();
                }

                this._xAPIEndpointObject.Label = this._TokenLabel.Text;
                this._xAPIEndpointObject.IsActive = this._TokenIsActive.Checked;

                // save the xAPI Endpoint properties
                this._xAPIEndpointObject.Save();

                //re bind data grid
                this.xAPIEndpointGrid.BindData();

                this._ModifyKeyModal.DisplayFeedback(_GlobalResources.xAPIEndpointHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ModifyKeyModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ModifyKeyModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ModifyKeyModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ModifyKeyModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException)
            {
                // display the failure message
                this._ModifyKeyModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            catch (Exception)
            {
                // display the failure message
                this._ModifyKeyModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateModifyModal(bool isKeyGenerateProcess)
        {
            bool isValid = true;

            if (isKeyGenerateProcess)
            {
                // token label field
                if (String.IsNullOrWhiteSpace(this._TokenLabelGenerate.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._ModalPropertiesContainerGenerate, "TokenLabelGenerate", _GlobalResources.Label + " " + _GlobalResources.IsRequired);
                }
            }
            else
            {
                if (String.IsNullOrWhiteSpace(this._TokenLabel.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._ModalPropertiesContainer, "TokenLabel", _GlobalResources.Label + " " + _GlobalResources.IsRequired);
                }
            }

            return isValid;
        }
        #endregion

        #region _ModifyKeyHiddenLink_Command
        /// <summary>
        /// hidden link command to populate the data in the modal popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _ModifyKeyHiddenLink_Command(object sender, EventArgs e)
        {
            this._ModifyKeyModal.ClearFeedback();

            if (!String.IsNullOrWhiteSpace(this._KeyIdHidden.Value))
            {
                this._xAPIEndpointObject = new xAPIEndpoint(Convert.ToInt32(this._KeyIdHidden.Value));

                //populate modal popup input controls
                this._LoadData();
            }
        }
        #endregion

        #region _GenerateKeyHiddenLink_Command
        /// <summary>
        /// hidden link command to genrate the key
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _GenerateKeyHiddenLink_Command(object sender, EventArgs e)
        {
            this._GenerateKeyModal.ClearFeedback();
            this._TokenLabelGenerate.Text = string.Empty;
            this._KeyGenerate.Text = Guid.NewGuid().ToString();
            this._SecretGenerate.Text = Guid.NewGuid().ToString();
            this._GenerateKeyModal.SubmitButton.Enabled = true;
            this._GenerateKeyModal.SubmitButton.CssClass = "Button ActionButton";

        }
        #endregion

        #region _Load Data
        /// <summary>
        /// Method to load xAPIEndpoint data in the controls
        /// </summary>
        private void _LoadData()
        {
            if (this._xAPIEndpointObject != null)
            {
                this._TokenLabel.Text = this._xAPIEndpointObject.Label;
                this._KeyModify.Text = this._xAPIEndpointObject.Key;
                this._SecretModify.Text = this._xAPIEndpointObject.Secret;
                this._TokenIsActive.Checked = Convert.ToBoolean(this._xAPIEndpointObject.IsActive);
            }
        }
        #endregion
    }
}
