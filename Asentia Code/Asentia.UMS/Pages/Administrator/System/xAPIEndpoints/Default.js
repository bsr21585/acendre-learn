﻿function OnModifyXApiIKey(idxAPIoAuthConsumer) {

    //set idxAPIoAuthConsumer into hidden field
    $("#" + KeyIdHidden).val(idxAPIoAuthConsumer);

    //click the update hidden button to show  modal popup
    document.getElementById(ModifyKeyModalTargetControlButton).click();

    //click the update hidden button to show  modal popup
    document.getElementById(ModifyKeyHiddenLink).click();

}

function OnGenerateXApiIKey() {

    //click the update hidden button to show  modal popup
    document.getElementById(GenerateKeyHiddenButton).click();

    //click the update hidden button to show  modal popup
    document.getElementById(GenerateKeyHiddenLink).click();
}