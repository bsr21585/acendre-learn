﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.UMS.Pages.Administrator.System.TrackingCodes
{
    /// <summary>
    /// class for tracking code using google analytics
    /// </summary>
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel TrackingCodesFormContentWrapperContainer;
        public Panel TrackingCodesWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel TrackingCodesFormContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private TextBox _TrackingCode;
        private LinkButton _ClearTrackingCode;
        private Button _SaveButton;
        private Button _CancelButton;
        #endregion
                
        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_TrackingCodes))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.TrackingCodes));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.TrackingCodes, ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS,
                                                                                                                    ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.TrackingCodesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.TrackingCodesWrapperContainer.CssClass = "FormContentContainer";

            // format an page information panel with page instructions
            List<string> pageInstructionMessages = new List<string>();
            pageInstructionMessages.Add(_GlobalResources.ToBeginTrackingPasteYourTrackingCodeIntoTheFormBelow);
            pageInstructionMessages.Add(_GlobalResources.BeSureThatYouHaveConfiguredYourAnalyticsAccount_s);
            this.FormatPageInformationPanel(this.PageInstructionsPanel, pageInstructionMessages, true);

            // build the input controls
            this._BuildInputControls();

            // build action controls
            this._BuildActionsPanel();
        }
        #endregion

        #region _BuildInputControls
        /// <summary>
        /// Setup the Tags/Controls on the page
        /// </summary>
        private void _BuildInputControls()
        {
            // tracking code field
            List<Control> trackingCodeInputControls = new List<Control>();

            // instructions
            Panel instructionsPanelForTrackingCode = new Panel();
            instructionsPanelForTrackingCode.ID = "InstructionsPanelForTrackingCode";
            this.FormatFormInformationPanel(instructionsPanelForTrackingCode, _GlobalResources.EnterTheCompleteBlockOfCodeGeneratedByYourAnalyticsTool_s, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));
            trackingCodeInputControls.Add(instructionsPanelForTrackingCode);

            // field
            this._TrackingCode = new TextBox();
            this._TrackingCode.ID = "TrackingCode_Field";
            this._TrackingCode.TextMode = TextBoxMode.MultiLine;
            this._TrackingCode.Columns = 100;
            this._TrackingCode.Rows = 15;
            this._TrackingCode.Text = this.GetTrackingCode();
            trackingCodeInputControls.Add(this._TrackingCode);

            // tracking code clear button
            Panel clearIconContainer = new Panel();
            clearIconContainer.ID = "ClearTrackingCodeButtonContainer";

            HyperLink clearImageLink = new HyperLink();
            clearImageLink.CssClass = "ImageLink";
            clearImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                             ImageFiles.EXT_PNG);
            clearImageLink.NavigateUrl = "javascript:$(\"#TrackingCode_Field\").val(\"\");";
            clearIconContainer.Controls.Add(clearImageLink);

            HyperLink clearLink = new HyperLink();
            clearLink.Text = _GlobalResources.Clear;
            clearLink.NavigateUrl = "javascript:$(\"#TrackingCode_Field\").val(\"\");";
            clearIconContainer.Controls.Add(clearLink);

            trackingCodeInputControls.Add(clearIconContainer);

            this.TrackingCodesFormContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("TrackingCode",
                                                                                                 _GlobalResources.TrackingCode,
                                                                                                 trackingCodeInputControls,
                                                                                                 false,
                                                                                                 true));
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Performs the save action on the page.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        protected void _SaveButton_Command(object sender, EventArgs e)
        {
            try
            {
                // delete tracking code file if exists
                if (File.Exists(Server.MapPath(TrackingCodeFilePath)))
                { File.Delete(Server.MapPath(TrackingCodeFilePath)); }

                // if tracking codes field is not empty
                if (!(String.IsNullOrWhiteSpace(this._TrackingCode.Text)))
                {
                    // write new tracking code file
                    using (StreamWriter writer = new StreamWriter(Server.MapPath(TrackingCodeFilePath)))
                    { writer.Write(this._TrackingCode.Text); }
                }

                // show success message to user                    
                this.DisplayFeedback(_GlobalResources.TrackingCodeWasSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        protected void _CancelButton_Command(object sender, EventArgs e)
        {
            Response.Redirect("~/dashboard");
        }
        #endregion
    }
}