﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.UMS.Pages.Administrator.System.Interface
{
    public class Masthead : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel MastheadFormContentWrapperContainer;
        public Panel MastheadWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel MastheadContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private string _MastheadLogoImageName = null;
        private string _MastheadSecondaryImageName = null;
        private string _MastheadBackgroundImageName = null;
        private string _MastheadBackgroundImageColor = null;
        private string _MastheadLogoImageNameApplication = null;
        private string _MastheadIconImageNameApplication = null;
        private string _MastheadBackgroundColorApplication = null;

        private Image _MastheadHomePageLogoImage = new Image();
        private Image _MastheadApplicationLogoImage = new Image();
        private Image _MastheadApplicationIconImage = new Image();
        private Image _MastheadHomePageSecondaryImage = new Image();

        private ModalPopup _LogoImageUpload;
        private ModalPopup _LogoImageUploadApplication;
        private ModalPopup _IconImageUploadApplication;
        private ModalPopup _BackgroundImageUpload;
        private ModalPopup _SecondaryImageUpload;

        private LinkButton _LogoImageHomePageUploadButton;
        private LinkButton _LogoImageApplicationUploadButton;
        private LinkButton _IconImageApplicationUploadButton;
        private LinkButton _BackgroundImageUploadButtonHomePage;
        private LinkButton _SecondaryImageUploadButton;

        private LinkButton _LogoImageHomePageRemoveButton;
        private LinkButton _LogoImageApplicationRemoveButton;
        private LinkButton _IconImageApplicationRemoveButton;
        private LinkButton _BackgroundImageRemoveButton;
        private LinkButton _SecondaryImageRemoveButton;

        private LinkButton _BackgroundColorSelectButton;
        private LinkButton _BackgroundColorRemoveButton;
        private LinkButton _BackgroundColorSelectButtonApplication;
        private LinkButton _BackgroundColorRemoveButtonApplication;

        private UploaderAsync _LogoImageUploader;
        private UploaderAsync _LogoImageUploaderApplication;
        private UploaderAsync _IconImageUploaderApplication;
        private UploaderAsync _BackgroundImageUploader;
        private UploaderAsync _SecondaryImageUploader;

        private HiddenField _LogoImage;
        private HiddenField _SecondaryImage;
        private HiddenField _BackgroundImage;
        private HiddenField _SelectedColor;
        private HiddenField _LogoImageApplication;
        private HiddenField _IconImageApplication;
        private HiddenField _SelectedColorApplication;

        private Button _SaveButton;
        private Button _CancelButton;

        private Panel _MastheadHomePagePreviewContainer;
        private Panel _MastheadApplicationPreviewContainer;
        private Panel _MastheadHomePageFormContentContainer = new Panel();
        private Panel _MastheadApplicationFormContentContainer = new Panel();
        #endregion

        #region Page_Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_Masthead))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/system/interface/Masthead.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Masthead));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.InterfaceAndLayout, _GlobalResources.Masthead, ImageFiles.GetIconPath(ImageFiles.ICON_MASTHEAD,
                                                                                                                                    ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.MastheadFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.MastheadWrapperContainer.CssClass = "FormContentContainer";

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.ConfigureTheLookAndFeelOftheMastheadUsingTheInterfaceBelow, true);

            // initialize hidden fields
            this._InitializeHiddenFields();

            // build masthead tabs
            this._BuildMastheadTabs();

            // build masthead homepage panel
            this._BuildMastheadHomepagePanel();

            // build masthead application panel
            this._BuildMastheadApplicationPanel();

            // build logo image upload modal
            this._BuildLogoUploadModal();

            // build application logo image upload modal
            this._BuildApplicationLogoUploadModal();

            // build application logo image upload modal
            this._BuildApplicationIconUploadModal();

            // build secondary image upload modal
            this._BuildSecondaryUploadModal();

            // build background image upload modal
            this._BuildBackgroundUploadModal();

            // build the actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _BuildApplicationLogoUploadModal
        /// <summary>
        /// Build application logo upload modal popup
        /// </summary>
        private void _BuildApplicationLogoUploadModal()
        {
            // build the modal
            this._LogoImageUploadApplication = new ModalPopup("LogoImageUploadApplication");
            this._LogoImageUploadApplication.Type = ModalPopupType.Information;
            this._LogoImageUploadApplication.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD,
                                                                                     ImageFiles.EXT_PNG);
            this._LogoImageUploadApplication.HeaderIconAlt = "Upload";
            this._LogoImageUploadApplication.HeaderText = _GlobalResources.UploadLogoImage;
            this._LogoImageUploadApplication.TargetControlID = this._LogoImageApplicationUploadButton.ID;
            this._LogoImageUploadApplication.ReloadPageOnClose = false;

            // build the modal body

            // uploader
            this._LogoImageUploaderApplication = new UploaderAsync("LogoImageUploaderApplication", UploadType.MastheadImage, "LogoImageUploaderApplication_ErrorContainer");
            this._LogoImageUploaderApplication.ServerSideCompleteMethod += _UploadCompleteLogo;
            this._LogoImageUploaderApplication.ValidateSpecificExtension = false;
            this._LogoImageUploaderApplication.ResizeOnUpload = false;
            this._LogoImageUploaderApplication.ShowPreviewOnImageUpload = false;
            this._LogoImageUploaderApplication.ClientSideCompleteJSMethod = "ChangeLogoImagePreviewApplication";

            // uploader error panel
            Panel logoImageUploaderApplicationErrorContainer = new Panel();
            logoImageUploaderApplicationErrorContainer.ID = "LogoImageUploaderApplication_ErrorContainer";
            logoImageUploaderApplicationErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to body
            this._LogoImageUploadApplication.AddControlToBody(logoImageUploaderApplicationErrorContainer);
            this._LogoImageUploadApplication.AddControlToBody(this._LogoImageUploaderApplication);

            // add modal to form content container
            this._MastheadApplicationFormContentContainer.Controls.Add(this._LogoImageUploadApplication);
        }
        #endregion

        #region _BuildApplicationIconUploadModal
        /// <summary>
        /// Build application icon upload modal popup
        /// </summary>
        private void _BuildApplicationIconUploadModal()
        {
            // build the modal
            this._IconImageUploadApplication = new ModalPopup("IconImageUploadApplication");
            this._IconImageUploadApplication.Type = ModalPopupType.Information;
            this._IconImageUploadApplication.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD,
                                                                                     ImageFiles.EXT_PNG);
            this._IconImageUploadApplication.HeaderIconAlt = "Upload Icon";
            this._IconImageUploadApplication.HeaderText = _GlobalResources.UploadIconImage;
            this._IconImageUploadApplication.TargetControlID = this._IconImageApplicationUploadButton.ID;
            this._IconImageUploadApplication.ReloadPageOnClose = false;

            // build the modal body

            // uploader
            this._IconImageUploaderApplication = new UploaderAsync("IconImageUploaderApplication", UploadType.MastheadImage, "IconImageUploaderApplication_ErrorContainer");
            this._IconImageUploaderApplication.ServerSideCompleteMethod += _UploadCompleteIcon;
            this._IconImageUploaderApplication.ValidateSpecificExtension = false;
            this._IconImageUploaderApplication.ResizeOnUpload = false;
            this._IconImageUploaderApplication.ShowPreviewOnImageUpload = false;
            this._IconImageUploaderApplication.ClientSideCompleteJSMethod = "ChangeIconImagePreviewApplication";

            // uploader error panel
            Panel iconImageUploaderErrorContainer = new Panel();
            iconImageUploaderErrorContainer.ID = "IconImageUploaderApplication_ErrorContainer";
            iconImageUploaderErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to body
            this._IconImageUploadApplication.AddControlToBody(iconImageUploaderErrorContainer);
            this._IconImageUploadApplication.AddControlToBody(this._IconImageUploaderApplication);

            // add modal to form content container
            this._MastheadApplicationFormContentContainer.Controls.Add(this._IconImageUploadApplication);
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // execute the base
            base.OnPreRender(e);

            // registers the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.ColorPicker.ColorPicker.js");
            csm.RegisterClientScriptResource(typeof(Masthead), "Asentia.UMS.Pages.Administrator.System.Interface.Masthead.js");

            // embed css for ColorPicker
            string colorPickerCss = "<link href=\"" + this.ClientScript.GetWebResourceUrl(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.ColorPicker.ColorPicker.css") + "\" type=\"text/css\" rel=\"stylesheet\" />";
            csm.RegisterClientScriptBlock(typeof(Asentia.Controls.ClientScript), "ColorPickerCss", colorPickerCss, false);

            // attach the color picker JS as a startup script        
            StringBuilder attachColorPicker = new StringBuilder();
            attachColorPicker.AppendLine("Sys.Application.add_load(");
            attachColorPicker.AppendLine("function() { ");
            attachColorPicker.AppendLine("  $(\"#BackgroundColorSelectButtonHomePage\").ColorPicker({");
            attachColorPicker.AppendLine("          onShow: function (colpkr) {");
            attachColorPicker.AppendLine("              $(colpkr).fadeIn(500);");
            attachColorPicker.AppendLine("              return false;");
            attachColorPicker.AppendLine("          },");
            attachColorPicker.AppendLine("          onHide: function (colpkr) {");
            attachColorPicker.AppendLine("              $(colpkr).fadeOut(500);");
            attachColorPicker.AppendLine("              return false;");
            attachColorPicker.AppendLine("          },");
            attachColorPicker.AppendLine("          onChange: function (hsb, hex, rgb) {");
            attachColorPicker.AppendLine("              ChangeBackgroundColorPreview(\"#\" + hex);");
            attachColorPicker.AppendLine("          }");
            attachColorPicker.AppendLine("      });");
            attachColorPicker.AppendLine("  $(\"#BackgroundColorSelectButtonApplication\").ColorPicker({");
            attachColorPicker.AppendLine("          onShow: function (colpkr) {");
            attachColorPicker.AppendLine("              $(colpkr).fadeIn(500);");
            attachColorPicker.AppendLine("              return false;");
            attachColorPicker.AppendLine("          },");
            attachColorPicker.AppendLine("          onHide: function (colpkr) {");
            attachColorPicker.AppendLine("              $(colpkr).fadeOut(500);");
            attachColorPicker.AppendLine("              return false;");
            attachColorPicker.AppendLine("          },");
            attachColorPicker.AppendLine("          onChange: function (hsb, hex, rgb) {");
            attachColorPicker.AppendLine("              ChangeBackgroundColorPreviewApplication(\"#\" + hex);");
            attachColorPicker.AppendLine("          }");
            attachColorPicker.AppendLine("      });");
            attachColorPicker.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", attachColorPicker.ToString(), true);
        }
        #endregion

        #region _BuildMastheadTabs
        /// <summary>
        /// Build masthead tabs
        /// </summary>
        private void _BuildMastheadTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("HomePage", _GlobalResources.HomePage));
            tabs.Enqueue(new KeyValuePair<string, string>("Application", _GlobalResources.Application));            

            // build and attach the tabs
            this.MastheadContainer.Controls.Add(AsentiaPage.BuildTabListPanel("Masthead", tabs, null, this.Page, null));

            // instansiate other controls - THIS NEEDS TO BE MOVED
            this._MastheadHomePagePreviewContainer = new Panel();
            this._MastheadHomePagePreviewContainer.ID = "MastheadHomePagePreviewContainer";
            this._MastheadApplicationPreviewContainer = new Panel();
            this._MastheadApplicationPreviewContainer.ID = "MastheadApplicationPreviewContainer";
            this._MastheadApplicationPreviewContainer.CssClass = "MastheadContainer";
        }
        #endregion

        #region _BuildMastheadHomepagePanel
        /// <summary>
        /// Build masthead homepage panel
        /// </summary>
        private void _BuildMastheadHomepagePanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel homePagePanel = new Panel();
            homePagePanel.ID = "Masthead_" + "HomePage" + "_TabPanel";
            homePagePanel.CssClass = "FormContentContainer";
            homePagePanel.Attributes.Add("style", "display: block;");

            // load masthead content
            this._LoadMastheadContent();

            // build masthead preview container
            this._BuildMastheadPreviewContainer();
            homePagePanel.Controls.Add(this._MastheadHomePagePreviewContainer);

            // build masthead form container
            this._BuildMastheadHomePageFormContainer();
            homePagePanel.Controls.Add(this._MastheadHomePageFormContentContainer);

            // attach panel to container
            this.MastheadContainer.Controls.Add(homePagePanel);
        }
        #endregion

        #region _BuildMastheadApplicationPanel
        /// <summary>
        /// Build masthead application panel
        /// </summary>
        private void _BuildMastheadApplicationPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel applicationPanel = new Panel();
            applicationPanel.ID = "Masthead_" + "Application" + "_TabPanel";
            applicationPanel.CssClass = "FormContentContainer";
            applicationPanel.Attributes.Add("style", "display: none;");

            // load masthead content
            this._LoadMastheadContentApplication();

            // build masthead preview container
            this._BuildApplicationMastheadPreviewContainer();
            applicationPanel.Controls.Add(this._MastheadApplicationPreviewContainer);

            // build masthead form container
            this._BuildMastheadApplicationFormContainer();
            applicationPanel.Controls.Add(this._MastheadApplicationFormContentContainer);

            // attach panel to container
            this.MastheadContainer.Controls.Add(applicationPanel);
        }
        #endregion

        #region _InitializeHiddenFields
        /// <summary>
        /// Initialized the HiddenField elements that will store the user-selected
        /// masthead information.
        /// </summary>
        private void _InitializeHiddenFields()
        {
            this._MastheadHomePageFormContentContainer.ID = "MastheadHomePageFormContentContainer";
            // logo image field
            this._LogoImage = new HiddenField();
            this._LogoImage.ID = "LogoImageHiddenField";
            this._MastheadHomePageFormContentContainer.Controls.Add(this._LogoImage);

            // secondary image field
            this._SecondaryImage = new HiddenField();
            this._SecondaryImage.ID = "SecondaryImageHiddenField";
            this._MastheadHomePageFormContentContainer.Controls.Add(this._SecondaryImage);

            // background image field
            this._BackgroundImage = new HiddenField();
            this._BackgroundImage.ID = "BackgroundImageHiddenField";
            this._MastheadHomePageFormContentContainer.Controls.Add(this._BackgroundImage);

            // selected color field
            this._SelectedColor = new HiddenField();
            this._SelectedColor.ID = "SelectedColorHiddenField";
            this._MastheadHomePageFormContentContainer.Controls.Add(this._SelectedColor);

            #region Masthead Application Hiddenfields
            this._MastheadApplicationFormContentContainer.ID = "MastheadApplicationFormContentContainer";

            // icon image field
            this._IconImageApplication = new HiddenField();
            this._IconImageApplication.ID = "IconImageHiddenFieldApplication";
            this._MastheadApplicationFormContentContainer.Controls.Add(this._IconImageApplication);

            // logo image field
            this._LogoImageApplication = new HiddenField();
            this._LogoImageApplication.ID = "LogoImageHiddenFieldApplication";
            this._MastheadApplicationFormContentContainer.Controls.Add(this._LogoImageApplication);

            // selected color field
            this._SelectedColorApplication = new HiddenField();
            this._SelectedColorApplication.ID = "SelectedColorHiddenFieldApplication";
            this._MastheadApplicationFormContentContainer.Controls.Add(this._SelectedColorApplication);
            #endregion
        }
        #endregion

        #region _LoadMastheadContent
        /// <summary>
        /// Loads the current masthead parameters from the database.
        /// </summary>
        private void _LoadMastheadContent()
        {
            // get site params
            this._MastheadLogoImageName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_LOGO);
            this._MastheadSecondaryImageName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_SECONDARYIMAGE);
            this._MastheadBackgroundImageName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_BGIMAGE);
            this._MastheadBackgroundImageColor = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_BGCOLOR);

            // logo - Home Page
            if (!String.IsNullOrWhiteSpace(this._MastheadLogoImageName))
            {
                // set the hidden field value to the site param value
                this._LogoImage.Value = this._MastheadLogoImageName;

                // set the preview element
                if (File.Exists(MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageName)))
                { this._MastheadHomePageLogoImage.ImageUrl = SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageName; }
                else
                { this._MastheadHomePageLogoImage.ImageUrl = SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageName; }
            }


            // secondary image
            if (!String.IsNullOrWhiteSpace(this._MastheadSecondaryImageName))
            {
                // set the hidden field value to the site param value
                this._SecondaryImage.Value = this._MastheadSecondaryImageName;

                // set the preview element
                if (File.Exists(MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadSecondaryImageName)))
                { this._MastheadHomePageSecondaryImage.ImageUrl = SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadSecondaryImageName; }
                else
                { this._MastheadHomePageSecondaryImage.ImageUrl = SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadSecondaryImageName; }
            }

            // background image
            if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundImageName))
            {
                // set the hidden field value to the site param value
                this._BackgroundImage.Value = this._MastheadBackgroundImageName;

                // set the preview element
                if (File.Exists(MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadBackgroundImageName)))
                { this._MastheadHomePagePreviewContainer.Style.Add("background-image", "url('" + SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadBackgroundImageName + "')"); }
                else
                { this._MastheadHomePagePreviewContainer.Style.Add("background-image", "url('" + SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadBackgroundImageName + "')"); }

                this._MastheadHomePagePreviewContainer.Style.Add("background-repeat", "repeat-x");

            }

            // background color
            if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundImageColor))
            {
                // set the hidden field value to the site param value
                this._SelectedColor.Value = this._MastheadBackgroundImageColor;
                // set the preview element
                if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundImageName))
                { this._MastheadHomePagePreviewContainer.Style.Add("background-color", this._MastheadBackgroundImageColor); }
                else
                { this._MastheadHomePagePreviewContainer.Style.Add("background", this._MastheadBackgroundImageColor); }
            }
        }
        #endregion

        #region _LoadMastheadContentApplication
        /// <summary>
        /// Load masthead content for application
        /// </summary>
        private void _LoadMastheadContentApplication()
        {
            // get site params
            this._MastheadLogoImageNameApplication = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.APPLICATION_MASTHEAD_LOGO);
            this._MastheadIconImageNameApplication = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.APPLICATION_MASTHEAD_ICON);
            this._MastheadBackgroundColorApplication = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.APPLICATION_MASTHEAD_BGCOLOR);

            // logo - Application
            if (!String.IsNullOrWhiteSpace(this._MastheadLogoImageNameApplication))
            {
                // set the hidden field value to the site param value
                this._LogoImageApplication.Value = this._MastheadLogoImageNameApplication;

                // set the preview element
                if (File.Exists(MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageNameApplication)))
                { this._MastheadApplicationLogoImage.ImageUrl = SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageNameApplication; }
                else
                { this._MastheadApplicationLogoImage.ImageUrl = SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageNameApplication; }
            }

            // Icon - Application
            if (!String.IsNullOrWhiteSpace(this._MastheadIconImageNameApplication))
            {
                // set the hidden field value to the site param value
                this._IconImageApplication.Value = this._MastheadIconImageNameApplication;

                // set the preview element
                if (File.Exists(MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadIconImageNameApplication)))
                { this._MastheadApplicationIconImage.ImageUrl = SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadIconImageNameApplication; }
                else
                { this._MastheadApplicationIconImage.ImageUrl = SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadIconImageNameApplication; }
            }

            // set the hidden field value to the site param value
            this._SelectedColorApplication.Value = this._MastheadBackgroundColorApplication;

            // set the preview element
            if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundColorApplication))
            { this._MastheadApplicationPreviewContainer.Style.Add("background-color", this._MastheadBackgroundColorApplication + " !important"); }
            
        }
        #endregion

        #region _LoadMastheadContentFromInputs
        /// <summary>
        /// Loads the masthead parameters from inputs.
        /// </summary>
        private void _LoadMastheadContentFromInputs()
        {
            #region HOMEPAGE : get inputs
            // HOMEPAGE : get inputs
            this._MastheadLogoImageName = Path.GetFileName(this._LogoImage.Value);
            this._MastheadSecondaryImageName = Path.GetFileName(this._SecondaryImage.Value);
            this._MastheadBackgroundImageName = Path.GetFileName(this._BackgroundImage.Value);
            this._MastheadBackgroundImageColor = this._SelectedColor.Value;

            // logo
            if (!String.IsNullOrWhiteSpace(this._MastheadLogoImageName))
            {
                if (File.Exists(MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageName)))
                { this._MastheadHomePageLogoImage.ImageUrl = SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageName; }
                else
                { this._MastheadHomePageLogoImage.ImageUrl = SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageName; }
            }
            else
            { this._MastheadHomePageLogoImage.ImageUrl = ""; }

            // secondary image
            if (!String.IsNullOrWhiteSpace(this._MastheadSecondaryImageName))
            {
                if (File.Exists(MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadSecondaryImageName)))
                { this._MastheadHomePageSecondaryImage.ImageUrl = SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadSecondaryImageName; }
                else
                { this._MastheadHomePageSecondaryImage.ImageUrl = SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadSecondaryImageName; }
            }
            else
            { this._MastheadHomePageSecondaryImage.ImageUrl = ""; }

            // background image
            if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundImageName))
            {
                if (File.Exists(MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadBackgroundImageName)))
                { this._MastheadHomePagePreviewContainer.Style.Add("background-image", "url('" + SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadBackgroundImageName + "')"); }
                else
                { this._MastheadHomePagePreviewContainer.Style.Add("background-image", "url('" + SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadBackgroundImageName + "')"); }

                this._MastheadHomePagePreviewContainer.Style.Add("background-repeat", "repeat-x");
            }
            else
            {
                this._MastheadHomePagePreviewContainer.Style.Remove("background-image");
                this._MastheadHomePagePreviewContainer.Style.Remove("background-repeat");
            }

            // background color
            if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundImageColor))
            {
                if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundImageName))
                { this._MastheadHomePagePreviewContainer.Style.Add("background-color", this._MastheadBackgroundImageColor); }
                else
                { this._MastheadHomePagePreviewContainer.Style.Add("background", this._MastheadBackgroundImageColor); }
            }
            else
            {
                this._MastheadHomePagePreviewContainer.Style.Remove("background");
                this._MastheadHomePagePreviewContainer.Style.Remove("background-color");
            }
            #endregion

            #region APPLICATION : get inputs

            // APPLICATION : get inputs
            this._MastheadLogoImageNameApplication = Path.GetFileName(this._LogoImageApplication.Value);
            this._MastheadIconImageNameApplication = Path.GetFileName(this._IconImageApplication.Value);
            this._MastheadBackgroundColorApplication = this._SelectedColorApplication.Value;

            // logo
            if (!String.IsNullOrWhiteSpace(this._MastheadLogoImageNameApplication))
            {
                if (File.Exists(MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageNameApplication)))
                { this._MastheadApplicationLogoImage.ImageUrl = SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageNameApplication; }
                else
                { this._MastheadApplicationLogoImage.ImageUrl = SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadLogoImageNameApplication; }
            }
            else
            { this._MastheadApplicationLogoImage.ImageUrl = ""; }

            // icon image
            if (!String.IsNullOrWhiteSpace(this._MastheadIconImageNameApplication))
            {
                if (File.Exists(MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadIconImageNameApplication)))
                { this._MastheadApplicationIconImage.ImageUrl = SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadIconImageNameApplication; }
                else
                { this._MastheadApplicationIconImage.ImageUrl = SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + this._MastheadIconImageNameApplication; }
            }
            else
            { this._MastheadApplicationIconImage.ImageUrl = ""; }

            // background color
            if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundColorApplication))
            {
                this._MastheadApplicationPreviewContainer.Style.Add("background-color", this._MastheadBackgroundColorApplication);
                this._MastheadApplicationPreviewContainer.Style.Add("background", this._MastheadBackgroundColorApplication);
            }
            else
            {
                this._MastheadApplicationPreviewContainer.Style.Remove("background");
                this._MastheadApplicationPreviewContainer.Style.Remove("background-color");
            }

            #endregion
        }
        #endregion

        #region _SetRemoveButtonStatesAfterSave
        /// <summary>
        /// Sets the state of the "Remove" buttons. This is to be called
        /// after a successful save, so that the "Remove" button states
        /// reflect the newly saved masthead elements.
        /// </summary>
        private void _SetRemoveButtonStatesAfterSave()
        {
            #region Home Page : disable remove buttons for elements that have no value, enable them for those that do
            // disable remove buttons for elements that have no value, enable them for those that do

            // logo image remove button
            if (String.IsNullOrWhiteSpace(this._MastheadLogoImageName))
            {
                // make the button icon dim
                Image logoImageRemoveIcon = (Image)this._LogoImageHomePageRemoveButton.FindControl("LogoImageHomePageRemoveButtonIcon");

                if (logoImageRemoveIcon != null)
                {
                    logoImageRemoveIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                          ImageFiles.EXT_PNG);
                    logoImageRemoveIcon.CssClass = "MediumIcon DimIcon";
                }

                // disable the button
                this._LogoImageHomePageRemoveButton.Enabled = false;
            }
            else
            {
                // make the button icon active
                Image logoImageRemoveIcon = (Image)this._LogoImageHomePageRemoveButton.FindControl("LogoImageHomePageRemoveButtonIcon");

                if (logoImageRemoveIcon != null)
                {
                    logoImageRemoveIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                          ImageFiles.EXT_PNG);
                    logoImageRemoveIcon.CssClass = "MediumIcon";
                }

                // enable the button
                this._LogoImageHomePageRemoveButton.Enabled = true;
            }

            // background image remove button
            if (String.IsNullOrWhiteSpace(this._MastheadBackgroundImageName))
            {
                // make the button icon dim
                Image backgroundImageRemoveIcon = (Image)this._BackgroundImageRemoveButton.FindControl("BackgroundImageHomePageRemoveButtonIcon");

                if (backgroundImageRemoveIcon != null)
                {
                    backgroundImageRemoveIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                ImageFiles.EXT_PNG);
                    backgroundImageRemoveIcon.CssClass = "MediumIcon DimIcon";
                }

                // disable the button
                this._BackgroundImageRemoveButton.Enabled = false;
            }
            else
            {
                // make the button icon active
                Image backgroundImageRemoveIcon = (Image)this._BackgroundImageRemoveButton.FindControl("BackgroundImageHomePageRemoveButtonIcon");

                if (backgroundImageRemoveIcon != null)
                {
                    backgroundImageRemoveIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                ImageFiles.EXT_PNG);
                    backgroundImageRemoveIcon.CssClass = "MediumIcon";
                }

                // enable the button
                this._BackgroundImageRemoveButton.Enabled = true;
            }

            // background color remove button
            if (String.IsNullOrWhiteSpace(this._MastheadBackgroundImageColor))
            {
                // make the button icon dim
                Image backgroundColorRemoveIcon = (Image)this._BackgroundColorRemoveButton.FindControl("BackgroundColorHomePageRemoveButtonIcon");

                if (backgroundColorRemoveIcon != null)
                {
                    backgroundColorRemoveIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                ImageFiles.EXT_PNG);
                    backgroundColorRemoveIcon.CssClass = "MediumIcon DimIcon";
                }

                // disable the button
                this._BackgroundColorRemoveButton.Enabled = false;
            }
            else
            {
                // make the button icon active
                Image backgroundColorRemoveIcon = (Image)this._BackgroundColorRemoveButton.FindControl("BackgroundColorHomePageRemoveButtonIcon");

                if (backgroundColorRemoveIcon != null)
                {
                    backgroundColorRemoveIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                ImageFiles.EXT_PNG);
                    backgroundColorRemoveIcon.CssClass = "MediumIcon";
                }

                // enable the button
                this._BackgroundColorRemoveButton.Enabled = true;
            }

            // secondary image remove button
            if (String.IsNullOrWhiteSpace(this._MastheadSecondaryImageName))
            {
                // make the button icon dim
                Image secondaryImageRemoveIcon = (Image)this._SecondaryImageRemoveButton.FindControl("SecondaryImageRemoveButtonIconHomePage");

                if (secondaryImageRemoveIcon != null)
                {
                    secondaryImageRemoveIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                               ImageFiles.EXT_PNG);
                    secondaryImageRemoveIcon.CssClass = "MediumIcon DimIcon";
                }

                // disable the button
                this._SecondaryImageRemoveButton.Enabled = false;
            }
            else
            {
                // make the button icon active
                Image secondaryImageRemoveIcon = (Image)this._SecondaryImageRemoveButton.FindControl("SecondaryImageRemoveButtonIconHomePage");

                if (secondaryImageRemoveIcon != null)
                {
                    secondaryImageRemoveIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                               ImageFiles.EXT_PNG);
                    secondaryImageRemoveIcon.CssClass = "MediumIcon";
                }

                // enable the button
                this._SecondaryImageRemoveButton.Enabled = true;
            }
            #endregion
            #region Application : disable remove buttons for elements that have no value, enable them for those that do
            // disable remove buttons for elements that have no value, enable them for those that do

            // logo image remove button fro application
            if (String.IsNullOrWhiteSpace(this._MastheadLogoImageNameApplication))
            {
                // make the button icon dim
                Image logoImageRemoveIconApplication = (Image)this._LogoImageApplicationRemoveButton.FindControl("LogoImageApplicationRemoveButtonIcon");

                if (logoImageRemoveIconApplication != null)
                {
                    logoImageRemoveIconApplication.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                     ImageFiles.EXT_PNG);
                    logoImageRemoveIconApplication.CssClass = "MediumIcon DimIcon";
                }

                // disable the button
                this._LogoImageApplicationRemoveButton.Enabled = false;
            }
            else
            {
                // make the button icon active for application
                Image logoImageRemoveIconApplication = (Image)this._LogoImageHomePageRemoveButton.FindControl("LogoImageApplicationRemoveButtonIcon");

                if (logoImageRemoveIconApplication != null)
                {
                    logoImageRemoveIconApplication.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                     ImageFiles.EXT_PNG);
                    logoImageRemoveIconApplication.CssClass = "MediumIcon";
                }

                // enable the button
                this._LogoImageApplicationRemoveButton.Enabled = true;
            }

            // background color remove button for application
            if (String.IsNullOrWhiteSpace(this._MastheadBackgroundColorApplication))
            {
                // make the button icon dim
                Image backgroundColorRemoveIconApplication = (Image)this._BackgroundColorRemoveButtonApplication.FindControl("BackgroundColorApplicationRemoveButtonIcon");

                if (backgroundColorRemoveIconApplication != null)
                {
                    backgroundColorRemoveIconApplication.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                           ImageFiles.EXT_PNG);
                    backgroundColorRemoveIconApplication.CssClass = "MediumIcon DimIcon";
                }

                // disable the button
                this._BackgroundColorRemoveButtonApplication.Enabled = false;
            }
            else
            {
                // make the button icon active
                Image backgroundColorRemoveIconApplication = (Image)this._BackgroundColorRemoveButtonApplication.FindControl("BackgroundColorApplicationRemoveButtonIcon");

                if (backgroundColorRemoveIconApplication != null)
                {
                    backgroundColorRemoveIconApplication.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                           ImageFiles.EXT_PNG);
                    backgroundColorRemoveIconApplication.CssClass = "MediumIcon";
                }

                // enable the button
                this._BackgroundColorRemoveButtonApplication.Enabled = true;
            }
            #endregion

        }
        #endregion

        #region _BuildMastheadPreviewContainer
        /// <summary>
        /// Builds the masthead preview elements.
        /// </summary>
        private void _BuildMastheadPreviewContainer()
        {
            // set css on container
            this._MastheadHomePagePreviewContainer.ID = "MastheadHomePagePreviewContainer";
            this._MastheadHomePagePreviewContainer.CssClass = "MastheadContainer";

            // left side (logo)
            Panel mastheadPreviewLeftSide = new Panel();
            mastheadPreviewLeftSide.ID = "MastheadPreviewContainerLeft";
            mastheadPreviewLeftSide.CssClass = "MastheadColumnLeft";

            // set the id of the logo image
            this._MastheadHomePageLogoImage.ID = "MastheadHomePageLogoImage";

            // attach controls
            mastheadPreviewLeftSide.Controls.Add(this._MastheadHomePageLogoImage);
            this._MastheadHomePagePreviewContainer.Controls.Add(mastheadPreviewLeftSide);

            // right side (secondary image)
            Panel mastheadPreviewRightSide = new Panel();
            mastheadPreviewRightSide.ID = "MastheadPreviewContainerRight";
            mastheadPreviewRightSide.CssClass = "MastheadColumnRight";

            // set the id of the logo image
            this._MastheadHomePageSecondaryImage.ID = "MastheadHomePageSecondaryImage";

            // attach controls
            mastheadPreviewRightSide.Controls.Add(this._MastheadHomePageSecondaryImage);
            this._MastheadHomePagePreviewContainer.Controls.Add(mastheadPreviewRightSide);
            //this.MastheadContainer.Controls.Add(this._MastheadHomePagePreviewContainer);
        }
        #endregion
        
        #region _BuildApplicationMastheadPreviewContainer
        /// <summary>
        /// Builds the masthead application preview elements.
        /// </summary>
        private void _BuildApplicationMastheadPreviewContainer()
        {
            // set css on container
            this._MastheadApplicationPreviewContainer.CssClass = "MastheadContainer";

            // left side (logo)
            Panel mastheadApplicationPreviewLeftSide = new Panel();
            mastheadApplicationPreviewLeftSide.ID = "MastheadApplicationPreviewContainerLeft";
            mastheadApplicationPreviewLeftSide.CssClass = "MastheadColumnLeft";

            // set the id of the logo image and icon image
            this._MastheadApplicationLogoImage.ID = "MastheadApplicationLogoImage";
            this._MastheadApplicationIconImage.ID = "MastheadApplicationIconImage";

            // attach controls
            mastheadApplicationPreviewLeftSide.Controls.Add(this._MastheadApplicationIconImage);
            mastheadApplicationPreviewLeftSide.Controls.Add(this._MastheadApplicationLogoImage);
            this._MastheadApplicationPreviewContainer.Controls.Add(mastheadApplicationPreviewLeftSide);

            // right side container
            Panel mastheadApplicationPreviewRightSide = new Panel();
            mastheadApplicationPreviewRightSide.ID = "MastheadApplicationPreviewContainerRight";
            mastheadApplicationPreviewRightSide.CssClass = "MastheadColumnRight";

            // attach controls
            this._MastheadApplicationPreviewContainer.Controls.Add(mastheadApplicationPreviewRightSide);
        }
        #endregion

        #region _BuildMastheadHomePageFormContainer
        /// <summary>
        /// Builds the masthead form elements.
        /// </summary>
        private void _BuildMastheadHomePageFormContainer()
        {
            // table
            Table mastheadFormTable = new Table();
            mastheadFormTable.CssClass = "StandardTable GridTable";

            // header row
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.CssClass = "GridHeaderRow";
            TableHeaderCell row1col1 = new TableHeaderCell();
            TableHeaderCell row1col2 = new TableHeaderCell();
            TableHeaderCell row1col3 = new TableHeaderCell();

            // actions row
            TableRow actionsRow = new TableRow();
            TableCell row2col1 = new TableCell();
            TableCell row2col2 = new TableCell();
            TableCell row2col3 = new TableCell();

            // header row - column 1
            row1col1.CssClass = "centered";
            row1col1.Style.Add("width", "25%");
            row1col1.Style.Add("border-right", "2px solid #FFFFFF");
            Literal logoLabel = new Literal();
            logoLabel.Text = _GlobalResources.LogoImage;
            row1col1.Controls.Add(logoLabel);

            // header row - column 2
            row1col2.CssClass = "centered";
            row1col2.Style.Add("width", "50%");
            Literal backgroundLabel = new Literal();
            backgroundLabel.Text = _GlobalResources.Background;
            row1col2.Controls.Add(backgroundLabel);

            // header row - column 3
            row1col3.CssClass = "centered";
            row1col3.Style.Add("width", "25%");
            row1col3.Style.Add("border-left", "2px solid #FFFFFF");
            Literal secondaryLabel = new Literal();
            secondaryLabel.Text = _GlobalResources.SecondaryImage;
            row1col3.Controls.Add(secondaryLabel);

            // attach columns to header row
            headerRow.Controls.Add(row1col1);
            headerRow.Controls.Add(row1col2);
            headerRow.Controls.Add(row1col3);

            // attach header row to table
            mastheadFormTable.Rows.Add(headerRow);

            // actions row - column 1

            // upload image
            HtmlGenericControl uploadLogoImageButtonWrapper = new HtmlGenericControl("p");

            Image uploadLogoImageForLink = new Image();
            uploadLogoImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD,
                                                                     ImageFiles.EXT_PNG);
            uploadLogoImageForLink.CssClass = "MediumIcon";

            Localize uploadLogoTextForLink = new Localize();
            uploadLogoTextForLink.Text = _GlobalResources.UploadImage;

            this._LogoImageHomePageUploadButton = new LinkButton();
            this._LogoImageHomePageUploadButton.ID = "LogoImageHomePageUploadButton";
            this._LogoImageHomePageUploadButton.CssClass = "ImageLink";
            this._LogoImageHomePageUploadButton.Controls.Add(uploadLogoImageForLink);
            this._LogoImageHomePageUploadButton.Controls.Add(uploadLogoTextForLink);
            uploadLogoImageButtonWrapper.Controls.Add(this._LogoImageHomePageUploadButton);
            row2col1.Controls.Add(uploadLogoImageButtonWrapper);

            // remove image
            HtmlGenericControl removeLogoImageButtonWrapper = new HtmlGenericControl("p");

            Image removeLogoImageForLink = new Image();
            removeLogoImageForLink.ID = "LogoImageHomePageRemoveButtonIcon";

            if (!String.IsNullOrWhiteSpace(this._MastheadLogoImageName))
            {
                removeLogoImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                         ImageFiles.EXT_PNG);
                removeLogoImageForLink.CssClass = "MediumIcon";
            }
            else
            {
                removeLogoImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                         ImageFiles.EXT_PNG);
                removeLogoImageForLink.CssClass = "MediumIcon DimIcon";
            }

            Localize removeLogoTextForLink = new Localize();
            removeLogoTextForLink.Text = _GlobalResources.RemoveImage;

            this._LogoImageHomePageRemoveButton = new LinkButton();
            this._LogoImageHomePageRemoveButton.ID = "LogoImageHomePageRemoveButton";
            this._LogoImageHomePageRemoveButton.CssClass = "ImageLink";
            this._LogoImageHomePageRemoveButton.Controls.Add(removeLogoImageForLink);
            this._LogoImageHomePageRemoveButton.Controls.Add(removeLogoTextForLink);
            this._LogoImageHomePageRemoveButton.OnClientClick = "RemoveLogoImagePreview();return false;";

            if (String.IsNullOrWhiteSpace(this._MastheadLogoImageName))
            { this._LogoImageHomePageRemoveButton.Enabled = false; }

            removeLogoImageButtonWrapper.Controls.Add(this._LogoImageHomePageRemoveButton);
            row2col1.Controls.Add(removeLogoImageButtonWrapper);

            // actions row - column 2

            // upload image
            HtmlGenericControl uploadBackgroundImageButtonWrapper = new HtmlGenericControl("p");

            Image uploadBackgroundImageForLink = new Image();
            uploadBackgroundImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD,
                                                                           ImageFiles.EXT_PNG);
            uploadBackgroundImageForLink.CssClass = "MediumIcon";

            Localize uploadBackgroundTextForLink = new Localize();
            uploadBackgroundTextForLink.Text = _GlobalResources.UploadImage;

            this._BackgroundImageUploadButtonHomePage = new LinkButton();
            this._BackgroundImageUploadButtonHomePage.ID = "BackgroundImageUploadButtonHomePage";
            this._BackgroundImageUploadButtonHomePage.CssClass = "ImageLink";
            this._BackgroundImageUploadButtonHomePage.Controls.Add(uploadBackgroundImageForLink);
            this._BackgroundImageUploadButtonHomePage.Controls.Add(uploadBackgroundTextForLink);
            uploadBackgroundImageButtonWrapper.Controls.Add(this._BackgroundImageUploadButtonHomePage);
            row2col2.Controls.Add(uploadBackgroundImageButtonWrapper);

            // remove image
            HtmlGenericControl removeBackgroundImageButtonWrapper = new HtmlGenericControl("p");

            Image removeBackgroundImageForLink = new Image();
            removeBackgroundImageForLink.ID = "BackgroundImageHomePageRemoveButtonIcon";

            if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundImageName))
            {
                removeBackgroundImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                               ImageFiles.EXT_PNG);
                removeBackgroundImageForLink.CssClass = "MediumIcon";
            }
            else
            {
                removeBackgroundImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                               ImageFiles.EXT_PNG);
                removeBackgroundImageForLink.CssClass = "MediumIcon DimIcon";
            }

            Localize removeBackgroundTextForLink = new Localize();
            removeBackgroundTextForLink.Text = _GlobalResources.RemoveImage;

            this._BackgroundImageRemoveButton = new LinkButton();
            this._BackgroundImageRemoveButton.ID = "BackgroundImageRemoveButtonHomePage";
            this._BackgroundImageRemoveButton.CssClass = "ImageLink";
            this._BackgroundImageRemoveButton.Controls.Add(removeBackgroundImageForLink);
            this._BackgroundImageRemoveButton.Controls.Add(removeBackgroundTextForLink);
            this._BackgroundImageRemoveButton.OnClientClick = "RemoveBackgroundImagePreview();return false;";

            if (String.IsNullOrWhiteSpace(this._MastheadBackgroundImageName))
            { this._BackgroundImageRemoveButton.Enabled = false; }

            removeBackgroundImageButtonWrapper.Controls.Add(this._BackgroundImageRemoveButton);
            row2col2.Controls.Add(removeBackgroundImageButtonWrapper);

            // background color
            HtmlGenericControl backgroundColorButtonWrapper = new HtmlGenericControl("p");

            Image backgroundColorImageForLink = new Image();
            backgroundColorImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COLOR,
                                                                          ImageFiles.EXT_PNG);
            backgroundColorImageForLink.CssClass = "MediumIcon";

            Localize backgroundColorTextForLink = new Localize();
            backgroundColorTextForLink.Text = _GlobalResources.SelectColor;

            this._BackgroundColorSelectButton = new LinkButton();
            this._BackgroundColorSelectButton.ID = "BackgroundColorSelectButtonHomePage";
            this._BackgroundColorSelectButton.CssClass = "ImageLink";
            this._BackgroundColorSelectButton.Controls.Add(backgroundColorImageForLink);
            this._BackgroundColorSelectButton.Controls.Add(backgroundColorTextForLink);
            this._BackgroundColorSelectButton.OnClientClick = "return false;";
            backgroundColorButtonWrapper.Controls.Add(this._BackgroundColorSelectButton);
            row2col2.Controls.Add(backgroundColorButtonWrapper);

            // remove background color
            HtmlGenericControl removeBackgroundColorButtonWrapper = new HtmlGenericControl("p");

            Image removeBackgroundColorImageForLink = new Image();
            removeBackgroundColorImageForLink.ID = "BackgroundColorHomePageRemoveButtonIcon";

            if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundImageColor))
            {
                removeBackgroundColorImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                    ImageFiles.EXT_PNG);
                removeBackgroundColorImageForLink.CssClass = "MediumIcon";
            }
            else
            {
                removeBackgroundColorImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                    ImageFiles.EXT_PNG);
                removeBackgroundColorImageForLink.CssClass = "MediumIcon DimIcon";
            }

            Localize removeBackgroundColorTextForLink = new Localize();
            removeBackgroundColorTextForLink.Text = _GlobalResources.RemoveColor;

            this._BackgroundColorRemoveButton = new LinkButton();
            this._BackgroundColorRemoveButton.ID = "BackgroundColorRemoveButtonHomePage";
            this._BackgroundColorRemoveButton.CssClass = "ImageLink";
            this._BackgroundColorRemoveButton.Controls.Add(removeBackgroundColorImageForLink);
            this._BackgroundColorRemoveButton.Controls.Add(removeBackgroundColorTextForLink);
            this._BackgroundColorRemoveButton.OnClientClick = "RemoveBackgroundColorPreview();return false;";

            if (String.IsNullOrWhiteSpace(this._MastheadBackgroundImageColor))
            { this._BackgroundColorRemoveButton.Enabled = false; }

            removeBackgroundColorButtonWrapper.Controls.Add(this._BackgroundColorRemoveButton);
            row2col2.Controls.Add(removeBackgroundColorButtonWrapper);

            // actions row - column 3

            // upload image
            HtmlGenericControl uploadSecondaryImageButtonWrapper = new HtmlGenericControl("p");

            Image uploadSecondaryImageForLink = new Image();
            uploadSecondaryImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD,
                                                                          ImageFiles.EXT_PNG);
            uploadSecondaryImageForLink.CssClass = "MediumIcon";

            Localize uploadSecondaryTextForLink = new Localize();
            uploadSecondaryTextForLink.Text = _GlobalResources.UploadImage;

            this._SecondaryImageUploadButton = new LinkButton();
            this._SecondaryImageUploadButton.ID = "SecondaryImageUploadButtonHomePage";
            this._SecondaryImageUploadButton.CssClass = "ImageLink";
            this._SecondaryImageUploadButton.Controls.Add(uploadSecondaryImageForLink);
            this._SecondaryImageUploadButton.Controls.Add(uploadSecondaryTextForLink);
            uploadSecondaryImageButtonWrapper.Controls.Add(this._SecondaryImageUploadButton);
            row2col3.Controls.Add(uploadSecondaryImageButtonWrapper);

            // remove image
            HtmlGenericControl removeSecondaryImageButtonWrapper = new HtmlGenericControl("p");

            Image removeSecondaryImageForLink = new Image();
            removeSecondaryImageForLink.ID = "SecondaryImageRemoveButtonIconHomePage";

            if (!String.IsNullOrWhiteSpace(this._MastheadSecondaryImageName))
            {
                removeSecondaryImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                              ImageFiles.EXT_PNG);
                removeSecondaryImageForLink.CssClass = "MediumIcon";
            }
            else
            {
                removeSecondaryImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                              ImageFiles.EXT_PNG);
                removeSecondaryImageForLink.CssClass = "MediumIcon DimIcon";
            }

            Localize removeSecondaryTextForLink = new Localize();
            removeSecondaryTextForLink.Text = _GlobalResources.RemoveImage;

            this._SecondaryImageRemoveButton = new LinkButton();
            this._SecondaryImageRemoveButton.ID = "SecondaryImageRemoveButton";
            this._SecondaryImageRemoveButton.CssClass = "ImageLink";
            this._SecondaryImageRemoveButton.Controls.Add(removeSecondaryImageForLink);
            this._SecondaryImageRemoveButton.Controls.Add(removeSecondaryTextForLink);
            this._SecondaryImageRemoveButton.OnClientClick = "RemoveSecondaryImagePreview();return false;";

            if (String.IsNullOrWhiteSpace(this._MastheadSecondaryImageName))
            { this._SecondaryImageRemoveButton.Enabled = false; }

            removeSecondaryImageButtonWrapper.Controls.Add(this._SecondaryImageRemoveButton);
            row2col3.Controls.Add(removeSecondaryImageButtonWrapper);

            // attach columns to actions row
            actionsRow.Controls.Add(row2col1);
            actionsRow.Controls.Add(row2col2);
            actionsRow.Controls.Add(row2col3);

            // attach actions row to table
            mastheadFormTable.Rows.Add(actionsRow);

            // attach table to container
            this._MastheadHomePageFormContentContainer.Controls.Add(mastheadFormTable);
        }
        #endregion

        #region _BuildMastheadApplicationFormContainer
        /// <summary>
        /// Builds the masthead form elements.
        /// </summary>
        private void _BuildMastheadApplicationFormContainer()
        {
            // table
            Table mastheadFormTable = new Table();
            mastheadFormTable.CssClass = "StandardTable GridTable";

            // header row
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.CssClass = "GridHeaderRow";
            TableHeaderCell row1col1 = new TableHeaderCell();
            TableHeaderCell row1col2 = new TableHeaderCell();
            TableHeaderCell row1col3 = new TableHeaderCell();

            // actions row
            TableRow actionsRow = new TableRow();
            TableCell row2col1 = new TableCell();
            TableCell row2col2 = new TableCell();
            TableCell row2col3 = new TableCell();

            // header row - column 1
            row1col1.CssClass = "centered";
            row1col1.Style.Add("width", "25%");
            row1col1.Style.Add("border-right", "2px solid #FFFFFF");
            Literal iconLabel = new Literal();
            iconLabel.Text = _GlobalResources.IconImage;
            row1col1.Controls.Add(iconLabel);

            // header row - column 2
            row1col2.CssClass = "centered";
            row1col2.Style.Add("width", "50%");
            Literal backgroundLabel = new Literal();
            backgroundLabel.Text = _GlobalResources.LogoImage;
            row1col2.Controls.Add(backgroundLabel);

            // header row - column 3
            row1col3.CssClass = "centered";
            row1col3.Style.Add("width", "25%");
            row1col3.Style.Add("border-left", "2px solid #FFFFFF");
            Literal secondaryLabel = new Literal();
            secondaryLabel.Text = _GlobalResources.Background;
            row1col3.Controls.Add(secondaryLabel);

            // attach columns to header row
            headerRow.Controls.Add(row1col1);
            headerRow.Controls.Add(row1col2);
            headerRow.Controls.Add(row1col3);

            // attach header row to table
            mastheadFormTable.Rows.Add(headerRow);

            // actions row - column 1

            // upload image
            HtmlGenericControl uploadIconImageButtonWrapper = new HtmlGenericControl("p");

            Image uploadIconImageForLink = new Image();
            uploadIconImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD,
                                                                     ImageFiles.EXT_PNG);
            uploadIconImageForLink.CssClass = "MediumIcon";

            Localize uploadIconTextForLink = new Localize();
            uploadIconTextForLink.Text = _GlobalResources.UploadImage;

            this._IconImageApplicationUploadButton = new LinkButton();
            this._IconImageApplicationUploadButton.ID = "IconImageApplicationUploadButton";
            this._IconImageApplicationUploadButton.CssClass = "ImageLink";
            this._IconImageApplicationUploadButton.Controls.Add(uploadIconImageForLink);
            this._IconImageApplicationUploadButton.Controls.Add(uploadIconTextForLink);
            uploadIconImageButtonWrapper.Controls.Add(this._IconImageApplicationUploadButton);
            row2col1.Controls.Add(uploadIconImageButtonWrapper);

            // remove image
            HtmlGenericControl removeIconImageButtonWrapper = new HtmlGenericControl("p");

            Image removeIconImageForLink = new Image();
            removeIconImageForLink.ID = "IconImageApplicationRemoveButtonIcon";

            if (!String.IsNullOrWhiteSpace(this._MastheadIconImageNameApplication))
            {
                removeIconImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                         ImageFiles.EXT_PNG);
                removeIconImageForLink.CssClass = "MediumIcon";
            }
            else
            {
                removeIconImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                         ImageFiles.EXT_PNG);
                removeIconImageForLink.CssClass = "MediumIcon DimIcon";
            }

            Localize removeIconTextForLink = new Localize();
            removeIconTextForLink.Text = _GlobalResources.RemoveImage;

            this._IconImageApplicationRemoveButton = new LinkButton();
            this._IconImageApplicationRemoveButton.ID = "IconImageApplicationRemoveButton";
            this._IconImageApplicationRemoveButton.CssClass = "ImageLink";
            this._IconImageApplicationRemoveButton.Controls.Add(removeIconImageForLink);
            this._IconImageApplicationRemoveButton.Controls.Add(removeIconTextForLink);
            this._IconImageApplicationRemoveButton.OnClientClick = "RemoveIconImagePreviewApplication();return false;";

            if (String.IsNullOrWhiteSpace(this._MastheadIconImageNameApplication))
            { this._IconImageApplicationRemoveButton.Enabled = false; }

            removeIconImageButtonWrapper.Controls.Add(this._IconImageApplicationRemoveButton);
            row2col1.Controls.Add(removeIconImageButtonWrapper);

            // actions row - column 2

            // upload image
            HtmlGenericControl uploadLogoImageButtonWrapper = new HtmlGenericControl("p");

            Image uploadLogoImageForLink = new Image();
            uploadLogoImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD,
                                                                     ImageFiles.EXT_PNG);
            uploadLogoImageForLink.CssClass = "MediumIcon";

            Localize uploadLogoTextForLink = new Localize();
            uploadLogoTextForLink.Text = _GlobalResources.UploadImage;

            this._LogoImageApplicationUploadButton = new LinkButton();
            this._LogoImageApplicationUploadButton.ID = "LogoImageApplicationUploadButton";
            this._LogoImageApplicationUploadButton.CssClass = "ImageLink";
            this._LogoImageApplicationUploadButton.Controls.Add(uploadLogoImageForLink);
            this._LogoImageApplicationUploadButton.Controls.Add(uploadLogoTextForLink);
            uploadLogoImageButtonWrapper.Controls.Add(this._LogoImageApplicationUploadButton);
            row2col2.Controls.Add(uploadLogoImageButtonWrapper);

            // remove image
            HtmlGenericControl removeLogoImageButtonWrapper = new HtmlGenericControl("p");

            Image removeLogoImageForLink = new Image();
            removeLogoImageForLink.ID = "LogoImageApplicationRemoveButtonIcon";

            if (!String.IsNullOrWhiteSpace(this._MastheadLogoImageNameApplication))
            {
                removeLogoImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                         ImageFiles.EXT_PNG);
                removeLogoImageForLink.CssClass = "MediumIcon";
            }
            else
            {
                removeLogoImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                         ImageFiles.EXT_PNG);
                removeLogoImageForLink.CssClass = "MediumIcon DimIcon";
            }

            Localize removeLogoTextForLink = new Localize();
            removeLogoTextForLink.Text = _GlobalResources.RemoveImage;

            this._LogoImageApplicationRemoveButton = new LinkButton();
            this._LogoImageApplicationRemoveButton.ID = "LogoImageApplicationRemoveButton";
            this._LogoImageApplicationRemoveButton.CssClass = "ImageLink";
            this._LogoImageApplicationRemoveButton.Controls.Add(removeLogoImageForLink);
            this._LogoImageApplicationRemoveButton.Controls.Add(removeLogoTextForLink);
            this._LogoImageApplicationRemoveButton.OnClientClick = "RemoveLogoImagePreviewApplication();return false;";

            if (String.IsNullOrWhiteSpace(this._MastheadLogoImageNameApplication))
            { this._LogoImageApplicationRemoveButton.Enabled = false; }

            removeLogoImageButtonWrapper.Controls.Add(this._LogoImageApplicationRemoveButton);
            row2col2.Controls.Add(removeLogoImageButtonWrapper);

            // actions row - column 3

            // background color
            HtmlGenericControl backgroundColorButtonWrapper = new HtmlGenericControl("p");

            Image backgroundColorImageForLink = new Image();
            backgroundColorImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COLOR,
                                                                          ImageFiles.EXT_PNG);
            backgroundColorImageForLink.CssClass = "MediumIcon";

            Localize backgroundColorTextForLink = new Localize();
            backgroundColorTextForLink.Text = _GlobalResources.SelectColor;

            this._BackgroundColorSelectButtonApplication = new LinkButton();
            this._BackgroundColorSelectButtonApplication.ID = "BackgroundColorSelectButtonApplication";
            this._BackgroundColorSelectButtonApplication.CssClass = "ImageLink";
            this._BackgroundColorSelectButtonApplication.Controls.Add(backgroundColorImageForLink);
            this._BackgroundColorSelectButtonApplication.Controls.Add(backgroundColorTextForLink);
            this._BackgroundColorSelectButtonApplication.OnClientClick = "return false;";
            backgroundColorButtonWrapper.Controls.Add(this._BackgroundColorSelectButtonApplication);
            row2col3.Controls.Add(backgroundColorButtonWrapper);

            // remove background color
            HtmlGenericControl removeBackgroundColorButtonWrapper = new HtmlGenericControl("p");

            Image removeBackgroundColorImageForLink = new Image();
            removeBackgroundColorImageForLink.ID = "BackgroundColorApplicationRemoveButtonIcon";

            if (!String.IsNullOrWhiteSpace(this._MastheadBackgroundColorApplication))
            {
                removeBackgroundColorImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                    ImageFiles.EXT_PNG);
                removeBackgroundColorImageForLink.CssClass = "MediumIcon";
            }
            else
            {
                removeBackgroundColorImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                    ImageFiles.EXT_PNG);
                removeBackgroundColorImageForLink.CssClass = "MediumIcon DimIcon";
            }

            Localize removeBackgroundColorTextForLink = new Localize();
            removeBackgroundColorTextForLink.Text = _GlobalResources.RemoveColor;

            this._BackgroundColorRemoveButtonApplication = new LinkButton();
            this._BackgroundColorRemoveButtonApplication.ID = "BackgroundColorRemoveButtonApplication";
            this._BackgroundColorRemoveButtonApplication.CssClass = "ImageLink";
            this._BackgroundColorRemoveButtonApplication.Controls.Add(removeBackgroundColorImageForLink);
            this._BackgroundColorRemoveButtonApplication.Controls.Add(removeBackgroundColorTextForLink);
            this._BackgroundColorRemoveButtonApplication.OnClientClick = "RemoveBackgroundColorPreviewApplication();return false;";

            if (String.IsNullOrWhiteSpace(this._MastheadBackgroundColorApplication))
            { this._BackgroundColorRemoveButtonApplication.Enabled = false; }

            removeBackgroundColorButtonWrapper.Controls.Add(this._BackgroundColorRemoveButtonApplication);
            row2col3.Controls.Add(removeBackgroundColorButtonWrapper);

            // attach columns to actions row
            actionsRow.Controls.Add(row2col1);
            actionsRow.Controls.Add(row2col2);
            actionsRow.Controls.Add(row2col3);

            // attach actions row to table
            mastheadFormTable.Rows.Add(actionsRow);

            // attach table to container
            this._MastheadApplicationFormContentContainer.Controls.Add(mastheadFormTable);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _BuildLogoUploadModal
        /// <summary>
        /// Builds the modal that contains the uploader for the logo image.
        /// </summary>
        private void _BuildLogoUploadModal()
        {
            // build the modal
            this._LogoImageUpload = new ModalPopup("LogoImageUpload");
            this._LogoImageUpload.Type = ModalPopupType.Information;
            this._LogoImageUpload.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD,
                                                                          ImageFiles.EXT_PNG);
            this._LogoImageUpload.HeaderIconAlt = "Upload";
            this._LogoImageUpload.HeaderText = _GlobalResources.UploadLogoImage;
            this._LogoImageUpload.TargetControlID = this._LogoImageHomePageUploadButton.ID;
            this._LogoImageUpload.ReloadPageOnClose = false;

            // build the modal body

            // uploader
            this._LogoImageUploader = new UploaderAsync("LogoImageUploader", UploadType.MastheadImage, "LogoImageUploader_ErrorContainer");
            this._LogoImageUploader.ValidateSpecificExtension = false;
            this._LogoImageUploader.ResizeOnUpload = false;
            this._LogoImageUploader.ShowPreviewOnImageUpload = false;
            this._LogoImageUploader.ClientSideCompleteJSMethod = "ChangeLogoImagePreview";

            // uploader error panel
            Panel logoImageUploaderErrorContainer = new Panel();
            logoImageUploaderErrorContainer.ID = "LogoImageUploader_ErrorContainer";
            logoImageUploaderErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to body
            this._LogoImageUpload.AddControlToBody(logoImageUploaderErrorContainer);
            this._LogoImageUpload.AddControlToBody(this._LogoImageUploader);

            // add modal to form content container
            this._MastheadHomePageFormContentContainer.Controls.Add(this._LogoImageUpload);
        }
        #endregion

        #region _UploadCompleteLogo
        /// <summary>
        /// Method to resize the image and replace the original image with resized image
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <param name="fileName"></param>
        private void _UploadCompleteLogo(object o, EventArgs e, string filename)
        {
            try
            {
                ImageResizer resizer = new ImageResizer();
                resizer.MaxX = Convert.ToInt32(Config.ApplicationSettings.ApplicationLogoSize.Split(':')[0]);
                resizer.MaxY = Convert.ToInt32(Config.ApplicationSettings.ApplicationLogoSize.Split(':')[1]);
                resizer.TrimImage = true;
                resizer.Resize(MapPathSecure(filename), MapPathSecure(filename + ".resized"));

                // replace uploaded with "resized"
                File.Copy(MapPathSecure(filename + ".resized"), MapPathSecure(filename), true);

                // delete "resized"
                File.Delete(MapPathSecure(filename + ".resized"));
            }
            catch (Exception ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _UploadCompleteIcon
        /// <summary>
        /// Upload Complete for Application Icon
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <param name="fileName"></param>
        private void _UploadCompleteIcon(object o, EventArgs e, string filename)
        {
            try
            {
                ImageResizer resizer = new ImageResizer();
                resizer.MaxX = Convert.ToInt32(Config.ApplicationSettings.ApplicationIconSize.Split(':')[0]);
                resizer.MaxY = Convert.ToInt32(Config.ApplicationSettings.ApplicationIconSize.Split(':')[1]);
                resizer.TrimImage = true;
                resizer.Resize(MapPathSecure(filename), MapPathSecure(filename + ".resized"));

                // replace uploaded with "resized"
                File.Copy(MapPathSecure(filename + ".resized"), MapPathSecure(filename), true);

                // delete "resized"
                File.Delete(MapPathSecure(filename + ".resized"));
            }
            catch (Exception ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _BuildSecondaryUploadModal
        /// <summary>
        /// Builds the modal that contains the upoader for the secondary image.
        /// </summary>
        private void _BuildSecondaryUploadModal()
        {
            // build the modal
            this._SecondaryImageUpload = new ModalPopup("SecondaryImageUpload");
            this._SecondaryImageUpload.Type = ModalPopupType.Information;
            this._SecondaryImageUpload.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD,
                                                                               ImageFiles.EXT_PNG);
            this._SecondaryImageUpload.HeaderIconAlt = "Upload";
            this._SecondaryImageUpload.HeaderText = _GlobalResources.UploadSecondaryImage;
            this._SecondaryImageUpload.TargetControlID = this._SecondaryImageUploadButton.ID;
            this._SecondaryImageUpload.ReloadPageOnClose = false;

            // build the modal body

            // uploader
            this._SecondaryImageUploader = new UploaderAsync("SecondaryImageUploader", UploadType.MastheadImage, "SecondaryImageUploader_ErrorContainer");
            this._SecondaryImageUploader.ValidateSpecificExtension = false;
            this._SecondaryImageUploader.ResizeOnUpload = false;
            this._SecondaryImageUploader.ShowPreviewOnImageUpload = false;
            this._SecondaryImageUploader.ClientSideCompleteJSMethod = "ChangeSecondaryImagePreview";

            // uploader error panel
            Panel secondaryImageUploaderErrorContainer = new Panel();
            secondaryImageUploaderErrorContainer.ID = "SecondaryImageUploader_ErrorContainer";
            secondaryImageUploaderErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to body
            this._SecondaryImageUpload.AddControlToBody(secondaryImageUploaderErrorContainer);
            this._SecondaryImageUpload.AddControlToBody(this._SecondaryImageUploader);

            // add modal to form content container
            this._MastheadHomePageFormContentContainer.Controls.Add(this._SecondaryImageUpload);
        }
        #endregion

        #region _BuildBackgroundUploadModal
        /// <summary>
        /// Builds the modal that contains the upoader for the background image.
        /// </summary>
        private void _BuildBackgroundUploadModal()
        {
            // build the modal
            this._BackgroundImageUpload = new ModalPopup("BackgroundImageUpload");
            this._BackgroundImageUpload.Type = ModalPopupType.Information;
            this._BackgroundImageUpload.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD,
                                                                                ImageFiles.EXT_PNG);
            this._BackgroundImageUpload.HeaderIconAlt = "Upload";
            this._BackgroundImageUpload.HeaderText = _GlobalResources.MastheadUploadBackgroundImage;
            this._BackgroundImageUpload.TargetControlID = this._BackgroundImageUploadButtonHomePage.ID;
            this._BackgroundImageUpload.ReloadPageOnClose = false;

            // build the modal body

            // uploader
            this._BackgroundImageUploader = new UploaderAsync("BackgroundImageUploader", UploadType.MastheadImage, "BackgroundImageUploader_ErrorContainer");
            this._BackgroundImageUploader.ValidateSpecificExtension = false;
            this._BackgroundImageUploader.ResizeOnUpload = false;
            this._BackgroundImageUploader.ShowPreviewOnImageUpload = false;
            this._BackgroundImageUploader.ClientSideCompleteJSMethod = "ChangeBackgroundImagePreview";

            // uploader error panel
            Panel backgroundImageUploaderErrorContainer = new Panel();
            backgroundImageUploaderErrorContainer.ID = "BackgroundImageUploader_ErrorContainer";
            backgroundImageUploaderErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to body
            this._BackgroundImageUpload.AddControlToBody(backgroundImageUploaderErrorContainer);
            this._BackgroundImageUpload.AddControlToBody(this._BackgroundImageUploader);

            // add modal to form content container
            this._MastheadHomePageFormContentContainer.Controls.Add(this._BackgroundImageUpload);
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        protected void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // create datatable for site params
                DataTable siteParams = new DataTable(); ;
                siteParams.Columns.Add("param", typeof(string));
                siteParams.Columns.Add("value", typeof(string));

                // create variables for file names for masthead elements
                string logoImageFileName;
                string secondaryImageFileName;
                string backgroundImageFileName;
                string logoImageFileNameApplication = string.Empty;
                string iconImageFileNameApplication = string.Empty;

                // check to make sure directory exists, if not, create it
                if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE)))
                { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE)); }

                #region HOMEPAGE values
                // HOMEPAGE values
                // logo image - copy image to site masthead directory, delete uploaded image, and populate site param
                if (this._LogoImage.Value.Contains(SitePathConstants.UPLOAD_MASTHEAD))
                {
                    logoImageFileName = Path.GetFileName(this._LogoImage.Value);
                    File.Copy(Server.MapPath(this._LogoImage.Value), Server.MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName));
                    File.Delete(Server.MapPath(this._LogoImage.Value));
                }
                else // else is the default image or "blank"
                { logoImageFileName = this._LogoImage.Value; }

                DataRow logoImage = siteParams.NewRow();
                logoImage["param"] = SiteParamConstants.HOMEPAGE_MASTHEAD_LOGO;
                logoImage["value"] = logoImageFileName;
                siteParams.Rows.Add(logoImage);

                // secondary image - copy image to site masthead directory, delete uploaded image, and populate site param
                if (this._SecondaryImage.Value.Contains(SitePathConstants.UPLOAD_MASTHEAD))
                {
                    secondaryImageFileName = Path.GetFileName(this._SecondaryImage.Value);
                    File.Copy(Server.MapPath(this._SecondaryImage.Value), Server.MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName));
                    File.Delete(Server.MapPath(this._SecondaryImage.Value));
                }
                else // else is the default image or "blank"
                { secondaryImageFileName = this._SecondaryImage.Value; }

                DataRow secondaryImage = siteParams.NewRow();
                secondaryImage["param"] = SiteParamConstants.HOMEPAGE_MASTHEAD_SECONDARYIMAGE;
                secondaryImage["value"] = secondaryImageFileName;
                siteParams.Rows.Add(secondaryImage);

                // background image - copy image to site masthead directory, delete uploaded image, and populate site param
                if (this._BackgroundImage.Value.Contains(SitePathConstants.UPLOAD_MASTHEAD))
                {
                    backgroundImageFileName = Path.GetFileName(this._BackgroundImage.Value);
                    File.Copy(Server.MapPath(this._BackgroundImage.Value), Server.MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + backgroundImageFileName));
                    File.Delete(Server.MapPath(this._BackgroundImage.Value));
                }
                else // else is the default image or "blank"
                { backgroundImageFileName = this._BackgroundImage.Value; }

                DataRow backgroundImage = siteParams.NewRow();
                backgroundImage["param"] = SiteParamConstants.HOMEPAGE_MASTHEAD_BGIMAGE;
                backgroundImage["value"] = backgroundImageFileName;
                siteParams.Rows.Add(backgroundImage);

                // background color - just do site param
                DataRow backgroundColor = siteParams.NewRow();
                backgroundColor["param"] = SiteParamConstants.HOMEPAGE_MASTHEAD_BGCOLOR;
                backgroundColor["value"] = this._SelectedColor.Value;
                siteParams.Rows.Add(backgroundColor);
                #endregion

                // APPLICATION
                // logo image - copy image to site masthead directory, delete uploaded image, and populate site param
                if (this._LogoImageApplication.Value.Contains(SitePathConstants.UPLOAD_MASTHEAD))
                {
                    logoImageFileNameApplication = Path.GetFileName(this._LogoImageApplication.Value);
                    File.Copy(Server.MapPath(this._LogoImageApplication.Value), Server.MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileNameApplication));
                    File.Delete(Server.MapPath(this._LogoImageApplication.Value));
                }
                else // else is the default image or "blank"
                { logoImageFileNameApplication = this._LogoImageApplication.Value; }

                DataRow logoImageApplication = siteParams.NewRow();
                logoImageApplication["param"] = SiteParamConstants.APPLICATION_MASTHEAD_LOGO;
                logoImageApplication["value"] = logoImageFileNameApplication;
                siteParams.Rows.Add(logoImageApplication);

                // icon image - copy image to site masthead directory, delete uploaded image, and populate site param
                if (this._IconImageApplication.Value.Contains(SitePathConstants.UPLOAD_MASTHEAD))
                {
                    iconImageFileNameApplication = Path.GetFileName(this._IconImageApplication.Value);
                    File.Copy(Server.MapPath(this._IconImageApplication.Value), Server.MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileNameApplication));
                    File.Delete(Server.MapPath(this._IconImageApplication.Value));
                }
                else // else is the default image or "blank"
                { iconImageFileNameApplication = this._IconImageApplication.Value; }

                DataRow iconImageApplication = siteParams.NewRow();
                iconImageApplication["param"] = SiteParamConstants.APPLICATION_MASTHEAD_ICON;
                iconImageApplication["value"] = iconImageFileNameApplication;
                siteParams.Rows.Add(iconImageApplication);

                // background color - just do site param
                DataRow backgroundColorApplication = siteParams.NewRow();
                backgroundColorApplication["param"] = SiteParamConstants.APPLICATION_MASTHEAD_BGCOLOR;
                backgroundColorApplication["value"] = this._SelectedColorApplication.Value;
                siteParams.Rows.Add(backgroundColorApplication);

                // save the site params
                Common.AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite,
                                                  AsentiaSessionState.UserCulture,
                                                  AsentiaSessionState.IdSiteUser,
                                                  AsentiaSessionState.IdSite,
                                                  siteParams);

                // delete any unused files in the site's masthead directory
                DirectoryInfo mastheadDirectory = new DirectoryInfo(Server.MapPath(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE));

                foreach (FileInfo fileInfo in mastheadDirectory.GetFiles())
                {
                    // if file name is not the logo, secondary, or background image file name, delete
                    if (fileInfo.Name != logoImageFileName && fileInfo.Name != secondaryImageFileName && fileInfo.Name != backgroundImageFileName && fileInfo.Name != logoImageFileNameApplication && fileInfo.Name != iconImageFileNameApplication)
                    { fileInfo.Delete(); }
                }

                // load the newly saved masthead content from inputs
                this._LoadMastheadContentFromInputs();

                // reset the image file inputs
                this._LogoImage.Value = logoImageFileName;
                this._SecondaryImage.Value = secondaryImageFileName;
                this._BackgroundImage.Value = backgroundImageFileName;

                // reset the image file inputs
                this._LogoImageApplication.Value = logoImageFileNameApplication;
                this._IconImageApplication.Value = iconImageFileNameApplication;

                // set the "remove" button states based on the newly saved data
                this._SetRemoveButtonStatesAfterSave();

                // display success message to user
                this.DisplayFeedback(_GlobalResources.MastheadInformationHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        protected void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            // redirect user to dashboard area
            Response.Redirect("~/dashboard/");
        }
        #endregion
    }
}
