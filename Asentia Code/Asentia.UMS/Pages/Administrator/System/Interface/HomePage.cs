﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.UMS.Pages.Administrator.System.Interface
{
    public class HomePage : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel HomePageFormContentWrapperContainer;
        public Panel HomePageWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel HomePagePropertiesContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private TextBox _BodyHtmlTextBox;
        private Button _SaveButton;
        private Button _CancelButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");

            // build start up call for MCE and add to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));
            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine(" CKEDITOR.config.height = \"750px\";");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);

            base.OnPreRender(e);
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_HomePage))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.HomePage));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.InterfaceAndLayout, _GlobalResources.HomePage, ImageFiles.GetIconPath(ImageFiles.ICON_HOME,
                                                                                                                           ImageFiles.EXT_PNG));

            // show/hide admin menu
            this.InitializeAdminMenu();

            this.HomePageFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.HomePageWrapperContainer.CssClass = "FormContentContainer";

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.ModifyTheHomePageOfYourPortalUsingTheOptionsBelow, true);

            // build the input controls
            this._BuildInputControls();

            // if this is not a postback, load homepage content
            if (!this.IsPostBack)
            { this._LoadHomePageContent(); }

            // build the actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _BuildInputControls
        /// <summary>
        /// Builds the input controls on the page.
        /// </summary>
        private void _BuildInputControls()
        {
            // home page body field
            this._BodyHtmlTextBox = new TextBox();
            this._BodyHtmlTextBox.ID = "HomePageBody_Field";
            this._BodyHtmlTextBox.CssClass = "ckeditor";
            this._BodyHtmlTextBox.Style.Add("width", "100%");
            this._BodyHtmlTextBox.TextMode = TextBoxMode.MultiLine;

            this.HomePagePropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("HomePageBody",
                                                                              null,
                                                                              this._BodyHtmlTextBox.ID,
                                                                              this._BodyHtmlTextBox,
                                                                              false,
                                                                              false,
                                                                              true));
        }
        #endregion

        #region _LoadHomePageContent
        /// <summary>
        /// Loads the home page file.
        /// </summary>
        private void _LoadHomePageContent()
        {
            try
            {
                // load the home page's content
                Asentia.UMS.Library.HomePage homePage = new Asentia.UMS.Library.HomePage();

                foreach (Asentia.UMS.Library.HomePage.LanguageSpecificProperty homePageLanguageSpecificProperty in homePage.LanguageSpecificProperties)
                {
                    // if language is default, populate the control directly attached to this page
                    // otherwise, find the language-specific control and populate it
                    if (homePageLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._BodyHtmlTextBox.Text = homePageLanguageSpecificProperty.Html;
                    }
                    else
                    {
                        TextBox languageSpecificBodyHtmlTextBox = (TextBox)this.HomePagePropertiesContainer.FindControl(this._BodyHtmlTextBox.ID + "_" + homePageLanguageSpecificProperty.LangString);
                        if (languageSpecificBodyHtmlTextBox != null)
                        { languageSpecificBodyHtmlTextBox.Text = homePageLanguageSpecificProperty.Html; }
                    }
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/dashboard");
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Performs the save action on the page.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _SaveButton_Command(object sender, EventArgs e)
        {
            try
            {
                // save the home page's content
                Asentia.UMS.Library.HomePage homePage = new Asentia.UMS.Library.HomePage();

                // clear the language specific properties for the home page, we will build it new
                homePage.LanguageSpecificProperties.Clear();

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the default language, get values from the built in object
                    if (cultureInfo.Name == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        homePage.LanguageSpecificProperties.Add(new Asentia.UMS.Library.HomePage.LanguageSpecificProperty(cultureInfo.Name,
                                                                                                                          this._BodyHtmlTextBox.Text));
                    }
                    else
                    {
                        string html = null;

                        TextBox languageSpecificBodyHtmlTextBox = (TextBox)this.HomePagePropertiesContainer.FindControl(this._BodyHtmlTextBox.ID + "_" + cultureInfo.Name);
                        if (languageSpecificBodyHtmlTextBox != null)
                        { html = languageSpecificBodyHtmlTextBox.Text; }

                        homePage.LanguageSpecificProperties.Add(new Asentia.UMS.Library.HomePage.LanguageSpecificProperty(cultureInfo.Name,
                                                                                                                          html));
                    }
                }

                // save the home page content
                homePage.SaveHomePageContent();

                // display feedback
                this.DisplayFeedback(_GlobalResources.HomePageHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion
    }
}