﻿function ChangeThemeSamples() {
    var newlySelectedTheme = $("#ThemeStyling_Field").val();

    $(".SampleScreenImageLink").each(function () {
        var sampleScreenImageElement = this;
        var onclickAttributeValue = $(this).attr("onclick");
        var imageSrcAttribute = $(this).children(":first").attr("src");

        $("#ThemeStyling_Field option").each(function () {
            var optionValue = $(this).attr("value");

            if (onclickAttributeValue.indexOf("/templates/themes/" + optionValue + "/") > 0) {
                $(sampleScreenImageElement).attr("onclick", onclickAttributeValue.replace("/templates/themes/" + optionValue + "/", "/templates/themes/" + newlySelectedTheme + "/"));
                $(sampleScreenImageElement).children(":first").attr("src", imageSrcAttribute.replace("/templates/themes/" + optionValue + "/", "/templates/themes/" + newlySelectedTheme + "/"));
            }
        });        
    });
}

function ChangeIconsetSamples() {
    var newlySelectedIconset = $("#ThemeIconset_Field").val();

    $(".SampleIconsetImage").each(function () {
        var sampleIconsetImageElement = this;
        var imageSrcAttribute = $(this).attr("src");        

        $("#ThemeIconset_Field option").each(function () {
            var optionValue = $(this).attr("value");

            if (imageSrcAttribute.indexOf("/templates/iconsets/" + optionValue + "/") > 0) {
                $(sampleIconsetImageElement).attr("src", imageSrcAttribute.replace("/templates/iconsets/" + optionValue + "/", "/templates/iconsets/" + newlySelectedIconset + "/"));
            }
        });
    });
}

function ShowThemeSampleScreens(sampleScreenPath) {
    // launch the modal
    $("#ThemeSampleScreensModalHiddenLaunchButton").click();

    // build the sample screen
    var screensHTML = "<div><img src=\"" + sampleScreenPath + "\" /></div>";

    // attach the sample screen
    $("#ThemeSampleScreensModalContentPanel").html("").html(screensHTML)
}