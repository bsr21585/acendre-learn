﻿function EmptyWebsiteField() {
    // if the http protocol field is empty, clear out the website url field
    if ($('#HttpProtocol_Field').val() == '') {
        $('#WebsiteUrl_Field').val('');
    }
}