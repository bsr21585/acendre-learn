﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.UMS.Pages.Administrator.System.Interface
{
    public class Footer : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel FooterFormContentWrapperContainer;
        public Panel FooterWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel FooterFormContentContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private TextBox _CompanyName;
        private TextBox _Email;
        private TextBox _WebsiteUrl;
        private DropDownList _HttpProtocols;
        private Button _SaveButton;
        private Button _CancelButton;
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_Footer))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Footer));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.InterfaceAndLayout, _GlobalResources.Footer, ImageFiles.GetIconPath(ImageFiles.ICON_FOOTER,
                                                                                                                         ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.FooterFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.FooterWrapperContainer.CssClass = "FormContentContainer";

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.ModifyThePropertiesOfThePageFooterUsingTheFormBelow, true);

            // build the input controls
            this._BuildInputControls();

            // if this is not a postback, load footer content
            if (!IsPostBack)
            { this._LoadFooterContent(); }

            // build the actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // execute the base
            base.OnPreRender(e);

            // registers the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Footer), "Asentia.UMS.Pages.Administrator.System.Interface.Footer.js");
        }
        #endregion

        #region _BuildInputControls
        /// <summary>
        /// Builds the input controls on the page.
        /// </summary>
        private void _BuildInputControls()
        {
            // company name field
            this._CompanyName = new TextBox();
            this._CompanyName.ID = "CompanyName_Field";
            this._CompanyName.CssClass = "InputLong";

            //Add company name to main container
            this.FooterFormContentContainer.Controls.Add(AsentiaPage.BuildFormField("CompanyName",
                                                             _GlobalResources.CompanyName,
                                                             this._CompanyName.ID,
                                                             this._CompanyName,
                                                             false,
                                                             true,
                                                             false));

            // email field
            this._Email = new TextBox();
            this._Email.ID = "Email_Field";
            this._Email.CssClass = "InputLong";

            //Add email controls to main container
            this.FooterFormContentContainer.Controls.Add(AsentiaPage.BuildFormField("Email",
                                                             _GlobalResources.Email,
                                                             this._Email.ID,
                                                             this._Email,
                                                             false,
                                                             true,
                                                             false));

            //Website field controls
            List<Control> websiteInputControls = new List<Control>();

            this._HttpProtocols = new DropDownList();
            this._HttpProtocols.ID = "HttpProtocol_Field";
            this._HttpProtocols.Items.Add(new ListItem(""));
            this._HttpProtocols.Items.Add(new ListItem("http://"));
            this._HttpProtocols.Items.Add(new ListItem("https://"));
            this._HttpProtocols.Attributes.Add("onchange", "EmptyWebsiteField();");

            websiteInputControls.Add(this._HttpProtocols);

            this._WebsiteUrl = new TextBox();
            this._WebsiteUrl.ID = "WebsiteUrl_Field";
            this._WebsiteUrl.CssClass = "InputLong";

            websiteInputControls.Add(this._WebsiteUrl);

            //Add Website controls to main container
            this.FooterFormContentContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseEnableRatings",
                                                                                                         _GlobalResources.Website,
                                                                                                         websiteInputControls,
                                                                                                         false,
                                                                                                         true));
        }
        #endregion

        #region _LoadFooterContent
        /// <summary>
        /// Loads the footer information from the database.
        /// </summary>
        private void _LoadFooterContent()
        {
            // company name
            this._CompanyName.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_COMPANYNAME);

            // email
            this._Email.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_EMAIL);

            // website url
            string completeWebsiteUrl = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_WEBSITE);

            if (!String.IsNullOrWhiteSpace(completeWebsiteUrl))
            {
                if (completeWebsiteUrl.Contains("http://"))
                { this._HttpProtocols.SelectedValue = "http://"; }
                else if (completeWebsiteUrl.Contains("https://"))
                { this._HttpProtocols.SelectedValue = "https://"; }

                this._WebsiteUrl.Text = completeWebsiteUrl.Replace("http://", "").Replace("https://", "");
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/dashboard");
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;

            // company name
            /*if (String.IsNullOrWhiteSpace(this._CompanyName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.FooterFormContentContainer, "CompanyName", _GlobalResources.CompanyName + " " + _GlobalResources.NULL);
            }*/

            // email field
            /*if (String.IsNullOrWhiteSpace(this._Email.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.FooterFormContentContainer, "Email", _GlobalResources.Email + " " + _GlobalResources.NULL);
            }*/


            if (!String.IsNullOrWhiteSpace(this._Email.Text))
            {
                if (!Common.RegExValidation.ValidateEmailAddress(this._Email.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.FooterFormContentContainer, "Email", _GlobalResources.Email + " " + _GlobalResources.IsInvalid);
                }
            }

            // website url field
            /*if (String.IsNullOrWhiteSpace(this._WebsiteUrl.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.FooterFormContentContainer, "Website", _GlobalResources.Website + " " + _GlobalResources.NULL);
            }*/

            if (!String.IsNullOrWhiteSpace(this._WebsiteUrl.Text))
            {
                if (!Common.RegExValidation.ValidateUrl(this._HttpProtocols.SelectedValue + this._WebsiteUrl.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.FooterFormContentContainer, "Website", _GlobalResources.Website + " " + _GlobalResources.IsInvalid);
                }
            }

            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Performs the save action on the page.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        protected void _SaveButton_Command(object sender, EventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                // create datatable for site params
                DataTable siteParams = new DataTable(); ;
                siteParams.Columns.Add("param", typeof(string));
                siteParams.Columns.Add("value", typeof(string));

                // company name
                DataRow companyName = siteParams.NewRow();
                companyName["param"] = SiteParamConstants.FOOTER_COMPANYNAME;
                companyName["value"] = this._CompanyName.Text;
                siteParams.Rows.Add(companyName);

                // email
                DataRow email = siteParams.NewRow();
                email["param"] = SiteParamConstants.FOOTER_EMAIL;
                email["value"] = this._Email.Text;
                siteParams.Rows.Add(email);

                // create datarow for website address
                DataRow websiteUrl = siteParams.NewRow();
                websiteUrl["param"] = SiteParamConstants.FOOTER_WEBSITE;

                if (!String.IsNullOrWhiteSpace(this._WebsiteUrl.Text))
                { websiteUrl["value"] = this._HttpProtocols.SelectedValue + this._WebsiteUrl.Text; }
                else
                { websiteUrl["value"] = String.Empty; }

                siteParams.Rows.Add(websiteUrl);

                // save the site params
                Common.AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite,
                                                  AsentiaSessionState.UserCulture,
                                                  AsentiaSessionState.IdSiteUser,
                                                  AsentiaSessionState.IdSite,
                                                  siteParams);

                // find the right footer company name label control from the master page
                HtmlGenericControl displayCompanyName = this.Master.FindControl("FooterTagCompanyName") as HtmlGenericControl;

                // find the right footer email label control from the master page
                HtmlGenericControl displayEmail = this.Master.FindControl("FooterTagEmail") as HtmlGenericControl;

                // find the right footer website label control from the master page
                HtmlGenericControl displayWebsiteUrl = this.Master.FindControl("FooterTagWebsite") as HtmlGenericControl;

                // update the master page footer details on the page
                displayCompanyName.InnerHtml = Convert.ToString(companyName["value"]);
                displayEmail.InnerHtml = "<a href=\"mailto: " + Convert.ToString(email["value"]) + "\">" + Convert.ToString(email["value"]) + "</a>";
                displayWebsiteUrl.InnerHtml = "<a href=\"" + Convert.ToString(websiteUrl["value"]) + "\" target=\"_blank\">" + Convert.ToString(websiteUrl["value"]) + "</a>";

                // display success message to user
                this.DisplayFeedback(_GlobalResources.FooterInformationHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion
    }
}
