﻿/* ON LOAD */
/* 
Sets the methods to be called on the beginning of an async postback
and the end of an async postback, and sets the draggable and sortable
settings on the widget icons and widgets.
*/
$(document).ready(function () {

    ApplyEditableLayout();
});

/*
METHOD: ApplyEditableLayout
Method that places the jQuery sortable functionality to the editable layout.
*/
function ApplyEditableLayout() {
    
    // apply sortable to panels
    $("#EditableLayoutPanel").sortable({
        stop: function (event, ui) {
            SetLayoutOrderForHiddenField();
        },
        helper: function (event, ui) {
            return ui.clone().appendTo("body");
        }
    });

    // apply sortable for items
    $(".DroppableContainer").sortable({
        connectWith: ".DroppableContainer",
        cancel: "#EditorElement_Breadcrumb",
        stop: function (event, ui) {
            SetContainerItemsForHiddenFields();
        }
    });
}

/*
METHOD: SetLayoutOrderForHiddenField
Method that sets the layout order hidden field after layout "sort stop."
*/
function SetLayoutOrderForHiddenField() {

    var hiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#EditableLayoutPanel").children().each(function () {
        hiddenFieldValue += this.id.toString().replace("Editor_", "").replace("Outer", "") + "|";
    });

    // strip last character from hidden field value string
    hiddenFieldValue = hiddenFieldValue.substring(0, hiddenFieldValue.length - 1);

    // set hidden field value
    $("#LayoutOrderField").val(hiddenFieldValue);
}

/*
METHOD: SetContainerItemsForHiddenFields
Method that sets the items in each container column hidden field after "sort stop."
*/
function SetContainerItemsForHiddenFields() {

    // TOP BORDER LEFT

    var topBorderContainerLeftHiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#Editor_TopBorderColumnLeft").children().each(function () {
        if (this.id != "EditorElement_Breadcrumb")
        { topBorderContainerLeftHiddenFieldValue += this.id.toString().replace("EditorElement_", "") + "|"; }
    });

    // strip last character from hidden field value string
    topBorderContainerLeftHiddenFieldValue = topBorderContainerLeftHiddenFieldValue.substring(0, topBorderContainerLeftHiddenFieldValue.length - 1);

    // set hidden field value
    $("#TopBorderContainerLeftColumnItemsField").val(topBorderContainerLeftHiddenFieldValue);

    // TOP BORDER RIGHT

    var topBorderContainerRightHiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#Editor_TopBorderColumnRight").children().each(function () {
        if (this.id != "EditorElement_Breadcrumb")
        { topBorderContainerRightHiddenFieldValue += this.id.toString().replace("EditorElement_", "") + "|"; }
    });

    // strip last character from hidden field value string
    topBorderContainerRightHiddenFieldValue = topBorderContainerRightHiddenFieldValue.substring(0, topBorderContainerRightHiddenFieldValue.length - 1);

    // set hidden field value
    $("#TopBorderContainerRightColumnItemsField").val(topBorderContainerRightHiddenFieldValue);


    // MASTHEAD LEFT

    var mastheadContainerLeftHiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#Editor_MastheadColumnLeft").children().each(function () {
        if (this.id != "EditorElement_Breadcrumb")
        { mastheadContainerLeftHiddenFieldValue += this.id.toString().replace("EditorElement_", "") + "|"; }
    });

    // strip last character from hidden field value string
    mastheadContainerLeftHiddenFieldValue = mastheadContainerLeftHiddenFieldValue.substring(0, mastheadContainerLeftHiddenFieldValue.length - 1);

    // set hidden field value
    $("#MastheadContainerLeftColumnItemsField").val(mastheadContainerLeftHiddenFieldValue);

    // MASTHEAD RIGHT

    var mastheadContainerRightHiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#Editor_MastheadColumnRight").children().each(function () {
        if (this.id != "EditorElement_Breadcrumb")
        { mastheadContainerRightHiddenFieldValue += this.id.toString().replace("EditorElement_", "") + "|"; }
    });

    // strip last character from hidden field value string
    mastheadContainerRightHiddenFieldValue = mastheadContainerRightHiddenFieldValue.substring(0, mastheadContainerRightHiddenFieldValue.length - 1);

    // set hidden field value
    $("#MastheadContainerRightColumnItemsField").val(mastheadContainerRightHiddenFieldValue);


    // NAVIGATION LEFT

    var navigationContainerLeftHiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#Editor_NavigationColumnLeft").children().each(function () {
        if (this.id != "EditorElement_Breadcrumb")
        { navigationContainerLeftHiddenFieldValue += this.id.toString().replace("EditorElement_", "") + "|"; }
    });

    // strip last character from hidden field value string
    navigationContainerLeftHiddenFieldValue = navigationContainerLeftHiddenFieldValue.substring(0, navigationContainerLeftHiddenFieldValue.length - 1);

    // set hidden field value
    $("#NavigationContainerLeftColumnItemsField").val(navigationContainerLeftHiddenFieldValue);

    // NAVIGATION RIGHT

    var navigationContainerRightHiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#Editor_NavigationColumnRight").children().each(function () {
        if (this.id != "EditorElement_Breadcrumb")
        { navigationContainerRightHiddenFieldValue += this.id.toString().replace("EditorElement_", "") + "|"; }
    });

    // strip last character from hidden field value string
    navigationContainerRightHiddenFieldValue = navigationContainerRightHiddenFieldValue.substring(0, navigationContainerRightHiddenFieldValue.length - 1);

    // set hidden field value
    $("#NavigationContainerRightColumnItemsField").val(navigationContainerRightHiddenFieldValue);


    // BREADCRUMB LEFT

    var breadcrumbContainerLeftHiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#Editor_BreadcrumbColumnLeft").children().each(function () {
        if (this.id != "EditorElement_Breadcrumb")
        { breadcrumbContainerLeftHiddenFieldValue += this.id.toString().replace("EditorElement_", "") + "|"; }
    });

    // strip last character from hidden field value string
    breadcrumbContainerLeftHiddenFieldValue = breadcrumbContainerLeftHiddenFieldValue.substring(0, breadcrumbContainerLeftHiddenFieldValue.length - 1);

    // set hidden field value
    $("#BreadcrumbContainerLeftColumnItemsField").val(breadcrumbContainerLeftHiddenFieldValue);

    // BREADCRUMB RIGHT

    var breadcrumbContainerRightHiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#Editor_BreadcrumbColumnRight").children().each(function () {
        if (this.id != "EditorElement_Breadcrumb")
        { breadcrumbContainerRightHiddenFieldValue += this.id.toString().replace("EditorElement_", "") + "|"; }
    });

    // strip last character from hidden field value string
    breadcrumbContainerRightHiddenFieldValue = breadcrumbContainerRightHiddenFieldValue.substring(0, breadcrumbContainerRightHiddenFieldValue.length - 1);

    // set hidden field value
    $("#BreadcrumbContainerRightColumnItemsField").val(breadcrumbContainerRightHiddenFieldValue);


    // FOOTER LEFT

    var footerContainerLeftHiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#Editor_FooterColumnLeft").children().each(function () {
        if (this.id != "EditorElement_Breadcrumb")
        { footerContainerLeftHiddenFieldValue += this.id.toString().replace("EditorElement_", "") + "|"; }
    });

    // strip last character from hidden field value string
    footerContainerLeftHiddenFieldValue = footerContainerLeftHiddenFieldValue.substring(0, footerContainerLeftHiddenFieldValue.length - 1);

    // set hidden field value
    $("#FooterContainerLeftColumnItemsField").val(footerContainerLeftHiddenFieldValue);

    // FOOTER RIGHT

    var footerContainerRightHiddenFieldValue = "";

    // go through each child of the layout panel and add to hidden field value string
    $("#Editor_FooterColumnRight").children().each(function () {
        if (this.id != "EditorElement_Breadcrumb")
        { footerContainerRightHiddenFieldValue += this.id.toString().replace("EditorElement_", "") + "|"; }
    });

    // strip last character from hidden field value string
    footerContainerRightHiddenFieldValue = footerContainerRightHiddenFieldValue.substring(0, footerContainerRightHiddenFieldValue.length - 1);

    // set hidden field value
    $("#FooterContainerRightColumnItemsField").val(footerContainerRightHiddenFieldValue);
}