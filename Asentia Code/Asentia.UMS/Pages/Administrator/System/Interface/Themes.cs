﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

using System.IO;
using System.Xml;

using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.UMS.Pages.Administrator.System.Interface
{
    public class Themes : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ThemesFormContentWrapperContainer;
        public Panel ThemesWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel ThemesFormContentContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private DropDownList _SelectThemeStyling;
        private DropDownList _SelectThemeIconset;

        private string _CurrentTheme;
        private string _CurrentThemePreview;
        private string _CurrentIconset;
        private string _CurrentIconsetPreview;
        
        private ModalPopup _ThemeSampleScreensModal;
        private Button _ThemeSampleScreensModalHiddenLaunchButton;

        private Button _SaveButton;
        private Button _PreviewButton;
        private Button _CancelButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // execute the base
            base.OnPreRender(e);

            // registers the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Themes), "Asentia.UMS.Pages.Administrator.System.Interface.Themes.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_CSSEditor) && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_ImageEditor))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/system/interface/Themes.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Themes));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.InterfaceAndLayout, _GlobalResources.Themes, ImageFiles.GetIconPath(ImageFiles.ICON_THEMES, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.ThemesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ThemesWrapperContainer.CssClass = "FormContentContainer";

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.ConfigureYourPortalsThemeUsingTheFormBelow, true);

            // get the current theme and iconset, and selected preview theme and iconset
            this._CurrentTheme = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.THEME_ACTIVE);
            this._CurrentIconset = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ICONSET_ACTIVE);

            this._CurrentThemePreview = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.THEME_PREVIEW);
            this._CurrentIconsetPreview = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ICONSET_PREVIEW);

            // build the input controls
            this._BuildInputControls();

            // build the theme sample screens modal
            this._BuildThemeSampleScreensModal();

            // build the actions panel
            this._BuildActionsPanel();
        }
        #endregion        

        #region _BuildInputControls
        /// <summary>
        /// Builds the input controls on the page.
        /// </summary>
        private void _BuildInputControls()
        {
            // styling
            List<Control> themeStylingInputControls = new List<Control>();
            
            // selector
            Panel themeStylingSelectorPanel = new Panel();
            themeStylingSelectorPanel.ID = "ThemeStylingSelectorPanel";
            themeStylingSelectorPanel.CssClass = "ThemeSelectorPanel";
            themeStylingInputControls.Add(themeStylingSelectorPanel);

            this._SelectThemeStyling = new DropDownList();
            this._SelectThemeStyling.ID = "ThemeStyling_Field";
            this._SelectThemeStyling.DataTextField = "name";
            this._SelectThemeStyling.DataValueField = "folder";            
            this._SelectThemeStyling.DataSource = this._GetAvailableThemesForDropDown();
            this._SelectThemeStyling.DataBind();
            this._SelectThemeStyling.Attributes.Add("onchange", "ChangeThemeSamples();");

            if (AsentiaSessionState.IsThemePreviewMode)
            { this._SelectThemeStyling.SelectedValue = this._CurrentThemePreview; }
            else
            { this._SelectThemeStyling.SelectedValue = this._CurrentTheme; }

            themeStylingSelectorPanel.Controls.Add(this._SelectThemeStyling);

            // samples
            Panel themeStylingSamplesPanel = new Panel();
            themeStylingSamplesPanel.ID = "ThemeStylingSamplesPanel";
            themeStylingSamplesPanel.CssClass = "ThemeSamplesPanel";            
            themeStylingInputControls.Add(themeStylingSamplesPanel);

            // samples text
            Panel themeStylingSamplesTextContainer = new Panel();
            themeStylingSamplesTextContainer.CssClass = "ThemeSamplesTextContainer";
            themeStylingSamplesTextContainer.Controls.Add(new Literal() { Text = _GlobalResources.Samples + ": " });
            themeStylingSamplesPanel.Controls.Add(themeStylingSamplesTextContainer);

            // samples images
            Panel themeStylingSamplesImagesContainer = new Panel();
            themeStylingSamplesImagesContainer.CssClass = "ThemeSamplesImagesContainer";
            themeStylingSamplesPanel.Controls.Add(themeStylingSamplesImagesContainer);

            // get the screenshot paths
            string dashboardScreenshotPath = String.Empty;
            string formScreenshotPath = String.Empty;
            string gridScreenshotPath = String.Empty;

            if (
                Directory.Exists(MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_ROOT + this._SelectThemeStyling.SelectedValue + "/screenshots"))
                && File.Exists(MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_ROOT + this._SelectThemeStyling.SelectedValue + "/screenshots/Dashboard.png"))
               )
            {
                dashboardScreenshotPath = SitePathConstants.SITE_TEMPLATE_THEMES_ROOT + this._SelectThemeStyling.SelectedValue + "/screenshots/Dashboard.png";
                formScreenshotPath = SitePathConstants.SITE_TEMPLATE_THEMES_ROOT + this._SelectThemeStyling.SelectedValue + "/screenshots/Form.png";
                gridScreenshotPath = SitePathConstants.SITE_TEMPLATE_THEMES_ROOT + this._SelectThemeStyling.SelectedValue + "/screenshots/Grid.png";
            }
            else
            {
                dashboardScreenshotPath = SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_ROOT + this._SelectThemeStyling.SelectedValue + "/screenshots/Dashboard.png";
                formScreenshotPath = SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_ROOT + this._SelectThemeStyling.SelectedValue + "/screenshots/Form.png";
                gridScreenshotPath = SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_ROOT + this._SelectThemeStyling.SelectedValue + "/screenshots/Grid.png";
            }

            // images

            // dashboard image
            HyperLink dashboardImageLink = new HyperLink();
            dashboardImageLink.ID = "DashboardSampleScreenImageLink";
            dashboardImageLink.CssClass = "SampleScreenImageLink";
            dashboardImageLink.ImageUrl = dashboardScreenshotPath;
            dashboardImageLink.NavigateUrl = "javascript:void(0);";
            dashboardImageLink.Attributes.Add("onclick", "ShowThemeSampleScreens('" + dashboardScreenshotPath + "');");
            themeStylingSamplesImagesContainer.Controls.Add(dashboardImageLink);

            // form image
            HyperLink formImageLink = new HyperLink();
            formImageLink.ID = "FormSampleScreenImageLink";
            formImageLink.CssClass = "SampleScreenImageLink";
            formImageLink.ImageUrl = formScreenshotPath;
            formImageLink.NavigateUrl = "javascript:void(0);";
            formImageLink.Attributes.Add("onclick", "ShowThemeSampleScreens('" + formScreenshotPath + "');");
            themeStylingSamplesImagesContainer.Controls.Add(formImageLink);

            // dashboard image
            HyperLink gridImageLink = new HyperLink();
            gridImageLink.ID = "GridSampleScreenImageLink";
            gridImageLink.CssClass = "SampleScreenImageLink";
            gridImageLink.ImageUrl = gridScreenshotPath;
            gridImageLink.NavigateUrl = "javascript:void(0);";
            gridImageLink.Attributes.Add("onclick", "ShowThemeSampleScreens('" + gridScreenshotPath + "');");
            themeStylingSamplesImagesContainer.Controls.Add(gridImageLink);
            
            // attach the form field to the form
            this.ThemesFormContentContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ThemeStyling",
                                                                                                        _GlobalResources.Styling,
                                                                                                        themeStylingInputControls,
                                                                                                        false,
                                                                                                        true));

            // ICONSETS
            List<Control> themeIconsetInputControls = new List<Control>();

            // selector
            Panel themeIconsetSelectorPanel = new Panel();
            themeIconsetSelectorPanel.ID = "ThemeIconsetSelectorPanel";
            themeIconsetSelectorPanel.CssClass = "ThemeSelectorPanel";
            themeIconsetInputControls.Add(themeIconsetSelectorPanel);

            this._SelectThemeIconset = new DropDownList();
            this._SelectThemeIconset.ID = "ThemeIconset_Field";
            this._SelectThemeIconset.DataTextField = "name";
            this._SelectThemeIconset.DataValueField = "folder";
            this._SelectThemeIconset.DataSource = this._GetAvailableIconsetsForDropDown();
            this._SelectThemeIconset.DataBind();
            this._SelectThemeIconset.Attributes.Add("onchange", "ChangeIconsetSamples();");

            if (AsentiaSessionState.IsThemePreviewMode)
            { this._SelectThemeIconset.SelectedValue = this._CurrentIconsetPreview; }
            else
            { this._SelectThemeIconset.SelectedValue = this._CurrentIconset; }            

            themeIconsetSelectorPanel.Controls.Add(this._SelectThemeIconset);

            // samples
            Panel themeIconsetSamplesPanel = new Panel();
            themeIconsetSamplesPanel.ID = "ThemeIconsetSamplesPanel";            
            themeIconsetSamplesPanel.CssClass = "ThemeSamplesPanel";
            themeIconsetInputControls.Add(themeIconsetSamplesPanel);

            // samples text
            Panel themeIconsetSamplesTextContainer = new Panel();
            themeIconsetSamplesTextContainer.CssClass = "ThemeSamplesTextContainer";
            themeIconsetSamplesTextContainer.Controls.Add(new Literal() { Text = _GlobalResources.Samples + ": " });
            themeIconsetSamplesPanel.Controls.Add(themeIconsetSamplesTextContainer);

            // samples images
            Panel themeIconsetSamplesImagesContainer = new Panel();
            themeIconsetSamplesImagesContainer.ID = 
            themeIconsetSamplesImagesContainer.CssClass = "ThemeSamplesImagesContainer";
            themeIconsetSamplesPanel.Controls.Add(themeIconsetSamplesImagesContainer);

            // get the icon paths
            string iconPath1 = String.Empty;
            string iconPath2 = String.Empty;
            string iconPath3 = String.Empty;
            string iconPath4 = String.Empty;
            string iconPath5 = String.Empty;
            string iconPath6 = String.Empty;
            string iconPath7 = String.Empty;
            string iconPath8 = String.Empty;
            string iconPath9 = String.Empty;
            string iconPath10 = String.Empty;

            if (Directory.Exists(MapPathSecure(SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64")))
            {
                iconPath1 = SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/administrator.png";
                iconPath2 = SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/userm.png";
                iconPath3 = SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/system.localization.png";
                iconPath4 = SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/report.png";
                iconPath5 = SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/course.png";
                iconPath6 = SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/email.png";
                iconPath7 = SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/session.png";
                iconPath8 = SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/ojt.png";
                iconPath9 = SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/information.png";
                iconPath10 = SitePathConstants.SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/instructor.png";
            }
            else
            {
                iconPath1 = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/administrator.png";
                iconPath2 = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/userm.png";
                iconPath3 = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/system.localization.png";
                iconPath4 = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/report.png";
                iconPath5 = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/course.png";
                iconPath6 = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/email.png";
                iconPath7 = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/session.png";
                iconPath8 = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/ojt.png";
                iconPath9 = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/information.png";
                iconPath10 = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + this._SelectThemeIconset.SelectedValue + "/64/instructor.png";
            }

            // images
            
            // icon 1
            Image icon1 = new Image();
            icon1.ImageUrl = iconPath1;
            icon1.CssClass = "MediumIcon SampleIconsetImage";
            icon1.AlternateText = _GlobalResources.Administrator;
            themeIconsetSamplesImagesContainer.Controls.Add(icon1);

            // icon 2
            Image icon2 = new Image();
            icon2.ImageUrl = iconPath2;
            icon2.CssClass = "MediumIcon SampleIconsetImage";
            icon2.AlternateText = _GlobalResources.User;
            themeIconsetSamplesImagesContainer.Controls.Add(icon2);

            // icon 3
            Image icon3 = new Image();
            icon3.ImageUrl = iconPath3;
            icon3.CssClass = "MediumIcon SampleIconsetImage";
            icon3.AlternateText = _GlobalResources.Global;
            themeIconsetSamplesImagesContainer.Controls.Add(icon3);

            // icon 4
            Image icon4 = new Image();
            icon4.ImageUrl = iconPath4;
            icon4.CssClass = "MediumIcon SampleIconsetImage";
            icon4.AlternateText = _GlobalResources.Report;
            themeIconsetSamplesImagesContainer.Controls.Add(icon4);

            // icon 5
            Image icon5 = new Image();
            icon5.ImageUrl = iconPath5;
            icon5.CssClass = "MediumIcon SampleIconsetImage";
            icon5.AlternateText = _GlobalResources.Course;
            themeIconsetSamplesImagesContainer.Controls.Add(icon5);

            // icon 6
            Image icon6 = new Image();
            icon6.ImageUrl = iconPath6;
            icon6.CssClass = "MediumIcon SampleIconsetImage";
            icon6.AlternateText = _GlobalResources.Email;
            themeIconsetSamplesImagesContainer.Controls.Add(icon6);

            // icon 7
            Image icon7 = new Image();
            icon7.ImageUrl = iconPath7;
            icon7.CssClass = "MediumIcon SampleIconsetImage";
            icon7.AlternateText = _GlobalResources.Session;
            themeIconsetSamplesImagesContainer.Controls.Add(icon7);

            // icon 8
            Image icon8 = new Image();
            icon8.ImageUrl = iconPath8;
            icon8.CssClass = "MediumIcon SampleIconsetImage";
            icon8.AlternateText = _GlobalResources.OJT;
            themeIconsetSamplesImagesContainer.Controls.Add(icon8);

            // icon 9
            Image icon9 = new Image();
            icon9.ImageUrl = iconPath9;
            icon9.CssClass = "MediumIcon SampleIconsetImage";
            icon9.AlternateText = _GlobalResources.Information;
            themeIconsetSamplesImagesContainer.Controls.Add(icon9);

            // icon 10
            Image icon10 = new Image();
            icon10.ImageUrl = iconPath10;
            icon10.CssClass = "MediumIcon SampleIconsetImage";
            icon10.AlternateText = _GlobalResources.Instructor;
            themeIconsetSamplesImagesContainer.Controls.Add(icon10);            

            // attach the form field to the form
            this.ThemesFormContentContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ThemeIconset",
                                                                                                        _GlobalResources.Icons,
                                                                                                        themeIconsetInputControls,
                                                                                                        false,
                                                                                                        true));
        }
        #endregion

        #region _BuildThemeSampleScreensModal
        /// <summary>
        /// Builds the modal popup for displaying theme sample screens.
        /// </summary>
        private void _BuildThemeSampleScreensModal()
        {
            // THE SUPPORTING ELEMENTS - TRIGGERS, HIDDEN FIELDS, ETC.

            this._ThemeSampleScreensModalHiddenLaunchButton = new Button();
            this._ThemeSampleScreensModalHiddenLaunchButton.ID = "ThemeSampleScreensModalHiddenLaunchButton";
            this._ThemeSampleScreensModalHiddenLaunchButton.Style.Add("display", "none");

            // add trigger button to container
            this.ThemesFormContentContainer.Controls.Add(this._ThemeSampleScreensModalHiddenLaunchButton);

            // THE MODAL

            // set modal properties
            this._ThemeSampleScreensModal = new ModalPopup("ThemeSampleScreensModal");
            this._ThemeSampleScreensModal.Type = ModalPopupType.Information;
            this._ThemeSampleScreensModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_IMAGE, ImageFiles.EXT_PNG);
            this._ThemeSampleScreensModal.HeaderIconAlt = _GlobalResources.SampleScreens;
            this._ThemeSampleScreensModal.HeaderText = _GlobalResources.SampleScreens;
            this._ThemeSampleScreensModal.SubmitButton.Visible = false;
            this._ThemeSampleScreensModal.CloseButton.Visible = false;

            this._ThemeSampleScreensModal.TargetControlID = this._ThemeSampleScreensModalHiddenLaunchButton.ID;

            // build the modal body
            Panel themeSampleScreensModalContentPanel = new Panel();
            themeSampleScreensModalContentPanel.ID = "ThemeSampleScreensModalContentPanel";
            this._ThemeSampleScreensModal.AddControlToBody(themeSampleScreensModalContentPanel);

            // add modal to container
            this.ThemesFormContentContainer.Controls.Add(this._ThemeSampleScreensModal);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._ApplyThemeChanges);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // preview button
            this._PreviewButton = new Button();
            this._PreviewButton.ID = "PreviewButton";
            this._PreviewButton.CssClass = "Button ActionButton";
            this._PreviewButton.Text = _GlobalResources.PreviewChanges;
            this._PreviewButton.Command += new CommandEventHandler(this._SetPreviewTheme);
            this.ActionsPanel.Controls.Add(this._PreviewButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/dashboard");
        }
        #endregion
    }
}
