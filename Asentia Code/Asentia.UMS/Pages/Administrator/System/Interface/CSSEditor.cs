﻿using System;
using System.Collections;
using System.IO;
using Asentia.Common;
using Asentia.Controls;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.UMS.Pages.Administrator.System.Interface
{
    public class CSSEditor : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CSSEditorFormContentWrapperContainer;
        public Panel PageInstructionsPanel;
        public UpdatePanel CSSFileUpdatePanel;
        public CSSFileEditor CSSEditorControl;
        #endregion

        #region Page_Load
        /// <summary>
        /// Performs the action when page loads.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check to ensure xAPI endpoints is enabled on the portal
            if (!AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CSSEDITOR_ENABLE) ?? false)
            { Response.Redirect("/"); }

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_CSSEditor))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/system/interface/CSSEditor.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.CSSEditor));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.InterfaceAndLayout, _GlobalResources.CSSEditor, ImageFiles.GetIconPath(ImageFiles.ICON_CSS, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CSSEditorFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.CustomizeCSSFilesForThePortalsCurrentThemeByUsingTheOptionsBelow, true);            

            // build actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // save button
            this.CSSEditorControl.SaveButton = new Button();
            this.CSSEditorControl.SaveButton.ID = "SaveButton";
            this.CSSEditorControl.SaveButton.CssClass = "Button ActionButton SaveButton";
            this.CSSEditorControl.SaveButton.Text = _GlobalResources.SaveChanges;
            this.CSSEditorControl.SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);

            // revert to default button
            this.CSSEditorControl.RevertToDefaultButton = new Button();
            this.CSSEditorControl.RevertToDefaultButton.ID = "RevertToDefaultButton";
            this.CSSEditorControl.RevertToDefaultButton.CssClass = "Button NonActionButton";
            this.CSSEditorControl.RevertToDefaultButton.Text = _GlobalResources.RevertToDefault;
            this.CSSEditorControl.RevertToDefaultButton.Command += new CommandEventHandler(this._RevertToDefaultButton_Command);

            // cancel button
            this.CSSEditorControl.CancelButton = new Button();
            this.CSSEditorControl.CancelButton.ID = "CancelButton";
            this.CSSEditorControl.CancelButton.CssClass = "Button NonActionButton";
            this.CSSEditorControl.CancelButton.Text = _GlobalResources.Cancel;
            this.CSSEditorControl.CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);

        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        protected void _SaveButton_Command(object sender, EventArgs e)
        {
            try
            {
                // if file is saved successfully
                string savedFile = this.CSSEditorControl.SaveFile();

                // enable the revert button
                this.CSSEditorControl.RevertToDefaultButton.Enabled = true;
                this.CSSEditorControl.RevertToDefaultButton.CssClass = "Button NonActionButton";

                // display success message to user
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, String.Format(_GlobalResources.CSSFileHasBeenSavedSuccessfully, savedFile), false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display error message to user if any exception occur
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _RevertToDefaultButton_Command
        /// <summary>
        /// Reverts the css back to the default.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        protected void _RevertToDefaultButton_Command(object sender, EventArgs e)
        {
            try
            {                
                // delete the uploaded file
                string deleteFile = this.CSSEditorControl.DeleteFile();

                this.CSSFileUpdatePanel.Update();

                // enable actions panel, and disable revert to default button
                this.CSSEditorControl.RevertToDefaultButton.Enabled = false;
                this.CSSEditorControl.RevertToDefaultButton.CssClass = "Button NonActionButton DisabledButton";

                // display success message to user
                this.DisplayFeedback(String.Format(_GlobalResources.CSSFileWasSuccessfullyRevertedToDefault, deleteFile), false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display error message to user if any exception occur
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        protected void _CancelButton_Command(object sender, EventArgs e)
        {
            // redirect user to dashboard area
            Response.Redirect("~/dashboard/");
        }
        #endregion
    }
}
