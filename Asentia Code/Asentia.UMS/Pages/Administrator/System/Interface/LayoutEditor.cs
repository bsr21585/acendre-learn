﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.UMS.Pages.Administrator.System.Interface
{
    public class LayoutEditor : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel PageInstructionsPanel;
        public Panel EditableLayoutPanel;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private Button _SaveButton;
        private Button _RevertToDefaultButton;
        private bool _EnableRevertToDefaultButton = false;
        private Button _CancelButton;
        private HiddenField _LayoutOrder;
        private HiddenField _TopBorderContainerLeftColumnItems;
        private HiddenField _TopBorderContainerRightColumnItems;
        private HiddenField _MastheadContainerLeftColumnItems;
        private HiddenField _MastheadContainerRightColumnItems;
        private HiddenField _NavigationContainerLeftColumnItems;
        private HiddenField _NavigationContainerRightColumnItems;
        private HiddenField _BreadcrumbContainerLeftColumnItems;
        private HiddenField _BreadcrumbContainerRightColumnItems;
        private HiddenField _FooterContainerLeftColumnItems;
        private HiddenField _FooterContainerRightColumnItems;
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            /*
            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/system/interface/LayoutEditor.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.InterfaceLayoutEditor));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.InterfaceLayoutEditor, ImageFiles.GetIconPath(ImageFiles.ICON_INTERFACE,
                                                                                                                            ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.ModifyTheLayoutOfTheWebsiteUsingTheOptionsBelow, true);

            // build the layout editor
            this._BuildLayoutEditor();

            // build the actions panel
            this._BuildActionsPanel();
            */
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // execute the base
            base.OnPreRender(e);

            /*
            // registers the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(LayoutEditor), "Asentia.UMS.Pages.Administrator.System.Interface.LayoutEditor.js");
            */
        }
        #endregion

        #region _BuildLayoutEditor
        /// <summary>
        /// Builds the layout editor for the page.
        /// </summary>
        private void _BuildLayoutEditor()
        {
            // clear controls from panel
            this.EditableLayoutPanel.Controls.Clear();

            // get xml document for container items
            XmlDocument globalItemsDocument = new XmlDocument();

            if (File.Exists(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + "GlobalControls.xml")))
            { 
                globalItemsDocument.Load(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + "GlobalControls.xml"));
                this._EnableRevertToDefaultButton = true;
            }
            else
            { globalItemsDocument.Load(Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_MASTERPAGE + "GlobalControls.xml")); }

            // read the master page
            StreamReader sr;

            if (File.Exists(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + "GlobalSite.Master")))
            { 
                sr = new StreamReader(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + "GlobalSite.Master"));
                this._EnableRevertToDefaultButton = true;
            }
            else
            { sr = new StreamReader(Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_MASTERPAGE + "GlobalSite.Master")); }

            string line;
            string layoutOrder = String.Empty;

            // loop through each line of the master page to build the containers
            while ((line = sr.ReadLine()) != null)
            {
                if (line.Contains("ID=\"TopBorderContainer\""))
                {
                    // get top container items and populate hidden fields for items
                    XmlNodeList topBorderContainerItems = this._GetElementsForContainer(globalItemsDocument, "TopBorderContainer");
                    string leftColumnItems = String.Empty;
                    string rightColumnItems = String.Empty;

                    if (topBorderContainerItems != null)
                    {
                        foreach (XmlNode topBorderContainerItem in topBorderContainerItems)
                        {
                            if (topBorderContainerItem.Attributes["columnPlacement"].Value == "left")
                            { leftColumnItems += topBorderContainerItem.Attributes["type"].Value + "|"; }
                            else if (topBorderContainerItem.Attributes["columnPlacement"].Value == "right")
                            { rightColumnItems += topBorderContainerItem.Attributes["type"].Value + "|"; }
                            else
                            { }
                        }
                    }

                    // trim the trailing | from the strings
                    if (!String.IsNullOrWhiteSpace(leftColumnItems))
                    { leftColumnItems = leftColumnItems.Substring(0, leftColumnItems.Length - 1); }

                    if (!String.IsNullOrWhiteSpace(rightColumnItems))
                    { rightColumnItems = rightColumnItems.Substring(0, rightColumnItems.Length - 1); }

                    // set and attach the left column hidden field
                    this._TopBorderContainerLeftColumnItems = new HiddenField();
                    this._TopBorderContainerLeftColumnItems.ID = "TopBorderContainerLeftColumnItemsField";
                    this._TopBorderContainerLeftColumnItems.Value = leftColumnItems;
                    this.PageContentContainer.Controls.Add(this._TopBorderContainerLeftColumnItems);

                    // set and attach the right column hidden field
                    this._TopBorderContainerRightColumnItems = new HiddenField();
                    this._TopBorderContainerRightColumnItems.ID = "TopBorderContainerRightColumnItemsField";
                    this._TopBorderContainerRightColumnItems.Value = rightColumnItems;
                    this.PageContentContainer.Controls.Add(this._TopBorderContainerRightColumnItems);

                    // attach panel to container
                    this.EditableLayoutPanel.Controls.Add(this._TopBorderContainerForEditor());

                    // add panel to layout order hidden field
                    layoutOrder += "TopBorderContainer" + "|";
                }
                else if (line.Contains("ID=\"MastheadContainer\""))
                {
                    // get masthead container items and populate hidden fields for items
                    XmlNodeList mastheadContainerItems = this._GetElementsForContainer(globalItemsDocument, "MastheadContainer");
                    string leftColumnItems = String.Empty;
                    string rightColumnItems = String.Empty;

                    if (mastheadContainerItems != null)
                    {
                        foreach (XmlNode mastheadContainerItem in mastheadContainerItems)
                        {
                            if (mastheadContainerItem.Attributes["columnPlacement"].Value == "left")
                            { leftColumnItems += mastheadContainerItem.Attributes["type"].Value + "|"; }
                            else if (mastheadContainerItem.Attributes["columnPlacement"].Value == "right")
                            { rightColumnItems += mastheadContainerItem.Attributes["type"].Value + "|"; }
                            else
                            { }
                        }
                    }

                    // trim the trailing | from the strings
                    if (!String.IsNullOrWhiteSpace(leftColumnItems))
                    { leftColumnItems = leftColumnItems.Substring(0, leftColumnItems.Length - 1); }

                    if (!String.IsNullOrWhiteSpace(rightColumnItems))
                    { rightColumnItems = rightColumnItems.Substring(0, rightColumnItems.Length - 1); }

                    // set and attach the left column hidden field
                    this._MastheadContainerLeftColumnItems = new HiddenField();
                    this._MastheadContainerLeftColumnItems.ID = "MastheadContainerLeftColumnItemsField";
                    this._MastheadContainerLeftColumnItems.Value = leftColumnItems;
                    this.PageContentContainer.Controls.Add(this._MastheadContainerLeftColumnItems);

                    // set and attach the right column hidden field
                    this._MastheadContainerRightColumnItems = new HiddenField();
                    this._MastheadContainerRightColumnItems.ID = "MastheadContainerRightColumnItemsField";
                    this._MastheadContainerRightColumnItems.Value = rightColumnItems;
                    this.PageContentContainer.Controls.Add(this._MastheadContainerRightColumnItems);

                    // attach panel to container
                    this.EditableLayoutPanel.Controls.Add(this._MastheadContainerForEditor());

                    // add panel to layout order hidden field
                    layoutOrder += "MastheadContainer" + "|";
                }
                else if (line.Contains("ID=\"NavigationContainer\""))
                {
                    // get navigation container items and populate hidden fields for items
                    XmlNodeList navigationContainerItems = this._GetElementsForContainer(globalItemsDocument, "NavigationContainer");
                    string leftColumnItems = String.Empty;
                    string rightColumnItems = String.Empty;

                    if (navigationContainerItems != null)
                    {
                        foreach (XmlNode navigationContainerItem in navigationContainerItems)
                        {
                            if (navigationContainerItem.Attributes["columnPlacement"].Value == "left")
                            { leftColumnItems += navigationContainerItem.Attributes["type"].Value + "|"; }
                            else if (navigationContainerItem.Attributes["columnPlacement"].Value == "right")
                            { rightColumnItems += navigationContainerItem.Attributes["type"].Value + "|"; }
                            else
                            { }
                        }
                    }

                    // trim the trailing | from the strings
                    if (!String.IsNullOrWhiteSpace(leftColumnItems))
                    { leftColumnItems = leftColumnItems.Substring(0, leftColumnItems.Length - 1); }

                    if (!String.IsNullOrWhiteSpace(rightColumnItems))
                    { rightColumnItems = rightColumnItems.Substring(0, rightColumnItems.Length - 1); }

                    // set and attach the left column hidden field
                    this._NavigationContainerLeftColumnItems = new HiddenField();
                    this._NavigationContainerLeftColumnItems.ID = "NavigationContainerLeftColumnItemsField";
                    this._NavigationContainerLeftColumnItems.Value = leftColumnItems;
                    this.PageContentContainer.Controls.Add(this._NavigationContainerLeftColumnItems);

                    // set and attach the right column hidden field
                    this._NavigationContainerRightColumnItems = new HiddenField();
                    this._NavigationContainerRightColumnItems.ID = "NavigationContainerRightColumnItemsField";
                    this._NavigationContainerRightColumnItems.Value = rightColumnItems;
                    this.PageContentContainer.Controls.Add(this._NavigationContainerRightColumnItems);

                    // attach panel to container
                    this.EditableLayoutPanel.Controls.Add(this._NavigationContainerForEditor());

                    // add panel to layout order hidden field
                    layoutOrder += "NavigationContainer" + "|";
                }
                else if (line.Contains("ID=\"BreadcrumbContainer\""))
                {
                    // get breadcrumb container items and populate hidden fields for items
                    XmlNodeList breadcrumbContainerItems = this._GetElementsForContainer(globalItemsDocument, "BreadcrumbContainer");
                    string leftColumnItems = String.Empty;
                    string rightColumnItems = String.Empty;

                    if (breadcrumbContainerItems != null)
                    {
                        foreach (XmlNode breadcrumbContainerItem in breadcrumbContainerItems)
                        {
                            if (breadcrumbContainerItem.Attributes["columnPlacement"].Value == "left")
                            { leftColumnItems += breadcrumbContainerItem.Attributes["type"].Value + "|"; }
                            else if (breadcrumbContainerItem.Attributes["columnPlacement"].Value == "right")
                            { rightColumnItems += breadcrumbContainerItem.Attributes["type"].Value + "|"; }
                            else
                            { }
                        }
                    }

                    // trim the trailing | from the strings
                    if (!String.IsNullOrWhiteSpace(leftColumnItems))
                    { leftColumnItems = leftColumnItems.Substring(0, leftColumnItems.Length - 1); }

                    if (!String.IsNullOrWhiteSpace(rightColumnItems))
                    { rightColumnItems = rightColumnItems.Substring(0, rightColumnItems.Length - 1); }

                    // set and attach the left column hidden field
                    this._BreadcrumbContainerLeftColumnItems = new HiddenField();
                    this._BreadcrumbContainerLeftColumnItems.ID = "BreadcrumbContainerLeftColumnItemsField";
                    this._BreadcrumbContainerLeftColumnItems.Value = leftColumnItems;
                    this.PageContentContainer.Controls.Add(this._BreadcrumbContainerLeftColumnItems);

                    // set and attach the right column hidden field
                    this._BreadcrumbContainerRightColumnItems = new HiddenField();
                    this._BreadcrumbContainerRightColumnItems.ID = "BreadcrumbContainerRightColumnItemsField";
                    this._BreadcrumbContainerRightColumnItems.Value = rightColumnItems;
                    this.PageContentContainer.Controls.Add(this._BreadcrumbContainerRightColumnItems);

                    // attach panel to container
                    this.EditableLayoutPanel.Controls.Add(this._BreadcrumbContainerForEditor());

                    // add panel to layout order hidden field
                    layoutOrder += "BreadcrumbContainer" + "|";
                }
                else if (line.Contains("ID=\"ContentContainer\""))
                {
                    // CONTENT CONTAINER DOES NOT HAVE ITEMS PLACED WITHIN IT

                    // attach panel to container
                    this.EditableLayoutPanel.Controls.Add(this._ContentContainerForEditor());

                    // add panel to layout order hidden field
                    layoutOrder += "ContentContainer" + "|";
                }
                else if (line.Contains("ID=\"FooterContainer\""))
                {
                    // get footer container items and populate hidden fields for items
                    XmlNodeList footerContainerItems = this._GetElementsForContainer(globalItemsDocument, "FooterContainer");
                    string leftColumnItems = String.Empty;
                    string rightColumnItems = String.Empty;

                    if (footerContainerItems != null)
                    {
                        foreach (XmlNode footerContainerItem in footerContainerItems)
                        {
                            if (footerContainerItem.Attributes["columnPlacement"].Value == "left")
                            { leftColumnItems += footerContainerItem.Attributes["type"].Value + "|"; }
                            else if (footerContainerItem.Attributes["columnPlacement"].Value == "right")
                            { rightColumnItems += footerContainerItem.Attributes["type"].Value + "|"; }
                            else
                            { }
                        }
                    }

                    // trim the trailing | from the strings
                    if (!String.IsNullOrWhiteSpace(leftColumnItems))
                    { leftColumnItems = leftColumnItems.Substring(0, leftColumnItems.Length - 1); }

                    if (!String.IsNullOrWhiteSpace(rightColumnItems))
                    { rightColumnItems = rightColumnItems.Substring(0, rightColumnItems.Length - 1); }

                    // set and attach the left column hidden field
                    this._FooterContainerLeftColumnItems = new HiddenField();
                    this._FooterContainerLeftColumnItems.ID = "FooterContainerLeftColumnItemsField";
                    this._FooterContainerLeftColumnItems.Value = leftColumnItems;
                    this.PageContentContainer.Controls.Add(this._FooterContainerLeftColumnItems);

                    // set and attach the right column hidden field
                    this._FooterContainerRightColumnItems = new HiddenField();
                    this._FooterContainerRightColumnItems.ID = "FooterContainerRightColumnItemsField";
                    this._FooterContainerRightColumnItems.Value = rightColumnItems;
                    this.PageContentContainer.Controls.Add(this._FooterContainerRightColumnItems);

                    // attach panel to container
                    this.EditableLayoutPanel.Controls.Add(this._FooterContainerForEditor());

                    // add panel to layout order hidden field
                    layoutOrder += "FooterContainer" + "|";
                }
                else
                { }
            }

            sr.Close();

            // trim trailing | from layout order string
            if (!String.IsNullOrWhiteSpace(layoutOrder))
            { layoutOrder = layoutOrder.Substring(0, layoutOrder.Length - 1); }

            // set and attach the layout order hidden field
            this._LayoutOrder = new HiddenField();
            this._LayoutOrder.ID = "LayoutOrderField";
            this._LayoutOrder.Value = layoutOrder;
            this.PageContentContainer.Controls.Add(this._LayoutOrder);
        }
        #endregion

        #region _GetElementsForContainer
        /// <summary>
        /// Gets a list of element nodes for a specified container.
        /// </summary>
        /// <param name="xmlDoc">xml document</param>
        /// <param name="containerId">container id</param>
        /// <returns>xml node list of element nodes for container</returns>
        private XmlNodeList _GetElementsForContainer(XmlDocument xmlDoc, string containerId)
        {
            XmlNodeList containerNodes = xmlDoc.GetElementsByTagName("container");
            XmlNodeList elementNodes = null;

            foreach (XmlNode containerNode in containerNodes)
            {
                if (containerNode.Attributes["id"].Value == containerId)
                {
                    elementNodes = containerNode.SelectNodes("element");
                    break;
                }
            }

            return elementNodes;
        }
        #endregion

        #region _GetLabelForElement
        /// <summary>
        /// Gets the UI label for a global control element.
        /// </summary>
        /// <param name="elementId">the element id</param>
        /// <returns>string</returns>
        private string _GetLabelForElement(string elementId)
        {
            string elementLabel = String.Empty;

            switch (elementId)
            {
                case "Breadcrumb":
                    elementLabel = "[" + _GlobalResources.Breadcrumb + "]";
                    break;
                case "LoginLink":
                    elementLabel = "[" + _GlobalResources.LoginLink + "]";
                    break;
                case "LoggedInTag":
                    elementLabel = "[" + _GlobalResources.LoggedInTag + "]";
                    break;
                case "CurrentDateTag":
                    elementLabel = "[" + _GlobalResources.CurrentDateTag + "]";
                    break;
                case "LanguageSelectorControl":
                    elementLabel = "[" + _GlobalResources.LanguageSelector + "]";
                    break;
                case "LogoImage":
                    elementLabel = "[" + _GlobalResources.Logo + "]";
                    break;
                case "SecondaryImage":
                    elementLabel = "[" + _GlobalResources.SecondaryImage + "]";
                    break;
                case "MainNavigationControl":
                    elementLabel = "[" + _GlobalResources.Navigation + "]";
                    break;
                case "MyCartLink":
                    elementLabel = "[" + _GlobalResources.MyCart + "]";
                    break;
                case "MyProfileLink":
                    elementLabel = "[" + _GlobalResources.MySettings + "]";
                    break;
                case "PoweredByTag":
                    elementLabel = "[" + _GlobalResources.SystemBranding + "]";
                    break;
                case "FooterTag":
                    elementLabel = "[" + _GlobalResources.PortalBranding + "]";
                    break;
            }

            return elementLabel;
        }
        #endregion

        #region _HideElementIfNotAvailable
        /// <summary>
        /// Hides a global control element from the UI if it is not to be shown.
        /// Example: We will not show a My Cart link if the portal does not have ecommerce enabled.
        /// </summary>
        /// <param name="elementId">the id of the element</param>
        /// <returns>true/false</returns>
        private bool _HideElementIfNotAvailable(string elementId)
        {
            bool displayElement = true;

            switch (elementId)
            {
                case "Breadcrumb":
                    break;
                case "LoginLink":
                    break;
                case "LoggedInTag":
                    break;
                case "CurrentDateTag":
                    break;
                case "LanguageSelectorControl":
                    break;
                case "LogoImage":
                    break;
                case "SecondaryImage":
                    break;
                case "MainNavigationControl":
                    break;
                case "MyCartLink":
                    break;
                case "MyProfileLink":
                    break;
                case "PoweredByTag":
                    break;
                case "FooterTag":
                    break;
            }

            return displayElement;
        }
        #endregion

        #region _TopBorderContainerForEditor
        /// <summary>
        /// Builds the top border container for the layout editor.
        /// </summary>
        /// <returns>panel representing top border container for editor</returns>
        private Panel _TopBorderContainerForEditor()
        {
            // outer container
            Panel outerContainer = new Panel();
            outerContainer.ID = "Editor_TopBorderOuterContainer";
            outerContainer.CssClass = "LayoutEditableContainer";

            // container label
            HtmlGenericControl label = new HtmlGenericControl("p");
            label.InnerText = _GlobalResources.TopBorderContainer;

            // container
            Panel container = new Panel();
            container.ID = "Editor_TopBorderContainer";
            container.CssClass = "TopBorderContainer";

            // left column
            Panel leftColumn = new Panel();
            leftColumn.ID = "Editor_TopBorderColumnLeft";
            leftColumn.CssClass = "TopBorderColumnLeft DroppableContainer";

            // get the elements from the hidden field
            if (!String.IsNullOrWhiteSpace(this._TopBorderContainerLeftColumnItems.Value))
            {
                string[] leftColumnElements = this._TopBorderContainerLeftColumnItems.Value.Split('|');

                // append each container section in order
                foreach (string leftColumnElement in leftColumnElements)
                {
                    Label element = new Label();
                    element.ID = "EditorElement_" + leftColumnElement;
                    element.CssClass = "Editor_ContainerElementSpan DraggableItem";
                    element.Text = this._GetLabelForElement(leftColumnElement);
                    if (!this._HideElementIfNotAvailable(leftColumnElement))
                    { element.Attributes.Add("display", "none"); }
                    leftColumn.Controls.Add(element);
                }
            }

            container.Controls.Add(leftColumn);

            // right column
            Panel rightColumn = new Panel();
            rightColumn.ID = "Editor_TopBorderColumnRight";
            rightColumn.CssClass = "TopBorderColumnRight DroppableContainer";

            if (!String.IsNullOrWhiteSpace(this._TopBorderContainerRightColumnItems.Value))
            {
                // get the elements from the hidden field
                string[] rightColumnElements = this._TopBorderContainerRightColumnItems.Value.Split('|');

                // append each container section in order
                foreach (string rightColumnElement in rightColumnElements)
                {
                    Label element = new Label();
                    element.ID = "EditorElement_" + rightColumnElement;
                    element.CssClass = "Editor_ContainerElementSpan DraggableItem";
                    element.Text = this._GetLabelForElement(rightColumnElement);
                    if (!this._HideElementIfNotAvailable(rightColumnElement))
                    { element.Attributes.Add("display", "none"); }
                    rightColumn.Controls.Add(element);
                }
            }

            container.Controls.Add(rightColumn);

            // attach panels to outer container
            outerContainer.Controls.Add(label);
            outerContainer.Controls.Add(container);

            return outerContainer;
        }
        #endregion

        #region _MastheadContainerForEditor
        /// <summary>
        /// Builds the masthead container for the layout editor.
        /// </summary>
        /// <returns>panel representing masthead container for editor</returns>
        private Panel _MastheadContainerForEditor()
        {
            // outer container
            Panel outerContainer = new Panel();
            outerContainer.ID = "Editor_MastheadOuterContainer";
            outerContainer.CssClass = "LayoutEditableContainer";

            // container label
            HtmlGenericControl label = new HtmlGenericControl("p");
            label.InnerText = _GlobalResources.MastheadContainer;

            // container
            Panel container = new Panel();
            container.ID = "Editor_MastheadContainer";
            container.CssClass = "MastheadContainer";

            // left column
            Panel leftColumn = new Panel();
            leftColumn.ID = "Editor_MastheadColumnLeft";
            leftColumn.CssClass = "MastheadColumnLeft DroppableContainer";

            if (!String.IsNullOrWhiteSpace(this._MastheadContainerLeftColumnItems.Value))
            {
                // get the elements from the hidden field
                string[] leftColumnElements = this._MastheadContainerLeftColumnItems.Value.Split('|');

                // append each container section in order
                foreach (string leftColumnElement in leftColumnElements)
                {
                    Label element = new Label();
                    element.ID = "EditorElement_" + leftColumnElement;
                    element.CssClass = "Editor_ContainerElementSpan DraggableItem";
                    element.Text = this._GetLabelForElement(leftColumnElement);
                    if (!this._HideElementIfNotAvailable(leftColumnElement))
                    { element.Attributes.Add("display", "none"); }
                    leftColumn.Controls.Add(element);
                }
            }

            container.Controls.Add(leftColumn);

            // right column
            Panel rightColumn = new Panel();
            rightColumn.ID = "Editor_MastheadColumnRight";
            rightColumn.CssClass = "MastheadColumnRight DroppableContainer";

            if (!String.IsNullOrWhiteSpace(this._MastheadContainerRightColumnItems.Value))
            {
                // get the elements from the hidden field
                string[] rightColumnElements = this._MastheadContainerRightColumnItems.Value.Split('|');

                // append each container section in order
                foreach (string rightColumnElement in rightColumnElements)
                {
                    Label element = new Label();
                    element.ID = "EditorElement_" + rightColumnElement;
                    element.CssClass = "Editor_ContainerElementSpan DraggableItem";
                    element.Text = this._GetLabelForElement(rightColumnElement);
                    if (!this._HideElementIfNotAvailable(rightColumnElement))
                    { element.Attributes.Add("display", "none"); }
                    rightColumn.Controls.Add(element);
                }
            }

            container.Controls.Add(rightColumn);

            // attach panels to outer container
            outerContainer.Controls.Add(label);
            outerContainer.Controls.Add(container);

            return outerContainer;
        }
        #endregion

        #region _NavigationContainerForEditor
        /// <summary>
        /// Builds the navigation container for the layout editor.
        /// </summary>
        /// <returns>panel representing navigation container for editor</returns>
        private Panel _NavigationContainerForEditor()
        {
            // outer container
            Panel outerContainer = new Panel();
            outerContainer.ID = "Editor_NavigationOuterContainer";
            outerContainer.CssClass = "LayoutEditableContainer";

            // container label
            HtmlGenericControl label = new HtmlGenericControl("p");
            label.InnerText = _GlobalResources.NavigationContainer;

            // container
            Panel container = new Panel();
            container.ID = "Editor_NavigationContainer";
            container.CssClass = "NavigationContainer";

            // left column
            Panel leftColumn = new Panel();
            leftColumn.ID = "Editor_NavigationColumnLeft";
            leftColumn.CssClass = "NavigationColumnLeft DroppableContainer";

            if (!String.IsNullOrWhiteSpace(this._NavigationContainerLeftColumnItems.Value))
            {
                // get the elements from the hidden field
                string[] leftColumnElements = this._NavigationContainerLeftColumnItems.Value.Split('|');

                // append each container section in order
                foreach (string leftColumnElement in leftColumnElements)
                {
                    Label element = new Label();
                    element.ID = "EditorElement_" + leftColumnElement;
                    element.CssClass = "Editor_ContainerElementSpan DraggableItem";
                    element.Text = this._GetLabelForElement(leftColumnElement);
                    if (!this._HideElementIfNotAvailable(leftColumnElement))
                    { element.Attributes.Add("display", "none"); }
                    leftColumn.Controls.Add(element);
                }
            }

            container.Controls.Add(leftColumn);

            // right column
            Panel rightColumn = new Panel();
            rightColumn.ID = "Editor_NavigationColumnRight";
            rightColumn.CssClass = "NavigationColumnRight DroppableContainer";

            if (!String.IsNullOrWhiteSpace(this._NavigationContainerRightColumnItems.Value))
            {
                // get the elements from the hidden field
                string[] rightColumnElements = this._NavigationContainerRightColumnItems.Value.Split('|');

                // append each container section in order
                foreach (string rightColumnElement in rightColumnElements)
                {
                    Label element = new Label();
                    element.ID = "EditorElement_" + rightColumnElement;
                    element.CssClass = "Editor_ContainerElementSpan DraggableItem";
                    element.Text = this._GetLabelForElement(rightColumnElement);
                    if (!this._HideElementIfNotAvailable(rightColumnElement))
                    { element.Attributes.Add("display", "none"); }
                    rightColumn.Controls.Add(element);
                }
            }

            container.Controls.Add(rightColumn);

            // attach panels to outer container
            outerContainer.Controls.Add(label);
            outerContainer.Controls.Add(container);

            return outerContainer;
        }
        #endregion

        #region _BreadcrumbContainerForEditor
        /// <summary>
        /// Builds the breadcrumb container for the layout editor.
        /// </summary>
        /// <returns>panel representing breadcrumb container for editor</returns>
        private Panel _BreadcrumbContainerForEditor()
        {
            // outer container
            Panel outerContainer = new Panel();
            outerContainer.ID = "Editor_BreadcrumbOuterContainer";
            outerContainer.CssClass = "LayoutEditableContainer";

            // container label
            HtmlGenericControl label = new HtmlGenericControl("p");
            label.InnerText = _GlobalResources.BreadcrumbContainer;

            // container
            Panel container = new Panel();
            container.ID = "Editor_BreadcrumbContainer";
            container.CssClass = "BreadcrumbContainer";

            // left column
            Panel leftColumn = new Panel();
            leftColumn.ID = "Editor_BreadcrumbColumnLeft";
            leftColumn.CssClass = "BreadcrumbColumnLeft DroppableContainer";

            // breadcrumb element - static
            Label breadcrumbElement = new Label();
            breadcrumbElement.ID = "EditorElement_Breadcrumb";
            breadcrumbElement.CssClass = "Editor_ContainerElementSpan";
            breadcrumbElement.Text = this._GetLabelForElement("Breadcrumb");
            if (!this._HideElementIfNotAvailable("Breadcrumb"))
            { breadcrumbElement.Attributes.Add("display", "none"); }
            leftColumn.Controls.Add(breadcrumbElement);

            if (!String.IsNullOrWhiteSpace(this._BreadcrumbContainerLeftColumnItems.Value))
            {
                // get the elements from the hidden field
                string[] leftColumnElements = this._BreadcrumbContainerLeftColumnItems.Value.Split('|');

                // append each container section in order
                foreach (string leftColumnElement in leftColumnElements)
                {
                    Label element = new Label();
                    element.ID = "EditorElement_" + leftColumnElement;
                    element.CssClass = "Editor_ContainerElementSpan DraggableItem";
                    element.Text = this._GetLabelForElement(leftColumnElement);
                    if (!this._HideElementIfNotAvailable(leftColumnElement))
                    { element.Attributes.Add("display", "none"); }
                    leftColumn.Controls.Add(element);
                }
            }

            container.Controls.Add(leftColumn);

            // right column
            Panel rightColumn = new Panel();
            rightColumn.ID = "Editor_BreadcrumbColumnRight";
            rightColumn.CssClass = "BreadcrumbColumnRight DroppableContainer";

            if (!String.IsNullOrWhiteSpace(this._BreadcrumbContainerRightColumnItems.Value))
            {
                // get the elements from the hidden field
                string[] rightColumnElements = this._BreadcrumbContainerRightColumnItems.Value.Split('|');

                // append each container section in order
                foreach (string rightColumnElement in rightColumnElements)
                {
                    Label element = new Label();
                    element.ID = "EditorElement_" + rightColumnElement;
                    element.CssClass = "Editor_ContainerElementSpan DraggableItem";
                    element.Text = this._GetLabelForElement(rightColumnElement);
                    if (!this._HideElementIfNotAvailable(rightColumnElement))
                    { element.Attributes.Add("display", "none"); }
                    rightColumn.Controls.Add(element);
                }
            }

            container.Controls.Add(rightColumn);

            // attach panels to outer container
            outerContainer.Controls.Add(label);
            outerContainer.Controls.Add(container);

            return outerContainer;
        }
        #endregion

        #region _ContentContainerForEditor
        /// <summary>
        /// Builds the content container for the layout editor.
        /// </summary>
        /// <returns>panel representing content container for editor</returns>
        private Panel _ContentContainerForEditor()
        {
            // outer container
            Panel outerContainer = new Panel();
            outerContainer.ID = "Editor_ContentOuterContainer";
            outerContainer.CssClass = "LayoutEditableContainer";

            // container label
            HtmlGenericControl label = new HtmlGenericControl("p");
            label.InnerText = _GlobalResources.ContentContainer;

            // container
            Panel container = new Panel();
            container.ID = "Editor_ContentContainer";
            container.CssClass = "ContentContainer";

            // table
            Table table = new Table();
            table.ID = "Editor_MasterContentTable";
            table.CssClass = "MasterContentTable";

            // table row
            TableRow tableRow = new TableRow();

            // left column
            TableCell leftColumn = new TableCell();
            leftColumn.ID = "Editor_MasterContentTableLeft";
            tableRow.Cells.Add(leftColumn);

            // middle column
            TableCell middleColumn = new TableCell();
            middleColumn.ID = "Editor_MasterContentTableMiddle";
            tableRow.Cells.Add(middleColumn);

            // right column
            TableCell rightColumn = new TableCell();
            rightColumn.ID = "Editor_MasterContentTableRight";
            tableRow.Cells.Add(rightColumn);

            // attach table elements
            table.Rows.Add(tableRow);
            container.Controls.Add(table);

            // attach panels to outer container
            outerContainer.Controls.Add(label);
            outerContainer.Controls.Add(container);

            return outerContainer;
        }
        #endregion

        #region _FooterContainerForEditor
        /// <summary>
        /// Builds the footer container for the layout editor.
        /// </summary>
        /// <returns>panel representing footer container for editor</returns>
        private Panel _FooterContainerForEditor()
        {
            // outer container
            Panel outerContainer = new Panel();
            outerContainer.ID = "Editor_FooterOuterContainer";
            outerContainer.CssClass = "LayoutEditableContainer";

            // container label
            HtmlGenericControl label = new HtmlGenericControl("p");
            label.InnerText = _GlobalResources.FooterContainer;

            // container
            Panel container = new Panel();
            container.ID = "Editor_FooterContainer";
            container.CssClass = "FooterContainer";

            // left column
            Panel leftColumn = new Panel();
            leftColumn.ID = "Editor_FooterColumnLeft";
            leftColumn.CssClass = "FooterColumnLeft DroppableContainer";

            if (!String.IsNullOrWhiteSpace(this._FooterContainerLeftColumnItems.Value))
            {
                // get the elements from the hidden field
                string[] leftColumnElements = this._FooterContainerLeftColumnItems.Value.Split('|');

                // append each container section in order
                foreach (string leftColumnElement in leftColumnElements)
                {
                    Label element = new Label();
                    element.ID = "EditorElement_" + leftColumnElement;
                    element.CssClass = "Editor_ContainerElementSpan DraggableItem";
                    element.Text = this._GetLabelForElement(leftColumnElement);
                    if (!this._HideElementIfNotAvailable(leftColumnElement))
                    { element.Attributes.Add("display", "none"); }
                    leftColumn.Controls.Add(element);
                }
            }

            container.Controls.Add(leftColumn);

            // right column
            Panel rightColumn = new Panel();
            rightColumn.ID = "Editor_FooterColumnRight";
            rightColumn.CssClass = "FooterColumnRight DroppableContainer";

            if (!String.IsNullOrWhiteSpace(this._FooterContainerRightColumnItems.Value))
            {
                // get the elements from the hidden field
                string[] rightColumnElements = this._FooterContainerRightColumnItems.Value.Split('|');

                // append each container section in order
                foreach (string rightColumnElement in rightColumnElements)
                {
                    Label element = new Label();
                    element.ID = "EditorElement_" + rightColumnElement;
                    element.CssClass = "Editor_ContainerElementSpan DraggableItem";
                    element.Text = this._GetLabelForElement(rightColumnElement);
                    if (!this._HideElementIfNotAvailable(rightColumnElement))
                    { element.Attributes.Add("display", "none"); }
                    rightColumn.Controls.Add(element);
                }
            }

            container.Controls.Add(rightColumn);

            // attach panels to outer container
            outerContainer.Controls.Add(label);
            outerContainer.Controls.Add(container);

            return outerContainer;
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // clear controls from panel
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // revert to default button
            this._RevertToDefaultButton = new Button();
            this._RevertToDefaultButton.ID = "RevertToDefaultButton";

            if (this._EnableRevertToDefaultButton)
            { this._RevertToDefaultButton.CssClass = "Button ActionButton"; }
            else
            {
                this._RevertToDefaultButton.CssClass = "DisabledButton";
                this._RevertToDefaultButton.Enabled = false;
            }

            this._RevertToDefaultButton.Text = _GlobalResources.RevertToDefault;
            this._RevertToDefaultButton.Command += new CommandEventHandler(_RevertToDefaultButton_Command);
            this.ActionsPanel.Controls.Add(this._RevertToDefaultButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _GetTopSectionTemplate
        /// <summary>
        /// Gets the top section template for the master page.
        /// </summary>
        /// <returns>string representing top section of master page</returns>
        private string _GetTopSectionTemplate()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<%@ Master Language=\"C#\" AutoEventWireup=\"true\" Inherits=\"Asentia.Controls.AsentiaMasterPage\" %>");
            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
            sb.AppendLine("<head id=\"PageHead\" runat=\"server\">");
            sb.AppendLine("    <title>Asentia</title>");
            sb.AppendLine("    <!--[if lt IE 9]>");
            sb.AppendLine("        <script src=\"_scripts/IE9.js\"></script>");
            sb.AppendLine("    <![endif]-->");
            sb.AppendLine("    <asp:Literal ID=\"GlobalJavascriptVariables\" runat=\"server\"></asp:Literal>");
            sb.AppendLine("    <asp:ContentPlaceHolder ID=\"HeadPlaceholder\" runat=\"server\"></asp:ContentPlaceHolder>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body>");
            sb.AppendLine("    <form id=\"MasterForm\" runat=\"server\">");
            sb.AppendLine("    <atk:ToolkitScriptManager ID=\"asm\" runat=\"server\" ScriptMode=\"Release\" EnablePartialRendering=\"true\" EnableScriptLocalization=\"true\" EnableScriptGlobalization=\"true\"></atk:ToolkitScriptManager>");
            sb.AppendLine("");
            sb.AppendLine("    <asp:Panel ID=\"PageContainer\" runat=\"server\">");
            sb.AppendLine("");

            return sb.ToString();
        }
        #endregion

        #region _GetTopBorderContainerTemplate
        /// <summary>
        /// Gets the top border container template for the master page.
        /// </summary>
        /// <returns>string representing top section of master page</returns>
        private string _GetTopBorderContainerTemplate()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("        <asp:Panel ID=\"TopBorderContainer\" CssClass=\"TopBorderContainer\" runat=\"server\">");
            sb.AppendLine("            <asp:Panel ID=\"TopBorderColumnLeft\" CssClass=\"TopBorderColumnLeft\" runat=\"server\"></asp:Panel>");
            sb.AppendLine("            <asp:Panel ID=\"TopBorderColumnRight\" CssClass=\"TopBorderColumnRight\" runat=\"server\"></asp:Panel>");
            sb.AppendLine("        </asp:Panel>");
            sb.AppendLine("");

            return sb.ToString();
        }
        #endregion

        #region _GetMastheadContainerTemplate
        /// <summary>
        /// Gets the masthead container template for the master page.
        /// </summary>
        /// <returns>string representing top section of master page</returns>
        private string _GetMastheadContainerTemplate()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("        <asp:Panel ID=\"MastheadContainer\" CssClass=\"MastheadContainer\" runat=\"server\">");
            sb.AppendLine("            <asp:Panel ID=\"MastheadColumnLeft\" CssClass=\"MastheadColumnLeft\" runat=\"server\"></asp:Panel>");
            sb.AppendLine("            <asp:Panel ID=\"MastheadColumnRight\" CssClass=\"MastheadColumnRight\" runat=\"server\"></asp:Panel>");
            sb.AppendLine("        </asp:Panel>");
            sb.AppendLine("");

            return sb.ToString();
        }
        #endregion

        #region _GetNavigationContainerTemplate
        /// <summary>
        /// Gets the navigation container template for the master page.
        /// </summary>
        /// <returns>string representing top section of master page</returns>
        private string _GetNavigationContainerTemplate()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("        <asp:Panel ID=\"NavigationContainer\" CssClass=\"NavigationContainer\" runat=\"server\">");
            sb.AppendLine("            <asp:Panel ID=\"NavigationColumnLeft\" CssClass=\"NavigationColumnLeft\" runat=\"server\"></asp:Panel>");
            sb.AppendLine("            <asp:Panel ID=\"NavigationColumnRight\" CssClass=\"NavigationColumnRight\" runat=\"server\"></asp:Panel>");
            sb.AppendLine("        </asp:Panel>");
            sb.AppendLine("");

            return sb.ToString();
        }
        #endregion

        #region _GetBreadcrumbContainerTemplate
        /// <summary>
        /// Gets the breadcrumb container template for the master page.
        /// </summary>
        /// <returns>string representing top section of master page</returns>
        private string _GetBreadcrumbContainerTemplate()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("        <asp:Panel ID=\"BreadcrumbContainer\" CssClass=\"BreadcrumbContainer\" runat=\"server\">");
            sb.AppendLine("            <asp:Panel ID=\"BreadcrumbColumnLeft\" CssClass=\"BreadcrumbColumnLeft\" runat=\"server\">");
            sb.AppendLine("                <asp:ContentPlaceHolder ID=\"BreadcrumbContentPlaceholder\" runat=\"server\"></asp:ContentPlaceHolder>");
            sb.AppendLine("            </asp:Panel>");
            sb.AppendLine("            <asp:Panel ID=\"BreadcrumbColumnRight\" CssClass=\"BreadcrumbColumnRight\" runat=\"server\"></asp:Panel>");
            sb.AppendLine("        </asp:Panel>");
            sb.AppendLine("");

            return sb.ToString();
        }
        #endregion

        #region _GetContentContainerTemplate
        /// <summary>
        /// Gets the content container template for the master page.
        /// </summary>
        /// <returns>string representing top section of master page</returns>
        private string _GetContentContainerTemplate()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("        <asp:Panel ID=\"ContentContainer\" CssClass=\"ContentContainer\" runat=\"server\">");
            sb.AppendLine("");
            sb.AppendLine("            <table id=\"MasterContentTable\" class=\"MasterContentTable\" cellpadding=\"0\" cellspacing=\"0\">");
            sb.AppendLine("            <tr>");
            sb.AppendLine("                <td class=\"MasterContentTableLeft\"><asp:ContentPlaceHolder ID=\"SideContentPlaceholder1\" runat=\"server\"></asp:ContentPlaceHolder></td>");
            sb.AppendLine("                <td class=\"MasterContentTableMiddle\"><asp:ContentPlaceHolder ID=\"PageContentPlaceholder\" runat=\"server\"></asp:ContentPlaceHolder></td>");
            sb.AppendLine("                <td class=\"MasterContentTableRight\"><asp:ContentPlaceHolder ID=\"SideContentPlaceholder2\" runat=\"server\"></asp:ContentPlaceHolder></td>");
            sb.AppendLine("            </tr>");
            sb.AppendLine("            </table>");
            sb.AppendLine("");
            sb.AppendLine("        </asp:Panel>");
            sb.AppendLine("");

            return sb.ToString();
        }
        #endregion

        #region _GetFooterContainerTemplate
        /// <summary>
        /// Gets the footer container template for the master page.
        /// </summary>
        /// <returns>string representing top section of master page</returns>
        private string _GetFooterContainerTemplate()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("        <asp:Panel ID=\"FooterContainer\" CssClass=\"FooterContainer\" runat=\"server\">");
            sb.AppendLine("            <asp:Panel ID=\"FooterColumnLeft\" CssClass=\"FooterColumnLeft\" runat=\"server\"></asp:Panel>");
            sb.AppendLine("            <asp:Panel ID=\"FooterColumnRight\" CssClass=\"FooterColumnRight\" runat=\"server\"></asp:Panel>");
            sb.AppendLine("        </asp:Panel>");
            sb.AppendLine("");

            return sb.ToString();
        }
        #endregion

        #region _GetBottomSectionTemplate
        /// <summary>
        /// Gets the bottom section template for the master page.
        /// </summary>
        /// <returns>string representing top section of master page</returns>
        private string _GetBottomSectionTemplate()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("    </asp:Panel>");
            sb.AppendLine("    </form>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");

            return sb.ToString();
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/dashboard");
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Performs the save action on the page.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _SaveButton_Command(object sender, EventArgs e)
        {
            try
            {
                // MASTER PAGE FOR LAYOUT

                // get the layout order from the hidden field
                string[] layoutItemsInOrder = this._LayoutOrder.Value.Split('|');

                // build the layout from template methods for master page
                StringBuilder sb = new StringBuilder();

                // append top section
                sb.Append(this._GetTopSectionTemplate());

                // append each container section in order
                foreach (string layoutItem in layoutItemsInOrder)
                {
                    if (layoutItem == "TopBorderContainer")
                    { sb.Append(this._GetTopBorderContainerTemplate()); }
                    else if (layoutItem == "MastheadContainer")
                    { sb.Append(this._GetMastheadContainerTemplate()); }
                    else if (layoutItem == "NavigationContainer")
                    { sb.Append(this._GetNavigationContainerTemplate()); }
                    else if (layoutItem == "BreadcrumbContainer")
                    { sb.Append(this._GetBreadcrumbContainerTemplate()); }
                    else if (layoutItem == "ContentContainer")
                    { sb.Append(this._GetContentContainerTemplate()); }
                    else if (layoutItem == "FooterContainer")
                    { sb.Append(this._GetFooterContainerTemplate()); }
                    else
                    { }
                }

                // append the bottom section
                sb.Append(this._GetBottomSectionTemplate());

                // make sure the site's master page directory exists
                if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE)))
                { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE)); }

                // write the new master page file for site
                File.WriteAllText(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + "GlobalSite.Master"), sb.ToString(), Encoding.UTF8);

                // GLOBAL ELEMENTS FILE FOR MASTER PAGE

                // code goes here
                XDocument globalControlsDocument = new XDocument();

                // containers node - document element
                XElement containersElement = new XElement("containers");

                // loop through layout items and create element containers for each
                foreach (string layoutItem in layoutItemsInOrder)
                {
                    // top border container
                    if (layoutItem == "TopBorderContainer")
                    {
                        // build element
                        XElement topBorderContainerElement = new XElement("container");
                        topBorderContainerElement.Add(new XAttribute("id", layoutItem));

                        // left column
                        if (!String.IsNullOrWhiteSpace(this._TopBorderContainerLeftColumnItems.Value))
                        {
                            string[] topBorderContainerLeftElements = this._TopBorderContainerLeftColumnItems.Value.Split('|');

                            foreach (string topBorderContainerLeftElement in topBorderContainerLeftElements)
                            {
                                XElement element = new XElement("element");
                                element.Add(new XAttribute("type", topBorderContainerLeftElement));
                                element.Add(new XAttribute("id", String.Empty));
                                element.Add(new XAttribute("columnPlacement", "left"));
                                topBorderContainerElement.Add(element);
                            }
                        }

                        // right column
                        if (!String.IsNullOrWhiteSpace(this._TopBorderContainerRightColumnItems.Value))
                        {
                            string[] topBorderContainerRightElements = this._TopBorderContainerRightColumnItems.Value.Split('|');

                            foreach (string topBorderContainerRightElement in topBorderContainerRightElements)
                            {
                                XElement element = new XElement("element");
                                element.Add(new XAttribute("type", topBorderContainerRightElement));
                                element.Add(new XAttribute("id", String.Empty));
                                element.Add(new XAttribute("columnPlacement", "right"));
                                topBorderContainerElement.Add(element);
                            }
                        }

                        // attach element to containers element
                        containersElement.Add(topBorderContainerElement);
                    }
                    // masthead container
                    else if (layoutItem == "MastheadContainer")
                    {
                        // build element
                        XElement mastheadContainerElement = new XElement("container");
                        mastheadContainerElement.Add(new XAttribute("id", layoutItem));

                        // left column
                        if (!String.IsNullOrWhiteSpace(this._MastheadContainerLeftColumnItems.Value))
                        {
                            string[] mastheadContainerLeftElements = this._MastheadContainerLeftColumnItems.Value.Split('|');

                            foreach (string mastheadContainerLeftElement in mastheadContainerLeftElements)
                            {
                                XElement element = new XElement("element");
                                element.Add(new XAttribute("type", mastheadContainerLeftElement));
                                element.Add(new XAttribute("id", String.Empty));
                                element.Add(new XAttribute("columnPlacement", "left"));
                                mastheadContainerElement.Add(element);
                            }
                        }

                        // right column
                        if (!String.IsNullOrWhiteSpace(this._MastheadContainerRightColumnItems.Value))
                        {
                            string[] mastheadContainerRightElements = this._MastheadContainerRightColumnItems.Value.Split('|');

                            foreach (string mastheadContainerRightElement in mastheadContainerRightElements)
                            {
                                XElement element = new XElement("element");
                                element.Add(new XAttribute("type", mastheadContainerRightElement));
                                element.Add(new XAttribute("id", String.Empty));
                                element.Add(new XAttribute("columnPlacement", "right"));
                                mastheadContainerElement.Add(element);
                            }
                        }

                        // attach element to containers element
                        containersElement.Add(mastheadContainerElement);
                    }
                    // navigation container
                    else if (layoutItem == "NavigationContainer")
                    {
                        // build element
                        XElement navigationContainerElement = new XElement("container");
                        navigationContainerElement.Add(new XAttribute("id", layoutItem));

                        // left column
                        if (!String.IsNullOrWhiteSpace(this._NavigationContainerLeftColumnItems.Value))
                        {
                            string[] navigationContainerLeftElements = this._NavigationContainerLeftColumnItems.Value.Split('|');

                            foreach (string navigationContainerLeftElement in navigationContainerLeftElements)
                            {
                                XElement element = new XElement("element");
                                element.Add(new XAttribute("type", navigationContainerLeftElement));
                                element.Add(new XAttribute("id", String.Empty));
                                element.Add(new XAttribute("columnPlacement", "left"));
                                navigationContainerElement.Add(element);
                            }
                        }

                        // right column
                        if (!String.IsNullOrWhiteSpace(this._NavigationContainerRightColumnItems.Value))
                        {
                            string[] navigationContainerRightElements = this._NavigationContainerRightColumnItems.Value.Split('|');

                            foreach (string navigationContainerRightElement in navigationContainerRightElements)
                            {
                                XElement element = new XElement("element");
                                element.Add(new XAttribute("type", navigationContainerRightElement));
                                element.Add(new XAttribute("id", String.Empty));
                                element.Add(new XAttribute("columnPlacement", "right"));
                                navigationContainerElement.Add(element);
                            }
                        }

                        // attach element to containers element
                        containersElement.Add(navigationContainerElement);
                    }
                    // breadcrumb container
                    else if (layoutItem == "BreadcrumbContainer")
                    {
                        // build element
                        XElement breadcrumbContainerElement = new XElement("container");
                        breadcrumbContainerElement.Add(new XAttribute("id", layoutItem));

                        // left column
                        if (!String.IsNullOrWhiteSpace(this._BreadcrumbContainerLeftColumnItems.Value))
                        {
                            string[] breadcrumbContainerLeftElements = this._BreadcrumbContainerLeftColumnItems.Value.Split('|');

                            foreach (string breadcrumbContainerLeftElement in breadcrumbContainerLeftElements)
                            {
                                XElement element = new XElement("element");
                                element.Add(new XAttribute("type", breadcrumbContainerLeftElement));
                                element.Add(new XAttribute("id", String.Empty));
                                element.Add(new XAttribute("columnPlacement", "left"));
                                breadcrumbContainerElement.Add(element);
                            }
                        }

                        // right column
                        if (!String.IsNullOrWhiteSpace(this._BreadcrumbContainerRightColumnItems.Value))
                        {
                            string[] breadcrumbContainerRightElements = this._BreadcrumbContainerRightColumnItems.Value.Split('|');

                            foreach (string breadcrumbContainerRightElement in breadcrumbContainerRightElements)
                            {
                                XElement element = new XElement("element");
                                element.Add(new XAttribute("type", breadcrumbContainerRightElement));
                                element.Add(new XAttribute("id", String.Empty));
                                element.Add(new XAttribute("columnPlacement", "right"));
                                breadcrumbContainerElement.Add(element);
                            }
                        }

                        // attach element to containers element
                        containersElement.Add(breadcrumbContainerElement);
                    }
                    // content container
                    else if (layoutItem == "ContentContainer")
                    {
                        // build element
                        XElement contentContainerElement = new XElement("container");
                        contentContainerElement.Add(new XAttribute("id", layoutItem));

                        // attach element to containers element
                        containersElement.Add(contentContainerElement);
                    }
                    // footer container
                    else if (layoutItem == "FooterContainer")
                    {
                        // build element
                        XElement footerContainerElement = new XElement("container");
                        footerContainerElement.Add(new XAttribute("id", layoutItem));

                        // left column
                        if (!String.IsNullOrWhiteSpace(this._FooterContainerLeftColumnItems.Value))
                        {
                            string[] footerContainerLeftElements = this._FooterContainerLeftColumnItems.Value.Split('|');

                            foreach (string footerContainerLeftElement in footerContainerLeftElements)
                            {
                                XElement element = new XElement("element");
                                element.Add(new XAttribute("type", footerContainerLeftElement));
                                element.Add(new XAttribute("id", String.Empty));
                                element.Add(new XAttribute("columnPlacement", "left"));
                                footerContainerElement.Add(element);
                            }
                        }

                        // right column
                        if (!String.IsNullOrWhiteSpace(this._FooterContainerRightColumnItems.Value))
                        {
                            string[] footerContainerRightElements = this._FooterContainerRightColumnItems.Value.Split('|');

                            foreach (string footerContainerRightElement in footerContainerRightElements)
                            {
                                XElement element = new XElement("element");
                                element.Add(new XAttribute("type", footerContainerRightElement));
                                element.Add(new XAttribute("id", String.Empty));
                                element.Add(new XAttribute("columnPlacement", "right"));
                                footerContainerElement.Add(element);
                            }
                        }

                        // attach element to containers element
                        containersElement.Add(footerContainerElement);
                    }
                    else
                    {}
                }

                // add containers element to document
                globalControlsDocument.Add(containersElement);

                // save the xml to file
                globalControlsDocument.Save(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + "GlobalControls.xml"));

                // rebuild layout editor and actions panel
                this._BuildLayoutEditor();
                this._BuildActionsPanel();

                // display success message
                this.DisplayFeedback(_GlobalResources.LayoutSuccessfullySaved, false);
            }
            catch (AsentiaException ex)
            {
                // display failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _RevertToDefaultButton_Command
        /// <summary>
        /// Handles the "Revert To Default" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RevertToDefaultButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // delete the master page from the site
                if (File.Exists(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + "GlobalSite.Master")))
                { File.Delete(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + "GlobalSite.Master")); }

                // delete the global controls xml file from the site
                if (File.Exists(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + "GlobalControls.xml")))
                { File.Delete(Server.MapPath(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + "GlobalControls.xml")); }

                // rebuild layout editor and actions panel
                this._BuildLayoutEditor();
                this._EnableRevertToDefaultButton = false;
                this._BuildActionsPanel();
                
                // display success message
                this.DisplayFeedback(_GlobalResources.LayoutSuccessfullyRevertedToDefault, false);
            }
            catch (AsentiaException ex)
            {
                // display failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion
    }
}