﻿using System;
using System.Collections;
using SysDrawing = System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.UMS.Pages.Administrator.System.Interface
{
    public class ImageEditor : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ImageEditorFormContentWrapperContainer;
        public UpdatePanel ImageFileUpdatePanel;
        public Panel PageInstructionsPanel;
        public Panel ImageFileTreeContainer;
        public Panel ImageFileEditWrapperContainer;
        public Panel ImageFileEditContainer;
        public Panel ImageFilePreviewContainer;
        public Panel ActionsPanel;
        public TreeView ImageFileTree;
        public Image SelectedImage;
        public Button SubmitButtonId;
        public HiddenField SelectedImageURL;
        public Panel ImageFileTreePanel;        
        public Panel ImageEditorEditingPanel;
        public Panel ImageSelectionPanel;
        #endregion

        #region Private Properties
        private Button _SaveButton;
        private Button _RevertToDefaultButton;
        private UploaderAsync _ImageUploader;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // execute the base
            base.OnPreRender(e);

            // registers the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(ImageEditor), "Asentia.UMS.Pages.Administrator.System.Interface.ImageEditor.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check to ensure image editor is enabled on the portal
            if (!AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.IMAGE_EDITOR_ENABLE) ?? false)
            { Response.Redirect("/"); }

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_ImageEditor))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/system/interface/ImageEditor.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ImageEditor));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.InterfaceAndLayout, _GlobalResources.ImageEditor, ImageFiles.GetIconPath(ImageFiles.ICON_IMAGE, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.ImageEditorFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.UploadYourOwnImageFilesToReplaceImagesInThePortalsCurrentThemeByUsingTheOptionsBelow, true);

            // apply css class to containers
            this.ImageFileTreeContainer.CssClass = "xd-5 xm-12";
            this.ImageFileEditWrapperContainer.CssClass = "xd-7 xm-12";            

            // build the actions panel
            this._BuildActionsPanel();            

            // build the image uploader panel
            this._BuildImageUploaderPanel();

            // build the image tree            
            this._BuildImageTree(this.ImageFileTree, Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET));

            // set styling for the image tree selected nodes
            this.ImageFileTree.SkipLinkText = String.Empty;
            this.ImageFileTree.SelectedNodeStyle.BackColor = SysDrawing.Color.LightBlue;
            this.ImageFileTree.SelectedNodeStyle.ForeColor = SysDrawing.Color.White;

            // set selected node changed event for tree view
            this.ImageFileTree.SelectedNodeChanged += new EventHandler(this._SelectedNodeChanged);

            // set selected image url, hidden field, and save button visibility if there is a selected node with a value
            if (this.ImageFileTree.SelectedNode != null)
            {
                // if selected node value is not null or space
                if (!string.IsNullOrWhiteSpace(this.ImageFileTree.SelectedNode.Value))
                {
                    this.SelectedImage.ImageUrl = this.ImageFileTree.SelectedNode.Value + "?r=" + Guid.NewGuid();
                    this.SelectedImageURL.Value = this.ImageFileTree.SelectedNode.Value;
                }
                else
                {
                    this.SelectedImage.Visible = false;
                    this.ImageSelectionPanel.Style.Add("display", "none");
                    this.ActionsPanel.Style.Add("display", "none");
                }
            }
            else
            {
                this.SelectedImage.Visible = false;
                this.ActionsPanel.Style.Add("display", "none");
                this.ImageSelectionPanel.Style.Add("display", "none");
            }

            if (!this.IsPostBack)
            { this.ImageFileTree.CollapseAll(); }
        }
        #endregion        

        #region _UploadComplete
        /// <summary>
        /// Method upload complete method to resize the uploaded image
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        /// <param name="filename">filename</param>
        private void _UploadComplete(object sender, EventArgs e, string filename)
        {
            try
            {
                ImageResizer resizer = new ImageResizer();

                if (this.ImageFileTree.SelectedNode != null && !String.IsNullOrWhiteSpace(this.ImageFileTree.SelectedNode.Value))
                {
                    TreeNode immediateParent = this.ImageFileTree.SelectedNode.Parent;
                    int iconSize = 0;
                    bool isInteger = int.TryParse(immediateParent.Text, out iconSize);

                    if (isInteger && immediateParent != null && immediateParent.Parent != null && immediateParent.Text == "64")
                    {
                        resizer.MaxX = 128; // iconSize;
                        resizer.MaxY = 128; // iconSize;
                        resizer.TrimImage = true;
                        resizer.Resize(Server.MapPath(filename), Server.MapPath(filename + ".resized"));

                        // replace uploaded with "resized"
                        File.Copy(Server.MapPath(filename + ".resized"), Server.MapPath(filename), true);

                        // delete "resized"
                        File.Delete(Server.MapPath(filename + ".resized"));
                    }
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display error message to user if any exception occur
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _BuildImageUploaderPanel
        /// <summary>
        /// Method to create the uploader control on the page.
        /// </summary>
        private void _BuildImageUploaderPanel()
        {
            // uploader
            this._ImageUploader = new UploaderAsync("ImageUploader", UploadType.Image, "ImageUploader_ErrorContainer");
            this._ImageUploader.ValidateSpecificExtension = true;
            this._ImageUploader.ServerSideCompleteMethod += this._UploadComplete;
            this._ImageUploader.ResizeOnUpload = false;
            this._ImageUploader.ShowPreviewOnImageUpload = false;
            this._ImageUploader.ClientSideCompleteJSMethod = "ChangeImagePreview";

            // uploader error panel
            Panel logoImageUploaderErrorContainer = new Panel();
            logoImageUploaderErrorContainer.ID = "ImageUploader_ErrorContainer";
            logoImageUploaderErrorContainer.CssClass = "FormFieldErrorContainer";

            this.ImageSelectionPanel.Controls.Add(logoImageUploaderErrorContainer);
            this.ImageSelectionPanel.Controls.Add(this._ImageUploader);
        }
        #endregion        

        #region _BuildImageTree
        /// <summary>
        /// Builds the image file tree.
        /// </summary>
        /// <param name="treeView">the tree view control</param>
        /// <param name="rootPath">the root path of the images folder</param>
        private void _BuildImageTree(TreeView treeView, string rootPath)
        {
            // clear nodes from tree view
            treeView.Nodes.Clear();

            // get directory info of root path
            var rootDirectoryInfo = new DirectoryInfo(rootPath);

            // build image tree nodes
            treeView.Nodes.Add(this._BuildImageTreeNode(rootDirectoryInfo, rootPath, true));
        }
        #endregion

        #region _BuildImageTreeNode
        /// <summary>
        /// Builds image file tree nodes.
        /// </summary>
        /// <param name="directoryInfo">directory info for the node we're building</param>
        /// <param name="rootPath">the root path of the images folder</param>
        private TreeNode _BuildImageTreeNode(DirectoryInfo directoryInfo, string rootPath, bool isCalledForRootPath)
        {
            // get the directory node
            TreeNode directoryNode = new TreeNode(directoryInfo.Name, string.Empty);

            // set the image for the directory node
            directoryNode.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_FOLDER, ImageFiles.EXT_PNG);

            // set the action on the directory node
            directoryNode.SelectAction = TreeNodeSelectAction.SelectExpand;

            // loop through sub directories and build nodes for those
            foreach (DirectoryInfo subDirectory in directoryInfo.GetDirectories())
            {
                // add node if the current directory is 64 (where the icons reside) or its upper directory is one of them.
                if (!isCalledForRootPath || (isCalledForRootPath && (subDirectory.Name == "64")))
                { directoryNode.ChildNodes.Add(_BuildImageTreeNode(subDirectory, rootPath, false)); }
            }

            // loop through all the files and add them to their respective directory nodes
            // only if they are image files
            foreach (FileInfo subFile in directoryInfo.GetFiles())
            {
                if (this._IsImageFile(subFile.Extension))
                {
                    TreeNode fileNode = new TreeNode(subFile.Name);

                    // get the path to the file that is relative to the root images folder
                    string filePathRelativeToRoot = subFile.FullName.Substring(rootPath.Length, subFile.FullName.Length - rootPath.Length);
                    //filePathRelativeToRoot = subFile.FullName.Replace(rootPath.Substring(0, rootPath.IndexOf("/") - 1), String.Empty);
                    filePathRelativeToRoot = filePathRelativeToRoot.Replace("\\", "/");
                    filePathRelativeToRoot = filePathRelativeToRoot.Replace(SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET, "");
                    filePathRelativeToRoot = filePathRelativeToRoot.Replace(SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET, "");

                    // build the url to the file based on whether or not it exist within the site's images folder
                    string fileURL = String.Empty;

                    if (File.Exists(Server.MapPath(SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET + filePathRelativeToRoot)))
                    {
                        fileNode.Text += " (" + _GlobalResources.CUSTOMIZED + ")";
                        fileURL = SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET + filePathRelativeToRoot; 
                    }
                    else
                    { fileURL = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET + filePathRelativeToRoot; }

                    // set the select action, url, and image for the file node
                    fileNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    fileNode.Value = fileURL;
                    fileNode.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_IMAGE, ImageFiles.EXT_PNG);

                    // attach file node
                    directoryNode.ChildNodes.Add(fileNode);
                }
            }

            // return directory node
            return directoryNode;
        }
        #endregion

        #region _IsImageFile
        /// <summary>
        /// Checks if file is an image file.
        /// </summary>
        /// <param name="fileExtension">extension of the file to be checked</param>
        private bool _IsImageFile(string fileExtension)
        {
            switch (fileExtension.ToLower())
            {
                case ".jpg":
                case ".jpeg":
                case ".bmp":
                case ".gif":
                case ".png":
                    return true;
                default:
                    return false;
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);

            // revert to default button
            this._RevertToDefaultButton = new Button();
            this._RevertToDefaultButton.ID = "RevertToDefaultButton";
            this._RevertToDefaultButton.CssClass = "Button NonActionButton";
            this._RevertToDefaultButton.Text = _GlobalResources.RevertToDefault;
            this._RevertToDefaultButton.Command += new CommandEventHandler(this._RevertToDefaultButton_Command);

            // add controls to body
            this.ActionsPanel.Controls.Add(this._SaveButton);
            this.ActionsPanel.Controls.Add(this._RevertToDefaultButton);
        }
        #endregion

        #region _SelectedNodeChanged
        /// <summary>
        /// Handles the selected node change of the tree view.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _SelectedNodeChanged(object sender, EventArgs e)
        {
            // if selected node value is not null or empty space
            if (!string.IsNullOrWhiteSpace(this.ImageFileTree.SelectedNode.Value))
            {
                this.SelectedImage.ImageUrl = this.ImageFileTree.SelectedNode.Value + "?" + DateTime.UtcNow.ToString("o");
                this.SelectedImageURL.Value = this.ImageFileTree.SelectedNode.Value;

                // hidden field variable set to the specific extension type to validate. 
                HiddenField extensionToValidateHiddenField = (HiddenField)this._ImageUploader.FindControl("ImageUploader_ExtensionToValidateHiddenField");
                if (extensionToValidateHiddenField != null)
                { extensionToValidateHiddenField.Value = Path.GetExtension(this.ImageFileTree.SelectedNode.Text).Replace(".", ""); }

                this.SelectedImage.Visible = true;
                this.ActionsPanel.Style.Add("display", "block");
                this.ImageSelectionPanel.Style.Add("display", "block");

                if (this.ImageFileTree.SelectedNode.Value.Contains(SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET))
                {
                    this._RevertToDefaultButton.Enabled = true;
                    this._RevertToDefaultButton.CssClass = "Button NonActionButton";
                }
                else
                {
                    this._RevertToDefaultButton.Enabled = false;
                    this._RevertToDefaultButton.CssClass = "Button NonActionButton DisabledButton";
                }
            }
            else
            {
                this.SelectedImage.Visible = false;
                this.ActionsPanel.Style.Add("display", "none");
                this.ImageSelectionPanel.Style.Add("display", "none");
            }
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Performs the save action on the page.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        protected void _SaveButton_Command(object sender, EventArgs e)
        {
            try
            {
                // do validation
                if (String.IsNullOrWhiteSpace(this._ImageUploader.SavedFilePath))
                { throw new AsentiaException(_GlobalResources.NoImageFileWasUploaded); }

                if (String.IsNullOrWhiteSpace(this.SelectedImageURL.Value))
                { throw new AsentiaException(_GlobalResources.NoImageFileToReplace); }

                // validate the image type
                string SavedFilePathExtension = Path.GetExtension(this._ImageUploader.SavedFilePath);
                string SelectedImageURLExtension = Path.GetExtension(this.SelectedImageURL.Value);

                if (SavedFilePathExtension != SelectedImageURLExtension)
                { throw new AsentiaException(_GlobalResources.ImageTypeMustMatchWithPrevious); }

                // get the path to the file that is relative to the root images folder
                string filePathRelativeToRoot = this.SelectedImageURL.Value;
                filePathRelativeToRoot = filePathRelativeToRoot.Replace(SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET, "");
                filePathRelativeToRoot = filePathRelativeToRoot.Replace(SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET, "");

                // get the folder path relative to the root images folder
                string fileFolderRelativeToRoot = Path.GetDirectoryName(filePathRelativeToRoot);
                fileFolderRelativeToRoot.Replace("\\", "/");

                // create directory if not exists
                if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET + fileFolderRelativeToRoot)))
                { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET + fileFolderRelativeToRoot)); }

                // move the uploaded file into the folder
                File.Copy(Server.MapPath(this._ImageUploader.SavedFilePath), Server.MapPath(SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET + filePathRelativeToRoot), true);
                File.Delete(Server.MapPath(this._ImageUploader.SavedFilePath));

                // display image, enable actions panel, and enable revert to default button
                this.SelectedImage.Visible = true;
                this.SelectedImage.ImageUrl = SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET + filePathRelativeToRoot + "?r=" + Guid.NewGuid();
                this.ActionsPanel.Style.Add("display", "block");
                this.ImageSelectionPanel.Style.Add("display", "block");
                this._RevertToDefaultButton.Enabled = true;
                this._RevertToDefaultButton.CssClass = "Button NonActionButton";

                // display success message to user
                this.DisplayFeedback(String.Format(_GlobalResources.ImageXWasSuccessfullySaved, filePathRelativeToRoot), false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display error message to user if any exception occur
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _RevertToDefaultButton_Command
        /// <summary>
        /// Reverts the image back to the default.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        protected void _RevertToDefaultButton_Command(object sender, EventArgs e)
        {
            try
            {
                // get the path to the file that is relative to the root images folder
                string filePathRelativeToRoot = this.SelectedImageURL.Value;
                filePathRelativeToRoot = filePathRelativeToRoot.Replace(SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET, "");
                filePathRelativeToRoot = filePathRelativeToRoot.Replace(SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET, "");

                // delete the uploaded file
                File.Delete(Server.MapPath(SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET + filePathRelativeToRoot));

                // display image, enable actions panel, and disable revert to default button
                this.SelectedImage.Visible = true;
                this.SelectedImage.ImageUrl = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET + filePathRelativeToRoot + "?r=" + Guid.NewGuid();
                this.ActionsPanel.Style.Add("display", "block");
                this.ImageSelectionPanel.Style.Add("display", "block");
                this._RevertToDefaultButton.Enabled = false;
                this._RevertToDefaultButton.CssClass = "Button NonActionButton DisabledButton";

                // display success message to user
                this.DisplayFeedback(String.Format(_GlobalResources.ImageXWasSuccessfullyReverted, filePathRelativeToRoot), false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display error message to user if any exception occur
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion
    }
}
