﻿// change logo image for home page 
function ChangeLogoImagePreview(uploadedFilePath) {
    // set hidden field value to uploaded image path
    $("#LogoImageHiddenField").val(uploadedFilePath);

    // add uploaded logo image to preview
    $("#MastheadHomePageLogoImage").prop("src", uploadedFilePath);

    // make remove image button enabled
    $("#LogoImageHomePageRemoveButton").prop("href", "#");
    $("#LogoImageHomePageRemoveButton").removeClass("aspNetDisabled");
    $("#LogoImageHomePageRemoveButtonIcon").removeClass("DimIcon");
}

// remove logo image for home page 
function RemoveLogoImagePreview() {
    // remove value from hidden field
    $("#LogoImageHiddenField").val("");

    // remove logo image from preview
    $("#MastheadHomePageLogoImage").prop("src", "");

    // make remove image button dim and disabled
    $("#LogoImageHomePageRemoveButton").removeAttr("href");
    $("#LogoImageHomePageRemoveButton").addClass("aspNetDisabled");
    $("#LogoImageHomePageRemoveButtonIcon").addClass("DimIcon");
}

// Change logo image for Application
function ChangeLogoImagePreviewApplication(uploadedFilePath) {
    // set hidden field value to uploaded image path
    $("#LogoImageHiddenFieldApplication").val(uploadedFilePath);

    // add uploaded logo image to preview
    $("#MastheadApplicationLogoImage").prop("src", uploadedFilePath);

    // make remove image button enabled
    $("#LogoImageApplicationRemoveButton").prop("href", "#");
    $("#LogoImageApplicationRemoveButton").removeClass("aspNetDisabled");
    $("#LogoImageApplicationRemoveButtonIcon").removeClass("DimIcon");
}

// Remove logo image for Application
function RemoveLogoImagePreviewApplication() {
    // remove value from hidden field
    $("#LogoImageHiddenFieldApplication").val("");

    // remove logo image from preview
    $("#MastheadApplicationLogoImage").prop("src", "");

    // make remove image button dim and disabled
    $("#LogoImageApplicationRemoveButton").removeAttr("href");
    $("#LogoImageApplicationRemoveButton").addClass("aspNetDisabled");
    $("#LogoImageApplicationRemoveButtonIcon").addClass("DimIcon");
}

// Change icon image for Application
function ChangeIconImagePreviewApplication(uploadedFilePath) {
    // set hidden field value to uploaded image path
    $("#IconImageHiddenFieldApplication").val(uploadedFilePath);

    // add uploaded logo image to preview
    $("#MastheadApplicationIconImage").prop("src", uploadedFilePath);

    // make remove image button enabled
    $("#IconImageApplicationRemoveButton").prop("href", "#");
    $("#IconImageApplicationRemoveButton").removeClass("aspNetDisabled");
    $("#IconImageApplicationRemoveButtonIcon").removeClass("DimIcon");
}

// Remove icon image for Application
function RemoveIconImagePreviewApplication() {
    // remove value from hidden field
    $("#IconImageHiddenFieldApplication").val("");

    // remove icon image from preview
    $("#MastheadApplicationIconImage").prop("src", "");

    // make remove image button dim and disabled
    $("#IconImageApplicationRemoveButton").removeAttr("href");
    $("#IconImageApplicationRemoveButton").addClass("aspNetDisabled");
    $("#IconImageApplicationRemoveButtonIcon").addClass("DimIcon");
}

// Change secondary image preview 
function ChangeSecondaryImagePreview(uploadedFilePath) {
    // set hidden field value to uploaded image path
    $("#SecondaryImageHiddenField").val(uploadedFilePath);

    // add secondary image to preview
    $("#MastheadHomePageSecondaryImage").prop("src", uploadedFilePath);

    // make remove image button enabled
    $("#SecondaryImageRemoveButton").prop("href", "#");
    $("#SecondaryImageRemoveButton").removeClass("aspNetDisabled");
    $("#SecondaryImageRemoveButtonIconHomePage").removeClass("DimIcon");
}

// Remove secondary image preview
function RemoveSecondaryImagePreview() {
    // remove value from hidden field
    $("#SecondaryImageHiddenField").val("");

    // remove secondary image from preview
    $("#MastheadHomePageSecondaryImage").prop("src", "");

    // make remove image button dim and disabled
    $("#SecondaryImageRemoveButton").removeAttr("href");
    $("#SecondaryImageRemoveButton").addClass("aspNetDisabled");
    $("#SecondaryImageRemoveButtonIconHomePage").addClass("DimIcon");
}

// Change background image preview
function ChangeBackgroundImagePreview(uploadedFilePath) {
    // set hidden field value to uploaded image path
    $("#BackgroundImageHiddenField").val(uploadedFilePath);

    // add background image to preview
    $("#MastheadHomePagePreviewContainer").css("background-image", "url('" + uploadedFilePath + "')");
    $("#MastheadHomePagePreviewContainer").css("background-repeat", "repeat-x");

    // make remove image button enabled
    $("#BackgroundImageRemoveButton").prop("href", "#");
    $("#BackgroundImageRemoveButton").removeClass("aspNetDisabled");
    $("#BackgroundImageRemoveButtonIcon").removeClass("DimIcon");
}

// Remove background image preview
function RemoveBackgroundImagePreview() {
    // remove value from hidden field
    $("#BackgroundImageHiddenField").val("");

    // remove background image from preview
    $("#MastheadHomePagePreviewContainer").css("background-image", "");
    $("#MastheadHomePagePreviewContainer").css("background-repeat", "");

    // if a background-color property is set, change it to background
    if ($("#MastheadHomePagePreviewContainer").css("background-color") != "") {
        $("#MastheadHomePagePreviewContainer").css("background", $("#MastheadHomePagePreviewContainer").css("background-color"));
    }

    // if both the image and color are gone, remove all related styles
    if ($("#BackgroundImageHiddenField").val() == "" && $("#SelectedColorHiddenField").val() == "") {
        $("#MastheadHomePagePreviewContainer").css("background", "");
        $("#MastheadHomePagePreviewContainer").css("background-color", "");
        $("#MastheadHomePagePreviewContainer").css("background-image", "");
        $("#MastheadHomePagePreviewContainer").css("background-repeat", "");
    }

    // make remove image button dim and disabled
    $("#BackgroundImageRemoveButton").removeAttr("href");
    $("#BackgroundImageRemoveButton").addClass("aspNetDisabled");
    $("#BackgroundImageRemoveButtonIcon").addClass("DimIcon");
}

// Change background color preview
function ChangeBackgroundColorPreview(backgroundColor) {
    // set hidden field value to selected color
    $("#SelectedColorHiddenField").val(backgroundColor);
    
    // add background color to preview, if there is no image, use "background"; else, use "background-color"
    if ($("#BackgroundImageHiddenField").val() == "") {
        $("#MastheadHomePagePreviewContainer").css("background", backgroundColor);
    }
    else {
        $("#MastheadHomePagePreviewContainer").css("background-color", backgroundColor);
    }

    // make remove color button enabled
    $("#BackgroundColorRemoveButtonHomePage").prop("href", "#");
    $("#BackgroundColorRemoveButtonHomePage").removeClass("aspNetDisabled");
    $("#BackgroundColorHomePageRemoveButtonIcon").removeClass("DimIcon");
}

// Change background color preview for application masthead
function ChangeBackgroundColorPreviewApplication(backgroundColor) {

    RemoveBackgroundColorPreviewApplication();

    // set hidden field value to selected color
    $("#SelectedColorHiddenFieldApplication").val(backgroundColor);

    // add background color to preview, if there is no image, use "background"; else, use "background-color"
    //$("#MastheadApplicationPreviewContainer").css("background", backgroundColor);
    $("#MastheadApplicationPreviewContainer").css("background-color", backgroundColor);
    
    // make remove color button enabled
    $("#BackgroundColorRemoveButtonApplication").prop("href", "#");
    $("#BackgroundColorRemoveButtonApplication").removeClass("aspNetDisabled");
    $("#BackgroundColorApplicationRemoveButtonIcon").removeClass("DimIcon");
}

// Remove background color preview
function RemoveBackgroundColorPreview() {
    // remove value from hidden field
    $("#SelectedColorHiddenField").val("");

    // remove background color from preview
    $("#MastheadHomePagePreviewContainer").css("background-color", "");

    // if both the image and color are gone, remove all related styles
    if ($("#BackgroundImageHiddenField").val() == "" && $("#SelectedColorHiddenField").val() == "") {
        $("#MastheadHomePagePreviewContainer").css("background", "");
        $("#MastheadHomePagePreviewContainer").css("background-color", "");
        $("#MastheadHomePagePreviewContainer").css("background-image", "");
        $("#MastheadHomePagePreviewContainer").css("background-repeat", "");
    }

    // make remove color button dim and disabled
    $("#BackgroundColorRemoveButtonHomePage").removeAttr("href");
    $("#BackgroundColorRemoveButtonHomePage").addClass("aspNetDisabled");
    $("#BackgroundColorApplicationRemoveButtonIcon").addClass("DimIcon");
}

// Remove background color preview for application masthead
function RemoveBackgroundColorPreviewApplication() {
    // remove value from hidden field
    $("#SelectedColorHiddenFieldApplication").val("");

    // remove background color from preview
    $("#MastheadApplicationPreviewContainer").css("background-color", "");

    // if both the image and color are gone, remove all related styles
    if ($("#SelectedColorHiddenFieldApplication").val() == "") {
        $("#MastheadApplicationPreviewContainer").css("background", "");
        $("#MastheadApplicationPreviewContainer").css("background-color", "");
        $("#MastheadApplicationPreviewContainer").css("background-image", "");
        $("#MastheadApplicationPreviewContainer").css("background-repeat", "");
    }

    // make remove color button dim and disabled
    $("#BackgroundColorRemoveButtonApplication").removeAttr("href");
    $("#BackgroundColorRemoveButtonApplication").addClass("aspNetDisabled");
    $("#BackgroundColorApplicationRemoveButtonIcon").addClass("DimIcon");
}