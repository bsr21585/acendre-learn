﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages.Administrator.System.Users
{
    /// <summary>
    /// Class to manage user field configuration
    /// </summary>
    public class UserFieldConfiguration : AsentiaAuthenticatedPage
    {
        #region Public Properties
        public Panel UserFieldConfigurationFormContentWrapperContainer;
        public Panel UserFieldConfigurationWrapperContainer;
        public Panel ObjectLanguageSelectorContainer;
        public Panel PageInstructionsPanel;
        public LanguageSelector LanguageSelectorControl;
        public Panel UserFieldConfigurationPropertiesContainer;
        public Panel RegistrationInstructionsContainer;
        public Panel UserAccountDataTableContainer;
        public Panel FormPreviewsContainer;
        public Panel UserFieldConfigurationPropertiesTabPanelsContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private UserAccountData _UserAccountDataObject;
        private Table _UserAccountDataTable = new Table();
        private string _SelectedLanguage;
        private HiddenField _CurrentFieldsWithDateType;
        private LinkButton _AdminViewPreviewButton;
        private LinkButton _UserViewPreviewButton;
        private LinkButton _RegistrationFormPreviewButton;
        private HiddenField _AdministratorViewPreviewModalTargetPlaceholder;
        private HiddenField _UserViewPreviewModalTargetPlaceholder;
        private HiddenField _RegistrationFormPreviewModalTargetPlaceholder;
        private UpdatePanel _FormPreviewAdministratorViewUpdatePanel;
        private UpdatePanel _FormPreviewUserViewUpdatePanel;
        private UpdatePanel _FormPreviewRegistrationFormUpdatePanel;
        private ModalPopup _FormPreviewModalAdministratorView;
        private ModalPopup _FormPreviewModalUserView;
        private ModalPopup _FormPreviewModalRegistrationForm;
        private Button _SaveButton;
        private Button _CancelButton;
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overridden OnPreRender method for adding ck editor and script file
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(UserFieldConfiguration), "Asentia.UMS.Pages.Administrator.System.Users.UserFieldConfiguration.js");

            // for multiple "start up" scripts, we need to build start up calls
            // and add them to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));
            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine(" InitializeUserFieldConfigurationPage();");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);

            base.OnPreRender(e);
        }
        #endregion
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_UserFieldConfiguration))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/system/users/UserFieldConfiguration.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.UserFieldConfiguration));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, _GlobalResources.UserFieldConfiguration, ImageFiles.GetIconPath(ImageFiles.ICON_USERACCOUNTDATA,
                                                                                                                                     ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.UserFieldConfigurationFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.UserFieldConfigurationWrapperContainer.CssClass = "FormContentContainer";

            // get the language querystring parameter
            this._SelectedLanguage = this.QueryStringString("lang", AsentiaSessionState.GlobalSiteObject.LanguageString);

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.ConfigureFunctionalityAndDisplaySettingsForTheUserAccountFields, true);

            // build the language selector control
            this._BuildLanguageSelectorContainer();

            // build the user field configuration properties form tabs
            this._BuildUserFieldConfigurationPropertiesFormTabs();

            this.UserFieldConfigurationPropertiesTabPanelsContainer = new Panel();
            this.UserFieldConfigurationPropertiesTabPanelsContainer.ID = "UserFieldConfigurationProperties_TabPanelsContainer";
            this.UserFieldConfigurationPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.UserFieldConfigurationPropertiesContainer.Controls.Add(this.UserFieldConfigurationPropertiesTabPanelsContainer);

            // create the user account data object
            this._UserAccountDataObject = new UserAccountData(UserAccountDataFileType.Site, true, false);

            // build the registration instructions container
            this._BuildRegistrationInstructionsContainer();

            // build the user account data table
            this._BuildUserAccountDataTable();

            // build the form previews container
            this._BuildFormPreviewsContainer();

            // build the actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region Private methods
        #region _BuildLanguageSelectorContainer
        /// <summary>
        /// Builds a language selector control for selecting which language you
        /// are editing an object in.
        /// </summary>
        private void _BuildLanguageSelectorContainer()
        {
            this.ObjectLanguageSelectorContainer.CssClass = "ObjectLanguageSelectorContainer";

            // build the label
            Localize LanguageSelectorLabel = new Localize();
            LanguageSelectorLabel.Text = _GlobalResources.LanguageSelector;

            // build the control
            this.LanguageSelectorControl = new LanguageSelector(LanguageDropDownType.Object);

            // set the selected value
            this.LanguageSelectorControl.SelectedValue = this._SelectedLanguage;

            // attach the controls to the container
            this.ObjectLanguageSelectorContainer.Controls.Add(LanguageSelectorLabel);
            this.ObjectLanguageSelectorContainer.Controls.Add(this.LanguageSelectorControl);
        }
        #endregion

        #region _BuildUserFieldConfigurationPropertiesFormTabs
        /// <summary>
        /// Method to build form tab 
        /// </summary>
        private void _BuildUserFieldConfigurationPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("RegistrationInstructions", _GlobalResources.Instructions));
            tabs.Enqueue(new KeyValuePair<string, string>("DataFields", _GlobalResources.DataFields));
            tabs.Enqueue(new KeyValuePair<string, string>("FormPreviews", _GlobalResources.FormPreviews));

            // build and attach the tabs
            this.UserFieldConfigurationPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("UserFieldConfigurationProperties", tabs, null, this.Page, null));
        }
        #endregion

        #region _BuildRegistrationInstructionsContainer
        /// <summary>
        /// Builds the container and multi-line text box that contains the registration instructions.
        /// </summary>
        private void _BuildRegistrationInstructionsContainer()
        {
            this.RegistrationInstructionsContainer = new Panel();
            this.RegistrationInstructionsContainer.ID = "UserFieldConfigurationProperties_" + "RegistrationInstructions" + "_TabPanel";
            this.RegistrationInstructionsContainer.Attributes.Add("style", "display: block;");

            Panel registrationInstructionsTextBoxContainer = new Panel();
            registrationInstructionsTextBoxContainer.ID = "uad_RegistrationInstructionsTextBoxContainer";

            TextBox registrationInstructionsTextBox = new TextBox();
            registrationInstructionsTextBox.ID = "uad_RegistrationInstructionsTextBox";
            registrationInstructionsTextBox.CssClass = "ckeditor";
            registrationInstructionsTextBox.Style.Add("width", "100%");
            registrationInstructionsTextBox.TextMode = TextBoxMode.MultiLine;
            registrationInstructionsTextBox.Rows = 10;
            registrationInstructionsTextBox.Text = this._UserAccountDataObject.GetRegistrationInstructionsHTMLInLanguage(this._SelectedLanguage);
            registrationInstructionsTextBoxContainer.Controls.Add(registrationInstructionsTextBox);

            this.RegistrationInstructionsContainer.Controls.Add(registrationInstructionsTextBoxContainer);

            this.UserFieldConfigurationPropertiesTabPanelsContainer.Controls.Add(this.RegistrationInstructionsContainer);
        }
        #endregion

        #region _BuildUserAccountDataTable
        /// <summary>
        /// Builds the user account data table.
        /// </summary>
        private void _BuildUserAccountDataTable()
        {
            this.UserAccountDataTableContainer = new Panel();
            this.UserAccountDataTableContainer.ID = "UserFieldConfigurationProperties_" + "DataFields" + "_TabPanel";
            this.UserAccountDataTableContainer.Attributes.Add("style", "display: none;");

            // hidden field to store field names that currently have the "Date" type assigned to them
            this._CurrentFieldsWithDateType = new HiddenField();
            this._CurrentFieldsWithDateType.ID = "CurrentFieldsWithDateType";
            this.UserAccountDataTableContainer.Controls.Add(this._CurrentFieldsWithDateType);

            // hidden field to store tab and user field order
            HiddenField tabAndUserFieldOrder = new HiddenField();
            tabAndUserFieldOrder.ID = "uad_tabAndUserFieldOrder";
            this.UserAccountDataTableContainer.Controls.Add(tabAndUserFieldOrder);

            // create the user account data table
            this._UserAccountDataTable.ID = "UserAccountDataTable";
            this._UserAccountDataTable.CssClass = "GridTable";

            // HEADER ROW AND COLUMNS

            // create the header row
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.ID = "UserAccountDataTableHeaderRow";
            headerRow.CssClass = "GridHeaderRow";

            // field header cell
            TableHeaderCell fieldCell = new TableHeaderCell();
            fieldCell.ID = "UserAccountDataTableFieldColumn";
            fieldCell.Text = _GlobalResources.Field;
            headerRow.Cells.Add(fieldCell);

            // label header cell
            TableHeaderCell labelCell = new TableHeaderCell();
            labelCell.ID = "UserAccountDataTableLabelColumn";
            labelCell.Text = _GlobalResources.Label;
            headerRow.Cells.Add(labelCell);

            // admin view header cell
            TableHeaderCell adminViewCell = new TableHeaderCell();
            adminViewCell.ID = "UserAccountDataTableAdminViewColumn";
            adminViewCell.Text = _GlobalResources.AdminView;
            headerRow.Cells.Add(adminViewCell);

            // user view header cell
            TableHeaderCell userViewCell = new TableHeaderCell();
            userViewCell.ID = "UserAccountDataTableUserViewColumn";
            userViewCell.Text = _GlobalResources.UserView;
            headerRow.Cells.Add(userViewCell);

            // registration form header cell
            TableHeaderCell regFormCell = new TableHeaderCell();
            regFormCell.ID = "UserAccountDataTableRegFormColumn";
            regFormCell.Text = _GlobalResources.RegForm;
            headerRow.Cells.Add(regFormCell);

            // required header cell
            TableHeaderCell reqCell = new TableHeaderCell();
            reqCell.ID = "UserAccountDataTableRequiredColumn";
            reqCell.CssClass = "centered";
            reqCell.Text = _GlobalResources.Req;
            headerRow.Cells.Add(reqCell);

            // options header cell
            TableHeaderCell optionsCell = new TableHeaderCell();
            optionsCell.ID = "UserAccountDataTableOptionsColumn";
            optionsCell.CssClass = "centered";
            optionsCell.Text = _GlobalResources.Options;
            headerRow.Cells.Add(optionsCell);

            // attach header row to table
            this._UserAccountDataTable.Rows.Add(headerRow);

            // TABS AND USER FIELDS ROWS AND COLUMNS

            // loop through user account data tabs and build the table
            foreach (UserAccountData.Tab tab in this._UserAccountDataObject.Tabs)
            {
                // create a tab row
                TableRow tabRow = new TableRow();
                tabRow.ID = "tab_" + tab.Identifier + "_Row";
                tabRow.CssClass = "Tab GridDataRow TabRow";

                // tab identifier
                TableCell tabIdentifierCell = new TableCell();
                tabIdentifierCell.ID = "tab_" + tab.Identifier + "_Identifier";
                tabIdentifierCell.CssClass = "TabIdentifierCell";
                // Rename the "tabs" to "sections"
                tabIdentifierCell.Text = tab.Identifier.Replace("tab", "section");
                tabRow.Cells.Add(tabIdentifierCell);

                // tab label
                TableCell tabLabelCell = new TableCell();
                tabLabelCell.ID = "tab_" + tab.Identifier + "_Label";
                tabLabelCell.ColumnSpan = 6;

                Panel tabLabelContainer = new Panel();
                tabLabelContainer.ID = "tab_" + tab.Identifier + "_LabelTextContainer";

                Label tabLabel = new Label();
                tabLabel.ID = "tab_" + tab.Identifier + "_LabelText";
                tabLabel.Text = HttpUtility.HtmlEncode(this._UserAccountDataObject.GetTabLabelInLanguage(tab, this._SelectedLanguage));
                tabLabelContainer.Controls.Add(tabLabel);

                Image tabEditLabelIcon = new Image();
                tabEditLabelIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                   ImageFiles.EXT_PNG);
                tabEditLabelIcon.ID = "tab_" + tab.Identifier + "_EditLabelIcon";
                tabEditLabelIcon.CssClass = "TabEditLabelIcon XSmallIcon";
                tabEditLabelIcon.Attributes.Add("onClick", "EditLabelText(this.id);");
                tabLabelContainer.Controls.Add(tabEditLabelIcon);

                tabLabelCell.Controls.Add(tabLabelContainer);

                Panel tabLabelTextBoxContainer = new Panel();
                tabLabelTextBoxContainer.ID = "tab_" + tab.Identifier + "_LabelTextBoxContainer";
                tabLabelTextBoxContainer.Style.Add("display", "none");

                TextBox tabLabelTextBox = new TextBox();
                tabLabelTextBox.ID = "tab_" + tab.Identifier + "_LabelTextBox";
                tabLabelTextBox.CssClass = "TabLabelEditTextBox";
                tabLabelTextBox.Text = this._UserAccountDataObject.GetTabLabelInLanguage(tab, this._SelectedLanguage);
                tabLabelTextBoxContainer.Controls.Add(tabLabelTextBox);

                HiddenField tabLabelField = new HiddenField();
                tabLabelField.ID = "tab_" + tab.Identifier + "_LabelField";
                tabLabelField.Value = this._UserAccountDataObject.GetTabLabelInLanguage(tab, this._SelectedLanguage);
                tabLabelTextBoxContainer.Controls.Add(tabLabelField);

                Button tabLabelOKButton = new Button();
                tabLabelOKButton.ID = "tab_" + tab.Identifier + "_LabelOKButton";
                tabLabelOKButton.OnClientClick = "ConfirmLabelEdit(this.id);return false;";
                tabLabelOKButton.CssClass = "Button ActionButton";
                tabLabelOKButton.Text = _GlobalResources.OK;
                tabLabelTextBoxContainer.Controls.Add(tabLabelOKButton);

                Button tabLabelCancelButton = new Button();
                tabLabelCancelButton.ID = "tab_" + tab.Identifier + "_LabelCancelButton";
                tabLabelCancelButton.OnClientClick = "CancelLabelEdit(this.id);return false;";
                tabLabelCancelButton.CssClass = "Button NonActionButton";
                tabLabelCancelButton.Text = _GlobalResources.Cancel;
                tabLabelTextBoxContainer.Controls.Add(tabLabelCancelButton);

                tabLabelCell.Controls.Add(tabLabelTextBoxContainer);
                tabRow.Cells.Add(tabLabelCell);

                // attach tab row to table
                this._UserAccountDataTable.Rows.Add(tabRow);

                // loop through each userfield in the tab and add user field rows to table
                int i = 0;

                foreach (UserAccountData.UserField userField in tab.UserFields)
                {
                    // get the global ruleset object for the field so we can evaluate
                    // the global rules for this field
                    UserFieldGlobalRules userFieldGlobalRuleSet = null;

                    foreach (UserFieldGlobalRules ruleSet in this._UserAccountDataObject.GlobalUserFieldRules)
                    {
                        if (ruleSet.Identifier == userField.Identifier)
                        { userFieldGlobalRuleSet = ruleSet; }
                    }

                    // throw exception if global ruleset is not found for this field
                    if (userFieldGlobalRuleSet == null)
                    { throw new AsentiaException(String.Format(_GlobalResources.GlobalRulesetNotFoundForUserFieldX, userField.Identifier)); }

                    // create a user field row
                    TableRow userFieldRow = new TableRow();
                    userFieldRow.ID = "uf_" + userField.Identifier + "_Row";
                    userFieldRow.Visible = !((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LITEUSERFIELDCONFIGURATION_ENABLE) &&
                                            (bool)(userField.Identifier.Remove(userField.Identifier.Length - 1) == "field1"));
                    // apply css for alternating rows
                    if (i % 2 == 0)
                    { userFieldRow.CssClass = "UserField GridDataRow"; }
                    else
                    { userFieldRow.CssClass = "UserField GridDataRow GridDataRowAlternate"; }

                    // user field identifier
                    TableCell userFieldIdentifierCell = new TableCell();
                    userFieldIdentifierCell.ID = "uf_" + userField.Identifier + "_Identifier";
                    userFieldIdentifierCell.CssClass = "UserFieldIdentifierCell";
                    userFieldIdentifierCell.Text = userField.Identifier;
                    userFieldRow.Cells.Add(userFieldIdentifierCell);

                    // user field label
                    TableCell userFieldLabelCell = new TableCell();
                    userFieldLabelCell.ID = "uf_" + userField.Identifier + "_Label";

                    Panel userFieldLabelContainer = new Panel();
                    userFieldLabelContainer.ID = "uf_" + userField.Identifier + "_LabelTextContainer";

                    Label userFieldLabel = new Label();
                    userFieldLabel.ID = "uf_" + userField.Identifier + "_LabelText";
                    userFieldLabel.Text = HttpUtility.HtmlEncode(this._UserAccountDataObject.GetUserFieldLabelInLanguage(userField, this._SelectedLanguage));
                    userFieldLabelContainer.Controls.Add(userFieldLabel);

                    Image userFieldEditLabelIcon = new Image();
                    userFieldEditLabelIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                             ImageFiles.EXT_PNG);
                    userFieldEditLabelIcon.ID = "uf_" + userField.Identifier + "_EditLabelIcon";
                    userFieldEditLabelIcon.CssClass = "UserFieldEditLabelIcon XSmallIcon";
                    userFieldEditLabelIcon.Attributes.Add("onClick", "EditLabelText(this.id);");
                    userFieldLabelContainer.Controls.Add(userFieldEditLabelIcon);

                    userFieldLabelCell.Controls.Add(userFieldLabelContainer);

                    Panel userFieldLabelTextBoxContainer = new Panel();
                    userFieldLabelTextBoxContainer.ID = "uf_" + userField.Identifier + "_LabelTextBoxContainer";
                    userFieldLabelTextBoxContainer.Style.Add("display", "none");

                    TextBox userFieldLabelTextBox = new TextBox();
                    userFieldLabelTextBox.ID = "uf_" + userField.Identifier + "_LabelTextBox";
                    userFieldLabelTextBox.CssClass = "UserFieldLabelEditTextBox";
                    userFieldLabelTextBox.Text = this._UserAccountDataObject.GetUserFieldLabelInLanguage(userField, this._SelectedLanguage);
                    userFieldLabelTextBoxContainer.Controls.Add(userFieldLabelTextBox);

                    HiddenField userFieldLabelField = new HiddenField();
                    userFieldLabelField.ID = "uf_" + userField.Identifier + "_LabelField";
                    userFieldLabelField.Value = this._UserAccountDataObject.GetUserFieldLabelInLanguage(userField, this._SelectedLanguage);
                    userFieldLabelTextBoxContainer.Controls.Add(userFieldLabelField);

                    Button userFieldLabelOKButton = new Button();
                    userFieldLabelOKButton.ID = "uf_" + userField.Identifier + "_LabelOKButton";
                    userFieldLabelOKButton.OnClientClick = "ConfirmLabelEdit(this.id);return false;";
                    userFieldLabelOKButton.CssClass = "Button ActionButton";
                    userFieldLabelOKButton.Text = _GlobalResources.OK;
                    userFieldLabelTextBoxContainer.Controls.Add(userFieldLabelOKButton);

                    Button userFieldLabelCancelButton = new Button();
                    userFieldLabelCancelButton.ID = "uf_" + userField.Identifier + "_LabelCancelButton";
                    userFieldLabelCancelButton.OnClientClick = "CancelLabelEdit(this.id);return false;";
                    userFieldLabelCancelButton.CssClass = "Button NonActionButton";
                    userFieldLabelCancelButton.Text = _GlobalResources.Cancel;
                    userFieldLabelTextBoxContainer.Controls.Add(userFieldLabelCancelButton);

                    userFieldLabelCell.Controls.Add(userFieldLabelTextBoxContainer);
                    userFieldRow.Cells.Add(userFieldLabelCell);

                    // user field admin view
                    TableCell userFieldAdminViewCell = new TableCell();
                    userFieldAdminViewCell.ID = "uf_" + userField.Identifier + "_AdminView";
                    CheckBox adminViewCheckBox = new CheckBox();
                    userFieldAdminViewCell.Controls.Add(this._BuildViewModifyCheckBoxes("AdministratorEditPage", userField, userFieldGlobalRuleSet));
                    userFieldRow.Cells.Add(userFieldAdminViewCell);

                    // user field user view
                    TableCell userFieldUserViewCell = new TableCell();
                    userFieldUserViewCell.ID = "uf_" + userField.Identifier + "_UserView";
                    userFieldUserViewCell.Controls.Add(this._BuildViewModifyCheckBoxes("UserEditPage", userField, userFieldGlobalRuleSet));
                    userFieldRow.Cells.Add(userFieldUserViewCell);

                    // user field registration form view
                    TableCell userFieldRegFormCell = new TableCell();
                    userFieldRegFormCell.ID = "uf_" + userField.Identifier + "_RegFormView";
                    userFieldRegFormCell.Controls.Add(this._BuildViewModifyCheckBoxes("RegistrationPage", userField, userFieldGlobalRuleSet));
                    userFieldRow.Cells.Add(userFieldRegFormCell);

                    // user field required
                    TableCell userFieldReqCell = new TableCell();
                    userFieldReqCell.ID = "uf_" + userField.Identifier + "_Required";
                    userFieldReqCell.CssClass = "centered";
                    userFieldReqCell.Controls.Add(this._BuildIsRequiredCheckBox(userField, userFieldGlobalRuleSet));
                    userFieldRow.Cells.Add(userFieldReqCell);

                    // user field options button
                    TableCell userFieldOptionsCell = new TableCell();
                    userFieldOptionsCell.ID = "uf_" + userField.Identifier + "_Options";
                    userFieldOptionsCell.CssClass = "centered";
                    Image optionsIcon = new Image();
                    optionsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                  ImageFiles.EXT_PNG);
                    optionsIcon.ID = "uf_" + userField.Identifier + "_OptionsIcon";
                    optionsIcon.CssClass = "UserFieldOptionsIcon SmallIcon";
                    optionsIcon.Attributes.Add("onClick", "EditOptionsRow(this.id);");
                    userFieldOptionsCell.Controls.Add(optionsIcon);
                    userFieldRow.Cells.Add(userFieldOptionsCell);

                    // attach user field row to table
                    this._UserAccountDataTable.Rows.Add(userFieldRow);

                    // create a user field options properties row
                    TableRow userFieldOptionsRow = new TableRow();
                    userFieldOptionsRow.ID = "uf_" + userField.Identifier + "_OptionsRow";
                    userFieldOptionsRow.Style.Add("display", "none");

                    // apply css for alternating rows to options row
                    if (i % 2 == 0)
                    { userFieldOptionsRow.CssClass = "GridDataRow"; }
                    else
                    { userFieldOptionsRow.CssClass = "GridDataRow GridDataRowAlternate"; }

                    // user field options cell
                    TableCell userFieldOptionsContainerCell = new TableCell();
                    userFieldOptionsContainerCell.ID = "uf_" + userField.Identifier + "_OptionsContainerCell";
                    userFieldOptionsContainerCell.ColumnSpan = 7;

                    // LEFT SIDE OF OPTIONS ROW

                    Panel leftSideContainerPanel = new Panel();
                    leftSideContainerPanel.ID = "uf_" + userField.Identifier + "_OptionsLeftSideContainer";
                    leftSideContainerPanel.CssClass = "UserFieldOptionsLeftSideContainer";

                    // description
                    Panel descriptionContainerPanel = new Panel();
                    Panel descriptionTitlePanel = new Panel();
                    Panel descriptionInstructionsPanel = new Panel();
                    Panel descriptionTextBoxPanel = new Panel();
                    Literal descriptionTitle = new Literal();
                    Literal descriptionInstructions = new Literal();
                    TextBox descriptionTextBox = new TextBox();

                    descriptionContainerPanel.ID = "uf_" + userField.Identifier + "_DescriptionContainer";
                    descriptionContainerPanel.CssClass = "UserFieldDescriptionContainer";

                    descriptionTitlePanel.ID = "uf_" + userField.Identifier + "_DescriptionTitleContainer";
                    descriptionTitlePanel.CssClass = "UserFieldDescriptionTitleContainer";

                    descriptionInstructionsPanel.ID = "uf_" + userField.Identifier + "_DescriptionInstructionsContainer";
                    descriptionInstructionsPanel.CssClass = "UserFieldDescriptionInstructionsContainer";

                    descriptionTitle.Text = _GlobalResources.Description;
                    descriptionInstructions.Text = _GlobalResources.EnterADetailedDescriptionOfTheField;

                    descriptionTextBox.ID = "uf_" + userField.Identifier + "_DescriptionTextBox";
                    descriptionTextBox.TextMode = TextBoxMode.MultiLine;
                    descriptionTextBox.Rows = 7;
                    descriptionTextBox.CssClass = "UserFieldDescriptionTextBox";
                    descriptionTextBox.Text = this._UserAccountDataObject.GetUserFieldDescriptionInLanguage(userField, this._SelectedLanguage);

                    HiddenField descriptionField = new HiddenField();
                    descriptionField.ID = "uf_" + userField.Identifier + "_DescriptionField";
                    descriptionField.Value = this._UserAccountDataObject.GetUserFieldDescriptionInLanguage(userField, this._SelectedLanguage);

                    descriptionTitlePanel.Controls.Add(descriptionTitle);
                    descriptionInstructionsPanel.Controls.Add(descriptionInstructions);
                    descriptionTextBoxPanel.Controls.Add(descriptionTextBox);
                    descriptionTextBoxPanel.Controls.Add(descriptionField);

                    descriptionContainerPanel.Controls.Add(descriptionTitlePanel);
                    descriptionContainerPanel.Controls.Add(descriptionInstructionsPanel);
                    descriptionContainerPanel.Controls.Add(descriptionTextBoxPanel);

                    leftSideContainerPanel.Controls.Add(descriptionContainerPanel);

                    // OK & Cancel buttons
                    Panel optionsOkCancelContainerPanel = new Panel();
                    optionsOkCancelContainerPanel.ID = "uf_" + userField.Identifier + "_OptionsOKCancelContainer";
                    optionsOkCancelContainerPanel.CssClass = "UserFieldOptionsOKCancelContainer";

                    Button userFieldOptionsOKButton = new Button();
                    userFieldOptionsOKButton.ID = "uf_" + userField.Identifier + "_OptionsOKButton";
                    userFieldOptionsOKButton.OnClientClick = "ConfirmOptionsRowEdit(this.id);return false;";
                    userFieldOptionsOKButton.CssClass = "Button ActionButton";
                    userFieldOptionsOKButton.Text = _GlobalResources.OK;
                    optionsOkCancelContainerPanel.Controls.Add(userFieldOptionsOKButton);

                    Button userFieldOptionsCancelButton = new Button();
                    userFieldOptionsCancelButton.ID = "uf_" + userField.Identifier + "_OptionsCancelButton";
                    userFieldOptionsCancelButton.OnClientClick = "CancelOptionsRowEdit(this.id);return false;";
                    userFieldOptionsCancelButton.CssClass = "Button NonActionButton";
                    userFieldOptionsCancelButton.Text = _GlobalResources.Cancel;
                    optionsOkCancelContainerPanel.Controls.Add(userFieldOptionsCancelButton);

                    leftSideContainerPanel.Controls.Add(optionsOkCancelContainerPanel);

                    // attach left side container
                    userFieldOptionsContainerCell.Controls.Add(leftSideContainerPanel);

                    // RIGHT SIDE OF OPTIONS ROW

                    Panel rightSideContainerPanel = new Panel();
                    rightSideContainerPanel.ID = "uf_" + userField.Identifier + "_OptionsRightSideContainer";
                    rightSideContainerPanel.CssClass = "UserFieldOptionsRightSideContainer";

                    // regular expression, and type and choice values - only for string data types, except for avatar
                    if (userFieldGlobalRuleSet.DataType == UserAccountData.DataType.String && userFieldGlobalRuleSet.Identifier != "avatar")
                    {
                        // input type and choice values - only for isTypeable
                        if (userFieldGlobalRuleSet.IsTypeable)
                        {
                            // input type
                            Panel inputTypeContainerPanel = new Panel();
                            Panel inputTypeTitlePanel = new Panel();
                            Panel inputTypeInstructionsPanel = new Panel();
                            Panel inputTypeTextBoxPanel = new Panel();
                            Literal inputTypeTitle = new Literal();
                            Literal inputTypeInstructions = new Literal();
                            DropDownList inputTypeDropDown = new DropDownList();

                            inputTypeContainerPanel.ID = "uf_" + userField.Identifier + "_InputTypeContainer";
                            inputTypeContainerPanel.CssClass = "UserFieldInputTypeContainer";

                            inputTypeTitlePanel.ID = "uf_" + userField.Identifier + "_InputTypeTitleContainer";
                            inputTypeTitlePanel.CssClass = "UserFieldInputTypeTitleContainer";

                            inputTypeInstructionsPanel.ID = "uf_" + userField.Identifier + "_InputTypeInstructionsContainer";
                            inputTypeInstructionsPanel.CssClass = "UserFieldInputTypeInstructionsContainer";

                            inputTypeTitle.Text = _GlobalResources.InputType + ": ";
                            this.FormatFormInformationPanel(inputTypeInstructionsPanel, _GlobalResources.ChangingFromOtherInputTypesToADateInputWillClearAllExistingDataForThisField, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

                            inputTypeDropDown.ID = "uf_" + userField.Identifier + "_InputTypeDropDown";

                            inputTypeDropDown.Items.Add(new ListItem(_GlobalResources.TextField, UserAccountData.InputType.TextField.ToString()));
                            inputTypeDropDown.Items.Add(new ListItem(_GlobalResources.Date, UserAccountData.InputType.Date.ToString()));
                            inputTypeDropDown.Items.Add(new ListItem(_GlobalResources.RadioButtons, UserAccountData.InputType.RadioButtons.ToString()));
                            inputTypeDropDown.Items.Add(new ListItem(_GlobalResources.Checkboxes, UserAccountData.InputType.CheckBoxes.ToString()));
                            inputTypeDropDown.Items.Add(new ListItem(_GlobalResources.SelectBoxSingleSelect, UserAccountData.InputType.SelectBoxSingleSelect.ToString()));
                            inputTypeDropDown.Items.Add(new ListItem(_GlobalResources.SelectboxMultipleSelect, UserAccountData.InputType.SelectBoxMultipleSelect.ToString()));
                            inputTypeDropDown.Items.Add(new ListItem(_GlobalResources.DropDownList, UserAccountData.InputType.DropDownList.ToString()));

                            inputTypeDropDown.SelectedValue = userField.InputType.ToString();

                            //When the user field is isTypeable and the field input type is date, then put that into the string to be compared and checked for saving purpose
                            if (userField.InputType == UserAccountData.InputType.Date)
                            {
                                this._CurrentFieldsWithDateType.Value += userField.Identifier + "|";
                            }

                            inputTypeDropDown.Attributes.Add("onchange", "InputTypeDropDownChange(this.id);");

                            HiddenField inputTypeField = new HiddenField();
                            inputTypeField.ID = "uf_" + userField.Identifier + "_InputTypeField";
                            inputTypeField.Value = userField.InputType.ToString();

                            inputTypeTitlePanel.Controls.Add(inputTypeTitle);                            
                            inputTypeTextBoxPanel.Controls.Add(inputTypeDropDown);
                            inputTypeInstructionsPanel.Controls.Add(inputTypeInstructions);
                            inputTypeTextBoxPanel.Controls.Add(inputTypeField);

                            inputTypeContainerPanel.Controls.Add(inputTypeTitlePanel);
                            inputTypeContainerPanel.Controls.Add(inputTypeTextBoxPanel);
                            inputTypeContainerPanel.Controls.Add(inputTypeInstructionsPanel);

                            rightSideContainerPanel.Controls.Add(inputTypeContainerPanel);

                            // choice values
                            Panel choiceValuesContainerPanel = new Panel();
                            Panel choiceValuesTitlePanel = new Panel();
                            Panel choiceValuesInstructionsPanel = new Panel();
                            Panel choiceValuesTextBoxPanel = new Panel();
                            Panel choiceValuesAlphabetizeCheckBoxPanel = new Panel();
                            Literal choiceValuesTitle = new Literal();
                            Literal choiceValuesInstructions = new Literal();
                            TextBox choiceValuesTextBox = new TextBox();
                            CheckBox choiceValuesAlphabetizeCheckBox = new CheckBox();

                            choiceValuesContainerPanel.ID = "uf_" + userField.Identifier + "_ChoiceValuesContainer";
                            choiceValuesContainerPanel.CssClass = "UserFieldChoiceValuesContainer";

                            choiceValuesTitlePanel.ID = "uf_" + userField.Identifier + "_ChoiceValuesTitleContainer";
                            choiceValuesTitlePanel.CssClass = "UserFieldChoiceValuesTitleContainer";

                            choiceValuesInstructionsPanel.ID = "uf_" + userField.Identifier + "_ChoiceValuesInstructionsContainer";
                            choiceValuesInstructionsPanel.CssClass = "UserFieldChoiceValuesInstructionsContainer";

                            choiceValuesTitle.Text = _GlobalResources.ChoiceValues + ": ";
                            choiceValuesInstructions.Text = String.Empty;

                            choiceValuesTextBox.ID = "uf_" + userField.Identifier + "_ChoiceValuesTextBox";
                            choiceValuesTextBox.TextMode = TextBoxMode.MultiLine;
                            choiceValuesTextBox.Rows = 6;
                            choiceValuesTextBox.CssClass = "UserFieldChoiceValuesTextBox";
                            choiceValuesTextBox.Text = this._UserAccountDataObject.GetUserFieldChoiceValuesInLanguage(userField, this._SelectedLanguage);

                            HiddenField choiceValuesField = new HiddenField();
                            choiceValuesField.ID = "uf_" + userField.Identifier + "_ChoiceValuesField";
                            choiceValuesField.Value = this._UserAccountDataObject.GetUserFieldChoiceValuesInLanguage(userField, this._SelectedLanguage);

                            choiceValuesAlphabetizeCheckBox.ID = "uf_" + userField.Identifier + "_ChoiceValuesAlphabetizeCheckBox";
                            choiceValuesAlphabetizeCheckBox.Text = _GlobalResources.SortChoicesAlphabeticallyWhenDisplaying;
                            choiceValuesAlphabetizeCheckBox.Checked = this._UserAccountDataObject.GetUserFieldChoiceValuesAlphabetizeSettingInLanguage(userField, this._SelectedLanguage);

                            HiddenField choiceValuesAlphabetizeSettingField = new HiddenField();
                            choiceValuesAlphabetizeSettingField.ID = "uf_" + userField.Identifier + "_ChoiceValuesAlphabetizeSettingField";
                            choiceValuesAlphabetizeSettingField.Value = this._UserAccountDataObject.GetUserFieldChoiceValuesAlphabetizeSettingInLanguage(userField, this._SelectedLanguage).ToString().ToLower();

                            choiceValuesTitlePanel.Controls.Add(choiceValuesTitle);
                            choiceValuesInstructionsPanel.Controls.Add(choiceValuesInstructions);
                            choiceValuesTextBoxPanel.Controls.Add(choiceValuesTextBox);
                            choiceValuesTextBoxPanel.Controls.Add(choiceValuesField);
                            choiceValuesAlphabetizeCheckBoxPanel.Controls.Add(choiceValuesAlphabetizeCheckBox);
                            choiceValuesAlphabetizeCheckBoxPanel.Controls.Add(choiceValuesAlphabetizeSettingField);

                            choiceValuesContainerPanel.Controls.Add(choiceValuesTitlePanel);
                            choiceValuesContainerPanel.Controls.Add(choiceValuesInstructionsPanel);
                            choiceValuesContainerPanel.Controls.Add(choiceValuesTextBoxPanel);
                            choiceValuesContainerPanel.Controls.Add(choiceValuesAlphabetizeCheckBoxPanel);

                            rightSideContainerPanel.Controls.Add(choiceValuesContainerPanel);
                        }

                        // regular expression
                        Panel regularExpressionContainerPanel = new Panel();
                        Panel regularExpressionTitlePanel = new Panel();
                        Panel regularExpressionInstructionsPanel = new Panel();
                        Panel regularExpressionTextBoxPanel = new Panel();
                        Literal regularExpressionTitle = new Literal();
                        Literal regularExpressionInstructions = new Literal();
                        TextBox regularExpressionTextBox = new TextBox();

                        regularExpressionContainerPanel.ID = "uf_" + userField.Identifier + "_RegularExpressionContainer";
                        regularExpressionContainerPanel.CssClass = "UserFieldRegularExpressionContainer";

                        regularExpressionTitlePanel.ID = "uf_" + userField.Identifier + "_RegularExpressionTitleContainer";
                        regularExpressionTitlePanel.CssClass = "UserFieldRegularExpressionTitleContainer";

                        regularExpressionInstructionsPanel.ID = "uf_" + userField.Identifier + "_RegularExpressionInstructionsContainer";
                        regularExpressionInstructionsPanel.CssClass = "UserFieldRegularExpressionInstructionsContainer";

                        regularExpressionTitle.Text = _GlobalResources.RegularExpression + ": ";
                        regularExpressionInstructions.Text = _GlobalResources.WhenInformationIsEnteredInThisFieldItMustMatchTheRegularExpression;

                        regularExpressionTextBox.ID = "uf_" + userField.Identifier + "_RegularExpressionTextBox";
                        regularExpressionTextBox.TextMode = TextBoxMode.SingleLine;
                        regularExpressionTextBox.CssClass = "UserFieldRegularExpressionTextBox";
                        regularExpressionTextBox.Text = userField.RegularExpression;
                        
                        HiddenField regularExpressionField = new HiddenField();
                        regularExpressionField.ID = "uf_" + userField.Identifier + "_RegularExpressionField";
                        regularExpressionField.Value = userField.RegularExpression;

                        regularExpressionTitlePanel.Controls.Add(regularExpressionTitle);
                        regularExpressionInstructionsPanel.Controls.Add(regularExpressionInstructions);
                        regularExpressionTextBoxPanel.Controls.Add(regularExpressionTextBox);
                        regularExpressionTextBoxPanel.Controls.Add(regularExpressionField);

                        regularExpressionContainerPanel.Controls.Add(regularExpressionTitlePanel);
                        regularExpressionContainerPanel.Controls.Add(regularExpressionInstructionsPanel);
                        regularExpressionContainerPanel.Controls.Add(regularExpressionTextBoxPanel);

                        rightSideContainerPanel.Controls.Add(regularExpressionContainerPanel);
                    }

                    // attach left side container
                    userFieldOptionsContainerCell.Controls.Add(rightSideContainerPanel);

                    // attach user field options cell to user field options row
                    userFieldOptionsRow.Cells.Add(userFieldOptionsContainerCell);

                    // attach user field options row to table
                    this._UserAccountDataTable.Rows.Add(userFieldOptionsRow);

                    // increment i for the alternating rows logic
                    i++;
                }
            }

            // attach able to container
            this.UserAccountDataTableContainer.Controls.Add(this._UserAccountDataTable);

            // allow users to upload files field
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERPROFILEFILEATTACHMENTS_ENABLE)) 
            {
                  CheckBox allowUserToUploadFilesCheckBox = new CheckBox();
                  allowUserToUploadFilesCheckBox.ID = "uad_AllowUsersToUploadFiles_Field";
                  allowUserToUploadFilesCheckBox.Text = _GlobalResources.AllowUsersToUploadFilesToTheirProfile;
                  allowUserToUploadFilesCheckBox.Checked = this._UserAccountDataObject.AllowUserToUploadFiles;
            
                  this.UserAccountDataTableContainer.Controls.Add(AsentiaPage.BuildFormField("AllowUsersToUploadFiles",
                                                                                       _GlobalResources.UserFileUpload,
                                                                                       allowUserToUploadFilesCheckBox.ID,
                                                                                       allowUserToUploadFilesCheckBox,
                                                                                       false,
                                                                                       false,
                                                                                       false));
            }
            this.UserFieldConfigurationPropertiesTabPanelsContainer.Controls.Add(this.UserAccountDataTableContainer);
        }
        #endregion

        #region _BuildViewModifyCheckBoxes
        /// <summary>
        /// Builds a panel with checkboxes and labels for the "View" and "Modify" settings
        /// of a "view type" for a user account data user field.
        /// </summary>
        /// <param name="viewType">the "view type" we're building this panel for</param>
        /// <param name="userField">user field object</param>
        /// <param name="userFieldGlobalRuleSet">global rules for the user field</param>
        /// <returns>panel with checkboxes and labels</returns>
        private Panel _BuildViewModifyCheckBoxes(string viewType, UserAccountData.UserField userField, UserFieldGlobalRules userFieldGlobalRuleSet)
        {
            Panel containerPanel = new Panel();
            containerPanel.ID = "uf_" + userField.Identifier + "_" + viewType + "_ViewModifyContainer";

            Panel viewCheckBoxContainer = new Panel();
            viewCheckBoxContainer.ID = "uf_" + userField.Identifier + "_" + viewType + "_ViewContainer";

            Panel modifyCheckBoxContainer = new Panel();
            modifyCheckBoxContainer.ID = "uf_" + userField.Identifier + "_" + viewType + "_ModifyContainer";

            bool isViewChecked = false;
            bool isViewDisabled = false;
            bool isModifyChecked = false;
            bool isModifyDisabled = false;
            string viewOnChangeFunctions = String.Empty;
            string modifyOnChangeFunctions = String.Empty;

            if (viewType == "AdministratorEditPage")
            {
                // EVALUATE AND SET CHECKBOX STATES BASED ON USER ACCOUNT DATA
                // SETTINGS AND THE GLOBAL RULES FOR THE USER ACCOUNT DATA

                // check user field settings
                if (userField.AdministratorEditPageView)
                {
                    isViewChecked = true;
                }
                else
                {
                    isViewChecked = false;
                    isModifyDisabled = true;
                }

                if (userField.AdministratorEditPageModify)
                { isModifyChecked = true; }

                // check global rules
                if (userFieldGlobalRuleSet.AlwaysVisible)
                {
                    isViewChecked = true;
                    isViewDisabled = true;
                }

                if (!userFieldGlobalRuleSet.AdminViewAllowed)
                {
                    isViewChecked = false;
                    isViewDisabled = true;
                    isModifyChecked = false;
                    isModifyDisabled = true;
                }

                if (!userFieldGlobalRuleSet.AdminModifyAllowed)
                {
                    isModifyChecked = false;
                    isModifyDisabled = true;
                }

                // SET THE JAVASCRIPT FUNCTION CALLS FOR THE ONCHANGE
                // EVENTS OF THE CHECKBOXES

                viewOnChangeFunctions = "ToggleAdministratorEditPageViewCheckBox('" + "uf_" + userField.Identifier + "_', " + userFieldGlobalRuleSet.AlwaysRequired.ToString().ToLower() + ");";
                if (!userFieldGlobalRuleSet.AlwaysRequired)
                { modifyOnChangeFunctions = "CheckAndSetIsRequiredCheckBox('" + "uf_" + userField.Identifier + "_');"; }
            }
            else if (viewType == "UserEditPage")
            {
                // EVALUATE AND SET CHECKBOX STATES BASED ON USER ACCOUNT DATA
                // SETTINGS AND THE GLOBAL RULES FOR THE USER ACCOUNT DATA

                // check user field settings
                if (userField.UserEditPageView)
                {
                    isViewChecked = true;
                }
                else
                {
                    isViewChecked = false;
                    isModifyDisabled = true;
                }

                if (userField.UserEditPageModify)
                { isModifyChecked = true; }

                // check global rules
                if (userFieldGlobalRuleSet.AlwaysVisible)
                {
                    isViewChecked = true;
                    isViewDisabled = true;
                }

                if (!userFieldGlobalRuleSet.UserViewAllowed)
                {
                    isViewChecked = false;
                    isViewDisabled = true;
                    isModifyChecked = false;
                    isModifyDisabled = true;
                }

                if (!userFieldGlobalRuleSet.UserModifyAllowed)
                {
                    isModifyChecked = false;
                    isModifyDisabled = true;
                }

                // SET THE JAVASCRIPT FUNCTION CALLS FOR THE ONCHANGE
                // EVENTS OF THE CHECKBOXES

                viewOnChangeFunctions = "ToggleUserEditPageViewCheckBox('" + "uf_" + userField.Identifier + "_', " + userFieldGlobalRuleSet.AlwaysRequired.ToString().ToLower() + ");";
                if (!userFieldGlobalRuleSet.AlwaysRequired)
                { modifyOnChangeFunctions = "CheckAndSetIsRequiredCheckBox('" + "uf_" + userField.Identifier + "_');"; }
            }
            else if (viewType == "RegistrationPage")
            {
                // EVALUATE AND SET CHECKBOX STATES BASED ON USER ACCOUNT DATA
                // SETTINGS AND THE GLOBAL RULES FOR THE USER ACCOUNT DATA

                // always disable modify because it is synonomous with view
                isModifyDisabled = true;

                // check user field settings
                if (userField.RegistrationPageView)
                {
                    isViewChecked = true;
                    isModifyChecked = true;
                }
                else
                {
                    isViewChecked = false;
                    isModifyChecked = false;
                }

                if (userField.RegistrationPageModify)
                {
                    isViewChecked = true;
                    isModifyChecked = true;
                }

                // check global rules
                if (userFieldGlobalRuleSet.AlwaysVisible)
                {
                    isViewChecked = true;
                    isViewDisabled = true;
                    isModifyChecked = true;
                }

                if (!userFieldGlobalRuleSet.RegistrationPageAllowed)
                {
                    isViewChecked = false;
                    isViewDisabled = true;
                    isModifyChecked = false;
                }

                // SET THE JAVASCRIPT FUNCTION CALLS FOR THE ONCHANGE
                // EVENTS OF THE CHECKBOXES

                viewOnChangeFunctions = "ToggleRegistrationPageViewCheckBox('" + "uf_" + userField.Identifier + "_', " + userFieldGlobalRuleSet.AlwaysRequired.ToString().ToLower() + ");";
            }

            CheckBox viewCheckBox = new CheckBox();
            viewCheckBox.ID = "uf_" + userField.Identifier + "_" + viewType + "_ViewCheckBox";
            viewCheckBox.Text = _GlobalResources.View;
            viewCheckBox.Checked = isViewChecked;
            viewCheckBox.Enabled = !isViewDisabled;
            viewCheckBox.Attributes.Add("onchange", viewOnChangeFunctions);

            viewCheckBoxContainer.Controls.Add(viewCheckBox);

            CheckBox modifyCheckBox = new CheckBox();
            modifyCheckBox.ID = "uf_" + userField.Identifier + "_" + viewType + "_ModifyCheckBox";
            modifyCheckBox.Text = _GlobalResources.Modify;
            modifyCheckBox.Checked = isModifyChecked;
            modifyCheckBox.Enabled = !isModifyDisabled;
            modifyCheckBox.Attributes.Add("onchange", modifyOnChangeFunctions);

            modifyCheckBoxContainer.Controls.Add(modifyCheckBox);

            containerPanel.Controls.Add(viewCheckBoxContainer);
            containerPanel.Controls.Add(modifyCheckBoxContainer);

            return containerPanel;
        }
        #endregion

        #region _BuildIsRequiredCheckBox
        /// <summary>
        /// Builds a panel with a checkbox for the "Is Required" setting for a user account data user field.
        /// </summary>
        /// <param name="userField">user field object</param>
        /// <param name="userFieldGlobalRuleSet">global rules for the user field</param>
        /// <returns>panel that contains a checkbox</returns>
        private Panel _BuildIsRequiredCheckBox(UserAccountData.UserField userField, UserFieldGlobalRules userFieldGlobalRuleSet)
        {
            Panel isRequiredCheckBoxContainer = new Panel();
            isRequiredCheckBoxContainer.ID = "uf_" + userField.Identifier + "_IsRequiredContainer";

            bool isChecked = false;
            bool isDisabled = false;

            isChecked = userField.IsRequired;

            if (userFieldGlobalRuleSet.AlwaysRequired)
            {
                isChecked = true;
                isDisabled = true;
            }

            CheckBox isRequiredCheckBox = new CheckBox();
            isRequiredCheckBox.ID = "uf_" + userField.Identifier + "_IsRequiredCheckBox";
            isRequiredCheckBox.Checked = isChecked;
            isRequiredCheckBox.Enabled = !isDisabled;
            isRequiredCheckBox.Attributes.Add("onchange", "");

            isRequiredCheckBoxContainer.Controls.Add(isRequiredCheckBox);

            return isRequiredCheckBoxContainer;
        }
        #endregion

        #region _BuildFormPreviewsContainer
        /// <summary>
        /// Builds the container that contains the buttons to launch form previews.
        /// </summary>
        private void _BuildFormPreviewsContainer()
        {
            this.FormPreviewsContainer = new Panel();
            this.FormPreviewsContainer.ID = "UserFieldConfigurationProperties_" + "FormPreviews" + "_TabPanel";
            this.FormPreviewsContainer.Attributes.Add("style", "display: none;");

            this._FormPreviewAdministratorViewUpdatePanel = new UpdatePanel();
            this._FormPreviewUserViewUpdatePanel = new UpdatePanel();
            this._FormPreviewRegistrationFormUpdatePanel = new UpdatePanel();

            // HIDDEN FIELDS TO ACT A PLACEHOLDER FOR MODAL'S TARGET CONTROL

            this._AdministratorViewPreviewModalTargetPlaceholder = new HiddenField();
            this._AdministratorViewPreviewModalTargetPlaceholder.ID = "AdministratorViewPreviewModalTarget";
            this._FormPreviewAdministratorViewUpdatePanel.ContentTemplateContainer.Controls.Add(this._AdministratorViewPreviewModalTargetPlaceholder);

            this._UserViewPreviewModalTargetPlaceholder = new HiddenField();
            this._UserViewPreviewModalTargetPlaceholder.ID = "UserViewPreviewModalTarget";
            this._FormPreviewUserViewUpdatePanel.ContentTemplateContainer.Controls.Add(this._UserViewPreviewModalTargetPlaceholder);

            this._RegistrationFormPreviewModalTargetPlaceholder = new HiddenField();
            this._RegistrationFormPreviewModalTargetPlaceholder.ID = "RegistrationFormPreviewModalTarget";
            this._FormPreviewRegistrationFormUpdatePanel.ContentTemplateContainer.Controls.Add(this._RegistrationFormPreviewModalTargetPlaceholder);

            // ADMINISTRATOR VIEW PREVIEW LINK BUTTON

            Panel adminViewPreviewButtonPanel = new Panel();
            adminViewPreviewButtonPanel.ID = "AdminViewPreviewButtonContainer";

            this._AdminViewPreviewButton = new LinkButton();
            this._AdminViewPreviewButton.ID = "AdminViewPreviewButton";
            this._AdminViewPreviewButton.Command += new CommandEventHandler(this._AdminViewPreviewButton_Command);

            Image adminViewPreviewImage = new Image();
            adminViewPreviewImage.ID = "AdminViewPreviewButtonImage";
            adminViewPreviewImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADMINISTRATOR,
                                                                    ImageFiles.EXT_PNG);
            adminViewPreviewImage.CssClass = "MediumIcon";
            this._AdminViewPreviewButton.Controls.Add(adminViewPreviewImage);

            Literal adminViewPreviewText = new Literal();
            adminViewPreviewText.Text = _GlobalResources.AdministratorView;
            this._AdminViewPreviewButton.Controls.Add(adminViewPreviewText);

            // attach button to panel and panel to update panel
            adminViewPreviewButtonPanel.Controls.Add(this._AdminViewPreviewButton);
            this._FormPreviewAdministratorViewUpdatePanel.ContentTemplateContainer.Controls.Add(adminViewPreviewButtonPanel);

            // USER VIEW PREVIEW LINK BUTTON

            Panel userViewPreviewButtonPanel = new Panel();
            userViewPreviewButtonPanel.ID = "UserViewPreviewButtonContainer";

            this._UserViewPreviewButton = new LinkButton();
            this._UserViewPreviewButton.ID = "UserViewPreviewButton";
            this._UserViewPreviewButton.Command += new CommandEventHandler(this._UserViewPreviewButton_Command);

            Image userViewPreviewImage = new Image();
            userViewPreviewImage.ID = "UserViewPreviewButtonImage";
            userViewPreviewImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                   ImageFiles.EXT_PNG);
            userViewPreviewImage.CssClass = "MediumIcon";
            this._UserViewPreviewButton.Controls.Add(userViewPreviewImage);

            Literal userViewPreviewText = new Literal();
            userViewPreviewText.Text = _GlobalResources.UserView;
            this._UserViewPreviewButton.Controls.Add(userViewPreviewText);

            // attach button to panel and panel to update panel
            userViewPreviewButtonPanel.Controls.Add(this._UserViewPreviewButton);
            this._FormPreviewUserViewUpdatePanel.ContentTemplateContainer.Controls.Add(userViewPreviewButtonPanel);

            // REGISTRATION FORM PREVIEW LINK BUTTON

            Panel registrationFormPreviewButtonPanel = new Panel();
            registrationFormPreviewButtonPanel.ID = "RegistrationFormPreviewButtonContainer";

            this._RegistrationFormPreviewButton = new LinkButton();
            this._RegistrationFormPreviewButton.ID = "RegistrationFormPreviewButton";
            this._RegistrationFormPreviewButton.Command += new CommandEventHandler(this._RegistrationFormPreviewButton_Command);

            Image registrationFormPreviewImage = new Image();
            registrationFormPreviewImage.ID = "RegistrationFormPreviewButtonImage";
            registrationFormPreviewImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                           ImageFiles.EXT_PNG);
            registrationFormPreviewImage.CssClass = "MediumIcon";
            this._RegistrationFormPreviewButton.Controls.Add(registrationFormPreviewImage);

            Literal registrationFormPreviewText = new Literal();
            registrationFormPreviewText.Text = _GlobalResources.RegistrationForm;
            this._RegistrationFormPreviewButton.Controls.Add(registrationFormPreviewText);

            // attach button to panel and panel to update panel
            registrationFormPreviewButtonPanel.Controls.Add(this._RegistrationFormPreviewButton);
            this._FormPreviewRegistrationFormUpdatePanel.ContentTemplateContainer.Controls.Add(registrationFormPreviewButtonPanel);

            // CONFIGURE THE UPDATE PANELS

            // set the ids
            this._FormPreviewAdministratorViewUpdatePanel.ID = "FormPreviewAdministratorViewUpdatePanel";
            this._FormPreviewAdministratorViewUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Always;

            this._FormPreviewUserViewUpdatePanel.ID = "FormPreviewUserViewUpdatePanel";
            this._FormPreviewUserViewUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Always;

            this._FormPreviewRegistrationFormUpdatePanel.ID = "FormPreviewRegistrationFormUpdatePanel";
            this._FormPreviewRegistrationFormUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Always;

            // make the admin view preview button a trigger for the preview update panel
            AsyncPostBackTrigger trigger1 = new AsyncPostBackTrigger();
            trigger1.ControlID = this._AdminViewPreviewButton.ClientID;
            trigger1.EventName = "Click";
            this._FormPreviewAdministratorViewUpdatePanel.Triggers.Add(trigger1);

            // make the user view preview button a trigger for the preview update panel
            AsyncPostBackTrigger trigger2 = new AsyncPostBackTrigger();
            trigger2.ControlID = this._UserViewPreviewButton.ClientID;
            trigger2.EventName = "Click";
            this._FormPreviewUserViewUpdatePanel.Triggers.Add(trigger2);

            // make the registration form preview button a trigger for the preview update panel
            AsyncPostBackTrigger trigger3 = new AsyncPostBackTrigger();
            trigger3.ControlID = this._RegistrationFormPreviewButton.ClientID;
            trigger3.EventName = "Click";
            this._FormPreviewRegistrationFormUpdatePanel.Triggers.Add(trigger3);

            // build the preview modals
            this._BuildFormPreviewModalForAdministratorView();
            this._BuildFormPreviewModalForUserView();
            this._BuildFormPreviewModalForRegistrationForm();

            // attach the update panels to the form preview container
            this.FormPreviewsContainer.Controls.Add(this._FormPreviewAdministratorViewUpdatePanel);
            this.FormPreviewsContainer.Controls.Add(this._FormPreviewUserViewUpdatePanel);
            this.FormPreviewsContainer.Controls.Add(this._FormPreviewRegistrationFormUpdatePanel);

            this.UserFieldConfigurationPropertiesTabPanelsContainer.Controls.Add(this.FormPreviewsContainer);
        }
        #endregion

        #region _BuildFormPreviewModalForAdministratorView
        /// <summary>
        /// Builds the modal container for Administrator View form preview.
        /// </summary>
        private void _BuildFormPreviewModalForAdministratorView()
        {
            // give the form preview modal an id
            this._FormPreviewModalAdministratorView = new ModalPopup("FormPreviewModalAdministratorView");

            // set modal properties
            this._FormPreviewModalAdministratorView.CssClass += " FormPreviewModal";
            this._FormPreviewModalAdministratorView.Type = ModalPopupType.Information;
            this._FormPreviewModalAdministratorView.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_ADMINISTRATOR,
                                                                                            ImageFiles.EXT_PNG);
            this._FormPreviewModalAdministratorView.HeaderIconAlt = String.Empty;
            this._FormPreviewModalAdministratorView.HeaderText = _GlobalResources.FormPreview + ": " + _GlobalResources.AdministratorView;
            this._FormPreviewModalAdministratorView.TargetControlID = this._AdministratorViewPreviewModalTargetPlaceholder.ClientID;

            // attach modal to update panel
            this._FormPreviewAdministratorViewUpdatePanel.ContentTemplateContainer.Controls.Add(this._FormPreviewModalAdministratorView);
        }
        #endregion

        #region _BuildFormPreviewModalForUserView
        /// <summary>
        /// Builds the modal container for User View form preview.
        /// </summary>
        private void _BuildFormPreviewModalForUserView()
        {
            // give the form preview modal an id
            this._FormPreviewModalUserView = new ModalPopup("FormPreviewModalUserView");

            // set modal properties
            this._FormPreviewModalUserView.CssClass += " FormPreviewModal";
            this._FormPreviewModalUserView.Type = ModalPopupType.Information;
            this._FormPreviewModalUserView.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                                   ImageFiles.EXT_PNG);
            this._FormPreviewModalUserView.HeaderIconAlt = String.Empty;
            this._FormPreviewModalUserView.HeaderText = _GlobalResources.FormPreview + ": " + _GlobalResources.UserView;
            this._FormPreviewModalUserView.TargetControlID = this._UserViewPreviewModalTargetPlaceholder.ClientID;

            // attach modal to update panel
            this._FormPreviewUserViewUpdatePanel.ContentTemplateContainer.Controls.Add(this._FormPreviewModalUserView);
        }
        #endregion

        #region _BuildFormPreviewModalForRegistrationForm
        /// <summary>
        /// Builds the modal container for Registration Form form preview.
        /// </summary>
        private void _BuildFormPreviewModalForRegistrationForm()
        {
            // give the form preview modal an id
            this._FormPreviewModalRegistrationForm = new ModalPopup("FormPreviewModalRegistrationForm");

            // set modal properties
            this._FormPreviewModalRegistrationForm.CssClass += " FormPreviewModal";
            this._FormPreviewModalRegistrationForm.Type = ModalPopupType.Information;
            this._FormPreviewModalRegistrationForm.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                                           ImageFiles.EXT_PNG);
            this._FormPreviewModalRegistrationForm.HeaderIconAlt = String.Empty;
            this._FormPreviewModalRegistrationForm.HeaderText = _GlobalResources.FormPreview + ": " + _GlobalResources.RegistrationForm;
            this._FormPreviewModalRegistrationForm.TargetControlID = this._RegistrationFormPreviewModalTargetPlaceholder.ClientID;

            // attach modal to update panel
            this._FormPreviewRegistrationFormUpdatePanel.ContentTemplateContainer.Controls.Add(this._FormPreviewModalRegistrationForm);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _BuildAndSaveUserAccountDataObject
        /// <summary>
        /// Builds a User Account Data object from the form inputs and saves it.
        /// </summary>
        /// <param name="fileType">user account data file type</param>
        private void _BuildAndSaveUserAccountDataObject(UserAccountDataFileType fileType)
        {
            // GET VALUES FROM FORM INTO OBJECTS

            HiddenField tabAndUserFieldOrderField = (HiddenField)this._UserAccountDataTable.FindControl("uad_tabAndUserFieldOrder");
            string[] tabAndUserFieldIdentifiers = tabAndUserFieldOrderField.Value.Split('|');


            // registration instructions
            TextBox registrationInstructionsTextBox = (TextBox)this.RegistrationInstructionsContainer.FindControl("uad_RegistrationInstructionsTextBox");

            UserAccountData.RegistrationInstructionsLanguageSpecific registrationInstructions = new UserAccountData.RegistrationInstructionsLanguageSpecific();
            registrationInstructions.LangString = this._SelectedLanguage;
            registrationInstructions.HTML = HttpUtility.HtmlDecode(registrationInstructionsTextBox.Text);
            if (this._SelectedLanguage == AsentiaSessionState.GlobalSiteObject.LanguageString)
            { registrationInstructions.IsDefaultLanguage = true; }
            else
            { registrationInstructions.IsDefaultLanguage = false; }
            
            // tabs
            List<UserAccountData.Tab> tabs = new List<UserAccountData.Tab>();
            string currentTabIdentifier = String.Empty;
            
            foreach (string identifier in tabAndUserFieldIdentifiers)
            {
                if (identifier.IndexOf("tab_") == 0)
                {
                    currentTabIdentifier = identifier;
                    UserAccountData.Tab tabObject = new UserAccountData.Tab();
                    tabObject.Identifier = identifier.Replace("tab_", "");

                    HiddenField tabLabelField = (HiddenField)this._UserAccountDataTable.FindControl(identifier + "_LabelField");

                    if (tabLabelField != null)
                    {
                        UserAccountData.TabLanguageSpecificProperties tabLanguageSpecificProperty = new UserAccountData.TabLanguageSpecificProperties();
                        tabLanguageSpecificProperty.LangString = this._SelectedLanguage;
                        tabLanguageSpecificProperty.Label = tabLabelField.Value;
                        if (this._SelectedLanguage == AsentiaSessionState.GlobalSiteObject.LanguageString)
                        { tabLanguageSpecificProperty.IsDefaultLanguage = true; }
                        else
                        { tabLanguageSpecificProperty.IsDefaultLanguage = false; }

                        tabObject.LanguageSpecificProperties.Add(tabLanguageSpecificProperty);
                    }

                    foreach (string identifier1 in tabAndUserFieldIdentifiers)
                    {
                        if (identifier1.IndexOf("tab_") == 0 && identifier1 != currentTabIdentifier)
                        { currentTabIdentifier = identifier1; }

                        if (identifier1.IndexOf("uf_") == 0 && currentTabIdentifier == identifier)
                        {
                            UserAccountData.UserField userField = new UserAccountData.UserField();
                            userField.Identifier = identifier1.Replace("uf_", "");

                            CheckBox adminViewCheckBox = (CheckBox)this._UserAccountDataTable.FindControl(identifier1 + "_AdministratorEditPage_ViewCheckBox");
                            CheckBox adminModifyCheckBox = (CheckBox)this._UserAccountDataTable.FindControl(identifier1 + "_AdministratorEditPage_ModifyCheckBox");
                            CheckBox userViewCheckBox = (CheckBox)this._UserAccountDataTable.FindControl(identifier1 + "_UserEditPage_ViewCheckBox");
                            CheckBox userModifyCheckBox = (CheckBox)this._UserAccountDataTable.FindControl(identifier1 + "_UserEditPage_ModifyCheckBox");
                            CheckBox regFormViewCheckBox = (CheckBox)this._UserAccountDataTable.FindControl(identifier1 + "_RegistrationPage_ViewCheckBox");
                            CheckBox regFormModifyCheckBox = (CheckBox)this._UserAccountDataTable.FindControl(identifier1 + "_RegistrationPage_ModifyCheckBox");
                            CheckBox isRequiredCheckBox = (CheckBox)this._UserAccountDataTable.FindControl(identifier1 + "_IsRequiredCheckBox");

                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LITEUSERFIELDCONFIGURATION_ENABLE) &&
                                (bool)(userField.Identifier.Remove(userField.Identifier.Length - 1) == "field1"))
                            {
                                userField.AdministratorEditPageView = false;
                                userField.AdministratorEditPageModify = false;
                                userField.UserEditPageView = false;
                                userField.UserEditPageModify = false;
                                userField.RegistrationPageView = false;
                                userField.RegistrationPageModify = false;
                                userField.IsRequired = false;
                            }
                            else if (adminViewCheckBox != null &&
                                    adminModifyCheckBox != null &&
                                    userViewCheckBox != null &&
                                    userModifyCheckBox != null &&
                                    regFormViewCheckBox != null &&
                                    regFormModifyCheckBox != null &&
                                    isRequiredCheckBox != null)
                            {
                                userField.AdministratorEditPageView = adminViewCheckBox.Checked;
                                userField.AdministratorEditPageModify = adminModifyCheckBox.Checked;
                                userField.UserEditPageView = userViewCheckBox.Checked;
                                userField.UserEditPageModify = userModifyCheckBox.Checked;
                                userField.RegistrationPageView = regFormViewCheckBox.Checked;
                                //userField.RegistrationPageModify = regFormModifyCheckBox.Checked;
                                userField.RegistrationPageModify = regFormViewCheckBox.Checked; // registration page modify should be the same as view
                                userField.IsRequired = isRequiredCheckBox.Checked;
                            }

                            HiddenField userFieldLabelField = (HiddenField)this._UserAccountDataTable.FindControl(identifier1 + "_LabelField");
                            HiddenField userFieldDescriptionField = (HiddenField)this._UserAccountDataTable.FindControl(identifier1 + "_DescriptionField");
                            HiddenField userFieldInputTypeField = (HiddenField)this._UserAccountDataTable.FindControl(identifier1 + "_InputTypeField");
                            HiddenField userFieldRegularExpressionField = (HiddenField)this._UserAccountDataTable.FindControl(identifier1 + "_RegularExpressionField");
                            HiddenField userFieldChoiceValuesField = (HiddenField)this._UserAccountDataTable.FindControl(identifier1 + "_ChoiceValuesField");
                            HiddenField userFieldChoiceValuesAlphabetizeSettingsField = (HiddenField)this._UserAccountDataTable.FindControl(identifier1 + "_ChoiceValuesAlphabetizeSettingField");

                            if (userFieldLabelField != null)
                            {
                                UserAccountData.UserFieldLanguageSpecificProperties userFieldLanguageSpecificProperty = new UserAccountData.UserFieldLanguageSpecificProperties();
                                userFieldLanguageSpecificProperty.LangString = this._SelectedLanguage;
                                userFieldLanguageSpecificProperty.Label = userFieldLabelField.Value;

                                if (userFieldDescriptionField != null)
                                { userFieldLanguageSpecificProperty.Description = userFieldDescriptionField.Value; }
                                else
                                { userFieldLanguageSpecificProperty.Description = String.Empty; }

                                if (userFieldChoiceValuesField != null)
                                { userFieldLanguageSpecificProperty.ChoiceValues = userFieldChoiceValuesField.Value; }
                                else
                                { userFieldLanguageSpecificProperty.ChoiceValues = String.Empty; }

                                if (userFieldChoiceValuesAlphabetizeSettingsField != null)
                                { userFieldLanguageSpecificProperty.AlphabetizeChoiceValues = Convert.ToBoolean(userFieldChoiceValuesAlphabetizeSettingsField.Value); }
                                else
                                { userFieldLanguageSpecificProperty.AlphabetizeChoiceValues = false; }

                                userField.LanguageSpecificProperties.Add(userFieldLanguageSpecificProperty);
                            }

                            if (userFieldInputTypeField != null)
                            {
                                if (userFieldInputTypeField.Value == UserAccountData.InputType.TextField.ToString())
                                { userField.InputType = UserAccountData.InputType.TextField; }
                                else if (userFieldInputTypeField.Value == UserAccountData.InputType.Date.ToString())
                                { userField.InputType = UserAccountData.InputType.Date;}
                                else if (userFieldInputTypeField.Value == UserAccountData.InputType.RadioButtons.ToString())
                                { userField.InputType = UserAccountData.InputType.RadioButtons; }
                                else if (userFieldInputTypeField.Value == UserAccountData.InputType.CheckBoxes.ToString())
                                { userField.InputType = UserAccountData.InputType.CheckBoxes; }
                                else if (userFieldInputTypeField.Value == UserAccountData.InputType.SelectBoxSingleSelect.ToString())
                                { userField.InputType = UserAccountData.InputType.SelectBoxSingleSelect; }
                                else if (userFieldInputTypeField.Value == UserAccountData.InputType.SelectBoxMultipleSelect.ToString())
                                { userField.InputType = UserAccountData.InputType.SelectBoxMultipleSelect; }
                                else if (userFieldInputTypeField.Value == UserAccountData.InputType.DropDownList.ToString())
                                { userField.InputType = UserAccountData.InputType.DropDownList; }
                                else
                                { userField.InputType = null; }
                            }
                            else
                            { userField.InputType = null; }

                            if (userFieldRegularExpressionField != null)
                            { userField.RegularExpression = userFieldRegularExpressionField.Value; }
                            else
                            { userField.RegularExpression = String.Empty; }

                            tabObject.UserFields.Add(userField);
                        }
                    }

                    tabs.Add(tabObject);
                }
            }

            // allow user to upload files
            bool userUploadFilesAllowed = false;

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERPROFILEFILEATTACHMENTS_ENABLE))

            {
                CheckBox allowUserToUploadFilesCheckBox = (CheckBox)this.UserAccountDataTableContainer.FindControl("uad_AllowUsersToUploadFiles_Field");
                userUploadFilesAllowed = allowUserToUploadFilesCheckBox.Checked;
            }
            
            // PUT TOGETHER THE USER ACCOUNT DATA OBJECT FOR SAVING

            UserAccountData userAccountDataForSaving = new UserAccountData(fileType, false, true);
            
            // registration instructions
            userAccountDataForSaving.RegistrationInstructions = new List<UserAccountData.RegistrationInstructionsLanguageSpecific>();
            userAccountDataForSaving.RegistrationInstructions.Add(registrationInstructions);

            foreach (UserAccountData.RegistrationInstructionsLanguageSpecific existingRegistrationInstructionsObject in this._UserAccountDataObject.RegistrationInstructions)
            {
                if (existingRegistrationInstructionsObject.LangString != this._SelectedLanguage)
                { userAccountDataForSaving.RegistrationInstructions.Add(existingRegistrationInstructionsObject); }
            }
            
            // tabs
            userAccountDataForSaving.Tabs = tabs;

            foreach (UserAccountData.Tab newTab in userAccountDataForSaving.Tabs)
            {
                foreach (UserAccountData.Tab existingTab in this._UserAccountDataObject.Tabs)
                {
                    if (newTab.Identifier == existingTab.Identifier)
                    {
                        foreach (UserAccountData.TabLanguageSpecificProperties existingTabLanguageSpecificProperty in existingTab.LanguageSpecificProperties)
                        {
                            if (existingTabLanguageSpecificProperty.LangString != this._SelectedLanguage)
                            { newTab.LanguageSpecificProperties.Add(existingTabLanguageSpecificProperty); }
                        }
                    }
                }

                foreach (UserAccountData.UserField newUserField in newTab.UserFields)
                {
                    foreach (UserAccountData.Tab existingTab in this._UserAccountDataObject.Tabs)
                    {
                        foreach (UserAccountData.UserField existingUserField in existingTab.UserFields)
                        {
                            if (newUserField.Identifier == existingUserField.Identifier)
                            {
                                foreach (UserAccountData.UserFieldLanguageSpecificProperties existingUserFieldLanguageSpecificProperty in existingUserField.LanguageSpecificProperties)
                                {
                                    if (existingUserFieldLanguageSpecificProperty.LangString != this._SelectedLanguage)
                                    { newUserField.LanguageSpecificProperties.Add(existingUserFieldLanguageSpecificProperty); }
                                }
                            }
                        }
                    }
                }
            }
            
            // user agreement
            userAccountDataForSaving.UserAgreement = new List<UserAccountData.UserAgreementLanguageSpecific>();

            foreach (UserAccountData.UserAgreementLanguageSpecific existingUserAgreementObject in this._UserAccountDataObject.UserAgreement)
            {
                userAccountDataForSaving.UserAgreement.Add(existingUserAgreementObject);
            }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERPROFILEFILEATTACHMENTS_ENABLE))
            {
                // files configuration
                userAccountDataForSaving.AllowUserToUploadFiles = userUploadFilesAllowed;
            }

            // save the user account data, this will compare the user saving data with the string array to get the newly changed date-type fields            
            string[] fieldWithDateType = this._CurrentFieldsWithDateType.Value.Split('|');
            userAccountDataForSaving.Save(fieldWithDateType);


        }
        #endregion
        #endregion

        #region Command Handler Methods
        #region _AdminViewPreviewButton_Command
        /// <summary>
        /// Handles the "Administrator View" form preview button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _AdminViewPreviewButton_Command(object sender, CommandEventArgs e)
        {
            // clear the controls from the modal body
            this._FormPreviewModalAdministratorView.ClearBodyControls();

            try
            {
                // build and save the data
                this._BuildAndSaveUserAccountDataObject(UserAccountDataFileType.SiteTemp);

                // add controls to modal body
                this._FormPreviewModalAdministratorView.AddControlToBody(new UserForm("AdministratorEditUserForm", UserFormViewType.AdminView, UserAccountDataFileType.SiteTemp, this._SelectedLanguage));
            }
            catch
            {
                // display the failure message
                this._FormPreviewModalAdministratorView.DisplayFeedback(_GlobalResources.UnableToLoadFormPreview, true);
            }

            // show the modal
            this._FormPreviewModalAdministratorView.ShowModal();
        }
        #endregion

        #region _UserViewPreviewButton_Command
        /// <summary>
        /// Handles the "User View" form preview button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _UserViewPreviewButton_Command(object sender, CommandEventArgs e)
        {
            // clear the controls from the modal body
            this._FormPreviewModalUserView.ClearBodyControls();

            try
            {
                // build and save the data
                this._BuildAndSaveUserAccountDataObject(UserAccountDataFileType.SiteTemp);

                // add controls to modal body
                this._FormPreviewModalUserView.AddControlToBody(new UserForm("UserEditUserForm", UserFormViewType.UserView, UserAccountDataFileType.SiteTemp, this._SelectedLanguage));
            }
            catch
            {
                // display the failure message
                this._FormPreviewModalUserView.DisplayFeedback(_GlobalResources.UnableToLoadFormPreview, true);
            }

            // show the modal
            this._FormPreviewModalUserView.ShowModal();
        }
        #endregion

        #region _RegistrationFormPreviewButton_Command
        /// <summary>
        /// Handles the "Registration Form" form preview button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RegistrationFormPreviewButton_Command(object sender, CommandEventArgs e)
        {
            // clear the controls from the modal body
            this._FormPreviewModalRegistrationForm.ClearBodyControls();

            try
            {
                // build and save the data
                this._BuildAndSaveUserAccountDataObject(UserAccountDataFileType.SiteTemp);

                // add controls to modal body
                this._FormPreviewModalRegistrationForm.AddControlToBody(new UserForm("RegistrationFormUserForm", UserFormViewType.RegistrationPage, UserAccountDataFileType.SiteTemp, this._SelectedLanguage));
            }
            catch
            {
                // display the failure message
                this._FormPreviewModalRegistrationForm.DisplayFeedback(_GlobalResources.UnableToLoadFormPreview, true);
            }

            // show the modal
            this._FormPreviewModalRegistrationForm.ShowModal();
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // build and save the data
                this._BuildAndSaveUserAccountDataObject(UserAccountDataFileType.Site);
                
                // CLEAR ALL CONTAINERS AND RELOAD USER ACCOUNT DATA TO SHOW THE NEWLY SAVED DATA

                // clear all the controls that contain user account data
                this.RegistrationInstructionsContainer.Controls.Clear();
                this._UserAccountDataTable.Controls.Clear();
                this.UserAccountDataTableContainer.Controls.Clear();
                this.FormPreviewsContainer.Controls.Clear();
                this.UserFieldConfigurationPropertiesTabPanelsContainer.Controls.Clear();

                // create the user account data object
                this._UserAccountDataObject = new UserAccountData(UserAccountDataFileType.Site, true, false);

                // build the registration instructions container
                this._BuildRegistrationInstructionsContainer();

                // build the user account data table
                this._BuildUserAccountDataTable();

                // build the form previews container
                this._BuildFormPreviewsContainer();

                // STATUS MESSAGE

                // display the success message
                this.DisplayFeedback(_GlobalResources.UserFieldConfigurationHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            catch (Exception exc)
            {
                this.DisplayFeedback(exc.InnerException.StackTrace, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/dashboard");
        }
        #endregion
        #endregion
    }
}
