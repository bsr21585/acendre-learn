﻿function InitializeUserFieldConfigurationPage() {
    // get the tab and user field order
    GetTabAndUserFieldOrder();

    // initialize checkbox states
    InitializeCheckboxStates();

    // initialize input type states
    InitializeInputTypeStates();

    // attach "sortable" to the user account data table
    // this is so that user fields can be re-ordered
    $(".GridTable").sortable({
        items: '.UserField', // attach only to "user fields"
        axis: 'y',
        cursor: 'move',
        update: function (e, ui) {
            MoveOptionsRowsAndChangeCss();
            GetTabAndUserFieldOrder();
        },
        start: function (e, ui) {
            CancelAllOptionsRowEdits();
            CancelAllLabelEdits();
        },
        stop: function (e, ui) {
            ShowElementsAfterDragging(ui);
            GetTabAndUserFieldOrder();
        },
        helper: function (event, ui) {
            HideElementsWhileDragging(ui);
            return ui.clone().appendTo("body"); // this needs to be done so that the page will scroll when a field is dragged downward
        }
    });
}

// Function : GetTabAndUserFieldOrder
// Loops through all tab and user field rows in order and stores the ids in a hidden field so
// that we have the user field to tab memberships and order for server-side processing.
function GetTabAndUserFieldOrder() {
    var tabAndUserFieldOrder = "";
    var separator = '|';

    $(".Tab, .UserField").each(function (index, tr) {
        var identifier = tr.id.replace("_Row", ""); 

        if (index == 0) {
            tabAndUserFieldOrder = identifier;
        } else {
            tabAndUserFieldOrder += separator + identifier;
        }
    });

    var tabAndUserFieldOrderField = $("#" + "uad_tabAndUserFieldOrder");
    tabAndUserFieldOrderField.val(tabAndUserFieldOrder);
}

// Function : InitializeCheckboxStates
// This function examines the View and Modify checkbox states across the "view types" for each
// user field and enables/disables or checks/unchecks "RegistrationPage View" checkboxes and 
// IsRequired checkboxes based on "common sense" logic, i.e. if "Modify" is not checked on any
// of the "view types" for a field, then IsRequired shouldn't be checked or be able to be checked.
//
// Note that most of the enable/disable, check/uncheck logic for each "view type" for each user
// field is done server-side based on the user account data configuration for the site and the
// global rules for user account data. This is simply a final bit of logic based on "common sense."
function InitializeCheckboxStates() {
    $(".UserField").each(function (index, tr) {
        // get the identifier prefix
        var identifierPrefix = tr.id.replace("_Row", "_");

        // get the checkboxes for the field
        var adminEditViewCheckBox = $("#" + identifierPrefix + "AdministratorEditPage_ViewCheckBox");
        var adminEditModifyCheckBox = $("#" + identifierPrefix + "AdministratorEditPage_ModifyCheckBox");
        var userEditViewCheckBox = $("#" + identifierPrefix + "UserEditPage_ViewCheckBox");
        var userEditModifyCheckBox = $("#" + identifierPrefix + "UserEditPage_ModifyCheckBox");
        var registrationFormViewCheckBox = $("#" + identifierPrefix + "RegistrationPage_ViewCheckBox");
        var registrationFormModifyCheckBox = $("#" + identifierPrefix + "RegistrationPage_ModifyCheckBox");
        var isRequiredCheckBox = $("#" + identifierPrefix + "IsRequiredCheckBox");

        // if the "admin view" view checkbox is not checked, uncheck and disable the registration form checkbox
        if (!adminEditViewCheckBox.is(':checked')) {
            registrationFormViewCheckBox.prop("checked", false);
            registrationFormViewCheckBox.prop("disabled", true);
        }

        // if none of the view's modify checkboxes are checked, uncheck and disable the is required checkbox
        if (!adminEditModifyCheckBox.is(':checked') && !userEditModifyCheckBox.is(':checked') && !registrationFormModifyCheckBox.is(':checked')) {
            isRequiredCheckBox.prop("checked", false);
            isRequiredCheckBox.prop("disabled", true);
        }
    });
}

// Function : InitializeInputTypeStates
// This function examines the Input Type drop down for each user field and shows/hides options
// related or unrelated to the selected value.
function InitializeInputTypeStates() {
    $(".UserField").each(function (index, tr) {
        // get the identifier prefix
        var identifierPrefix = tr.id.replace("_Row", "_");

        // get the dropdowns, text fields, and hidden fields related to input type
        var inputTypeDropDown = $("#" + identifierPrefix + "InputTypeDropDown");
        var choiceValuesContainer = $("#" + identifierPrefix + "ChoiceValuesContainer");
        var choiceValuesTextBox = $("#" + identifierPrefix + "ChoiceValuesTextBox");
        var choiceValuesField = $("#" + identifierPrefix + "ChoiceValuesField");
        var regularExpressionContainer = $("#" + identifierPrefix + "RegularExpressionContainer");
        var regularExpressionTextBox = $("#" + identifierPrefix + "RegularExpressionTextBox");
        var regularExpressionField = $("#" + identifierPrefix + "RegularExpressionField");

        // set values and hidden/visible states for choice values and 
        // regular expression based on the selected input type
        if (inputTypeDropDown.length > 0) {
            if (inputTypeDropDown.val() == "TextField") {
                choiceValuesTextBox.val("");
                choiceValuesField.val("");
                choiceValuesContainer.hide();
                regularExpressionContainer.show();
            }
            else if (inputTypeDropDown.val() == "Date") {
                choiceValuesTextBox.val("");
                choiceValuesField.val("");
                choiceValuesContainer.hide();
                regularExpressionTextBox.val("");
                regularExpressionField.val("");
                regularExpressionContainer.hide();
            }
            else {
                regularExpressionTextBox.val("");
                regularExpressionField.val("");
                regularExpressionContainer.hide();
                choiceValuesContainer.show();
            }
        }
    });
}

// Function : MoveOptionsRowsAndChangeCss
// This function re-aligns the options properties row for a user field row when the user field 
// row is moved so that the options properties row for a user field is together with its user 
// field row. This also re-calculates and applies alternating row CSS for the user field rows.
function MoveOptionsRowsAndChangeCss() {
    $(".UserField").each(function (index, tr) {
        // get the identifier prefix
        var identifierPrefix = tr.id.replace("_Row", "_");

        // get the row and options row
        var row = $("#" + identifierPrefix + "Row");
        var optionsRow = $("#" + identifierPrefix + "OptionsRow");

        // ensure that user field options rows follow their user field rows
        optionsRow.detach();
        row.after(optionsRow);

        // maintain alternating row css
        if (index % 2 == 0) {
            $(row).removeClass("GridDataRowAlternate");
            $(optionsRow).removeClass("GridDataRowAlternate");
        } else {
            $(row).addClass("GridDataRowAlternate");
            $(optionsRow).addClass("GridDataRowAlternate");
        }
    });
}

// Function : HideElementsWhileDragging
// Hides all <td>s, with the exception of the identifier <td> for a user field row
// when the user field row is being dragged.
function HideElementsWhileDragging(ui) {
    // loop through children of the row (<td>s) and hide if not the identifier <td>
    ui.children().each(function (index) {
        if ($(this).prop("id").indexOf("_Identifier") == -1) {
            $(this).hide();
        }
    });
}

// Function : ShowElementsAfterDragging
// Shows the hidden <td>s of a user field row after the row has stopped being dragged.
function ShowElementsAfterDragging(ui) {
    // get the identifier prefix
    var identifierPrefix = ui.item.prop("id").replace("_Row", "_");

    // get the <td>s
    var labelTD = $("#" + identifierPrefix + "Label");
    var adminViewTD = $("#" + identifierPrefix + "AdminView");
    var userViewTD = $("#" + identifierPrefix + "UserView");
    var regFormViewTD = $("#" + identifierPrefix + "RegFormView");
    var requiredTD = $("#" + identifierPrefix + "Required");
    var optionsTD = $("#" + identifierPrefix + "Options");

    // show the <td>s
    labelTD.show();
    adminViewTD.show();
    userViewTD.show();
    regFormViewTD.show();
    requiredTD.show();
    optionsTD.show();
}

// Function : CancelAllOptionsRowEdits
// This function hides the options properties rows for all user fields and resets the
// values of the inputs back to their hidden field values, basically the same operation
// as cancelling the editing of an options row.
function CancelAllOptionsRowEdits() {
    $(".UserField").each(function (index, tr) {
        // get the identifier prefix
        var identifierPrefix = tr.id.replace("_Row", "_");

        // cancel options row edit
        CancelOptionsRowEdit(identifierPrefix);
    });
}

// Function : CancelAllLabelEdits
// This function hides the label edit text boxes for all user fields and tabs, and
// resets the value of the inputs back to their hidden field values, basically the
// same operation as cancelling the editing of a label.
function CancelAllLabelEdits() {
    // loop through user fields and cancel label edit for each
    $(".UserField").each(function (index, tr) {
        // get the identifier prefix
        var identifierPrefix = tr.id.replace("_Row", "_");

        // cancel label edit
        CancelLabelEdit(identifierPrefix);
    });

    // loop through tabs and cancel label edit for each
    $(".Tab").each(function (index, tr) {
        // get the identifier prefix
        var identifierPrefix = tr.id.replace("_Row", "_");

        // cancel label edit
        CancelLabelEdit(identifierPrefix);
    });
}

// Function : ToggleAdministratorEditPageViewCheckBox
// Changes checkbox states based on the state of the AdministratorEditPageViewCheckBox
// for a user field when it is changed.
function ToggleAdministratorEditPageViewCheckBox(identifierPrefix, isFieldAlwaysRequired) {
    // get the checkboxes linked to "admin view"
    var adminEditViewCheckBox = $("#" + identifierPrefix + "AdministratorEditPage_ViewCheckBox");
    var adminEditModifyCheckBox = $("#" + identifierPrefix + "AdministratorEditPage_ModifyCheckBox");
    var registrationFormViewCheckBox = $("#" + identifierPrefix + "RegistrationPage_ViewCheckBox");
    var registrationFormModifyCheckBox = $("#" + identifierPrefix + "RegistrationPage_ModifyCheckBox");

    // if the "admin view" view checkbox is not checked, uncheck and disable
    // "admin view" modify and registration form checkboxes
    if (!adminEditViewCheckBox.is(':checked')) {
        adminEditModifyCheckBox.prop("checked", false);
        adminEditModifyCheckBox.prop("disabled", true);

        registrationFormViewCheckBox.prop("checked", false);
        registrationFormViewCheckBox.prop("disabled", true);
        registrationFormModifyCheckBox.prop("checked", false);
    }
        // else, enable them
    else {
        adminEditModifyCheckBox.prop("disabled", false);
        registrationFormViewCheckBox.prop("disabled", false);
    }

    // check and set is required
    if (!isFieldAlwaysRequired) {
        CheckAndSetIsRequiredCheckBox(identifierPrefix);
    }
}

// Function : ToggleUserEditPageViewCheckBox
// Changes checkbox states based on the state of the UserEditPageViewCheckBox
// for a user field when it is changed.
function ToggleUserEditPageViewCheckBox(identifierPrefix, isFieldAlwaysRequired) {
    // get the checkboxes linked to "user view"
    var userEditViewCheckBox = $("#" + identifierPrefix + "UserEditPage_ViewCheckBox");
    var userEditModifyCheckBox = $("#" + identifierPrefix + "UserEditPage_ModifyCheckBox");

    // if the "user view" view checkbox is not checked, uncheck and disable
    // "user view" modify checkbox
    if (!userEditViewCheckBox.is(':checked')) {
        userEditModifyCheckBox.prop("checked", false);
        userEditModifyCheckBox.prop("disabled", true);
    }
        // else, enable it
    else {
        userEditModifyCheckBox.prop("disabled", false);
    }

    // check and set is required
    if (!isFieldAlwaysRequired) {
        CheckAndSetIsRequiredCheckBox(identifierPrefix);
    }
}

// Function : ToggleRegistrationPageViewCheckBox
// Changes checkbox states based on the state of the RegistrationPageViewCheckBox
// for a user field when it is changed.
function ToggleRegistrationPageViewCheckBox(identifierPrefix, isFieldAlwaysRequired) {
    // get the checkboxes linked to "registration form view"
    var registrationFormViewCheckBox = $("#" + identifierPrefix + "RegistrationPage_ViewCheckBox");
    var registrationFormModifyCheckBox = $("#" + identifierPrefix + "RegistrationPage_ModifyCheckBox");

    // if the "registration form view" view checkbox is not checked, uncheck
    // "registration form view" modify checkbox
    if (!registrationFormViewCheckBox.is(':checked')) {
        registrationFormModifyCheckBox.prop("checked", false);
    }
        // else, check it
    else {
        registrationFormModifyCheckBox.prop("checked", true);
    }

    // check and set is required
    if (!isFieldAlwaysRequired) {
        CheckAndSetIsRequiredCheckBox(identifierPrefix);
    }
}

// Function : CheckAndSetIsRequiredCheckBox
// Examines the Modify check box states for all "view types" for a user field and modifies
// the IsRequired checkbox accordingly.
function CheckAndSetIsRequiredCheckBox(identifierPrefix) {
    // get the modify checkboxes for each view and the is required checkbox
    var adminEditModifyCheckBox = $("#" + identifierPrefix + "AdministratorEditPage_ModifyCheckBox");
    var userEditModifyCheckBox = $("#" + identifierPrefix + "UserEditPage_ModifyCheckBox");
    var registrationFormModifyCheckBox = $("#" + identifierPrefix + "RegistrationPage_ModifyCheckBox");
    var isRequiredCheckBox = $("#" + identifierPrefix + "IsRequiredCheckBox");

    // if none of the view's modify checkboxes are checked, uncheck and disable the is required checkbox
    if (!adminEditModifyCheckBox.is(':checked') && !userEditModifyCheckBox.is(':checked') && !registrationFormModifyCheckBox.is(':checked')) {
        isRequiredCheckBox.prop("checked", false);
        isRequiredCheckBox.prop("disabled", true);
    }
        // else, enable it
    else {
        isRequiredCheckBox.prop("disabled", false);
    }
}

// Function : EditLabelText
// This function makes a labe text box visible for the row being edited, and cancels
// all other edits.
function EditLabelText(optionsIconId) {
    // get the identifier prefix
    var identifierPrefix = optionsIconId.replace("_EditLabelIcon", "_");

    // get the label containers
    var labelTextContainer = $("#" + identifierPrefix + "LabelTextContainer");
    var labelTextBoxContainer = $("#" + identifierPrefix + "LabelTextBoxContainer");
    var labelTextBox = $("#" + identifierPrefix + "LabelTextBox");

    // hide static text and show text box
    labelTextContainer.hide();
    labelTextBoxContainer.show();
    labelTextBox.focus();
    labelTextBox.select();

    // loop through all other user field and tab rows, cancel label edits for any
    // row that is not the one being edited, and ALL option row edits
    $(".UserField").each(function (index, tr) {
        // get identifier prefix
        var _identifierPrefix = tr.id.replace("_Row", "_");

        if (_identifierPrefix != identifierPrefix) {
            CancelLabelEdit(_identifierPrefix);
        }

        // options rows
        CancelAllOptionsRowEdits();
    });

    $(".Tab").each(function (index, tr) {
        // get identifier prefix
        var _identifierPrefix = tr.id.replace("_Row", "_");

        if (_identifierPrefix != identifierPrefix) {
            CancelLabelEdit(_identifierPrefix);
        }
    });
}

// Function : ConfirmLabelEdit
// This function "confirms" the editing of a label (OK clicked) by changing the static text to the
// value typed in the text box, changing the value of the hidden field, and hiding the text box.
function ConfirmLabelEdit(elementId) {
    // get the identifier prefix
    var identifierPrefix = elementId.replace("_LabelOKButton", "_");

    // get the containers, static text, and inputs
    var labelTextContainer = $("#" + identifierPrefix + "LabelTextContainer");
    var labelTextBoxContainer = $("#" + identifierPrefix + "LabelTextBoxContainer");
    var labelText = $("#" + identifierPrefix + "LabelText");
    var labelTextBox = $("#" + identifierPrefix + "LabelTextBox");
    var labelField = $("#" + identifierPrefix + "LabelField");

    labelText.text(labelTextBox.val());
    labelField.val(labelTextBox.val());
    labelTextContainer.show();
    labelTextBoxContainer.hide();
}

// Function : CancelLabelEdit
// This function "cancels" the editing of a label (Cancel clicked) by changing the text box text 
// back to the value of the hidden field, and hiding the text box.
function CancelLabelEdit(elementId) {
    // get the identifier prefix
    var identifierPrefix = elementId.replace("_LabelCancelButton", "_");

    // get the containers, static text, and inputs
    var labelTextContainer = $("#" + identifierPrefix + "LabelTextContainer");
    var labelTextBoxContainer = $("#" + identifierPrefix + "LabelTextBoxContainer");
    var labelText = $("#" + identifierPrefix + "LabelText");
    var labelTextBox = $("#" + identifierPrefix + "LabelTextBox");
    var labelField = $("#" + identifierPrefix + "LabelField");

    labelTextBox.val(labelField.val());
    labelTextContainer.show();
    labelTextBoxContainer.hide();
}

// Function : EditOptionsRow
// This function makes an options row visible for the row being edited, and cancels
// all other edits.
function EditOptionsRow(optionsIconId) {
    // get the identifier prefix
    var identifierPrefix = optionsIconId.replace("_OptionsIcon", "_");

    // get options row elements
    var optionsIcon = $("#" + identifierPrefix + "OptionsIcon");
    var optionsRow = $("#" + identifierPrefix + "OptionsRow");
    var descriptionTextBox = $("#" + identifierPrefix + "DescriptionTextBox");
    var instructionPanel = $("#" + identifierPrefix + "InputTypeInstructionsContainer");

    optionsIcon.hide();
    optionsRow.show();
    instructionPanel.hide();
    descriptionTextBox.focus();
    descriptionTextBox.select();

    // loop through all other user field rows, cancel option row edits for any
    // row that is not the one being edited, and ALL label edits
    $(".UserField").each(function (index, tr) {
        // get identifier prefix
        var _identifierPrefix = tr.id.replace("_Row", "_");

        if (_identifierPrefix != identifierPrefix) {
            CancelOptionsRowEdit(_identifierPrefix);
        }

        // labels
        CancelAllLabelEdits();
    });
}

// Function : ConfirmOptionsRowEdit
// This function "confirms" the editing of an options row (OK clicked) by changing the hidden field
// values to the values of the inputs.
function ConfirmOptionsRowEdit(elementId) {
    // get the identifier prefix
    var identifierPrefix = elementId.replace("_OptionsOKButton", "_");

    // get the options row and input fields in the options row
    var optionsRow = $("#" + identifierPrefix + "OptionsRow");
    var optionsIcon = $("#" + identifierPrefix + "OptionsIcon");
    var descriptionTextBox = $("#" + identifierPrefix + "DescriptionTextBox");
    var descriptionField = $("#" + identifierPrefix + "DescriptionField");
    var inputTypeDropDown = $("#" + identifierPrefix + "InputTypeDropDown");
    var inputTypeField = $("#" + identifierPrefix + "InputTypeField");
    var choiceValuesTextBox = $("#" + identifierPrefix + "ChoiceValuesTextBox");
    var choiceValuesField = $("#" + identifierPrefix + "ChoiceValuesField");
    var choiceValuesAlphabetizeCheckBox = $("#" + identifierPrefix + "ChoiceValuesAlphabetizeCheckBox");
    var choiceValuesAlphabetizeField = $("#" + identifierPrefix + "ChoiceValuesAlphabetizeSettingField");
    var regularExpressionTextBox = $("#" + identifierPrefix + "RegularExpressionTextBox");
    var regularExpressionField = $("#" + identifierPrefix + "RegularExpressionField");

    // hide the row and show the icon
    optionsRow.hide();
    optionsIcon.show();

    // set hidden field values to their input values
    descriptionField.val(descriptionTextBox.val());
    inputTypeField.val(inputTypeDropDown.val());
    choiceValuesField.val(choiceValuesTextBox.val());    
    choiceValuesAlphabetizeField.val(choiceValuesAlphabetizeCheckBox.prop("checked"));
    regularExpressionField.val(regularExpressionTextBox.val());
}

// Function : CancelOptionsRowEdit
// This function "cancels" the editing of an options row (Cancel clicked) by changing the input 
// values back to the values of the hidden fields.
function CancelOptionsRowEdit(elementId) {
    // get the identifier prefix
    var identifierPrefix = elementId.replace("_OptionsCancelButton", "_");

    // get the options row and input fields in the options row
    var optionsRow = $("#" + identifierPrefix + "OptionsRow");
    var optionsIcon = $("#" + identifierPrefix + "OptionsIcon");
    var descriptionTextBox = $("#" + identifierPrefix + "DescriptionTextBox");
    var descriptionField = $("#" + identifierPrefix + "DescriptionField");
    var inputTypeDropDown = $("#" + identifierPrefix + "InputTypeDropDown");
    var inputTypeField = $("#" + identifierPrefix + "InputTypeField");
    var choiceValuesContainer = $("#" + identifierPrefix + "ChoiceValuesContainer");
    var choiceValuesTextBox = $("#" + identifierPrefix + "ChoiceValuesTextBox");
    var choiceValuesField = $("#" + identifierPrefix + "ChoiceValuesField");
    var choiceValuesAlphabetizeCheckBox = $("#" + identifierPrefix + "ChoiceValuesAlphabetizeCheckBox");
    var choiceValuesAlphabetizeField = $("#" + identifierPrefix + "ChoiceValuesAlphabetizeSettingField");
    var regularExpressionContainer = $("#" + identifierPrefix + "RegularExpressionContainer");
    var regularExpressionTextBox = $("#" + identifierPrefix + "RegularExpressionTextBox");
    var regularExpressionField = $("#" + identifierPrefix + "RegularExpressionField");

    // hide the row and show the icon
    optionsRow.hide();
    optionsIcon.show();

    // reset the input values back to the value of the hidden fields
    descriptionTextBox.val(descriptionField.val());
    inputTypeDropDown.val(inputTypeField.val());
    choiceValuesTextBox.val(choiceValuesField.val());
    choiceValuesAlphabetizeCheckBox.prop("checked", choiceValuesAlphabetizeField.val());
    regularExpressionTextBox.val(regularExpressionField.val());

    // set hidden/visible states for choice values and 
    // regular expression based on the selected input type
    if (inputTypeDropDown.length > 0) {
        if (inputTypeDropDown.val() == "TextField") {
            choiceValuesContainer.hide();
            regularExpressionContainer.show();
        }
        else if (inputTypeDropDown.val() == "Date") {
            choiceValuesContainer.hide();
            regularExpressionContainer.hide();
        }
        else {
            regularExpressionContainer.hide();
            choiceValuesContainer.show();
        }
    }
}

// Function : InputTypeDropDownChange
// This function shows/hides options related or unrelated to the selected value
// for the input type of a user field. 
function InputTypeDropDownChange(inputTypeDropDownId) {
    // get the identifier prefix
    var identifierPrefix = inputTypeDropDownId.replace("_InputTypeDropDown", "_");

    // get the elements related to the input type drop-down
    var inputTypeDropDown = $("#" + inputTypeDropDownId);
    var choiceValuesContainer = $("#" + identifierPrefix + "ChoiceValuesContainer");
    var choiceValuesTextBox = $("#" + identifierPrefix + "ChoiceValuesTextBox");
    var regularExpressionContainer = $("#" + identifierPrefix + "RegularExpressionContainer");
    var regularExpressionTextBox = $("#" + identifierPrefix + "RegularExpressionTextBox");
    var instructionPanel = $("#" + identifierPrefix + "InputTypeInstructionsContainer");

    // set values and hidden/visible states for choice values and 
    // regular expression based on the selected input type
    if (inputTypeDropDown.val() == "TextField") {
        choiceValuesTextBox.val("");
        choiceValuesContainer.hide();
        regularExpressionContainer.show();
        instructionPanel.hide();
    }
    else if (inputTypeDropDown.val() == "Date") {
        choiceValuesTextBox.val("");
        choiceValuesContainer.hide();
        regularExpressionTextBox.val("");
        regularExpressionContainer.hide();
        instructionPanel.show();
    }
    else {
        regularExpressionTextBox.val("");
        regularExpressionContainer.hide();
        choiceValuesContainer.show();
        instructionPanel.hide();
    }
}