﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.UMS.Pages.Administrator.Groups
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel GroupsFormContentWrapperContainer;
        public UpdatePanel GroupGridUpdatePanel;
        public Grid GroupGrid;
        public Panel ActionsPanel;        
        #endregion

        #region Private Properties
        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmAction;
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupCreator)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupDeleter)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupEditor)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/Default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, _GlobalResources.Groups, ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.GroupsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.GroupGrid.BindData();
            }
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            // ensure user has permissions to create groups before showing links
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupCreator))
            {
                Panel optionsPanelLinksContainer = new Panel();
                optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
                optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

                // ADD GROUP
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("AddGroupLink",
                                                    null,
                                                    "Modify.aspx",
                                                    null,
                                                    _GlobalResources.NewGroup,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                    );

                this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
            }
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the group grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // apply css class to container
            this.GroupGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.GroupGrid.StoredProcedure = Library.Group.GridProcedure;
            this.GroupGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.GroupGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.GroupGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.GroupGrid.IdentifierField = "idGroup";
            this.GroupGrid.DefaultSortColumn = "name";
            this.GroupGrid.SearchBoxPlaceholderText = _GlobalResources.SearchGroups;

            // data key names
            this.GroupGrid.DataKeyNames = new string[] { "idGroup" };

            // columns
            GridColumn nameMembers = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.Members + "", null, "name"); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.GroupGrid.AddColumn(nameMembers);
            this.GroupGrid.AddColumn(options);

            // add row data bound event
            this.GroupGrid.RowDataBound += new GridViewRowEventHandler(this._GroupGrid_RowDataBound);
        }
        #endregion

        #region _GroupGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _GroupGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idGroup = Convert.ToInt32(rowView["idGroup"]);

                // CHECK DELETE PERMISSIONS

                // default the delete checkbox to false
                string rowOrdinal = e.Row.DataItemIndex.ToString();
                CheckBox rowCheckBox = (CheckBox)e.Row.Cells[0].FindControl(this.GroupGrid.ID + "_GridSelectRecord_" + rowOrdinal);

                if (rowCheckBox != null)
                { rowCheckBox.Enabled = false; }

                // check the permission and if permitted, grant
                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupDeleter))
                {
                    if (rowCheckBox != null)
                    { rowCheckBox.Enabled = true; }
                }

                // AVATAR, NAME, MEMEBERS

                string avatar = rowView["avatar"].ToString();
                string name = Server.HtmlEncode(rowView["name"].ToString());
                int memberCount = Convert.ToInt32(rowView["memberCount"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                if (!String.IsNullOrWhiteSpace(avatar))
                {
                    avatarImagePath = SitePathConstants.SITE_GROUPS_ROOT + idGroup.ToString() + "/" + avatar;
                    avatarImageClass += " AvatarImage";
                }

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = name;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name - check permissions for dashboard link
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                // CHECK DASHBOARD PERMISSIONS - dashboard falls under manager permission

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, idGroup))
                {
                    HyperLink nameLink = new HyperLink();
                    nameLink.NavigateUrl = "Dashboard.aspx?id=" + idGroup.ToString();
                    nameLink.Text = name;
                    nameLabel.Controls.Add(nameLink);
                }
                else
                {
                    Literal nameLit = new Literal();
                    nameLit.Text = name;
                    nameLabel.Controls.Add(nameLit);
                }

                // number of members
                Label membersLabel = new Label();
                membersLabel.CssClass = "GridSecondaryLine";
                membersLabel.Text = memberCount + " " + ((memberCount != 1) ? _GlobalResources.Members : _GlobalResources.Member);
                e.Row.Cells[1].Controls.Add(membersLabel);

                // OPTIONS COLUMN

                // modify requires editor, manager, or role manager permission
                if (
                    AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupEditor, idGroup)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, idGroup)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager)
                   )
                {
                    // modify
                    Label modifySpan = new Label();
                    modifySpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(modifySpan);

                    HyperLink modifyLink = new HyperLink();
                    modifyLink.NavigateUrl = "Modify.aspx?id=" + idGroup.ToString();
                    modifySpan.Controls.Add(modifyLink);

                    Image modifyIcon = new Image();
                    modifyIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
                    modifyIcon.AlternateText = _GlobalResources.Modify;
                    modifyIcon.ToolTip = _GlobalResources.GroupProperties;
                    modifyLink.Controls.Add(modifyIcon);
                }

                // auto-join rules, enrollments, documents require manager permission
                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, idGroup))
                {
                    // auto-join rules
                    Label autojoinRulesSpan = new Label();
                    autojoinRulesSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(autojoinRulesSpan);

                    HyperLink autojoinRulesLink = new HyperLink();
                    autojoinRulesLink.NavigateUrl = "/administrator/groups/AutoJoinRules.aspx?gid=" + idGroup.ToString();
                    autojoinRulesSpan.Controls.Add(autojoinRulesLink);

                    Image autojoinRulesIcon = new Image();
                    autojoinRulesIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYNCHRONIZE_BUTTON, ImageFiles.EXT_PNG);
                    autojoinRulesIcon.AlternateText = _GlobalResources.AutoJoinRules;
                    autojoinRulesIcon.ToolTip = _GlobalResources.AutoJoinRules;
                    autojoinRulesLink.Controls.Add(autojoinRulesIcon);

                    // enrollments
                    Label enrollmentsSpan = new Label();
                    enrollmentsSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(enrollmentsSpan);

                    HyperLink enrollmentsLink = new HyperLink();
                    enrollmentsLink.NavigateUrl = "/administrator/groups/enrollments/Default.aspx?gid=" + idGroup.ToString();
                    enrollmentsSpan.Controls.Add(enrollmentsLink);

                    Image enrollmentsIcon = new Image();
                    enrollmentsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_BUTTON, ImageFiles.EXT_PNG);
                    enrollmentsIcon.AlternateText = _GlobalResources.Enrollments;
                    enrollmentsIcon.ToolTip = _GlobalResources.Enrollments;
                    enrollmentsLink.Controls.Add(enrollmentsIcon);

                    // documents - only if feature is enabled
                    if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE))
                    {
                        Label documentsSpan = new Label();
                        documentsSpan.CssClass = "GridImageLink";
                        e.Row.Cells[2].Controls.Add(documentsSpan);

                        HyperLink documentsLink = new HyperLink();
                        documentsLink.NavigateUrl = "/administrator/groups/documents/Default.aspx?gid=" + idGroup.ToString();
                        documentsSpan.Controls.Add(documentsLink);

                        Image documentsIcon = new Image();
                        documentsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MYACCOUNT_BUTTON, ImageFiles.EXT_PNG);
                        documentsIcon.AlternateText = _GlobalResources.Documents;
                        documentsIcon.ToolTip = _GlobalResources.Documents;
                        documentsLink.Controls.Add(documentsIcon);
                    }
                }
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedGroup_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedGroup_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseGroup_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.GroupGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.GroupGrid.Rows[i].FindControl(this.GroupGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }
                
                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.Group.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedGroup_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoGroup_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.GroupGrid.BindData();
            }
        }
        #endregion
    }
}
