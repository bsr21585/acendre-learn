﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using Asentia.UMS.Controls;

namespace Asentia.UMS.Pages.Administrator.Groups
{
    public class Roles : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel RolesFormContentWrapperContainer;
        public Panel GroupObjectMenuContainer;
        public Panel RolesPageWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel GroupRolesMembershipContainer;        
        #endregion

        #region Private Properties
        private Group _GroupObject;

        private ModalPopup _AddRolesToGroup;
        private ModalPopup _RemoveRolesFromGroup;

        private DataTable _RolesForGroup;
        private DataTable _RemoveEligibleRolesForGroup;
        private DataTable _RolesNotForGroup;

        private DynamicListBox _AddEligibleRoles;
        private DynamicListBox _RemoveEligibleRoles;        

        private UpdatePanel _GroupRolesUpdatePanel;
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the group object
            this._GetGroupObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/Roles.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.RolesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.RolesPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";            

            // build the group object menu
            if (this._GroupObject != null)
            {
                GroupObjectMenu groupObjectMenu = new GroupObjectMenu(this._GroupObject);
                groupObjectMenu.SelectedItem = GroupObjectMenu.MenuObjectItem.Roles;

                this.GroupObjectMenuContainer.Controls.Add(groupObjectMenu);
            }

            // build the role membership panel
            this._BuildRoleMembershipPanel();           
        }
        #endregion

        #region _GetGroupObject
        /// <summary>
        /// Gets a group object based on querystring if exists, redirects if not.
        /// </summary>
        private void _GetGroupObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("gid", 0);

            if (qsId > 0)
            {
                try
                { this._GroupObject = new Group(qsId); }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
            else
            { Response.Redirect("~/administrator/groups"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get group name information
            string groupNameInInterfaceLanguage = this._GroupObject.Name;
            string groupImagePath;
            string groupImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                }
            }

            if (this._GroupObject.Avatar != null)
            {
                groupImagePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                groupImageCssClass = "AvatarImage";
            }
            else
            {
                groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            }            

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups, "/administrator/groups"));
            breadCrumbLinks.Add(new BreadcrumbLink(groupNameInInterfaceLanguage, "/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Roles));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, groupNameInInterfaceLanguage, groupImagePath, _GlobalResources.Roles, ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG), groupImageCssClass);
        }
        #endregion

        #region _BuildRoleMembershipPanel
        /// <summary>
        /// Builds the panel that contains role membership information.
        /// </summary>
        private void _BuildRoleMembershipPanel()
        {
            // populate datatables for role selection
            this._RolesForGroup = this._GroupObject.GetRoles(false, null);
            this._RemoveEligibleRolesForGroup = this._GroupObject.GetRoles(true, null);
            this._RolesNotForGroup = Asentia.UMS.Library.Role.IdsAndNamesForGroupSelectList(this._GroupObject.Id, null);

            // clear controls from container
            this.GroupRolesMembershipContainer.Controls.Clear();

            this._GroupRolesUpdatePanel = new UpdatePanel();
            this._GroupRolesUpdatePanel.ID = "GroupRolesUpdatePanel";
            this._GroupRolesUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            // build a container for the role membership count
            Panel roleMembershipCountContainer = new Panel();
            roleMembershipCountContainer.ID = "GroupRolesMembershipCountContainer";

            HtmlGenericControl roleMembershipCountP = new HtmlGenericControl("p");
            roleMembershipCountP.InnerText = String.Format(_GlobalResources.ThisGroupIsAMemberOfXRole_s, this._RolesForGroup.Rows.Count.ToString());

            roleMembershipCountContainer.Controls.Add(roleMembershipCountP);

            // build a container for the role membership listing
            Panel roleListContainer = new Panel();
            roleListContainer.ID = "GroupRolesListingContainer";
            roleListContainer.CssClass = "ItemListingContainer";

            // loop through the datatable and add each member to the listing container
            foreach (DataRow row in this._RolesForGroup.Rows)
            {
                Panel roleNameContainer = new Panel();
                roleNameContainer.ID = "Role_" + row["idRole"].ToString();

                Literal roleName = new Literal();
                roleName.Text = row["name"].ToString();

                if (Convert.ToInt32(row["numRulesJoinedBy"]) > 0)
                { roleName.Text += " - " + String.Format(_GlobalResources.AutoJoinedByNumRules, row["numRulesJoinedBy"].ToString()); }

                roleNameContainer.Controls.Add(roleName);
                roleListContainer.Controls.Add(roleNameContainer);
            }

            // attach the group membership listing to the update panel
            this._GroupRolesUpdatePanel.ContentTemplateContainer.Controls.Add(roleMembershipCountContainer);
            this._GroupRolesUpdatePanel.ContentTemplateContainer.Controls.Add(roleListContainer);

            // attach update panel to container
            this.GroupRolesMembershipContainer.Controls.Add(this._GroupRolesUpdatePanel);

            // ADD/REMOVE USERS BUTTONS
            Panel buttonsPanel = new Panel();
            buttonsPanel.ID = "GroupRolesButtonsContainer";

            // add roles to group link
            Image addRolesToGroupImageForLink = new Image();
            addRolesToGroupImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            addRolesToGroupImageForLink.CssClass = "MediumIcon";

            Localize addRolesToGroupTextForLink = new Localize();
            addRolesToGroupTextForLink.Text = _GlobalResources.AddRole_s;

            LinkButton addRolesToGroupLink = new LinkButton();
            addRolesToGroupLink.ID = "LaunchAddRolesToGroupModal";
            addRolesToGroupLink.CssClass = "ImageLink";
            addRolesToGroupLink.Controls.Add(addRolesToGroupImageForLink);
            addRolesToGroupLink.Controls.Add(addRolesToGroupTextForLink);
            buttonsPanel.Controls.Add(addRolesToGroupLink);

            // remove roles from group link
            Image removeRolesFromGroupImageForLink = new Image();
            removeRolesFromGroupImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            removeRolesFromGroupImageForLink.CssClass = "MediumIcon";

            Localize removeRolesFromGroupTextForLink = new Localize();
            removeRolesFromGroupTextForLink.Text = _GlobalResources.RemoveRoles_s;

            LinkButton removeRolesFromGroupLink = new LinkButton();
            removeRolesFromGroupLink.ID = "LaunchRemoveRolesFromGroupModal";
            removeRolesFromGroupLink.CssClass = "ImageLink";
            removeRolesFromGroupLink.Controls.Add(removeRolesFromGroupImageForLink);
            removeRolesFromGroupLink.Controls.Add(removeRolesFromGroupTextForLink);
            buttonsPanel.Controls.Add(removeRolesFromGroupLink);

            // attach the buttons panel to the container
            this.GroupRolesMembershipContainer.Controls.Add(buttonsPanel);

            // build modals for adding and removing groups to/from roles
            this._BuildAddRolesToGroupModal(addRolesToGroupLink.ID);
            this._BuildRemoveRolesFromGroupModal(removeRolesFromGroupLink.ID);
        }
        #endregion

        #region _BuildAddRolesToGroupModal
        /// <summary>
        /// Builds the modal for adding roles to the group.
        /// </summary>
        private void _BuildAddRolesToGroupModal(string targetControlId)
        {
            // set modal properties
            this._AddRolesToGroup = new ModalPopup("AddRolesToGroupModal");
            this._AddRolesToGroup.Type = ModalPopupType.Form;
            this._AddRolesToGroup.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG);
            this._AddRolesToGroup.HeaderIconAlt = _GlobalResources.AddRole_sToGroup;
            this._AddRolesToGroup.HeaderText = _GlobalResources.AddRole_sToGroup;
            this._AddRolesToGroup.TargetControlID = targetControlId;
            this._AddRolesToGroup.SubmitButton.Command += new CommandEventHandler(this._AddRolesToGroupSubmit_Command);
            this._AddRolesToGroup.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the roles listing
            this._AddEligibleRoles = new DynamicListBox("AddEligibleRoles");
            this._AddEligibleRoles.NoRecordsFoundMessage = _GlobalResources.NoRolesFound;
            this._AddEligibleRoles.SearchButton.Command += new CommandEventHandler(this._SearchAddRolesButton_Command);
            this._AddEligibleRoles.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchAddRolesButton_Command);
            this._AddEligibleRoles.ListBoxControl.DataSource = this._RolesNotForGroup;
            this._AddEligibleRoles.ListBoxControl.DataTextField = "name";
            this._AddEligibleRoles.ListBoxControl.DataValueField = "idRole";
            this._AddEligibleRoles.ListBoxControl.DataBind();

            // add controls to body
            this._AddRolesToGroup.AddControlToBody(this._AddEligibleRoles);

            // add modal to container
            this.GroupRolesMembershipContainer.Controls.Add(this._AddRolesToGroup);
        }
        #endregion

        #region _BuildRemoveRolesFromGroupModal
        /// <summary>
        /// Builds the modal for removing roles from the group.
        /// </summary>
        private void _BuildRemoveRolesFromGroupModal(string targetControlId)
        {
            // set modal properties
            this._RemoveRolesFromGroup = new ModalPopup("RemoveRolesFromGroupModal");
            this._RemoveRolesFromGroup.Type = ModalPopupType.Form;
            this._RemoveRolesFromGroup.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            this._RemoveRolesFromGroup.HeaderIconAlt = _GlobalResources.RemoveRole_sFromGroup;
            this._RemoveRolesFromGroup.HeaderText = _GlobalResources.RemoveRole_sFromGroup;
            this._RemoveRolesFromGroup.TargetControlID = targetControlId;
            this._RemoveRolesFromGroup.SubmitButton.Command += new CommandEventHandler(this._RemoveRolesFromGroupSubmit_Command);
            this._RemoveRolesFromGroup.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the roles listing
            this._RemoveEligibleRoles = new DynamicListBox("RemoveEligibleRoles");
            this._RemoveEligibleRoles.NoRecordsFoundMessage = _GlobalResources.NoRolesFound;
            this._RemoveEligibleRoles.SearchButton.Command += new CommandEventHandler(this._SearchRemoveRolesButton_Command);
            this._RemoveEligibleRoles.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchRemoveRolesButton_Command);
            this._RemoveEligibleRoles.ListBoxControl.DataSource = this._RemoveEligibleRolesForGroup;
            this._RemoveEligibleRoles.ListBoxControl.DataTextField = "name";
            this._RemoveEligibleRoles.ListBoxControl.DataValueField = "idRole";
            this._RemoveEligibleRoles.ListBoxControl.DataBind();

            // add controls to body
            this._RemoveRolesFromGroup.AddControlToBody(this._RemoveEligibleRoles);

            // add modal to container
            this.GroupRolesMembershipContainer.Controls.Add(this._RemoveRolesFromGroup);
        }
        #endregion

        #region _SearchAddRolesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Add Roles" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchAddRolesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._AddRolesToGroup.ClearFeedback();

            // clear the listbox control
            this._AddEligibleRoles.ListBoxControl.Items.Clear();

            // do the search
            this._RolesNotForGroup = Asentia.UMS.Library.Role.IdsAndNamesForGroupSelectList(this._GroupObject.Id, this._AddEligibleRoles.SearchTextBox.Text);

            this._SelectAddEligibleRolesDataBind();
        }
        #endregion

        #region _ClearSearchAddRolesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Add Roles" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchAddRolesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._AddRolesToGroup.ClearFeedback();

            // clear the listbox control and search text box
            this._AddEligibleRoles.ListBoxControl.Items.Clear();
            this._AddEligibleRoles.SearchTextBox.Text = "";

            // clear the search
            this._RolesNotForGroup = Asentia.UMS.Library.Role.IdsAndNamesForGroupSelectList(this._GroupObject.Id, this._AddEligibleRoles.SearchTextBox.Text);

            this._SelectAddEligibleRolesDataBind();
        }
        #endregion

        #region _SelectAddEligibleRolesDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectAddEligibleRolesDataBind()
        {
            this._AddEligibleRoles.ListBoxControl.DataSource = this._RolesNotForGroup;
            this._AddEligibleRoles.ListBoxControl.DataTextField = "name";
            this._AddEligibleRoles.ListBoxControl.DataValueField = "idRole";
            this._AddEligibleRoles.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._AddEligibleRoles.ListBoxControl.Items.Count == 0)
            {
                this._AddRolesToGroup.SubmitButton.Enabled = false;
                this._AddRolesToGroup.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._AddRolesToGroup.SubmitButton.Enabled = true;
                this._AddRolesToGroup.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _SearchRemoveRolesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Remove Roles" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchRemoveRolesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._RemoveRolesFromGroup.ClearFeedback();

            // clear the listbox control
            this._RemoveEligibleRoles.ListBoxControl.Items.Clear();

            // do the search
            this._RemoveEligibleRolesForGroup = this._GroupObject.GetRoles(true, this._RemoveEligibleRoles.SearchTextBox.Text);

            this._SelectRemoveEligibleRolesDataBind();
        }
        #endregion

        #region _ClearSearchRemoveRolesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Remove Roles" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchRemoveRolesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._RemoveRolesFromGroup.ClearFeedback();

            // clear the listbox control and search text box
            this._RemoveEligibleRoles.ListBoxControl.Items.Clear();
            this._RemoveEligibleRoles.SearchTextBox.Text = "";

            // clear the search
            this._RemoveEligibleRolesForGroup = this._GroupObject.GetRoles(true, null);

            this._SelectRemoveEligibleRolesDataBind();
        }
        #endregion

        #region _SelectRemoveEligibleRolesDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectRemoveEligibleRolesDataBind()
        {
            this._RemoveEligibleRoles.ListBoxControl.DataSource = this._RemoveEligibleRolesForGroup;
            this._RemoveEligibleRoles.ListBoxControl.DataTextField = "name";
            this._RemoveEligibleRoles.ListBoxControl.DataValueField = "idRole";
            this._RemoveEligibleRoles.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._RemoveEligibleRoles.ListBoxControl.Items.Count == 0)
            {
                this._RemoveRolesFromGroup.SubmitButton.Enabled = false;
                this._RemoveRolesFromGroup.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._RemoveRolesFromGroup.SubmitButton.Enabled = true;
                this._RemoveRolesFromGroup.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _AddRolesToGroupSubmit_Command
        /// <summary>
        /// Click event handler for Add Roles To Group modal submit button.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _AddRolesToGroupSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get selected roles from the list box
                List<string> selectedRoles = this._AddEligibleRoles.GetSelectedValues();

                // throw exception if no groups selected
                if (selectedRoles.Count <= 0)
                { throw new AsentiaException(_GlobalResources.NoRolesSelected); }

                // add selected groups to data table
                DataTable rolesToAdd = new DataTable(); ;
                rolesToAdd.Columns.Add("id", typeof(int));

                foreach (string selectedRole in selectedRoles)
                { rolesToAdd.Rows.Add(Convert.ToInt32(selectedRole)); }

                // join the selected roles to the group
                this._GroupObject.JoinRoles(rolesToAdd);

                // remove the selected roles from the list box so they cannot be re-selected
                this._AddEligibleRoles.RemoveSelectedItems();

                //rebind roles membership panel
                this._BuildRoleMembershipPanel();
                this._GroupRolesUpdatePanel.Update();

                // display success feedback
                this._AddRolesToGroup.DisplayFeedback(_GlobalResources.Role_sAddedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddRolesToGroup.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddRolesToGroup.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddRolesToGroup.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AddRolesToGroup.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _RemoveRolesFromGroupSubmit_Command
        /// <summary>
        /// Click event handler for Remove Roles From Group modal submit button.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _RemoveRolesFromGroupSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get selected roles from the list box
                List<string> selectedRoles = this._RemoveEligibleRoles.GetSelectedValues();

                // throw exception if no groups selected
                if (selectedRoles.Count <= 0)
                { throw new AsentiaException(_GlobalResources.NoRolesSelected); }

                // add selected roles to data table
                DataTable rolesToRemove = new DataTable(); ;
                rolesToRemove.Columns.Add("id", typeof(int));

                foreach (string selectedRole in selectedRoles)
                { rolesToRemove.Rows.Add(Convert.ToInt32(selectedRole)); }

                // remove the selected roles from the group
                this._GroupObject.RemoveRoles(rolesToRemove);

                // remove the selected roles from the list box so they cannot be re-selected
                this._RemoveEligibleRoles.RemoveSelectedItems();

                //rebind roles membership panel
                this._BuildRoleMembershipPanel();
                this._GroupRolesUpdatePanel.Update();

                // display success feedback
                this._RemoveRolesFromGroup.DisplayFeedback(_GlobalResources.Role_sRemovedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._RemoveRolesFromGroup.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._RemoveRolesFromGroup.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._RemoveRolesFromGroup.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._RemoveRolesFromGroup.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._RemoveRolesFromGroup.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion
    }
}
