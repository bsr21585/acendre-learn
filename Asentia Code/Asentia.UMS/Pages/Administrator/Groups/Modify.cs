﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text;
using System.Web;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages.Administrator.Groups
{    
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel GroupModifyFormContentWrapperContainer;
        public Panel GroupObjectMenuContainer;
        public Panel GroupModifyWrapperContainer;
        public Panel GroupPropertiesInstructionsPanel;
        public Panel GroupPropertiesFeedbackContainer;
        public Panel GroupPropertiesContainer;        
        public Panel GroupPropertiesActionsPanel;
        public Panel GroupPropertiesTabPanelsContainer;
        #endregion

        #region Private Properties
        private HiddenField _PageAction;

        // GROUP MODIFY

        private Group _GroupObject;
        private bool _IsFormViewOnly = false;

        private UploaderAsync _GroupAvatar;

        private TextBox _GroupName;
        private TextBox _GroupShortDescription;
        private TextBox _GroupLongDescription;
        private TextBox _GroupSearchTags;

        private CheckBox _IsSelfJoin;
        private CheckBox _MembershipIsPublicized;
        private CheckBox _IsWallModerated;

        private RadioButtonList _GroupWall;        

        private HiddenField _ClearAvatar;

        private Image _AvatarImage;

        private Button _GroupPropertiesSaveButton;
        private Button _GroupPropertiesCancelButton;

        // GROUP MEMBERSHIP

        private ModalPopup _AddUsersToGroup;
        private ModalPopup _RemoveUsersFromGroup;
        
        private DataTable _UsersNotInGroup;
        private DataTable _UsersInGroup;
        private DataTable _RemoveEligibleUsersInGroup;

        private DynamicListBox _AddEligibleUsers;
        private DynamicListBox _RemoveEligibleUsers;        
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.UMS.Pages.Administrator.Groups.Modify.js");

            // build start up call for MCE and add to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));
            multipleStartUpCallsScript.AppendLine("var IsFormViewOnly = " + this._IsFormViewOnly.ToString().ToLower());

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine(" InitializeForm();");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the group object
            this._GetGroupObject();

            // check permissions
            if (
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupCreator)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupEditor, this._GroupObject.Id)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager)
                )
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/Modify.css");

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetGroupObject
        /// <summary>
        /// Gets a group object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetGroupObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    {
                        this._GroupObject = new Group(id);

                        // if the calling user does not have group editor permission, make the form view only
                        if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupEditor, this._GroupObject.Id))
                        { this._IsFormViewOnly = true; }
                    }
                }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // set container classes
            this.GroupModifyFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.GroupModifyWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";
            
            // only build the object menu for existing group and if the calling user has group or role manager permissions
            if (this._GroupObject != null 
                && 
                (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id)
                || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager)))
            {
                GroupObjectMenu groupObjectMenu = new GroupObjectMenu(this._GroupObject);
                groupObjectMenu.SelectedItem = GroupObjectMenu.MenuObjectItem.GroupProperties;

                this.GroupObjectMenuContainer.Controls.Add(groupObjectMenu);
            }                                  

            // build the group properties form
            this._BuildGroupPropertiesForm();

            // build the group properties form actions panel
            this._BuildGroupPropertiesActionsPanel();

            // if there is a group object and if the calling user has group manager permission - KEEP HERE FOR NOW, PLANNING TO ADD FUNCTIONALITY THAT WILL REQUIRE THIS
            if (this._GroupObject != null && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            {                
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string groupImagePath;
            string imageCssClass = null;
            string pageTitle;

            if (this._GroupObject != null)
            {
                string groupNameInInterfaceLanguage = this._GroupObject.Name;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                    {
                        if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                    }
                }                

                if (this._GroupObject.Avatar != null)
                {
                    groupImagePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    imageCssClass = "AvatarImage";
                }
                else
                {
                    groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
                }

                breadCrumbPageTitle = groupNameInInterfaceLanguage;
                pageTitle = groupNameInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewGroup;
                groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
                pageTitle = _GlobalResources.NewGroup;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups, "/administrator/groups"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, pageTitle, groupImagePath, imageCssClass);
        }
        #endregion

        #region _BuildGroupPropertiesForm
        /// <summary>
        /// Builds the Group Properties form.
        /// </summary>
        private void _BuildGroupPropertiesForm()
        {
            // clear controls from container
            this.GroupPropertiesContainer.Controls.Clear();

            // build the group properties form tabs
            this._BuildGroupPropertiesFormTabs();
            
            // form instructions panel - "Properties" is the first tab so do not hide this
            Panel formInstructionsPanel = new Panel();
            formInstructionsPanel.ID = "GroupFormInstructionsPanel";
            this.FormatPageInformationPanel(formInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisGroup, true);
            this.GroupPropertiesContainer.Controls.Add(formInstructionsPanel);

            // modules instructions panel
            Panel groupMembershipInstructionsPanel = new Panel();
            groupMembershipInstructionsPanel.ID = "GroupMembershipInstructionsPanel";
            groupMembershipInstructionsPanel.Style.Add("display", "none");
            this.FormatPageInformationPanel(groupMembershipInstructionsPanel, _GlobalResources.YouCanViewAndManageGroupMembershipUsingTheFormBelow, true);
            this.GroupPropertiesContainer.Controls.Add(groupMembershipInstructionsPanel);

            // tab panels container
            this.GroupPropertiesTabPanelsContainer = new Panel();
            this.GroupPropertiesTabPanelsContainer.ID = "GroupProperties_TabPanelsContainer";
            this.GroupPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.GroupPropertiesContainer.Controls.Add(this.GroupPropertiesTabPanelsContainer);

            // build the "properties" panel of the group properties form
            this._BuildGroupPropertiesFormPropertiesPanel();

            if (this._GroupObject != null && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            {
                // build the "group membership" panel of the group properties form
                this._BuildGroupPropertiesFormMembershipPanel();
            }

            // populate the form input elements
            this._PopulateGroupPropertiesInputElements();
        }
        #endregion

        #region _BuildGroupPropertiesFormTabs
        /// <summary>
        /// Method to build the group property form tabs
        /// </summary>
        private void _BuildGroupPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));

            if (this._GroupObject != null && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            { 
                tabs.Enqueue(new KeyValuePair<string, string>("GroupMembers", _GlobalResources.GroupMembers)); 
            }

            // build and attach the tabs
            this.GroupPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("GroupProperties", tabs, null, this.Page, "GroupPropertiesTabChange"));
        }
        #endregion

        #region _BuildGroupPropertiesFormPropertiesPanel
        /// <summary>
        /// Builds the group properties form fields under properties tab.
        /// </summary>
        private void _BuildGroupPropertiesFormPropertiesPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "GroupProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.CssClass = "TabPanelContainer";
            propertiesPanel.Attributes.Add("style", "display: block;");

            // group name field
            this._GroupName = new TextBox();
            this._GroupName.ID = "GroupName_Field";
            this._GroupName.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("GroupName",
                                                             _GlobalResources.Name,
                                                             this._GroupName.ID,
                                                             this._GroupName,
                                                             true,
                                                             true,
                                                             true));

            // group avatar field
            List<Control> avatarInputControls = new List<Control>();

            if (this._GroupObject != null)
            {
                if (this._GroupObject.Avatar != null)
                {
                    // group avatar image panel
                    Panel avatarImageContainer = new Panel();
                    avatarImageContainer.ID = "GroupAvatar_Field_ImageContainer";
                    avatarImageContainer.CssClass = "AvatarImageContainer";

                    // avatar image
                    this._AvatarImage = new Image();
                    avatarImageContainer.Controls.Add(this._AvatarImage);
                    avatarInputControls.Add(avatarImageContainer);

                    Panel avatarImageDeleteButtonContainer = new Panel();
                    avatarImageDeleteButtonContainer.ID = "AvatarImageDeleteButtonContainer";
                    avatarImageDeleteButtonContainer.CssClass = "AvatarDeleteButtonContainer";
                    avatarImageContainer.Controls.Add(avatarImageDeleteButtonContainer);

                    // delete group avatar image
                    Image deleteAvatarImage = new Image();
                    deleteAvatarImage.ID = "AvatarImageDeleteButton";
                    deleteAvatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    deleteAvatarImage.CssClass = "SmallIcon";
                    deleteAvatarImage.Attributes.Add("onclick", "javascript:DeleteGroupAvatar();");
                    deleteAvatarImage.Style.Add("cursor", "pointer");

                    avatarImageDeleteButtonContainer.Controls.Add(deleteAvatarImage);

                    // clear avatar hidden field
                    this._ClearAvatar = new HiddenField();
                    this._ClearAvatar.ID = "ClearAvatar_Field";
                    this._ClearAvatar.Value = "false";
                    avatarInputControls.Add(this._ClearAvatar);
                }
            }

            // avatar image upload
            this._GroupAvatar = new UploaderAsync("GroupAvatar_Field", UploadType.Avatar, "GroupAvatar_ErrorContainer");

            // set params to resize the avatar upon upload
            this._GroupAvatar.ResizeOnUpload = true;
            this._GroupAvatar.ResizeMaxWidth = 256;
            this._GroupAvatar.ResizeMaxHeight = 256;

            avatarInputControls.Add(this._GroupAvatar);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("GroupAvatar",
                                                                                 _GlobalResources.Avatar,
                                                                                 avatarInputControls,
                                                                                 false,
                                                                                 true));

            // is self join field
            this._IsSelfJoin = new CheckBox();
            this._IsSelfJoin.ID = "IsSelfJoin_Field";
            this._IsSelfJoin.Attributes.Add("onclick", "SetIsMembershipPublicizedStatus();");
            this._IsSelfJoin.Text = _GlobalResources.AllowUsersToJoinThemselvesToThisGroup;

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
            {
                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("IsSelfJoin",
                                                                 _GlobalResources.SelfJoin,
                                                                 this._IsSelfJoin.ID,
                                                                 this._IsSelfJoin,
                                                                 false,
                                                                 false,
                                                                 false));
            }

            // membership is publicized field
            this._MembershipIsPublicized = new CheckBox();
            this._MembershipIsPublicized.ID = "MembershipIsPublicized_Field";
            this._MembershipIsPublicized.Text = _GlobalResources.IfThisIsCheckedUserWillBeAwareOfTheirMembershipOfThisGroup;

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
            {
                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("MembershipIsPublicized",
                                                                 _GlobalResources.PublicizeMembership,
                                                                 this._MembershipIsPublicized.ID,
                                                                 this._MembershipIsPublicized,
                                                                 false,
                                                                 false,
                                                                 false));
            }

            // group wall field - only if group discussion feature is enabled
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
            {
                List<Control> groupWallInputControls = new List<Control>();

                Panel groupWallFieldInputContainer = new Panel();
                groupWallFieldInputContainer.ID = "GroupWall_Field_Container";

                this._GroupWall = new RadioButtonList();
                this._GroupWall.ID = "GroupWall_Field";

                ListItem groupWallOn = new ListItem();
                groupWallOn.Text = _GlobalResources.On;
                groupWallOn.Value = "True";
                groupWallOn.Attributes.Add("onclick", "GroupWallOn();");

                ListItem groupWallOff = new ListItem();
                groupWallOff.Text = _GlobalResources.Off;
                groupWallOff.Value = "False";
                groupWallOff.Attributes.Add("onclick", "GroupWallOff();");

                this._GroupWall.Items.Add(groupWallOn);
                this._GroupWall.Items.Add(groupWallOff);
                this._GroupWall.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
                this._GroupWall.SelectedIndex = 0;
                groupWallFieldInputContainer.Controls.Add(this._GroupWall);
                groupWallInputControls.Add(groupWallFieldInputContainer);

                // is wall moderated field
                Panel groupWallIsModeratedFieldInputContainer = new Panel();
                groupWallIsModeratedFieldInputContainer.ID = "IsGroupWallModerated_Field_Container";

                this._IsWallModerated = new CheckBox();
                this._IsWallModerated.ID = "IsGroupWallModerated_Field";
                this._IsWallModerated.Text = _GlobalResources.IfThisIsCheckedDiscussionPostsForThisGroupWillBeModerated;
                groupWallIsModeratedFieldInputContainer.Controls.Add(this._IsWallModerated);
                groupWallInputControls.Add(groupWallIsModeratedFieldInputContainer);

                propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Group",
                                                                                    _GlobalResources.Discussion,
                                                                                    groupWallInputControls,
                                                                                    false,
                                                                                    true));
            }

            // group short description field
            this._GroupShortDescription = new TextBox();
            this._GroupShortDescription.ID = "GroupShortDescription_Field";
            this._GroupShortDescription.Style.Add("width", "98%");
            this._GroupShortDescription.TextMode = TextBoxMode.MultiLine;
            this._GroupShortDescription.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("GroupShortDescription",
                                                             _GlobalResources.ShortDescription,
                                                             this._GroupShortDescription.ID,
                                                             this._GroupShortDescription,
                                                             false,
                                                             true,
                                                             true));

            // group long description field
            this._GroupLongDescription = new TextBox();
            this._GroupLongDescription.ID = "GroupLongDescription_Field";
            this._GroupLongDescription.CssClass = "ckeditor";
            this._GroupLongDescription.Style.Add("width", "98%");
            this._GroupLongDescription.TextMode = TextBoxMode.MultiLine;
            this._GroupLongDescription.Rows = 10;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("GroupLongDescription",
                                                             _GlobalResources.Description,
                                                             this._GroupLongDescription.ID,
                                                             this._GroupLongDescription,
                                                             false,
                                                             true,
                                                             true));

            // search tags field
            this._GroupSearchTags = new TextBox();
            this._GroupSearchTags.ID = "GroupSearchTags_Field";
            this._GroupSearchTags.Style.Add("width", "98%");
            this._GroupSearchTags.TextMode = TextBoxMode.MultiLine;
            this._GroupSearchTags.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("GroupSearchTags",
                                                             _GlobalResources.SearchTags,
                                                             this._GroupSearchTags.ID,
                                                             this._GroupSearchTags,
                                                             false,
                                                             false,
                                                             true));

            // attach panel to container
            this.GroupPropertiesTabPanelsContainer.Controls.Add(propertiesPanel);            
        }
        #endregion

        #region _BuildGroupPropertiesFormMembershipPanel
        /// <summary>
        /// Builds the group properties form fields under group membership tab.
        /// </summary>
        private void _BuildGroupPropertiesFormMembershipPanel()
        {
            Panel groupMembershipPanel = new Panel();
            groupMembershipPanel.ID = "GroupProperties_" + "GroupMembers" + "_TabPanel";
            groupMembershipPanel.CssClass = "TabPanelContainer";
            groupMembershipPanel.Attributes.Add("style", "display: none;");

            // build the update panel for group membership
            UpdatePanel groupMembershipFormUpdatePanel = new UpdatePanel();
            groupMembershipFormUpdatePanel.ID = "GroupMembershipFormUpdatePanel";
            groupMembershipFormUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;            

            // build a container for the user membership listing
            Panel userListContainer = new Panel();
            userListContainer.ID = "GroupMembershipListingContainer";
            userListContainer.CssClass = "ItemListingContainer";

            // attach the group users membership listing to the update panel
            groupMembershipFormUpdatePanel.ContentTemplateContainer.Controls.Add(userListContainer);

            // add the update panel to group membership panel
            groupMembershipPanel.Controls.Add(groupMembershipFormUpdatePanel);

            // ADD/REMOVE USERS BUTTONS

            Panel buttonsPanel = new Panel();
            buttonsPanel.ID = "GroupMembershipButtonsContainer";

            // add users to group link
            Image addUsersToGroupImageForLink = new Image();
            addUsersToGroupImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            addUsersToGroupImageForLink.CssClass = "MediumIcon";

            Localize addUsersToGroupTextForLink = new Localize();
            addUsersToGroupTextForLink.Text = _GlobalResources.AddUser_s;

            LinkButton addUsersToGroupLink = new LinkButton();
            addUsersToGroupLink.ID = "LaunchAddUsersToGroupModal";
            addUsersToGroupLink.CssClass = "ImageLink";
            addUsersToGroupLink.Controls.Add(addUsersToGroupImageForLink);
            addUsersToGroupLink.Controls.Add(addUsersToGroupTextForLink);
            buttonsPanel.Controls.Add(addUsersToGroupLink);

            // remove users from group link
            Image removeUsersFromGroupImageForLink = new Image();
            removeUsersFromGroupImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            removeUsersFromGroupImageForLink.CssClass = "MediumIcon";

            Localize removeUsersFromGroupTextForLink = new Localize();
            removeUsersFromGroupTextForLink.Text = _GlobalResources.RemoveUser_s;

            LinkButton removeUsersFromGroupLink = new LinkButton();
            removeUsersFromGroupLink.ID = "LaunchRemoveUsersFromGroupModal";
            removeUsersFromGroupLink.CssClass = "ImageLink";
            removeUsersFromGroupLink.Controls.Add(removeUsersFromGroupImageForLink);
            removeUsersFromGroupLink.Controls.Add(removeUsersFromGroupTextForLink);
            buttonsPanel.Controls.Add(removeUsersFromGroupLink);

            // attach the buttons panel to the group membership panel
            groupMembershipPanel.Controls.Add(buttonsPanel);

            // attach panel to container
            this.GroupPropertiesTabPanelsContainer.Controls.Add(groupMembershipPanel);

            // build the group members listing
            this._BuildGroupMembersListing();

            // build modals for adding and removing users to/from the group
            this._BuildAddUsersToGroupModal(addUsersToGroupLink.ID);
            this._BuildRemoveUsersFromGroupModal(removeUsersFromGroupLink.ID);
        }
        #endregion

        #region _BuildGroupMembersListing
        private void _BuildGroupMembersListing()
        {
            // get the user listing container
            Panel userListContainer = (Panel)this.GroupPropertiesTabPanelsContainer.FindControl("GroupMembershipListingContainer");

            // clear any controls
            userListContainer.Controls.Clear();

            // populate datatables with lists of users who are in the group and who are not            
            this._UsersInGroup = this._GroupObject.GetUsers(false, null);
            this._RemoveEligibleUsersInGroup = this._GroupObject.GetUsers(true, null);
            this._UsersNotInGroup = Asentia.UMS.Library.User.IdsAndNamesForGroupSelectList(this._GroupObject.Id, null);            

            // loop through the datatable and add each member to the listing container
            foreach (DataRow row in this._UsersInGroup.Rows)
            {
                Panel userNameContainer = new Panel();
                userNameContainer.ID = "Member_" + row["idUser"].ToString();

                Literal userName = new Literal();

                // check whether the logged in user has permission to view the user dashboard
                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._GroupObject.Id))
                { userName.Text = "<a href=\"/administrator/users/Dashboard.aspx?id=" + row["idUser"].ToString() + "\">" + row["displayName"].ToString() + "</a>"; }
                else
                { userName.Text = row["displayName"].ToString(); }

                if (Convert.ToInt32(row["numRulesJoinedBy"]) > 0)
                { userName.Text += " - " + String.Format(_GlobalResources.AutoJoinedByNumRules, row["numRulesJoinedBy"].ToString()); }

                userNameContainer.Controls.Add(userName);
                userListContainer.Controls.Add(userNameContainer);
            }
        }
        #endregion

        #region _BuildGroupPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for Group Properties actions.
        /// </summary>
        private void _BuildGroupPropertiesActionsPanel()
        {
            // clear controls from container
            this.GroupPropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.GroupPropertiesActionsPanel.CssClass = "ActionsPanel";

            // GROUP SAVE BUTTON

            if (!this._IsFormViewOnly)
            {
                this._GroupPropertiesSaveButton = new Button();
                this._GroupPropertiesSaveButton.ID = "GroupSaveButton";
                this._GroupPropertiesSaveButton.CssClass = "Button ActionButton SaveButton";

                // if the object is null, it's a new object, so make button text say "Create"
                if (this._GroupObject == null)
                { this._GroupPropertiesSaveButton.Text = _GlobalResources.CreateGroup; }
                else
                { this._GroupPropertiesSaveButton.Text = _GlobalResources.SaveChanges; }

                this._GroupPropertiesSaveButton.Command += new CommandEventHandler(this._GroupPropertiesSaveButton_Command);

                this.GroupPropertiesActionsPanel.Controls.Add(this._GroupPropertiesSaveButton);

                // set the default button for the group properties container to the id of the properties save button
                this.GroupPropertiesContainer.DefaultButton = this._GroupPropertiesSaveButton.ID;
            }
            
            // CANCEL BUTTON - "DONE" BUTTON IF "VIEW ONLY"

            this._GroupPropertiesCancelButton = new Button();
            this._GroupPropertiesCancelButton.ID = "CancelButton";
            this._GroupPropertiesCancelButton.CssClass = "Button NonActionButton";

            if (this._IsFormViewOnly)
            {
                this._GroupPropertiesCancelButton.CssClass = "Button ActionButton";
                this._GroupPropertiesCancelButton.Text = _GlobalResources.Done;
            }
            else
            { this._GroupPropertiesCancelButton.Text = _GlobalResources.Cancel; }

            this._GroupPropertiesCancelButton.Command += new CommandEventHandler(this._GroupPropertiesCancelButton_Command);

            this.GroupPropertiesActionsPanel.Controls.Add(this._GroupPropertiesCancelButton);

            // PAGE ACTION HIDDEN FIELD

            this._PageAction = new HiddenField();
            this._PageAction.ID = "PageAction";
            this._PageAction.Value = this.QueryStringString("action", String.Empty);
            this.GroupPropertiesActionsPanel.Controls.Add(this._PageAction);
        }
        #endregion

        #region _PopulateGroupPropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateGroupPropertiesInputElements()
        {
            if (this._GroupObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // name, short description, long description
                bool isDefaultPopulated = false;

                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._GroupName.Text = groupLanguageSpecificProperty.Name;
                        this._GroupShortDescription.Text = groupLanguageSpecificProperty.ShortDescription;
                        this._GroupLongDescription.Text = groupLanguageSpecificProperty.LongDescription;
                        this._GroupSearchTags.Text = groupLanguageSpecificProperty.SearchTags;

                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificGroupNameTextBox = (TextBox)this.GroupPropertiesContainer.FindControl(this._GroupName.ID + "_" + groupLanguageSpecificProperty.LangString);
                        TextBox languageSpecificGroupShortDescriptionTextBox = (TextBox)this.GroupPropertiesContainer.FindControl(this._GroupShortDescription.ID + "_" + groupLanguageSpecificProperty.LangString);
                        TextBox languageSpecificGroupLongDescriptionTextBox = (TextBox)this.GroupPropertiesContainer.FindControl(this._GroupLongDescription.ID + "_" + groupLanguageSpecificProperty.LangString);
                        TextBox languageSpecificGroupSearchTagsTextBox = (TextBox)this.GroupPropertiesContainer.FindControl(this._GroupSearchTags.ID + "_" + groupLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificGroupNameTextBox != null)
                        { languageSpecificGroupNameTextBox.Text = groupLanguageSpecificProperty.Name; }

                        if (languageSpecificGroupShortDescriptionTextBox != null)
                        { languageSpecificGroupShortDescriptionTextBox.Text = groupLanguageSpecificProperty.ShortDescription; }

                        if (languageSpecificGroupLongDescriptionTextBox != null)
                        { languageSpecificGroupLongDescriptionTextBox.Text = groupLanguageSpecificProperty.LongDescription; }

                        if (languageSpecificGroupSearchTagsTextBox != null)
                        { languageSpecificGroupSearchTagsTextBox.Text = groupLanguageSpecificProperty.SearchTags; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._GroupName.Text = this._GroupObject.Name;
                    this._GroupShortDescription.Text = this._GroupObject.ShortDescription;
                    this._GroupLongDescription.Text = this._GroupObject.LongDescription;
                    this._GroupSearchTags.Text = this._GroupObject.SearchTags;
                }

                // NON-LANGUAGE SPECIFIC PROPERTIES

                // avatar image
                if (this._GroupObject.Avatar != null)
                {
                    this._AvatarImage.ImageUrl = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                }

                // group feed object (wall) - if group discussion feature is enabled
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
                {
                    if (Convert.ToBoolean(this._GroupObject.IsFeedActive))
                    {
                        this._GroupWall.SelectedIndex = 0;
                        this._IsWallModerated.Checked = Convert.ToBoolean(this._GroupObject.IsFeedModerated);
                    }
                    else
                    {
                        this._GroupWall.SelectedIndex = 1;
                        this._IsWallModerated.Checked = false;
                    }
                }

                // allow self-join
                this._IsSelfJoin.Checked = Convert.ToBoolean(this._GroupObject.IsSelfJoinAllowed);

                // publicize membership
                if (this._IsSelfJoin.Checked || Convert.ToBoolean(this._GroupObject.IsFeedActive))
                {
                    this._MembershipIsPublicized.Checked = true;
                    this._MembershipIsPublicized.Enabled = false;
                }
                else
                { this._MembershipIsPublicized.Checked = Convert.ToBoolean(this._GroupObject.MembershipIsPublicized); }
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;

            // TITLE - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._GroupName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.GroupPropertiesContainer, "Name", _GlobalResources.Name + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            return isValid;
        }
        #endregion

        #region _GroupPropertiesSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _GroupPropertiesSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                // if there is no group object, create one
                if (this._GroupObject == null)
                { this._GroupObject = new Group(); }

                // bounce if the calling user does not have permission to create or edit the group
                if (
                    (this._GroupObject.Id == 0 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupCreator))
                    || (this._GroupObject.Id > 0 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupEditor, this._GroupObject.Id))
                   )
                { Response.Redirect("/"); }
                
                int id;

                // populate the object
                this._GroupObject.Name = this._GroupName.Text;                

                if (!String.IsNullOrWhiteSpace(this._GroupShortDescription.Text))
                { this._GroupObject.ShortDescription = this._GroupShortDescription.Text; }
                else
                { this._GroupObject.ShortDescription = null; }
                
                if (!String.IsNullOrWhiteSpace(this._GroupLongDescription.Text))
                { this._GroupObject.LongDescription = HttpUtility.HtmlDecode(this._GroupLongDescription.Text); }
                else
                { this._GroupObject.LongDescription = null; }
                
                if (!String.IsNullOrWhiteSpace(this._GroupSearchTags.Text))
                { this._GroupObject.SearchTags = this._GroupSearchTags.Text; }
                else
                { this._GroupObject.SearchTags = null; }

                this._GroupObject.IsSelfJoinAllowed = this._IsSelfJoin.Checked;

                // group discussion - only if group discussion feature is enabled
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
                {
                    if (this._IsSelfJoin.Checked || Convert.ToBoolean(this._GroupWall.SelectedValue))
                    { this._GroupObject.MembershipIsPublicized = true; }
                    else
                    { this._GroupObject.MembershipIsPublicized = this._MembershipIsPublicized.Checked; }
                
                    if (Convert.ToBoolean(this._GroupWall.SelectedValue))
                    {
                        this._GroupObject.IsFeedActive = true;
                        this._GroupObject.IsFeedModerated = this._IsWallModerated.Checked;
                    }
                    else
                    {
                        this._GroupObject.IsFeedActive = false;
                        this._GroupObject.IsFeedModerated = false;
                    }
                }
                else
                {
                    if (this._IsSelfJoin.Checked)
                    { this._GroupObject.MembershipIsPublicized = true; }
                    else
                    { this._GroupObject.MembershipIsPublicized = this._MembershipIsPublicized.Checked; }
                }

                // do avatar if existing group, if its a new group, we'll do it after initial save
                bool isNewGroup = true;
                
                if (this._GroupObject.Id > 0)
                {
                    isNewGroup = false;

                    if (this._GroupAvatar.SavedFilePath != null)
                    {
                        // check user folder existence and create if necessary
                        if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id)))
                        { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id)); }

                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._GroupAvatar.SavedFilePath)))
                        {
                            fullSavedFilePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar" + Path.GetExtension(this._GroupAvatar.SavedFilePath);
                            fullSavedFilePathSmall = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar.small" + Path.GetExtension(this._GroupAvatar.SavedFilePath);

                            // delete existing avatar images if any
                            if (this._GroupObject.Avatar != null)
                            {                                
                                if (File.Exists(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar" + Path.GetExtension(this._GroupObject.Avatar))))
                                { File.Delete(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar" + Path.GetExtension(this._GroupObject.Avatar))); }

                                if (File.Exists(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar.small" + Path.GetExtension(this._GroupObject.Avatar))))
                                { File.Delete(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar.small" + Path.GetExtension(this._GroupObject.Avatar))); }
                            }

                            // move the uploaded file into the group's folder
                            File.Copy(Server.MapPath(this._GroupAvatar.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._GroupAvatar.SavedFilePath));
                            this._GroupObject.Avatar = "avatar" + Path.GetExtension(this._GroupAvatar.SavedFilePath);

                            // create a smaller version of the avatar fo use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            this._GroupObject.SaveAvatar();
                        }
                        else
                        {
                            this._GroupObject.Avatar = null;
                        }
                    }
                    else if (this._ClearAvatar != null)
                    {
                        if (this._ClearAvatar.Value == "true")
                        {
                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar" + Path.GetExtension(this._GroupObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar" + Path.GetExtension(this._GroupObject.Avatar))); }

                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar.small" + Path.GetExtension(this._GroupObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar.small" + Path.GetExtension(this._GroupObject.Avatar))); }

                            this._GroupObject.Avatar = null;

                            this._GroupObject.SaveAvatar();
                        }
                    }
                    else
                    { }
                }

                // clean up self join and publicize membership option in lite version
                if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
                {
                    this._GroupObject.IsSelfJoinAllowed = false;
                    this._GroupObject.MembershipIsPublicized = false;
                }

                // save the group, save its returned id to viewstate, and set the current group object's id
                id = this._GroupObject.Save();
                this.ViewState["id"] = id;
                this._GroupObject.Id = id;

                // if this was a new group we just saved, we now have the id so that
                // we can create the group's folder and update the avatar if necessary.
                if (isNewGroup)
                {
                    // check group folder existence and create if necessary
                    if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id)))
                    { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id)); }

                    // if there is an avatar, move it and update the database record
                    if (this._GroupAvatar.SavedFilePath != null)
                    {
                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._GroupAvatar.SavedFilePath)))
                        {
                            fullSavedFilePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar" + Path.GetExtension(this._GroupAvatar.SavedFilePath);
                            fullSavedFilePathSmall = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + "avatar.small" + Path.GetExtension(this._GroupAvatar.SavedFilePath);

                            // move the uploaded file into the user's folder
                            File.Copy(Server.MapPath(this._GroupAvatar.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._GroupAvatar.SavedFilePath));
                            this._GroupObject.Avatar = "avatar" + Path.GetExtension(this._GroupAvatar.SavedFilePath);

                            // create a smaller version of the avatar fo use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            this._GroupObject.SaveAvatar();
                        }
                    }
                }

                // do group language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string groupName = null;
                        string groupShortDescription = null;
                        string groupLongDescription = null;
                        string groupSearchTags = null;

                        // get text boxes
                        TextBox languageSpecificGroupNameTextBox = (TextBox)this.GroupPropertiesContainer.FindControl(this._GroupName.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificGroupShortDescriptionTextBox = (TextBox)this.GroupPropertiesContainer.FindControl(this._GroupShortDescription.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificGroupLongDescriptionTextBox = (TextBox)this.GroupPropertiesContainer.FindControl(this._GroupLongDescription.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificGroupSearchTagsTextBox = (TextBox)this.GroupPropertiesContainer.FindControl(this._GroupSearchTags.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificGroupNameTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificGroupNameTextBox.Text))
                            { groupName = languageSpecificGroupNameTextBox.Text; }
                        }

                        if (languageSpecificGroupShortDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificGroupShortDescriptionTextBox.Text))
                            { groupShortDescription = languageSpecificGroupShortDescriptionTextBox.Text; }
                        }

                        if (languageSpecificGroupLongDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificGroupLongDescriptionTextBox.Text))
                            { groupLongDescription = languageSpecificGroupLongDescriptionTextBox.Text; }
                        }

                        if (languageSpecificGroupSearchTagsTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificGroupSearchTagsTextBox.Text))
                            { groupSearchTags = languageSpecificGroupSearchTagsTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(groupName) ||
                            !String.IsNullOrWhiteSpace(groupShortDescription) ||
                            !String.IsNullOrWhiteSpace(groupLongDescription) ||
                            !String.IsNullOrWhiteSpace(groupSearchTags))
                        {
                            this._GroupObject.SaveLang(cultureInfo.Name,
                                                       groupName,
                                                       groupShortDescription,
                                                       HttpUtility.HtmlDecode(groupLongDescription),
                                                       groupSearchTags);
                        }
                    }
                }

                // load the saved group object
                this._GroupObject = new Group(id);

                // if the calling user does not have group editor permission, make the form view only
                if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupEditor, this._GroupObject.Id))
                { this._IsFormViewOnly = true; }
                
                // clear controls for containers that have dynamically added elements
                this.GroupObjectMenuContainer.Controls.Clear();                

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.GroupPropertiesFeedbackContainer, _GlobalResources.GroupHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.GroupPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.GroupPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.GroupPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.GroupPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.GroupPropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _GroupPropertiesCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _GroupPropertiesCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/groups");
        }
        #endregion

        #region _BuildAddUsersToGroupModal
        /// <summary>
        /// Builds the modal for adding users to the group.
        /// </summary>
        private void _BuildAddUsersToGroupModal(string targetControlId)
        {
            // set modal properties
            this._AddUsersToGroup = new ModalPopup("AddUsersToGroupModal");
            this._AddUsersToGroup.Type = ModalPopupType.Form;
            this._AddUsersToGroup.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            this._AddUsersToGroup.HeaderIconAlt = _GlobalResources.AddUser_sToGroup;
            this._AddUsersToGroup.HeaderText = _GlobalResources.AddUser_sToGroup;
            this._AddUsersToGroup.TargetControlID = targetControlId;
            this._AddUsersToGroup.SubmitButton.Command += new CommandEventHandler(this._AddUsersToGroupSubmit_Command);

            // build the modal body
            // build a container for the user listing
            this._AddEligibleUsers = new DynamicListBox("AddEligibleUsers");
            this._AddEligibleUsers.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._AddEligibleUsers.SearchButton.Command += new CommandEventHandler(this._SearchAddUsersButton_Command);
            this._AddEligibleUsers.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchAddUsersButton_Command);
            this._AddEligibleUsers.ListBoxControl.DataSource = this._UsersNotInGroup;
            this._AddEligibleUsers.ListBoxControl.DataTextField = "displayName";
            this._AddEligibleUsers.ListBoxControl.DataValueField = "idUser";
            this._AddEligibleUsers.ListBoxControl.DataBind();

            // add controls to body
            this._AddUsersToGroup.AddControlToBody(this._AddEligibleUsers);

            // add modal to container
            this.GroupPropertiesContainer.Controls.Add(this._AddUsersToGroup);
        }
        #endregion

        #region _BuildRemoveUsersFromGroupModal
        /// <summary>
        /// Builds the modal for removing users from the group.
        /// </summary>
        private void _BuildRemoveUsersFromGroupModal(string targetControlId)
        {
            // set modal properties
            this._RemoveUsersFromGroup = new ModalPopup("RemoveUsersFromGroupModal");
            this._RemoveUsersFromGroup.Type = ModalPopupType.Form;
            this._RemoveUsersFromGroup.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            this._RemoveUsersFromGroup.HeaderIconAlt = _GlobalResources.RemoveUser_sFromGroup;
            this._RemoveUsersFromGroup.HeaderText = _GlobalResources.RemoveUser_sFromGroup;
            this._RemoveUsersFromGroup.TargetControlID = targetControlId;
            this._RemoveUsersFromGroup.SubmitButton.Command += new CommandEventHandler(this._RemoveUsersFromGroupSubmit_Command);

            // build the modal body
            // build a container for the user listing
            this._RemoveEligibleUsers = new DynamicListBox("RemoveEligibleUsers");
            this._RemoveEligibleUsers.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._RemoveEligibleUsers.SearchButton.Command += new CommandEventHandler(this._SearchRemoveUsersButton_Command);
            this._RemoveEligibleUsers.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchRemoveUsersButton_Command);
            this._RemoveEligibleUsers.ListBoxControl.DataSource = this._RemoveEligibleUsersInGroup;
            this._RemoveEligibleUsers.ListBoxControl.DataTextField = "displayName";
            this._RemoveEligibleUsers.ListBoxControl.DataValueField = "idUser";
            this._RemoveEligibleUsers.ListBoxControl.DataBind();

            // add controls to body
            this._RemoveUsersFromGroup.AddControlToBody(this._RemoveEligibleUsers);

            // add modal to container
            this.GroupPropertiesContainer.Controls.Add(this._RemoveUsersFromGroup);
        }
        #endregion                

        #region _SearchAddUsersButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Add Users" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchAddUsersButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._AddUsersToGroup.ClearFeedback();

            // clear the listbox control
            this._AddEligibleUsers.ListBoxControl.Items.Clear();

            // do the search
            this._UsersNotInGroup = Asentia.UMS.Library.User.IdsAndNamesForGroupSelectList(this._GroupObject.Id, this._AddEligibleUsers.SearchTextBox.Text);

            // bind the data
            this._SelectAddUsersDataBind();
        }
        #endregion

        #region _ClearSearchAddUsersButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Add Users" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchAddUsersButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._AddUsersToGroup.ClearFeedback();

            // clear the listbox control and search text box
            this._AddEligibleUsers.ListBoxControl.Items.Clear();
            this._AddEligibleUsers.SearchTextBox.Text = "";

            // clear the search
            this._UsersNotInGroup = Asentia.UMS.Library.User.IdsAndNamesForGroupSelectList(this._GroupObject.Id, null);

            // bind the data
            this._SelectAddUsersDataBind();
        }
        #endregion

        #region _SelectAddUsersDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectAddUsersDataBind()
        {
            this._AddEligibleUsers.ListBoxControl.DataSource = this._UsersNotInGroup;
            this._AddEligibleUsers.ListBoxControl.DataTextField = "displayName";
            this._AddEligibleUsers.ListBoxControl.DataValueField = "idUser";
            this._AddEligibleUsers.ListBoxControl.DataBind();

            // if no records available then disable the submit button
            if (this._AddEligibleUsers.ListBoxControl.Items.Count == 0)
            {
                this._AddUsersToGroup.SubmitButton.Enabled = false;
                this._AddUsersToGroup.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._AddUsersToGroup.SubmitButton.Enabled = true;
                this._AddUsersToGroup.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _SearchRemoveUsersButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Remove Users" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchRemoveUsersButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._RemoveUsersFromGroup.ClearFeedback();

            // clear the listbox control
            this._RemoveEligibleUsers.ListBoxControl.Items.Clear();

            // do the search
            this._RemoveEligibleUsersInGroup = this._GroupObject.GetUsers(true, this._RemoveEligibleUsers.SearchTextBox.Text);

            // bind the data
            this._SelectRemoveEligibleUsersDataBind();
        }
        #endregion

        #region _ClearSearchRemoveUsersButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Remove Users" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchRemoveUsersButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._RemoveUsersFromGroup.ClearFeedback();

            // clear the listbox control and search text box
            this._RemoveEligibleUsers.ListBoxControl.Items.Clear();
            this._RemoveEligibleUsers.SearchTextBox.Text = "";

            // clear the search
            this._RemoveEligibleUsersInGroup = this._GroupObject.GetUsers(true, null);

            // bind the dats
            this._SelectRemoveEligibleUsersDataBind();
        }
        #endregion

        #region _SelectRemoveEligibleUsersDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectRemoveEligibleUsersDataBind()
        {
            this._RemoveEligibleUsers.ListBoxControl.DataSource = this._RemoveEligibleUsersInGroup;
            this._RemoveEligibleUsers.ListBoxControl.DataTextField = "displayName";
            this._RemoveEligibleUsers.ListBoxControl.DataValueField = "idUser";
            this._RemoveEligibleUsers.ListBoxControl.DataBind();

            // if no records available then disable the submit button
            if (this._RemoveEligibleUsers.ListBoxControl.Items.Count == 0)
            {
                this._RemoveUsersFromGroup.SubmitButton.Enabled = false;
                this._RemoveUsersFromGroup.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._RemoveUsersFromGroup.SubmitButton.Enabled = true;
                this._RemoveUsersFromGroup.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _AddUsersToGroupSubmit_Command
        /// <summary>
        /// Click event handler for Add Users To Group modal submit button.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _AddUsersToGroupSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get selected users from the list box
                List<string> selectedUsers = this._AddEligibleUsers.GetSelectedValues();

                // throw exception if no users selected
                if (selectedUsers.Count <= 0)
                { throw new AsentiaException(_GlobalResources.NoUsersSelected); }

                // add selected users to data table
                DataTable usersToAdd = new DataTable(); ;
                usersToAdd.Columns.Add("id", typeof(int));

                foreach (string selectedUser in selectedUsers)
                { usersToAdd.Rows.Add(Convert.ToInt32(selectedUser)); }

                // join the selected users to the group
                this._GroupObject.JoinUsers(usersToAdd);

                // remove the selected users from the list box so they cannot be re-selected
                this._AddEligibleUsers.RemoveSelectedItems();

                // rebuild the listing panel and update the update panel
                this._BuildGroupMembersListing();                
                this._AddUsersToGroup.ClearFeedback();
                UpdatePanel groupMembershipFormUpdatePanel = (UpdatePanel)this.GroupPropertiesTabPanelsContainer.FindControl("GroupMembershipFormUpdatePanel");
                groupMembershipFormUpdatePanel.Update();

                // display success feedback
                this._AddUsersToGroup.DisplayFeedback(_GlobalResources.User_sAddedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddUsersToGroup.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._AddUsersToGroup.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddUsersToGroup.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddUsersToGroup.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AddUsersToGroup.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _RemoveUsersFromGroupSubmit_Command
        /// <summary>
        /// Click event handler for Remove Users From Group modal submit button.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _RemoveUsersFromGroupSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get selected users from the list box
                List<string> selectedUsers = this._RemoveEligibleUsers.GetSelectedValues();

                // throw exception if no users selected
                if (selectedUsers.Count <= 0)
                { throw new AsentiaException(_GlobalResources.NoUsersSelected); }

                // add selected users to data table
                DataTable usersToRemove = new DataTable(); ;
                usersToRemove.Columns.Add("id", typeof(int));

                foreach (string selectedUser in selectedUsers)
                { usersToRemove.Rows.Add(Convert.ToInt32(selectedUser)); }

                // remove the selected users from the group
                this._GroupObject.RemoveUsers(usersToRemove);

                // remove the selected users from the list box so they cannot be re-selected
                this._RemoveEligibleUsers.RemoveSelectedItems();

                // rebuild the listing panel and update the update panel
                this._BuildGroupMembersListing();
                this._RemoveUsersFromGroup.ClearFeedback();
                UpdatePanel groupMembershipFormUpdatePanel = (UpdatePanel)this.GroupPropertiesTabPanelsContainer.FindControl("GroupMembershipFormUpdatePanel");
                groupMembershipFormUpdatePanel.Update();

                // display success feedback
                this._RemoveUsersFromGroup.DisplayFeedback(_GlobalResources.User_sRemovedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._RemoveUsersFromGroup.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._AddUsersToGroup.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._RemoveUsersFromGroup.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._RemoveUsersFromGroup.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._RemoveUsersFromGroup.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion
    }
}