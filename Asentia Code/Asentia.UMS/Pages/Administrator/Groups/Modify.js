﻿/* GROUP PROPERTIES */

function InitializeForm() {
    // if the form is view only, disable all inputs for it
    if (IsFormViewOnly) {
        $("#GroupProperties_Properties_TabPanel input").prop("disabled", true);
        $("#GroupProperties_Properties_TabPanel textarea").prop("disabled", true);
        $("#GroupProperties_Properties_TabPanel select").prop("disabled", true);
    }
}

//Page-specific tab change method
function GroupPropertiesTabChange(selectedTabId, tabContainerId) {
    switch (selectedTabId) {
        case "Properties":                    
            $("#GroupMembershipInstructionsPanel").hide();
            $("#GroupFormInstructionsPanel").show();
            
            $("#GroupSaveButton").show();
            $("#CancelButton").show();
            break;
        case "GroupMembers":            
            $("#GroupFormInstructionsPanel").hide();
            $("#GroupMembershipInstructionsPanel").show();

            $("#GroupSaveButton").hide();
            $("#CancelButton").hide();            
            break;        
        default:
            break;
    }
}

//Method to set group wall on
function GroupWallOn() {
    $("#IsGroupWallModerated_Field").prop("disabled", false);

    $("#MembershipIsPublicized_Field").prop("checked", true);
    $("#MembershipIsPublicized_Field").prop("disabled", true);
}

//Method to set group wall off
function GroupWallOff() {
    $("#IsGroupWallModerated_Field").prop("checked", false);
    $("#IsGroupWallModerated_Field").prop("disabled", true);

    $("#MembershipIsPublicized_Field").prop("checked", false);
    $("#MembershipIsPublicized_Field").prop("disabled", false);
}

//Method to set the is membership publicized status
function SetIsMembershipPublicizedStatus() {
    var isSelfJoinChecked = $("#IsSelfJoin_Field").is(":checked");
    var isWallOnChecked = $("#GroupWall_Field_0").is(":checked");    

    if (isSelfJoinChecked || isWallOnChecked) {
        $("#MembershipIsPublicized_Field").prop("checked", true);
        $("#MembershipIsPublicized_Field").prop("disabled", true);
    }
    else {
        $("#MembershipIsPublicized_Field").prop("checked", false);
        $("#MembershipIsPublicized_Field").prop("disabled", false);
    }
}

//Method to remove a group avatar
function DeleteGroupAvatar() {
    $("#GroupAvatar_Field_ImageContainer").remove();
    $("#ClearAvatar_Field").val("true");
}