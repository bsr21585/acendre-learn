﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages.Administrator.License
{
    public class ExecuteLicenseAgreement : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ExecuteLicenseAgreementFormContentWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel ExecuteLicenseAgreementFormWrapperContainer;
        public Panel ExecuteLicenseAgreementFormContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private Button _SaveButton = new Button();

        private CheckBox _ExecuteLicenseAgreementCheckBox;
        private HtmlGenericControl _LicenseAgreementHtml;
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {

            // bounce if the caller does not need to execute license agreement
            if (!AsentiaSessionState.MustExecuteLicenseAgreement)
            { Response.Redirect("~/dashboard/"); }

            this.ExecuteLicenseAgreementFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ExecuteLicenseAgreementFormWrapperContainer.CssClass = "FormContentContainer";

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string userImagePath;
            string imageCssClass = null;
            string pageTitle;

            breadCrumbPageTitle = _GlobalResources.LicenseAgreement;

            userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_LICENSE,
                                                    ImageFiles.EXT_PNG);

            pageTitle = _GlobalResources.LicenseAgreement;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, pageTitle, userImagePath, imageCssClass);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.YouMustAgreeToTheFollowingLicenseAgreementBeforeYouMayProceed, true);

            // build the change password form
            this._BuildExecuteLicenseAgreementForm();

            // build the form actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _BuildExecuteLicenseAgreementForm
        /// <summary>
        /// Builds the change password form.
        /// </summary>
        private void _BuildExecuteLicenseAgreementForm()
        {
            this.ExecuteLicenseAgreementFormContainer.Controls.Clear();

            this._LicenseAgreementHtml = new HtmlGenericControl();
            this._LicenseAgreementHtml.ID = "LicenseAgreementHtml_Field";

            if (File.Exists(Server.MapPath(@"~\_config\" + AsentiaSessionState.SiteHostname + @"\License.html")))
            { this._LicenseAgreementHtml.InnerHtml = File.ReadAllText(Server.MapPath(@"~\_config\" + AsentiaSessionState.SiteHostname + @"\License.html")); }

            this.ExecuteLicenseAgreementFormContainer.Controls.Add(AsentiaPage.BuildFormField("LicenseAgreementHtml",
                                                                    null,
                                                                    this._LicenseAgreementHtml.ID,
                                                                    this._LicenseAgreementHtml,
                                                                    false,
                                                                    false,
                                                                    false));

            this._ExecuteLicenseAgreementCheckBox = new CheckBox();
            this._ExecuteLicenseAgreementCheckBox.ID = "ExecuteLicenseAgreementCheckBox_Field";
            this._ExecuteLicenseAgreementCheckBox.Text = _GlobalResources.IAgreeToTheTermsOfTheLicenseAgreement;

            this.ExecuteLicenseAgreementFormContainer.Controls.Add(AsentiaPage.BuildFormField("ExecuteLicenseAgreementCheckBox",
                                                                    null,
                                                                    this._ExecuteLicenseAgreementCheckBox.ID,
                                                                    this._ExecuteLicenseAgreementCheckBox,
                                                                    true,
                                                                    true,
                                                                    false));
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";
            this._SaveButton.Text = _GlobalResources.ExecuteLicenseAgreement;

            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);
        }
        #endregion

        #region _ValidateExecuteLicenseAgreementForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateExecuteLicenseAgreementForm()
        {
            bool isValid = true;

            // validate required
            if (!this._ExecuteLicenseAgreementCheckBox.Checked)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.ExecuteLicenseAgreementFormContainer, "ExecuteLicenseAgreementCheckBox", _GlobalResources.PleaseCheckTheBoxToAgreeToTheTermsOfTheLicenseAgreement);
            }

            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateExecuteLicenseAgreementForm())
                { throw new AsentiaException(); }

                // save dtExecuted to the Site table
                AsentiaSite site = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite);
                DateTime dtExecuted = DateTime.UtcNow;
                site.DtLicenseAgreementExecuted = dtExecuted;

                site.Save(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser);

                // generate a guid 
                Guid executeLicenseAgreementGUID = Guid.NewGuid();

                // get the user's ip address
                string clientIPAddress = String.Empty;

                if (!String.IsNullOrWhiteSpace(Request.ServerVariables["HTTP_X_FORWARDED_FOR"])) // client's ip address if application is behind load balancer
                { clientIPAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"]; }
                else
                { clientIPAddress = Request.UserHostAddress; }

                // create the license executed record
                site.SaveExecutedLicenseAgreement(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, dtExecuted, executeLicenseAgreementGUID, clientIPAddress);

                // copy the license agreement and rename it with the guid
                File.Copy(Server.MapPath(@"~\_config\" + AsentiaSessionState.SiteHostname + @"\License.html"), Server.MapPath(@"~\_config\" + AsentiaSessionState.SiteHostname + @"\ExecutedLicense_" + executeLicenseAgreementGUID.ToString() + ".html"));

                // free up the must change password flag
                AsentiaSessionState.MustExecuteLicenseAgreement = false;

                // disable the "execute license aggreement" button
                this._SaveButton.Enabled = false;

                // display the saved feedback and register script to redirect on click of feedback ok button
                this.DisplayFeedback(_GlobalResources.TheLicenseAgreementHasBeenExecutedSuccessfullyClickOKToGoToYourDashboard, false);
                ScriptManager.RegisterStartupScript(this.ExecuteLicenseAgreementFormContainer, typeof(ExecuteLicenseAgreement), "RedirectToDashboard", "$(\"#FeedbackInformationStatusPanelCloseButton\").click(function () { window.location.href = \"/dashboard\"; });", true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseFieldConstraintException fceEx)
            {
                // display the failure message
                this.DisplayFeedback(fceEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion
    }
}
