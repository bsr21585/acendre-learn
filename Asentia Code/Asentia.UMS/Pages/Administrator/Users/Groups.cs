﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using Asentia.UMS.Controls;
using System.Web.UI;

namespace Asentia.UMS.Pages.Administrator.Users
{
    public class Groups : AsentiaAuthenticatedPage
    {        
        #region Properties
        public Panel GroupsFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel GroupsPageWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel UserGroupsMembershipContainer;        
        #endregion

        #region Private Properties
        private User _UserObject;
        
        private ModalPopup _AddGroupsToUser;
        private ModalPopup _RemoveGroupsFromUser;

        private DataTable _GroupsForUser;
        private DataTable _RemoveEligibleGroupsForUser;
        private DataTable _GroupsNotForUser;

        private DynamicListBox _AddEligibleGroups;
        private DynamicListBox _RemoveEligibleGroups;

        private UpdatePanel _GroupListingUpdatePanel;
        #endregion        

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the user object
            this._GetUserObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/users/Groups.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();            

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.GroupsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.GroupsPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu
            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Groups;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // build the group membership panel
            this._BuildGroupMembershipPanel();
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on querystring if exists, redirects if not.
        /// </summary>
        private void _GetUserObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("uid", 0);

            if (qsId > 0)
            {
                try
                { this._UserObject = new User(qsId); }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            { Response.Redirect("~/administrator/users"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {            
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;            

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }                
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(userDisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, _GlobalResources.Groups, ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion

        #region _BuildGroupMembershipPanel
        /// <summary>
        /// Builds the panel that contains group membership information.
        /// </summary>
        private void _BuildGroupMembershipPanel()
        {
            // populate datatables for group selection
            this._GroupsForUser = this._UserObject.GetGroups(false, null);
            this._RemoveEligibleGroupsForUser = this._UserObject.GetGroups(true, null);
            this._GroupsNotForUser = Asentia.UMS.Library.Group.IdsAndNamesForUserSelectList(this._UserObject.Id, null);            

            // clear controls from container
            this.UserGroupsMembershipContainer.Controls.Clear();

            this._GroupListingUpdatePanel = new UpdatePanel();
            this._GroupListingUpdatePanel.ID = "GroupListingUpdatePanel";
            this._GroupListingUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;            

            // build a container for the group membership count
            Panel groupMembershipCountContainer = new Panel();
            groupMembershipCountContainer.ID = "UserGroupsMembershipCountContainer";

            HtmlGenericControl groupMembershipCountP = new HtmlGenericControl("p");
            groupMembershipCountP.InnerText = String.Format(_GlobalResources.ThisUserIsAMemberOfXGroups, this._GroupsForUser.Rows.Count.ToString());

            groupMembershipCountContainer.Controls.Add(groupMembershipCountP);
            
            // build a container for the group membership listing
            Panel groupListContainer = new Panel();
            groupListContainer.ID = "UserGroupsListingContainer";
            groupListContainer.CssClass = "ItemListingContainer";

            // loop through the datatable and add each member to the listing container
            foreach (DataRow row in this._GroupsForUser.Rows)
            {
                Panel groupNameContainer = new Panel();
                groupNameContainer.ID = "Group_" + row["idGroup"].ToString();

                Literal groupName = new Literal();
                groupName.Text = row["name"].ToString();

                if (Convert.ToInt32(row["numRulesJoinedBy"]) > 0)
                { groupName.Text += " - " + String.Format(_GlobalResources.AutoJoinedByNumRules, row["numRulesJoinedBy"].ToString()); }

                groupNameContainer.Controls.Add(groupName);
                groupListContainer.Controls.Add(groupNameContainer);
            }

            // attach the user membership listing to the update panel
            this._GroupListingUpdatePanel.ContentTemplateContainer.Controls.Add(groupMembershipCountContainer);
            this._GroupListingUpdatePanel.ContentTemplateContainer.Controls.Add(groupListContainer);

            // attach update panel to container
            this.UserGroupsMembershipContainer.Controls.Add(this._GroupListingUpdatePanel);

            // ADD/REMOVE USERS BUTTONS
            Panel buttonsPanel = new Panel();
            buttonsPanel.ID = "UserGroupsButtonsContainer";

            // add groups to user link
            Image addGroupsToUserImageForLink = new Image();
            addGroupsToUserImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            addGroupsToUserImageForLink.CssClass = "MediumIcon";

            Localize addGroupsToUserTextForLink = new Localize();
            addGroupsToUserTextForLink.Text = _GlobalResources.AddGroup_s;

            LinkButton addGroupsToUserLink = new LinkButton();
            addGroupsToUserLink.ID = "LaunchAddGroupsToUserModal";
            addGroupsToUserLink.CssClass = "ImageLink";
            addGroupsToUserLink.Controls.Add(addGroupsToUserImageForLink);
            addGroupsToUserLink.Controls.Add(addGroupsToUserTextForLink);
            buttonsPanel.Controls.Add(addGroupsToUserLink);

            // remove groups from user link
            Image removeGroupsFromUserImageForLink = new Image();
            removeGroupsFromUserImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            removeGroupsFromUserImageForLink.CssClass = "MediumIcon";

            Localize removeGroupsFromUserTextForLink = new Localize();
            removeGroupsFromUserTextForLink.Text = _GlobalResources.RemoveGroup_s;

            LinkButton removeGroupsFromUserLink = new LinkButton();
            removeGroupsFromUserLink.ID = "LaunchRemoveGroupsFromUserModal";
            removeGroupsFromUserLink.CssClass = "ImageLink";
            removeGroupsFromUserLink.Controls.Add(removeGroupsFromUserImageForLink);
            removeGroupsFromUserLink.Controls.Add(removeGroupsFromUserTextForLink);
            buttonsPanel.Controls.Add(removeGroupsFromUserLink);

            // attach the buttons panel to the container
            this.UserGroupsMembershipContainer.Controls.Add(buttonsPanel);

            // build modals for adding and removing users to/from the groups
            this._BuildAddGroupsToUserModal(addGroupsToUserLink.ID);
            this._BuildRemoveGroupsFromUserModal(removeGroupsFromUserLink.ID);
        }
        #endregion

        #region _BuildAddGroupsToUserModal
        /// <summary>
        /// Builds the modal for adding user to groups.
        /// </summary>
        private void _BuildAddGroupsToUserModal(string targetControlId)
        {
            // set modal properties
            this._AddGroupsToUser = new ModalPopup("AddGroupsToUserModal");
            this._AddGroupsToUser.Type = ModalPopupType.Form;
            this._AddGroupsToUser.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            this._AddGroupsToUser.HeaderIconAlt = _GlobalResources.AddGroup_sToUser;
            this._AddGroupsToUser.HeaderText = _GlobalResources.AddGroup_sToUser;
            this._AddGroupsToUser.TargetControlID = targetControlId;
            this._AddGroupsToUser.SubmitButton.Command += new CommandEventHandler(this._AddGroupsToUserSubmit_Command);
            this._AddGroupsToUser.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the groups listing
            this._AddEligibleGroups = new DynamicListBox("AddEligibleGroups");
            this._AddEligibleGroups.NoRecordsFoundMessage = _GlobalResources.NoGroupsFound;
            this._AddEligibleGroups.SearchButton.Command += new CommandEventHandler(this._SearchAddGroupsButton_Command);
            this._AddEligibleGroups.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchAddGroupsButton_Command);
            this._AddEligibleGroups.ListBoxControl.DataSource = this._GroupsNotForUser;
            this._AddEligibleGroups.ListBoxControl.DataTextField = "name";
            this._AddEligibleGroups.ListBoxControl.DataValueField = "idGroup";
            this._AddEligibleGroups.ListBoxControl.DataBind();

            // add controls to body
            this._AddGroupsToUser.AddControlToBody(this._AddEligibleGroups);

            // add modal to container
            this.UserGroupsMembershipContainer.Controls.Add(this._AddGroupsToUser);
        }
        #endregion

        #region _BuildRemoveGroupsFromUserModal
        /// <summary>
        /// Builds the modal for removing the user from groups.
        /// </summary>
        private void _BuildRemoveGroupsFromUserModal(string targetControlId)
        {
            // set modal properties
            this._RemoveGroupsFromUser = new ModalPopup("RemoveGroupsFromUserModal");
            this._RemoveGroupsFromUser.Type = ModalPopupType.Form;
            this._RemoveGroupsFromUser.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            this._RemoveGroupsFromUser.HeaderIconAlt = _GlobalResources.RemoveGroup_sFromUser;
            this._RemoveGroupsFromUser.HeaderText = _GlobalResources.RemoveGroup_sFromUser;
            this._RemoveGroupsFromUser.TargetControlID = targetControlId;
            this._RemoveGroupsFromUser.SubmitButton.Command += new CommandEventHandler(this._RemoveGroupsFromUserSubmit_Command);
            this._RemoveGroupsFromUser.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the groups listing
            this._RemoveEligibleGroups = new DynamicListBox("RemoveEligibleGroups");
            this._RemoveEligibleGroups.NoRecordsFoundMessage = _GlobalResources.NoGroupsFound;
            this._RemoveEligibleGroups.SearchButton.Command += new CommandEventHandler(this._SearchRemoveGroupsButton_Command);
            this._RemoveEligibleGroups.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchRemoveGroupsButton_Command);
            this._RemoveEligibleGroups.ListBoxControl.DataSource = this._RemoveEligibleGroupsForUser;
            this._RemoveEligibleGroups.ListBoxControl.DataTextField = "name";
            this._RemoveEligibleGroups.ListBoxControl.DataValueField = "idGroup";
            this._RemoveEligibleGroups.ListBoxControl.DataBind();

            // add controls to body
            this._RemoveGroupsFromUser.AddControlToBody(this._RemoveEligibleGroups);

            // add modal to container
            this.UserGroupsMembershipContainer.Controls.Add(this._RemoveGroupsFromUser);
        }
        #endregion

        #region _SearchAddGroupsButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Add Groups" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchAddGroupsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._AddGroupsToUser.ClearFeedback();

            // clear the listbox control
            this._AddEligibleGroups.ListBoxControl.Items.Clear();

            // do the search
            this._GroupsNotForUser = Asentia.UMS.Library.Group.IdsAndNamesForUserSelectList(this._UserObject.Id, this._AddEligibleGroups.SearchTextBox.Text);

            this._SelectAddEligibleGroupsDataBind();
        }
        #endregion

        #region _ClearSearchAddGroupsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Add Groups" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchAddGroupsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._AddGroupsToUser.ClearFeedback();

            // clear the listbox control and search text box
            this._AddEligibleGroups.ListBoxControl.Items.Clear();
            this._AddEligibleGroups.SearchTextBox.Text = "";

            // clear the search
            this._GroupsNotForUser = Asentia.UMS.Library.Group.IdsAndNamesForUserSelectList(this._UserObject.Id, this._AddEligibleGroups.SearchTextBox.Text);

            this._SelectAddEligibleGroupsDataBind();
        }
        #endregion

        #region _SelectAddEligibleGroupsDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectAddEligibleGroupsDataBind()
        {
            this._AddEligibleGroups.ListBoxControl.DataSource = this._GroupsNotForUser;
            this._AddEligibleGroups.ListBoxControl.DataTextField = "name";
            this._AddEligibleGroups.ListBoxControl.DataValueField = "idGroup";
            this._AddEligibleGroups.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._AddEligibleGroups.ListBoxControl.Items.Count == 0)
            {
                this._AddGroupsToUser.SubmitButton.Enabled = false;
                this._AddGroupsToUser.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._AddGroupsToUser.SubmitButton.Enabled = true;
                this._AddGroupsToUser.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _SearchRemoveGroupsButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Remove Groups" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchRemoveGroupsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._RemoveGroupsFromUser.ClearFeedback();

            // clear the listbox control
            this._RemoveEligibleGroups.ListBoxControl.Items.Clear();

            // do the search
            this._RemoveEligibleGroupsForUser = this._UserObject.GetGroups(true, this._RemoveEligibleGroups.SearchTextBox.Text);

            this._SelectRemoveEligibleGroupsDataBind();
        }
        #endregion

        #region _ClearSearchRemoveGroupsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Remove Groups" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchRemoveGroupsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._RemoveGroupsFromUser.ClearFeedback();

            // clear the listbox control and search text box
            this._RemoveEligibleGroups.ListBoxControl.Items.Clear();
            this._RemoveEligibleGroups.SearchTextBox.Text = "";

            // clear the search
            this._RemoveEligibleGroupsForUser = this._UserObject.GetGroups(true, null);

            this._SelectRemoveEligibleGroupsDataBind();
        }
        #endregion

        #region _SelectRemoveEligibleGroupsDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectRemoveEligibleGroupsDataBind()
        {
            this._RemoveEligibleGroups.ListBoxControl.DataSource = this._RemoveEligibleGroupsForUser;
            this._RemoveEligibleGroups.ListBoxControl.DataTextField = "name";
            this._RemoveEligibleGroups.ListBoxControl.DataValueField = "idGroup";
            this._RemoveEligibleGroups.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._RemoveEligibleGroups.ListBoxControl.Items.Count == 0)
            {
                this._RemoveGroupsFromUser.SubmitButton.Enabled = false;
                this._RemoveGroupsFromUser.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._RemoveGroupsFromUser.SubmitButton.Enabled = true;
                this._RemoveGroupsFromUser.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _AddGroupsToUserSubmit_Command
        /// <summary>
        /// Click event handler for Add Groups To User modal submit button.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _AddGroupsToUserSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get selected groups from the list box
                List<string> selectedGroups = this._AddEligibleGroups.GetSelectedValues();

                // throw exception if no users selected
                if (selectedGroups.Count <= 0)
                { throw new AsentiaException(_GlobalResources.NoGroupsSelected); }

                // add selected users to data table
                DataTable groupsToAdd = new DataTable(); ;
                groupsToAdd.Columns.Add("id", typeof(int));

                foreach (string selectedGroup in selectedGroups)
                { groupsToAdd.Rows.Add(Convert.ToInt32(selectedGroup)); }

                // join the selected groups to the user
                this._UserObject.JoinGroups(groupsToAdd);

                // remove the selected groups from the list box so they cannot be re-selected
                this._AddEligibleGroups.RemoveSelectedItems();

                //Rebind controls and refresh the update panel
                this._BuildGroupMembershipPanel();
                this._GroupListingUpdatePanel.Update();

                // display success feedback
                this._AddGroupsToUser.DisplayFeedback(_GlobalResources.Group_sAddedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddGroupsToUser.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._AddGroupsToUser.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddGroupsToUser.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddGroupsToUser.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AddGroupsToUser.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _RemoveGroupsFromUserSubmit_Command
        /// <summary>
        /// Click event handler for Remove Groups From User modal submit button.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _RemoveGroupsFromUserSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get selected groups from the list box
                List<string> selectedGroups = this._RemoveEligibleGroups.GetSelectedValues();

                // throw exception if no users selected
                if (selectedGroups.Count <= 0)
                { throw new AsentiaException(_GlobalResources.NoGroupsSelected); }

                // add selected groups to data table
                DataTable groupsToRemove = new DataTable(); ;
                groupsToRemove.Columns.Add("id", typeof(int));

                foreach (string selectedGroup in selectedGroups)
                { groupsToRemove.Rows.Add(Convert.ToInt32(selectedGroup)); }

                // remove the selected groups from the user
                this._UserObject.RemoveGroups(groupsToRemove);

                // remove the selected groups from the list box so they cannot be re-selected
                this._RemoveEligibleGroups.RemoveSelectedItems();

                //Rebind controls and refresh the update panel
                this._BuildGroupMembershipPanel();
                this._GroupListingUpdatePanel.Update();

                // display success feedback
                this._RemoveGroupsFromUser.DisplayFeedback(_GlobalResources.Group_sRemovedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._RemoveGroupsFromUser.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._RemoveGroupsFromUser.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._RemoveGroupsFromUser.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._RemoveGroupsFromUser.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._RemoveGroupsFromUser.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion
    }
}
