﻿/* TODO: ALL THIS BELOW NEEDS TO BE CLEANED UP */

$(document).ready(function () {
    $('#UserBatchFormUpdatePanel').addClass('TabPanelContainer');
});

$('#UserFormUpdatePanel').width($('#PageContentContainer').width() - 30);

$('body').keydown(function (e) {
     if (e.keyCode == 39) { 
         idNum++;
         $('#ErrorPosition' + idNum).focus(); 
     } else if (e.keyCode == 37) { 
         if (idNum>1) idNum--; 
         $('#ErrorPosition' + idNum).focus(); 
     }
});

/* END CLEAN UP AREA */

var idNum = 0;

function PreviousError() {
    if (idNum>1) idNum--; 
    $('#ErrorPosition' + idNum).focus(); 
}

function NextError() {
    idNum++;
    $('#ErrorPosition' + idNum).focus(); 
}

function InitializeUploadBatchModal(hideFeedbackPanel) {
    // reset the feedback panel
    if (hideFeedbackPanel) {
        $("#BatchFileModalModalPopupFeedbackContainer").hide();
    }

    // reset the uploader
    $("#BatchUploader_CompletedPanel").html("");
    $("#BatchFile_ErrorContainer").html("");
    $("#BatchUploader_UploadControl input").attr("style", "");
    $("#BatchUploader_UploadHiddenFieldOriginalFileName").val("");
    $("#BatchUploader_UploadHiddenField").val("");
    $("#BatchUploader_UploadHiddenFieldFileSize").val("");

    // reset the new users checkbox
    $("#NewUsersOnly").prop("checked", false);
}