﻿// function for clicking next button in user account merge tool
function ClickNextButton() {
   
    if ($('#SelectedSourceUser_Field_Container').css("display") == "none") {
        
        // set source user id and name on the page
        $('#IdUserMergeSource').val($('#SelectSourceUserListBox').val() + '|' + $('#SelectSourceUserListBox option:selected').text());

        // bind destination list
        $('#HiddenDestinationBindButton').click();

    } else if ($('#SelectedDestinationUser_Field_Container').css("display") == "none") {
        
        // destination account has been chosen - update modal to prepare for merge
        $('#SelectedDestinationUser_Field_InputContainer').text($('#SelectDestinationUserListBox option:selected').text());
        $('#SelectedDestinationUser_Field_Container').css("display", "");
        $('#IdUserMergeDestination').val($('#SelectDestinationUserListBox').val() + '|' + $('#SelectDestinationUserListBox option:selected').text());
        $('#DestinationSelectUser_Field_Container').css("display", "none");
        $('#MergeAccountsSubmitButton').val($('#HiddenMergeButton').val());

    } else {
        
        // merge button is clicked - merge accounts
        $('#MergeAccountsSubmitButton').attr("disabled", "disabled");
        $('#MergeAccountsSubmitButton').addClass("DisabledButton");
        $('#HiddenMergeButton').click();

    }
}

// function used to enable / disable "next" button when a user account is selected / deselected
function SelectUserAccount() {
    
    if ($('#SelectSourceUserListBox option:selected').text() != '') {
        $('#MergeAccountsSubmitButton').removeAttr("disabled");
        $('#MergeAccountsSubmitButton').removeClass("DisabledButton");
    } else {
        $('#MergeAccountsSubmitButton').attr("disabled", "disabled");
        $('#MergeAccountsSubmitButton').addClass("DisabledButton");
    }
}

// function for clearing merge accounts modal
function ClearMergeAccountsModal() {

    $('#IdUserMergeSource').val('0|null');
    $('#IdUserMergeDestination').val('0|null');

    $("#MergeUserAccountsModalFeedbackInformationStatusPanelMessagingContainer").css("display", "none");

    $('#SelectSourceUserListBox').val([]);
    $('#SelectDestinationUserListBox').val([]);
    
    $('#SelectSourceUserSearchTextBox').val("");
    $('#SelectDestinationUserSearchTextBox').val("");

    $('#HiddenSourceBindButton').click();
    
}