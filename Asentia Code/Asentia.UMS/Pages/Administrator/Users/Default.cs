﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Services;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.UMS.Pages.Administrator.Users
{    
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel UsersFormContentWrapperContainer;
        public Panel GridAnalyticPanel;

        public UpdatePanel UserGridUpdatePanel;
        public Grid UserGrid;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmAction;

        /* MERGE USERS CONTROLS */

        private ModalPopup _MergeUserAccountsModal;
        private LinkButton _MergeUserAccountsLinkButton;

        private DynamicListBox _SelectSourceUser;
        private DynamicListBox _SelectDestinationUser;

        private HiddenField _IdUserMergeSource;
        private HiddenField _IdUserMergeDestination;

        private Button _HiddenMergeButton;
        private Button _HiddenSourceBindButton;
        private Button _HiddenDestinationBindButton;

        private DataTable _EligibleUsersForSelectList;
        private Panel _InstructionsPanel;
        private Panel _SourceUserPanel;
        private Panel _DestinationUserPanel;
        private Panel _SelectedSourceUserPanel;
        private Panel _SelectedDestinationUserPanel;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.UMS.Pages.Administrator.Users.Default.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserCreator)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserDeleter)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserEditor)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserImpersonator)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("GridAnalytic.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/users/Default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, _GlobalResources.Users, ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.UsersFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the grid, actions panel, and modals
            this._BuildObjectOptionsPanel();
            this._BuildGridAnalytics();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // only build the user merge modal if the calling user has necessary permissions
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERACCOUNTMERGING_ENABLE)
                && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserDeleter)
                && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserEditor)
                && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager)
               )
            {
                this._BuildMergeUserAccountsModal();
            }

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.UserGrid.BindData();
            }
        }
        #endregion        

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ensure user has permissions to create users before showing links
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserCreator))
            {
                // ADD USER
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("AddUserLink",
                                                    null,
                                                    "Modify.aspx",
                                                    null,
                                                    _GlobalResources.NewUser,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                    );

                // USER BATCH
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("UserBatchLink",
                                                    null,
                                                    "BatchUpload.aspx",
                                                    null,
                                                    _GlobalResources.UserBatch,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                    );
            }

            // ensure user has permission to delete, edit, and manage users before showing the merge user accounts button
            // (all those functions occur in the merge process)                    
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERACCOUNTMERGING_ENABLE)
                && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserDeleter)
                && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserEditor)
                && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager)
               )
            {
                this._MergeUserAccountsLinkButton = new LinkButton();
                this._MergeUserAccountsLinkButton.ID = "MergeUserAccounts_LinkButton";
                this._MergeUserAccountsLinkButton.OnClientClick = "ClearMergeAccountsModal(); return false;";
                this._MergeUserAccountsLinkButton.Visible = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERACCOUNTMERGING_ENABLE);

                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("MergeUserAccountsLink",
                                                    this._MergeUserAccountsLinkButton,
                                                    null,
                                                    null,
                                                    _GlobalResources.MergeUserAccounts,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_MERGE, ImageFiles.EXT_PNG))
                    );
            }

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGridAnalytics
        /// <summary>
        /// Builds the grid analytics for the page.
        /// </summary>
        private void _BuildGridAnalytics()
        {            
            // get the analytic data
            DataTable analyticData = GridAnalyticData.Users();
            
            int total = Convert.ToInt32(analyticData.Rows[0]["total"]);
            int active = Convert.ToInt32(analyticData.Rows[0]["active"]);
            int inactive = Convert.ToInt32(analyticData.Rows[0]["inactive"]);
            int online = Convert.ToInt32(analyticData.Rows[0]["online"]);
            int createdThisWeek = Convert.ToInt32(analyticData.Rows[0]["createdThisWeek"]);
            int createdThisMonth = Convert.ToInt32(analyticData.Rows[0]["createdThisMonth"]);
            int createdThisYear = Convert.ToInt32(analyticData.Rows[0]["createdThisYear"]);

            // build title for column 1
            GridAnalytic.DataBlock column1Title = new GridAnalytic.DataBlock(total.ToString("N0"), _GlobalResources.Users);

            // build a DataBlock list for the column 1 data
            List<GridAnalytic.DataBlock> dataBlocksColumn1 = new List<GridAnalytic.DataBlock>();
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(active.ToString("N0"), _GlobalResources.Active.ToLower()));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(inactive.ToString("N0"), _GlobalResources.Inactive.ToLower()));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(online.ToString("N0"), _GlobalResources.Online.ToLower()));

            // build title for column 2
            GridAnalytic.DataBlock column2Title = new GridAnalytic.DataBlock(null, _GlobalResources.NewUsers);

            // build a DataBlock list for the column 2 data
            List<GridAnalytic.DataBlock> dataBlocksColumn2 = new List<GridAnalytic.DataBlock>();
            dataBlocksColumn2.Add(new GridAnalytic.DataBlock(createdThisWeek.ToString("N0"), _GlobalResources.thisweek_lower.ToLower()));
            dataBlocksColumn2.Add(new GridAnalytic.DataBlock(createdThisMonth.ToString("N0"), _GlobalResources.thismonth_lower.ToLower()));
            dataBlocksColumn2.Add(new GridAnalytic.DataBlock(createdThisYear.ToString("N0"), _GlobalResources.thisyear_lower.ToLower()));

            // build the GridAnalytic object
            GridAnalytic gridAnalyticObject = new GridAnalytic("UserStatistics", column1Title, dataBlocksColumn1, column2Title, dataBlocksColumn2);
            
            // attach the object to the container
            this.GridAnalyticPanel.Controls.Add(gridAnalyticObject);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // apply css class to container
            this.UserGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.UserGrid.StoredProcedure = Library.User.GridProcedure;
            this.UserGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.UserGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.UserGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.UserGrid.IdentifierField = "idUser";
            this.UserGrid.DefaultSortColumn = "displayName";
            this.UserGrid.SearchBoxPlaceholderText = _GlobalResources.SearchUsers;

            // data key names
            this.UserGrid.DataKeyNames = new string[] { "idUser", "email" };

            // columns
            GridColumn nameUsernameEmail = new GridColumn(_GlobalResources.Name + " (" + _GlobalResources.Username + "), " + _GlobalResources.Email + "", null, "displayName"); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.UserGrid.AddColumn(nameUsernameEmail);
            this.UserGrid.AddColumn(options);
            
            // add row data bound event
            this.UserGrid.RowDataBound += new GridViewRowEventHandler(this._UserGrid_RowDataBound);
        }
        #endregion

        #region _UserGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _UserGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idUser = Convert.ToInt32(rowView["idUser"]);

                // get the user's group memberships from the data row
                string groupMembershipsString = rowView["groupMemberships"].ToString();
                List<int> groupMembershipsList = null;

                if (!String.IsNullOrWhiteSpace(groupMembershipsString))
                {
                    string[] groupMembershipsArray = groupMembershipsString.Split(',');
                    groupMembershipsList = new List<int>();

                    int idGroup;
                    foreach (string str in groupMembershipsArray)
                    {
                        if (Int32.TryParse(str, out idGroup))
                        { groupMembershipsList.Add(idGroup); }
                    }
                }

                // CHECK DELETE PERMISSIONS

                // default the delete checkbox to false
                string rowOrdinal = e.Row.DataItemIndex.ToString();
                CheckBox rowCheckBox = (CheckBox)e.Row.Cells[0].FindControl(this.UserGrid.ID + "_GridSelectRecord_" + rowOrdinal);

                if (rowCheckBox != null)
                { rowCheckBox.Enabled = false; }

                // check the permission and if permitted, grant
                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserDeleter, groupMembershipsList))
                {
                    if (rowCheckBox != null)
                    { rowCheckBox.Enabled = true; }
                }

                // AVATAR, NAME, (USERNAME), EMAIL COLUMN

                string avatar = rowView["avatar"].ToString();
                string displayName = Server.HtmlEncode(rowView["displayName"].ToString());
                string username = Server.HtmlEncode(rowView["username"].ToString());
                string email = Server.HtmlEncode(rowView["email"].ToString());
                bool isActive = Convert.ToBoolean(rowView["isActive"]);
                bool isOnline = Convert.ToBoolean(rowView["isOnline"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                if (!String.IsNullOrWhiteSpace(avatar))
                { 
                    avatarImagePath = SitePathConstants.SITE_USERS_ROOT + idUser.ToString() + "/" + avatar; 
                    avatarImageClass += " AvatarImage";
                }

                if (!isActive)
                { avatarImageClass += " DimIcon"; }

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = displayName;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name - check permissions for modify link
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, groupMembershipsList))
                {                    
                    HyperLink nameLink = new HyperLink();
                    nameLink.NavigateUrl = "Dashboard.aspx?id=" + idUser.ToString();
                    nameLink.Text = displayName;
                    nameLabel.Controls.Add(nameLink);
                }
                else
                {
                    Literal nameLit = new Literal();
                    nameLit.Text = displayName;
                    nameLabel.Controls.Add(nameLit);
                }

                // username
                Label usernameLabel = new Label();
                usernameLabel.CssClass = "GridSecondaryTitle";
                usernameLabel.Text = "(" + username + ")";
                e.Row.Cells[1].Controls.Add(usernameLabel);

                // online indicator
                Label onlineIndicator = new Label();

                if (isOnline)
                {
                    onlineIndicator.CssClass = "GridOnlineIndicator UserOnline";
                    onlineIndicator.ToolTip = _GlobalResources.Online;
                }
                else
                {
                    onlineIndicator.CssClass = "GridOnlineIndicator UserOffline";
                    onlineIndicator.ToolTip = _GlobalResources.Offline;
                }

                e.Row.Cells[1].Controls.Add(onlineIndicator);

                // email
                if (!String.IsNullOrWhiteSpace(email))
                {
                    Label emailLabel = new Label();
                    emailLabel.CssClass = "GridSecondaryLine";
                    e.Row.Cells[1].Controls.Add(emailLabel);

                    HyperLink emailLink = new HyperLink();
                    emailLink.NavigateUrl = "mailto:" + email;
                    emailLink.Text = email;
                    emailLabel.Controls.Add(emailLink);
                }

                // OPTIONS COLUMN 

                // modify requres editor, manager, or role manager permission
                if (
                    AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserEditor, groupMembershipsList)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, groupMembershipsList)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager)
                   )
                {
                    // modify
                    Label modifySpan = new Label();
                    modifySpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(modifySpan);

                    HyperLink modifyLink = new HyperLink();
                    modifyLink.NavigateUrl = "Modify.aspx?id=" + idUser.ToString();
                    modifySpan.Controls.Add(modifyLink);

                    Image modifyIcon = new Image();
                    modifyIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
                    modifyIcon.AlternateText = _GlobalResources.Modify;
                    modifyIcon.ToolTip = _GlobalResources.UserProfile;
                    modifyLink.Controls.Add(modifyIcon);
                }

                // impersonate
                Label impersonateSpan = new Label();
                impersonateSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(impersonateSpan);

                if (
                    AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserImpersonator, groupMembershipsList)
                    && idUser != AsentiaSessionState.IdSiteUser
                   )
                {
                    HyperLink impersonateLink = new HyperLink();
                    impersonateLink.NavigateUrl = "Impersonate.aspx?uid=" + idUser.ToString();
                    impersonateSpan.Controls.Add(impersonateLink);

                    Image impersonateIcon = new Image();
                    impersonateIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN_IMPERSONATE, ImageFiles.EXT_PNG);
                    impersonateIcon.AlternateText = _GlobalResources.Jump;
                    impersonateIcon.ToolTip = _GlobalResources.Impersonate;
                    impersonateLink.Controls.Add(impersonateIcon);
                }
                else
                {
                    Image impersonateIcon = new Image();
                    impersonateIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN_IMPERSONATE, ImageFiles.EXT_PNG);
                    impersonateIcon.AlternateText = _GlobalResources.JumpDisabled;
                    impersonateIcon.CssClass = "DimIcon";
                    impersonateSpan.Controls.Add(impersonateIcon);                    
                }

                // groups
                Label groupsSpan = new Label();
                groupsSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(groupsSpan);

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, groupMembershipsList))
                {
                    HyperLink groupsLink = new HyperLink();
                    groupsLink.NavigateUrl = "Groups.aspx?uid=" + idUser.ToString();
                    groupsSpan.Controls.Add(groupsLink);

                    Image groupsIcon = new Image();
                    groupsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP_BUTTON, ImageFiles.EXT_PNG);
                    groupsIcon.AlternateText = _GlobalResources.Groups;
                    groupsIcon.ToolTip = _GlobalResources.Groups;
                    groupsLink.Controls.Add(groupsIcon);
                }
                else
                {
                    Image groupsIcon = new Image();
                    groupsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP_BUTTON, ImageFiles.EXT_PNG);
                    groupsIcon.AlternateText = _GlobalResources.Groups;
                    groupsIcon.CssClass = "DimIcon";
                    groupsSpan.Controls.Add(groupsIcon);
                }

                // roles
                Label rolesSpan = new Label();
                rolesSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(rolesSpan);

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
                {
                    HyperLink rolesLink = new HyperLink();
                    rolesLink.NavigateUrl = "Roles.aspx?uid=" + idUser.ToString();
                    rolesSpan.Controls.Add(rolesLink);

                    Image rolesIcon = new Image();
                    rolesIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION_BUTTON, ImageFiles.EXT_PNG);
                    rolesIcon.AlternateText = _GlobalResources.Roles;
                    rolesIcon.ToolTip = _GlobalResources.Roles;
                    rolesLink.Controls.Add(rolesIcon);
                }
                else
                {
                    Image rolesIcon = new Image();
                    rolesIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION_BUTTON, ImageFiles.EXT_PNG);
                    rolesIcon.AlternateText = _GlobalResources.Roles;
                    rolesIcon.CssClass = "DimIcon";
                    rolesSpan.Controls.Add(rolesIcon);
                }

                // enrollments
                Label enrollmentsSpan = new Label();
                enrollmentsSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(enrollmentsSpan);

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, groupMembershipsList))
                {
                    HyperLink enrollmentsLink = new HyperLink();
                    enrollmentsLink.NavigateUrl = "enrollments/Default.aspx?uid=" + idUser.ToString();
                    enrollmentsSpan.Controls.Add(enrollmentsLink);

                    Image enrollmentsIcon = new Image();
                    enrollmentsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_BUTTON, ImageFiles.EXT_PNG);
                    enrollmentsIcon.AlternateText = _GlobalResources.Enrollments;
                    enrollmentsIcon.ToolTip = _GlobalResources.Enrollments;                    
                    enrollmentsLink.Controls.Add(enrollmentsIcon);
                }
                else
                {
                    Image enrollmentsIcon = new Image();
                    enrollmentsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_BUTTON, ImageFiles.EXT_PNG);
                    enrollmentsIcon.AlternateText = _GlobalResources.Enrollments;
                    enrollmentsIcon.CssClass = "DimIcon";
                    enrollmentsSpan.Controls.Add(enrollmentsIcon);
                }
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on the data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedUser_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedUser_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");            
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseUser_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.UserGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.UserGrid.Rows[i].FindControl(this.UserGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.User.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedUser_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoUser_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {                
                // rebind the grid
                this.UserGrid.BindData();
            }
        }
        #endregion

        #region _BuildMergeUserAccountsModal
        /// <summary>
        /// Builds the modal for merging  user accounts.
        /// </summary>
        private void _BuildMergeUserAccountsModal()
        {
            this._MergeUserAccountsModal = new ModalPopup("MergeUserAccounts_ModalPopup");

            // set modal properties            
            this._MergeUserAccountsModal.Type = ModalPopupType.Form;
            this._MergeUserAccountsModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_MERGE, ImageFiles.EXT_PNG);
            this._MergeUserAccountsModal.HeaderIconAlt = _GlobalResources.MergeUserAccounts;
            this._MergeUserAccountsModal.HeaderText = _GlobalResources.MergeUserAccounts;
            this._MergeUserAccountsModal.TargetControlID = this._MergeUserAccountsLinkButton.ClientID;
            this._MergeUserAccountsModal.SubmitButton.ID = "MergeAccountsSubmitButton";
            this._MergeUserAccountsModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._MergeUserAccountsModal.SubmitButtonCustomText = _GlobalResources.Next;
            this._MergeUserAccountsModal.SubmitButton.OnClientClick = "ClickNextButton(); return false;";
            this._MergeUserAccountsModal.ShowLoadingPlaceholder = true;

            // hidden source and destination user ids
            this._IdUserMergeSource = new HiddenField();
            this._IdUserMergeSource.ID = "IdUserMergeSource";
            this._MergeUserAccountsModal.AddControlToBody(this._IdUserMergeSource);

            this._IdUserMergeDestination = new HiddenField();
            this._IdUserMergeDestination.ID = "IdUserMergeDestination";
            this._MergeUserAccountsModal.AddControlToBody(this._IdUserMergeDestination);

            // information panel
            Panel mergeUserAccountsInformationPanel = new Panel();
            mergeUserAccountsInformationPanel.ID = "MergeUserAccountsInformationPanel";
            
            this.FormatPageInformationPanel(mergeUserAccountsInformationPanel, _GlobalResources.MergingUserAccountsWillCopyAllUserAssociatedObjectsFromTheSelectedSourceAccountToTheSelectedDestinationAccount, true);

            this._MergeUserAccountsModal.AddControlToBody(mergeUserAccountsInformationPanel);

            // source account field
            this._SelectedSourceUserPanel = AsentiaPage.BuildFormField(
                                                                "SelectedSourceUser_Field",
                                                                _GlobalResources.SourceUserAccount,
                                                                null,
                                                                new Literal(),
                                                                false,
                                                                false,
                                                                false);

            this._SelectedSourceUserPanel.Style.Add("display", "none");

            this._MergeUserAccountsModal.AddControlToBody(this._SelectedSourceUserPanel);

            // destination account field
            this._SelectedDestinationUserPanel = AsentiaPage.BuildFormField(
                                                                     "SelectedDestinationUser_Field",
                                                                     _GlobalResources.DestinationUserAccount,
                                                                     null,
                                                                     new Literal(),
                                                                     false,
                                                                     false,
                                                                     false);

            this._SelectedDestinationUserPanel.Style.Add("display", "none");

            this._MergeUserAccountsModal.AddControlToBody(this._SelectedDestinationUserPanel);

            // source user list box
            this._SelectSourceUser = new DynamicListBox("SelectSourceUser");
            this._SelectSourceUser.NoRecordsFoundMessage = _GlobalResources.NoUsersFound;
            this._SelectSourceUser.SearchButton.Command += new CommandEventHandler(this._SearchSelectSourceUserButton_Command);
            this._SelectSourceUser.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectSourceUserButton_Command);
            this._SelectSourceUser.IsMultipleSelect = false;
            this._SelectSourceUser.IncludeSelectAllNone = false;
            this._SelectSourceUser.ListBoxControl.DataSource = this._EligibleUsersForSelectList;
            this._SelectSourceUser.ListBoxControl.Attributes.Add("onchange", "SelectUserAccount();");
            this._SelectSourceUser.ListBoxControl.DataTextField = "displayName";
            this._SelectSourceUser.ListBoxControl.DataValueField = "idUser";            

            // source user panel
            this._SourceUserPanel = AsentiaPage.BuildFormField(
                                                        "SourceSelectUser_Field",
                                                        _GlobalResources.ChooseSourceUserAccount,
                                                        null,
                                                        this._SelectSourceUser,
                                                        true,
                                                        true,
                                                        false);

            this._MergeUserAccountsModal.AddControlToBody(this._SourceUserPanel);            

            // destination user list box
            this._SelectDestinationUser = new DynamicListBox("SelectDestinationUser");
            this._SelectDestinationUser.NoRecordsFoundMessage = _GlobalResources.NoUsersFound;
            this._SelectDestinationUser.SearchButton.Command += new CommandEventHandler(this._SearchSelectDestinationUserButton_Command);
            this._SelectDestinationUser.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectDestinationUserButton_Command);
            this._SelectDestinationUser.IsMultipleSelect = false;
            this._SelectDestinationUser.IncludeSelectAllNone = false;
            this._SelectDestinationUser.ListBoxControl.DataSource = this._EligibleUsersForSelectList;
            this._SelectDestinationUser.ListBoxControl.Attributes.Add("onchange", "SelectUserAccount();");
            this._SelectDestinationUser.ListBoxControl.DataTextField = "displayName";
            this._SelectDestinationUser.ListBoxControl.DataValueField = "idUser";

            // destination user panel
            this._DestinationUserPanel = AsentiaPage.BuildFormField(
                                                             "DestinationSelectUser_Field",
                                                             _GlobalResources.ChooseDestinationUserAccount,
                                                             null,
                                                             this._SelectDestinationUser,
                                                             true,
                                                             true,
                                                             false);
            
            this._DestinationUserPanel.Style.Add("display", "none");

            this._MergeUserAccountsModal.AddControlToBody(this._DestinationUserPanel);

            // hidden merge button
            this._HiddenMergeButton = new Button();
            this._HiddenMergeButton.ID = "HiddenMergeButton";
            this._HiddenMergeButton.Text = _GlobalResources.Merge;
            this._HiddenMergeButton.Style.Add("display", "none");
            this._HiddenMergeButton.Click += this._MergeUserAccounts;
            this._MergeUserAccountsModal.AddControlToBody(this._HiddenMergeButton);

            // hidden button for binding source user listbox
            this._HiddenSourceBindButton = new Button();
            this._HiddenSourceBindButton.ID = "HiddenSourceBindButton";
            this._HiddenSourceBindButton.Style.Add("display", "none");
            this._HiddenSourceBindButton.Click += this._HiddenSourceBindButton_Click;
            this._MergeUserAccountsModal.AddControlToBody(this._HiddenSourceBindButton);

            // hidden button for binding destination user listbox
            this._HiddenDestinationBindButton = new Button();
            this._HiddenDestinationBindButton.ID = "HiddenDestinationBindButton";
            this._HiddenDestinationBindButton.Style.Add("display", "none");
            this._HiddenDestinationBindButton.Click += this._HiddenDestinationBindButton_Click;
            this._MergeUserAccountsModal.AddControlToBody(this._HiddenDestinationBindButton);

            // add modal to actions panel
            this.ActionsPanel.Controls.Add(this._MergeUserAccountsModal);
        }
        #endregion

        #region _HiddenSourceBindButton_Click
        /// <summary>
        /// Binds the user data table to the source user select box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _HiddenSourceBindButton_Click(object sender, EventArgs e)
        {
            this._EligibleUsersForSelectList = Library.User.IdsAndNamesForMergeUsersSelectList(0, null);
            this._SelectSourceUserDataBind();
        }
        #endregion

        #region _HiddenDestinationBindButton_Click
        /// <summary>
        /// Binds the user data table to the destination user select box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _HiddenDestinationBindButton_Click(object sender, EventArgs e)
        {
            // get source user id from the hidden field value
            int idUserSource;

            if (this._IdUserMergeSource.Value == null)
            { idUserSource = 0; }
            else
            { idUserSource = Convert.ToInt32(this._IdUserMergeSource.Value.Split('|')[0]); }

            this._EligibleUsersForSelectList = Library.User.IdsAndNamesForMergeUsersSelectList(idUserSource, null);
            this._SelectDestinationUserDataBind();
        }
        #endregion

        #region _SearchSelectSourceUserButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Source User" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectSourceUserButton_Command(object sender, CommandEventArgs e)
        {            
            // clear the modal's feedback container
            this._MergeUserAccountsModal.ClearFeedback();
            
            // clear the listbox control
            this._SelectSourceUser.ListBoxControl.Items.Clear();

            // get source user id from the hidden field value
            int idUserSource;

            if (this._IdUserMergeSource.Value == null)
            { idUserSource = 0; }
            else
            { idUserSource = Convert.ToInt32(this._IdUserMergeSource.Value.Split('|')[0]); }
            
            // do the search
            this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(idUserSource, this._SelectSourceUser.SearchTextBox.Text);            
            this._SelectSourceUserDataBind();
        }
        #endregion        

        #region _ClearSearchSelectSourceUserButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Source User" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectSourceUserButton_Command(object sender, CommandEventArgs e)
        {            
            // clear the modal's feedback container
            this._MergeUserAccountsModal.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectSourceUser.ListBoxControl.Items.Clear();
            this._SelectSourceUser.SearchTextBox.Text = "";

            // get source user id from the hidden field value
            int idUserSource;

            if (this._IdUserMergeSource.Value == null)
            { idUserSource = 0; }
            else
            { idUserSource = Convert.ToInt32(this._IdUserMergeSource.Value.Split('|')[0]); }

            // clear the search
            this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(0, null);
            this._SelectSourceUserDataBind();
        }
        #endregion

        #region _SelectSourceUserDataBind
        /// <summary>
        /// DataBind for the select source user list box.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectSourceUserDataBind()
        {
            this._SelectSourceUser.ListBoxControl.DataSource = this._EligibleUsersForSelectList;
            this._SelectSourceUser.ListBoxControl.DataTextField = "displayName";
            this._SelectSourceUser.ListBoxControl.DataValueField = "idUser";
            this._SelectSourceUser.ListBoxControl.DataBind();

            // if no records available then disable the submit button
            if (this._SelectSourceUser.ListBoxControl.Items.Count == 0 || this._SelectSourceUser.ListBoxControl.SelectedValue == "")
            {
                // this._MergeUserAccountsModal.SubmitButton.Enabled = false;
                this._MergeUserAccountsModal.SubmitButton.Attributes.Add("disabled", "disabled");
                this._MergeUserAccountsModal.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._MergeUserAccountsModal.SubmitButton.Enabled = true;
                this._MergeUserAccountsModal.SubmitButton.CssClass = "Button ActionButton";
            }

            // adjust the display and other properties of the source and destination panels
            this._SelectedSourceUserPanel.Style.Add("display", "none");
            this._SelectedDestinationUserPanel.Style.Add("display", "none");
            this._DestinationUserPanel.Style.Add("display", "none");
            this._SourceUserPanel.Style.Add("display", "");
            this._MergeUserAccountsModal.SubmitButtonCustomText = _GlobalResources.Next;
        }
        #endregion

        #region _SearchSelectDestinationUserButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Destination User" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectDestinationUserButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._MergeUserAccountsModal.ClearFeedback();

            // clear the listbox control
            this._SelectDestinationUser.ListBoxControl.Items.Clear();

            // get source user id from the hidden field value
            int idUserSource;

            if (this._IdUserMergeSource.Value == null)
            { idUserSource = 0; }
            else
            { idUserSource = Convert.ToInt32(this._IdUserMergeSource.Value.Split('|')[0]); }

            // do the search
            this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(idUserSource, this._SelectDestinationUser.SearchTextBox.Text);
            this._SelectDestinationUserDataBind();
        }
        #endregion        

        #region _ClearSearchSelectDestinationUserButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Destination User" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectDestinationUserButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._MergeUserAccountsModal.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectDestinationUser.ListBoxControl.Items.Clear();
            this._SelectDestinationUser.SearchTextBox.Text = "";

            // get source user id from the hidden field value
            int idUserSource;

            if (this._IdUserMergeSource.Value == null)
            { idUserSource = 0; }
            else
            { idUserSource = Convert.ToInt32(this._IdUserMergeSource.Value.Split('|')[0]); }

            // clear the search
            this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(idUserSource, null);
            this._SelectDestinationUserDataBind();
        }
        #endregion

        #region _SelectDestinationUserDataBind
        /// <summary>
        /// DataBind for the select destination user list box.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectDestinationUserDataBind()
        {
            this._SelectDestinationUser.ListBoxControl.DataSource = this._EligibleUsersForSelectList;
            this._SelectDestinationUser.ListBoxControl.DataTextField = "displayName";
            this._SelectDestinationUser.ListBoxControl.DataValueField = "idUser";
            this._SelectDestinationUser.ListBoxControl.DataBind();

            // if no records available then disable the list
            if (this._SelectDestinationUser.ListBoxControl.Items.Count == 0 || this._SelectDestinationUser.ListBoxControl.SelectedValue == "")
            {
                // this._MergeUserAccountsModal.SubmitButton.Enabled = false;
                this._MergeUserAccountsModal.SubmitButton.Attributes.Add("disabled", "disabled");
                this._MergeUserAccountsModal.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._MergeUserAccountsModal.SubmitButton.Enabled = true;
                this._MergeUserAccountsModal.SubmitButton.CssClass = "Button ActionButton";
            }

            // adjust the display and other properties of the source and destination panels
            this._SelectedSourceUserPanel.Style.Add("display", "");
            this._SourceUserPanel.Style.Add("display", "none");
            this._DestinationUserPanel.Style.Add("display", "");

            // get source user display name from the hidden field value and display it in the modal
            if (this._IdUserMergeSource.Value != null)
            {
                string[] sourceValues = new string[2];
                sourceValues = this._IdUserMergeSource.Value.Split('|');

                if (sourceValues[0] != "0")
                {
                    Literal sourceUserLiteral = new Literal();
                    sourceUserLiteral.Text = sourceValues[1];

                    this._SelectedSourceUserPanel.FindControl("SelectedSourceUser_Field_InputContainer").Controls.Add(sourceUserLiteral);
                }
            }
        }
        #endregion

        #region _MergeUserAccounts
        /// <summary>
        /// Merges the selected source user account into the selected destination user account.
        /// </summary>
        private void _MergeUserAccounts(object sender, EventArgs e)
        {
            try
            {
                // make sure the source and destination user values are not null
                if (String.IsNullOrWhiteSpace(this._IdUserMergeSource.Value) || String.IsNullOrWhiteSpace(this._IdUserMergeDestination.Value))
                {
                    this._IdUserMergeSource.Value = "0|null";
                    this._IdUserMergeDestination.Value = "0|null";

                    // display error message
                    this._MergeUserAccountsModal.DisplayFeedback(_GlobalResources.AnErrorOccurredWhileAttemptingToMergeTheSelectedUserAccountsPleaseTryAgain, true);
                }
                else
                {
                    int idUserSource = Convert.ToInt32(this._IdUserMergeSource.Value.Split('|')[0]);
                    int idUserDestination = Convert.ToInt32(this._IdUserMergeDestination.Value.Split('|')[0]);

                    // make sure the user and destination ids are greater than 0
                    if (idUserSource > 0 && idUserDestination > 0)
                    {
                        // execute the merge user accounts procedure
                        Library.User.MergeUserAccounts(idUserSource, idUserDestination);

                        // UPDATE FILESYSTEM

                        // copy lesson data from source to destination and delete source lesson data
                        if (Directory.Exists(Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + idUserSource.ToString())))
                        {
                            Utility.CopyDirectory(Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + idUserSource.ToString()), Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + idUserDestination.ToString()));
                            Utility.EmptyOldFolderItems(Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + idUserSource.ToString()), 0, true);
                            Directory.Delete(Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + idUserSource.ToString()), true);
                        }
                        
                        // delete source config user folder
                        if (Directory.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + idUserSource.ToString())))
                        {
                            Utility.EmptyOldFolderItems(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + idUserSource.ToString()), 0, true);
                            Directory.Delete(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + idUserSource.ToString()), true);
                        }

                        // copy profile files from source to destination and delete source user profile files
                        if (Directory.Exists(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_USER + idUserSource.ToString())))
                        {
                            Utility.CopyDirectory(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_USER + idUserSource.ToString()), Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_USER + idUserDestination.ToString()));
                            Utility.EmptyOldFolderItems(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_USER + idUserSource.ToString()), 0, true);
                            Directory.Delete(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_USER + idUserSource.ToString()), true);
                        }

                        // reset the modal fields
                        this._SelectSourceUser.ListBoxControl.Items.Clear();
                        this._SelectSourceUser.SearchTextBox.Text = "";

                        this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(0, null);

                        this._IdUserMergeSource.Value = "0|null";
                        this._IdUserMergeDestination.Value = "0|null";

                        this._SelectSourceUserDataBind();

                        // display the success message
                        this._MergeUserAccountsModal.DisplayFeedback(_GlobalResources.TheSelectedUserAccountsHaveBeenMergedSuccessfully, false);
                    }
                    else
                    {
                        // reset the modal fields
                        this._SelectSourceUser.ListBoxControl.Items.Clear();
                        this._SelectSourceUser.SearchTextBox.Text = "";

                        this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(0, null);

                        this._IdUserMergeSource.Value = "0|null";
                        this._IdUserMergeDestination.Value = "0|null";

                        this._SelectSourceUserDataBind();

                        // display error message
                        this._MergeUserAccountsModal.DisplayFeedback(_GlobalResources.AnErrorOccurredWhileAttemptingToMergeTheSelectedUserAccountsPleaseTryAgain, true);
                    }
                }                
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // reset the modal fields
                this._SelectSourceUser.ListBoxControl.Items.Clear();
                this._SelectSourceUser.SearchTextBox.Text = "";

                this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(0, null);

                this._IdUserMergeSource.Value = "0|null";
                this._IdUserMergeDestination.Value = "0|null";

                this._SelectSourceUserDataBind();

                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // reset the modal fields
                this._SelectSourceUser.ListBoxControl.Items.Clear();
                this._SelectSourceUser.SearchTextBox.Text = "";

                this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(0, null);

                this._IdUserMergeSource.Value = "0|null";
                this._IdUserMergeDestination.Value = "0|null";

                this._SelectSourceUserDataBind();

                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // reset the modal fields
                this._SelectSourceUser.ListBoxControl.Items.Clear();
                this._SelectSourceUser.SearchTextBox.Text = "";

                this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(0, null);

                this._IdUserMergeSource.Value = "0|null";
                this._IdUserMergeDestination.Value = "0|null";

                this._SelectSourceUserDataBind();

                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // reset the modal fields
                this._SelectSourceUser.ListBoxControl.Items.Clear();
                this._SelectSourceUser.SearchTextBox.Text = "";

                this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(0, null);

                this._IdUserMergeSource.Value = "0|null";
                this._IdUserMergeDestination.Value = "0|null";

                this._SelectSourceUserDataBind();

                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // reset the modal fields
                this._SelectSourceUser.ListBoxControl.Items.Clear();
                this._SelectSourceUser.SearchTextBox.Text = "";

                this._EligibleUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForMergeUsersSelectList(0, null);

                this._IdUserMergeSource.Value = "0|null";
                this._IdUserMergeDestination.Value = "0|null";

                this._SelectSourceUserDataBind();

                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion
    }
}
