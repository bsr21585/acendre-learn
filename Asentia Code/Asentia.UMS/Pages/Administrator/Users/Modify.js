﻿/* USER PROPERTIES */

function InitializeForm() {
    // if the form is view only, disable all inputs for it
    if (IsFormViewOnly) {
        $("#UserFormContainer input").prop("disabled", true);
        $("#UserFormContainer textarea").prop("disabled", true);
        $("#UserFormContainer select").prop("disabled", true);
    }
}