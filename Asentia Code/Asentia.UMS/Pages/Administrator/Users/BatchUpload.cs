﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages.Administrator.Users
{
    public class BatchUpload : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel PanelUploadContainer;
        public UpdatePanel UserFormUpdatePanel;
        public Panel UserBatchUploadWrapperPanel;
        public Panel UserAccountData = new Panel();
        public Panel ErrorTablePanel;
        public Panel PanelMsgTable = new Panel();
        public Panel BatchFileUploadPanel;
        #endregion

        #region Private Properties
        private LinkButton _UploadUserBatchButton;
        private DataTable _UsersDataTable = new DataTable();
        private Table _BatchUserDataTable = new Table();
        private Table _UserFieldInfoTable = new Table();
        private TableHeaderRow _RowBatchUserDataTableHeader = new TableHeaderRow();        
        private int _ErrorNum = 0;
        private int _RowNum = 0;
        private const int _MAX_RECORDS_FOR_IMPORT = 10000;
        private string _ErrorList = String.Empty;
        private Button _PreviousErrorButton = new Button();
        private Button _NextErrorButton = new Button();
        private List<string> _ExisitingUsernames;
        private CheckBox _NewUsersOnly;
        private UploaderAsync _BatchUploader;
        private ModalPopup _BatchFileModal;
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            StringBuilder globalJs = new StringBuilder();
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(BatchUpload), "Asentia.UMS.Pages.Administrator.Users.BatchUpload.js");

            globalJs.AppendLine("     Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler); ");
            globalJs.AppendLine("         function EndRequestHandler(sender, args){ ");
            globalJs.AppendLine("         if (args.get_error() != undefined){ ");
            globalJs.AppendLine("          var url = window.location.href; ");   
            globalJs.AppendLine("           if (url.indexOf('?') > -1){  ");
            globalJs.AppendLine("              url += '&timeout=1'  ");
            globalJs.AppendLine("              }else{  ");
            globalJs.AppendLine("               url += '?timeout=1'  ");
            globalJs.AppendLine("              }  ");
            globalJs.AppendLine("           window.location.href = url;  ");
            globalJs.AppendLine("             } ");
            globalJs.AppendLine("         } ");

            // add start up script
            csm.RegisterStartupScript(typeof(BatchUpload), "GlobalJs", globalJs.ToString(), true);

        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // set timeout 
            Server.ScriptTimeout = 600;

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserCreator))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.UserBatchUpload));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, _GlobalResources.UserBatchUpload, ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                                                                          ImageFiles.EXT_PNG));
            this._BuildObjectOptionsPanel();
            this.InitializeAdminMenu();

            this.UserBatchUploadWrapperPanel.CssClass = "FormContentWrapperContainer";

            _LoadPageControls();
        }
        #endregion

        #region _BuildDetailInstructionPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildDetailInstructionPanel()
        {
            Panel activityFileUploadInstructionsPanel = new Panel();
            this.FormatPageInformationPanel(activityFileUploadInstructionsPanel, _GlobalResources.UseTheTableBelowAsReferenceForCreatingTabDelimitedUserFile, true);
            this.UserFormUpdatePanel.ContentTemplateContainer.Controls.Add(activityFileUploadInstructionsPanel);
            
            #region Detail Instruction Panel

            //Detail Instruction Panel
            Panel uploadDetailedInstruction = new Panel();
            uploadDetailedInstruction.ID = "ActivityImportExpandableParagraphContainer";
            uploadDetailedInstruction.CssClass = "ExpandableParagraphContainer";

            Panel sectionTitlePanel1 = new Panel();
            HtmlGenericControl sectionTitle1 = new HtmlGenericControl("h2");
            sectionTitle1.InnerText = _GlobalResources.Important + ":";
            sectionTitlePanel1.Controls.Add(sectionTitle1);
            uploadDetailedInstruction.Controls.Add(sectionTitlePanel1);

            Panel sectionDetail1 = new Panel();
            this.FormatFormInformationPanel(sectionDetail1, _GlobalResources.UsernameColumnCommentForUserBatch, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            Panel sectionDetail2 = new Panel();
            this.FormatFormInformationPanel(sectionDetail2, _GlobalResources.ThisProcessIsARawDataProcess, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            uploadDetailedInstruction.Controls.Add(sectionDetail1);
            uploadDetailedInstruction.Controls.Add(sectionDetail2);

            Panel sectionTitlePanel2 = new Panel();
            HtmlGenericControl sectionTitle2 = new HtmlGenericControl("h2");
            sectionTitle2.InnerText = _GlobalResources.ColumnDetails + ":";
            sectionTitlePanel2.Controls.Add(sectionTitle2);
            uploadDetailedInstruction.Controls.Add(sectionTitlePanel2);

            Panel sectionDetailPanel2 = new Panel();
            HtmlGenericControl ul2 = new HtmlGenericControl("ul");
            HtmlGenericControl li12 = new HtmlGenericControl("li");
            li12.InnerHtml = "<span class=\"bold\">" + _GlobalResources.Required + "</span> - " + _GlobalResources.YESIndicatesThatAllRowsMustContainData;
            ul2.Controls.Add(li12);

            HtmlGenericControl li22 = new HtmlGenericControl("li");
            li22.InnerHtml = "<span class=\"bold\">" + _GlobalResources.Unique + "</span> - " + _GlobalResources.YESIndicatesThatAllRowsMustContainData;
            ul2.Controls.Add(li22);

            HtmlGenericControl li32 = new HtmlGenericControl("li");
            li32.InnerHtml = "<span class=\"bold\">" + _GlobalResources.Length + "</span> - " + _GlobalResources.TheMaximumNumberOfCharacters;
            ul2.Controls.Add(li32);

            HtmlGenericControl li42 = new HtmlGenericControl("li");
            li42.InnerHtml = "<span class=\"bold\">" + _GlobalResources.Format + "</span> - " + _GlobalResources.DisplaysFormattingInformationForTheColumn;
            ul2.Controls.Add(li42);
            sectionDetailPanel2.Controls.Add(ul2);
            uploadDetailedInstruction.Controls.Add(sectionDetailPanel2);

            Panel sectionTitlePanel4 = new Panel();
            HtmlGenericControl sectionTitle4 = new HtmlGenericControl("h2");
            sectionTitle4.InnerText = _GlobalResources.ImportantFormattingNotes + ":";
            sectionTitlePanel4.Controls.Add(sectionTitle4);
            uploadDetailedInstruction.Controls.Add(sectionTitlePanel4);

            Panel sectionDetail4 = new Panel();
            this.FormatFormInformationPanel(sectionDetail4, _GlobalResources.NoteTheFollowingBeforeCreatingYourDataFile, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            HtmlGenericControl ul = new HtmlGenericControl("ul");
            HtmlGenericControl li1 = new HtmlGenericControl("li");
            li1.InnerHtml = _GlobalResources.UploadedFileMustBeATabDelimitedTextFileWithExtensionTXT;
            ul.Controls.Add(li1);

            HtmlGenericControl li2 = new HtmlGenericControl("li");
            li2.InnerHtml = _GlobalResources.IncludeAllDataColumns;
            ul.Controls.Add(li2);

            HtmlGenericControl li3 = new HtmlGenericControl("li");
            li3.InnerHtml = _GlobalResources.AllRequiredFieldsMust;
            ul.Controls.Add(li3);

            HtmlGenericControl li4 = new HtmlGenericControl("li");
            li4.InnerHtml = _GlobalResources.BeSureThatNoExtraEmptyLines;
            ul.Controls.Add(li4);

            HtmlGenericControl li5 = new HtmlGenericControl("li");
            li5.InnerHtml = _GlobalResources.DoNotIncludeColumnHeaders;
            ul.Controls.Add(li5);

            HtmlGenericControl li6 = new HtmlGenericControl("li");
            li6.InnerHtml = _GlobalResources.DoNotEncloseCellValuesInQuotations;
            ul.Controls.Add(li6);

            HtmlGenericControl li7 = new HtmlGenericControl("li");
            li7.InnerHtml = _GlobalResources.DoNotIncludeTabsInCellValues;
            ul.Controls.Add(li7);

            sectionDetail4.Controls.Add(ul);
            uploadDetailedInstruction.Controls.Add(sectionDetail4);

            this.UserFormUpdatePanel.ContentTemplateContainer.Controls.Add(uploadDetailedInstruction);

            #endregion
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            //Upload Image Panel
            ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            // UPLOAD PACKAGE
            this._UploadUserBatchButton = new LinkButton();
            this._UploadUserBatchButton.ID = "BatchFileUploadButton";

            ObjectOptionsPanel.Controls.Add(
                this.BuildOptionsPanelImageLink("UploadBatchFileLink",
                                                this._UploadUserBatchButton,
                                                null,
                                                "InitializeUploadBatchModal(true)",
                                                _GlobalResources.UploadUserBatchFile,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG))
                );
            string qsTimeOut = this.QueryStringString("timeout", String.Empty);

            if (!String.IsNullOrWhiteSpace(qsTimeOut))
            { this.DisplayFeedback(_GlobalResources.ThePageHasReachedItsExecutionTimeoutLimitBecauseTheFileYouAreUploadingContainsTooManyRecords, true); }
        }
        #endregion

        #region _LoadPageControls
        /// <summary>
        /// Setup the Tags/Controls on the page
        /// </summary>
        private void _LoadPageControls()
        {
            this.UserBatchUploadWrapperPanel.ID = this.ID + "_" + "UserBatchUploadWrapperPanel";

            this.UserFormUpdatePanel.ID = "UserBatchFormUpdatePanel";
            this.PanelUploadContainer.CssClass = "TabPanelContainer";

            this._BuildDetailInstructionPanel();

            this._BuildBatchFileUploadModal();

            this._BuildUsersDataTableColumns();

            // build the table
            this._BuildUserInfoTable();

        }
        #endregion

        #region _BuildUsersDataTableColumn
        /// <summary>
        /// Builds datatable columns for UsersDataTable
        /// </summary>
        private void _BuildUsersDataTableColumns()
        {       
            UserAccountData userAccountDataObj = new UserAccountData(UserAccountDataFileType.Site, true, false);
            List<UserAccountData.UserFieldForLabel> labelPool = userAccountDataObj.GetAdminViewableFieldLabelsInLanguage(AsentiaSessionState.UserCulture);
            List<UserAccountData.Tab> userAccountTabs = userAccountDataObj.Tabs;
            
            DataColumn dcUser;
            int columnNum = 0;

            TableHeaderCell cellColumnRowHeader = new TableHeaderCell();

            this._RowBatchUserDataTableHeader.Controls.Add(cellColumnRowHeader);

            foreach (UserFieldGlobalRules ruleSet in userAccountDataObj.GlobalUserFieldRules)
            {
                if (ruleSet.Identifier != "avatar" && ruleSet.Identifier != "disableLoginFromLoginForm" && ruleSet.Identifier != "dtCreated" && ruleSet.Identifier != "dtLastLogin" && ruleSet.Identifier != "optOutOfEmailNotifications")
                {
                    columnNum++;
                    bool matched = false;
                    bool hasRx = false;
                    bool hasChoiceValues = false;
                    bool customizedRequiredField = false;   
                    string columnOrderText = ((columnNum > 26) ? ((Char)(64 + (columnNum - 1) / 26)).ToString() : String.Empty) + ((Char)(64 + columnNum % 26)).ToString();
                    string columnName = ruleSet.Identifier;
                    string batchTableColumnToolTip = String.Empty;
                    string regularExpression = String.Empty;
                    string choiceValues = String.Empty;
                    foreach (UserAccountData.UserFieldForLabel li in labelPool)
                    {
                        if (li.Identifier == ruleSet.Identifier)
                        {
                            columnName = li.Label;
                            matched = true;
                        }
                    }

                    foreach (UserAccountData.Tab tab in userAccountTabs)
                    {
                        List<UserAccountData.UserField> userFields = tab.UserFields;
                        foreach (UserAccountData.UserField userField in userFields)
                        {
                            if (userField.Identifier == ruleSet.Identifier)
                            {
                                customizedRequiredField = userField.IsRequired;
                                if (!String.IsNullOrWhiteSpace(userField.RegularExpression))
                                {
                                    regularExpression = userField.RegularExpression;
                                    hasRx = true;
                                }
                                if (!String.IsNullOrWhiteSpace(userAccountDataObj.GetUserFieldChoiceValuesInLanguage(userField, AsentiaSessionState.UserCulture)))
                                {
                                    choiceValues = userAccountDataObj.GetUserFieldChoiceValuesInLanguage(userField, AsentiaSessionState.UserCulture);
                                    hasChoiceValues = true;
                                }
                                
                            }
                        }
                    }

                    Type dcType;
                    switch (ruleSet.DataType.ToString())
                    {
                        case "String":
                            dcType = typeof(string);
                            batchTableColumnToolTip += _GlobalResources.Text;
                            break;
                        case "Integer":
                            dcType = typeof(int);
                            batchTableColumnToolTip += _GlobalResources.Integer;
                            break;
                        case "Boolean":
                            dcType = typeof(bool);
                            batchTableColumnToolTip += _GlobalResources.Boolean;
                            break;
                        case "Date":
                            dcType = typeof(DateTime);
                            batchTableColumnToolTip += _GlobalResources.DateTime;
                            break;
                        default:
                            dcType = typeof(string);
                            batchTableColumnToolTip += _GlobalResources.Text;
                            break;
                    }

                    dcUser = new DataColumn(columnName, dcType);
                    dcUser.Unique = ruleSet.IsUnique;
                    dcUser.ExtendedProperties.Add("NotUsed", !matched);
                    dcUser.ExtendedProperties.Add("Identifier", ruleSet.Identifier);
                    dcUser.ExtendedProperties.Add("hasRx", hasRx);
                    dcUser.ExtendedProperties.Add("RegularExpression", regularExpression);
                    dcUser.ExtendedProperties.Add("hasChoiceValues", hasChoiceValues);
                    dcUser.ExtendedProperties.Add("ChoiceValues", choiceValues);

                    //the following two extended properties for information table & validation usage, it won't be used for validating against data added to the datatable
                    dcUser.ExtendedProperties.Add("CanBeEmpty", !(ruleSet.AlwaysRequired || customizedRequiredField));
                    if (dcType == typeof(string))
                    {
                        dcUser.ExtendedProperties.Add("MaxCharNum",ruleSet.MaxLength);
                    }

                    if (ruleSet.Identifier == "idTimezone")
                    {
                        dcUser.DataType = typeof(string);
                        dcUser.ExtendedProperties["MaxCharNum"] = 50;
                    }
                    else if (ruleSet.Identifier == "idLanguage")
                    {
                        dcUser.DataType = typeof(string);
                        dcUser.ExtendedProperties["MaxCharNum"] = 10;
                    }
                    else if (ruleSet.Identifier == "password")
                    {
                        dcUser.ExtendedProperties["CanBeEmpty"] = true;
                    }
                    this._UsersDataTable.Columns.Add(dcUser);

                    //Create header row for the _RowBatchUserDataTableHeader
                    TableHeaderCell cellColumnHeader = new TableHeaderCell();
                    cellColumnHeader.Text = columnOrderText.Replace('@', 'Z'); ;
                    cellColumnHeader.ToolTip = matched ? columnName + " (" + batchTableColumnToolTip + ")" : _GlobalResources.NOTUSED_UPPER;
                    _RowBatchUserDataTableHeader.Controls.Add(cellColumnHeader);

                }
            }
                    //Create column Supervisors
                    columnNum++;
                    dcUser = new DataColumn(_GlobalResources.Supervisor_s, typeof(string));
                    dcUser.Unique = false;
                    dcUser.ExtendedProperties.Add("NotUsed", false);
                    dcUser.ExtendedProperties.Add("Identifier", "supervisors");
                    dcUser.ExtendedProperties.Add("hasRx", false);
                    dcUser.ExtendedProperties.Add("RegularExpression", false);
                    dcUser.ExtendedProperties.Add("hasChoiceValues", false);
                    dcUser.ExtendedProperties.Add("ChoiceValues", String.Empty);
                    dcUser.ExtendedProperties.Add("CanBeEmpty", true);
                    dcUser.ExtendedProperties.Add("MaxCharNum", 255);
                    this._UsersDataTable.Columns.Add(dcUser);

                    TableHeaderCell cellColumnHeaderSupervisors = new TableHeaderCell();
                    cellColumnHeaderSupervisors.Text = ((columnNum > 26) ? ((Char)(64 + (columnNum - 1) / 26)).ToString() : String.Empty) + ((Char)(64 + columnNum % 26)).ToString();
                    cellColumnHeaderSupervisors.ToolTip = "Supervisors";
                    _RowBatchUserDataTableHeader.Controls.Add(cellColumnHeaderSupervisors);



                    //Create extra column Row End
                    columnNum++;
                    TableHeaderCell cellColumnHeaderEnd = new TableHeaderCell();
                    cellColumnHeaderEnd.Text = ((columnNum > 26) ? ((Char)(64 + (columnNum - 1) / 26)).ToString() : String.Empty) + ((Char)(64 + columnNum % 26)).ToString();
                    cellColumnHeaderEnd.ToolTip = "Row End";
                    _RowBatchUserDataTableHeader.Controls.Add(cellColumnHeaderEnd);

                    _BatchUserDataTable.CssClass = "ExcelTableFormat";
                    _BatchUserDataTable.Controls.Add(_RowBatchUserDataTableHeader);
        }

        #endregion

        #region _BuildUserInfoTable
        /// <summary>
        /// Builds the UserInfoTable for the page.
        /// </summary>
        private void _BuildUserInfoTable()
        {
            // apply css class to container
            this.UserAccountData.CssClass = "FormContentContainer";

            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.CssClass = "GridHeaderRow";
            TableHeaderCell columnOrder = new TableHeaderCell();
            TableHeaderCell dataHeader = new TableHeaderCell();
            TableHeaderCell requiredHeader = new TableHeaderCell();
            TableHeaderCell uniqueHeader = new TableHeaderCell();
            TableHeaderCell typeHeader = new TableHeaderCell();
            TableHeaderCell lengthHeader = new TableHeaderCell();
            TableHeaderCell formatHeader = new TableHeaderCell();

            columnOrder.Text = _GlobalResources.Column;
            int columnNum = 0;
            dataHeader.Text = _GlobalResources.Data;
            requiredHeader.Text = _GlobalResources.Required;
            uniqueHeader.Text = _GlobalResources.Unique;
            typeHeader.Text = _GlobalResources.Type;
            lengthHeader.Text = _GlobalResources.Length;
            formatHeader.Text = _GlobalResources.Format;

            headerRow.Controls.Add(columnOrder);
            headerRow.Controls.Add(dataHeader);
            headerRow.Controls.Add(requiredHeader);
            headerRow.Controls.Add(uniqueHeader);
            headerRow.Controls.Add(typeHeader);
            headerRow.Controls.Add(lengthHeader);
            headerRow.Controls.Add(formatHeader);

            _UserFieldInfoTable.Controls.Add(headerRow);
            _UserFieldInfoTable.CssClass = "GridTable";

            foreach (DataColumn column in this._UsersDataTable.Columns)
            {
                columnNum++;
                bool notUsed = Convert.ToBoolean(column.ExtendedProperties["NotUsed"]);
                string columnOrderText = "Column " + ((columnNum >26) ? ((Char)(64 + (columnNum -1) / 26)).ToString() : String.Empty) + ((Char)(64 + columnNum % 26)).ToString();
                    TableRow userInfoTableRow = new TableRow();
                    userInfoTableRow.CssClass = ((columnNum % 2) == 0) ? "GridDataRow GridDataRowAlternate" : "GridDataRow";
                    if (notUsed) userInfoTableRow.CssClass = ((columnNum % 2) == 0) ? "GridDataRow GridDataRowAlternate GridDataRowDisabled" : "GridDataRow GridDataRowDisabled";

                    // Column Order
                    TableCell columnTableCell = new TableCell();
                    columnTableCell.Text = columnOrderText.Replace('@','Z');

                    if (notUsed) columnTableCell.CssClass = "CellNotUsed";
                    userInfoTableRow.Controls.Add(columnTableCell);

                    // Column Identifier
                    TableCell labelTableCell = new TableCell();
                    labelTableCell.Text = notUsed ? _GlobalResources.NOTUSED_UPPER : column.Caption;

                    if (notUsed)
                        labelTableCell.CssClass = "CellNotUsed";
                    else
                        labelTableCell.CssClass = "ColumnDataName";

                    userInfoTableRow.Controls.Add(labelTableCell);

                    // Column Always Required
                    TableCell requiredTableCell = new TableCell();
                    requiredTableCell.Text = notUsed ? "" : ((Convert.ToBoolean(column.ExtendedProperties["CanBeEmpty"]) && (column.ExtendedProperties["Identifier"].ToString() != "password")) ? " -" : _GlobalResources.Yes);
                    userInfoTableRow.Controls.Add(requiredTableCell);

                    // Unique 
                    TableCell uniqueTableCell = new TableCell();
                    uniqueTableCell.Text = notUsed ? "" : ((column.Unique) ? _GlobalResources.Yes : " -");
                    userInfoTableRow.Controls.Add(uniqueTableCell);

                    // Field Type
                    TableCell typeTableCell = new TableCell();
                    if (!notUsed)
                    switch (column.DataType.ToString()){
                        case ("System.String"):
                            typeTableCell.Text = _GlobalResources.Text;
                            break;
                        case ("System.DateTime"):
                            typeTableCell.Text = _GlobalResources.DateTime;
                            break;
                        case ("System.Boolean"):
                            typeTableCell.Text = _GlobalResources.Boolean;
                            break;
                        case ("System.Int32"):
                            typeTableCell.Text = _GlobalResources.Integer;
                            break;
                        default:
                            typeTableCell.Text = column.DataType.ToString();
                            break;
                    }
                    userInfoTableRow.Controls.Add(typeTableCell);


                    // Field Length
                    TableCell lengthTableCell = new TableCell();
                    lengthTableCell.Text = notUsed? "" : ((column.DataType == typeof(string)) ? column.ExtendedProperties["MaxCharNum"].ToString() : " -");
                    userInfoTableRow.Controls.Add(lengthTableCell);

                    // Field Format
                    TableCell formatTableCell = new TableCell();
                    if (!notUsed)
                    {
                        switch (column.DataType.ToString())
                        {
                            case ("System.String"):
                                switch (column.ExtendedProperties["Identifier"].ToString())
                                {
                                    case "country":
                                        formatTableCell.Text = _GlobalResources.CountryCodeFormatShouldBeISOStandard;
                                        break;
                                    case "idTimezone":                                        
                                        // cell text
                                        Label timezoneFormatText = new Label();
                                        timezoneFormatText.Text = _GlobalResources.TimezoneFormatShouldBeISOStandard;
                                        timezoneFormatText.Style.Add("margin-right", "6px"); // its ok to set an inline style for margin-right on this since we know there is a popup trigger going next to it
                                        formatTableCell.Controls.Add(timezoneFormatText);

                                        // create a table of timezones and place it in a information popup
                                        DataTable timezoneValueData = Timezone.GetTimezones();
                                        
                                        Table timezoneValues = new Table();
                                        timezoneValues.CssClass = "GridTable";

                                        // header
                                        TableHeaderRow timezoneValuesHeader = new TableHeaderRow();
                                        timezoneValuesHeader.CssClass = "GridHeaderRow";
                                        timezoneValues.Rows.Add(timezoneValuesHeader);

                                        TableHeaderCell timezoneValuesHeaderTimezone = new TableHeaderCell();
                                        timezoneValuesHeaderTimezone.Text = _GlobalResources.Timezone;
                                        timezoneValuesHeader.Cells.Add(timezoneValuesHeaderTimezone);

                                        TableHeaderCell timezoneValuesHeaderData = new TableHeaderCell();
                                        timezoneValuesHeaderData.Text = _GlobalResources.Value;
                                        timezoneValuesHeader.Cells.Add(timezoneValuesHeaderData);

                                        // data
                                        foreach (DataRow row in timezoneValueData.Rows)
                                        {
                                            TableRow tableDataRow = new TableRow();
                                            tableDataRow.CssClass = "GridDataRow";
                                            timezoneValues.Rows.Add(tableDataRow);

                                            TableCell tableCellTimezone = new TableCell();
                                            tableCellTimezone.Text = row["displayName"].ToString();
                                            tableDataRow.Cells.Add(tableCellTimezone);

                                            TableCell tableCellValue = new TableCell();
                                            tableCellValue.Text = row["dotNetName"].ToString();
                                            tableDataRow.Cells.Add(tableCellValue);
                                        }

                                        // information popup
                                        InformationStatusPanel timezoneValuesInformationPopup = new InformationStatusPanel("TimezoneValuesInformation", InformationStatusPanel_Type.DraggablePopup, InformationStatusPanel_MessageType.Help, _GlobalResources.BelowIsAListOfTimezoneValuesThatAreValidForUseInTheUserBatchFile, timezoneValues);
                                        timezoneValuesInformationPopup.UseLaunchTrigger = true;
                                        timezoneValuesInformationPopup.DrawLaunchTriggerIconButton = true;
                                        timezoneValuesInformationPopup.DisplayInlineBlock = true;
                                        timezoneValuesInformationPopup.PositionRelative = true;
                                        formatTableCell.Controls.Add(timezoneValuesInformationPopup);

                                        break;
                                    case "idLanguage":                                        
                                        // cell text
                                        Label languageFormatText = new Label();
                                        languageFormatText.Text = _GlobalResources.LanguageCodeFormatShouldBeISOStandard;
                                        languageFormatText.Style.Add("margin-right", "6px"); // its ok to set an inline style for margin-right on this since we know there is a popup trigger going next to it
                                        formatTableCell.Controls.Add(languageFormatText);

                                        // create a table of languages and place it in a information popup
                                        ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                                        Table languageValues = new Table();
                                        languageValues.CssClass = "GridTable";

                                        // header
                                        TableHeaderRow languageValuesHeader = new TableHeaderRow();
                                        languageValuesHeader.CssClass = "GridHeaderRow";
                                        languageValues.Rows.Add(languageValuesHeader);

                                        TableHeaderCell languageValuesHeaderTimezone = new TableHeaderCell();
                                        languageValuesHeaderTimezone.Text = _GlobalResources.Timezone;
                                        languageValuesHeader.Cells.Add(languageValuesHeaderTimezone);

                                        TableHeaderCell languageValuesHeaderData = new TableHeaderCell();
                                        languageValuesHeaderData.Text = _GlobalResources.Value;
                                        languageValuesHeader.Cells.Add(languageValuesHeaderData);

                                        // data
                                        foreach (string availableLanguage in availableLanguages)
                                        {
                                            CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                                            TableRow tableDataRow = new TableRow();
                                            tableDataRow.CssClass = "GridDataRow";
                                            languageValues.Rows.Add(tableDataRow);

                                            TableCell tableCellLanguage = new TableCell();
                                            tableCellLanguage.Text = cultureInfo.NativeName;
                                            tableDataRow.Cells.Add(tableCellLanguage);

                                            TableCell tableCellValue = new TableCell();
                                            tableCellValue.Text = cultureInfo.Name;
                                            tableDataRow.Cells.Add(tableCellValue);
                                        }

                                        // information popup
                                        InformationStatusPanel languageValuesInformationPopup = new InformationStatusPanel("LanguageValuesInformation", InformationStatusPanel_Type.DraggablePopup, InformationStatusPanel_MessageType.Help, _GlobalResources.BelowIsAListOfLanguageValuesThatAreValidForUseInTheUserBatchFile, languageValues);
                                        languageValuesInformationPopup.UseLaunchTrigger = true;
                                        languageValuesInformationPopup.DrawLaunchTriggerIconButton = true;
                                        languageValuesInformationPopup.DisplayInlineBlock = true;
                                        languageValuesInformationPopup.PositionRelative = true;
                                        formatTableCell.Controls.Add(languageValuesInformationPopup);

                                        break;
                                    case "gender":
                                        formatTableCell.Text = _GlobalResources.MaleOrFemaleSpecifiedAsMOrF;
                                        break;
                                    case "supervisors":
                                        formatTableCell.Text = _GlobalResources.Username1Username2;
                                        break;
                                    default:
                                        formatTableCell.Text = string.Empty;
                                        break;
                                }
                                break;
                            case ("System.DateTime"):
                                formatTableCell.Text = _GlobalResources.ISODatetime + ": " + DateTime.UtcNow.ToString("yyyy-MM-dd") + " 00:00:00";
                                break;
                            case ("System.Boolean"):
                                formatTableCell.Text = _GlobalResources.TrueOrFalseSpecifiedAs1Or0;
                                break;
                        }
                        if (Convert.ToBoolean(column.ExtendedProperties["hasRx"])) formatTableCell.Text = _GlobalResources.RegularExpression + ": " +column.ExtendedProperties["RegularExpression"].ToString();
                        char[] trimChars = {' ','|'};
                        if (Convert.ToBoolean(column.ExtendedProperties["hasChoiceValues"])) formatTableCell.Text = column.ExtendedProperties["ChoiceValues"].ToString().Replace("\n", " | ").TrimEnd(trimChars);
                    }

                    userInfoTableRow.Controls.Add(formatTableCell);

                    this._UserFieldInfoTable.Controls.Add(userInfoTableRow);
            }

            //Add Row End 
                    columnNum++;
                    TableRow userInfoTableRowEnd = new TableRow();
                    userInfoTableRowEnd.CssClass = ((columnNum % 2) == 0) ? "GridDataRow GridDataRowAlternate" : "GridDataRow";

                    TableCell columnTableCellEnd = new TableCell();
                    columnTableCellEnd.Text = "Column " + ((columnNum > 26) ? ((Char)(64 + (columnNum - 1) / 26)).ToString() : String.Empty) + ((Char)(64 + columnNum % 26)).ToString();
                    userInfoTableRowEnd.Controls.Add(columnTableCellEnd);

                    TableCell labelTableCellEnd = new TableCell();
                    labelTableCellEnd.Text = _GlobalResources.ROWEND_UPPER;
                    labelTableCellEnd.CssClass = "ColumnDataName";
                    userInfoTableRowEnd.Controls.Add(labelTableCellEnd);

                    TableCell noteTableCellEnd = new TableCell();
                    noteTableCellEnd.Text = _GlobalResources.RowEndInfo;
                    noteTableCellEnd.ColumnSpan = 5;
                    userInfoTableRowEnd.Controls.Add(noteTableCellEnd);

                    this._UserFieldInfoTable.Controls.Add(userInfoTableRowEnd);
            this.UserAccountData.Controls.Add(_UserFieldInfoTable);
            this.UserFormUpdatePanel.ContentTemplateContainer.Controls.Add(this.UserAccountData);
        }
        #endregion

        #region _BuildBatchFileUploadModal
        /// <summary>
        /// Pop up window for user to upload batch file
        /// </summary>
        private void _BuildBatchFileUploadModal()
        {
            this._BatchFileModal = new ModalPopup("BatchFileModal");
            this._BatchFileModal.ID = "BatchFileModal";
            this._BatchFileModal.Type = ModalPopupType.Form;
            this._BatchFileModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);
            this._BatchFileModal.HeaderIconAlt = _GlobalResources.UploadUserBatchFile;
            this._BatchFileModal.HeaderText = _GlobalResources.UploadUserBatchFile;
            this._BatchFileModal.TargetControlID = this._UploadUserBatchButton.ID;

            this._BatchFileModal.SubmitButtonCustomText = _GlobalResources.Submit;
            this._BatchFileModal.SubmitButton.ID = "BatchFileModalSubmitButton";
            this._BatchFileModal.SubmitButton.OnClientClick = "javascript:  document.getElementById('BatchFileModalModalPopupHeader').innerHTML = '" + _GlobalResources.ProcessingUserBatchFile + "';";
            this._BatchFileModal.SubmitButton.Command += new CommandEventHandler(this._BatchFileUploadButton_Command);

            // build uploader 
            List<Control> batchFileInputControls = new List<Control>();

            this._BatchUploader = new UploaderAsync("BatchUploader", UploadType.UserBatch, "BatchUploader_ErrorContainer");
            batchFileInputControls.Add(this._BatchUploader);

            // build checkbox
            this._NewUsersOnly = new CheckBox();
            this._NewUsersOnly.ID = "NewUsersOnly";            
            this._NewUsersOnly.Text = _GlobalResources.OnlyCreateNewUsers;

            // add controls to body
            batchFileInputControls.Add(this._NewUsersOnly);

            this._BatchFileModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("BatchUploader",
                                                                                         _GlobalResources.File,
                                                                                         batchFileInputControls,
                                                                                         false,
                                                                                         true));

            // add modal to page
            this.PageContentContainer.Controls.Add(this._BatchFileModal);
        }
        #endregion

        #region _BatchFileUploadButton_Command
        /// <summary>
        /// Handles the event initiated by the Save Changes Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _BatchFileUploadButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(this._BatchUploader.SavedFilePath) && File.Exists(Server.MapPath(this._BatchUploader.SavedFilePath)))
                {
                    bool columnMatched = true;
                    
                    List<string> uploadedUsernames = new List<string>();
                    
                    this._ExisitingUsernames = Asentia.UMS.Library.User.GetAllUsernamesForSite();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeExpandableParagraphs", "Helper.ExpandableParagraphContainer();", true);

                    List<string> lines = File.ReadAllLines(Server.MapPath(this._BatchUploader.SavedFilePath), Encoding.Default).ToList();

                    if (lines.Count > _MAX_RECORDS_FOR_IMPORT)
                    {
                        this.DisplayFeedback(_GlobalResources.YourUserBatchDataImportFileContainsXrecordsWhichExceedsTheaximumNumberOfLRecordsAllowedPleaseBreakYourUserBatchDataImportIntoMultipleFiles.Replace("##X##", lines.Count.ToString()).Replace("##L##", _MAX_RECORDS_FOR_IMPORT.ToString()), true);
                    }
                    else
                    {
                        this.PanelMsgTable.ID = "PanelErrorMsgTable";
                        this.PanelMsgTable.CssClass = "ExcelTableWrapper";
                        this.PanelMsgTable.ScrollBars = ScrollBars.Auto;

                        int lineNum = 0;
                        bool lineBreakFlag = false;
                        bool totalLineBreakFlag = false;
                        string lineBreakError = string.Empty;

                         //read file line by line for line break checking
                        foreach (string line in lines)
                        {
                            lineNum++;
                            string firstItemWithValue = string.Empty;

                            string[] docColumns = line.Replace(Environment.NewLine, "").Split('\t');

                            for (int i = 0; i < docColumns.Length; i++)
                            {
                                if (!String.IsNullOrEmpty(docColumns[i].Trim()) && docColumns[i].Trim() != "\"")
                                {
                                    firstItemWithValue = docColumns[i];
                                    break;
                                }
                            }

                            if (lineBreakFlag)
                            {
                                lineBreakError += "\"" + firstItemWithValue + "\"";
                                lineBreakFlag = false;
                            }
                            else if (docColumns.Length != this._UsersDataTable.Columns.Count + 1)
                            {
                                if (lineNum == 1)
                                {
                                    totalLineBreakFlag = true;
                                    columnMatched = false;
                                    throw new AsentiaException(_GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain);
                                }

                                if (docColumns[docColumns.Length - 1].ToUpper() != "END")
                                {
                                    lineBreakError += "<br>" + _GlobalResources.AtLine + " " + lineNum.ToString() + ":" + _GlobalResources.ThereAreLineBreakOrNewLineCharactersInYourDataBetween + " \"" + (string.IsNullOrEmpty(docColumns[docColumns.Length - 1]) ? docColumns[docColumns.Length - 2] : docColumns[docColumns.Length - 1]) + "\" " + _GlobalResources.and_lower + " ";
                                    lineBreakFlag = true;
                                    totalLineBreakFlag = true;
                                }
                                else
                                {
                                    columnMatched = false;
                                    lineBreakError += "<br>" + _GlobalResources.AtLine + lineNum.ToString() + ": " + _GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(lineBreakError))
                        {
                            if (totalLineBreakFlag)
                            { throw new AsentiaException(_GlobalResources.ThereAreErrorsInTheFollowingPlaces + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain + ":" + lineBreakError); }
                            else
                            { throw new AsentiaException(_GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain); }
                        }

                        //read file line by line
                        foreach (string line in lines)
                        {
                            bool sameUsername = false;
                            string selectString = string.Empty;
                            string[] columns = line.Replace(Environment.NewLine, "").Split('\t');

                            //check duplication of the usernames
                            foreach (string existedUsername in uploadedUsernames)
                            {
                                if (columns[4] == existedUsername)
                                {
                                    sameUsername = true;
                                }
                            }
                            uploadedUsernames.Add(columns[4]);

                            try
                            {
                                DataRow userRow = this._UsersDataTable.NewRow();

                                //#datatype conversion
                                for (int i = 0; i < columns.Length - 1; i++)
                                {
                                    if (String.IsNullOrWhiteSpace(columns[i]))
                                        userRow[i] = DBNull.Value;
                                    else if (Convert.ToBoolean(this._UsersDataTable.Columns[i].ExtendedProperties["NotUsed"].ToString()))
                                        userRow[i] = DBNull.Value;
                                    else if (this._UsersDataTable.Columns[i].DataType == typeof(bool))
                                        userRow[i] = (columns[i] == "1") ? true : false;
                                    else if (this._UsersDataTable.Columns[i].DataType == typeof(DateTime))
                                        userRow[i] = Convert.ToDateTime(columns[i]);
                                    else
                                        userRow[i] = columns[i];
                                }                                

                            //if we're only doing new users and this doesnt exist; or the new users checkbox isn't checked, add the user to the batch
                                if (
                                    (this._NewUsersOnly.Checked && !this._ExisitingUsernames.Contains(columns[4]))
                                    || (!this._NewUsersOnly.Checked)
                                   )                                
                                {
                                    this._UsersDataTable.Rows.Add(userRow);
                                }

                            }
                            catch (Exception ex)
                            {
                                this.DisplayFeedback(ex.Message, false);
                            }

                            if (columnMatched)
                            {
                                this._ColumnValueValidation(columns, sameUsername);
                            }

                            this.PanelMsgTable.Controls.Add(_BatchUserDataTable);
                        }

                        if (this._ErrorNum > 0)
                        {
                            this._PreviousErrorButton.ID = "PreviousErrorButton";
                            this._PreviousErrorButton.CssClass = "Button ActionButton";
                            this._PreviousErrorButton.Attributes.Add("onclick", "PreviousError();return false;");
                            this._PreviousErrorButton.Text = _GlobalResources.PreviousError;

                            this._NextErrorButton.ID = "NextErrorButton";
                            this._NextErrorButton.CssClass = "Button ActionButton";
                            this._NextErrorButton.Attributes.Add("onclick", "NextError(); return false;");
                            this._NextErrorButton.Text = _GlobalResources.NextError;

                            this.ErrorTablePanel.Controls.Add(_PreviousErrorButton);
                            this.ErrorTablePanel.Controls.Add(_NextErrorButton);

                            this.ErrorTablePanel.Controls.Add(this.PanelMsgTable);

                            throw new AsentiaException(_GlobalResources.ThisFileCannotBeImportedDueToMistakesPleaseTryAgain.Replace("NUMBER", _ErrorNum.ToString()));
                        }
                        else if (this._UsersDataTable.Rows.Count == 0)
                        {
                            if (this._NewUsersOnly.Checked)
                            { throw new AsentiaException(_GlobalResources.AllUsersInYourDataFileAlreadyExistInThePortal); }
                            else
                            { throw new AsentiaException(_GlobalResources.ThereAreNoUsersInYourDataFile); }
                        }
                        else if (columnMatched)
                        {
                            using (TransactionScope scope = new TransactionScope())
                            {
                                Asentia.UMS.Library.User.SynchronizeUserBatch(this._UsersDataTable);
                                scope.Complete();

                                this.DisplayFeedback(_GlobalResources.UsersHaveBeenSavedSuccessfully, false);
                            }
                            this.UserAccountData.Controls.Add(_UserFieldInfoTable);

                            File.Delete(Server.MapPath(this._BatchUploader.SavedFilePath));
                        }
                    }
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            catch (Exception)
            { ;}

            this._BatchFileModal.HideModal();
        }
        #endregion

        #region _ColumnValueValidation
        /// <summary>
        /// Validation for each user field based on the datatype or its special format
        /// </summary>
        private void _ColumnValueValidation(string[] columns, bool sameUsername)
        {
            _RowNum++;
            string originalData = String.Empty;
            DateTime dateValue;           

            TableRow rowUserData = new TableRow();
            
            TableCell rowNum = new TableCell();
            rowNum.Text = _RowNum.ToString();
            rowNum.CssClass = "ExcelTableRowHeader";
            rowUserData.Controls.Add(rowNum);

            for (int i = 0; i < columns.Length-1; i++)
            {
                TableCell cellUserData = new TableCell();
                cellUserData.CssClass = "ExcelTableData";
                string cellText = columns[i];
                string columnIdentifier = this._UsersDataTable.Columns[i].ExtendedProperties["Identifier"].ToString();
                string errorText = "<a href='#' id='ErrorPositionError#'><font color=red>" + cellText + "<sup>Error#</sup></font></ a>";

                if (Convert.ToBoolean(this._UsersDataTable.Columns[i].ExtendedProperties["NotUsed"].ToString()))
                {
                    cellUserData.CssClass = "ExcelTableRowHeader";
                }
                else
                {
                    //Check if the columns allow null value, if not, check the import value for that column.
                    if (!Convert.ToBoolean(this._UsersDataTable.Columns[i].ExtendedProperties["CanBeEmpty"]) && String.IsNullOrWhiteSpace(cellText) && columnIdentifier != "password")
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_IsARequiredFieldThisFieldMustContainAValue.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption);
                    }
                    //Check the String length
                    else if (this._UsersDataTable.Columns[i].DataType.ToString() == "System.String" && cellText.Length > Convert.ToInt16(this._UsersDataTable.Columns[i].ExtendedProperties["MaxCharNum"]))
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources.YouHaveExceededTheMaximumLengthOfCharactersFor_field_PleaseLimitYour_field_LengthToXNumberOfCharacters.Replace("##field##", this._UsersDataTable.Columns[i].Caption).Replace("##X##", this._UsersDataTable.Columns[i].ExtendedProperties["MaxCharNum"].ToString());
                    }
                    //Check the regular expression
                    else if (this._UsersDataTable.Columns[i].DataType.ToString() == "System.String" && !String.IsNullOrWhiteSpace(cellText) && Convert.ToBoolean(this._UsersDataTable.Columns[i].ExtendedProperties["hasRx"]) && !Common.RegExValidation.ValidateCustomizedReg("" + this._UsersDataTable.Columns[i].ExtendedProperties["RegularExpression"].ToString() + "", cellText))
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_MustContainAValueMatchedRegularExpression.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption) + " " + this._UsersDataTable.Columns[i].ExtendedProperties["RegularExpression"].ToString();
                    }
                    //Check the Datetime format
                    else if (this._UsersDataTable.Columns[i].DataType.ToString() == "System.DateTime" && !String.IsNullOrWhiteSpace(cellText) && !DateTime.TryParse(cellText, out dateValue))
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_MustBeAValidDateInTheISODateTimeFormat.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption);
                    }
                    //Check the boolean format
                    else if (this._UsersDataTable.Columns[i].DataType.ToString() == "System.Boolean" && (cellText != "0") && (cellText != "1") && !(Convert.ToBoolean(this._UsersDataTable.Columns[i].ExtendedProperties["CanBeEmpty"]) && String.IsNullOrWhiteSpace(cellText)))
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_MustContainAValueOf0FalseOr1True.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption);
                    }
                    //Check the Email Format for Email Column
                    else if (columnIdentifier == "email" && !String.IsNullOrWhiteSpace(cellText) && !Convert.ToBoolean(this._UsersDataTable.Columns[i].ExtendedProperties["hasRx"]) && !Common.RegExValidation.ValidateEmailAddress(cellText))
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_IsNotAValidEmailAddress.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption);
                    }
                    //Check username duplication
                    else if (columnIdentifier == "username" && sameUsername && !String.IsNullOrWhiteSpace(cellText))
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources.TheUsername_username_AppearsMoreThanOnceInTheUploadFile.Replace("##username##", cellText);
                    }
                    //Check the Zip Code Format
                    else if (columnIdentifier == "postalcode" && !Common.RegExValidation.ValidateUSCACountryCode(columns[16]) && !String.IsNullOrWhiteSpace(cellText) && !Convert.ToBoolean(this._UsersDataTable.Columns[i].ExtendedProperties["hasRx"]) && !Common.RegExValidation.ValidateZipCode(cellText))
                    {
                        _ErrorNum++;
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_MustBeAValidPostalCode.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption);
                    }
                    //Check the language code Format
                    else if (columnIdentifier == "idLanguage" && !String.IsNullOrWhiteSpace(cellText) && !Common.RegExValidation.ValidateLanguageCode(cellText, this.GetArrayListOfSiteAvailableInstalledLanguages()))
                    {
                        _ErrorNum++;
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_MustContainAValidISOLanguageCodeValue.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption);
                    }
                    //Check the country Format
                    else if (columnIdentifier == "country" && !String.IsNullOrWhiteSpace(cellText) && !Convert.ToBoolean(this._UsersDataTable.Columns[i].ExtendedProperties["hasRx"]) && !Common.RegExValidation.ValidateCountryCode(cellText))
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_MustContainAValidISO2CharacterCountryCodeValue.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption);
                    }
                    //Check the timezone Format
                    else if (columnIdentifier == "idTimezone" && !String.IsNullOrWhiteSpace(cellText) && !Common.RegExValidation.ValidateTimeZone(cellText))
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_MustContainAValidTimezoneName.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption);
                    }
                    //Check the gender Format
                    else if (columnIdentifier == "gender" && !String.IsNullOrWhiteSpace(cellText) && !Convert.ToBoolean(this._UsersDataTable.Columns[i].ExtendedProperties["hasRx"]) && !Common.RegExValidation.ValidateCustomizedReg("(m|M|f|F)", cellText))
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_MustContainAValueOfMMaleOrFFemale.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption);
                    }
                    //Check password column null value not allowed if username is new to the site
                    else if (String.IsNullOrWhiteSpace(cellText) && columnIdentifier == "password" && !this._ExisitingUsernames.Contains(columns[4]))
                    {
                        _ErrorNum++;
                        cellUserData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                        cellUserData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                        cellUserData.ToolTip = "(" + this._UsersDataTable.Columns[i].Caption + ") " + _GlobalResources._FIELD_IsARequiredFieldThisFieldMustContainAValue.Replace("##FIELD##", this._UsersDataTable.Columns[i].Caption);
                    }
                    else
                    {
                        cellUserData.Text = columns[i];                        
                    }
                }

                //hide password information from showing in the feedback panel for reviewing errors
                if (columnIdentifier == "password" && !String.IsNullOrWhiteSpace(cellText))
                {
                    cellUserData.Text = "********";
                }               

                rowUserData.Controls.Add(cellUserData);
            }

            //ROW END
            TableCell cellUserDataEnd = new TableCell();
            cellUserDataEnd.CssClass = "ExcelTableData";
            if (columns[columns.Length - 1].ToUpper() != _GlobalResources.END_UPPER)
            {
                _ErrorNum++;
                cellUserDataEnd.Text = "<a href='#' id='ErrorPosition" + _ErrorNum + "'><font color=red>" + columns[columns.Length - 1] + "<sup>" + _ErrorNum + "</sup></font></ a>";
                cellUserDataEnd.ToolTip = "(ROW END) " + _GlobalResources._FIELD_MustContainTheTextEND.Replace("##FIELD##", "ROW END");
            }
            else
                cellUserDataEnd.Text = columns[columns.Length - 1];

            rowUserData.Controls.Add(cellUserDataEnd);

            this._BatchUserDataTable.Controls.Add(rowUserData);
        }
        #endregion
    }
}