﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages.Administrator.Users
{
    public class Impersonate : AsentiaAuthenticatedPage
    {
        #region Private Properties
        private User _UserObject;
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the user object
            this._GetUserObject();

            if (AsentiaSessionState.IdSite != this._UserObject.IdSite)
            { Response.Redirect("~/administrator/users"); }

            if (AsentiaSessionState.IdSiteUser == this._UserObject.Id)
            { Response.Redirect("~/administrator/users"); }

            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserImpersonator, this._UserObject.GroupMemberships))
            { Response.Redirect("~/administrator/users"); }

            // impersonate the user
            this._ImpersonateUser();
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on querystring if exists.
        /// </summary>
        private void _GetUserObject()
        {
            // get the id querystring parameter
            int id = this.QueryStringInt("uid", 0);

            if (id > 0)
            {
                try
                { this._UserObject = new User(id); }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            { Response.Redirect("~/administrator/users"); }
        }
        #endregion

        #region _ImpersonateUser
        /// <summary>
        /// Impersonates the user and redirects.
        /// </summary>
        private void _ImpersonateUser()
        {
            // set the currently logged in user as the impersonating user
            AsentiaSessionState.IdImpersonatingSiteUser = AsentiaSessionState.IdSiteUser;
            AsentiaSessionState.ImpersonatingUserFirstName = AsentiaSessionState.UserFirstName;
            AsentiaSessionState.ImpersonatingUserLastName = AsentiaSessionState.UserLastName;
            AsentiaSessionState.ImpersonatingUserAvatar = AsentiaSessionState.UserAvatar;
            AsentiaSessionState.ImpersonatingUserGender = AsentiaSessionState.UserGender;
            AsentiaSessionState.ImpersonatingUserCulture = AsentiaSessionState.UserCulture;
            AsentiaSessionState.ImpersonatingUserIsUserACourseExpert = AsentiaSessionState.IsUserACourseExpert;
            AsentiaSessionState.ImpersonatingUserIsUserASupervisor = AsentiaSessionState.IsUserASupervisor;
            AsentiaSessionState.ImpersonatingUserIsUserAWallModerator = AsentiaSessionState.IsUserAWallModerator;
            AsentiaSessionState.ImpersonatingUserIsUserAnILTInstructor = AsentiaSessionState.IsUserAnILTInstructor;

            // set the user we're impersonating as the current session user
            AsentiaSessionState.IdSiteUser = this._UserObject.Id;
            AsentiaSessionState.UserCulture = this._UserObject.LanguageString;
            AsentiaSessionState.UserFirstName = this._UserObject.FirstName;
            AsentiaSessionState.UserLastName = this._UserObject.LastName;
            AsentiaSessionState.UserAvatar = this._UserObject.Avatar;
            AsentiaSessionState.UserGender = this._UserObject.Gender;
            AsentiaSessionState.IsUserACourseExpert = this._UserObject.IsUserACourseExpert;
            AsentiaSessionState.IsUserACourseApprover = this._UserObject.IsUserACourseApprover;
            AsentiaSessionState.IsUserASupervisor = this._UserObject.IsUserASupervisor;
            AsentiaSessionState.IsUserAWallModerator = this._UserObject.IsUserAWallModerator;
            AsentiaSessionState.IsUserAnILTInstructor = this._UserObject.IsUserAnILTInstructor;

            // get the impersonated user's effective permissions
            DataTable effectivePermissions = Asentia.UMS.Library.User.GetEffectivePermissions();
            List<AsentiaPermissionWithScope> effectivePermissionsList = new List<AsentiaPermissionWithScope>();

            foreach (DataRow row in effectivePermissions.Rows)
            {
                AsentiaPermissionWithScope permissionWithScope = new AsentiaPermissionWithScope((AsentiaPermission)Convert.ToInt32(row["idPermission"]), row["scope"].ToString());
                effectivePermissionsList.Add(permissionWithScope);
            }

            AsentiaSessionState.UserEffectivePermissions = effectivePermissionsList;

            // Redirect to the landing page
            Response.Redirect(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_DEFAULTLANDINGPAGE_LOCALPATH));
        }
        #endregion
    }
}
