﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages.Administrator.Users
{
    public class Roles : AsentiaAuthenticatedPage
    {        
        #region Properties
        public Panel RolesFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel RolesPageWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel ObjectOptionsPanel;
        public Panel UserRolesMembershipContainer;        
        #endregion

        #region Private Properties
        private EcommerceSettings _EcommerceSettings;
        private User _UserObject;

        private ModalPopup _AddRolesToUser;
        private ModalPopup _RemoveRolesFromUser;

        private DataTable _RolesForUser;
        private DataTable _RemoveEligibleRolesForUser;
        private DataTable _RolesNotForUser;

        private DynamicListBox _AddEligibleRoles;
        private DynamicListBox _RemoveEligibleRoles;

        private UpdatePanel _RoleListingUpdatePanel;

        // USER EFFECTIVE PERMISSIONS

        private Panel _PermissionsModalBodyPanel;        
        private ModalPopup _DisplayUserPermissionsWithEffectiveScopeModal;        
        private Button _HiddenPermissionButton;
        private LinkButton _DisplayEffectivePermissionLink;

        // PERMISSION CHECKBOXES

        private CheckBox _System_AccountSettings;
        private CheckBox _System_Configuration;
        private CheckBox _System_UserFieldConfiguration;
        private CheckBox _System_Ecommerce;
        private CheckBox _System_CouponCodes;
        private CheckBox _System_TrackingCodes;
        private CheckBox _System_EmailNotifications;
        private CheckBox _System_API;
        private CheckBox _System_xAPIEndpoints;
        private CheckBox _System_Logs;
        private CheckBox _System_WebMeetingIntegration;
        private CheckBox _System_RulesEngine;

        private CheckBox _UsersAndGroups_UserRegistrationApproval;
        private CheckBox _UsersAndGroups_UserCreator;
        private CheckBox _UsersAndGroups_UserDeleter;
        private CheckBox _UsersAndGroups_UserEditor;
        private CheckBox _UsersAndGroups_UserManager;
        private CheckBox _UsersAndGroups_UserImpersonator;
        private CheckBox _UsersAndGroups_GroupCreator;
        private CheckBox _UsersAndGroups_GroupDeleter;
        private CheckBox _UsersAndGroups_GroupEditor;
        private CheckBox _UsersAndGroups_GroupManager;
        private CheckBox _UsersAndGroups_RoleManager;
        private CheckBox _UsersAndGroups_ActivityImport;
        private CheckBox _UsersAndGroups_CertificateImport;
        private CheckBox _UsersAndGroups_Leaderboards;

        private CheckBox _InterfaceAndLayout_HomePage;
        private CheckBox _InterfaceAndLayout_Masthead;
        private CheckBox _InterfaceAndLayout_Footer;
        private CheckBox _InterfaceAndLayout_CSSEditor;
        private CheckBox _InterfaceAndLayout_ImageEditor;

        private CheckBox _LearningAssets_CourseCatalog;
        private CheckBox _LearningAssets_SelfEnrollmentApproval;
        private CheckBox _LearningAssets_CourseContentManager;
        private CheckBox _LearningAssets_CourseEnrollmentManager;
        private CheckBox _LearningAssets_LearningPathContentManager;
        private CheckBox _LearningAssets_LearningPathEnrollmentManager;
        private CheckBox _LearningAssets_CertificationsManager;
        private CheckBox _LearningAssets_ContentPackageManager;
        private CheckBox _LearningAssets_QuizAndSurveyManager;       
        private CheckBox _LearningAssets_CertificateTemplateManager;
        private CheckBox _LearningAssets_InstructorLedTrainingManager;
        private CheckBox _LearningAssets_ResourceManager;

        private CheckBox _Reporting_Reporter_UserDemographics;
        private CheckBox _Reporting_Reporter_UserCourseTranscripts;
        private CheckBox _Reporting_Reporter_UserLearningPathTranscripts;
        private CheckBox _Reporting_Reporter_UserInstructorLedTrainingTranscripts;
        private CheckBox _Reporting_Reporter_CatalogAndCourseInformation;
        private CheckBox _Reporting_Reporter_Certificates;
        //private CheckBox _Reporting_Reporter_XAPI;
        private CheckBox _Reporting_Reporter_Purchases;
        private CheckBox _Reporting_Reporter_UserCertificationTranscripts;        
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the user object
            this._GetUserObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
            { Response.Redirect("/"); }

            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/users/Roles.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the object options panel
            this._BuildObjectOptionsPanel();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.RolesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.RolesPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu
            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Roles;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // build the role membership panel
            this._BuildRoleMembershipPanel();
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on querystring if exists, redirects if not.
        /// </summary>
        private void _GetUserObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("uid", 0);

            if (qsId > 0)
            {
                try
                { this._UserObject = new User(qsId); }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            { Response.Redirect("~/administrator/users"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(userDisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Roles));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, _GlobalResources.Roles, ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion        

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the display effective permissions link.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            this._DisplayEffectivePermissionLink = new LinkButton();
            this._DisplayEffectivePermissionLink.ID = "DisplayEffectivePermissionLink";

            this.ObjectOptionsPanel.Controls.Add(
                this.BuildOptionsPanelImageLink("LaunchEffectivePermissionModalLink",
                                                this._DisplayEffectivePermissionLink,
                                                null,
                                                "$('#HiddenPermissionButton').click(); return false;",
                                                _GlobalResources.DisplayEffectivePermissions,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED, ImageFiles.EXT_PNG))
                );
        }
        #endregion

        #region _BuildRoleMembershipPanel
        /// <summary>
        /// Builds the panel that contains role membership information.
        /// </summary>
        private void _BuildRoleMembershipPanel()
        {
            // populate datatables for role selection
            this._RolesForUser = this._UserObject.GetRoles(false, null);
            this._RemoveEligibleRolesForUser = this._UserObject.GetRoles(true, null);
            this._RolesNotForUser = Asentia.UMS.Library.Role.IdsAndNamesForUserSelectList(this._UserObject.Id, null);

            // clear controls from container
            this.UserRolesMembershipContainer.Controls.Clear();

            this._RoleListingUpdatePanel = new UpdatePanel();
            this._RoleListingUpdatePanel.ID = "RoleListingUpdatePanel";
            this._RoleListingUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;            

            // build a container for the role membership count
            Panel roleMembershipCountContainer = new Panel();
            roleMembershipCountContainer.ID = "UserRolesMembershipCountContainer";

            HtmlGenericControl roleMembershipCountP = new HtmlGenericControl("p");
            roleMembershipCountP.InnerText = String.Format(_GlobalResources.ThisUserIsAMemberOfXRoles, this._RolesForUser.Rows.Count.ToString());

            roleMembershipCountContainer.Controls.Add(roleMembershipCountP);            

            // build a container for the role membership listing
            Panel roleListContainer = new Panel();
            roleListContainer.ID = "UserRolesListingContainer";
            roleListContainer.CssClass = "ItemListingContainer";

            // loop through the datatable and add each member to the listing container
            foreach (DataRow row in this._RolesForUser.Rows)
            {
                Panel roleNameContainer = new Panel();
                roleNameContainer.ID = "Role_" + row["idRole"].ToString();

                Literal roleName = new Literal();
                roleName.Text = row["name"].ToString();

                if (Convert.ToInt32(row["numRulesJoinedBy"]) > 0)
                { roleName.Text += " - " + String.Format(_GlobalResources.AutoJoinedByNumRules, row["numRulesJoinedBy"].ToString()); }

                roleNameContainer.Controls.Add(roleName);
                roleListContainer.Controls.Add(roleNameContainer);
            }

            // attach the user membership listing to the update panel
            this._RoleListingUpdatePanel.ContentTemplateContainer.Controls.Add(roleMembershipCountContainer);
            this._RoleListingUpdatePanel.ContentTemplateContainer.Controls.Add(roleListContainer);

            // attach update panel to container
            this.UserRolesMembershipContainer.Controls.Add(this._RoleListingUpdatePanel);

            // ADD/REMOVE USERS BUTTONS
            Panel buttonsPanel = new Panel();
            buttonsPanel.ID = "UserRolesButtonsContainer";

            // add roles to user link
            Image addRolesToUserImageForLink = new Image();
            addRolesToUserImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            addRolesToUserImageForLink.CssClass = "MediumIcon";

            Localize addRolesToUserTextForLink = new Localize();
            addRolesToUserTextForLink.Text = _GlobalResources.AddRole_s;

            LinkButton addRolesToUserLink = new LinkButton();
            addRolesToUserLink.ID = "LaunchAddRolesToUserModal";
            addRolesToUserLink.CssClass = "ImageLink";
            addRolesToUserLink.Controls.Add(addRolesToUserImageForLink);
            addRolesToUserLink.Controls.Add(addRolesToUserTextForLink);
            buttonsPanel.Controls.Add(addRolesToUserLink);

            // remove roles from user link
            Image removeRolesFromUserImageForLink = new Image();
            removeRolesFromUserImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            removeRolesFromUserImageForLink.CssClass = "MediumIcon";

            Localize removeRolesFromUserTextForLink = new Localize();
            removeRolesFromUserTextForLink.Text = _GlobalResources.RemoveRoles_s;

            LinkButton removeRolesFromUserLink = new LinkButton();
            removeRolesFromUserLink.ID = "LaunchRemoveRolesFromUserModal";
            removeRolesFromUserLink.CssClass = "ImageLink";
            removeRolesFromUserLink.Controls.Add(removeRolesFromUserImageForLink);
            removeRolesFromUserLink.Controls.Add(removeRolesFromUserTextForLink);
            buttonsPanel.Controls.Add(removeRolesFromUserLink);

            // attach the buttons panel to the container
            this.UserRolesMembershipContainer.Controls.Add(buttonsPanel);

            // build modals for adding and removing users to/from roles
            this._BuildAddRolesToUserModal(addRolesToUserLink.ID);
            this._BuildRemoveRolesFromUserModal(removeRolesFromUserLink.ID);

            // add modal for displaying the user effective permissions
            this._BuildDisplayEffectivePermissionModal(this._DisplayEffectivePermissionLink.ID);
        }
        #endregion

        #region _HiddenPermissionButton_Click
        /// <summary>
        /// Hidden button click event for loading effective permissions modal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _HiddenPermissionButton_Click(object sender, EventArgs e)
        {
            // build and attach permissions containers
            this._BuildUserEffectivePermissionsContainer();

            // populate the form input elements
            this._PopulateUserPermissionsInputElements();
        }
        #endregion

        #region _BuildDisplayEffectivePermissionModal
       /// <summary>
       /// Builds the modal for display effective permissions.
       /// </summary>
       /// <param name="targetControlId"></param>
        private void _BuildDisplayEffectivePermissionModal(string targetControlId)
        {
            // set modal properties
            this._DisplayUserPermissionsWithEffectiveScopeModal = new ModalPopup("DisplayEffectivePermissionModal");
            this._DisplayUserPermissionsWithEffectiveScopeModal.Type = ModalPopupType.Form;
            this._DisplayUserPermissionsWithEffectiveScopeModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED, ImageFiles.EXT_PNG);
            this._DisplayUserPermissionsWithEffectiveScopeModal.HeaderIconAlt = _GlobalResources.DisplayEffectivePermissions;
            this._DisplayUserPermissionsWithEffectiveScopeModal.HeaderText = _GlobalResources.DisplayEffectivePermissions;
            this._DisplayUserPermissionsWithEffectiveScopeModal.TargetControlID = targetControlId;
            this._DisplayUserPermissionsWithEffectiveScopeModal.ReloadPageOnClose = false;
            this._DisplayUserPermissionsWithEffectiveScopeModal.SubmitButton.Visible = false;
            this._DisplayUserPermissionsWithEffectiveScopeModal.CloseButton.Visible = false;

            // build the modal body
            this._PermissionsModalBodyPanel = new Panel();

            this._HiddenPermissionButton = new Button();
            this._HiddenPermissionButton.ID = "HiddenPermissionButton";
            this._HiddenPermissionButton.Style.Add("display", "none");
            this._HiddenPermissionButton.Click += _HiddenPermissionButton_Click;
            this._PermissionsModalBodyPanel.Controls.Add(this._HiddenPermissionButton);

            this._DisplayUserPermissionsWithEffectiveScopeModal.AddControlToBody(this._PermissionsModalBodyPanel);

            // add modal to container
            this.UserRolesMembershipContainer.Controls.Add(this._DisplayUserPermissionsWithEffectiveScopeModal);
        }
        #endregion

        #region _BuildAddRolesToUserModal
        /// <summary>
        /// Builds the modal for adding roles to the user.
        /// </summary>
        private void _BuildAddRolesToUserModal(string targetControlId)
        {
            // set modal properties
            this._AddRolesToUser = new ModalPopup("AddRolesToUserModal");
            this._AddRolesToUser.Type = ModalPopupType.Form;
            this._AddRolesToUser.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG);
            this._AddRolesToUser.HeaderIconAlt = _GlobalResources.AddRole_sToUser;
            this._AddRolesToUser.HeaderText = _GlobalResources.AddRole_sToUser;
            this._AddRolesToUser.TargetControlID = targetControlId;
            this._AddRolesToUser.SubmitButton.Command += new CommandEventHandler(this._AddRolesToUserSubmit_Command);
            this._AddRolesToUser.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the roles listing
            this._AddEligibleRoles = new DynamicListBox("AddEligibleRoles");
            this._AddEligibleRoles.NoRecordsFoundMessage = _GlobalResources.NoRolesFound;
            this._AddEligibleRoles.SearchButton.Command += new CommandEventHandler(this._SearchAddRolesButton_Command);
            this._AddEligibleRoles.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchAddRolesButton_Command);
            this._AddEligibleRoles.ListBoxControl.DataSource = this._RolesNotForUser;
            this._AddEligibleRoles.ListBoxControl.DataTextField = "name";
            this._AddEligibleRoles.ListBoxControl.DataValueField = "idRole";
            this._AddEligibleRoles.ListBoxControl.DataBind();

            // add controls to body
            this._AddRolesToUser.AddControlToBody(this._AddEligibleRoles);

            // add modal to container
            this.UserRolesMembershipContainer.Controls.Add(this._AddRolesToUser);
        }
        #endregion

        #region _BuildRemoveRolesFromUserModal
        /// <summary>
        /// Builds the modal for removing roles from the user.
        /// </summary>
        private void _BuildRemoveRolesFromUserModal(string targetControlId)
        {
            // set modal properties
            this._RemoveRolesFromUser = new ModalPopup("RemoveRolesFromUserModal");
            this._RemoveRolesFromUser.Type = ModalPopupType.Form;
            this._RemoveRolesFromUser.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG);
            this._RemoveRolesFromUser.HeaderIconAlt = _GlobalResources.RemoveRole_sFromUser;
            this._RemoveRolesFromUser.HeaderText = _GlobalResources.RemoveRole_sFromUser;
            this._RemoveRolesFromUser.TargetControlID = targetControlId;
            this._RemoveRolesFromUser.SubmitButton.Command += new CommandEventHandler(this._RemoveRolesFromUserSubmit_Command);
            this._RemoveRolesFromUser.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the roles listing
            this._RemoveEligibleRoles = new DynamicListBox("RemoveEligibleRoles");
            this._RemoveEligibleRoles.NoRecordsFoundMessage = _GlobalResources.NoRolesFound;
            this._RemoveEligibleRoles.SearchButton.Command += new CommandEventHandler(this._SearchRemoveRolesButton_Command);
            this._RemoveEligibleRoles.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchRemoveRolesButton_Command);
            this._RemoveEligibleRoles.ListBoxControl.DataSource = this._RemoveEligibleRolesForUser;
            this._RemoveEligibleRoles.ListBoxControl.DataTextField = "name";
            this._RemoveEligibleRoles.ListBoxControl.DataValueField = "idRole";
            this._RemoveEligibleRoles.ListBoxControl.DataBind();

            // add controls to body
            this._RemoveRolesFromUser.AddControlToBody(this._RemoveEligibleRoles);

            // add modal to container
            this.UserRolesMembershipContainer.Controls.Add(this._RemoveRolesFromUser);
        }
        #endregion

        #region _SearchAddRolesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Add Roles" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchAddRolesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._AddRolesToUser.ClearFeedback();

            // clear the listbox control
            this._AddEligibleRoles.ListBoxControl.Items.Clear();

            // do the search
            this._RolesNotForUser = Asentia.UMS.Library.Role.IdsAndNamesForUserSelectList(this._UserObject.Id, this._AddEligibleRoles.SearchTextBox.Text);

            this._SelectAddEligibleRolesDataBind();
        }
        #endregion

        #region _ClearSearchAddRolesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Add Roles" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchAddRolesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._AddRolesToUser.ClearFeedback();

            // clear the listbox control and search text box
            this._AddEligibleRoles.ListBoxControl.Items.Clear();
            this._AddEligibleRoles.SearchTextBox.Text = "";

            // clear the search
            this._RolesNotForUser = Asentia.UMS.Library.Role.IdsAndNamesForUserSelectList(this._UserObject.Id, this._AddEligibleRoles.SearchTextBox.Text);

            this._SelectAddEligibleRolesDataBind();
        }
        #endregion

        #region _SelectAddEligibleRolesDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectAddEligibleRolesDataBind()
        {
            this._AddEligibleRoles.ListBoxControl.DataSource = this._RolesNotForUser;
            this._AddEligibleRoles.ListBoxControl.DataTextField = "name";
            this._AddEligibleRoles.ListBoxControl.DataValueField = "idRole";
            this._AddEligibleRoles.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._AddEligibleRoles.ListBoxControl.Items.Count == 0)
            {
                this._AddRolesToUser.SubmitButton.Enabled = false;
                this._AddRolesToUser.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._AddRolesToUser.SubmitButton.Enabled = true;
                this._AddRolesToUser.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _SearchRemoveRolesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Remove Roles" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchRemoveRolesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._RemoveRolesFromUser.ClearFeedback();

            // clear the listbox control
            this._RemoveEligibleRoles.ListBoxControl.Items.Clear();

            // do the search
            this._RemoveEligibleRolesForUser = this._UserObject.GetRoles(true, this._RemoveEligibleRoles.SearchTextBox.Text);

            this._SelectRemoveEligibleRolesDataBind();
        }
        #endregion

        #region _ClearSearchRemoveRolesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Remove Roles" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchRemoveRolesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._RemoveRolesFromUser.ClearFeedback();

            // clear the listbox control and search text box
            this._RemoveEligibleRoles.ListBoxControl.Items.Clear();
            this._RemoveEligibleRoles.SearchTextBox.Text = "";

            // clear the search
            this._RemoveEligibleRolesForUser = this._UserObject.GetRoles(true, null);

            this._SelectRemoveEligibleRolesDataBind();
        }
        #endregion

        #region _SelectRemoveEligibleRolesDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectRemoveEligibleRolesDataBind()
        {
            this._RemoveEligibleRoles.ListBoxControl.DataSource = this._RemoveEligibleRolesForUser;
            this._RemoveEligibleRoles.ListBoxControl.DataTextField = "name";
            this._RemoveEligibleRoles.ListBoxControl.DataValueField = "idRole";
            this._RemoveEligibleRoles.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._RemoveEligibleRoles.ListBoxControl.Items.Count == 0)
            {
                this._RemoveRolesFromUser.SubmitButton.Enabled = false;
                this._RemoveRolesFromUser.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._RemoveRolesFromUser.SubmitButton.Enabled = true;
                this._RemoveRolesFromUser.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _AddRolesToUserSubmit_Command
        /// <summary>
        /// Click event handler for Add Roles To User modal submit button.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _AddRolesToUserSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get selected roles from the list box
                List<string> selectedRoles = this._AddEligibleRoles.GetSelectedValues();

                // throw exception if no users selected
                if (selectedRoles.Count <= 0)
                { throw new AsentiaException(_GlobalResources.NoRolesSelected); }

                // add selected users to data table
                DataTable rolesToAdd = new DataTable(); ;
                rolesToAdd.Columns.Add("id", typeof(int));

                foreach (string selectedRole in selectedRoles)
                { rolesToAdd.Rows.Add(Convert.ToInt32(selectedRole)); }

                // join the selected roles to the user
                this._UserObject.JoinRoles(rolesToAdd);

                // remove the selected roles from the list box so they cannot be re-selected
                this._AddEligibleRoles.RemoveSelectedItems();

                //Rebind controls and refresh the update panel
                this._BuildRoleMembershipPanel();
                this._RoleListingUpdatePanel.Update();

                // display success feedback
                this._AddRolesToUser.DisplayFeedback(_GlobalResources.Role_sAddedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddRolesToUser.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddRolesToUser.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dfnuEx)
            {
                // display the failure message
                this._AddRolesToUser.DisplayFeedback(dfnuEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddRolesToUser.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AddRolesToUser.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _RemoveRolesFromUserSubmit_Command
        /// <summary>
        /// Click event handler for Remove Roles From User modal submit button.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _RemoveRolesFromUserSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get selected roles from the list box
                List<string> selectedRoles = this._RemoveEligibleRoles.GetSelectedValues();

                // throw exception if no users selected
                if (selectedRoles.Count <= 0)
                { throw new AsentiaException(_GlobalResources.NoRolesSelected); }

                // add selected roles to data table
                DataTable rolesToRemove = new DataTable(); ;
                rolesToRemove.Columns.Add("id", typeof(int));

                foreach (string selectedRole in selectedRoles)
                { rolesToRemove.Rows.Add(Convert.ToInt32(selectedRole)); }

                // remove the selected roles from the user
                this._UserObject.RemoveRoles(rolesToRemove);

                // remove the selected roles from the list box so they cannot be re-selected
                this._RemoveEligibleRoles.RemoveSelectedItems();

                //Rebind controls and refresh the update panel
                this._BuildRoleMembershipPanel();
                this._RoleListingUpdatePanel.Update();

                // display success feedback
                this._RemoveRolesFromUser.DisplayFeedback(_GlobalResources.Role_sRemovedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._RemoveRolesFromUser.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._RemoveRolesFromUser.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dfnuEx)
            {
                // display the failure message
                this._RemoveRolesFromUser.DisplayFeedback(dfnuEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._RemoveRolesFromUser.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._RemoveRolesFromUser.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _BuildUserEffectivePermissionsContainer
        /// <summary>
        /// Builds the container for user's effective permissions.
        /// </summary>
        /// <returns></returns>
        private void _BuildUserEffectivePermissionsContainer()
        {
            // building effective permissions container panel
            Panel permissionsContainer = new Panel();
            permissionsContainer.ID = "PermissionsContainer";

            // get the groups list for group scoped permissions
            DataTable groups = Group.IdsAndNamesForPermissionScopeList();

            // role permissions
            List<Control> rolePermissionsInputControls = new List<Control>();

            // build and attach permissions containers
            rolePermissionsInputControls.Add(this._BuildSystemPermissionsContainer(groups));
            rolePermissionsInputControls.Add(this._BuildUsersAndGroupsPermissionsContainer(groups));
            rolePermissionsInputControls.Add(this._BuildInterfaceAndLayoutPermissionsContainer(groups));
            rolePermissionsInputControls.Add(this._BuildLearningAssetsPermissionsContainer(groups));
            rolePermissionsInputControls.Add(this._BuildReportingPermissionsContainer(groups));

            permissionsContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("RolePermissions",
                                                                                       _GlobalResources.Permissions,
                                                                                       rolePermissionsInputControls,
                                                                                       false,
                                                                                       true));

            this._PermissionsModalBodyPanel.Controls.Add(permissionsContainer);
        }
        #endregion

        #region Methods for building individual permissions containers in effective permissions modal
        #region _BuildSystemPermissionsContainer
        /// <summary>
        /// Builds the container for System permission checkboxes/options.
        /// </summary>
        /// <returns></returns>
        private Panel _BuildSystemPermissionsContainer(DataTable groups)
        {
            // container
            Panel permissionsPanel = new Panel();
            permissionsPanel.ID = "SystemPermissionsContainer";
            permissionsPanel.CssClass = "PermissionsContainer";

            // table
            Table permissionsTable = new Table();

            // header row
            TableRow headerRow = new TableRow();

            // header cell
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = 2;

            // header image and label
            Panel categoryHeaderContainer = new Panel();
            categoryHeaderContainer.CssClass = "PermissionsCategoryHeaderContainer";

            Image categoryImage = new Image();
            categoryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM, ImageFiles.EXT_PNG);
            categoryImage.CssClass = "SmallIcon";
            categoryHeaderContainer.Controls.Add(categoryImage);

            Label categoryLabel = new Label();
            categoryLabel.Text = _GlobalResources.System;
            categoryHeaderContainer.Controls.Add(categoryLabel);

            headerCell.Controls.Add(categoryHeaderContainer);

            headerRow.Cells.Add(headerCell);
            permissionsTable.Rows.Add(headerRow);

            // permissions checkbox rows

            // account settings
            TableRow row1 = new TableRow();
            TableCell row1Cell1 = new TableCell();
            TableCell row1Cell2 = new TableCell();

            this._System_AccountSettings = new CheckBox();
            this._System_AccountSettings.Enabled = false;
            this._System_AccountSettings.ID = "System_AccountSettings";
            this._System_AccountSettings.Text = _GlobalResources.AccountSettingsAllowsAccessToConfigureSettingsForThePortal;
            row1Cell1.Controls.Add(this._System_AccountSettings);

            Panel row1Cell2GlobalLabelContainer = new Panel();
            Literal row1Cell2GlobalLabel = new Literal();
            row1Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row1Cell2GlobalLabelContainer.Controls.Add(row1Cell2GlobalLabel);
            row1Cell2.Controls.Add(row1Cell2GlobalLabelContainer);

            row1.Cells.Add(row1Cell1);
            row1.Cells.Add(row1Cell2);
            permissionsTable.Rows.Add(row1);

            // configuration
            TableRow row2 = new TableRow();
            TableCell row2Cell1 = new TableCell();
            TableCell row2Cell2 = new TableCell();

            this._System_Configuration = new CheckBox();
            this._System_Configuration.Enabled = false;
            this._System_Configuration.ID = "System_Configuration";
            this._System_Configuration.Text = _GlobalResources.ConfigurationAllowsAccessToConfigurationItemsForLoginWidgetsCatalogCourseLeaderboardsAndCourseRatings;
            row2Cell1.Controls.Add(this._System_Configuration);

            Panel row2Cell2GlobalLabelContainer = new Panel();
            Literal row2Cell2GlobalLabel = new Literal();
            row2Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row2Cell2GlobalLabelContainer.Controls.Add(row2Cell2GlobalLabel);
            row2Cell2.Controls.Add(row2Cell2GlobalLabelContainer);

            row2.Cells.Add(row2Cell1);
            row2.Cells.Add(row2Cell2);
            permissionsTable.Rows.Add(row2);

            // user field configuration
            TableRow row10 = new TableRow();
            TableCell row10Cell1 = new TableCell();
            TableCell row10Cell2 = new TableCell();

            this._System_UserFieldConfiguration = new CheckBox();
            this._System_UserFieldConfiguration.Enabled = false;
            this._System_UserFieldConfiguration.ID = "System_UserFieldConfiguration";
            this._System_UserFieldConfiguration.Text = _GlobalResources.UserFieldConfigurationAllowsAccessToConfigureUserFieldPropertiesForUserProfiles;
            row10Cell1.Controls.Add(this._System_UserFieldConfiguration);

            Panel row10Cell2GlobalLabelContainer = new Panel();
            Literal row10Cell2GlobalLabel = new Literal();
            row10Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row10Cell2GlobalLabelContainer.Controls.Add(row10Cell2GlobalLabel);
            row10Cell2.Controls.Add(row10Cell2GlobalLabelContainer);

            row10.Cells.Add(row10Cell1);
            row10.Cells.Add(row10Cell2);
            permissionsTable.Rows.Add(row10);

            // rules engine
            TableRow row12 = new TableRow();
            TableCell row12Cell1 = new TableCell();
            TableCell row12Cell2 = new TableCell();

            this._System_RulesEngine = new CheckBox();
            this._System_RulesEngine.Enabled = false;
            this._System_RulesEngine.ID = "System_RulesEngine";
            this._System_RulesEngine.Text = _GlobalResources.RulesEngineAllowsAccessToRulesEngineOverview;
            row12Cell1.Controls.Add(this._System_RulesEngine);

            Panel row12Cell2GlobalLabelContainer = new Panel();
            Literal row12Cell2GlobalLabel = new Literal();
            row12Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row12Cell2GlobalLabelContainer.Controls.Add(row12Cell2GlobalLabel);
            row12Cell2.Controls.Add(row12Cell2GlobalLabelContainer);

            row12.Cells.Add(row12Cell1);
            row12.Cells.Add(row12Cell2);
            permissionsTable.Rows.Add(row12);

            // ecommerce
            TableRow row3 = new TableRow();
            TableCell row3Cell1 = new TableCell();
            TableCell row3Cell2 = new TableCell();

            this._System_Ecommerce = new CheckBox();
            this._System_Ecommerce.Enabled = false;
            this._System_Ecommerce.ID = "System_Ecommerce";
            this._System_Ecommerce.Text = _GlobalResources.ECommerceAllowsAccessToEcommerceSettingsForThePortal;
            row3Cell1.Controls.Add(this._System_Ecommerce);

            Panel row3Cell2GlobalLabelContainer = new Panel();
            Literal row3Cell2GlobalLabel = new Literal();
            row3Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row3Cell2GlobalLabelContainer.Controls.Add(row3Cell2GlobalLabel);
            row3Cell2.Controls.Add(row3Cell2GlobalLabelContainer);

            row3.Cells.Add(row3Cell1);
            row3.Cells.Add(row3Cell2);
            permissionsTable.Rows.Add(row3);

            // coupon codes
            TableRow row4 = new TableRow();
            TableCell row4Cell1 = new TableCell();
            TableCell row4Cell2 = new TableCell();

            this._System_CouponCodes = new CheckBox();
            this._System_CouponCodes.Enabled = false;
            this._System_CouponCodes.ID = "System_CouponCodes";
            this._System_CouponCodes.Text = _GlobalResources.CouponCodesAllowsAccessToCreateAndModifyCouponCodes;
            row4Cell1.Controls.Add(this._System_CouponCodes);

            Panel row4Cell2GlobalLabelContainer = new Panel();
            Literal row4Cell2GlobalLabel = new Literal();
            row4Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row4Cell2GlobalLabelContainer.Controls.Add(row4Cell2GlobalLabel);
            row4Cell2.Controls.Add(row4Cell2GlobalLabelContainer);

            row4.Cells.Add(row4Cell1);
            row4.Cells.Add(row4Cell2);
            permissionsTable.Rows.Add(row4);

            // tracking codes
            TableRow row5 = new TableRow();
            TableCell row5Cell1 = new TableCell();
            TableCell row5Cell2 = new TableCell();

            this._System_TrackingCodes = new CheckBox();
            this._System_TrackingCodes.Enabled = false;
            this._System_TrackingCodes.ID = "System_TrackingCodes";
            this._System_TrackingCodes.Text = _GlobalResources.TrackingCodesAllowsAccessToConfigureGoogleAnalyticsCode;
            row5Cell1.Controls.Add(this._System_TrackingCodes);

            Panel row5Cell2GlobalLabelContainer = new Panel();
            Literal row5Cell2GlobalLabel = new Literal();
            row5Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row5Cell2GlobalLabelContainer.Controls.Add(row5Cell2GlobalLabel);
            row5Cell2.Controls.Add(row5Cell2GlobalLabelContainer);

            row5.Cells.Add(row5Cell1);
            row5.Cells.Add(row5Cell2);
            permissionsTable.Rows.Add(row5);

            // email notifications
            TableRow row6 = new TableRow();
            TableCell row6Cell1 = new TableCell();
            TableCell row6Cell2 = new TableCell();

            this._System_EmailNotifications = new CheckBox();
            this._System_EmailNotifications.Enabled = false;
            this._System_EmailNotifications.ID = "System_EmailNotifications";
            this._System_EmailNotifications.Text = _GlobalResources.EmailNotificationsAllowsAccessToCreateAndModifyEmailNotificationsAtTheSystemLevel;
            row6Cell1.Controls.Add(this._System_EmailNotifications);

            Panel row6Cell2GlobalLabelContainer = new Panel();
            Literal row6Cell2GlobalLabel = new Literal();
            row6Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row6Cell2GlobalLabelContainer.Controls.Add(row6Cell2GlobalLabel);
            row6Cell2.Controls.Add(row6Cell2GlobalLabelContainer);

            row6.Cells.Add(row6Cell1);
            row6.Cells.Add(row6Cell2);
            permissionsTable.Rows.Add(row6);

            // web meeting integration
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE)
                || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE)
                || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE)
                || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE))
            {
                TableRow row11 = new TableRow();
                TableCell row11Cell1 = new TableCell();
                TableCell row11Cell2 = new TableCell();

                this._System_WebMeetingIntegration = new CheckBox();
                this._System_WebMeetingIntegration.Enabled = false;
                this._System_WebMeetingIntegration.ID = "System_WebMeetingIntegration";
                this._System_WebMeetingIntegration.Text = _GlobalResources.WebMeetingIntegrationAllowsAccessToConfigurationSettingsForIntegrationWithWebMeetingSoftware;
                row10Cell1.Controls.Add(this._System_WebMeetingIntegration);

                Panel row11Cell2GlobalLabelContainer = new Panel();
                Literal row11Cell2GlobalLabel = new Literal();
                row10Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row10Cell2GlobalLabelContainer.Controls.Add(row10Cell2GlobalLabel);
                row10Cell2.Controls.Add(row10Cell2GlobalLabelContainer);

                row10.Cells.Add(row10Cell1);
                row10.Cells.Add(row10Cell2);
                permissionsTable.Rows.Add(row10);
            }

            // api
            TableRow row7 = new TableRow();
            TableCell row7Cell1 = new TableCell();
            TableCell row7Cell2 = new TableCell();

            this._System_API = new CheckBox();
            this._System_API.Enabled = false;
            this._System_API.ID = "System_API";
            this._System_API.Text = _GlobalResources.APIAllowsAccessToConfigureAPISettings;
            row7Cell1.Controls.Add(this._System_API);

            Panel row7Cell2GlobalLabelContainer = new Panel();
            Literal row7Cell2GlobalLabel = new Literal();
            row7Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row7Cell2GlobalLabelContainer.Controls.Add(row7Cell2GlobalLabel);
            row7Cell2.Controls.Add(row7Cell2GlobalLabelContainer);

            row7.Cells.Add(row7Cell1);
            row7.Cells.Add(row7Cell2);
            permissionsTable.Rows.Add(row7);

            // xapi endpoints
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.XAPIENDPOINTS_ENABLE))
            {
                TableRow row8 = new TableRow();
                TableCell row8Cell1 = new TableCell();
                TableCell row8Cell2 = new TableCell();

                this._System_xAPIEndpoints = new CheckBox();
                this._System_xAPIEndpoints.Enabled = false;
                this._System_xAPIEndpoints.ID = "System_xAPIEndpoints";
                this._System_xAPIEndpoints.Text = _GlobalResources.xAPIEndpointsAllowsAccessToConfigureEndpointsForxAPI;
                row8Cell1.Controls.Add(this._System_xAPIEndpoints);

                Panel row8Cell2GlobalLabelContainer = new Panel();
                Literal row8Cell2GlobalLabel = new Literal();
                row8Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row8Cell2GlobalLabelContainer.Controls.Add(row8Cell2GlobalLabel);
                row8Cell2.Controls.Add(row8Cell2GlobalLabelContainer);

                row8.Cells.Add(row8Cell1);
                row8.Cells.Add(row8Cell2);
                permissionsTable.Rows.Add(row8);
            }

            // logs
            TableRow row9 = new TableRow();
            TableCell row9Cell1 = new TableCell();
            TableCell row9Cell2 = new TableCell();

            this._System_Logs = new CheckBox();
            this._System_Logs.Enabled = false;
            this._System_Logs.ID = "System_Logs";
            this._System_Logs.Text = _GlobalResources.LogsAllowsAccessToSystemLogs;
            row9Cell1.Controls.Add(this._System_Logs);

            Panel row9Cell2GlobalLabelContainer = new Panel();
            Literal row9Cell2GlobalLabel = new Literal();
            row9Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row9Cell2GlobalLabelContainer.Controls.Add(row9Cell2GlobalLabel);
            row9Cell2.Controls.Add(row9Cell2GlobalLabelContainer);

            row9.Cells.Add(row9Cell1);
            row9.Cells.Add(row9Cell2);
            permissionsTable.Rows.Add(row9);

            // attach the table and return
            permissionsPanel.Controls.Add(permissionsTable);
            return permissionsPanel;
        }
        #endregion

        #region _BuildUsersAndGroupsPermissionsContainer
        /// <summary>
        /// Builds the container for Users & Groups permission checkboxes/options.
        /// </summary>
        /// <returns></returns>
        private Panel _BuildUsersAndGroupsPermissionsContainer(DataTable groups)
        {
            // container
            Panel permissionsPanel = new Panel();
            permissionsPanel.ID = "UsersAndGroupsPermissionsContainer";
            permissionsPanel.CssClass = "PermissionsContainer";

            // table
            Table permissionsTable = new Table();

            // header row
            TableRow headerRow = new TableRow();

            // header cell
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = 2;

            // header image and label
            Panel categoryHeaderContainer = new Panel();
            categoryHeaderContainer.CssClass = "PermissionsCategoryHeaderContainer";

            Image categoryImage = new Image();
            categoryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERSANDGROUPS, ImageFiles.EXT_PNG);
            categoryImage.CssClass = "SmallIcon";
            categoryHeaderContainer.Controls.Add(categoryImage);

            Label categoryLabel = new Label();
            categoryLabel.Text = _GlobalResources.UsersAndGroups;
            categoryHeaderContainer.Controls.Add(categoryLabel);

            headerCell.Controls.Add(categoryHeaderContainer);

            headerRow.Cells.Add(headerCell);
            permissionsTable.Rows.Add(headerRow);

            // permissions checkbox rows

            // user registration approval
             if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERREGISTRATIONAPPROVAL_ENABLE))
             {
                 TableRow row15 = new TableRow();
                 TableCell row15Cell1 = new TableCell();
                 TableCell row15Cell2 = new TableCell();

                 this._UsersAndGroups_UserRegistrationApproval = new CheckBox();
                 this._UsersAndGroups_UserRegistrationApproval.Enabled = false;
                 this._UsersAndGroups_UserRegistrationApproval.ID = "UsersAndGroups_UserRegistrationApproval";
                 this._UsersAndGroups_UserRegistrationApproval.Text = _GlobalResources.UserRegistrationApproverAllowsTheAbilityToApproveOrRejectUserRegistrations;
                 row15Cell1.Controls.Add(this._UsersAndGroups_UserRegistrationApproval);

                 Panel row15Cell2GlobalLabelContainer = new Panel();
                 Literal row15Cell2GlobalLabel = new Literal();
                 row15Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                 row15Cell2GlobalLabelContainer.Controls.Add(row15Cell2GlobalLabel);
                 row15Cell2.Controls.Add(row15Cell2GlobalLabelContainer);

                 row15.Cells.Add(row15Cell1);
                 row15.Cells.Add(row15Cell2);
                 permissionsTable.Rows.Add(row15);
             }

            // user creator
            TableRow row2 = new TableRow();
            TableCell row2Cell1 = new TableCell();
            TableCell row2Cell2 = new TableCell();

            this._UsersAndGroups_UserCreator = new CheckBox();
            this._UsersAndGroups_UserCreator.Enabled = false;
            this._UsersAndGroups_UserCreator.ID = "UsersAndGroups_UserCreator";
            this._UsersAndGroups_UserCreator.Text = _GlobalResources.UserCreatorAllowsToCreateNewUserAccounts;
            row2Cell1.Controls.Add(this._UsersAndGroups_UserCreator);

            Panel row2Cell2GlobalLabelContainer = new Panel();
            Literal row2Cell2GlobalLabel = new Literal();
            row2Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row2Cell2GlobalLabelContainer.Controls.Add(row2Cell2GlobalLabel);
            row2Cell2.Controls.Add(row2Cell2GlobalLabelContainer);

            row2.Cells.Add(row2Cell1);
            row2.Cells.Add(row2Cell2);
            permissionsTable.Rows.Add(row2);

            // user deleter
            TableRow row3 = new TableRow();
            TableCell row3Cell1 = new TableCell();
            TableCell row3Cell2 = new TableCell();

            this._UsersAndGroups_UserDeleter = new CheckBox();
            this._UsersAndGroups_UserDeleter.Enabled = false;
            this._UsersAndGroups_UserDeleter.ID = "UsersAndGroups_UserDeleter";
            this._UsersAndGroups_UserDeleter.Text = _GlobalResources.UserDeleterAllowsAccessToDeleteUserAccounts;
            row3Cell1.Controls.Add(this._UsersAndGroups_UserDeleter);

            // defined scope
            row3Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_UserDeleter", _GlobalResources.ThisUserMayDelete, groups));

            row3.Cells.Add(row3Cell1);
            row3.Cells.Add(row3Cell2);
            permissionsTable.Rows.Add(row3);

            // user editor
            TableRow row4 = new TableRow();
            TableCell row4Cell1 = new TableCell();
            TableCell row4Cell2 = new TableCell();

            this._UsersAndGroups_UserEditor = new CheckBox();
            this._UsersAndGroups_UserEditor.Enabled = false;
            this._UsersAndGroups_UserEditor.ID = "UsersAndGroups_UserEditor";
            this._UsersAndGroups_UserEditor.Text = _GlobalResources.UserEditorAllowsAccessToModifyUserAccountData;
            row4Cell1.Controls.Add(this._UsersAndGroups_UserEditor);

            // defined scope
            row4Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_UserEditor", _GlobalResources.ThisUserMayEdit, groups));

            row4.Cells.Add(row4Cell1);
            row4.Cells.Add(row4Cell2);
            permissionsTable.Rows.Add(row4);

            // user manager
            TableRow row5 = new TableRow();
            TableCell row5Cell1 = new TableCell();
            TableCell row5Cell2 = new TableCell();

            this._UsersAndGroups_UserManager = new CheckBox();
            this._UsersAndGroups_UserManager.Enabled = false;
            this._UsersAndGroups_UserManager.ID = "UsersAndGroups_UserManager";
            this._UsersAndGroups_UserManager.Text = _GlobalResources.UserManagerAllowsAccessToPerformUserManagementFunctionsSuchAsAssigningAndRevokingUserEnrollmentsAwardingAndRevokingCertificatesAndImportingExternalTrainingHistories;
            row5Cell1.Controls.Add(this._UsersAndGroups_UserManager);

            // defined scope
            row5Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_UserManager", _GlobalResources.ThisUserMayManage, groups));

            row5.Cells.Add(row5Cell1);
            row5.Cells.Add(row5Cell2);
            permissionsTable.Rows.Add(row5);

            // user impersonator
            TableRow row6 = new TableRow();
            TableCell row6Cell1 = new TableCell();
            TableCell row6Cell2 = new TableCell();

            this._UsersAndGroups_UserImpersonator = new CheckBox();
            this._UsersAndGroups_UserImpersonator.Enabled = false;
            this._UsersAndGroups_UserImpersonator.ID = "UsersAndGroups_UserImpersonator";
            this._UsersAndGroups_UserImpersonator.Text = _GlobalResources.UserImpersonatorAllowsAccessToImpersonateLoginAsOtherUsers;
            row6Cell1.Controls.Add(this._UsersAndGroups_UserImpersonator);

            // defined scope
            row6Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_UserImpersonator", _GlobalResources.ThisUserMayImpersonate, groups));

            row6.Cells.Add(row6Cell1);
            row6.Cells.Add(row6Cell2);
            permissionsTable.Rows.Add(row6);

            // group creator
            TableRow row7 = new TableRow();
            TableCell row7Cell1 = new TableCell();
            TableCell row7Cell2 = new TableCell();

            this._UsersAndGroups_GroupCreator = new CheckBox();
            this._UsersAndGroups_GroupCreator.Enabled = false;
            this._UsersAndGroups_GroupCreator.ID = "UsersAndGroups_GroupCreator";
            this._UsersAndGroups_GroupCreator.Text = _GlobalResources.GroupCreatorAllowsToCreateNewGroups;
            row7Cell1.Controls.Add(this._UsersAndGroups_GroupCreator);

            Panel row7Cell2GlobalLabelContainer = new Panel();
            Literal row7Cell2GlobalLabel = new Literal();
            row7Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row7Cell2GlobalLabelContainer.Controls.Add(row7Cell2GlobalLabel);
            row7Cell2.Controls.Add(row7Cell2GlobalLabelContainer);

            row7.Cells.Add(row7Cell1);
            row7.Cells.Add(row7Cell2);
            permissionsTable.Rows.Add(row7);

            // group deleter
            TableRow row8 = new TableRow();
            TableCell row8Cell1 = new TableCell();
            TableCell row8Cell2 = new TableCell();

            this._UsersAndGroups_GroupDeleter = new CheckBox();
            this._UsersAndGroups_GroupDeleter.Enabled = false;
            this._UsersAndGroups_GroupDeleter.ID = "UsersAndGroups_GroupDeleter";
            this._UsersAndGroups_GroupDeleter.Text = _GlobalResources.GroupDeleterAllowsAccessToDeleteGroups;
            row8Cell1.Controls.Add(this._UsersAndGroups_GroupDeleter);

            Panel row8Cell2GlobalLabelContainer = new Panel();
            Literal row8Cell2GlobalLabel = new Literal();
            row8Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row8Cell2GlobalLabelContainer.Controls.Add(row8Cell2GlobalLabel);
            row8Cell2.Controls.Add(row8Cell2GlobalLabelContainer);

            row8.Cells.Add(row8Cell1);
            row8.Cells.Add(row8Cell2);
            permissionsTable.Rows.Add(row8);

            // group editor
            TableRow row9 = new TableRow();
            TableCell row9Cell1 = new TableCell();
            TableCell row9Cell2 = new TableCell();

            this._UsersAndGroups_GroupEditor = new CheckBox();
            this._UsersAndGroups_GroupEditor.Enabled = false;
            this._UsersAndGroups_GroupEditor.ID = "UsersAndGroups_GroupEditor";
            this._UsersAndGroups_GroupEditor.Text = _GlobalResources.GroupEditorAllowsAccessToModifyGroupProperties;
            row9Cell1.Controls.Add(this._UsersAndGroups_GroupEditor);

            // defined scope
            row9Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_GroupEditor", _GlobalResources.ThisUserMayEdit, groups, false));

            row9.Cells.Add(row9Cell1);
            row9.Cells.Add(row9Cell2);
            permissionsTable.Rows.Add(row9);

            // group manager
            TableRow row10 = new TableRow();
            TableCell row10Cell1 = new TableCell();
            TableCell row10Cell2 = new TableCell();

            this._UsersAndGroups_GroupManager = new CheckBox();
            this._UsersAndGroups_GroupManager.Enabled = false;
            this._UsersAndGroups_GroupManager.ID = "UsersAndGroups_GroupManager";
            this._UsersAndGroups_GroupManager.Text = _GlobalResources.GroupManagerAllowsAccessToPerformGroupManagementFunctionsSuchAsAddingAndRemovingUsersFromGroupsAssigningAndRevokingGroupEnrollmentsCreatingAndModifyingAutoJoinRulesAndManagingGroupDiscussions;
            row10Cell1.Controls.Add(this._UsersAndGroups_GroupManager);

            // defined scope
            row10Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_GroupManager", _GlobalResources.ThisUserMayManage, groups, false));

            row10.Cells.Add(row10Cell1);
            row10.Cells.Add(row10Cell2);
            permissionsTable.Rows.Add(row10);

            // role manager
            TableRow row11 = new TableRow();
            TableCell row11Cell1 = new TableCell();
            TableCell row11Cell2 = new TableCell();

            this._UsersAndGroups_RoleManager = new CheckBox();
            this._UsersAndGroups_RoleManager.Enabled = false;
            this._UsersAndGroups_RoleManager.ID = "UsersAndGroups_RoleManager";
            this._UsersAndGroups_RoleManager.Text = _GlobalResources.RoleManagerAllowsAccessToPerformRoleManagementFunctionsSuchAsCreatingAndModifyingRolesCreatingAndModifyingAutoJoinRulesAndAssigningRolesForUsersAndGroups;
            row11Cell1.Controls.Add(this._UsersAndGroups_RoleManager);

            Panel row11Cell2GlobalLabelContainer = new Panel();
            Literal row11Cell2GlobalLabel = new Literal();
            row11Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row11Cell2GlobalLabelContainer.Controls.Add(row11Cell2GlobalLabel);
            row11Cell2.Controls.Add(row11Cell2GlobalLabelContainer);

            row11.Cells.Add(row11Cell1);
            row11.Cells.Add(row11Cell2);
            permissionsTable.Rows.Add(row11);

            // activity import
            TableRow row12 = new TableRow();
            TableCell row12Cell1 = new TableCell();
            TableCell row12Cell2 = new TableCell();

            this._UsersAndGroups_ActivityImport = new CheckBox();
            this._UsersAndGroups_ActivityImport.Enabled = false;
            this._UsersAndGroups_ActivityImport.ID = "UsersAndGroups_ActivityImport";
            this._UsersAndGroups_ActivityImport.Text = _GlobalResources.ActivityImportAllowsAccessToUploadAndManageExternalTrainingHistoriesForMultipleUsers;
            row12Cell1.Controls.Add(this._UsersAndGroups_ActivityImport);

            Panel row12Cell2GlobalLabelContainer = new Panel();
            Literal row12Cell2GlobalLabel = new Literal();
            row12Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row12Cell2GlobalLabelContainer.Controls.Add(row12Cell2GlobalLabel);
            row12Cell2.Controls.Add(row12Cell2GlobalLabelContainer);

            row12.Cells.Add(row12Cell1);
            row12.Cells.Add(row12Cell2);
            permissionsTable.Rows.Add(row12);

            // certificate import
            TableRow row13 = new TableRow();
            TableCell row13Cell1 = new TableCell();
            TableCell row13Cell2 = new TableCell();

            this._UsersAndGroups_CertificateImport = new CheckBox();
            this._UsersAndGroups_CertificateImport.Enabled = false;
            this._UsersAndGroups_CertificateImport.ID = "UsersAndGroups_CertificateImport";
            this._UsersAndGroups_CertificateImport.Text = _GlobalResources.CertificateImportAllowsAccessToUploadAndManageCertificateHistoriesForMultipleUsers;
            row13Cell1.Controls.Add(this._UsersAndGroups_CertificateImport);

            Panel row13Cell2GlobalLabelContainer = new Panel();
            Literal row13Cell2GlobalLabel = new Literal();
            row13Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row13Cell2GlobalLabelContainer.Controls.Add(row13Cell2GlobalLabel);
            row13Cell2.Controls.Add(row13Cell2GlobalLabelContainer);

            row13.Cells.Add(row13Cell1);
            row13.Cells.Add(row13Cell2);
            permissionsTable.Rows.Add(row13);

            // leaderboards
            TableRow row14 = new TableRow();
            TableCell row14Cell1 = new TableCell();
            TableCell row14Cell2 = new TableCell();

            this._UsersAndGroups_Leaderboards = new CheckBox();
            this._UsersAndGroups_Leaderboards.Enabled = false;
            this._UsersAndGroups_Leaderboards.ID = "UsersAndGroups_Leaderboards";
            this._UsersAndGroups_Leaderboards.Text = _GlobalResources.TeamLeaderboardsUsersWithThisPermissionCanViewLeaderboards;
            row14Cell1.Controls.Add(this._UsersAndGroups_Leaderboards);

            Panel row14Cell2GlobalLabelContainer = new Panel();
            Literal row14Cell2GlobalLabel = new Literal();
            row14Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row14Cell2GlobalLabelContainer.Controls.Add(row14Cell2GlobalLabel);
            row14Cell2.Controls.Add(row14Cell2GlobalLabelContainer);

            row14.Cells.Add(row14Cell1);
            row14.Cells.Add(row14Cell2);
            permissionsTable.Rows.Add(row14);

            // attach the table and return
            permissionsPanel.Controls.Add(permissionsTable);
            return permissionsPanel;
        }
        #endregion

        #region _BuildInterfaceAndLayoutPermissionsContainer
        /// <summary>
        /// Builds the container for Interface & Layout permission checkboxes/options.
        /// </summary>
        /// <returns></returns>
        private Panel _BuildInterfaceAndLayoutPermissionsContainer(DataTable groups)
        {
            // container
            Panel permissionsPanel = new Panel();
            permissionsPanel.ID = "InterfaceAndLayoutPermissionsContainer";
            permissionsPanel.CssClass = "PermissionsContainer";

            // table
            Table permissionsTable = new Table();

            // header row
            TableRow headerRow = new TableRow();

            // header cell
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = 2;

            // header image and label
            Panel categoryHeaderContainer = new Panel();
            categoryHeaderContainer.CssClass = "PermissionsCategoryHeaderContainer";

            Image categoryImage = new Image();
            categoryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_APPLICATION, ImageFiles.EXT_PNG);
            categoryImage.CssClass = "SmallIcon";
            categoryHeaderContainer.Controls.Add(categoryImage);

            Label categoryLabel = new Label();
            categoryLabel.Text = _GlobalResources.InterfaceAndLayout;
            categoryHeaderContainer.Controls.Add(categoryLabel);

            headerCell.Controls.Add(categoryHeaderContainer);

            headerRow.Cells.Add(headerCell);
            permissionsTable.Rows.Add(headerRow);

            // permissions checkbox rows

            // home page
            TableRow row1 = new TableRow();
            TableCell row1Cell1 = new TableCell();
            TableCell row1Cell2 = new TableCell();

            this._InterfaceAndLayout_HomePage = new CheckBox();
            this._InterfaceAndLayout_HomePage.Enabled = false;
            this._InterfaceAndLayout_HomePage.ID = "InterfaceAndLayout_HomePage";
            this._InterfaceAndLayout_HomePage.Text = _GlobalResources.HomePageAllowsAccessToModifyThePortalsHomePage;
            row1Cell1.Controls.Add(this._InterfaceAndLayout_HomePage);

            Panel row1Cell2GlobalLabelContainer = new Panel();
            Literal row1Cell2GlobalLabel = new Literal();
            row1Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row1Cell2GlobalLabelContainer.Controls.Add(row1Cell2GlobalLabel);
            row1Cell2.Controls.Add(row1Cell2GlobalLabelContainer);

            row1.Cells.Add(row1Cell1);
            row1.Cells.Add(row1Cell2);
            permissionsTable.Rows.Add(row1);

            // masthead
            TableRow row2 = new TableRow();
            TableCell row2Cell1 = new TableCell();
            TableCell row2Cell2 = new TableCell();

            this._InterfaceAndLayout_Masthead = new CheckBox();
            this._InterfaceAndLayout_Masthead.Enabled = false;
            this._InterfaceAndLayout_Masthead.ID = "InterfaceAndLayout_Masthead";
            this._InterfaceAndLayout_Masthead.Text = _GlobalResources.MastheadAllowsAccessToModifyThePortalsMasthead;
            row2Cell1.Controls.Add(this._InterfaceAndLayout_Masthead);

            Panel row2Cell2GlobalLabelContainer = new Panel();
            Literal row2Cell2GlobalLabel = new Literal();
            row2Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row2Cell2GlobalLabelContainer.Controls.Add(row2Cell2GlobalLabel);
            row2Cell2.Controls.Add(row2Cell2GlobalLabelContainer);

            row2.Cells.Add(row2Cell1);
            row2.Cells.Add(row2Cell2);
            permissionsTable.Rows.Add(row2);

            // footer
            TableRow row3 = new TableRow();
            TableCell row3Cell1 = new TableCell();
            TableCell row3Cell2 = new TableCell();

            this._InterfaceAndLayout_Footer = new CheckBox();
            this._InterfaceAndLayout_Footer.Enabled = false;
            this._InterfaceAndLayout_Footer.ID = "InterfaceAndLayout_Footer";
            this._InterfaceAndLayout_Footer.Text = _GlobalResources.FooterAllowsAccessToModifyThePortalsFooter;
            row3Cell1.Controls.Add(this._InterfaceAndLayout_Footer);

            Panel row3Cell2GlobalLabelContainer = new Panel();
            Literal row3Cell2GlobalLabel = new Literal();
            row3Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row3Cell2GlobalLabelContainer.Controls.Add(row3Cell2GlobalLabel);
            row3Cell2.Controls.Add(row3Cell2GlobalLabelContainer);

            row3.Cells.Add(row3Cell1);
            row3.Cells.Add(row3Cell2);
            permissionsTable.Rows.Add(row3);

            // css editor
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CSSEDITOR_ENABLE))
            {
                TableRow row4 = new TableRow();
                TableCell row4Cell1 = new TableCell();
                TableCell row4Cell2 = new TableCell();

                this._InterfaceAndLayout_CSSEditor = new CheckBox();
                this._InterfaceAndLayout_CSSEditor.Enabled = false;
                this._InterfaceAndLayout_CSSEditor.ID = "InterfaceAndLayout_CSSEditor";
                this._InterfaceAndLayout_CSSEditor.Text = _GlobalResources.CSSEditorAllowsAccessToModifyCSSFilesForThePortal;
                row4Cell1.Controls.Add(this._InterfaceAndLayout_CSSEditor);

                Panel row4Cell2GlobalLabelContainer = new Panel();
                Literal row4Cell2GlobalLabel = new Literal();
                row4Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row4Cell2GlobalLabelContainer.Controls.Add(row4Cell2GlobalLabel);
                row4Cell2.Controls.Add(row4Cell2GlobalLabelContainer);

                row4.Cells.Add(row4Cell1);
                row4.Cells.Add(row4Cell2);
                permissionsTable.Rows.Add(row4);
            } 

            // image editor
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.IMAGE_EDITOR_ENABLE))
            {
                TableRow row5 = new TableRow();
                TableCell row5Cell1 = new TableCell();
                TableCell row5Cell2 = new TableCell();

                this._InterfaceAndLayout_ImageEditor = new CheckBox();
                this._InterfaceAndLayout_ImageEditor.Enabled = false;
                this._InterfaceAndLayout_ImageEditor.ID = "InterfaceAndLayout_ImageEditor";
                this._InterfaceAndLayout_ImageEditor.Text = _GlobalResources.ImageEditorAllowsAccessToModifyIconsAndImagesForThePortal;
                row5Cell1.Controls.Add(this._InterfaceAndLayout_ImageEditor);

                Panel row5Cell2GlobalLabelContainer = new Panel();
                Literal row5Cell2GlobalLabel = new Literal();
                row5Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row5Cell2GlobalLabelContainer.Controls.Add(row5Cell2GlobalLabel);
                row5Cell2.Controls.Add(row5Cell2GlobalLabelContainer);

                row5.Cells.Add(row5Cell1);
                row5.Cells.Add(row5Cell2);
                permissionsTable.Rows.Add(row5);
            }

            // attach the table and return
            permissionsPanel.Controls.Add(permissionsTable);
            return permissionsPanel;
        }
        #endregion

        #region _BuildLearningAssetsPermissionsContainer
        /// <summary>
        /// Builds the container for Learning Assets permission checkboxes/options.
        /// </summary>
        /// <returns></returns>
        private Panel _BuildLearningAssetsPermissionsContainer(DataTable groups)
        {
            // container
            Panel permissionsPanel = new Panel();
            permissionsPanel.ID = "LearningAssetsPermissionsContainer";
            permissionsPanel.CssClass = "PermissionsContainer";

            // table
            Table permissionsTable = new Table();

            // header row
            TableRow headerRow = new TableRow();

            // header cell
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = 2;

            // header image and label
            Panel categoryHeaderContainer = new Panel();
            categoryHeaderContainer.CssClass = "PermissionsCategoryHeaderContainer";

            Image categoryImage = new Image();
            categoryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGASSETS, ImageFiles.EXT_PNG);
            categoryImage.CssClass = "SmallIcon";
            categoryHeaderContainer.Controls.Add(categoryImage);

            Label categoryLabel = new Label();
            categoryLabel.Text = _GlobalResources.LearningAssets;
            categoryHeaderContainer.Controls.Add(categoryLabel);

            headerCell.Controls.Add(categoryHeaderContainer);

            headerRow.Cells.Add(headerCell);
            permissionsTable.Rows.Add(headerRow);

            // permissions checkbox rows

            // course catalog
            TableRow row1 = new TableRow();
            TableCell row1Cell1 = new TableCell();
            TableCell row1Cell2 = new TableCell();

            this._LearningAssets_CourseCatalog = new CheckBox();
            this._LearningAssets_CourseCatalog.Enabled = false;
            this._LearningAssets_CourseCatalog.ID = "LearningAssets_CourseCatalog";
            this._LearningAssets_CourseCatalog.Text = _GlobalResources.CourseCatalogAllowsAccessToManageThePortalsCourseCatalog;
            row1Cell1.Controls.Add(this._LearningAssets_CourseCatalog);

            Panel row1Cell2GlobalLabelContainer = new Panel();
            Literal row1Cell2GlobalLabel = new Literal();
            row1Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row1Cell2GlobalLabelContainer.Controls.Add(row1Cell2GlobalLabel);
            row1Cell2.Controls.Add(row1Cell2GlobalLabelContainer);

            row1.Cells.Add(row1Cell1);
            row1.Cells.Add(row1Cell2);
            permissionsTable.Rows.Add(row1);

            // self-enrollment approval
             if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
             {
                 TableRow row10 = new TableRow();
                 TableCell row10Cell1 = new TableCell();
                 TableCell row10Cell2 = new TableCell();

                 this._LearningAssets_SelfEnrollmentApproval = new CheckBox();
                 this._LearningAssets_SelfEnrollmentApproval.Enabled = false;
                 this._LearningAssets_SelfEnrollmentApproval.ID = "LearningAssets_SelfEnrollmentApproval";
                 this._LearningAssets_SelfEnrollmentApproval.Text = _GlobalResources.SelfEnrollmentApproverAllowsTheAbilityToApproveOrRejectSelfEnrollmentsForCoursesThatRequireSelfEnrollmentApproval;
                 row10Cell1.Controls.Add(this._LearningAssets_SelfEnrollmentApproval);

                 Panel row10Cell2GlobalLabelContainer = new Panel();
                 Literal row10Cell2GlobalLabel = new Literal();
                 row10Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                 row10Cell2GlobalLabelContainer.Controls.Add(row10Cell2GlobalLabel);
                 row10Cell2.Controls.Add(row10Cell2GlobalLabelContainer);

                 row10.Cells.Add(row10Cell1);
                 row10.Cells.Add(row10Cell2);
                 permissionsTable.Rows.Add(row10);
             }

            // course content manager
            TableRow row2 = new TableRow();
            TableCell row2Cell1 = new TableCell();
            TableCell row2Cell2 = new TableCell();

            this._LearningAssets_CourseContentManager = new CheckBox();
            this._LearningAssets_CourseContentManager.Enabled = false;
            this._LearningAssets_CourseContentManager.ID = "LearningAssets_CourseContentManager";
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE))
            {
                this._LearningAssets_CourseContentManager.Text = _GlobalResources.CourseContentManagerAllowsAccessToCreateAndModifyCoursesManageCourseSpecificEmailNotificationsAndCertificatesAndManageCourseDiscussions;
            }
            else
            {
                this._LearningAssets_CourseContentManager.Text = _GlobalResources.CourseContentManagerAllowsAccessToCreateAndModifyCoursesManageCourseSpecificCertificatesAndManageCourseDiscussions;
            }
            row2Cell1.Controls.Add(this._LearningAssets_CourseContentManager);

            Panel row2Cell2GlobalLabelContainer = new Panel();
            Literal row2Cell2GlobalLabel = new Literal();
            row2Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row2Cell2GlobalLabelContainer.Controls.Add(row2Cell2GlobalLabel);
            row2Cell2.Controls.Add(row2Cell2GlobalLabelContainer);

            row2.Cells.Add(row2Cell1);
            row2.Cells.Add(row2Cell2);
            permissionsTable.Rows.Add(row2);

            // course enrollment manager
            TableRow row3 = new TableRow();
            TableCell row3Cell1 = new TableCell();
            TableCell row3Cell2 = new TableCell();

            this._LearningAssets_CourseEnrollmentManager = new CheckBox();
            this._LearningAssets_CourseEnrollmentManager.Enabled = false;
            this._LearningAssets_CourseEnrollmentManager.ID = "LearningAssets_CourseEnrollmentManager";
            this._LearningAssets_CourseEnrollmentManager.Text = _GlobalResources.CourseEnrollmentManagerAllowsAccessToCreateCourseEnrollmentsAndEnrollmentRules;
            row3Cell1.Controls.Add(this._LearningAssets_CourseEnrollmentManager);

            Panel row3Cell2GlobalLabelContainer = new Panel();
            Literal row3Cell2GlobalLabel = new Literal();
            row3Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row3Cell2GlobalLabelContainer.Controls.Add(row3Cell2GlobalLabel);
            row3Cell2.Controls.Add(row3Cell2GlobalLabelContainer);

            row3.Cells.Add(row3Cell1);
            row3.Cells.Add(row3Cell2);
            permissionsTable.Rows.Add(row3);

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                // learning path content manager
                TableRow row4 = new TableRow();
                TableCell row4Cell1 = new TableCell();
                TableCell row4Cell2 = new TableCell();

                this._LearningAssets_LearningPathContentManager = new CheckBox();
                this._LearningAssets_LearningPathContentManager.Enabled = false;
                this._LearningAssets_LearningPathContentManager.ID = "LearningAssets_LearningPathContentManager";
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE))
                {
                    this._LearningAssets_LearningPathContentManager.Text = _GlobalResources.LearningPathContentManagerAllowsAccessToCreateAndModifyLearningPathsAndManageLearningPathSpecificEmailNotificationsAndCertificates;
                }
                else
                {
                    this._LearningAssets_LearningPathContentManager.Text = _GlobalResources.LearningPathContentManagerAllowsAccessToCreateAndModifyLearningPathsAndManageLearningPathSpecificCertificates;
                }
                row4Cell1.Controls.Add(this._LearningAssets_LearningPathContentManager);

                Panel row4Cell2GlobalLabelContainer = new Panel();
                Literal row4Cell2GlobalLabel = new Literal();
                row4Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row4Cell2GlobalLabelContainer.Controls.Add(row4Cell2GlobalLabel);
                row4Cell2.Controls.Add(row4Cell2GlobalLabelContainer);

                row4.Cells.Add(row4Cell1);
                row4.Cells.Add(row4Cell2);
                permissionsTable.Rows.Add(row4);

                // learning path enrollment manager
                TableRow row5 = new TableRow();
                TableCell row5Cell1 = new TableCell();
                TableCell row5Cell2 = new TableCell();

                this._LearningAssets_LearningPathEnrollmentManager = new CheckBox();
                this._LearningAssets_LearningPathEnrollmentManager.Enabled = false;
                this._LearningAssets_LearningPathEnrollmentManager.ID = "LearningAssets_LearningPathEnrollmentManager";
                this._LearningAssets_LearningPathEnrollmentManager.Text = _GlobalResources.LearningPathEnrollmentManagerAllowsAccessToCreateLearningPathEnrollmentsAndEnrollmentRules;
                row5Cell1.Controls.Add(this._LearningAssets_LearningPathEnrollmentManager);

                Panel row5Cell2GlobalLabelContainer = new Panel();
                Literal row5Cell2GlobalLabel = new Literal();
                row5Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row5Cell2GlobalLabelContainer.Controls.Add(row5Cell2GlobalLabel);
                row5Cell2.Controls.Add(row5Cell2GlobalLabelContainer);

                row5.Cells.Add(row5Cell1);
                row5.Cells.Add(row5Cell2);
                permissionsTable.Rows.Add(row5);
            }

            // certifications manager
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
            {
                TableRow row6 = new TableRow();
                TableCell row6Cell1 = new TableCell();
                TableCell row6Cell2 = new TableCell();

                this._LearningAssets_CertificationsManager = new CheckBox();
                this._LearningAssets_CertificationsManager.Enabled = false;
                this._LearningAssets_CertificationsManager.ID = "LearningAssets_CertificationsManager";
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE))
                {
                    this._LearningAssets_CertificationsManager.Text = _GlobalResources.CertificationsManagerAllowsAccessToCreateAndModifyCertificationsCreateCertificationEnrollmentsAndEnrollmentRulesAndManageCertificationSpecificEmailNotifications;
                }
                else
                {
                    this._LearningAssets_CertificationsManager.Text = _GlobalResources.CertificationsManagerAllowsAccessToCreateAndModifyCertificationsCreateCertificationEnrollmentsAndEnrollmentRules;
                }
                row6Cell1.Controls.Add(this._LearningAssets_CertificationsManager);

                Panel row6Cell2GlobalLabelContainer = new Panel();
                Literal row6Cell2GlobalLabel = new Literal();
                row6Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row6Cell2GlobalLabelContainer.Controls.Add(row6Cell2GlobalLabel);
                row6Cell2.Controls.Add(row6Cell2GlobalLabelContainer);

                row6.Cells.Add(row6Cell1);
                row6.Cells.Add(row6Cell2);
                permissionsTable.Rows.Add(row6);
            }

            // content package manager
            TableRow row7 = new TableRow();
            TableCell row7Cell1 = new TableCell();
            TableCell row7Cell2 = new TableCell();

            this._LearningAssets_ContentPackageManager = new CheckBox();
            this._LearningAssets_ContentPackageManager.Enabled = false;
            this._LearningAssets_ContentPackageManager.ID = "LearningAssets_ContentPackageManager";
            this._LearningAssets_ContentPackageManager.Text = _GlobalResources.ContentPackageManagerAllowsAccessToUploadAndManageContentPackages;
            row7Cell1.Controls.Add(this._LearningAssets_ContentPackageManager);

            Panel row7Cell2GlobalLabelContainer = new Panel();
            Literal row7Cell2GlobalLabel = new Literal();
            row7Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row7Cell2GlobalLabelContainer.Controls.Add(row7Cell2GlobalLabel);
            row7Cell2.Controls.Add(row7Cell2GlobalLabelContainer);

            row7.Cells.Add(row7Cell1);
            row7.Cells.Add(row7Cell2);
            permissionsTable.Rows.Add(row7);

            // quiz and survey manager
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE))
            {
                TableRow row8 = new TableRow();
                TableCell row8Cell1 = new TableCell();
                TableCell row8Cell2 = new TableCell();

                this._LearningAssets_QuizAndSurveyManager = new CheckBox();
                this._LearningAssets_QuizAndSurveyManager.Enabled = false;
                this._LearningAssets_QuizAndSurveyManager.ID = "LearningAssets_QuizAndSurveyManager";
                this._LearningAssets_QuizAndSurveyManager.Text = _GlobalResources.QuizAndSurveyManagerAllowsAccessToCreateAndManageQuizzesAndSurveys;                
                row8Cell1.Controls.Add(this._LearningAssets_QuizAndSurveyManager);

                Panel row8Cell2GlobalLabelContainer = new Panel();
                Literal row8Cell2GlobalLabel = new Literal();
                row8Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row8Cell2GlobalLabelContainer.Controls.Add(row8Cell2GlobalLabel);
                row8Cell2.Controls.Add(row8Cell2GlobalLabelContainer);

                row8.Cells.Add(row8Cell1);
                row8.Cells.Add(row8Cell2);
                permissionsTable.Rows.Add(row8);
            }

            // certificate template manager
            TableRow row9 = new TableRow();
            TableCell row9Cell1 = new TableCell();
            TableCell row9Cell2 = new TableCell();

            this._LearningAssets_CertificateTemplateManager = new CheckBox();
            this._LearningAssets_CertificateTemplateManager.Enabled = false;
            this._LearningAssets_CertificateTemplateManager.ID = "LearningAssets_CertificateTemplateManager";
            this._LearningAssets_CertificateTemplateManager.Text = _GlobalResources.CertificateTemplateManagerAllowsAccessToCreateAndModifyCertificateTemplates;
            row9Cell1.Controls.Add(this._LearningAssets_CertificateTemplateManager);

            Panel row9Cell2GlobalLabelContainer = new Panel();
            Literal row9Cell2GlobalLabel = new Literal();
            row9Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row9Cell2GlobalLabelContainer.Controls.Add(row9Cell2GlobalLabel);
            row9Cell2.Controls.Add(row9Cell2GlobalLabelContainer);

            row9.Cells.Add(row9Cell1);
            row9.Cells.Add(row9Cell2);
            permissionsTable.Rows.Add(row9);

            // instructor led training manager
            TableRow row11 = new TableRow();
            TableCell row11Cell1 = new TableCell();
            TableCell row11Cell2 = new TableCell();

            this._LearningAssets_InstructorLedTrainingManager = new CheckBox();
            this._LearningAssets_InstructorLedTrainingManager.Enabled = false;
            this._LearningAssets_InstructorLedTrainingManager.ID = "LearningAssets_InstructorLedTrainingManager";
            this._LearningAssets_InstructorLedTrainingManager.Text = _GlobalResources.InstructorLedTrainingManagerAllowsAccessToCreateAndModifyInstructorLedTrainingModulesAndSessionsAndManageInstructorLedTrainingRosters;
            row11Cell1.Controls.Add(this._LearningAssets_InstructorLedTrainingManager);

            Panel row11Cell2GlobalLabelContainer = new Panel();
            Literal row11Cell2GlobalLabel = new Literal();
            row11Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row11Cell2GlobalLabelContainer.Controls.Add(row11Cell2GlobalLabel);
            row11Cell2.Controls.Add(row11Cell2GlobalLabelContainer);

            row11.Cells.Add(row11Cell1);
            row11.Cells.Add(row11Cell2);
            permissionsTable.Rows.Add(row11);

            // resource manager
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE))
            {
                TableRow row12 = new TableRow();
                TableCell row12Cell1 = new TableCell();
                TableCell row12Cell2 = new TableCell();

                this._LearningAssets_ResourceManager = new CheckBox();
                this._LearningAssets_ResourceManager.Enabled = false;
                this._LearningAssets_ResourceManager.ID = "LearningAssets_ResourceManager";
                this._LearningAssets_ResourceManager.Text = _GlobalResources.ResourceManagerAllowsAccessToCreateAndModifyResourcesToBeUsedInInstructorLedTrainingModules;
                row12Cell1.Controls.Add(this._LearningAssets_ResourceManager);

                Panel row12Cell2GlobalLabelContainer = new Panel();
                Literal row12Cell2GlobalLabel = new Literal();
                row12Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row12Cell2GlobalLabelContainer.Controls.Add(row12Cell2GlobalLabel);
                row12Cell2.Controls.Add(row12Cell2GlobalLabelContainer);

                row12.Cells.Add(row12Cell1);
                row12.Cells.Add(row12Cell2);
                permissionsTable.Rows.Add(row12);
            }
            // attach the table and return
            permissionsPanel.Controls.Add(permissionsTable);
            return permissionsPanel;
        }
        #endregion

        #region _BuildReportingPermissionsContainer
        /// <summary>
        /// Builds the container for Reporting permission checkboxes/options.
        /// </summary>
        /// <returns></returns>
        private Panel _BuildReportingPermissionsContainer(DataTable groups)
        {
            // container
            Panel permissionsPanel = new Panel();
            permissionsPanel.ID = "ReportingPermissionsContainer";
            permissionsPanel.CssClass = "PermissionsContainer";

            // table
            Table permissionsTable = new Table();

            // header row
            TableRow headerRow = new TableRow();

            // header cell
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = 2;

            // header image and label
            Panel categoryHeaderContainer = new Panel();
            categoryHeaderContainer.CssClass = "PermissionsCategoryHeaderContainer";

            Image categoryImage = new Image();
            categoryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GRAPH, ImageFiles.EXT_PNG);
            categoryImage.CssClass = "SmallIcon";
            categoryHeaderContainer.Controls.Add(categoryImage);

            Label categoryLabel = new Label();
            categoryLabel.Text = _GlobalResources.Reporting;
            categoryHeaderContainer.Controls.Add(categoryLabel);

            headerCell.Controls.Add(categoryHeaderContainer);

            headerRow.Cells.Add(headerCell);
            permissionsTable.Rows.Add(headerRow);

            // permissions checkbox rows

            // reporter - user demographics dataset
            TableRow row1 = new TableRow();
            TableCell row1Cell1 = new TableCell();
            TableCell row1Cell2 = new TableCell();

            this._Reporting_Reporter_UserDemographics = new CheckBox();
            this._Reporting_Reporter_UserDemographics.Enabled = false;
            this._Reporting_Reporter_UserDemographics.ID = "Reporting_Reporter_UserDemographics";
            this._Reporting_Reporter_UserDemographics.Text = _GlobalResources.ReportsUserDemographics;
            row1Cell1.Controls.Add(this._Reporting_Reporter_UserDemographics);

            // defined scope
            row1Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_UserDemographics", _GlobalResources.ThisUserMaySee, groups));

            row1.Cells.Add(row1Cell1);
            row1.Cells.Add(row1Cell2);
            permissionsTable.Rows.Add(row1);

            // reporter - user course transcripts dataset
            TableRow row2 = new TableRow();
            TableCell row2Cell1 = new TableCell();
            TableCell row2Cell2 = new TableCell();

            this._Reporting_Reporter_UserCourseTranscripts = new CheckBox();
            this._Reporting_Reporter_UserCourseTranscripts.Enabled = false;
            this._Reporting_Reporter_UserCourseTranscripts.ID = "Reporting_Reporter_UserCourseTranscripts";
            this._Reporting_Reporter_UserCourseTranscripts.Text = _GlobalResources.ReportsUserCourseTranscripts;
            row2Cell1.Controls.Add(this._Reporting_Reporter_UserCourseTranscripts);

            // defined scope
            row2Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_UserCourseTranscripts", _GlobalResources.ThisUserMaySee, groups));

            row2.Cells.Add(row2Cell1);
            row2.Cells.Add(row2Cell2);
            permissionsTable.Rows.Add(row2);

            // reporter - user learning path transcripts dataset
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                TableRow row3 = new TableRow();
                TableCell row3Cell1 = new TableCell();
                TableCell row3Cell2 = new TableCell();

                this._Reporting_Reporter_UserLearningPathTranscripts = new CheckBox();
                this._Reporting_Reporter_UserLearningPathTranscripts.Enabled = false;
                this._Reporting_Reporter_UserLearningPathTranscripts.ID = "Reporting_Reporter_UserLearningPathTranscripts";
                this._Reporting_Reporter_UserLearningPathTranscripts.Text = _GlobalResources.ReportsUserLearningPathTranscripts;
                row3Cell1.Controls.Add(this._Reporting_Reporter_UserLearningPathTranscripts);

                // defined scope
                row3Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_UserLearningPathTranscripts", _GlobalResources.ThisUserMaySee, groups));

                row3.Cells.Add(row3Cell1);
                row3.Cells.Add(row3Cell2);
                permissionsTable.Rows.Add(row3);
            }

            // reporter - user instructor led training transcripts dataset
            TableRow row4 = new TableRow();
            TableCell row4Cell1 = new TableCell();
            TableCell row4Cell2 = new TableCell();

            this._Reporting_Reporter_UserInstructorLedTrainingTranscripts = new CheckBox();
            this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.Enabled = false;
            this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.ID = "Reporting_Reporter_UserInstructorLedTrainingTranscripts";
            this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.Text = _GlobalResources.ReportsUserInstructorLedTrainingTranscripts;
            row4Cell1.Controls.Add(this._Reporting_Reporter_UserInstructorLedTrainingTranscripts);

            // defined scope
            row4Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_UserInstructorLedTrainingTranscripts", _GlobalResources.ThisUserMaySee, groups));

            row4.Cells.Add(row4Cell1);
            row4.Cells.Add(row4Cell2);
            permissionsTable.Rows.Add(row4);

            // reporter - catalog and course information dataset
            TableRow row5 = new TableRow();
            TableCell row5Cell1 = new TableCell();
            TableCell row5Cell2 = new TableCell();

            this._Reporting_Reporter_CatalogAndCourseInformation = new CheckBox();
            this._Reporting_Reporter_CatalogAndCourseInformation.Enabled = false;
            this._Reporting_Reporter_CatalogAndCourseInformation.ID = "Reporting_Reporter_CatalogAndCourseInformation";
            this._Reporting_Reporter_CatalogAndCourseInformation.Text = _GlobalResources.ReportsCatalogAndCourseInformation;
            row5Cell1.Controls.Add(this._Reporting_Reporter_CatalogAndCourseInformation);

            // defined scope
            row5Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_CatalogAndCourseInformation", _GlobalResources.ThisUserMaySee, groups));

            row5.Cells.Add(row5Cell1);
            row5.Cells.Add(row5Cell2);
            permissionsTable.Rows.Add(row5);

            // reporter - certificates dataset
            TableRow row6 = new TableRow();
            TableCell row6Cell1 = new TableCell();
            TableCell row6Cell2 = new TableCell();

            this._Reporting_Reporter_Certificates = new CheckBox();
            this._Reporting_Reporter_Certificates.Enabled = false;
            this._Reporting_Reporter_Certificates.ID = "Reporting_Reporter_Certificates";
            this._Reporting_Reporter_Certificates.Text = _GlobalResources.ReportsCertificates;
            row6Cell1.Controls.Add(this._Reporting_Reporter_Certificates);

            // defined scope
            row6Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_Certificates", _GlobalResources.ThisUserMaySee, groups));

            row6.Cells.Add(row6Cell1);
            row6.Cells.Add(row6Cell2);
            permissionsTable.Rows.Add(row6);

            /*
            // reporter - xAPI dataset
            TableRow row7 = new TableRow();
            TableCell row7Cell1 = new TableCell();
            TableCell row7Cell2 = new TableCell();

            this._Reporting_Reporter_XAPI = new CheckBox();
            this._Reporting_Reporter_XAPI.Enabled = false;
            this._Reporting_Reporter_XAPI.ID = "Reporting_Reporter_XAPI";
            this._Reporting_Reporter_XAPI.Text = _GlobalResources.ReportsxAPIReports;            
            row7Cell1.Controls.Add(this._Reporting_Reporter_XAPI);

            // defined scope
            row7Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_XAPI", _GlobalResources.ThisUserMaySee, groups));

            row7.Cells.Add(row7Cell1);
            row7.Cells.Add(row7Cell2);
            permissionsTable.Rows.Add(row7);
            */

            // reporter - purchases dataset - only build if ecommerce is set and verified
            if (this._EcommerceSettings.IsEcommerceSetAndVerified)
            {
                TableRow row8 = new TableRow();
                TableCell row8Cell1 = new TableCell();
                TableCell row8Cell2 = new TableCell();

                this._Reporting_Reporter_Purchases = new CheckBox();
                this._Reporting_Reporter_Purchases.Enabled = false;
                this._Reporting_Reporter_Purchases.ID = "Reporting_Reporter_Purchases";
                this._Reporting_Reporter_Purchases.Text = _GlobalResources.ReportsPurchases;                
                row8Cell1.Controls.Add(this._Reporting_Reporter_Purchases);

                // defined scope
                row8Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_Purchases", _GlobalResources.ThisUserMaySee, groups));

                row8.Cells.Add(row8Cell1);
                row8.Cells.Add(row8Cell2);
                permissionsTable.Rows.Add(row8);
            }

            // reporter - certifications dataset - only build if certifications is enabled
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
            {
                TableRow row9 = new TableRow();
                TableCell row9Cell1 = new TableCell();
                TableCell row9Cell2 = new TableCell();

                this._Reporting_Reporter_UserCertificationTranscripts = new CheckBox();
                this._Reporting_Reporter_UserCertificationTranscripts.Enabled = false;
                this._Reporting_Reporter_UserCertificationTranscripts.ID = "Reporting_Reporter_UserCertificationTranscripts";
                this._Reporting_Reporter_UserCertificationTranscripts.Text = _GlobalResources.ReportsUserCertificationTranscripts;                
                row9Cell1.Controls.Add(this._Reporting_Reporter_UserCertificationTranscripts);

                // defined scope
                row9Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_UserCertificationTranscripts", _GlobalResources.ThisUserMaySee, groups));

                row9.Cells.Add(row9Cell1);
                row9.Cells.Add(row9Cell2);
                permissionsTable.Rows.Add(row9);
            }

            // attach the table and return
            permissionsPanel.Controls.Add(permissionsTable);
            return permissionsPanel;
        }
        #endregion

        #region _BuildScopeSelectorControls
        private Panel _BuildScopeSelectorControls(string idPrefix, string headingText, DataTable groups, bool definedScopeIsUsersWithinGroups = true)
        {
            // container
            Panel scopeSelectorContainer = new Panel();
            scopeSelectorContainer.ID = idPrefix + "_ScopeSelectorContainer";

            // heading label
            Panel scopeSelectorHeadingContainer = new Panel();
            Literal scopeSelectorHeading = new Literal();
            scopeSelectorHeading.Text = headingText + ":";
            scopeSelectorHeadingContainer.Controls.Add(scopeSelectorHeading);
            scopeSelectorContainer.Controls.Add(scopeSelectorHeadingContainer);

            // scope type radio buttons
            RadioButtonList scopeType = new RadioButtonList();
            scopeType.Enabled = false; 
            scopeType.ID = idPrefix + "_ScopeType";
            scopeType.CssClass = "ScopeTypeContainer";

            ListItem globalScope = new ListItem(); 

            if (definedScopeIsUsersWithinGroups)
            { globalScope.Text = _GlobalResources.anyuser; }
            else
            { globalScope.Text = _GlobalResources.anygroup; }

            globalScope.Value = "global";
            scopeType.Items.Add(globalScope);

            ListItem definedScope = new ListItem();

            if (definedScopeIsUsersWithinGroups)
            { definedScope.Text = _GlobalResources.onlyuserswithinthegroupsselectedbelow; }
            else
            { definedScope.Text = _GlobalResources.onlythegroupsselectedbelow; }

            definedScope.Value = "scope";
            scopeType.Items.Add(definedScope);

            scopeSelectorContainer.Controls.Add(scopeType);

            // scoped items
            Panel scopedItemsContainer = new Panel();
            scopedItemsContainer.ID = idPrefix + "_ScopedItemsContainer";

            Panel scopedItemsCheckboxesContainer = new Panel();
            scopedItemsCheckboxesContainer.CssClass = "ScopedItemsCheckboxContainer";

            // loop through groups and add them to the selector
            foreach (DataRow row in groups.Rows)
            {
                CheckBox groupScope = new CheckBox();
                groupScope.Enabled = false;
                groupScope.ID = idPrefix + "_Scope_" + row["idGroup"].ToString();
                groupScope.Text = row["name"].ToString();
                scopedItemsCheckboxesContainer.Controls.Add(groupScope);
            }

            scopedItemsContainer.Controls.Add(scopedItemsCheckboxesContainer);

            // all/none selector
            Panel scopedItemsAllNoneSelectorContainer = new Panel();
            scopedItemsAllNoneSelectorContainer.ID = idPrefix + "_ScopedItemsAllNoneSelectorContainer";

            scopedItemsContainer.Controls.Add(scopedItemsAllNoneSelectorContainer);

            // attach scoped items container to scope selector
            scopeSelectorContainer.Controls.Add(scopedItemsContainer);

            // hidden field for scoped items
            HiddenField scopedItemsHiddenField = new HiddenField();
            scopedItemsHiddenField.ID = idPrefix + "_ScopedItemsHiddenField";
            scopeSelectorContainer.Controls.Add(scopedItemsHiddenField);

            // return
            return scopeSelectorContainer;
        }
        #endregion

        #region _PopulateUserPermissionsInputElements
        /// <summary>
        /// Populates the user's effective permissions input elements with values from the object.
        /// </summary>
        private void _PopulateUserPermissionsInputElements()
        {
            if (this._UserObject != null)
            {
                DataTable effectivePermissions = Asentia.UMS.Library.User.GetEffectivePermissionDetails(this._UserObject.Id);
                List<AsentiaPermissionWithScope> effectivePermissionsList = new List<AsentiaPermissionWithScope>();

                foreach (DataRow row in effectivePermissions.Rows)
                {
                    AsentiaPermissionWithScope permissionWithScope = new AsentiaPermissionWithScope((AsentiaPermission)Convert.ToInt32(row["idPermission"]), row["scope"].ToString());
                    effectivePermissionsList.Add(permissionWithScope);
                }

                if (effectivePermissionsList.Count > 0)
                {
                    // PERMISSIONS
                    foreach (AsentiaPermissionWithScope permission in effectivePermissionsList)
                    {
                        switch (permission.PermissionCode)
                        {
                            case AsentiaPermission.System_AccountSettings:
                                this._System_AccountSettings.Checked = true;
                                this._PopulatePermissionScope(this._System_AccountSettings.ID, permission.Scope);
                                break;
                            case AsentiaPermission.System_Configuration:
                                this._System_Configuration.Checked = true;
                                this._PopulatePermissionScope(this._System_Configuration.ID, permission.Scope);
                                break;
                            case AsentiaPermission.System_UserFieldConfiguration:
                                this._System_UserFieldConfiguration.Checked = true;
                                this._PopulatePermissionScope(this._System_UserFieldConfiguration.ID, permission.Scope);
                                break;
                            case AsentiaPermission.System_RulesEngine:
                                this._System_RulesEngine.Checked = true;
                                this._PopulatePermissionScope(this._System_RulesEngine.ID, permission.Scope);
                                break;
                            case AsentiaPermission.System_Ecommerce:
                                this._System_Ecommerce.Checked = true;
                                this._PopulatePermissionScope(this._System_Ecommerce.ID, permission.Scope);
                                break;
                            case AsentiaPermission.System_CouponCodes:
                                this._System_CouponCodes.Checked = true;
                                this._PopulatePermissionScope(this._System_CouponCodes.ID, permission.Scope);
                                break;
                            case AsentiaPermission.System_TrackingCodes:
                                this._System_TrackingCodes.Checked = true;
                                this._PopulatePermissionScope(this._System_TrackingCodes.ID, permission.Scope);
                                break;
                            case AsentiaPermission.System_EmailNotifications:
                                this._System_EmailNotifications.Checked = true;
                                this._PopulatePermissionScope(this._System_EmailNotifications.ID, permission.Scope);
                                break;
                            case AsentiaPermission.System_API:
                                this._System_API.Checked = true;
                                this._PopulatePermissionScope(this._System_API.ID, permission.Scope);
                                break;
                            case AsentiaPermission.System_xAPIEndpoints:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.XAPIENDPOINTS_ENABLE))
                                {
                                    this._System_xAPIEndpoints.Checked = true;
                                    this._PopulatePermissionScope(this._System_xAPIEndpoints.ID, permission.Scope);
                                }
                                break;
                            case AsentiaPermission.System_Logs:
                                this._System_Logs.Checked = true;
                                this._PopulatePermissionScope(this._System_Logs.ID, permission.Scope);
                                break;
                            case AsentiaPermission.System_WebMeetingIntegration:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE)
                                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE)
                                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE)
                                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE))
                                {
                                    this._System_WebMeetingIntegration.Checked = true;
                                    this._PopulatePermissionScope(this._System_WebMeetingIntegration.ID, permission.Scope);
                                }

                                break;
                            case AsentiaPermission.UsersAndGroups_UserRegistrationApproval:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERREGISTRATIONAPPROVAL_ENABLE))
                                {
                                    this._UsersAndGroups_UserRegistrationApproval.Checked = true;
                                    this._PopulatePermissionScope(this._UsersAndGroups_UserRegistrationApproval.ID, permission.Scope);
                                }
                                break;
                            case AsentiaPermission.UsersAndGroups_UserCreator:
                                this._UsersAndGroups_UserCreator.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_UserCreator.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_UserDeleter:
                                this._UsersAndGroups_UserDeleter.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_UserDeleter.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_UserEditor:
                                this._UsersAndGroups_UserEditor.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_UserEditor.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_UserManager:
                                this._UsersAndGroups_UserManager.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_UserManager.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_UserImpersonator:
                                this._UsersAndGroups_UserImpersonator.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_UserImpersonator.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_GroupCreator:
                                this._UsersAndGroups_GroupCreator.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_GroupCreator.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_GroupDeleter:
                                this._UsersAndGroups_GroupDeleter.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_GroupDeleter.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_GroupEditor:
                                this._UsersAndGroups_GroupEditor.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_GroupEditor.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_GroupManager:
                                this._UsersAndGroups_GroupManager.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_GroupManager.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_RoleManager:
                                this._UsersAndGroups_RoleManager.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_RoleManager.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_ActivityImport:
                                this._UsersAndGroups_ActivityImport.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_ActivityImport.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_CertificateImport:
                                this._UsersAndGroups_CertificateImport.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_CertificateImport.ID, permission.Scope);
                                break;
                            case AsentiaPermission.UsersAndGroups_Leaderboards:
                                this._UsersAndGroups_Leaderboards.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_Leaderboards.ID, permission.Scope);
                                break;
                            case AsentiaPermission.InterfaceAndLayout_HomePage:
                                this._InterfaceAndLayout_HomePage.Checked = true;
                                this._PopulatePermissionScope(this._InterfaceAndLayout_HomePage.ID, permission.Scope);
                                break;
                            case AsentiaPermission.InterfaceAndLayout_Masthead:
                                this._InterfaceAndLayout_Masthead.Checked = true;
                                this._PopulatePermissionScope(this._InterfaceAndLayout_Masthead.ID, permission.Scope);
                                break;
                            case AsentiaPermission.InterfaceAndLayout_Footer:
                                this._InterfaceAndLayout_Footer.Checked = true;
                                this._PopulatePermissionScope(this._InterfaceAndLayout_Footer.ID, permission.Scope);
                                break;
                            case AsentiaPermission.InterfaceAndLayout_CSSEditor:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CSSEDITOR_ENABLE))
                                {
                                    this._InterfaceAndLayout_CSSEditor.Checked = true;
                                    this._PopulatePermissionScope(this._InterfaceAndLayout_CSSEditor.ID, permission.Scope);
                                }
                                break;
                            case AsentiaPermission.InterfaceAndLayout_ImageEditor:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.IMAGE_EDITOR_ENABLE))
                                {
                                    this._InterfaceAndLayout_ImageEditor.Checked = true;
                                    this._PopulatePermissionScope(this._InterfaceAndLayout_ImageEditor.ID, permission.Scope);
                                }
                                break;
                            case AsentiaPermission.LearningAssets_CourseCatalog:
                                this._LearningAssets_CourseCatalog.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_CourseCatalog.ID, permission.Scope);
                                break;
                            case AsentiaPermission.LearningAssets_SelfEnrollmentApproval:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
                                {
                                    this._LearningAssets_SelfEnrollmentApproval.Checked = true;
                                    this._PopulatePermissionScope(this._LearningAssets_SelfEnrollmentApproval.ID, permission.Scope);
                                }
                                break;
                            case AsentiaPermission.LearningAssets_CourseContentManager:
                                this._LearningAssets_CourseContentManager.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_CourseContentManager.ID, permission.Scope);
                                break;
                            case AsentiaPermission.LearningAssets_CourseEnrollmentManager:
                                this._LearningAssets_CourseEnrollmentManager.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_CourseEnrollmentManager.ID, permission.Scope);
                                break;
                            case AsentiaPermission.LearningAssets_LearningPathContentManager:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                                {
                                    this._LearningAssets_LearningPathContentManager.Checked = true;
                                    this._PopulatePermissionScope(this._LearningAssets_LearningPathContentManager.ID, permission.Scope);
                                }

                                break;
                            case AsentiaPermission.LearningAssets_LearningPathEnrollmentManager:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                                {
                                    this._LearningAssets_LearningPathEnrollmentManager.Checked = true;
                                    this._PopulatePermissionScope(this._LearningAssets_LearningPathEnrollmentManager.ID, permission.Scope);
                                }

                                break;
                            case AsentiaPermission.LearningAssets_CertificationsManager:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                                {
                                    this._LearningAssets_CertificationsManager.Checked = true;
                                    this._PopulatePermissionScope(this._LearningAssets_CertificationsManager.ID, permission.Scope);
                                }
                                
                                break;
                            case AsentiaPermission.LearningAssets_ContentPackageManager:
                                this._LearningAssets_ContentPackageManager.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_ContentPackageManager.ID, permission.Scope);
                                break;
                            case AsentiaPermission.LearningAssets_QuizAndSurveyManager:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE))
                                {
                                    this._LearningAssets_QuizAndSurveyManager.Checked = true;
                                    this._PopulatePermissionScope(this._LearningAssets_QuizAndSurveyManager.ID, permission.Scope);
                                }
                                
                                break;
                            case AsentiaPermission.LearningAssets_CertificateTemplateManager:
                                this._LearningAssets_CertificateTemplateManager.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_CertificateTemplateManager.ID, permission.Scope);
                                break;
                            case AsentiaPermission.LearningAssets_InstructorLedTrainingManager:
                                this._LearningAssets_InstructorLedTrainingManager.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_InstructorLedTrainingManager.ID, permission.Scope);
                                break;
                            case AsentiaPermission.LearningAssets_ResourceManager:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE))
                                {
                                    this._LearningAssets_ResourceManager.Checked = true;
                                    this._PopulatePermissionScope(this._LearningAssets_ResourceManager.ID, permission.Scope);
                                }
                                break;
                            case AsentiaPermission.Reporting_Reporter_UserDemographics:
                                this._Reporting_Reporter_UserDemographics.Checked = true;
                                this._PopulatePermissionScope(this._Reporting_Reporter_UserDemographics.ID, permission.Scope);
                                break;
                            case AsentiaPermission.Reporting_Reporter_UserCourseTranscripts:
                                this._Reporting_Reporter_UserCourseTranscripts.Checked = true;
                                this._PopulatePermissionScope(this._Reporting_Reporter_UserCourseTranscripts.ID, permission.Scope);
                                break;
                            case AsentiaPermission.Reporting_Reporter_UserLearningPathTranscripts:
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                                {
                                    this._Reporting_Reporter_UserLearningPathTranscripts.Checked = true;
                                    this._PopulatePermissionScope(this._Reporting_Reporter_UserLearningPathTranscripts.ID, permission.Scope);
                                }

                                break;
                            case AsentiaPermission.Reporting_Reporter_UserInstructorLedTrainingTranscripts:
                                this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.Checked = true;
                                this._PopulatePermissionScope(this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.ID, permission.Scope);
                                break;
                            case AsentiaPermission.Reporting_Reporter_CatalogAndCourseInformation:
                                this._Reporting_Reporter_CatalogAndCourseInformation.Checked = true;
                                this._PopulatePermissionScope(this._Reporting_Reporter_CatalogAndCourseInformation.ID, permission.Scope);
                                break;
                            case AsentiaPermission.Reporting_Reporter_Certificates:
                                this._Reporting_Reporter_Certificates.Checked = true;
                                this._PopulatePermissionScope(this._Reporting_Reporter_Certificates.ID, permission.Scope);
                                break;
                            case AsentiaPermission.Reporting_Reporter_Purchases:                                
                                // only populate if ecommerce is set and verified
                                if (this._EcommerceSettings.IsEcommerceSetAndVerified)
                                {
                                    this._Reporting_Reporter_Purchases.Checked = true;
                                    this._PopulatePermissionScope(this._Reporting_Reporter_Purchases.ID, permission.Scope);
                                }

                                break;
                            case AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts:
                                // only populate if certifications is enabled
                                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                                {
                                    this._Reporting_Reporter_UserCertificationTranscripts.Checked = true;
                                    this._PopulatePermissionScope(this._Reporting_Reporter_UserCertificationTranscripts.ID, permission.Scope);
                                }

                                break;
                        }
                    }
                }
            }
        }
        #endregion

        #region _PopulatePermissionScope
        /// <summary>
        /// Populates the scoping options for the permission inputs.
        /// </summary>
        /// <param name="idPrefix">id of the permission checkbox</param>
        /// <param name="scope">scope list</param>
        private void _PopulatePermissionScope(string idPrefix, List<int> scope)
        {
            // get the global scope/item scope radio list
            RadioButtonList scopeType = (RadioButtonList)FindControlRecursive(this._PermissionsModalBodyPanel, idPrefix + "_ScopeType");

            if (scope != null) // item scope
            {
                if (scopeType != null)
                {
                    // select "scope"
                    scopeType.SelectedValue = "scope";

                    // select each item in the scope list
                    foreach (int scopeItem in scope)
                    {
                        CheckBox scopeItemCheckBox = (CheckBox)FindControlRecursive(this._PermissionsModalBodyPanel, idPrefix + "_Scope_" + scopeItem.ToString());

                        if (scopeItemCheckBox != null)
                        { scopeItemCheckBox.Checked = true; }
                    }
                }
            }
            else // global scope
            {
                // select "global"
                if (scopeType != null)
                { scopeType.SelectedValue = "global"; }
            }
        }
        #endregion
        #endregion
    }
}
