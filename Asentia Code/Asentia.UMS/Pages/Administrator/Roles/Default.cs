﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.UMS.Pages.Administrator.Roles
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel RolesFormContentWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel RoleGridUpdatePanel;
        public Grid RoleGrid;
        public Panel ActionsPanel;
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup GridConfirmAction;        
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Roles));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, _GlobalResources.Roles, ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION,
                                                                                                                    ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.RoleGrid.BindData();
            }
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD ROLE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddRoleLink",
                                                null,
                                                "Modify.aspx",
                                                null,
                                                _GlobalResources.NewRole,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.RoleGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.RoleGrid.StoredProcedure = Library.Role.GridProcedure;
            this.RoleGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.RoleGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.RoleGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.RoleGrid.IdentifierField = "idRole";
            this.RoleGrid.DefaultSortColumn = "name";
            this.RoleGrid.SearchBoxPlaceholderText = _GlobalResources.SearchRoles;

            // data key names
            this.RoleGrid.DataKeyNames = new string[] { "idRole" };

            // columns
            GridColumn roleName = new GridColumn(_GlobalResources.Role, null, "name");           

            // add columns to data grid
            this.RoleGrid.AddColumn(roleName);

            // add row data bound event
            this.RoleGrid.RowDataBound += this._RoleGrid_RowDataBound;
        }
        #endregion

        #region _RoleGrid_RowDataBound
        private void _RoleGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idSite = Convert.ToInt32(rowView["idSite"]); // will never be null
                int idRole = Convert.ToInt32(rowView["idRole"]); // will never be null

                // CHECK DELETE PERMISSIONS

                // if this role belongs to site 1 (system built-in), disable the checkbox that marks the row for deletion
                if (idSite == 1)
                {
                    CheckBox deleteCheckBox = (CheckBox)e.Row.Cells[0].FindControl(this.RoleGrid.ID + "_GridSelectRecord_" + e.Row.DataItemIndex.ToString());

                    if (deleteCheckBox != null)
                    { deleteCheckBox.Enabled = false; }
                }

                // AVATAR, NAME, MEMEBERS

                string name = Server.HtmlEncode(rowView["name"].ToString());

                // avatar
                Image avatarImage = new Image();
                avatarImage.CssClass = "GridAvatarImage";
                avatarImage.AlternateText = _GlobalResources.Role;
                avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG);
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name                
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink nameLink = new HyperLink();
                nameLink.NavigateUrl = "Modify.aspx?id=" + idRole.ToString();
                nameLink.Text = name;
                nameLabel.Controls.Add(nameLink);

                // system built-in
                if (idSite == 1)
                {
                    Label systemBuiltInLabel = new Label();
                    systemBuiltInLabel.CssClass = "GridSecondaryLine";
                    systemBuiltInLabel.Text = _GlobalResources.SystemRole;
                    e.Row.Cells[1].Controls.Add(systemBuiltInLabel);
                }
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedRole_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this.DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedRole_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseRole_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.RoleGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.RoleGrid.Rows[i].FindControl(this.RoleGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.Role.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedRole_sHaveBeenDeletedSuccessfully, false);

                    // rebind the grid
                    this.RoleGrid.BindData();
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoRole_sSelectedForDeletion, true);
                }
            }
           
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.RoleGrid.BindData();
            }
        }
        #endregion
    }
}
