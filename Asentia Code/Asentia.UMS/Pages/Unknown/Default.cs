﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages.Unknown
{
    public class Default : AsentiaPage
    {
        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/unknown/Default.css");

            // if this is a valid side, redirect back to home as we shouldn't be here
            if (AsentiaSessionState.IdSite > 1)
            { Response.Redirect("/"); }

            // display the unknown portal message
            this.DisplayFeedback(_GlobalResources.UnknownPortal, true, false);
        }
        #endregion
    }
}