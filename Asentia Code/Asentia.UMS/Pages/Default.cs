﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages
{
    public class Default : AsentiaPage
    {
        #region Properties
        public Literal HomePageContent;
        private const string _HOME_PAGE_FILE_NAME = "HomePage.{0}.html";
        private bool _LoginFormInPage = false;

        public LoginForm LoginFormControl = new LoginForm();
        public ModalPopup LoginFormModal = new ModalPopup("LoginFormModal");
        #endregion

        #region OnPreInit
        protected override void OnPreInit(EventArgs e)
        {
            // if we have a logged in user, hide the login form
            if (AsentiaSessionState.IdSiteUser > 0)
            { LoginFormControl.Visible = false; }            

            base.OnPreInit(e);
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;

            string googleAnalyticsTrackingCode = String.Empty;
            googleAnalyticsTrackingCode = this.GetTrackingCode();

            if (!String.IsNullOrWhiteSpace(googleAnalyticsTrackingCode))
            {
                bool applyScriptTags = true;

                if (googleAnalyticsTrackingCode.Contains("<script") && googleAnalyticsTrackingCode.Contains("</script"))
                { applyScriptTags = false; }

                // build start up call for google analytics tracking code
                StringBuilder googleAnalyticsTrackingScript = new StringBuilder();
                googleAnalyticsTrackingScript.AppendLine(googleAnalyticsTrackingCode);

                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "GoogleAnalyticsTrackingCode", googleAnalyticsTrackingScript.ToString(), applyScriptTags);
            }            

            // if we have the log in form in the page content, remove the log in button on the masthead            
            if (this._LoginFormInPage)
            { csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "RemoveLogInButtonFromMasthead", "$(\"#SettingsControlForLogin\").remove();", true); }
            else // login form in modal, attach clean up actions to modal close
            {
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "CloseIconClientScript", "$(\"#LoginFormModalModalCloseIcon\").click(function () {" +
                " $(\"#" + this.LoginFormControl.UsernameBox.ID + "\").val(''); " +
                " $(\"#" + this.LoginFormControl.PasswordBox.ID + "\").val(''); " +
                " $(\"#LoginFormForgotPasswordCancelButton\").click(); " +
                " $(\"#LoginFormFeedbackPanel\").hide(); " +
                "});", true);
            }

            // set the target control for the login modal
            // note: this is already done when setting up the modal, but needs to be done again here for
            // reasons that we need to figure out, in the meantime this works
            this.LoginFormModal.TargetControlID = AsentiaMasterPage.LogInButtonId;
        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {            
            // load home page content
            this._LoadHomePageContent();

            // build the login modal if the login form is not in the page
            if (!this._LoginFormInPage)
            { this._BuildLoginModal(); }            
        }
        #endregion

        #region _BuildLoginModal
        /// <summary>
        /// _BuildLoginModal
        /// </summary>
        private void _BuildLoginModal()
        {
            // if the user isn't logged in, show the login form
            if (AsentiaSessionState.IdSiteUser == 0)
            {
                // set form render type and forgot password submit button command
                this.LoginFormControl.FormRenderType = LoginFormRenderType.InModal;
                this.LoginFormControl.LoginButton.Command += new CommandEventHandler(this._LoginButton_Command);
                this.LoginFormControl.ForgotPasswordSubmitButton.Command += new CommandEventHandler(this._ForgotPassword_Command);

                // build the modal
                this.LoginFormModal.Type = ModalPopupType.EmbeddedForm;
                this.LoginFormModal.TargetControlID = AsentiaMasterPage.LogInButtonId;
                this.LoginFormModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                                                                            ImageFiles.EXT_PNG);
                this.LoginFormModal.HeaderIconAlt = _GlobalResources.Log_In;
                this.LoginFormModal.HeaderText = _GlobalResources.Log_In;
                
                this.LoginFormModal.FocusControlIdOnLaunch = this.LoginFormControl.UsernameBox.ID;
                this.LoginFormModal.ReloadPageOnClose = false;

                // attach login control to modal
                this.LoginFormModal.AddControlToBody(this.LoginFormControl);

                // attach modal to page
                this.PageContentContainer.Controls.Add(this.LoginFormModal);

                // ensure controls exist
                this.EnsureChildControls();
            }
        }
        #endregion

        #region _LoadHomePageContent
        /// <summary>
        /// Load the home page content from file.
        /// </summary>
        private void _LoadHomePageContent()
        {
            try
            {
                HomePage homePage = new HomePage();

                foreach (HomePage.LanguageSpecificProperty homePageLanguageSpecificProperty in homePage.LanguageSpecificProperties)
                {
                    if (homePageLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    {                  
                        // if we find the log in form placeholder in the home page content, try to put the login form in the page
                        if (homePageLanguageSpecificProperty.Html.Contains("##login_form##"))
                        {
                            string[] homePageText = homePageLanguageSpecificProperty.Html.Split(new string[] { "##login_form##" }, StringSplitOptions.None);
                            
                            // there must be one and only one login form placeholder which results in the home page content being split into two 
                            // array elements (before form and after form), if there is any more or any less than 2 items in the array, just display
                            // the home page as normal
                            if (homePageText.Length == 2)
                            {
                                // before form content
                                Literal homePageBeforeForm = new Literal();
                                homePageBeforeForm.Text = homePageText[0];
                                this.PageContentContainer.Controls.Add(homePageBeforeForm);

                                // the form
                                Panel homePageLoginForm = new Panel();

                                this.LoginFormControl.FormRenderType = LoginFormRenderType.Standard;
                                this.LoginFormControl.LoginButton.Command += new CommandEventHandler(this._LoginButton_Command);
                                this.LoginFormControl.ForgotPasswordModal.SubmitButton.Command += new CommandEventHandler(this._ForgotPassword_Command);
                                
                                homePageLoginForm.Controls.Add(this.LoginFormControl);
                                this.PageContentContainer.Controls.Add(this.LoginFormControl);

                                // after form content
                                Literal homePageAfterForm = new Literal();
                                homePageAfterForm.Text = homePageText[1];
                                this.PageContentContainer.Controls.Add(homePageAfterForm);

                                // flag that the login form is in the page
                                this._LoginFormInPage = true;
                            }
                            else // display home page as normal
                            { this.HomePageContent.Text = homePageLanguageSpecificProperty.Html; }
                        }
                        else // display home page as normal
                        { this.HomePageContent.Text = homePageLanguageSpecificProperty.Html; }  
                    }                                       
                }                
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _LoginButton_Command
        /// <summary>
        /// Method to perform the user login.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">arguments</param>
        private void _LoginButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string lastActiveSessionId = "";
                bool simultaniousLogin = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_SIMULTANEOUSLOGIN_ENABLED);
                string loginPriority = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_LOGINPRIORITY);

                // make sure username and password are specified
                if (String.IsNullOrWhiteSpace(this.LoginFormControl.UsernameBox.Text)
                    || String.IsNullOrWhiteSpace(this.LoginFormControl.PasswordBox.Text))
                {
                    throw new AsentiaException(_GlobalResources.UsernameAndPasswordMustBeSpecified);
                }

                // authenticate login
                Library.Login.AuthenticateUser(this.LoginFormControl.UsernameBox.Text, this.LoginFormControl.PasswordBox.Text, out lastActiveSessionId);

                if (simultaniousLogin == false && loginPriority == "latest" && !String.IsNullOrWhiteSpace(lastActiveSessionId) && lastActiveSessionId != AsentiaSessionState.SessionId)
                { Library.Login.DeleteBySessionId(lastActiveSessionId); }

                // if we get here without throwing an exception, it means a successful login, so redirect to the landing page
                Response.Redirect(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_DEFAULTLANDINGPAGE_LOCALPATH));
            }
            catch (Exception ex)
            {
                // show the error
                this.LoginFormControl.DisplayFeedback(ex.Message, true, true);

                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                { this.LoginFormControl.ShowFormContentPanel(); }
            }
        }
        #endregion

        #region _ForgotPassword_Command
        /// <summary>
        /// Method to process the forgot password form submission from the modal.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">arguments</param>
        private void _ForgotPassword_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // make sure username is specified
                if (String.IsNullOrWhiteSpace(this.LoginFormControl.ForgotPasswordModalUsernameBox.Text))
                {
                    throw new AsentiaException(_GlobalResources.UsernameMustBeSpecified);
                }

                // reset the password
                string emailSentTo = Library.Login.ResetPassword(this.LoginFormControl.ForgotPasswordModalUsernameBox.Text);

                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(true);
                    this.LoginFormControl.DisplayFeedback(String.Format(_GlobalResources.YourPasswordHasBeenSuccessfullyResetAndSentToEmail, emailSentTo), false, false);
                }
                else
                {
                    // disable the submit button and show the success feedback in the forgot password modal
                    this.LoginFormControl.ForgotPasswordModal.DisableSubmitButton();
                    this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(String.Format(_GlobalResources.YourPasswordHasBeenSuccessfullyResetAndSentToEmail, emailSentTo), false);
                }

            }
            catch (Exception ex)
            {
                // show the error
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(false);
                    this.LoginFormControl.DisplayFeedback(ex.Message, true, false);
                }
                else
                { this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(ex.Message, true); }
            }
        }
        #endregion
    }
}