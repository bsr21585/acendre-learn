﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages
{
    public class Register : AsentiaPage
    {
        #region Properties
        public Panel UserFormContentWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel UserFormWrapperContainer;
        public Panel UserFormContainer;
        public Panel ActionsPanel;

        public LoginForm LoginFormControl;
        public ModalPopup LoginFormModal;
        #endregion

        #region Private Properties
        private User _UserObject;
        private UserForm _UserForm;
        private Button _SaveButton;
        private Button _CancelButton;

        // referrer information
        private bool _HasReferrer = false;
        private string _ReferrerURL;
    
        // auto-login after registering?
        private bool _AutoLoginAfterRegistering = true;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "CloseIconClientScript", "$(\"#LoginFormModalModalCloseIcon\").click(function () {" +
                " $(\"#" + this.LoginFormControl.UsernameBox.ID + "\").val(''); " +
                " $(\"#" + this.LoginFormControl.PasswordBox.ID + "\").val(''); " +
                " $(\"#LoginFormForgotPasswordCancelButton\").click(); " +
                " $(\"#LoginFormFeedbackPanel\").hide(); " +
                "});", true);
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // IMPORTANT NOTE: HAS REFERRER AND AUTO-LOGIN ARE CONNECTED TO ONE ANOTHER, AND ALWAYS TRUE AT THE MOMENT
            // They are handled as if they could be false (we have logic that checks them) to allow for furure development 
            // where registrations must be approved and therefore we would not be auto-logging the user in and redirecting 
            // them to the referrer.           

            // if self registration is not allowed, no one can see this page
            if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_SELFREGISTRATION_ENABLED))
            { Response.Redirect("/"); }

            // logged in users cannot see this page
            if (AsentiaSessionState.IdSiteUser > 0)
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/Register.css");

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, _GlobalResources.Register, ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                                                                       ImageFiles.EXT_PNG));

            this.UserFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.UserFormWrapperContainer.CssClass = "FormContentContainer";

            // process referrer querystring
            this._ProcessReferrerQuerystring();

            // build the page controls
            this._BuildControls();

            // build the login modal
            this._BuildLoginModal();
        }
        #endregion

        #region _ProcessReferrerQuerystring
        /// <summary>
        /// Processes the referrer querystring so that we can pass the registered user back to the referring page
        /// after they have registered.
        /// </summary>
        private void _ProcessReferrerQuerystring()
        {
            // get the referrer querystring parameter
            string qsReferrer = this.QueryStringString("referrer", null);
            string qsCatalogIdParam;
            string qsCatalogTypeParam;
            string qsCatalogScParam;

            // if there is a referrer, process it            
            switch (qsReferrer)
            {
                case "catalog":
                    this._HasReferrer = true;
                    this._ReferrerURL = "/catalog/";

                    qsCatalogIdParam = this.QueryStringString("id", null);
                    qsCatalogTypeParam = this.QueryStringString("type", null);

                    if (!String.IsNullOrWhiteSpace(qsCatalogIdParam) && !String.IsNullOrWhiteSpace(qsCatalogTypeParam))
                    { this._ReferrerURL += "?id=" + qsCatalogIdParam + "&type=" + qsCatalogTypeParam; }

                    break;

                case "shortcode":
                    this._HasReferrer = true;
                    this._ReferrerURL = "/catalog/";

                    qsCatalogTypeParam = this.QueryStringString("type", null);
                    qsCatalogScParam = this.QueryStringString("sc", null);

                    if (!String.IsNullOrWhiteSpace(qsCatalogTypeParam) && !String.IsNullOrWhiteSpace(qsCatalogScParam))
                    { this._ReferrerURL += "?type=" + qsCatalogTypeParam + "&sc=" + qsCatalogScParam; }

                    break;
                default:
                    this._HasReferrer = true;
                    this._ReferrerURL = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_DEFAULTLANDINGPAGE_LOCALPATH);
                    break;
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the user form
            this._BuildUserForm();

            // build the actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _BuildUserForm
        /// <summary>
        /// Builds the user form.
        /// </summary>
        private void _BuildUserForm()
        {
            this.UserFormContainer.Controls.Clear();

            this._UserForm = new UserForm("UserRegister", UserFormViewType.RegistrationPage, UserAccountDataFileType.Site, AsentiaSessionState.UserCulture);

            this.UserFormContainer.Controls.Add(this._UserForm);
        }
        #endregion

        #region _BuildLoginModal
        /// <summary>
        /// Builds Login modal pop up
        /// </summary>
        private void _BuildLoginModal()
        {
            // if the user isn't logged in, show the login form
            if (AsentiaSessionState.IdSiteUser == 0)
            {
                // set form render type and forgot password submit button command
                this.LoginFormControl = new LoginForm();
                this.LoginFormControl.FormRenderType = LoginFormRenderType.InModal;
                this.LoginFormControl.LoginButton.Command += new CommandEventHandler(this._LoginButton_Command);
                this.LoginFormControl.ForgotPasswordSubmitButton.Command += new CommandEventHandler(this._ForgotPassword_Command);

                // build the modal
                this.LoginFormModal = new ModalPopup("LoginFormModal");
                this.LoginFormModal.Type = ModalPopupType.EmbeddedForm;
                this.LoginFormModal.TargetControlID = AsentiaMasterPage.LogInButtonId;
                this.LoginFormModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                                                                            ImageFiles.EXT_PNG);
                this.LoginFormModal.HeaderIconAlt = _GlobalResources.Log_In;
                this.LoginFormModal.HeaderText = _GlobalResources.Log_In;

                this.LoginFormModal.FocusControlIdOnLaunch = this.LoginFormControl.UsernameBox.ID;
                this.LoginFormModal.ReloadPageOnClose = false;

                // attach login control to modal
                this.LoginFormModal.AddControlToBody(this.LoginFormControl);

                // attach modal to page
                this.PageContentContainer.Controls.Add(this.LoginFormModal);

                // ensure controls exist
                this.EnsureChildControls();
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";
            this._SaveButton.Text = _GlobalResources.NewAccount;
            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {                
                // create a user object
                this._UserObject = new User();

                // perform validation on the form data
                List<User.FieldError> formErrors = this._UserObject.ValidateDataFromForm(this._UserForm);

                // if there are errors, report them
                if (formErrors.Count > 0)
                {
                    this._UserForm.ProcessFormErrors(formErrors);
                    throw new AsentiaException();
                }

                // populate the object
                this._UserObject.FirstName = this._UserForm.FirstName;
                this._UserObject.MiddleName = this._UserForm.MiddleName;
                this._UserObject.LastName = this._UserForm.LastName;
                this._UserObject.Email = this._UserForm.Email;
                this._UserObject.Username = this._UserForm.Username;
                this._UserObject.Password = this._UserForm.Password;
                this._UserObject.MustChangePassword = Convert.ToBoolean(this._UserForm.MustChangePassword);
                this._UserObject.IdTimezone = Convert.ToInt32(this._UserForm.Timezone);
                this._UserObject.LanguageString = AsentiaSessionState.UserCulture;
                this._UserObject.DtExpires = (this._UserForm.DtExpires == null) ? (DateTime?)null : Convert.ToDateTime(this._UserForm.DtExpires);
                this._UserObject.IsActive = true;
                this._UserObject.Company = this._UserForm.Company;
                this._UserObject.Address = this._UserForm.Address;
                this._UserObject.City = this._UserForm.City;
                this._UserObject.Province = this._UserForm.Province;
                this._UserObject.PostalCode = this._UserForm.PostalCode;
                this._UserObject.Country = this._UserForm.Country;
                this._UserObject.PhonePrimary = this._UserForm.PhonePrimary;
                this._UserObject.PhoneHome = this._UserForm.PhoneHome;
                this._UserObject.PhoneWork = this._UserForm.PhoneWork;
                this._UserObject.PhoneFax = this._UserForm.PhoneFax;
                this._UserObject.PhoneMobile = this._UserForm.PhoneMobile;
                this._UserObject.PhonePager = this._UserForm.PhonePager;
                this._UserObject.PhoneOther = this._UserForm.PhoneOther;
                this._UserObject.EmployeeId = this._UserForm.EmployeeID;
                this._UserObject.JobTitle = this._UserForm.JobTitle;
                this._UserObject.JobClass = this._UserForm.JobClass;
                this._UserObject.Division = this._UserForm.Division;
                this._UserObject.Region = this._UserForm.Region;
                this._UserObject.Department = this._UserForm.Department;
                this._UserObject.DtHire = (this._UserForm.DtHire == null) ? (DateTime?)null : Convert.ToDateTime(this._UserForm.DtHire);
                this._UserObject.DtTerm = (this._UserForm.DtTerm == null) ? (DateTime?)null : Convert.ToDateTime(this._UserForm.DtTerm);
                this._UserObject.Gender = this._UserForm.Gender;
                this._UserObject.Race = this._UserForm.Race;
                this._UserObject.DtDOB = (this._UserForm.DtDOB == null) ? (DateTime?)null : Convert.ToDateTime(this._UserForm.DtDOB);
                this._UserObject.Field00 = this._UserForm.Field00;
                this._UserObject.Field01 = this._UserForm.Field01;
                this._UserObject.Field02 = this._UserForm.Field02;
                this._UserObject.Field03 = this._UserForm.Field03;
                this._UserObject.Field04 = this._UserForm.Field04;
                this._UserObject.Field05 = this._UserForm.Field05;
                this._UserObject.Field06 = this._UserForm.Field06;
                this._UserObject.Field07 = this._UserForm.Field07;
                this._UserObject.Field08 = this._UserForm.Field08;
                this._UserObject.Field09 = this._UserForm.Field09;
                this._UserObject.Field10 = this._UserForm.Field10;
                this._UserObject.Field11 = this._UserForm.Field11;
                this._UserObject.Field12 = this._UserForm.Field12;
                this._UserObject.Field13 = this._UserForm.Field13;
                this._UserObject.Field14 = this._UserForm.Field14;
                this._UserObject.Field15 = this._UserForm.Field15;
                this._UserObject.Field16 = this._UserForm.Field16;
                this._UserObject.Field17 = this._UserForm.Field17;
                this._UserObject.Field18 = this._UserForm.Field18;
                this._UserObject.Field19 = this._UserForm.Field19;
                this._UserObject.DtApproved = null;
                this._UserObject.DtDenied = null;
                this._UserObject.IdApprover = null;
                this._UserObject.RejectionComments = null;                
                
                // check site parameter for the registration approval requirement
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_ACCOUNTREGISTRATIONAPPROVAL))
                {
                    // if registration approval is required, then set the approved flag to false and disable auto-login
                    this._UserObject.IsRegistrationApproved = false;
                    this._AutoLoginAfterRegistering = false;
                }
                else 
                { 
                    // if registration approval is not required, then set the approval to NULL
                    this._UserObject.IsRegistrationApproved = null;
                }

                // save the user and assign it's id to this user object's id
                int id = this._UserObject.Save(1);
                this._UserObject.Id = id;

                // save the user agreement agreed date if it is required
                if (this._UserForm.IsUserAgreementRequired)
                {
                    this._UserObject.SaveUserAgreementAgreed(1, id);
                }

                // we now have the new user's id so that we can create the user's folder and update the avatar if necessary.

                // check user folder existence and create if necessary
                if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id)))
                { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id)); }

                // if there is an avatar, move it and update the database record
                if (this._UserForm.Avatar != null)
                {
                    string fullSavedFilePath = null;
                    string fullSavedFilePathSmall = null;

                    if (File.Exists(Server.MapPath(this._UserForm.Avatar)))
                    {
                        fullSavedFilePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar" + Path.GetExtension(this._UserForm.Avatar);
                        fullSavedFilePathSmall = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar.small" + Path.GetExtension(this._UserForm.Avatar);

                        // move the uploaded file into the user's folder
                        File.Copy(Server.MapPath(this._UserForm.Avatar), Server.MapPath(fullSavedFilePath), true);
                        File.Delete(Server.MapPath(this._UserForm.Avatar));
                        this._UserObject.Avatar = "avatar" + Path.GetExtension(this._UserForm.Avatar);

                        // create a smaller version of the avatar for use in wall feeds
                        ImageResizer resizer = new ImageResizer();
                        resizer.MaxX = 40;
                        resizer.MaxY = 40;
                        resizer.TrimImage = true;
                        resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                        // save the avatar path to the database
                        this._UserObject.SaveAvatar(1);
                    }
                }

                // if we should auto-login the user, do it now
                if (this._AutoLoginAfterRegistering)
                { this._AutoLoginRegisteredUser();  }

                // clear the controls from the user form container and actions panel
                this.UserFormContainer.Controls.Clear();
                this.ActionsPanel.Controls.Clear();

                // display the saved feedback
                if (this._HasReferrer && this._AutoLoginAfterRegistering)
                { this.DisplayFeedback(String.Format(_GlobalResources.YourAccountHasBeenCreatedPleaseClickHereToLogInAndContinue, this._ReferrerURL), false, false); }
                else if (!(bool)this._UserObject.IsRegistrationApproved)
                { this.DisplayFeedback(_GlobalResources.YourRegistrationRequestHasBeenSubmittedYouWillBeAbleToLogInAfterAnAdministratorHasApprovedYourRegistration, false, false); }
                else
                { this.DisplayFeedback(_GlobalResources.YourAccountHasBeenCreatedToAccessTheSystemPleaseLoginUsingTheUsernameAndPasswordYouEntered, false); }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _AutoLoginRegisteredUser
        /// <summary>
        /// Sets AsentiaSessionState parameters for the newly registered user so that they are logged in. 
        /// </summary>
        private void _AutoLoginRegisteredUser()
        {
            // set the user parameters
            AsentiaSessionState.IdSiteUser = this._UserObject.Id;
            AsentiaSessionState.UserCulture = this._UserObject.LanguageString;
            AsentiaSessionState.UserFirstName = this._UserObject.FirstName;
            AsentiaSessionState.UserLastName = this._UserObject.LastName;
            AsentiaSessionState.UserAvatar = this._UserObject.Avatar;
            AsentiaSessionState.UserGender = this._UserObject.Gender;
            AsentiaSessionState.IsUserACourseExpert = this._UserObject.IsUserACourseExpert;
            AsentiaSessionState.IsUserACourseApprover = this._UserObject.IsUserACourseApprover;
            AsentiaSessionState.IsUserASupervisor = this._UserObject.IsUserASupervisor;
            AsentiaSessionState.IsUserAWallModerator = this._UserObject.IsUserAWallModerator;
            AsentiaSessionState.IsUserAnILTInstructor = this._UserObject.IsUserAnILTInstructor;

            // get the impersonated user's effective permissions

            DataTable effectivePermissions = Asentia.UMS.Library.User.GetEffectivePermissions();
            List<AsentiaPermissionWithScope> effectivePermissionsList = new List<AsentiaPermissionWithScope>();

            foreach (DataRow row in effectivePermissions.Rows)
            {
                AsentiaPermissionWithScope permissionWithScope = new AsentiaPermissionWithScope((AsentiaPermission)Convert.ToInt32(row["idPermission"]), row["scope"].ToString());
                effectivePermissionsList.Add(permissionWithScope);
            }

            AsentiaSessionState.UserEffectivePermissions = effectivePermissionsList;
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/");
        }
        #endregion

        #region _LoginButton_Command
        /// <summary>
        /// Method to perform the user login.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">arguments</param>
        private void _LoginButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string lastActiveSessionId = "";
                bool simultaniousLogin = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_SIMULTANEOUSLOGIN_ENABLED);
                string loginPriority = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_LOGINPRIORITY);

                // make sure username and password are specified
                if (String.IsNullOrWhiteSpace(this.LoginFormControl.UsernameBox.Text)
                    || String.IsNullOrWhiteSpace(this.LoginFormControl.PasswordBox.Text))
                {
                    throw new AsentiaException(_GlobalResources.UsernameAndPasswordMustBeSpecified);
                }

                // authenticate login
                Library.Login.AuthenticateUser(this.LoginFormControl.UsernameBox.Text, this.LoginFormControl.PasswordBox.Text, out lastActiveSessionId);

                if (simultaniousLogin == false && loginPriority == "latest" && !String.IsNullOrWhiteSpace(lastActiveSessionId) && lastActiveSessionId != AsentiaSessionState.SessionId)
                { Library.Login.DeleteBySessionId(lastActiveSessionId); }

                // if we get here without throwing an exception, it means a successful login, so redirect to the landing page
                Response.Redirect(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_DEFAULTLANDINGPAGE_LOCALPATH));
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.LoginFormControl.DisplayFeedback(dnfEx.Message, true, true);
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                { this.LoginFormControl.ShowFormContentPanel(); }
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.LoginFormControl.DisplayFeedback(fnuEx.Message, true, true);
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                { this.LoginFormControl.ShowFormContentPanel(); }
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.LoginFormControl.DisplayFeedback(cpeEx.Message, true, true);
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                { this.LoginFormControl.ShowFormContentPanel(); }
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.LoginFormControl.DisplayFeedback(dEx.Message, true, true);
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                { this.LoginFormControl.ShowFormContentPanel(); }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.LoginFormControl.DisplayFeedback(ex.Message, true, true);
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                { this.LoginFormControl.ShowFormContentPanel(); }
            }
        }
        #endregion

        #region _ForgotPassword_Command
        /// <summary>
        /// Method to process the forgot password form submission from the modal.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">arguments</param>
        private void _ForgotPassword_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // make sure username is specified
                if (String.IsNullOrWhiteSpace(this.LoginFormControl.ForgotPasswordModalUsernameBox.Text))
                {
                    throw new AsentiaException(_GlobalResources.UsernameMustBeSpecified);
                }

                // reset the password
                string emailSentTo = Library.Login.ResetPassword(this.LoginFormControl.ForgotPasswordModalUsernameBox.Text);

                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(true);
                    this.LoginFormControl.DisplayFeedback(String.Format(_GlobalResources.YourPasswordHasBeenSuccessfullyResetAndSentToEmail, emailSentTo), false, false);
                }
                else
                {
                    // disable the submit button and show the success feedback in the forgot password modal
                    this.LoginFormControl.ForgotPasswordModal.DisableSubmitButton();
                    this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(String.Format(_GlobalResources.YourPasswordHasBeenSuccessfullyResetAndSentToEmail, emailSentTo), false);
                }

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // show the error
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(false);
                    this.LoginFormControl.DisplayFeedback(dnfEx.Message, true, false);
                }
                else
                { this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(dnfEx.Message, true); }
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // show the error
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(false);
                    this.LoginFormControl.DisplayFeedback(fnuEx.Message, true, false);
                }
                else
                { this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(fnuEx.Message, true); }
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // show the error
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(false);
                    this.LoginFormControl.DisplayFeedback(cpeEx.Message, true, false);
                }
                else
                { this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(cpeEx.Message, true); }
            }
            catch (DatabaseException dEx)
            {
                // show the error
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(false);
                    this.LoginFormControl.DisplayFeedback(dEx.Message, true, false);
                }
                else
                { this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(dEx.Message, true); }
            }
            catch (AsentiaException ex)
            {
                // show the error
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(false);
                    this.LoginFormControl.DisplayFeedback(ex.Message, true, false);
                }
                else
                { this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(ex.Message, true); }
            }
        }
        #endregion
    }

}