﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Data;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages.MyProfile
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel UserFormContentWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel UserFormWrapperContainer;
        public Panel UserFormContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private User _UserObject;
        private UserForm _UserForm;
        private Button _SaveButton = new Button();
        private Button _CancelButton = new Button();
        private ModalPopup _SelfDeletetionModal;
        private LinkButton _SelfDeletionButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.UMS.Pages.Administrator.Users.Modify.js");
        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get the user object
            this._GetUserObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.UserFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.UserFormWrapperContainer.CssClass = "FormContentContainer";

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetUserObject()
        {
            this._UserObject = new User(AsentiaSessionState.IdSiteUser);
            
            try
            {
                this._UserObject = new User(AsentiaSessionState.IdSiteUser);
            }
            catch
            {
                Response.Redirect("/");
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string userImagePath;
            string imageCssClass = null;
            string pageTitle;

            if (this._UserObject != null)
            {
                breadCrumbPageTitle = this._UserObject.DisplayName;

                if (this._UserObject.Avatar != null)
                {
                    userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    imageCssClass = "AvatarImage";
                }
                else
                {
                    if (this._UserObject.Gender == "f")
                    {
                        userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF,
                                                               ImageFiles.EXT_PNG);
                    }
                    else
                    {
                        userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                               ImageFiles.EXT_PNG);
                    }
                }

                pageTitle = this._UserObject.DisplayName;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.MyProfile;

                userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                       ImageFiles.EXT_PNG);

                pageTitle = _GlobalResources.MyProfile;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, pageTitle, userImagePath, imageCssClass);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.ModifyYourInformationUsingTheFormBelow, true);

            // build the self delete panel
            this._BuildObjectOptionsPanel();     

            // build the user form
            this._BuildUserForm();

            // build the form actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.Controls.Clear();
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            // build self deletion link if the site allow user self deletion
            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.PRIVACY_ALLOWSELFDELETION) == true)
            {
                Panel optionsPanelLinksContainer = new Panel();
                optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
                optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

                this._SelfDeletionButton = new LinkButton();
                this._SelfDeletionButton.ID = "SelfDeletionButton";

                // SELF DELETION
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("SelfDeletionLink",
                                                    this._SelfDeletionButton,
                                                    null,
                                                    null,
                                                    _GlobalResources.DeleteMyAccount,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_X, ImageFiles.EXT_PNG))
                    );

                this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);

                //Builds the confirmation modal for domain alias delete process.
                this._BuildDeleteSelfDeletionModal();
           }
        }
        #endregion

        #region _BuildDeleteSelfDeletionModal
        /// <summary>
        /// Builds the confirmation modal for domain alias delete process.
        /// </summary>
        private void _BuildDeleteSelfDeletionModal()
        {
            this._SelfDeletetionModal = new ModalPopup("SelfDeleteAction");

            // set modal properties
            this._SelfDeletetionModal.Type = ModalPopupType.Confirm;
            this._SelfDeletetionModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._SelfDeletetionModal.HeaderIconAlt = _GlobalResources.SelfDeletion;
            this._SelfDeletetionModal.HeaderText = _GlobalResources.SelfDeletion;
            this._SelfDeletetionModal.TargetControlID = this._SelfDeletionButton.ClientID;
            this._SelfDeletetionModal.SubmitButton.Command += new CommandEventHandler(this._SelfDeletionButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            body1.Text = _GlobalResources.WarningDeletingYourAccountWillRemoveAllDataAndTranscriptsAssociatedWithYourUserProfileThisActionIsNotReversible;
            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._SelfDeletetionModal.AddControlToBody(body1Wrapper);

            this.PageContentContainer.Controls.Add(this._SelfDeletetionModal);
        }
        #endregion

        #region _SelfDeletionButton_Command
        /// <summary>
        /// Handles the "Self Deletion" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelfDeletionButton_Command(object sender, CommandEventArgs e)
        {
            DataTable usersToDelete = new DataTable();
            usersToDelete.Columns.Add("id", typeof(int));
            usersToDelete.Rows.Add(AsentiaSessionState.IdSiteUser.ToString());

            Library.User.Delete(usersToDelete);

            //kill user sessions
            AsentiaAuthenticatedPage.UpdateUserSessionExpiration(null);
            AsentiaSessionState.EndSession(true);

            Response.Redirect("/");
        }
        #endregion

        #region _BuildUserForm
        /// <summary>
        /// Builds the user form.
        /// </summary>
        private void _BuildUserForm()
        {
            this.UserFormContainer.Controls.Clear();

            if (this._UserObject != null)
            { this._UserForm = new UserForm("UserModify", UserFormViewType.UserView, UserAccountDataFileType.Site, AsentiaSessionState.UserCulture, this._UserObject); }
            else
            { this._UserForm = new UserForm("UserModify", UserFormViewType.UserView, UserAccountDataFileType.Site, AsentiaSessionState.UserCulture); }

            this.UserFormContainer.Controls.Add(this._UserForm);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";

            if (this._UserObject == null)
            { this._SaveButton.Text = _GlobalResources.NewAccount; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // if there is no user object, create one
                if (this._UserObject == null)
                { this._UserObject = new User(); }

                // perform validation on the form data
                List<User.FieldError> formErrors = this._UserObject.ValidateDataFromForm(this._UserForm);

                // if there are errors, report them
                if (formErrors.Count > 0)
                {
                    this._UserForm.ProcessFormErrors(formErrors);
                    throw new AsentiaException();
                }

                // populate the object
                this._UserObject.FirstName = this._UserForm.FirstName;
                this._UserObject.MiddleName = this._UserForm.MiddleName;
                this._UserObject.LastName = this._UserForm.LastName;
                this._UserObject.Email = this._UserForm.Email;
                this._UserObject.Username = this._UserForm.Username;
                this._UserObject.Password = this._UserForm.Password;
                this._UserObject.MustChangePassword = Convert.ToBoolean(this._UserForm.MustChangePassword);
                this._UserObject.IdTimezone = Convert.ToInt32(this._UserForm.Timezone);
                this._UserObject.LanguageString = this._UserForm.Language;
                this._UserObject.DtExpires = (this._UserForm.DtExpires == null) ? (DateTime?)null : Convert.ToDateTime(this._UserForm.DtExpires);
                this._UserObject.IsActive = Convert.ToBoolean(this._UserForm.IsActive);
                this._UserObject.Company = this._UserForm.Company;
                this._UserObject.Address = this._UserForm.Address;
                this._UserObject.City = this._UserForm.City;
                this._UserObject.Province = this._UserForm.Province;
                this._UserObject.PostalCode = this._UserForm.PostalCode;
                this._UserObject.Country = this._UserForm.Country;
                this._UserObject.PhonePrimary = this._UserForm.PhonePrimary;
                this._UserObject.PhoneHome = this._UserForm.PhoneHome;
                this._UserObject.PhoneWork = this._UserForm.PhoneWork;
                this._UserObject.PhoneFax = this._UserForm.PhoneFax;
                this._UserObject.PhoneMobile = this._UserForm.PhoneMobile;
                this._UserObject.PhonePager = this._UserForm.PhonePager;
                this._UserObject.PhoneOther = this._UserForm.PhoneOther;
                this._UserObject.EmployeeId = this._UserForm.EmployeeID;
                this._UserObject.JobTitle = this._UserForm.JobTitle;
                this._UserObject.JobClass = this._UserForm.JobClass;
                this._UserObject.Division = this._UserForm.Division;
                this._UserObject.Region = this._UserForm.Region;
                this._UserObject.Department = this._UserForm.Department;
                this._UserObject.DtHire = (this._UserForm.DtHire == null) ? (DateTime?)null : Convert.ToDateTime(this._UserForm.DtHire);
                this._UserObject.DtTerm = (this._UserForm.DtTerm == null) ? (DateTime?)null : Convert.ToDateTime(this._UserForm.DtTerm);
                this._UserObject.Gender = this._UserForm.Gender;
                this._UserObject.Race = this._UserForm.Race;
                this._UserObject.DtDOB = (this._UserForm.DtDOB == null) ? (DateTime?)null : Convert.ToDateTime(this._UserForm.DtDOB);
                this._UserObject.Field00 = this._UserForm.Field00;
                this._UserObject.Field01 = this._UserForm.Field01;
                this._UserObject.Field02 = this._UserForm.Field02;
                this._UserObject.Field03 = this._UserForm.Field03;
                this._UserObject.Field04 = this._UserForm.Field04;
                this._UserObject.Field05 = this._UserForm.Field05;
                this._UserObject.Field06 = this._UserForm.Field06;
                this._UserObject.Field07 = this._UserForm.Field07;
                this._UserObject.Field08 = this._UserForm.Field08;
                this._UserObject.Field09 = this._UserForm.Field09;
                this._UserObject.Field10 = this._UserForm.Field10;
                this._UserObject.Field11 = this._UserForm.Field11;
                this._UserObject.Field12 = this._UserForm.Field12;
                this._UserObject.Field13 = this._UserForm.Field13;
                this._UserObject.Field14 = this._UserForm.Field14;
                this._UserObject.Field15 = this._UserForm.Field15;
                this._UserObject.Field16 = this._UserForm.Field16;
                this._UserObject.Field17 = this._UserForm.Field17;
                this._UserObject.Field18 = this._UserForm.Field18;
                this._UserObject.Field19 = this._UserForm.Field19;
                this._UserObject.OptOutOfEmailNotifications = Convert.ToBoolean(this._UserForm.OptOutOfEmailNotifications);
                this._UserObject.ExcludeFromLeaderboards = Convert.ToBoolean(this._UserForm.ExcludeFromLeaderboards);

                // do avatar if existing user, if its a new user, we'll do it after initial save
                bool isNewUser = true;

                if (this._UserObject.Id > 0)
                {
                    isNewUser = false;

                    if (this._UserForm.Avatar != null)
                    {
                        // check user folder existence and create if necessary
                        if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id)))
                        { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id)); }

                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._UserForm.Avatar)))
                        {
                            fullSavedFilePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar" + Path.GetExtension(this._UserForm.Avatar);
                            fullSavedFilePathSmall = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar.small" + Path.GetExtension(this._UserForm.Avatar);

                            // delete existing avatar images if any
                            if (this._UserObject.Avatar != null)
                            {
                                if (File.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar" + Path.GetExtension(this._UserObject.Avatar))))
                                { File.Delete(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar" + Path.GetExtension(this._UserObject.Avatar))); }

                                if (File.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar.small" + Path.GetExtension(this._UserObject.Avatar))))
                                { File.Delete(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar.small" + Path.GetExtension(this._UserObject.Avatar))); }
                            }

                            // move the uploaded file into the user's folder
                            File.Copy(Server.MapPath(this._UserForm.Avatar), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._UserForm.Avatar));
                            this._UserObject.Avatar = "avatar" + Path.GetExtension(this._UserForm.Avatar);

                            // create a smaller version of the avatar fo use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                        }
                        else
                        { 
                            this._UserObject.Avatar = null; 
                        }
                    }
                    else if (this._UserForm.ClearAvatar != null)
                    {
                        if (this._UserForm.ClearAvatar.Value == "true")
                        {
                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar" + Path.GetExtension(this._UserObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar" + Path.GetExtension(this._UserObject.Avatar))); }

                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar.small" + Path.GetExtension(this._UserObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar.small" + Path.GetExtension(this._UserObject.Avatar))); }

                            this._UserObject.Avatar = null;

                            this._UserObject.SaveAvatar();
                        }
                    }
                    else
                    { }
                }

                // save the user, save its returned id to viewstate, and 
                // instansiate a new user object with the id
                int id = this._UserObject.Save();
                this.ViewState["id"] = id;
                this._UserObject.Id = id;

                // if this was a new user we just saved, we now have the id so that
                // we can create the user's folder and update the avatar if necessary.
                if (isNewUser)
                {
                    // check user folder existence and create if necessary
                    if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id)))
                    { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id)); }

                    // if there is an avatar, move it and update the database record
                    if (this._UserForm.Avatar != null)
                    {
                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._UserForm.Avatar)))
                        {
                            fullSavedFilePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar" + Path.GetExtension(this._UserForm.Avatar);
                            fullSavedFilePathSmall = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + "avatar.small" + Path.GetExtension(this._UserForm.Avatar);

                            // move the uploaded file into the user's folder
                            File.Copy(Server.MapPath(this._UserForm.Avatar), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._UserForm.Avatar));
                            this._UserObject.Avatar = "avatar" + Path.GetExtension(this._UserForm.Avatar);

                            // create a smaller version of the avatar fo use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            this._UserObject.SaveAvatar();
                        }
                    }
                }

                // load the saved user object
                this._UserObject = new User(id);

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedback(_GlobalResources.YourProfileHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/dashboard/");
        }
        #endregion
    }
}
