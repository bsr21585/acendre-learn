﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.UMS.Pages.MyProfile
{
    public class ChangePassword : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ChangePasswordFormContentWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel ChangePasswordFormWrapperContainer;
        public Panel ChangePasswordFormContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private User _UserObject;
        private Button _SaveButton = new Button();

        private string _PasswordRegularExpression;
        private string _PasswordFieldDescriptionInLanguage;
        private TextBox _PasswordField;
        private TextBox _ConfirmPasswordField;
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // bounce if the caller is admin
            if (AsentiaSessionState.IdSiteUser == 1)
            { Response.Redirect("/"); }

            // bounce if the caller is not forced to change password
            if (!AsentiaSessionState.UserMustChangePassword)
            { Response.Redirect("~/dashboard/"); }

            // get the user object
            this._GetUserObject();

            this.ChangePasswordFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ChangePasswordFormWrapperContainer.CssClass = "FormContentContainer";

            // get the user account data configured password field information
            this._GetUserAccountDataConfiguredPasswordFieldInformation();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on the session state.
        /// </summary>
        private void _GetUserObject()
        {
            this._UserObject = new User(AsentiaSessionState.IdSiteUser);

            try
            {
                this._UserObject = new User(AsentiaSessionState.IdSiteUser);
            }
            catch
            {
                Response.Redirect("/");
            }
        }
        #endregion

        #region _GetUserAccountDataConfiguredPasswordFieldInformation
        /// <summary>
        /// Gets the user account data configuration information for the password field.
        /// </summary>
        private void _GetUserAccountDataConfiguredPasswordFieldInformation()
        {
            UserAccountData uad = new UserAccountData(UserAccountDataFileType.Site, true, false);

            foreach (UserAccountData.Tab tab in uad.Tabs)
            {
                foreach (UserAccountData.UserField userField in tab.UserFields)
                {
                    if (userField.Identifier == "password")
                    {
                        this._PasswordFieldDescriptionInLanguage = HttpUtility.HtmlEncode(uad.GetUserFieldDescriptionInLanguage(userField, AsentiaSessionState.UserCulture));
                        this._PasswordRegularExpression = userField.RegularExpression;
                        return;
                    }
                }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string userImagePath;
            string imageCssClass = null;
            string pageTitle;

            breadCrumbPageTitle = _GlobalResources.ChangePassword;

            userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                    ImageFiles.EXT_PNG);

            pageTitle = _GlobalResources.ChangePassword;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, pageTitle, userImagePath, imageCssClass);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.YouAreRequiredToChangeYourPassword, true);

            // build the change password form
            this._BuildChangePasswordForm();

            // build the form actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _BuildChangePasswordForm
        /// <summary>
        /// Builds the change password form.
        /// </summary>
        private void _BuildChangePasswordForm()
        {
            this.ChangePasswordFormContainer.Controls.Clear();

            // old password field
            Label oldPasswordText = new Label();
            oldPasswordText.ID = "OldPasswordField_Field";
            oldPasswordText.Text = "********";

            this.ChangePasswordFormContainer.Controls.Add(AsentiaPage.BuildFormField("OldPasswordField",
                                                          _GlobalResources.OldPassword,
                                                          oldPasswordText.ID,
                                                          oldPasswordText,
                                                          false,
                                                          false,
                                                          false));

            // new password field
            List<Control> newPasswordInputControls = new List<Control>();

            if (!String.IsNullOrWhiteSpace(this._PasswordFieldDescriptionInLanguage))
            {
                Panel newPasswordDescriptionContainer = new Panel();
                newPasswordDescriptionContainer.ID = "NewPasswordField_DescriptionContainer";
                newPasswordDescriptionContainer.CssClass = "FormFieldDescriptionContainer";

                Literal newPasswordDescription = new Literal();
                newPasswordDescription.Text = this._PasswordFieldDescriptionInLanguage;
                newPasswordDescriptionContainer.Controls.Add(newPasswordDescription);

                newPasswordInputControls.Add(newPasswordDescriptionContainer);
            }

            this._PasswordField = new TextBox();
            this._PasswordField.ID = "PasswordField_Field";
            this._PasswordField.TextMode = TextBoxMode.Password;
            this._PasswordField.CssClass = "InputMedium";
            newPasswordInputControls.Add(this._PasswordField); 

            this._ConfirmPasswordField = new TextBox();
            this._ConfirmPasswordField.ID = "ConfirmPasswordField_Field";
            this._ConfirmPasswordField.TextMode = TextBoxMode.Password;
            this._ConfirmPasswordField.CssClass = "InputMedium";

            Panel confirmPasswordLabelContainer = new Panel();
            confirmPasswordLabelContainer.ID = "ConfirmPasswordField_DescriptionContainer";
            confirmPasswordLabelContainer.CssClass = "FormFieldDescriptionContainer";

            Literal confirmPasswordLabel = new Literal();
            confirmPasswordLabel.Text = _GlobalResources.ConfirmByEnteringAgain;
            confirmPasswordLabelContainer.Controls.Add(confirmPasswordLabel);

            newPasswordInputControls.Add(confirmPasswordLabelContainer);
            newPasswordInputControls.Add(this._ConfirmPasswordField);

            this.ChangePasswordFormContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("PasswordField",
                                                                                                         _GlobalResources.NewPassword,
                                                                                                         newPasswordInputControls,
                                                                                                         true,
                                                                                                         true,
                                                                                                         true));
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.ChangePassword;

            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);
        }
        #endregion

        #region _ValidateChangePasswordForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateChangePasswordForm()
        {
            bool isValid = true;

            // validate required
            if (String.IsNullOrWhiteSpace(this._PasswordField.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.ChangePasswordFormContainer, "PasswordField", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
            }
                            
            // ensure password and confirmation match
            if (this._PasswordField.Text != this._ConfirmPasswordField.Text)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.ChangePasswordFormContainer, "PasswordField", _GlobalResources.Password + " " + _GlobalResources.MustMatchConfirmationField);
            }

            // validate against regular expression for the field - if there is one
            if (!String.IsNullOrWhiteSpace(this._PasswordRegularExpression) && !String.IsNullOrWhiteSpace(this._PasswordField.Text))
            {
                Match match = Regex.Match(this._PasswordField.Text, this._PasswordRegularExpression, RegexOptions.IgnoreCase);

                if (!match.Success)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.ChangePasswordFormContainer, "PasswordField", _GlobalResources.Password + " " + _GlobalResources.IsInvalid);
                }
            }

            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // if there is no user object, ERROR!
                if (this._UserObject == null)
                {
                    // display an error feedback
                    this.DisplayFeedback(_GlobalResources.AnUnknownErrorOccurredWhileProcessingYourRequest, true);

                    // clear the form container and actions panel
                    this.ChangePasswordFormContainer.Controls.Clear();
                    this.ActionsPanel.Controls.Clear();
                }
                else
                {
                    // validate the form
                    if (!this._ValidateChangePasswordForm())
                    { throw new AsentiaException(); }

                    // change the password
                    this._UserObject.ChangePassword(this._PasswordField.Text);

                    // free up the must change password flag
                    AsentiaSessionState.UserMustChangePassword = false;

                    // disable the fields
                    this._PasswordField.Enabled = false;
                    this._ConfirmPasswordField.Enabled = false;
                    this._SaveButton.Enabled = false;

                    // display the saved feedback and register script to redirect on click of feedback ok button
                    this.DisplayFeedback(_GlobalResources.YourPasswordHasBeenChangedSuccessfullyClickOKToGoToYourDashboard, false);
                    ScriptManager.RegisterStartupScript(this.ChangePasswordFormContainer, typeof(ChangePassword), "RedirectToDashboard", "$(\"#FeedbackInformationStatusPanelCloseButton\").click(function () { window.location.href = \"/dashboard\"; });", true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseFieldConstraintException fceEx)
            {
                // display the failure message
                this.DisplayFeedback(fceEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion        
    }
}
