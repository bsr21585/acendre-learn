﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Asentia.UMS")]
[assembly: AssemblyDescription("Libraries for the Asentia UMS.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ICS Learning Group")]
[assembly: AssemblyProduct("Asentia.UMS")]
[assembly: AssemblyCopyright("Copyright © ICS Learning Group 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("eb2491d9-165b-43a3-8bed-cb3ab8a8a58a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.X.*")]
[assembly: AssemblyVersion("1.16.*")]

[assembly: WebResource("Asentia.UMS.Pages.Administrator.System.Users.UserFieldConfiguration.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.System.Interface.Masthead.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.System.Interface.Footer.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.System.Interface.ImageEditor.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.System.Interface.LayoutEditor.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.System.Interface.Themes.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.Groups.Modify.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.Users.BatchUpload.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.Users.Modify.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.Users.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.System.API.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Pages.Administrator.System.xAPIEndpoints.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.UMS.Controls.UserForm.js", "text/javascript")]