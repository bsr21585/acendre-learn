﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Asentia.Common
{
    public class Timezone
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Timezone()
        { ;}

        /// <summary>
        /// Overload Constructor for Already Selected TimeZoneID
        /// </summary>
        /// <param name="id">TimeZone ID</param>
        public Timezone(int id)
        { _Details(id); }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[TimeZone.GetGrid]";

        /// <summary>
        /// TimeZone Id.
        /// </summary>
        public int idTimezone = 0;

        /// <summary>
        /// Name of Timezone. in dotNet
        /// </summary>
        /// <seealso cref="Site" />
        public string dotNetName;

        /// <summary>
        /// Displayable Name of Timezone.
        /// </summary>
        public string displayName;

        /// <summary>
        /// gmt Off Set From GMT.
        /// </summary>
        public double gmtOffset;

        /// <summary>
        /// IS Day Light Enabled in that Selected Timezone.
        /// </summary>
        public bool blnUseDaylightSavings;

        /// <summary>
        /// Oredr of timezone
        /// </summary>
        public int? order;

        /// <summary>
        /// is TimeZone Enabled.
        /// </summary>
        public bool? isEnabled;
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idTimezone">TimeZone Id</param>
        private void _Details(int idTimezone)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idTimezone", idTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@dotNetName", this.dotNetName, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@displayName", this.displayName, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@gmtOffset", this.gmtOffset, SqlDbType.Float, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@blnUseDaylightSavings", this.blnUseDaylightSavings, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@order", this.order, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@isEnabled", this.isEnabled, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[TimeZone.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.idTimezone = idTimezone;
                this.dotNetName = Convert.ToString(databaseObject.Command.Parameters["@dotNetName"].Value);
                this.displayName = Convert.ToString(databaseObject.Command.Parameters["@displayName"].Value);
                this.gmtOffset = Convert.ToDouble(databaseObject.Command.Parameters["@gmtOffset"].Value);

                this.blnUseDaylightSavings = Convert.ToBoolean(databaseObject.Command.Parameters["@blnUseDaylightSavings"].Value);
                this.order = Convert.ToInt32(databaseObject.Command.Parameters["@order"].Value);
                this.isEnabled = Convert.ToBoolean(databaseObject.Command.Parameters["@isEnabled"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region GetTimezones
        /// <summary>
        /// Returns a recordset of timezone ids and names.
        /// </summary>
        /// <returns>DataTable of timezone ids and names.</returns>
        public static DataTable GetTimezones()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[System.GetTimezones]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        /// <summary>
        /// Returns a recordset of timezone ids and names.
        /// Overridden to allow specified site, culture, and caller instead of from session state. 
        /// </summary>
        /// <returns>DataTable of timezone ids and names.</returns>
        public static DataTable GetTimezones(int idSite, string userCulture, int idCaller, string accountWebConfigPath = null)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject;

            if (!String.IsNullOrWhiteSpace(accountWebConfigPath))
            { databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath); }
            else
            { databaseObject = new AsentiaDatabase(); }

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", userCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[System.GetTimezones]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetTimezoneNameFromDotnetName
        /// <summary>
        /// Gets the timezone display name from dotnet name
        /// </summary>
        /// <param name="dotnetName">Dotnet Name</param>
        /// <returns></returns>
        public static string GetTimezoneNameFromDotnetName(string dotnetName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", null, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", null, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@dotNetName", dotnetName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@displayName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Timezone.GetDisplayNameFromDotnetName]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@displayName"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
