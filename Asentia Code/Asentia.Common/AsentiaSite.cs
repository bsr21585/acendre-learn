﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace Asentia.Common
{
    #region Interfaces
    /// <summary>
    /// Interface for reading only.
    /// </summary>    
    public interface AsentiaSite_ReadOnly
    {
        int Id { get; }
        string Hostname { get; }
        string Password { get; }
        bool IsActive { get; }
        DateTime? Expires { get; }
        string Title { get; }
        string Company { get; }
        string ContactName { get; }
        string ContactEmail { get; }
        int UserCount { get; }
        int UsersCurrentlyLoggedInCount { get; }
        int? UserLimit { get; }        
        int? PctUserLimitUsed { get; }
        int KbCount { get; }
        decimal MbCount { get; }
        decimal GbCount { get; }
        int? KbLimit { get; }
        decimal? MbLimit { get; }
        decimal? GbLimit { get; }
        int? PctKbLimitUsed { get; }
        string LanguageString { get; }
        int IdLanguage { get; }
        int IdTimezone { get; }
        DateTime? DtLicenseAgreementExecuted { get; }
        DateTime? DtUserAgreementModified { get; }
        ArrayList AvailableLanguages { get; }
        string ParamString(string paramName);
        int? ParamInt(string paramName);
        decimal? ParamDecimal(string paramName);
        bool? ParamBool(string paramName);
        DateTime? ParamDateTime(string paramName);
    }
    #endregion

    [Serializable]
    public class AsentiaSite : AsentiaSite_ReadOnly
    {
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public AsentiaSite()
        {
            this.Id = 0;
            this.AvailableLanguages = new ArrayList();
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor used when loading a site for read only where the caller's site is the same as the
        /// site being loaded, i.e. no need for for an idCallerSite.
        /// It loads the site object with the language specific properites for the user's current culture.
        /// </summary>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        public AsentiaSite(int idCaller, int idSite)
        {
            this.AvailableLanguages = new ArrayList();
            this.LanguageSpecificProperties = new ArrayList();
            this._Initialize(idSite, AsentiaSessionState.UserCulture, idCaller, idSite, false);
        }

        /// <summary>
        /// Constructor used when loading a site for read only where the caller's site is not the same as the
        /// site being loaded, i.e. called from the "control panel."
        /// It loads the site object with the language specific properites for the user's current culture.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        public AsentiaSite(int idCallerSite, int idCaller, int idSite)
        {
            this.AvailableLanguages = new ArrayList();
            this.LanguageSpecificProperties = new ArrayList();
            this._Initialize(idCallerSite, AsentiaSessionState.UserCulture, idCaller, idSite, false);
        }

        /// <summary>
        /// Constructor used when loading a site for read/write where the caller's site is the same as the
        /// site being loaded, i.e. no need for for an idCallerSite.
        /// It loads the site object with the default language specific properties and loads a language object 
        /// containing language specific properties for all available languages.
        /// </summary>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        /// <param name="forEditing">for editing flag</param>
        public AsentiaSite(int idCaller, int idSite, bool forEditing)
        {
            this.AvailableLanguages = new ArrayList();
            this.LanguageSpecificProperties = new ArrayList();
            this._Initialize(idSite, String.Empty, idCaller, idSite, forEditing);
        }

        /// <summary>
        /// Constructor used when loading a site for read/write where the caller's site is not the same as the
        /// site being loaded, i.e. called from the "control panel."
        /// It loads the site object with the default language specific properties and loads a language object 
        /// containing language specific properties for all available languages.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        /// <param name="forEditing">for editing flag</param>
        public AsentiaSite(int idCallerSite, int idCaller, int idSite, bool forEditing)
        {
            this.AvailableLanguages = new ArrayList();
            this.LanguageSpecificProperties = new ArrayList();
            this._Initialize(idCallerSite, String.Empty, idCaller, idSite, forEditing);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Site.GetGrid]";

        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Favicon
        /// </summary>
        public string Favicon { get; set; }

        /// <summary>
        /// Hostname
        /// </summary>
        public string Hostname { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Expires
        /// </summary>
        public DateTime? Expires { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Company
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// ContactName
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// ContactEmail
        /// </summary>
        public string ContactEmail { get; set; }

        /// <summary>
        /// UserCount
        /// </summary>
        public int UserCount { get; set; }

        /// <summary>
        /// UsersCurrentlyLoggedInCount
        /// </summary>
        public int UsersCurrentlyLoggedInCount { get; set; }

        /// <summary>
        /// UserLimit
        /// </summary>
        public int? UserLimit { get; set; }

        /// <summary>
        /// Percent User Limit Used
        /// </summary>
        public int? PctUserLimitUsed { get; set; }

        /// <summary>
        /// KbCount
        /// </summary>
        public int KbCount { get; set; }

        /// <summary>
        /// MbCount
        /// </summary>
        public decimal MbCount { get; set; }

        /// <summary>
        /// GbCount
        /// </summary>
        public decimal GbCount { get; set; }

        /// <summary>
        /// KbLimit
        /// </summary>
        public int? KbLimit { get; set; }

        /// <summary>
        /// MbLimit
        /// </summary>
        public decimal? MbLimit { get; set; }

        /// <summary>
        /// GbLimit
        /// </summary>
        public decimal? GbLimit { get; set; }

        /// <summary>
        /// Percent Kb Limit Used
        /// </summary>
        public int? PctKbLimitUsed { get; set; }

        /// <summary>
        /// LanguageString
        /// </summary>
        public string LanguageString { get; set; }

        /// <summary>
        /// IdLanguage
        /// </summary>
        public int IdLanguage { get; set; }

        /// <summary>
        /// IdTimezone
        /// </summary>
        public int IdTimezone { get; set; }

        /// <summary>
        /// DtLicenseAgreementExecuted
        /// </summary>
        public DateTime? DtLicenseAgreementExecuted { get; set; }

        /// <summary>
        /// DtLicenseAgreementModified
        /// </summary>
        public DateTime? DtUserAgreementModified { get; set; }

        /// <summary>
        /// List of languages that are available to the site.
        /// </summary>
        public ArrayList AvailableLanguages { get; set; }

        /// <summary>
        /// ArrayList of language specific properties of type LanguageSpecificProperty
        /// </summary>
        public ArrayList LanguageSpecificProperties { get; set; }
        #endregion

        #region Private Properties
        /// <summary>
        /// ArrayList of site parameters of type _SiteParam
        /// </summary>
        private ArrayList _SiteParams = new ArrayList();
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties of a Site.
        /// </summary>
        [Serializable]
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Title;
            public string Company;

            public LanguageSpecificProperty(string langString, string title, string company)
            {
                this.LangString = langString;
                this.Title = title;
                this.Company = company;
            }
        }
        #endregion

        #region Private Classes
        /// <summary>
        /// Class that represeents the properties of a Site Parameter.
        /// </summary>
        [Serializable]
        private class _SiteParam
        {
            public string Param;
            public string Value;

            public _SiteParam(string param, string value)
            {
                this.Param = param;
                this.Value = value;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves account information.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <returns>id of the saved site</returns>
        public int Save(int idCallerSite, string callerLangString, int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@hostname", this.Hostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@favicon", this.Favicon, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@isActive", this.IsActive, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@company", this.Company, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@contactName", this.ContactName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@contactEmail", this.ContactEmail, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", this.LanguageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@password", this.Password, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@dtExpires", this.Expires, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@userLimit", this.UserLimit, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@kbLimit", this.KbLimit, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@dtLicenseAgreementExecuted", this.DtLicenseAgreementExecuted, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@dtUserAgreementModified", this.DtUserAgreementModified, SqlDbType.DateTime, 8, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Site.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // get the returned site id
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idSite"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return this.Id;
        }
        #endregion

        #region SavePropertiesInLanguage
        /// <summary>
        /// Saves "language-specific" properties for site.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="languageString">the language</param>
        /// <param name="title">site title</param>
        /// <param name="company">site company</param>
        public void SavePropertiesInLanguage(int idCallerSite, string callerLangString, int idCaller, string languageString, string title, string company)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureSiteSavePropertiesInLanguageCannot); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@title", title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@company", company, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Site.SavePropertiesInLanguage]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ParamString
        /// <summary>
        /// Gets a site parameter value from the collection and returns it as a string.
        /// </summary>
        /// <param name="paramName">parameter name ("key")</param>
        /// <returns>string containing the site parameter value</returns>
        public string ParamString(string paramName)
        {
            try
            { return (string)_ParamObject(paramName); }
            catch (Exception e)
            { throw new AsentiaException(String.Format(_GlobalResources.SiteParamNotString, paramName), e); }
        }
        #endregion

        #region ParamInt
        /// <summary>
        /// Gets a site parameter value from the collection and returns it as an integer.
        /// </summary>
        /// <param name="paramName">parameter name ("key")</param>
        /// <returns>integer containing the site parameter value</returns>
        public int? ParamInt(string paramName)
        {
            try
            { return Convert.ToInt32(_ParamObject(paramName)); }
            catch (Exception e)
            { throw new AsentiaException(String.Format(_GlobalResources.SiteParameterNotInteger, paramName), e); }
        }
        #endregion

        #region ParamDecimal
        /// <summary>
        /// Gets a site parameter value from the collection and returns it as a decimal.
        /// </summary>
        /// <param name="paramName">parameter name ("key")</param>
        /// <returns>decimal containing the site parameter value</returns>
        public decimal? ParamDecimal(string paramName)
        {
            try
            { return Convert.ToDecimal(_ParamObject(paramName)); }
            catch (Exception e)
            { throw new AsentiaException(String.Format(_GlobalResources.SiteParameterNotDecimal, paramName), e); }
        }
        #endregion

        #region ParamBool
        /// <summary>
        /// Gets a site parameter value from the collection and returns it as a bool.
        /// </summary>
        /// <param name="paramName">parameter name ("key")</param>
        /// <returns>bool containing the site parameter value</returns>
        public bool? ParamBool(string paramName)
        {
            try
            { return Convert.ToBoolean(_ParamObject(paramName)); }
            catch (Exception e)
            { throw new AsentiaException(String.Format(_GlobalResources.SiteParameterNotBoolean, paramName), e); }
        }
        #endregion

        #region ParamDateTime
        /// <summary>
        /// Gets a site parameter value from the collection and returns it as a DateTime.
        /// </summary>
        /// <param name="paramName">parameter name ("key")</param>
        /// <returns>DateTime containing the site parameter value</returns>
        public DateTime? ParamDateTime(string paramName)
        {
            try
            { return Convert.ToDateTime(_ParamObject(paramName)); }
            catch (Exception e)
            { throw new AsentiaException(String.Format(_GlobalResources.SiteParameterNotDateTime, paramName), e); }
        }
        #endregion

        #region SaveAvailableLanguages
        /// <summary>
        /// Saves the languages that are available to a site.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        /// <param name="languageIds">list of language ids</param>
        public void SaveAvailableLanguages(int idCallerSite, string callerLangString, int idCaller, int idSite, DataTable languageIds)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", String.Empty, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languages", languageIds, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {

                databaseObject.ExecuteNonQuery("[Site.SaveAvailableLanguages]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveExecutedLicenseAgreement
        /// <summary>
        /// Saves the executed license agreement info in the linking table
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        /// <param name="languageIds">list of language ids</param>
        public void SaveExecutedLicenseAgreement(int idCallerSite, string callerLangString, int idCaller, DateTime dtExecuted, Guid executeLicenseAgreementGUID, string licenseAgreementIPAddress)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", String.Empty, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@dtExecuted", dtExecuted, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@executeLicenseAgreementGUID", executeLicenseAgreementGUID.ToString(), SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@licenseAgreementIPAddress", licenseAgreementIPAddress, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            try
            {

                databaseObject.ExecuteNonQuery("[LicenseAgreementToSiteLink.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes a site.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="deletees">DataTable of site(s) to delete</param>
        public static DataTable Delete(int idCallerSite, string callerLangString, int idCaller, DataTable deletees)
        {
            DataTable dt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase(300);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Sites", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Site.Delete]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveUserAgreementModifiedDate
        /// <summary>
        /// Save User Agreement Modified Date
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        public static void SaveUserAgreementModifiedDate(int idCallerSite, string callerLangString, int idCaller)
        {
            DataTable dt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Site.SaveUserAgreementModifiedDate]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveSiteParams
        /// <summary>
        /// Saves site parameters.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        /// <param name="siteParams">DataTable of site params</param>
        public static void SaveSiteParams(int idCallerSite, string callerLangString, int idCaller, int idSite, DataTable siteParams)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@SiteParams", siteParams, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Site.SaveParams]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DeleteSiteParams
        /// <summary>
        /// Deletes site param(s).
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        /// <param name="siteParams">DataTable of site params</param>
        public static void DeleteSiteParams(int idCallerSite, string callerLangString, int idCaller, int idSite, DataTable siteParams)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@SiteParams", siteParams, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Site.DeleteParams]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region AttachLanguages
        /// <summary>
        /// Attaches languages to a site.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        /// <param name="languages">DataTable of language(s)</param>
        public static void AttachLanguages(int idCallerSite, string callerLangString, int idCaller, int idSite, DataTable languages)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Languages", languages, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Site.AttachLanguages]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DetachLanguages
        /// <summary>
        /// Detaches languages from a site.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        /// <param name="languages">DataTable of languages</param>
        public static void DetachLanguages(int idCallerSite, string callerLangString, int idCaller, int idSite, DataTable languages)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Languages", languages, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Site.DetachLanguages]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetStatisticsForAllSites
        /// <summary>
        /// Gets a listing of all sites and their statistics.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <returns>DataTable of sites and their statistics</returns>
        public static DataTable GetStatisticsForAllSites(int idCallerSite, string callerLangString, int idCaller)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Site.GetStatisticsForAllSites]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Initialize
        /// <summary>
        /// Method to load site properties, parameters, and language specific properties (if for editing).
        /// Called by the constructors.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        /// <param name="forEditing">for editing flag</param>
        private void _Initialize(int idCallerSite, string callerLangString, int idCaller, int idSite, bool forEditing)
        {
            // get site properties
            this._Details(idCallerSite, callerLangString, idCaller, idSite);

            // get site parameters
            this._GetSiteParams(idCallerSite, callerLangString, idCaller, idSite);

            // get available languages
            this._GetAvailableLanguages(idCallerSite, callerLangString, idCaller, idSite);

            // if this is for editing, get the language specific properties
            if (forEditing)
            { this._GetPropertiesInLanguages(idCallerSite, String.Empty, idCaller, idSite); }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Loads the properties for a site.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        private void _Details(int idCallerSite, string callerLangString, int idCaller, int idSite)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@hostname", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@favicon", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@password", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@isActive", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtExpires", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@title", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@company", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@contactName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@contactEmail", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userLimit", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@userCount", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@usersCurrentlyLoggedInCount", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@kbLimit", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@kbCount", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@languageString", null, SqlDbType.NVarChar, 10, ParameterDirection.Output);
            databaseObject.AddParameter("@idLanguage", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idTimezone", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dtLicenseAgreementExecuted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtUserAgreementModified", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Site.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // populate the properties
                this.Id = idSite;
                this.Hostname = databaseObject.Command.Parameters["@hostname"].Value.ToString();
                this.Favicon = databaseObject.Command.Parameters["@favicon"].Value.ToString();
                this.Password = databaseObject.Command.Parameters["@password"].Value.ToString();
                this.IsActive = Convert.ToBoolean(databaseObject.Command.Parameters["@isActive"].Value);
                this.Expires = databaseObject.Command.Parameters["@dtExpires"].Value == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(databaseObject.Command.Parameters["@dtExpires"].Value);
                this.Title = databaseObject.Command.Parameters["@title"].Value.ToString();
                this.Company = databaseObject.Command.Parameters["@company"].Value.ToString();
                this.ContactName = databaseObject.Command.Parameters["@contactName"].Value.ToString();
                this.ContactEmail = databaseObject.Command.Parameters["@contactEmail"].Value.ToString();
                this.UserLimit = databaseObject.Command.Parameters["@userLimit"].Value == DBNull.Value ? (int?)null : Convert.ToInt32(databaseObject.Command.Parameters["@userLimit"].Value);
                this.UserCount = Convert.ToInt32(databaseObject.Command.Parameters["@userCount"].Value);
                this.UsersCurrentlyLoggedInCount = Convert.ToInt32(databaseObject.Command.Parameters["@usersCurrentlyLoggedInCount"].Value);
                this.KbLimit = databaseObject.Command.Parameters["@kbLimit"].Value == DBNull.Value ? (int?)null : Convert.ToInt32(databaseObject.Command.Parameters["@kbLimit"].Value);
                this.KbCount = Convert.ToInt32(databaseObject.Command.Parameters["@kbCount"].Value);
                this.LanguageString = databaseObject.Command.Parameters["@languageString"].Value.ToString();
                this.IdLanguage = Convert.ToInt32(databaseObject.Command.Parameters["@idLanguage"].Value);
                this.IdTimezone = Convert.ToInt32(databaseObject.Command.Parameters["@idTimezone"].Value);
                this.DtLicenseAgreementExecuted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtLicenseAgreementExecuted"].Value);
                this.DtUserAgreementModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtUserAgreementModified"].Value);

                // calculate MB and GB counts from KbCount
                if (this.KbCount > 0)
                {
                    this.MbCount = Decimal.Round((decimal)this.KbCount / (decimal)1024, 2);
                    this.GbCount = Decimal.Round((decimal)this.MbCount / (decimal)1024, 2);
                }
                else
                {
                    this.MbCount = 0;
                    this.GbCount = 0;
                }

                // calculate MB and GB limits from KbLimit
                if (this.KbLimit > 0)
                {
                    this.MbLimit = Decimal.Round((decimal)this.KbLimit / (decimal)1024, 2);
                    this.GbLimit = Decimal.Round((decimal)this.MbLimit / (decimal)1024, 2);
                }
                else
                {
                    this.MbLimit = null;
                    this.GbLimit = null;
                }

                // calculate PctUserLimitUsed from UserCount and UserLimit
                if (this.UserLimit != null && this.UserLimit > 0 && this.UserCount > 0)
                { this.PctUserLimitUsed = (int)Decimal.Round((((decimal)this.UserCount / (decimal)this.UserLimit) * 100), 0); }
                else if (this.UserLimit != null && this.UserLimit > 0 && this.UserCount == 0)
                { this.PctUserLimitUsed = 0; }
                else
                { this.PctUserLimitUsed = null; }
                
                // calculate PctKbLimitUsed from KbCount and KbLimit
                if (this.KbLimit != null && this.KbLimit > 0 && this.KbCount > 0)
                { this.PctKbLimitUsed = (int)Decimal.Round((((decimal)this.KbCount / (decimal)this.KbLimit) * 100), 0); }
                else if (this.KbLimit != null && this.KbLimit > 0 && this.KbCount == 0)
                { this.PctKbLimitUsed = 0; }
                else
                { this.PctKbLimitUsed = null; }
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetSiteParams
        /// <summary>
        /// Loads the site parameters for a site into a collection.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        private void _GetSiteParams(int idCallerSite, string callerLangString, int idCaller, int idSite)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", String.Empty, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sqlDataReader = databaseObject.ExecuteDataReader("[Site.GetParams]", true);

                // loop through the returned recordset, instansiate a _SiteParam object
                // and add it to the _SiteParams ArrayList
                while (sqlDataReader.Read())
                {
                    _SiteParams.Add(
                        new _SiteParam(
                            sqlDataReader["param"].ToString(),
                            sqlDataReader["value"].ToString()
                            )
                    );
                }

                sqlDataReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetAvailableLanguages
        /// <summary>
        /// Gets the languages that are available to a site.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        private void _GetAvailableLanguages(int idCallerSite, string callerLangString, int idCaller, int idSite)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", String.Empty, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sqlDataReader = databaseObject.ExecuteDataReader("[Site.GetAvailableLanguages]", true);

                // loop through the returned recordset, add each language string to 
                // the AvailableLanguages ArrayList
                while (sqlDataReader.Read())
                {
                    AvailableLanguages.Add(sqlDataReader["langString"].ToString());
                }

                sqlDataReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a site into a collection.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        private void _GetPropertiesInLanguages(int idCallerSite, string callerLangString, int idCaller, int idSite)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", String.Empty, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sqlDataReader = databaseObject.ExecuteDataReader("[Site.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a _SiteLanguage object
                // and add it to the _SiteLanguages ArrayList
                while (sqlDataReader.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sqlDataReader["langString"].ToString(),
                            sqlDataReader["title"].ToString(),
                            sqlDataReader["company"].ToString()
                            )
                    );
                }

                sqlDataReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _ParamObject
        /// <summary>
        /// Gets a site parameter value from the collection and returns it as an object.
        /// Used by the public, type specific methods to get the value of a site parameter.
        /// </summary>
        /// <param name="paramName">parameter name ("key")</param>
        /// <returns>object containing the site parameter value</returns>
        private object _ParamObject(string paramName)
        {
            foreach (_SiteParam param in _SiteParams)
            {
                if (param.Param == paramName)
                { return param.Value; }
            }

            return null;
        }
        #endregion
        #endregion
    }
}
