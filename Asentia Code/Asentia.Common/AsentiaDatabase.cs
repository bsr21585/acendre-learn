﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace Asentia.Common
{
    public class AsentiaDatabase : IDisposable
    {
        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AsentiaDatabase()
        {
            // this constructor does not override command timeout, so set that
            this._CommandTimeoutOverridden = false;

            // build the connection string
            this._BuildConnectionString();

            // open the connection to the database
            this._Connect();
        }

        public AsentiaDatabase(int commandTimeout)
        {
            // override the command timeout
            this._CommandTimeoutOverridden = true;
            this._CommandTimeout = commandTimeout;

            // build the connection string
            this._BuildConnectionString();

            // open the connection to the database
            this._Connect();
        }

        public AsentiaDatabase(DatabaseType databaseType)
        {
            // this constructor does not override command timeout, so set that
            this._CommandTimeoutOverridden = false;

            // set the database type
            this._DatabaseType = databaseType;

            // build the connection string
            this._BuildConnectionString();

            // open the connection to the database
            this._Connect();
        }

        public AsentiaDatabase(DatabaseType databaseType, int commandTimeout)
        {
            // set the database type
            this._DatabaseType = databaseType;

            // override the command timeout
            this._CommandTimeoutOverridden = true;
            this._CommandTimeout = commandTimeout;

            // build the connection string
            this._BuildConnectionString();

            // open the connection to the database
            this._Connect();
        }

        public AsentiaDatabase(string webConfigPath, DatabaseType databaseType)
        {
            // this constructor does not override command timeout, so set that
            this._CommandTimeoutOverridden = false;

            // ensure that the database type is one of these two types
            if (databaseType != DatabaseType.AccountDatabaseUsingWebConfigPath
                && databaseType != DatabaseType.CustomerManagerDatabaseUsingWebConfigPath)
            { throw new AsentiaException(_GlobalResources.ThisDatabaseConstructorCanOnlyBeUsed); }

            // set the database type
            this._DatabaseType = databaseType;

            // set the web config file path
            this._WebConfigPath = webConfigPath;

            // build the connection string
            this._BuildConnectionString();

            // open the connection to the database
            this._Connect();
        }

        public AsentiaDatabase(string webConfigPath, DatabaseType databaseType, int commandTimeout)
        {
            // ensure that the database type is one of these two types
            if (databaseType != DatabaseType.AccountDatabaseUsingWebConfigPath
                && databaseType != DatabaseType.CustomerManagerDatabaseUsingWebConfigPath)
            { throw new AsentiaException(_GlobalResources.ThisDatabaseConstructorCanOnlyBeUsed); }

            // set the database type
            this._DatabaseType = databaseType;

            // set the account web config file path
            this._WebConfigPath = webConfigPath;

            // override the command timeout
            this._CommandTimeoutOverridden = true;
            this._CommandTimeout = commandTimeout;

            // build the connection string
            this._BuildConnectionString();

            // open the connection to the database
            this._Connect();
        }

        /// <summary>
        /// This is a constructor where we explicitly define connection string parameters.
        /// This is used if we need to connect to a database other than one of the Asentia
        /// databases. 
        /// 
        /// For example, the Asentia PowerPoint Processor is built to also do processing
        /// jobs for Inquisiq, and rather than use a separate database handler for Inquisiq, we
        /// use the Asentia database handler with this constructor to connect to Inquisiq. 
        /// </summary>
        /// <param name="databaseServer">the name of the server to connect to</param>
        /// <param name="databaseName">the name of the database we are querying on</param>
        /// <param name="login">username for the database user</param>
        /// <param name="password">password for the database user</param>
        /// <param name="applicationName">the name of the application, for pooling</param>
        /// <param name="commandTimeout">command timeout in seconds, set to at least 30</param>
        /// <param name="useTrustedConnection">use trusted connection? (no need for credentials)</param>
        public AsentiaDatabase(string databaseServer, string databaseName, string login, string password, string applicationName, int commandTimeout, bool useTrustedConnection = false)
        {
            // override the command timeout
            this._CommandTimeoutOverridden = true;
            this._CommandTimeout = commandTimeout;

            // set the database name
            this._DatabaseName = databaseName;

            if (useTrustedConnection)
            {
                // build the connection string
                _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout=30; Trusted_Connection=true; Pooling=true; Min Pool Size=10; Max Pool Size=200; application name={2};",
                                databaseServer,
                                databaseName,
                                applicationName);
            }
            else
            {
                // build the connection string
                _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout=30; User Id={2}; Password={3}; Pooling=true; Min Pool Size=10; Max Pool Size=200; application name={4};",
                                databaseServer,
                                databaseName,
                                login,
                                password,
                                applicationName);
            }

            // open the connection to the database
            this._Connect();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Public property to access the private property Command 
        /// </summary>
        public SqlCommand Command { get { return _Command; } }

        /// <summary>
        /// Public property to access the private property _Transaction
        /// </summary>
        public SqlTransaction Transaction { get { return _Transaction; } }
        #endregion

        #region Private Properties
        /// <summary>
        /// The SQL Command property.
        /// </summary>
        private SqlCommand _Command = null;

        /// <summary>
        /// The SQL Connection property.
        /// </summary>
        private SqlConnection _Connection = null;

        /// <summary>
        /// The SQL Transaction property.
        /// </summary>
        private SqlTransaction _Transaction = null;

        /// <summary>
        /// The connection string to the database.
        /// </summary>
        private static string _ConnectionString = null;

        /// <summary>
        /// The name of the database.
        /// </summary>
        private string _DatabaseName = String.Empty;

        /// <summary>
        /// Is the default (from config file) timeout being overridden?
        /// </summary>
        private bool _CommandTimeoutOverridden;

        /// <summary>
        /// The database command timeout.
        /// </summary>
        private int _CommandTimeout;

        /// <summary>
        /// The type of database to connect to.
        /// </summary>
        private DatabaseType _DatabaseType = DatabaseType.Account;

        /// <summary>
        /// Property to store path to Web.Config file that contains the 
        /// connection string parameters for this connection.
        /// Note that this is only used when connecting to a database
        /// from outside the web application, i.e. the Job Processor Service.
        /// </summary>
        private string _WebConfigPath = null;

        /// <summary>
        /// Determines whether or not this object has been disposed.
        /// </summary>
        private bool _Disposed = false;

        /// <summary>
        /// ArrayList of parameters to add to the SQL Command.
        /// </summary>
        private ArrayList _SqlParameters = null;
        #endregion

        #region Methods
        #region AddParameter()
        /// <summary>
        /// Adds a SqlParameter to the _sqlParameters array list that is to be added to the command.
        /// </summary>
        /// <param name="paramName">the parameter name</param>
        /// <param name="paramValue">the parameter value</param>
        /// <param name="paramType">the parameter type <see cref="SqlDbType"/></param>
        /// <param name="paramSize">the parameter size</param>
        /// <param name="paramDirection">the parameter direction <see cref="ParameterDirection"/></param>
        public void AddParameter(string paramName, object paramValue, SqlDbType paramType, int? paramSize, ParameterDirection paramDirection)
        {
            if (this._SqlParameters == null)
            {
                this._SqlParameters = new ArrayList();
            }

            SqlParameter param;

            param = new SqlParameter();

            param.ParameterName = paramName;
            param.SqlDbType = paramType;

            if (paramSize != null)
            { param.Size = (int)paramSize; }

            param.Direction = paramDirection;

            if (paramDirection == ParameterDirection.Input || paramDirection == ParameterDirection.InputOutput)
            {
                if (paramValue == null)
                { param.Value = DBNull.Value; }
                else
                { param.Value = paramValue; }
            }

            this._SqlParameters.Add(param);
        }
        #endregion

        #region ClearParameters()
        /// <summary>
        /// Clears the _sqlParameters array list.
        /// </summary>
        public void ClearParameters()
        {
            if (this._SqlParameters == null)
            { return; }

            this._SqlParameters = null;
        }
        #endregion

        #region BuildCommand
        /// <summary>
        /// Constructs a SqlCommand with the given parameters. This method is normally called
        /// from the other methods and not called directly. But, it can be called directly if
        /// needed. But, if you call this method directly be sure to properly close the connection
        /// to the database.
        /// </summary>
        /// <param name="query">sql query or stored procedure name</param>
        /// <param name="type">type of sql command</param>
        /// <param name="sqlParameters">an ArrayList of SqlParameter objects to be added to the command</param>
        /// <returns>A SqlCommand object.</returns>
        public SqlCommand BuildCommand(string query, CommandType type, bool includeParameters = true)
        {
            SqlCommand command = new SqlCommand(query, _Connection);

            // associate with current transaction, if any
            if (this._Transaction != null)
            { command.Transaction = this._Transaction; }

            // set command timeout
            command.CommandTimeout = this._CommandTimeout;

            // set command type
            command.CommandType = type;

            // add sql parameters to the command, if any
            if (this._SqlParameters != null && includeParameters)
            {
                foreach (object sqlParameter in this._SqlParameters)
                {
                    if (sqlParameter.GetType() == typeof(SqlParameter))
                    { command.Parameters.Add(sqlParameter); }
                    else
                    { throw new AsentiaException(_GlobalResources.ParameterTypeNotValidSqlParameter); }
                }
            }

            // return the command
            return command;
        }
        #endregion

        #region ExecuteNonQuery
        /// <summary>
        /// Executes a query or stored procedure that returns no results. Used for writing records
        /// to, or deleteing records from the database.
        /// </summary>
        /// <param name="query">sql query or stored procedure name</param>
        /// <param name="isProcedure">determines if the query is a stored procedure name</param>
        /// <param name="sqlParameters">an ArrayList of SqlParameter objects to be added to the command</param>
        /// <returns>The number of rows affected.</returns>
        public int ExecuteNonQuery(string query, bool isProcedure)
        {
            CommandType commandType = CommandType.Text;

            if (isProcedure)
            { commandType = CommandType.StoredProcedure; }

            try
            {
                // USE the selected database, this is done to try to prevent against fragmentation
                using (SqlCommand useCommand = this.BuildCommand("USE [" + this._DatabaseName + "]", CommandType.Text, false))
                { useCommand.ExecuteNonQuery(); }

                using (SqlCommand command = this.BuildCommand(query, commandType))
                {
                    // set this command to be the "global" command so it can be accessed
                    this._Command = command;

                    return command.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlEx)
            {
                throw new DatabaseException(sqlEx.Message, sqlEx);
            }
        }
        #endregion

        #region ExecuteScalar
        /// <summary>
        /// Executes a query or stored procedure that returns a single value.
        /// </summary>
        /// <param name="query">sql query or stored procedure name</param>
        /// <param name="isProcedure">determines if the query is a stored procedure name</param>
        /// <param name="sqlParameters">an ArrayList of SqlParameter objects to be added to the command</param>
        /// <returns>Value of first column and first row of the results.</returns>
        public object ExecuteScalar(string query, bool isProcedure)
        {
            CommandType commandType = CommandType.Text;

            if (isProcedure)
            { commandType = CommandType.StoredProcedure; }

            // USE the selected database, this is done to try to prevent against fragmentation
            using (SqlCommand useCommand = this.BuildCommand("USE [" + this._DatabaseName + "]", CommandType.Text, false))
            { useCommand.ExecuteNonQuery(); }

            using (SqlCommand command = this.BuildCommand(query, commandType))
            {
                return command.ExecuteScalar();
            }
        }
        #endregion

        #region ExecuteDataReader
        /// <summary>
        /// Executes a query or stored procedure and returns the results as a SqlDataReader. Used for
        /// reading query results from the database.
        /// </summary>
        /// <param name="query">sql query or stored procedure name</param>
        /// <param name="isProcedure">determines if the query is a stored procedure name</param>
        /// <param name="sqlParameters">an ArrayList of SqlParameter objects to be added to the command</param>
        /// <returns>Results as a SqlDataReader.</returns>
        public SqlDataReader ExecuteDataReader(string query, bool isProcedure)
        {
            CommandType commandType = CommandType.Text;

            if (isProcedure)
            { commandType = CommandType.StoredProcedure; }

            // USE the selected database, this is done to try to prevent against fragmentation
            using (SqlCommand useCommand = this.BuildCommand("USE [" + this._DatabaseName + "]", CommandType.Text, false))
            { useCommand.ExecuteNonQuery(); }

            using (SqlCommand command = this.BuildCommand(query, commandType))
            {
                // set this command to be the "global" command so it can be accessed
                this._Command = command;

                return command.ExecuteReader(CommandBehavior.CloseConnection);
            }
        }
        #endregion

        #region ExecuteDataSet
        /// <summary>
        /// Executes a query or stored procedure and returns the results as a DataSet. Used for
        /// reading query results from the database.
        /// </summary>
        /// <param name="query">sql query or stored procedure name</param>
        /// <param name="isProcedure">determines if the query is a stored procedure name</param>
        /// <param name="sqlParameters">an ArrayList of SqlParameter objects to be added to the command</param>
        /// <returns>Results as a DataSet</returns>
        public DataSet ExecuteDataSet(string query, bool isProcedure)
        {
            CommandType commandType = CommandType.Text;

            if (isProcedure)
            { commandType = CommandType.StoredProcedure; }

            // USE the selected database, this is done to try to prevent against fragmentation
            using (SqlCommand useCommand = this.BuildCommand("USE [" + this._DatabaseName + "]", CommandType.Text, false))
            { useCommand.ExecuteNonQuery(); }

            using (SqlCommand command = this.BuildCommand(query, commandType))
            {
                this._Command = command;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);

                return dataSet;
            }
        }
        #endregion

        #region BeginTransaction
        /// <summary>
        /// Begins a SQL transaction.
        /// </summary>
        /// <returns>The new SqlTransaction object.</returns>
        public SqlTransaction BeginTransaction()
        {
            this.RollbackTransaction();
            this._Transaction = this._Connection.BeginTransaction();
            return Transaction;
        }
        #endregion

        #region CommitTransaction
        /// <summary>
        /// Commits any transaction in effect.
        /// </summary>
        public void CommitTransaction()
        {
            if (this._Transaction != null)
            {
                this._Transaction.Commit();
                this._Transaction = null;
            }
        }
        #endregion

        #region RollbackTransaction
        /// <summary>
        /// Rolls back any transaction in effect.
        /// </summary>
        public void RollbackTransaction()
        {
            if (this._Transaction != null)
            {
                this._Transaction.Rollback();
                this._Transaction = null;
            }
        }
        #endregion

        #region ParseDbParamInt
        public static int ParseDbParamInt(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return 0; }
            else
            { return Convert.ToInt32(parsedParam); }
        }
        #endregion

        #region ParseDbParamNullableInt
        public static int? ParseDbParamNullableInt(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return null; }
            else
            { return Convert.ToInt32(parsedParam); }
        }
        #endregion

        #region ParseDbParamInt64
        public static Int64 ParseDbParamInt64(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return 0; }
            else
            { return Convert.ToInt64(parsedParam); }
        }
        #endregion

        #region ParseDbParamNullableInt64
        public static Int64? ParseDbParamNullableInt64(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return null; }
            else
            { return Convert.ToInt64(parsedParam); }
        }
        #endregion

        #region ParseDbParamDouble
        public static double ParseDbParamDouble(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return 0; }
            else
            { return Convert.ToDouble(parsedParam); }
        }
        #endregion

        #region ParseDbParamNullableDouble
        public static double? ParseDbParamNullableDouble(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return null; }
            else
            { return Convert.ToDouble(parsedParam); }
        }
        #endregion

        #region ParseDbParamFloat
        public static float ParseDbParamFloat(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return 0; }
            else
            { return (float)Convert.ToDouble(parsedParam); }
        }
        #endregion

        #region ParseDbParamNullableFloat
        public static float? ParseDbParamNullableFloat(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return null; }
            else
            { return float.Parse((string)parsedParam); }
        }
        #endregion

        #region ParseDbParamBool
        public static bool ParseDbParamBool(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return false; }
            else
            { return Convert.ToBoolean(parsedParam); }
        }
        #endregion

        #region ParseDbParamNullableBool
        public static bool? ParseDbParamNullableBool(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return null; }
            else
            { return Convert.ToBoolean(parsedParam); }
        }
        #endregion

        #region ParseDbParamDateTime
        public static DateTime ParseDbParamDateTime(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return DateTime.MinValue; }
            else
            { return Convert.ToDateTime(parsedParam); }
        }
        #endregion

        #region ParseDbParamNullableDateTime
        public static DateTime? ParseDbParamNullableDateTime(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return null; }
            else
            { return Convert.ToDateTime(parsedParam); }
        }
        #endregion

        #region ParseDbParamNullableGuid
        public static Guid? ParseDbParamNullableGuid(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return null; }
            else
            { return Guid.Parse((string)parsedParam); }
        }
        #endregion

        #region ParseDbParamString
        public static string ParseDbParamString(object dbParam)
        {
            object parsedParam = ParseDbParam(dbParam);

            if (parsedParam == null)
            { return null; }
            else
            { return parsedParam.ToString(); }
        }
        #endregion

        #region ParseDbParam
        public static object ParseDbParam(object dbParam)
        {
            if (dbParam == DBNull.Value)
            { return null; }
            else
            { return dbParam; }
        }
        #endregion

        #region ThrowDBExceptionIfReturnCodeValueIsError
        /// <summary>
        /// Examines return code value, and if its an error, throws the appropriate database exception
        /// with the appropriate error message string.
        /// </summary>
        /// <param name="returnValue"></param>
        /// <param name="errorDescriptionCode"></param>
        public static void ThrowDBExceptionIfReturnCodeValueIsError(DBReturnValue returnValue, string errorDescriptionCode)
        {
            switch (returnValue)
            {
                case DBReturnValue.OK:
                    return;
                case DBReturnValue.DetailsNotFound:
                    throw new DatabaseDetailsNotFoundException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode));
                case DBReturnValue.FieldNotUnique:
                    throw new DatabaseFieldNotUniqueException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode));
                case DBReturnValue.CallerPermissionError:
                    throw new DatabaseCallerPermissionException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode));
                case DBReturnValue.FieldConstraintError:
                    throw new DatabaseFieldConstraintException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode));
                case DBReturnValue.SpecifiedLanguageNotDefault:
                    throw new DatabaseSpecifiedLanguageNotDefaultException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode));
                case DBReturnValue.SpecifiedLanguageNotFound:
                    throw new DatabaseSpecifiedLanguageNotFoundException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode));
                case DBReturnValue.ObjectNotUnique:
                    throw new DatabaseSpecifiedLanguageNotFoundException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode));
                default:
                    return;
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _BuildConnectionString()
        /// <summary>
        /// Builds a connection string using parameters set in the web.config.
        /// </summary>
        protected void _BuildConnectionString()
        {
            switch (this._DatabaseType)
            {
                case DatabaseType.Account:
                    this._DatabaseName = Config.AccountDatabaseSettings.Name;

                    // if we did not override the command timeout in a constructor, use the default value from config
                    if (!this._CommandTimeoutOverridden)
                    { this._CommandTimeout = Config.AccountDatabaseSettings.CommandTimeout; }

                    // build the connection string based on data from web.config
                    if (Config.AccountDatabaseSettings.UseTrustedConnection)
                    {
                        _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout={2}; Trusted_Connection=true; Pooling={3}; Min Pool Size={4}; Max Pool Size={5}; application name=Asentia LMS;",
                            Config.AccountDatabaseSettings.Server,
                            Config.AccountDatabaseSettings.Name,
                            Config.AccountDatabaseSettings.ConnectionTimeout,
                            Config.AccountDatabaseSettings.UseConnectionPooling,
                            Config.AccountDatabaseSettings.MinPoolSize,
                            Config.AccountDatabaseSettings.MaxPoolSize);
                    }
                    else
                    {
                        _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout={2}; User Id={3}; Password={4}; Pooling={5}; Min Pool Size={6}; Max Pool Size={7}; application name=Asentia LMS;",
                            Config.AccountDatabaseSettings.Server,
                            Config.AccountDatabaseSettings.Name,
                            Config.AccountDatabaseSettings.ConnectionTimeout,
                            Config.AccountDatabaseSettings.Login,
                            Config.AccountDatabaseSettings.Password,
                            Config.AccountDatabaseSettings.UseConnectionPooling,
                            Config.AccountDatabaseSettings.MinPoolSize,
                            Config.AccountDatabaseSettings.MaxPoolSize);
                    }

                    break;
                case DatabaseType.CustomerManager:
                    this._DatabaseName = Config.CustomerManagerDatabaseSettings.Name;

                    // if we did not override the command timeout in a constructor, use the default value from config
                    if (!this._CommandTimeoutOverridden)
                    { this._CommandTimeout = Config.CustomerManagerDatabaseSettings.CommandTimeout; }

                    // build the connection string based on data from web.config
                    if (Config.CustomerManagerDatabaseSettings.UseTrustedConnection)
                    {
                        _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout={2}; Trusted_Connection=true; Pooling={3}; Min Pool Size={4}; Max Pool Size={5}; application name=Asentia LMS;",
                            Config.CustomerManagerDatabaseSettings.Server,
                            Config.CustomerManagerDatabaseSettings.Name,
                            Config.CustomerManagerDatabaseSettings.ConnectionTimeout,
                            Config.CustomerManagerDatabaseSettings.UseConnectionPooling,
                            Config.CustomerManagerDatabaseSettings.MinPoolSize,
                            Config.CustomerManagerDatabaseSettings.MaxPoolSize);
                    }
                    else
                    {
                        _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout={2}; User Id={3}; Password={4}; Pooling={5}; Min Pool Size={6}; Max Pool Size={7}; application name=Asentia LMS;",
                            Config.CustomerManagerDatabaseSettings.Server,
                            Config.CustomerManagerDatabaseSettings.Name,
                            Config.CustomerManagerDatabaseSettings.ConnectionTimeout,
                            Config.CustomerManagerDatabaseSettings.Login,
                            Config.CustomerManagerDatabaseSettings.Password,
                            Config.CustomerManagerDatabaseSettings.UseConnectionPooling,
                            Config.CustomerManagerDatabaseSettings.MinPoolSize,
                            Config.CustomerManagerDatabaseSettings.MaxPoolSize);
                    }

                    break;
                case DatabaseType.SessionState:
                    this._DatabaseName = Config.SessionStateDatabaseSettings.Name;

                    // if we did not override the command timeout in a constructor, use the default value from config
                    if (!this._CommandTimeoutOverridden)
                    { this._CommandTimeout = Config.SessionStateDatabaseSettings.CommandTimeout; }

                    // build the connection string based on data from web.config
                    if (Config.SessionStateDatabaseSettings.UseTrustedConnection)
                    {
                        _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout={2}; Trusted_Connection=true; Pooling={3}; Min Pool Size={4}; Max Pool Size={5}; application name=Asentia LMS;",
                            Config.SessionStateDatabaseSettings.Server,
                            Config.SessionStateDatabaseSettings.Name,
                            Config.SessionStateDatabaseSettings.ConnectionTimeout,
                            Config.SessionStateDatabaseSettings.UseConnectionPooling,
                            Config.SessionStateDatabaseSettings.MinPoolSize,
                            Config.SessionStateDatabaseSettings.MaxPoolSize);
                    }
                    else
                    {
                        _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout={2}; User Id={3}; Password={4}; Pooling={5}; Min Pool Size={6}; Max Pool Size={7}; application name=Asentia LMS;",
                            Config.SessionStateDatabaseSettings.Server,
                            Config.SessionStateDatabaseSettings.Name,
                            Config.SessionStateDatabaseSettings.ConnectionTimeout,
                            Config.SessionStateDatabaseSettings.Login,
                            Config.SessionStateDatabaseSettings.Password,
                            Config.SessionStateDatabaseSettings.UseConnectionPooling,
                            Config.SessionStateDatabaseSettings.MinPoolSize,
                            Config.SessionStateDatabaseSettings.MaxPoolSize);
                    }

                    break;
                case DatabaseType.AccountDatabaseUsingWebConfigPath:
                    // make sure a web.config path is specified
                    if (this._WebConfigPath == null)
                    { throw new DatabaseException(String.Format(_GlobalResources.WebConfigPathCannotBeNull, "AccountDatabaseUsingWebConfigPath")); }

                    // set the path to the web.config file we're using for database settings
                    Config.WebConfigFilePath = this._WebConfigPath;

                    this._DatabaseName = Config.AccountDatabaseSettings.Name;

                    // if we did not override the command timeout in a constructor, use the default value from config
                    if (!this._CommandTimeoutOverridden)
                    { this._CommandTimeout = Config.AccountDatabaseSettings.CommandTimeout; }

                    // build the connection string based on data from web.config
                    if (Config.AccountDatabaseSettings.UseTrustedConnection)
                    {
                        _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout={2}; Trusted_Connection=true; Pooling={3}; Min Pool Size={4}; Max Pool Size={5}; application name=Asentia LMS;",
                            Config.AccountDatabaseSettings.Server,
                            Config.AccountDatabaseSettings.Name,
                            Config.AccountDatabaseSettings.ConnectionTimeout,
                            Config.AccountDatabaseSettings.UseConnectionPooling,
                            Config.AccountDatabaseSettings.MinPoolSize,
                            Config.AccountDatabaseSettings.MaxPoolSize);
                    }
                    else
                    {
                        _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout={2}; User Id={3}; Password={4}; Pooling={5}; Min Pool Size={6}; Max Pool Size={7}; application name=Asentia LMS;",
                            Config.AccountDatabaseSettings.Server,
                            Config.AccountDatabaseSettings.Name,
                            Config.AccountDatabaseSettings.ConnectionTimeout,
                            Config.AccountDatabaseSettings.Login,
                            Config.AccountDatabaseSettings.Password,
                            Config.AccountDatabaseSettings.UseConnectionPooling,
                            Config.AccountDatabaseSettings.MinPoolSize,
                            Config.AccountDatabaseSettings.MaxPoolSize);
                    }

                    break;
                case DatabaseType.CustomerManagerDatabaseUsingWebConfigPath:
                    // make sure a web.config path is specified
                    if (this._WebConfigPath == null)
                    { throw new DatabaseException(String.Format(_GlobalResources.WebConfigPathCannotBeNull, "CustomerManagerDatabaseUsingWebConfigPath")); }

                    // set the path to the web.config file we're using for database settings
                    Config.WebConfigFilePath = this._WebConfigPath;

                    this._DatabaseName = Config.CustomerManagerDatabaseSettings.Name;

                    // if we did not override the command timeout in a constructor, use the default value from config
                    if (!this._CommandTimeoutOverridden)
                    { this._CommandTimeout = Config.CustomerManagerDatabaseSettings.CommandTimeout; }

                    // build the connection string based on data from web.config
                    if (Config.CustomerManagerDatabaseSettings.UseTrustedConnection)
                    {
                        _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout={2}; Trusted_Connection=true; Pooling={3}; Min Pool Size={4}; Max Pool Size={5}; application name=Asentia LMS;",
                            Config.CustomerManagerDatabaseSettings.Server,
                            Config.CustomerManagerDatabaseSettings.Name,
                            Config.CustomerManagerDatabaseSettings.ConnectionTimeout,
                            Config.CustomerManagerDatabaseSettings.UseConnectionPooling,
                            Config.CustomerManagerDatabaseSettings.MinPoolSize,
                            Config.CustomerManagerDatabaseSettings.MaxPoolSize);
                    }
                    else
                    {
                        _ConnectionString = String.Format("Data Source={0}; Initial Catalog=master; Connection Timeout={2}; User Id={3}; Password={4}; Pooling={5}; Min Pool Size={6}; Max Pool Size={7}; application name=Asentia LMS;",
                            Config.CustomerManagerDatabaseSettings.Server,
                            Config.CustomerManagerDatabaseSettings.Name,
                            Config.CustomerManagerDatabaseSettings.ConnectionTimeout,
                            Config.CustomerManagerDatabaseSettings.Login,
                            Config.CustomerManagerDatabaseSettings.Password,
                            Config.CustomerManagerDatabaseSettings.UseConnectionPooling,
                            Config.CustomerManagerDatabaseSettings.MinPoolSize,
                            Config.CustomerManagerDatabaseSettings.MaxPoolSize);
                    }

                    break;
            }
        }
        #endregion

        #region _Connect()
        /// <summary>
        /// Opens a SqlConnection to the database. The connection string is static
        /// and is built using BuildConnectionString().
        /// </summary>
        protected void _Connect()
        {
            try
            {
                this._Connection = new SqlConnection(_ConnectionString);
                this._Connection.Open();
            }
            catch
            {
                throw new DatabaseException(String.Format(_GlobalResources.UnableToConnectToDatabase, this._DatabaseName));
            }
        }
        #endregion
        #endregion

        #region IDisposable Members
        /// <summary>
        /// Disposes of the objects in this class.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);

            // clear the base config path if it was set anywhere
            // we need to reset webconfig path on dispose, because if it is set in this object, it will not be rest by base config
            BaseConfig.WebConfigFilePath = null;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._Disposed)
            {
                // need to dispose managed resources if being called manually
                if (disposing)
                {
                    // set sql parameters to null, if it isn't already
                    if (this._SqlParameters != null)
                    {
                        this._SqlParameters = null;
                    }

                    // dispose of the "global" command, if any
                    if (this._Command != null)
                    {
                        this._Command.Dispose();
                        this._Command = null;
                    }

                    // dispose of the connection, if any
                    if (this._Connection != null)
                    {
                        this.RollbackTransaction();
                        this._Connection.Dispose();
                        this._Connection = null;
                    }
                }

                this._Disposed = true;
            }
        }
        #endregion
    }
}