﻿using System;
using System.IO;
using System.Web;

namespace Asentia.Common
{
    /// <summary>
    /// Static class to catalog all images used throughout the system.
    /// </summary>
    /// <example>
    /// Example of how to implement images from here as the SRC for an Image on a page:
    /// 
    /// // USING DIRECT LINK TO FOLDER
    /// image.ImageUrl = SitePathConstants.[PATH_TO_FOLDER] <see cref="SitePathConstants"/>
    ///             + ImageFiles.[ICON_NAME]
    ///             + ImageFiles.EXT_PNG;
    ///   
    /// 
    /// // USING 'GetIconPath' METHOD - RECOMMENDED
    /// image.ImageUrl = ImageFiles.GetIconPath(ImageFiles.[ICON_NAME],
    ///                                    ImageFiles.EXT_PNG,
    ///                                    ImageFiles.IconSize.ICON64,
    ///                                    OPTIONAL [forCustomerManager] true or false,
    ///                                    OPTIONAL [useDefaultOnly] true or false);
    ///                                    
    /// </example>
    public static class ImageFiles
    {
        #region Properties
        #region Image Extension Properties
        public static string EXT_GIF { get { return ".gif"; } }
        public static string EXT_JPG { get { return ".jpg"; } }
        public static string EXT_JPEG { get { return ".jpeg"; } }
        public static string EXT_PNG { get { return ".png"; } }
        #endregion

        #region Icon Properties
        /// <summary>
        /// Enum for the size of icon to be used. This is used in "GetPath" methods
        /// of this class to grab the correct folder for the icon size.
        /// </summary>
        public enum IconSize
        {
            ICON16 = 16,
            ICON24 = 24,
            ICON32 = 32,
            ICON48 = 48,
            ICON64 = 64,
        }

        // avatar names
        public static string AVATAR_CATALOG_CATALOG { get { return "catalog.catalog"; } }
        public static string AVATAR_CATALOG_COMMUNITY { get { return "catalog.community"; } }
        public static string AVATAR_CATALOG_COURSE { get { return "catalog.course"; } }
        public static string AVATAR_CATALOG_ILT { get { return "catalog.ilt"; } }
        public static string AVATAR_CATALOG_LEARNINGPATH { get { return "catalog.learningpath"; } }

        // icon names
        public static string ICON_ACTIVITY { get { return "activity"; } }
        public static string ICON_ADD { get { return "add"; } }
        public static string ICON_ADMINISTRATOR { get { return "administrator"; } }
        public static string ICON_ADMINISTRATOR_BUTTON { get { return "administrator.button"; } }
        public static string ICON_ALERT_ERROR_RED { get { return "alert.error.red"; } }
        public static string ICON_ALERT_ERROR_RED_BUTTON { get { return "alert.error.red.button"; } }
        public static string ICON_ALERT_WARNING_YELLOW { get { return "alert.warning.yellow"; } }
        public static string ICON_ANALYTICS { get { return "analytics"; } }
        public static string ICON_ANALYTICS_BUTTON { get { return "analytics.button"; } }
        public static string ICON_ANALYTICS_ALT_BUTTON { get { return "analytics.alt.button"; } }
        public static string ICON_ANALYTICS_ALT { get { return "analytics.alt"; } }
        public static string ICON_ANNOUNCEMENT { get { return "announcement"; } }
        public static string ICON_ANNOUNCEMENT_BUTTON { get { return "announcement.button"; } }
        public static string ICON_API { get { return "api"; } }
        public static string ICON_API_BUTTON { get { return "api.button"; } }
        public static string ICON_APPLICATION { get { return "application"; } }
        public static string ICON_ASC { get { return "asc"; } }
        public static string ICON_ASC_WHITE { get { return "asc.white"; } }
        public static string ICON_AUTHENTICATION { get { return "authentication"; } }
        public static string ICON_AUTHENTICATION_BUTTON { get { return "authentication.button"; } }
        public static string ICON_BACK { get { return "back"; } }
        public static string ICON_BOOK { get { return "book"; } }
        public static string ICON_BOOK_BUTTON { get { return "book.button"; } }
        public static string ICON_CALENDAR { get { return "calendar"; } }
        public static string ICON_CALENDAR_BUTTON { get { return "calendar.button"; } }
        public static string ICON_CART { get { return "cart"; } }
        public static string ICON_CART_BUTTON { get { return "cart.button"; } }
        public static string ICON_CATALOG { get { return "catalog"; } }
        public static string ICON_CATALOG_BUTTON { get { return "catalog.button"; } }
        public static string ICON_CATALOG_WHITE { get { return "catalog.white"; } }
        public static string ICON_CATALOG_ALT { get { return "catalog.alt"; } }
        public static string ICON_CATALOG_ALT_WHITE { get { return "catalog.alt.white"; } }
        public static string ICON_CATALOG_PRIVATE { get { return "catalog.private"; } }
        public static string ICON_CATALOG_PRIVATE_BUTTON { get { return "catalog.private.button"; } }
        public static string ICON_CATALOG_PRIVATE_WHITE { get { return "catalog.private.white"; } }        
        public static string ICON_CERTIFICATE { get { return "certificate"; } }
        public static string ICON_CERTIFICATE_TEMPLATE { get { return "certificate.template"; } }
        public static string ICON_CERTIFICATION { get { return "certification"; } }
        public static string ICON_CHECK_GREEN { get { return "check.green"; } }
        public static string ICON_CHROME { get { return "chrome"; } }
        public static string ICON_CLASSROOM { get { return "classroom"; } }
        public static string ICON_CLASSROOM_BUTTON { get { return "classroom.button"; } }
        public static string ICON_CLOCK { get { return "clock"; } }
        public static string ICON_CLOCK_BUTTON { get { return "clock.button"; } }
        public static string ICON_CLONEPORTAL { get { return "cloneportal"; } }
        public static string ICON_CLONEPORTAL_BUTTON { get { return "cloneportal.button"; } }
        public static string ICON_CLOSE { get { return "close"; } }
        public static string ICON_CLOSE_MODAL { get { return "close.modal"; } }
        public static string ICON_CLOSE_MODAL_ALT { get { return "close.modal.alt"; } }
        public static string ICON_CLOSED { get { return "closed"; } }
        public static string ICON_COLLAPSE { get { return "collapse"; } }
        public static string ICON_COLOR { get { return "color"; } }
        public static string ICON_COLOR_BUTTON { get { return "color.button"; } }
        public static string ICON_COMPANY { get { return "company"; } }
        public static string ICON_COMPANY_BUTTON { get { return "company.button"; } }
        public static string ICON_CONFIGURATION { get { return "configuration"; } }
        public static string ICON_CONFIGURATION_BUTTON { get { return "configuration.button"; } }
        public static string ICON_COPY { get { return "copy"; } }
        public static string ICON_COPY_BUTTON { get { return "copy.button"; } }
        public static string ICON_COUPONCODE { get { return "couponcode"; } }
        public static string ICON_COUPONCODE_BUTTON { get { return "couponcode.button"; } }
        public static string ICON_COURSE { get { return "course"; } }
        public static string ICON_COURSE_BUTTON { get { return "course.button"; } }
        public static string ICON_COURSE_WHITE { get { return "course.white"; } }
        public static string ICON_COURSE_MATERIALS { get { return "course.materials"; } }
        public static string ICON_COURSE_MATERIALS_BUTTON { get { return "course.materials.button"; } }
        public static string ICON_COURSECATALOG { get { return "coursecatalog"; } }
        public static string ICON_CSS { get { return "css"; } }
        public static string ICON_CSS_BUTTON { get { return "css.button"; } }
        public static string ICON_CUSTOMFORUM { get { return "customforum"; } }
        public static string ICON_CUSTOMFORUM_BUTTON { get { return "customforum.button"; } }
        public static string ICON_DASHBOARD { get { return "dashboard"; } }
        public static string ICON_DASHBOARD_BUTTON { get { return "dashboard.button"; } }
        public static string ICON_DASHBOARD_WHITE { get { return "dashboard.white"; } }
        public static string ICON_DELETE { get { return "delete"; } }
        public static string ICON_DELETE_ALT { get { return "delete.alt"; } }
        public static string ICON_DEMOTE { get { return "demote"; } }
        public static string ICON_DESC { get { return "desc"; } }
        public static string ICON_DESC_WHITE { get { return "desc.white"; } }
        public static string ICON_DISCONNECT { get { return "disconnect"; } }
        public static string ICON_DISCUSSION { get { return "discussion"; } }
        public static string ICON_DISCUSSION_BUTTON { get { return "discussion.button"; } }
        public static string ICON_DOCUMENT { get { return "document"; } }
        public static string ICON_DOCUMENT_BUTTON { get { return "document.button"; } }
        public static string ICON_DOWNLOAD { get { return "download"; } }
        public static string ICON_DROPDOWN { get { return "dropdown"; } }
        public static string ICON_ECOMMERCE { get { return "ecommerce"; } }
        public static string ICON_ECOMMERCE_BUTTON { get { return "ecommerce.button"; } }
        public static string ICON_EMAIL { get { return "email"; } }
        public static string ICON_EMAIL_BUTTON { get { return "email.button"; } }
        public static string ICON_EMAIL_WHITE { get { return "email.white"; } }
        public static string ICON_EMAIL_CHECK { get { return "email.check"; } }
        public static string ICON_ENROLLMENT { get { return "enrollment"; } }
        public static string ICON_ENROLLMENT_BUTTON { get { return "enrollment.button"; } }
        public static string ICON_ENROLLMENT_SERIES { get { return "enrollment.series"; } }
        public static string ICON_ERROR { get { return "error"; } }
        public static string ICON_EXPAND { get { return "expand"; } }
        public static string ICON_EXPAND_ARROW { get { return "expand_arrow"; } }
        public static string ICON_FACEBOOK { get { return "facebook"; } }
        public static string ICON_FEED { get { return "feed"; } }
        public static string ICON_FETCH { get { return "fetch"; } }
        public static string ICON_FETCH_BUTTON { get { return "fetch.button"; } }
        public static string ICON_FIREFOX { get { return "firefox"; } }
        public static string ICON_FIRST { get { return "first"; } }
        public static string ICON_FIRST_ALT { get { return "first.alt"; } }
        public static string ICON_FOLDER { get { return "folder"; } }
        public static string ICON_FOLDER_BUTTON { get { return "folder.button"; } }
        public static string ICON_FOOTER { get { return "footer"; } }
        public static string ICON_FOOTER_BUTTON { get { return "footer.button"; } }
        public static string ICON_FORWARD { get { return "forward"; } }
        public static string ICON_GO { get { return "go"; } }
        public static string ICON_GO_REVIEW { get { return "go.review"; } }
        public static string ICON_GOOGLE { get { return "google"; } }
        public static string ICON_GOTOMEETING { get { return "gotomeeting"; } }
        public static string ICON_GOTOWEBINAR { get { return "gotowebinar"; } }
        public static string ICON_GOTOTRAINING { get { return "gototraining"; } }
        public static string ICON_GRAPH { get { return "graph"; } }
        public static string ICON_GROUP { get { return "group"; } }
        public static string ICON_GROUP_BUTTON { get { return "group.button"; } }
        public static string ICON_GROUP_WHITE { get { return "group.white"; } }
        public static string ICON_HOME { get { return "home"; } }
        public static string ICON_HOME_BUTTON { get { return "home.button"; } }
        public static string ICON_IE { get { return "ie"; } }
        public static string ICON_IMAGE { get { return "image"; } }
        public static string ICON_IMAGE_BUTTON { get { return "image.button"; } }
        public static string ICON_IMPORTACTIVITYDATA { get { return "importactivitydata"; } }
        public static string ICON_IMPORTACTIVITYDATA_BUTTON { get { return "importactivitydata.button"; } }
        public static string ICON_IMPORTCERTIFICATEDATA { get { return "importcertificatedata"; } }
        public static string ICON_INFORMATION { get { return "information"; } }
        public static string ICON_INFORMATION_BLUE { get { return "information.blue"; } }
        public static string ICON_INSTRUCTOR { get { return "instructor"; } }
        public static string ICON_INSTRUCTOR_BUTTON { get { return "instructor.button"; } }
        public static string ICON_LAST { get { return "last"; } }
        public static string ICON_LAST_ALT { get { return "last.alt"; } }
        public static string ICON_LEADERBOARD { get { return "leaderboard"; } }
        public static string ICON_LEADERBOARD_BUTTON { get { return "leaderboard.button"; } }
        public static string ICON_LEADERBOARD_BRONZE { get { return "leaderboard.bronze"; } }
        public static string ICON_LEADERBOARD_GOLD { get { return "leaderboard.gold"; } }
        public static string ICON_LEADERBOARD_SILVER { get { return "leaderboard.silver"; } }
        public static string ICON_LEARNINGASSETS { get { return "learningassets"; } }
        public static string ICON_LEARNINGPATH { get { return "learningpath"; } }
        public static string ICON_LEARNINGPATH_BUTTON { get { return "learningpath.button"; } }
        public static string ICON_LEARNINGPATH_WHITE { get { return "learningpath.white"; } }
        public static string ICON_LEGAL { get { return "legal"; } }
        public static string ICON_LEGAL_BUTTON { get { return "legal.button"; } }
        public static string ICON_LESSON { get { return "lesson"; } }
        public static string ICON_LESSON_BUTTON { get { return "lesson.button"; } }
        public static string ICON_LICENSE { get { return "license"; } }
        public static string ICON_LINKEDIN { get { return "linkedin"; } }
        public static string ICON_LOADING { get { return "loading"; } }
        public static string ICON_LOADING_DARKBG { get { return "loading.darkbg"; } }
        public static string ICON_LOCKED { get { return "locked"; } }
        public static string ICON_LOCKED_WHITE { get { return "locked.white"; } }
        public static string ICON_LOCKED_BUTTON_GOLD { get { return "locked.button.gold"; } }
        public static string ICON_LOCKED_RED { get { return "locked.red"; } }
        public static string ICON_LOG { get { return "log"; } }
        public static string ICON_LOG_EMAIL { get { return "log.email"; } }
        public static string ICON_LOG_EXCEPTION { get { return "log.exception"; } }
        public static string ICON_LOG_REPORT { get { return "log.report"; } }
        public static string ICON_LOG_SYSTEM { get { return "log.system"; } }
        public static string ICON_LOGIN { get { return "login"; } }
        public static string ICON_LOGIN_BUTTON { get { return "login.button"; } }
        public static string ICON_LOGIN_IMPERSONATE { get { return "login.impersonate"; } }
        public static string ICON_LOGIN_WHITE { get { return "login.white"; } }
        public static string ICON_MASTHEAD { get { return "masthead"; } }
        public static string ICON_MASTHEAD_BUTTON { get { return "masthead.button"; } }
        public static string ICON_MERGE { get { return "merge"; } }
        public static string ICON_MODIFY { get { return "modify"; } }
        public static string ICON_MSOFFICE_2013 { get { return "msoffice.2013"; } }
        public static string ICON_MYACCOUNT { get { return "myaccount"; } }
        public static string ICON_MYACCOUNT_BUTTON { get { return "myaccount.button"; } }
        public static string ICON_NAVIGATION { get { return "navigation"; } }
        public static string ICON_NAVIGATION_BUTTON { get { return "navigation.button"; } }
        public static string ICON_NEXT { get { return "next"; } }
        public static string ICON_NEXT_ALT { get { return "next.alt"; } }
        public static string ICON_NOTEPAD { get { return "notepad"; } }
        public static string ICON_NOTEPAD_BUTTON { get { return "notepad.button"; } }
        public static string ICON_OJT { get { return "ojt"; } }
        public static string ICON_OJT_BUTTON { get { return "ojt.button"; } }
        public static string ICON_ONECOLUMN { get { return "onecolumn"; } }
        public static string ICON_ONECOLUMN_BUTTON { get { return "onecolumn.button"; } }
        public static string ICON_OPEN { get { return "open"; } }
        public static string ICON_OPENSESAME { get { return "opensesame"; } }
        public static string ICON_OVERLAY_BLANK { get { return "overlay.blank"; } } // used specifically as a spacing placeholder for image links with no overlay
        public static string ICON_OVERLAY_CHECK { get { return "overlay.check"; } }
        public static string ICON_OVERLAY_PLUS { get { return "overlay.plus"; } }
        public static string ICON_OVERLAY_X { get { return "overlay.x"; } }
        public static string ICON_PACKAGE { get { return "package"; } }
        public static string ICON_PACKAGE_BUTTON { get { return "package.button"; } }
        public static string ICON_PACKAGE_BUTTON_ALT { get { return "package.button.alt"; } }
        public static string ICON_PASSWORD { get { return "password"; } }
        public static string ICON_PASSWORD_BUTTON { get { return "password.button"; } }
        public static string ICON_PAYPAL { get { return "paypal"; } }
        public static string ICON_PDF { get { return "pdf"; } }
        public static string ICON_PERMISSION { get { return "permission"; } }
        public static string ICON_PERMISSION_BUTTON { get { return "permission.button"; } }
        public static string ICON_POWERPOINT { get { return "powerpoint"; } }
        public static string ICON_PREVIOUS { get { return "previous"; } }
        public static string ICON_PREVIOUS_ALT { get { return "previous.alt"; } }
        public static string ICON_PRINT { get { return "print"; } }
        public static string ICON_PROMOTE { get { return "promote"; } }
        public static string ICON_QUESTION { get { return "question"; } }
        public static string ICON_QUIZ { get { return "quiz"; } }
        public static string ICON_QUIZ_BUTTON { get { return "quiz.button"; } }
        public static string ICON_RECEIPT { get { return "receipt"; } }
        public static string ICON_RECEIPT_BUTTON { get { return "receipt.button"; } }
        public static string ICON_RELOAD { get { return "reload"; } }
        public static string ICON_REPORT { get { return "report"; } }
        public static string ICON_REPORT_BUTTON { get { return "report.button"; } }
        public static string ICON_RESET { get { return "reset"; } }
        public static string ICON_RESOURCE { get { return "resource"; } }
        public static string ICON_RESOURCE_BUTTON { get { return "resource.button"; } }
        public static string ICON_RESOURCE_MANAGEMENT { get { return "resource.management"; } }
        public static string ICON_RESOURCE_TYPE { get { return "resource.type"; } }
        public static string ICON_RESOURCE_TYPE_BUTTON { get { return "resource.type.button"; } }
        public static string ICON_RULESET { get { return "ruleset"; } }
        public static string ICON_RULESET_BUTTON { get { return "ruleset.button"; } }
        public static string ICON_RULESET_ALT { get { return "ruleset.alt"; } }
        public static string ICON_RULESET_ALT_BUTTON { get { return "ruleset.alt.button"; } }
        public static string ICON_SAFARI { get { return "safari"; } }
        public static string ICON_SAVE { get { return "save"; } }
        public static string ICON_SAVE_BUTTON { get { return "save.button"; } }
        public static string ICON_SEARCH { get { return "search"; } }
        public static string ICON_SERVER { get { return "server"; } }
        public static string ICON_SERVER_BUTTON { get { return "server.button"; } }
        public static string ICON_SESSION { get { return "session"; } }
        public static string ICON_SESSION_BUTTON { get { return "session.button"; } }
        public static string ICON_SETTINGS { get { return "settings"; } }
        public static string ICON_SETTINGS_BUTTON { get { return "settings.button"; } }
        public static string ICON_SITE { get { return "site"; } }
        public static string ICON_SITE_BUTTON { get { return "site.button"; } }
        public static string ICON_STANDUPTRAINING { get { return "standuptraining"; } }
        public static string ICON_STANDUPTRAINING_WHITE { get { return "standuptraining.white"; } }
        public static string ICON_STANDUPTRAINING_BUTTON { get { return "standuptraining.button"; } }
        public static string ICON_STANDUPTRAINING_SESSION { get { return "standuptraining.session"; } }
        public static string ICON_STANDUPTRAINING_SESSION_BUTTON { get { return "standuptraining.session.button"; } }
        public static string ICON_SYNCHRONIZE { get { return "synchronize"; } }
        public static string ICON_SYNCHRONIZE_BUTTON { get { return "synchronize.button"; } }
        public static string ICON_SYSTEM { get { return "system"; } }
        public static string ICON_SYSTEM_DATASETS { get { return "system.datasets"; } }
        public static string ICON_SYSTEM_DATASETS_BUTTON { get { return "system.datasets.button"; } }
        public static string ICON_SYSTEM_LOCALIZATION { get { return "system.localization"; } }
        public static string ICON_SYSTEM_LOCALIZATION_BUTTON { get { return "system.localization.button"; } }
        public static string ICON_SYSTEM_LOCALIZATION_WHITE { get { return "system.localization.white"; } }
        public static string ICON_TAB_APPLICATION { get { return "tab.application"; } }
        public static string ICON_TAB_CATALOG { get { return "tab.catalog"; } }
        public static string ICON_TAB_CATALOGS { get { return "tab.catalogs"; } }
        public static string ICON_TAB_CERTIFICATE_BUILDER { get { return "tab.certificate.builder"; } }
        public static string ICON_TAB_COLUMNS { get { return "tab.columns"; } }
        public static string ICON_TAB_COURSES { get { return "tab.courses"; } }
        public static string ICON_TAB_DATAFIELDS { get { return "tab.datafields"; } }
        public static string ICON_TAB_DATASETS { get { return "tab.datasets"; } }
        public static string ICON_TAB_DRAFTS { get { return "tab.drafts"; } }
        public static string ICON_TAB_EXPERTS { get { return "tab.experts"; } }
        public static string ICON_TAB_FEED { get { return "tab.feed"; } }
        public static string ICON_TAB_FILTERS { get { return "tab.filters"; } }
        public static string ICON_TAB_FORM_PROPERTIES { get { return "tab.form.properties"; } }
        public static string ICON_TAB_GATEWAYPROCESSOR { get { return "tab.gatewayprocessor"; } }
        public static string ICON_TAB_GROUP_MEMBERS { get { return "tab.group.members"; } }
        public static string ICON_TAB_GROUPS { get { return "tab.groups"; } }
        public static string ICON_TAB_HOMEPAGE { get { return "tab.homepage"; } }
        public static string ICON_TAB_INBOX { get { return "tab.inbox"; } }
        public static string ICON_TAB_LEARNINGPATHS { get { return "tab.learningpaths"; } }
        public static string ICON_TAB_LOCATION { get { return "tab.location"; } }
        public static string ICON_TAB_LOGINSETUP { get { return "tab.loginsetup"; } }
        public static string ICON_TAB_MANAGEACTIVITYDATA { get { return "tab.manageactivitydata"; } }
        public static string ICON_TAB_MANIFEST { get { return "tab.manifest"; } }
        public static string ICON_TAB_MESSAGE { get { return "tab.message"; } }
        public static string ICON_TAB_MODERATORS { get { return "tab.moderators"; } }
        public static string ICON_TAB_OPTIONS { get { return "tab.options"; } }
        public static string ICON_TAB_ORDERING { get { return "tab.ordering"; } }
        public static string ICON_TAB_OWNER { get { return "tab.owner"; } }
        public static string ICON_TAB_PREREQUISITES { get { return "tab.prerequisites"; } }
        public static string ICON_TAB_PROPERTIES { get { return "tab.properties"; } }
        public static string ICON_TAB_PUBLICREPORTS { get { return "tab.publicreports"; } }
        public static string ICON_TAB_REGISTRATIONINSTRUCTIONS { get { return "tab.registrationinstructions"; } }
        public static string ICON_TAB_RESOURCEPARENT { get { return "tab.resourceparent"; } }
        public static string ICON_TAB_RESOURCES { get { return "tab.resources"; } }
        public static string ICON_TAB_RULESETS { get { return "tab.rulesets"; } }
        public static string ICON_TAB_SAVEDREPORTS { get { return "tab.savedreports"; } }
        public static string ICON_TAB_SENTITEMS { get { return "tab.sentitems"; } }
        public static string ICON_TAB_SESSIONS { get { return "tab.sessions"; } }
        public static string ICON_TAB_SETTINGS { get { return "tab.settings"; } }
        public static string ICON_TAB_UPLOADACTIVITYDATA { get { return "tab.uploadactivitydata"; } }
        public static string ICON_TAB_UPLOADCERTIFICATEDATA { get { return "tab.uploadcertificatedata"; } }
        public static string ICON_TAB_USERAGREEMENT { get { return "tab.useragreement"; } }
        public static string ICON_TAB_USERFIELDSTAB_1 { get { return "tab.userfieldstab.1"; } }
        public static string ICON_TAB_USERFIELDSTAB_2 { get { return "tab.userfieldstab.2"; } }
        public static string ICON_TAB_USERFIELDSTAB_3 { get { return "tab.userfieldstab.3"; } }
        public static string ICON_TAB_USERFIELDSTAB_4 { get { return "tab.userfieldstab.4"; } }
        public static string ICON_TASK { get { return "task"; } }
        public static string ICON_TASK_BUTTON { get { return "task.button"; } }
        public static string ICON_THEMES { get { return "themes"; } }
        public static string ICON_TRANSCRIPT { get { return "transcript"; } }
        public static string ICON_TRASH { get { return "trash"; } }
        public static string ICON_TRASH_BUTTON { get { return "trash.button"; } }
        public static string ICON_TWITTER { get { return "twitter"; } }
        public static string ICON_TWOCOLUMN { get { return "twocolumn"; } }
        public static string ICON_TWOCOLUMN_BUTTON { get { return "twocolumn.button"; } }
        public static string ICON_UPLOAD { get { return "upload"; } }
        public static string ICON_UPLOAD_ALT { get { return "upload.alt"; } }
        public static string ICON_USERACCOUNTDATA { get { return "useraccountdata"; } }
        public static string ICON_USERF { get { return "userf"; } }
        public static string ICON_USERM { get { return "userm"; } }
        public static string ICON_USERM_BUTTON { get { return "userm.button"; } }
        public static string ICON_USERM_WHITE { get { return "userm.white"; } }
        public static string ICON_USERSANDGROUPS { get { return "usersandgroups"; } }
        public static string ICON_VCARD { get { return "vcard"; } }
        public static string ICON_VIDEO { get { return "video"; } }
        public static string ICON_VIDEO_BUTTON { get { return "video.button"; } }
        public static string ICON_VIEW { get { return "view"; } }
        public static string ICON_WEBEX { get { return "webex"; } }
        public static string ICON_WEBMEETING { get { return "webmeeting"; } }
        public static string ICON_WIDGET_ANALYTICS { get { return "widget.analytics"; } }
        public static string ICON_WIDGET_SYSTEM { get { return "widget.system"; } }
        public static string ICON_YOUTUBE { get { return "youtube"; } }

        public static string BUTTON_LAUNCHER_EXIT { get { return "launcher.exit"; } }
        public static string BUTTON_LAUNCHER_NEXT { get { return "launcher.next"; } }
        public static string BUTTON_LAUNCHER_MORE { get { return "launcher.more"; } }
        public static string BUTTON_LAUNCHER_PREVIOUS { get { return "launcher.previous"; } }
        public static string BUTTON_MENU { get { return "menubugicon"; } }

        // credit card icons
        public static string ICON_MASTER_CARD { get { return "mastercard"; } }
        public static string ICON_VISA_CARD { get { return "visacard"; } }
        public static string ICON_AMEX_CARD { get { return "amexcard"; } }
        public static string ICON_DISCOVER_CARD { get { return "discovercard"; } }

        #endregion
        #endregion

        #region Methods
        #region GetIconPath
        public static string GetIconPath(string iconName, string iconExtension, bool forCustomerManager = false, bool useDefaultOnly = false)
        {
            string iconPath = String.Empty;

            if (forCustomerManager)
            {
                // set the icon path
                iconPath += SitePathConstants.CM_IMAGES_ICONS_64 + iconName + iconExtension;
            }
            else
            {
                string iconFolderDefault = String.Empty;
                string iconFolderSite = String.Empty;

                // get the paths to the iconset
                if (AsentiaSessionState.IsThemePreviewMode)
                {
                    iconFolderDefault += SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET_PREVIEW_64;
                    iconFolderSite += SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET_PREVIEW_64;
                }
                else
                {
                    iconFolderDefault += SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET_64;
                    iconFolderSite += SitePathConstants.SITE_TEMPLATE_ICONSETS_ICONSET_64;
                }

                // set the full icon path
                if (useDefaultOnly || AsentiaSessionState.SiteHostname == "default")
                {
                    // set the icon path
                    iconPath += iconFolderDefault + iconName + iconExtension;
                }
                else
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath(iconFolderSite + iconName + iconExtension)))
                    {
                        // set the icon path
                        iconPath += iconFolderSite + iconName + iconExtension;
                    }
                    else
                    {
                        // set the icon path
                        iconPath += iconFolderDefault + iconName + iconExtension;
                    }
                }
            }

            return iconPath;
        }
        #endregion

        #region GetButtonPath
        public static string GetButtonPath(string buttonName, string buttonExtension, bool forCustomerManager = false, bool useDefaultOnly = false)
        {
            string buttonPath = String.Empty;

            if (forCustomerManager)
            {
                // set the button path
                buttonPath += SitePathConstants.CM_IMAGES_BUTTONS + buttonName + buttonExtension;
            }
            else
            {
                string buttonFolderDefault = String.Empty;
                string buttonFolderSite = String.Empty;

                buttonFolderDefault += SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_BUTTONS;
                buttonFolderSite += SitePathConstants.SITE_TEMPLATE_IMAGES_BUTTONS;

                if (useDefaultOnly || AsentiaSessionState.SiteHostname == "default")
                {
                    // set the button path
                    buttonPath += buttonFolderDefault + buttonName + buttonExtension;
                }
                else
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath(buttonFolderSite + buttonName + buttonExtension)))
                    {
                        // set the button path
                        buttonPath += buttonFolderSite + buttonName + buttonExtension;
                    }
                    else
                    {
                        // set the button path
                        buttonPath += buttonFolderDefault + buttonName + buttonExtension;
                    }
                }
            }

            return buttonPath;
        }
        #endregion

        #region GetAvatarPath
        public static string GetAvatarPath(string avatarName, string avatarExtension, bool forCustomerManager = false, bool useDefaultOnly = false)
        {
            string avatarPath = String.Empty;

            if (forCustomerManager)
            {
                // set the avatar path
                avatarPath += SitePathConstants.CM_IMAGES_AVATARS + avatarName + avatarExtension;
            }
            else
            {
                string avatarFolderDefault = String.Empty;
                string avatarFolderSite = String.Empty;

                avatarFolderDefault += SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_AVATARS;
                avatarFolderSite += SitePathConstants.SITE_TEMPLATE_IMAGES_AVATARS;

                if (useDefaultOnly || AsentiaSessionState.SiteHostname == "default")
                {
                    // set the avatar path
                    avatarPath += avatarFolderDefault + avatarName + avatarExtension;
                }
                else
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath(avatarFolderSite + avatarName + avatarExtension)))
                    {
                        // set the avatar path
                        avatarPath += avatarFolderSite + avatarName + avatarExtension;
                    }
                    else
                    {
                        // set the avatar path
                        avatarPath += avatarFolderDefault + avatarName + avatarExtension;
                    }
                }
            }

            return avatarPath;
        }
        #endregion
        #endregion
    }
}
