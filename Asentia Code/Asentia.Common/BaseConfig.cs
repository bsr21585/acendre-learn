﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Web;
using System.Xml;

namespace Asentia.Common
{
    #region WebConfigKeyException
    /// <summary>
    /// Thrown when a requested key is not found in the web.config, or when 
    /// an unhandled exception occurs while retrieving a value.
    /// </summary>
    public class WebConfigKeyException : AsentiaException
    {
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public WebConfigKeyException()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message</param>
        public WebConfigKeyException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message</param>
        /// <param name="innerException">inner exception</param>
        public WebConfigKeyException(string message, Exception innerException)
            : base(message, innerException)
        { ;}
    }
    #endregion

    /// <summary>
    /// Abstract class to retrieve Configuration Settings from the web.config.
    /// </summary>
    public abstract class BaseConfig
    {
        #region WebConfigFilePath
        /// <summary>
        /// Direct path to the web.config to be used. This is used when we cannot
        /// determine the appropriate account web.config to be used because of 
        /// session state not being available, i.e. the Job Processor service.
        /// Utilized in conjunction with web.config parameters that have overrideAllowed
        /// set to true. The presence of a value here will override all other attempts
        /// to grab a value from any other web.config, so if the path stored here is not
        /// correct, an exception will be thrown.
        /// </summary>
        [ThreadStatic]
        public static string WebConfigFilePath;
        #endregion

        #region BaseConfigSection
        /// <summary>
        /// The Configuration Section from which base settings are pulled.
        /// </summary>
        public static string BaseConfigSection
        {
            get { return "CommonSettings"; }
        }
        #endregion

        #region RootFolder
        /// <summary>
        /// The fully-qualified path on the web server of the web site root directory.
        /// </summary>
        public static string RootFolder
        {
            get
            {
                string configPath = HttpContext.Current.Server.MapPath("/web.config");
                return Path.GetDirectoryName(configPath) + @"\";
            }
        }
        #endregion

        #region AppSettingsObject
        /// <summary>
        /// Returns an application settings Object.
        /// </summary>
        /// <remarks>
        /// Used by the other member functions to retrieve the value from the web.config, which
        /// is then cast to the appropriate type. This should not be called directly. Use the
        /// member functions.
        /// </remarks>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <param name="key">configuration key</param>
        /// <returns>Config value as object.</returns>
        protected static object AppSettingsObject(string key)
        {
            object value = (object)ConfigurationManager.AppSettings[key];

            if (value != null)
            { return value; }
            else
            { throw new WebConfigKeyException(String.Format(_GlobalResources.KeyNotFound, key)); }
        }

        /// <summary>
        /// Returns an application settings Object.
        /// </summary>
        /// <remarks>
        /// Used by the other member functions to retrieve the value from the web.config, which
        /// is then cast to the appropriate type.
        /// </remarks>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <param name="key">configuration key</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as object.</returns>
        protected static object AppSettingsObject(string key, object defaultValue)
        {
            try
            {
                return AppSettingsObject(key);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region AppSettingsString
        /// <summary>
        /// Returns AppSettings string.
        /// </summary>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <seealso cref="AppSettingsObject" />
        /// <param name="key">key</param>
        /// <returns>Config value as string.</returns>
        public static string AppSettingsString(string key)
        {
            try
            {
                return (string)AppSettingsObject(key);
            }
            catch (Exception e)
            {
                throw new WebConfigKeyException(String.Format(_GlobalResources.KeyIsNotAString, key), e);
            }
        }

        /// <summary>
        /// Returns AppSettings string.
        /// </summary>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <seealso cref="AppSettingsObject" />
        /// <param name="key">key</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as string.</returns>
        public static string AppString(string key, string defaultValue)
        {
            try
            {
                return AppSettingsString(key);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region AppSettingsInt
        /// <summary>
        /// Returns AppSettings int.
        /// </summary>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <seealso cref="AppSettingsObject" />
        /// <param name="key">key</param>
        /// <returns>Config value as int.</returns>
        public static int AppSettingsInt(string key)
        {
            try
            {
                return Convert.ToInt32(AppSettingsObject(key));
            }
            catch (Exception e)
            {
                throw new WebConfigKeyException(String.Format(_GlobalResources.KeyIsNotAnInteger, key), e);
            }
        }

        /// <summary>
        /// Returns AppSettings int.
        /// </summary>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <seealso cref="AppSettingsObject" />
        /// <param name="key">key</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as int.</returns>
        public static int AppSettingsInt(string key, int defaultValue)
        {
            try
            {
                return AppSettingsInt(key);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region AppSettingsDecimal
        /// <summary>
        /// Returns AppSettings decimal.
        /// </summary>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <seealso cref="AppSettingsObject" />
        /// <param name="key">key</param>
        /// <returns>Config value as decimal.</returns>
        public static decimal AppSettingsDecimal(string key)
        {
            try
            {
                return Convert.ToDecimal(AppSettingsObject(key));
            }
            catch (Exception e)
            {
                throw new WebConfigKeyException(String.Format(_GlobalResources.KeyIsNotADecimal, key), e);
            }
        }

        /// <summary>
        /// Returns AppSettings decimal.
        /// </summary>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <seealso cref="AppSettingsObject" />
        /// <param name="key">key</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as decimal.</returns>
        public static decimal AppSettingsDecimal(string key, decimal defaultValue)
        {
            try
            {
                return AppSettingsDecimal(key);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region AppSettingsBool
        /// <summary>
        /// Returns AppSettings boolean.
        /// </summary>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <seealso cref="AppSettingsObject" />
        /// <param name="key">key</param>
        /// <returns>Config value as boolean.</returns>
        public static bool AppSettingsBool(string key)
        {
            try
            {
                return Convert.ToBoolean(AppSettingsObject(key));
            }
            catch (Exception e)
            {
                throw new WebConfigKeyException(String.Format(_GlobalResources.KeyIsNotABoolean, key), e);
            }
        }

        /// <summary>
        /// Returns AppSettings boolean.
        /// </summary>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <seealso cref="AppSettingsObject" />
        /// <param name="key">key</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as boolean.</returns>
        public static bool AppSettingsBool(string key, bool defaultValue)
        {
            try
            {
                return AppSettingsBool(key);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region AppSettingsDateTime
        /// <summary>
        /// Returns AppSettings DateTime.
        /// </summary>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <seealso cref="AppSettingsObject" />
        /// <param name="key">key</param>
        /// <returns>Config value as DateTime.</returns>
        public static DateTime AppSettingsDateTime(string key)
        {
            try
            {
                return Convert.ToDateTime(AppSettingsObject(key));
            }
            catch (Exception e)
            {
                throw new WebConfigKeyException(String.Format(_GlobalResources.KeyIsNotADateTime, key), e);
            }
        }

        /// <summary>
        /// Returns AppSettings DateTime.
        /// </summary>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <seealso cref="AppSettingsObject" />
        /// <param name="key">key</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as DateTime.</returns>
        public static DateTime AppSettingsDateTime(string key, DateTime defaultValue)
        {
            try
            {
                return AppSettingsDateTime(key);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region ConfigObject
        /// <summary>
        /// Returns configuration section Object.
        /// </summary>
        /// <remarks>
        /// Used by the other member functions to retrieve the value from the web.config, which
        /// is then cast to the appropriate type. This should not be called directly. Use the
        /// member functions.
        /// </remarks>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <example>
        ///	If the web.config has a section that looks like this:
        ///	<code>
        ///		<Section>
        ///			<add key="KeyName" value="KeyValue" />
        ///		</Section>
        ///	</code>
        ///	You would use the following code to retrieve the value:
        ///	<code>object objValue = BaseConfig.ConfigObject("Section", "KeyName");</code>
        ///	objValue would be equal to "KeyValue"
        /// </example>
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <returns>Config value as object.</returns>
        protected static object ConfigObject(string section, string key, bool overrideAllowed)
        {
            object configKey = null;

            // first check if WebConfigFilePath is not null
            // the presence of a value here overrides everything
            // therefore, the absence of a value equals an exception
            if (WebConfigFilePath != null)
            {
                configKey = _ConfigObjectFromSpecifiedConfig(section, key);

                if (configKey == null)
                { throw new WebConfigKeyException(section + " : " + String.Format(_GlobalResources.KeyNotFound, key)); }
                else
                { return configKey; }
            }

            if (overrideAllowed)
            {
                // check for presence of IdAccount session variable
                if (AsentiaSessionState.IdAccount > 0)
                {
                    configKey = _ConfigObjectFromAccountConfig(section, key);

                    if (configKey != null)
                    { return configKey; }
                }
            }

            // if we've gotten to this point, it means either the key does not have overrideAllowed, or 
            // it could not be loaded from the account web.config, we will never get here when the web.config
            // has been directly specified.

            NameValueCollection defaultConfigSection = (NameValueCollection)ConfigurationManager.GetSection(section);
            
            if (defaultConfigSection != null)
            {
                configKey = (object)defaultConfigSection[key];

                if (configKey == null)
                { throw new WebConfigKeyException(section + " : " + String.Format(_GlobalResources.KeyNotFound, key)); }

                return configKey;
            }
            else
            { throw new WebConfigKeyException(String.Format(_GlobalResources.KeyNotFound, section)); }
        }

        /// <summary>
        /// Returns configuration section Object.
        /// </summary>
        /// <remarks>
        /// Used by the other member functions to retrieve the value from the web.config, which
        /// is then cast to the appropriate type. If the key is not found, or any exception occurs, 
        /// the default value is returned. No exceptions are thrown.
        /// </remarks>
        /// <example>
        ///	If the web.config has a section that looks like this:
        ///	<code>
        ///		<Section>
        ///			<add key="KeyName" value="KeyValue" />
        ///		</Section>
        ///	</code>
        ///	You would use the following code to retrieve the value:
        ///	<code>object objValue = BaseConfig.ConfigObject("Section", "KeyName");</code>
        ///	objValue would be equal to "KeyValue"
        /// </example>
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <param name="defaultValue">default value of the key if the key is not found</param>
        /// <returns>Config value as object.</returns>
        protected static object ConfigObject(string section, string key, bool overrideAllowed, object defaultValue)
        {
            try
            {
                return ConfigObject(section, key, overrideAllowed);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region _ConfigObjectFromAccountConfig
        private static object _ConfigObjectFromAccountConfig(string section, string key)
        {
            try
            {
                string accountConfigFullPath = HttpContext.Current.Server.MapPath("/_accounts/" + 
                    AsentiaSessionState.IdAccount.ToString() + 
                    "/Web.config");

                ExeConfigurationFileMap accountConfigFileMap = new ExeConfigurationFileMap()
                { ExeConfigFilename = accountConfigFullPath };

                Configuration accountConfig = ConfigurationManager.OpenMappedExeConfiguration(
                   accountConfigFileMap, ConfigurationUserLevel.None);

                ConfigurationSection accountConfigSection = accountConfig.GetSection(section);

                if (accountConfigSection != null)
                {
                    string accountConfigSectionRawXml = accountConfigSection.SectionInformation.GetRawXml();
                    XmlDocument accountConfigSectionXmlDoc = new XmlDocument();
                    accountConfigSectionXmlDoc.Load(new StringReader(accountConfigSectionRawXml));

                    NameValueSectionHandler handler = new NameValueSectionHandler();

                    NameValueCollection handlerCreatedCollection =
                        (NameValueCollection)handler.Create(null,
                                                            null,
                                                            accountConfigSectionXmlDoc.DocumentElement);

                    if (handlerCreatedCollection != null)
                    {
                        object configKey = (object)handlerCreatedCollection[key];
                        return configKey;
                    }
                    else
                    { return null; }
                }
                else
                { return null; }
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region _ConfigObjectFromSpecifiedConfig
        private static object _ConfigObjectFromSpecifiedConfig(string section, string key)
        {
            try
            {
                string accountConfigFullPath;

                if (WebConfigFilePath.Substring(WebConfigFilePath.Length - 1, 1) != "\\")
                { accountConfigFullPath = WebConfigFilePath + "\\web.config"; }
                else
                { accountConfigFullPath = WebConfigFilePath + "web.config"; }

                ExeConfigurationFileMap accountConfigFileMap = new ExeConfigurationFileMap() { ExeConfigFilename = accountConfigFullPath };

                Configuration accountConfig = ConfigurationManager.OpenMappedExeConfiguration(
                   accountConfigFileMap, ConfigurationUserLevel.None);

                ConfigurationSection accountConfigSection = accountConfig.GetSection(section);

                if (accountConfigSection != null)
                {
                    string accountConfigSectionRawXml = accountConfigSection.SectionInformation.GetRawXml();
                    XmlDocument accountConfigSectionXmlDoc = new XmlDocument();
                    accountConfigSectionXmlDoc.Load(new StringReader(accountConfigSectionRawXml));

                    NameValueSectionHandler handler = new NameValueSectionHandler();

                    NameValueCollection handlerCreatedCollection =
                        (NameValueCollection)handler.Create(null,
                                                            null,
                                                            accountConfigSectionXmlDoc.DocumentElement);

                    if (handlerCreatedCollection != null)
                    {
                        object configKey = (object)handlerCreatedCollection[key];
                        return configKey;
                    }
                    else
                    { return null; }
                }
                else
                { return null; }
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region ConfigString
        /// <summary>
        /// Returns configuration section string.
        /// </summary>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <seealso cref="ConfigObject" />
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <returns>Config value as string.</returns>
        public static string ConfigString(string section, string key, bool overrideAllowed)
        {
            try
            {
                return (string)ConfigObject(section, key, overrideAllowed);
            }
            catch (Exception e)
            {
                throw new WebConfigKeyException(String.Format(_GlobalResources.KeyIsNotAString, key), e);
            }
        }

        /// <summary>
        /// Returns configuration section string
        /// </summary>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <seealso cref="ConfigObject" />
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as string.</returns>
        public static string ConfigString(string section, string key, bool overrideAllowed, string defaultValue)
        {
            try
            {
                return ConfigString(section, key, overrideAllowed);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region ConfigInt
        /// <summary>
        /// Returns configuration section int.
        /// </summary>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <seealso cref="ConfigObject" />
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <returns>Config value as int.</returns>
        public static int ConfigInt(string section, string key, bool overrideAllowed)
        {
            try
            {
                return Convert.ToInt32(ConfigObject(section, key, overrideAllowed));
            }
            catch (Exception e)
            {
                throw new WebConfigKeyException(String.Format(_GlobalResources.KeyIsNotAnInteger, key), e);
            }
        }

        /// <summary>
        /// Returns configuration section int.
        /// </summary>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <seealso cref="ConfigObject" />
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as int.</returns>
        public static int ConfigInt(string section, string key, bool overrideAllowed, int defaultValue)
        {
            try
            {
                return ConfigInt(section, key, overrideAllowed);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region ConfigDecimal
        /// <summary>
        /// Returns configuration section decimal.
        /// </summary>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <seealso cref="ConfigObject" />
        /// <param name="section">configuration section</param>
        /// <param name="ley">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <returns>Config value as decimal.</returns>
        public static decimal ConfigDecimal(string section, string key, bool overrideAllowed)
        {
            try
            {
                return Convert.ToDecimal(ConfigObject(section, key, overrideAllowed));
            }
            catch (Exception e)
            {
                throw new WebConfigKeyException(String.Format(_GlobalResources.KeyIsNotADecimal, key), e);
            }
        }

        /// <summary>
        /// Returns configuration section decimal.
        /// </summary>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <seealso cref="ConfigObject" />
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as decimal.</returns>
        public static decimal ConfigDecimal(string section, string key, bool overrideAllowed, decimal defaultValue)
        {
            try
            {
                return ConfigDecimal(section, key, overrideAllowed);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region ConfigBool
        /// <summary>
        /// Returns Configuration section boolean.
        /// </summary>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <seealso cref="ConfigObject" />
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <returns>Config value as boolean.</returns>
        public static bool ConfigBool(string section, string key, bool overrideAllowed)
        {
            try
            {
                return Convert.ToBoolean(ConfigObject(section, key, overrideAllowed));
            }
            catch (Exception e)
            {
                throw new WebConfigKeyException(String.Format(_GlobalResources.KeyIsNotABoolean, key), e);
            }
        }

        /// <summary>
        /// Returns Configuration section boolean.
        /// </summary>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <seealso cref="ConfigObject" />
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as boolean.</returns>
        public static bool ConfigBool(string section, string key, bool overrideAllowed, bool defaultValue)
        {
            try
            {
                return ConfigBool(section, key, overrideAllowed);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region ConfigDateTime
        /// <summary>
        /// Returns configuration section as DateTime.
        /// </summary>
        /// <exception cref="WebConfigKeyException">
        /// Thrown when the configuration key is not found.
        /// </exception>
        /// <seealso cref="ConfigObject" />
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <returns>Config value as DateTime.</returns>
        public static DateTime ConfigDateTime(string section, string key, bool overrideAllowed)
        {
            try
            {
                return Convert.ToDateTime(ConfigObject(section, key, overrideAllowed));
            }
            catch (Exception e)
            {
                throw new WebConfigKeyException(String.Format(_GlobalResources.KeyIsNotADateTime, key), e);
            }
        }

        /// <summary>
        /// Returns configuration section as DateTime.
        /// </summary>
        /// <remarks>
        /// If the key is not found, or any exception occurs, the default value is
        /// returned. No exceptions are thrown.
        /// </remarks>
        /// <seealso cref="ConfigObject" />
        /// <param name="section">configuration section</param>
        /// <param name="key">configuration key</param>
        /// <param name="overrideAllowed">can this key be overridden by a matching key in the account's web.config</param>
        /// <param name="defaultValue">default value</param>
        /// <returns>Config value as DateTime.</returns>
        public static DateTime ConfigDateTime(string section, string key, bool overrideAllowed, DateTime defaultValue)
        {
            try
            {
                return ConfigDateTime(section, key, overrideAllowed);
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion
    }
}