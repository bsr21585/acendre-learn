﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Asentia.Common
{
    #region PurchaseItemType ENUM
    /// <summary>
    /// PurchaseItemType ENUM is used with TransactionItems to determine the type of item.
    /// </summary>
    public enum PurchaseItemType
    {
        Course = 1,
        Catalog = 2,
        LearningPath = 3,
        InstructorLedTraining = 4,
        Community = 5,
    }
    #endregion
    public class EcommerceSettings
    {
        #region Currency ENUM & Metadata
        public enum Currency
        {
            USDollar = 1,
            Euro = 2,
            PoundSterling = 3,
            CanadianDollar = 4,
            MexicanPeso = 5,
            JapaneseYen = 6,
            AustralianDollar = 7,
            SwedishKronor = 8,
            SwissFranc = 9,
            SouthAfricanRand = 10
        }

        public class CurrencyMetadata
        {
            public Currency Id;
            public string Code;
            public string Symbol;
            public string Label;
            public bool IsAuthorizeEnabled;
            public bool IsPayPalEnabled;
            public bool AllowDecimals;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public EcommerceSettings()
        {
            // get portal specific settings
            this.IsEcommerceEnabled = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_ENABLED);
            this.ProcessingMethod = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD);
            this.CurrencyCode = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_CURRENCY);

            // get IsEcommerceSet by checking enabled and the processor
            if (
                this.IsEcommerceEnabled
                && (this.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE || this.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
               )
            { this.IsEcommerceSet = true; }
            else
            { this.IsEcommerceSet = false; }
            
            // get IsEcommerceSetAndVerified by checking all e-commerce conditions
            if (
                this.IsEcommerceEnabled
                && this.IsEcommerceSet
                && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD_VERIFIED)
               )
            { this.IsEcommerceSetAndVerified = true; }
            else
            { this.IsEcommerceSetAndVerified = false; }

            // get extended properties for currency
            foreach (Currency key in Currencies.Keys)
            {
                if (this.CurrencyCode == Currencies[key].Code)
                {
                    this.CurrencySymbol = Currencies[key].Symbol;
                    this.CurrencyDisplayName = Currencies[key].Label;
                }
            }
        }
        #endregion

        #region Properties
        // STATIC CONSTANTS

        /// <summary>
        ///  Dictionary to hold the list of currencies and their metadata.
        /// </summary>
        public static IReadOnlyDictionary<Currency, CurrencyMetadata> Currencies
        {
            get
            {
                // set-up the dictionary of currencies and their metadata
                Dictionary<Currency, CurrencyMetadata> currencyList = new Dictionary<Currency, CurrencyMetadata>();
                currencyList.Add(Currency.USDollar, new CurrencyMetadata { Id = Currency.USDollar, Code = "USD", Symbol = "$", Label = _GlobalResources.USDollar, IsAuthorizeEnabled = true, IsPayPalEnabled = true, AllowDecimals = true });
                currencyList.Add(Currency.Euro, new CurrencyMetadata { Id = Currency.Euro, Code = "EUR", Symbol = "€", Label = _GlobalResources.Euro, IsAuthorizeEnabled = true, IsPayPalEnabled = true, AllowDecimals = true });
                currencyList.Add(Currency.PoundSterling, new CurrencyMetadata { Id = Currency.PoundSterling, Code = "GBP", Symbol = "£", Label = _GlobalResources.PoundSterling, IsAuthorizeEnabled = true, IsPayPalEnabled = true, AllowDecimals = true });
                currencyList.Add(Currency.CanadianDollar, new CurrencyMetadata { Id = Currency.CanadianDollar, Code = "CAD", Symbol = "$", Label = _GlobalResources.CanadianDollar, IsAuthorizeEnabled = true, IsPayPalEnabled = true, AllowDecimals = true });
                currencyList.Add(Currency.MexicanPeso, new CurrencyMetadata { Id = Currency.MexicanPeso, Code = "MXN", Symbol = "$", Label = _GlobalResources.MexicanPeso, IsAuthorizeEnabled = false, IsPayPalEnabled = true, AllowDecimals = true });
                currencyList.Add(Currency.JapaneseYen, new CurrencyMetadata { Id = Currency.JapaneseYen, Code = "JPY", Symbol = "¥", Label = _GlobalResources.JapaneseYen, IsAuthorizeEnabled = false, IsPayPalEnabled = true, AllowDecimals = false });
                currencyList.Add(Currency.AustralianDollar, new CurrencyMetadata { Id = Currency.AustralianDollar, Code = "AUD", Symbol = "$", Label = _GlobalResources.AustralianDollar, IsAuthorizeEnabled = true, IsPayPalEnabled = true, AllowDecimals = true });
                currencyList.Add(Currency.SwedishKronor, new CurrencyMetadata { Id = Currency.SwedishKronor, Code = "SEK", Symbol = "kr", Label = _GlobalResources.SwedishKronor, IsAuthorizeEnabled = true, IsPayPalEnabled = true, AllowDecimals = true });
                currencyList.Add(Currency.SwissFranc, new CurrencyMetadata { Id = Currency.SwissFranc, Code = "CHF", Symbol = "CHF", Label = _GlobalResources.SwissFranc, IsAuthorizeEnabled = true, IsPayPalEnabled = true, AllowDecimals = true });
                currencyList.Add(Currency.SouthAfricanRand, new CurrencyMetadata { Id = Currency.SouthAfricanRand, Code = "ZAR", Symbol = "R", Label = _GlobalResources.SouthAfricanRand, IsAuthorizeEnabled = true, IsPayPalEnabled = false, AllowDecimals = true });

                return new ReadOnlyDictionary<Currency, CurrencyMetadata>(currencyList);
            }
        }

        /// <summary>
        /// Constants for processing method names.
        /// </summary>
        public static readonly string PROCESSING_METHOD_NONE = "none";
        public static readonly string PROCESSING_METHOD_AUTHORIZE = "authorize";
        public static readonly string PROCESSING_METHOD_PAYPAL = "paypal";

        /// <summary>
        /// Constants for processing mode names.
        /// </summary>
        public static readonly string PROCESSING_MODE_LIVE = "live";
        public static readonly string PROCESSING_MODE_TEST = "test";
        public static readonly string PROCESSING_MODE_DEVELOPER = "developer";

        /// <summary>
        /// Constants for credit card icons.
        /// </summary>
        public const string CREDITCARDICON_VISA = "/_images/CreditCards/visacard.png";
        public const string CREDITCARDICON_MASTERCARD = "/_images/CreditCards/mastercard.png";
        public const string CREDITCARDICON_AMEX = "/_images/CreditCards/amexcard.png";
        public const string CREDITCARDICON_DISCOVERCARD = "/_images/CreditCards/discovercard.png";

        /// <summary>
        /// Constant for the filename used to store the HTML terms and conditions.
        /// </summary>
        public const string TERMS_AND_CONDITIONS_FILENAME = "EcommerceTermsAndConditions.html";

        // SETTINGS SPECIFIC TO PORTAL

        /// <summary>
        /// Is e-commerce enabled on this portal?
        /// </summary>
        public bool IsEcommerceEnabled;

        /// <summary>
        /// The portal's e-commerce processing method.
        /// </summary>
        public string ProcessingMethod;

        /// <summary>
        /// Is e-commerce on and set to a processor?
        /// </summary>
        public bool IsEcommerceSet;

        /// <summary>
        /// Is e-commerce on, set to a processor, and verified?
        /// </summary>
        public bool IsEcommerceSetAndVerified;

        /// <summary>
        /// The portal's e-commerce currency code.
        /// </summary>
        public string CurrencyCode;

        /// <summary>
        /// The portal's e-commerce currency symbol.
        /// </summary>
        public string CurrencySymbol;

        /// <summary>
        /// The portal's e-commerce currency display name.
        /// </summary>
        public string CurrencyDisplayName;
        #endregion
    }
}