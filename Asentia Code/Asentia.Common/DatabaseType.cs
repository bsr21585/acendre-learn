﻿using System.ComponentModel;

namespace Asentia.Common
{
    /// <summary>
    /// Database Types
    /// </summary>
    /// <remarks>
    /// Specifies the type of database. Used by the database handler <see cref="AsentiaDatabase"/>
    /// </remarks>
    public enum DatabaseType
    {
        /// <summary>
        /// Account Database
        /// </summary>
        /// <remarks>
        [Description("Account Database")]
        Account = 0,

        /// <summary>
        /// Server Database
        /// </summary>
        /// <remarks>
        [Description("Customer Manager Database")]
        CustomerManager = 1,

        /// <summary>
        /// Session State Database
        /// </summary>
        /// <remarks>
        [Description("Session State Database")]
        SessionState = 2,

        /// <summary>
        /// Account Database Using Web Config Path
        /// </summary>
        /// <remarks>
        [Description("Account Database Using Web Config Path")]
        AccountDatabaseUsingWebConfigPath = 3,

        /// <summary>
        /// Customer Manager Database Using Web Config Path
        /// </summary>
        /// <remarks>
        [Description("Customer Manager Database Using Web Config Path")]
        CustomerManagerDatabaseUsingWebConfigPath = 4,
    }
}