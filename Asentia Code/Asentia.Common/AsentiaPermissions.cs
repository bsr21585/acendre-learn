﻿using System;
using System.Collections.Generic;

namespace Asentia.Common
{
    public enum AsentiaPermission
    {
        // SYSTEM 0 - 99 (0 RESERVED)
        System_AccountSettings = 1,
        System_Configuration = 2,
        System_Ecommerce = 3,
        System_CouponCodes = 4,
        System_TrackingCodes = 5,
        System_EmailNotifications = 6,
        System_API = 7,
        System_xAPIEndpoints = 8,
        System_Logs = 9,
        System_WebMeetingIntegration = 10,
        System_RulesEngine = 11,
        
        // USERS AND GROUPS 100 - 199 (100 RESERVED)
        System_UserFieldConfiguration = 101, // This has been moved from "Users and Groups" to "System"
        UsersAndGroups_UserCreator = 102,
        UsersAndGroups_UserDeleter = 103,
        UsersAndGroups_UserEditor = 104,
        UsersAndGroups_UserManager = 105,
        UsersAndGroups_UserImpersonator = 106,
        UsersAndGroups_GroupManager = 107,
        UsersAndGroups_RoleManager = 108,
        UsersAndGroups_ActivityImport = 109,
        UsersAndGroups_CertificateImport = 110,
        UsersAndGroups_Leaderboards = 111,
        UsersAndGroups_GroupCreator = 112,
        UsersAndGroups_GroupDeleter = 113,
        UsersAndGroups_GroupEditor = 114,
        UsersAndGroups_UserRegistrationApproval = 115,

        // INTERFACE AND LAYOUT 200 - 299 (200 RESERVED)
        InterfaceAndLayout_HomePage = 201,
        InterfaceAndLayout_Masthead = 202,
        InterfaceAndLayout_Footer = 203,
        InterfaceAndLayout_CSSEditor = 204,
        InterfaceAndLayout_ImageEditor = 205,
        
        // LEARNING ASSETS 300 - 399 (300 RESERVED)
        LearningAssets_CourseCatalog = 301,
        LearningAssets_CourseContentManager = 302,
        LearningAssets_LearningPathContentManager = 303,
        LearningAssets_ContentPackageManager = 304,
        LearningAssets_CertificateTemplateManager = 305,
        LearningAssets_InstructorLedTrainingManager = 306,
        LearningAssets_ResourceManager = 307,
        LearningAssets_CertificationsManager = 308,
        LearningAssets_QuizAndSurveyManager = 309,
        LearningAssets_SelfEnrollmentApproval = 310,
        LearningAssets_CourseEnrollmentManager = 311,
        LearningAssets_LearningPathEnrollmentManager = 312,

        // REPORTING 400 - 499 (400 RESERVED)
        Reporting_Reporter_UserDemographics = 401,
        Reporting_Reporter_UserCourseTranscripts = 402,
        Reporting_Reporter_CatalogAndCourseInformation = 403,
        Reporting_Reporter_Certificates = 404,
        Reporting_Reporter_XAPI = 405,
        Reporting_Reporter_UserLearningPathTranscripts = 406,
        Reporting_Reporter_UserInstructorLedTrainingTranscripts = 407,
        Reporting_Reporter_Purchases = 408,
        Reporting_Reporter_UserCertificationTranscripts = 409,
    }

    /// <summary>
    /// Class that represents permissions for a role.
    /// </summary>
    [Serializable]
    public class AsentiaPermissionWithScope
    {
        public AsentiaPermission PermissionCode;
        public List<int> Scope;

        public AsentiaPermissionWithScope(AsentiaPermission permissionCode)
        {
            this.PermissionCode = permissionCode;
            this.Scope = null;
        }

        public AsentiaPermissionWithScope(AsentiaPermission permissionCode, string scope)
        {
            this.PermissionCode = permissionCode;

            if (!String.IsNullOrWhiteSpace(scope))
            {
                this.Scope = new List<int>();
                string[] stringArray = scope.Split(',');

                foreach (string value in stringArray)
                { Scope.Add(Convert.ToInt32(value)); }
            }
            else
            { this.Scope = null; }
        }
    }
}