﻿AuthorizeNetAccept = function (
    mode,
    acceptJSInstance,
    globallyScopedCallbackHandler,
    returnCodeMessagesDictionary,
    apiLogin,
    apiPublicKey,
    cardNumberField,
    cardExpirationMonthField,
    cardExpirationYearField,
    cardCVV2Field,
    cardholderZipField,
    cardholderNameField,    
    responseStatusOutputField,
    responseErrorMessagesOutputField,
    responseDataDescriptorField,
    responseDataTokenField,
    responseFinishedCallbackFunction
)
{
    // set the properties
    this.Mode = mode;
    this.AcceptJSInstance = acceptJSInstance;
    this.GloballyScopedCallbackHandler = globallyScopedCallbackHandler;
    this.ApiLogin = apiLogin;
    this.ApiPublicKey = apiPublicKey;
    this.CardNumberField = cardNumberField;
    this.CardExpirationMonthField = cardExpirationMonthField;
    this.CardExpirationYearField = cardExpirationYearField;
    this.CardCVV2Field = cardCVV2Field;
    this.CardholderZipField = cardholderZipField;
    this.CardholderNameField = cardholderNameField;
    this.ResponseDataDescriptorField = responseDataDescriptorField;
    this.ResponseDataTokenField = responseDataTokenField;
    this.ResponseStatusOutputField = responseStatusOutputField;
    this.ResponseErrorMessagesOutputField = responseErrorMessagesOutputField;
    this.ResponseFinishedCallbackFunction = responseFinishedCallbackFunction;

    // Response Codes
    // I_WC_01 - Success
    // E_WC_01 - Accept.js not included from CDN.
    // E_WC_02 - HTTPS connection is required.
    // E_WC_03 - Accept.js not loaded correctly.
    // E_WC_04 - Required field(s) are missing.
    // E_WC_05 - Credit Card Number is invalid.
    // E_WC_06 - Credit Card Expiration Month is invalid.
    // E_WC_07 - Credit Card Expiration Year is invalid.
    // E_WC_08 - Credit Card Expiration must be in future.
    // E_WC_10 - API Login is invalid.
    // E_WC_14 - Accept.js encryption failed.
    // E_WC_15 - CVV2 is invalid.
    // E_WC_16 - Cardholder zip code is invalid.
    // E_WC_17 - Cardholder name is invalid.
    // E_WC_18 - Public Key is invalid.
    
    // get the language-specific return code messages dictionary
    this.ReturnCodeMessagesDictionary = returnCodeMessagesDictionary;

    // declare the default return code messages dictionary - this is used if no language-specific dictionary is passed in
    this.DefaultReturnCodeMessagesDictionary = {
        "I_WC_01" : "Success",
        "E_WC_01" : "Accept.js library is not sourced from CDN. Please contact an administrator.",
        "E_WC_02" : "Transactions must be processed over HTTPS.",
        "E_WC_03" : "Accept.js library has not been loaded correctly. Please contact an administrator.",
        "E_WC_04" : "Credit Card Number, Expiration Date, and CVV2 are required.",
        "E_WC_05" : "Credit Card Number is invalid.",
        "E_WC_06" : "Expiration Month is invalid.",
        "E_WC_07" : "Expiration Year is invalid.",
        "E_WC_08" : "Expiration Date must be in the future.",
        "E_WC_10" : "Authorize.net API Login is invalid.",
        "E_WC_14" : "Accept.js encryption failed. Please contact an administrator.",
        "E_WC_15" : "CVV2 is invalid.",
        "E_WC_16" : "Cardholder Zip Code is invalid.",
        "E_WC_17" : "Cardholder Name is invalid.",
        "E_WC_18" : "Authorize.net Public Key is invalid.",
        "E_WC_A0" : "Authentication failed due to invalid API Login and/or Public Key."
    };

    // declare a flag for notification of the finish of the response back from Authorize.net
    this.ResponseIsFinished = false;

    // declare an array to hold errors coming from the response    
    this.ResponseErrors = [];
}

AuthorizeNetAccept.prototype.SendPaymentData = function () {
    var secureData = {}, authData = {}, cardData = {};

    // card data
    cardData.cardNumber = this.GetFieldValue(this.CardNumberField);
    cardData.month = this.GetFieldValue(this.CardExpirationMonthField);
    cardData.year = this.GetFieldValue(this.CardExpirationYearField);
    cardData.cardCode = this.GetFieldValue(this.CardCVV2Field);
    cardData.zip = this.GetFieldValue(this.CardholderZipField);
    cardData.fullName = this.GetFieldValue(this.CardholderNameField);

    secureData.cardData = cardData;

    // authentication data
    authData.apiLoginID = this.ApiLogin;
    authData.clientKey = this.ApiPublicKey;

    secureData.authData = authData;

    // "Accept" is the global object from Authorize.net Accept.js CDN
    this.AcceptJSInstance.dispatchData(secureData, this.GloballyScopedCallbackHandler);
}

AuthorizeNetAccept.prototype.GetFieldValue = function (field) {
    if (field != null && field != "undefined") {
        return field.value;
    }
    else {
        return "";
    }
}

AuthorizeNetAccept.prototype.HandleResponse = function (response) {
    // re-initialize all hidden input values that hold response data
    this.ResponseDataDescriptorField.value = "";
    this.ResponseDataTokenField.value = "";
    this.ResponseStatusOutputField.value = "";
    this.ResponseErrorMessagesOutputField.value = "";

    // handle the response
    if (response.messages.resultCode === "Error") {
        for (var i = 0; i < response.messages.message.length; i++) {            
            // because Accept.js has bugs with error codes, we need to detect a "ValidateMode"
            // and re-assign to a custom error code for failed authentication, we will remove this
            // when and if it is fixed in Accept.js
            if (this.Mode == "ValidateGateway" && (response.messages.message[i].code == "E_WC_15" || response.messages.message[i].code == "E_WC_17")) {
                this.ResponseErrors.push("E_WC_A0");
            }            
            else {
                this.ResponseErrors.push(response.messages.message[i].code);
            }

            this.HandleResponseError();
        }
    }
    else {
        this.HandleResponseSuccess(response.opaqueData);        
    }

    return false;
}

AuthorizeNetAccept.prototype.HandleResponseSuccess = function (responseData) {
    this.ResponseStatusOutputField.value = "SUCCESS";
    this.ResponseDataDescriptorField.value = responseData.dataDescriptor;
    this.ResponseDataTokenField.value = responseData.dataValue;

    // notify that the response is finished
    this.ResponseIsFinished = true;

    // call the response finished callback method if one is present
    if (this.ResponseFinishedCallbackFunction != null) {
        this.ResponseFinishedCallbackFunction();
    }
}

AuthorizeNetAccept.prototype.HandleResponseError = function () {
    this.ResponseStatusOutputField.value = "ERROR";
    var responseOutputFieldValue = "";

    // loop through errors and get the corresponding messages
    for (var i = 0; i < this.ResponseErrors.length; i++)
    {
        var responseErrorMessage = "";

        if (this.ReturnCodeMessagesDictionary != null && this.ReturnCodeMessagesDictionary != "undefined") {
            if (this.ReturnCodeMessagesDictionary[this.ResponseErrors[i]] != null && this.ReturnCodeMessagesDictionary[this.ResponseErrors[i]] != "undefined") {
                responseErrorMessage = this.ReturnCodeMessagesDictionary[this.ResponseErrors[i]];
            }
            else {
                responseErrorMessage = this.DefaultReturnCodeMessagesDictionary[this.ResponseErrors[i]];
            }
        }
        else {
            responseErrorMessage = this.DefaultReturnCodeMessagesDictionary[this.ResponseErrors[i]];
        }

        if (i == 0) {
            responseOutputFieldValue = this.ResponseErrors[i] + " - " + responseErrorMessage;
        }
        else {
            responseOutputFieldValue += "|" + this.ResponseErrors[i] + " - " + responseErrorMessage;
        }
    }

    // put the error messages in the field
    this.ResponseErrorMessagesOutputField.value = responseOutputFieldValue;

    // notify that the response is finished
    this.ResponseIsFinished = true;

    // call the response finished callback method if one is present
    if (this.ResponseFinishedCallbackFunction != null) {
        this.ResponseFinishedCallbackFunction();
    }
}