﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Security;
using System.Security.Cryptography;

namespace Asentia.Common
{
    public class AsentiaAESEncryption
    {
        #region Constructor
        public AsentiaAESEncryption()
        {
            this._PrivateKey = this._GetPrivateKey();
        }
        #endregion

        #region Private Properties
        private string _PrivateKey;
        #endregion

        #region Methods
        #region Encrypt
        /// <summary>
        /// Encrypts a string.
        /// </summary>
        /// <param name="plainString">the string to encrypt</param>
        /// <returns>the encrypted string</returns>
        public string Encrypt(string plainString)
        {
            if (String.IsNullOrWhiteSpace(plainString))
            { return null; }

            // ensure that we have the private key            
            if (String.IsNullOrWhiteSpace(this._PrivateKey))
            { throw new AsentiaException(_GlobalResources.EncryptionKeyIsNotSet); }

            // BEGIN ENCRYPTION PROCESS

            SymmetricAlgorithm aesManager = new AesCryptoServiceProvider();
            SHA256 sha256 = SHA256.Create();

            // use hashed private key as the crypto key
            aesManager.KeySize = 256;
            aesManager.Key = sha256.ComputeHash(Encoding.UTF8.GetBytes(this._PrivateKey));

            // set the cipher and padding modes
            aesManager.Mode = CipherMode.CBC;
            aesManager.Padding = PaddingMode.PKCS7;

            // generate an initialization vector
            aesManager.GenerateIV();

            // create the encryptor
            ICryptoTransform encryptor = aesManager.CreateEncryptor(aesManager.Key, aesManager.IV);

            // convert string to bytes
            byte[] plainBytes = Encoding.UTF8.GetBytes(plainString);

            // do the encryption
            byte[] encryptedBytes;

            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (BinaryWriter bwEncrypt = new BinaryWriter(csEncrypt))
                    {
                        msEncrypt.Write(aesManager.IV, 0, aesManager.IV.Length); // IV is placed as the first bytes of the byte array
                        bwEncrypt.Write(plainBytes);
                        csEncrypt.FlushFinalBlock();
                    }
                }

                encryptedBytes = msEncrypt.ToArray();
            }

            // return base 64 converted string
            return Convert.ToBase64String(encryptedBytes);
        }
        #endregion

        #region Decrypt
        /// <summary>
        /// Decrypts a string.
        /// </summary>
        /// <param name="cipherString">the string to encrypt</param>
        /// <returns>the decrypted string</returns>
        public string Decrypt(string cipherString)
        {
            if (String.IsNullOrWhiteSpace(cipherString))
            { return null; }

            // ensure that we have the private key            
            if (String.IsNullOrWhiteSpace(this._PrivateKey))
            { throw new AsentiaException(_GlobalResources.EncryptionKeyIsNotSet); }

            // convert the cipher string to a byte array
            byte[] cipherBytes = Convert.FromBase64String(cipherString);

            // BEGIN DECRYPTION PROCESS

            SymmetricAlgorithm aesManager = new AesCryptoServiceProvider();
            SHA256 sha256 = SHA256.Create();

            // use hashed private key information as the crypto key
            aesManager.KeySize = 256;
            aesManager.Key = sha256.ComputeHash(Encoding.UTF8.GetBytes(this._PrivateKey));

            // set the cipher and padding modes
            aesManager.Mode = CipherMode.CBC;
            aesManager.Padding = PaddingMode.PKCS7;

            // set the initialization vector from the first bytes of the cipher byte array
            byte[] iv = new byte[16];
            Array.Copy(cipherBytes, 0, iv, 0, iv.Length);
            aesManager.IV = iv;

            // create the decryptor
            ICryptoTransform decryptor = aesManager.CreateDecryptor(aesManager.Key, aesManager.IV);

            // do the decryption
            byte[] decryptedBytes;

            using (MemoryStream msDecrypt = new MemoryStream())
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Write))
                {
                    using (BinaryWriter bwDecrypt = new BinaryWriter(csDecrypt))
                    {
                        bwDecrypt.Write(cipherBytes, iv.Length, cipherBytes.Length - iv.Length);
                    }
                }

                decryptedBytes = msDecrypt.ToArray();
            }

            // return decrypted string
            return Encoding.UTF8.GetString(decryptedBytes);
        }
        #endregion
        #endregion

        #region Private Methods
        #region _GetPrivateKey
        /// <summary>
        /// Decrypts the public key that's stored in the web.config to get the "real key" (private key)
        /// that we will use for encryption. 
        /// </summary>
        /// <returns>string cotaining the private key</returns>
        public string _GetPrivateKey()
        {
            string publicKey = Config.ApplicationSettings.EARCryptoKey;

            if (String.IsNullOrWhiteSpace(publicKey))
            { throw new AsentiaException(_GlobalResources.CouldNotObtainPublicKey); }

            // get system specific information, and make sure we got it (otherwise die)
            string systemSpecificInformation = this._GetSystemSpecificInformation();

            if (String.IsNullOrWhiteSpace(systemSpecificInformation))
            { throw new AsentiaException(_GlobalResources.UnableToRetrieveSystemSpecificInformationForPublicKeyDecryption); }

            // convert the cipher string to a byte array
            byte[] publicKeyBytes = Convert.FromBase64String(publicKey);

            // BEGIN PUBLIC KEY DECRYPTION PROCESS

            SymmetricAlgorithm aesManager = new AesCryptoServiceProvider();
            SHA256 sha256 = SHA256.Create();

            // use hashed system information as the crypto key
            aesManager.KeySize = 256;
            aesManager.Key = sha256.ComputeHash(Encoding.UTF8.GetBytes(systemSpecificInformation));

            // set the cipher and padding modes
            aesManager.Mode = CipherMode.CBC;
            aesManager.Padding = PaddingMode.PKCS7;

            // set the initialization vector from the first bytes of the cipher byte array
            byte[] iv = new byte[16];
            Array.Copy(publicKeyBytes, 0, iv, 0, iv.Length);
            aesManager.IV = iv;

            // create the decryptor
            ICryptoTransform decryptor = aesManager.CreateDecryptor(aesManager.Key, aesManager.IV);

            // do the decryption
            byte[] decryptedBytes;

            using (MemoryStream msDecrypt = new MemoryStream())
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Write))
                {
                    using (BinaryWriter bwDecrypt = new BinaryWriter(csDecrypt))
                    {
                        bwDecrypt.Write(publicKeyBytes, iv.Length, publicKeyBytes.Length - iv.Length);
                    }
                }

                decryptedBytes = msDecrypt.ToArray();
            }

            // return decrypted string (private key)
            return Encoding.UTF8.GetString(decryptedBytes);
        }
        #endregion

        #region _GetSystemSpecificInformation
        /// <summary>
        /// Gets the system's HDD serial number and OS serial number.
        /// </summary>
        /// <returns>string of HDD serial + OS serial</returns>
        private string _GetSystemSpecificInformation()
        {
            string hddSerialNumber = null;
            string osSerialNumber = null;

            // get disk serial number (1st drive)            
            ManagementObjectSearcher diskObjectSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");

            foreach (ManagementObject wmi_HD in diskObjectSearcher.Get())
            {
                // get only the first drive, then break
                if (wmi_HD["SerialNumber"] != null)
                {
                    hddSerialNumber = wmi_HD["SerialNumber"].ToString();
                    break;
                }
            }

            // get the os serial number
            ManagementClass osManager = new ManagementClass("Win32_OperatingSystem");
            ManagementObjectCollection osObjectCollection = osManager.GetInstances();

            foreach (ManagementObject osObject in osObjectCollection)
            {
                if (!String.IsNullOrWhiteSpace(osObject["SerialNumber"].ToString()))
                {
                    osSerialNumber = osObject["SerialNumber"].ToString();
                    break;
                }

            }

            // if we were able to grab BOTH serial numbers, put them together and return; otherwise, return null
            if (!String.IsNullOrWhiteSpace(hddSerialNumber) && !String.IsNullOrWhiteSpace(osSerialNumber))
            { return hddSerialNumber + osSerialNumber; }
            else
            { return null; }
        }
        #endregion
        #endregion
    }
}
