﻿using System;
using System.Net.Mail;

namespace Asentia.Common
{
    public class PasswordResetEmail : EmailTemplate
    {
        #region Properties
        public string UserPassword = String.Empty;
        public string SystemFromAddressOverride = String.Empty;
        public string SystemSmtpServerNameOverride = String.Empty;
        public int SystemSmtpServerPortOverride = 0;
        public bool SystemSmtpServerUseSslOverride = false;
        public string SystemSmtpServerUsernameOverride = String.Empty;
        public string SystemSmtpServerPasswordOverride = String.Empty;
        #endregion

        #region Constructors
        public PasswordResetEmail(string language, string systemFromAddressOverride, string systemSmtpServerNameOverride, int systemSmtpServerPortOverride, bool systemSmtpServerUseSslOverride, string systemSmtpServerUsernameOverride, string systemSmtpServerPasswordOverride)
        {
            // path to the email notification's XML file
            string templateFilePath = SitePathConstants.DEFAULT_SITE_EMAILNOTIFICATIONS_ROOT + "PasswordReset.xml";

            // set the language
            this.Language = language;

            // set the system from address override
            if (!String.IsNullOrWhiteSpace(systemFromAddressOverride))
            { this.SystemFromAddressOverride = systemFromAddressOverride; }
            else
            { this.SystemFromAddressOverride = null; }

            // set the system smtp server name override
            if (!String.IsNullOrWhiteSpace(systemSmtpServerNameOverride))
            { this.SystemSmtpServerNameOverride = systemSmtpServerNameOverride; }
            else
            { this.SystemSmtpServerNameOverride = null; }

            // set the system smtp server port override
            this.SystemSmtpServerPortOverride = systemSmtpServerPortOverride;

            // set the system smtp server use ssl override
            this.SystemSmtpServerUseSslOverride = systemSmtpServerUseSslOverride;

            // set the system smtp server username override
            if (!String.IsNullOrWhiteSpace(systemSmtpServerUsernameOverride))
            { this.SystemSmtpServerUsernameOverride = systemSmtpServerUsernameOverride; }
            else
            { this.SystemSmtpServerUsernameOverride = null; }

            // set the system smtp server password override
            if (!String.IsNullOrWhiteSpace(systemSmtpServerPasswordOverride))
            { this.SystemSmtpServerPasswordOverride = systemSmtpServerPasswordOverride; }
            else
            { this.SystemSmtpServerPasswordOverride = null; }

            // load the message body
            this._LoadMessageBody(templateFilePath);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sends the email.
        /// </summary>
        public override void Send()
        {
            // replace specific placeholders
            this._ReplacePlaceholder("user_password", this.UserPassword);

            try
            {
                // send the email
                this._SendEmail(this.RecipientEmail,
                                null,
                                null,
                                null,
                                null,
                                MailPriority.High,
                                false,
                                this.SystemFromAddressOverride,
                                this.SystemSmtpServerNameOverride,
                                this.SystemSmtpServerPortOverride,
                                this.SystemSmtpServerUseSslOverride,
                                this.SystemSmtpServerUsernameOverride,
                                this.SystemSmtpServerPasswordOverride);
            }
            catch
            { throw; }
        }
        #endregion
    }
}
