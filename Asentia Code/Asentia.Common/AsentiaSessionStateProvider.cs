﻿using System;
using System.Web;
using System.Web.Configuration;
using System.Configuration;
using System.Collections.Specialized;
using System.Web.SessionState;
using System.Data;
using System.IO;

namespace Asentia.Common
{
    public sealed class AsentiaSessionStateProvider : SessionStateStoreProviderBase
    {
        #region Properties
        public string Application = null;
        #endregion

        #region Private Properties
        private SessionStateSection _Config = null;
        #endregion

        #region Private Methods
        #region _GetSessionStoreItem
        // GetSessionStoreItem is called by both the GetItem and 
        // GetItemExclusive methods. GetSessionStoreItem retrieves the 
        // session data from the data source. If the lockRecord parameter
        // is true (in the case of GetItemExclusive), then GetSessionStoreItem
        // locks the record and sets a new LockId and LockDate.
        private SessionStateStoreData _GetSessionStoreItem(bool lockRecord, HttpContext context, string sessionId, out bool isLocked, out TimeSpan lockAge, out object lockId, out SessionStateActions actionFlags)
        {
            // Initial values for return value and out parameters.
            SessionStateStoreData sessionStateData = null;
            lockAge = TimeSpan.Zero;
            lockId = null;
            isLocked = false;
            actionFlags = 0;

            // String to hold serialized SessionStateItemCollection.
            string sessionStateItemsSerialized = String.Empty;

            // True if a record is found in the database.
            bool foundSessionStateRecord = false;

            // True if the returned session item is expired and needs to be deleted.
            bool deleteSessionStateRecord = false;

            // Timeout value from the data store.
            int sessionTimeout = 0;

            // Variable to store the UTC "NOW" we retrieve from the database.
            DateTime utcNow;

            AsentiaDatabase DatabaseObject = new AsentiaDatabase(DatabaseType.SessionState);
            DBReturnValue dbReturnCode;

            try
            {
                // lockRecord is true when called from GetItemExclusive and
                // false when called from GetItem.
                // Obtain a lock if possible. Ignore the record if it is expired.
                if (lockRecord)
                {
                    DatabaseObject.ClearParameters();
                    DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

                    DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);

                    DatabaseObject.ExecuteNonQuery("[SessionState.Lock]", true);

                    dbReturnCode = (DBReturnValue)Convert.ToInt32(DatabaseObject.Command.Parameters["@Return_Code"].Value);

                    if (dbReturnCode != DBReturnValue.OK)
                    { isLocked = true; }
                    else
                    { isLocked = false; }
                }

                // Retrieve the current session item information.
                DatabaseObject.ClearParameters();
                DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

                DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);
                DatabaseObject.AddParameter("@isExpired", null, SqlDbType.Bit, 1, ParameterDirection.Output);
                DatabaseObject.AddParameter("@sessionItems", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
                DatabaseObject.AddParameter("@isLocked", null, SqlDbType.Bit, 1, ParameterDirection.Output);
                DatabaseObject.AddParameter("@lockId", null, SqlDbType.Int, 4, ParameterDirection.Output);
                DatabaseObject.AddParameter("@dtLocked", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
                DatabaseObject.AddParameter("@utcNow", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
                DatabaseObject.AddParameter("@flags", null, SqlDbType.Int, 4, ParameterDirection.Output);
                DatabaseObject.AddParameter("@timeout", null, SqlDbType.Int, 4, ParameterDirection.Output);
                DatabaseObject.AddParameter("@application", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
                DatabaseObject.AddParameter("@dtExpires", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

                DatabaseObject.ExecuteNonQuery("[SessionState.Get]", true);

                dbReturnCode = (DBReturnValue)Convert.ToInt32(DatabaseObject.Command.Parameters["@Return_Code"].Value);

                DateTime dtExpires = DateTime.UtcNow;

                if (dbReturnCode == DBReturnValue.OK)
                {
                    dtExpires = Convert.ToDateTime(DatabaseObject.Command.Parameters["@dtExpires"].Value);
                    this.Application = DatabaseObject.Command.Parameters["@application"].Value.ToString();

                    if (Convert.ToBoolean(DatabaseObject.Command.Parameters["@isExpired"].Value))
                    {
                        // The record was expired. Mark it as not locked.
                        isLocked = false;
                        // The session was expired. Mark the data for deletion.
                        deleteSessionStateRecord = true;
                    }
                    else
                    { foundSessionStateRecord = true; }

                    sessionStateItemsSerialized = DatabaseObject.Command.Parameters["@sessionItems"].Value.ToString();
                    lockId = Convert.ToInt32(DatabaseObject.Command.Parameters["@lockId"].Value);
                    
                    // we use the database for UTC "NOW" as opposed to code (DateTime.UtcNow) so that all of our
                    // session state date/times, and ages stay uniform throughout
                    utcNow = Convert.ToDateTime(DatabaseObject.Command.Parameters["@utcNow"].Value);
                    lockAge = utcNow.Subtract(Convert.ToDateTime(DatabaseObject.Command.Parameters["@dtLocked"].Value));
                    
                    actionFlags = (SessionStateActions)Convert.ToInt32(DatabaseObject.Command.Parameters["@flags"].Value);
                    sessionTimeout = Convert.ToInt32(DatabaseObject.Command.Parameters["@timeout"].Value);

                    // If it's a read-only session check if the session is currently locked
                    if (!lockRecord)
                    { isLocked = Convert.ToBoolean(DatabaseObject.Command.Parameters["@isLocked"].Value); }
                }

                // If the returned session item is expired, 
                // delete the record from the data source.
                if (deleteSessionStateRecord)
                {
                    DatabaseObject.ClearParameters();
                    DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

                    DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);

                    DatabaseObject.ExecuteNonQuery("[SessionState.Remove]", true);
                }

                // The record was not found. Ensure that locked is false.
                if (!foundSessionStateRecord)
                { isLocked = false; }

                // If the record was found and you obtained a lock, then set 
                // the lockId, clear the actionFlags,
                // and create the SessionStateStoreItem to return.
                if (foundSessionStateRecord && !isLocked)
                {
                    lockId = (int)lockId + 1;

                    DatabaseObject.ClearParameters();
                    DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

                    DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);
                    DatabaseObject.AddParameter("@lockId", lockId, SqlDbType.Int, 4, ParameterDirection.Input);

                    DatabaseObject.ExecuteNonQuery("[SessionState.SetLockId]", true);

                    // If the actionFlags parameter is not InitializeItem, 
                    // deserialize the stored SessionStateItemCollection.
                    if (actionFlags == SessionStateActions.InitializeItem)
                    { sessionStateData = CreateNewStoreData(context, _Config.Timeout.Minutes); }
                    else
                    { sessionStateData = _Deserialize(context, sessionStateItemsSerialized, sessionTimeout, dtExpires); }
                }

                return sessionStateData;
            }
            catch
            {
                throw;
            }
            finally
            {
                DatabaseObject.Dispose();
            }
        }
        #endregion

        #region _Serialize
        // Serialize is called by the SetAndReleaseItemExclusive method to 
        // convert the SessionStateItemCollection into a Base64 string to    
        // be stored in an Access Memo field.
        private string _Serialize(SessionStateItemCollection sessionStateItems)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(ms);

            if (sessionStateItems != null)
            { sessionStateItems.Serialize(writer); }

            writer.Close();

            return Convert.ToBase64String(ms.ToArray());
        }
        #endregion

        #region _Deserialize
        // DeSerialize is called by the GetSessionStoreItem method to 
        // convert the Base64 string stored in the Access Memo field to a 
        // SessionStateItemCollection.
        private SessionStateStoreData _Deserialize(HttpContext context, string serializedSessionStateItems, int sessionTimeout, DateTime dtExpires)
        {
            MemoryStream ms = new MemoryStream(Convert.FromBase64String(serializedSessionStateItems));

            SessionStateItemCollection sessionStateItems = new SessionStateItemCollection();

            if (ms.Length > 0)
            {
                BinaryReader reader = new BinaryReader(ms);
                sessionStateItems = SessionStateItemCollection.Deserialize(reader);
            }
            sessionStateItems["DtExpires"] = dtExpires;
            return new SessionStateStoreData(sessionStateItems, SessionStateUtility.GetSessionStaticObjects(context), sessionTimeout);
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region Initialize
        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
            { throw new ArgumentNullException("config"); }

            if (name == null || name.Length == 0)
            { name = "AsentiaSessionStateProvider"; }

            if (this.Application == null || this.Application.Length == 0)
            { this.Application = Config.ApplicationSettings.ApplicationName; }

            // Initialize the abstract base class.
            base.Initialize(name, config);

            // Get <sessionState> configuration element.
            _Config = (SessionStateSection)(ConfigurationManager.GetSection("system.web/sessionState"));
        }
        #endregion

        #region Dispose
        // SessionStateStoreProviderBase members
        public override void Dispose()
        { }
        #endregion

        #region SetItemExpireCallback
        // SessionStateProviderBase.SetItemExpireCallback
        public override bool SetItemExpireCallback(SessionStateItemExpireCallback expireCallback)
        {
            return false; 
        }
        #endregion

        #region SetAndReleaseItemExclusive
        // SessionStateProviderBase.SetAndReleaseItemExclusive
        public override void SetAndReleaseItemExclusive(HttpContext context, string sessionId, SessionStateStoreData sessionStateData, object lockId, bool isNewSession)                                           
        {
            // Serialize the SessionStateItemCollection as a string.
            string sessionStateItemsSerialized = _Serialize((SessionStateItemCollection)sessionStateData.Items);

            AsentiaDatabase DatabaseObject = new AsentiaDatabase(DatabaseType.SessionState);

            try
            {
                if (isNewSession)
                {

                    DatabaseObject.ClearParameters();
                    DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

                    DatabaseObject.ExecuteNonQuery("[SessionState.RemoveExpired]", true);

                    DatabaseObject.ClearParameters();
                    DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

                    DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);
                    DatabaseObject.AddParameter("@timeout", sessionStateData.Timeout, SqlDbType.Int, 4, ParameterDirection.Input);
                    DatabaseObject.AddParameter("@sessionItems", sessionStateItemsSerialized, SqlDbType.NVarChar, -1, ParameterDirection.Input);
                    DatabaseObject.AddParameter("@flags", 0, SqlDbType.Int, 4, ParameterDirection.Input);
                    DatabaseObject.AddParameter("@application", this.Application, SqlDbType.NVarChar, 255, ParameterDirection.Input);

                    DatabaseObject.ExecuteNonQuery("[SessionState.Initialize]", true);
                }
                else
                {
                    DatabaseObject.ClearParameters();
                    DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

                    DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);
                    DatabaseObject.AddParameter("@lockId", lockId, SqlDbType.Int, 4, ParameterDirection.Input);
                    DatabaseObject.AddParameter("@timeout", sessionStateData.Timeout, SqlDbType.Int, 4, ParameterDirection.Input);
                    DatabaseObject.AddParameter("@isLocked", false, SqlDbType.Bit, 1, ParameterDirection.Input);
                    DatabaseObject.AddParameter("@sessionItems", sessionStateItemsSerialized, SqlDbType.NVarChar, -1, ParameterDirection.Input);

                    DatabaseObject.ExecuteNonQuery("[SessionState.Update]", true);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DatabaseObject.Dispose();
            }
        }
        #endregion

        #region GetItem
        // SessionStateProviderBase.GetItem
        public override SessionStateStoreData GetItem(HttpContext context, string sessionId, out bool isLocked, out TimeSpan lockAge, out object lockId, out SessionStateActions actionFlags)
        {
            return _GetSessionStoreItem(false, context, sessionId, out isLocked, out lockAge, out lockId, out actionFlags);
        }
        #endregion

        #region GetItemExclusive
        // SessionStateProviderBase.GetItemExclusive
        public override SessionStateStoreData GetItemExclusive(HttpContext context, string sessionId, out bool isLocked, out TimeSpan lockAge, out object lockId, out SessionStateActions actionFlags)
        {
            return _GetSessionStoreItem(true, context, sessionId, out isLocked, out lockAge, out lockId, out actionFlags);
        }
        #endregion

        #region ReleaseItemExclusive
        // SessionStateProviderBase.ReleaseItemExclusive
        public override void ReleaseItemExclusive(HttpContext context, string sessionId, object lockId)
        {
            AsentiaDatabase DatabaseObject = new AsentiaDatabase(DatabaseType.SessionState);

            DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

            DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);
            DatabaseObject.AddParameter("@lockId", lockId, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                DatabaseObject.ExecuteNonQuery("[SessionState.ReleaseLock]", true);
            }
            catch
            {
                throw;
            }
            finally
            {
                DatabaseObject.Dispose();
            }
        }
        #endregion

        #region RemoveItem
        // SessionStateProviderBase.RemoveItem
        public override void RemoveItem(HttpContext context, string sessionId, object lockId, SessionStateStoreData sessionStateData)
        {
            AsentiaDatabase DatabaseObject = new AsentiaDatabase(DatabaseType.SessionState);

            DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

            DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);
            DatabaseObject.AddParameter("@lockId", lockId, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                DatabaseObject.ExecuteNonQuery("[SessionState.RemoveByLock]", true);
            }
            catch
            {
                throw;
            }
            finally
            {
                DatabaseObject.Dispose();
            }
        }
        #endregion

        #region CreateUninitializedItem
        // SessionStateProviderBase.CreateUninitializedItem
        public override void CreateUninitializedItem(HttpContext context, string sessionId, int sessionTimeout)
        {
            AsentiaDatabase DatabaseObject = new AsentiaDatabase(DatabaseType.SessionState);

            DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

            DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);
            DatabaseObject.AddParameter("@timeout", sessionTimeout, SqlDbType.Int, 4, ParameterDirection.Input);
            DatabaseObject.AddParameter("@sessionItems", String.Empty, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            DatabaseObject.AddParameter("@flags", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            DatabaseObject.AddParameter("@application", this.Application, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                DatabaseObject.ExecuteNonQuery("[SessionState.Initialize]", true);
            }
            catch
            {
                throw;
            }
            finally
            {
                DatabaseObject.Dispose();
            }
        }
        #endregion

        #region CreateNewStoreData
        // SessionStateProviderBase.CreateNewStoreData
        public override SessionStateStoreData CreateNewStoreData(HttpContext context, int sessionTimeout)
        {
            return new SessionStateStoreData(new SessionStateItemCollection(), SessionStateUtility.GetSessionStaticObjects(context), sessionTimeout);
        }
        #endregion

        #region ResetItemTimeout
        // SessionStateProviderBase.ResetItemTimeout
        public override void ResetItemTimeout(HttpContext context, string sessionId)
        {
            // When .NET loads .axd resources, the System.Web.Handlers.AssemblyResourceLoader
            // handler it uses likes to call ResetItemTimeout. This is unnecessary because
            // 1) it creates an unnecessary call to the database, and 2) .axd resource loading
            // takes place before Session is loaded into the context, and that results in an 
            // exception on the timeout parameter which then causes the resource not to be loaded. 
            // So, if the handler is System.Web.Handlers.AssemblyResourceLoader, skip the database call.
            if (
                context.CurrentHandler.ToString() == "System.Web.Handlers.AssemblyResourceLoader" ||
                context.CurrentHandler.ToString() == "System.Web.Handlers.ScriptResourceHandler"
               )
            { return; }
            else
            {
                AsentiaDatabase DatabaseObject = new AsentiaDatabase(DatabaseType.SessionState);

                try
                {
                    DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);

                    DatabaseObject.AddParameter("@sessionId", sessionId, SqlDbType.NVarChar, 80, ParameterDirection.Input);
                    DatabaseObject.AddParameter("@timeout", context.Session.Timeout, SqlDbType.Int, 4, ParameterDirection.Input); // throws exception when "context" not available

                    DatabaseObject.ExecuteNonQuery("[SessionState.ResetTimeout]", true);
                }
                catch
                {
                    // just swallow the exception as an exception 
                    // here will not affect session state
                }
                finally
                {
                    DatabaseObject.Dispose();
                }
            }
        }
        #endregion

        #region InitializeRequest
        // SessionStateProviderBase.InitializeRequest
        public override void InitializeRequest(HttpContext context)
        { }
        #endregion

        #region EndRequest
        // SessionStateProviderBase.EndRequest
        public override void EndRequest(HttpContext context)
        { }
        #endregion
        #endregion
    }
}
