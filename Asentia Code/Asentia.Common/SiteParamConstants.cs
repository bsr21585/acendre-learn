﻿
namespace Asentia.Common
{
    public static class SiteParamConstants
    {
        // hidden params - params that we (ICS) can set to control things, but are hidden from the interface
        public static readonly string SYSTEM_EMAIL_FROMADDRESSOVERRIDE = "System.EmailFromAddressOverride";         // this can override the default system from address in email notifications
        public static readonly string SYSTEM_EMAIL_SMTPSERVERNAMEOVERRIDE = "System.EmailSmtpServerNameOverride";      // this can override the default system SMTP server
        public static readonly string SYSTEM_EMAIL_SMTPSERVERPORTOVERRIDE = "System.EmailSmtpServerPortOverride";      // this can override the default system SMTP server port (must be specified if server name specified)
        public static readonly string SYSTEM_EMAIL_SMTPSERVERUSESSLOVERRIDE = "System.EmailSmtpServerUseSslOverride";    // this can override the default system SMTP server use ssl setting
        public static readonly string SYSTEM_EMAIL_SMTPSERVERUSERNAMEOVERRIDE = "System.EmailSmtpServerUsernameOverride";  // this can override the default system SMTP server username
        public static readonly string SYSTEM_EMAIL_SMTPSERVERPASSWORDOVERRIDE = "System.EmailSmtpServerPasswordOverride";  // this can override the default system SMTP server password

        // homepage masthead
        public static readonly string HOMEPAGE_MASTHEAD_LOGO = "HomePage.Masthead.Logo";
        public static readonly string HOMEPAGE_MASTHEAD_SECONDARYIMAGE = "HomePage.Masthead.SecondaryImage";
        public static readonly string HOMEPAGE_MASTHEAD_BGCOLOR = "HomePage.Masthead.BgColor";
        public static readonly string HOMEPAGE_MASTHEAD_BGIMAGE = "HomePage.Masthead.BgImage";

        // application masthead
        public static readonly string APPLICATION_MASTHEAD_ICON = "Application.Masthead.Icon";
        public static readonly string APPLICATION_MASTHEAD_LOGO = "Application.Masthead.Logo";
        public static readonly string APPLICATION_MASTHEAD_BGCOLOR = "Application.Masthead.BgColor";

        // footer
        public static readonly string FOOTER_COMPANYNAME = "Footer.Company";
        public static readonly string FOOTER_EMAIL = "Footer.Email";
        public static readonly string FOOTER_WEBSITE = "Footer.Website";

        // login settings
        public static readonly string SYSTEM_SELFREGISTRATION_ENABLED = "System.SelfRegistration.Enabled";
        public static readonly string SYSTEM_SELFREGISTRATION_TYPE = "System.SelfRegistration.Type";
        public static readonly string SYSTEM_SELFREGISTRATION_URLPREFIX = "System.SelfRegistration.UrlPrefix";
        public static readonly string SYSTEM_SELFREGISTRATION_URL = "System.SelfRegistration.Url";
        public static readonly string SYSTEM_PASSWORDRESET_ENABLED = "System.PasswordReset.Enabled";
        public static readonly string SYSTEM_DEFAULTLANDINGPAGE = "System.DefaultLandingPage";
        public static readonly string SYSTEM_DEFAULTLANDINGPAGE_LOCALPATH = "System.DefaultLandingPage.LocalPath";
        public static readonly string SYSTEM_SIMULTANEOUSLOGIN_ENABLED = "System.SimultaneousLogin.Enabled";
        public static readonly string SYSTEM_LOGINPRIORITY = "System.LoginPriority";
        public static readonly string SYSTEM_ACCOUNTREGISTRATIONAPPROVAL = "System.AccountRegistrationApproval";
        public static readonly string SYSTEM_LOGOUT_ENABLED = "System.Lockout.Enabled";
        public static readonly string SYSTEM_LOGOUT_MINUTES = "System.Lockout.Minutes";
        public static readonly string SYSTEM_LOGOUT_ATTEMPTS = "System.Lockout.Attempts";

        // dashboard widget settings
        public static readonly string WIDGET_LEARNER_PURCHASES_ENABLED = "Widget.Learner.Purchases.Enabled";
        public static readonly string WIDGET_LEARNER_CERTIFICATES_ENABLED = "Widget.Learner.Certificates.Enabled";
        public static readonly string WIDGET_LEARNER_CALENDAR_ENABLED = "Widget.Learner.Calendar.Enabled";
        public static readonly string WIDGET_LEARNER_FEED_ENABLED = "Widget.Learner.Feed.Enabled";
        public static readonly string WIDGET_LEARNER_TRANSCRIPT_ENABLED = "Widget.Learner.Transcript.Enabled";
        public static readonly string WIDGET_LEARNER_MYCOMMUNITIES_ENABLED = "Widget.Learner.MyCommunities.Enabled";
        public static readonly string WIDGET_LEARNER_MYLEARNINGPATHS_ENABLED = "Widget.Learner.MyLearningPaths.Enabled";
        public static readonly string WIDGET_LEARNER_LEADERBOARDS_ENABLED = "Widget.Learner.Leaderboards.Enabled";
        public static readonly string WIDGET_LEARNER_MYCERTIFICATIONS_ENABLED = "Widget.Learner.MyCertifications.Enabled";
        public static readonly string WIDGET_LEARNER_DOCUMENTS_ENABLED = "Widget.Learner.Documents.Enabled";
        public static readonly string WIDGET_LEARNER_ILTSESSIONS_ENABLED = "Widget.Learner.ILTSessions.Enabled";
        public static readonly string WIDGET_LEARNER_ENROLLMENTS_MODE = "Widget.Learner.Enrollments.Mode";

        public static readonly string WIDGET_ADMINISTRATOR_PORTALSTATISTICS_ENABLED = "Widget.Administrator.PortalStatistics.Enabled";
        public static readonly string WIDGET_ADMINISTRATOR_ENROLLMENTSTATISTICS_ENABLED = "Widget.Administrator.EnrollmentStatistics.Enabled";
        public static readonly string WIDGET_ADMINISTRATOR_LEADERBOARDS_ENABLED = "Widget.Administrator.Leaderboards.Enabled";
        public static readonly string WIDGET_ADMINISTRATOR_WALLMODERATION_ENABLED = "Widget.Administrator.WallModeration.Enabled";
        public static readonly string WIDGET_ADMINISTRATOR_TASKPROCTORING_ENABLED = "Widget.Administrator.TaskProctoring.Enabled";
        public static readonly string WIDGET_ADMINISTRATOR_OJTPROCTORING_ENABLED = "Widget.Administrator.OJTProctoring.Enabled";
        public static readonly string WIDGET_ADMINISTRATOR_ILTROSTERMANAGEMENT_ENABLED = "Widget.Administrator.ILTRosterManagement.Enabled";
        public static readonly string WIDGET_ADMINISTRATOR_PENDINGUSERREGISTRATIONS_ENABLED = "Widget.Administrator.PendingUserRegistrations.Enabled";
        public static readonly string WIDGET_ADMINISTRATOR_PENDINGCOURSEENROLLMENTS_ENABLED = "Widget.Administrator.PendingCourseEnrollments.Enabled";
        public static readonly string WIDGET_ADMINISTRATOR_CERTIFICATIONTASKPROCTORING_ENABLED = "Widget.Administrator.CertificationTaskProctoring.Enabled";
        public static readonly string WIDGET_ADMINISTRATOR_ADMINISTRATIVETASKS_ENABLED = "Widget.Administrator.AdministrativeTasks.Enabled";

        public static readonly string WIDGET_REPORTER_REPORTSUBSCRIPTIONS_ENABLED = "Widget.Reporter.ReportSubscription.Enabled";
        public static readonly string WIDGET_REPORTER_REPORTSHORTCUTS_ENABLED = "Widget.Reporter.ReportShortcuts.Enabled";

        // catalog settings
        public static readonly string CATALOG_SHOW_PRIVATECOURSECATALOGS = "Catalog.ShowPrivateCourseCatalogs";
        public static readonly string CATALOG_REQUIRE_LOGIN = "Catalog.RequireLogin";
        public static readonly string CATALOG_HIDE_COURSE_CODES = "Catalog.HideCourseCodes";
        public static readonly string CATALOG_ENABLE = "Catalog.Enable";
        public static readonly string CATALOG_SEARCH_ENABLE = "Catalog.Search.Enable";
        public static readonly string CATALOG_EVENTCALENDAR_ENABLE = "Catalog.EventCalendar.Enable";
        public static readonly string CATALOG_COMMUNITY_ENABLE = "Catalog.Communities.Enable";
        public static readonly string CATALOG_LEARNINGPATHS_ENABLE = "Catalog.LearningPaths.Enable";
        public static readonly string CATALOG_STANDUPTRAINING_ENABLE = "Catalog.StandupTraining.Enable";
        
        // api
        public static readonly string SYSTEM_API_ENABLED = "System.API.Enabled";
        public static readonly string SYSTEM_API_KEY = "System.API.Key";
        public static readonly string SYSTEM_API_REQUIREHTTPS = "System.API.RequireHTTPS";

        // google analytics - FIX
        public static readonly string SYSTEM_TRACKING_CODE = "System.TrackingCode";

        // e-commerce
        public static readonly string ECOMMERCE_ENABLED = "Ecommerce.Enabled";
        public static readonly string ECOMMERCE_PROCESSING_METHOD = "Ecommerce.ProcessingMethod"; // none, authorize, paypal
        public static readonly string ECOMMERCE_PROCESSING_METHOD_VERIFIED = "Ecommerce.ProcessingMethod.Verified";
        public static readonly string ECOMMERCE_CURRENCY = "Ecommerce.Currency";

        public static readonly string ECOMMERCE_AUTHORIZENET_MODE = "Ecommerce.AuthorizeNet.Mode"; // live, test, developer
        public static readonly string ECOMMERCE_AUTHORIZENET_LOGIN = "Ecommerce.AuthorizeNet.Login";
        public static readonly string ECOMMERCE_AUTHORIZENET_TRANSACTIONKEY = "Ecommerce.AuthorizeNet.TransactionKey";
        public static readonly string ECOMMERCE_AUTHORIZENET_PUBLICKEY = "Ecommerce.AuthorizeNet.PublicKey";

        public static readonly string ECOMMERCE_PAYPAL_MODE = "Ecommerce.PayPal.Mode"; // live, test
        public static readonly string ECOMMERCE_PAYPAL_LOGIN = "Ecommerce.PayPal.Login";

        public static readonly string ECOMMERCE_VISA_ENABLED = "Ecommerce.Visa.Enabled";
        public static readonly string ECOMMERCE_MASTERCARD_ENABLED = "Ecommerce.MasterCard.Enabled";
        public static readonly string ECOMMERCE_AMEX_ENABLED = "Ecommerce.AMEX.Enabled";
        public static readonly string ECOMMERCE_DISCOVERCARD_ENABLED = "Ecommerce.DiscoverCard.Enabled";

        public static readonly string ECOMMERCE_FIELD_NAME_REQUIRED = "Ecommerce.Field.Name.Required";
        public static readonly string ECOMMERCE_FIELD_CREDITCARDNUM_REQUIRED = "Ecommerce.Field.CreditCardNum.Required";
        public static readonly string ECOMMERCE_FIELD_EXPIRATIONDATE_REQUIRED = "Ecommerce.Field.ExpirationDate.Required";
        public static readonly string ECOMMERCE_FIELD_CVV2_REQUIRED = "Ecommerce.Field.CVV2.Required";        
        public static readonly string ECOMMERCE_FIELD_COMPANY_REQUIRED = "Ecommerce.Field.Company.Required";        
        public static readonly string ECOMMERCE_FIELD_ADDRESS_REQUIRED = "Ecommerce.Field.Address.Required";
        public static readonly string ECOMMERCE_FIELD_CITY_REQUIRED = "Ecommerce.Field.City.Required";
        public static readonly string ECOMMERCE_FIELD_STATEPROVINCE_REQUIRED = "Ecommerce.Field.StateProvince.Required";
        public static readonly string ECOMMERCE_FIELD_POSTALCODE_REQUIRED = "Ecommerce.Field.PostalCode.Required";        
        public static readonly string ECOMMERCE_FIELD_COUNTRY_REQUIRED = "Ecommerce.Field.Country.Required";
        public static readonly string ECOMMERCE_FIELD_EMAIL_REQUIRED = "Ecommerce.Field.Email.Required";
        
        public static readonly string ECOMMERCE_CHARGEDESCRIPTION = "Ecommerce.ChargeDescription";         
        public static readonly string ECOMMERCE_COUPONCODE_GRACEPERIOD = "Ecommerce.CouponCode.GracePeriod";

        // leaderboards
        public static readonly string LEADERBOARDS_COURSETOTAL_ENABLE = "Leaderboards.CourseTotal.Enable";
        public static readonly string LEADERBOARDS_COURSECREDITS_ENABLE = "Leaderboards.CourseCredits.Enable";
        public static readonly string LEADERBOARDS_CERTIFICATETOTAL_ENABLE = "Leaderboards.CertificateTotal.Enable";
        public static readonly string LEADERBOARDS_CERTIFICATECREDITS_ENABLE = "Leaderboards.CertificateCredits.Enable";

        // ratings
        public static readonly string RATINGS_COURSE_ENABLE = "Ratings.Course.Enable";
        public static readonly string RATINGS_COURSE_PUBLICIZE = "Ratings.Course.Publicize";

        // certifications
        public static readonly string CERTIFICATIONS_ENABLE = "Certifications.Enable";

        // quiz and survey
        public static readonly string QUIZZESANDSURVEYS_ENABLE = "QuizzesAndSurveys.Enable";

        // privacy
        public static readonly string PRIVACY_REQUIRECOOKIECONSENT = "Privacy.RequireCookieConsent";
        public static readonly string PRIVACY_ALLOWUSEREMAILOPTOUT = "Privacy.AllowUserEmailOptOut";
        public static readonly string PRIVACY_PERMANENTLYREMOVEDELETEDUSERS = "Privacy.PermanentlyRemoveDeletedUsers";
        public static readonly string PRIVACY_ALLOWSELFDELETION = "Privacy.AllowSelfDeletion";

        // user agreement
        public static readonly string USERAGREEMENT_DISPLAY = "UserAgreement.Display";
        public static readonly string USERAGREEMENT_REQUIRED = "UserAgreement.Required";

        // webmeeting integration
        public static readonly string WEBMEETING_GTM_ENABLE = "WebMeeting.GTM.Enable";
        public static readonly string WEBMEETING_GTM_ON = "WebMeeting.GTM.On";
        public static readonly string WEBMEETING_GTM_APPLICATION = "WebMeeting.GTM.Application";
        public static readonly string WEBMEETING_GTM_USERNAME = "WebMeeting.GTM.Username";
        public static readonly string WEBMEETING_GTM_PASSWORD = "WebMeeting.GTM.Password";
        public static readonly string WEBMEETING_GTM_CONSUMERSECRET = "WebMeeting.GTM.ConsumerSecret";
        public static readonly string WEBMEETING_GTM_PLAN = "WebMeeting.GTM.Plan";
        public static readonly string WEBMEETING_GTW_ENABLE = "WebMeeting.GTW.Enable";
        public static readonly string WEBMEETING_GTW_ON = "WebMeeting.GTW.On";
        public static readonly string WEBMEETING_GTW_APPLICATION = "WebMeeting.GTW.Application";
        public static readonly string WEBMEETING_GTW_USERNAME = "WebMeeting.GTW.Username";
        public static readonly string WEBMEETING_GTW_PASSWORD = "WebMeeting.GTW.Password";
        public static readonly string WEBMEETING_GTW_CONSUMERSECRET = "WebMeeting.GTW.ConsumerSecret";
        public static readonly string WEBMEETING_GTW_PLAN = "WebMeeting.GTW.Plan";
        public static readonly string WEBMEETING_GTT_ENABLE = "WebMeeting.GTT.Enable";
        public static readonly string WEBMEETING_GTT_ON = "WebMeeting.GTT.On";
        public static readonly string WEBMEETING_GTT_APPLICATION = "WebMeeting.GTT.Application";
        public static readonly string WEBMEETING_GTT_USERNAME = "WebMeeting.GTT.Username";
        public static readonly string WEBMEETING_GTT_PASSWORD = "WebMeeting.GTT.Password";
        public static readonly string WEBMEETING_GTT_CONSUMERSECRET = "WebMeeting.GTT.ConsumerSecret";
        public static readonly string WEBMEETING_GTT_PLAN = "WebMeeting.GTT.Plan";
        public static readonly string WEBMEETING_WEBEX_ENABLE = "WebMeeting.WebEx.Enable";
        public static readonly string WEBMEETING_WEBEX_ON = "WebMeeting.WebEx.On";
        public static readonly string WEBMEETING_WEBEX_APPLICATION = "WebMeeting.WebEx.Application";
        public static readonly string WEBMEETING_WEBEX_USERNAME = "WebMeeting.WebEx.Username";
        public static readonly string WEBMEETING_WEBEX_PASSWORD = "WebMeeting.WebEx.Password";
        public static readonly string WEBMEETING_WEBEX_PLAN = "WebMeeting.WebEx.Plan";

        // OpenSesame

        // OpenSesame IntegrationName, IntegrationKey, and IntegrationSecret are set for ICS's OpenSesame reseller account by system default. They're done 
	    // as site parameters so that we could set client or even portal specific integrations to the client's reseller account if we wanted to (if they paid 
	    // for it). Generally, there should never be portal-specific site params for these, unless we're using the client's OpenSesame reseller account.
        public static readonly string OPENSESAME_INTEGRATIONNAME = "OpenSesame.IntegrationName";
        public static readonly string OPENSESAME_INTEGRATIONKEY = "OpenSesame.IntegrationKey";
        public static readonly string OPENSESAME_INTEGRATIONSECRET = "OpenSesame.IntegrationSecret";

        // These are the portal-specific OpenSesame params.
        public static readonly string OPENSESAME_ENABLE = "OpenSesame.Enable";
        public static readonly string OPENSESAME_TENANTID = "OpenSesame.TenantID";
        public static readonly string OPENSESAME_TENANTCONSUMERKEY = "OpenSesame.TenantConsumerKey";
        public static readonly string OPENSESAME_TENANTCONSUMERSECRET = "OpenSesame.TenantConsumerSecret";

        // Themes and Iconsets
        public static readonly string ICONSET_ACTIVE = "Iconset.Active";
        public static readonly string ICONSET_PREVIEW = "Iconset.Preview";
        public static readonly string THEME_ACTIVE = "Theme.Active";
        public static readonly string THEME_PREVIEW = "Theme.Preview";

        // Features

        // xAPI Endpoints
        public static readonly string XAPIENDPOINTS_ENABLE = "Feature.System.xAPIEndpoints.Enabled";

        // User Account Merging
        public static readonly string USERACCOUNTMERGING_ENABLE = "Feature.System.UserAccountMerging.Enabled";

        // User Profile File Attachments
        public static readonly string USERPROFILEFILEATTACHMENTS_ENABLE = "Feature.System.UserProfileFiles.Enabled";

        // Lite User Field Configuration
        public static readonly string LITEUSERFIELDCONFIGURATION_ENABLE = "Feature.System.LiteUserFieldConfiguration.Enabled";

        // User Registration Approval
        public static readonly string USERREGISTRATIONAPPROVAL_ENABLE = "Feature.System.UserRegistrationApproval.Enabled"; 

        // Image Editor
        public static readonly string IMAGE_EDITOR_ENABLE = "Feature.Interface.ImageEditor.Enabled";

        // CSS Editor
        public static readonly string CSSEDITOR_ENABLE = "Feature.Interface.CSSEditor.Enabled";

        // Self-Enrollment Approval
        public static readonly string SELFENROLLMENTAPPROVAL_ENABLE = "Feature.LearningAssets.SelfEnrollmentApproval.Enabled";

        // Task Modules
        public static readonly string TASKMODULES_ENABLE = "Feature.LearningAssets.TaskModules.Enabled";

        // OJT Modules
        public static readonly string OJTMODULES_ENABLE = "Feature.LearningAssets.OJTModules.Enabled";

        // Learning Paths
        public static readonly string LEARNINGPATHS_ENABLE = "Feature.LearningAssets.LearningPaths.Enabled";

        // ILT Resources
        public static readonly string ILTRESOURCES_ENABLE = "Feature.LearningAssets.ILTResources.Enabled";

        // Reporting PDF Export
        public static readonly string REPORTINGPDFEXPORT_ENABLE = "Feature.Reporting.PDFExport.Enabled";

        // Message Center
        public static readonly string MESSAGECENTER_ENABLE = "Feature.MessageCenter.Enabled";

        // Object Specific Email Notifications
        public static readonly string OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE = "Feature.System.ObjectSpecificEmailNotifications.Enabled";

        // Roles Rules
        public static readonly string ROLESRULES_ENABLE = "Feature.Roles.Rules.Enabled";

        // Communities
        public static readonly string COMMUNITIES_ENABLE = "Feature.Groups.CommunityFunctions.Enabled";

        // Dashboard Mode
        public static readonly string FEATURE_DASHBOARDMODE = "Feature.DashboardMode";

        // Course Discussion Boards
        public static readonly string LEARNINGASSETS_COURSEDISCUSSION_ENABLE = "Feature.LearningAssets.CourseDiscussion.Enabled";

        // Group Documents
        public static readonly string GROUPS_DOCUMENTS_ENABLE = "Feature.Groups.Documents.Enabled";

        // Group Discussion Boards
        public static readonly string GROUPS_DISCUSSION_ENABLE = "Feature.Groups.Discussion.Enabled";

        // Standalone Enrollment of ILT
        public static readonly string STANDALONEENROLLMENT_ENABLE = "Feature.LearningAssets.StandaloneILTEnrollment.Enabled";

        // Restricted Rule Criteria
        public static readonly string RESTRICTEDRULECRITERIA_ENABLE = "Feature.LearningAssets.RestrictedRuleCriteria.Enabled";
    }
}