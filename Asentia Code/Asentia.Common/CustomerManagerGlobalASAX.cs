﻿using System;

namespace Asentia.Common
{
    public class CustomerManagerGlobalASAX : System.Web.HttpApplication
    {
        #region Application_Start
        protected void Application_Start(object sender, EventArgs e)
        { ;}
        #endregion

        #region Session_Start
        protected void Session_Start(object sender, EventArgs e)
        {
            // Set the session timeout based on the value set in the web.config for the account.
            AsentiaSessionState.SessionTimeout = Config.AccountSettings.SessionTimeout;
        }
        #endregion

        #region Application_BeginRequest
        protected void Application_BeginRequest(object sender, EventArgs e)
        { ;}
        #endregion

        #region Application_AuthenticateRequest
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        { ;}
        #endregion

        #region Application_Error
        protected void Application_Error(object sender, EventArgs e)
        { ;}
        #endregion

        #region Session_End
        protected void Session_End(object sender, EventArgs e)
        { ;}
        #endregion

        #region Application_End
        protected void Application_End(object sender, EventArgs e)
        { ;}
        #endregion
    }
}
