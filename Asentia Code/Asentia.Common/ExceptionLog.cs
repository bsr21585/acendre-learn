﻿using System;
using System.Data;

namespace Asentia.Common
{
    public class ExceptionLog
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ExceptionLog()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="ExceptionLog.IdExceptionLog" /> are not found.
        /// </exception>
        /// <param name="idExceptionLog">Exception Log Id</param>
        public ExceptionLog(int idExceptionLog)
        {
            this._Details(idExceptionLog);
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="ExceptionLog.IdExceptionLog" /> are not found.
        /// </exception>
        /// <param name="idExceptionLog">Exception Log Id</param>
        /// <param name="idCaller">The User Id of the user calling this method.</param>
        public ExceptionLog(int idExceptionLog, int idCaller)
        {
            _Details(idExceptionLog, idCaller);
        }
        #endregion

        #region Properties
        public static readonly string GridProcedure = "[ExceptionLog.GetGrid]";

        /// <summary>
        /// Exception Log Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        public int? IdSite;

        /// <summary>
        /// User Id of the user who's request caused the exception.
        /// </summary>
        /// <seealso cref="Site" />
        public int? IdUser;

        /// <summary>
        /// Timestamp of when the exception occurred.
        /// </summary>
        public DateTime Timestamp;

        /// <summary>
        /// The page that was requested when the exception occurred.
        /// </summary>
        public string Page;

        /// <summary>
        /// Exception Type.
        /// </summary>
        public string ExceptionType;

        /// <summary>
        /// Exception Message
        /// </summary>
        public string ExceptionMessage;

        /// <summary>
        /// Exception Stack Trace.
        /// </summary>
        public string ExceptionStackTrace;
        #endregion

        #region Methods
        #region Add
        /// <summary>
        /// Adds an exception record to the Exception Log.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="User.idUser" /> are not found.
        /// </exception>
        /// <param name="idSite">The Id of the site the exception was thrown from.</param>
        /// <param name="idUser">The Id of the user who's request threw the exception.</param>
        /// <param name="page">The page that was requested when the exception was thrown.</param>
        /// <param name="exceptionType">The type of exception.</param>
        /// <param name="exceptionMessage">The exception's message.</param>
        /// <param name="exceptionStackTrace">The stack trace of the exception.</param>
        public static int Add(int? idSite, int? idUser, string page, string exceptionType, string exceptionMessage, string exceptionStackTrace)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idExceptionLog", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@page", page, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@exceptionType", exceptionType, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@exceptionMessage", exceptionMessage, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@exceptionStackTrace", exceptionStackTrace, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[ExceptionLog.Add]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return Convert.ToInt32(databaseObject.Command.Parameters["@idExceptionLog"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="ExceptionLog.IdExceptionLog" /> are not found.
        /// </exception>
        /// <param name="idExceptionLog">Exception Log Id</param>
        private void _Details(int idExceptionLog)
        {
            _Details(idExceptionLog, 0);
        }

        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="ExceptionLog.IdExceptionLog" /> are not found.
        /// </exception>
        /// <param name="idExceptionLog">Exception Log Id</param>
        /// <param name="idCaller">The User Id of the user calling this method.</param>
        private void _Details(int idExceptionLog, int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idExceptionLog", idExceptionLog, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@timestamp", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@page", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@exceptionType", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@exceptionMessage", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@exceptionStackTrace", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[ExceptionLog.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idExceptionLog;
                this.IdSite = databaseObject.Command.Parameters["@idSite"].Value == DBNull.Value ? (int?)null : Convert.ToInt32(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdUser = databaseObject.Command.Parameters["@idUser"].Value == DBNull.Value ? (int?)null : Convert.ToInt32(databaseObject.Command.Parameters["@idUser"].Value);
                this.Timestamp = Convert.ToDateTime(databaseObject.Command.Parameters["@timestamp"].Value);
                this.Page = databaseObject.Command.Parameters["@page"].Value.ToString();
                this.ExceptionType = databaseObject.Command.Parameters["@exceptionType"].Value.ToString();
                this.ExceptionMessage = databaseObject.Command.Parameters["@exceptionMessage"].Value.ToString();
                this.ExceptionStackTrace = databaseObject.Command.Parameters["@exceptionStackTrace"].Value.ToString();
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
