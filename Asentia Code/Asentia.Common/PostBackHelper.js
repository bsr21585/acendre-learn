﻿// This script is to help the .NET AJAX code identifiy the element that caused an
// async postback. There is a deficency in the .NET AJAX code that causes elements
// with a static id included under a master page to not be identified properly.
// This script is necessary because we only use static identifiers for our elements.

// TO DO: MONITOR THIS FIX TO SEE IF IT HAS ADVERSE EFFECTS ON ANY OTHER .NET AJAX CODE
$(document).ready(function () {
    if (Sys.WebForms.PageRequestManager) {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        __doPostBack = function (eventTarget, eventArgument) {
            prm._additionalInput = null;

            var form = prm._form;
            if ((eventTarget === null) || (typeof (eventTarget) === "undefined") || (prm._isCrossPost)) {
                prm._postBackSettings = prm._createPostBackSettings(false);
                prm._isCrossPost = false;
            }
            else {
                var mpUniqueID = prm._masterPageUniqueID;
                var clientID = prm._uniqueIDToClientID(eventTarget);
                var postBackElement = document.getElementById(clientID);
                if (!postBackElement && mpUniqueID) {
                    //--------------------------------------------------------------------------------------
                    // LEGACY CODE - THIS IS WHAT THE ORIGINAL ASP.NET AJAX METHOD HAS
                    //if (clientID.indexOf(mpUniqueID + "$") === 0) {
                    //    postBackElement = document.getElementById(clientID.substr(mpUniqueID.length + 1));
                    //}
                    //--------------------------------------------------------------------------------------

                    //--------------------------------------------------------------------------------------
                    // NEW CODE - accounts for ClientIDMode = "Static" bug in ASP.NET AJAX
                    if (eventTarget.indexOf(mpUniqueID + "$") === 0) {
                        clientID = prm._uniqueIDToClientID(eventTarget.substr(mpUniqueID.length + 1));
                        postBackElement = document.getElementById(clientID);

                        // account for static id
                        if (!postBackElement) {
                            var clientIDArr = eventTarget.split("$");
                            clientID = clientIDArr[clientIDArr.length - 1];
                            postBackElement = document.getElementById(clientID);
                        }
                    }
                    //--------------------------------------------------------------------------------------
                }
                if (!postBackElement) {
                    if (Array.contains(prm._asyncPostBackControlIDs, eventTarget)) {
                        prm._postBackSettings = prm._createPostBackSettings(true, null, eventTarget);
                    }
                    else {
                        if (Array.contains(prm._postBackControlIDs, eventTarget)) {
                            prm._postBackSettings = prm._createPostBackSettings(false);
                        }
                        else {
                            var nearestUniqueIDMatch = prm._findNearestElement(eventTarget);
                            if (nearestUniqueIDMatch) {
                                prm._postBackSettings = prm._getPostBackSettings(nearestUniqueIDMatch, eventTarget);
                            }
                            else {
                                if (mpUniqueID) {
                                    mpUniqueID += "$";
                                    if (eventTarget.indexOf(mpUniqueID) === 0) {
                                        nearestUniqueIDMatch = prm._findNearestElement(eventTarget.substr(mpUniqueID.length));
                                    }
                                }
                                if (nearestUniqueIDMatch) {
                                    prm._postBackSettings = prm._getPostBackSettings(nearestUniqueIDMatch, eventTarget);
                                }
                                else {
                                    prm._postBackSettings = prm._createPostBackSettings(false);
                                }
                            }
                        }
                    }
                }
                else {
                    prm._postBackSettings = prm._getPostBackSettings(postBackElement, eventTarget);
                }
            }

            if (!prm._postBackSettings.async) {
                form.onsubmit = prm._onsubmit;
                prm._originalDoPostBack(eventTarget, eventArgument);
                form.onsubmit = null;
                return;
            }

            form.__EVENTTARGET.value = eventTarget;
            form.__EVENTARGUMENT.value = eventArgument;
            prm._onFormSubmit();
        };
    }
});