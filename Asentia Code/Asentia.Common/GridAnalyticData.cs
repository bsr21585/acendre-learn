﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Asentia.Common
{
    public class GridAnalyticData
    {
        #region Static Methods
        #region ContentPackages
        /// <summary>
        /// Gets grid analytic data for content packages.
        /// </summary>
        /// <returns>DataTable of grid analytic data.</returns>
        public static DataTable ContentPackages()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[GridAnalytic.ContentPackages]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Courses
        /// <summary>
        /// Gets grid analytic data for courses.
        /// </summary>
        /// <returns>DataTable of grid analytic data.</returns>
        public static DataTable Courses()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[GridAnalytic.Courses]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region LearningPaths
        /// <summary>
        /// Gets grid analytic data for learning paths.
        /// </summary>
        /// <returns>DataTable of grid analytic data.</returns>
        public static DataTable LearningPaths()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[GridAnalytic.LearningPaths]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Users
        /// <summary>
        /// Gets grid analytic data for users.
        /// </summary>
        /// <returns>DataTable of grid analytic data.</returns>
        public static DataTable Users()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[GridAnalytic.Users]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Rulesets
        /// <summary>
        /// Gets grid analytic data for rulesets.
        /// </summary>
        /// <returns>DataTable of grid analytic data.</returns>
        public static DataTable Rulesets()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[GridAnalytic.Rulesets]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
