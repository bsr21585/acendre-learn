﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Asentia.Common
{
    public static class AsentiaSessionState
    {
        #region UserDeviceType [ENUM]
        public enum UserDeviceType
        {
            Mobile = 1,
            Tablet = 2,
            Desktop = 3,
        }
        #endregion

        #region Session Properties
        #region SessionId
        public static string SessionId
        {
            get
            {
                return HttpContext.Current.Session.SessionID;
            }
        }
        #endregion

        #region SessionTimeout
        public static int SessionTimeout
        {
            get
            {
                return Convert.ToInt32(HttpContext.Current.Session.Timeout);
            }

            set
            {
                HttpContext.Current.Session.Timeout = value;
            }
        }
        #endregion

        #region DtExpires
        public static DateTime DtExpires
        {
            get
            {
                if (HttpContext.Current.Session["DtExpires"] != null)
                { return Convert.ToDateTime(HttpContext.Current.Session["DtExpires"]); }
                else
                {
                    return DateTime.UtcNow;
                }
            }
            set
            {
                HttpContext.Current.Session["DtExpires"] = value;
            }
        }
        #endregion

        #region DtUserAgreementAgreed
        public static DateTime DtUserAgreementAgreed
        {
            get
            {
                if (HttpContext.Current.Session["DtUserAgreementAgreed"] != null)
                { return Convert.ToDateTime(HttpContext.Current.Session["DtUserAgreementAgreed"]); }
                else
                {
                    return new DateTime(1900, 1, 1);
                }
            }
            set
            {
                HttpContext.Current.Session["DtUserAgreementAgreed"] = value;
            }
        }
        #endregion

        #region UtcNow
        public static DateTime UtcNow
        {
            get
            {
                if (HttpContext.Current.Session["UtcNow"] != null)
                { return Convert.ToDateTime(HttpContext.Current.Session["UtcNow"]); }
                else
                { return DateTime.UtcNow; }
            }

            set
            {
                HttpContext.Current.Session["UtcNow"] = value;
            }
        }
        #endregion

        #region IdAccount
        public static int IdAccount
        {
            get
            {
                if (HttpContext.Current.Session["IdAccount"] != null)
                { return Convert.ToInt32(HttpContext.Current.Session["IdAccount"]); }
                else
                { return 0; }
            }

            set
            {
                HttpContext.Current.Session["IdAccount"] = value;
            }
        }
        #endregion

        #region IdAccountUser
        public static int IdAccountUser
        {
            get
            {
                if (HttpContext.Current.Session["IdAccountUser"] != null)
                { return Convert.ToInt32(HttpContext.Current.Session["IdAccountUser"]); }
                else
                { return 0; }
            }

            set
            {
                HttpContext.Current.Session["IdAccountUser"] = value;
            }
        }
        #endregion

        #region IdImpersonatingAccount
        public static int IdImpersonatingAccount
        {
            get
            {
                if (HttpContext.Current.Session["IdImpersonatingAccount"] != null)
                { return Convert.ToInt32(HttpContext.Current.Session["IdImpersonatingAccount"]); }
                else
                { return 0; }
            }

            set
            {
                HttpContext.Current.Session["IdImpersonatingAccount"] = value;
            }
        }
        #endregion

        #region IdImpersonatingAccountUser
        public static int IdImpersonatingAccountUser
        {
            get
            {
                if (HttpContext.Current.Session["IdImpersonatingAccountUser"] != null)
                { return Convert.ToInt32(HttpContext.Current.Session["IdImpersonatingAccountUser"]); }
                else
                { return 0; }
            }

            set
            {
                HttpContext.Current.Session["IdImpersonatingAccountUser"] = value;
            }
        }
        #endregion

        #region IdSite
        public static int IdSite
        {
            get
            {
                if (HttpContext.Current.Session["IdSite"] != null)
                { return Convert.ToInt32(HttpContext.Current.Session["IdSite"]); }
                else
                { return 0; }
            }

            set
            {
                HttpContext.Current.Session["IdSite"] = value;
            }
        }
        #endregion

        #region SiteHostname
        public static string SiteHostname
        {
            get
            {
                if (HttpContext.Current.Session["SiteHostname"] != null)
                { return HttpContext.Current.Session["SiteHostname"].ToString(); }
                else
                { return "default"; }
            }

            set
            {
                HttpContext.Current.Session["SiteHostname"] = value;
            }
        }
        #endregion

        #region GlobalSiteObject
        public static AsentiaSite_ReadOnly GlobalSiteObject
        {
            get
            {
                if (HttpContext.Current.Session["GlobalSiteObject"] != null)
                { return (AsentiaSite_ReadOnly)HttpContext.Current.Session["GlobalSiteObject"]; }
                else
                { return null; }
            }

            set
            {
                HttpContext.Current.Session["GlobalSiteObject"] = value;
            }
        }
        #endregion

        #region IdSiteUser
        public static int IdSiteUser
        {
            get
            {
                if (HttpContext.Current.Session["IdSiteUser"] != null)
                { return Convert.ToInt32(HttpContext.Current.Session["IdSiteUser"]); }
                else
                { return 0; }
            }

            set
            {
                HttpContext.Current.Session["IdSiteUser"] = value;
            }
        }
        #endregion

        #region IdImpersonatingSiteUser
        public static int IdImpersonatingSiteUser
        {
            get
            {
                if (HttpContext.Current.Session["IdImpersonatingSiteUser"] != null)
                { return Convert.ToInt32(HttpContext.Current.Session["IdImpersonatingSiteUser"]); }
                else
                { return 0; }
            }

            set
            {
                HttpContext.Current.Session["IdImpersonatingSiteUser"] = value;
            }
        }
        #endregion

        #region UserFirstName
        public static string UserFirstName
        {
            get
            {
                if (HttpContext.Current.Session["UserFirstName"] != null)
                { return HttpContext.Current.Session["UserFirstName"].ToString(); }
                else
                { return String.Empty; }
            }

            set
            {
                HttpContext.Current.Session["UserFirstName"] = value;
            }
        }
        #endregion

        #region UserLastName
        public static string UserLastName
        {
            get
            {
                if (HttpContext.Current.Session["UserLastName"] != null)
                { return HttpContext.Current.Session["UserLastName"].ToString(); }
                else
                { return String.Empty; }
            }

            set
            {
                HttpContext.Current.Session["UserLastName"] = value;
            }
        }
        #endregion

        #region UserAvatar
        public static string UserAvatar
        {
            get
            {
                if (HttpContext.Current.Session["UserAvatar"] != null)
                { return HttpContext.Current.Session["UserAvatar"].ToString(); }
                else
                { return String.Empty; }
            }

            set
            {
                HttpContext.Current.Session["UserAvatar"] = value;
            }
        }
        #endregion

        #region UserGender
        public static string UserGender
        {
            get
            {
                if (HttpContext.Current.Session["UserGender"] != null)
                { return HttpContext.Current.Session["UserGender"].ToString(); }
                else
                { return String.Empty; }
            }

            set
            {
                HttpContext.Current.Session["UserGender"] = value;
            }
        }
        #endregion

        #region UserCulture
        public static string UserCulture
        {
            get
            {
                if (HttpContext.Current.Session["UserCulture"] != null)
                { return HttpContext.Current.Session["UserCulture"].ToString(); }
                else
                { return "en-US"; }
            }

            set
            {
                HttpContext.Current.Session["UserCulture"] = value;
            }
        }
        #endregion

        #region UserTimezoneDisplayName
        public static string UserTimezoneDisplayName
        {
            get
            {
                if (HttpContext.Current.Session["UserTimezoneDisplayName"] != null)
                { return HttpContext.Current.Session["UserTimezoneDisplayName"].ToString(); }
                else
                { return "(GMT-05:00) Eastern Time (US & Canada)"; }
            }

            set
            {
                HttpContext.Current.Session["UserTimezoneDisplayName"] = value;
            }
        }
        #endregion

        #region UserTimezoneDotNetName
        public static string UserTimezoneDotNetName
        {
            get
            {
                if (HttpContext.Current.Session["UserTimezoneDotNetName"] != null)
                { return HttpContext.Current.Session["UserTimezoneDotNetName"].ToString(); }
                else
                { return "Eastern Standard Time"; }
            }

            set
            {
                HttpContext.Current.Session["UserTimezoneDotNetName"] = value;
            }
        }
        #endregion

        #region UserTimezoneBaseUtcOffset
        public static float UserTimezoneBaseUtcOffset
        {
            get
            {
                if (HttpContext.Current.Session["UserTimezoneBaseUtcOffset"] != null)
                { return float.Parse(HttpContext.Current.Session["UserTimezoneBaseUtcOffset"].ToString()); }
                else
                { return -5; }
            }

            set
            {
                HttpContext.Current.Session["UserTimezoneBaseUtcOffset"] = value;
            }
        }
        #endregion

        #region UserTimezoneCurrentLocalTime
        public static DateTime UserTimezoneCurrentLocalTime
        {
            get
            {
                if (HttpContext.Current.Session["UserTimezoneCurrentLocalTime"] != null)
                { return DateTime.Parse(HttpContext.Current.Session["UserTimezoneCurrentLocalTime"].ToString()); }
                else
                { return TimeZoneInfo.ConvertTimeFromUtc(AsentiaSessionState.UtcNow, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)); }
            }

            set
            {
                HttpContext.Current.Session["UserTimezoneCurrentLocalTime"] = value;
            }
        }
        #endregion

        #region UserEffectivePermissions
        public static List<AsentiaPermissionWithScope> UserEffectivePermissions
        {
            get
            {
                if (HttpContext.Current.Session["UserEffectivePermissions"] != null)
                { return (List<AsentiaPermissionWithScope>)HttpContext.Current.Session["UserEffectivePermissions"]; }
                else
                { return null; }
            }

            set
            {
                HttpContext.Current.Session["UserEffectivePermissions"] = value;
            }
        }
        #endregion

        #region UserLastPermissionCheckUtc
        public static DateTime UserLastPermissionCheckUtc
        {
            get
            {
                if (HttpContext.Current.Session["UserLastPermissionCheckUtc"] != null)
                { return DateTime.Parse(HttpContext.Current.Session["UserLastPermissionCheckUtc"].ToString()); }
                else
                { return DateTime.UtcNow; }
            }

            set
            {
                HttpContext.Current.Session["UserLastPermissionCheckUtc"] = value;
            }
        }
        #endregion

        #region UserMustChangePassword
        public static bool UserMustChangePassword
        {
            get
            {
                if (HttpContext.Current.Session["UserMustChangePassword"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["UserMustChangePassword"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["UserMustChangePassword"] = value;
            }
        }
        #endregion

        #region MustExecuteLicenseAgreement
        public static bool MustExecuteLicenseAgreement
        {
            get
            {
                if (HttpContext.Current.Session["MustExecuteLicenseAgreement"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["MustExecuteLicenseAgreement"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["MustExecuteLicenseAgreement"] = value;
            }
        }
        #endregion

        #region MustExecuteUserAgreement
        public static bool MustExecuteUserAgreement
        {
            get
            {
                if (HttpContext.Current.Session["MustExecuteUserAgreement"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["MustExecuteUserAgreement"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["MustExecuteUserAgreement"] = value;
            }
        }
        #endregion

        #region IsUserACourseExpert
        public static bool IsUserACourseExpert
        {
            get
            {
                if (HttpContext.Current.Session["IsUserACourseExpert"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["IsUserACourseExpert"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["IsUserACourseExpert"] = value;
            }
        }
        #endregion

        #region IsUserACourseApprover
        public static bool IsUserACourseApprover
        {
            get
            {
                if (HttpContext.Current.Session["IsUserACourseApprover"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["IsUserACourseApprover"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["IsUserACourseApprover"] = value;
            }
        }
        #endregion

        #region IsUserASupervisor
        public static bool IsUserASupervisor
        {
            get
            {
                if (HttpContext.Current.Session["IsUserASupervisor"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["IsUserASupervisor"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["IsUserASupervisor"] = value;
            }
        }
        #endregion

        #region IsUserAWallModerator
        public static bool IsUserAWallModerator
        {
            get
            {
                if (HttpContext.Current.Session["IsUserAWallModerator"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["IsUserAWallModerator"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["IsUserAWallModerator"] = value;
            }
        }
        #endregion

        #region IsUserAnILTInstructor
        public static bool IsUserAnILTInstructor
        {
            get
            {
                if (HttpContext.Current.Session["IsUserAnILTInstructor"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["IsUserAnILTInstructor"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["IsUserAnILTInstructor"] = value;
            }
        }
        #endregion

        #region ImpersonatingUserFirstName
        public static string ImpersonatingUserFirstName
        {
            get
            {
                if (HttpContext.Current.Session["ImpersonatingUserFirstName"] != null)
                { return HttpContext.Current.Session["ImpersonatingUserFirstName"].ToString(); }
                else
                { return String.Empty; }
            }

            set
            {
                HttpContext.Current.Session["ImpersonatingUserFirstName"] = value;
            }
        }
        #endregion

        #region ImpersonatingUserLastName
        public static string ImpersonatingUserLastName
        {
            get
            {
                if (HttpContext.Current.Session["ImpersonatingUserLastName"] != null)
                { return HttpContext.Current.Session["ImpersonatingUserLastName"].ToString(); }
                else
                { return String.Empty; }
            }

            set
            {
                HttpContext.Current.Session["ImpersonatingUserLastName"] = value;
            }
        }
        #endregion

        #region ImpersonatingUserAvatar
        public static string ImpersonatingUserAvatar
        {
            get
            {
                if (HttpContext.Current.Session["ImpersonatingUserAvatar"] != null)
                { return HttpContext.Current.Session["ImpersonatingUserAvatar"].ToString(); }
                else
                { return String.Empty; }
            }

            set
            {
                HttpContext.Current.Session["ImpersonatingUserAvatar"] = value;
            }
        }
        #endregion

        #region ImpersonatingUserGender
        public static string ImpersonatingUserGender
        {
            get
            {
                if (HttpContext.Current.Session["ImpersonatingUserGender"] != null)
                { return HttpContext.Current.Session["ImpersonatingUserGender"].ToString(); }
                else
                { return String.Empty; }
            }

            set
            {
                HttpContext.Current.Session["ImpersonatingUserGender"] = value;
            }
        }
        #endregion

        #region ImpersonatingUserCulture
        public static string ImpersonatingUserCulture
        {
            get
            {
                if (HttpContext.Current.Session["ImpersonatingUserCulture"] != null)
                { return HttpContext.Current.Session["ImpersonatingUserCulture"].ToString(); }
                else
                { return "en-US"; }
            }

            set
            {
                HttpContext.Current.Session["ImpersonatingUserCulture"] = value;
            }
        }
        #endregion

        #region ImpersonatingUserIsUserACourseExpert
        public static bool ImpersonatingUserIsUserACourseExpert
        {
            get
            {
                if (HttpContext.Current.Session["ImpersonatingUserIsUserACourseExpert"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["ImpersonatingUserIsUserACourseExpert"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["ImpersonatingUserIsUserACourseExpert"] = value;
            }
        }
        #endregion

        #region ImpersonatingUserIsUserACourseApprover
        public static bool ImpersonatingUserIsUserACourseApprover
        {
            get
            {
                if (HttpContext.Current.Session["ImpersonatingUserIsUserACourseApprover"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["ImpersonatingUserIsUserACourseApprover"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["ImpersonatingUserIsUserACourseApprover"] = value;
            }
        }
        #endregion

        #region ImpersonatingUserIsUserASupervisor
        public static bool ImpersonatingUserIsUserASupervisor
        {
            get
            {
                if (HttpContext.Current.Session["ImpersonatingUserIsUserASupervisor"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["ImpersonatingUserIsUserASupervisor"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["ImpersonatingUserIsUserASupervisor"] = value;
            }
        }
        #endregion

        #region ImpersonatingUserIsUserAWallModerator
        public static bool ImpersonatingUserIsUserAWallModerator
        {
            get
            {
                if (HttpContext.Current.Session["ImpersonatingUserIsUserAWallModerator"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["ImpersonatingUserIsUserAWallModerator"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["ImpersonatingUserIsUserAWallModerator"] = value;
            }
        }
        #endregion

        #region ImpersonatingUserIsUserAnILTInstructor
        public static bool ImpersonatingUserIsUserAnILTInstructor
        {
            get
            {
                if (HttpContext.Current.Session["ImpersonatingUserIsUserAnILTInstructor"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["ImpersonatingUserIsUserAnILTInstructor"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["ImpersonatingUserIsUserAnILTInstructor"] = value;
            }
        }
        #endregion

        #region UserDevice
        public static UserDeviceType UserDevice
        {
            get
            {
                if (HttpContext.Current.Session["UserDevice"] != null)
                { return (UserDeviceType)Convert.ToInt32(HttpContext.Current.Session["UserDevice"]); }
                else
                { return UserDeviceType.Desktop; }
            }

            set
            {
                HttpContext.Current.Session["UserDevice"] = value;
            }
        }
        #endregion

        #region BounceForNoAccessIPRestriction
        public static bool BounceForNoAccessIPRestriction
        {
            get
            {
                if (HttpContext.Current.Session["BounceForNoAccessIPRestriction"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["BounceForNoAccessIPRestriction"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["BounceForNoAccessIPRestriction"] = value;
            }
        }
        #endregion

        #region IsThemePreviewMode
        public static bool IsThemePreviewMode
        {
            get
            {
                if (HttpContext.Current.Session["IsThemePreviewMode"] != null)
                { return Convert.ToBoolean(HttpContext.Current.Session["IsThemePreviewMode"]); }
                else
                { return false; }
            }

            set
            {
                HttpContext.Current.Session["IsThemePreviewMode"] = value;
            }
        }
        #endregion
        #endregion

        #region Object Specific Properties
        #region DateTimeForCalendar
        public static DateTime DateTimeForCalendar
        {
            get
            {
                if (HttpContext.Current.Session["DateTimeForCalendar"] != null)
                { return Convert.ToDateTime(HttpContext.Current.Session["DateTimeForCalendar"]); }
                else
                { return AsentiaSessionState.UserTimezoneCurrentLocalTime; }
            }

            set
            {
                HttpContext.Current.Session["DateTimeForCalendar"] = value;
            }
        }
        #endregion

        #region TinCan IdDataLesson
        /// <summary>
        /// idDataLesson used in tin can api
        /// </summary>
        public static int CurrentlyLaunchedDataLessonId
        {
            get
            {
                if (HttpContext.Current.Session["IdDataLesson"] != null)
                { return Convert.ToInt32(HttpContext.Current.Session["IdDataLesson"]); }
                else
                { return 0; }
            }

            set
            {
                HttpContext.Current.Session["IdDataLesson"] = value;
            }
        }
        #endregion
        #endregion

        #region Methods
        public static void EndSession(bool redirect)
        {
            HttpContext.Current.Session.Abandon();

            if (redirect)
            { HttpContext.Current.Response.Redirect("/"); }
        }
        #endregion
    }
}
