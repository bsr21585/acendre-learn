﻿using System;
using System.IO;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Xml;
using System.Xml.XPath;
using PdfSharp;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using MigraDoc.Rendering;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel.Shapes;

namespace Asentia.Common
{
    /// <summary>
    /// Provides methods to generate PDF documents, saves to file system and returns memory stream of the document.
    /// </summary>
    public class PdfGenerator : IDisposable
    {
        #region Fields
        private bool _Disposed;
        private PdfDocument _Document;
        private string _BackgroundImageUrl;
        private double _ImageWidth; //Background image width
        private double _ImageHeight; //Background image height
        private string _Title;
        private string _Author;
        private string _Subject;
        private PdfPage _Page;
        private PageSize _PageSize;
        private Orientation _Orientation;
        private double _PositionX;
        private double _PositionY;
        private double _FontSize;
        private double _TextLayoutRectWidth; //Width of the layout rectangle in which text to be drawn.
        private double _TextLayoutRectHeight; //Height of the layout rectangle in which text to be drawn.
        private Alignment? _TextAlignment; //Horizontal text alignment.

        // RGB colors
        readonly static Color TableBorder = new Color(81, 125, 192);
        readonly static Color TableBlue = new Color(235, 240, 249);
        readonly static Color TableGray = new Color(242, 242, 242);
        #endregion
        
        #region Constructors
        /// <summary>
        /// Initializes the document with page size A4 and orientation Portrait.
        /// </summary>
        public PdfGenerator()
        {
            this._PageSize = PageSize.A4;
            this._Orientation = Orientation.Portrait;

            _InitDocument();
        }


        /// <summary>
        /// Initializes the document with background image.
        /// It uses default page size A4 and orientation Portrait.
        /// </summary>
        /// <param name="backgroundImageUrl">Full file path of the background image.</param>
        /// <param name="imageWidth">Width of the background image.</param>
        /// <param name="imageHeight">Height of the background image.</param>
        public PdfGenerator(string backgroundImageUrl, double imageWidth, double imageHeight)
        {
            this._PageSize = PageSize.A4;
            this._Orientation = Orientation.Portrait;
            this._BackgroundImageUrl = backgroundImageUrl;
            this._ImageWidth = imageWidth;
            this._ImageHeight = imageHeight;

            _InitDocument();
        }

        /// <summary>
        /// Initializes the document with background image, title, author and subject.
        /// It uses default page size A4 and orientation Portrait.
        /// </summary>
        /// <param name="backgroundImageUrl">Full file path of the background image.</param>
        /// <param name="imageWidth">Width of the background image.</param>
        /// <param name="imageHeight">Height of the background image.</param>
        /// <param name="title">Title of the document.</param>
        /// <param name="author">Author of the document.</param>
        /// <param name="subject">Subject of the document.</param>
        public PdfGenerator(string backgroundImageUrl, double imageWidth, double imageHeight, string title, string author, string subject)
        {
            this._PageSize = PageSize.A4;
            this._Orientation = Orientation.Portrait;

            this._BackgroundImageUrl = backgroundImageUrl;
            this._ImageWidth = imageWidth;
            this._ImageHeight = imageHeight;
            this._Title = title;
            this._Author = author;
            this._Subject = subject;

            _InitDocument();
        }

        /// <summary>
        /// Initializes the document with background image, page size and orientation.
        /// </summary>
        /// <param name="backgroundImageUrl">Full file path of the background image.</param>
        /// <param name="imageWidth">Width of the background image.</param>
        /// <param name="imageHeight">Height of the background image.</param>
        /// <param name="pageSize">Page size such as A4, Letter or Legal.</param>
        /// <param name="orientation">Page orientation such as Portrait or Landscape.</param>
        public PdfGenerator(string backgroundImageUrl, double imageWidth, double imageHeight, PageSize pageSize, Orientation orientation)
        {
            this._PageSize = pageSize;
            this._Orientation = orientation;
            this._BackgroundImageUrl = backgroundImageUrl;
            this._ImageWidth = imageWidth;
            this._ImageHeight = imageHeight;

            _InitDocument();
        }

        /// <summary>
        /// Initializes the document with background image, title, author, subject, page size and orientation.
        /// </summary>
        /// <param name="backgroundImageUrl">Full file path of the background image.</param>
        /// <param name="imageWidth">Width of the background image.</param>
        /// <param name="imageHeight">Height of the background image.</param>
        /// <param name="title">Title of the document.</param>
        /// <param name="author">Author of the document.</param>
        /// <param name="subject">Subject of the document.</param>
        /// <param name="pageSize">Page size such as A4, Letter or Legal.</param>
        /// <param name="orientation">Page orientation such as Portrait or Landscape.</param>
        public PdfGenerator(string backgroundImageUrl, double imageWidth, double imageHeight, string title, string author, string subject, PageSize pageSize, Orientation orientation)
        {
            this._PageSize = pageSize;
            this._Orientation = orientation;

            this._BackgroundImageUrl = backgroundImageUrl;
            this._ImageWidth = imageWidth;
            this._ImageHeight = imageHeight;
            this._Title = title;
            this._Author = author;
            this._Subject = subject;

            _InitDocument();
        }


    /// <summary>
    /// The MigraDoc document that represents the invoice.
    /// </summary>
    Document document;

    /// <summary>
    /// An XML invoice based on a sample created with Microsoft InfoPath.
    /// </summary>
    DataTable _dtOriginal;
    string path;
    /// <summary>
    /// The root navigator for the XML document.
    /// </summary>



    /// <summary>
    /// The table of the MigraDoc document that contains the report items.
    /// </summary>
    Table[] table = new Table[5];

    public void PDFform(DataTable dtIn, string reportTitle, string path)
    {
            // JC EDIT
            //MemoryStream memoryStream = new MemoryStream();

            _dtOriginal = dtIn;
        
            this._Title = reportTitle;
        
            Document document = this.CreateDocument();
            document.DefaultPageSetup.Orientation = MigraDoc.DocumentObjectModel.Orientation.Landscape;
            
            document.UseCmykColor = true;

            // Create a renderer for PDF that uses Unicode font encoding
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(true);

            // Set the MigraDoc document
            pdfRenderer.Document = document;

            // Create the PDF document
            pdfRenderer.RenderDocument();

            pdfRenderer.PdfDocument.Save(path);
            // JC EDIT
            // ...and start a viewer.
            //Process.Start(path);

        // JC EDIT
        //return memoryStream;
    }

    /// <summary>
    /// Creates the invoice document.
    /// </summary>
    public Document CreateDocument()
    {
        // Create a new MigraDoc document
        this.document = new Document();

        DefineStyles();

        CreatePage();

        return this.document;
    }

    /// <summary>
    /// Defines the styles used to format the MigraDoc document.
    /// </summary>
    void DefineStyles()
    {
        // Get the predefined style Normal.
        Style style = this.document.Styles["Normal"];
        // Because all styles are derived from Normal, the next line changes the 
        // font of the whole document. Or, more exactly, it changes the font of
        // all styles and paragraphs that do not redefine the font.
        style.Font.Name = "Times New Roman";

        style = this.document.Styles[StyleNames.Header];
        //style.ParagraphFormat.AddTabStop("0cm", TabAlignment.Right);

        style = this.document.Styles[StyleNames.Footer];
        //style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

        // Create a new style called Table based on style Normal
        style = this.document.Styles.AddStyle("Table", "Normal");
        style.Font.Name = "Open Sans";
        //style.Font.Name = "Times New Roman";
        style.Font.Size = 9;

        // Create a new style called Reference based on style Normal
        style = this.document.Styles.AddStyle("Reference", "Normal");
        style.ParagraphFormat.SpaceBefore = "5mm";
        style.ParagraphFormat.SpaceAfter = "5mm";
        //style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
    }

    /// <summary>
    /// Creates the static parts of the invoice.
    /// </summary>
   
    void CreatePage()
    {
        // Each MigraDoc document needs at least one section.
        Section section = this.document.AddSection();
      
        // Add the print date field
        Paragraph paragraph = section.Headers.Primary.AddParagraph();
        paragraph.Format.SpaceBefore = "0cm";
        paragraph.Style = "Reference";
        paragraph.AddFormattedText(this._Title, TextFormat.Bold);
        paragraph.Format.Alignment = ParagraphAlignment.Center;

        // Before you can add a row, you must define the columns
        for (int p = 0; p < (_dtOriginal.Columns.Count + 7) / 8; p++)
        {
            DataTable dt = _dtOriginal.Copy();

            while (dt.Columns.Count > (p+1)*8 )
            {
                dt.Columns.RemoveAt( (p+1)*8);
            }

            while (dt.Columns.Count > Math.Min(8, _dtOriginal.Columns.Count - p*8))
            {
                dt.Columns.RemoveAt(0);
            }

            // Create the item table
            if (p > 0) section = this.document.AddSection();
            this.table[p] = section.AddTable();
            this.table[p].Style = "Table";
            this.table[p].Borders.Color = TableBorder;
            this.table[p].Borders.Width = 0.25;
            this.table[p].Borders.Left.Width = 0;
            this.table[p].Borders.Right.Width = 0.5;
            this.table[p].Rows.LeftIndent = 0;

            Column column;
            // for row number
            column = this.table[p].AddColumn(Unit.FromCentimeter(0.6));
            column.Borders.Width = 0;
            column.Shading.Color = Color.Empty;

            foreach (DataColumn col in dt.Columns)
            {
                column = this.table[p].AddColumn((col.ColumnName == "Email") ? Unit.FromCentimeter(5) : Unit.FromCentimeter(3));
                column.Format.Alignment = ParagraphAlignment.Center;
            }

            // Create the header of the table
            Row row = table[p].AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;

            for (int i = 1; i <= dt.Columns.Count; i++)
            {
                row.Cells[i].AddParagraph(dt.Columns[i-1].ColumnName);
                row.Cells[i].Shading.Color = TableBlue;
                row.Cells[i].Format.Font.Bold = false;
                row.Cells[i].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[i].VerticalAlignment = VerticalAlignment.Bottom;
            }

            this.table[p].SetEdge(1, 0, dt.Columns.Count, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);

            FillContent(dt, this.table[p]);
        }
    }



    /// <summary>
    /// Creates the dynamic parts
    /// </summary>
    private void FillContent(DataTable dt, Table tbl)
    {
        // Fill address in address text frame

        Row row1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row1 = tbl.AddRow();
            row1.TopPadding = 1.5;
            row1.Format.Borders.Width = 0;

            row1.Cells[0].AddParagraph(Convert.ToString(i+1));
            row1.Cells[0].Shading.Color = TableGray;
            row1.Cells[0].Format.Alignment = ParagraphAlignment.Right;
            row1.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row1.Cells[0].Borders.Color = Color.Empty;
            row1.Cells[0].Shading.Color = Color.Empty;
            row1.Cells[0].Format.Font.Color = TableBorder;
            row1.Cells[0].Format.Font.Subscript = true;
            row1.Cells[0].Format.Font.Size = Unit.FromCentimeter(0.3);

            for (int j = 1; j <= dt.Columns.Count; j++)
            {
                row1.Cells[j].Shading.Color = TableGray;
                row1.Cells[j].VerticalAlignment = VerticalAlignment.Center;

                row1.Cells[j].Format.Alignment = ParagraphAlignment.Left;
                row1.Cells[j].Format.FirstLineIndent = 1;
                row1.Cells[j].AddParagraph(dt.Rows[i][j-1].ToString());


                tbl.SetEdge(1, tbl.Rows.Count - 1, dt.Columns.Count, 1, Edge.Box, BorderStyle.Single, 0.75);
            }
        }  
    }

        #endregion

        #region Private Methods
        /// <summary>
        /// Initializes document.
        /// </summary>
        private void _InitDocument()
        {
            _Document = new PdfDocument();
            _Document.Info.Title = this._Title;
            _Document.Info.Author = this._Author;
            _Document.Info.Title = this._Title;

            _Page = _Document.AddPage();

            #region Set page orientation
            switch (this._Orientation)
            {
                case Orientation.Portrait:
                    _Page.Orientation = PdfSharp.PageOrientation.Portrait;
                    break;
                case Orientation.Landscape:
                    _Page.Orientation = PdfSharp.PageOrientation.Landscape;
                    break;
            }
            #endregion

            #region Set page size
            switch (this._PageSize)
            {
                case PageSize.A0:
                    _Page.Size = PdfSharp.PageSize.A0;
                    break;
                case PageSize.A1:
                    _Page.Size = PdfSharp.PageSize.A1;
                    break;
                case PageSize.A2:
                    _Page.Size = PdfSharp.PageSize.A2;
                    break;
                case PageSize.A3:
                    _Page.Size = PdfSharp.PageSize.A3;
                    break;
                case PageSize.A4:
                    _Page.Size = PdfSharp.PageSize.A4;
                    break;
                case PageSize.A5:
                    _Page.Size = PdfSharp.PageSize.A5;
                    break;
                case PageSize.B0:
                    _Page.Size = PdfSharp.PageSize.B0;
                    break;
                case PageSize.B1:
                    _Page.Size = PdfSharp.PageSize.B1;
                    break;
                case PageSize.B2:
                    _Page.Size = PdfSharp.PageSize.B2;
                    break;
                case PageSize.B3:
                    _Page.Size = PdfSharp.PageSize.B3;
                    break;
                case PageSize.B4:
                    _Page.Size = PdfSharp.PageSize.B4;
                    break;
                case PageSize.B5:
                    _Page.Size = PdfSharp.PageSize.B5;
                    break;
                case PageSize.Letter:
                    _Page.Size = PdfSharp.PageSize.Letter;
                    break;
                case PageSize.Folio:
                    _Page.Size = PdfSharp.PageSize.Folio;
                    break;
                case PageSize.Legal:
                    _Page.Size = PdfSharp.PageSize.Legal;
                    break;
            }
            #endregion

            //Draw the background image.
            if (File.Exists(this._BackgroundImageUrl))
            {
                using (XGraphics graphics = XGraphics.FromPdfPage(this._Page))
                {
                    XRect layoutRect = new XRect(0, 0, this._ImageWidth, this._ImageHeight);
                    XImage img = XImage.FromFile(this._BackgroundImageUrl);
                    graphics.DrawImage(img, layoutRect);
                }
            }
        }

        /// <summary>
        /// Draws string on the document.
        /// </summary>
        /// <param name="str">The text to be drawn on the document.</param>
        private void _DrawString(string str)
        {
            using (XGraphics graphics = XGraphics.FromPdfPage(this._Page))
            {
                var options = new XPdfFontOptions(PdfFontEmbedding.Always);
                XFont font = new XFont("Calibri", _FontSize, XFontStyle.Regular, options);

                if (this._TextAlignment != null)
                {
                    XStringAlignment stringAlignment;
                    if (this._TextAlignment == Alignment.Right)
                    {
                        stringAlignment = XStringAlignment.Far;
                    }
                    else if (this._TextAlignment == Alignment.Center)
                    {
                        stringAlignment = XStringAlignment.Center;
                    }
                    else //Otherwise alignment is left
                    {
                        stringAlignment = XStringAlignment.Near;
                    }

                    XStringFormat stringFormat = XStringFormats.Center; //Vertically center
                    stringFormat.Alignment = stringAlignment; //Horizontal alignment

                    XRect layoutRect = new XRect(this._PositionX, this._PositionY, this._TextLayoutRectWidth, this._TextLayoutRectHeight);
                    graphics.DrawString(str, font, XBrushes.Black, layoutRect, stringFormat);
                }
                else
                {
                    XSize size = graphics.MeasureString(str, font);
                    _PositionY += 11;
                    graphics.DrawString(str, font, XBrushes.Black, _PositionX, _PositionY + size.Height);
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Saves document to the file system.
        /// </summary>
        /// <param name="url">Full file path where to save the document.</param>
        public void Save(string url)
        {
            _Document.Save(url);
        }

        /// <summary>
        /// Saves the document to a memory stream and returns it.
        /// </summary>
        /// <returns>Returns memory stream of the document.</returns>
        public MemoryStream GetStream()
        {
            MemoryStream memoryStream = new MemoryStream();
            _Document.Save(memoryStream, false);

            return memoryStream;
        }

        /// <summary>
        /// Writes string to the document with 0 as x as well as y position.
        /// </summary>
        /// <param name="str">The string to be written to the document.</param>
        public void WriteString(string str)
        {
            this._FontSize = 18;
            this._PositionX = 0;
            this._PositionY = 0;
            this._DrawString(str);
        }

        /// <summary>
        /// Writes string to the document with the specified coordinates.
        /// </summary>
        /// <param name="str">The string to be written to the document.</param>
        /// <param name="x">The x position of the string.</param>
        /// <param name="y">The y position of the string.</param>
        public void WriteString(string str, double x, double y)
        {
            this._FontSize = 18;
            this._PositionX = x;
            this._PositionY = y;
            this._DrawString(str);
        }

        /// <summary>
        /// Writes string to the document with the specified coordinates and font size.
        /// </summary>
        /// <param name="str">The string to be written to the document.</param>
        /// <param name="x">The x position of the string.</param>
        /// <param name="y">The y position of the string.</param>
        /// <param name="fontSize">The font size.</param>
        public void WriteString(string str, double x, double y, double fontSize)
        {
            this._FontSize = fontSize;
            this._PositionX = x;
            this._PositionY = y;
            this._DrawString(str);
        }

        /// <summary>
        /// Writes string to the document with the specified coordinates, font size.
        /// </summary>
        /// <param name="str">The string to be written to the document.</param>
        /// <param name="x">The x position of the string.</param>
        /// <param name="y">The y position of the string.</param>
        /// <param name="fontSize">The font size.</param>
        /// <param name="alignment">Horizontal text alignment.</param>
        /// <param name="width">Width of the layout rectangle in which text to be drawn.</param>
        /// <param name="height">Height of the layout rectangle in which text to be drawn.</param>
        public void WriteString(string str, double x, double y, double fontSize, Alignment alignment, double width, double height)
        {
            this._FontSize = fontSize;
            this._PositionX = x;
            this._PositionY = y;
            this._TextAlignment = alignment;
            this._TextLayoutRectWidth = width;
            this._TextLayoutRectHeight = height;

            this._DrawString(str);
        }

        /// <summary>
        /// Releases all the resources used by this object.
        /// </summary>
        public void Dispose()
        {
            if (!_Disposed && _Document != null)
            {
                _Document.Dispose();
                _Disposed = true;
            }
        }
        #endregion

        #region Enums
        /// <summary>
        /// Determines size of the page.
        /// </summary>
        public enum PageSize
        {
            /// <summary>
            /// Page size A0.
            /// </summary>
            A0,

            /// <summary>
            /// Page size A1.
            /// </summary>
            A1,

            /// <summary>
            /// Page size A2.
            /// </summary>
            A2,

            /// <summary>
            /// Page size A3.
            /// </summary>
            A3,

            /// <summary>
            /// Page size A4.
            /// </summary>
            A4,

            /// <summary>
            /// Page size A5.
            /// </summary>
            A5,

            /// <summary>
            /// Page size B0.
            /// </summary>
            B0,

            /// <summary>
            /// Page size B1.
            /// </summary>
            B1,

            /// <summary>
            /// Page size B2.
            /// </summary>
            B2,

            /// <summary>
            /// Page size B3.
            /// </summary>
            B3,

            /// <summary>
            /// Page size B4.
            /// </summary>
            B4,

            /// <summary>
            /// Page size B5.
            /// </summary>
            B5,

            /// <summary>
            /// Page size Letter.
            /// </summary>
            Letter,

            /// <summary>
            /// Page size Folio.
            /// </summary>
            Folio,

            /// <summary>
            /// Page size Legal.
            /// </summary>
            Legal
        }

        /// <summary>
        /// Determines orientation of the page.
        /// </summary>
        public enum Orientation
        {
            /// <summary>
            /// Page orientation Portrait.
            /// </summary>
            Portrait,

            /// <summary>
            /// Page orientation Landscape.
            /// </summary>
            Landscape
        }

        /// <summary>
        /// Determines horizontal text alignment.
        /// </summary>
        public enum Alignment
        {
            /// <summary>
            /// Specifies that text is aligned in the left.
            /// </summary>
            Left,

            /// <summary>
            /// Specifies that text is aligned in the center.
            /// </summary>
            Center,

            /// <summary>
            /// Specifies that text is aligned in the right.
            /// </summary>
            Right
        }
        #endregion
    }
}