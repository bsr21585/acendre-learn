﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace Asentia.Common
{
    public class RegExValidation
    {
        #region ValidateEmailAddress
        public static bool ValidateEmailAddress(string candidate)
        {
            Regex emailRegex = new Regex(@"^([a-zA-Z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?)$");
            return emailRegex.IsMatch(candidate);
        }
        #endregion

        #region ValidateUrl
        public static bool ValidateUrl(string candidate)
        {
            Regex urlRegex = new Regex(@"^(http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z0-9]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$");
            return urlRegex.IsMatch(candidate);
        }
        #endregion

        //Check zip code for US and Canada but not case sensitive
        #region ValidateZipCode
        public static bool ValidateZipCode(string candidate)
        {
            Regex zipRegex = new Regex(@"^(\d{5}(?:[-\s]\d{4})?)|([abceghjklmnprstvxy]{1}\d{1}[a-z]{1} *\d{1}[a-z]{1}\d{1})$" ,RegexOptions.IgnoreCase);            
            return zipRegex.IsMatch(candidate);
        }
        #endregion

        //Check country code for US and Canada
        #region ValidateUSCACountryCode
        public static bool ValidateUSCACountryCode(string candidate)
        {
            Regex uscaRegex = new Regex(@"^(us)|(ca)|(NULL)$", RegexOptions.IgnoreCase);
            return uscaRegex.IsMatch(candidate);
        }
        #endregion

        //Check ISO country code
        #region ValidateCountryCode
        public static bool ValidateCountryCode(string candidate)
        {
            Regex countryRegex = new Regex(@"^[a-z]{2}$", RegexOptions.IgnoreCase);
            return countryRegex.IsMatch(candidate);
        }
        #endregion

        //Check ISO language code
        #region ValidateLanguageCode
        public static bool ValidateLanguageCode(string candidate)
        {
            Regex languageRegex = new Regex(@"^[a-z]{2}-[a-z]{2}$", RegexOptions.IgnoreCase);
            return languageRegex.IsMatch(candidate);
        }

        public static bool ValidateLanguageCode(string candidate, ArrayList availableLanguageCode)
        {
            Regex languageRegex = new Regex(@"^[a-z]{2}-[a-z]{2}$", RegexOptions.IgnoreCase);
            return languageRegex.IsMatch(candidate) && availableLanguageCode.Contains(candidate);
        }
        #endregion

        //Check timezone string
        #region ValidateTimeZone
        public static bool ValidateTimeZone(string candidate)
        {
            bool isMatch = false;
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            foreach (TimeZoneInfo timeZone in timeZones)
            {
                if (timeZone.StandardName.ToUpper() == candidate.ToUpper())
                    isMatch = true;
            }
            return isMatch;
        }
        #endregion

        //Check customized regular validation with string
        #region ValidateRegularExpression
        public static bool ValidateCustomizedReg(string reString, string candidate)
        {
            Regex customizedRegex = new Regex(reString);
            return customizedRegex.IsMatch(candidate);
        }
        #endregion
    }
}