﻿using System;

namespace Asentia.Common
{
    /// <summary>
    /// Class to parse text.
    /// </summary>
    public class TextParser
    {
        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public TextParser()
        {
            Reset(null);
        }

        /// <summary>
        /// Constructor that takes a string.
        /// </summary>
        /// <param name="text"></param>
        public TextParser(string text)
        {
            Reset(text);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Text.
        /// </summary>
        public string Text { get { return _Text; } }

        /// <summary>
        /// Position.
        /// </summary>
        public int Position { get { return _Pos; } }

        /// <summary>
        /// Characters remaining after the current position.
        /// </summary>
        public int Remaining { get { return _Text.Length - _Pos; } }

        /// <summary>
        /// Null character.
        /// </summary>
        public char NullChar = (char)0;

        /// <summary>
        /// Indicates if the current position is at the end of the current document
        /// </summary>
        public bool EndOfText
        {
            get { return (_Pos >= _Text.Length); }
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// Private property to hold text to parse.
        /// </summary>
        private string _Text;

        /// <summary>
        /// Private property to hold the position in the text.
        /// </summary>
        private int _Pos;
        #endregion

        #region Methods
        #region Reset
        /// <summary>
        /// Resets the current position to the start of the current document
        /// </summary>
        public void Reset()
        {
            _Pos = 0;
        }

        /// <summary>
        /// Sets the current document and resets the current position to the start of it
        /// </summary>
        /// <param name="html"></param>
        public void Reset(string text)
        {
            _Text = (text != null) ? text : String.Empty;
            _Pos = 0;
        }
        #endregion

        #region Peek
        /// <summary>
        /// Returns the character at the current position, or a null character if we're
        /// at the end of the document
        /// </summary>
        /// <returns>The character at the current position</returns>
        public char Peek()
        {
            return Peek(0);
        }

        /// <summary>
        /// Returns the character at the specified number of characters beyond the current
        /// position, or a null character if the specified position is at the end of the
        /// document
        /// </summary>
        /// <param name="ahead">The number of characters beyond the current position</param>
        /// <returns>The character at the specified position</returns>
        public char Peek(int ahead)
        {
            int pos = (_Pos + ahead);
            if (pos < _Text.Length)
                return _Text[pos];
            return NullChar;
        }
        #endregion

        #region Extract
        /// <summary>
        /// Extracts a substring from the specified position to the end of the text
        /// </summary>
        /// <param name="start"></param>
        /// <returns></returns>
        public string Extract(int start)
        {
            return Extract(start, _Text.Length);
        }

        /// <summary>
        /// Extracts a substring from the specified range of the current text
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public string Extract(int start, int end)
        {
            return _Text.Substring(start, end - start);
        }
        #endregion

        #region MoveAhead
        /// <summary>
        /// Moves the current position ahead one character
        /// </summary>
        public void MoveAhead()
        {
            MoveAhead(1);
        }

        /// <summary>
        /// Moves the current position ahead the specified number of characters
        /// </summary>
        /// <param name="ahead">The number of characters to move ahead</param>
        public void MoveAhead(int ahead)
        {
            _Pos = Math.Min(_Pos + ahead, _Text.Length);
        }
        #endregion

        #region MoveTo
        /// <summary>
        /// Moves to the next occurrence of the specified string. Comparisons
        /// are case-sensitive.
        /// </summary>
        /// <param name="s">String to find</param>
        public void MoveTo(string s)
        {
            MoveTo(s, false);
        }

        /// <summary>
        /// Moves to the next occurrence of the specified string
        /// </summary>
        /// <param name="s">String to find</param>
        /// <param name="ignoreCase">Indicates if case-insensitive comparisons are used</param>
        public void MoveTo(string s, bool ignoreCase)
        {
            _Pos = _Text.IndexOf(s, _Pos, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal);
            if (_Pos < 0)
                _Pos = _Text.Length;
        }

        /// <summary>
        /// Moves to the next occurrence of the specified character
        /// </summary>
        /// <param name="c">Character to find</param>
        public void MoveTo(char c)
        {
            _Pos = _Text.IndexOf(c, _Pos);
            if (_Pos < 0)
                _Pos = _Text.Length;
        }

        /// <summary>
        /// Moves to the next occurrence of any one of the specified
        /// characters
        /// </summary>
        /// <param name="chars">Array of characters to find</param>
        public void MoveTo(char[] chars)
        {
            _Pos = _Text.IndexOfAny(chars, _Pos);
            if (_Pos < 0)
                _Pos = _Text.Length;
        }
        #endregion

        #region MovePast
        /// <summary>
        /// Moves to the next occurrence of any character that is not one
        /// of the specified characters
        /// </summary>
        /// <param name="chars">Array of characters to move past</param>
        public void MovePast(char[] chars)
        {
            while (IsInArray(Peek(), chars))
                MoveAhead();
        }
        #endregion

        #region IsInArray
        /// <summary>
        /// Determines if the specified character exists in the specified
        /// character array.
        /// </summary>
        /// <param name="c">Character to find</param>
        /// <param name="chars">Character array to search</param>
        /// <returns></returns>
        protected bool IsInArray(char c, char[] chars)
        {
            foreach (char ch in chars)
            {
                if (c == ch)
                    return true;
            }
            return false;
        }
        #endregion

        #region MoveToEndOfLine
        /// <summary>
        /// Moves the current position to the first character that is part of a newline
        /// </summary>
        public void MoveToEndOfLine()
        {
            char c = Peek();
            while (c != '\r' && c != '\n' && !EndOfText)
            {
                MoveAhead();
                c = Peek();
            }
        }
        #endregion

        #region MovePastWhitespace
        /// <summary>
        /// Moves the current position to the next character that is not whitespace
        /// </summary>
        public void MovePastWhitespace()
        {
            while (Char.IsWhiteSpace(Peek()))
                MoveAhead();
        }
        #endregion
        #endregion
    }
}
