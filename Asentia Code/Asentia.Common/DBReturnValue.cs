﻿using System.ComponentModel;

namespace Asentia.Common
{
    /// <summary>
    /// Database Return Values.
    /// </summary>
    /// <remarks>
    /// Used by Stored Procedures that need to return a status to the calling code.
    /// 
    /// Note: When adding values to this enum, you also need to add corresponding excpetions to
    /// Asentia.Common.DatabaseException, and you need to account for the new value you've added
    /// in the "switch" statement in the AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError
    /// method.
    /// </remarks>
    /// <seealso cref="DatabaseDetailsNotFoundException" />
    /// <seealso cref="DatabaseFieldNotUniqueException" />
    /// <seealso cref="DatabaseCallerPermissionException" />
    /// <seealso cref="DatabaseFieldConstraintException" />
    /// <seealso cref="DatabaseSpecifiedLanguageNotDefaultException" />
    /// <seealso cref="DatabaseSpecifiedLanguageNotFoundException" />
    public enum DBReturnValue
    {
        /// <summary>
        /// OK.
        /// </summary>
        /// <remarks>
        /// The stored procedure executed normally.
        /// </remarks>
        [Description("OK")]
        OK = 0,

        /// <summary>
        /// Details Not Found.
        /// </summary>
        /// <remarks>
        /// This return value signals to the calling code that no row was found for the supplied
        /// primary key. The code will throw <see cref="DatabaseDetailsNotFoundException" />.
        /// </remarks>
        [Description("Details Not Found")]
        DetailsNotFound = 1,

        /// <summary>
        /// Field Not Unique.
        /// </summary>
        /// <remarks>
        /// This return value signals to the calling code that one of the fields supplied
        /// must be unique, and the stored procedure has failed. The code will throw
        /// <see cref="DatabaseFieldNotUniqueException" />.
        /// </remarks>
        [Description("Field Not Unique")]
        FieldNotUnique = 2,

        /// <summary>
        /// Caller (User) Permission Error.
        /// </summary>
        /// <remarks>
        /// This return value signals to the calling code that an caller (user) permission
        /// error has occurred while executing the stored procedure. This means that the user
        /// the stored procedure is called on behalf of does not have permission to access/modify
        /// the object. The code will throw <see cref="DatabaseCallerPermissionException" />.
        /// </remarks>
        [Description("Caller Permission Error")]
        CallerPermissionError = 3,

        /// <summary>
        /// Field Constraint Error.
        /// </summary>
        /// <remarks>
        /// This return value signals to the calling code that the supplied parameters are
        /// violating a business rule. The code will throw <see cref="DatabaseFieldConstraintException" />.
        /// </remarks>
        [Description("Field Constraint Error")]
        FieldConstraintError = 4,

        /// <summary>
        /// Specified Language Not Default.
        /// </summary>
        /// <remarks>
        /// This return value signals to the calling code that the language parameter specified is
        /// not the default language. The code will throw <see cref="DatabaseSpecifiedLanguageNotDefaultException" />.
        /// </remarks>
        [Description("Specified Language Not Default")]
        SpecifiedLanguageNotDefault = 5,

        /// <summary>
        /// Specified Language Not Found or Invalid
        /// </summary>
        /// <remarks>
        /// This return value signals to the calling code that the language parameter specified was
        /// not found or invalid. The code will throw <see cref="DatabaseSpecifiedLanguageNotFoundException" />.
        /// </remarks>
        [Description("Specified Language Not Found or Invalid")]
        SpecifiedLanguageNotFound = 6,

        /// <summary>
        /// Object Not Unique.
        /// </summary>
        /// <remarks>
        /// This return value signals to the calling code that a combination of fields supplied
        /// must be unique, and the stored procedure has failed. The code will throw
        /// <see cref="DatabaseObjectNotUniqueException" />.
        /// </remarks>
        [Description("Object Not Unique")]
        ObjectNotUnique = 7,
    }
}