﻿using System;
using System.Net.Mail;

namespace Asentia.Common
{
    public class ReportSubscriptionEmail : EmailTemplate
    {
        #region Properties
        public string From = String.Empty;
        public string SystemFromAddressOverride = String.Empty;
        public string SystemSmtpServerNameOverride = String.Empty;
        public int SystemSmtpServerPortOverride = 0;
        public bool SystemSmtpServerUseSslOverride = false;
        public string SystemSmtpServerUsernameOverride = String.Empty;
        public string SystemSmtpServerPasswordOverride = String.Empty;
        public MailPriority Priority = MailPriority.Normal;

        public string RecipientId = String.Empty;
        public string RecipientSubscriptionToken = String.Empty;
        #endregion

        #region Constructors
        public ReportSubscriptionEmail(int? priority, string language, string systemFromAddressOverride, string systemSmtpServerNameOverride, int systemSmtpServerPortOverride, bool systemSmtpServerUseSslOverride, string systemSmtpServerUsernameOverride, string systemSmtpServerPasswordOverride, string asentiaApplicationRootPath)
        {
            // path to the email notification's XML file
            string templateFilePath = asentiaApplicationRootPath + "_config\\default\\emailNotifications\\ReportSubscription.xml";

            // set the language
            this.Language = language;

            // set the from property
            this.From = null;

            // set the system from address override
            if (!String.IsNullOrWhiteSpace(systemFromAddressOverride))
            { this.SystemFromAddressOverride = systemFromAddressOverride; }
            else
            { this.SystemFromAddressOverride = null; }

            // set the system smtp server name override
            if (!String.IsNullOrWhiteSpace(systemSmtpServerNameOverride))
            { this.SystemSmtpServerNameOverride = systemSmtpServerNameOverride; }
            else
            { this.SystemSmtpServerNameOverride = null; }

            // set the system smtp server port override
            this.SystemSmtpServerPortOverride = systemSmtpServerPortOverride;

            // set the system smtp server use ssl override
            this.SystemSmtpServerUseSslOverride = systemSmtpServerUseSslOverride;

            // set the system smtp server username override
            if (!String.IsNullOrWhiteSpace(systemSmtpServerUsernameOverride))
            { this.SystemSmtpServerUsernameOverride = systemSmtpServerUsernameOverride; }
            else
            { this.SystemSmtpServerUsernameOverride = null; }

            // set the system smtp server password override
            if (!String.IsNullOrWhiteSpace(systemSmtpServerPasswordOverride))
            { this.SystemSmtpServerPasswordOverride = systemSmtpServerPasswordOverride; }
            else
            { this.SystemSmtpServerPasswordOverride = null; }

            // set the priority
            if (priority == null)
            { this.Priority = MailPriority.Normal; }
            else
            {
                if (priority == 1)
                { this.Priority = MailPriority.Low; }
                else if (priority == 2)
                { this.Priority = MailPriority.High; }
                else
                { this.Priority = MailPriority.Normal; }
            }

            // load the message body
            this._LoadMessageBody(templateFilePath);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sends the email.
        /// </summary>
        public override void Send()
        {
            // replace specific placeholders
            this._ReplacePlaceholder("recipient_id", this.RecipientId);
            this._ReplacePlaceholder("recipient_subscription_token", this.RecipientSubscriptionToken);

            try
            {
                // send the email
                this._SendEmail(this.RecipientEmail,
                                this.From,
                                null,
                                null,
                                null,
                                this.Priority,
                                false,
                                this.SystemFromAddressOverride,
                                this.SystemSmtpServerNameOverride,
                                this.SystemSmtpServerPortOverride,
                                this.SystemSmtpServerUseSslOverride,
                                this.SystemSmtpServerUsernameOverride,
                                this.SystemSmtpServerPasswordOverride);
            }
            catch
            { throw; }
        }
        #endregion
    }
}
