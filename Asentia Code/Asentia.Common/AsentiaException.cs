﻿using System;

namespace Asentia.Common
{
    /// <summary>
    /// The base exception from which all application generated Exceptions
    /// are derived.
    /// </summary>
    public class AsentiaException : Exception
    {
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public AsentiaException()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message</param>
        public AsentiaException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message</param>
        /// <param name="innerException">inner exception</param>
        public AsentiaException(string message, Exception innerException)
            : base(message, innerException)
		{ ;}
    }
}
