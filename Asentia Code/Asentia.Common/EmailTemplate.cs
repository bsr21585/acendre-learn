﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace Asentia.Common
{
    #region EmailTemplateException
    /// <summary>
    /// Thrown when an Exception occurs while working with <see cref="EmailTemplate" /> 
    /// </summary>
    public class EmailTemplateException : AsentiaException
    {
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public EmailTemplateException()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message</param>
        public EmailTemplateException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message</param>
        /// <param name="innerException">inner exception</param>
        public EmailTemplateException(string message, Exception inner)
            : base(message, inner)
        { ;}
    }
    #endregion

    /// <summary>
    /// Abstract class that exposes an easy to use email templating system.
    /// Email bodies are stored as text files with variable placeholders in them.
    /// </summary>
    public abstract class EmailTemplate
    {
        #region Properties
        public string Language = String.Empty;
        
        /// <summary>
        /// An List for attachments.
        /// </summary>
        public List<Attachment> Attachments = new List<Attachment>();

        public string LmsUrl = String.Empty;
        public string LmsHostname = String.Empty;

        public string ObjectName = String.Empty;
        public string EventDate = String.Empty;
        public string EventTime = String.Empty;
        public string EmailOptOutLink = String.Empty;

        public string UserFullName = String.Empty;
        public string UserFirstName = String.Empty;
        public string UserLogin = String.Empty;
        public string UserEmail = String.Empty;

        public string RecipientFullName = String.Empty;
        public string RecipientFirstName = String.Empty;
        public string RecipientLogin = String.Empty;
        public string RecipientEmail = String.Empty;

        public string Body { get { return this._MessageBody; } }
        public string Subject { get { return this._MessageSubject; } }
        #endregion

        #region Private Properties
        /// <summary>
        /// Email Message Subject.
        /// </summary>
        /// <remarks>
        /// May or may not contain variable PlaceHolders, depending on whether or not
        /// a template is used, and how many fields have already been substituted by
        /// <see cref="EnterField" />.
        /// </remarks>
        private string _MessageSubject = String.Empty;

        /// <summary>
        /// Email Message Body.
        /// </summary>
        /// <remarks>
        /// May or may not contain variable PlaceHolders, depending on whether or not
        /// a template is used, and how many fields have already been substituted by
        /// <see cref="EnterField" />.
        /// </remarks>
        private string _MessageBody = String.Empty;

        /// <summary>
        /// Left side variable placeholder.
        /// </summary>
        /// <remarks>
        /// Means that the enclosed text in the template is to be treated as a
        /// variable. The default value is ##;.
        /// </remarks>
        private string _LeftPlaceHolderIdentifier = "##";

        /// <summary>
        /// Right side variable placeholder.
        /// </summary>
        /// <remarks>
        /// Means that the enclosed text in the template is to be treated as a
        /// variable. The default value is ##.
        /// </remarks>
        private string _RightPlaceHolderIdentifier = "##";
        #endregion

        #region Methods
        #region Send
        /// <summary>
        /// The final step of the email template process where email is actually sent.
        /// This routine should call <see cref="_ReplacePlaceholder" /> for every variable
        /// substitution that is performed, then call <see cref="_SendEmail" />.
        /// </summary>
        public abstract void Send();
        #endregion
        #endregion

        #region Private/Protected Methods
        #region _SendEmail
        /// <summary>
        /// Send the Email
        /// </summary>
        /// <exception cref="EmailTemplateException" />
        /// <param name="emailTo">email to address</param>
        /// <param name="emailFrom">email from address</param>
        /// <param name="emailReplyTo">email reply-to address</param>
        /// <param name="emailCc">email cc address</param>
        /// <param name="emailBcc">email bcc address</param>
        /// <param name="priority">email priority</param>
        /// <param name="sendAsHtml">send HTML mail?</param>
        protected void _SendEmail(string emailTo,
                                  string emailFrom,
                                  string emailReplyTo,
                                  string emailCc,
                                  string emailBcc,
                                  MailPriority priority,
                                  bool sendAsHtml,
                                  string systemFromAddressOverride,
                                  string systemSmtpServerNameOverride,
                                  int systemSmtpServerPortOverride,
                                  bool systemSmtpServerUseSslOverride,
                                  string systemSmtpServerUsernameOverride,
                                  string systemSmtpServerPasswordOverride)
        {
            try
            {                
                // if the from address is null or empty, make the from address the system default email address
                //if (String.IsNullOrWhiteSpace(emailFrom))
                //{ emailFrom = Config.EmailSettings.SystemEmailAddress; }

                // replace placeholders that are common to all email notifications
                this._ReplaceCommonPlaceholders();

                // declare smtp client and mail message
                SmtpClient mailClient = new SmtpClient();
                MailMessage emailMessage = new MailMessage();

                // set to and from of email
                emailMessage.To.Add(new MailAddress(emailTo));

                if (!String.IsNullOrWhiteSpace(systemFromAddressOverride))
                { emailMessage.From = (new MailAddress(systemFromAddressOverride)); }
                else
                { emailMessage.From = (new MailAddress(Config.EmailSettings.SystemEmailAddress)); }

                // "from" is sender if we want "on behalf of"
                if (!String.IsNullOrWhiteSpace(emailFrom))
                { emailMessage.Sender = (new MailAddress(emailFrom)); }

                // set reply-to, if necessary
                if (!String.IsNullOrWhiteSpace(emailReplyTo))
                { emailMessage.ReplyToList.Add(new MailAddress(emailReplyTo)); }

                // set message priority
                emailMessage.Priority = priority;

                // set cc, if necessary
                if (!String.IsNullOrWhiteSpace(emailCc))
                {
                    string[] emailAllCc = emailCc.Split(';');
                    foreach (string emailCopyTo in emailAllCc)
                    {
                        emailMessage.CC.Add(new MailAddress(emailCopyTo));
                    }                 
                }

                // set bcc, if necessary
                if (!String.IsNullOrWhiteSpace(emailBcc))
                { emailMessage.Bcc.Add(new MailAddress(emailBcc)); }

                // set subject and encoding
                emailMessage.Subject = this._MessageSubject;
                emailMessage.SubjectEncoding = Encoding.UTF8;

                // set body and encoding
                emailMessage.Body = this._MessageBody;
                emailMessage.BodyEncoding = Encoding.UTF8;
                
                // send as html?
                if (sendAsHtml)
                { emailMessage.IsBodyHtml = true; }

                // loop through any attachements and add them to this message
                foreach (Attachment attachment in Attachments)
                {
                    emailMessage.Attachments.Add(attachment);
                }

                if (
                    !String.IsNullOrWhiteSpace(systemSmtpServerNameOverride)
                    && systemSmtpServerPortOverride > 0
                    )
                {
                    // smtp server settings from override
                    mailClient.Host = systemSmtpServerNameOverride;
                    mailClient.Port = systemSmtpServerPortOverride;
                    mailClient.EnableSsl = systemSmtpServerUseSslOverride;

                    // if credentials are set, apply them
                    if (!String.IsNullOrWhiteSpace(systemSmtpServerUsernameOverride)
                        && !String.IsNullOrWhiteSpace(systemSmtpServerPasswordOverride))
                    {
                        mailClient.Credentials = new NetworkCredential(
                            systemSmtpServerUsernameOverride,
                            systemSmtpServerPasswordOverride);
                    }
                }
                else
                {
                    // smtp server settings from Web.config
                    mailClient.Host = Config.EmailSettings.SmtpServer;
                    mailClient.Port = Config.EmailSettings.SmtpPort;
                    mailClient.EnableSsl = Config.EmailSettings.SmtpUseSsl;

                    // if credentials are set, apply them
                    if (!String.IsNullOrWhiteSpace(Config.EmailSettings.SmtpUsername)
                        && !String.IsNullOrWhiteSpace(Config.EmailSettings.SmtpPassword))
                    {
                        mailClient.Credentials = new NetworkCredential(
                            Config.EmailSettings.SmtpUsername,
                            Config.EmailSettings.SmtpPassword);
                    }
                }

                // make sure at least to, subject, and body are populated; if not, error
                if (String.IsNullOrWhiteSpace(emailTo) || String.IsNullOrWhiteSpace(this._MessageSubject) || String.IsNullOrWhiteSpace(this._MessageBody))
                { throw new EmailTemplateException(_GlobalResources.CannotSendEmailOneOrMoreRequiredEmailFieldsToFromSubjectBodyAreMissing); }

                // send the message
                mailClient.Send(emailMessage);
            }
            catch (Exception e)
            {
                throw new EmailTemplateException(e.Message, e);
            }
        }

        /// <summary>
        /// Send the Email - Overloaded
        /// </summary>
        /// <exception cref="EmailTemplateException" />
        /// <param name="emailTo">email to address</param>
        /// <param name="emailFrom">email from address</param>
        /// <param name="emailReplyTo">email reply-to address</param>
        /// <param name="emailCc">email cc address</param>
        /// <param name="emailBcc">email bcc address</param>
        /// <param name="sendAsHtml">send HTML mail?</param>
        protected void _SendEmail(string emailTo,
                                  string emailFrom,
                                  string emailReplyTo,
                                  string emailCc,
                                  string emailBcc,
                                  bool sendAsHtml,
                                  string systemFromAddressOverride,
                                  string systemSmtpServerNameOverride,
                                  int systemSmtpServerPortOverride,
                                  bool systemSmtpServerUseSslOverride,
                                  string systemSmtpServerUsernameOverride,
                                  string systemSmtpServerPasswordOverride)
        {
            _SendEmail(emailTo, emailFrom, emailReplyTo, emailCc, emailBcc, MailPriority.Normal, sendAsHtml, systemFromAddressOverride, systemSmtpServerNameOverride, systemSmtpServerPortOverride, systemSmtpServerUseSslOverride, systemSmtpServerUsernameOverride, systemSmtpServerPasswordOverride);
        }
        #endregion

        #region _LoadMessageBody
        /// <summary>
        /// Reads the contents of the template file.
        /// </summary>
        /// <remarks>
        /// Does not do any parsing or variable substitution.
        /// </remarks>
        /// <param name="templateFilePath">path to the template file</param>
        protected void _LoadMessageBody(string templateFilePath)
        {
            string fullTemplateFilePath = String.Empty;
            
            // if the template file path contains a \, it is already a fully mapped path
            if (!templateFilePath.Contains("\\"))
            { fullTemplateFilePath = HttpContext.Current.Server.MapPath(templateFilePath); }
            else
            { fullTemplateFilePath = templateFilePath; }
            
            // if the file exists, parse it
            if (File.Exists(fullTemplateFilePath))
            {
                XmlDocument emailNotificationXml = new XmlDocument();
                emailNotificationXml.Load(fullTemplateFilePath);              
                
                // subject
                if (!String.IsNullOrWhiteSpace(emailNotificationXml.SelectSingleNode("/emailNotification/language[@code='" + this.Language + "']/subject").InnerText))
                { this._MessageSubject = emailNotificationXml.SelectSingleNode("/emailNotification/language[@code='" + this.Language + "']/subject").InnerText; }
                else
                {
                    if (emailNotificationXml.SelectSingleNode("/emailNotification/language[@code='default']/subject") != null)
                    { this._MessageSubject = emailNotificationXml.SelectSingleNode("/emailNotification/language[@code='default']/subject").InnerText; }
                }

                // body
                if (!String.IsNullOrWhiteSpace(emailNotificationXml.SelectSingleNode("/emailNotification/language[@code='" + this.Language + "']/body").InnerText))
                { this._MessageBody = emailNotificationXml.SelectSingleNode("/emailNotification/language[@code='" + this.Language + "']/body").InnerText; }
                else
                {
                    if (!String.IsNullOrWhiteSpace(emailNotificationXml.SelectSingleNode("/emailNotification/language[@code='default']/body").InnerText))
                    { this._MessageBody = emailNotificationXml.SelectSingleNode("/emailNotification/language[@code='default']/body").InnerText; }
                }
            }
        }
        #endregion

        #region _ReplaceCommonPlaceholders
        /// <summary>
        /// Replaces all placeholders that are common to all email notifications.
        /// </summary>
        protected void _ReplaceCommonPlaceholders()
        {
            this._ReplacePlaceholder("lms_url", this.LmsUrl);
            this._ReplacePlaceholder("lms_hostname", this.LmsHostname);
            this._ReplacePlaceholder("object_name", this.ObjectName);
            this._ReplacePlaceholder("event_date", this.EventDate);
            this._ReplacePlaceholder("event_time", this.EventTime);
            this._ReplacePlaceholder("optout_link", this.EmailOptOutLink);
            this._ReplacePlaceholder("user_fullname", this.UserFullName);
            this._ReplacePlaceholder("user_firstname", this.UserFirstName);
            this._ReplacePlaceholder("user_login", this.UserLogin);
            this._ReplacePlaceholder("user_email", this.UserEmail);
            this._ReplacePlaceholder("recipient_fullname", this.RecipientFullName);
            this._ReplacePlaceholder("recipient_firstname", this.RecipientFirstName);
            this._ReplacePlaceholder("recipient_login", this.RecipientLogin);
            this._ReplacePlaceholder("recipient_email", this.RecipientEmail);
        }
        #endregion

        #region _ReplacePlaceholder
        /// <summary>
        /// Replaces a variable placeholder in the MessageSubject and MessageBody with the value 
        /// of the variable the placeholder represents.
        /// </summary>
        /// <param name="placeholderName">placeholder name (w/o the variable placeholder identifiers)</param>
        /// <param name="value">field value</param>
        protected void _ReplacePlaceholder(string placeholderName, string value)
        {
            if (!String.IsNullOrWhiteSpace(value))
            {
                this._MessageSubject = this._MessageSubject.Replace(this._LeftPlaceHolderIdentifier
                                                  + placeholderName
                                                  + this._RightPlaceHolderIdentifier,
                                                  value);

                this._MessageBody = this._MessageBody.Replace(this._LeftPlaceHolderIdentifier
                                                  + placeholderName
                                                  + this._RightPlaceHolderIdentifier,
                                                  value);
            }
            else
            {
                this._MessageSubject = this._MessageSubject.Replace(this._LeftPlaceHolderIdentifier
                                                  + placeholderName
                                                  + this._RightPlaceHolderIdentifier,
                                                  "");

                this._MessageBody = this._MessageBody.Replace(this._LeftPlaceHolderIdentifier
                                                  + placeholderName
                                                  + this._RightPlaceHolderIdentifier,
                                                  "");
            }
        }
        #endregion
        #endregion
    }
}
