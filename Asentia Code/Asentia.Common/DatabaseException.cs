﻿using System;

namespace Asentia.Common
{
    /// <summary>
    /// Thrown when an Database exception occurs. Exceptions of type 
    /// <see cref="DatabaseException" /> are generally not thrown, but
    /// the Exceptions that inherit from it are.
    /// </summary>
    /// <seealso cref="DBReturnValue" />
    /// <seealso cref="DatabaseDetailsNotFoundException" />
    /// <seealso cref="DatabaseFieldNotUniqueException" />
    /// <seealso cref="DatabaseCallerPermissionException" />
    /// <seealso cref="DatabaseFieldConstraintException" />
    /// <seealso cref="DatabaseSpecifiedLanguageNotDefaultException" />
    public class DatabaseException : AsentiaException
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public DatabaseException()
            : base()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message</param>
        public DatabaseException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message</param>
        /// <param name="innerException">inner exception</param>
        public DatabaseException(string message, Exception innerException)
            : base(message, innerException)
        { ;}
    }

    /// <summary>
    /// Thrown when the Details function of a class does not find a row given the supplied primary key.
    /// The return value from the stored procedure will be <see cref="DBReturnValue.DetailsNotFound" />
    /// </summary>
    public class DatabaseDetailsNotFoundException : DatabaseException
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public DatabaseDetailsNotFoundException()
            : base()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message.</param>
        public DatabaseDetailsNotFoundException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message.</param>
        /// <param name="innerException">inner exception.</param>
        public DatabaseDetailsNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        { ;}
    }

    /// <summary>
    /// Thrown when a table field must be unique,
    /// the return value from the stored procedure will be <see cref="DBReturnValue.FieldNotUnique" />
    /// </summary>
    /// <example>
    /// Usually occurs when the name of something must be unique. Trying to add a User
    /// with the same username as one that already exists will cause this exception to be thrown.
    /// </example>
    public class DatabaseFieldNotUniqueException : DatabaseException
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public DatabaseFieldNotUniqueException()
            : base()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message.</param>
        public DatabaseFieldNotUniqueException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message.</param>
        /// <param name="innerException">inner exception.</param>
        public DatabaseFieldNotUniqueException(string message, Exception innerException)
            : base(message, innerException)
        { ;}

    }

    /// <summary>
    /// Thrown when the caller of a stored procedure does not have permission to access/modify an object.
    /// The return value from the stored procedure will be <see cref="DBReturnValue.CallerPermissionError" />
    /// </summary>
    public class DatabaseCallerPermissionException : DatabaseException
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public DatabaseCallerPermissionException()
            : base()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message.</param>
        public DatabaseCallerPermissionException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message.</param>
        /// <param name="innerException">inner exception.</param>
        public DatabaseCallerPermissionException(string message, Exception innerException)
            : base(message, innerException)
        { ;}
    }

    /// <summary>
    /// Thrown when a Field Constraint Exception occurs in a stored procedure,
    /// the return value from the stored procedure will be <see cref="DBReturnValue.FieldConstraintError" />
    /// </summary>
    /// <example>
    /// Usually thrown when a value passed into a stored procedure violates a business rule.
    /// </example>
    public class DatabaseFieldConstraintException : DatabaseException
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public DatabaseFieldConstraintException()
            : base()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message.</param>
        public DatabaseFieldConstraintException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message.</param>
        /// <param name="innerException">inner exception.</param>
        public DatabaseFieldConstraintException(string message, Exception innerException)
            : base(message, innerException)
        { ;}
    }

    /// <summary>
    /// Thrown when an Database Specified Language Not Default Exception occurs in a stored procedure,
    /// the return value from the stored procedure will be <see cref="DBReturnValue.DatabaseSpecifiedLanguageNotDefaultException" />
    /// </summary>
    public class DatabaseSpecifiedLanguageNotDefaultException : DatabaseException
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public DatabaseSpecifiedLanguageNotDefaultException()
            : base()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message.</param>
        public DatabaseSpecifiedLanguageNotDefaultException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message</param>
        /// <param name="innerException">inner exception</param>
        public DatabaseSpecifiedLanguageNotDefaultException(string message, Exception innerException)
            : base(message, innerException)
        { ;}
    }

    /// <summary>
    /// Thrown when an Database Specified Language Not Found Exception occurs in a stored procedure,
    /// the return value from the stored procedure will be <see cref="DBReturnValue.DatabaseSpecifiedLanguageNotFoundException" />
    /// </summary>
    public class DatabaseSpecifiedLanguageNotFoundException : DatabaseException
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public DatabaseSpecifiedLanguageNotFoundException()
            : base()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message.</param>
        public DatabaseSpecifiedLanguageNotFoundException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message</param>
        /// <param name="innerException">inner exception</param>
        public DatabaseSpecifiedLanguageNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        { ;}
    }

    /// <summary>
    /// Thrown when a combination of fields must be unique,
    /// the return value from the stored procedure will be <see cref="DBReturnValue.ObjectNotUnique" />
    /// </summary>
    /// <example>
    /// Occurs when a combination of fields representing the object must be unique. Trying to add an Email 
    /// Notification with the same properties  as one that already exists will cause this exception to be thrown.
    /// </example>
    public class DatabaseObjectNotUniqueException : DatabaseException
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public DatabaseObjectNotUniqueException()
            : base()
        { ;}

        /// <summary>
        /// Constuctor which takes a message.
        /// </summary>
        /// <param name="message">error message.</param>
        public DatabaseObjectNotUniqueException(string message)
            : base(message)
        { ;}

        /// <summary>
        /// Constructor which takes a message and an inner Exception.
        /// </summary>
        /// <param name="message">error message.</param>
        /// <param name="innerException">inner exception.</param>
        public DatabaseObjectNotUniqueException(string message, Exception innerException)
            : base(message, innerException)
        { ;}

    }
}
