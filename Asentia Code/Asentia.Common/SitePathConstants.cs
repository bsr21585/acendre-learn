﻿
namespace Asentia.Common
{
    public static class SitePathConstants
    {
        // Asentia Customer Manager
        public static string CM_BIN { get { return "/bin/"; } }
        public static string CM_CSS { get { return "/_css/"; } }
        public static string CM_IMAGES { get { return "/_images/"; } }
        public static string CM_IMAGES_AVATARS { get { return CM_IMAGES + "avatars/"; } }
        public static string CM_IMAGES_BUTTONS { get { return CM_IMAGES + "buttons/"; } }
        public static string CM_IMAGES_ICONS { get { return CM_IMAGES + "icons/"; } }
        public static string CM_IMAGES_ICONS_64 { get { return CM_IMAGES_ICONS + "64/"; } }
        public static string CM_IMAGES_MASTERPAGE { get { return CM_IMAGES + "masterPage/"; } }
        public static string CM_MASTERPAGE { get { return "/_masterPage/"; } }
        public static string CM_SCRIPTS { get { return "/_scripts/"; } }
        public static string CM_UTIL { get { return "/_util/"; } }
        public static string CM_ACCOUNTS { get { return "/_accounts/"; } }
        public static string CM_TEMPLATE { get { return CM_ACCOUNTS + "_template/"; } }
        public static string CM_ACCOUNTDATABASESCRIPTS { get { return "/_accountDatabaseScripts/"; } }

        // Asentia
        public static string ACCOUNTS { get { return "/_accounts/"; } }
        public static string _BIN { get { return "/_bin/"; } }
        public static string ADMINISTRATOR { get { return "/administrator/"; } }
        public static string BIN { get { return "/bin/"; } }
        public static string CONFIG { get { return "/_config/"; } }
        public static string CSS { get { return "/_css/"; } }
        public static string DOWNLOAD { get { return "/_download/"; } }
        public static string IMAGES_ROOT { get { return "/_images/"; } }
        public static string IMAGES_FLAGS { get { return IMAGES_ROOT + "flags/"; } }
        public static string LOG { get { return "/_log/"; } }
        public static string REPORTING { get { return "/reporting/"; } }
        public static string SCRIPTS { get { return "/_scripts/"; } }
        public static string UPLOAD { get { return "/_upload/"; } }
        public static string UPLOAD_FAVICON { get { return UPLOAD + "favicon/"; } }
        public static string UPLOAD_ACTIVITYDATAIMPORT { get { return UPLOAD + "activitydataimport/"; } }
        public static string UPLOAD_AVATAR { get { return UPLOAD + "avatar/"; } }
        public static string UPLOAD_CERTIFICATEDATAIMPORT { get { return UPLOAD + "certificatedataimport/"; } }
        public static string UPLOAD_CONTENTPACKAGE { get { return UPLOAD + "contentpackage/"; } }
        public static string UPLOAD_COURSEMATERIAL { get { return UPLOAD + "coursematerial/"; } }
        public static string UPLOAD_GROUPDOCUMENT { get { return UPLOAD + "groupdocument/"; } }
        public static string UPLOAD_TASK { get { return UPLOAD + "task/"; } }
        public static string UPLOAD_IMAGE { get { return UPLOAD + "image/"; } }
        public static string UPLOAD_LEARNINGPATHMATERIAL { get { return UPLOAD + "learningpathmaterial/"; } }
        public static string UPLOAD_LESSONMATERIAL { get { return UPLOAD + "lessonmaterial/"; } }
        public static string UPLOAD_MASTHEAD { get { return UPLOAD + "masthead/"; } }
        public static string UPLOAD_USERBATCH { get { return UPLOAD + "userbatch/"; } }
        public static string UPLOAD_PROFILEFILE { get { return UPLOAD + "profilefile/"; } }
        public static string UPLOAD_ROSTERBATCH { get { return UPLOAD + "rosterbatch/"; } }
        public static string UTIL { get { return "/_util/"; } }
        public static string WAREHOUSE { get { return "/warehouse/"; } }

        public static string ACCOUNT_ROOT { get { return ACCOUNTS + AsentiaSessionState.IdAccount + "/"; } }
        public static string DEFAULT_SITE_CONFIG_ROOT { get { return CONFIG + "default/"; } }
        public static string DEFAULT_SITE_TEMPLATE_ROOT { get { return DEFAULT_SITE_CONFIG_ROOT + "templates/"; } }
        public static string DEFAULT_SITE_USERS_ROOT { get { return DEFAULT_SITE_CONFIG_ROOT + "users/"; } }
        public static string DEFAULT_SITE_GROUPS_ROOT { get { return DEFAULT_SITE_CONFIG_ROOT + "groups/"; } }
        public static string DEFAULT_SITE_CATALOGS_ROOT { get { return DEFAULT_SITE_CONFIG_ROOT + "catalogs/"; } }
        public static string DEFAULT_SITE_COURSES_ROOT { get { return DEFAULT_SITE_CONFIG_ROOT + "courses/"; } }
        public static string DEFAULT_SITE_LEARNINGPATHS_ROOT { get { return DEFAULT_SITE_CONFIG_ROOT + "learningPaths/"; } }
        public static string DEFAULT_SITE_EMAILNOTIFICATIONS_ROOT { get { return DEFAULT_SITE_CONFIG_ROOT + "emailNotifications/"; } }

        public static string SITE_CONFIG_ROOT { get { return CONFIG + AsentiaSessionState.SiteHostname + "/"; } }
        public static string SITE_LOG_ROOT { get { return LOG + AsentiaSessionState.SiteHostname + "/"; } }
        public static string SITE_WAREHOUSE_ROOT { get { return WAREHOUSE + AsentiaSessionState.SiteHostname + "/"; } }
        public static string SITE_TEMPLATE_ROOT { get { return SITE_CONFIG_ROOT + "templates/"; } }
        public static string SITE_USERS_ROOT { get { return SITE_CONFIG_ROOT + "users/"; } }
        public static string SITE_GROUPS_ROOT { get { return SITE_CONFIG_ROOT + "groups/"; } }
        public static string SITE_CATALOGS_ROOT { get { return SITE_CONFIG_ROOT + "catalogs/"; } }
        public static string SITE_COURSES_ROOT { get { return SITE_CONFIG_ROOT + "courses/"; } }
        public static string SITE_LEARNINGPATHS_ROOT { get { return SITE_CONFIG_ROOT + "learningPaths/"; } }
        public static string SITE_CERTIFICATES_ROOT { get { return SITE_CONFIG_ROOT + "certificates/"; } }
        public static string SITE_EMAILNOTIFICATIONS_ROOT { get { return SITE_CONFIG_ROOT + "emailNotifications/"; } }
        public static string SITE_STANDUPTRAINING_ROOT { get { return SITE_CONFIG_ROOT + "standuptraining/"; } }
        
        public static string SITE_TEMPLATE_ICONSETS { get { return SITE_TEMPLATE_ROOT + "iconsets/"; } }
        public static string SITE_TEMPLATE_ICONSETS_ICONSET { get { return SITE_TEMPLATE_ICONSETS + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ICONSET_ACTIVE) + "/"; } }
        public static string SITE_TEMPLATE_ICONSETS_ICONSET_64 { get { return SITE_TEMPLATE_ICONSETS_ICONSET + "64/"; } }
        public static string SITE_TEMPLATE_ICONSETS_ICONSET_PREVIEW { get { return SITE_TEMPLATE_ICONSETS + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ICONSET_PREVIEW) + "/"; } }
        public static string SITE_TEMPLATE_ICONSETS_ICONSET_PREVIEW_64 { get { return SITE_TEMPLATE_ICONSETS_ICONSET_PREVIEW + "64/"; } }
        public static string SITE_TEMPLATE_IMAGES { get { return SITE_TEMPLATE_ROOT + "images/"; } }
        public static string SITE_TEMPLATE_IMAGES_AVATARS { get { return SITE_TEMPLATE_IMAGES + "avatars/"; } }
        public static string SITE_TEMPLATE_IMAGES_BUTTONS { get { return SITE_TEMPLATE_IMAGES + "buttons/"; } }                        
        public static string SITE_TEMPLATE_IMAGES_MASTERPAGE { get { return SITE_TEMPLATE_IMAGES + "masterPage/"; } }
        public static string SITE_TEMPLATE_MASTERPAGE { get { return SITE_TEMPLATE_ROOT + "masterPage/"; } }
        public static string SITE_TEMPLATE_MENU { get { return SITE_TEMPLATE_ROOT + "menu/"; } }
        public static string SITE_TEMPLATE_THEMES_ROOT { get { return SITE_TEMPLATE_ROOT + "themes/"; } }
        public static string SITE_TEMPLATE_THEMES_THEME { get { return SITE_TEMPLATE_THEMES_ROOT + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.THEME_ACTIVE) + "/"; } }        
        public static string SITE_TEMPLATE_THEMES_THEME_CSS { get { return SITE_TEMPLATE_THEMES_THEME + "css/"; } }
        public static string SITE_TEMPLATE_THEMES_THEME_PREVIEW { get { return SITE_TEMPLATE_THEMES_ROOT + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.THEME_PREVIEW) + "/"; } }
        public static string SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS { get { return SITE_TEMPLATE_THEMES_THEME_PREVIEW + "css/"; } }
        public static string SITE_TEMPLATE_THEMES_THEME_SCREENSHOTS { get { return SITE_TEMPLATE_THEMES_THEME + "screenshots/"; } }
        public static string SITE_USERS_DEFAULT { get { return SITE_USERS_ROOT + "default/"; } }

        public static string SITE_LOG_EMAIL { get { return SITE_LOG_ROOT + "email/"; } }
        public static string SITE_LOG_LESSONDATA { get { return SITE_LOG_ROOT + "lessondata/"; } }
        public static string SITE_LOG_TRANSACTION { get { return SITE_LOG_ROOT + "transaction/"; } }
        public static string SITE_LOG_TRANSACTION_AUTHORIZE { get { return SITE_LOG_TRANSACTION + "authorize/"; } }
        public static string SITE_LOG_TRANSACTION_PAYPAL { get { return SITE_LOG_TRANSACTION + "paypal/"; } }

        public static string SITE_WAREHOUSE_DOCUMENTS_ROOT { get { return SITE_WAREHOUSE_ROOT + "documents/"; } }
        public static string SITE_WAREHOUSE_DOCUMENTS_COURSE { get { return SITE_WAREHOUSE_DOCUMENTS_ROOT + "course/"; } }
        public static string SITE_WAREHOUSE_DOCUMENTS_GROUP { get { return SITE_WAREHOUSE_DOCUMENTS_ROOT + "group/"; } }
        public static string SITE_WAREHOUSE_DOCUMENTS_LEARNINGPATH { get { return SITE_WAREHOUSE_DOCUMENTS_ROOT + "learningpath/"; } }
        public static string SITE_WAREHOUSE_DOCUMENTS_LESSON { get { return SITE_WAREHOUSE_DOCUMENTS_ROOT + "lesson/"; } }
        public static string SITE_WAREHOUSE_REPORTDATA { get { return SITE_WAREHOUSE_ROOT + "reportdata/"; } }
        public static string SITE_WAREHOUSE_ANALYTICDATA { get { return SITE_WAREHOUSE_ROOT + "analyticdata/"; } }
        public static string SITE_WAREHOUSE_DOCUMENTS_USER { get { return SITE_WAREHOUSE_DOCUMENTS_ROOT + "user/"; } }
        
        public static string DEFAULT_SITE_TEMPLATE_ICONSETS { get { return DEFAULT_SITE_TEMPLATE_ROOT + "iconsets/"; } }
        public static string DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET { get { return DEFAULT_SITE_TEMPLATE_ICONSETS + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ICONSET_ACTIVE) + "/"; } }
        public static string DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET_64 { get { return DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET + "64/"; } }
        public static string DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET_PREVIEW { get { return DEFAULT_SITE_TEMPLATE_ICONSETS + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ICONSET_PREVIEW) + "/"; } }
        public static string DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET_PREVIEW_64 { get { return DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET_PREVIEW + "64/"; } }
        public static string DEFAULT_SITE_TEMPLATE_IMAGES { get { return DEFAULT_SITE_TEMPLATE_ROOT + "images/"; } }
        public static string DEFAULT_SITE_TEMPLATE_IMAGES_AVATARS { get { return DEFAULT_SITE_TEMPLATE_IMAGES + "avatars/"; } }
        public static string DEFAULT_SITE_TEMPLATE_IMAGES_BUTTONS { get { return DEFAULT_SITE_TEMPLATE_IMAGES + "buttons/"; } }
        public static string DEFAULT_SITE_TEMPLATE_IMAGES_CERT { get { return DEFAULT_SITE_TEMPLATE_IMAGES + "cert/"; } }
        public static string DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE { get { return DEFAULT_SITE_TEMPLATE_IMAGES + "masterPage/"; } }
        public static string DEFAULT_SITE_TEMPLATE_MASTERPAGE { get { return DEFAULT_SITE_TEMPLATE_ROOT + "masterPage/"; } }
        public static string DEFAULT_SITE_TEMPLATE_MENU { get { return DEFAULT_SITE_TEMPLATE_ROOT + "menu/"; } }
        public static string DEFAULT_SITE_TEMPLATE_THEMES_ROOT { get { return DEFAULT_SITE_TEMPLATE_ROOT + "themes/"; } }
        public static string DEFAULT_SITE_TEMPLATE_THEMES_THEME { get { return DEFAULT_SITE_TEMPLATE_THEMES_ROOT + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.THEME_ACTIVE) + "/"; } }
        public static string DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS { get { return DEFAULT_SITE_TEMPLATE_THEMES_THEME + "css/"; } }
        public static string DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW { get { return DEFAULT_SITE_TEMPLATE_THEMES_ROOT + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.THEME_PREVIEW) + "/"; } }
        public static string DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS { get { return DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW + "css/"; } }
        public static string DEFAULT_SITE_TEMPLATE_THEMES_THEME_SCREENSHOTS { get { return DEFAULT_SITE_TEMPLATE_THEMES_THEME + "screenshots/"; } }
        public static string DEFAULT_SITE_USERS_DEFAULT { get { return DEFAULT_SITE_USERS_ROOT + "default/"; } }

        // SCORM XSD
        public static string SCORM_MANIFEST_XSD_DIRECTORY { get { return ADMINISTRATOR + "packages/SCORM/MANIFEST_XSD/"; } }

        // API Schema
        public static string API_Schema { get { return UTIL + "API/Schema/"; } }
    }
}
