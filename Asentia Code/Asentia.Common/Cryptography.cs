﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Asentia.Common
{
    public class Cryptography
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Cryptography()
        { ;}
        #endregion

        #region Properties
        /// <summary>
        /// Enumeration of encryption types.
        /// </summary>
        public enum HashType : int { MD5, SHA1, SHA256, SHA512 };
        #endregion

        #region Methods
        #region GetHash
        /// <summary>
        /// Takes in a string and hashes it according to the given HashType.
        /// </summary>
        /// <param name="plainString">plain string to hash</param>
        /// <param name="hashType">the encryption method to hash the string in</param>
        /// <param name="secretKey">secret key to use in hashing the string, optional</param>
        /// <returns>Hashed string.</returns>
        public static string GetHash(string plainString, HashType hashType, string secretKey = null)
        {
            string returnString;

            switch (hashType)
            {
                case HashType.MD5:
                    returnString = _GetMD5(plainString, secretKey);
                    break;
                case HashType.SHA1:
                    returnString = _GetSHA1(plainString, secretKey);
                    break;
                case HashType.SHA256:
                    returnString = _GetSHA256(plainString, secretKey);
                    break;
                case HashType.SHA512:
                    returnString = _GetSHA512(plainString, secretKey);
                    break;
                default:
                    throw new AsentiaException("Invalid HashType");
            }

            return returnString;
        }
        #endregion
        
        #region CompareHash
        /// <summary>
        /// Compares a hashed string to an unhashed string by hahsing the unhashed
        /// string. 
        /// </summary>
        /// <param name="strOriginal">Plain string to hash and compare to strHash</param>
        /// <param name="strHash">Hashed string for comparison</param>
        /// <param name="hshType">The encryption method to hash the string in</param>
        /// <param name="secretKey">secret key to use in hashing the string, optional</param>
        /// <returns>True if the strings match, false if not</returns>
        public static bool CompareHash(string strOriginal, string strHash, HashType hshType, string secretKey = null)
        {
            string strOrigHash = GetHash(strOriginal, hshType, secretKey);
            return (strOrigHash == strHash.ToUpper());
        }
        #endregion
        #endregion

        #region Private Methods
        #region _GetMD5
        /// <summary>
        /// Takes in a string and hashes it in MD5.
        /// </summary>
        /// <param name="input">String to hash</param>
        /// <param name="secretKey">secret key to use in hashing the string, optional</param>
        /// <returns>Hashed string</returns>
        private static string _GetMD5(string input, string secretKey = null)
        {
            if (!String.IsNullOrWhiteSpace(secretKey))
            {
                HMACMD5 hmacsMd5 = new HMACMD5();

                // get UTF8 bytes for input string
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);

                // get UTF8 bytes for key
                byte[] keyBytes = Encoding.UTF8.GetBytes(secretKey);

                // set the key
                hmacsMd5.Key = keyBytes;

                // do the hash
                byte[] hashedBytes;
                hashedBytes = hmacsMd5.ComputeHash(inputBytes);

                // convert hashed bytes to string
                string strHex = String.Empty;

                foreach (byte b in hashedBytes)
                { strHex += String.Format("{0:x2}", b); }

                // return
                return strHex.ToUpper();
            }
            else
            {
                MD5 md5 = new MD5CryptoServiceProvider();

                // get UTF8 bytes for input string
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);

                // do the hash
                byte[] hashedBytes;
                hashedBytes = md5.ComputeHash(inputBytes);

                // convert hashed bytes to string
                string strHex = String.Empty;

                foreach (byte b in hashedBytes)
                { strHex += String.Format("{0:x2}", b); }

                // return
                return strHex.ToUpper();
            }
        }
        #endregion

        #region _GetSHA1
        /// <summary>
        /// Takes in a string and hashes it in SHA1.
        /// </summary>
        /// <param name="input">String to hash</param>
        /// <param name="secretKey">secret key to use in hashing the string, optional</param>
        /// <returns>Hashed string</returns>
        private static string _GetSHA1(string input, string secretKey = null)
        {
            if (!String.IsNullOrWhiteSpace(secretKey))
            {
                HMACSHA1 hmacsSha1 = new HMACSHA1();

                // get UTF8 bytes for input string
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);

                // get UTF8 bytes for key
                byte[] keyBytes = Encoding.UTF8.GetBytes(secretKey);

                // set the key
                hmacsSha1.Key = keyBytes;

                // do the hash
                byte[] hashedBytes;
                hashedBytes = hmacsSha1.ComputeHash(inputBytes);

                // convert hashed bytes to string
                string strHex = String.Empty;

                foreach (byte b in hashedBytes)
                { strHex += String.Format("{0:x2}", b); }

                // return
                return strHex.ToUpper();
            }
            else
            {
                SHA1Managed sha1 = new SHA1Managed();

                // get UTF8 bytes for input string
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);

                // do the hash
                byte[] hashedBytes;
                hashedBytes = sha1.ComputeHash(inputBytes);

                // convert hashed bytes to string
                string strHex = String.Empty;

                foreach (byte b in hashedBytes)
                { strHex += String.Format("{0:x2}", b); }

                // return
                return strHex.ToUpper();
            }
        }
        #endregion

        #region _GetSHA256
        /// <summary>
        /// Takes in a string and hashes it in SHA256.
        /// </summary>
        /// <param name="input">String to hash</param>
        /// <param name="secretKey">secret key to use in hashing the string, optional</param>
        /// <returns>Hashed string</returns>
        private static string _GetSHA256(string input, string secretKey = null)
        {
            if (!String.IsNullOrWhiteSpace(secretKey))
            {
                HMACSHA256 hmacsSha256 = new HMACSHA256();

                // get UTF8 bytes for input string
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);

                // get UTF8 bytes for key
                byte[] keyBytes = Encoding.UTF8.GetBytes(secretKey);

                // set the key
                hmacsSha256.Key = keyBytes;

                // do the hash
                byte[] hashedBytes;
                hashedBytes = hmacsSha256.ComputeHash(inputBytes);

                // convert hashed bytes to string
                string strHex = String.Empty;

                foreach (byte b in hashedBytes)
                { strHex += String.Format("{0:x2}", b); }

                // return
                return strHex.ToUpper();
            }
            else
            {
                SHA256Managed sha256 = new SHA256Managed();

                // get UTF8 bytes for input string
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);

                // do the hash
                byte[] hashedBytes;
                hashedBytes = sha256.ComputeHash(inputBytes);

                // convert hashed bytes to string
                string strHex = String.Empty;

                foreach (byte b in hashedBytes)
                { strHex += String.Format("{0:x2}", b); }

                // return
                return strHex.ToUpper();
            }
        }
        #endregion

        #region _GetSHA512
        /// <summary>
        /// Takes in a string and hashes it in SHA512.
        /// </summary>
        /// <param name="input">String to hash</param>
        /// <param name="secretKey">secret key to use in hashing the string, optional</param>
        /// <returns>Hashed string</returns>
        private static string _GetSHA512(string input, string secretKey = null)
        {
            if (!String.IsNullOrWhiteSpace(secretKey))
            {
                HMACSHA512 hmacsSha512 = new HMACSHA512();

                // get UTF8 bytes for input string
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);

                // get UTF8 bytes for key
                byte[] keyBytes = Encoding.UTF8.GetBytes(secretKey);

                // set the key
                hmacsSha512.Key = keyBytes;

                // do the hash
                byte[] hashedBytes;
                hashedBytes = hmacsSha512.ComputeHash(inputBytes);

                // convert hashed bytes to string
                string strHex = String.Empty;

                foreach (byte b in hashedBytes)
                { strHex += String.Format("{0:x2}", b); }

                // return
                return strHex.ToUpper();
            }
            else
            {
                SHA512Managed sha512 = new SHA512Managed();

                // get UTF8 bytes for input string
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);

                // do the hash
                byte[] hashedBytes;
                hashedBytes = sha512.ComputeHash(inputBytes);

                // convert hashed bytes to string
                string strHex = String.Empty;

                foreach (byte b in hashedBytes)
                { strHex += String.Format("{0:x2}", b); }

                // return
                return strHex.ToUpper();
            }
        }
        #endregion
        #endregion
    }
}
