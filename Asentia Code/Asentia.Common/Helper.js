﻿// All COMMON JAVASCRIPT HELPER FUNCTIONS SHOULD BE DEFINED HERE.

// Constructor
function Helper()
{ }

// Function: AddClass
// Adds a CSS class to an element.
Helper.AddClass = function (element, name) {
    if (!Helper.HasClass(element, name)) { 
        element.className += (element.className ? ' ' : '') + name; 
    }
};

// Function: AddClass
// Checks to see whether or not an element has a class.
Helper.HasClass = function (element, name) {
    return new RegExp('(\\s|^)' + name + '(\\s|$)').test(element.className);
};

// Function: RemoveClass
// Removes a CSS class from an element.
Helper.RemoveClass = function (element, name) {
   if (Helper.HasClass(element, name)) {
       element.className = element.className.replace(new RegExp('(\\s|^)' + name + '(\\s|$)'), ' ').replace(/^\s+|\s+$/g, '');
   }
};

// Function: RemoveAllClasses
// Removes all CSS classes from an element.
Helper.RemoveAllClasses = function (element) {
    element.className = "";
};

// Function: GetElementStaticId
// Gets the static id of an element.
// ASP.NET AJAX likes to auto id elements with the id's of
// parent elements, plus the id of the element itself separated
// by $ even though the ClientIDMode of everything is static.
// So, this function gets the true id of the element by splitting
// on $ and returning the last value.
Helper.GetElementStaticId = function (elementId) {
    if (elementId.indexOf("$") > 0) {
        var stringArray = elementId.split("$");
        return stringArray[stringArray.length - 1];
    }
    else {
        return elementId;
    }
};

// Function: SetContentContainerHeights
// Sets the heights of content containers based on the content contained within.
// This is used to properly set heights for the administrator menu that runs the height of the page.
Helper.SetContentContainerHeights = function () {    
    var contentContainerHeight = $("#PageContentContainer").outerHeight(true);
    var windowHeightWithoutMasthead = $(window).outerHeight(true) - $("#MastheadContainer").outerHeight(true);
    if (windowHeightWithoutMasthead > contentContainerHeight) {
        $("#MasterContentLeft").height(windowHeightWithoutMasthead);
        $("#MasterContentRight").height(windowHeightWithoutMasthead);
    }
    else {
        $("#MasterContentLeft").height(contentContainerHeight);
        $("#MasterContentRight").height(contentContainerHeight);
    }
};

// Function: ShowHideContainer
Helper.ShowHideContainer = function (expandCollapseImageId, containerId, expandImagePath, collapseImagePath) {
    var container = $("#" + containerId);
    var expandCollapseImage = $("#" + expandCollapseImageId);

    if (container.is(":visible")) {
        container.hide();
        expandCollapseImage.prop("src", expandImagePath);
    }
    else {
        container.show();
        expandCollapseImage.prop("src", collapseImagePath);
    }
};

// Function: ResponsiveLabelClick
Helper.ResponsiveLabelClick = function (containerId) {    
    // get the UL element associated with the tabs
    var tabULElement = $("#" + containerId + "_TabsUL");

    if (tabULElement.hasClass("ShowTabsUL")) {
        tabULElement.removeClass("ShowTabsUL");
        tabULElement.addClass("HideTabsUL");
    }
    else {
        tabULElement.removeClass("HideTabsUL");
        tabULElement.addClass("ShowTabsUL");
    }
}

// Function: SelectActiveTab
Helper.SelectActiveTab = function (containerId, tabChangeCallbackJSFunction) {    
    // get the active tab field identifier
    var activeTabFieldIdentifier = containerId + "_ActiveTab";

    // get the id of the currently active tab
    var activeTabId = $("#" + activeTabFieldIdentifier).val();

    // call Helper.ToggleTab
    if (activeTabId != null && activeTabId != "") {
        Helper.ToggleTab(activeTabId, containerId);
    }

    // if there is a page-specific callback function, call it
    if (tabChangeCallbackJSFunction != null) {
        window[tabChangeCallbackJSFunction](activeTabId, containerId);
    }
}

// Function: ToggleTab
Helper.ToggleTab = function (tabId, containerId) {
    // get the responsive label identifier associated with the tabs
    var responsiveLabelElementIdentifier = containerId + "_TabsResponsiveLabel span";

    // get the active tab field identifier
    var activeTabFieldIdentifier = containerId + "_ActiveTab";

    // get the UL element associated with the tabs
    var tabULElement = $("#" + containerId + "_TabsUL");
    
    // build the tab LI identifier
    var tabLIIdentifier = containerId + "_" + tabId + "_TabLI"

    // build the tab panel identifier
    var tabPanelIdentifier = containerId + "_" + tabId + "_TabPanel";

    // show the tab panel, mark the tab as "on", update the responsive label display, and update the active tab field
    $("#" + tabPanelIdentifier).show();
    $("#" + tabLIIdentifier).addClass("TabbedListLIOn");
    $("#" + responsiveLabelElementIdentifier).text($("#" + tabLIIdentifier).text());
    tabULElement.removeClass("ShowTabsUL");
    tabULElement.addClass("HideTabsUL");
    $("#" + activeTabFieldIdentifier).val(tabId);

    // loop through tab LI elements, and hide them if they arent the one we clicked
    tabULElement.children().each(function (index, li) {
        if (li.id != tabLIIdentifier) {
            var tabPanelToHide = li.id.toString().replace("_TabLI", "_TabPanel");

            $("#" + tabPanelToHide).hide()
            $("#" + li.id).removeClass("TabbedListLIOn");
        }
    });

    // set the container heights of the page to adjust for new tab content
    Helper.SetContentContainerHeights();

    // hide the actions panel if we are on the files tab
    if (tabId == "Files" && containerId == "UserModify") {
        $(".ActionsPanel").hide();
    } else {
        $(".ActionsPanel").show();
    }
};

// Function: ExpandParagraph
Helper.ExpandParagraph = function (id, originalHeight) {
    $("#" + id).css("height", originalHeight);

    id = id.replace("ExpandableParagraphContainer", "");

    $("#" + id + "ExpandParagraphControlsContainer").hide();
    $("#" + id + "CollapseParagraphControlsContainer").show();
};

// Function: CollapseParagraph
Helper.CollapseParagraph = function (id) {
    $("#" + id).css("height", ThresholdHeight);

    id = id.replace("ExpandableParagraphContainer", "");

    $("#" + id + "ExpandParagraphControlsContainer").show();
    $("#" + id + "CollapseParagraphControlsContainer").hide();
};

// Function: ExpandableParagraphContainer
Helper.ExpandableParagraphContainer = function () {
    $(".ExpandableParagraphContainer").each(function () {
        if ($(this).height() > ThresholdHeight) {
            var id = $(this).attr('id');
            var prefixID = $(this).attr('id').replace('ExpandableParagraphContainer', '');

            var expandPanel = "<div id=\"" + prefixID + "ExpandableParagraphControlsContainer\" class=\"ExpandableParagraphControlsContainer\">" +
            "<div id=\"" + prefixID + "ExpandParagraphControlsContainer\" class=\"ExpandParagraphControlsContainer\">" +
            "<div>" + MoreText + "</div>" +
            "<div><img class=\"SmallIcon\" onclick=\"Helper.ExpandParagraph('" + id + "','" + $(this).height() + "');\" src=\"" + ExpandArrowURL + "\" style=\"transform:rotate(90deg);\" /></div>" +
            "</div>" +
            "<div id=\"" + prefixID + "CollapseParagraphControlsContainer\" class=\"CollapseParagraphControlsContainer\">" +
            "<div>" + LessText + "</div>" +
            "<div><img class=\"SmallIcon\" onclick=\"Helper.CollapseParagraph('" + id + "');\" src=\"" + ExpandArrowURL + "\" style=\"transform:rotate(270deg);\" /></div>" +
            "</div></div>";

            $(this).after(expandPanel);
            $(this).css("height", ThresholdHeight);
            $(this).css("overflow", "hidden");
            $("#" + prefixID + "ExpandParagraphControlsContainer").show();
            $("#" + prefixID + "CollapseParagraphControlsContainer").hide();
        }
    });
};

// Function: CreateICalendarObject
Helper.CreateICalendarObject = function () {
    $("#HiddenDownloadCalendarFileButton").click();
}

// Function: SessionKeepAlive
Helper.SessionKeepAlive = function (callingUserId) {
    $.ajax({
        type: "POST",
        url: "/_util/UtilityServices.asmx/SessionKeepAlive",
        data: "{idUser: " + callingUserId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        onSuccess: function (response) { },
        onError: function (response) { }
    });
}