﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;

namespace Asentia.Common
{
    public class Utility
    {
        #region Public Properties
        public static string APPLICATION_COMMON_VERSION 
        {
            get
            {
                return System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductVersion;
            }
        }
        #endregion

        #region Static Methods
        #region GenerateTokenForSSO
        /// <summary>
        /// Generates an SSO token for a user. Used in SSO processes for both the API and "jump" from Customer Manager.
        /// </summary>
        /// <param name="idCallerSite">caller site</param>
        /// <param name="callerLangString">caller language string</param>
        /// <param name="idCaller">calling user</param>
        /// <param name="idUser">user to do SSO for</param>
        /// <returns></returns>
        public static string GenerateTokenForSSO(int idCallerSite, string callerLangString, int idCaller, int idUser)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                // get the messages from the database object
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@token", null, SqlDbType.NVarChar, 40, ParameterDirection.Output);

                databaseObject.ExecuteNonQuery("[System.GenerateTokenForSSO]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // get the token
                string token = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@token"].Value);

                // return the token
                return token;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ValidateTokenForSSO
        /// <summary>
        /// Validates that an SSO token is valid. Returns the user id the token is for if valid.
        /// </summary>
        /// <param name="idCallerSite">caller site</param>
        /// <param name="callerLangString">caller language string</param>
        /// <param name="idCaller">calling user</param>
        /// <param name="token">the token to validate</param>
        /// <returns></returns>
        public static int ValidateTokenForSSO(int idCallerSite, string callerLangString, int idCaller, string token)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                // get the messages from the database object
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);
                
                databaseObject.AddParameter("@token", token, SqlDbType.NVarChar, 40, ParameterDirection.Input);
                databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Output);

                databaseObject.ExecuteNonQuery("[System.ValidateTokenForSSO]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // get the token
                int idUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);

                // return the idUser
                return idUser;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region RemoveSpecialCharactersForFileName
        /// <summary>
        /// Removes any characters that are not permitted in a file name from a string.
        /// </summary>
        public static string RemoveSpecialCharactersForFileName(string str)
        {
            StringBuilder sb = new StringBuilder();

            // remove non-permitted characters by building a string consisting of only the permitted characters
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                { sb.Append(c); }
            }

            return sb.ToString();
        }
        #endregion

        #region EscapeSpecialCharactersForJSON
        /// <summary>
        /// Escape special characters from a string.
        /// </summary>
        public static string EscapeSpecialCharactersForJSON(string str, bool forWebservice)
        {
            if (!String.IsNullOrWhiteSpace(str))
            {
                str = str.Replace(Environment.NewLine, String.Empty);

                StringBuilder sb = new StringBuilder();

                // remove non-permitted characters by building a string consisting of only the permitted characters
                if (forWebservice) // when escaping characters for JSON used with a web service
                {
                    foreach (char c in str)
                    {
                        if (c == '\\')          // backslash
                        { sb.Append("\\\\"); }
                        else if (c == '"')      // double quote
                        { sb.Append("\\\""); }
                        else if (c == '\'')     // single quote
                        { sb.Append("\\\'"); }
                        else
                        { sb.Append(c); }
                    }
                }
                else // when escaping characters for JSON used on page
                {
                    foreach (char c in str)
                    {
                        if (c == '\\')          // backslash
                        { sb.Append("\\\\\\\\"); }
                        else if (c == '"')      // double quote
                        { sb.Append("\\\\\""); }
                        else if (c == '\'')     // single quote
                        { sb.Append("\\\'"); }
                        else
                        { sb.Append(c); }
                    }
                }

                return sb.ToString();
            }
            else
            { return String.Empty; }
        }
        #endregion

        #region CalculateDirectorySizeInKb
        /// <summary>
        /// Gets the total disk space usage in KB of a directory, this includes the size
        /// of all files and directories contained within the specified directory.
        /// </summary>
        /// <param name="directoryPath">the path to the directory to calculate</param>
        /// <returns></returns>
        public static int CalculateDirectorySizeInKb(string directoryPath)
        {
            int directorySizeKb = 0;
            DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);

            // get the size of everything in the directory, including everything in child directories
            try
            {
                long totalSizeInBytes = 0;

                foreach (FileInfo fileInfo in directoryInfo.EnumerateFiles("*", SearchOption.AllDirectories))
                { totalSizeInBytes += fileInfo.Length; }

                directorySizeKb = Convert.ToInt32(totalSizeInBytes / 1024);
            }
            catch (Exception ex)
            {
                // just bury the exception, size in kb will stay 0
            }

            // return
            return directorySizeKb;
        }
        #endregion

        #region GetSizeStringFromKB
        public static string GetSizeStringFromKB(int kb)
        {
            if (kb < 1024) // if less than 1 MB, return the KB value
            { return kb.ToString() + " KB"; }
            else
            {
                decimal mbCount = Decimal.Round((decimal)kb / (decimal)1024, 2);
                decimal gbCount = Decimal.Round((decimal)mbCount / (decimal)1024, 2);

                if (gbCount >= new Decimal(1.00)) // greater than 1 GB, return GB
                { return gbCount.ToString() + " GB"; }
                else // if we get here, it is in MB, return MB
                { return mbCount.ToString() + " MB"; }
            }
        }
        #endregion

        #region CopyDirectory
        /// <summary>
        /// Copies the contents of one directory into another. Default is to recursively copy sub directories too.
        /// </summary>
        public static void CopyDirectory(string sourceDirectoryPath, string destinationDirectoryPath, bool copySubDirectories = true, bool overwriteExistingFiles = false)
        {
            DirectoryInfo sourceDir = new DirectoryInfo(sourceDirectoryPath);
            DirectoryInfo[] sourceDirSubDirs = sourceDir.GetDirectories();

            // if the source directory doesn't exist, throw an exception
            if (!sourceDir.Exists)
            { throw new AsentiaException(String.Format(_GlobalResources.TheSourceDirectoryDoesNotExistOrCouldNotBeFound, sourceDirectoryPath)); }

            // if the destination directory does not exist, create it
            if (!Directory.Exists(destinationDirectoryPath))
            { Directory.CreateDirectory(destinationDirectoryPath); }

            // get the file contents of the directory to copy
            FileInfo[] files = sourceDir.GetFiles();

            foreach (FileInfo file in files)
            {
                // create the path to the new copy of the file
                string filePath = Path.Combine(destinationDirectoryPath, file.Name);

                // copy the file
                file.CopyTo(filePath, overwriteExistingFiles);
            }

            // if copySubDirectories is true, recursively copy the sub directories
            if (copySubDirectories)
            {
                foreach (DirectoryInfo subDir in sourceDirSubDirs)
                {
                    // create the sub directory
                    string directoryPath = Path.Combine(destinationDirectoryPath, subDir.Name);

                    // copy the sub directories
                    CopyDirectory(subDir.FullName, directoryPath, copySubDirectories, overwriteExistingFiles);
                }
            }
        }
        #endregion

        #region _GetICalendarFormatedString
        /// <summary>
        /// Generates a string in iCalendar format for downloadable .ics file.
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="title"></param>
        /// <param name="location"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static string GetICalendarFormatedString(string dtStart, string dtEnd, string title, string location, string description)
        {
            // html decode the descriptions
            location = HttpUtility.HtmlDecode(location);
            description = HttpUtility.HtmlDecode(description);

            // date stamp value as current date 
            string dtStamp = DateTime.UtcNow.ToString("o");

            // generate a guid so we can have a unique identifier for the icalendar
            Guid guidIdentifier = new Guid();

            // fetch the inner text out of the html description
            var splitDescription = description.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            string formatedDescription = String.Empty;

            for (int i = 0; i < splitDescription.Length; i++)
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(splitDescription[i]);
                formatedDescription += doc.DocumentNode.InnerText + @"\\" + "n ";
            }

            // format the iCalendar message string for displaying the description along with meeting time
            string icsContent = "BEGIN:VCALENDAR\\nVERSION2.0\\nPRODID:-//ASENTIA//NONSGML v1.0//EN\\nBEGIN:VEVENT\\nUID:" + guidIdentifier.ToString() + "\\nDTSTAMP:" + dtStamp + "\\nDTSTART:" + dtStart + "\\nDTEND:" + dtEnd + "\\nSUMMARY:" + title + "\\nLOCATION:";
            icsContent += location.Replace("\n", "\\n").Replace("\r", "\\r") + "\\nDESCRIPTION:";
            icsContent += formatedDescription + "\\nEND:VEVENT\\nEND:VCALENDAR";

            // return
            return icsContent;
        }
        #endregion

        #region EmptyOldFolderItems
        /// <summary>
        /// Clears out a folder of files older than a certain age, it will also delete the folder(s) too if specified.
        /// </summary>
        /// <param name="folderPathServerMapped">Server.MapPath(ed) folder path</param>
        /// <param name="ageThresholdInMinutes">age of the files to be deleted, in minutes</param>
        /// <param name="includeDirectories">delete folders too?</param>
        public static void EmptyOldFolderItems(string folderPathServerMapped, int ageThresholdInMinutes, bool includeDirectories = false)
        {            
            DirectoryInfo dir = new DirectoryInfo(folderPathServerMapped);

            foreach (FileInfo fi in dir.GetFiles())
            {
                if (fi.LastAccessTime.AddMinutes(ageThresholdInMinutes) <= DateTime.Now)
                { fi.Delete(); }
            }

            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                EmptyOldFolderItems(di.FullName, ageThresholdInMinutes, includeDirectories);

                // only get rid of the folder if it is specified to do so, andthe folder has no files
                if (includeDirectories && di.GetFiles().Length == 0 && includeDirectories)
                { di.Delete(); }
            }
        }
        #endregion

        #region GetCSSFilePathForCKEditor
        public static string GetCSSFilePathForCKEditor(string fileName, bool getSiteSpecific)
        {
            if (getSiteSpecific)
            {
                if (AsentiaSessionState.IsThemePreviewMode)
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + fileName)))
                    { return "SiteSpecificLayoutCSSFilePath = \"" + SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + fileName + "\";"; }
                    else
                    { return "SiteSpecificLayoutCSSFilePath = \"\";"; }
                }
                else
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + fileName)))
                    { return "SiteSpecificLayoutCSSFilePath = \"" + SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + fileName + "\";"; }
                    else
                    { return "SiteSpecificLayoutCSSFilePath = \"\";"; }
                }
            }
            else
            {
                if (AsentiaSessionState.IsThemePreviewMode)
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + fileName)))
                    { return "DefaultLayoutCSSFilePath = \"" + SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + fileName + "\";"; }
                    else
                    { return "DefaultLayoutCSSFilePath = \"\";"; }
                }
                else
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + fileName)))
                    { return "DefaultLayoutCSSFilePath = \"" + SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + fileName + "\";"; }
                    else
                    { return "DefaultLayoutCSSFilePath = \"\";"; }
                }
            }
        }
        #endregion
        #endregion
    }
}
