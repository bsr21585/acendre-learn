﻿
namespace Asentia.Common
{
    /// <summary>
    /// Contains All Asentia specific configuration setting properties that are
    /// defined in the web.config
    /// </summary>
    /// <seealso cref="Config" />
    public class Config : BaseConfig
    {
        #region ApplicationSettings
        /// <summary>
        /// Configuration Settings for the application.
        public struct ApplicationSettings
        {
            /// <summary>
            /// The Configuration Section that contains the settings.
            /// </summary>
            private static string _ConfigSection = "ApplicationSettings";

            /// <summary>
            /// ApplicationName
            /// </summary>
            public static string ApplicationName
            {
                get { return ConfigString(_ConfigSection, "ApplicationName", false, "Asentia"); }
            }

            /// <summary>
            /// MaximumFileUploadSizeInBytes
            /// </summary>
            public static int MaximumFileUploadSizeInBytes
            {
                get { return ConfigInt(_ConfigSection, "MaximumFileUploadSizeInBytes", false, 1073741824); }
            }

            /// <summary>
            /// Throw404OnNonAuthenticatedPages
            /// </summary>
            public static bool Throw404OnNonAuthenticatedPages
            {
                get { return ConfigBool(_ConfigSection, "Throw404OnNonAuthenticatedPages", true, false); }
            }

            /// <summary>
            /// ApplicationLogoSize
            /// </summary>
            public static string ApplicationLogoSize
            {
                get { return ConfigString(_ConfigSection, "ApplicationLogoSize", false, "200:80"); }
            }


            /// <summary>
            /// ApplicationIconSize
            /// </summary>
            public static string ApplicationIconSize
            {
                get { return ConfigString(_ConfigSection, "ApplicationIconSize", false, "80:80"); }
            }

            /// <summary>
            /// HomePageIconSize
            /// </summary>
            public static int HomePageIconSize
            {
                get { return ConfigInt(_ConfigSection, "HomePageIconSize", false, 80); }
            }

            /// <summary>
            /// MinimumUsernameRequirementsRegEx
            /// </summary>
            public static string MinimumUsernameRequirementsRegEx
            {
                get { return ConfigString(_ConfigSection, "MinimumUsernameRequirementsRegEx", true, ""); }
            }

            /// <summary>
            /// MinimumPasswordRequirementsRegEx
            /// </summary>
            public static string MinimumPasswordRequirementsRegEx
            {
                get { return ConfigString(_ConfigSection, "MinimumPasswordRequirementsRegEx", true, ""); }
            }

            /// <summary>
            /// UsernameExclusionsRegEx
            /// </summary>
            public static string UsernameExclusionsRegEx
            {
                get { return ConfigString(_ConfigSection, "UsernameExclusionsRegEx", true, ""); }
            }

            /// <summary>
            /// PasswordExclusionsRegEx
            /// </summary>
            public static string PasswordExclusionsRegEx
            {
                get { return ConfigString(_ConfigSection, "PasswordExclusionsRegEx", true, ""); }
            }

            /// <summary>
            /// UsernameRequirementsErrorResourceKey
            /// </summary>
            public static string UsernameRequirementsErrorResourceKey
            {
                get { return ConfigString(_ConfigSection, "UsernameRequirementsErrorResourceKey", true, "UsernameDoesNotMeetTheMinimumSystemRequirementsForAPassword"); }
            }

            /// <summary>
            /// PasswordRequirementsErrorResourceKey
            /// </summary>
            public static string PasswordRequirementsErrorResourceKey
            {
                get { return ConfigString(_ConfigSection, "PasswordRequirementsErrorResourceKey", true, "PasswordDoesNotMeetTheMinimumSystemRequirementsForAPassword"); }
            }

            /// <summary>
            /// CustomerManagerSSOSecretKey
            /// </summary>
            public static string CustomerManagerSSOSecretKey
            {
                get { return ConfigString(_ConfigSection, "CustomerManagerSSOSecretKey", false, ""); }
            }

            /// <summary>
            /// GoToMeetingAuthenticationURL
            /// </summary>
            public static string GoToMeetingAuthenticationURL
            {
                get { return ConfigString(_ConfigSection, "GoToMeetingAuthenticationURL", false, ""); }
            }

            /// <summary>
            /// GoToMeetingAuthenticationURLv2
            /// </summary>
            public static string GoToMeetingAuthenticationURLv2
            {
                get { return ConfigString(_ConfigSection, "GoToMeetingAuthenticationURLv2", false, ""); }
            }

            /// <summary>
            /// GoToMeetingRestAPIURL
            /// </summary>
            public static string GoToMeetingRestAPIURL
            {
                get { return ConfigString(_ConfigSection, "GoToMeetingRestAPIURL", false, ""); }
            }

            /// <summary>
            /// GoToWebinarAuthenticationURL
            /// </summary>
            public static string GoToWebinarAuthenticationURL
            {
                get { return ConfigString(_ConfigSection, "GoToWebinarAuthenticationURL", false, ""); }
            }

            /// <summary>
            /// GoToWebinarAuthenticationURLv2
            /// </summary>
            public static string GoToWebinarAuthenticationURLv2
            {
                get { return ConfigString(_ConfigSection, "GoToWebinarAuthenticationURLv2", false, ""); }
            }

            /// <summary>
            /// GoToWebinarRestAPIURL
            /// </summary>
            public static string GoToWebinarRestAPIURL
            {
                get { return ConfigString(_ConfigSection, "GoToWebinarRestAPIURL", false, ""); }
            }

            /// <summary>
            /// GoToTrainingAuthenticationURL
            /// </summary>
            public static string GoToTrainingAuthenticationURL
            {
                get { return ConfigString(_ConfigSection, "GoToTrainingAuthenticationURL", false, ""); }
            }

            /// <summary>
            /// GoToTrainingAuthenticationURLv2
            /// </summary>
            public static string GoToTrainingAuthenticationURLv2
            {
                get { return ConfigString(_ConfigSection, "GoToTrainingAuthenticationURLv2", false, ""); }
            }

            /// <summary>
            /// GoToTrainingRestAPIURL
            /// </summary>
            public static string GoToTrainingRestAPIURL
            {
                get { return ConfigString(_ConfigSection, "GoToTrainingRestAPIURL", false, ""); }
            }

            /// <summary>
            /// WebExRestAPIURL
            /// </summary>
            public static string WebExRestAPIURL
            {
                get { return ConfigString(_ConfigSection, "WebExRestAPIURL", false, ""); }
            }

            /// <summary>
            /// EARCryptoKey - This is the Encryption At Rest Cryptography Key. It's keyed to the machine.
            /// </summary>
            public static string EARCryptoKey
            {
                get { return ConfigString(_ConfigSection, "EARCryptoKey", false, ""); }
            }

            /// <summary>
            /// OpenSesameTenantMode - The "tenant mode" for OpenSesame provisioned tenants (portals). 
            /// Can be "Demo", "QA", or "Prod" - "Prod" is default.
            /// </summary>
            public static string OpenSesameTenantMode
            {
                get { return ConfigString(_ConfigSection, "OpenSesameTenantMode", false, "Prod"); }
            }

            /// <summary>
            /// AsentiaContentSourceRootPath 
            /// </summary>
            public static string AsentiaContentSourceRootPath
            {
                get { return ConfigString(_ConfigSection, "AsentiaContentSourceRootPath", false, ""); }
            }

            /// <summary>
            /// AsentiaContentDestinationRootPath
            /// </summary>
            public static string AsentiaContentDestinationRootPath
            {
                get { return ConfigString(_ConfigSection, "AsentiaContentDestinationRootPath", false, ""); }
            }
        }
        #endregion

        #region AccountSettings
        /// <summary>
        /// Configuration Settings for the account.
        public struct AccountSettings
        {
            /// <summary>
            /// The Configuration Section that contains the settings.
            /// </summary>
            private static string _ConfigSection = "AccountSettings";

            /// <summary>
            /// BaseDomain
            /// </summary>
            public static string BaseDomain
            {
                get { return ConfigString(_ConfigSection, "BaseDomain", false); }
            }

            /// <summary>
            /// AdministratorEmail
            /// </summary>
            public static string AdministratorEmail
            {
                get { return ConfigString(_ConfigSection, "AdministratorEmail", false); }
            }

            /// <summary>
            /// SessionTimeout
            /// </summary>
            public static int SessionTimeout
            {
                get { return ConfigInt(_ConfigSection, "SessionTimeout", true, 20); }
            }

            /// <summary>
            /// Default Timezone
            /// </summary>
            public static string DefaultTimezoneDotNetName
            {
                get { return ConfigString(_ConfigSection, "DefaultTimezone", true, "Eastern Standard Time"); }
            }

            /// <summary>
            /// Default language (Code)
            /// </summary>
            public static string DefaultLanguage
            {
                get { return ConfigString(_ConfigSection, "DefaultLanguage", true, "en-US"); }
            }
        }
        #endregion

        #region SessionStateDatabaseSettings
        /// <summary>
        /// Configuration Settings that are used by <see cref="Database" />
        /// </summary>
        /// <seealso cref="Config" />
        /// <seealso cref="BaseConfig" />
        /// <seealso cref="Database" />
        public struct SessionStateDatabaseSettings
        {
            /// <summary>
            /// The Configuration Section that contains the settings for <see cref="Config.DatabaseSettings" />.
            /// </summary>
            private static string _ConfigSection = "SessionStateDatabaseSettings";

            /// <summary>
            /// Database server.
            /// </summary>
            public static string Server
            {
                get { return ConfigString(_ConfigSection, "Server", false); }
            }

            /// <summary>
            /// Database name.
            /// </summary>
            public static string Name
            {
                get { return ConfigString(_ConfigSection, "Name", false); }
            }

            /// <summary>
            /// The time (in seconds) to keep an idle connection open. The default value is 30 seconds.
            /// </summary>
            public static int ConnectionTimeout
            {
                get { return ConfigInt(_ConfigSection, "ConnectionTimeout", false, 30); }
            }

            /// <summary>
            /// The time (in seconds) to wait for the command to execute. The default value is 30 seconds.
            /// </summary>
            public static int CommandTimeout
            {
                get { return ConfigInt(_ConfigSection, "CommandTimeout", false, 30); }
            }

            /// <summary>
            /// Determines whether or not we should use connection pooling. The default value is true.
            /// </summary>
            public static bool UseConnectionPooling
            {
                get { return ConfigBool(_ConfigSection, "UseConnectionPooling", false, true); }
            }

            /// <summary>
            /// The minimum number of connections to be stored in the connection pool. The default value is 10.
            /// </summary>
            public static int MinPoolSize
            {
                get { return ConfigInt(_ConfigSection, "MinPoolSize", false, 10); }
            }

            /// <summary>
            /// The maximum number of connections to be stored in the connection pool. The default value is 200.
            /// </summary>
            public static int MaxPoolSize
            {
                get { return ConfigInt(_ConfigSection, "MaxPoolSize", false, 200); }
            }

            /// <summary>
            /// Determines whether or not we should use a trusted connection to the database. The default value is true.
            /// </summary>
            public static bool UseTrustedConnection
            {
                get { return ConfigBool(_ConfigSection, "UseTrustedConnection", false, true); }
            }

            /// <summary>
            /// Database login (if not trusted connection).
            /// </summary>
            public static string Login
            {
                get { return ConfigString(_ConfigSection, "Login", false); }
            }

            /// <summary>
            /// Database password (if not trusted connection).
            /// </summary>
            public static string Password
            {
                get { return ConfigString(_ConfigSection, "Password", false); }
            }
        }
        #endregion

        #region CustomerManagerDatabaseSettings
        /// <summary>
        /// Configuration Settings that are used by <see cref="Database" />
        /// </summary>
        /// <seealso cref="Config" />
        /// <seealso cref="BaseConfig" />
        /// <seealso cref="Database" />
        public struct CustomerManagerDatabaseSettings
        {
            /// <summary>
            /// The Configuration Section that contains the settings for <see cref="Config.DatabaseSettings" />.
            /// </summary>
            private static string _ConfigSection = "CustomerManagerDatabaseSettings";

            /// <summary>
            /// Database server.
            /// </summary>
            public static string Server
            {
                get { return ConfigString(_ConfigSection, "Server", false); }
            }

            /// <summary>
            /// Database name.
            /// </summary>
            public static string Name
            {
                get { return ConfigString(_ConfigSection, "Name", false); }
            }

            /// <summary>
            /// The time (in seconds) to keep an idle connection open. The default value is 30 seconds.
            /// </summary>
            public static int ConnectionTimeout
            {
                get { return ConfigInt(_ConfigSection, "ConnectionTimeout", false, 30); }
            }

            /// <summary>
            /// The time (in seconds) to wait for the command to execute. The default value is 30 seconds.
            /// </summary>
            public static int CommandTimeout
            {
                get { return ConfigInt(_ConfigSection, "CommandTimeout", false, 30); }
            }

            /// <summary>
            /// Determines whether or not we should use connection pooling. The default value is true.
            /// </summary>
            public static bool UseConnectionPooling
            {
                get { return ConfigBool(_ConfigSection, "UseConnectionPooling", false, true); }
            }

            /// <summary>
            /// The minimum number of connections to be stored in the connection pool. The default value is 10.
            /// </summary>
            public static int MinPoolSize
            {
                get { return ConfigInt(_ConfigSection, "MinPoolSize", false, 10); }
            }

            /// <summary>
            /// The maximum number of connections to be stored in the connection pool. The default value is 200.
            /// </summary>
            public static int MaxPoolSize
            {
                get { return ConfigInt(_ConfigSection, "MaxPoolSize", false, 200); }
            }

            /// <summary>
            /// Determines whether or not we should use a trusted connection to the database. The default value is true.
            /// </summary>
            public static bool UseTrustedConnection
            {
                get { return ConfigBool(_ConfigSection, "UseTrustedConnection", false, true); }
            }

            /// <summary>
            /// Database login (if not trusted connection).
            /// </summary>
            public static string Login
            {
                get { return ConfigString(_ConfigSection, "Login", false); }
            }

            /// <summary>
            /// Database password (if not trusted connection).
            /// </summary>
            public static string Password
            {
                get { return ConfigString(_ConfigSection, "Password", false); }
            }
        }
        #endregion

        #region AccountDatabaseSettings
        /// <summary>
        /// Configuration Settings that are used by <see cref="Database" />
        /// </summary>
        /// <seealso cref="Config" />
        /// <seealso cref="BaseConfig" />
        /// <seealso cref="Database" />
        public struct AccountDatabaseSettings
        {
            /// <summary>
            /// The Configuration Section that contains the settings for <see cref="Config.DatabaseSettings" />.
            /// </summary>
            private static string _ConfigSection = "AccountDatabaseSettings";

            /// <summary>
            /// Database server.
            /// </summary>
            public static string Server
            {
                get { return ConfigString(_ConfigSection, "Server", true); }
            }

            /// <summary>
            /// Database name.
            /// </summary>
            public static string Name
            {
                get { return ConfigString(_ConfigSection, "Name", true); }
            }

            /// <summary>
            /// The time (in seconds) to keep an idle connection open. The default value is 30 seconds.
            /// </summary>
            public static int ConnectionTimeout
            {
                get { return ConfigInt(_ConfigSection, "ConnectionTimeout", true, 30); }
            }

            /// <summary>
            /// The time (in seconds) to wait for the command to execute. The default value is 30 seconds.
            /// </summary>
            public static int CommandTimeout
            {
                get { return ConfigInt(_ConfigSection, "CommandTimeout", true, 30); }
            }

            /// <summary>
            /// Determines whether or not we should use connection pooling. The default value is true.
            /// </summary>
            public static bool UseConnectionPooling
            {
                get { return ConfigBool(_ConfigSection, "UseConnectionPooling", true, true); }
            }

            /// <summary>
            /// The minimum number of connections to be stored in the connection pool. The default value is 10.
            /// </summary>
            public static int MinPoolSize
            {
                get { return ConfigInt(_ConfigSection, "MinPoolSize", true, 10); }
            }

            /// <summary>
            /// The maximum number of connections to be stored in the connection pool. The default value is 200.
            /// </summary>
            public static int MaxPoolSize
            {
                get { return ConfigInt(_ConfigSection, "MaxPoolSize", true, 200); }
            }

            /// <summary>
            /// Determines whether or not we should use a trusted connection to the database. The default value is true.
            /// </summary>
            public static bool UseTrustedConnection
            {
                get { return ConfigBool(_ConfigSection, "UseTrustedConnection", true, true); }
            }

            /// <summary>
            /// Database login (if not trusted connection).
            /// </summary>
            public static string Login
            {
                get { return ConfigString(_ConfigSection, "Login", true); }
            }

            /// <summary>
            /// Database password (if not trusted connection).
            /// </summary>
            public static string Password
            {
                get { return ConfigString(_ConfigSection, "Password", true); }
            }
        }
        #endregion

        #region EmailSettings
        /// <summary>
        /// Configuration Settings that are used by <see cref="EmailTemplate" />
        /// </summary>
        /// <seealso cref="Config" />
        /// <seealso cref="BaseConfig" />
        /// <seealso cref="EmailTemplate" />
        public struct EmailSettings
        {
            /// <summary>
            /// The Configuration Section that contains the settings for <see cref="Config.DatabaseSettings" />.
            /// </summary>
            private static string _ConfigSection = "EmailSettings";

            /// <summary>
            /// SMTP Server
            /// </summary>
            public static string SystemEmailAddress
            {
                get { return ConfigString(_ConfigSection, "SystemEmailAddress", false, "no-reply@mycompany.com"); }
            }

            /// <summary>
            /// SMTP Server
            /// </summary>
            public static string SmtpServer
            {
                get { return ConfigString(_ConfigSection, "SmtpServer", false, "127.0.0.1"); }
            }

            /// <summary>
            /// SMTP Port
            /// </summary>
            public static int SmtpPort
            {
                get { return ConfigInt(_ConfigSection, "SmtpPort", false, 25); }
            }

            /// <summary>
            /// SMTP Username
            /// </summary>
            public static string SmtpUsername
            {
                get { return ConfigString(_ConfigSection, "SmtpUsername", false, null); }
            }

            /// <summary>
            /// SMTP Password
            /// </summary>
            public static string SmtpPassword
            {
                get { return ConfigString(_ConfigSection, "SmtpPassword", false, null); }
            }

            /// <summary>
            /// SMTP Use SSL
            /// </summary>
            public static bool SmtpUseSsl
            {
                get { return ConfigBool(_ConfigSection, "SmtpUseSsl", false, false); }
            }
        }
        #endregion

        #region OAuthSettings
        /// <summary>
        /// Configuration Settings that are used by <see cref="EmailTemplate" />
        /// </summary>
        /// <seealso cref="Config" />
        /// <seealso cref="BaseConfig" />
        /// <seealso cref="EmailTemplate" />
        public struct OAuthSettings
        {
            /// <summary>
            /// The Configuration Section that contains the settings for <see cref="Config.OAuthSettings" />.
            /// </summary>
            private static string _ConfigSection = "OAuthSettings";

            /// <summary>
            /// SMTP Server
            /// </summary>
            public static int TokenExpiryDays
            {
                get { return ConfigInt(_ConfigSection, "TokenExpiryDays", false, 2); }
            }

        }
        #endregion

        #region RegularExpressions
        /// <summary>
        /// Configuration Settings that contain regular expression strings
        /// for Regex validations.
        /// </summary>
        /// <seealso cref="Config" />
        /// <seealso cref="BaseConfig" />
        public struct RegularExpressions
        {
            /// <summary>
            /// The Configuration Section that contains the settings for <see cref="Config.RegularExpressions" />.
            /// </summary>
            private static string _ConfigSection = "RegularExpressions";

            /// <summary>
            /// Regular expression pattern for email addresses.
            /// </summary>
            /// <seealso cref="BaseConfig" />
            /// <seealso cref="Config" />
            public static string EmailPattern
            {
                get { return ConfigString(_ConfigSection, "EmailPattern", true); }
            }

            /// <summary>
            /// Regular expression pattern for a url.
            /// </summary>
            /// <seealso cref="BaseConfig" />
            /// <seealso cref="Config" />
            public static string URLPattern
            {
                get { return ConfigString(_ConfigSection, "URLPattern", true); }
            }

            /// <summary>
            /// Regular expression pattern for fully qualified domain names.
            /// </summary>
            /// <seealso cref="BaseConfig" />
            /// <seealso cref="Config" />
            public static string FQDNPattern
            {
                get { return ConfigString(_ConfigSection, "FQDNPattern", true); }
            }

            /// <summary>
            /// Regular expression pattern for file names.
            /// </summary>
            /// <seealso cref="BaseConfig" />
            /// <seealso cref="Config" />
            public static string FileNamePattern
            {
                get { return ConfigString(_ConfigSection, "FileNamePattern", true); }
            }

            /// <summary>
            /// Regular expression pattern for an ISO date.
            /// </summary>
            /// <seealso cref="BaseConfig" />
            /// <seealso cref="Config" />
            public static string ISODatePattern
            {
                get { return ConfigString(_ConfigSection, "ISODatePattern", true); }
            }

            /// <summary>
            /// Regular expression pattern for an ISO date and time.
            /// </summary>
            /// <seealso cref="BaseConfig" />
            /// <seealso cref="Config" />
            public static string ISODateTimePattern
            {
                get { return ConfigString(_ConfigSection, "ISODateTimePattern", true); }
            }
        }
        #endregion

        #region TinCanApiSettings
        /// <summary>
        /// Configuration Settings for the experiance/TinCan api.
        public struct TinCanApiSettings
        {
            /// <summary>
            /// The Configuration Section that contains the settings.
            /// </summary>
            private static string _ConfigSection = "TinCanApiSettings";


            /// <summary>
            /// Api End point
            /// </summary>
            public static string EndPoint
            {
                get { return ConfigString(_ConfigSection, "EndPoint", false, "/_util/xAPI/xAPIStatementRESTService.svc/"); }
            }


            /// <summary>
            /// AuthUser
            /// </summary>
            public static string AuthUser
            {
                get { return ConfigString(_ConfigSection, "AuthUser", false, "80519be2-139a-4e1e-b99f-066a51967102"); }
            }

            /// <summary>
            /// AuthPassword
            /// </summary>
            public static string AuthPassword
            {
                get { return ConfigString(_ConfigSection, "AuthPassword", false, "a870bb4c-a867-41d3-90f2-28e7f1b573ca"); }
            }

            /// <summary>
            /// RegistrationKey
            /// </summary>
            public static string RegistrationKey
            {
                get { return ConfigString(_ConfigSection, "RegistrationKey", false, "b17acdcc-6aba-44f4-b36f-510781c0e3b8"); }
            }

        }
        #endregion

        #region EcommerceSettings
        /// <summary>
        /// Configuration Settings for e-commerce.
        /// </summary>
        public struct EcommerceSettings
        {
            /// <summary>
            /// The Configuration Section that contains the settings.
            /// </summary>
            private static string _ConfigSection = "EcommerceSettings";

            /// <summary>
            /// AuthorizeNetAcceptJSLiveURL
            /// </summary>
            public static string AuthorizeNetAcceptJSLiveURL
            {
                get { return ConfigString(_ConfigSection, "AuthorizeNetAcceptJSLiveURL", false, null); }
            }

            /// <summary>
            /// AuthorizeNetAcceptJSTestURL
            /// </summary>
            public static string AuthorizeNetAcceptJSTestURL
            {
                get { return ConfigString(_ConfigSection, "AuthorizeNetAcceptJSTestURL", false, null); }
            }

            /// <summary>
            /// AuthorizeNetAIMLiveURL
            /// </summary>
            public static string AuthorizeNetAIMLiveURL
            {
                get { return ConfigString(_ConfigSection, "AuthorizeNetAIMLiveURL", false, null); }
            }

            /// <summary>
            /// AuthorizeNetAIMTestURL
            /// </summary>
            public static string AuthorizeNetAIMTestURL
            {
                get { return ConfigString(_ConfigSection, "AuthorizeNetAIMTestURL", false, null); }
            }

            /// <summary>
            /// PayPalIPNReturnLiveURL
            /// </summary>
            public static string PayPalIPNReturnLiveURL
            {
                get { return ConfigString(_ConfigSection, "PayPalIPNReturnLiveURL", false, null); }
            }

            /// <summary>
            /// PayPalIPNReturnTestURL
            /// </summary>
            public static string PayPalIPNReturnTestURL
            {
                get { return ConfigString(_ConfigSection, "PayPalIPNReturnTestURL", false, null); }
            }
        }

        #endregion
    }
}
