﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Web;

namespace Asentia.Common
{
    public class EventEmail : EmailTemplate
    {
        #region Properties
        public string From = String.Empty;
        public string CopyTo = String.Empty;
        public string SystemFromAddressOverride = String.Empty;
        public string SystemSmtpServerNameOverride = String.Empty;
        public int SystemSmtpServerPortOverride = 0;
        public bool SystemSmtpServerUseSslOverride = false;
        public string SystemSmtpServerUsernameOverride = String.Empty;
        public string SystemSmtpServerPasswordOverride = String.Empty;
        public MailPriority Priority = MailPriority.Normal;
        public bool IsHTMLBased;

        public string EnrollmentCourseDescription = String.Empty;
        public string EnrollmentCourseEstCompletionTime = String.Empty;
        public string EnrollmentLearningPathDescription = String.Empty;
        public string EnrollmentDueDate = String.Empty;
        public string EnrollmentExpireDate = String.Empty;
        public string EnrollmentRequestRejectionComments = String.Empty;

        public string StandupTrainingSessionMeetingTime = String.Empty;
        public string StandupTrainingSessionTimezone = String.Empty;
        public string StandupTrainingSessionLocation = String.Empty;
        public string StandupTrainingSessionLocationDescription = String.Empty;
        public string StandupTrainingSessionInstructorFullName = String.Empty;
        public string StandupTrainingSessionDropLink = String.Empty;

        public string UserRegistrationRejectionComments = String.Empty;

        public string CertificationDescription = String.Empty;

        public string DiscussionMessage = String.Empty;

        #endregion

        #region Constructors
        public EventEmail(int idEventEmailNotification, string siteHostname, string from, string copyTo, string systemFromAddressOverride, string systemSmtpServerNameOverride, int systemSmtpServerPortOverride, bool systemSmtpServerUseSslOverride, string systemSmtpServerUsernameOverride, string systemSmtpServerPasswordOverride, int? priority, bool isHTMLBased, string language, string asentiaApplicationRootPath)
        {
            // path to the email notification's XML file
            string templateFilePath = String.Empty;

            if (!String.IsNullOrWhiteSpace(asentiaApplicationRootPath)) // explicitly defined
            { templateFilePath = asentiaApplicationRootPath + "_config\\" + siteHostname + "\\emailNotifications\\" + idEventEmailNotification.ToString() + "\\EmailNotification.xml"; }
            else // site path 
            { templateFilePath = HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + "emailNotifications/" + idEventEmailNotification.ToString() + "/EmailNotification.xml"); }
            
            // set the language
            this.Language = language;

            // set the from property
            if (!String.IsNullOrWhiteSpace(from))
            { this.From = from; }
            else
            { this.From = null; }

            // set the copyTo to property
            if (!String.IsNullOrWhiteSpace(copyTo))
            { this.CopyTo = copyTo; }
            else
            { this.CopyTo = null; }

            // set the system from address override
            if (!String.IsNullOrWhiteSpace(systemFromAddressOverride))
            { this.SystemFromAddressOverride = systemFromAddressOverride; }
            else
            { this.SystemFromAddressOverride = null; }

            // set the system smtp server name override
            if (!String.IsNullOrWhiteSpace(systemSmtpServerNameOverride))
            { this.SystemSmtpServerNameOverride = systemSmtpServerNameOverride; }
            else
            { this.SystemSmtpServerNameOverride = null; }

            // set the system smtp server port override
            this.SystemSmtpServerPortOverride = systemSmtpServerPortOverride;

            // set the system smtp server use ssl override
            this.SystemSmtpServerUseSslOverride = systemSmtpServerUseSslOverride;

            // set the system smtp server username override
            if (!String.IsNullOrWhiteSpace(systemSmtpServerUsernameOverride))
            { this.SystemSmtpServerUsernameOverride = systemSmtpServerUsernameOverride; }
            else
            { this.SystemSmtpServerUsernameOverride = null; }

            // set the system smtp server password override
            if (!String.IsNullOrWhiteSpace(systemSmtpServerPasswordOverride))
            { this.SystemSmtpServerPasswordOverride = systemSmtpServerPasswordOverride; }
            else
            { this.SystemSmtpServerPasswordOverride = null; }

            // set the priority
            if (priority == null)
            { this.Priority = MailPriority.Normal; }
            else
            {
                if (priority == 1)
                { this.Priority = MailPriority.Low; }
                else if (priority == 2)
                { this.Priority = MailPriority.High; }
                else
                { this.Priority = MailPriority.Normal; }
            }

            this.IsHTMLBased = isHTMLBased;

            // load the message body
            this._LoadMessageBody(templateFilePath);
        }
        #endregion

        #region Methods
        #region Send
        /// <summary>
        /// Sends the email.
        /// </summary>
        public override void Send()
        {
            // replace specific placeholders
            this._ReplacePlaceholder("enrollment_coursedescription", this.EnrollmentCourseDescription);
            this._ReplacePlaceholder("enrollment_courseestcompletiontime", this.EnrollmentCourseEstCompletionTime);
            this._ReplacePlaceholder("enrollment_learningpathdescription", this.EnrollmentLearningPathDescription);
            this._ReplacePlaceholder("enrollment_duedate", this.EnrollmentDueDate);
            this._ReplacePlaceholder("enrollment_expiredate", this.EnrollmentExpireDate);

            this._ReplacePlaceholder("session_datetime", this.StandupTrainingSessionMeetingTime);
            this._ReplacePlaceholder("session_timezone", this.StandupTrainingSessionTimezone);
            this._ReplacePlaceholder("session_location", this.StandupTrainingSessionLocation);
            this._ReplacePlaceholder("session_locationdescription", this.StandupTrainingSessionLocationDescription);
            this._ReplacePlaceholder("session_instructorfullname", this.StandupTrainingSessionInstructorFullName);
            this._ReplacePlaceholder("session_droplink", this.StandupTrainingSessionDropLink);
            
            
            this._ReplacePlaceholder("user_registration_rejection_comments", this.UserRegistrationRejectionComments);

            this._ReplacePlaceholder("enrollment_request_rejection_comments", this.EnrollmentRequestRejectionComments);

            this._ReplacePlaceholder("certification_description", this.CertificationDescription);

            this._ReplacePlaceholder("discussion_message", this.DiscussionMessage);

            try
            {
                // send the email
                this._SendEmail(this.RecipientEmail,
                                this.From,
                                this.From,
                                this.CopyTo,
                                null,
                                this.Priority,
                                this.IsHTMLBased,
                                this.SystemFromAddressOverride,
                                this.SystemSmtpServerNameOverride,
                                this.SystemSmtpServerPortOverride,
                                this.SystemSmtpServerUseSslOverride,
                                this.SystemSmtpServerUsernameOverride,
                                this.SystemSmtpServerPasswordOverride);
            }
            catch
            { throw; }
        }
        #endregion

        #region FormatForView
        /// <summary>
        /// Saves the subject and body of the email by replacing placeholders for viewing the email
        /// </summary>
        public void FormatForView()
        {            
            // replace specific placeholders
            this._ReplacePlaceholder("enrollment_coursedescription", this.EnrollmentCourseDescription);
            this._ReplacePlaceholder("enrollment_courseestcompletiontime", this.EnrollmentCourseEstCompletionTime);
            this._ReplacePlaceholder("enrollment_learningpathdescription", this.EnrollmentLearningPathDescription);
            this._ReplacePlaceholder("enrollment_duedate", this.EnrollmentDueDate);
            this._ReplacePlaceholder("enrollment_expiredate", this.EnrollmentExpireDate);

            this._ReplacePlaceholder("session_datetime", this.StandupTrainingSessionMeetingTime);
            this._ReplacePlaceholder("session_timezone", this.StandupTrainingSessionTimezone);
            this._ReplacePlaceholder("session_location", this.StandupTrainingSessionLocation);
            this._ReplacePlaceholder("session_locationdescription", this.StandupTrainingSessionLocationDescription);
            this._ReplacePlaceholder("session_instructorfullname", this.StandupTrainingSessionInstructorFullName);
            this._ReplacePlaceholder("session_droplink", this.StandupTrainingSessionDropLink);

            this._ReplacePlaceholder("user_registration_rejection_comments", this.UserRegistrationRejectionComments);

            this._ReplacePlaceholder("enrollment_request_rejection_comments", this.EnrollmentRequestRejectionComments);

            this._ReplacePlaceholder("certification_description", this.CertificationDescription);

            this._ReplacePlaceholder("discussion_message", this.DiscussionMessage);

            // replace common placeholders
            this._ReplaceCommonPlaceholders();
        }
        #endregion
        #endregion
    }
}
