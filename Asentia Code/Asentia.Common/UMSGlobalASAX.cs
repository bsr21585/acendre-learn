﻿using System;
using System.Data;
using System.Diagnostics;
using System.Web;

namespace Asentia.Common
{
    public class UMSGlobalASAX : System.Web.HttpApplication
    {
        #region Application_Start
        protected void Application_Start(object sender, EventArgs e)
        { }
        #endregion

        #region Session_Start
        protected void Session_Start(object sender, EventArgs e)
        {
            /*
             
             * 
             * 
             * LEGACY SESSION START CODE
             * 
             * THIS WAS MOVED TO Application_AcquireRequestState SINCE THAT EVENT FIRES EVERY TIME AND WE NEED TO ENSURE THAT OUR DATABASE
             * SESSION VARIABLES STAY POPULATED. ONCE FULLY TESTED TO BE WORKING, WE WILL REMOVE THIS CODE.
             * 
             * 
             
             
            //EventLog.WriteEntry("Asentia", String.Format("Session Started: The session identifier is: {0}", AsentiaSessionState.SessionId), EventLogEntryType.Information);

            // Retrieve the account and site information from the requested URL. Try up to 3 times to do this.
            int i;

            for (i = 0; i < 3; i++)
            {
                _RetrieveAccountAndSiteFromURL();

                if (AsentiaSessionState.IdAccount > 0 && AsentiaSessionState.IdSite > 0)
                {
                    //EventLog.WriteEntry("Asentia", String.Format("Try # {2} | Session IdAccount: {0} | Session IdSite: {1}", AsentiaSessionState.IdAccount, AsentiaSessionState.IdSite, i.ToString()), EventLogEntryType.Information);
                    break; 
                }
            }

            // if we've tried and still cannot get an account and site in session, bounce
            if (AsentiaSessionState.IdAccount == 0 || AsentiaSessionState.IdSite == 0)
            { 
                //EventLog.WriteEntry("Asentia", "The session state variables IdAccount and IdSite could not be stored after 3 tries.", EventLogEntryType.Error); 
            }

            // Set the session timeout based on the value set in the web.config for the account.
            AsentiaSessionState.SessionTimeout = Config.AccountSettings.SessionTimeout;
            
            */
        }
        #endregion

        #region Application_BeginRequest
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            // set access control allow origin header to allow any script origin if the requested url is the xAPI service
            Uri requestedURL = HttpContext.Current.Request.Url;
            if (requestedURL.ToString().ToLower().Contains("_util/xapi/xapistatementrestservice.svc"))
            { HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*"); }
        }

        #endregion

        #region Application_AcquireRequestState
        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            // ensure that we have a session before attempting to do anything with session variables
            // this event may be called during the lifecycle several times and sometimes without session
            if (HttpContext.Current.Session != null)
            {
                // if we do not have an idAccount and idSite, which are the things needed to connect to the database, 
                // in session already, grab them by mapping the requested url to account and site
                // checking this keeps us from doing unnecessary work if we already know the account and site
                if (AsentiaSessionState.IdAccount == 0 || AsentiaSessionState.IdSite == 0)
                {
                    // retrieve the account and site information from the requested URL. Try up to 3 times to do this.
                    int i;

                    for (i = 1; i <= 3; i++)
                    {
                        // get the account and site information
                        _RetrieveAccountAndSiteFromURL();

                        // we got them, temporarily announce to the event log that we have populated the session variables, then break
                        // TODO: remove the logging after we have fully tested this and it has no issues
                        if (AsentiaSessionState.IdAccount > 0 && AsentiaSessionState.IdSite > 0)
                        {
                            //EventLog.WriteEntry("Asentia", String.Format("Session ID: {3} | Try # {2} | Session IdAccount: {0} | Session IdSite: {1}", AsentiaSessionState.IdAccount, AsentiaSessionState.IdSite, i.ToString(), AsentiaSessionState.SessionId), EventLogEntryType.Information);
                            break; 
                        }
                    }

                    // if we've tried and still cannot get an account and site in session, log it and bounce
                    if (AsentiaSessionState.IdAccount == 0 || AsentiaSessionState.IdSite == 0)
                    {
                        //EventLog.WriteEntry("Asentia", "The session state variables IdAccount and IdSite could not be stored after 3 tries for Session ID: " + AsentiaSessionState.SessionId + ".", EventLogEntryType.Error);
                        HttpContext.Current.Response.Redirect("/SessionError.html");
                    }

                    // if there is a "no-access" ip restriction and the user is not coming from a whitelisted ip, kill the session and throw a 403 (Forbidden)
                    if (AsentiaSessionState.BounceForNoAccessIPRestriction)
                    {
                        // kill the session
                        AsentiaSessionState.EndSession(false);

                        // throw a 403 (Forbidden) error
                        Response.Clear();
                        Response.StatusCode = 403;
                        Response.End();
                    }

                    // set the session timeout based on the value set in the web.config for the account.
                    AsentiaSessionState.SessionTimeout = Config.AccountSettings.SessionTimeout;
                }                
            }
        }
        #endregion

        #region Application_AuthenticateRequest
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        { ;}
        #endregion

        #region Application_Error
        protected void Application_Error(object sender, EventArgs e)
        {      
            // get the exception that was thrown
            Exception thrownException = Server.GetLastError().GetBaseException();            

            // if this is an HTTP error, handle it elsewhere
            if (thrownException.GetType() == typeof(HttpException))
            { return; }

            // try to log the exception and forward to the error page, if we 
            // encounter an exception while doing that, pass that exception
            // to the error page    
            
            try
            {
                // write the exception to the exception log
                int idExceptionLog = ExceptionLog.Add(AsentiaSessionState.IdSite,
                                                      AsentiaSessionState.IdSiteUser,
                                                      HttpContext.Current.Request.Url.ToString(),
                                                      thrownException.GetType().ToString(),
                                                      thrownException.Message,
                                                      thrownException.StackTrace);

                // add the exception information to the current context for transfer to error page
                HttpContext.Current.Items.Add("IdExceptionLog", idExceptionLog);
                HttpContext.Current.Items.Add("RequestedPage", HttpContext.Current.Request.Url.ToString());
                HttpContext.Current.Items.Add("Exception", thrownException);

                // if this is the application root, just return, the Server.Transfer method does not work on application root
                if (HttpContext.Current.Request.Path == "/default.aspx")
                { return; }

                // clear the error
                Server.ClearError();

                // transfer to the exception handler page
                Server.Transfer("~/Error.aspx");                
            }
            catch (Exception fatalException)
            {
                // if this is the application root, just return, the Server.Transfer method does not work on application root
                if (HttpContext.Current.Request.Path == "/default.aspx")
                { return; }

                // add the exception information to the current context for transfer to error page
                HttpContext.Current.Items.Add("IdExceptionLog", 0);
                HttpContext.Current.Items.Add("RequestedPage", HttpContext.Current.Request.Url.ToString());
                HttpContext.Current.Items.Add("Exception", thrownException);

                // clear the error
                Server.ClearError();

                // redirect to the exception handler page
                Server.Transfer("~/Error.aspx");
            }
        }
        #endregion

        #region Session_End
        protected void Session_End(object sender, EventArgs e)
        {
            //EventLog.WriteEntry("Asentia", String.Format("Session Ended: The session identifier is: {0}", AsentiaSessionState.SessionId), EventLogEntryType.Information);
        }
        #endregion

        #region Application_End
        protected void Application_End(object sender, EventArgs e)
        { }
        #endregion

        #region _RetrieveAccountAndSiteFromURL
        private void _RetrieveAccountAndSiteFromURL()
        {
            // get the url that was requested, and strip the base domain from it
            Uri requestedURL = HttpContext.Current.Request.Url;
            string url = requestedURL.Authority.Replace("." + Config.AccountSettings.BaseDomain, "");

            // if the url is just the base domain name (no hostname), and the BaseDomain is asentialms.com then redirect to http://www.asentialms.com
            // this is done because of the way asentialms.com is DNSed to have all requests except www hit the application, requests without a hostname should just be sent to www.
            if (url == Config.AccountSettings.BaseDomain && Config.AccountSettings.BaseDomain == "asentialms.com")
            { HttpContext.Current.Response.Redirect("http://www.asentialms.com"); }

            // Connect to Customer Manager database to retrieve the account id mapped to the url
            AsentiaDatabase serverDatabaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            serverDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            serverDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            serverDatabaseObject.AddParameter("@url", url, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            serverDatabaseObject.AddParameter("@idAccount", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                serverDatabaseObject.ExecuteNonQuery("[System.MatchRequestedUrlToAccount]", true);

                DBReturnValue returnCode = (DBReturnValue)AsentiaDatabase.ParseDbParamInt(serverDatabaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = AsentiaDatabase.ParseDbParamString(serverDatabaseObject.Command.Parameters["@Error_Description_Code"].Value);
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                int idAccount = AsentiaDatabase.ParseDbParamInt(serverDatabaseObject.Command.Parameters["@idAccount"].Value);

                AsentiaSessionState.IdAccount = idAccount; // IF THIS BECOMES 1, IT MEANS THE ACCOUNT WAS NOT FOUND

                if (AsentiaSessionState.IdAccount == 0) // fatal error if 0
                { throw new Exception("Could not map Account ID."); }
            }
            catch (Exception ex)
            {
                //EventLog.WriteEntry("Asentia", ex.Message, EventLogEntryType.Error);
                throw;
            }
            finally
            {
                serverDatabaseObject.Dispose();
            }

            if (AsentiaSessionState.IdAccount > 1) // we only do this for an account mapping that we have found. otherwise, we bounce to the "unknown" page.
            {
                // Connect to Account database to retrieve the site id and hostname mapped to the url
                AsentiaDatabase accountDatabaseObject = new AsentiaDatabase();

                accountDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                accountDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                accountDatabaseObject.AddParameter("@url", url, SqlDbType.NVarChar, 255, ParameterDirection.Input);
                accountDatabaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
                accountDatabaseObject.AddParameter("@hostname", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
                accountDatabaseObject.AddParameter("@langString", null, SqlDbType.NVarChar, 10, ParameterDirection.Output);

                // get and send the user's ip address to the procedure so we can check against "no-access" ip restrictions
                string clientIPAddress = String.Empty;

                if (!String.IsNullOrWhiteSpace(Request.ServerVariables["HTTP_X_FORWARDED_FOR"])) // client's ip address if application is behind load balancer
                { clientIPAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"]; }
                else
                { clientIPAddress = Request.UserHostAddress; }

                accountDatabaseObject.AddParameter("@clientIPAddress", clientIPAddress, SqlDbType.NVarChar, 15, ParameterDirection.Input);
                accountDatabaseObject.AddParameter("@bounceForNoAccessIPRestriction", null, SqlDbType.Bit, 1, ParameterDirection.Output);

                try
                {
                    accountDatabaseObject.ExecuteNonQuery("[System.MatchRequestedUrlToSite]", true);

                    DBReturnValue returnCode = (DBReturnValue)AsentiaDatabase.ParseDbParamInt(accountDatabaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = AsentiaDatabase.ParseDbParamString(accountDatabaseObject.Command.Parameters["@Error_Description_Code"].Value);
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                    int idSite = AsentiaDatabase.ParseDbParamInt(accountDatabaseObject.Command.Parameters["@idSite"].Value);
                    string hostname = AsentiaDatabase.ParseDbParamString(accountDatabaseObject.Command.Parameters["@hostname"].Value);
                    string siteCulture = AsentiaDatabase.ParseDbParamString(accountDatabaseObject.Command.Parameters["@langString"].Value);
                    bool bounceForIPRestriction = AsentiaDatabase.ParseDbParamBool(accountDatabaseObject.Command.Parameters["@bounceForNoAccessIPRestriction"].Value);

                    AsentiaSessionState.IdSite = idSite; // IF THIS BECOMES 1, IT MEANS THE SITE WAS NOT FOUND
                    AsentiaSessionState.SiteHostname = hostname;
                    AsentiaSessionState.UserCulture = siteCulture;
                    AsentiaSessionState.BounceForNoAccessIPRestriction = bounceForIPRestriction;

                    if (AsentiaSessionState.IdSite == 0) // fatal error if 0
                    { throw new Exception("Could not map Site ID."); }

                    // redirect to "unknown" if id site is 1
                    if (idSite == 1)
                    { Response.Redirect("/unknown"); }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    accountDatabaseObject.Dispose();
                }
            }
            else // we couldnt map the account originally, so this just sets defaults needed to show the "unknown" page, then redirects
            {
                AsentiaSessionState.IdSite = 1;
                AsentiaSessionState.SiteHostname = "default";
                AsentiaSessionState.UserCulture = "en-US";
                Response.Redirect("/unknown");
            }
        }
        #endregion
    }
}
