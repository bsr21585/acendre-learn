﻿
namespace Asentia.Common
{
    public static class PlaceholderConstants
    {
        //EVENT/OBJECT
        public static readonly string PLACEHOLDER_OBJECT_NAME = "##object_name##";
        public static readonly string PLACEHOLDER_EVENT_DATE = "##event_date##";
        public static readonly string PLACEHOLDER_EVENT_TIME = "##event_time##";
        public static readonly string PLACEHOLDER_OPTOUT_LINK = "##optout_link##";

        //USER
        public static readonly string PLACEHOLDER_USER_FIRSTNAME = "##user_firstname##";
        public static readonly string PLACEHOLDER_USER_FULLNAME = "##user_fullname##";
        public static readonly string PLACEHOLDER_USER_LOGIN = "##user_login##";
        public static readonly string PLACEHOLDER_USER_EMAIL = "##user_email##";
        public static readonly string PLACEHOLDER_USERREGISTRATIONREJECTION_REJECTIONCOMMENTS = "##user_registration_rejection_comments##";

        //RECIPIENT
        public static readonly string PLACEHOLDER_RECIPIENT_FULLNAME = "##recipient_fullname##";
        public static readonly string PLACEHOLDER_RECIPIENT_FIRSTNAME = "##recipient_firstname##";
        public static readonly string PLACEHOLDER_RECIPIENT_LOGIN = "##recipient_login##";
        public static readonly string PLACEHOLDER_RECIPIENT_EMAIL = "##recipient_email##";

        //ENROLLMENT
        public static readonly string PLACEHOLDER_ENROLLMENT_COURSEESTCOMPLETIONTIME = "##enrollment_courseestcompletiontime##";
        public static readonly string PLACEHOLDER_ENROLLMENT_COURSEDESCRIPTION = "##enrollment_coursedescription##";
        public static readonly string PLACEHOLDER_ENROLLMENTREQUEST_REJECTIONCOMMENTS = "##enrollment_request_rejection_comments##";
        public static readonly string PLACEHOLDER_ENROLLMENT_DUEDATE = "##enrollment_duedate##";
        public static readonly string PLACEHOLDER_ENROLLMENT_EXPIREDATE = "##enrollment_expiredate##";

        //LEARNING PATH ENROLLMENT
        public static readonly string PLACEHOLDER_ENROLLMENT_LEARNINGPATHEDESCRIPTION = "##enrollment_learningpathdescription##";

        //SESSION
        public static readonly string PLACEHOLDER_SESSION_DATETIME = "##session_datetime##";
        public static readonly string PLACEHOLDER_SESSION_TIMEZONE = "##session_timezone##";
        public static readonly string PLACEHOLDER_SESSION_LOCATION = "##session_location##";
        public static readonly string PLACEHOLDER_SESSION_LOCATIONDESCRIPTION = "##session_locationdescription##";
        public static readonly string PLACEHOLDER_SESSION_INSTRUCTORFULLNAME = "##session_instructorfullname##";
        public static readonly string PLACEHOLDER_SESSION_DROPLINK = "##session_droplink##";

        //CERTIFICATION
        public static readonly string PLACEHOLDER_CERTIFICATION_DESCRIPTION = "##certification_description##";

        //DISCUSSION
        public static readonly string PLACEHOLDER_DISCUSSION_MESSAGE = "##discussion_message##";   
    }
}