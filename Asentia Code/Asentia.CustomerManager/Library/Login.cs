﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;

namespace Asentia.CustomerManager.Library
{
    public class Login
    {
        #region AuthenticateUser
        public static void AuthenticateUser(string username, string password)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            // RETURN CODE / ERROR DESCRIPTION CODE
            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            // INPUTS
            databaseObject.AddParameter("@username", username, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@password", password, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            // OUTPUTS
            databaseObject.AddParameter("@idAccount", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idAccountUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@userFirstName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userLastName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userCulture", null, SqlDbType.NVarChar, 20, ParameterDirection.Output);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[System.AuthenticateLogin]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                AsentiaSessionState.IdAccount = Convert.ToInt32(databaseObject.Command.Parameters["@idAccount"].Value);
                AsentiaSessionState.IdAccountUser = Convert.ToInt32(databaseObject.Command.Parameters["@idAccountUser"].Value);
                AsentiaSessionState.UserFirstName = databaseObject.Command.Parameters["@userFirstName"].Value.ToString();
                AsentiaSessionState.UserLastName = databaseObject.Command.Parameters["@userLastName"].Value.ToString();
                AsentiaSessionState.UserCulture = databaseObject.Command.Parameters["@userCulture"].Value.ToString();
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ResetPassword
        public static string ResetPassword(string username)
        {
            // generate a new password
            string newPassword = RandomPassword.Generate();

            // RESET THE USER'S PASSWORD

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            // RETURN CODE / ERROR DESCRIPTION CODE
            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            // INPUTS
            databaseObject.AddParameter("@username", username, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@newPassword", newPassword, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            // OUTPUTS
            databaseObject.AddParameter("@userFirstName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userLastName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userEmail", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userCulture", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 20, ParameterDirection.InputOutput);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[AccountUser.ResetPassword]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                string firstName = databaseObject.Command.Parameters["@userFirstName"].Value.ToString();
                string lastName = databaseObject.Command.Parameters["@userLastName"].Value.ToString();
                string email = databaseObject.Command.Parameters["@userEmail"].Value.ToString();
                AsentiaSessionState.UserCulture = databaseObject.Command.Parameters["@userCulture"].Value.ToString();

                // SEND EMAIL TO USER CONTAINING PASSWORD INFORMATION
                PasswordResetEmail passwordResetEmail = new PasswordResetEmail(AsentiaSessionState.UserCulture, null, null, 0, false, null, null);

                // lms information
                passwordResetEmail.LmsHostname = "customermanager";
                passwordResetEmail.LmsUrl = "http://customermanager." + Config.AccountSettings.BaseDomain;

                // user information
                passwordResetEmail.UserFullName = firstName + " " + lastName;
                passwordResetEmail.UserFirstName = firstName;
                passwordResetEmail.UserLogin = username;
                passwordResetEmail.UserPassword = newPassword;
                passwordResetEmail.UserEmail = email;

                // recipient information
                passwordResetEmail.RecipientFullName = firstName + " " + lastName;
                passwordResetEmail.RecipientFirstName = firstName;
                passwordResetEmail.RecipientLogin = username;
                passwordResetEmail.RecipientEmail = email;

                // send mail
                passwordResetEmail.Send();

                return email;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
    }
}
