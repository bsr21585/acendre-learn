﻿using Asentia.Common;
using Asentia.Controls;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.CompilerServices;
using System.ComponentModel;

namespace Asentia.CustomerManager.Library
{
    class DatabaseServer
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public DatabaseServer()
        {
            ;
        }

        public DatabaseServer(int idDatabaseServer)
        {
            this._Details(idDatabaseServer);
        }

        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[DatabaseServer.GetGrid]";

        /// <summary>
        /// Id
        /// </summary>
        public int idDatabaseServer;

        /// <summary>
        /// Name
        /// </summary>
        public string ServerName;

        /// <summary>
        /// Company
        /// </summary>
        public string NetworkName;

        /// <summary>
        /// Contact Name
        /// </summary>
        public string UserName;

        /// <summary>
        /// Password
        /// </summary>
        public string Password;


        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsTrusted;


        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        [NonSerialized]
        [JsonIgnore]
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// all available languages
        /// </summary>
        public Dictionary<string, string> AvailableLanguages = new Dictionary<string, string>();
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves account information.
        /// </summary>
        /// <param name="idCallerSite">caller's site id</param>
        /// <param name="callerLangString">caller's language</param>
        /// <param name="idCaller">caller id</param>
        /// <returns>id of the saved site</returns>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDatabaseServer", this.idDatabaseServer, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@serverName", this.ServerName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@networkName", this.NetworkName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@username", this.UserName, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@password", this.Password, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@isTrusted", this.IsTrusted, SqlDbType.Bit, 1, ParameterDirection.Input);
            try
            {
                databaseObject.ExecuteNonQuery("[DatabaseServer.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // get the returned site id
                this.idDatabaseServer = Convert.ToInt32(databaseObject.Command.Parameters["@idDatabaseServer"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return this.idDatabaseServer;
        }
        #endregion
        #endregion

        #region Static Methods

        #region DoesServerNameExists
        /// <summary>
        /// Determines if the current session Site alredy has a host name with same name.
        /// </summary>
        /// <param name="hostname">Host name</param>
        /// <returns></returns>
        public static bool DoesServerNameExists(string servername)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@serverName", servername, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@isServerNameAlredyExists", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[DatabaseServer.DoesServerNameExists]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isServerNameAlredyExists"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a database.
        /// </summary>
        /// <param name="deletees">DataTable of database(s) to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@DatabaseServer", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[DatabaseServer.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForDatabaseServerList
        /// <summary>
        /// Method to fetch the ids and names of the server from the database
        /// </summary>
        public static DataTable IdsAndNamesForDatabaseServerList()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds = databaseObject.ExecuteDataSet("[DatabaseServer.IdsAndNamesForDatabaseServerList]", true);
                dt = ds.Tables[0];

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetDatabaseList

        /// <summary>
        /// Method to fetch the names of the database by database server id
        /// </summary>
        /// <param name="idDatabaseServer"></param>
        /// <returns></returns>
        public static DataTable GetDatabaseList(int idDatabaseServer)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idDatabaseServer", idDatabaseServer, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds = databaseObject.ExecuteDataSet("[Account.GetDatabaseList]", true);
                dt = ds.Tables[0];

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetAccountList

        /// <summary>
        /// Method to fetch the accounts
        /// </summary>
        public static DataTable GetAccountList()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            
            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds = databaseObject.ExecuteDataSet("SELECT idAccount, company FROM [dbo].[tblAccount]", false);
                dt = ds.Tables[0];

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region _Details
        /// <summary>
        /// Loads the details of the database server with id idDatabaseServer.
        /// </summary>
        /// <param name="idDatabaseServer">idDatabaseServer</param>
        private void _Details(int idDatabaseServer)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDatabaseServer", idDatabaseServer, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@serverName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@networkName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@username", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@password", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@isTrusted", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[DatabaseServer.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // populate the properties
                this.idDatabaseServer = idDatabaseServer;
                this.ServerName = databaseObject.Command.Parameters["@serverName"].Value.ToString();
                this.NetworkName = databaseObject.Command.Parameters["@networkName"].Value.ToString();
                this.IsTrusted = Convert.ToBoolean(databaseObject.Command.Parameters["@isTrusted"].Value);
                this.UserName = databaseObject.Command.Parameters["@username"].Value.ToString();
                this.Password = databaseObject.Command.Parameters["@password"].Value.ToString();

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetIdSiteFromDatabase

        /// <summary>
        ///  Method to fetch the idSite and hostnames from given server and database
        /// </summary>
        /// <param name="DBServerName">Server name</param>
        /// <param name="DatabaseName">database name</param>
        /// <returns>list of hostname and idSite</returns>
        public static DataTable GetIdSiteFromDatabase(string DBServerName, string DatabaseName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@DBServerName", DBServerName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@DatabaseName", DatabaseName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {

                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds = databaseObject.ExecuteDataSet("[DatabaseServer.GetSitesFromDatabase]", true);
                if (ds.Tables.Count != 0)
                { dt = ds.Tables[0]; }
                else
                { dt = null; }

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region CreateClonePortal

        /// <summary>
        ///  Method for creating the clone portal 
        /// </summary>
        /// <param name="fullCopyFlag"></param>
        /// <param name="isNewHostName"></param>
        /// <param name="idSiteSource"></param>
        /// <param name="idSiteDestination"></param>
        /// <param name="sourceDBServer"></param>
        /// <param name="sourceDBName"></param>
        /// <param name="destinationDBServer"></param>
        /// <param name="destinationDBName"></param>
        /// <param name="destinationHostName"></param>
        /// <param name="idAccount">Destination database idAccount</param>
        /// <returns>objects name, oldId and newId mappings</returns>
        public static DataTable CreateClonePortal(bool fullCopyFlag, bool isNotNewHostName, int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName, string destinationHostName, int idAccount)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 500);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@fullCopyFlag", fullCopyFlag, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@isNewHostName", isNotNewHostName, SqlDbType.Bit, 1, ParameterDirection.Input);

            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostName", destinationHostName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds = databaseObject.ExecuteDataSet("[System.ClonePortal]", true);
                if (ds.Tables.Count != 0)
                {
                    dt = ds.Tables[0];
                }
                else
                {
                    dt = null;
                }

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetObjectIdsForFileSystemCleanup

        /// <summary>
        ///  Method to fetch object ids for the file system cleanup
        /// </summary>
        /// <param name="DBServerName">Server name</param>
        /// <param name="DatabaseName">database name</param>
        /// <returns>list of object ids</returns>
        public static DataTable GetObjectIdsForFileSystemCleanup(string DBServerName, string DatabaseName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@DBServerName", DBServerName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@DatabaseName", DatabaseName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();

                ds = databaseObject.ExecuteDataSet("[System.GetObjectIdsForFileSystemCleanup]", true);

                if (ds.Tables.Count != 0)
                { dt = ds.Tables[0]; }
                else
                { dt = null; }

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetContentPackagePathsForFileSystemCleanup

        /// <summary>
        ///  Method to fetch content package paths for the file system cleanup
        /// </summary>
        /// <param name="DBServerName">Server name</param>
        /// <param name="DatabaseName">database name</param>
        /// <returns>list of content package paths</returns>
        public static DataTable GetContentPackagePathsForFileSystemCleanup(string DBServerName, string DatabaseName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@DBServerName", DBServerName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@DatabaseName", DatabaseName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {

                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds = databaseObject.ExecuteDataSet("[System.GetContentPackagePathsForFileSystemCleanup]", true);
                if (ds.Tables.Count != 0)
                { dt = ds.Tables[0]; }
                else
                { dt = null; }

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetDocumentRepositoryItemObjectIdsForFileSystemCleanup

        /// <summary>
        ///  Method to fetch document repository object ids for the file system cleanup
        /// </summary>
        /// <param name="DBServerName">Server name</param>
        /// <param name="DatabaseName">database name</param>
        /// <returns>list of object ids</returns>
        public static DataTable GetDocumentRepositoryItemObjectIdsForFileSystemCleanup(string DBServerName, string DatabaseName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@DBServerName", DBServerName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@DatabaseName", DatabaseName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {

                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds = databaseObject.ExecuteDataSet("[System.GetDocumentRepositoryItemObjectIdsForFileSystemCleanup]", true);
                if (ds.Tables.Count != 0)
                { dt = ds.Tables[0]; }
                else
                { dt = null; }

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetIDMappings
        /// <summary>
        /// Gets the ID Mappings tables after the database clone is complete
        /// </summary>
        /// <param name="destinationDBServer">Destination Asentia Server</param>
        /// <param name="destinationDBName">Destination Asentia DB Name</param>
        /// <returns></returns>        
        public static DataTable GetIDMappings(string destinationDBServer, string destinationDBName, string forFolder)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 3600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 50, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 50, ParameterDirection.Input);
            databaseObject.AddParameter("@forFolder", forFolder, SqlDbType.NVarChar, 50, ParameterDirection.Input);

            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();

                ds = databaseObject.ExecuteDataSet("[System.ClonePortal.GetIDMappings]", true);

                if (ds.Tables.Count != 0)
                { dt = ds.Tables[0]; }
                else
                { dt = null; }

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
    }
}
