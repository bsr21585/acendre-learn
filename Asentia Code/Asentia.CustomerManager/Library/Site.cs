﻿using Asentia.Common;
using Asentia.Controls;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace Asentia.CustomerManager.Library
{
    class Site
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Site()
        {
            ;
        }

        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Site.GetGrid]";

        /// <summary>
        /// Id
        /// </summary>
        public int Id;

        /// <summary>
        /// Name
        /// </summary>
        public string Title;

        /// <summary>
        /// Company
        /// </summary>
        public string Company;

        /// <summary>
        /// Contact Name
        /// </summary>
        public string ContactName;

        /// <summary>
        /// Contact Email
        /// </summary>
        public string ContactEmail;

        /// <summary>
        /// Password
        /// </summary>
        public string Password;

        /// <summary>
        /// HostName
        /// </summary>
        public string HostName;

        /// <summary>
        /// HostName
        /// </summary>
        public string Domain;

        /// <summary>
        /// UserLimit
        /// </summary>
        public int? UserLimit;

        /// <summary>
        /// KbLimit
        /// </summary>
        public int? KbLimit;

        /// <summary>
        /// TimeZone
        /// </summary>
        public int IdTimezone;

        /// <summary>
        /// DtExpires
        /// </summary>
        public DateTime? DtExpires;

        /// <summary>
        /// IsActive
        /// </summary>
        public bool Status;

        /// <summary>
        /// Language String
        /// </summary>
        public string LanguageString;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        [NonSerialized]
        [JsonIgnore]
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// all available languages
        /// </summary>
        public Dictionary<string, string> AvailableLanguages = new Dictionary<string, string>();
        #endregion

        #region Methods
        #region Delete
        /// <summary>
        /// Deletes an Account.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Accounts", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Account.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods

        #region SaveSiteToDomainAliasLink
        /// <summary>
        /// Insert site to domain alis link
        /// </summary>
        /// <param name="idAccount"></param>
        /// <param name="idSite"></param>
        public static void SaveSiteToDomainAliasLink(int idSite, string hostName, int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@domain", hostName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[SiteToDomainAliasLink.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DeleteDomainAlias
        /// <summary>
        /// Deletes DomainAlias
        /// </summary>
        /// <param name="domainAlias"></param>
        public static void DeleteDomainAlias(string domainAlias, int idSite, int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
 
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@domainAliass", domainAlias, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[SiteToDomainAliasLink.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetDomains
        /// <summary>
        /// Gets existing account to domain alis link for a particular site
        /// </summary>
        /// <param name="idAccount"></param>
        /// <param name="idSite"></param>
        public static DataTable GetDomains(int idSite, string hostName)
        {
            DataTable dt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", null, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", null, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@hostName", hostName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Site.GetDomains]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion


        #region ClonePortalSiteDeleteAfterCopy

        /// <summary>
        /// Deletes a site after cloning the portal.
        /// </summary>
        /// <param name="dBServerName"></param>
        /// <param name="sourceDBName"></param>
        /// <param name="idSite"></param>
        /// <param name="idAccountOld"></param>
        /// <param name="hostNameOld"></param>
        /// <returns></returns>
        public static DataTable ClonePortalSiteDeleteAfterCopy(string dBServerName, string sourceDBName, int idSite, int idAccountOld, string hostNameOld)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);
            DataTable dt = new DataTable();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", dBServerName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idAccountOld", idAccountOld, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@hostNameOld", hostNameOld, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[ClonePortalSite.Delete]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion

    }
}
