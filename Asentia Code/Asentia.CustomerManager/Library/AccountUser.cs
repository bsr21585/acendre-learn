﻿using Asentia.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Asentia.CustomerManager.Library
{
    class AccountUser
    {
        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public AccountUser()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idUser">User Id</param>
        public AccountUser(int idAccountUser)
        {
            this._Details(idAccountUser);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[AccountUser.GetGrid]";

        /// <summary>
        /// IdAccountUser
        /// </summary>
        public int Id;

        /// <summary>
        /// idAccount
        /// </summary>
        public int IdAccount;

        /// <summary>
        /// firstName
        /// </summary>
        public string FirstName;

        /// <summary>
        /// LastName
        /// </summary>
        public string LastName;

        /// <summary>
        /// DisplayName
        /// </summary>
        public string DisplayName;

        /// <summary>
        /// Email
        /// </summary>
        public string Email;

        /// <summary>
        /// Username
        /// </summary>
        public string Username;

        /// <summary>
        /// Password
        /// </summary>
        public string Password;

        /// <summary>
        /// IdRole
        /// </summary>
        public int IdRole;

        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive;
        #endregion

        #region Enums
        /// <summary>
        /// Role type of account user
        /// </summary>
        public enum RoleType
        {
            SystemUser = 1,
            AccountUser = 2
        } 
        #endregion

        #region Methods
        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified user's properties.
        /// </summary>
        /// <param name="idAccountUser">Account User Id</param>
        private void _Details(int idAccountUser)
        {
            if (idAccountUser == 0)
            { return; }

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idAccountUser", idAccountUser, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idAccount", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@firstName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@lastName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@displayName", null, SqlDbType.NVarChar, 768, ParameterDirection.Output);
            databaseObject.AddParameter("@email", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@username", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@password", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@idRole", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@roleName", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@isActive", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[AccountUser.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idAccountUser"].Value);
                this.IdAccount = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idAccount"].Value);
                this.FirstName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@firstName"].Value);
                this.LastName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@lastName"].Value);
                this.DisplayName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@displayName"].Value);
                this.Email = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@email"].Value);
                this.Username = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@username"].Value);
                this.IdRole = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idRole"].Value);
                this.IsActive = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isActive"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes an Account.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@AccountUsers", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[AccountUser.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Save
        /// <summary>
        /// Saves learning path data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved learning path</returns>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idAccountUser", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idAccount", this.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@firstName", this.FirstName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@lastName", this.LastName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@email", this.Email, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@username", this.Username, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@password", this.Password, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@idRole", this.IdRole, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isActive", this.IsActive, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[AccountUser.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved learning path
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idAccountUser"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
