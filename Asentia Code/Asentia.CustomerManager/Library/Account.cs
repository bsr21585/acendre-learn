﻿using System;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;

namespace Asentia.CustomerManager.Library
{
    class Account
    {
        #region Constructors
        public Account(int idAccount)
        {
            this._Details(idAccount);
        }

        public Account()
        {
            ;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Account.GetGrid]";

        /// <summary>
        /// Id
        /// </summary>
        public int Id;

        /// <summary>
        /// Database Server
        /// </summary>
        public int IdDatabaseServer;

        /// <summary>
        /// Company
        /// </summary>
        public string Company;

        /// <summary>
        /// Contact First Name
        /// </summary>
        public string ContactFirstName;

        /// <summary>
        /// Contact Last Name
        /// </summary>
        public string ContactLastName;

        /// <summary>
        /// Contact Name
        /// </summary>
        public string ContactDisplayName;

        /// <summary>
        /// Contact Email
        /// </summary>
        public string ContactEmail;

        /// <summary>
        /// User Name
        /// </summary>
        public string UserName;

        /// <summary>
        /// Password
        /// </summary>
        public string Password;

        /// <summary>
        /// Status
        /// </summary>
        public bool IsActive;

        /// <summary>
        /// Status
        /// </summary>
        public bool IsDeleted;

        /// <summary>
        /// Database Name
        /// </summary>
        public string DatabaseName;

        #endregion

        #region Methods

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified user's properties.
        /// </summary>
        /// <param name="idAccountUser">Account User Id</param>
        private void _Details(int idAccount)
        {
            if (idAccount == 0)
            { return; }

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idDatabaseServer", idAccount, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@company", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@contactFirstName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@contactLastName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@contactDisplayName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@contactEmail", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@userName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@password", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@isActive", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@databaseName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Account.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idAccount"].Value);
                this.IdDatabaseServer = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idDatabaseServer"].Value);
                this.Company = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@company"].Value);
                this.ContactFirstName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@contactFirstName"].Value);
                this.ContactLastName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@contactLastName"].Value);
                this.ContactDisplayName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@contactDisplayName"].Value);

                this.ContactEmail = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@contactEmail"].Value);
                this.UserName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@userName"].Value);
                this.Password = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@password"].Value);
                this.IsActive = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isActive"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DatabaseName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@databaseName"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Save
        /// <summary>
        /// Saves object data to the database.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseFieldNotUniqueException">
        /// Thrown when a field must be unique.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <exception cref="DatabaseFieldConstraintException">
        /// Thrown when a value passed into a stored procedure violates a business rule.
        /// </exception>
        /// <exception cref="DatabaseSpecifiedLanguageNotDefaultException">
        /// Thrown when a Database Specified Language Not Default Exception occurs in a stored procedure.
        /// </exception>
        /// <param name=""></param>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idAccount", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idDatabaseServer", this.IdDatabaseServer, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@company", this.Company, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@contactFirstName", this.ContactFirstName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@contactLastName", this.ContactLastName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@contactEmail", this.ContactEmail, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@userName", this.UserName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@password", this.Password, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@isActive", this.IsActive, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@isDeleted", this.IsDeleted, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@databaseName", this.DatabaseName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Account.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idAccount"].Value);

                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes an Account.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Accounts", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Account.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DoesHostNameExists
        /// <summary>
        /// Determines if the current session Site alredy has a host name with same name.
        /// </summary>
        /// <param name="hostname">Host name</param>
        /// <returns></returns>
        public static bool DoesHostNameExists(string hostname)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@hostName", hostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@isHostNameAlredyExists", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Account.DoesHostNameExists]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isHostNameAlredyExists"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveDomainAliasInCustomerManagerDataBase
        /// <summary>
        /// Insert account to domain alis link for a site
        /// </summary>
        /// <param name="idAccount"></param>
        /// <param name="domainAliases"></param>
        public static void SaveDomainAliasInCustomerManagerDataBase(int idAccount, string domainAlias)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", null, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", null, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@domainAlias", domainAlias, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Account.SaveDomainAlias]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DeleteHostNameFromCustomerManagerDatabase
        /// <summary>
        /// Deletes host name (while updating it)
        /// </summary>
        /// <param name="domainAlias"></param>
        public static void DeleteHostNameFromCustomerManagerDatabase(string domainAlias)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@domainAliass", domainAlias, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[AccountToDomainAliasLink.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DeleteDomainAliasFromCustomerManagerDatabase
        /// <summary>
        /// Deletes DomainAlias
        /// </summary>
        /// <param name="domainAlias"></param>
        public static void DeleteDomainAliasFromCustomerManagerDatabase(string domainAlias)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@domainAliass", domainAlias, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Account.DeleteDomain]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveAccountToDomainAliasLink
        /// <summary>
        /// Insert account to domain alis link
        /// </summary>
        /// <param name="idAccount"></param>
        public static void SaveAccountToDomainAliasLink(int idAccount, string hostName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerAccount", AsentiaSessionState.IdAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@hostName", hostName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[AccountToDomainAliasLink.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetAccountsList
        /// <summary>
        /// Gets a list of all of the accounts
        /// </summary>
        public static DataTable GetAccountsList()
        {
            
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager);
            

            try
            {
                
                DataSet accountDataSet = databaseObject.ExecuteDataSet("SELECT idAccount, company FROM [dbo].[tblAccount] WHERE idAccount <> 1", false);
                return accountDataSet.Tables[0];

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
