﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Asentia.CustomerManager")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ICS Learning Group")]
[assembly: AssemblyProduct("Asentia.CustomerManager")]
[assembly: AssemblyCopyright("Copyright © ICS Learning Group 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a1016be1-f7e5-4729-97d4-695315d96edf")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.X.*")]
[assembly: AssemblyVersion("1.16.*")]

// Embeded Resources
[assembly: WebResource("Asentia.CustomerManager.Pages._MasterPage.CustomerManager.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.Accounts.Sites.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.Accounts.Sites.Modify.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.System.Database.Modify.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.ClonePortal.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.InquisiqMigration.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.Accounts.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.Accounts.Users.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.Users.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.System.License.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.Accounts.License.Default.js", "text/javascript")]
[assembly: WebResource("Asentia.CustomerManager.Pages.System.FileSystemCleanup.Default.js", "text/javascript")]