﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.CustomerManager.Pages.System.Exceptions
{
    public class Default : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel LogFormContentWrapperContainer;
        public Panel ActionsPanel;
        public UpdatePanel ExceptionLogGridUpdatePanel;
        public Grid ExceptionLogGrid;
        public ModalPopup GridConfirmAction = new ModalPopup();
        public LinkButton DeleteButton = new LinkButton();
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ExceptionLog));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(_GlobalResources.ExceptionLog, ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EXCEPTION, ImageFiles.EXT_PNG, true));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.LogFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the grid
            this._BuildGrid();

            // bind the grid
            this.ExceptionLogGrid.BindData();
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // apply css class to container
            this.ExceptionLogGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.ExceptionLogGrid = new Grid("CustomerManager");
            this.ExceptionLogGrid.ID = "ExceptionLogGrid";
            this.ExceptionLogGrid.OverrideLoadImagesFromCustomerManager = true;
            this.ExceptionLogGrid.DBType = DatabaseType.CustomerManager;
            this.ExceptionLogGrid.StoredProcedure = ExceptionLog.GridProcedure;
            this.ExceptionLogGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.ExceptionLogGrid.AddFilter("@idAccount", SqlDbType.Int, 4, 0);
            this.ExceptionLogGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.ExceptionLogGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.ExceptionLogGrid.IdentifierField = "idExceptionLog";
            this.ExceptionLogGrid.DefaultSortColumn = "timestamp";
            this.ExceptionLogGrid.AddCheckboxColumn = false;
            this.ExceptionLogGrid.ShowTimeInDateStrings = true;

            // data key names
            this.ExceptionLogGrid.DataKeyNames = new string[] { "idExceptionLog", "exceptionMessage", "page" };

            // columns
            GridColumn exceptionMessage = new GridColumn(_GlobalResources.Message, "exceptionMessage", "exceptionMessage");
            GridColumn page = new GridColumn(_GlobalResources.Page, "page", "page");
            GridColumn company = new GridColumn(_GlobalResources.CompanyName, "company", "company");
            GridColumn exceptionType = new GridColumn(_GlobalResources.Type, "exceptionType", "exceptionType");
            GridColumn user = new GridColumn(_GlobalResources.User, "user", "user");
            GridColumn timestamp = new GridColumn(_GlobalResources.Timestamp, "timestamp", "timestamp");

            // add columns to data grid
            this.ExceptionLogGrid.AddColumn(exceptionMessage);
            this.ExceptionLogGrid.AddColumn(page);
            this.ExceptionLogGrid.AddColumn(company);
            this.ExceptionLogGrid.AddColumn(exceptionType);
            this.ExceptionLogGrid.AddColumn(user);
            this.ExceptionLogGrid.AddColumn(timestamp);    
      
            // attach the grid to the update panel
            this.ExceptionLogGridUpdatePanel.ContentTemplateContainer.Controls.Add(this.ExceptionLogGrid);
        }
        #endregion
    }
}