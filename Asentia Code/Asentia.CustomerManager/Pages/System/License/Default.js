﻿function ClearModalSuccess(modalId) {
    $('#' + modalId + 'ModalPopupSubmitButton').removeClass("DisabledButton");
    $('#' + modalId + 'ModalPopupSubmitButton').addClass("ActionButton");
    $('#' + modalId + 'ModalPopupSubmitButton').prop('disabled', false);
    $('#' + modalId + 'ModalPopupFeedbackContainer').html("");
    $(':checkbox').removeProp("checked");
}