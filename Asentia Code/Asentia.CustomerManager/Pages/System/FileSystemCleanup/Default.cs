﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit.HTMLEditor;
using System.IO;
using web = System.Web;

namespace Asentia.CustomerManager.Pages.System.FileSystemCleanup
{
    public class Default : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel ActionsPanel;
        public Panel PropertiesPanel;
        #endregion

        #region Private Properties
        private Button _CleanupButton;
        private ModalPopup _CleanupModal;
        private DropDownList _AccountDropDownList;
        private TextBox _AsentiaRootFolder;

        private Button _HiddenCleanupLoadButton;
        private Button _HiddenCleanupLaunchButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            base.OnPreRender(e);

            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.CustomerManager.Pages.System.FileSystemCleanup.Default.js");
        }
        #endregion
        
        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.FileSystemCleanup));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(_GlobalResources.FileSystemCleanup, ImageFiles.GetIconPath(ImageFiles.ICON_SAVE,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the properties and actions panels
            this._BuildPropertiesPanel();
            this._BuildActionsPanel();

            // build cleanup modal
            this._BuildCleanupModal();

        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the actions panel
        /// </summary>
        private void _BuildActionsPanel()
        {
            this._CleanupButton = new Button();
            this._CleanupButton.ID = "CleanupButton";
            this._CleanupButton.CssClass = "Button ActionButton";
            this._CleanupButton.Text = _GlobalResources.GenerateCleanupReport;
            this._CleanupButton.OnClientClick = "Cleanup(); return false;";
            this.ActionsPanel.Controls.Add(this._CleanupButton);

            this._HiddenCleanupLaunchButton = new Button();
            this._HiddenCleanupLaunchButton.ID = "HiddenCleanupLaunchButton";
            this._HiddenCleanupLaunchButton.Style.Add("display", "none");
            this.ActionsPanel.Controls.Add(this._HiddenCleanupLaunchButton);

        }
        #endregion

        #region _BuildPropertiesPanel
        /// <summary>
        /// Builds the properties panel
        /// </summary>
        private void _BuildPropertiesPanel()
        {

            // ACCOUNT
            this._AccountDropDownList = new DropDownList();
            this._AccountDropDownList.ID = "AccountDropDownList";
            this._AccountDropDownList.DataSource = Library.DatabaseServer.GetAccountList();
            this._AccountDropDownList.DataValueField = "idAccount";
            this._AccountDropDownList.DataTextField = "company";
            this._AccountDropDownList.DataBind();
            this._AccountDropDownList.ClientIDMode = ClientIDMode.AutoID;
            this._AccountDropDownList.AutoPostBack = true;
            this._AccountDropDownList.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
            this._AccountDropDownList.Items.Insert(this._AccountDropDownList.Items.Count, _GlobalResources.All);
            //this._AccountDropDownList.SelectedIndexChanged += this._ServerDropDownList_SelectedIndexChanged;
            this.PropertiesPanel.Controls.Add(AsentiaPage.BuildFormField("AccountDropDownList_Field",  
                                                                            _GlobalResourcesCHETU1.Account,
                                                                            null,
                                                                            this._AccountDropDownList,
                                                                            false, 
                                                                            false, 
                                                                            false));

            // ASENTIA ROOT FOLDER
            this._AsentiaRootFolder = new TextBox();
            this._AsentiaRootFolder.ID = "AsentiaRootFolder";
            this.PropertiesPanel.Controls.Add(AsentiaPage.BuildFormField("AsentiaRootFolder_Field",
                                                                            _GlobalResources.AsentiaRootFolder,
                                                                            null,
                                                                            this._AsentiaRootFolder,
                                                                            false,
                                                                            false,
                                                                            false));
        }

        #endregion

        #region _BuildCleanupModal
        /// <summary>
        /// Builds the modal for displaying / deleting orphaned items
        /// </summary>
        private void _BuildCleanupModal()
        {
            this._CleanupModal = new ModalPopup("CleanupModal");
            this._CleanupModal.OverrideLoadImagesFromCustomerManager = true;
            this._CleanupModal.Type = ModalPopupType.Form;
            this._CleanupModal.ShowLoadingPlaceholder = true;
            this._CleanupModal.TargetControlID = this._HiddenCleanupLaunchButton.ID;
            this._CleanupModal.HeaderText = _GlobalResources.FileSystemCleanup;
            this._CleanupModal.HeaderIconAlt = _GlobalResources.FileSystemCleanup;
            this._CleanupModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_SAVE,
                                                                              ImageFiles.EXT_PNG,
                                                                              true);
            this._CleanupModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._CleanupModal.SubmitButtonCustomText = _GlobalResources.CleanupFileSystem;
            this._CleanupModal.SubmitButton.Command += this._CleanupFileSystem;
            this._CleanupModal.SubmitButton.Enabled = false;
            this._CleanupModal.SubmitButton.Visible = false; // NEED TO REMOVE THIS WHEN WE WANT TO AUTOMATICALLY DELETE FILE SYSTEM ITEMS

            this._HiddenCleanupLoadButton = new Button();
            this._HiddenCleanupLoadButton.ID = "HiddenCleanupLoadButton";
            this._HiddenCleanupLoadButton.Style.Add("display", "none");
            this._HiddenCleanupLoadButton.Command += this._BuildCleanupReport;
            this._CleanupModal.AddControlToBody(this._HiddenCleanupLoadButton);


            this.ActionsPanel.Controls.Add(this._CleanupModal);

        }
        #endregion

        #region _BuildCleanupReport
        /// <summary>
        /// Builds the cleanup report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _BuildCleanupReport(object sender, CommandEventArgs e)
        {
            DataTable accountsList = new DataTable();

            // get all accounts to iterate through if "All" is chosen in the account drop down,
            // otherwise just get the account we are working with
            if (this._AccountDropDownList.SelectedValue == "All") {
                accountsList = Library.Account.GetAccountsList();
            }else{
                accountsList.Columns.Add("idAccount");
                accountsList.Columns.Add("company");
                DataRow row = accountsList.NewRow();
                row["idAccount"] = this._AccountDropDownList.SelectedItem.Value;
                row["company"] = this._AccountDropDownList.SelectedItem.Text;
                accountsList.Rows.Add(row);
            }

            File.WriteAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "");

            // iterate through the list of accounts
            foreach (DataRow accountRow in accountsList.Rows)
            {
                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "CLEANUP REPORT FOR " + accountRow["company"] + "\r\n");
                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "------------------------------------------------------------\r\n");

                // get account info
                Library.Account accountObject = new Library.Account(Convert.ToInt32(accountRow["idAccount"]));

                // get database server info
                Library.DatabaseServer databaseServerObject = new Library.DatabaseServer(accountObject.IdDatabaseServer);

                // get server and database names from objects
                string serverName = databaseServerObject.ServerName;
                string databaseName = accountObject.DatabaseName;

                // Asentia Site Folder - Strip out trailing slash if it exists
                string asentiaRootFolder = this._AsentiaRootFolder.Text.Substring(
                    this._AsentiaRootFolder.Text.Length - 1, 1) != @"\" ?
                    this._AsentiaRootFolder.Text :
                    this._AsentiaRootFolder.Text.Substring(0, this._AsentiaRootFolder.Text.Length - 1);

                string configDirectory = asentiaRootFolder + @"\_config\";

                // CONFIG DIRECTORY

                // Get All Item IDs
                DataTable objectIds = Library.DatabaseServer.GetObjectIdsForFileSystemCleanup(serverName, databaseName);

                /*File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "THESE OBJECTS ARE IN THE DATABASE BUT NOT IN THE FILE SYSTEM\r\n");
                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "------------------------------------------------------------\r\n");

                foreach (DataRow row in objectIds.Rows)
                {
                    // CONFIG DIRECTORY - Find Objects that Are in the database but not in the config directory
                    if (!Directory.Exists(configDirectory + row["siteHostname"].ToString() + @"\" + row["objectName"].ToString() + @"\" + row["objectId"].ToString()))
                    {

                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), row["objectName"].ToString() + ": " + row["objectId"].ToString() + "\r\n");
                    }

                }*/

                // Find Objects that are in the config directory but not in the database
                DirectoryInfo dirInfo = new DirectoryInfo(configDirectory);
                DirectoryInfo[] hostnameDirectories = dirInfo.GetDirectories();

                // ITERATE THROUGH THE DIRECTORIES AND CHECK EACH ID IN THE DATATABLE
                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "\r\nCONFIG DIRECTORY CLEANUP\r\n");
                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "------------------------------------------------------------\r\n");

                foreach (DirectoryInfo hostnameDir in hostnameDirectories)
                {
                    string hostname = hostnameDir.Name;
                    File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "\r\nHOSTNAME: " + hostname + "\r\n");
                    File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "---------------------------------\r\n");

                    DirectoryInfo[] objectDirectories = hostnameDir.GetDirectories();

                    foreach (DirectoryInfo objectDir in objectDirectories)
                    {
                        string objectName = objectDir.Name;
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "\r\nOBJECT: " + objectName + "\r\n");
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "--------------------------------\r\n");

                        DirectoryInfo[] idDirectories = objectDir.GetDirectories();
                        foreach (DirectoryInfo idDir in idDirectories)
                        {
                            string objectId = idDir.Name;
                            int id;

                            // make sure the object id is numeric
                            if (int.TryParse(objectId, out id))
                            {
                                if (objectIds.Select("siteHostname = '" + hostname + "' AND objectName = '" + objectName + "' AND objectId = " + objectId).Length == 0)
                                {
                                    File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), objectId + "\r\n");
                                }
                            }
                        }
                    }
                }

                // CONTENT PACKAGES

                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "\r\nCONTENT PACKAGE CLEANUP\r\n");
                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "---------------------------------------------------------------\r\n");

                // get content package paths from database
                DataTable contentPackagePaths = Library.DatabaseServer.GetContentPackagePathsForFileSystemCleanup(serverName, databaseName);
                DataTable documentRepositoryObjectItemIds = Library.DatabaseServer.GetDocumentRepositoryItemObjectIdsForFileSystemCleanup(serverName, databaseName);

                string warehouseDirectory = asentiaRootFolder + @"\warehouse\";

                DirectoryInfo warehouseDir = new DirectoryInfo(warehouseDirectory);
                DirectoryInfo[] contentPackageDirectories = warehouseDir.GetDirectories();

                foreach (DirectoryInfo contentPackageDir in contentPackageDirectories)
                {
                    /*
                    // check content package path
                    if ((contentPackagePaths.Select("[path] = '/warehouse/" + contentPackageDir.Name + "'").Length == 0) && 
                        (documentRepositoryObjectItemIds.Select("siteHostname = '" + contentPackageDir.Name + "'").Length == 0))
                    {
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), contentPackageDir.Name + "\r\n");
                    }
                    */
                     
                     
                    // check document repository item object
                    if (documentRepositoryObjectItemIds.Select("siteHostname = '" + contentPackageDir.Name + "'").Length > 0)
                    {
                        // COURSE DOCUMENTS
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "\r\nCOURSE DOCUMENT CLEANUP FOR " + contentPackageDir.Name + "\r\n");
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "---------------------------------------------------------------------------------\r\n");

                        DirectoryInfo courseDocuments = new DirectoryInfo(warehouseDirectory + contentPackageDir.Name + @"\documents\course\");
                        DirectoryInfo[] courseDocumentsDirectories = courseDocuments.GetDirectories();
                        foreach (DirectoryInfo courseDocumentsDir in courseDocumentsDirectories)
                        {
                            if (documentRepositoryObjectItemIds.Select("siteHostname = '" + contentPackageDir.Name + "' AND objectName = 'course' AND objectId = " + courseDocumentsDir.Name).Length == 0)
                            {
                                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), courseDocumentsDir.Name + "\r\n");
                            }
                        }

                        // GROUP DOCUMENTS
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "\r\nGROUP DOCUMENT CLEANUP FOR " + contentPackageDir.Name + "\r\n");
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "--------------------------------------------------------------------------------\r\n");

                        DirectoryInfo groupDocuments = new DirectoryInfo(warehouseDirectory + contentPackageDir.Name + @"\documents\group\");
                        DirectoryInfo[] groupDocumentsDirectories = groupDocuments.GetDirectories();
                        foreach (DirectoryInfo groupDocumentsDir in groupDocumentsDirectories)
                        {
                            if (documentRepositoryObjectItemIds.Select("siteHostname = '" + contentPackageDir.Name + "' AND objectName = 'group' AND objectId = " + groupDocumentsDir.Name).Length == 0)
                            {
                                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), groupDocumentsDir.Name + "\r\n");
                            }
                        }

                        // LEARNING PATH DOCUMENTS
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "\r\nLEARNING PATH DOCUMENT CLEANUP " + contentPackageDir.Name + "\r\n");
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "----------------------------------------------------------------------------------------\r\n");

                        DirectoryInfo learningPathDocuments = new DirectoryInfo(warehouseDirectory + contentPackageDir.Name + @"\documents\learningpath\");
                        DirectoryInfo[] learningPathDocumentsDirectories = learningPathDocuments.GetDirectories();
                        foreach (DirectoryInfo learningPathDocumentsDir in learningPathDocumentsDirectories)
                        {
                            if (documentRepositoryObjectItemIds.Select("siteHostname = '" + contentPackageDir.Name + "' AND objectName = 'learningpath' AND objectId = " + learningPathDocumentsDir.Name).Length == 0)
                            {
                                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), learningPathDocumentsDir.Name + "\r\n");
                            }
                        }

                        // USER DOCUMENTS
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "\r\nUSER DOCUMENT CLEANUP " + contentPackageDir.Name + "\r\n");
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), "-------------------------------------------------------------------------------\r\n");

                        DirectoryInfo userDocuments = new DirectoryInfo(warehouseDirectory + contentPackageDir.Name + @"\documents\user\");
                        DirectoryInfo[] userDocumentsDirectories = userDocuments.GetDirectories();
                        foreach (DirectoryInfo userDocumentsDir in userDocumentsDirectories)
                        {
                            if (documentRepositoryObjectItemIds.Select("siteHostname = '" + contentPackageDir.Name + "' AND objectName = 'user' AND objectId = " + userDocumentsDir.Name).Length == 0)
                            {
                                File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), userDocumentsDir.Name + "\r\n");
                            }
                        }
                    }
                    else if (contentPackagePaths.Select("[path] = '/warehouse/" + contentPackageDir.Name + "'").Length == 0)
                    {
                        File.AppendAllText(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"), contentPackageDir.Name + "\r\n");
                    }
                }
            }

            this._CleanupModal.DisplayFeedback(String.Format(_GlobalResources.ClickHereToViewCleanupReport, @"<a href='../../_fileSystemCleanup/fileSystemCleanupReport.txt" + "' target='_blank'>", "</a>"), false);

           

        }

        #endregion

        #region _CleanupFileSystem
        /// <summary>
        /// Cleans up the file system
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _CleanupFileSystem(object sender, CommandEventArgs e)
        {
            try
            {
                // create stream reader to read the file
                StreamReader fileSystemCleanupReport = new StreamReader(Server.MapPath(@"~\_fileSystemCleanup\fileSystemCleanupReport.txt"));

                // Iterate through the text file and delete the item folders that do not exist in the database
                string line;
                string hostname = "";
                string objectName = "";
                string folderToDelete = "";

                // flags to determine which cleanup method we are on
                bool isConfig = false; // config folder cleanup
                bool isContent = false; // content package cleanup
                bool isDocument = false; // document repository cleanup

                // Asentia Site Folder - Strip out trailing slash if it exists
                string asentiaRootFolder = this._AsentiaRootFolder.Text.Substring(
                    this._AsentiaRootFolder.Text.Length - 1, 1) != @"\" ?
                    this._AsentiaRootFolder.Text :
                    this._AsentiaRootFolder.Text.Substring(0, this._AsentiaRootFolder.Text.Length - 1);

                while ((line = fileSystemCleanupReport.ReadLine()) != null)
                {
                    

                    if (line.Contains("HOSTNAME:")) // enter config directory cleanup
                    {
                        hostname = line.Split(':')[1].Trim();
                        isConfig = true;
                        isContent = false;
                        isDocument = false;
                    }else if(line.Contains("CONTENT PACKAGE")) // enter content package cleanup
                    {
                        isConfig = false;
                        isContent = true;
                        isDocument = false;
                    }
                    else if (line.Contains("DOCUMENT CLEANUP")) // enter document repository cleanup
                    {
                        isConfig = false;
                        isContent = false;
                        isDocument = true;
                    }

                    int objectId;

                    if (isConfig) // config directory cleanup
                    {
                        if (line.Contains("OBJECT:"))
                        {
                            objectName = line.Split(':')[1].Trim();
                        }
                        else if (int.TryParse(line, out objectId))
                        {
                            folderToDelete = asentiaRootFolder + @"\_config\" + hostname + @"\" + objectName + @"\" + line + "\r\n";
                            
                            if (Directory.Exists(folderToDelete))
                            {
                                Directory.Delete(folderToDelete, true);
                            }
                        }
                    }
                    else if (isContent) // content package cleanup
                    {
                        if (line != String.Empty)
                        {
                            if (int.TryParse(line.Substring(0, 1), out objectId))
                            {
                                folderToDelete = asentiaRootFolder + @"\warehouse\" + line + "\r\n";
                                if (Directory.Exists(folderToDelete))
                                {
                                    Directory.Delete(folderToDelete, true);
                                }
                            }
                        }
                    }
                    else if (isDocument) // document repository cleanup
                    {
                        if (line.Contains("COURSE")) // course document cleanup
                        {
                            objectName = "course";
                            hostname = line.Substring(24);
                        }
                        else if (line.Contains("GROUP")) // group document cleanup
                        {
                            objectName = "group";
                            hostname = line.Substring(23);
                        }
                        else if (line.Contains("LEARNING PATH")) // learning path document cleanup
                        {
                            objectName = "learningPath";
                            hostname = line.Substring(31);
                        }
                        else if (line.Contains("USER")) // user document cleanup
                        {
                            objectName = "user";
                            hostname = line.Substring(22);
                        }
                        else if (int.TryParse(line, out objectId))
                        {
                            folderToDelete = asentiaRootFolder + @"\warehouse\" + hostname + @"\documents\" + objectName + @"\" + line + "\r\n";
                            if (Directory.Exists(folderToDelete))
                            {
                                Directory.Delete(folderToDelete, true);
                            }
                        } 
                    }
                }

                this._CleanupModal.DisplayFeedback(_GlobalResources.TheFileSystemHasBeenCleanedUpSuccessfully, false);
            }
            catch (AsentiaException ex)
            {
                this._CleanupModal.DisplayFeedback(ex.Message, true);
            }
        }

        #endregion


    }
}
