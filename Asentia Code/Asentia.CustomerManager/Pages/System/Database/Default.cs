﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.CustomerManager.Pages.System.Database
{
    public class Default : CustomerManagerAuthenticatedPage
    {
        public Panel ObjectOptionsPanel;
        public UpdatePanel DatabaseGridUpdatePanel;
        public Grid DatabaseGrid;
        public Panel GridActionsPanel = new Panel();
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup GridConfirmAction;

        #region Page_Load
        /// <summary>
        /// Page load.
        /// </summary>
        /// <param name="sender">Page</param>
        /// <param name="e">Event arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.DatabaseServers));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(_GlobalResources.DatabaseServers, ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));


            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the grid, actions panel, and modal
            this._BuildGrid();
            this._BuildObjectOptionsPanel();
            this._BuildGridActionsPanel();
            this._BuildGridActionsModal();

            // if not postback


            if (!IsPostBack)
            {
                // bind data grid
                this.DatabaseGrid.BindData();
            }
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.DatabaseGrid = new Grid("CustomerManager");
            this.DatabaseGrid.ID = "DatabaseGrid";
            this.DatabaseGrid.OverrideLoadImagesFromCustomerManager = true;
            this.DatabaseGrid.DBType = DatabaseType.CustomerManager;
            this.DatabaseGrid.StoredProcedure = Library.DatabaseServer.GridProcedure;
            this.DatabaseGrid.AddFilter("@idCallerAccount", SqlDbType.Int, 4, AsentiaSessionState.IdAccount);
            this.DatabaseGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdAccountUser);
            this.DatabaseGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.DatabaseGrid.IdentifierField = "idDatabaseServer";
            this.DatabaseGrid.DefaultSortColumn = "serverName";

            // data key names
            this.DatabaseGrid.DataKeyNames = new string[] { "idDatabaseServer" };

            // columns

            GridColumn serverName = new GridColumn(_GlobalResources.ServerName, "serverName", "serverName");
            GridColumn networkName = new GridColumn(_GlobalResources.NetworkName, "networkName", "networkName");
            GridColumn username = new GridColumn(_GlobalResources.Username, "username", "username");
            GridColumn isTrusted = new GridColumn(_GlobalResources.UseTrustedConnection, "isTrusted", "isTrusted");

            GridColumn modify = new GridColumn(_GlobalResources.Modify, "isModifyOn", true);
            modify.AddProperty(new GridColumnProperty("True", "<a href=\"Modify.aspx?id=##idDatabaseServer##\">"
                                                                + "<img class=\"SmallIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG, true)
                                                                + "\" alt=\"" + _GlobalResources.Modify + "\" />"
                                                                + "</a>"));
            modify.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG, true)
                                                                + "\" alt=\"" + _GlobalResources.ModifyDisabled + "\" />"));

            // add columns to data grid
            this.DatabaseGrid.AddColumn(serverName);
            this.DatabaseGrid.AddColumn(networkName);
            this.DatabaseGrid.AddColumn(username);
            this.DatabaseGrid.AddColumn(isTrusted);
            this.DatabaseGrid.AddColumn(modify);

            // attach the grid to the update panel
            this.DatabaseGridUpdatePanel.ContentTemplateContainer.Controls.Add(this.DatabaseGrid);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD COURSE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddSiteLink",
                                                null,
                                                "Modify.aspx",
                                                null,
                                                _GlobalResources.NewDatabaseServer,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_SITE, ImageFiles.EXT_PNG, true),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG, true))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsPanel()
        {
            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG,
                                                          true);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedDatabase_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.GridActionsPanel.Controls.Add(this.DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction = new ModalPopup("GridConfirmAction");
            this.GridConfirmAction.OverrideLoadImagesFromCustomerManager = true;

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedDatabase_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            Literal body2 = new Literal();
            body1Wrapper.ID = "GridConfirmActionModalBody1";

            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseDatabase_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.GridActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.DatabaseGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.DatabaseGrid.Rows[i].FindControl(this.DatabaseGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Asentia.CustomerManager.Library.DatabaseServer.Delete(recordsToDelete);
                // display the success message
                this.DisplayFeedback(_GlobalResources.TheSelectedDatabase_sHaveBeenDeletedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.DatabaseGrid.BindData();
            }
        }
        #endregion
    }
}
