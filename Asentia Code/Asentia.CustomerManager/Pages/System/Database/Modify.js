﻿//handles the use trusted connection checkbox actions
$(document).ready(function () {

    if ($("#IsTrusted_Field").prop('checked'))
    {
        $("#UserName_Field,#Password_Field").val("");
        $("#UserName_Field,#Password_Field").prop("disabled", $("#IsTrusted_Field").prop('checked'));

    }
   

    $("#IsTrusted_Field").click(function () {
        $("#UserName_Field,#Password_Field").val("");
        $("#UserName_Field,#Password_Field").attr('disabled', this.checked);
    });

});

