﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.CustomerManager.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.CustomerManager.Pages.System.Database
{
    public class Modify : CustomerManagerAuthenticatedPage
    {
        #region Public Properties
        public Panel DatabasePropertiesContainer;
        public Panel DatabasePropertiesInstructionsPanel;
        public Panel DatabasePropertiesTabPanelsContainer;
        public Panel DatabasePropertiesActionsPanel;
        public Panel DatabasePropertiesFeedbackContainer;
        #endregion

        #region Private Properties
        private DatabaseServer _DatabaseObject;
        private TextBox _ServerName;
        private TextBox _NetworkName;
        private TextBox _UserName;
        private TextBox _Password;
        private CheckBox _IsTrusted;
        private Button _SaveButton;
        private Button _CancelButton;
        private LinkButton _DeleteAliasButton;

        private const string _RegexPatternForFullyQualifiedDomainName = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b";
        private const string _RegexNetworkName = @"\w[0-9A-Z]\\\w[0-9A-Z]";
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            base.OnPreRender(e);

            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.CustomerManager.Pages.System.Database.Modify.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the site object
            this._GetDatabaseObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();

            if (!Page.IsPostBack)
            { }
        }
        #endregion

        #region _GetSiteObject
        /// <summary>
        /// Gets a database object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetDatabaseObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    {
                        this._DatabaseObject = new DatabaseServer(id);
                    }
                }
                catch
                { Response.Redirect("~/Database"); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // clear controls from wrapper container
            this.DatabasePropertiesContainer.Controls.Clear();

            // build the site properties form tabs
            this._BuildDatabasePropertiesFormTabs();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.DatabasePropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisDatabaseServer, true);

            this.DatabasePropertiesTabPanelsContainer = new Panel();
            this.DatabasePropertiesTabPanelsContainer.ID = "SiteProperties_TabPanelsContainer";
            this.DatabasePropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.DatabasePropertiesContainer.Controls.Add(this.DatabasePropertiesTabPanelsContainer);
            
            // build the database properties form
            this._BuildPropertiesForm();

            this._BuildPropertiesActionsPanel();

        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string siteImagePath;
            string pageTitle;

            if (this._DatabaseObject != null)
            {
                string siteTitleInInterfaceLanguage = this._DatabaseObject.ServerName;

                breadCrumbPageTitle = siteTitleInInterfaceLanguage;

                pageTitle = siteTitleInInterfaceLanguage;

                siteImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                         ImageFiles.EXT_PNG, true);
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewDatabaseServer;

                siteImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                         ImageFiles.EXT_PNG, true);

                pageTitle = _GlobalResources.NewDatabaseServer;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();

            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.DatabaseServers, "/System/database/"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));
        }
        #endregion

        #region _BuildDatabasePropertiesFormTabs
        /// <summary>
        /// Method to build the database property form tabs
        /// </summary>
        private void _BuildDatabasePropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));            

            // build and attach the tabs
            this.DatabasePropertiesContainer.Controls.Add(CustomerManagerPage.BuildTabListPanel("DatabaseProperties", tabs));
        }
        #endregion

        #region _BuildPropertiesForm
        /// <summary>
        /// Builds the properties form.
        /// </summary>
        private void _BuildPropertiesForm()
        {
            this._BuildDatabasePropertiesFormPanel();

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _BuildSitePropertiesFormPanel
        /// <summary>
        /// Builds the database properties form fields under properties tab.
        /// </summary>
        private void _BuildDatabasePropertiesFormPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "DatabaseProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.Attributes.Add("style", "display: block;");

            #region Server Name field container

            this._ServerName = new TextBox();
            this._ServerName.ID = "ServerName_Field";
            this._ServerName.CssClass = "InputLong";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("ServerName",
                                                             _GlobalResources.ServerName,
                                                             this._ServerName.ID,
                                                             this._ServerName,
                                                             true,
                                                             true));
            #endregion

            #region Network Name container

            this._NetworkName = new TextBox();
            this._NetworkName.ID = "NetworkName_Field";
            this._NetworkName.CssClass = "InputLong";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("NetworkName",
                                                             _GlobalResources.NetworkName,
                                                             this._NetworkName.ID,
                                                             this._NetworkName,
                                                             true,
                                                             true));
            #endregion

            #region Is Trusted field container

            this._IsTrusted = new CheckBox();
            this._IsTrusted.ID = "IsTrusted_Field";
            this._IsTrusted.Text = _GlobalResources.UseTrustedConnection;
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("IsTrusted",
                                                             "",
                                                             this._IsTrusted.ID,
                                                             this._IsTrusted,
                                                             false,
                                                             true));
            #endregion

            #region User Name field container

            this._UserName = new TextBox();
            this._UserName.ID = "UserName_Field";
            this._UserName.CssClass = "InputLong";
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("UserName",
                                                             _GlobalResources.Username,
                                                             this._UserName.ID,
                                                             this._UserName,
                                                             false,
                                                             true));
            #endregion

            #region Password field container
            //Password
            this._Password = new TextBox();
            this._Password.ID = "Password_Field";
            this._Password.CssClass = "InputLong";
            this._Password.TextMode = TextBoxMode.Password;
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("Password",
                                                             _GlobalResources.Password,
                                                             this._Password.ID,
                                                             this._Password,
                                                             false,
                                                             true));
            #endregion

            this.DatabasePropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for properties actions.
        /// </summary>
        private void _BuildPropertiesActionsPanel()
        {
            // clear controls from container
            this.DatabasePropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.DatabasePropertiesActionsPanel.CssClass = "FormActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._DatabaseObject == null)
            { this._SaveButton.Text = _GlobalResources.CreateDatabase; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.DatabasePropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.DatabasePropertiesActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            if (this._DatabaseObject != null)
            {
                // database SPECIFIC PROPERTIES
                this._NetworkName.Text = this._DatabaseObject.NetworkName;
                this._ServerName.Text = this._DatabaseObject.ServerName;
                this._UserName.Text = this._DatabaseObject.UserName;
                this._Password.Text = this._DatabaseObject.Password;
                this._IsTrusted.Checked = this._DatabaseObject.IsTrusted;

            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;

            // Network Name -IS REQUIRED 
            if (String.IsNullOrWhiteSpace(this._NetworkName.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabasePropertiesContainer, "NetworkName", _GlobalResources.NetworkName + " " + _GlobalResources.IsRequired);
            }

            else if (this._NetworkName.Text.Trim().Contains(" "))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabasePropertiesContainer, "NetworkName", _GlobalResources.NetworkName + "  " + _GlobalResources.SpaceIsNotAllowedInNetworkName);

            }
            // Network Name -must be fully qualified name(this validation have been removed as per client suggestion). 
           
            // Server Name -IS REQUIRED
            if (String.IsNullOrWhiteSpace(this._ServerName.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabasePropertiesContainer, "ServerName", _GlobalResources.ServerName + " " + _GlobalResources.IsRequired);
            }
            if (!this._IsTrusted.Checked && this._DatabaseObject == null)
            {
                if (String.IsNullOrWhiteSpace(this._UserName.Text))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.DatabasePropertiesContainer, "UserName", _GlobalResources.Username + " " + _GlobalResources.IsRequired);
                }
                if (String.IsNullOrWhiteSpace(this._Password.Text))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.DatabasePropertiesContainer, "Password", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
                }
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.DatabasePropertiesContainer, "DatabaseProperties_Properties_TabLI"); }


            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidatePropertiesForm())
                {
                    throw new AsentiaException();
                }

                // if there is no course object, create one
                if (this._DatabaseObject == null)
                { this._DatabaseObject = new DatabaseServer(); }

                this._DatabaseObject.ServerName = this._ServerName.Text.Trim();
                this._DatabaseObject.NetworkName = this._NetworkName.Text.Trim();
                this._DatabaseObject.IsTrusted = this._IsTrusted.Checked;
                if (this._IsTrusted.Checked)
                {
                    this._DatabaseObject.Password = null;
                    this._DatabaseObject.UserName = null;
                }
                else
                {
                    this._DatabaseObject.Password = this._Password.Text.Trim();
                    this._DatabaseObject.UserName = this._UserName.Text.Trim();
                }

                //sets true otherwise        

                bool isServerNameExists = false;
                isServerNameExists = Asentia.CustomerManager.Library.DatabaseServer.DoesServerNameExists(this._ServerName.Text);

                if (isServerNameExists && this._DatabaseObject.idDatabaseServer <= 0)
                {
                    // display the failure message
                    this.DisplayFeedbackInSpecifiedContainer(this.DatabasePropertiesFeedbackContainer, _GlobalResources.AServerWithTheSameNameAlreadyExistsPleaseTryAgain, true);

                }
                else
                {
                    //saves site details
                    int idDatabaseServer = this._DatabaseObject.Save();
                    this.ViewState["id"] = idDatabaseServer;

                    // build the page controls
                    this._BuildControls();

                    // display the saved feedback
                    this.DisplayFeedbackInSpecifiedContainer(this.DatabasePropertiesFeedbackContainer, _GlobalResources.DatabaseDetailsHaveBeenSavedSuccessfully, false);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.DatabasePropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.DatabasePropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.DatabasePropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.DatabasePropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.DatabasePropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/System/database");
        }
        #endregion
    }
}
