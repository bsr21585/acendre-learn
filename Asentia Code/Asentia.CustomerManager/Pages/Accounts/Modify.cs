﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using defaultSystem = System;

namespace Asentia.CustomerManager.Pages.Accounts
{
    public class Modify : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel ActionsPanel;
        public Panel PageInstructionsPanel;
        public Panel DatabaseFormContainer;
        public Panel PageFeedbackContainer;
        public UpdatePanel DataBaseFormUpdatePanel;

        #endregion

        #region Private Properties
        private Library.Account _AccountObject;

        private Button _SaveButton = new Button();
        private Button _CancelButton = new Button();

        private TextBox _CompanyName;
        private TextBox _ContactFirstName;
        private TextBox _ContactLastName;
        private TextBox _ContactEmail;
        private TextBox _Password;
        private TextBox _ConfirmPassword;
        private TextBox _UserName;
        private TextBox _DatabaseName;

        private DropDownList _Status;
        private DropDownList _ServerName;
        private const string _RegexPatternForEmailId = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            //  Get account user's details
            this._GetAccountObject();

            // build the breadcrumb
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisAccount, true);

            //builds page controls
            this._BuildControls();
        }
        #endregion

        #region _GetAccountObject
        /// <summary>
        /// Gets a user object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetAccountObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._AccountObject = new Library.Account(id); }
                }
                catch
                { Response.Redirect("~/accounts/"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            string breadCrumbPageTitle;
            string pageTitle;
            if (this._AccountObject != null)
            {
                breadCrumbPageTitle = this._AccountObject.Company;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewAccount;
            }
            pageTitle = breadCrumbPageTitle;

            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Accounts, "/accounts/"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));

            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds page controls
        /// </summary>
        private void _BuildControls()
        {
            //adding update progress to update panel
            this.DataBaseFormUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            UpdateProgress dataBaseFormUpdateProgress = new UpdateProgress();
            dataBaseFormUpdateProgress.AssociatedUpdatePanelID = this.DataBaseFormUpdatePanel.ID;
            dataBaseFormUpdateProgress.ProgressTemplate = new UpdateProgressTemplate(true);
            dataBaseFormUpdateProgress.ID = "DataBaseFormUpdateProgress";
            this.PageContentContainer.Controls.Add(dataBaseFormUpdateProgress);

            // build the user form
            this._BuildUserForm();

            // build the form actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "FormActionsPanel";

            // save button
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";

            if (this._AccountObject == null)
            { this._SaveButton.Text = _GlobalResources.CreateAccount; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }

        #endregion

        #region _BuildUserForm
        /// <summary>
        /// Builds the user form.
        /// </summary>
        private void _BuildUserForm()
        {
            //Company name feild
            this._CompanyName = new TextBox();
            this._CompanyName.ID = "Company" + "_Field";
            this._CompanyName.CssClass = "InputMedium";

            this.DatabaseFormContainer.Controls.Add(BuildFormField("Company",
                                                                _GlobalResources.Company,
                                                                _CompanyName.ID,
                                                                _CompanyName,
                                                                true,
                                                                true));

            //First name feild
            this._ContactFirstName = new TextBox();
            this._ContactFirstName.ID = "FirstName" + "_Field";
            this._ContactFirstName.CssClass = "InputMedium";

            this.DatabaseFormContainer.Controls.Add(BuildFormField("FirstName",
                                                                _GlobalResources.FirstName,
                                                                _ContactFirstName.ID,
                                                                _ContactFirstName,
                                                                true,
                                                                true));
            //Last name feild
            this._ContactLastName = new TextBox();
            this._ContactLastName.ID = "LastName" + "_Field";
            this._ContactLastName.CssClass = "InputMedium";

            this.DatabaseFormContainer.Controls.Add(BuildFormField("LastName",
                                                                _GlobalResources.LastName,
                                                                _ContactLastName.ID,
                                                                _ContactLastName,
                                                                true,
                                                                true));

            //Email feild
            this._ContactEmail = new TextBox();
            this._ContactEmail.ID = "Email" + "_Field";
            this._ContactEmail.CssClass = "InputMedium";

            this.DatabaseFormContainer.Controls.Add(BuildFormField("Email",
                                                                _GlobalResources.Email,
                                                                _ContactEmail.ID,
                                                                _ContactEmail,
                                                                true,
                                                                true));
            //Username feild
            this._UserName = new TextBox();
            this._UserName.ID = "Username" + "_Field";
            this._UserName.CssClass = "InputMedium";

            this.DatabaseFormContainer.Controls.Add(BuildFormField("Username",
                                                                _GlobalResources.Username,
                                                                _UserName.ID,
                                                                _UserName,
                                                                true,
                                                                true));

            //Password feild

            #region passwordAndConfirmPassword
            // merging two controls password and Confirm password in a panel

            Panel userFieldInputContainer = new Panel();
            userFieldInputContainer.ID = "Password" + "_InputContainer";
            userFieldInputContainer.CssClass = "FormFieldInputContainer";

            this._Password = new TextBox();
            this._Password.ID = "Password" + "_Field";
            this._Password.TextMode = TextBoxMode.Password;
            this._Password.CssClass = "Password" + "_Field";

            userFieldInputContainer.Controls.Add(this._Password);

            // label
            Panel userFieldLabelContainer = new Panel();
            userFieldLabelContainer.ID = "Password" + "_LabelContainer";
            userFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label userFieldLabel = new Label();
            userFieldLabel.Text = _GlobalResources.Password + ":";
            userFieldLabel.AssociatedControlID = "Password" + "_Field";

            userFieldLabelContainer.Controls.Add(userFieldLabel);

            // Asterisk label Field
            Label requiredAsterisk = new Label();
            requiredAsterisk.Text = " * ";
            requiredAsterisk.CssClass = "RequiredAsterisk";
            userFieldLabelContainer.Controls.Add(requiredAsterisk);

            // error panel
            Panel userFieldErrorContainer = new Panel();
            userFieldErrorContainer.ID = "Password" + "_ErrorContainer";
            userFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to container
            this.DatabaseFormContainer.Controls.Add(userFieldLabelContainer);
            this.DatabaseFormContainer.Controls.Add(userFieldErrorContainer);
            this.DatabaseFormContainer.Controls.Add(userFieldInputContainer);

            // field
            Panel confirmPasswordInputContainer = new Panel();
            confirmPasswordInputContainer.ID = "Password" + "Confirm_InputContainer";
            confirmPasswordInputContainer.CssClass = "FormFieldInputContainer";
           
            //Password confirmation field
            this._ConfirmPassword = new TextBox();
            this._ConfirmPassword.ID = "Password" + "Confirm_Field";
            this._ConfirmPassword.TextMode = TextBoxMode.Password;
            this._ConfirmPassword.CssClass = "password_Field";
            this._ConfirmPassword.MaxLength = 10;

            confirmPasswordInputContainer.Controls.Add(this._ConfirmPassword);

            // label
            Panel confirmPasswordLabelContainer = new Panel();
            confirmPasswordLabelContainer.ID = "Password" + "Confirm_LabelContainer";
            confirmPasswordLabelContainer.CssClass = "FormFieldLabelContainer";

            Label confirmPasswordLabel = new Label();
            confirmPasswordLabel.Text = _GlobalResources.ConfirmByEnteringAgain + ":";
            confirmPasswordLabel.AssociatedControlID = "Password" + "Confirm_Field";
            confirmPasswordLabelContainer.Controls.Add(confirmPasswordLabel);

            Panel deviderPanel = new Panel();
            deviderPanel.CssClass = "FormFieldContainer";

            // add controls to container
            this.DatabaseFormContainer.Controls.Add(confirmPasswordLabelContainer);
            this.DatabaseFormContainer.Controls.Add(confirmPasswordInputContainer);
            this.DatabaseFormContainer.Controls.Add(deviderPanel);

            #endregion

            //Status feild
            this._Status = new DropDownList();
            this._Status.ID = "Status" + "_Field";
            this._Status.CssClass = "InputMedium";

            this._Status.Items.Add(new ListItem(_GlobalResources.Enabled, "True"));
            this._Status.Items.Add(new ListItem(_GlobalResources.Disabled, "False"));

            this.DatabaseFormContainer.Controls.Add(BuildFormField("Status",
                                                                    _GlobalResources.Status,
                                                                    _Status.ID,
                                                                    _Status,
                                                                    false,
                                                                    false));

            //Server Name feild
            this._ServerName = new DropDownList();
            this._ServerName.ID = "DatabaseServer";

            this._ServerName.DataSource = Library.DatabaseServer.IdsAndNamesForDatabaseServerList();
            this._ServerName.DataValueField = "idDatabaseServer";
            this._ServerName.DataTextField = "ServerName";
            this._ServerName.DataBind();

            this.DatabaseFormContainer.Controls.Add(BuildFormField("DatabaseServer",
                                                                _GlobalResources.DatabaseServers,
                                                                _ServerName.ID,
                                                                _ServerName,
                                                                false,
                                                                false));

            //Database feild
            this._DatabaseName = new TextBox();
            this._DatabaseName.ID = "DatabaseName" + "_Field";
            this._DatabaseName.CssClass = "InputMedium";

            this.DatabaseFormContainer.Controls.Add(BuildFormField("DatabaseName",
                                                                _GlobalResources.DatabaseName,
                                                                _DatabaseName.ID,
                                                                _DatabaseName,
                                                                true,
                                                                true));



            this._PopulateInputControls();
        }
        #endregion

        #region _PopulateInputControls
        /// <summary>
        /// Populates all input controls
        /// </summary>
        private void _PopulateInputControls()
        {
            if (this._AccountObject != null)
            {
                //POPULATE FIELDS

                //comapny name
                this._CompanyName.Text = this._AccountObject.Company;

                //first name
                this._ContactFirstName.Text = this._AccountObject.ContactFirstName;

                //last name
                this._ContactLastName.Text = this._AccountObject.ContactLastName;

                //email 
                this._ContactEmail.Text = this._AccountObject.ContactEmail;

                //user name
                this._UserName.Text = this._AccountObject.UserName;

                //Status
                this._Status.SelectedValue = Convert.ToString(this._AccountObject.IsActive);

                //databse name
                this._DatabaseName.Text = this._AccountObject.DatabaseName;
                this._DatabaseName.Enabled = false;

                //databse server name
                if (this._ServerName.Items.FindByValue(Convert.ToString(this._AccountObject.IdDatabaseServer)) != null)
                {
                    this._ServerName.SelectedValue = Convert.ToString(this._AccountObject.IdDatabaseServer);
                }

                this._ServerName.Enabled = false;
            }
            else
            {
                //Status
                this._Status.SelectedValue = "True";
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/accounts/");
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click, Save/update the new/existing user details.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                bool existingAccount = false;

                // perform validation on the user data
                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                // if there is no user object, create one
                if (this._AccountObject == null)
                { this._AccountObject = new Library.Account(); }

                //comapny name
                this._AccountObject.Company = this._CompanyName.Text;

                //first name
                this._AccountObject.ContactFirstName = this._ContactFirstName.Text;

                //last name
                this._AccountObject.ContactLastName = this._ContactLastName.Text;

                //email 
                this._AccountObject.ContactEmail = this._ContactEmail.Text;

                //user name
                this._AccountObject.UserName = this._UserName.Text;

                //password
                this._AccountObject.Password = this._Password.Text.Trim();

                //Status
                this._AccountObject.IsActive = Convert.ToBoolean(this._Status.SelectedValue);

                //database
                this._AccountObject.DatabaseName = this._DatabaseName.Text;

                //database server
                this._AccountObject.IdDatabaseServer = Convert.ToInt32(this._ServerName.SelectedItem.Value);

                //Save
                int idAccount = this._AccountObject.Save();
                this.ViewState["id"] = idAccount;

                #region CREATE NEW CONFIG FOLDER AND CREATE A OBJECT SPECIFIC CONFIG FILE

                // Specify the directory you want to manipulate.
                string configFolderPath = MapPathSecure(SitePathConstants.CM_ACCOUNTS + idAccount);

                Library.DatabaseServer objectDatabaseServer = new Library.DatabaseServer(this._AccountObject.IdDatabaseServer);
                try
                {
                    // Determine whether the directory exists.
                    if (Directory.Exists(configFolderPath))
                    {
                        existingAccount = true;
                    }

                    if (!existingAccount)
                    {
                        // Try to create the directory.
                        DirectoryInfo configFolder = Directory.CreateDirectory(configFolderPath);

                        //template config file path
                        string sourceFile = MapPathSecure(SitePathConstants.CM_TEMPLATE + "Web.config");

                        //destination file path
                        string destFile = MapPathSecure(SitePathConstants.CM_ACCOUNTS + idAccount + "/Web.config");

                        //read text from template config file path
                        string configFileData = File.ReadAllText(sourceFile);

                        //replace text with objects value 
                        configFileData = configFileData.Replace("##TemplateServerValue##", objectDatabaseServer.NetworkName);
                        configFileData = configFileData.Replace("##TemplateDatabaseValue##", this._AccountObject.DatabaseName);

                        if (objectDatabaseServer.IsTrusted)
                        {
                            configFileData = configFileData.Replace("##TemplateUseTrustedConnectionValue##", "true");
                            configFileData = configFileData.Replace("##TemplateLoginValue##", "");
                            configFileData = configFileData.Replace("##TemplatePasswordValue##", "");
                        }
                        else
                        {
                            configFileData = configFileData.Replace("##TemplateUseTrustedConnectionValue##", "false");
                            configFileData = configFileData.Replace("##TemplateLoginValue##", objectDatabaseServer.UserName);
                            configFileData = configFileData.Replace("##TemplatePasswordValue##", objectDatabaseServer.Password);
                        }

                        //write the new config settings
                        File.WriteAllText(destFile, configFileData);
                    }

                }
                catch (Exception ex)
                {
                    // display the failure message
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.ErrorHasOccurredWhileConfiguringTheWebConfigSettings, true);
                    return;
                }
                #endregion

                #region RUN SCRIPT TO CREATE NEW DATABASE

                try
                {
                    if (!existingAccount)
                    {
                        using (SqlConnection connection = new SqlConnection())
                        {

                            using (SqlConnection connectionMaster = new SqlConnection(this._CreateConnectionString(objectDatabaseServer)))
                            {
                                connectionMaster.Open();

                                String str = "CREATE DATABASE [" + this._AccountObject.DatabaseName + "]";

                                SqlCommand myCommand = new SqlCommand(str, connectionMaster);

                                myCommand.ExecuteNonQuery();

                            }
                            connection.ConnectionString = this._CreateConnectionString(objectDatabaseServer, this._AccountObject.DatabaseName);
                            connection.Open();

                            string[] scriptFiles = Directory.GetFiles(MapPathSecure(SitePathConstants.CM_ACCOUNTDATABASESCRIPTS), "*.sql");

                            foreach (string scriptFilePath in scriptFiles)
                            {
                                string script = File.ReadAllText(scriptFilePath);

                                // split script on GO command
                                IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*GO\s*$",
                                                         RegexOptions.Multiline | RegexOptions.IgnoreCase);

                                foreach (string commandString in commandStrings)
                                {
                                    if (commandString.Trim() != "")
                                    {
                                        using (var command = new SqlCommand(commandString, connection))
                                        {
                                            command.ExecuteNonQuery();
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // display the failure message
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.ErrorHasOccurredWhileCreatingTheDatabase, true);
                    return;
                }
                #endregion

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.AccountHasBeenCreatedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }

        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;

            // Company Name field
            if (String.IsNullOrWhiteSpace(this._CompanyName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Company", _GlobalResources.Company + " " + _GlobalResources.IsRequired);
            }

            // First Name field
            if (String.IsNullOrWhiteSpace(this._ContactFirstName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "FirstName", _GlobalResources.FirstName + " " + _GlobalResources.IsRequired);
            }

            // Last Name field
            if (String.IsNullOrWhiteSpace(this._ContactLastName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "LastName", _GlobalResources.LastName + " " + _GlobalResources.IsRequired);
            }

            // Email Name field
            if (String.IsNullOrWhiteSpace(this._ContactEmail.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Email", _GlobalResources.Email + " " + _GlobalResources.IsRequired);
            }
            else
            {
                //EMAIL ADDRESS SHOULD BE VALID
                if (!Regex.IsMatch(this._ContactEmail.Text.Trim(), _RegexPatternForEmailId))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Email", _GlobalResources.ContactEmail + " " + _GlobalResources.IsInvalid);
                }
            }

            // Username field
            if (String.IsNullOrWhiteSpace(this._UserName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Username", _GlobalResources.Username + " " + _GlobalResources.IsRequired);
            }

            // Password Name field
            if (String.IsNullOrWhiteSpace(this._Password.Text.Trim()))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Password", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
            }
            else if (String.IsNullOrWhiteSpace(this._ConfirmPassword.Text.Trim()))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "PasswordConfirm", _GlobalResources.Password + " " + _GlobalResources.ConfirmByEnteringAgain);
            }
            else if (this._Password.Text.Trim() != this._ConfirmPassword.Text.Trim())
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Password", _GlobalResources.Password + " " + _GlobalResources.MustMatchConfirmationField);
            }

            // Database Name field
            if (String.IsNullOrWhiteSpace(this._DatabaseName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "DatabaseName", _GlobalResources.DatabaseName + " " + _GlobalResources.IsRequired);
            }

            return isValid;
        }
        #endregion

        #region _CreateConnectionString
        /// <summary>
        /// Creates dynamic connection string
        /// </summary>
        /// <param name="objectDatabaseServer"></param>
        /// <param name="databaseName"></param>
        /// <returns></returns>
        private string _CreateConnectionString(Library.DatabaseServer objectDatabaseServer, String databaseName = "master")
        {
            if (objectDatabaseServer.IsTrusted)
            {
                return "Data Source=" + objectDatabaseServer.NetworkName + ";" + "Initial Catalog=" + databaseName + "; Integrated Security=SSPI;";
            }
            else
            {
                return "Data Source=" + objectDatabaseServer.NetworkName + ";" + "Initial Catalog=" + databaseName + ";" + "User id=" + objectDatabaseServer.UserName + ";" + "Password=" + objectDatabaseServer.Password + ";";
            }
        }

    }
        #endregion
}
