﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.CustomerManager.Library;

namespace Asentia.CustomerManager.Pages.Accounts
{
    public class Default : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public UpdatePanel AccountGridUpdatePanel;
        public Grid AccountGrid;
        public Panel GridActionsPanel = new Panel();
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup GridConfirmAction = new ModalPopup();

        public ModalPopup UpdateStatusModal;
        public HiddenField UpdateStatusDataHiddenField;
        public Button UpdateStatusModalHiddenButton;
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Accounts));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(_GlobalResources.Accounts, ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildGridActionsPanel();
            this._BuildGridActionsModal();
            this._BuildUpdateStatusModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.AccountGrid.BindData();
            }
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.CustomerManager.Pages.Accounts.Default.js");

            base.OnPreRender(e);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.AccountGrid = new Grid("CustomerManager");
            this.AccountGrid.ID = "AccountGrid";
            this.AccountGrid.OverrideLoadImagesFromCustomerManager = true;
            this.AccountGrid.DBType = DatabaseType.CustomerManager;
            this.AccountGrid.StoredProcedure = Library.Account.GridProcedure;
            this.AccountGrid.AddFilter("@idCallerAccount", SqlDbType.Int, 4, AsentiaSessionState.IdAccount);
            this.AccountGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdAccountUser);
            this.AccountGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.AccountGrid.IdentifierField = "idAccount";
            this.AccountGrid.DefaultSortColumn = "company";

            // data key names
            this.AccountGrid.DataKeyNames = new string[] { "idAccount", "company", "contactDisplayName", "contactEmail" };

            // columns
            GridColumn company = new GridColumn(_GlobalResources.Company + ", " + _GlobalResources.ContactName + " (" + _GlobalResources.Email + ")", "company", "company");
            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // generated dynamically by row data bound event
            /*GridColumn isActive = new GridColumn(_GlobalResources.Status, null, true);

            GridColumn jump = new GridColumn(_GlobalResources.Log_In, "isLogonOn", true);
            jump.AddProperty(new GridColumnProperty("True", "<a href=\"Impersonate.aspx?aid=##idAccount##\">"
                                                                + "<img class=\"SmallIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                                                                                         ImageFiles.EXT_PNG,
                                                                                         true)
                                                                + "\" alt=\"" + _GlobalResources.Jump + "\" />"
                                                                + "</a>"));
            jump.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                                                                                         ImageFiles.EXT_PNG,
                                                                                         true)
                                                                + "\" alt=\"" + _GlobalResources.JumpDisabled + "\" />"));*/

            // add columns to data grid
            this.AccountGrid.AddColumn(company);
            this.AccountGrid.AddColumn(options);
            /*this.AccountGrid.AddColumn(isActive);
            this.AccountGrid.AddColumn(jump);*/

            this.AccountGrid.RowDataBound += AccountGrid_RowDataBound;

            // attach the grid to the update panel
            this.AccountGridUpdatePanel.ContentTemplateContainer.Controls.Add(this.AccountGrid);
        }

        #endregion

        #region AccountGrid_RowDataBound
        /// <summary>
        /// Row Data Bound event handler for account grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void AccountGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                bool isActive = Convert.ToBoolean(rowView["isActive"]);
                int idAccount = Convert.ToInt32(rowView["idAccount"]);
                string company = rowView["company"].ToString();
                string contactDisplayName = rowView["contactDisplayName"].ToString();
                string contactEmail = rowView["contactEmail"].ToString();
                bool isLogonOn = Convert.ToBoolean(rowView["isLogonOn"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY, ImageFiles.EXT_PNG, true);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = company;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // title
                Label titleLinkLabel = new Label();
                titleLinkLabel.CssClass = "GridBaseTitle";

                HyperLink titleLink = new HyperLink();
                titleLink.Text = company;
                titleLink.NavigateUrl = "Modify.aspx?id=" + idAccount.ToString();
                titleLinkLabel.Controls.Add(titleLink);
                e.Row.Cells[1].Controls.Add(titleLinkLabel);

                // contact display name (contact email)
                Label contactLabel = new Label();
                contactLabel.CssClass = "GridSecondaryLine";
                contactLabel.Text = contactDisplayName + " (" + contactEmail + ")";
                e.Row.Cells[1].Controls.Add(contactLabel);
                
                // status
                Label statusSpan = new Label();
                statusSpan.CssClass = "GridImageLink";

                LinkButton statusLinkButton = new LinkButton();
                statusLinkButton.ID = "StatusLinkButton";
                statusLinkButton.OnClientClick = "UpdateAccountStatus('" + idAccount.ToString() + "', '" + isActive.ToString() + "'); return false;";

                Image statusImage = new Image();
                statusImage.ID = "StatusImage";
                statusImage.CssClass = "SmallIcon";

                // set the appropriate link image based on the account being active or not
                if(isActive)
                {
                    statusLinkButton.ToolTip = _GlobalResourcesCHETU1.DisableAccount;
                    statusImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN,
                                            ImageFiles.EXT_PNG,
                                            true);
                    
                }
                else
                {
                    statusLinkButton.ToolTip = _GlobalResourcesCHETU1.EnableAccount;
                    statusImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED,
                                            ImageFiles.EXT_PNG,
                                            true);
                }

                statusLinkButton.Controls.Add(statusImage);
                statusSpan.Controls.Add(statusLinkButton);

                e.Row.Cells[2].Controls.Add(statusSpan);

                // log in
                Label loginSpan = new Label();
                loginSpan.CssClass = "GridImageLink";

                LinkButton loginLinkButton = new LinkButton();
                loginLinkButton.ID = "LoginLinkButton";

                Image loginImage = new Image();
                loginImage.ID = "LoginImage";
                loginImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                                            ImageFiles.EXT_PNG,
                                            true);

                // set the appropriate link image based on the login being on or not
                if (isLogonOn)
                {
                    loginLinkButton.ToolTip = _GlobalResources.Log_In;
                    loginLinkButton.Enabled = true;
                    loginLinkButton.PostBackUrl = "Impersonate.aspx?aid=" + idAccount;
                    loginImage.CssClass = "SmallIcon";

                }
                else
                {
                    loginLinkButton.Enabled = false;
                    loginLinkButton.PostBackUrl = "";
                    loginImage.CssClass = "SmallIcon DimIcon";
                }

                loginLinkButton.Controls.Add(loginImage);
                loginSpan.Controls.Add(loginLinkButton);

                e.Row.Cells[2].Controls.Add(loginSpan);


                
            }
            
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD USER
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddAccountLink",
                                                null,
                                                "Modify.aspx",
                                                null,
                                                _GlobalResources.NewAccount,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY, ImageFiles.EXT_PNG, true),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG, true))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsPanel()
        {
            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG,
                                                          true);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedAccount_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.GridActionsPanel.Controls.Add(this.DeleteButton);

            // hidden update status data
            this.UpdateStatusDataHiddenField = new HiddenField();
            this.UpdateStatusDataHiddenField.ID = "UpdateStatusDataHiddenField";
            
            // add hidden status data to page
            this.GridActionsPanel.Controls.Add(this.UpdateStatusDataHiddenField);

            // hidden button to trigger update status modal
            this.UpdateStatusModalHiddenButton = new Button();
            this.UpdateStatusModalHiddenButton.ID = "UpdateStatusModalHiddenButton";
            this.UpdateStatusModalHiddenButton.Style.Add("display", "none");

            // add hidden button to page
            this.GridActionsPanel.Controls.Add(this.UpdateStatusModalHiddenButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction.ID = "GridConfirmAction";

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.OverrideLoadImagesFromCustomerManager = true;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedAccount_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            Literal body2 = new Literal();
            body1Wrapper.ID = "GridConfirmActionModalBody1";

            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseAccount_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.GridActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.AccountGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.AccountGrid.Rows[i].FindControl(this.AccountGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Asentia.CustomerManager.Library.Account.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedback(_GlobalResources.TheSelectedAccount_sHaveSuccessfullyBeenDeleted, false);

                // rebind the grid
                this.AccountGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this.AccountGrid.BindData();
            }
        }
        #endregion

        #region _BuildUpdateStatusModal
        /// <summary>
        /// Builds the modal for updating the status of an account.
        /// </summary>
        private void _BuildUpdateStatusModal()
        {
            this.UpdateStatusModal = new ModalPopup();
            this.UpdateStatusModal.ID = "UpdateStatusModal";

            // set modal properties
            this.UpdateStatusModal.Type = ModalPopupType.Confirm;
            this.UpdateStatusModal.OverrideLoadImagesFromCustomerManager = true;
            this.UpdateStatusModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this.UpdateStatusModal.HeaderIconAlt = _GlobalResources.UpdateStatus;
            this.UpdateStatusModal.HeaderText = _GlobalResources.UpdateStatus;
            this.UpdateStatusModal.TargetControlID = this.UpdateStatusModalHiddenButton.ID;
            this.UpdateStatusModal.SubmitButton.Command += new CommandEventHandler(this._UpdateStatus_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            Literal body2 = new Literal();
            body1Wrapper.ID = "UpdateStatusModalBody1";

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.UpdateStatusModal.AddControlToBody(body1Wrapper);

            this.GridActionsPanel.Controls.Add(this.UpdateStatusModal);
        }
        #endregion

        #region _UpdateStatus_Command
        /// <summary>
        /// Updates the status of an account.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _UpdateStatus_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string[] data = this.UpdateStatusDataHiddenField.Value.Split('|');
                int idAccount = Convert.ToInt32(data[0]);
                int isActive = Convert.ToInt32(data[1]);
                
                // update the status
                Account account = new Account(idAccount);
                if (isActive == 1)
                {
                    account.IsActive = true;
                }
                else
                {
                    account.IsActive = false;
                }
                account.Save();

                // display the success message
                if (isActive == 1)
                {
                    this.DisplayFeedback(_GlobalResources.TheAccountHasBeenEnabledSuccessfully, false);
                }
                else
                {
                    this.DisplayFeedback(_GlobalResources.TheAccountHasBeenDisabledSuccessfully, false);
                }

                // rebind the grid
                this.AccountGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this.AccountGrid.BindData();
            }
        }
        #endregion
    }
}
