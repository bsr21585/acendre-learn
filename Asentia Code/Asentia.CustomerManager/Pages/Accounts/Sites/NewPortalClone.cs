﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.CustomerManager.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using web = System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Services = System.Web.Services;
using ScriptServices = System.Web.Script.Services;


namespace Asentia.CustomerManager.Pages.Accounts.Sites
{
    public class NewPortalClone : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel SitePropertiesContainer;
        public Panel SitePropertiesInstructionsPanel;
        public Panel SitePropertiesTabPanelsContainer;
        public Panel SitePropertiesActionsPanel;
        public Panel SitePropertiesFeedbackContainer;
        public Panel passworrdContainerPannel;
        public Panel userFieldErrorContainer;

        #endregion

        #region Private Properties
        private AsentiaSite _SiteObject;

        private DropDownList _SiteSource;
        private DropDownList _Status;
        private DropDownList _DefaultLanguage;

        private TextBox _Title;
        private TextBox _HostName;
        private TextBox _Company;
        private TextBox _ContactName;
        private TextBox _ContactEmail;
        private TextBox _UserLimit;
        private TextBox _KbLimit;
        private TextBox _Password;
        private TextBox _ConfirmPassword;
        private TextBox _DomainAlias;
        private CheckBox _CopyLicenseAgreementCheckBox;
        private Button _ViewLicenseAgreementButton;

        private CheckBox _kbLimitNone;
        private CheckBox _UserLimitNone;


        private DatePicker _DtExpires;
        private TimeZoneSelector _Timezone;

        private Button _SaveButton;
        private Button _CancelButton;
        private LinkButton _DeleteAliasButton;

        private CheckBoxList _AvailableLanguages;

        private Dictionary<string, string> _AvailableLanguageList;

        private HiddenField _SiteSourceHidden;
        private HiddenField _DefaultLanguageHidden;
        private HiddenField _DomainAliaIdHidden;


        private Panel _SiteDomainAliasesListContainer;
        private Panel _ModalPropertiesContainer;

        private ModalPopup _DomainAliasModal;
        private ModalPopup _DeleteDomainAliasModal;
        private ModalPopup _ViewLicenseAgreementModal;

        private ModalPopup _LoadingModal;
        private HiddenField _CloneStatusHiddenField;
        private Button _HiddenModalButton;

        private HiddenField _IdAccountHiddenField;
        private HiddenField _DatabaseNameHiddenField;
        private HiddenField _DatabaseServerHiddenField;

        private UpdatePanel _DomainAliasUpdatePanel;

        private DataTable _SiteList;
        private DataTable _SiteDomainAliases;
        private DataTable _ClonePortalIdMappings = new DataTable();

        // FEATURES
        private CheckBox _ECommerce;
        private CheckBox _Certifications;
        private CheckBox _QuizzesAndSurveys;
        private CheckBox _GoToMeeting;
        private CheckBox _GoToWebinar;
        private CheckBox _GoToTraining;
        private CheckBox _WebEx;
        private CheckBox _OpenSesame;

        private const string _RegexPatternForEmailId = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        private const string _RegexPatternForFullyQualifiedDomainName = @"([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?";
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            base.OnPreRender(e);

            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.CustomerManager.Pages.Accounts.Sites.Modify.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the site object
            this._GetSiteObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();

            // build view license agreement modal
            this._BuildViewLicenseAgreementModal();

            // build the loading modal
            this._BuildLoadingModal();
        }
        #endregion

        #region _GetSiteObject
        /// <summary>
        /// Gets a site object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetSiteObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._SiteObject = new AsentiaSite(id, 1, id); }
                }
                catch
                { Response.Redirect("~/Sites"); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // clear controls from wrapper container
            this.SitePropertiesContainer.Controls.Clear();

            // build the site properties form tabs
            this._BuildSitePropertiesFormTabs();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.SitePropertiesInstructionsPanel, _GlobalResourcesCHETU1.CompleteTheFormBelowToPerformPartialCloningOfSourceSiteIntoNewlyCreatedDestinationSite, true);

            this.SitePropertiesTabPanelsContainer = new Panel();
            this.SitePropertiesTabPanelsContainer.ID = "SiteProperties_TabPanelsContainer";
            this.SitePropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.SitePropertiesContainer.Controls.Add(this.SitePropertiesTabPanelsContainer);

            // build the site properties form
            this._BuildPropertiesForm();

            // build the site properties action panel
            this._BuildPropertiesActionsPanel();

            this._BuildDeleteAliasControls();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string siteImagePath;
            string pageTitle;

            if (this._SiteObject != null)
            {
                string siteTitleInInterfaceLanguage = this._SiteObject.Title;

                breadCrumbPageTitle = siteTitleInInterfaceLanguage;

                pageTitle = siteTitleInInterfaceLanguage;

                siteImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                         ImageFiles.EXT_PNG,
                                                         true);
            }
            else
            {
                breadCrumbPageTitle = _GlobalResourcesCHETU1.NewSiteFromTemplate;

                siteImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                         ImageFiles.EXT_PNG,
                                                         true);

                pageTitle = _GlobalResourcesCHETU1.NewSiteFromTemplate;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();

            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Sites, "/Accounts/sites"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));
        }
        #endregion

        #region _BuildSitePropertiesFormTabs
        /// <summary>
        /// Method to build the site property form tabs
        /// </summary>
        private void _BuildSitePropertiesFormTabs()
        {
            Panel sitePropertiesTabsPanel = new Panel();
            sitePropertiesTabsPanel.ID = "SiteProperties_TabsPanel";

            // UL for tabs
            HtmlGenericControl sitePropertiesTabsUL = new HtmlGenericControl("ul");
            sitePropertiesTabsUL.ID = "SiteProperties_TabsUL";
            sitePropertiesTabsUL.Attributes.Add("class", "TabbedList");

            // PROPERTIES TAB
            // "Properties" is the default tab, so this is "on" on page load.
            HtmlGenericControl propertiesTabLI = new HtmlGenericControl("li");
            propertiesTabLI.ID = "SiteProperties_" + "Properties" + "_TabLI";
            propertiesTabLI.Attributes.Add("onclick", "Helper.ToggleTab('" + "Properties" + "', '" + "SiteProperties" + "');");
            propertiesTabLI.Attributes.Add("class", "TabbedListLI TabbedListLIOn");

            HyperLink propertiesTabLink = new HyperLink();
            propertiesTabLink.ID = "SiteProperties_" + "Properties" + "_TabLink";
            propertiesTabLink.CssClass = "TabLink";
            propertiesTabLink.NavigateUrl = "javascript:void(0);";
            propertiesTabLink.Text = _GlobalResources.Properties;

            // attach controls
            propertiesTabLI.Controls.Add(propertiesTabLink);
            sitePropertiesTabsUL.Controls.Add(propertiesTabLI);
            // END PROPERTIES TAB

            if (this._SiteObject != null)
            {
                // DOMAIN ALIAS TAB
                HtmlGenericControl domainAliasTabLI = new HtmlGenericControl("li");
                domainAliasTabLI.ID = "SiteProperties_" + "DomainAliases" + "_TabLI";
                domainAliasTabLI.Attributes.Add("onclick", "Helper.ToggleTab('" + "DomainAliases" + "', '" + "SiteProperties" + "');");
                domainAliasTabLI.Attributes.Add("class", "TabbedListLI");

                HyperLink domainAliasTabLink = new HyperLink();
                domainAliasTabLink.ID = "SiteProperties_" + "DomainAliases" + "_TabLink";
                domainAliasTabLink.CssClass = "TabLink";
                domainAliasTabLink.NavigateUrl = "javascript:void(0);";
                domainAliasTabLink.Text = _GlobalResources.DomainAliases;

                // attach controls
                domainAliasTabLI.Controls.Add(domainAliasTabLink);
                sitePropertiesTabsUL.Controls.Add(domainAliasTabLI);
                // END DOMAIN ALIAS TAB
            }

            // attach tab ul control
            sitePropertiesTabsPanel.Controls.Add(sitePropertiesTabsUL);
            this.SitePropertiesContainer.Controls.Add(sitePropertiesTabsPanel);
        }
        #endregion

        #region _BuildPropertiesForm
        /// <summary>
        /// Builds the properties form.
        /// </summary>
        private void _BuildPropertiesForm()
        {
            this._BuildSitePropertiesFormPanel();

            this._BuildSitePropertiesFormDomainAliasesPanel();

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _BuildSitePropertiesFormPanel
        /// <summary>
        /// Builds the site properties form fields under properties tab.
        /// </summary>
        private void _BuildSitePropertiesFormPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "SiteProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.Attributes.Add("style", "display: block;");

            #region Site Source container
            //Site Source

            this._SiteSource = new DropDownList();
            this._SiteSource.ID = "SiteSource_Field";
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("SiteSource",
                                                            _GlobalResourcesCHETU1.SiteSource,
                                                             this._SiteSource.ID,
                                                             this._SiteSource,
                                                             false,
                                                             false));

            this._SiteSourceHidden = new HiddenField();
            this._SiteSourceHidden.ID = "SiteSourcehidden";
            propertiesPanel.Controls.Add(_SiteSourceHidden);
            this._SiteSource.Items.Clear();


            // get the site list from passed database server and datatable.
            this._SiteList = DatabaseServer.GetIdSiteFromDatabase(Config.AccountDatabaseSettings.Server, Config.AccountDatabaseSettings.Name);

            //Bind the source site drop down
            this._SiteSource.DataSource = _SiteList;
            this._SiteSource.DataTextField = "hostname";
            this._SiteSource.DataValueField = "idSite";
            this._SiteSource.DataBind();
            this._SiteSource.Attributes.Add("onchange", "changeSourceSite();");


            #endregion

            #region Title field container
            //title
            this._Title = new TextBox();
            this._Title.ID = "Name_Field";
            this._Title.CssClass = "InputLong";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("Name",
                                                             _GlobalResources.Title,
                                                             this._Title.ID,
                                                             this._Title,
                                                             true,
                                                             true));
            #endregion

            #region Host Name field container
            //Host Name
            this._HostName = new TextBox();
            this._HostName.ID = "HostName_Field";
            this._HostName.CssClass = "InputShort";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("HostName",
                                                             _GlobalResources.Hostname,
                                                             this._HostName.ID,
                                                             this._HostName,
                                                             true,
                                                             true));
            #endregion

            #region Password field

            // password and confirm password input control's list 
            List<Control> PasswordInputControlList = new List<Control>();

            // password input control
            this._Password = new TextBox();
            this._Password.ID = "Password" + "_Field";
            this._Password.TextMode = TextBoxMode.Password;
            this._Password.CssClass = "Password" + "_Field";
            PasswordInputControlList.Add(this._Password);

            // confirm password label control's container panel
            Panel confirmPasswordLabelContainer = new Panel();
            confirmPasswordLabelContainer.ID = "ConfirmPasswordField_Container";
            confirmPasswordLabelContainer.CssClass = "FormFieldDescriptionContainer";

            // confirm password label control
            Literal confirmPasswordLabel = new Literal();
            confirmPasswordLabel.Text = _GlobalResources.ConfirmByEnteringAgain;
            confirmPasswordLabelContainer.Controls.Add(confirmPasswordLabel);

            PasswordInputControlList.Add(confirmPasswordLabelContainer);

            // confirm password input control
            this._ConfirmPassword = new TextBox();
            this._ConfirmPassword.ID = "Password" + "Confirm_Field";
            this._ConfirmPassword.TextMode = TextBoxMode.Password;
            this._ConfirmPassword.CssClass = "password_Field";
            PasswordInputControlList.Add(this._ConfirmPassword);


            // if creating the new site then show the password as required field, otherwise not
            if (this._SiteObject == null)
            {
                // add all password and confirm password controls to main panel
                propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Password",
                                                                                         _GlobalResources.Password,
                                                                                         PasswordInputControlList,
                                                                                         true,
                                                                                         true,
                                                                                         true));
            }
            else
            {
                // add all password and confirm password controls to main panel
                propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Password",
                                                                                         _GlobalResources.Password,
                                                                                         PasswordInputControlList,
                                                                                         false,
                                                                                         true,
                                                                                         true));
            }
            #endregion

            #region Company field container
            //Company
            this._Company = new TextBox();
            this._Company.ID = "Company_Field";
            this._Company.CssClass = "InputLong";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("Company",
                                                             _GlobalResources.Company,
                                                             this._Company.ID,
                                                             this._Company,
                                                             false,
                                                             false
                                                             ));
            #endregion

            #region Contact Name field container
            //Contact Name
            this._ContactName = new TextBox();
            this._ContactName.ID = "ContactName_Field";
            this._ContactName.CssClass = "InputLong";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("ContactName",
                                                             _GlobalResources.ContactName,
                                                             this._ContactName.ID,
                                                             this._ContactName,
                                                             false,
                                                             false));
            #endregion

            #region Contact Email container
            //Contact Email
            this._ContactEmail = new TextBox();
            this._ContactEmail.ID = "ContactEmail_Field";
            this._ContactEmail.CssClass = "InputLong";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("ContactEmail",
                                                             _GlobalResources.ContactEmail,
                                                             this._ContactEmail.ID,
                                                             this._ContactEmail,
                                                             false,
                                                             true
                                                             ));
            #endregion

            #region User Limit field container
            //User Limit

            List<Control> userLimitnputControls = new List<Control>();

            this._UserLimit = new TextBox();
            this._UserLimit.ID = "UserLimit_Field";
            this._UserLimit.CssClass = "InputXShort";
            this._UserLimit.MaxLength = 10;


            this._UserLimitNone = new CheckBox();
            this._UserLimitNone.ID = "UserLimitNone";
            this._UserLimitNone.Checked = true;
            this._UserLimitNone.Text = _GlobalResources.Unlimited;

            userLimitnputControls.Add(this._UserLimit);
            userLimitnputControls.Add(this._UserLimitNone);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("UserLimit",
                                                                                      _GlobalResources.UserLimit,
                                                                                   userLimitnputControls,
                                                                                   false,
                                                                                   true));
            #endregion

            #region KB Limit field container
            //KB Limit

            List<Control> kbLimitnputControls = new List<Control>();

            this._KbLimit = new TextBox();
            this._KbLimit.ID = "KbLimit_Field";
            this._KbLimit.CssClass = "InputXShort";
            this._KbLimit.MaxLength = 10;

            this._kbLimitNone = new CheckBox();
            this._kbLimitNone.ID = "KbLimitNone";
            this._kbLimitNone.Text = _GlobalResources.Unlimited;
            this._kbLimitNone.Checked = true;
            kbLimitnputControls.Add(this._KbLimit);
            kbLimitnputControls.Add(this._kbLimitNone);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("KbLimit",
                                                                                     _GlobalResources.KBLimit + "(MB)",
                                                                                    kbLimitnputControls,
                                                                                    false,
                                                                                    true));
            #endregion

            #region Timezone field container
            //Timezone
            this._Timezone = new TimeZoneSelector();
            this._Timezone.ID = "TimeZone_Field";
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("TimeZone",
                                                             _GlobalResources.Timezone,
                                                             this._Timezone.ID,
                                                             this._Timezone,
                                                             false,
                                                             false
                                                             ));

            string timeZoneName = Timezone.GetTimezoneNameFromDotnetName(Config.AccountSettings.DefaultTimezoneDotNetName);
            if (!String.IsNullOrWhiteSpace(timeZoneName) && this._Timezone.Items.FindByText(timeZoneName) != null)
            {
                this._Timezone.Items.FindByText(timeZoneName).Selected = true;
            }

            #endregion

            #region Default Language container
            //Default Language

            this._DefaultLanguage = new DropDownList();
            this._DefaultLanguage.ID = "DefaultLanguage_Field";
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("DefaultLanguage",
                                                             _GlobalResources.DefaultLanguage,
                                                             this._DefaultLanguage.ID,
                                                             this._DefaultLanguage,
                                                             false,
                                                             false));

            this._DefaultLanguageHidden = new HiddenField();
            this._DefaultLanguageHidden.ID = "DefaultLanguagehidden";
            propertiesPanel.Controls.Add(_DefaultLanguageHidden);
            this._DefaultLanguage.Items.Clear();
            this._DefaultLanguage.Items.Add(new ListItem { Text = this.GetLanguageNameByCode(Config.AccountSettings.DefaultLanguage), Value = Config.AccountSettings.DefaultLanguage });

            #endregion

            #region Available Languages container
            //Available Languages

            this._AvailableLanguages = new CheckBoxList();
            this._AvailableLanguages.ID = "AvailableLanguages_Field";
            this._AvailableLanguages.RepeatDirection = RepeatDirection.Vertical;
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("AvailableLanguages",
                                                             _GlobalResources.AvailableLanguages,
                                                             this._AvailableLanguages.ID,
                                                             this._AvailableLanguages,
                                                             false,
                                                             false));


            _AvailableLanguageList = this.GetAvailableInstalledLanguages();

            this._AvailableLanguages.DataSource = _AvailableLanguageList;
            this._AvailableLanguages.DataTextField = "Value";
            this._AvailableLanguages.DataValueField = "key";
            this._AvailableLanguages.DataBind();
            #endregion

            #region Status field container
            //Status
            this._Status = new DropDownList();
            this._Status.ID = "IsActive";

            this._Status.Items.Add(new ListItem(_GlobalResources.Enabled, "True"));
            this._Status.Items.Add(new ListItem(_GlobalResources.Disabled, "False"));

            propertiesPanel.Controls.Add(BuildFormField("IsActive",
                                                        _GlobalResources.Status,
                                                        _Status.ID,
                                                        _Status,
                                                        false,
                                                        false));
            this._Status.SelectedIndex = 0;
            #endregion

            #region DtExpires field container

            //DtExpires
            this._DtExpires = new DatePicker("DtExpires_Field", true, false, false, true);
            this._DtExpires.NoneCheckboxText = _GlobalResources.NeverExpires;
            this._DtExpires.NoneCheckBoxChecked = true;
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("DtExpires",
                                                             _GlobalResources.ExpirationDate,
                                                             this._DtExpires.ID,
                                                             this._DtExpires,
                                                             false,
                                                             false));
            #endregion

            #region License Agreement Container

            List<Control> licenseAgreementControls = new List<Control>();

            // Copy License Agreement
            this._CopyLicenseAgreementCheckBox = new CheckBox();
            this._CopyLicenseAgreementCheckBox.ID = "CopyLicenseAgreement_Field";
            licenseAgreementControls.Add(this._CopyLicenseAgreementCheckBox);

            // View License Agreement
            this._ViewLicenseAgreementButton = new Button();
            this._ViewLicenseAgreementButton.ID = "ViewLicenseAgreement_Field";
            this._ViewLicenseAgreementButton.CssClass = "Button ActionButton";
            this._ViewLicenseAgreementButton.Text = _GlobalResources.ViewLicenseAgreement;
            licenseAgreementControls.Add(this._ViewLicenseAgreementButton);

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildMultipleInputControlFormField("LicenseAgreement_Fields", _GlobalResources.CopyLicenseAgreementToSite, licenseAgreementControls, false, false));

            #endregion

            #region Features field container

            // features field
            List<Control> featuresInputControls = new List<Control>();

            // e-commerce
            this._ECommerce = new CheckBox();
            this._ECommerce.ID = "Ecommerce_Field";
            this._ECommerce.ClientIDMode = ClientIDMode.Static;
            this._ECommerce.Text = _GlobalResources.ECommerce;
            featuresInputControls.Add(this._ECommerce);

            // certifications
            this._Certifications = new CheckBox();
            this._Certifications.ID = "Certifications_Field";
            this._Certifications.ClientIDMode = ClientIDMode.Static;
            this._Certifications.Text = _GlobalResources.Certifications;
            featuresInputControls.Add(this._Certifications);

            // quizzes and surveys
            this._QuizzesAndSurveys = new CheckBox();
            this._QuizzesAndSurveys.ID = "QuizzesAndSurveys_Field";
            this._QuizzesAndSurveys.ClientIDMode = ClientIDMode.Static;
            this._QuizzesAndSurveys.Text = _GlobalResourcesCHETU1.QuizzesAndSurveysAuthoring;
            featuresInputControls.Add(this._QuizzesAndSurveys);

            // GoToMeeting
            this._GoToMeeting = new CheckBox();
            this._GoToMeeting.ID = "GoToMeeting_Field";
            this._GoToMeeting.ClientIDMode = ClientIDMode.Static;
            this._GoToMeeting.Text = _GlobalResources.GoToMeeting;
            featuresInputControls.Add(this._GoToMeeting);

            // GoToWebinar
            this._GoToWebinar = new CheckBox();
            this._GoToWebinar.ID = "GoToWebinar_Field";
            this._GoToWebinar.ClientIDMode = ClientIDMode.Static;
            this._GoToWebinar.Text = _GlobalResources.GoToWebinar;
            featuresInputControls.Add(this._GoToWebinar);

            // GoToTraining
            this._GoToTraining = new CheckBox();
            this._GoToTraining.ID = "GoToTraining_Field";
            this._GoToTraining.ClientIDMode = ClientIDMode.Static;
            this._GoToTraining.Text = _GlobalResources.GoToTraining;
            featuresInputControls.Add(this._GoToTraining);

            // WebEx
            this._WebEx = new CheckBox();
            this._WebEx.ID = "WebEx_Field";
            this._WebEx.ClientIDMode = ClientIDMode.Static;
            this._WebEx.Text = _GlobalResources.WebEx;
            featuresInputControls.Add(this._WebEx);

            // OpenSesame
            this._OpenSesame = new CheckBox();
            this._OpenSesame.ID = "OpenSesame_Field";
            this._OpenSesame.ClientIDMode = ClientIDMode.Static;
            this._OpenSesame.Text = _GlobalResources.OpenSesame;
            featuresInputControls.Add(this._OpenSesame);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Features",
                                                                              _GlobalResourcesCHETU1.Features,
                                                                              featuresInputControls,
                                                                              false,
                                                                              true,
                                                                              true));
            #endregion

            // attach panel to container
            this.SitePropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }

        #endregion

        #region _BuildSitePropertiesFormDomainAliasesPanel
        /// <summary>
        /// Builds the site properties form fields under domain aliases tab.
        /// </summary>
        private void _BuildSitePropertiesFormDomainAliasesPanel()
        {
            // populate datatables with lists of users who are course experts and who are not
            if (this._SiteObject != null)
            {
                this._SiteDomainAliases = Library.Site.GetDomains(this._SiteObject.Id, this._SiteObject.Hostname);
            }

            Panel domainAliasesPanel = new Panel();
            domainAliasesPanel.ID = "SiteProperties_" + "DomainAliases" + "_TabPanel";
            domainAliasesPanel.CssClass = "TabPanelContainer";
            domainAliasesPanel.Attributes.Add("style", "display: none;");

            this._DomainAliasUpdatePanel = new UpdatePanel();
            this._DomainAliasUpdatePanel.ID = "DomainAliasUpdatePanel";
            this._DomainAliasUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;


            // build a container for the domain aliases listing
            this._SiteDomainAliasesListContainer = new Panel();
            this._SiteDomainAliasesListContainer.ID = "SiteDomainAliasesList";
            this._SiteDomainAliasesListContainer.CssClass = "ItemListingContainer";

            this._DomainAliasUpdatePanel.ContentTemplateContainer.Controls.Add(this._SiteDomainAliasesListContainer);

            domainAliasesPanel.Controls.Add(CustomerManagerPage.BuildFormField("SiteDomainAliasesList",
                                                         _GlobalResources.DomainAliases,
                                                        this._DomainAliasUpdatePanel.ID,
                                                        this._DomainAliasUpdatePanel,
                                                        false,
                                                        false));

            // domain alias  field
            List<Control> siteDomainAliasesInputControls = new List<Control>();

            Panel addDomainAliasButtonsPanel = new Panel();
            addDomainAliasButtonsPanel.ID = "AddDomainAlias_ButtonsPanel";

            // Add domain alias button
            // link
            Image addDomainAliasImageForLink = new Image();
            addDomainAliasImageForLink.ID = "LaunchAddDomainAliasModalImage";
            addDomainAliasImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD,
                                                                              ImageFiles.EXT_PNG, true);
            addDomainAliasImageForLink.CssClass = "MediumIcon";

            Localize selectAddDomainAliasTextForLink = new Localize();
            selectAddDomainAliasTextForLink.Text = _GlobalResources.AddDomainAlias;

            LinkButton addDomainAliasModalButton = new LinkButton();
            addDomainAliasModalButton.ID = "LaunchAddDomainAliasModalButton";
            addDomainAliasModalButton.CssClass = "ImageLink";
            addDomainAliasModalButton.Controls.Add(addDomainAliasImageForLink);
            addDomainAliasModalButton.Controls.Add(selectAddDomainAliasTextForLink);
            addDomainAliasModalButton.Attributes.Add("onClick", "javascript:HideDisplayFeedBack();");
            addDomainAliasButtonsPanel.Controls.Add(addDomainAliasModalButton);

            // attach the buttons panel to the container
            siteDomainAliasesInputControls.Add(addDomainAliasButtonsPanel);

            // build modals for adding and removing prerequisites to/from the course
            this._CreateDomainAliasModal(addDomainAliasModalButton.ID);

            domainAliasesPanel.Controls.Add(CustomerManagerPage.BuildMultipleInputControlFormField("DomainAlias",
                                                                                    String.Empty,
                                                                                    siteDomainAliasesInputControls,
                                                                                    false,
                                                                                    true));
            // attach panel to container
            this.SitePropertiesTabPanelsContainer.Controls.Add(domainAliasesPanel);
        }
        #endregion

        #region _CreateDomainAliasModal
        /// <summary>
        /// Builds the modal for adding domain alias.
        /// </summary>
        private void _CreateDomainAliasModal(string targetControlId)
        {
            // set modal properties
            this._DomainAliasModal = new ModalPopup("DomainAliasModal");
            this._DomainAliasModal.OverrideLoadImagesFromCustomerManager = true;

            this._DomainAliasModal.Type = ModalPopupType.Form;
            this._DomainAliasModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                                                       ImageFiles.EXT_PNG, true);
            this._DomainAliasModal.HeaderIconAlt = _GlobalResources.AddDomainAlias;
            this._DomainAliasModal.HeaderText = _GlobalResources.AddDomainAlias;
            this._DomainAliasModal.TargetControlID = targetControlId;
            this._DomainAliasModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._DomainAliasModal.SubmitButtonCustomText = _GlobalResources.AddDomainAlias;
            this._DomainAliasModal.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._DomainAliasModal.CloseButtonCustomText = _GlobalResources.Done;
            this._DomainAliasModal.ReloadPageOnClose = false;
            this._DomainAliasModal.SubmitButton.Command += new CommandEventHandler(this._AddandModifyDomainAliasModalSubmit_Command);

            // build the modal body
            this._ModalPropertiesContainer = new Panel();
            this._ModalPropertiesContainer.ID = "ModalPropertiesContainer";

            //domain alias
            this._DomainAlias = new TextBox();
            this._DomainAlias.ID = "DomainAlias_Field";
            this._DomainAlias.CssClass = "InputLong";

            // add controls to body
            this._ModalPropertiesContainer.Controls.Add(CustomerManagerPage.BuildFormField("DomainAlias",
                                                                            _GlobalResources.DomainAlias,
                                                                              this._DomainAlias.ID,
                                                                             this._DomainAlias,
                                                                             true,
                                                                             true));

            this._DomainAliasModal.AddControlToBody(this._ModalPropertiesContainer);

            // add modal to container
            this.SitePropertiesTabPanelsContainer.Controls.Add(this._DomainAliasModal);
        }
        #endregion

        #region _BuildPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for properties actions.
        /// </summary>
        private void _BuildPropertiesActionsPanel()
        {
            // clear controls from container
            this.SitePropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.SitePropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";
            this._SaveButton.Text = _GlobalResourcesCHETU1.CreateClonedPortal;
            this._SaveButton.Attributes.Add("onclick", "hiddenModalClick();");
            this.SitePropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.SitePropertiesActionsPanel.Controls.Add(this._CancelButton);

            // hidden button to trigger clone
            this._HiddenModalButton = new Button();
            this._HiddenModalButton.ID = "HiddenModalButton";
            this._HiddenModalButton.Text = "Hidden Button";
            this._HiddenModalButton.Style.Add("display", "none");
            this._HiddenModalButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.SitePropertiesActionsPanel.Controls.Add(this._HiddenModalButton);

            // hidden field to show clone status
            this._CloneStatusHiddenField = new HiddenField();
            this._CloneStatusHiddenField.ID = "CloneStatusHiddenField";
            this._CloneStatusHiddenField.Value = "Not Started";
            this.SitePropertiesActionsPanel.Controls.Add(this._CloneStatusHiddenField);

            // hidden field to hold account ID
            this._IdAccountHiddenField = new HiddenField();
            this._IdAccountHiddenField.ID = "IdAccountHiddenField";
            this._IdAccountHiddenField.Value = AsentiaSessionState.IdAccount.ToString();
            this.SitePropertiesActionsPanel.Controls.Add(this._IdAccountHiddenField);

            // hidden field to hold database server
            this._DatabaseServerHiddenField = new HiddenField();
            this._DatabaseServerHiddenField.ID = "DatabaseServerHiddenField";
            this._DatabaseServerHiddenField.Value = Config.AccountDatabaseSettings.Server;
            this.SitePropertiesActionsPanel.Controls.Add(this._DatabaseServerHiddenField);

            // hidden field to hold database name
            this._DatabaseNameHiddenField = new HiddenField();
            this._DatabaseNameHiddenField.ID = "DatabaseNameHiddenField";
            this._DatabaseNameHiddenField.Value = Config.AccountDatabaseSettings.Name;
            this.SitePropertiesActionsPanel.Controls.Add(this._DatabaseNameHiddenField);


        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            if (this._SiteObject != null)
            {

                // NON-LANGUAGE SPECIFIC PROPERTIES

                // assigning old idsite 
                this._SiteSource.SelectedValue = this.ViewStateInt(this.ViewState, "oldSiteId", 0).ToString();

                this._Title.Text = this._SiteObject.Title;

                this._Company.Text = this._SiteObject.Company;

                this._HostName.Text = this._SiteObject.Hostname;

                this._ContactEmail.Text = this._SiteObject.ContactEmail;

                this._ContactName.Text = this._SiteObject.ContactName;

                this._UserLimitNone.Checked = this._SiteObject.UserLimit > 0 ? false : true;
                if (!this._UserLimitNone.Checked)
                {
                    this._UserLimit.Text = Convert.ToString(this._SiteObject.UserLimit);
                }
                else
                {
                    this._UserLimit.Text = string.Empty;
                }

                this._kbLimitNone.Checked = this._SiteObject.KbLimit > 0 ? false : true;
                if (!this._kbLimitNone.Checked)
                {
                    this._KbLimit.Text = Convert.ToString(this._SiteObject.KbLimit);
                }
                else
                {
                    this._KbLimit.Text = string.Empty;
                }

                this._Timezone.SelectedValue = Convert.ToString(this._SiteObject.IdTimezone);

                this._Status.SelectedValue = Convert.ToString(this._SiteObject.IsActive);

                if (this._SiteObject.Expires != null && this._SiteObject.Expires is DateTime)
                {
                    this._DtExpires.Value = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._SiteObject.Expires, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._SiteObject.IdTimezone).dotNetName));
                    this._DtExpires.NoneCheckBoxChecked = false;
                }

                if (this._AvailableLanguages.Items.FindByValue(this._SiteObject.LanguageString) != null)
                {
                    this._AvailableLanguages.SelectedValue = this._SiteObject.LanguageString;
                }

                this._DefaultLanguage.Items.Clear();
                this._DefaultLanguage.Items.Add(new ListItem { Text = this.GetLanguageNameByCode(Config.AccountSettings.DefaultLanguage), Value = Config.AccountSettings.DefaultLanguage });

                foreach (ListItem item in this._AvailableLanguages.Items)
                {
                    if (item.Selected)
                    {
                        this._DefaultLanguage.Items.Add(item);
                    }
                }

                if (this._DefaultLanguage.Items.FindByValue(this._SiteObject.LanguageString) != null)
                {
                    this._DefaultLanguage.SelectedValue = this._SiteObject.LanguageString;
                }

                /* DOMAIN ALIASES */

                // loop through the datatable and add each member to the listing container
                foreach (DataRow row in this._SiteDomainAliases.Rows)
                {
                    // container
                    Panel userNameContainer = new Panel();
                    userNameContainer.ID = "Domain_" + row["id"].ToString();

                    // remove expert button
                    Image removeExpertImage = new Image();
                    removeExpertImage.ID = "Domain_" + row["id"].ToString() + "_RemoveImage";
                    removeExpertImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                        ImageFiles.EXT_PNG, true);
                    removeExpertImage.CssClass = "SmallIcon";
                    removeExpertImage.Attributes.Add("onClick", "javascript:RemoveDomainAlias('" + row["displayName"].ToString() + "');");
                    removeExpertImage.Style.Add("cursor", "pointer");

                    // expert name
                    Literal userName = new Literal();
                    userName.Text = row["displayName"].ToString();

                    // add controls to container
                    userNameContainer.Controls.Add(removeExpertImage);
                    userNameContainer.Controls.Add(userName);
                    this._SiteDomainAliasesListContainer.Controls.Add(userNameContainer);
                }

                // features

                this._ECommerce.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.ECOMMERCE_ENABLED);
                this._Certifications.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE);
                this._QuizzesAndSurveys.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE);
                this._GoToMeeting.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE);
                this._GoToWebinar.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE);
                this._GoToTraining.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE);
                this._WebEx.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE);
                this._OpenSesame.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.OPENSESAME_ENABLE);
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;
            bool domainAliasTabHasErrors = false;

            // TITLE - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._Title.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "Name", _GlobalResources.Title + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // PASSWORD -IS REQUIRED
            if (String.IsNullOrWhiteSpace(this._Password.Text.Trim()))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "Password", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
            }
            else
            {
                //PASSWORD SHOULD MATCH
                if (!this._Password.Text.Equals(this._ConfirmPassword.Text.Trim()))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "Password", _GlobalResources.Password + " " + _GlobalResources.MustMatchConfirmationField);
                }
            }

            //EMAIL ADDRESS SHOULD BE VALID
            if (!String.IsNullOrWhiteSpace(this._ContactEmail.Text))
            {
                if (!Regex.IsMatch(this._ContactEmail.Text.Trim(), _RegexPatternForEmailId))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "ContactEmail", _GlobalResources.ContactEmail + " " + _GlobalResources.IsInvalid);
                }
            }

            // HOSTNAME -IS REQUIRED
            if (String.IsNullOrWhiteSpace(this._HostName.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "HostName", _GlobalResources.Hostname + " " + _GlobalResources.IsRequired);
            }

            // USER LIMIT - IS REQUIRED & MUST BE POSITIVE INT
            if (!String.IsNullOrWhiteSpace(this._UserLimit.Text))
            {
                bool result = true;
                int userLimit;
                if (!int.TryParse(this._UserLimit.Text, out userLimit))
                {
                    result = false;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "UserLimit", _GlobalResources.UserLimit + " " + _GlobalResources.IsInvalid);
                }
                if (result && userLimit <= 0)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "UserLimit", _GlobalResources.UserLimit + " " + _GlobalResources.IsInvalid);
                }
            }

            // KB LIMIT - IS REQUIRED & MUST BE POSITIVE INT
            if (!String.IsNullOrWhiteSpace(this._KbLimit.Text))
            {
                bool result = true;
                int kbLimit;
                if (!int.TryParse(this._KbLimit.Text, out kbLimit))
                {
                    isValid = false;
                    result = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "KbLimit", _GlobalResources.KBLimit + " " + _GlobalResources.IsInvalid);
                }
                if (result && kbLimit <= 0)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "KbLimit", _GlobalResources.KBLimit + " " + _GlobalResources.IsInvalid);
                }
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.SitePropertiesContainer, "SiteProperties_Properties_TabLI"); }

            if (domainAliasTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.SitePropertiesContainer, "SiteProperties_DomainAliases_TabLI"); }

            return isValid;
        }
        #endregion

        #region _MaintainControlState
        /// <summary>
        /// Maintains the state of control (Javascript changes) after validations exception occurs.
        /// </summary>
        private void _MaintainControlState()
        {
            this._DefaultLanguage.Items.Clear();
            this._DefaultLanguage.Items.Add(new ListItem { Text = this.GetLanguageNameByCode(Config.AccountSettings.DefaultLanguage), Value = Config.AccountSettings.DefaultLanguage });
            foreach (ListItem item in this._AvailableLanguages.Items)
            {
                if (item.Selected)
                { this._DefaultLanguage.Items.Add(item); }
            }

            if (this._SiteObject != null && this._DefaultLanguage.Items.FindByValue(this._SiteObject.LanguageString) != null)
            {
                this._DefaultLanguage.SelectedValue = this._SiteObject.LanguageString;
            }
        }
        #endregion

        #region _AddandModifyDomainAliasModalSubmit_Command
        /// <summary>
        /// Handels the process of saving a "Domain Alias" .
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AddandModifyDomainAliasModalSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateAddAliasModalProperties())
                { throw new AsentiaException(); }

                // DO DOMIAN ALIASES TAB WORK
                Library.Account.SaveDomainAliasInCustomerManagerDataBase(AsentiaSessionState.IdAccount, this._DomainAlias.Text);

                // passed idCaller value as 1, because the Account DB procedure is accessed here from inside the Customer Manager application.
                Library.Site.SaveSiteToDomainAliasLink(this._SiteObject.Id, this._DomainAlias.Text, 1);

                // build the page controls
                this._BuildControls();

                _DomainAliasUpdatePanel.Update();

                // showing success feedback message
                this._DomainAliasModal.DisplayFeedback(_GlobalResources.DomainAliasHasBeenAddedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._DomainAliasModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._DomainAliasModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._DomainAliasModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._DomainAliasModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._DomainAliasModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _ValidateAddAliasModalProperties
        /// <summary>
        /// Validates the properties of modal.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateAddAliasModalProperties()
        {
            bool isValid = true;

            // HOSTNAME -IS REQUIRED
            if (String.IsNullOrWhiteSpace(this._DomainAlias.Text.Trim()))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._ModalPropertiesContainer, "DomainAlias", _GlobalResources.DomainAlias + " " + _GlobalResources.IsRequired);
            }
            else
            {
                //DOMAIN ALIAS SHOULD BE FULLY QUALIFIED
                if (!Regex.IsMatch(this._DomainAlias.Text.Trim(), _RegexPatternForFullyQualifiedDomainName))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._ModalPropertiesContainer, "DomainAlias", _GlobalResources.DomainAlias + " " + _GlobalResources.IsInvalid);
                }
            }
            return isValid;
        }
        #endregion

        #region _BuildDeleteAliasControls
        /// <summary>
        /// Builds controls to launch the delete alias confirmation modal
        /// </summary>
        private void _BuildDeleteAliasControls()
        {
            //hidden button to simulate the action on click of update 
            this._DeleteAliasButton = new LinkButton();
            this._DeleteAliasButton.ClientIDMode = ClientIDMode.Static;
            this._DeleteAliasButton.ID = "DeleteAliasButton";
            this._DeleteAliasButton.Style.Add("display", "none");

            this._DomainAliaIdHidden = new HiddenField();
            this._DomainAliaIdHidden.ID = "DomainAliaIdHidden";

            //add controls to container
            this.SitePropertiesActionsPanel.Controls.Add(this._DeleteAliasButton);
            this.SitePropertiesActionsPanel.Controls.Add(this._DomainAliaIdHidden);

            //Builds the confirmation modal for domain alias delete process.
            this._BuildDeleteAliasModal();
        }
        #endregion

        #region _BuildDeleteAliasModal
        /// <summary>
        /// Builds the confirmation modal for domain alias delete process.
        /// </summary>
        private void _BuildDeleteAliasModal()
        {
            this._DeleteDomainAliasModal = new ModalPopup("DeleteAliasAction");
            this._DeleteDomainAliasModal.OverrideLoadImagesFromCustomerManager = true;

            // set modal properties
            this._DeleteDomainAliasModal.Type = ModalPopupType.Confirm;
            this._DeleteDomainAliasModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this._DeleteDomainAliasModal.HeaderIconAlt = _GlobalResources.Delete;
            this._DeleteDomainAliasModal.HeaderText = _GlobalResources.DeleteDomainAlias;
            this._DeleteDomainAliasModal.TargetControlID = this._DeleteAliasButton.ClientID;
            this._DeleteDomainAliasModal.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            Literal body2 = new Literal();
            body1Wrapper.ID = "GridDeleteAliasActionModalBody1";

            body1.Text = _GlobalResources.AreYouSureYouWanttoDeleteSelectedDomainAlias;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._DeleteDomainAliasModal.AddControlToBody(body1Wrapper);

            this.SitePropertiesActionsPanel.Controls.Add(this._DeleteDomainAliasModal);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on domain alias.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(this._DomainAliaIdHidden.Value))
                {
                    // delete selected domain alias from account database
                    // passed idCaller value as 1, because the Account DB procedure is accessed here from inside the Customer Manager application.
                    Library.Site.DeleteDomainAlias(this._DomainAliaIdHidden.Value, this._SiteObject.Id, 1);

                    // delete selected domain alias from customer manager database
                    Library.Account.DeleteDomainAliasFromCustomerManagerDatabase(this._DomainAliaIdHidden.Value);

                    this._BuildControls();

                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, _GlobalResources.SelectedDomainAliasHasBeenDeletedSuccessfully, false);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, ex.Message, true);
            }
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            if (this._CloneStatusHiddenField.Value == "Success")
            {
                // Close the loading modal
                this._LoadingModal.HideModal();

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, _GlobalResourcesCHETU1.PortalHasBeenClonedSuccessfully, false);
            }
            else
            {
                // Close the loading modal
                this._LoadingModal.HideModal();

                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, _GlobalResourcesCHETU1.ErrorHasOccurredWhileCloneingTheDatabase + ": " + this._CloneStatusHiddenField.Value, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/Accounts/sites");
        }
        #endregion

        #region _ClonePartialPortalConfigObject

        /// <summary>
        /// Clone _config folder objects for destination portal for partial copy.
        /// </summary>
        /// <param name="clonePortalIdMappings">clone Portal IdMappings table</param>
        private void _ClonePartialPortalConfigObject(DataTable clonePortalIdMappings)
        {

            // format source source directory
            //String sourceDirectoryPath = Server.MapPath(SitePathConstants.CONFIG + this._SiteSource.SelectedItem.Text);
            String sourceDirectoryPath = Config.ApplicationSettings.AsentiaContentSourceRootPath + @"\_config\" + this._SiteSource.SelectedItem.Text;

            // replace the mapped path for customer manage website with asentia website
            //sourceDirectoryPath = sourceDirectoryPath.Replace("Asentia.CustomerManagerWebsite", "Asentia.Website");
            DirectoryInfo sourceDir = new DirectoryInfo(sourceDirectoryPath);

            // format destination directory
            //String destinationDirectoryPath = Server.MapPath(SitePathConstants.CONFIG + this._SiteObject.Hostname);
            String destinationDirectoryPath = Config.ApplicationSettings.AsentiaContentDestinationRootPath + @"\_config\" + this._SiteObject.Hostname;

            // replace the mapped path for customer manage website with asentia website
            //destinationDirectoryPath = destinationDirectoryPath.Replace("Asentia.CustomerManagerWebsite", "Asentia.Website");

            // check Source directory existence 
            if (!sourceDir.Exists)
            { throw new AsentiaException(String.Format(_GlobalResources.TheSourceDirectoryDoesNotExistOrCouldNotBeFound, sourceDirectoryPath)); }


            // copy source hostname into destination hostname
            Utility.CopyDirectory(sourceDirectoryPath, destinationDirectoryPath, true, true);


            // get the sub directories path of destination portal
            var portalSubDirectories = Directory.GetDirectories(destinationDirectoryPath);
            foreach (String subFolder in portalSubDirectories)
            {
                // getting subDirectorie's name inside destination folder, like 'course', 'user' etc.
                String objectName = new DirectoryInfo(subFolder).Name;

                // do not rename 'templates' folder
                if (!objectName.Equals("templates"))
                {
                    // get the path list of directories of "subFolder".
                    var objectSubDirectories = Directory.GetDirectories(subFolder);

                    foreach (String subFolderId in objectSubDirectories)
                    {
                        // folder name would be id of object
                        String oldObjectId = new DirectoryInfo(subFolderId).Name;
                        // getting the resultant row from clonePortalIdMappings datatable
                        DataRow[] resultRow = clonePortalIdMappings.Select("sourceID =" + Convert.ToInt32(oldObjectId) + " AND object = '" + objectName + "'");


                        if (resultRow.Length > 0)
                        {
                            // fetching the corresponding mapped id with the old source id
                            string newObjectId = resultRow[0]["destinationID"].ToString();

                            // replace "[sourceID]" folder name with "[destinationID]" and suffix "-temp" 
                            String pathFordestinationID = subFolderId.Replace("\\" + oldObjectId, "\\" + newObjectId + "-temp");

                            // rename "[sourceID]" folder to "[destinationID]-temp" folder.
                            Directory.Move(subFolderId, pathFordestinationID);
                        }
                    }
                }

            }

            #region remove the "-temp" suffux from each folder name

            var portalSubDirectoriesTemp = Directory.GetDirectories(destinationDirectoryPath);
            foreach (String subFolder in portalSubDirectoriesTemp)
            {
                String objectName = new DirectoryInfo(subFolder).Name;

                if (!objectName.Equals("templates"))
                {
                    // get the subdirectories of folder
                    var objectSubDirectories = Directory.GetDirectories(subFolder);
                    foreach (String subFolderId in objectSubDirectories)
                    {
                        // remove the -temp suffix from the destination.
                        String pathFordestinationID = subFolderId.Replace("-temp", "");
                        if (!subFolderId.Equals(pathFordestinationID))
                        {
                            Directory.Move(subFolderId, pathFordestinationID);
                        }
                        else
                        {
                            Directory.Delete(pathFordestinationID, true);
                        }
                    }
                }
            }

            #endregion

        }


        #endregion

        #region _ClonePartialPortalWarehouseObject

        /// <summary>
        /// Clone _config folder objects for destination portal for partial copy.
        /// </summary>
        /// <param name="clonePortalIdMappings">clone Portal IdMappings table</param>
        private void _ClonePartialPortalWarehouseObject(DataTable clonePortalIdMappings)
        {
            // format source portal path
            //String sourceDirectoryPath = Server.MapPath(SitePathConstants.WAREHOUSE + this._SiteSource.SelectedItem.Text);
            String sourceDirectoryPath = Config.ApplicationSettings.AsentiaContentSourceRootPath + @"\warehouse\" + this._SiteSource.SelectedItem.Text;

            //sourceDirectoryPath = sourceDirectoryPath.Replace("Asentia.CustomerManagerWebsite", "Asentia.Website");
            DirectoryInfo sourceDir = new DirectoryInfo(sourceDirectoryPath);

            // format destination portal path
            //String destinationDirectoryPath = Server.MapPath(SitePathConstants.WAREHOUSE + this._SiteObject.Hostname);
            String destinationDirectoryPath = Config.ApplicationSettings.AsentiaContentDestinationRootPath + @"\warehouse\" + this._SiteObject.Hostname;
            //destinationDirectoryPath = destinationDirectoryPath.Replace("Asentia.CustomerManagerWebsite", "Asentia.Website");

            // format destination warehouse path
            //String destinationWarehouseDoucmentRoot = Server.MapPath(SitePathConstants.WAREHOUSE + this._SiteObject.Hostname + "/documents");
            String destinationWarehouseDoucmentRoot = Config.ApplicationSettings.AsentiaContentDestinationRootPath + @"\warehouse\" + this._SiteObject.Hostname + "/documents";
            //destinationWarehouseDoucmentRoot = destinationWarehouseDoucmentRoot.Replace("Asentia.CustomerManagerWebsite", "Asentia.Website");
            DirectoryInfo sourceDirDocuments = new DirectoryInfo(destinationWarehouseDoucmentRoot);

            // check portal folder existence 
            if (!sourceDir.Exists)
            { throw new AsentiaException(String.Format(_GlobalResources.TheSourceDirectoryDoesNotExistOrCouldNotBeFound, sourceDirectoryPath)); }



            #region Rename content package start

            // format warehouse path
            //String contentPackagePath = Server.MapPath(SitePathConstants.WAREHOUSE);
            String contentPackagePath = Config.ApplicationSettings.AsentiaContentSourceRootPath + @"\warehouse";
            //contentPackagePath = contentPackagePath.Replace("Asentia.CustomerManagerWebsite", "Asentia.Website");

            // get the subdirectories of warehouse 
            var contentPackagesDirectories = Directory.GetDirectories(contentPackagePath);
            foreach (String contentPackageFolderPath in contentPackagesDirectories)
            {
                String contentPackageFolderName = new DirectoryInfo(contentPackageFolderPath).Name;

                // find the index of first dash and second dash to replace the content package name
                int indexOfFirstDash = contentPackageFolderName.IndexOf('-');
                int indexOfSecondDash = contentPackageFolderName.IndexOf('-', contentPackageFolderName.IndexOf('-') + 1);

                // get the new created site id from data table
                DataRow[] result = clonePortalIdMappings.Select("object = 'site'");
                if (result.Length > 0)
                {
                    // fetching the new idSite and old idSite.
                    int newSiteId = Convert.ToInt32(result[0]["destinationID"]);
                    int oldSiteId = Convert.ToInt32(result[0]["sourceID"]);

                    // concatinating IdAccount and idSite for replacing the content package name.
                    String replacementText = AsentiaSessionState.IdAccount + "-" + newSiteId;
                    String oldAccountAndSiteId = AsentiaSessionState.IdAccount + "-" + oldSiteId;

                    // check that only for content package folder 
                    if (indexOfSecondDash > 0)
                    {
                        // take the part of content package name that needs to be replaced
                        String idAccountAndidSiteObject = contentPackageFolderName.Substring(0, indexOfSecondDash).ToString();

                        if (oldAccountAndSiteId.Equals(idAccountAndidSiteObject))
                        {

                            String newContentPackageFolderName = contentPackageFolderName.Substring(0, 0) + replacementText + contentPackageFolderName.Substring(indexOfSecondDash);

                            // replace the accont id and site id within the content package folder name string
                            String newContentPackageFolderPath = contentPackageFolderPath.Replace("\\" + contentPackageFolderName, "\\" + newContentPackageFolderName);

                            // copy source portal's content packages into destination portal 
                            Utility.CopyDirectory(contentPackageFolderPath, newContentPackageFolderPath, true, true);

                        }
                    }
                }

            }
            #endregion


            // copy source hostname into destination hostname
            Utility.CopyDirectory(sourceDirectoryPath, destinationDirectoryPath, true, true);

            // check document folder existence inside warehouse 
            if (sourceDirDocuments.Exists)
            {
                // get the subdirectories of document folder.
                var portalSubDirectories = Directory.GetDirectories(destinationWarehouseDoucmentRoot);

                foreach (String subFolder in portalSubDirectories)
                {
                    String objectName = new DirectoryInfo(subFolder).Name;

                    // get the subdirectories of each folder
                    var objectSubDirectories = Directory.GetDirectories(subFolder);
                    foreach (String subFolderId in objectSubDirectories)
                    {
                        String oldObjectId = new DirectoryInfo(subFolderId).Name;

                        //fetch corresponding row for source id from mapping table
                        DataRow[] resultRow = clonePortalIdMappings.Select("sourceID =" + Convert.ToInt32(oldObjectId) + " AND object like '%" + objectName + "%'");

                        if (resultRow.Length > 0)
                        {
                            //fetch mapped new id in the row
                            string newObjectId = resultRow[0]["destinationID"].ToString();

                            // replace "[sourceID]" folder name by "[destinationID]-temp"
                            String pathFordestinationID = subFolderId.Replace("\\" + oldObjectId, "\\" + newObjectId + "-temp");

                            // rename sourceID folder to destinationID-temp folder.
                            Directory.Move(subFolderId, pathFordestinationID);
                        }

                    }
                }
            }


            #region remove the "-temp" suffux from each folder name

            var portalSubDirectoriesTemp = Directory.GetDirectories(destinationWarehouseDoucmentRoot);
            foreach (String subFolder in portalSubDirectoriesTemp)
            {
                String objectName = new DirectoryInfo(subFolder).Name;

                // get the subdirectories of folder
                var objectSubDirectories = Directory.GetDirectories(subFolder);
                foreach (String subFolderId in objectSubDirectories)
                {
                    // remove the -temp suffix from the destination path .
                    String pathFordestinationID = subFolderId.Replace("-temp", "");
                    if (!subFolderId.Equals(pathFordestinationID))
                    {
                        // replace "-temp" from directory name
                        Directory.Move(subFolderId, pathFordestinationID);
                    }
                    else
                    {

                        Directory.Delete(pathFordestinationID, true);
                    }
                }
            }
            #endregion

        }

        #endregion

        #region _BuildViewLicenseAgreementModal
        /// <summary>
        /// Builds the modal for viewing the license agreement
        /// </summary>
        private void _BuildViewLicenseAgreementModal()
        {
            // Create license agreement modal
            this._ViewLicenseAgreementModal = new ModalPopup("ViewLicenseAgreementModal");
            this._ViewLicenseAgreementModal.TargetControlID = this._ViewLicenseAgreementButton.ID;
            this._ViewLicenseAgreementModal.Type = ModalPopupType.Information;
            this._ViewLicenseAgreementModal.OverrideLoadImagesFromCustomerManager = true;
            this._ViewLicenseAgreementModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LICENSE,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this._ViewLicenseAgreementModal.HeaderIconAlt = _GlobalResources.LicenseAgreement;
            this._ViewLicenseAgreementModal.HeaderText = _GlobalResources.LicenseAgreement;
            this._ViewLicenseAgreementModal.ShowLoadingPlaceholder = true;
            this._ViewLicenseAgreementModal.ReloadPageOnClose = false;

            // License Agreement HTML
            HtmlGenericControl licenseAgreementHtml = new HtmlGenericControl();
            licenseAgreementHtml.ID = "LicenseAgreementHtml_Field";

            if (File.Exists(Config.ApplicationSettings.AsentiaContentSourceRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + this._SiteSource.SelectedItem.Text + "\\License.html"))
            { licenseAgreementHtml.InnerHtml = File.ReadAllText(Config.ApplicationSettings.AsentiaContentSourceRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + this._SiteSource.SelectedItem.Text + "\\License.html"); }

            this._ViewLicenseAgreementModal.AddControlToBody(CustomerManagerPage.BuildFormField("LicenseAgreementHtml", null, licenseAgreementHtml.ID, licenseAgreementHtml, false, false));

            this.SitePropertiesActionsPanel.Controls.Add(this._ViewLicenseAgreementModal);
        }
        #endregion

        #region _BuildLoadingModal
        /// <summary>
        /// Builds loading or saving information, with loader, modal popup.
        /// </summary>
        private void _BuildLoadingModal()
        {
            this._LoadingModal = new ModalPopup("LoadingModal");
            this._LoadingModal.OverrideLoadImagesFromCustomerManager = true;

            // set modal properties
            this._LoadingModal.Type = ModalPopupType.Information;
            this._LoadingModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CLONEPORTAL,
                                                                                          ImageFiles.EXT_PNG,
                                                                                          true
                                                                                          );

            this._LoadingModal.HeaderText = _GlobalResourcesCHETU1.ClonePortal;
            this._LoadingModal.TargetControlID = "SaveButton";
            this._LoadingModal.SubmitButton.Visible = false;
            this._LoadingModal.CloseButton.Visible = false;
            this._LoadingModal.ShowCloseIcon = false;
            this._LoadingModal.ShowLoadingPlaceholder = true;

            this.SitePropertiesActionsPanel.Controls.Add(this._LoadingModal);

        }
        #endregion



    }
}
