﻿//handles the kb limit checkbox actions
$(document).ready(function () {

    $("#KbLimit_Field").prop("disabled", $("#KbLimitNone").prop('checked'));
    $("#UserLimit_Field").prop("disabled", $("#UserLimitNone").prop('checked'));

    //Seelct and disbale  first checkbox item (en-US default) in available language list
    $("#AvailableLanguages_Field_0").attr("checked", "true")
    $("#AvailableLanguages_Field_0").attr("disabled", "true")

    $('#KbLimitNone').click(function () {
        $("#KbLimit_Field").val('');
        $("#KbLimit_Field").attr('disabled', this.checked);
    });

    $('#UserLimitNone').click(function () {
        $("#UserLimit_Field").attr('disabled', this.checked);
        $("#UserLimit_Field").val('');
    });


    //add and remove items to/from default language list accarding to available languages selection
    $("#AvailableLanguages_Field input[type=checkbox]").on('change', function () {
        var x = document.getElementById("DefaultLanguage_Field");
        //check if checkbox checked(selected)
        if (this.checked) {
            //checks if selected item already exists in dropdown list
            if (x.options.length > 0) {
                var ifAlreadyExists = false;
                for (var i = x.options.length - 1; i >= 0; i--) {
                    if (x.options[i].value == this.value) {
                        x.remove(i);
                        ifAlreadyExists = true;
                        return;
                    }
                }

                //If not exists,add item to dropdown list
                if (!ifAlreadyExists) {
                    var option = document.createElement("option");
                    option.text = $(this).next('label').text();
                    option.value = this.value;
                    x.add(option);
                }
            }
        }
            //if checkbox Unchecked(DeSelected), remove item from list
        else {
            if (x.options.length > 0) {
                for (var i = x.options.length - 1; i >= 0; i--) {
                    if (x.options[i].value == this.value) {
                        x.remove(i);
                        return false;
                    }
                }
            }
        }
    })


    $('#DefaultLanguage_Field').change(function () {
        // if changed to, for example, the last option, then

        // get whatever value you want into a variable
        var x = $(this).val();
        // and update the hidden input's value
        $('#DefaultLanguagehidden').val("");
        $('#DefaultLanguagehidden').val(x);
    });

});

//Handles domain alias delete process
function RemoveDomainAlias(idDomainAlias) {

    ////set domain alias text into hidden field
    $("#DomainAliaIdHidden").val(idDomainAlias);

    //click the hidden button to show modal popup
    document.getElementById("DeleteAliasButton").click();
}

//hide display feedback
function HideDisplayFeedBack() {
    $("#DomainAliasModalModalPopupFeedbackContainer").hide();
    $("#DomainAlias_ErrorContainer").hide();
    $("#DomainAlias_Field").val('');
}

// click hidden button to open modal popup
function hiddenModalClick() {

    // MAKE SURE ALL REQUIRED FIELDS ARE FILLED IN
    if ($('#Name_Field').val() == "" || $('#HostName_Field').val() == "" || $('#Password_Field').val() == "") {
        
        // fields are not filled in, so terminate clone
        $('#CloneStatusHiddenField').val("Please Ensure All Required Fields are Filled In");
        $('#HiddenModalButton').click();

    } else if ($('#Password_Field').text() != $('#PasswordConfirm_Field').text()) {
        
        // password field and confirm password field do not match, so terminate clone
        $('#CloneStatusHiddenField').val("Password Field and Confirm Password Field do not match.");
        $('#HiddenModalButton').click();

    } else {
        
        // fields are filled in, so continue with clone
        $('#CloneStatusHiddenField').val("Success");
        $(".LoadingPlaceholder").css("display", "block");

        $("#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper").text("");

        // call the cloning procedures
        clone();
    }
}

var idAccount;
var dbServer;
var dbName;

var idSiteSource;
var sourceHostname;

var destinationHostname;
var idSiteDestination;
var destinationTitle;
var destinationPassword;
var destinationCompany;
var destinationContactName;
var destinationContactEmail;
var destinationUserLimit;
var destinationKbLimit;
var destinationTimezone;
var destinationDefaultLanguage;
var destinationIsActive;
var destinationExpirationDate;
var destinationCopyLicenseAgreementToSite;
var destinationFeatureEcommerce;
var destinationFeatureCertifications;
var destinationFeatureQuizSurveyAuthoring;
var destinationFeatureGotoMeeting;
var destinationFeatureGoToWebinar;
var destinationFeatureGoToTraining;
var destinationFeatureWebEx;
var destinationFeatureOpenSesame;
var destinationAvailableLanguages = [];



// starts the cloning process
function clone() {

    idAccount = $('#IdAccountHiddenField').val();
    dbServer = $('#DatabaseServerHiddenField').val();
    dbName = $('#DatabaseNameHiddenField').val();

    sourceHostname = $('#SiteSource_Field option:selected').text();
    idSiteSource = $('#SiteSource_Field').val();
    destinationTitle = $('#Name_Field').val();
    destinationHostname = $('#HostName_Field').val();
    destinationPassword = $('#Password_Field').val();
    destinationCompany = $('#Company_Field').val();
    destinationContactName = $('#ContactName_Field').val();
    destinationContactEmail = $('#ContactEmail_Field').val();
    destinationUserLimit = $('#UserLimit_Field').val();
    destinationKbLimit = $('#KbLimit_Field').val();
    destinationTimezone = $('#TimeZone_Field').val();
    destinationDefaultLanguage = $('#DefaultLanguage_Field').val();
    destinationIsActive = $('#IsActive').val();
    if ($('#DtExpires_Field_DateInputControl').val() == "") {
        destinationExpirationDate = null;
    } else {
        destinationExpirationDate = $('#DtExpires_Field_DateInputControl').val();
    }
    destinationCopyLicenseAgreementToSite = $('#CopyLicenseAgreement_Field').is(':checked');
    destinationFeatureEcommerce = $('#Ecommerce_Field').is(':checked');
    destinationFeatureCertifications = $('#Certifications_Field').is(':checked');
    destinationFeatureQuizSurveyAuthoring = $('#QuizzesAndSurveys_Field').is(':checked');
    destinationFeatureGotoMeeting = $('#GoToMeeting_Field').is(':checked');
    destinationFeatureGoToWebinar = $('#GoToWebinar_Field').is(':checked');
    destinationFeatureGoToTraining = $('#GoToTraining_Field').is(':checked');
    destinationFeatureWebEx = $('#WebEx_Field').is(':checked');
    destinationFeatureOpenSesame = $('#OpenSesame_Field').is(':checked');

    // available languages
    $("#AvailableLanguages_Field input[type=checkbox]").each(function () {
        if (this.checked) {
            destinationAvailableLanguages.push(this.value);
        }

    });
    
    $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 1 of 11: Copying Site Data");
    savePartialCloneSiteProperties();
}

// STEP 0 - Save New Site Properties Pulled From Form
function savePartialCloneSiteProperties() {
    
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/SavePartialCloneSiteProperties",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idAccount: idAccount, title: destinationTitle, company: destinationCompany, contactName: destinationContactName, hostname: destinationHostname, contactEmail: destinationContactEmail, defaultLanguage: destinationDefaultLanguage, timezone: destinationTimezone, password: destinationPassword, userLimit: destinationUserLimit, kbLimit: destinationKbLimit, isActive: destinationIsActive, expirationDate: destinationExpirationDate, eCommerce: destinationFeatureEcommerce, certifications: destinationFeatureCertifications, quizzesAndSurveys: destinationFeatureQuizSurveyAuthoring, goToMeeting: destinationFeatureGotoMeeting, goToWebinar: destinationFeatureGoToWebinar, goToTraining: destinationFeatureGoToTraining, webEx: destinationFeatureWebEx, openSesame: destinationFeatureOpenSesame, availableLanguages: destinationAvailableLanguages }),
        success: function (data) {
            // if the save was a success, start the clone process
            if (data.d.includes("Success")) {
                idSiteDestination = data.d.split("|")[1];
                copySite();
            } else {
                $('#CloneStatusHiddenField').val(data.d);
                $('#HiddenModalButton').click();
            }
        },
        error: function (xhr, status, error) {
            
            // Display the error and terminate clone
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 1 - Copy Site Data
function copySite() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopySite",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: false, sourceDBServer: dbServer, sourceDBName: dbName, idSiteSource: idSiteSource, destinationDBServer: dbServer, destinationDBName: dbName, idSiteDestination: idSiteDestination, idAccount: idAccount, destinationHostname: destinationHostname }),
        success: function (data) {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Site Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 2 of 11: Copying Catalog and Course Data");
            copyCatalogCourse();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate clone
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 2 - Copy Catalog / Course Data
function copyCatalogCourse() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyCatalogCourse",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: dbServer, sourceDBName: dbName, idSiteSource: idSiteSource, destinationDBServer: dbServer, destinationDBName: dbName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Catalog and Course Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 3 of 11: Copying Group Data");
            copyGroup();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 3 - Copy Group Data
function copyGroup() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyGroup",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: dbServer, sourceDBName: dbName, idSiteSource: idSiteSource, destinationDBServer: dbServer, destinationDBName: dbName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Group Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 4 of 11: Copying Content And Resource Data");
            copyContentResource();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 5 - Copy Content / Resource Data
function copyContentResource() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyContentResource",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: false, sourceDBServer: dbServer, sourceDBName: dbName, idSiteSource: idSiteSource, destinationDBServer: dbServer, destinationDBName: dbName, idAccount: idAccount, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Content and Resource Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 5 of 11: Copying Instructor Led Training Data");
            copyILT();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 6 - Copy ILT Data
function copyILT() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyILT",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: false, sourceDBServer: dbServer, sourceDBName: dbName, idSiteSource: idSiteSource, destinationDBServer: dbServer, destinationDBName: dbName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Instructor Led Training Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 6 of 11: Copying Role and Rule Data");
            copyRoleRule();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}


// STEP 7 - Copy Role / Rule Data
function copyRoleRule() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyRoleRule",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: false, sourceDBServer: dbServer, sourceDBName: dbName, idSiteSource: idSiteSource, destinationDBServer: dbServer, destinationDBName: dbName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Role and Rule Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 7 of 11: Copying Coupon Code Data");
            copyCouponCode();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 8 - Copy Coupon Code Data
function copyCouponCode() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyCouponCode",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: dbServer, sourceDBName: dbName, idSiteSource: idSiteSource, destinationDBServer: dbServer, destinationDBName: dbName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Coupon Code Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 8 of 11: Copying Certificate and Certification Data");
            copyCertificateCertification();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 9 - Copy Certificate / Certification Data
function copyCertificateCertification() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyCertificateCertification",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: false, sourceDBServer: dbServer, sourceDBName: dbName, idSiteSource: idSiteSource, destinationDBServer: dbServer, destinationDBName: dbName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Certificate and Certification Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 9 of 11: Copying Document Repository and Event Email Data");
            copyDocumentRepositoryEventEmail();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 12 - Copy Document Repository / Event Email Data
function copyDocumentRepositoryEventEmail() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyDocumentRepositoryEventEmail",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: false, sourceDBServer: dbServer, sourceDBName: dbName, idSiteSource: idSiteSource, destinationDBServer: dbServer, destinationDBName: dbName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Document Repository and Event Email Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 10 of 11: Copying Config Files");
            copyConfig();
        },
        error: function (xhr, status, error) {
            
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}


// STEP 16 - Copy Config Items
function copyConfig() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyConfig",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ destinationPortalName: destinationHostname, sourcePortalName: sourceHostname, destinationDBServer: dbServer, destinationDBName: dbName, copyLicenseAgreementToSite: destinationCopyLicenseAgreementToSite }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Config Files Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 11 of 11: Copying Warehouse Files");
            copyWarehouse();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 11 - Copy Warehouse Items
function copyWarehouse() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyWarehouse",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ destinationPortalName: destinationHostname, sourcePortalName: sourceHostname, sourceIdAccount: idAccount, destinationIdAccount: idAccount, destinationDBServer: dbServer, destinationDBName: dbName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Warehouse Files Copy Complete");
            $('#HiddenModalButton').click();
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// deletes site if an error occurs
function deleteSite() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/DeleteSite",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSite: idSiteDestination, destinationSiteHostname: destinationHostname, idAccount: idAccount, databaseServer: dbServer, databaseName: dbName }),
        success: function () {
            // do nothing
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
        }
    });

}

function changeSourceSite() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/UpdateLicenseAgreement",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ hostname: $('#SiteSource_Field option:selected').text() }),
        success: function (data) {
            $('#LicenseAgreementHtml_Field').html(data.d);
        },
        error: function (xhr, status, error) {
            // do nothing
        }
    })
}

function toggleProfile(profileName) {
    if (profileName == 'standard') {
        $('#Ecommerce_Field').prop("checked", true);
        $('#QuizzesAndSurveys_Field').prop("checked", true);
        $('#Certifications_Field').prop("checked", false);
        $('#GoToMeeting_Field').prop("checked", true);
        $('#GoToWebinar_Field').prop("checked", true);
        $('#GoToTraining_Field').prop("checked", true);
        $('#WebEx_Field').prop("checked", true);
        $('#OpenSesame_Field').prop("checked", true);
        $('#xAPIEndpoints_Field').prop("checked", true);
        $('#UserAccountMerging_Field').prop("checked", true);
        $('#UserProfileFileAttachments_Field').prop("checked", true);
        $('#LiteUserFieldConfiguration_Field').prop("checked", false);
        $('#UserRegistrationApproval_Field').prop("checked", true);
        $('#ImageEditor_Field').prop("checked", true);
        $('#CSSEditor_Field').prop("checked", true);
        $('#SelfEnrollmentApproval_Field').prop("checked", true);
        $('#TaskModules_Field').prop("checked", true);
        $('#OJTModules_Field').prop("checked", true);
        $('#LearningPaths_Field').prop("checked", true);
        $('#StandaloneEnrollmentILT_Field').prop("checked", true);
        $('#ILTResources_Field').prop("checked", true);
        $('#ReportingPDFExport_Field').prop("checked", true);
        $('#MessageCenter_Field').prop("checked", true);
        $('#ObjectSpecificEmailNotifications_Field').prop("checked", true);
        $('#RolesRules_Field').prop("checked", true);
        $('#Communities_Field').prop("checked", true);
        $('#CourseDiscussionBoards_Field').prop("checked", true);
        $('#GroupDiscussionBoards_Field').prop("checked", true);
        $('#GroupDocuments_Field').prop("checked", true);
        $('#AdvancedRules_Field').prop("checked", false);
    } else {
        $('#Ecommerce_Field').prop("checked", true);
        $('#QuizzesAndSurveys_Field').prop("checked", true);
        $('#Certifications_Field').prop("checked", false);
        $('#GoToMeeting_Field').prop("checked", false);
        $('#GoToWebinar_Field').prop("checked", false);
        $('#GoToTraining_Field').prop("checked", false);
        $('#WebEx_Field').prop("checked", false);
        $('#OpenSesame_Field').prop("checked", true);
        $('#xAPIEndpoints_Field').prop("checked", false);
        $('#UserAccountMerging_Field').prop("checked", false);
        $('#UserProfileFileAttachments_Field').prop("checked", false);
        $('#LiteUserFieldConfiguration_Field').prop("checked", true);
        $('#UserRegistrationApproval_Field').prop("checked", false);
        $('#ImageEditor_Field').prop("checked", false);
        $('#CSSEditor_Field').prop("checked", false);
        $('#SelfEnrollmentApproval_Field').prop("checked", false);
        $('#TaskModules_Field').prop("checked", false);
        $('#OJTModules_Field').prop("checked", false);
        $('#LearningPaths_Field').prop("checked", false);
        $('#StandaloneEnrollmentILT_Field').prop("checked", false);
        $('#ILTResources_Field').prop("checked", false);
        $('#ReportingPDFExport_Field').prop("checked", false);
        $('#MessageCenter_Field').prop("checked", false);
        $('#ObjectSpecificEmailNotifications_Field').prop("checked", false);
        $('#RolesRules_Field').prop("checked", false);
        $('#Communities_Field').prop("checked", false);
        $('#CourseDiscussionBoards_Field').prop("checked", false);
        $('#GroupDiscussionBoards_Field').prop("checked", false);
        $('#GroupDocuments_Field').prop("checked", false);
        $('#AdvancedRules_Field').prop("checked", true);
    }
}