﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.CustomerManager.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.CustomerManager.Pages.Accounts.Sites
{
    public class Modify : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel SitePropertiesContainer;
        public Panel SitePropertiesInstructionsPanel;
        public Panel SitePropertiesTabPanelsContainer;
        public Panel SitePropertiesActionsPanel;
        public Panel SitePropertiesFeedbackContainer;
        public Panel passworrdContainerPannel;

        #endregion

        #region Private Properties
        private AsentiaSite _SiteObject;
        private TextBox _Title;
        private TextBox _HostName;
        private TextBox _Company;
        private TextBox _ContactName;
        private TextBox _ContactEmail;
        private TextBox _UserLimit;
        private TextBox _KbLimit;
        private TextBox _Password;
        private TextBox _ConfirmPassword;
        private TextBox _DomainAlias;

        private CheckBox _kbLimitNone;
        private CheckBox _UserLimitNone;
        private DropDownList _Status;
        private DatePicker _DtExpires;
        private TimeZoneSelector _Timezone;
        private CheckBox _CopyLicenseAgreementCheckBox;
        private Button _ViewLicenseAgreementButton;

        private Button _SaveButton;
        private Button _CancelButton;
        private LinkButton _DeleteAliasButton;

        private CheckBoxList _AvailableLanguages;
        private DropDownList _DefaultLanguage;
        private Dictionary<string, string> _AvailableLanguageList;

        private HiddenField _DefaultLanguageHidden;
        private HiddenField _DomainAliaIdHidden;
        private DataTable _SiteDomainAliases;
        private Panel _SiteDomainAliasesListContainer;
        private Panel _ModalPropertiesContainer;
        private ModalPopup _DomainAliasModal;
        private ModalPopup _DeleteDomainAliasModal;
        private UpdatePanel _DomainAliasUpdatePanel;
        private ModalPopup _ViewLicenseAgreementModal;

        // PROFILE BUTTONS
        private Panel _ProfileButtonContainer;
        private Button _StandardProfileButton;
        private Button _LiteProfileButton;

        // FEATURES
        private CheckBox _ECommerce;
        private CheckBox _Certifications;
        private CheckBox _QuizzesAndSurveys;
        private CheckBox _GoToMeeting;
        private CheckBox _GoToWebinar;
        private CheckBox _GoToTraining;
        private CheckBox _WebEx;
        private CheckBox _OpenSesame;
        private CheckBox _xAPIEndpoints;
        private CheckBox _UserAccountMerging;
        private CheckBox _UserProfileFileAttachments;
        private CheckBox _LiteUserFieldConfiguration;
        private CheckBox _UserRegistrationApproval;
        private CheckBox _ImageEditor;
        private CheckBox _CSSEditor;
        private CheckBox _SelfEnrollmentApproval;
        private CheckBox _TaskModules;
        private CheckBox _OJTModules;
        private CheckBox _LearningPaths;
        private CheckBox _ILTResources;
        private CheckBox _ReportingPDFExport;
        private CheckBox _MessageCenter;
        private CheckBox _ObjectSpecificEmailNotifications;
        private CheckBox _RolesRules;
        private CheckBox _Communities;
        private CheckBox _CourseDiscussionBoards;
        private CheckBox _GroupDocuments;
        private CheckBox _GroupDiscussionBoards;
        private CheckBox _StandaloneEnrollmentILT;
        private CheckBox _RestrictedRuleCriteria;

        private RadioButtonList _DashboardMode;

        private const string _RegexPatternForEmailId = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        private const string _RegexPatternForFullyQualifiedDomainName = @"([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?";
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            base.OnPreRender(e);

            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.CustomerManager.Pages.Accounts.Sites.Modify.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {

            // get the site object
            this._GetSiteObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();

            // only build the view license agreement modal if a License.html file exists for an account
            if (File.Exists(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + @"\License.html")))
            {
                // build view license agreement modal
                this._BuildViewLicenseAgreementModal();
            }

            if (!Page.IsPostBack)
            { }
        }
        #endregion

        #region _GetSiteObject
        /// <summary>
        /// Gets a site object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetSiteObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._SiteObject = new AsentiaSite(id, 1, id); }
                }
                catch
                { Response.Redirect("~/Sites"); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // clear controls from wrapper container
            this.SitePropertiesContainer.Controls.Clear();

            // build the site properties form tabs
            this._BuildSitePropertiesFormTabs();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.SitePropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisSite, true);

            this.SitePropertiesTabPanelsContainer = new Panel();
            this.SitePropertiesTabPanelsContainer.ID = "SiteProperties_TabPanelsContainer";
            this.SitePropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.SitePropertiesContainer.Controls.Add(this.SitePropertiesTabPanelsContainer);

            // build the site properties form
            this._BuildPropertiesForm();

            this._BuildPropertiesActionsPanel();

            this._BuildDeleteAliasControls();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string siteImagePath;
            string pageTitle;

            if (this._SiteObject != null)
            {
                string siteTitleInInterfaceLanguage = this._SiteObject.Title;

                breadCrumbPageTitle = siteTitleInInterfaceLanguage;

                pageTitle = siteTitleInInterfaceLanguage;

                siteImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                         ImageFiles.EXT_PNG, true);
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewSite;

                siteImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                         ImageFiles.EXT_PNG, true);

                pageTitle = _GlobalResources.NewSite;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();

            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Sites, "/Accounts/sites"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));
        }
        #endregion

        #region _BuildSitePropertiesFormTabs
        /// <summary>
        /// Method to build the site property form tabs
        /// </summary>
        private void _BuildSitePropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));

            if (this._SiteObject != null)
            { tabs.Enqueue(new KeyValuePair<string, string>("DomainAliases", _GlobalResources.DomainAliases)); }

            // build and attach the tabs
            this.SitePropertiesContainer.Controls.Add(CustomerManagerPage.BuildTabListPanel("SiteProperties", tabs));
        }
        #endregion

        #region _BuildPropertiesForm
        /// <summary>
        /// Builds the properties form.
        /// </summary>
        private void _BuildPropertiesForm()
        {
            this._BuildSitePropertiesFormPanel();

            this._BuildSitePropertiesFormDomainAliasesPanel();

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _BuildSitePropertiesFormPanel
        /// <summary>
        /// Builds the site properties form fields under properties tab.
        /// </summary>
        private void _BuildSitePropertiesFormPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "SiteProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.Attributes.Add("style", "display: block;");

            #region Title field container
            //title
            this._Title = new TextBox();
            this._Title.ID = "Name_Field";
            this._Title.CssClass = "InputLong";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("Name",
                                                             _GlobalResources.Title,
                                                             this._Title.ID,
                                                             this._Title,
                                                             true,
                                                             true));
            #endregion

            #region Host Name field container
            //Host Name
            this._HostName = new TextBox();
            this._HostName.ID = "HostName_Field";
            this._HostName.CssClass = "InputShort";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("HostName",
                                                             _GlobalResources.Hostname,
                                                             this._HostName.ID,
                                                             this._HostName,
                                                             true,
                                                             true));
            #endregion

            #region Password field

            // password and confirm password input control's list 
            List<Control> PasswordInputControlList = new List<Control>();

            // password input control
            this._Password = new TextBox();
            this._Password.ID = "Password" + "_Field";
            this._Password.TextMode = TextBoxMode.Password;
            this._Password.CssClass = "Password" + "_Field";
            PasswordInputControlList.Add(this._Password);

            // confirm password label control's container panel
            Panel confirmPasswordLabelContainer = new Panel();
            confirmPasswordLabelContainer.ID = "ConfirmPasswordField_Container";
            confirmPasswordLabelContainer.CssClass = "FormFieldDescriptionContainer";

            // confirm password label control
            Literal confirmPasswordLabel = new Literal();
            confirmPasswordLabel.Text = _GlobalResources.ConfirmByEnteringAgain;
            confirmPasswordLabelContainer.Controls.Add(confirmPasswordLabel);

            PasswordInputControlList.Add(confirmPasswordLabelContainer);

            // confirm password input control
            this._ConfirmPassword = new TextBox();
            this._ConfirmPassword.ID = "Password" + "Confirm_Field";
            this._ConfirmPassword.TextMode = TextBoxMode.Password;
            this._ConfirmPassword.CssClass = "password_Field";
            PasswordInputControlList.Add(this._ConfirmPassword);


            // if creating the new site then show the password as required field, otherwise don't
            if (this._SiteObject == null)
            {
                // add all password and confirm password controls to main panel
                propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Password",
                                                                                         _GlobalResources.Password,
                                                                                         PasswordInputControlList,
                                                                                         true,
                                                                                         true,
                                                                                         true));
            }
            else
            {
                // add all password and confirm password controls to main panel
                propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Password",
                                                                                         _GlobalResources.Password,
                                                                                         PasswordInputControlList,
                                                                                         false,
                                                                                         true,
                                                                                         true));
            }
            #endregion

            #region Company field container
            //Company
            this._Company = new TextBox();
            this._Company.ID = "Company_Field";
            this._Company.CssClass = "InputLong";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("Company",
                                                             _GlobalResources.Company,
                                                             this._Company.ID,
                                                             this._Company,
                                                             false,
                                                             false
                                                             ));
            #endregion

            #region Contact Name field container
            //Contact Name
            this._ContactName = new TextBox();
            this._ContactName.ID = "ContactName_Field";
            this._ContactName.CssClass = "InputLong";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("ContactName",
                                                             _GlobalResources.ContactName,
                                                             this._ContactName.ID,
                                                             this._ContactName,
                                                             false,
                                                             false));
            #endregion

            #region Contact Email container
            //Contact Email
            this._ContactEmail = new TextBox();
            this._ContactEmail.ID = "ContactEmail_Field";
            this._ContactEmail.CssClass = "InputLong";

            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("ContactEmail",
                                                             _GlobalResources.ContactEmail,
                                                             this._ContactEmail.ID,
                                                             this._ContactEmail,
                                                             false,
                                                             true
                                                             ));
            #endregion

            #region User Limit field container
            //User Limit

            List<Control> userLimitnputControls = new List<Control>();

            this._UserLimit = new TextBox();
            this._UserLimit.ID = "UserLimit_Field";
            this._UserLimit.CssClass = "InputXShort";
            this._UserLimit.MaxLength = 10;


            this._UserLimitNone = new CheckBox();
            this._UserLimitNone.ID = "UserLimitNone";
            this._UserLimitNone.Checked = true;
            this._UserLimitNone.Text = _GlobalResources.Unlimited;

            userLimitnputControls.Add(this._UserLimit);
            userLimitnputControls.Add(this._UserLimitNone);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("UserLimit",
                                                                                      _GlobalResources.UserLimit,
                                                                                   userLimitnputControls,
                                                                                   false,
                                                                                   true));
            #endregion

            #region KB Limit field container
            //KB Limit

            List<Control> kbLimitnputControls = new List<Control>();

            this._KbLimit = new TextBox();
            this._KbLimit.ID = "KbLimit_Field";
            this._KbLimit.CssClass = "InputXShort";
            this._KbLimit.MaxLength = 10;

            this._kbLimitNone = new CheckBox();
            this._kbLimitNone.ID = "KbLimitNone";
            this._kbLimitNone.Text = _GlobalResources.Unlimited;
            this._kbLimitNone.Checked = true;
            kbLimitnputControls.Add(this._KbLimit);
            kbLimitnputControls.Add(this._kbLimitNone);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("KbLimit",
                                                                                     _GlobalResources.KBLimit + "(MB)",
                                                                                    kbLimitnputControls,
                                                                                    false,
                                                                                    true));
            #endregion

            #region Timezone field container
            //Timezone
            this._Timezone = new TimeZoneSelector();
            this._Timezone.ID = "TimeZone_Field";
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("TimeZone",
                                                             _GlobalResources.Timezone,
                                                             this._Timezone.ID,
                                                             this._Timezone,
                                                             false,
                                                             false
                                                             ));

            string timeZoneName = Timezone.GetTimezoneNameFromDotnetName(Config.AccountSettings.DefaultTimezoneDotNetName);
            if (!String.IsNullOrWhiteSpace(timeZoneName) && this._Timezone.Items.FindByText(timeZoneName) != null)
            {
                this._Timezone.Items.FindByText(timeZoneName).Selected = true;
            }

            #endregion

            #region Default Language container
            //Default Language

            this._DefaultLanguage = new DropDownList();
            this._DefaultLanguage.ID = "DefaultLanguage_Field";
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("DefaultLanguage",
                                                             _GlobalResources.DefaultLanguage,
                                                             this._DefaultLanguage.ID,
                                                             this._DefaultLanguage,
                                                             false,
                                                             false));

            this._DefaultLanguageHidden = new HiddenField();
            this._DefaultLanguageHidden.ID = "DefaultLanguagehidden";
            propertiesPanel.Controls.Add(_DefaultLanguageHidden);
            this._DefaultLanguage.Items.Clear();
            this._DefaultLanguage.Items.Add(new ListItem { Text = this.GetLanguageNameByCode(Config.AccountSettings.DefaultLanguage), Value = Config.AccountSettings.DefaultLanguage });

            #endregion

            #region Available Languages container
            //Available Languages

            this._AvailableLanguages = new CheckBoxList();
            this._AvailableLanguages.ID = "AvailableLanguages_Field";
            this._AvailableLanguages.RepeatDirection = RepeatDirection.Vertical;
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("AvailableLanguages",
                                                             _GlobalResources.AvailableLanguages,
                                                             this._AvailableLanguages.ID,
                                                             this._AvailableLanguages,
                                                             false,
                                                             false));


            this._AvailableLanguageList = this.GetAvailableInstalledLanguages();

            this._AvailableLanguages.DataSource = _AvailableLanguageList;
            this._AvailableLanguages.DataTextField = "Value";
            this._AvailableLanguages.DataValueField = "key";
            this._AvailableLanguages.DataBind();
            #endregion

            #region Status field container
            //Status
            this._Status = new DropDownList();
            this._Status.ID = "IsActive";

            this._Status.Items.Add(new ListItem(_GlobalResources.Enabled, "True"));
            this._Status.Items.Add(new ListItem(_GlobalResources.Disabled, "False"));

            propertiesPanel.Controls.Add(BuildFormField("IsActive",
                                                        _GlobalResources.Status,
                                                        _Status.ID,
                                                        _Status,
                                                        false,
                                                        false));
            this._Status.SelectedIndex = 0;
            #endregion

            #region DtExpires field container

            //DtExpires
            this._DtExpires = new DatePicker("DtExpires_Field", true, false, false, true);
            this._DtExpires.NoneCheckboxText = _GlobalResources.NeverExpires;
            this._DtExpires.NoneCheckBoxChecked = true;
            propertiesPanel.Controls.Add(CustomerManagerPage.BuildFormField("DtExpires",
                                                             _GlobalResources.ExpirationDate,
                                                             this._DtExpires.ID,
                                                             this._DtExpires,
                                                             false,
                                                             false));
            #endregion

            #region License Agreement Container

            // only show license agreement controls if a License.html exist for the account
            if (File.Exists(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + @"\License.html")))
            {
                List<Control> licenseAgreementControls = new List<Control>();

                // Copy License Agreement
                this._CopyLicenseAgreementCheckBox = new CheckBox();
                this._CopyLicenseAgreementCheckBox.ID = "CopyLicenseAgreement_Field";
                licenseAgreementControls.Add(this._CopyLicenseAgreementCheckBox);

                // View License Agreement
                this._ViewLicenseAgreementButton = new Button();
                this._ViewLicenseAgreementButton.ID = "ViewLicenseAgreement_Field";
                this._ViewLicenseAgreementButton.CssClass = "Button ActionButton";
                this._ViewLicenseAgreementButton.Text = _GlobalResources.ViewLicenseAgreement;
                licenseAgreementControls.Add(this._ViewLicenseAgreementButton);

                propertiesPanel.Controls.Add(CustomerManagerPage.BuildMultipleInputControlFormField("LicenseAgreement_Fields", _GlobalResources.CopyLicenseAgreementToSite, licenseAgreementControls, false, false));
            }
            #endregion

            #region Profile Buttons
            List<Control> profileButtons = new List<Control>();

            this._StandardProfileButton = new Button();
            this._StandardProfileButton.ID = "StandardProfileButton";
            this._StandardProfileButton.CssClass = "Button ActionButton";
            this._StandardProfileButton.Text = _GlobalResources.Standard;
            this._StandardProfileButton.OnClientClick = "toggleProfile('standard'); return false;";
            profileButtons.Add(this._StandardProfileButton);

            this._LiteProfileButton = new Button();
            this._LiteProfileButton.ID = "LiteProfileButton";
            this._LiteProfileButton.CssClass = "Button ActionButton";
            this._LiteProfileButton.Text = _GlobalResources.Lite;
            this._LiteProfileButton.OnClientClick = "toggleProfile('lite'); return false;";
            profileButtons.Add(this._LiteProfileButton);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Profile",
                                                                              _GlobalResources.Profile,
                                                                              profileButtons,
                                                                              false,
                                                                              false,
                                                                              false));
            #endregion

            #region Features field container

            // features field
            List<Control> featuresInputControls = new List<Control>();

            // e-commerce
            this._ECommerce = new CheckBox();
            this._ECommerce.ID = "Ecommerce_Field";
            this._ECommerce.ClientIDMode = ClientIDMode.Static;
            this._ECommerce.Text = _GlobalResources.ECommerce;
            featuresInputControls.Add(this._ECommerce);

            // quizezs and surveys
            this._QuizzesAndSurveys = new CheckBox();
            this._QuizzesAndSurveys.ID = "QuizzesAndSurveys_Field";
            this._QuizzesAndSurveys.ClientIDMode = ClientIDMode.Static;
            this._QuizzesAndSurveys.Text = _GlobalResourcesCHETU1.QuizzesAndSurveysAuthoring;
            featuresInputControls.Add(this._QuizzesAndSurveys);

            // certifications
            this._Certifications = new CheckBox();
            this._Certifications.ID = "Certifications_Field";
            this._Certifications.ClientIDMode = ClientIDMode.Static;
            this._Certifications.Text = _GlobalResources.Certifications;
            featuresInputControls.Add(this._Certifications);

            // GoToMeeting
            this._GoToMeeting = new CheckBox();
            this._GoToMeeting.ID = "GoToMeeting_Field";
            this._GoToMeeting.ClientIDMode = ClientIDMode.Static;
            this._GoToMeeting.Text = _GlobalResources.GoToMeeting;
            featuresInputControls.Add(this._GoToMeeting);

            // GoToWebinar
            this._GoToWebinar = new CheckBox();
            this._GoToWebinar.ID = "GoToWebinar_Field";
            this._GoToWebinar.ClientIDMode = ClientIDMode.Static;
            this._GoToWebinar.Text = _GlobalResources.GoToWebinar;
            featuresInputControls.Add(this._GoToWebinar);

            // GoToTraining
            this._GoToTraining = new CheckBox();
            this._GoToTraining.ID = "GoToTraining_Field";
            this._GoToTraining.ClientIDMode = ClientIDMode.Static;
            this._GoToTraining.Text = _GlobalResources.GoToTraining;
            featuresInputControls.Add(this._GoToTraining);

            // WebEx
            this._WebEx = new CheckBox();
            this._WebEx.ID = "WebEx_Field";
            this._WebEx.ClientIDMode = ClientIDMode.Static;
            this._WebEx.Text = _GlobalResources.WebEx;
            featuresInputControls.Add(this._WebEx);

            // OpenSesame
            this._OpenSesame = new CheckBox();
            this._OpenSesame.ID = "OpenSesame_Field";
            this._OpenSesame.ClientIDMode = ClientIDMode.Static;
            this._OpenSesame.Text = _GlobalResources.OpenSesame;
            featuresInputControls.Add(this._OpenSesame);

            // xAPI Endpoints
            this._xAPIEndpoints = new CheckBox();
            this._xAPIEndpoints.ID = "xAPIEndpoints_Field";
            this._xAPIEndpoints.ClientIDMode = ClientIDMode.Static;
            this._xAPIEndpoints.Text = _GlobalResources.xAPIEndpoints;
            featuresInputControls.Add(this._xAPIEndpoints);

            // User Account Merging
            this._UserAccountMerging = new CheckBox();
            this._UserAccountMerging.ID = "UserAccountMerging_Field";
            this._UserAccountMerging.ClientIDMode = ClientIDMode.Static;
            this._UserAccountMerging.Text = _GlobalResources.MergeUserAccounts;
            featuresInputControls.Add(this._UserAccountMerging);

            // User Profile File Attachments
            this._UserProfileFileAttachments = new CheckBox();
            this._UserProfileFileAttachments.ID = "UserProfileFileAttachments_Field";
            this._UserProfileFileAttachments.ClientIDMode = ClientIDMode.Static;
            this._UserProfileFileAttachments.Text = _GlobalResources.UserProfileFileAttachments;
            featuresInputControls.Add(this._UserProfileFileAttachments);

            // Lite User Field Configuration
            this._LiteUserFieldConfiguration = new CheckBox();
            this._LiteUserFieldConfiguration.ID = "LiteUserFieldConfiguration_Field";
            this._LiteUserFieldConfiguration.ClientIDMode = ClientIDMode.Static;
            this._LiteUserFieldConfiguration.Text = _GlobalResources.ReduceTheNumberOfUserDefinedFieldsTo10;
            featuresInputControls.Add(this._LiteUserFieldConfiguration);

            // User Registration Approval
            this._UserRegistrationApproval = new CheckBox();
            this._UserRegistrationApproval.ID = "UserRegistrationApproval_Field";
            this._UserRegistrationApproval.ClientIDMode = ClientIDMode.Static;
            this._UserRegistrationApproval.Text = _GlobalResources.UserRegistrationApproval;
            featuresInputControls.Add(this._UserRegistrationApproval);

            // Image Editor
            this._ImageEditor = new CheckBox();
            this._ImageEditor.ID = "ImageEditor_Field";
            this._ImageEditor.ClientIDMode = ClientIDMode.Static;
            this._ImageEditor.Text = _GlobalResources.ImageEditor;
            featuresInputControls.Add(this._ImageEditor);

            // CSS Editor
            this._CSSEditor = new CheckBox();
            this._CSSEditor.ID = "CSSEditor_Field";
            this._CSSEditor.ClientIDMode = ClientIDMode.Static;
            this._CSSEditor.Text = _GlobalResources.CSSEditor;
            featuresInputControls.Add(this._CSSEditor);

            // Self-Enrollment Approval
            this._SelfEnrollmentApproval = new CheckBox();
            this._SelfEnrollmentApproval.ID = "SelfEnrollmentApproval_Field";
            this._SelfEnrollmentApproval.ClientIDMode = ClientIDMode.Static;
            this._SelfEnrollmentApproval.Text = _GlobalResources.SelfEnrollmentApproval;
            featuresInputControls.Add(this._SelfEnrollmentApproval);

            // Task Modules
            this._TaskModules = new CheckBox();
            this._TaskModules.ID = "TaskModules_Field";
            this._TaskModules.ClientIDMode = ClientIDMode.Static;
            this._TaskModules.Text = _GlobalResources.TaskModules;
            featuresInputControls.Add(this._TaskModules);

            // OJT Modules
            this._OJTModules = new CheckBox();
            this._OJTModules.ID = "OJTModules_Field";
            this._OJTModules.ClientIDMode = ClientIDMode.Static;
            this._OJTModules.Text = _GlobalResources.OJTModules;
            featuresInputControls.Add(this._OJTModules);

            // Learning Paths
            this._LearningPaths = new CheckBox();
            this._LearningPaths.ID = "LearningPaths_Field";
            this._LearningPaths.ClientIDMode = ClientIDMode.Static;
            this._LearningPaths.Text = _GlobalResources.LearningPaths;
            featuresInputControls.Add(this._LearningPaths);

            // _Standalone Enrollment of ILT
            this._StandaloneEnrollmentILT = new CheckBox();
            this._StandaloneEnrollmentILT.ID = "StandaloneEnrollmentILT_Field";
            this._StandaloneEnrollmentILT.ClientIDMode = ClientIDMode.Static;
            this._StandaloneEnrollmentILT.Text = _GlobalResources.StandaloneEnrollmentOfILT;
            featuresInputControls.Add(this._StandaloneEnrollmentILT);


            // ILT Resources
            this._ILTResources = new CheckBox();
            this._ILTResources.ID = "ILTResources_Field";
            this._ILTResources.ClientIDMode = ClientIDMode.Static;
            this._ILTResources.Text = _GlobalResources.ILTResources;
            featuresInputControls.Add(this._ILTResources);

            // Reporting PDF Export
            this._ReportingPDFExport = new CheckBox();
            this._ReportingPDFExport.ID = "ReportingPDFExport_Field";
            this._ReportingPDFExport.ClientIDMode = ClientIDMode.Static;
            this._ReportingPDFExport.Text = _GlobalResources.ReportingPDFExport;
            featuresInputControls.Add(this._ReportingPDFExport);

            // Message Center
            this._MessageCenter = new CheckBox();
            this._MessageCenter.ID = "MessageCenter_Field";
            this._MessageCenter.ClientIDMode = ClientIDMode.Static;
            this._MessageCenter.Text = _GlobalResources.MessageCenter;
            featuresInputControls.Add(this._MessageCenter);

            // Object Specific Email Notifications
            this._ObjectSpecificEmailNotifications = new CheckBox();
            this._ObjectSpecificEmailNotifications.ID = "ObjectSpecificEmailNotifications_Field";
            this._ObjectSpecificEmailNotifications.ClientIDMode = ClientIDMode.Static;
            this._ObjectSpecificEmailNotifications.Text = _GlobalResources.ObjectSpecificEmailNotifications;
            featuresInputControls.Add(this._ObjectSpecificEmailNotifications);

            // Roles Rules
            this._RolesRules = new CheckBox();
            this._RolesRules.ID = "RolesRules_Field";
            this._RolesRules.ClientIDMode = ClientIDMode.Static;
            this._RolesRules.Text = _GlobalResources.RolesRulesets;
            featuresInputControls.Add(this._RolesRules);

            // Communities
            this._Communities = new CheckBox();
            this._Communities.ID = "Communities_Field";
            this._Communities.ClientIDMode = ClientIDMode.Static;
            this._Communities.Text = _GlobalResources.Communities;
            featuresInputControls.Add(this._Communities);

            // Course Discussion Boards
            this._CourseDiscussionBoards = new CheckBox();
            this._CourseDiscussionBoards.ID = "CourseDiscussionBoards_Field";
            this._CourseDiscussionBoards.ClientIDMode = ClientIDMode.Static;
            this._CourseDiscussionBoards.Text = _GlobalResources.CourseDiscussionBoards;
            featuresInputControls.Add(this._CourseDiscussionBoards);

            // Group Discussion Boards
            this._GroupDiscussionBoards = new CheckBox();
            this._GroupDiscussionBoards.ID = "GroupDiscussionBoards_Field";
            this._GroupDiscussionBoards.ClientIDMode = ClientIDMode.Static;
            this._GroupDiscussionBoards.Text = _GlobalResources.GroupDiscussionBoards;
            featuresInputControls.Add(this._GroupDiscussionBoards);

            // Group Documents
            this._GroupDocuments = new CheckBox();
            this._GroupDocuments.ID = "GroupDocuments_Field";
            this._GroupDocuments.ClientIDMode = ClientIDMode.Static;
            this._GroupDocuments.Text = _GlobalResources.GroupDocuments;
            featuresInputControls.Add(this._GroupDocuments);

            // Advanced Rules
            this._RestrictedRuleCriteria = new CheckBox();
            this._RestrictedRuleCriteria.ID = "AdvancedRules_Field";
            this._RestrictedRuleCriteria.ClientIDMode = ClientIDMode.Static;
            this._RestrictedRuleCriteria.Text = _GlobalResources.LimitRulesEngineForLearningAssetsToMemberOfGroup;
            featuresInputControls.Add(this._RestrictedRuleCriteria);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Features",
                                                                              _GlobalResourcesCHETU1.Features,
                                                                              featuresInputControls,
                                                                              false,
                                                                              true,
                                                                              true));

            // dashboard mode
            this._DashboardMode = new RadioButtonList();
            this._DashboardMode.ID = "DashboardMode_Field";
            this._DashboardMode.ClientIDMode = ClientIDMode.Static;
            this._DashboardMode.RepeatDirection = RepeatDirection.Vertical;
            this._DashboardMode.Items.Add(new ListItem(_GlobalResources.Standard, "Standard"));
            this._DashboardMode.Items.Add(new ListItem(_GlobalResources.Restricted, "Restricted"));
            featuresInputControls.Add(this._DashboardMode);

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("DashboardMode",
                                                                    _GlobalResources.DashboardMode,
                                                                    this._DashboardMode.ID,
                                                                    this._DashboardMode,
                                                                    false,
                                                                    false,
                                                                    false));
            #endregion

            // attach panel to container
            this.SitePropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildSitePropertiesFormDomainAliasesPanel
        /// <summary>
        /// Builds the site properties form fields under domain aliases tab.
        /// </summary>
        private void _BuildSitePropertiesFormDomainAliasesPanel()
        {
            // populate datatables with lists of users who are course experts and who are not
            if (this._SiteObject != null)
            {
                this._SiteDomainAliases = Library.Site.GetDomains(this._SiteObject.Id, this._SiteObject.Hostname);
            }

            Panel domainAliasesPanel = new Panel();
            domainAliasesPanel.ID = "SiteProperties_" + "DomainAliases" + "_TabPanel";
            domainAliasesPanel.CssClass = "TabPanelContainer";
            domainAliasesPanel.Attributes.Add("style", "display: none;");

            this._DomainAliasUpdatePanel = new UpdatePanel();
            this._DomainAliasUpdatePanel.ID = "DomainAliasUpdatePanel";
            this._DomainAliasUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;


            // build a container for the domain aliases listing
            this._SiteDomainAliasesListContainer = new Panel();
            this._SiteDomainAliasesListContainer.ID = "SiteDomainAliasesList";
            this._SiteDomainAliasesListContainer.CssClass = "ItemListingContainer";

            this._DomainAliasUpdatePanel.ContentTemplateContainer.Controls.Add(this._SiteDomainAliasesListContainer);

            domainAliasesPanel.Controls.Add(CustomerManagerPage.BuildFormField("SiteDomainAliasesList",
                                                         _GlobalResources.DomainAliases,
                                                        this._DomainAliasUpdatePanel.ID,
                                                        this._DomainAliasUpdatePanel,
                                                        false,
                                                        false));

            // domain alias  field
            List<Control> siteDomainAliasesInputControls = new List<Control>();

            Panel addDomainAliasButtonsPanel = new Panel();
            addDomainAliasButtonsPanel.ID = "AddDomainAlias_ButtonsPanel";

            // Add domain alias button
            // link
            Image addDomainAliasImageForLink = new Image();
            addDomainAliasImageForLink.ID = "LaunchAddDomainAliasModalImage";
            addDomainAliasImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD,
                                                                              ImageFiles.EXT_PNG, true);
            addDomainAliasImageForLink.CssClass = "MediumIcon";

            Localize selectAddDomainAliasTextForLink = new Localize();
            selectAddDomainAliasTextForLink.Text = _GlobalResources.AddDomainAlias;

            LinkButton addDomainAliasModalButton = new LinkButton();
            addDomainAliasModalButton.ID = "LaunchAddDomainAliasModalButton";
            addDomainAliasModalButton.CssClass = "ImageLink";
            addDomainAliasModalButton.Controls.Add(addDomainAliasImageForLink);
            addDomainAliasModalButton.Controls.Add(selectAddDomainAliasTextForLink);
            addDomainAliasModalButton.Attributes.Add("onClick", "javascript:HideDisplayFeedBack();");
            addDomainAliasButtonsPanel.Controls.Add(addDomainAliasModalButton);

            // attach the buttons panel to the container
            siteDomainAliasesInputControls.Add(addDomainAliasButtonsPanel);

            // build modals for adding and removing prerequisites to/from the course
            this._CreateDomainAliasModal(addDomainAliasModalButton.ID);

            domainAliasesPanel.Controls.Add(CustomerManagerPage.BuildMultipleInputControlFormField("DomainAlias",
                                                                                    String.Empty,
                                                                                    siteDomainAliasesInputControls,
                                                                                    false,
                                                                                    true));
            // attach panel to container
            this.SitePropertiesTabPanelsContainer.Controls.Add(domainAliasesPanel);
        }
        #endregion

        #region _CreateDomainAliasModal
        /// <summary>
        /// Builds the modal for adding domain alias.
        /// </summary>
        private void _CreateDomainAliasModal(string targetControlId)
        {
            // set modal properties
            this._DomainAliasModal = new ModalPopup("DomainAliasModal");
            this._DomainAliasModal.OverrideLoadImagesFromCustomerManager = true;

            this._DomainAliasModal.Type = ModalPopupType.Form;
            this._DomainAliasModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                                                       ImageFiles.EXT_PNG, true);
            this._DomainAliasModal.HeaderIconAlt = _GlobalResources.AddDomainAlias;
            this._DomainAliasModal.HeaderText = _GlobalResources.AddDomainAlias;
            this._DomainAliasModal.TargetControlID = targetControlId;
            this._DomainAliasModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._DomainAliasModal.SubmitButtonCustomText = _GlobalResources.AddDomainAlias;
            this._DomainAliasModal.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._DomainAliasModal.CloseButtonCustomText = _GlobalResources.Done;
            this._DomainAliasModal.ReloadPageOnClose = false;
            this._DomainAliasModal.SubmitButton.Command += new CommandEventHandler(this._AddandModifyDomainAliasModalSubmit_Command);

            // build the modal body
            this._ModalPropertiesContainer = new Panel();
            this._ModalPropertiesContainer.ID = "ModalPropertiesContainer";

            //domain alias
            this._DomainAlias = new TextBox();
            this._DomainAlias.ID = "DomainAlias_Field";
            this._DomainAlias.CssClass = "InputLong";

            // add controls to body
            this._ModalPropertiesContainer.Controls.Add(CustomerManagerPage.BuildFormField("DomainAlias",
                                                                            _GlobalResources.DomainAlias,
                                                                              this._DomainAlias.ID,
                                                                             this._DomainAlias,
                                                                             true,
                                                                             true));

            this._DomainAliasModal.AddControlToBody(this._ModalPropertiesContainer);

            // add modal to container
            this.SitePropertiesTabPanelsContainer.Controls.Add(this._DomainAliasModal);
        }
        #endregion

        #region _BuildPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for properties actions.
        /// </summary>
        private void _BuildPropertiesActionsPanel()
        {
            // clear controls from container
            this.SitePropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.SitePropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._SiteObject == null)
            { this._SaveButton.Text = _GlobalResources.CreateSite; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.SitePropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.SitePropertiesActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            if (this._SiteObject != null)
            {
                // NON-LANGUAGE SPECIFIC PROPERTIES
                this._Title.Text = this._SiteObject.Title;
                this._Company.Text = this._SiteObject.Company;
                this._HostName.Text = this._SiteObject.Hostname;
                this._ContactEmail.Text = this._SiteObject.ContactEmail;
                this._ContactName.Text = this._SiteObject.ContactName;

                this._UserLimitNone.Checked = this._SiteObject.UserLimit > 0 ? false : true;
                if (!this._UserLimitNone.Checked)
                { this._UserLimit.Text = Convert.ToString(this._SiteObject.UserLimit); }
                else
                { this._UserLimit.Text = string.Empty; }

                this._kbLimitNone.Checked = this._SiteObject.KbLimit > 0 ? false : true;
                if (!this._kbLimitNone.Checked)
                { this._KbLimit.Text = Convert.ToString(this._SiteObject.KbLimit); }
                else
                { this._KbLimit.Text = string.Empty; }

                this._Timezone.SelectedValue = Convert.ToString(this._SiteObject.IdTimezone);

                this._Status.SelectedValue = Convert.ToString(this._SiteObject.IsActive);

                if (this._SiteObject.Expires != null && this._SiteObject.Expires is DateTime)
                {
                    this._DtExpires.Value = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._SiteObject.Expires, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._SiteObject.IdTimezone).dotNetName));
                    this._DtExpires.NoneCheckBoxChecked = false;
                }

                // Default Language drop down
                this._DefaultLanguage.Items.Clear();
                this._DefaultLanguage.Items.Add(new ListItem { Text = this.GetLanguageNameByCode(Config.AccountSettings.DefaultLanguage), Value = Config.AccountSettings.DefaultLanguage });

                // Check appropriate available languages and add to default language drop down
                foreach (string item in this._SiteObject.AvailableLanguages)
                {
                    if (this._AvailableLanguages.Items.FindByValue(item) != null)
                    {
                        this._AvailableLanguages.Items.FindByValue(item).Selected = true;
                        this._DefaultLanguage.Items.Add(new ListItem { Text = this.GetLanguageNameByCode(item), Value = item });
                    }
                }

                if (this._DefaultLanguage.Items.FindByValue(this._SiteObject.LanguageString) != null)
                {
                    this._DefaultLanguage.SelectedValue = this._SiteObject.LanguageString;
                }

                /* DOMAIN ALIASES */

                // loop through the datatable and add each member to the listing container
                foreach (DataRow row in this._SiteDomainAliases.Rows)
                {
                    // container
                    Panel userNameContainer = new Panel();
                    userNameContainer.ID = "Domain_" + row["id"].ToString();

                    // remove expert button
                    Image removeExpertImage = new Image();
                    removeExpertImage.ID = "Domain_" + row["id"].ToString() + "_RemoveImage";
                    removeExpertImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                        ImageFiles.EXT_PNG, true);
                    removeExpertImage.CssClass = "SmallIcon";
                    removeExpertImage.Attributes.Add("onClick", "javascript:RemoveDomainAlias('" + row["displayName"].ToString() + "');");
                    removeExpertImage.Style.Add("cursor", "pointer");

                    // expert name
                    Literal userName = new Literal();
                    userName.Text = row["displayName"].ToString();

                    // add controls to container
                    userNameContainer.Controls.Add(removeExpertImage);
                    userNameContainer.Controls.Add(userName);
                    this._SiteDomainAliasesListContainer.Controls.Add(userNameContainer);
                }

                // features
                this._ECommerce.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.ECOMMERCE_ENABLED);
                this._Certifications.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE);
                this._QuizzesAndSurveys.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE);
                this._GoToMeeting.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE);
                this._GoToWebinar.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE);
                this._GoToTraining.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE);
                this._WebEx.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE);
                this._OpenSesame.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.OPENSESAME_ENABLE);
                this._xAPIEndpoints.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.XAPIENDPOINTS_ENABLE);
                this._UserAccountMerging.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.USERACCOUNTMERGING_ENABLE);
                this._UserProfileFileAttachments.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.USERPROFILEFILEATTACHMENTS_ENABLE);
                this._LiteUserFieldConfiguration.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.LITEUSERFIELDCONFIGURATION_ENABLE);
                this._UserRegistrationApproval.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.USERREGISTRATIONAPPROVAL_ENABLE);
                this._ImageEditor.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.IMAGE_EDITOR_ENABLE);
                this._CSSEditor.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.CSSEDITOR_ENABLE);
                this._SelfEnrollmentApproval.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE);
                this._TaskModules.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.TASKMODULES_ENABLE);
                this._TaskModules.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.TASKMODULES_ENABLE);
                this._OJTModules.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.OJTMODULES_ENABLE);
                this._LearningPaths.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE);
                this._ILTResources.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE);
                this._ReportingPDFExport.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.REPORTINGPDFEXPORT_ENABLE);
                this._MessageCenter.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.MESSAGECENTER_ENABLE);
                this._ObjectSpecificEmailNotifications.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE);
                this._RolesRules.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.ROLESRULES_ENABLE);
                this._Communities.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE);
                this._CourseDiscussionBoards.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE);
                this._GroupDiscussionBoards.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE);
                this._GroupDocuments.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE);
                this._RestrictedRuleCriteria.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.RESTRICTEDRULECRITERIA_ENABLE);
                this._StandaloneEnrollmentILT.Checked = (bool)this._SiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE);
                this._DashboardMode.SelectedValue = (string)this._SiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE);
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;
            bool domainAliasTabHasErrors = false;

            // TITLE - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._Title.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "Name", _GlobalResources.Title + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // PASSWORD -IS REQUIRED IN CREATE NEW SITE MODE
            if (String.IsNullOrWhiteSpace(this._Password.Text.Trim()) && this._SiteObject == null)
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "Password", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
            }
            else
            {
                //PASSWORD SHOULD MATCH
                if (!this._Password.Text.Equals(this._ConfirmPassword.Text.Trim()))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "Password", _GlobalResources.Password + " " + _GlobalResources.MustMatchConfirmationField);
                }
            }

            //EMAIL ADDRESS SHOULD BE VALID
            if (!String.IsNullOrWhiteSpace(this._ContactEmail.Text))
            {
                if (!Regex.IsMatch(this._ContactEmail.Text.Trim(), _RegexPatternForEmailId))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "ContactEmail", _GlobalResources.ContactEmail + " " + _GlobalResources.IsInvalid);
                }
            }

            // HOSTNAME -IS REQUIRED
            if (String.IsNullOrWhiteSpace(this._HostName.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "HostName", _GlobalResources.Hostname + " " + _GlobalResources.IsRequired);
            }

            // USER LIMIT - REQUIRED & MUST BE INT
            if (!String.IsNullOrWhiteSpace(this._UserLimit.Text))
            {
                bool result = true;
                int userLimit;
                if (!int.TryParse(this._UserLimit.Text, out userLimit))
                {
                    result = false;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "UserLimit", _GlobalResources.UserLimit + " " + _GlobalResources.IsInvalid);
                }
                if (result && userLimit <= 0)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "UserLimit", _GlobalResources.UserLimit + " " + _GlobalResources.IsInvalid);
                }
            }

            // KB LIMIT - REQUIRED & MUST BE INT
            if (!String.IsNullOrWhiteSpace(this._KbLimit.Text))
            {
                bool result = true;
                int kbLimit;
                if (!int.TryParse(this._KbLimit.Text, out kbLimit))
                {
                    isValid = false;
                    result = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "KbLimit", _GlobalResources.KBLimit + " " + _GlobalResources.IsInvalid);
                }
                if (result && kbLimit <= 0)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SitePropertiesContainer, "KbLimit", _GlobalResources.KBLimit + " " + _GlobalResources.IsInvalid);
                }
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.SitePropertiesContainer, "SiteProperties_Properties_TabLI"); }

            if (domainAliasTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.SitePropertiesContainer, "SiteProperties_DomainAliases_TabLI"); }
            return isValid;
        }
        #endregion

        #region _MaintainControlState
        /// <summary>
        /// Maintains the state of control (Javascript changes) after validations exception occurs.
        /// </summary>
        private void _MaintainControlState()
        {
            this._DefaultLanguage.Items.Clear();
            this._DefaultLanguage.Items.Add(new ListItem { Text = this.GetLanguageNameByCode(Config.AccountSettings.DefaultLanguage), Value = Config.AccountSettings.DefaultLanguage });
            foreach (ListItem item in this._AvailableLanguages.Items)
            {
                if (item.Selected)
                { this._DefaultLanguage.Items.Add(item); }
            }

            if (this._SiteObject != null && this._DefaultLanguage.Items.FindByValue(this._SiteObject.LanguageString) != null)
            {
                this._DefaultLanguage.SelectedValue = this._SiteObject.LanguageString;
            }
        }
        #endregion

        #region _AddandModifyDomainAliasModalSubmit_Command
        /// <summary>
        ///Handels the process of saving a "Domain Alias" .
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AddandModifyDomainAliasModalSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateAddAliasModalProperties())
                { throw new AsentiaException(); }

                //DO DOMIAN ALIASES TAB WORK
                Library.Account.SaveDomainAliasInCustomerManagerDataBase(AsentiaSessionState.IdAccount, this._DomainAlias.Text);

                // passed idCaller value as 1, because the Account DB procedure is accessed here from inside the Customer Manager application.
                Library.Site.SaveSiteToDomainAliasLink(this._SiteObject.Id, this._DomainAlias.Text, 1);

                // build the page controls
                this._BuildControls();
                _DomainAliasUpdatePanel.Update();

                //showing success feedback message
                this._DomainAliasModal.DisplayFeedback(_GlobalResources.DomainAliasHasBeenAddedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._DomainAliasModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._DomainAliasModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._DomainAliasModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._DomainAliasModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._DomainAliasModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _ValidateAddAliasModalProperties
        /// <summary>
        /// Validates the properties of modal.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateAddAliasModalProperties()
        {
            bool isValid = true;

            // HOSTNAME -IS REQUIRED
            if (String.IsNullOrWhiteSpace(this._DomainAlias.Text.Trim()))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._ModalPropertiesContainer, "DomainAlias", _GlobalResources.DomainAlias + " " + _GlobalResources.IsRequired);
            }
            else
            {
                //DOMAIN ALIAS SHOULD BE FULLY QUALIFIED
                if (!Regex.IsMatch(this._DomainAlias.Text.Trim(), _RegexPatternForFullyQualifiedDomainName))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._ModalPropertiesContainer, "DomainAlias", _GlobalResources.DomainAlias + " " + _GlobalResources.IsInvalid);
                }
            }
            return isValid;
        }
        #endregion

        #region _BuildDeleteAliasControls
        /// <summary>
        ///Builds controls to launch the delete alias confirmation modal
        /// </summary>
        private void _BuildDeleteAliasControls()
        {
            //hidden button to simulate the action on click of update 
            this._DeleteAliasButton = new LinkButton();
            this._DeleteAliasButton.ClientIDMode = ClientIDMode.Static;
            this._DeleteAliasButton.ID = "DeleteAliasButton";
            this._DeleteAliasButton.Style.Add("display", "none");

            this._DomainAliaIdHidden = new HiddenField();
            this._DomainAliaIdHidden.ID = "DomainAliaIdHidden";

            //add controls to container
            this.SitePropertiesActionsPanel.Controls.Add(this._DeleteAliasButton);
            this.SitePropertiesActionsPanel.Controls.Add(this._DomainAliaIdHidden);

            //Builds the confirmation modal for domain alias delete process.
            this._BuildDeleteAliasModal();
        }
        #endregion

        #region _BuildDeleteAliasModal
        /// <summary>
        /// Builds the confirmation modal for domain alias delete process.
        /// </summary>
        private void _BuildDeleteAliasModal()
        {
            this._DeleteDomainAliasModal = new ModalPopup("DeleteAliasAction");
            this._DeleteDomainAliasModal.OverrideLoadImagesFromCustomerManager = true;

            // set modal properties
            this._DeleteDomainAliasModal.Type = ModalPopupType.Confirm;
            this._DeleteDomainAliasModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this._DeleteDomainAliasModal.HeaderIconAlt = _GlobalResources.Delete;
            this._DeleteDomainAliasModal.HeaderText = _GlobalResources.DeleteDomainAlias;
            this._DeleteDomainAliasModal.TargetControlID = this._DeleteAliasButton.ClientID;
            this._DeleteDomainAliasModal.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            Literal body2 = new Literal();
            body1Wrapper.ID = "GridDeleteAliasActionModalBody1";

            body1.Text = _GlobalResources.AreYouSureYouWanttoDeleteSelectedDomainAlias;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._DeleteDomainAliasModal.AddControlToBody(body1Wrapper);

            this.SitePropertiesActionsPanel.Controls.Add(this._DeleteDomainAliasModal);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on domain alias.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(this._DomainAliaIdHidden.Value))
                {
                    //delete selected domain alias from account database
                    // passed idCaller value as 1, because the Account DB procedure is accessed here from inside the Customer Manager application.
                    Library.Site.DeleteDomainAlias(this._DomainAliaIdHidden.Value, this._SiteObject.Id, 1);

                    //delete selected domain alias from customer manager database
                    Library.Account.DeleteDomainAliasFromCustomerManagerDatabase(this._DomainAliaIdHidden.Value);

                    this._BuildControls();

                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, _GlobalResources.SelectedDomainAliasHasBeenDeletedSuccessfully, false);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, ex.Message, true);
            }
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidatePropertiesForm())
                {
                    this._MaintainControlState();
                    throw new AsentiaException();
                }

                // if there is no site object, create one
                if (this._SiteObject == null)
                { this._SiteObject = new AsentiaSite(); }

                bool updatingHostName = false;
                string existingHostName = string.Empty;

                // assign the object
                this._SiteObject.Title = this._Title.Text;
                this._SiteObject.Company = this._Company.Text;
                this._SiteObject.ContactName = this._ContactName.Text;

                //checks if we are going to update hostname
                if (this._SiteObject.Id > 0)
                {
                    if (this._SiteObject.Hostname.Trim() == this._HostName.Text.Trim())
                    { this._SiteObject.Hostname = this._HostName.Text.Trim(); }
                    else
                    {
                        updatingHostName = true;
                        existingHostName = this._SiteObject.Hostname.Trim();
                        this._SiteObject.Hostname = this._HostName.Text.Trim();
                    }
                }
                else
                {
                    updatingHostName = false;
                    this._SiteObject.Hostname = this._HostName.Text.Trim();
                }

                this._SiteObject.Hostname = this._HostName.Text;
                this._SiteObject.ContactEmail = this._ContactEmail.Text;

                if (!String.IsNullOrWhiteSpace(this._DefaultLanguageHidden.Value))
                { this._SiteObject.LanguageString = this._DefaultLanguageHidden.Value; }
                else
                { this._SiteObject.LanguageString = this._DefaultLanguage.SelectedValue; }

                // save languages

                // get a list of all languages
                DataTable languages = Language.GetLanguages();

                // declare data table for available languages
                DataTable languageIds = new DataTable();
                languageIds.Columns.Add("id", typeof(int));

                // loop through the available languages and add the IDs to the language id array list
                foreach (ListItem item in this._AvailableLanguages.Items)
                {
                    if (item.Selected)
                    {
                        foreach (DataRow row in languages.Rows)
                        {
                            if (row["code"].ToString() == item.Value)
                            {
                                languageIds.Rows.Add(Convert.ToInt32(row["idLanguage"].ToString()));
                                break;
                            }
                        }
                    }
                }

                this._SiteObject.IdTimezone = Convert.ToInt32(this._Timezone.SelectedValue);
                this._SiteObject.Password = this._Password.Text.Trim();

                if (!this._UserLimitNone.Checked)
                { this._SiteObject.UserLimit = Convert.ToInt32(this._UserLimit.Text); }
                else { this._SiteObject.UserLimit = null; }

                if (!this._kbLimitNone.Checked)
                { this._SiteObject.KbLimit = Convert.ToInt32(this._KbLimit.Text); }
                else { this._SiteObject.KbLimit = null; }

                this._SiteObject.Title = this._Title.Text;

                this._SiteObject.IsActive = Convert.ToBoolean(this._Status.SelectedValue);

                // convert the expiration date to UTC if it is not null
                if (this._DtExpires.Value != null)
                {
                    DateTime expiration = (DateTime)this._DtExpires.Value;
                    this._SiteObject.Expires = (DateTime)expiration.ToUniversalTime();
                }
                else
                {
                    this._SiteObject.Expires = null;
                }

                // Copy license Agreement if the checkbox is checked
                if (this._CopyLicenseAgreementCheckBox != null)
                {
                    if (this._CopyLicenseAgreementCheckBox.Checked)
                    {
                        Directory.CreateDirectory(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + this._HostName.Text);

                        if (File.Exists(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + @"\License.html")))
                        { File.Copy(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + "\\License.html"), Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + this._HostName.Text + "\\License.html", true); }
                    }
                }

                // FEATURES

                DataTable siteParams = new DataTable();
                siteParams.Columns.Add("key");
                siteParams.Columns.Add("value");

                DataRow newSiteParamRow;

                // e-commerce
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.ECOMMERCE_ENABLED;
                newSiteParamRow["value"] = this._ECommerce.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // quizzes and survey
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.QUIZZESANDSURVEYS_ENABLE;
                newSiteParamRow["value"] = this._QuizzesAndSurveys.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // certifications
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.CERTIFICATIONS_ENABLE;
                newSiteParamRow["value"] = this._Certifications.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // GoToMeeting
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTM_ENABLE;
                newSiteParamRow["value"] = this._GoToMeeting.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // GoToWebinar
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTW_ENABLE;
                newSiteParamRow["value"] = this._GoToWebinar.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // GoToTraining
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTT_ENABLE;
                newSiteParamRow["value"] = this._GoToTraining.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // WebEx
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_WEBEX_ENABLE;
                newSiteParamRow["value"] = this._WebEx.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // OpenSesame
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.OPENSESAME_ENABLE;
                newSiteParamRow["value"] = this._OpenSesame.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // xAPI Endpoints
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.XAPIENDPOINTS_ENABLE;
                newSiteParamRow["value"] = this._xAPIEndpoints.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // User Account Merging
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.USERACCOUNTMERGING_ENABLE;
                newSiteParamRow["value"] = this._UserAccountMerging.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // User Profile File Attachments
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.USERPROFILEFILEATTACHMENTS_ENABLE;
                newSiteParamRow["value"] = this._UserProfileFileAttachments.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Lite User Field Configuration
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.LITEUSERFIELDCONFIGURATION_ENABLE;
                newSiteParamRow["value"] = this._LiteUserFieldConfiguration.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // User Registration Approval
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.USERREGISTRATIONAPPROVAL_ENABLE;
                newSiteParamRow["value"] = this._UserRegistrationApproval.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Self-Enrollment Approval
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE;
                newSiteParamRow["value"] = this._SelfEnrollmentApproval.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Image Editor
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.IMAGE_EDITOR_ENABLE;
                newSiteParamRow["value"] = this._ImageEditor.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // CSS Editor
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.CSSEDITOR_ENABLE;
                newSiteParamRow["value"] = this._CSSEditor.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Task Modules
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.TASKMODULES_ENABLE;
                newSiteParamRow["value"] = this._TaskModules.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // OJT Modules
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.OJTMODULES_ENABLE;
                newSiteParamRow["value"] = this._OJTModules.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Learning Paths
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.LEARNINGPATHS_ENABLE;
                newSiteParamRow["value"] = this._LearningPaths.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // ILT Resources
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.ILTRESOURCES_ENABLE;
                newSiteParamRow["value"] = this._ILTResources.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Report PDF Export
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.REPORTINGPDFEXPORT_ENABLE;
                newSiteParamRow["value"] = this._ReportingPDFExport.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Message Center
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.MESSAGECENTER_ENABLE;
                newSiteParamRow["value"] = this._MessageCenter.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Object Specific Email Notifications
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE;
                newSiteParamRow["value"] = this._ObjectSpecificEmailNotifications.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Roles Rules
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.ROLESRULES_ENABLE;
                newSiteParamRow["value"] = this._RolesRules.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Communities
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.COMMUNITIES_ENABLE;
                newSiteParamRow["value"] = this._Communities.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);
                // Course Discussion Boards
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE;
                newSiteParamRow["value"] = this._CourseDiscussionBoards.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Group Discussion Boards
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.GROUPS_DISCUSSION_ENABLE;
                newSiteParamRow["value"] = this._GroupDiscussionBoards.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Group Documents
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.GROUPS_DOCUMENTS_ENABLE;
                newSiteParamRow["value"] = this._GroupDocuments.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Dashboard Mode
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.FEATURE_DASHBOARDMODE;
                newSiteParamRow["value"] = this._DashboardMode.SelectedValue;
                siteParams.Rows.Add(newSiteParamRow);

                // Standalone Enrollment of ILT
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.STANDALONEENROLLMENT_ENABLE;
                newSiteParamRow["value"] = this._StandaloneEnrollmentILT.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Advanced Rules
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.RESTRICTEDRULECRITERIA_ENABLE;
                newSiteParamRow["value"] = this._RestrictedRuleCriteria.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);


                //Checks for existance of hostname with same name only when creating new site
                //sets true otherwise
                bool isHostNameExists = false;
                if (this._SiteObject.Id > 0)
                {
                    isHostNameExists = Asentia.CustomerManager.Library.Account.DoesHostNameExists(this._HostName.Text);
                }

                if (!isHostNameExists)
                {
                    //saves site details
                    int idSite = this._SiteObject.Save(0, Config.AccountSettings.DefaultLanguage, 1);
                    this.ViewState["id"] = idSite;

                    if (idSite > 0)
                    {
                        //process for updating hostname
                        if (updatingHostName)
                        {
                            // delete selected domain alias from account database
                            // passed idCaller value as 1, because the Account DB procedure is accessed here from inside the Customer Manager application.
                            Library.Site.DeleteDomainAlias(existingHostName, idSite, 1);

                            //delete selected domain alias from customer manager database
                            Library.Account.DeleteHostNameFromCustomerManagerDatabase(existingHostName);
                        }

                        // save site to domain alias link (Account Database)
                        // passed idCaller value as 1, because the Account DB procedure is accessed here from inside the Customer Manager application.
                        Library.Site.SaveSiteToDomainAliasLink(idSite, this._SiteObject.Hostname, 1);

                        //saves site to domain alis link (Customer manager database)
                        Library.Account.SaveAccountToDomainAliasLink(AsentiaSessionState.IdAccount, this._SiteObject.Hostname);

                        // saves the available languages
                        this._SiteObject.SaveAvailableLanguages(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, this._SiteObject.Id, languageIds);

                        // save the site params (ECommerce.Enabled, Certifications.Enable and QuizzesAndSurveys.Enable)
                        AsentiaSite.SaveSiteParams(idSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdAccountUser, idSite, siteParams);
                    }

                    this._SiteObject = new AsentiaSite(idSite, 1, idSite);

                    // build the page controls
                    this._BuildControls();

                    // display the saved feedback
                    this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, _GlobalResources.SiteDetailsHaveBeenSavedSuccessfully, false);
                }
                else
                {
                    // display the failure message
                    this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, _GlobalResources.HostnameAlreadyExistsPleaseTryAgain, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SitePropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/Accounts/sites");
        }
        #endregion

        #region _BuildLicenseAgreementModal
        /// <summary>
        /// Builds the modal for viewing the license agreement
        /// </summary>
        private void _BuildViewLicenseAgreementModal()
        {
            // Create license agreement modal
            this._ViewLicenseAgreementModal = new ModalPopup("ViewLicenseAgreementModal");
            this._ViewLicenseAgreementModal.TargetControlID = this._ViewLicenseAgreementButton.ID;
            this._ViewLicenseAgreementModal.Type = ModalPopupType.Information;
            this._ViewLicenseAgreementModal.OverrideLoadImagesFromCustomerManager = true;
            this._ViewLicenseAgreementModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LICENSE,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this._ViewLicenseAgreementModal.HeaderIconAlt = _GlobalResources.LicenseAgreement;
            this._ViewLicenseAgreementModal.HeaderText = _GlobalResources.LicenseAgreement;
            this._ViewLicenseAgreementModal.ReloadPageOnClose = false;

            // License Agreement HTML
            HtmlGenericControl licenseAgreementHtml = new HtmlGenericControl();
            licenseAgreementHtml.ID = "LicenseAgreementHtml_Field";

            if (File.Exists(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + @"\License.html")))
            { licenseAgreementHtml.InnerHtml = File.ReadAllText(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + @"\License.html")); }

            this._ViewLicenseAgreementModal.AddControlToBody(CustomerManagerPage.BuildFormField("LicenseAgreementHtml", null, licenseAgreementHtml.ID, licenseAgreementHtml, false, false));

            this.SitePropertiesActionsPanel.Controls.Add(this._ViewLicenseAgreementModal);
        }
        #endregion
    }
}
