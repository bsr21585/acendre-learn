﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace Asentia.CustomerManager.Pages.Accounts.Sites
{
    public class Default : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public UpdatePanel SiteGridUpdatePanel;
        public Grid SiteGrid;
        public Panel GridActionsPanel = new Panel();
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup GridConfirmAction;

        public ModalPopup UpdateStatusModal;
        public HiddenField UpdateStatusDataHiddenField;
        public Button UpdateStatusModalHiddenButton;
        #endregion

        #region Private Properties
        HiddenField _SSOHostname;
        Button _InitiateSSOHiddenButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            base.OnPreRender(e);

            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.CustomerManager.Pages.Accounts.Sites.Default.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Page</param>
        /// <param name="e">Event arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Sites));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(_GlobalResources.Sites, ImageFiles.GetIconPath(ImageFiles.ICON_SITE, ImageFiles.EXT_PNG, true));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildGridActionsPanel();
            this._BuildGridActionsModal();
            this._BuildUpdateStatusModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.SiteGrid.BindData();
            }
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {        
            this.SiteGrid = new Grid("AccountWithCMImages");
            this.SiteGrid.ID = "SiteGrid";
            this.SiteGrid.OverrideLoadImagesFromCustomerManager = true;         
            this.SiteGrid.StoredProcedure = AsentiaSite.GridProcedure;
            this.SiteGrid.AddFilter("@idCallerAccount", SqlDbType.Int, 4, AsentiaSessionState.IdAccount);
            this.SiteGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdAccountUser);
            this.SiteGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.SiteGrid.IdentifierField = "idSite";
            this.SiteGrid.DefaultSortColumn = "title";

            // data key names
            this.SiteGrid.DataKeyNames = new string[] { "idSite", "hostname", "title", "userLimit", "kbLimit", "dtExpires", "isLogonOn" };

            // columns

            GridColumn siteName = new GridColumn(_GlobalResources.Title + " (" + _GlobalResources.Hostname + "), " + _GlobalResources.ExpirationDate + ", "
                                                + _GlobalResources.UserLimit + ", " + _GlobalResources.DiskLimit, "title", "title");
            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is generated dynamically in row data bound event

            // add columns to data grid
            this.SiteGrid.AddColumn(siteName);
            this.SiteGrid.AddColumn(options);

            this.SiteGrid.RowDataBound += SiteGrid_RowDataBound;

            // attach the grid to the update panel
            this.SiteGridUpdatePanel.ContentTemplateContainer.Controls.Add(this.SiteGrid);
        }
        #endregion

        #region SiteGrid_RowDataBound
        /// <summary>
        /// Row Data Bound event handler for site grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SiteGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                bool isActive = Convert.ToBoolean(rowView["isActive"]);
                int idSite = Convert.ToInt32(rowView["idSite"]);
                string dtExpires = rowView["dtExpires"].ToString();
                string kbLimit = rowView["kbLimit"].ToString();
                string title = rowView["title"].ToString();
                string userLimit = rowView["userLimit"].ToString();
                string hostname = rowView["hostname"].ToString();
                bool isLogonOn = Convert.ToBoolean(rowView["isLogonOn"]);

                if (dtExpires == "" || dtExpires == null)
                {
                    dtExpires = "Never";
                }

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_SITE, ImageFiles.EXT_PNG, true);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = title;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // title
                Label titleLinkLabel = new Label();
                titleLinkLabel.CssClass = "GridBaseTitle";

                HyperLink titleLink = new HyperLink();
                titleLink.Text = title;
                titleLink.NavigateUrl = "Modify.aspx?id=" + idSite.ToString();
                titleLinkLabel.Controls.Add(titleLink);

                // hostname
                Label hostnameLabel = new Label();
                hostnameLabel.Text = " (" + hostname + ")";
                hostnameLabel.CssClass = "GridSecondaryTitle";
                titleLinkLabel.Controls.Add(hostnameLabel);
                e.Row.Cells[1].Controls.Add(titleLinkLabel);

                // Expiration date
                Label expirationLabel = new Label();
                expirationLabel.Text = "Expires: " + dtExpires;
                expirationLabel.CssClass = "GridSecondaryLine";
                e.Row.Cells[1].Controls.Add(expirationLabel);

                // user and disk limit
                Label userDiskLimitLabel = new Label();
                userDiskLimitLabel.CssClass = "GridSecondaryLine";
                userDiskLimitLabel.Text = "User Limit: " + userLimit + "  Disk Limit: ";

                // If the disk limit is not unlimited, calculate the disk size string
                if (kbLimit == "Unlimited")
                {
                    userDiskLimitLabel.Text = userDiskLimitLabel.Text + "Unlimited";
                }
                else
                {
                    userDiskLimitLabel.Text = userDiskLimitLabel.Text + Utility.GetSizeStringFromKB(Convert.ToInt32(kbLimit));
                }
                e.Row.Cells[1].Controls.Add(userDiskLimitLabel);

                Label statusSpan = new Label();
                statusSpan.CssClass = "GridImageLink";

                // Status
                LinkButton statusLinkButton = new LinkButton();
                statusLinkButton.ID = "StatusLinkButton";
                statusLinkButton.OnClientClick = "UpdateSiteStatus('" + idSite.ToString() + "', '" + isActive.ToString() + "'); return false;";

                Image statusImage = new Image();
                statusImage.ID = "StatusImage";
                statusImage.CssClass = "SmallIcon";

                // set the appropriate link image based on the site being active or not
                if (isActive)
                {
                    statusLinkButton.ToolTip = _GlobalResources.DisableSite;
                    statusImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN,
                                            ImageFiles.EXT_PNG,
                                            true);

                }
                else
                {
                    statusLinkButton.ToolTip = _GlobalResources.EnableSite;
                    statusImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED,
                                            ImageFiles.EXT_PNG,
                                            true);
                }

                statusLinkButton.Controls.Add(statusImage);
                statusSpan.Controls.Add(statusLinkButton);

                e.Row.Cells[2].Controls.Add(statusSpan);


                // Log In
                Label loginSpan = new Label();
                loginSpan.CssClass = "GridImageLink";

                HyperLink loginLinkButton = new HyperLink();
                loginLinkButton.ID = "LoginLinkButton";
                loginLinkButton.NavigateUrl = "javascript:void(0);";
                loginLinkButton.Attributes.Add("onclick", "InitiateSSOToSite('" + hostname + "');");

                Image loginImage = new Image();
                loginImage.ID = "LoginImage";
                loginImage.CssClass = "LoginIcon";
                loginImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                                            ImageFiles.EXT_PNG,
                                            true);

                // set the appropriate css based on the isLogonOn flag being active or not
                if (isLogonOn)
                {
                    loginLinkButton.ToolTip = _GlobalResources.Log_In;
                    loginLinkButton.Enabled = true;
                    loginImage.CssClass = "SmallIcon";

                }
                else
                {
                    loginLinkButton.Enabled = false;
                    loginImage.CssClass = "SmallIcon DimIcon";
                }

                loginLinkButton.Controls.Add(loginImage);
                loginSpan.Controls.Add(loginLinkButton);

                e.Row.Cells[2].Controls.Add(loginSpan);
            }

        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD SITE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddSiteLink",
                                                null,
                                                "Modify.aspx",
                                                null,
                                                _GlobalResources.NewSite,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_SITE, ImageFiles.EXT_PNG, true),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG, true))
                );

            // ADD NEW SITE FROM TEMPLATE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddNewPortalCloneLink",
                                                null,
                                                "NewPortalClone.aspx",
                                                null,
                                                _GlobalResourcesCHETU1.NewSiteFromTemplate,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_SITE, ImageFiles.EXT_PNG, true),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG, true))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);

            // attach the hidden input and button for SSO
            this._SSOHostname = new HiddenField();
            this._SSOHostname.ID = "SSOHostname";
            this.ObjectOptionsPanel.Controls.Add(this._SSOHostname);

            this._InitiateSSOHiddenButton = new Button();
            this._InitiateSSOHiddenButton.ID = "InitiateSSOHiddenButton";
            this._InitiateSSOHiddenButton.Command += this._InitiateSSOHiddenButton_Command;
            this._InitiateSSOHiddenButton.Style.Add("display", "none");
            this.ObjectOptionsPanel.Controls.Add(this._InitiateSSOHiddenButton);
        }
        #endregion

        #region _BuildGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsPanel()
        {
            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG,
                                                          true);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedSite_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.GridActionsPanel.Controls.Add(this.DeleteButton);

            // hidden update status data
            this.UpdateStatusDataHiddenField = new HiddenField();
            this.UpdateStatusDataHiddenField.ID = "UpdateStatusDataHiddenField";

            // add hidden status data to page
            this.GridActionsPanel.Controls.Add(this.UpdateStatusDataHiddenField);

            // hidden button to trigger update status modal
            this.UpdateStatusModalHiddenButton = new Button();
            this.UpdateStatusModalHiddenButton.ID = "UpdateStatusModalHiddenButton";
            this.UpdateStatusModalHiddenButton.Style.Add("display", "none");

            // add hidden button to page
            this.GridActionsPanel.Controls.Add(this.UpdateStatusModalHiddenButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction = new ModalPopup("GridConfirmAction");
            this.GridConfirmAction.OverrideLoadImagesFromCustomerManager = true;

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.OverrideLoadImagesFromCustomerManager = true;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedSite_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.ShowLoadingPlaceholder = true;
            this.GridConfirmAction.SubmitButton.OnClientClick = "UpdateLoadingPlaceholder();";
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            Panel confirmActionPanel = new Panel();
            confirmActionPanel.ID = "ConfirmActionPanel";

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            Literal body2 = new Literal();
            body1Wrapper.ID = "GridConfirmActionModalBody1";

            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseSite_s;

            body1Wrapper.Controls.Add(body1);

            confirmActionPanel.Controls.Add(body1Wrapper);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(confirmActionPanel);

            this.GridActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.SiteGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.SiteGrid.Rows[i].FindControl(this.SiteGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        {
                            // get records to delete from database
                            recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"]));

                        }
                    }
                }

                // delete the records
                DataTable domainList = AsentiaSite.Delete(0, AsentiaSessionState.UserCulture, 1, recordsToDelete);
                string lastDeletedHostname = String.Empty;

                // loop through deleted sites, removing hostname entries from customer manager, and cleaning up the file system
                foreach (DataRow row in domainList.Rows)
                {
                    // delete the hostnames from customer manager database
                    Library.Account.DeleteHostNameFromCustomerManagerDatabase(Convert.ToString(row["domain"]));

                    // clean up the file system
                    if (lastDeletedHostname != Convert.ToString(row["hostname"]))
                    {
                        // _config/[hostname]
                        if (Directory.Exists(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + Convert.ToString(row["hostname"])))
                        {
                            Utility.EmptyOldFolderItems(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + Convert.ToString(row["hostname"]), 0, true);
                            Directory.Delete(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + Convert.ToString(row["hostname"]), true);
                        }

                        // _log/[hostname]
                        if (Directory.Exists(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.LOG.Replace("/", "\\") + Convert.ToString(row["hostname"])))
                        {
                            Utility.EmptyOldFolderItems(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.LOG.Replace("/", "\\") + Convert.ToString(row["hostname"]), 0, true);
                            Directory.Delete(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.LOG.Replace("/", "\\") + Convert.ToString(row["hostname"]), true);
                        }

                        // warehouse/[hostname]
                        if (Directory.Exists(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + Convert.ToString(row["hostname"])))
                        {
                            Utility.EmptyOldFolderItems(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + Convert.ToString(row["hostname"]), 0, true);
                            Directory.Delete(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + Convert.ToString(row["hostname"]), true);
                        }

                        // warehouse content packages
                        string directorySearchPattern = AsentiaSessionState.IdAccount + "-" + row["idSite"].ToString() + "-*";
                        string[] directoryList = Directory.GetDirectories(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\"), directorySearchPattern);

                        foreach (string directory in directoryList)
                        { Directory.Delete(directory, true); }

                        // set the last deleted hostname
                        lastDeletedHostname = Convert.ToString(row["hostname"]);
                    }
                }

                // display the success message
                this.DisplayFeedback(_GlobalResources.TheSelectedsite_sHaveBeenDeletedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid                
                this.SiteGrid.BindData();
            }
        }
        #endregion

        #region _InitiateSSOHiddenButton_Command
        /// <summary>
        /// Performs the single sign-on by generating a token and passing it to the portal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _InitiateSSOHiddenButton_Command(object sender, CommandEventArgs e)
        {
            string responseString = String.Empty;

            try
            {
                // validate that we have a hostname
                if (String.IsNullOrEmpty(this._SSOHostname.Value))
                { throw new AsentiaException(_GlobalResources.TheSSOHostnameIsInvalid); }

                // set up variables for SSO
                string sharedSecretKeyJSON = "{ secretKey: \"" + Config.ApplicationSettings.CustomerManagerSSOSecretKey + "\" }";
                string ssoTokenGeneratorURL = "https://" + this._SSOHostname.Value + "." + Config.AccountSettings.BaseDomain + "/_util/UtilityServices.asmx/GetSSOFromCMToken";
                string ssoURL = "https://" + this._SSOHostname.Value + "." + Config.AccountSettings.BaseDomain + "/_util/DoSSO.aspx?token={0}";
                string token = null;

                // GENERATE THE SSO TOKEN BY HITTING THE WEB SERVICE

                // set security protocol to TLS 1.2
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                if (Config.AccountSettings.BaseDomain.Contains("icslearning.local"))
                { ServicePointManager.ServerCertificateValidationCallback = delegate { return true; }; }

                // create the request
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ssoTokenGeneratorURL);

                // encode payload into bytes
                byte[] bytes;
                bytes = Encoding.ASCII.GetBytes(sharedSecretKeyJSON);

                // set the payload type, length, and method
                request.ContentType = "application/json; charset=utf-8";
                request.ContentLength = bytes.Length;
                request.Method = "POST";

                // write the request stream (payload)
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                // get the response
                //string responseString = String.Empty;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        { responseString = new StreamReader(responseStream).ReadToEnd(); }
                    }
                    else
                    { throw new AsentiaException(_GlobalResources.UnableToConnectToSSOTokenGeneratorService); }
                }

                // get the response and turn it into a JSON object
                JObject jsonResponse = JObject.Parse(responseString); // this only gets the "d" object, we need to do further parsing in the next line
                JObject jsonData = JObject.Parse(jsonResponse["d"].ToString());

                bool actionSuccessful = (bool)jsonData["actionSuccessful"];
                string exception = jsonData["exception"].ToString();
                token = jsonData["token"].ToString();

                // validate that we got a token back
                if (!actionSuccessful)
                { throw new AsentiaException(exception); }

                if (String.IsNullOrEmpty(token))
                { throw new AsentiaException(_GlobalResources.UnableToAcquireSSOToken); }

                // if we get here without exception, we got a token, do the SSO
                // note: ScriptManager opens in new window but needs popup blocker off, Response.Redirect redirects
                ScriptManager.RegisterStartupScript(this.ObjectOptionsPanel, typeof(Default), "DoSSO", "window.open(\"" + String.Format(ssoURL, token) + "\", \"_blank\");", true);
                //Response.Redirect(String.Format(ssoURL, token));  
            }
            catch (AsentiaException aEx)
            {
                this.DisplayFeedback(aEx.Message, true);
            }
            catch (Exception ex)
            {
                this.DisplayFeedback(ex.Message + " | " + ex.StackTrace + " | The response string: " + responseString, true);
            }
            finally
            {
                // clear the hostname input and re-bind the grid
                this._SSOHostname.Value = "";
                this.SiteGrid.BindData();
            }
        }
        #endregion

        #region _BuildUpdateStatusModal
        /// <summary>
        /// Builds the modal for updating the status of an account.
        /// </summary>
        private void _BuildUpdateStatusModal()
        {
            this.UpdateStatusModal = new ModalPopup();
            this.UpdateStatusModal.ID = "UpdateStatusModal";

            // set modal properties
            this.UpdateStatusModal.Type = ModalPopupType.Confirm;
            this.UpdateStatusModal.OverrideLoadImagesFromCustomerManager = true;
            this.UpdateStatusModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this.UpdateStatusModal.HeaderIconAlt = _GlobalResources.UpdateStatus;
            this.UpdateStatusModal.HeaderText = _GlobalResources.UpdateStatus;
            this.UpdateStatusModal.TargetControlID = this.UpdateStatusModalHiddenButton.ID;
            this.UpdateStatusModal.ShowLoadingPlaceholder = true;
            this.UpdateStatusModal.SubmitButton.Command += new CommandEventHandler(this._UpdateStatus_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            Literal body2 = new Literal();
            body1Wrapper.ID = "UpdateStatusModalBody1";

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.UpdateStatusModal.AddControlToBody(body1Wrapper);

            this.GridActionsPanel.Controls.Add(this.UpdateStatusModal);
        }
        #endregion

        #region _UpdateStatus_Command
        /// <summary>
        /// Updates the status of an account.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _UpdateStatus_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string[] data = this.UpdateStatusDataHiddenField.Value.Split('|');
                int idSite = Convert.ToInt32(data[0]);
                int isActive = Convert.ToInt32(data[1]);

                // update the status
                AsentiaSite site = new AsentiaSite(AsentiaSessionState.IdSiteUser, idSite, true);

                if (isActive == 1)
                {
                    site.IsActive = true;
                }
                else
                {
                    site.IsActive = false;
                }

                site.Save(idSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser);

                // display the success message
                if (isActive == 1)
                {
                    this.DisplayFeedback(_GlobalResources.TheSiteHasBeenEnabledSuccessfully, false);
                }
                else
                {
                    this.DisplayFeedback(_GlobalResources.TheSiteHasBeenDisabledSuccessfully, false);
                }

                // rebind the grid
                this.SiteGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this.SiteGrid.BindData();
            }
        }
        #endregion
    }
}
