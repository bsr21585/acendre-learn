﻿function InitiateSSOToSite(hostname) {
    $("#SSOHostname").val(hostname);
    $("#InitiateSSOHiddenButton").click();
}

// updates the stie status
function UpdateSiteStatus(idSite, isActive) {

    if (isActive == "True") { // if isActive is true, then change the status to 0 (false)
        isActive = '0';
        $('#UpdateStatusModalBody1').html("Are you sure you want to disable this site?");
    } else { // if isActive is false, then change the status to 1 (true)
        isActive = '1';
        $('#UpdateStatusModalBody1').html("Are you sure you want to enable this site?");
    }

    // update hidden field with account and status values
    $('#UpdateStatusDataHiddenField').val(idSite + '|' + isActive);

    // click the hidden button to activate modal
    $('#UpdateStatusModalHiddenButton').click();
}

// Shows the loading animation when deleting sites
function UpdateLoadingPlaceholder() {
   
    $('#GridConfirmActionModalPopupPostbackLoadingPlaceholder').css("display", "");

    $('#GridConfirmActionModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').html("Deleting Sites");

    $('#ConfirmActionPanel').css("display", "none");
    $('#GridConfirmActionModalPopupButtons').css("display", "none");
}