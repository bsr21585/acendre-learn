﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.CustomerManager.Pages.Accounts
{
    public class Impersonate : CustomerManagerAuthenticatedPage
    {
        #region Private Properties
        private Library.Account _AccountObject;
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get the user object
            this._GetUserObject();

            //// TODO: CHECK PERMISSIONS
            //if (AsentiaSessionState.IdAccountUser!= this._AccountObject.Id)
            //{ Response.Redirect("~/accounts"); }

            // impersonate the user
            this._ImpersonateUser();
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on querystring if exists.
        /// </summary>
        private void _GetUserObject()
        {
            // get the id querystring parameter
            int id = this.QueryStringInt("aid", 0);

            if (id > 0)
            {
                try
                { this._AccountObject = new Library.Account(id); }
                catch
                { Response.Redirect("~/accounts"); }
            }
            else
            { Response.Redirect("~/accounts"); }
        }
        #endregion

        #region _ImpersonateUser
        /// <summary>
        /// Impersonates the user and redirects.
        /// </summary>
        private void _ImpersonateUser()
        {
            // set the currently logged in user as the impersonating user
            AsentiaSessionState.IdImpersonatingAccountUser = AsentiaSessionState.IdAccountUser;
            AsentiaSessionState.ImpersonatingUserFirstName = AsentiaSessionState.UserFirstName;
            AsentiaSessionState.ImpersonatingUserLastName = AsentiaSessionState.UserLastName;
            AsentiaSessionState.IdImpersonatingAccount = AsentiaSessionState.IdAccount;

            // set the user we're impersonating as the current session user
            AsentiaSessionState.IdAccount = this._AccountObject.Id;
            AsentiaSessionState.UserFirstName = this._AccountObject.ContactFirstName;
            AsentiaSessionState.UserLastName = this._AccountObject.ContactLastName;

            // redirect
            Response.Redirect("~/");
        }
        #endregion
    }
}
