﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit.HTMLEditor;
using System.IO;

namespace Asentia.CustomerManager.Pages.Accounts.License
{
    public class Default : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public UpdatePanel LicenseAgreementPanel;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private Button _EditLicenseAgreementButton;
        private HtmlGenericControl _LicenseAgreementHtml;

        private Editor _LicenseAgreementEditor;
        private ModalPopup _LicenseAgreementEditorModal = new ModalPopup("LicenseAgreementEditorModal");
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            base.OnPreRender(e);

            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.CustomerManager.Pages.Accounts.License.Default.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.LicenseAgreement));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(_GlobalResources.LicenseAgreement, ImageFiles.GetIconPath(ImageFiles.ICON_LICENSE,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the license agreement, actions panel, and modal
            this._BuildLicenseAgreementUpdatePanel();
            this._BuildActionsPanel();
            this._BuildLicenseAgreementEditorModal();

        }
        #endregion

        #region _BuildLicenseAgreementUpdatePanel
        /// <summary>
        /// Builds the license agreement panel
        /// </summary>
        private void _BuildLicenseAgreementUpdatePanel()
        {
            this._LicenseAgreementHtml = new HtmlGenericControl();
            this._LicenseAgreementHtml.ID = "LicenseAgreementHtml_Field";

            if (File.Exists(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + @"\License.html")))
            { this._LicenseAgreementHtml.InnerHtml = File.ReadAllText(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + @"\License.html")); }

            this.LicenseAgreementPanel.ContentTemplateContainer.Controls.Add(CustomerManagerPage.BuildFormField("LicenseAgreementHtml", _GlobalResources.LicenseAgreement, this._LicenseAgreementHtml.ID, this._LicenseAgreementHtml, false, false));
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the license agreement actions panel
        /// </summary>
        private void _BuildActionsPanel()
        {
            this._EditLicenseAgreementButton = new Button();
            this._EditLicenseAgreementButton.ID = "EditLicenseAgreementButton";
            this._EditLicenseAgreementButton.CssClass = "Button ActionButton";
            this._EditLicenseAgreementButton.Text = _GlobalResources.EditLicenseAgreement;
            this._EditLicenseAgreementButton.OnClientClick = "ClearModalSuccess('" + this._LicenseAgreementEditorModal.ID + "');";
            this.ActionsPanel.Controls.Add(this._EditLicenseAgreementButton);

        }
        #endregion

        #region _BuildLicenseAgreementEditorModal
        /// <summary>
        /// Builds the modal for editing the license agreement
        /// </summary>
        private void _BuildLicenseAgreementEditorModal()
        {
            // Create license agreement modal
            this._LicenseAgreementEditorModal.TargetControlID = this._EditLicenseAgreementButton.ID;
            this._LicenseAgreementEditorModal.Type = ModalPopupType.Form;
            this._LicenseAgreementEditorModal.OverrideLoadImagesFromCustomerManager = true;
            this._LicenseAgreementEditorModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LICENSE,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this._LicenseAgreementEditorModal.HeaderIconAlt = _GlobalResources.LicenseAgreement;
            this._LicenseAgreementEditorModal.HeaderText = _GlobalResources.LicenseAgreement;
            this._LicenseAgreementEditorModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._LicenseAgreementEditorModal.SubmitButtonCustomText = _GlobalResources.Save;
            this._LicenseAgreementEditorModal.ReloadPageOnClose = false;
            this._LicenseAgreementEditorModal.SubmitButton.Command += this._EditLicenseAgreement_SubmitButton_Command;

            // License Agreement Editor
            this._LicenseAgreementEditor = new Editor();
            this._LicenseAgreementEditor.ClientIDMode = ClientIDMode.AutoID;

            if (File.Exists(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + @"\License.html")))
            { this._LicenseAgreementEditor.Content = File.ReadAllText(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + @"\License.html")); }

            this._LicenseAgreementEditorModal.AddControlToBody(this._LicenseAgreementEditor);

            this.ActionsPanel.Controls.Add(this._LicenseAgreementEditorModal);
        }
        #endregion

        #region _EditLicenseAgreement_SubmitButton_Command
        /// <summary>
        /// Command handler for the edit license agreement modal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _EditLicenseAgreement_SubmitButton_Command(object sender, CommandEventArgs e)
        {
            // extract the body of the license agreement from the editor content
            string htmlBody = this._LicenseAgreementEditor.Content;

            // if the content contains the title tag, strip out the body text from the content
            if (this._LicenseAgreementEditor.Content.Contains("</title>"))
            {
                htmlBody = this._LicenseAgreementEditor.Content.Split(new string[] { "</title>" }, StringSplitOptions.None)[1];
            }

            // build the html text
            string fullHtml = @"<!DOCTYPE html><html xmlns=""http://www.w3.org/1999/xhtml""<head><title>License Agreement</title><body>" + htmlBody + "</body></html>";

            // write the html to the file
            File.WriteAllText(Server.MapPath(@"~\_accounts\" + AsentiaSessionState.IdAccount + @"\License.html"), fullHtml);

            this._LicenseAgreementHtml.InnerHtml = fullHtml;
            this.LicenseAgreementPanel.Update();

            this._LicenseAgreementEditorModal.SubmitButton.CssClass += " DisabledButton";
            this._LicenseAgreementEditorModal.SubmitButton.Enabled = false;
            this._LicenseAgreementEditorModal.DisplayFeedback(_GlobalResources.TheLicenseAgreementHasBeenSavedSuccessfully, false);
        }
        #endregion
    }
}
