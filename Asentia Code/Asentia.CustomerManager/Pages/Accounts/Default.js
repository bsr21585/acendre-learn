﻿function UpdateAccountStatus(idAccount, isActive) {
    
    if (isActive == "True") { // if isActive is true, then change the status to 0 (false)
        isActive = '0';
        $('#UpdateStatusModalBody1').html("Are you sure you want to disable this account?");
    } else { // if isActive is false, then change the status to 1 (true)
        isActive = '1';
        $('#UpdateStatusModalBody1').html("Are you sure you want to enable this account?");
    }

    // update hidden field with account and status values
    $('#UpdateStatusDataHiddenField').val(idAccount + '|' + isActive);

    // click the hidden button to activate modal
    $('#UpdateStatusModalHiddenButton').click();
}