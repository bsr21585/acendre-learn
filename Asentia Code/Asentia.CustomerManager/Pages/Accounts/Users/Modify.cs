﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace Asentia.CustomerManager.Pages.Accounts.Users
{
    public class Modify : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel ActionsPanel;
        public Panel PageInstructionsPanel;
        public Panel UserFormContainer;
        #endregion

        #region Private Properties
        private Library.AccountUser _AccountUserObject;

        private Button _SaveButton = new Button();
        private Button _CancelButton = new Button();

        private TextBox _FirstName;
        private TextBox _LastName;
        private TextBox _Email;
        private TextBox _Password;
        private TextBox _ConfirmPassword;
        private TextBox _UserName;

        private DropDownList _Status;

        private const string _RegexPatternForEmailId = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            //  Get account user's details
            this._GetUserObject();

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.AddOrModifyThisUsersPropertiesUsingTheFormBelow, true);

            // build the user form
            this._BuildUserForm();

            // build the form actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetUserObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._AccountUserObject = new Library.AccountUser(id); }
                }
                catch
                { Response.Redirect("~/accounts/users"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            string breadCrumbPageTitle;
            string pageTitle;
            if (this._AccountUserObject != null)
            {
                breadCrumbPageTitle = this._AccountUserObject.DisplayName;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewUser;

            }
            pageTitle = breadCrumbPageTitle;

            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AccountUsers, "~/accounts/users/"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));

            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";

            if (this._AccountUserObject == null)
            { this._SaveButton.Text = _GlobalResources.NewAccount; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }

        #endregion

        #region _BuildUserForm
        /// <summary>
        /// Builds the user form.
        /// </summary>
        private void _BuildUserForm()
        {
            //First name feild
            this._FirstName = new TextBox();
            this._FirstName.ID = "FirstName" + "_Field";
            this._FirstName.CssClass = "InputMedium";

            this.UserFormContainer.Controls.Add(BuildFormField("FirstName",
                                                                _GlobalResources.FirstName,
                                                                _FirstName.ID,
                                                                _FirstName,
                                                                true,
                                                                true));

            //Last name feild
            this._LastName = new TextBox();
            this._LastName.ID = "LastName" + "_Field";
            this._LastName.CssClass = "InputMedium";

            this.UserFormContainer.Controls.Add(BuildFormField("LastName",
                                                                _GlobalResources.LastName,
                                                                _LastName.ID,
                                                                _LastName,
                                                                true,
                                                                true));
            //Email feild
            this._Email = new TextBox();
            this._Email.ID = "Email" + "_Field";
            this._Email.CssClass = "InputMedium";

            this.UserFormContainer.Controls.Add(BuildFormField("Email",
                                                                _GlobalResources.Email,
                                                                _Email.ID,
                                                                _Email,
                                                                true,
                                                                true));
            //Username feild
            this._UserName = new TextBox();
            this._UserName.ID = "Username" + "_Field";
            this._UserName.CssClass = "InputMedium";

            this.UserFormContainer.Controls.Add(BuildFormField("Username",
                                                                _GlobalResources.Username,
                                                                _UserName.ID,
                                                                _UserName,
                                                                true,
                                                                true));

            //Password feild

            #region passwordAndConfirmPassword
            // merging two controls password and Confirm password in a panel

            Panel userFieldInputContainer = new Panel();
            userFieldInputContainer.ID = "Password" + "_InputContainer";
            userFieldInputContainer.CssClass = "FormFieldInputContainer";

            //password field
            this._Password = new TextBox();
            this._Password.ID = "Password" + "_Field";
            this._Password.TextMode = TextBoxMode.Password;
            this._Password.CssClass = "Password" + "_Field";

            userFieldInputContainer.Controls.Add(this._Password);

            // label
            Panel userFieldLabelContainer = new Panel();
            userFieldLabelContainer.ID = "Password" + "_LabelContainer";
            userFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label userFieldLabel = new Label();
            userFieldLabel.Text = _GlobalResources.Password + ":";
            userFieldLabel.AssociatedControlID = "Password" + "_Field";

            userFieldLabelContainer.Controls.Add(userFieldLabel);

            // Asterisk label field
            Label requiredAsterisk = new Label();
            requiredAsterisk.Text = " * ";
            requiredAsterisk.CssClass = "RequiredAsterisk";
            userFieldLabelContainer.Controls.Add(requiredAsterisk);

            // error panel
            Panel userFieldErrorContainer = new Panel();
            userFieldErrorContainer.ID = "Password" + "_ErrorContainer";
            userFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to container
            this.UserFormContainer.Controls.Add(userFieldLabelContainer);
            this.UserFormContainer.Controls.Add(userFieldErrorContainer);
            this.UserFormContainer.Controls.Add(userFieldInputContainer);

            // field
            Panel confirmPasswordInputContainer = new Panel();
            confirmPasswordInputContainer.ID = "Password" + "Confirm_InputContainer";
            confirmPasswordInputContainer.CssClass = "FormFieldInputContainer";
            
            //Password confirmation field
            this._ConfirmPassword = new TextBox();
            this._ConfirmPassword.ID = "Password" + "Confirm_Field";
            this._ConfirmPassword.TextMode = TextBoxMode.Password;
            this._ConfirmPassword.CssClass = "password_Field";

            confirmPasswordInputContainer.Controls.Add(this._ConfirmPassword);

            // label
            Panel confirmPasswordLabelContainer = new Panel();
            confirmPasswordLabelContainer.ID = "Password" + "Confirm_LabelContainer";
            confirmPasswordLabelContainer.CssClass = "FormFieldLabelContainer";

            Label confirmPasswordLabel = new Label();
            confirmPasswordLabel.Text = _GlobalResources.ConfirmByEnteringAgain + ":";
            confirmPasswordLabel.AssociatedControlID = "Password" + "Confirm_Field";
            confirmPasswordLabelContainer.Controls.Add(confirmPasswordLabel);

            Panel deviderPanel = new Panel();
            deviderPanel.CssClass = "FormFieldContainer";

            // add controls to container
            this.UserFormContainer.Controls.Add(confirmPasswordLabelContainer);
            this.UserFormContainer.Controls.Add(confirmPasswordInputContainer);
            this.UserFormContainer.Controls.Add(deviderPanel);

            #endregion

            //Is Active feild
            this._Status = new DropDownList();
            this._Status.ID = "IsActive";
            this._Status.CssClass = "InputMedium";
            this._Status.Items.Add(new ListItem(_GlobalResources.Enabled, "True"));
            this._Status.Items.Add(new ListItem(_GlobalResources.Disabled, "False"));

            this.UserFormContainer.Controls.Add(BuildFormField("IsActive",
                                                                _GlobalResources.Status,
                                                                _Status.ID,
                                                                _Status,
                                                                false,
                                                                false));

            this._PopulateInputControls();
        }
        #endregion

        #region _PopulateInputControls
        /// <summary>
        /// Populates all input controls
        /// </summary>
        private void _PopulateInputControls()
        {
            if (this._AccountUserObject != null)
            {
                this._FirstName.Text = this._AccountUserObject.FirstName;
                this._LastName.Text = this._AccountUserObject.LastName;
                this._Email.Text = this._AccountUserObject.Email;
                this._UserName.Text = this._AccountUserObject.Username;
                this._Status.SelectedValue = Convert.ToString(this._AccountUserObject.IsActive);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/accounts/users");
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click, Save/update the new/existing user details.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // perform validation on the user data
                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                // if there is no user object, create one
                if (this._AccountUserObject == null)
                { this._AccountUserObject = new Library.AccountUser(); }

                // populate the object
                this._AccountUserObject.IdAccount = AsentiaSessionState.IdAccount;
                this._AccountUserObject.FirstName = this._FirstName.Text;
                this._AccountUserObject.LastName = this._LastName.Text;
                this._AccountUserObject.Email = this._Email.Text;
                this._AccountUserObject.Username = this._UserName.Text;
                this._AccountUserObject.Password = this._Password.Text.Trim();
                this._AccountUserObject.IsActive = Convert.ToBoolean(this._Status.SelectedValue);
                this._AccountUserObject.IdRole = Convert.ToInt32(Library.AccountUser.RoleType.AccountUser);

                //Save
                int idAccountUser = this._AccountUserObject.Save();
                this.ViewState["id"] = idAccountUser;

                this._AccountUserObject = new Library.AccountUser(idAccountUser);

                // display the success message
                this.DisplayFeedback(_GlobalResources.UserProfileHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;
            // Package Name field
            if (String.IsNullOrWhiteSpace(this._FirstName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "FirstName", _GlobalResources.FirstName + " " + _GlobalResources.IsRequired);
            }

            if (String.IsNullOrWhiteSpace(this._LastName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "LastName", _GlobalResources.LastName + " " + _GlobalResources.IsRequired);
            }

            if (String.IsNullOrWhiteSpace(this._Email.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "Email", _GlobalResources.Email + " " + _GlobalResources.IsRequired);
            }
            //EMAIL ADDRESS SHOULD BE VALID
            else
            {
                if (!Regex.IsMatch(this._Email.Text.Trim(), _RegexPatternForEmailId))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "Email", _GlobalResources.ContactEmail + " " + _GlobalResources.IsInvalid);
                }
            }

            if (String.IsNullOrWhiteSpace(this._UserName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "Username", _GlobalResources.Username + " " + _GlobalResources.IsRequired);
            }

            if (String.IsNullOrWhiteSpace(this._Password.Text.Trim()))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "Password", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
            }

            if (String.IsNullOrWhiteSpace(this._ConfirmPassword.Text.Trim()) || this._ConfirmPassword.Text.Trim() != this._Password.Text.Trim())
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "Password", _GlobalResources.Password + " " + _GlobalResources.MustMatchConfirmationField);
            }

            return isValid;
        }
        #endregion
    }
}
