﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace Asentia.CustomerManager.Pages.Users
{
    public class Modify : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel ActionsPanel;
        public Panel PageInstructionsPanel;
        public Panel UserFormContainer;
        #endregion

        #region Private Properties
        private Library.AccountUser _SystemUserObject;

        private Button _SaveButton = new Button();
        private Button _CancelButton = new Button();

        private TextBox _FirstName;
        private TextBox _LastName;
        private TextBox _Email;
        private TextBox _Password;
        private TextBox _ConfirmPassword;
        private TextBox _UserName;

        private RadioButtonList _Status;

        private const string _RegexPatternForEmailId = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            //  Get account user's details
            this._GetUserObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this._BuildControls();

            // build the form actions panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetUserObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._SystemUserObject = new Library.AccountUser(id); }
                }
                catch
                { Response.Redirect("~/accounts/users"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string siteImagePath;
            string pageTitle;

            if (this._SystemUserObject != null)
            {
                string siteTitleInInterfaceLanguage = this._SystemUserObject.Username;

                breadCrumbPageTitle = siteTitleInInterfaceLanguage;

                pageTitle = siteTitleInInterfaceLanguage;

                siteImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                         ImageFiles.EXT_PNG, true);
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewUser;

                siteImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                         ImageFiles.EXT_PNG, true);
                pageTitle = _GlobalResources.NewUser;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();

            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/Users"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "FormActionsPanel";

            // save button
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";

            if (this._SystemUserObject == null)
            { this._SaveButton.Text = _GlobalResources.NewAccount; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }

        #endregion

        #region _BuildControls
        /// <summary>
        /// builds page controls
        /// </summary>
        private void _BuildControls()
        {
            this.UserFormContainer.Controls.Clear();

            // build the breadcrumb
            this._BuildBreadcrumbAndPageTitle();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.AddOrModifyThisUsersPropertiesUsingTheFormBelow, true);

            // build the user form
            this._BuildUserForm();
        }
        #endregion

        #region _BuildUserForm
        /// <summary>
        /// Builds the user form.
        /// </summary>
        private void _BuildUserForm()
        {
            //First name feild
            this._FirstName = new TextBox();
            this._FirstName.ID = "FirstName" + "_Field";
            this._FirstName.CssClass = "InputMedium";

            this.UserFormContainer.Controls.Add(BuildFormField("FirstName",
                                                                _GlobalResources.FirstName,
                                                                _FirstName.ID,
                                                                _FirstName,
                                                                true,
                                                                true));
            //Last name feild
            this._LastName = new TextBox();
            this._LastName.ID = "LastName" + "_Field";
            this._LastName.CssClass = "InputMedium";

            this.UserFormContainer.Controls.Add(BuildFormField("LastName",
                                                                _GlobalResources.LastName,
                                                                _LastName.ID,
                                                                _LastName,
                                                                true,
                                                                true));
            //Email feild
            this._Email = new TextBox();
            this._Email.ID = "Email" + "_Field";
            this._Email.CssClass = "InputMedium";

            this.UserFormContainer.Controls.Add(BuildFormField("Email",
                                                                _GlobalResources.Email,
                                                               this._Email.ID,
                                                               this._Email,
                                                                true,
                                                                true));
            //Username feild
            this._UserName = new TextBox();
            this._UserName.ID = "Username" + "_Field";
            this._UserName.CssClass = "InputMedium";

            this.UserFormContainer.Controls.Add(BuildFormField("Username",
                                                                _GlobalResources.Username,
                                                               this._UserName.ID,
                                                               this._UserName,
                                                                true,
                                                                true));

            //Password feild
            this._Password = new TextBox();
            this._Password.ID = "Password" + "_Field";
            this._Password.CssClass = "InputMedium";
            this._Password.TextMode = TextBoxMode.Password;

            this.UserFormContainer.Controls.Add(BuildFormField("Password",
                                                                _GlobalResources.Password,
                                                               this._Password.ID,
                                                                this._Password,
                                                                false,
                                                                true));

            //Password confirmation feild
            this._ConfirmPassword = new TextBox();
            this._ConfirmPassword.ID = "PasswordConfirm" + "_Field";
            this._ConfirmPassword.CssClass = "InputMedium";
            this._ConfirmPassword.TextMode = TextBoxMode.Password;

            this.UserFormContainer.Controls.Add(BuildFormField("PasswordConfirm",
                                                                _GlobalResources.ConfirmByEnteringAgain,
                                                                 this._ConfirmPassword.ID,
                                                                 this._ConfirmPassword,
                                                                false,
                                                                true));

            //Status feild
            this._Status = new RadioButtonList();
            this._Status.ID = "Status" + "_Field"; ;
            this._Status.RepeatDirection = RepeatDirection.Horizontal;

            this._Status.Items.Add(new ListItem(_GlobalResources.Enabled, "True"));
            this._Status.Items.Add(new ListItem(_GlobalResources.Disabled, "False"));

            this.UserFormContainer.Controls.Add(BuildFormField("Status",
                                                                _GlobalResources.Status,
                                                                 this._Status.ID,
                                                                 this._Status,
                                                                false,
                                                                false));
            this._PopulateInputControls();
        }
        #endregion

        #region _PopulateInputControls
        /// <summary>
        /// Populates all input controls
        /// </summary>
        private void _PopulateInputControls()
        {
            if (this._SystemUserObject != null)
            {
                this._FirstName.Text = this._SystemUserObject.FirstName;
                this._LastName.Text = this._SystemUserObject.LastName;
                this._Email.Text = this._SystemUserObject.Email;
                this._UserName.Text = this._SystemUserObject.Username;
                this._Status.SelectedValue = Convert.ToString(this._SystemUserObject.IsActive);
            }
            else
            {
                this._Status.SelectedValue = "True";
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/users");
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click, Save/update the new/existing user details.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // perform validation on the user data
                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                // if there is no user object, create one
                if (this._SystemUserObject == null)
                { this._SystemUserObject = new Library.AccountUser(); }

                // populate the object
                this._SystemUserObject.IdAccount = AsentiaSessionState.IdAccount;
                this._SystemUserObject.FirstName = this._FirstName.Text;
                this._SystemUserObject.LastName = this._LastName.Text;
                this._SystemUserObject.Email = this._Email.Text;
                this._SystemUserObject.Username = this._UserName.Text;
                this._SystemUserObject.Password = this._Password.Text;
                this._SystemUserObject.IsActive = Convert.ToBoolean(this._Status.SelectedValue);
                this._SystemUserObject.IdRole = Convert.ToInt32(Library.AccountUser.RoleType.SystemUser);

                //Save
                int idAccountUser = this._SystemUserObject.Save();
                this.ViewState["id"] = idAccountUser;

                this._SystemUserObject = new Library.AccountUser(idAccountUser);

                this._BuildControls();

                // display the success message
                this.DisplayFeedback(_GlobalResources.UserProfileHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;
            // Package Name field
            if (String.IsNullOrWhiteSpace(this._FirstName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "FirstName", _GlobalResources.FirstName + " " + _GlobalResources.IsRequired);
            }

            if (String.IsNullOrWhiteSpace(this._LastName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "LastName", _GlobalResources.LastName + " " + _GlobalResources.IsRequired);
            }

            if (String.IsNullOrWhiteSpace(this._Email.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "Email", _GlobalResources.Email + " " + _GlobalResources.IsRequired);
            }
            //EMAIL ADDRESS SHOULD BE VALID
            else if (!String.IsNullOrWhiteSpace(this._Email.Text))
            {
                if (!Regex.IsMatch(this._Email.Text.Trim(), _RegexPatternForEmailId))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "Email", _GlobalResources.ContactEmail + " " + _GlobalResources.IsInvalid);
                }
            }

            if (String.IsNullOrWhiteSpace(this._UserName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "Username", _GlobalResources.Username + " " + _GlobalResources.IsRequired);
            }

            if (this._SystemUserObject == null)
            {
                if (String.IsNullOrWhiteSpace(this._Password.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "Password", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
                }
                if (String.IsNullOrWhiteSpace(this._ConfirmPassword.Text) || this._ConfirmPassword.Text != this._Password.Text)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.UserFormContainer, "PasswordConfirm", _GlobalResources.MustMatchConfirmationField);
                }
            }

            return isValid;
        }
        #endregion
    }
}
