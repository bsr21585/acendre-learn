﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.CustomerManager.Library;

namespace Asentia.CustomerManager.Pages.Users
{
    public class Default : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public UpdatePanel UserGridUpdatePanel;
        public Grid UserGrid;
        public Panel GridActionsPanel;

        public ModalPopup UpdateStatusModal;
        public HiddenField UpdateStatusDataHiddenField;
        public Button UpdateStatusModalHiddenButton;
        #endregion

        #region Private Properties
        private LinkButton _DeleteButton = new LinkButton();
        private ModalPopup GridConfirmAction;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            base.OnPreRender(e);

            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.CustomerManager.Pages.Users.Default.js");
        }
        #endregion
        
        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(_GlobalResources.Users, ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                              ImageFiles.EXT_PNG,
                                                                              true));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the grid, actions panel, and modal
            this._BuildGrid();
            this._BuildGridActionsPanel();
            this._BuildGridActionsModal();
            this._BuildObjectOptionsPanel();
            this._BuildUpdateStatusModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.UserGrid.BindData();
            }
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD USER
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddUserLink",
                                                null,
                                                "Modify.aspx",
                                                null,
                                                _GlobalResources.NewUser,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG, true),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG, true))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.UserGrid = new Grid("CustomerManager");
            this.UserGrid.ID = "UserGrid";
            this.UserGrid.OverrideLoadImagesFromCustomerManager = true;
            this.UserGrid.DBType = DatabaseType.CustomerManager;
            this.UserGrid.StoredProcedure = Library.AccountUser.GridProcedure;
            this.UserGrid.AddFilter("@idCallerAccount", SqlDbType.Int, 4, AsentiaSessionState.IdAccount);
            this.UserGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdAccountUser);
            this.UserGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.UserGrid.IdentifierField = "idAccountUser";
            this.UserGrid.DefaultSortColumn = "displayName";

            // data key names
            this.UserGrid.DataKeyNames = new string[] { "idAccountUser", "displayName", "username", "email", "isActive" };

            // columns
            GridColumn displayName = new GridColumn(_GlobalResources.Name, "displayName", "displayName");

            GridColumn isActive = new GridColumn(_GlobalResources.Status, "isActive", true);

            // add columns to data grid
            this.UserGrid.AddColumn(displayName);
            this.UserGrid.AddColumn(isActive);

            this.UserGrid.RowDataBound += AccountUserGrid_RowDataBound;

            // bind the grid to the update panel
            this.UserGridUpdatePanel.ContentTemplateContainer.Controls.Add(this.UserGrid);
        }
        #endregion

        #region AccountUserGrid_RowDataBound
        /// <summary>
        /// Row Data Bound event handler for account user grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void AccountUserGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                bool isActive = Convert.ToBoolean(rowView["isActive"]);
                int idAccountUser = Convert.ToInt32(rowView["idAccountUser"]);
                string displayName = rowView["displayName"].ToString();
                string username = rowView["username"].ToString();
                string email = rowView["email"].ToString();


                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG, true);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = displayName;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // display nmae
                Label nameLinkLabel = new Label();
                nameLinkLabel.CssClass = "GridBaseTitle";

                HyperLink nameLink = new HyperLink();
                nameLink.Text = displayName;
                nameLink.NavigateUrl = "Modify.aspx?id=" + idAccountUser.ToString();
                nameLinkLabel.Controls.Add(nameLink);

                // username
                Label usernameLabel = new Label();
                usernameLabel.Text = " (" + username + ")";
                usernameLabel.CssClass = "GridSecondaryTitle";
                nameLinkLabel.Controls.Add(usernameLabel);
                e.Row.Cells[1].Controls.Add(nameLinkLabel);

                // email
                Label emailLabel = new Label();
                emailLabel.CssClass = "GridSecondaryLine";

                HyperLink emailLink = new HyperLink();
                emailLink.Text = email;
                emailLink.NavigateUrl = "mailto:" + email;
                emailLabel.Controls.Add(emailLink);
                e.Row.Cells[1].Controls.Add(emailLabel);

                Label statusSpan = new Label();
                statusSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(statusSpan);

                LinkButton statusLinkButton = new LinkButton();
                statusLinkButton.ID = "StatusLinkButton";
                statusLinkButton.OnClientClick = "UpdateUserStatus('" + idAccountUser.ToString() + "', '" + isActive.ToString() + "'); return false;";

                Image statusImage = new Image();
                statusImage.ID = "StatusImage";
                statusImage.CssClass = "SmallIcon";

                // set the appropriate link image based on the site being active or not
                if (isActive)
                {
                    statusLinkButton.ToolTip = _GlobalResources.DisableAccountUser;
                    statusImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN,
                                            ImageFiles.EXT_PNG,
                                            true);

                }
                else
                {
                    statusLinkButton.ToolTip = _GlobalResources.EnableAccountUser;
                    statusImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED,
                                            ImageFiles.EXT_PNG,
                                            true);
                }

                statusLinkButton.Controls.Add(statusImage);
                statusSpan.Controls.Add(statusLinkButton);



            }

        }
        #endregion

        #region _BuildGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsPanel()
        {
            // delete button
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG,
                                                          true);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedUser_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.GridActionsPanel.Controls.Add(this._DeleteButton);

            // hidden update status data
            this.UpdateStatusDataHiddenField = new HiddenField();
            this.UpdateStatusDataHiddenField.ID = "UpdateStatusDataHiddenField";

            // add hidden status data to page
            this.GridActionsPanel.Controls.Add(this.UpdateStatusDataHiddenField);

            // hidden button to trigger update status modal
            this.UpdateStatusModalHiddenButton = new Button();
            this.UpdateStatusModalHiddenButton.ID = "UpdateStatusModalHiddenButton";
            this.UpdateStatusModalHiddenButton.Style.Add("display", "none");

            // add hidden button to page
            this.GridActionsPanel.Controls.Add(this.UpdateStatusModalHiddenButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.OverrideLoadImagesFromCustomerManager = true;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedUser_s;
            this.GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl bodyWrapper = new HtmlGenericControl("p");
            bodyWrapper.ID = "GridConfirmActionModalBody1";
            Literal body = new Literal();
            body.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseUser_s;
            bodyWrapper.Controls.Add(body);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(bodyWrapper);

            this.GridActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.UserGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.UserGrid.Rows[i].FindControl(this.UserGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Asentia.CustomerManager.Library.AccountUser.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedback(_GlobalResources.TheSelectedUser_sHaveBeenDeletedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.UserGrid.BindData();
            }
        }
        #endregion

        #region _BuildUpdateStatusModal
        /// <summary>
        /// Builds the modal for updating the status of an account user.
        /// </summary>
        private void _BuildUpdateStatusModal()
        {
            this.UpdateStatusModal = new ModalPopup();
            this.UpdateStatusModal.ID = "UpdateStatusModal";

            // set modal properties
            this.UpdateStatusModal.Type = ModalPopupType.Confirm;
            this.UpdateStatusModal.OverrideLoadImagesFromCustomerManager = true;
            this.UpdateStatusModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN,
                                                                           ImageFiles.EXT_PNG,
                                                                           true);
            this.UpdateStatusModal.HeaderIconAlt = _GlobalResources.UpdateStatus;
            this.UpdateStatusModal.HeaderText = _GlobalResources.UpdateStatus;
            this.UpdateStatusModal.TargetControlID = this.UpdateStatusModalHiddenButton.ID;
            this.UpdateStatusModal.SubmitButton.Command += new CommandEventHandler(this._UpdateStatus_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            Literal body2 = new Literal();
            body1Wrapper.ID = "UpdateStatusModalBody1";

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.UpdateStatusModal.AddControlToBody(body1Wrapper);

            this.GridActionsPanel.Controls.Add(this.UpdateStatusModal);
        }
        #endregion

        #region _UpdateStatus_Command
        /// <summary>
        /// Updates the status of an account user.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _UpdateStatus_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string[] data = this.UpdateStatusDataHiddenField.Value.Split('|');
                int idAccountUser = Convert.ToInt32(data[0]);
                int isActive = Convert.ToInt32(data[1]);

                // update the status
                AccountUser user = new AccountUser(idAccountUser);
                if (isActive == 1)
                {
                    user.IsActive = true;
                }
                else
                {
                    user.IsActive = false;
                }
                
                user.Save();

                // display the success message
                if (isActive == 1)
                {
                    this.DisplayFeedback(_GlobalResources.TheUserHasBeenEnabledSuccessfully, false);
                }
                else
                {
                    this.DisplayFeedback(_GlobalResources.TheUserHasBeenDisabledSuccessfully, false);
                }

                // rebind the grid
                this.UserGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this.UserGrid.BindData();
            }
        }
        #endregion
    }
}
