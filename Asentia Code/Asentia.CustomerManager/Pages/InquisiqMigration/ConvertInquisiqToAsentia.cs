﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using defaultSystem = System;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Services = System.Web.Services;
using ScriptServices = System.Web.Script.Services;

namespace Asentia.CustomerManager.Pages.InquisiqMigration
{
    [Services.WebService(Namespace = "http://default.asentia.com/ConvertInquisiqToAsentia")]
    [Services.WebServiceBinding(ConformsTo = Services.WsiProfiles.BasicProfile1_1)]
    [ScriptServices.ScriptService]
    public class ConvertInquisiqToAsentia : Services.WebService
    {
        #region ConvertSite
        /// <summary>
        /// Converts the site data from Inquisiq to Asentia
        /// </summary>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        /// <returns>source site ID and destination site ID</returns>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public Tuple<int, int> ConvertSite(string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", null, SqlDbType.Int, 4, ParameterDirection.Output);

            // Asentia Parametemrs
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia.Site]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return new Tuple<int, int>(
                    AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSiteSource"].Value),
                    AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSiteDestination"].Value));


            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ConvertUser
        /// <summary>
        /// Converts the user and group data from Inquisiq to Asentia
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void ConvertUser(int idSiteSource, int idSiteDestination, string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);

            // Asentia Parameters
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia.User]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ConvertCourse
        /// <summary>
        /// Converts the course, lesson, ILT, and catalog data from Inquisiq to Asentia
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void ConvertCourse(int idSiteSource, int idSiteDestination, string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);

            // Asentia Parametemrs
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia.Course]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ConvertContent
        /// <summary>
        /// Converts the content and quiz survey data from Inquisiq to Asentia
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        /// <param name="useNewContentPath">Should we use the new content path from the May 2018 Inquisiq update?</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void ConvertContent(int idSiteSource, int idSiteDestination, string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName, bool useNewContentPath)
        {
            
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);

            // Asentia Parametemrs
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@useNewContentPath", useNewContentPath, SqlDbType.Bit, 1, ParameterDirection.Input);            

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia.Content]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ConvertCertificate
        /// <summary>
        /// Converts the certificate data from Inquisiq to Asentia
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void ConvertCertificate(int idSiteSource, int idSiteDestination, string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);

            // Asentia Parametemrs
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia.Certificate]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ConvertActivityImportEmail
        /// <summary>
        /// Converts the activity import and email notification data from Inquisiq to Asentia
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void ConvertActivityImportEmail(int idSiteSource, int idSiteDestination, string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);

            // Asentia Parametemrs
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia.ActivityImportEmail]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ConvertCouponCode
        /// <summary>
        /// Converts the coupon code data from Inquisiq to Asentia
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void ConvertCouponCode(int idSiteSource, int idSiteDestination, string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);

            // Asentia Parametemrs
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia.CouponCode]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ConvertLinkingTables
        /// <summary>
        /// Converts the linking tables data from Inquisiq to Asentia
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void ConvertLinkingTables(int idSiteSource, int idSiteDestination, string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);

            // Asentia Parametemrs
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia.LinkingTables]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ConvertEnrollment
        /// <summary>
        /// Converts the enrollment data from Inquisiq to Asentia
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void ConvertEnrollment(int idSiteSource, int idSiteDestination, string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);

            // Asentia Parametemrs
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia.Enrollment]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ConvertReport
        /// <summary>
        /// Converts the report data from Inquisiq to Asentia
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void ConvertReport(int idSiteSource, int idSiteDestination, string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);

            // Asentia Parametemrs
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia.Report]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region CopyCourseMaterial
        /// <summary>
        /// Copies course material from Inquisiq to Asentia
        /// </summary>
        /// <param name="sourceFolderInput">Inquisiq folder</param>
        /// <param name="destinationFolderInput">Asentia folder</param>
        /// <param name="sourceSiteHostname">Inquisiq hostname</param>
        /// <param name="destinationSiteHostname">Asentia hostname</param>
        /// <param name="destinationDBServer">Asentia DB Server name</param>
        /// <param name="destinationDBName">Asentia DB Name</param>
        /// <param name="idSiteDestination">Asentia site ID</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyCourseMaterial(string sourceFolderInput, string destinationFolderInput, string sourceSiteHostname, string destinationSiteHostname, string destinationDBServer, string destinationDBName, int idSiteDestination)
        {
            
            this._DropProcedures(destinationDBServer, destinationDBName);

            // Create stored procedures to get course IDs for course material and course screenshots
            this._CreateProcedures(destinationDBServer, destinationDBName);

            // Inquisiq Web Folder - Strip out trailing slash if it exists
            string sourceFolder = sourceFolderInput.Substring(
                sourceFolderInput.Length - 1, 1) != @"\" ?
                sourceFolderInput :
                sourceFolderInput.Substring(0, sourceFolderInput.Length - 1);

            // Asentia Destination Site Folder - Strip out trailing slash if it exists
            string destinationFolder = destinationFolderInput.Substring(
                destinationFolderInput.Length - 1, 1) != @"\" ?
                destinationFolderInput :
                destinationFolderInput.Substring(0, destinationFolderInput.Length - 1);

            // Inquisiq course material
            string sourceWarehouse = sourceFolder + @"\warehouse"; // warehouse folder
            string sourceCourseMaterials = sourceWarehouse + @"\coursematerials\" + sourceSiteHostname; // course material

            // Asentia course material
            string destinationWarehouse = destinationFolder + @"\warehouse"; // warehouse folder
            string destinationCourseMaterials = destinationWarehouse + @"\" + destinationSiteHostname + @"\documents\course"; // course material
            string destinationSiteFolder = destinationFolder + @"\_config\" + destinationSiteHostname;

            // Creates a directory for the current hostname under _config
            Directory.CreateDirectory(destinationSiteFolder);

            // course material copy
            FileInfo[] files = null;
            DirectoryInfo sourceDir = new DirectoryInfo(sourceCourseMaterials);
            int courseId;
            string newDestinationCourseMaterials;
            files = sourceDir.GetFiles("*.*");

            // Iterate through all course material and put them under the proper course ID folder
            foreach (FileInfo f in files)
            {
                // get the course ID for the current file
                courseId = this._GetCourseIDForCourseMaterial(f.Name, idSiteDestination, destinationDBServer, destinationDBName);

                // If the course ID is not zero (an entry for the file exists in the database),
                // create a subfolder for that course ID and move the file into it.
                if (courseId != 0)
                {
                    newDestinationCourseMaterials = destinationCourseMaterials + @"\" + courseId.ToString();
                    Directory.CreateDirectory(newDestinationCourseMaterials);
                    File.Copy(sourceCourseMaterials + @"\" + f.Name, newDestinationCourseMaterials + @"\" + f.Name, true);
                }

            }
            
        }
        #endregion

        #region CopyCourseScreenshots
        /// <summary>
        /// Copies course screenshots from Inquisiq to Asentia
        /// </summary>
        /// <param name="sourceFolderInput">Inquisiq folder</param>
        /// <param name="destinationFolderInput">Asentia folder</param>
        /// <param name="sourceSiteHostname">Inquisiq site hostname</param>
        /// <param name="destinationSiteHostname">Asentia site hostname</param>
        /// <param name="destinationDBServer">Asentia DB Server Name</param>
        /// <param name="destinationDBName">Asentia DB name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyCourseScreenshots(string sourceFolderInput, string destinationFolderInput, string sourceSiteHostname, string destinationSiteHostname, string destinationDBServer, string destinationDBName)
        {
            // Inquisiq Web Folder - Strip out trailing slash if it exists
            string sourceFolder = sourceFolderInput.Substring(
                sourceFolderInput.Length - 1, 1) != @"\" ?
                sourceFolderInput :
                sourceFolderInput.Substring(0, sourceFolderInput.Length - 1);

            // Asentia Destination Site Folder - Strip out trailing slash if it exists
            string destinationFolder = destinationFolderInput.Substring(
                destinationFolderInput.Length - 1, 1) != @"\" ?
                destinationFolderInput :
                destinationFolderInput.Substring(0, destinationFolderInput.Length - 1);

            // Inquisiq
            string sourceWarehouse = sourceFolder + @"\warehouse"; // content packages
            string sourceCourseScreenshots = sourceFolder + @"\_config\" + sourceSiteHostname + @"\_images\samples"; // course screenshots

            // Asentia
            string destinationWarehouse = destinationFolder + @"\warehouse"; // content packages
            string destinationSiteFolder = destinationFolder + @"\_config\" + destinationSiteHostname;
            string destinationCourseScreenshots = destinationSiteFolder + @"\courses"; // course screenshots

            // course screenshot copy
            DirectoryInfo[] directories = null;
            DirectoryInfo sourceDir = new DirectoryInfo(sourceCourseScreenshots);
            int courseId;
            string newDestinationCourseScreenshots;
            string newSourceCourseScreenshots;
            directories = sourceDir.GetDirectories();

            // Iterate through all course screenshots and put them under the proper course ID folder
            foreach (DirectoryInfo dir in directories)
            {
                // get the course ID for the current file
                courseId = this._GetCourseIDForCourseScreenshots(Convert.ToInt32(dir.Name), destinationDBServer, destinationDBName);

                // If the course ID is not zero, create a subfolder for that course ID and move the files into it.
                if (courseId != 0)
                {
                    newDestinationCourseScreenshots = destinationCourseScreenshots + @"\" + courseId.ToString() + @"\samplescreens";
                    newSourceCourseScreenshots = sourceCourseScreenshots + @"\" + dir.Name;
                    Utility.CopyDirectory(newSourceCourseScreenshots, newDestinationCourseScreenshots, true, false);
                }

            }
            
        }
        #endregion

        #region CopyCertificates
        /// <summary>
        /// Copies certificates from Inquisiq to Asentia
        /// </summary>
        /// <param name="sourceFolderInput">Inquisiq folder</param>
        /// <param name="destinationFolderInput">Asentia folder</param>
        /// <param name="sourceSiteHostname">Inquisiq site hostname</param>
        /// <param name="destinationSiteHostname">Asentia site hostname</param>
        /// <param name="destinationDBServer">Asentia DB Server name</param>
        /// <param name="destinationDBName">Asentia DB Name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyCertificates(string sourceFolderInput, string destinationFolderInput, string sourceSiteHostname, string destinationSiteHostname, string destinationDBServer, string destinationDBName)
        {
            
            // Inquisiq Web Folder - Strip out trailing slash if it exists
            string sourceFolder = sourceFolderInput.Substring(
                sourceFolderInput.Length - 1, 1) != @"\" ?
                sourceFolderInput :
                sourceFolderInput.Substring(0, sourceFolderInput.Length - 1);

            // Asentia Destination Site Folder - Strip out trailing slash if it exists
            string destinationFolder = destinationFolderInput.Substring(
                destinationFolderInput.Length - 1, 1) != @"\" ?
                destinationFolderInput :
                destinationFolderInput.Substring(0, destinationFolderInput.Length - 1);

            // Inquisiq
            string sourceWarehouse = sourceFolder + @"\warehouse"; // content packages
            string sourceCertificates = sourceFolder + @"\_config\" + sourceSiteHostname + @"\_images\certs"; // certificate image

            // Asentia
            string destinationWarehouse = destinationFolder + @"\warehouse"; // content packages
            string destinationSiteFolder = destinationFolder + @"\_config\" + destinationSiteHostname;
            string destinationCertificates = destinationSiteFolder + @"\certificates"; // certificate

            // certificate copy -- iterate through data set
            DataTable certificateData = this._GetCertificateData(destinationDBServer, destinationDBName);

            // make sure certificate data contains data
            if (certificateData != null)
            {
                string newDestinationCertificates;
                string certFilename;
                string certId;
                string certData;

                // Iterate through all certificates
                foreach (DataRow row in certificateData.Rows)
                {
                    // get the certificate info
                    certId = row["certId"].ToString();
                    certFilename = row["certFilename"].ToString();
                    certData = row["certData"].ToString();

                    newDestinationCertificates = destinationCertificates + @"\" + certId;
                    Directory.CreateDirectory(newDestinationCertificates);
                    // copy certification image file to Asentia from Inquisiq
                    File.Copy(sourceCertificates + @"\" + certFilename, newDestinationCertificates + @"\" + certFilename);
                    // write the certificate json data to Asentia
                    File.WriteAllText(newDestinationCertificates + @"\Certificate.json", certData);

                }
            }
            

        }
        #endregion

        #region CopyEmailNotifications
        /// <summary>
        /// Copies email notifications from Inquisiq to Asentia
        /// </summary>
        /// <param name="sourceFolderInput">Inquisiq folder</param>
        /// <param name="destinationFolderInput">Asentia folder</param>
        /// <param name="sourceSiteHostname">Inquisiq source site hostname</param>
        /// <param name="destinationSiteHostname">Asentia site hostname</param>
        /// <param name="destinationDBServer">Asentia DB Server name</param>
        /// <param name="destinationDBName">Asentia DB Name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyEmailNotifications(string sourceFolderInput, string destinationFolderInput, string sourceSiteHostname, string destinationSiteHostname, string destinationDBServer, string destinationDBName)
        {
            
            // Inquisiq Web Folder - Strip out trailing slash if it exists
            string sourceFolder = sourceFolderInput.Substring(
                sourceFolderInput.Length - 1, 1) != @"\" ?
                sourceFolderInput :
                sourceFolderInput.Substring(0, sourceFolderInput.Length - 1);

            // Asentia Destination Site Folder - Strip out trailing slash if it exists
            string destinationFolder = destinationFolderInput.Substring(
                destinationFolderInput.Length - 1, 1) != @"\" ?
                destinationFolderInput :
                destinationFolderInput.Substring(0, destinationFolderInput.Length - 1);

            // Inquisiq
            string sourceWarehouse = sourceFolder + @"\warehouse"; // content packages

            // Asentia
            string destinationWarehouse = destinationFolder + @"\warehouse"; // content packages
            string destinationSiteFolder = destinationFolder + @"\_config\" + destinationSiteHostname;
            string destinationEmailNotifications = destinationSiteFolder + @"\emailNotifications"; // email notifications

            // email notification copy - iterate through data set
            DataTable emailNotificationData = this._GetEmailNotificationData(destinationDBServer, destinationDBName);

            // make sure email notification data contains data
            if (emailNotificationData != null)
            {
                string newDestinationEmailNotifications;
                string emailId;
                string emailData;

                // Iterate through all email notifications
                foreach (DataRow row in emailNotificationData.Rows)
                {
                    // get the email notification info
                    emailId = row["emailId"].ToString();
                    emailData = row["emailData"].ToString();

                    newDestinationEmailNotifications = destinationEmailNotifications + @"\" + emailId;

                    // write the email data to Asentia
                    Directory.CreateDirectory(newDestinationEmailNotifications);
                    File.WriteAllText(newDestinationEmailNotifications + @"\EmailNotification.xml", emailData);

                }
            }
            
        }
        #endregion

        #region CopyContentPackages
        /// <summary>
        /// Copies content packages from Inquisiq to Asentia
        /// </summary>
        /// <param name="sourceFolderInput">Inquisiq folder</param>
        /// <param name="destinationFolderInput">Asentia folder</param>
        /// <param name="sourceSiteHostname">Inquisiq site hostname</param>
        /// <param name="destinationSiteHostname">Asentia site hostname</param>
        /// <param name="destinationDBServer">Asentia DB Server name</param>
        /// <param name="destinationDBName">Asentia DB name</param>
        /// <param name="idAccount">Account ID</param>
        /// <param name="idSiteSource">Inquisiq site ID</param>
        /// <param name="idSiteDestination">Asentia site ID</param>
        /// <param name="useNewContentPath">Should we use the new content path in the May 2018 Inquisiq Update?</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyContentPackages(string sourceFolderInput, string destinationFolderInput, string sourceSiteHostname, string destinationSiteHostname, string destinationDBServer, string destinationDBName, int idAccount, int idSiteSource, int idSiteDestination, bool useNewContentPath)
        {
            
            // Inquisiq Web Folder - Strip out trailing slash if it exists
            string sourceFolder = sourceFolderInput.Substring(
                sourceFolderInput.Length - 1, 1) != @"\" ?
                sourceFolderInput :
                sourceFolderInput.Substring(0, sourceFolderInput.Length - 1);

            // Asentia Destination Site Folder - Strip out trailing slash if it exists
            string destinationFolder = destinationFolderInput.Substring(
                destinationFolderInput.Length - 1, 1) != @"\" ?
                destinationFolderInput :
                destinationFolderInput.Substring(0, destinationFolderInput.Length - 1);
            
            // Inquisiq
            // check if we are using old or new content package path
            string sourceWarehouse = "";
            if (useNewContentPath)
            {
                sourceWarehouse = sourceFolder + @"\warehouse\sco\" + sourceSiteHostname; // new content package path
            }
            else
            {
                sourceWarehouse = sourceFolder + @"\warehouse"; // old content package path
            }
            
            // Asentia
            string destinationWarehouse = destinationFolder + @"\warehouse"; // content packages

            // Drop procedure
            this._DropProcedures(destinationDBServer, destinationDBName);

            // content package copy
            DirectoryInfo[] sourceContentDirectories = new DirectoryInfo(sourceWarehouse).GetDirectories();
            string newDirectoryName = "";
            if (useNewContentPath)
            {
                DataTable contentPackageDataTable = this._GetContentPackageData(idSiteDestination, destinationDBServer, destinationDBName);
                foreach (DirectoryInfo dirInfo in sourceContentDirectories)
                {
                    foreach (DataRow row in contentPackageDataTable.Rows)
                    {
                        // find the new directory name with the correct package name
                        if (row["path"].ToString().Contains("-" + dirInfo.Name))
                        {
                            newDirectoryName = destinationFolder + row["path"].ToString().Replace(@"/", @"\");
                        }
                    }
                    // copy directory
                    Utility.CopyDirectory(sourceWarehouse + @"\" + dirInfo.Name, newDirectoryName, true, true);// destinationWarehouse + @"\" + newDirectoryName, true, true);
                }
                
            }
            else
            {
                foreach (DirectoryInfo dirInfo in sourceContentDirectories)
                {
                    // Only copy directories that start with the source site id (if using the old warehouse content path)
                    if (dirInfo.ToString().StartsWith(idSiteSource.ToString()))
                    {
                        // create new directory name based on Asentia requirements and then copy the directory
                        Regex regEx = new Regex(Regex.Escape(idSiteSource + "-"));
                        newDirectoryName = regEx.Replace(dirInfo.Name, idAccount.ToString() + "-" + idSiteDestination.ToString() + "-", 1);

                        Utility.CopyDirectory(sourceWarehouse + @"\" + dirInfo.Name, destinationWarehouse + @"\" + newDirectoryName, true, true);
                    }
                }
            }
            
        }
        #endregion

        #region DeleteSite
        /// <summary>
        /// Deletes the site data from the database / filesystem if an error occurs during the migration
        /// </summary>
        /// <param name="idSite"> Asentia site ID</param>
        /// <param name="destinationFolderInput">Asentia folder</param>
        /// <param name="destinationSiteHostname">Asentia site hostname</param>
        /// <param name="idAccount">Account ID</param>
        /// <param name="databaseServer">Asentia Database Server Name</param>
        /// <param name="databaseName">Asentia Database Name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void DeleteSite(int idSite, string destinationFolderInput, string destinationSiteHostname, int idAccount, string databaseServer, string databaseName)
        {
            DataTable sites = new DataTable();
            sites.Columns.Add("id", typeof(int));
            sites.Rows.Add(idSite);

            // database object for deleting site data
            AsentiaDatabase databaseObject = new AsentiaDatabase(databaseServer, databaseName, null, null, "InquisiqToAsentiaMigration", 1140, true);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Sites", sites, SqlDbType.Structured, null, ParameterDirection.Input);

            // database object for deleting account to domain alias link
            AsentiaDatabase customerManagerDatabaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            customerManagerDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            customerManagerDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            customerManagerDatabaseObject.AddParameter("@idCallerAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            customerManagerDatabaseObject.AddParameter("@domainAliass", destinationSiteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                
                // delete site data
                databaseObject.ExecuteNonQuery("[Site.Delete]", true);
                
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // delete account to domain alias link
                customerManagerDatabaseObject.ExecuteNonQuery("[AccountToDomainAliasLink.Delete]", true);

                returnCode = (DBReturnValue)Convert.ToInt32(customerManagerDatabaseObject.Command.Parameters["@Return_Code"].Value);
                errorDescriptionCode = customerManagerDatabaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                
                // Asentia Destination Site Folder - Strip out trailing slash if it exists
                string destinationFolder = destinationFolderInput.Substring(
                    destinationFolderInput.Length - 1, 1) != @"\" ?
                    destinationFolderInput :
                    destinationFolderInput.Substring(0, destinationFolderInput.Length - 1);

                string destinationWarehouse = destinationFolder + @"\warehouse"; // warehouse folder
                string destinationDocuments = destinationWarehouse + @"\" + destinationSiteHostname; // documents
                string destinationSiteFolder = destinationFolder + @"\_config\" + destinationSiteHostname;

                // Delete files if the folders exist
                if (Directory.Exists(destinationDocuments))
                {
                    Utility.EmptyOldFolderItems(Server.MapPath(destinationDocuments), 0, true);
                }

                if (Directory.Exists(destinationSiteFolder))
                {
                    Utility.EmptyOldFolderItems(Server.MapPath(destinationSiteFolder), 0, true);
                }

                if (Directory.Exists(destinationWarehouse))
                {
                    DirectoryInfo warehouseDir = new DirectoryInfo(destinationWarehouse);
                    DirectoryInfo[] subDirs = warehouseDir.GetDirectories();

                    foreach (DirectoryInfo dir in subDirs)
                    {
                        if (dir.Name.StartsWith(idAccount.ToString() + "-" + idSite.ToString()))
                        {
                            Utility.EmptyOldFolderItems(Server.MapPath(destinationWarehouse + @"\" + dir.Name), 0, true);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
                customerManagerDatabaseObject.Dispose();
            }

        }
        #endregion

        #region _GetCourseIDForCourseMaterial
        /// <summary>
        ///Gets the course id based on the filename.
        /// </summary>
        /// <param name="filename">The filename to be searched for</param>
        /// <param name="idSite">The site id</param>
        /// <param name="databaseServer">the database server</param>
        /// <param name="databaseName">the database name</param>
        /// <returns>course id</returns>
        private int _GetCourseIDForCourseMaterial(string filename, int idSite, string databaseServer, string databaseName)
        {
            
            AsentiaDatabase databaseObject = new AsentiaDatabase(databaseServer, databaseName, null, null, "InquisiqToAsentiaMigration", 1140, true);

            databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@fileName", filename, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idCourse", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                // get course ID
                databaseObject.ExecuteNonQuery("[dbo].[InquisiqMigration_GetCourseByFilename]", true);

                // return the course ID
                return AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idCourse"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

        }
        #endregion

        #region _GetCourseIDForCourseScreenshots
        /// <summary>
        ///Gets the new course id based on the old course ID.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="databaseServer">the database server</param>
        /// <param name="databaseName">the database name</param>
        /// <returns>destination course id</returns>
        private int _GetCourseIDForCourseScreenshots(int idCourse, string databaseServer, string databaseName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(databaseServer, databaseName, null, null, "InquisiqToAsentiaMigration", 1140, true);

            databaseObject.AddParameter("@idSourceCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDestinationCourse", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {

                // get Asentia course ID
                databaseObject.ExecuteNonQuery("[dbo].[InquisiqMigration_GetDestinationCourseId]", true);

                // return the course ID
                return AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idDestinationCourse"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }


        }
        #endregion

        #region _GetContentPackageData
        /// <summary>
        ///Gets the content package data.
        /// </summary>
        /// <param name="idSite">site id</param>
        /// <param name="databaseName">database name</param>
        /// <param name="databaseServer">database server</param>
        /// <returns>content package data table</returns>
        private DataTable _GetContentPackageData(int idSite, string databaseServer, string databaseName)
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase(databaseServer, databaseName, null, null, "InquisiqToAsentiaMigration", 1140, true);

            try
            {

                // get the content package data
                ds = databaseObject.ExecuteDataSet("SELECT CP.path AS path FROM tblContentPackage CP WHERE idSite = " + idSite.ToString(), false);

                // if there is no data, return null
                if (ds.Tables.Count != 0)
                {
                    dt = ds.Tables[0];
                }
                else
                {
                    dt = null;
                }

                return dt;

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

        }
        #endregion

        #region _GetCertificateData
        /// <summary>
        ///Gets the certificate json data
        /// </summary>
        /// <param name="databaseServer">the database server</param>
        /// <param name="databaseName">the database name</param>
        /// <returns>data set of certificates with filename and json data</returns>
        private DataTable _GetCertificateData(string databaseServer, string databaseName)
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase(databaseServer, databaseName, null, null, "InquisiqToAsentiaMigration", 1140, true);

            try
            {

                // get the certificate data
                ds = databaseObject.ExecuteDataSet("SELECT CM.destinationID AS certId, CM.data AS certData, C.filename AS certFilename FROM InquisiqMigration_CertificateLayoutDataMappings CM LEFT JOIN tblCertificate C ON C.idCertificate = CM.destinationID", false);

                // if there is no data, return null
                if (ds.Tables.Count != 0)
                {
                    dt = ds.Tables[0];
                }
                else
                {
                    dt = null;
                }

                return dt;

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

        }
        #endregion

        #region _GetEmailNotificationData
        /// <summary>
        ///Gets the email notification xml data
        /// </summary>
        /// <param name="databaseServer">the database server</param>
        /// <param name="databaseName">the database name</param>
        /// <returns>data table with email notification data</returns>
        private DataTable _GetEmailNotificationData(string databaseServer, string databaseName)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase(databaseServer, databaseName, null, null, "InquisiqToAsentiaMigration", 1140, true);

            try
            {

                // get email notification data
                ds = databaseObject.ExecuteDataSet("SELECT EN.destinationID AS emailId, EN.data AS emailData FROM InquisiqMigration_EmailNotificationDataMappings EN", false);

                // if there is no data, return null
                if (ds.Tables.Count != 0)
                {
                    dt = ds.Tables[0];
                }
                else
                {
                    dt = null;
                }

                return dt;

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

        }
        #endregion

        #region _CreateProcedures
        /// <summary>
        /// Creates the temporary procedures used for file copy and creation.
        /// </summary>
        /// <param name="databaseServer">the database server</param>
        /// <param name="databaseName">the database name</param>
        private void _CreateProcedures(string databaseServer, string databaseName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(databaseServer, databaseName, null, null, "InquisiqToAsentiaMigration", 1140, true);
            
            try
            {

                // Create GetCourseByFilename procedure used to get the course ID based on filename
                // used for copying over course material
                databaseObject.ExecuteNonQuery(
                    "CREATE PROCEDURE [dbo].[InquisiqMigration_GetCourseByFilename](@idCourse INT OUTPUT, @idSite INT, @fileName NVARCHAR(255)) "
                    + "AS BEGIN SET NOCOUNT ON SELECT @idCourse = DRI.idObject FROM tblDocumentRepositoryItem DRI "
                    + "WHERE DRI.idSite = @idSite AND DRI.[fileName] = @fileName AND DRI.idDocumentRepositoryObjectType = 2 "
                    + "IF @idCourse IS NULL BEGIN SET @idCourse = 0 END END", false);

                // Create GetDestinationCourseId procedure used to get the destination course ID
                // to be used for copying over course screenshots
                databaseObject.ExecuteNonQuery(
                    "CREATE PROCEDURE [dbo].[InquisiqMigration_GetDestinationCourseId](@idSourceCourse INT, @idDestinationCourse INT OUTPUT) "
                    + "AS BEGIN SET NOCOUNT ON SELECT @idDestinationCourse = IM.destinationID FROM InquisiqMigration_IDMappings IM "
                    + "WHERE IM.sourceID = @idSourceCourse AND IM.[object] = 'course' "
                    + " IF @idDestinationCourse IS NULL BEGIN SET @idDestinationCourse = 0 END END", false);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _DropProcedures
        /// <summary>
        /// Drops the temporary procedures used for file copy and creation.
        /// </summary>
        /// <param name="databaseServer">the database server</param>
        /// <param name="databaseName">the database name</param>
        private void _DropProcedures(string databaseServer, string databaseName)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase(databaseServer, databaseName, null, null, "InquisiqToAsentiaMigration", 1140, true);

            try
            {

                // Drops the GetCourseByFilename procedure
                databaseObject.ExecuteNonQuery(
                    "IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[InquisiqMigration_GetCourseByFilename]') "
                    + "and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
                    + "DROP PROCEDURE [dbo].[InquisiqMigration_GetCourseByFilename]", false);

                // Drops the GetDestinationCourseId procedure
                databaseObject.ExecuteNonQuery(
                    "IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[InquisiqMigration_GetDestinationCourseId]') "
                    + "and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
                    + "DROP PROCEDURE [dbo].[InquisiqMigration_GetDestinationCourseId]", false);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

        }
        #endregion

        #region MigrateInquisiqDataToAsentia

        /// <summary>
        ///  Method for migrating Inquisiq data to Asentia (doing a full copy)
        /// </summary>
        /// <param name="siteHostName">Inquisiq site hostname</param>
        /// <param name="sourceDBServer">Inquisiq DB Server</param>
        /// <param name="sourceDBName">Inquisiq DB Name</param>
        /// <param name="idAccount">Asentia Account ID</param>
        /// <param name="destinationHostname">Asentia site Hostname</param>
        /// <param name="destinationDBServer">Asentia DB Server</param>
        /// <param name="destinationDBName">Asentia DB Name</param>
        /// <returns>Asentia site ID</returns>
        public Tuple<int, int> MigrateInquisiqDataToAsentia(string siteHostname, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdAccountUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDefaultLang", 57, SqlDbType.Int, 4, ParameterDirection.Input);

            // Inquisiq Parameters
            databaseObject.AddParameter("@siteHostname", siteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", null, SqlDbType.Int, 4, ParameterDirection.Output);

            // Asentia Parametemrs
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {

                // Copy Inquisiq Data To Asentia
                databaseObject.ExecuteNonQuery("[System.CopyInquisiqToAsentia]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // returns the source site id and destination site id
                return new Tuple<int, int>(
                    AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSiteSource"].Value),
                    AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSiteDestination"].Value));

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }


        #endregion
    }
}
