﻿$(document).ready(function () {

    $(".LoadingPlaceholder").css("display", "Block");

    $("#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper").text("");

});


// click hidden button to open modal popup
function hiddenModalClick() {

    // MAKE SURE ALL FIELDS ARE FILLED IN
    if (($('#SourceFolder').val() == "") || ($('#SourceDatabaseServer').val() == "") || ($('#SourceDatabaseName').val() == "") ||
        ($('#SourceSiteHostname').val() == "") || ($('#DestinationFolder').val() == "") || ($('#DestinationDatabaseServer').val() == "") ||
        ($('#DestinationDatabaseName').val() == "") || ($('#DestinationSiteHostname').val() == "") || ($('#Account').val() == 0)){

        // fields are not filled in, so terminate migration
        $('#MigrationStatusHiddenField').val("Please Ensure All Fields are Filled In");
        $('#HiddenModalButton').click();
    } else {

        // fields are filled in, so continue with migration
        $('#MigrationStatusHiddenField').val("Success");
        // call the conversion procedures
        convert();
    }
}


var siteHostname;
var sourceDBServer;
var sourceDBName;
var sourceFolder;
var idAccount;
var idSiteSource;
var useNewContentPath;

var destinationHostname;
var destinationDBServer;
var destinationDBName;
var destinationFolder;
var idSiteDestination;


// starts the conversion process
function convert() {

    siteHostname = $('#SourceSiteHostname').val();
    sourceDBServer = $('#SourceDatabaseServer').val();
    sourceDBName = $('#SourceDatabaseName').val();
    sourceFolder = $('#SourceFolder').val();
    idAccount = $('#Account option:selected').val();
    useNewContentPath = $('#UseNewContentPackagePath').is(':checked');

    destinationHostname = $('#DestinationSiteHostname').val();
    destinationDBServer = $('#DestinationDatabaseServer').val();
    destinationDBName = $('#DestinationDatabaseName').val();
    destinationFolder = $('#DestinationFolder').val();

    $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 1 of 15: Migrating Site Data");
    convertSite();
}



// converts site data
function convertSite() {

    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/ConvertSite",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ siteHostname: siteHostname, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idAccount: idAccount, destinationHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function(data) {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Site Data Migration Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 2 of 15: Migrating User and Group Data");
            idSiteSource = data.d.Item1;
            idSiteDestination = data.d.Item2;
            convertUser();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
        }
    });

}

// converts user and group data
function convertUser() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/ConvertUser",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSiteSource: idSiteSource, idSiteDestination: idSiteDestination, siteHostname: siteHostname, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idAccount: idAccount, destinationHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("User and Group Data Migration Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 3 of 15: Migrating Course Data");
            convertCourse();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// converts course, lesson, ilt, and catalog data
function convertCourse() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/ConvertCourse",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSiteSource: idSiteSource, idSiteDestination: idSiteDestination, siteHostname: siteHostname, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idAccount: idAccount, destinationHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Course Data Migration Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 4 of 15: Migrating Content Data");
            convertContent();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// converts content package and quiz survey data
function convertContent() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/ConvertContent",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSiteSource: idSiteSource, idSiteDestination: idSiteDestination, siteHostname: siteHostname, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idAccount: idAccount, destinationHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, useNewContentPath: useNewContentPath}),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Content Data Migration Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 5 of 15: Migrating Certificate Data");
            convertCertificate();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// converts certificate data
function convertCertificate() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/ConvertCertificate",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSiteSource: idSiteSource, idSiteDestination: idSiteDestination, siteHostname: siteHostname, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idAccount: idAccount, destinationHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Certificate Data Migration Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 6 of 15: Migrating Activity Import and Email Notification Data");
            convertActivityImportEmail();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// converts activity import and email notification data
function convertActivityImportEmail() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/ConvertActivityImportEmail",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSiteSource: idSiteSource, idSiteDestination: idSiteDestination, siteHostname: siteHostname, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idAccount: idAccount, destinationHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Activity Import and Email Notification Data Migration Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 7 of 15: Migrating Coupon Code Data");
            convertCouponCode();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}


// converts coupon code data
function convertCouponCode() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/ConvertCouponCode",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSiteSource: idSiteSource, idSiteDestination: idSiteDestination, siteHostname: siteHostname, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idAccount: idAccount, destinationHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Coupon Code Data Migration Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 8 of 15: Migrating Linking Table Data");
            convertLinkingTables();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// converts linking tables data
function convertLinkingTables() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/ConvertLinkingTables",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSiteSource: idSiteSource, idSiteDestination: idSiteDestination, siteHostname: siteHostname, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idAccount: idAccount, destinationHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Linking Table Data Migration Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 9 of 15: Migrating Enrollment Data");
            convertEnrollment();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// converts enrollment data
function convertEnrollment() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/ConvertEnrollment",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSiteSource: idSiteSource, idSiteDestination: idSiteDestination, siteHostname: siteHostname, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idAccount: idAccount, destinationHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Enrollment Data Migration Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 10 of 15: Copying Report Data");
            convertReport();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// converts report data
function convertReport() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/ConvertReport",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSiteSource: idSiteSource, idSiteDestination: idSiteDestination, siteHostname: siteHostname, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idAccount: idAccount, destinationHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Report Data Migration Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 11 of 15: Copying Course Material");
            copyCourseMaterial();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// copies course material
function copyCourseMaterial() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/CopyCourseMaterial",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceFolderInput: sourceFolder, destinationFolderInput: destinationFolder, sourceSiteHostname: siteHostname, destinationSiteHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Course Material Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 12 of 15: Copying Course Screenshots");
            copyCourseScreenshots();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// copies course screenshots
function copyCourseScreenshots() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/CopyCourseScreenshots",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceFolderInput: sourceFolder, destinationFolderInput: destinationFolder, sourceSiteHostname: siteHostname, destinationSiteHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Course Screenshots Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 13 of 15: Copying Certificates");
            copyCertificates();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// copies certificates
function copyCertificates() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/CopyCertificates",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceFolderInput: sourceFolder, destinationFolderInput: destinationFolder, sourceSiteHostname: siteHostname, destinationSiteHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Certificates Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 14 of 15: Copying Email Notifications");
            copyEmailNotifications();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// copies email notificatons
function copyEmailNotifications() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/CopyEmailNotifications",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceFolderInput: sourceFolder, destinationFolderInput: destinationFolder, sourceSiteHostname: siteHostname, destinationSiteHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Email Notifications Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 15 of 15: Copying Content Packages");
            copyContentPackages();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// copies content packages
function copyContentPackages() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/CopyContentPackages",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceFolderInput: sourceFolder, destinationFolderInput: destinationFolder, sourceSiteHostname: siteHostname, destinationSiteHostname: destinationHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idAccount: idAccount, idSiteSource: idSiteSource, idSiteDestination: idSiteDestination, useNewContentPath: useNewContentPath }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Content Package Copy Complete");
            $('#HiddenModalButton').click();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// deletes site if an error occurs
function deleteSite() {
    $.ajax({
        type: "POST",
        url: "/inquisiqMigration/ConvertInquisiqToAsentia.asmx/DeleteSite",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSite: idSiteDestination, destinationFolderInput: destinationFolder, destinationSiteHostname: destinationHostname, idAccount: idAccount, databaseServer: destinationDBServer, databaseName: destinationDBName }),
        success: function () {
            // do nothing
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#MigrationStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
        }
    });

}





