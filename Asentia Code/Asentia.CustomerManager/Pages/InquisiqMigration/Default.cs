﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using defaultSystem = System;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using Services = System.Web.Services;
using ScriptServices = System.Web.Script.Services;

namespace Asentia.CustomerManager.Pages.InquisiqMigration
{
    public class Default : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel ActionsPanel;
        public Panel PageInstructionsPanel;
        public Panel DatabaseFormContainer;
        public Panel PageFeedbackContainer;
        #endregion

        #region Private Properties

        private Button _SaveButton = new Button();
        private Button _CancelButton = new Button();

        private Button _HiddenModalButton = new Button();

        private TextBox _SourceFolder;
        private TextBox _SourceDatabaseName;
        private TextBox _SourceServerName;
        private TextBox _SourceSiteHostname;

        private TextBox _DestinationSiteHostname;
        private DropDownList _Account;
        private TextBox _DestinationFolder;
        private TextBox _DestinationServerName;
        private TextBox _DestinationDatabaseName;

        private CheckBox _UseNewContentPackagePath;
        private Panel _SourcePanel = new Panel();
        private Panel _DestinationPanel = new Panel();

        private ModalPopup _LoadingModal;
        private HiddenField _MigrationStatusHiddenField;

        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {

            // build the breadcrumb
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResourcesCHETU1.FormToMigrateInquisiqPortalDataToAsentiaPortal, true);

            // builds page controls
            this._BuildControls();

        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            base.OnPreRender(e);

            ScriptManager.RegisterClientScriptResource(this.Page, typeof(Default), "Asentia.CustomerManager.Pages.InquisiqMigration.Default.js");

        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            string breadCrumbPageTitle;
            string pageTitle;

            breadCrumbPageTitle = _GlobalResourcesCHETU1.InquisiqToAsentiaMigration;
            pageTitle = breadCrumbPageTitle;

            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));

            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_CLONEPORTAL,
                                                                              ImageFiles.EXT_PNG,
                                                                              true)
                                                                             );
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds page controls
        /// </summary>
        private void _BuildControls()
        {
            
            // build the user form
            this._BuildUserForm();

            // build the form actions panel
            this._BuildActionsPanel();

            // building update progress modal popup
            this._BuildLoadingModal();

        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "FormActionsPanel";

            // save button
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";
            this._SaveButton.Text = _GlobalResourcesCHETU1.MigrateData;
            this._SaveButton.Attributes.Add("onclick", "hiddenModalClick();");
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);

            // hidden button to trigger migration
            this._HiddenModalButton.ID = "HiddenModalButton";
            this._HiddenModalButton.Text = "Hidden Button";
            this._HiddenModalButton.Style.Add("display", "none");
            this._HiddenModalButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._HiddenModalButton);

            // hidden field to show migration status
            this._MigrationStatusHiddenField = new HiddenField();
            this._MigrationStatusHiddenField.ID = "MigrationStatusHiddenField";
            this._MigrationStatusHiddenField.Value = "Not Started";
            this.ActionsPanel.Controls.Add(this._MigrationStatusHiddenField);


        }

        #endregion

        #region _BuildUserForm
        /// <summary>
        /// Builds the clone portal form.
        /// </summary>
        private void _BuildUserForm()
        {

            #region BindSourceDetails

            this._SourcePanel.ID = "SourcePanel";

            List<Control> sourceControlsContainer = new List<Control>();

            // SOURCE FOLDER
            // label
            Label sourceFolderLabel = new Label();
            sourceFolderLabel.ID = "SourceFolderLabel";
            sourceFolderLabel.Text = _GlobalResourcesCHETU1.InquisiqWebFolder + ":";
            sourceControlsContainer.Add(sourceFolderLabel);

            // text box
            this._SourceFolder = new TextBox();
            this._SourceFolder.ID = "SourceFolder";
            sourceControlsContainer.Add(this._SourceFolder);

            // SOURCE SERVER NAME
            // label
            Label sourceServerNameLabel = new Label();
            sourceServerNameLabel.ID = "sourceServerNameLabel";
            sourceServerNameLabel.Text = _GlobalResourcesCHETU1.Server + ":";
            sourceControlsContainer.Add(sourceServerNameLabel);

            // Textbox
            this._SourceServerName = new TextBox();
            this._SourceServerName.ID = "SourceDatabaseServer";
            sourceControlsContainer.Add(this._SourceServerName);

            // SOURCE DATABASE
            // label
            Label sourceDatabaseNameLabel = new Label();
            sourceDatabaseNameLabel.ID = "SourceDatabaseNameLabel";
            sourceDatabaseNameLabel.Text = _GlobalResourcesCHETU1.Database + ":";
            sourceControlsContainer.Add(sourceDatabaseNameLabel);

            // text box
            this._SourceDatabaseName = new TextBox();
            this._SourceDatabaseName.ID = "SourceDatabaseName";
            sourceControlsContainer.Add(this._SourceDatabaseName);

            // SOURCE HOSTNAME
            // label
            Label sourceHostnameLabel = new Label();
            sourceHostnameLabel.ID = "SourceHostnameLabel";
            sourceHostnameLabel.Text = _GlobalResourcesCHETU1.SiteHostname + ":";
            sourceControlsContainer.Add(sourceHostnameLabel);

            // textbox
            this._SourceSiteHostname = new TextBox();
            this._SourceSiteHostname.ID = "SourceSiteHostname";
            sourceControlsContainer.Add(this._SourceSiteHostname);

            // NEW INQUISIQ CONTENT PACKAGE PATH            
            // checkbox
            this._UseNewContentPackagePath = new CheckBox();
            this._UseNewContentPackagePath.ID = "UseNewContentPackagePath";
            this._UseNewContentPackagePath.Text = _GlobalResources.UseNewWarehousePathsFromMay2018InquisiqUpdate;
            sourceControlsContainer.Add(this._UseNewContentPackagePath);
            

            // add control in source panel
            this._SourcePanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Source",
                                                                                          _GlobalResourcesCHETU1.Source_Inquisiq,
                                                                                          sourceControlsContainer,
                                                                                          false,
                                                                                          true,
                                                                                          true)
                                                                                         );

            

            #endregion


            #region BindDestinationDetails

            this._DestinationPanel.ID = "DestinationPanel";

            List<Control> destinationControlsContainer = new List<Control>();

            // ACCOUNT
            // label
            Label accountLabel = new Label();
            accountLabel.ID = "AccountLabel";
            accountLabel.Text = _GlobalResourcesCHETU1.Account + ":";
            destinationControlsContainer.Add(accountLabel);

            // drop down list
            this._Account = new DropDownList();
            this._Account.ID = "Account";
            this._Account.DataSource = Library.DatabaseServer.GetAccountList();
            this._Account.DataValueField = "idAccount";
            this._Account.DataTextField = "company";
            this._Account.DataBind();
            this._Account.ClientIDMode = ClientIDMode.Static;
            this._Account.AutoPostBack = false;
            this._Account.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
            destinationControlsContainer.Add(this._Account);

            // SITE HOSTNAME
            // label
            Label destinationSiteHostnameLabel = new Label();
            destinationSiteHostnameLabel.ID = "DestinationSiteHostnameLabel";
            destinationSiteHostnameLabel.Text = _GlobalResourcesCHETU1.SiteHostname + ":";
            destinationControlsContainer.Add(destinationSiteHostnameLabel);

            // text box
            this._DestinationSiteHostname = new TextBox();
            this._DestinationSiteHostname.ID = "DestinationSiteHostname";
            destinationControlsContainer.Add(this._DestinationSiteHostname);

            // DESTINATION FOLDER
            // label 
            Label destinationFolderLabel = new Label();
            destinationFolderLabel.ID = "DestinationFolderLabel";
            destinationFolderLabel.Text = _GlobalResourcesCHETU1.AsentiaFolder + ":";
            destinationControlsContainer.Add(destinationFolderLabel);

            // textbox
            this._DestinationFolder = new TextBox();
            this._DestinationFolder.ID = "DestinationFolder";
            destinationControlsContainer.Add(this._DestinationFolder);

            // DESTINATION SERVER NAME
            // label 
            Label destinationServerNameLabel = new Label();
            destinationServerNameLabel.ID = "DestinationServerNameLabel";
            destinationServerNameLabel.Text = _GlobalResourcesCHETU1.Server + ":";
            destinationControlsContainer.Add(destinationServerNameLabel);

            // text box
            this._DestinationServerName = new TextBox();
            this._DestinationServerName.ID = "DestinationDatabaseServer";
            destinationControlsContainer.Add(this._DestinationServerName);

            // DESTINATION DATABASE NAME
            // label
            Label destinationDatabaseNameLabel = new Label();
            destinationDatabaseNameLabel.ID = "DestinationDatabaseNameLabel";
            destinationDatabaseNameLabel.Text = _GlobalResourcesCHETU1.Database + ":";
            destinationControlsContainer.Add(destinationDatabaseNameLabel);

            // text box
            this._DestinationDatabaseName = new TextBox();
            this._DestinationDatabaseName.ID = "DestinationDatabaseName";
            destinationControlsContainer.Add(this._DestinationDatabaseName);

            // add controls in DestinationPanel
            this._DestinationPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Destination",
                                                          _GlobalResourcesCHETU1.Destination_Asentia,
                                                          destinationControlsContainer,
                                                             false,
                                                             true,
                                                             true
                                                             ));


            // adding controls container panel to DatabaseFormContainer
            this.DatabaseFormContainer.Controls.Add(this._SourcePanel);
            this.DatabaseFormContainer.Controls.Add(this._DestinationPanel);

            #endregion

        }

        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/");   
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click and migrates the data.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
            {
                
            if (this._MigrationStatusHiddenField.Value == "Success")
            {
                // Close the loading modal
                this._LoadingModal.HideModal();

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResourcesCHETU1.PortalHasBeenMigratedSuccessfully, false);
            }
                else
                {
                // Close the loading modal
                this._LoadingModal.HideModal();

                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer,_GlobalResourcesCHETU1.MigrationFailed + ": " + this._MigrationStatusHiddenField.Value, true);
            }
            }
        #endregion

        #region _BuildLoadingModal
        /// <summary>
        /// Builds modal popup to signify that the migration is running.
        /// </summary>
        private void _BuildLoadingModal()
        {
            this._LoadingModal = new ModalPopup("LoadingModal");
            this._LoadingModal.OverrideLoadImagesFromCustomerManager = true;

            // set modal properties
            this._LoadingModal.Type = ModalPopupType.Information;
            this._LoadingModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CLONEPORTAL,
                                                                                  ImageFiles.EXT_PNG,
                                                                                  true
                                                                                  );

            this._LoadingModal.HeaderText = _GlobalResourcesCHETU1.InquisiqToAsentiaMigrationInProgress;
            this._LoadingModal.TargetControlID = "SaveButton";
            this._LoadingModal.SubmitButton.Visible = false;
            this._LoadingModal.CloseButton.Visible = false;
            this._LoadingModal.ShowCloseIcon = false;
            this._LoadingModal.ShowLoadingPlaceholder = true;

            this.ActionsPanel.Controls.Add(this._LoadingModal);

                }
            }

        #endregion
}