﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Services = System.Web.Services;
using ScriptServices = System.Web.Script.Services;

namespace Asentia.CustomerManager.Pages.ClonePortal
{
    [Services.WebService(Namespace = "http://default.asentia.com/ClonePortal")]
    [Services.WebServiceBinding(ConformsTo = Services.WsiProfiles.BasicProfile1_1)]
    [ScriptServices.ScriptService]
    public class ClonePortal : Services.WebService
    {
        #region SavePartialCloneSiteProperties
        /// <summary>
        /// Saves the site properties for a partial clone
        /// </summary>
        /// <param name="idAccount">account ID</param>
        /// <param name="title">title</param>
        /// <param name="company">company</param>
        /// <param name="contactName">site contact name</param>
        /// <param name="hostname">site hostname</param>
        /// <param name="contactEmail">site contact email</param>
        /// <param name="defaultLanguage">site default language</param>
        /// <param name="timezone">site timezone</param>
        /// <param name="password">site password</param>
        /// <param name="userLimit">site user limit</param>
        /// <param name="kbLimit">site KB limit</param>
        /// <param name="isActive">is the site active?</param>
        /// <param name="expirationDate">site expiration date</param>
        /// <param name="eCommerce">is e-commerce enabled?</param>
        /// <param name="certifications">is certifications enabled?</param>
        /// <param name="quizzesAndSurveys">is quizzes and surveys enabled?</param>
        /// <param name="goToMeeting">is go to meeting enabled?</param>
        /// <param name="goToTraining">is go to training enabled?</param>
        /// <param name="goToWebinar">is to to webinar enabled?</param>
        /// <param name="openSesame">is open sesame enabled?</param>
        /// <param name="webEx">is web ex enabled?</param>
        /// <param name="availableLanguages">available languages</param>
        /// <returns></returns>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public string SavePartialCloneSiteProperties(int idAccount, string title, string company, string contactName, string hostname, string contactEmail, string defaultLanguage, int timezone, string password, string userLimit, string kbLimit, bool isActive, DateTime? expirationDate, bool eCommerce, bool certifications, bool quizzesAndSurveys, bool goToMeeting, bool goToWebinar, bool goToTraining, bool webEx, bool openSesame, List<string> availableLanguages)
        {
            try
            {
                // site object
                AsentiaSite siteObject = new AsentiaSite();

                // title
                siteObject.Title = title;

                // company name
                siteObject.Company = company;

                // contact name
                if (!String.IsNullOrWhiteSpace(contactName))
                { siteObject.ContactName = contactName; }
                else
                { siteObject.ContactName = null; }

                // destination site's host name
                siteObject.Hostname = hostname.Trim();

                // destination site's conatct email
                if (!String.IsNullOrWhiteSpace(contactEmail.Trim()))
                { siteObject.ContactEmail = contactEmail.Trim(); }
                else
                { siteObject.ContactEmail = null; }

                // destination site's language 
                siteObject.LanguageString = defaultLanguage;

                // destination site's time zone 
                siteObject.IdTimezone = timezone;

                // destination site's password
                siteObject.Password = password.Trim();

                // destination site's user limit 
                if (!String.IsNullOrWhiteSpace(userLimit))
                { siteObject.UserLimit = Convert.ToInt32(userLimit); }
                else
                { siteObject.UserLimit = null; }

                // destination site's kb limit 
                if (!String.IsNullOrWhiteSpace(kbLimit))
                { siteObject.KbLimit = Convert.ToInt32(kbLimit); }
                else
                { siteObject.KbLimit = null; }

                // destination site's isActive flag
                siteObject.IsActive = isActive;

                // destination site's expiration date
                siteObject.Expires = expirationDate;

                // FEATURES

                DataTable siteParams = new DataTable();
                siteParams.Columns.Add("key");
                siteParams.Columns.Add("value");

                DataRow newSiteParamRow;

                // e-commerce
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.ECOMMERCE_ENABLED;
                newSiteParamRow["value"] = eCommerce.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // certifications
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.CERTIFICATIONS_ENABLE;
                newSiteParamRow["value"] = certifications.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // quizzes and survey
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.QUIZZESANDSURVEYS_ENABLE;
                newSiteParamRow["value"] = quizzesAndSurveys.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // GoToMeeting
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTM_ENABLE;
                newSiteParamRow["value"] = goToMeeting.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // GoToWebinar
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTW_ENABLE;
                newSiteParamRow["value"] = goToWebinar.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // GoToTraining
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTT_ENABLE;
                newSiteParamRow["value"] = goToTraining.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // WebEx
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_WEBEX_ENABLE;
                newSiteParamRow["value"] = webEx.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // Open Sesame
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.OPENSESAME_ENABLE;
                newSiteParamRow["value"] = openSesame.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // check for existence of hostname with same name only when creating new site                
                bool isHostNameExists = false;

                isHostNameExists = Asentia.CustomerManager.Library.Account.DoesHostNameExists(hostname.Trim());

                // if the hostname does not already exist, proceed
                if (!isHostNameExists)
                {
                    int idSite = 0;

                    // create the new site
                    idSite = siteObject.Save(0, Config.AccountSettings.DefaultLanguage, 1);

                    // save the other site properties
                    if (idSite > 0)
                    {
                        // saves site to domain alias link (Account Database)
                        // passed idCaller value as 1, because the Account DB procedure is accessed here from inside the Customer Manager application.
                        Library.Site.SaveSiteToDomainAliasLink(idSite, siteObject.Hostname, 1);

                        // saves account to domain alias link (Customer Manager Database)
                        Library.Account.SaveAccountToDomainAliasLink(AsentiaSessionState.IdAccount, siteObject.Hostname);

                        // save the site params
                        AsentiaSite.SaveSiteParams(idSite, AsentiaSessionState.UserCulture, 1, idSite, siteParams);

                        // available languages

                        // get a list of all languages
                        DataTable languages = Language.GetLanguages();

                        // declare data table for available languages
                        DataTable languageIds = new DataTable();
                        languageIds.Columns.Add("id", typeof(int));

                        // loop through the available languages and add the IDs to the language id array list
                        foreach (string item in availableLanguages)
                        {
                            foreach (DataRow row in languages.Rows)
                            {
                                if (row["code"].ToString() == item)
                                {
                                    languageIds.Rows.Add(Convert.ToInt32(row["idLanguage"].ToString()));
                                    break;
                                }
                            }
                        }

                        siteObject.SaveAvailableLanguages(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, 1, siteObject.Id, languageIds);
                    }

                    // return destination site id, prepended with success to let the javascript know that it was successful
                    return "Success|" + idSite.ToString();
                }
                else
                {
                    // return failure message
                    return _GlobalResources.HostnameAlreadyExistsPleaseTryAgain;
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // return failure message
                return dnfEx.Message + "|" + dnfEx.StackTrace;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // return failure message
                return fnuEx.Message + "|" + fnuEx.StackTrace;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // return failure message
                return cpeEx.Message + "|" + cpeEx.StackTrace;
            }
            catch (DatabaseException dEx)
            {
                // return failure message
                return dEx.Message + "|" + dEx.StackTrace;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                return ex.Message + "|" + ex.StackTrace;
            }
        }
        #endregion

        #region STEP 1: CopySite
        /// <summary>
        /// Copies data in the following tables:
        ///     tblSite
        ///     tblSiteLanguage
        ///     tblAccountToDomainAliasLink
        ///     tblSiteToDomainAliasLink
        ///     tblSiteParam
        ///     tblSiteAvailableLanguage
        /// </summary>
        /// <param name="fullCopyFlag">full copy?</param>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="siteHostname">source site hostname</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationHostname">destination site hostname</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        /// <returns>destination site ID</returns>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public int CopySite(bool fullCopyFlag, int idSiteSource, string sourceDBServer, string sourceDBName, int idAccount, string destinationHostname, string destinationDBServer, string destinationDBName, int idSiteDestination)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 3600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            if (idSiteDestination == 0) // full Clone (we don't know the destination site id yet)
            { databaseObject.AddParameter("@idSiteDestination", null, SqlDbType.Int, 4, ParameterDirection.Output); }
            else // partial Clone (we already have the destination site id)
            { databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.InputOutput); }

            databaseObject.AddParameter("@fullCopyFlag", fullCopyFlag, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationHostname", destinationHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.Site]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSiteDestination"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 2: CopyCatalogCourse
        /// <summary>
        /// Copies data in the following tables:
        ///     tblCatalog
        ///     tblCatalogLanguage
        ///     tblCourse
        ///     tblCourseLanguage
        ///     tblCourseToCatalogLink
        ///     tblCourseToPrerequisiteLink
        ///     tblCourseToScreenshotLink
        ///     tblLesson
        ///     tblLessonLanguage
        ///     tblLearningPath
        ///     tblLearningPathLanguage
        ///     tblLearningPathToCourseLink
        ///     tblRuleSetLearningPathEnrollment
        ///     tblRuleSetLearningPathEnrollmentLanguage
        /// </summary>
        /// <param name="fullCopyFlag">full copy?</param>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyCatalogCourse(int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 3600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.CatalogCourse]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 3: CopyGroup
        /// <summary>
        /// Copies data in the following tables:
        ///     tblGroup
        ///     tblGroupLanguage
        ///     tblCatalogAccessToGroupLink
        ///     tblGroupEnrollment
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyGroup(int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 3600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.Group]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 4: CopyUser
        /// <summary>
        /// Copies data in the following tables:
        ///     tblUser
        ///     tblCourseExpert
        ///     tblCourseEnrollmentApprover
        ///     tblUserToSupervisorLink
        ///     tblActivityImport
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyUser(int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 3600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.User]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 5: CopyContentResource
        /// <summary>
        /// Copies data in the following tables:
        ///     tblQuizSurvey
        ///     tblContentPackage
        ///     tblResourceType
        ///     tblResourceTypeLanguage
        ///     tblResource
        ///     tblResourceLanguage
        /// </summary>
        /// <param name="fullCopyFlag">full copy?</param>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="idAccount">account id</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyContentResource(bool fullCopyFlag, int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, int idAccount, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@fullCopyFlag", fullCopyFlag, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.ContentResource]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 6: CopyILT
        /// <summary>
        /// Copies data in the following tables:
        ///     tblStandUpTraining
        ///     tblStandUpTrainingLanguage
        ///     tblStandUpTrainingInstance
        ///     tblStandUpTrainingInstanceLanguage
        ///     tblResourceToObjectLink
        ///     tblStandupTrainingInstanceToUserLink
        ///     tblStandupTrainingInstanceToInstructorLink
        ///     tblStandUpTrainingInstanceMeetingTime
        ///     tblLessonToContentLink
        /// </summary>
        /// <param name="fullCopyFlag">full copy?</param>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyILT(string fullCopyFlag, int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@fullCopyFlag", fullCopyFlag, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.InstructorLedTraining]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 7: CopyRoleRule
        /// <summary>
        /// Copies data in the following tables:
        ///     tblRole
        ///     tblRoleLanguage
        ///     tblRoleToPermissionLink
        ///     tblRuleset
        ///     tblRulesetLanguage
        ///     tblRule
        ///     tblRuleSetToGroupLink
        ///     tblRuleSetToRoleLink
        ///     tblRuleSetToRuleSetLearningPathEnrollmentLink
        ///     tblRuleSetEnrollment
        ///     tblRuleSetEnrollmentLanguage
        ///     tblRuleSetToRuleSetEnrollmentLink
        ///     tblUserToGroupLink
        ///     tblUserToRoleLink
        ///     tblGroupToRoleLink
        ///     tblUserToLearningPathLink
        /// </summary>
        /// <param name="fullCopyFlag">full copy?</param>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyRoleRule(bool fullCopyFlag, int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@fullCopyFlag", fullCopyFlag, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.RoleRule]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 8: CopyCouponCode
        /// <summary>
        /// Copies data in the following tables:
        ///     tblCouponCode
        ///     tblCouponCodeToStandupTrainingLink
        ///     tblCouponCodeToCatalogLink
        ///     tblCouponCodeToLearningPathLink
        ///     tblCouponCodeToCourseLink
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyCouponCode(int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.CouponCode]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 9: CopyCertificateCertification
        /// <summary>
        /// Copies data in the following tables:
        ///     tblCertificate
        ///     tblCertificateLanguage
        ///     tblCertificateImport
        ///     tblCertificateRecord
        ///     tblCertification
        ///     tblCertificationLanguage
        ///     tblCertificationModule
        ///     tblCertificationModuleLanguage
        ///     tblCertificationModuleRequirementSet
        ///     tblCertificationModuleRequirementSetLanguage
        ///     tblCertificationModuleRequirement
        ///     tblCertificationModuleRequirementLanguage
        ///     tblCertificationModuleRequirementToCourseLink
        ///     tblCertificationToCourseCreditLink
        ///     tblCertificationToUserLink
        ///     tblData-CertificationModuleRequirement
        /// </summary>
        /// <param name="fullCopyFlag">full copy?</param>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyCertificateCertification(bool fullCopyFlag, int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@fullCopyFlag", fullCopyFlag, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.CertificateCertification]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 10: CopyDiscussionFeed
        /// <summary>
        /// Copies data in the following tables:
        ///     tblCourseFeedMessage
        ///     tblCourseFeedModerator
        ///     tblCourseRating
        ///     tblGroupFeedMessage
        ///     tblGroupFeedModerator
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyDiscussionFeed(int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.DiscussionFeed]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 11: CopyEnrollment
        /// <summary>
        /// Copies data in the following tables:
        ///     tblEnrollment
        ///     tblEnrollmentRequest
        ///     tblData-Lesson
        ///     tblLearningPathEnrollment
        ///     tblLearningPathEnrollmentApprover
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyEnrollment(int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.Enrollment]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 12: CopyDocumentRepositoryEventEmail
        /// <summary>
        /// Copies data in the following tables:
        ///     tblDocumentRepositoryFolder
        ///     tblEventEmailNotification
        ///     tblEventEmailNotificationLanguage
        ///     tblDocumentRepositoryItem
        ///     tblDocumentRepositoryItemLanguage
        ///     tblEventLog
        ///     tblEventEmailQueue
        ///     tblInboxMessage
        /// </summary>
        /// <param name="fullCopyFlag">full copy?</param>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyDocumentRepositoryEventEmail(bool fullCopyFlag, int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@fullCopyFlag", fullCopyFlag, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.DocumentRepositoryEventEmail]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 13: CopyData
        /// <summary>
        /// Copies data in the following tables:
        ///     tblData-SCO
        ///     tblData-HomeworkAssignment
        ///     tblxAPIoAuthConsumer
        ///     tblData-TinCan
        ///     tblData-TinCanProfile
        ///     tblData-TinCanState
        ///     tblData-TinCanContextActivities
        ///     tblData-SCOInt
        ///     tblData-SCOObj
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyData(int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.Data]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 14: CopyPurchaseTransaction
        /// <summary>
        /// Copies data in the following table:
        ///     tblPurchase
        ///     tblTransactionItem
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyPurchaseTransaction(int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.PurchaseTransaction]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 15: CopyReport
        /// <summary>
        /// Copies data in the following tables:
        ///     tblReport
        ///     tblReportLanguage
        ///     tblReportFile
        ///     tblReportSubscription
        ///     tblReportToReportShortcutsWidgetLink
        ///     tblSystem
        /// </summary>
        /// <param name="idSiteSource">source site id</param>
        /// <param name="idSiteDestination">destination site id</param>
        /// <param name="sourceDBServer">source db server name</param>
        /// <param name="sourceDBName">source db name</param>
        /// <param name="destinationDBServer">destination db server name</param>
        /// <param name="destinationDBName">destination db name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyReport(int idSiteSource, int idSiteDestination, string sourceDBServer, string sourceDBName, string destinationDBServer, string destinationDBName)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 1140);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idSiteSource", idSiteSource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSiteDestination", idSiteDestination, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBServer", sourceDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@sourceDBName", sourceDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBServer", destinationDBServer, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@destinationDBName", destinationDBName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[System.ClonePortal.Report]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region STEP 16: CopyConfig
        /// <summary>
        /// Copies the _config/[portal] folder to _config/[cloned portal] and updates folders named for object ids.
        /// </summary>
        /// <param name="destinationPortalName">Destination Asentia Hostname</param>
        /// <param name="sourcePortalName">Source Asentia Hostname</param>
        /// <param name="destinationDBName">Destination DB Name</param>
        /// <param name="destinationDBServer">Destination DB Server</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyConfig(string destinationPortalName, string sourcePortalName, string destinationDBServer, string destinationDBName, bool copyLicenseAgreementToSite)
        {
            // get the object id mappings from the database portion of the clone process
            DataTable clonePortalIdMappings = new DataTable();
            clonePortalIdMappings = Library.DatabaseServer.GetIDMappings(destinationDBServer, destinationDBName, "_config");

            // format source directory path            
            string sourceDirectoryPath = Config.ApplicationSettings.AsentiaContentSourceRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + sourcePortalName;
            DirectoryInfo sourceDir = new DirectoryInfo(sourceDirectoryPath);

            // format destination directory path            
            string destinationDirectoryPath = Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + destinationPortalName;

            // check if source directory exists
            if (!sourceDir.Exists)
            { throw new AsentiaException(String.Format(_GlobalResources.TheSourceDirectoryDoesNotExistOrCouldNotBeFound, sourceDirectoryPath)); }

            // copy the source directory into the destination directory
            if (sourceDirectoryPath != destinationDirectoryPath)
            { Utility.CopyDirectory(sourceDirectoryPath, destinationDirectoryPath, true, true); }

            // get the destination portal's sub directories
            var destPortalSubDirectories = Directory.GetDirectories(destinationDirectoryPath);

            // loop through the sub directories, we need to copy items that are inside of folders
            // "certificates", "courses", "emailNotifications", "groups", "learningPaths", "standuptraining", and "users"
            foreach (string subFolder in destPortalSubDirectories)
            {
                // get the name of the sub-folder we're currently looking at
                string subFolderName = new DirectoryInfo(subFolder).Name;

                // get the object sub folders
                var objectSubFolders = Directory.GetDirectories(subFolder);

                switch (subFolderName)
                {
                    case "catalogs":

                        // loop through sub folders of the object folder, and rename its folder ids for new object mappings
                        foreach (string objectSubFolder in objectSubFolders)
                        {
                            // this is the object sub folder id that we need to rename
                            string oldObjectId = new DirectoryInfo(objectSubFolder).Name;

                            // get the new id for this object from our data mappings table
                            DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'catalog'");

                            if (resultRow.Length > 0)
                            {
                                // get the new id
                                string newObjectId = resultRow[0]["destinationID"].ToString();

                                // move the old folder to the new folder
                                if (Directory.Exists(objectSubFolder))
                                {
                                    string newObjectFolderPath = objectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                    Directory.Move(objectSubFolder, newObjectFolderPath);
                                }
                            }
                            else
                            {
                                // delete the directory if there is no object with that id
                                if (Directory.Exists(objectSubFolder))
                                {
                                    Utility.EmptyOldFolderItems(objectSubFolder, 0, true);
                                    Directory.Delete(objectSubFolder);
                                }
                            }
                        }

                        break;
                    case "certificates":

                        // loop through sub folders of the object folder, and rename its folder ids for new object mappings
                        foreach (string objectSubFolder in objectSubFolders)
                        {
                            // this is the object sub folder id that we need to rename
                            string oldObjectId = new DirectoryInfo(objectSubFolder).Name;

                            // get the new id for this object from our data mappings table
                            DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'certificates'");

                            if (resultRow.Length > 0)
                            {
                                // get the new id
                                string newObjectId = resultRow[0]["destinationID"].ToString();

                                // move the old folder to the new folder
                                if (Directory.Exists(objectSubFolder))
                                {
                                    string newObjectFolderPath = objectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                    Directory.Move(objectSubFolder, newObjectFolderPath);
                                }
                            }
                            else
                            {
                                // delete the directory if there is no object with that id
                                if (Directory.Exists(objectSubFolder))
                                {
                                    Utility.EmptyOldFolderItems(objectSubFolder, 0, true);
                                    Directory.Delete(objectSubFolder);
                                }
                            }
                        }

                        break;
                    case "courses":

                        // loop through sub folders of the object folder, and rename its folder ids for new object mappings
                        foreach (string objectSubFolder in objectSubFolders)
                        {
                            // this is the object sub folder id that we need to rename
                            string oldObjectId = new DirectoryInfo(objectSubFolder).Name;

                            // get the new id for this object from our data mappings table
                            DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'courses'");

                            if (resultRow.Length > 0)
                            {
                                // get the new id
                                string newObjectId = resultRow[0]["destinationID"].ToString();

                                // move the old folder to the new folder
                                if (Directory.Exists(objectSubFolder))
                                {
                                    string newObjectFolderPath = objectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                    Directory.Move(objectSubFolder, newObjectFolderPath);
                                }
                            }
                            else
                            {
                                // delete the directory if there is no object with that id
                                if (Directory.Exists(objectSubFolder))
                                {
                                    Utility.EmptyOldFolderItems(objectSubFolder, 0, true);
                                    Directory.Delete(objectSubFolder);
                                }
                            }
                        }

                        break;
                    case "emailNotifications":

                        // loop through sub folders of the object folder, and rename its folder ids for new object mappings
                        foreach (string objectSubFolder in objectSubFolders)
                        {
                            // this is the object sub folder id that we need to rename
                            string oldObjectId = new DirectoryInfo(objectSubFolder).Name;

                            // get the new id for this object from our data mappings table
                            DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'emailNotifications'");

                            if (resultRow.Length > 0)
                            {
                                // get the new id
                                string newObjectId = resultRow[0]["destinationID"].ToString();

                                // move the old folder to the new folder
                                if (Directory.Exists(objectSubFolder))
                                {
                                    string newObjectFolderPath = objectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                    Directory.Move(objectSubFolder, newObjectFolderPath);
                                }
                            }
                            else
                            {
                                // delete the directory if there is no object with that id
                                if (Directory.Exists(objectSubFolder))
                                {
                                    Utility.EmptyOldFolderItems(objectSubFolder, 0, true);
                                    Directory.Delete(objectSubFolder);
                                }
                            }
                        }

                        break;
                    case "groups":

                        // loop through sub folders of the object folder, and rename its folder ids for new object mappings
                        foreach (string objectSubFolder in objectSubFolders)
                        {
                            // this is the object sub folder id that we need to rename
                            string oldObjectId = new DirectoryInfo(objectSubFolder).Name;

                            // get the new id for this object from our data mappings table
                            DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'groups'");

                            if (resultRow.Length > 0)
                            {
                                // get the new id
                                string newObjectId = resultRow[0]["destinationID"].ToString();

                                // move the old folder to the new folder
                                if (Directory.Exists(objectSubFolder))
                                {
                                    string newObjectFolderPath = objectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                    Directory.Move(objectSubFolder, newObjectFolderPath);
                                }
                            }
                            else
                            {
                                // delete the directory if there is no object with that id
                                if (Directory.Exists(objectSubFolder))
                                {
                                    Utility.EmptyOldFolderItems(objectSubFolder, 0, true);
                                    Directory.Delete(objectSubFolder);
                                }
                            }
                        }

                        break;
                    case "learningPaths":

                        // loop through sub folders of the object folder, and rename its folder ids for new object mappings
                        foreach (string objectSubFolder in objectSubFolders)
                        {
                            // this is the object sub folder id that we need to rename
                            string oldObjectId = new DirectoryInfo(objectSubFolder).Name;

                            // get the new id for this object from our data mappings table
                            DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'learningPaths'");

                            if (resultRow.Length > 0)
                            {
                                // get the new id
                                string newObjectId = resultRow[0]["destinationID"].ToString();

                                // move the old folder to the new folder
                                if (Directory.Exists(objectSubFolder))
                                {
                                    string newObjectFolderPath = objectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                    Directory.Move(objectSubFolder, newObjectFolderPath);
                                }
                            }
                            else
                            {
                                // delete the directory if there is no object with that id
                                if (Directory.Exists(objectSubFolder))
                                {
                                    Utility.EmptyOldFolderItems(objectSubFolder, 0, true);
                                    Directory.Delete(objectSubFolder);
                                }
                            }
                        }

                        break;
                    case "standuptraining":

                        // loop through sub folders of the object folder, and rename its folder ids for new object mappings
                        foreach (string objectSubFolder in objectSubFolders)
                        {
                            // this is the object sub folder id that we need to rename
                            string oldObjectId = new DirectoryInfo(objectSubFolder).Name;

                            // get the new id for this object from our data mappings table
                            DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'standuptraining'");

                            if (resultRow.Length > 0)
                            {
                                // get the new id
                                string newObjectId = resultRow[0]["destinationID"].ToString();

                                // move the old folder to the new folder
                                if (Directory.Exists(objectSubFolder))
                                {
                                    string newObjectFolderPath = objectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                    Directory.Move(objectSubFolder, newObjectFolderPath);
                                }
                            }
                            else
                            {
                                // delete the directory if there is no object with that id
                                if (Directory.Exists(objectSubFolder))
                                {
                                    Utility.EmptyOldFolderItems(objectSubFolder, 0, true);
                                    Directory.Delete(objectSubFolder);
                                }
                            }
                        }

                        break;
                    case "users":

                        // loop through sub folders of the object folder, and rename its folder ids for new object mappings
                        foreach (string objectSubFolder in objectSubFolders)
                        {
                            // this is the object sub folder id that we need to rename
                            string oldObjectId = new DirectoryInfo(objectSubFolder).Name;

                            if (oldObjectId != "default" && oldObjectId != "1")
                            {
                                // get the new id for this object from our data mappings table
                                DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'users'");

                                if (resultRow.Length > 0)
                                {
                                    // get the new id
                                    string newObjectId = resultRow[0]["destinationID"].ToString();

                                    // move the old folder to the new folder
                                    if (Directory.Exists(objectSubFolder))
                                    {
                                        string newObjectFolderPath = objectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                        Directory.Move(objectSubFolder, newObjectFolderPath);

                                        // get the sub folders of the newly moved user object folder
                                        var userSubFolders = Directory.GetDirectories(newObjectFolderPath);

                                        // loop through the user object sub folders and rename object folders for Tasks and CertificationTasks
                                        foreach (string userSubFolder in userSubFolders)
                                        {
                                            // get the user sub folder name
                                            string userSubFolderName = new DirectoryInfo(userSubFolder).Name;

                                            // get the object sub folders
                                            var userObjectSubFolders = Directory.GetDirectories(userSubFolder);

                                            switch (userSubFolderName)
                                            {
                                                case "Tasks":

                                                    // loop through sub folders of the user object folder, and rename its folder ids for new object mappings
                                                    foreach (string userObjectSubFolder in userObjectSubFolders)
                                                    {
                                                        // this is the user object sub folder id that we need to rename
                                                        string userSubOldObjectId = new DirectoryInfo(userObjectSubFolder).Name;

                                                        // get the new id for this object from our data mappings table
                                                        DataRow[] userObjectResultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(userSubOldObjectId) + " AND object = 'datalesson'");

                                                        if (userObjectResultRow.Length > 0)
                                                        {
                                                            // get the new id
                                                            string userSubNewObjectId = userObjectResultRow[0]["destinationID"].ToString();

                                                            // move the old folder to the new folder
                                                            if (Directory.Exists(userObjectSubFolder))
                                                            {
                                                                string userObjectNewObjectFolderPath = userObjectSubFolder.Replace("\\" + userSubOldObjectId, "\\" + userSubNewObjectId + "__TEMP__");
                                                                Directory.Move(userObjectSubFolder, userObjectNewObjectFolderPath);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            // delete the directory if there is no object with that id
                                                            if (Directory.Exists(userObjectSubFolder))
                                                            {
                                                                Utility.EmptyOldFolderItems(userObjectSubFolder, 0, true);
                                                                Directory.Delete(userObjectSubFolder);
                                                            }
                                                        }
                                                    }

                                                    break;
                                                case "CertificationTasks":

                                                    // loop through sub folders of the user object folder, and rename its folder ids for new object mappings
                                                    foreach (string userObjectSubFolder in userObjectSubFolders)
                                                    {
                                                        // this is the user object sub folder id that we need to rename
                                                        string userSubOldObjectId = new DirectoryInfo(userObjectSubFolder).Name;

                                                        // get the new id for this object from our data mappings table
                                                        DataRow[] userObjectResultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(userSubOldObjectId) + " AND object = 'certificationmodulerequirement'");

                                                        if (userObjectResultRow.Length > 0)
                                                        {
                                                            // get the new id
                                                            string userSubNewObjectId = userObjectResultRow[0]["destinationID"].ToString();

                                                            // move the old folder to the new folder
                                                            if (Directory.Exists(userObjectSubFolder))
                                                            {
                                                                string userObjectNewObjectFolderPath = userObjectSubFolder.Replace("\\" + userSubOldObjectId, "\\" + userSubNewObjectId + "__TEMP__");
                                                                Directory.Move(userObjectSubFolder, userObjectNewObjectFolderPath);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            // delete the directory if there is no object with that id
                                                            if (Directory.Exists(userObjectSubFolder))
                                                            {
                                                                Utility.EmptyOldFolderItems(userObjectSubFolder, 0, true);
                                                                Directory.Delete(userObjectSubFolder);
                                                            }
                                                        }
                                                    }

                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    // delete the directory if there is no object with that id
                                    if (Directory.Exists(objectSubFolder))
                                    {
                                        Utility.EmptyOldFolderItems(objectSubFolder, 0, true);
                                        Directory.Delete(objectSubFolder);
                                    }
                                }
                            }
                        }

                        break;
                    default:
                        break;
                }
            }

            // remove __TEMP__ from object folder names            
            _RemoveTempPostfixFromFoldersRecursive(destinationDirectoryPath);

            // delete license agreement if it wasnt supposed to be copied
            if (!copyLicenseAgreementToSite)
            {
                if (File.Exists(@"\\" + Config.ApplicationSettings.AsentiaContentDestinationRootPath + @"\_config\" + destinationPortalName + @"\License.html"))
                { File.Delete(@"\\" + Config.ApplicationSettings.AsentiaContentDestinationRootPath + @"\_config\" + destinationPortalName + @"\License.html"); }
            }
        }
        #endregion

        #region STEP 17: CopyWarehouse
        /// <summary>
        /// Copies the warehouse/[portal] folder to warehouse/[cloned portal], updates folders named for object ids, and copies content packages from warehouse root.
        /// </summary>
        /// <param name="destinationPortalName">Destination Asentia Hostname</param>
        /// <param name="sourcePortalName">Source Asentia Hostname</param>
        /// <param name="destinationIdAccount">Destination Asentia Account ID</param>
        /// <param name="sourceIdAccount">Source Asentia Account ID</param>
        /// <param name="destinationDBName">Destination DB Name</param>
        /// <param name="destinationDBServer">Destination DB Server</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyWarehouse(string destinationPortalName, string sourcePortalName, int destinationIdAccount, int sourceIdAccount, string destinationDBServer, string destinationDBName)
        {
            // get the object id mappings from the database portion of the clone process
            DataTable clonePortalIdMappings = new DataTable();
            clonePortalIdMappings = Library.DatabaseServer.GetIDMappings(destinationDBServer, destinationDBName, "warehouse");

            // format source directory path            
            string sourceDirectoryPath = Config.ApplicationSettings.AsentiaContentSourceRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + sourcePortalName;
            DirectoryInfo sourceDir = new DirectoryInfo(sourceDirectoryPath);

            // format destination directory path            
            string destinationDirectoryPath = Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + destinationPortalName;

            // check if source directory exists
            if (!sourceDir.Exists)
            { throw new AsentiaException(String.Format(_GlobalResources.TheSourceDirectoryDoesNotExistOrCouldNotBeFound, sourceDirectoryPath)); }

            // copy the source directory into the destination directory
            if (sourceDirectoryPath != destinationDirectoryPath)
            { Utility.CopyDirectory(sourceDirectoryPath, destinationDirectoryPath, true, true); }

            #region Portal Specific Files
            // get the destination portal's sub directories
            var destPortalSubDirectories = Directory.GetDirectories(destinationDirectoryPath);

            // loop through the sub directories, we need to copy items that are inside of folders
            // "documents"
            foreach (string subFolder in destPortalSubDirectories)
            {
                // get the name of the sub-folder we're currently looking at
                string subFolderName = new DirectoryInfo(subFolder).Name;

                // get the object sub folders
                var objectSubFolders = Directory.GetDirectories(subFolder);

                switch (subFolderName)
                {
                    case "documents":

                        // loop through the sub directories, we need to copy items that are inside of folders
                        // "courses", "groups", "learningPaths", "lesson", "users"
                        foreach (string documentSubFolder in objectSubFolders)
                        {
                            // get the name of the sub-folder we're currently looking at
                            string documentSubFolderName = new DirectoryInfo(documentSubFolder).Name;

                            // get the object sub folders
                            var documentObjectSubFolders = Directory.GetDirectories(documentSubFolder);

                            switch (documentSubFolderName)
                            {
                                case "course":

                                    // loop through sub folders of the document object folder, and rename its folder ids for new object mappings
                                    foreach (string documentObjectSubFolder in documentObjectSubFolders)
                                    {
                                        // this is the object sub folder id that we need to rename
                                        string oldObjectId = new DirectoryInfo(documentObjectSubFolder).Name;

                                        // get the new id for this object from our data mappings table
                                        DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'courses'");

                                        if (resultRow.Length > 0)
                                        {
                                            // get the new id
                                            string newObjectId = resultRow[0]["destinationID"].ToString();

                                            // move the old folder to the new folder
                                            if (Directory.Exists(documentObjectSubFolder))
                                            {
                                                string newObjectFolderPath = documentObjectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                                Directory.Move(documentObjectSubFolder, newObjectFolderPath);
                                            }
                                        }
                                        else
                                        {
                                            // delete the directory if there is no object with that id
                                            if (Directory.Exists(documentObjectSubFolder))
                                            {
                                                Utility.EmptyOldFolderItems(documentObjectSubFolder, 0, true);
                                                Directory.Delete(documentObjectSubFolder);
                                            }
                                        }
                                    }

                                    break;
                                case "group":

                                    // loop through sub folders of the document object folder, and rename its folder ids for new object mappings
                                    foreach (string documentObjectSubFolder in documentObjectSubFolders)
                                    {
                                        // this is the object sub folder id that we need to rename
                                        string oldObjectId = new DirectoryInfo(documentObjectSubFolder).Name;

                                        // get the new id for this object from our data mappings table
                                        DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'groups'");

                                        if (resultRow.Length > 0)
                                        {
                                            // get the new id
                                            string newObjectId = resultRow[0]["destinationID"].ToString();

                                            // move the old folder to the new folder
                                            if (Directory.Exists(documentObjectSubFolder))
                                            {
                                                string newObjectFolderPath = documentObjectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                                Directory.Move(documentObjectSubFolder, newObjectFolderPath);
                                            }
                                        }
                                        else
                                        {
                                            // delete the directory if there is no object with that id
                                            if (Directory.Exists(documentObjectSubFolder))
                                            {
                                                Utility.EmptyOldFolderItems(documentObjectSubFolder, 0, true);
                                                Directory.Delete(documentObjectSubFolder);
                                            }
                                        }
                                    }

                                    break;
                                case "learningpath":

                                    // loop through sub folders of the document object folder, and rename its folder ids for new object mappings
                                    foreach (string documentObjectSubFolder in documentObjectSubFolders)
                                    {
                                        // this is the object sub folder id that we need to rename
                                        string oldObjectId = new DirectoryInfo(documentObjectSubFolder).Name;

                                        // get the new id for this object from our data mappings table
                                        DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'learningPaths'");

                                        if (resultRow.Length > 0)
                                        {
                                            // get the new id
                                            string newObjectId = resultRow[0]["destinationID"].ToString();

                                            // move the old folder to the new folder
                                            if (Directory.Exists(documentObjectSubFolder))
                                            {
                                                string newObjectFolderPath = documentObjectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                                Directory.Move(documentObjectSubFolder, newObjectFolderPath);
                                            }
                                        }
                                        else
                                        {
                                            // delete the directory if there is no object with that id
                                            if (Directory.Exists(documentObjectSubFolder))
                                            {
                                                Utility.EmptyOldFolderItems(documentObjectSubFolder, 0, true);
                                                Directory.Delete(documentObjectSubFolder);
                                            }
                                        }
                                    }

                                    break;
                                case "lesson":

                                    // loop through sub folders of the document object folder, and rename its folder ids for new object mappings
                                    foreach (string documentObjectSubFolder in documentObjectSubFolders)
                                    {
                                        // this is the object sub folder id that we need to rename
                                        string oldObjectId = new DirectoryInfo(documentObjectSubFolder).Name;

                                        // get the new id for this object from our data mappings table
                                        DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'lesson'");

                                        if (resultRow.Length > 0)
                                        {
                                            // get the new id
                                            string newObjectId = resultRow[0]["destinationID"].ToString();

                                            // move the old folder to the new folder
                                            if (Directory.Exists(documentObjectSubFolder))
                                            {
                                                string newObjectFolderPath = documentObjectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                                Directory.Move(documentObjectSubFolder, newObjectFolderPath);
                                            }
                                        }
                                        else
                                        {
                                            // delete the directory if there is no object with that id
                                            if (Directory.Exists(documentObjectSubFolder))
                                            {
                                                Utility.EmptyOldFolderItems(documentObjectSubFolder, 0, true);
                                                Directory.Delete(documentObjectSubFolder);
                                            }
                                        }
                                    }

                                    break;
                                case "user":

                                    // loop through sub folders of the document object folder, and rename its folder ids for new object mappings
                                    foreach (string documentObjectSubFolder in documentObjectSubFolders)
                                    {
                                        // this is the object sub folder id that we need to rename
                                        string oldObjectId = new DirectoryInfo(documentObjectSubFolder).Name;

                                        // get the new id for this object from our data mappings table
                                        DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'users'");

                                        if (resultRow.Length > 0)
                                        {
                                            // get the new id
                                            string newObjectId = resultRow[0]["destinationID"].ToString();

                                            // move the old folder to the new folder
                                            if (Directory.Exists(documentObjectSubFolder))
                                            {
                                                string newObjectFolderPath = documentObjectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                                Directory.Move(documentObjectSubFolder, newObjectFolderPath);
                                            }
                                        }
                                        else
                                        {
                                            // delete the directory if there is no object with that id
                                            if (Directory.Exists(documentObjectSubFolder))
                                            {
                                                Utility.EmptyOldFolderItems(documentObjectSubFolder, 0, true);
                                                Directory.Delete(documentObjectSubFolder);
                                            }
                                        }
                                    }

                                    break;
                                default:
                                    break;
                            }
                        }

                        break;
                    default:
                        break;
                }
            }

            // remove __TEMP__ from object folder names            
            _RemoveTempPostfixFromFoldersRecursive(destinationDirectoryPath);
            #endregion

            #region Content Package Files
            // get root warehouse path
            string rootWarehousePath = Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\");

            // get the site ids of the source and destination
            DataRow[] result = clonePortalIdMappings.Select("object = 'site'");
            int sourceIdSite = 0;
            int destinationIdSite = 0;

            if (result.Length > 0)
            {
                sourceIdSite = Convert.ToInt32(result[0]["sourceID"]);
                destinationIdSite = Convert.ToInt32(result[0]["destinationID"]);
            }

            // get a list of the content packages for the portal
            if (sourceIdSite > 0 && destinationIdSite > 0)
            {
                string directorySearchPattern = sourceIdAccount.ToString() + "-" + sourceIdSite.ToString() + "-*";
                string[] directoryList = Directory.GetDirectories(rootWarehousePath, directorySearchPattern);

                foreach (string contentPackageDirectory in directoryList)
                {
                    if (Directory.Exists(contentPackageDirectory))
                    {
                        string newContentPackageFolderPath = contentPackageDirectory.Replace("\\" + sourceIdAccount.ToString() + "-" + sourceIdSite.ToString() + "-", "\\" + destinationIdAccount.ToString() + "-" + destinationIdSite.ToString() + "-");
                        Utility.CopyDirectory(contentPackageDirectory, newContentPackageFolderPath, true, true);
                    }
                }
            }
            #endregion
        }
        #endregion

        #region STEP 18: CopyLog
        /// <summary>
        /// Copies the _log/[portal] folder to _log/[cloned portal] and updates folders named for object ids.
        /// </summary>
        /// <param name="destinationPortalName">Destination Asentia Hostname</param>
        /// <param name="sourcePortalName">Source Asentia Hostname</param>
        /// <param name="destinationDBName">Destination DB Name</param>
        /// <param name="destinationDBServer">Destination DB Server</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void CopyLog(string destinationPortalName, string sourcePortalName, string destinationDBServer, string destinationDBName, bool copyLicenseAgreementToSite)
        {
            // get the object id mappings from the database portion of the clone process
            DataTable clonePortalIdMappings = new DataTable();
            clonePortalIdMappings = Library.DatabaseServer.GetIDMappings(destinationDBServer, destinationDBName, "_log");

            // format source directory path            
            string sourceDirectoryPath = Config.ApplicationSettings.AsentiaContentSourceRootPath + SitePathConstants.LOG.Replace("/", "\\") + sourcePortalName;
            DirectoryInfo sourceDir = new DirectoryInfo(sourceDirectoryPath);

            // format destination directory path            
            string destinationDirectoryPath = Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.LOG.Replace("/", "\\") + destinationPortalName;

            // check if source directory exists
            if (!sourceDir.Exists)
            { throw new AsentiaException(String.Format(_GlobalResources.TheSourceDirectoryDoesNotExistOrCouldNotBeFound, sourceDirectoryPath)); }

            // copy the source directory into the destination directory
            if (sourceDirectoryPath != destinationDirectoryPath)
            { Utility.CopyDirectory(sourceDirectoryPath, destinationDirectoryPath, true, true); }

            // get the destination portal's sub directories
            var destPortalSubDirectories = Directory.GetDirectories(destinationDirectoryPath);

            // loop through the sub directories, we need to copy items that are inside of folders
            // "lessondata"
            foreach (string subFolder in destPortalSubDirectories)
            {
                // get the name of the sub-folder we're currently looking at
                string subFolderName = new DirectoryInfo(subFolder).Name;

                // get the object sub folders
                var objectSubFolders = Directory.GetDirectories(subFolder);

                switch (subFolderName)
                {
                    case "lessondata":

                        // loop through sub folders of the object folder, and rename its folder ids for new object mappings
                        foreach (string objectSubFolder in objectSubFolders)
                        {
                            // this is the object sub folder id that we need to rename
                            string oldObjectId = new DirectoryInfo(objectSubFolder).Name;

                            // get the new id for this object from our data mappings table
                            DataRow[] resultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(oldObjectId) + " AND object = 'users'");

                            if (resultRow.Length > 0)
                            {
                                // get the new id
                                string newObjectId = resultRow[0]["destinationID"].ToString();

                                // move the old folder to the new folder
                                if (Directory.Exists(objectSubFolder))
                                {
                                    string newObjectFolderPath = objectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "__TEMP__");
                                    Directory.Move(objectSubFolder, newObjectFolderPath);

                                    // move the lesson data directories into new lesson data directories                                                                        
                                    var lessonDataObjectSubFolders = Directory.GetDirectories(newObjectFolderPath);

                                    // loop through sub folders of the object folder, and rename its folder ids for new object mappings
                                    foreach (string lessonDataObjectSubFolder in lessonDataObjectSubFolders)
                                    {
                                        // this is the object sub folder id that we need to rename
                                        string lessonDataOldObjectId = new DirectoryInfo(lessonDataObjectSubFolder).Name;

                                        // get the new id for this object from our data mappings table
                                        DataRow[] lessonDataResultRow = clonePortalIdMappings.Select("sourceID = " + Convert.ToInt32(lessonDataOldObjectId) + " AND object = 'datalesson'");

                                        if (lessonDataResultRow.Length > 0)
                                        {
                                            // get the new id
                                            string lessonDataNewObjectId = lessonDataResultRow[0]["destinationID"].ToString();

                                            // move the old folder to the new folder
                                            if (Directory.Exists(lessonDataObjectSubFolder))
                                            {
                                                string lessonDataNewObjectFolderPath = lessonDataObjectSubFolder.Replace("\\" + lessonDataOldObjectId, "\\" + lessonDataNewObjectId + "__TEMP__");
                                                Directory.Move(lessonDataObjectSubFolder, lessonDataNewObjectFolderPath);
                                            }
                                        }
                                        else
                                        {
                                            // delete the directory if there is no object with that id
                                            if (Directory.Exists(lessonDataObjectSubFolder))
                                            {
                                                Utility.EmptyOldFolderItems(lessonDataObjectSubFolder, 0, true);
                                                Directory.Delete(lessonDataObjectSubFolder);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // delete the directory if there is no object with that id
                                if (Directory.Exists(objectSubFolder))
                                {
                                    Utility.EmptyOldFolderItems(objectSubFolder, 0, true);
                                    Directory.Delete(objectSubFolder);
                                }
                            }
                        }

                        break;
                    default:
                        break;
                }
            }

            // remove __TEMP__ from object folder names            
            _RemoveTempPostfixFromFoldersRecursive(destinationDirectoryPath);
        }
        #endregion

        #region _RemoveTempPostfixFromFoldersRecursive
        /// <summary>
        /// Recursively removes the "__TEMP__" postfix from the folders that were renamed for IDs.
        /// </summary>
        /// <param name="hostname">hostname</param>
        /// <returns></returns>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        private void _RemoveTempPostfixFromFoldersRecursive(string rootDirectoryPath)
        {
            var subDirectories = Directory.GetDirectories(rootDirectoryPath);

            foreach (string subDirectory in subDirectories)
            {
                // recurse down into lower level sub directories
                _RemoveTempPostfixFromFoldersRecursive(subDirectory);

                // rename the directory 
                string subDirectoryName = new DirectoryInfo(subDirectory).Name;

                if (subDirectoryName.Contains("__TEMP__"))
                {
                    string newDirectoryName = subDirectoryName.Replace("__TEMP__", "");
                    string newDirectoryPath = subDirectory.Replace(subDirectoryName, newDirectoryName);

                    try
                    { Directory.Move(subDirectory, newDirectoryPath); }
                    catch (Exception ex)
                    { throw new Exception("Error occured moving source: " + subDirectory + " to destination: " + newDirectoryPath + " | " + ex.Message); }
                }
            }
        }
        #endregion

        #region DeleteSite
        /// <summary>
        /// Deletes the site data from the database / filesystem if an error occurs during the clone.
        /// </summary>
        /// <param name="idSite">Asentia Site ID</param>
        /// <param name="destinationSiteHostname">Asentia Site Hostname</param>
        /// <param name="idAccount">Account ID</param>
        /// <param name="databaseServer">Asentia Database Server Name</param>
        /// <param name="databaseName">Asentia Database Name</param>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public void DeleteSite(int idSite, string destinationSiteHostname, int idAccount, string databaseServer, string databaseName)
        {
            DataTable sites = new DataTable();
            sites.Columns.Add("id", typeof(int));
            sites.Rows.Add(idSite);

            // database object for deleting site data
            AsentiaDatabase databaseObject = new AsentiaDatabase(databaseServer, databaseName, null, null, "InquisiqToAsentiaMigration", 3600, true);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Sites", sites, SqlDbType.Structured, null, ParameterDirection.Input);

            // database object for deleting account to domain alias link
            AsentiaDatabase customerManagerDatabaseObject = new AsentiaDatabase(DatabaseType.CustomerManager, 3600);

            customerManagerDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            customerManagerDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            customerManagerDatabaseObject.AddParameter("@idCallerAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            customerManagerDatabaseObject.AddParameter("@domainAliass", destinationSiteHostname, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                // delete site data
                databaseObject.ExecuteNonQuery("[Site.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // delete account to domain alias link
                customerManagerDatabaseObject.ExecuteNonQuery("[AccountToDomainAliasLink.Delete]", true);

                returnCode = (DBReturnValue)Convert.ToInt32(customerManagerDatabaseObject.Command.Parameters["@Return_Code"].Value);
                errorDescriptionCode = customerManagerDatabaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // clean up the file system

                // _config/[hostname]
                if (Directory.Exists(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + destinationSiteHostname))
                {
                    Utility.EmptyOldFolderItems(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + destinationSiteHostname, 0, true);
                    Directory.Delete(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + destinationSiteHostname, true);
                }

                // _log/[hostname]
                if (Directory.Exists(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.LOG.Replace("/", "\\") + destinationSiteHostname))
                {
                    Utility.EmptyOldFolderItems(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.LOG.Replace("/", "\\") + destinationSiteHostname, 0, true);
                    Directory.Delete(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.LOG.Replace("/", "\\") + destinationSiteHostname, true);
                }

                // warehouse/[hostname]
                if (Directory.Exists(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + destinationSiteHostname))
                {
                    Utility.EmptyOldFolderItems(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + destinationSiteHostname, 0, true);
                    Directory.Delete(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + destinationSiteHostname, true);
                }

                // warehouse content packages
                string directorySearchPattern = idAccount.ToString() + "-" + idSite.ToString() + "-*";
                string[] directoryList = Directory.GetDirectories(Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\"), directorySearchPattern);

                foreach (string directory in directoryList)
                { Directory.Delete(directory, true); }
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
                customerManagerDatabaseObject.Dispose();
            }
        }
        #endregion

        #region UpdateLicenseAgreement
        /// <summary>
        /// Updates the license agreement when the hostname is updated on the clone from template page.
        /// </summary>
        /// <param name="hostname">hostname</param>
        /// <returns></returns>
        [Services.WebMethod(EnableSession = true)]
        [ScriptServices.ScriptMethod(ResponseFormat = ScriptServices.ResponseFormat.Json)]
        public string UpdateLicenseAgreement(string hostname)
        {
            if (File.Exists(Config.ApplicationSettings.AsentiaContentSourceRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + hostname + "\\License.html"))
            { return File.ReadAllText(Config.ApplicationSettings.AsentiaContentSourceRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + hostname + "\\License.html"); }
            else
            { return null; }
        }
        #endregion
    }
}
