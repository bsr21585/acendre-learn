﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using defaultSystem = System;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Threading;
using System.Configuration;
using System.Web.Configuration;

namespace Asentia.CustomerManager.Pages.ClonePortal
{
    public class Default : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel ActionsPanel;
        public Panel PageInstructionsPanel;
        public Panel DatabaseFormContainer;
        public UpdatePanel DataBaseFormUpdatePanel;
        #endregion

        #region Private Properties

        private Button _SaveButton = new Button();
        private Button _CancelButton = new Button();

        private DropDownList _SourceDatabaseName;
        private DropDownList _SourceServerName;
        private DropDownList _DestinationDatabaseName;
        private DropDownList _DestinationServerName;
        private DropDownList _SourceSiteId;

        private TextBox _NewPortalName;

        private CheckBox _SameNameAsSource;

        private Library.DatabaseServer _ObjectSourceDatabaseServer;
        private Library.DatabaseServer _ObjectDestinationDatabaseServer;

        private Panel _SourcePanel = new Panel();
        private Panel _DestinationPanel = new Panel();

        private DataTable _ClonePortalIdMappings = new DataTable();

        private ModalPopup _LoadingModal;
        private HiddenField _CloneStatusHiddenField;
        private Button _HiddenModalButton;


        #endregion


        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {

            // build the breadcrumb
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResourcesCHETU1.FormToCloneSourcePortalToDestinationPortal, true);

            // builds page controls
            this._BuildControls();

        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            base.OnPreRender(e);

            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.CustomerManager.Pages.ClonePortal.Default.js");

        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            string breadCrumbPageTitle;
            string pageTitle;

            breadCrumbPageTitle = _GlobalResourcesCHETU1.ClonePortal;
            pageTitle = breadCrumbPageTitle;

            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.AsentiaCustomerManager, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));

            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_CLONEPORTAL,
                                                                              ImageFiles.EXT_PNG,
                                                                              true)
                                                                             );
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds page controls
        /// </summary>
        private void _BuildControls()
        {

            //this.DataBaseFormUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            // build the user form
            this._BuildUserForm();

            // build the form actions panel
            this._BuildActionsPanel();

            // building loading modal popup
            this._BuildLoadingModal();

        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "FormActionsPanel";

            // save button
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";
            this._SaveButton.Text = _GlobalResourcesCHETU1.CreateClonedPortal;
            this._SaveButton.Attributes.Add("onclick", "hiddenModalClick();");
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);

            // hidden button to trigger clone
            this._HiddenModalButton = new Button();
            this._HiddenModalButton.ID = "HiddenModalButton";
            this._HiddenModalButton.Text = "Hidden Button";
            this._HiddenModalButton.Style.Add("display", "none");
            this._HiddenModalButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._HiddenModalButton);

            // hidden field to show clone status
            this._CloneStatusHiddenField = new HiddenField();
            this._CloneStatusHiddenField.ID = "CloneStatusHiddenField";
            this._CloneStatusHiddenField.Value = "Not Started";
            this.ActionsPanel.Controls.Add(this._CloneStatusHiddenField);
        }

        #endregion

        #region _BuildUserForm
        /// <summary>
        /// Builds the clone portal form.
        /// </summary>
        private void _BuildUserForm()
        {

            this._SourcePanel.ID = "SourcePanel";

            #region BindSourceDetails

            List<Control> sourceControlsContainer = new List<Control>();

            // source Server Name feild
            // label
            Label sourceServerNameLabel = new Label();
            sourceServerNameLabel.ID = "sourceServerNameLabel";
            sourceServerNameLabel.Text = _GlobalResourcesCHETU1.Server + ":";
            sourceControlsContainer.Add(sourceServerNameLabel);

            // dropdown
            this._SourceServerName = new DropDownList();
            this._SourceServerName.ID = "SourceDatabaseServer";
            this._SourceServerName.DataSource = Library.DatabaseServer.IdsAndNamesForDatabaseServerList();
            this._SourceServerName.DataValueField = "idDatabaseServer";
            this._SourceServerName.DataTextField = "ServerName";
            this._SourceServerName.DataBind();
            this._SourceServerName.AutoPostBack = true;
            this._SourceServerName.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
            this._SourceServerName.SelectedIndexChanged += _SourceServerName_SelectedIndexChanged;
            sourceControlsContainer.Add(this._SourceServerName);

            // source Database field
            // label
            Label sourceDatabaseNameLabel = new Label();
            sourceDatabaseNameLabel.ID = "SourceDatabaseNameLabel";
            sourceDatabaseNameLabel.Text = _GlobalResourcesCHETU1.Database + ":";
            sourceControlsContainer.Add(sourceDatabaseNameLabel);

            // dropdown
            this._SourceDatabaseName = new DropDownList();
            this._SourceDatabaseName.ID = "SourceDatabaseName";
            this._SourceDatabaseName.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
            this._SourceDatabaseName.AutoPostBack = true;
            this._SourceDatabaseName.SelectedIndexChanged += _SourceDatabaseName_SelectedIndexChanged;
            this._SourceDatabaseName.Enabled = false;

            sourceControlsContainer.Add(this._SourceDatabaseName);

            // source IdSite field
            // label
            Label sourceSiteIdLabel = new Label();
            sourceSiteIdLabel.ID = "SourceSiteIdLabel";
            sourceSiteIdLabel.Text = _GlobalResourcesCHETU1.Portal + ":";
            sourceControlsContainer.Add(sourceSiteIdLabel);

            // dropdown
            this._SourceSiteId = new DropDownList();
            this._SourceSiteId.ID = "SourceSiteId";
            this._SourceSiteId.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
            this._SourceSiteId.Enabled = false;

            sourceControlsContainer.Add(this._SourceSiteId);



            // add control in source panel
            this._SourcePanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Source",
                                                                                          _GlobalResourcesCHETU1.Source,
                                                                                          sourceControlsContainer,
                                                                                          false,
                                                                                          true,
                                                                                          true)
                                                                                         );

            #endregion


            #region BindDestinationDetails

            this._DestinationPanel.ID = "DestinationPanel";

            List<Control> destinationControlsContainer = new List<Control>();

            // destination Server Name feild
            // label 
            Label destinationServerNameLabel = new Label();
            destinationServerNameLabel.ID = "DestinationServerNameLabel";
            destinationServerNameLabel.Text = _GlobalResourcesCHETU1.Server;
            destinationControlsContainer.Add(destinationServerNameLabel);

            // dropdown
            this._DestinationServerName = new DropDownList();
            this._DestinationServerName.ID = "DestinationDatabaseServer";

            this._DestinationServerName.DataSource = Library.DatabaseServer.IdsAndNamesForDatabaseServerList();
            this._DestinationServerName.DataValueField = "idDatabaseServer";
            this._DestinationServerName.DataTextField = "ServerName";
            this._DestinationServerName.DataBind();
            this._DestinationServerName.AutoPostBack = true;
            this._DestinationServerName.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
            this._DestinationServerName.SelectedIndexChanged += _DestinationServerName_SelectedIndexChanged;

            destinationControlsContainer.Add(this._DestinationServerName);

            // destination Database feild

            // label
            Label destinationDatabaseNameLabel = new Label();
            destinationDatabaseNameLabel.ID = "DestinationDatabaseNameLabel";
            destinationDatabaseNameLabel.Text = _GlobalResourcesCHETU1.Database;
            destinationControlsContainer.Add(destinationDatabaseNameLabel);

            // dropdown
            this._DestinationDatabaseName = new DropDownList();
            this._DestinationDatabaseName.ID = "DestinationDatabaseName";
            this._DestinationDatabaseName.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
            this._DestinationDatabaseName.AutoPostBack = true;
            this._DestinationDatabaseName.SelectedIndexChanged += _DestinationDatabaseName_SelectedIndexChanged;
            this._DestinationDatabaseName.Enabled = false;

            destinationControlsContainer.Add(this._DestinationDatabaseName);

            // destination new portal fields
            // label
            Label newPortalLabel = new Label();
            newPortalLabel.ID = "NewPortalLabel";
            newPortalLabel.Text = _GlobalResourcesCHETU1.NewPortalName + ":";

            // textbox
            this._NewPortalName = new TextBox();
            this._NewPortalName.ID = "NewPortalName_Field";
            this._NewPortalName.CssClass = "InputShort";
            this._NewPortalName.MaxLength = 100;

            // checkbox
            this._SameNameAsSource = new CheckBox();
            this._SameNameAsSource.ID = "SameNameAsSource";
            this._SameNameAsSource.Text = _GlobalResourcesCHETU1.SameNameAsSource;

            destinationControlsContainer.Add(newPortalLabel);
            destinationControlsContainer.Add(this._NewPortalName);
            destinationControlsContainer.Add(this._SameNameAsSource);

            // add controls in DestinationPanel
            this._DestinationPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Destination",
                                                          _GlobalResourcesCHETU1.Destination,
                                                          destinationControlsContainer,
                                                             false,
                                                             true,
                                                             true
                                                             ));

            // adding controls container panel to DatabaseFormContainer
            this.DatabaseFormContainer.Controls.Add(this._SourcePanel);
            this.DatabaseFormContainer.Controls.Add(this._DestinationPanel);

            #endregion

        }

        #endregion


        #region SourceDetails_SelectedIndexChanged

        /// <summary>
        /// Performs Select Index Change action on source dropdown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void _SourceServerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (_SourceServerName.SelectedIndex > 0)
                {
                    // pulling database name and id 
                    this._SourceDatabaseName.DataSource = Library.DatabaseServer.GetDatabaseList(Convert.ToInt32(this._SourceServerName.SelectedValue));
                    this._SourceDatabaseName.DataValueField = "idAccount";
                    this._SourceDatabaseName.DataTextField = "databaseName";
                    this._SourceDatabaseName.DataBind();
                    this._SourceDatabaseName.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);

                    this._SourceDatabaseName.Enabled = true;
                }
                else
                {
                    this._SourceServerName.SelectedIndex = 0;
                    this._SourceDatabaseName.Items.Clear();
                    this._SourceDatabaseName.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
                    this._SourceDatabaseName.SelectedIndex = 0;

                    this._SourceSiteId.Items.Clear();
                    this._SourceSiteId.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
                    this._SourceSiteId.SelectedIndex = 0;

                    this._SourceDatabaseName.Enabled = false;
                    this._SourceSiteId.Enabled = false;
                }

            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.ApplyErrorMessageToFieldErrorPanel(this._SourcePanel, "Source", _GlobalResources.Server + " " + _GlobalResourcesCHETU1.NotFoundOnLinkServer);
            }
        }

        /// <summary>
        /// Selected index change for populating idSite form source database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _SourceDatabaseName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (_SourceDatabaseName.SelectedIndex > 0)
                {
                    // pulling database server's netwok name 
                    this._ObjectSourceDatabaseServer = new Library.DatabaseServer(Convert.ToInt32(this._SourceServerName.SelectedValue));

                    // pulling host name by server name and database name
                    this._SourceSiteId.DataSource = Library.DatabaseServer.GetIdSiteFromDatabase(this._ObjectSourceDatabaseServer.NetworkName, this._SourceDatabaseName.SelectedItem.Text);
                    this._SourceSiteId.DataValueField = "idSite";
                    this._SourceSiteId.DataTextField = "hostname";
                    this._SourceSiteId.DataBind();
                    this._SourceSiteId.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
                    this._SourceSiteId.ClientIDMode = ClientIDMode.AutoID;

                    this._SourceSiteId.Enabled = true;
                    this._SourceDatabaseName.Enabled = true;
                }

                else
                {
                    this._SourceDatabaseName.SelectedIndex = 0;

                    this._SourceSiteId.Items.Clear();
                    this._SourceSiteId.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
                    this._SourceSiteId.SelectedIndex = 0;

                    this._SourceSiteId.Enabled = false;
                }

            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.ApplyErrorMessageToFieldErrorPanel(this._SourcePanel, "Source", _GlobalResources.DatabaseName + " " + _GlobalResourcesCHETU1.NotFoundOnLinkServer);
            }
        }

        #endregion


        #region DestinationDetails_SelectedIndexChanged

        /// <summary>
        /// Selected index change for populating database name form database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void _DestinationServerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (_DestinationServerName.SelectedIndex > 0)
                {
                    this._DestinationDatabaseName.DataSource = Library.DatabaseServer.GetDatabaseList(Convert.ToInt32(this._DestinationServerName.SelectedValue));
                    this._DestinationDatabaseName.DataValueField = "idAccount";
                    this._DestinationDatabaseName.DataTextField = "databaseName";
                    this._DestinationDatabaseName.DataBind();
                    this._DestinationDatabaseName.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);

                    this._DestinationDatabaseName.Enabled = true;
                }
                else
                {
                    this._DestinationDatabaseName.Items.Clear();
                    this._DestinationDatabaseName.Items.Insert(0, _GlobalResourcesCHETU1.PleaseSelect);
                    this._DestinationDatabaseName.SelectedIndex = 0;

                    this._DestinationDatabaseName.Enabled = false;
                }

            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.ApplyErrorMessageToFieldErrorPanel(this._DestinationPanel, "Destination", _GlobalResources.ServerName + " " + _GlobalResourcesCHETU1.NotFoundOnLinkServer);
            }
        }


        /// <summary>
        /// Validate selected server or database exists on link server  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DestinationDatabaseName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (_DestinationDatabaseName.SelectedIndex > 0)
                {
                    // pulling database server's netwok name 
                    this._ObjectDestinationDatabaseServer = new Library.DatabaseServer(Convert.ToInt32(this._DestinationServerName.SelectedValue));

                    // As soon as database is selected from dropdown control, this will be checking the existence of the database on selected destination server 
                    // if the selected database not exist on the server(could be linked server) then will throw the exception
                    // this will fetch idSite and host name, which we dont want at this place; so not grabbing them
                    // Method exist on Customer mgr DB because the destination portal is not created yet
                    Library.DatabaseServer.GetIdSiteFromDatabase(this._ObjectDestinationDatabaseServer.NetworkName, this._DestinationDatabaseName.SelectedItem.Text);

                    // if source and destination database are same then host name must be unique 
                    if ((this._SourceServerName.SelectedValue.Equals(this._DestinationServerName.SelectedValue)) && (this._SourceDatabaseName.SelectedValue.Equals(this._DestinationDatabaseName.SelectedValue)))
                    {
                        this._SameNameAsSource.Enabled = false;
                        this._SameNameAsSource.Checked = false;
                        this._NewPortalName.Enabled = true;
                    }
                    else
                    {
                        this._SameNameAsSource.Enabled = true;
                    }
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.ApplyErrorMessageToFieldErrorPanel(this._DestinationPanel, "Destination", _GlobalResources.DatabaseName + " " + _GlobalResourcesCHETU1.NotFoundOnLinkServer);
            }
        }

        #endregion


        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/");
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click, clone portal by given details.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {

            if (this._CloneStatusHiddenField.Value == "Success")
            {
                // Close the loading modal
                this._LoadingModal.HideModal();

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResourcesCHETU1.PortalHasBeenClonedSuccessfully, false);
            }
            else
            {
                // Close the loading modal
                this._LoadingModal.HideModal();

                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResourcesCHETU1.ErrorHasOccurredWhileCloneingTheDatabase + ": " + this._CloneStatusHiddenField.Value, true);
            }
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;


            // source Server Name 
            if (this._SourceServerName.SelectedIndex == 0)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Source", _GlobalResourcesCHETU1.PleaseSelectSourceServer);
            }

            // source Database Name 
            if (this._SourceDatabaseName.SelectedIndex == 0)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Source", _GlobalResourcesCHETU1.PleaseSelectSourceDatabase);
            }

            // source idSite or portal Name 
            if (this._SourceSiteId.SelectedIndex == 0)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Source", _GlobalResourcesCHETU1.PleaseSelectSourcePortal);
            }



            // destination Server Name 
            if (this._DestinationServerName.SelectedIndex == 0)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Destination", _GlobalResourcesCHETU1.PleaseSelectDestinationServer);
            }

            // destination Database Name 
            if (this._DestinationDatabaseName.SelectedIndex == 0)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.DatabaseFormContainer, "Destination", _GlobalResourcesCHETU1.PleaseSelectDestinationDatabase);
            }

            // destination New Portal Name 
            if ((!this._SameNameAsSource.Checked) && String.IsNullOrWhiteSpace(this._NewPortalName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._DestinationPanel, "Destination", _GlobalResourcesCHETU1.PleaseEnterNewPortalName);
            }

            // host name must be unique if source database and destination datablase are same
            if ((this._SourceServerName.SelectedValue.Equals(this._DestinationServerName.SelectedValue)) && (this._SourceDatabaseName.SelectedValue.Equals(this._DestinationDatabaseName.SelectedValue)))
            {
                if (!this._SameNameAsSource.Checked && this._SourceSiteId.SelectedItem.Text.Equals(this._NewPortalName.Text.Trim()))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._DestinationPanel, "Destination", _GlobalResourcesCHETU1.NewPortalNameMustBeUnique);
                }
            }



            return isValid;
        }
        #endregion


        #region _CloneFullPortalConfigObjects

        /// <summary>
        /// Clone _config folder objects for destination portal for full copy.
        /// </summary>
        /// <param name="clonePortalIdMappings">clone Portal IdMappings table</param>

        private void _CloneFullPortalConfigObjects(DataTable clonePortalIdMappings)
        {

            String destinationPortalName;

            // format source Director path            
            string sourceDirectoryPath = Config.ApplicationSettings.AsentiaContentSourceRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + this._SourceSiteId.SelectedItem.Text;
            DirectoryInfo sourceDir = new DirectoryInfo(sourceDirectoryPath);

            // check for host name is new host name or same name as sourse 
            if (this._SameNameAsSource.Checked)
            {
                destinationPortalName = this._SourceSiteId.SelectedItem.Text;
            }
            else
            {
                destinationPortalName = this._NewPortalName.Text;
            }

            // format destination Director path            
            string destinationDirectoryPath = Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.CONFIG.Replace("/", "\\") + destinationPortalName;

            // check Source directory existence
            if (!sourceDir.Exists)
            { throw new AsentiaException(String.Format(_GlobalResources.TheSourceDirectoryDoesNotExistOrCouldNotBeFound, sourceDirectoryPath)); }

            // check for replacing the host or copying of source site into destination site
            if (!this._SameNameAsSource.Checked && sourceDirectoryPath != destinationDirectoryPath)
            {
                // copy source hostname into destination hostname
                Utility.CopyDirectory(sourceDirectoryPath, destinationDirectoryPath, true, true);
            }

            // get the sub directories path of destination portal
            var destPortalSubDirectories = Directory.GetDirectories(destinationDirectoryPath);
            foreach (String subFolder in destPortalSubDirectories)
            {
                // getting subDirectorie's name inside destination folder, like 'course', 'user' etc.
                String objectName = new DirectoryInfo(subFolder).Name;
                // do not rename 'templates' folder
                if (objectName != "templates")
                {
                    // get the path list of directories of "subFolder".
                    var objectSubDirectories = Directory.GetDirectories(subFolder);
                    foreach (String objectSubFolder in objectSubDirectories)
                    {
                        // folder name would be id of object
                        String oldObjectId = new DirectoryInfo(objectSubFolder).Name;

                        // getting the resultant row from clonePortalIdMappings datatable
                        DataRow[] resultRow = clonePortalIdMappings.Select("oldId =" + Convert.ToInt32(oldObjectId) + " AND objectName = '" + objectName + "'");
                        if (resultRow.Length > 0)
                        {
                            // fetching the corresponding mapped id with the old source id
                            string newObjectId = resultRow[0]["newId"].ToString();

                            // replace "[oldId]" folder name with "[newId]" and suffix "-temp" 
                            String pathForNewid = objectSubFolder.Replace("\\" + oldObjectId, "\\" + newObjectId + "-temp");
                            //rename "[oldId]" folder to "[newId]-temp" folder.
                            Directory.Move(objectSubFolder, pathForNewid);


                            #region RenameHomeWorkAssignmentAndTaskSubFolders

                            // enter into users sub folders and check it has "homework assignment" and "task" subFolders
                            if (objectName == "users")
                            {

                                // find subDirectory of users directory  like "1-temp"
                                var userSubDirectories = Directory.GetDirectories(pathForNewid);
                                foreach (String userSubDirectorySubFolder in userSubDirectories)
                                {
                                    // getting userSubDirectories's subdirectories  name like "HomeworkAssignments" and "Tasks"
                                    String userSubObjectName = new DirectoryInfo(userSubDirectorySubFolder).Name;

                                    if (userSubObjectName.Contains("HomeworkAssignments") || userSubObjectName.Contains("Tasks"))
                                    {
                                        // getting list of HomeworkAssignments and Tasks subdirectories  
                                        var homeWorkAssignmentsAndTasksDirectories = Directory.GetDirectories(userSubDirectorySubFolder);
                                        foreach (String homeWorkAssignmentsAndTasksSubDirectories in homeWorkAssignmentsAndTasksDirectories)
                                        {
                                            // "homework assignment" and "task" id's return from clone portal on the basis of DataLeson
                                            String homeworkAssignmentAndTasksObjectName = "dataLesson";

                                            // getting the folder name from the folder path
                                            String oldHWAandTasksSubObjecId = new DirectoryInfo(homeWorkAssignmentsAndTasksSubDirectories).Name;

                                            // getting the resultant row for "homework assignment" and "task" from clonePortalIdMappings datatable
                                            DataRow[] homeWorkAssignmentsAndTasksResultRow = clonePortalIdMappings.Select("oldId =" + Convert.ToInt32(oldHWAandTasksSubObjecId) + " AND objectName =  '" + homeworkAssignmentAndTasksObjectName + "'");

                                            // rename the destination HWA and Task sub folder as per mapping table
                                            if (homeWorkAssignmentsAndTasksResultRow.Length > 0)
                                            {
                                                // getting new id which is generated by clone portal
                                                string newHWAandTasksResultId = homeWorkAssignmentsAndTasksResultRow[0]["newId"].ToString();

                                                // replace "[oldId]" in folder name to "[newId]" with suffix "-temp" in new folder name
                                                String pathForNewHWAandTasksId = homeWorkAssignmentsAndTasksSubDirectories.Replace("\\" + oldHWAandTasksSubObjecId, "\\" + newHWAandTasksResultId + "-temp");

                                                //rename "[oldId]" folder to "[newId]-temp" folder.
                                                Directory.Move(homeWorkAssignmentsAndTasksSubDirectories, pathForNewHWAandTasksId);

                                            }
                                        }
                                    }

                                }

                            }
                            #endregion

                        }
                    }
                }
            }

            #region remove the "-temp" suffux from each folder name

            var portalSubDirectoriesTemp = Directory.GetDirectories(destinationDirectoryPath);


            foreach (String subFolder in portalSubDirectoriesTemp)
            {
                String objectName = new DirectoryInfo(subFolder).Name;


                if (objectName != "templates")
                {
                    // get the subdirectories of folder
                    var objectSubDirectories = Directory.GetDirectories(subFolder);
                    foreach (String subFolderId in objectSubDirectories)
                    {
                        // remove the -temp suffix from the destination.
                        String pathForNewid = subFolderId.Replace("-temp", "");

                        // rename destination folder with removed '-temp' string name
                        if (!subFolderId.Equals(pathForNewid))
                        {
                            Directory.Move(subFolderId, pathForNewid);
                        }

                        #region replace '-temp' from Home Work Assignment and Task sub folders
                        if (objectName == "users")
                        {

                            // find subDirectory of folder like "1-temp"
                            var userSubDirectories = Directory.GetDirectories(pathForNewid);
                            foreach (String userSubDirectorySubFolder in userSubDirectories)
                            {
                                // getting sub folder name like "HomeworkAssignments"
                                String userSubObjectName = new DirectoryInfo(userSubDirectorySubFolder).Name;
                                if (userSubObjectName.Contains("HomeworkAssignments") || userSubObjectName.Contains("Tasks"))
                                {
                                    var homeWorkAssignmentsAndTasksDirectories = Directory.GetDirectories(userSubDirectorySubFolder);
                                    foreach (String homeWorkAssignmentsAndTasksSubDirectories in homeWorkAssignmentsAndTasksDirectories)
                                    {
                                        // remove the -temp suffix from the destination.
                                        String pathForNewHWAandTasksId = homeWorkAssignmentsAndTasksSubDirectories.Replace("-temp", "");
                                        if (!homeWorkAssignmentsAndTasksSubDirectories.Equals(pathForNewHWAandTasksId))
                                        {
                                            // rename the directory
                                            Directory.Move(homeWorkAssignmentsAndTasksSubDirectories, pathForNewHWAandTasksId);
                                        }
                                    }
                                }
                            }

                        }
                        #endregion
                    }
                }

            }

            #endregion

        }

        #endregion

        #region _CloneFullPortalWarehouseObjects

        /// <summary>
        ///  Rename or copy warehouse objects for cloneing full portal 
        /// </summary>
        /// <param name="clonePortalIdMappings">clone portal id Mapping table</param>

        private void _CloneFullPortalWarehouseObjects(DataTable clonePortalIdMappings)
        {
            String destinationPortalName;

            // format sourse portal path            
            string sourceDirectoryPath = Config.ApplicationSettings.AsentiaContentSourceRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + this._SourceSiteId.SelectedItem.Text;
            DirectoryInfo sourceDir = new DirectoryInfo(sourceDirectoryPath);

            // check host name is new host name or same name as sourse 
            if (this._SameNameAsSource.Checked)
            { destinationPortalName = this._SourceSiteId.SelectedItem.Text; }
            else
            { destinationPortalName = this._NewPortalName.Text; }

            // format destination portal path            
            string destinationDirectoryPath = Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + destinationPortalName;

            // format destination warehouse path            
            string destinationWarehouseDoucmentRoot = Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\") + destinationPortalName + "\\documents";
            DirectoryInfo sourceDirDocuments = new DirectoryInfo(destinationWarehouseDoucmentRoot);

            // check portal folder existence 
            if (!sourceDir.Exists)
            { throw new AsentiaException(String.Format(_GlobalResources.TheSourceDirectoryDoesNotExistOrCouldNotBeFound, sourceDirectoryPath)); }


            #region Rename content package start

            // format warehouse path            
            String contentPackagePath = Config.ApplicationSettings.AsentiaContentDestinationRootPath + SitePathConstants.WAREHOUSE.Replace("/", "\\");

            // get the subdirectories of warehouse
            var contentPackagesDirectories = Directory.GetDirectories(contentPackagePath);
            foreach (String contentPackageFolderPath in contentPackagesDirectories)
            {
                String contentPackageFolderName = new DirectoryInfo(contentPackageFolderPath).Name;

                // find the index of first dash and second dash to replace the content package name
                int indexOfFirstDash = contentPackageFolderName.IndexOf('-');
                int indexOfSecondDash = contentPackageFolderName.IndexOf('-', contentPackageFolderName.IndexOf('-') + 1);

                // get the new created site id from data table
                DataRow[] result = clonePortalIdMappings.Select("objectName = 'site'");
                if (result.Length > 0)
                {
                    // fetching the new idSite and old idSite.
                    int newSiteId = Convert.ToInt32(result[0]["newId"]);
                    int oldSiteId = Convert.ToInt32(result[0]["oldId"]);

                    // concatinating IdAccount and idSite for replacing the content package name.
                    String newIdAccountAndidSite = Convert.ToInt32(this._DestinationDatabaseName.SelectedValue) + "-" + newSiteId;
                    String oldAccountAndSiteId = Convert.ToInt32(this._SourceDatabaseName.SelectedValue) + "-" + oldSiteId;

                    // check that only for content package folder 
                    if (indexOfSecondDash > 0)
                    {
                        // take the part of content package name that needs to be replaced
                        String idAccountAndidSite = contentPackageFolderName.Substring(0, indexOfSecondDash).ToString();

                        if (oldAccountAndSiteId.Equals(idAccountAndidSite))
                        {
                            String newContentPackageFolderName = contentPackageFolderName.Substring(0, 0) + newIdAccountAndidSite + contentPackageFolderName.Substring(indexOfSecondDash);

                            // replace the accont id and site id within the content package folder name string
                            String newContentPackageFolderPath = contentPackageFolderPath.Replace("\\" + contentPackageFolderName, "\\" + newContentPackageFolderName);


                            // check is it to replace the source site with destination or to copy source into the destination portal
                            if (!this._SameNameAsSource.Checked && sourceDirectoryPath != destinationDirectoryPath)
                            {
                                // copy source portal's content packages into destination portal 
                                Utility.CopyDirectory(contentPackageFolderPath, newContentPackageFolderPath, true, true);
                            }
                            else
                            {
                                //rename the source portal's content packages into destination portal 
                                Directory.Move(contentPackageFolderPath, newContentPackageFolderPath);
                            }

                        }
                    }
                }

            }

            #endregion

            // check is it to replace the source site with destination or to copy source into the destination portal
            if (!this._SameNameAsSource.Checked && sourceDirectoryPath != destinationDirectoryPath)
            {
                // copy source hostname folder into destination portal
                Utility.CopyDirectory(sourceDirectoryPath, destinationDirectoryPath, true, true);
            }

            // check document folder existence inside warehouse 
            if (sourceDirDocuments.Exists)
            {
                // get the subdirectories of document folder.
                var warehouseDocSubDirectories = Directory.GetDirectories(destinationWarehouseDoucmentRoot);

                foreach (String docSubFolder in warehouseDocSubDirectories)
                {
                    String objectName = new DirectoryInfo(docSubFolder).Name;

                    // get the subdirectories of each folder
                    var objectSubDirectories = Directory.GetDirectories(docSubFolder);
                    foreach (String subFolderId in objectSubDirectories)
                    {
                        String oldObjectId = new DirectoryInfo(subFolderId).Name;

                        //fetch corresponding row for source id from mapping table
                        DataRow[] resultRow = clonePortalIdMappings.Select("oldId =" + Convert.ToInt32(oldObjectId) + " AND objectName like '" + objectName + "%'");

                        if (resultRow.Length > 0)
                        {
                            //fetch mapped new id in the row
                            string newObjectId = resultRow[0]["newId"].ToString();

                            // replace "[oldId]" folder name by "[newId]-temp"
                            String pathForNewid = subFolderId.Replace("\\" + oldObjectId, "\\" + newObjectId + "-temp");

                            // rename "[oldId]" folder by "[newId]-temp" folder.
                            Directory.Move(subFolderId, pathForNewid);
                        }

                    }
                }
            }

            #region remove the "-temp" suffux from each folder name

            var portalSubDirectoriesTemp = Directory.GetDirectories(destinationWarehouseDoucmentRoot);

            foreach (String subFolder in portalSubDirectoriesTemp)
            {
                String objectName = new DirectoryInfo(subFolder).Name;

                // get the subdirectories of folder
                var objectSubDirectories = Directory.GetDirectories(subFolder);
                foreach (String subFolderId in objectSubDirectories)
                {
                    // remove the -temp suffix from the destination path .
                    String pathForNewid = subFolderId.Replace("-temp", "");
                    if (!subFolderId.Equals(pathForNewid))
                    {
                        // replace "-temp" from directory name
                        Directory.Move(subFolderId, pathForNewid);
                    }
                }
            }
            #endregion
        }

        #endregion

        #region _BuildLoadingModal
        /// <summary>
        /// Builds loading or saving information, with loader, modal popup.
        /// </summary>
        private void _BuildLoadingModal()
        {
            this._LoadingModal = new ModalPopup("LoadingModal");
            this._LoadingModal.OverrideLoadImagesFromCustomerManager = true;

            // set modal properties
            this._LoadingModal.Type = ModalPopupType.Information;
            this._LoadingModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CLONEPORTAL,
                                                                                          ImageFiles.EXT_PNG,
                                                                                          true
                                                                                          );

            this._LoadingModal.HeaderText = _GlobalResourcesCHETU1.ClonePortal;
            this._LoadingModal.TargetControlID = "SaveButton";
            this._LoadingModal.SubmitButton.Visible = false;
            this._LoadingModal.CloseButton.Visible = false;
            this._LoadingModal.ShowCloseIcon = false;
            this._LoadingModal.ShowLoadingPlaceholder = true;

            this.ActionsPanel.Controls.Add(this._LoadingModal);
            //this._DataBaseFormUpdateProgress.Controls.Add(this._LoadAndSaveClonePortalDataModal);

        }
        #endregion
    }

}