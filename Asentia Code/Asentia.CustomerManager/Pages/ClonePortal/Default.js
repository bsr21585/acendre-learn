﻿//handles the same name as source checkbox actions

$(document).ready(function () {

    $(".LoadingPlaceholder").css("display", "block");

    //$("#LoadAndSaveClonePortalDataModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper").text("");
    $("#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper").text("");


});

// handle the check box functionality
function pageLoad() {

    if ($("#SameNameAsSource").prop("checked") == true) {

        $("#NewPortalName_Field").attr('disabled', true);
        $("#SameNameAsSource").attr("checked", "true");
        $("#NewPortalName_Field").val('');
    }

    $("#SameNameAsSource").click(function () {
        if ($("#SameNameAsSource").prop("checked") == true) {

            $("#NewPortalName_Field").val('');
            $("#NewPortalName_Field").attr('disabled', true);

        }
        else if ($("#SameNameAsSource").prop("checked") == false) {

            $("#NewPortalName_Field").removeAttr('disabled');
            $("#NewPortalName_Field").val('');
        }
    });

}

// click hidden button to open modal popup
function hiddenModalClick() {

    // MAKE SURE ALL FIELDS ARE FILLED IN
    if (($('#SourceDatabaseServer:selected').val() == "Please Select") || ($('#SourceDatabaseName:selected').val() == "Please Select") ||
        ($('#SourceSiteId:selected').val() == "Please Select") || ($('#DestinationDatabaseServer:selected').val() == "Please Select") ||
        ($('#DestinationDatabaseName:selected').val() == "Please Select") || (($('#NewPortalName_Field').val() == "") && (!$('#SameNameAsSource').is(':checked')))) {

        // fields are not filled in, so terminate clone
        $('#CloneStatusHiddenField').val("Please Ensure All Fields are Filled In");
        $('#HiddenModalButton').click();
    } else {

        // fields are filled in, so continue with clone
        $('#CloneStatusHiddenField').val("Success");
        $(".LoadingPlaceholder").css("display", "block");

        $("#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper").text("");

        // call the cloning procedures
        clone();
    }
}


var sourceDBServer;
var sourceDBName;
var sourceHostname;
var idSiteSource;

var destinationHostname;
var destinationDBServer;
var destinationDBName;
var idSiteDestination;
var destinationIdAccount;
var sourceIdAccount;


// starts the cloning process
function clone() {

    sourceDBServer = $('#SourceDatabaseServer option:selected').text();
    sourceDBName = $('#SourceDatabaseName option:selected').text();
    sourceHostname = $('#SourceSiteId option:selected').text();
    idSiteSource = $('#SourceSiteId').val();

    destinationDBServer = $('#DestinationDatabaseServer option:selected').text();
    destinationDBName = $('#DestinationDatabaseName option:selected').text();
    sourceIdAccount = $('#SourceDatabaseName').val();
    destinationIdAccount = $('#DestinationDatabaseName').val();
    if ($('#SameNameAsSource').is(':checked')) {
        destinationHostname = $('#SourceSiteId option:selected').text();
    } else {
        destinationHostname = $('#NewPortalName_Field').val();
    }

    $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 1 of 17: Copying Site Data");
    copySite();
}



// STEP 1 - Copy Site Data
function copySite() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopySite",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: true, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: 0, idAccount: destinationIdAccount, destinationHostname: destinationHostname }),
        success: function (data) {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Site Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 2 of 17: Copying Catalog and Course Data");
            idSiteDestination = data.d;
            copyCatalogCourse();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate clone
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
        }
    });

}

// STEP 2 - Copy Catalog / Course Data
function copyCatalogCourse() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyCatalogCourse",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Catalog and Course Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 3 of 17: Copying Group Data");
            copyGroup();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 3 - Copy Group Data
function copyGroup() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyGroup",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Group Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 4 of 17: Copying User Data");
            copyUser();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 4 - Copy User Data
function copyUser() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyUser",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("User Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 5 of 17: Copying Content and Resource Data");
            copyContentResource();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 5 - Copy Content / Resource Data
function copyContentResource() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyContentResource",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: true, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idAccount: destinationIdAccount, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Content and Resource Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 6 of 17: Copying Instructor Led Training Data");
            copyILT();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 6 - Copy ILT Data
function copyILT() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyILT",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: true, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Instructor Led Training Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 7 of 17: Copying Role and Rule Data");
            copyRoleRule();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}


// STEP 7 - Copy Role / Rule Data
function copyRoleRule() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyRoleRule",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: true, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Role and Rule Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 8 of 17: Copying Coupon Code Data");
            copyCouponCode();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 8 - Copy Coupon Code Data
function copyCouponCode() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyCouponCode",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Coupon Code Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 9 of 17: Copying Certificate and Certification Data");
            copyCertificateCertification();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 9 - Copy Certificate / Certification Data
function copyCertificateCertification() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyCertificateCertification",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: true, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Certificate and Certification Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 10 of 17: Copying Discussion Feed Data");
            copyDiscussionFeed();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 10 - Copy Discussion Feed Data
function copyDiscussionFeed() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyDiscussionFeed",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Discussion Feed Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 11 of 17: Copying Enrollment Data");
            copyEnrollment();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 11 - Copy Enrollment Data
function copyEnrollment() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyEnrollment",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Enrollment Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 12 of 17: Copying Document Repository and Event Email Data");
            copyDocumentRepositoryEventEmail();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 12 - Copy Document Repository / Event Email Data
function copyDocumentRepositoryEventEmail() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyDocumentRepositoryEventEmail",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ fullCopyFlag: true, sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Document Repository and Event Email Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 13 of 17: Copying Data Tables");
            copyData();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 13 - Copy Data Items
function copyData() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyData",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Data Table Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 14 of 17: Copying Purchase and Transaction Data");
            copyPurchaseTransaction();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 14 - Copy Purchase / Transaction Data
function copyPurchaseTransaction() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyPurchaseTransaction",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Purchase and Transaction Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 15 of 17: Copying Report Data");
            copyReport();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 15 - Copy Report Data
function copyReport() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyReport",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ sourceDBServer: sourceDBServer, sourceDBName: sourceDBName, idSiteSource: idSiteSource, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, idSiteDestination: idSiteDestination }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Report Data Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 16 of 17: Copying Config Files");
            copyConfig();

        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}


// STEP 16 - Copy Config Items
function copyConfig() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyConfig",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ destinationPortalName: destinationHostname, sourcePortalName: sourceHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, copyLicenseAgreementToSite: true }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Config Files Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 17 of 18: Copying Warehouse Files");
            copyWarehouse();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 17 - Copy Warehouse Items
function copyWarehouse() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyWarehouse",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ destinationPortalName: destinationHostname, sourcePortalName: sourceHostname, destinationIdAccount: destinationIdAccount, sourceIdAccount: sourceIdAccount, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Warehouse Files Copy Complete");
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Step 18 of 18: Copying Log Files");
            copyLog();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// STEP 18 - Copy Log Items
function copyLog() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/CopyLog",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ destinationPortalName: destinationHostname, sourcePortalName: sourceHostname, destinationDBServer: destinationDBServer, destinationDBName: destinationDBName, copyLicenseAgreementToSite: true }),
        success: function () {
            $('#LoadingModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper').text("Log Files Copy Complete");
            $('#HiddenModalButton').click();
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
            deleteSite();
        }
    });

}

// deletes site if an error occurs
function deleteSite() {
    $.ajax({
        type: "POST",
        url: "/clonePortal/ClonePortal.asmx/DeleteSite",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ idSite: idSiteDestination, destinationSiteHostname: destinationHostname, idAccount: destinationIdAccount, databaseServer: destinationDBServer, databaseName: destinationDBName }),
        success: function () {
            // do nothing
        },
        error: function (xhr, status, error) {
            // Display the error and terminate migration
            $('#CloneStatusHiddenField').val(xhr.responseText);
            $('#HiddenModalButton').click();
        }
    });

}







