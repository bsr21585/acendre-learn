﻿using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Asentia.Common;
using AjaxControlToolkit;
using System;
using Asentia.Controls;
using System.Collections;
using System.IO;
using System.Globalization;
using System.Data;

namespace Asentia.CustomerManager.Pages._MasterPage
{
    public class CustomerManager : MasterPage
    {
        #region Properties
        public HtmlForm MasterForm;
        public static Panel PageContainer;
        public Literal GlobalJavascriptVariables;
        public ContentPlaceHolder HeadPlaceholder;

        // masthead elements
        public Panel MastheadContainer;
        public Panel MastheadColumnLeft;
        public Panel MastheadColumnRight;

        // content elements
        public Panel ContentContainer;
        public Panel MasterContentLeft;
        public Panel MasterContentRight;
        public Panel MasterContentMiddle;

        // breadcrumb elements
        public Panel BreadcrumbContainer;
        public Panel BreadcrumbColumnLeft;
        public Panel BreadcrumbColumnRight;

        // page content placeholders
        public ContentPlaceHolder BreadcrumbContentPlaceholder;
        public ContentPlaceHolder SideContentPlaceholder1;
        public ContentPlaceHolder PageContentPlaceholder;
        public ContentPlaceHolder SideContentPlaceholder2;

        // footer elements
        public Panel FooterContainer;
        public Panel FooterColumnLeft;
        public Panel FooterColumnRight;

        public ToolkitScriptManager asm;

        // "GLOBAL OBJECTS"
        //
        // ANY "GLOBAL OBJECTS" SHOULD BE DECLARED HERE AS "public static" SO 
        // THAT THEY HAVE APPLICATION-WIDE SCOPE. A "GLOBAL OBJECT" IS DEFINED 
        // AS AN OBJECT THAT IS POPULATED FROM THE DATABASE, WHO'S PROPERTIES 
        // ARE NEEDED THROUGHOUT THE APPLICATION. 

        // global site
        public static AsentiaSite_ReadOnly GlobalSiteObject;

        // login button id
        public static string LogInButtonId = null;

        // log in button control exists
        public static bool LogInButtonControlExists = false;
        #endregion

        #region Private Properties

        // parameter that gets set to true if the master page instansiating this class is HomePage.master
        // if true, a different masthead will be loaded
        private bool _IsHomePage = false;
        #endregion

        #region Methods

        #region Page_Load
        /// <summary>
        /// Page load.
        /// </summary>
        /// <param name="sender">Page</param>
        /// <param name="e">Event arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // give the master page an ID so one is not auto-generated
            this.ID = "Master";

            // call functions to build page
            this._IncludeCSSFiles();
            this._BuildMasthead();
            this._AttachControlsToContainers();

            if (!Page.IsPostBack)
            { }
        }
        #endregion
        #endregion

        #region Private Methods

        #region _IncludeCSSFiles
        /// <summary>
        /// Method to include all css files.
        /// </summary>
        private void _IncludeCSSFiles()
        {
            // tag format for the stylesheet link
            const string STYLE_SHEET_LINK_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />\r\n";

            // style sheets that aren't site configurable - located in the root css folder
            ArrayList styleSheets = new ArrayList();

            // add stylesheets
            styleSheets.Add("Reset.css");
            styleSheets.Add("Layout.css");
            styleSheets.Add("Inputs.css");
            styleSheets.Add("Modal.css");
            styleSheets.Add("Grid.css");
            styleSheets.Add("LoginForm.css");
            styleSheets.Add("CMMainMenu.css");
            styleSheets.Add("_KNOTIOCUSTOM.css"); // This is temporary for Knotio styling changes
            styleSheets.Add("_CGIPPLECUSTOM.css"); // This is temporary for Chris's styling changes

            // attach references to stylesheets
            foreach (string stylesheet in styleSheets)
            {
                string pathToStylesheet = MapPathSecure(SitePathConstants.CM_CSS + stylesheet);

                if (File.Exists(pathToStylesheet))
                {
                    Page.Header.Controls.Add(
                        new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.CM_CSS + stylesheet)))
                    );
                }
            }
        }
        #endregion


        #region _AttachControlsToContainers()
        /// <summary>
        /// Method to load the controls template and build containers with the control elements.
        /// </summary>
        private void _AttachControlsToContainers()
        {
            this._PoweredByTag();
            this._FooterTag();
        }
        #endregion

        #region Control Builder Methods
        // Add builder methods for new controls here. 
        // Use the _GlobalControlTemplate method as a template.

        #region _MastheadBackground
        /// <summary>
        /// Applies masthead background-image and background-color styling to MastheadContainer if those site params exist.
        /// </summary>
        private void _MastheadBackground()
        {
            if (this._IsHomePage)
            {
                // masthead background image
                string backgroundImageFileName = GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_BGIMAGE);
                string backgroundImagePath = String.Empty;
                bool backgroundImageFileExists = false;

                if (!String.IsNullOrEmpty(backgroundImageFileName))
                {
                    if (backgroundImageFileName != null)
                    {
                        string pathToLogoImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + backgroundImageFileName);

                        if (File.Exists(pathToLogoImage))
                        {
                            backgroundImageFileExists = true;
                            backgroundImagePath = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + backgroundImageFileName);
                        }
                    }


                    // attach background-image style to masthead container
                    if (backgroundImageFileExists)
                    {
                        this.MastheadContainer.Style.Add("background-image", "url('" + backgroundImagePath + "')");
                        this.MastheadContainer.Style.Add("background-repeat", "repeat-x");
                    }
                }

                // masthead background color
                string backgroundColor = GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_BGCOLOR);

                if (!String.IsNullOrEmpty(backgroundColor))
                {
                    // attach background-color style to masthead container
                    this.MastheadContainer.Style.Add("background-color", backgroundColor);
                }
            }
            else
            {
                // masthead background color
                string backgroundColor = GlobalSiteObject.ParamString(SiteParamConstants.APPLICATION_MASTHEAD_BGCOLOR);

                if (!String.IsNullOrEmpty(backgroundColor))
                {
                    // attach background-color style to masthead container
                    this.MastheadContainer.Style.Add("background", backgroundColor);
                }
            }
        }
        #endregion



        #region _HomePageLogoImage
        /// <summary>
        /// Home Page logo image control.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _HomePageLogoImage(XmlNode elementNode, Panel container)
        {
            // id information
            string id = "HomePageLogoImage";

            // declare control and set its id
            HyperLink logoImageHyperLink = new HyperLink();
            logoImageHyperLink.ID = id;
            logoImageHyperLink.NavigateUrl = "/";

            // build control
            string logoImageFileName = GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_LOGO);
            bool logoImageFileExists = false;

            if (!String.IsNullOrEmpty(logoImageFileName))
            {
                if (logoImageFileName != null)
                {
                    string pathToLogoImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);

                    if (File.Exists(pathToLogoImage))
                    {
                        logoImageFileExists = true;
                        logoImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);
                    }
                }

                // attach control to container
                if (logoImageFileExists)
                { container.Controls.Add(logoImageHyperLink); }
            }
        }
        #endregion

        #region _HomePageSecondaryImage
        /// <summary>
        /// Home Page secondary image control.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _HomePageSecondaryImage(XmlNode elementNode, Panel container)
        {
            // id information
            string id = "HomePageSecondaryImage";
            string specifiedId = elementNode.Attributes["id"].Value;
            if (!String.IsNullOrEmpty(specifiedId))
            { id = specifiedId; }

            // declare control and set its id
            Image secondaryImage = new Image();
            secondaryImage.ID = id;

            // build control
            string secondaryImageFileName = GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_SECONDARYIMAGE);
            bool secondaryImageFileExists = false;

            if (!String.IsNullOrEmpty(secondaryImageFileName))
            {
                if (AsentiaSessionState.SiteHostname != "default")
                {
                    if (secondaryImageFileName != null)
                    {
                        string pathToSecondaryImage = MapPathSecure(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);

                        if (File.Exists(pathToSecondaryImage))
                        {
                            secondaryImageFileExists = true;
                            secondaryImage.ImageUrl = ResolveUrl(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);
                        }
                        else
                        {
                            pathToSecondaryImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);

                            if (File.Exists(pathToSecondaryImage))
                            {
                                secondaryImageFileExists = true;
                                secondaryImage.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);
                            }
                        }
                    }
                }
                else
                {
                    if (secondaryImageFileName != null)
                    {
                        string pathToSecondaryImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);

                        if (File.Exists(pathToSecondaryImage))
                        {
                            secondaryImageFileExists = true;
                            secondaryImage.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);
                        }
                    }
                }

                // attach control to container
                if (secondaryImageFileExists)
                { container.Controls.Add(secondaryImage); }
            }
        }
        #endregion

        #region _ApplicationIconImage
        /// <summary>
        /// Application icon image control.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _ApplicationIconImage(XmlNode elementNode, Panel container)
        {
            // id information
            string id = "ApplicationIconImage";
            string specifiedId = elementNode.Attributes["id"].Value;
            if (!String.IsNullOrEmpty(specifiedId))
            { id = specifiedId; }

            // declare control and set its id
            HyperLink iconImageHyperLink = new HyperLink();
            iconImageHyperLink.ID = id;
            iconImageHyperLink.NavigateUrl = "/";

            // build control
            string iconImageFileName = GlobalSiteObject.ParamString(SiteParamConstants.APPLICATION_MASTHEAD_ICON);
            bool iconImageFileExists = false;

            if (!String.IsNullOrEmpty(iconImageFileName))
            {
                if (AsentiaSessionState.SiteHostname != "default")
                {
                    if (iconImageFileName != null)
                    {
                        string pathToIconImage = MapPathSecure(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);

                        if (File.Exists(pathToIconImage))
                        {
                            iconImageFileExists = true;
                            iconImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);
                        }
                        else
                        {
                            pathToIconImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);

                            if (File.Exists(pathToIconImage))
                            {
                                iconImageFileExists = true;
                                iconImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);
                            }
                        }
                    }
                }
                else
                {
                    if (iconImageFileName != null)
                    {
                        string pathToIconImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);

                        if (File.Exists(pathToIconImage))
                        {
                            iconImageFileExists = true;
                            iconImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);
                        }
                    }
                }

                // attach control to container
                if (iconImageFileExists)
                { container.Controls.Add(iconImageHyperLink); }
            }
        }
        #endregion

        #region _ApplicationLogoImage
        /// <summary>
        /// Application logo image control.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _ApplicationLogoImage(XmlNode elementNode, Panel container)
        {
            // id information
            string id = "ApplicationLogoImage";
            string specifiedId = elementNode.Attributes["id"].Value;
            if (!String.IsNullOrEmpty(specifiedId))
            { id = specifiedId; }

            // declare control and set its id
            HyperLink logoImageHyperLink = new HyperLink();
            logoImageHyperLink.ID = id;
            logoImageHyperLink.CssClass = "MobileHidden";
            logoImageHyperLink.NavigateUrl = "/";

            // build control
            string logoImageFileName = GlobalSiteObject.ParamString(SiteParamConstants.APPLICATION_MASTHEAD_LOGO);
            bool logoImageFileExists = false;

            if (!String.IsNullOrEmpty(logoImageFileName))
            {
                if (AsentiaSessionState.SiteHostname != "default")
                {
                    if (logoImageFileName != null)
                    {
                        string pathToLogoImage = MapPathSecure(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);

                        if (File.Exists(pathToLogoImage))
                        {
                            logoImageFileExists = true;
                            logoImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);
                        }
                        else
                        {
                            pathToLogoImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);

                            if (File.Exists(pathToLogoImage))
                            {
                                logoImageFileExists = true;
                                logoImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);
                            }
                        }
                    }
                }
                else
                {
                    if (logoImageFileName != null)
                    {
                        string pathToLogoImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);

                        if (File.Exists(pathToLogoImage))
                        {
                            logoImageFileExists = true;
                            logoImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);
                        }
                    }
                }

                // attach control to container
                if (logoImageFileExists)
                { container.Controls.Add(logoImageHyperLink); }
            }
        }
        #endregion


        #region _BuildMasthead
        /// <summary>
        /// Control that displays a "Mast Head".
        /// </summary>
        private void _BuildMasthead()
        {
            //add aditional controls to mast head(Rightk)
            this._SettingsControl();
            this._LanguageSelectorControl();
        }
        #endregion

        #region _LanguageSelectorControl
        /// <summary>
        /// Control that displays a language selector.
        /// </summary>
        private void _LanguageSelectorControl()
        {
            if (!HttpContext.Current.Request.Url.ToString().Contains("/unknown/") && !HttpContext.Current.Request.Url.ToString().Contains("/offline/"))
            {
                // id information
                string id = "LanguageSelectorControl";

                Panel languageSelectorContainer = new Panel();
                languageSelectorContainer.ID = id;

                Image localizationImage = new Image();
                localizationImage.ID = id + "Image";
                localizationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM_LOCALIZATION,
                                                                    ImageFiles.EXT_PNG, true);
                localizationImage.CssClass = "SmallIcon";
                languageSelectorContainer.Controls.Add(localizationImage);

                Panel languageSelectorMenuItemContainer = new Panel();
                languageSelectorMenuItemContainer.ID = "LanguageSelectorMenuItemContainer";

                ArrayList installedLanguages = new ArrayList();

                // en-US is the default language, so it's already installed, add it
                installedLanguages.Add("en-US");

                // loop through each installed language based on the language folders
                // contained in bin
                foreach (string directory in Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN)))
                {
                    // loop through each language available to the site and add it
                    // to the array list for drop-down items
                    if (Path.GetFileName(directory).Contains("-"))
                    { installedLanguages.Add(Path.GetFileName(directory)); }
                }

                // loop through array list and add each language to the drop-down
                foreach (string installedLanguage in installedLanguages)
                {
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(installedLanguage);

                    Panel languageSelectorMenuItem = new Panel();
                    languageSelectorMenuItem.ID = "LanguageSelectorMenuItem_" + cultureInfo.Name;

                    LinkButton language = new LinkButton();
                    language.ID = "LanguageSelectorMenuItemLink_" + cultureInfo.Name;

                    if (AsentiaSessionState.UserCulture == cultureInfo.Name)
                    { language.CssClass = "bold"; }

                    language.Text = cultureInfo.NativeName;
                    language.Command += _LanguageSelector_Change_Session;
                    language.CommandArgument = cultureInfo.Name;

                    languageSelectorMenuItem.Controls.Add(language);
                    languageSelectorMenuItemContainer.Controls.Add(languageSelectorMenuItem);
                }

                languageSelectorContainer.Controls.Add(languageSelectorMenuItemContainer);

                this.MastheadColumnRight.Controls.Add(languageSelectorContainer);
            }
        }
        #endregion

        #region _SettingsControl
        /// <summary>
        /// Control that displays the settings control (log-in/out link etc).
        /// </summary>
        private void _SettingsControl()
        {
            if (!HttpContext.Current.Request.Url.ToString().Contains("/unknown/") && !HttpContext.Current.Request.Url.ToString().Contains("/offline/"))
            {
                Panel settingsControl = new Panel();

                if (AsentiaSessionState.IdAccountUser > 0 || AsentiaSessionState.IdSiteUser > 0)
                {
                    // id information
                    string id = "SettingsControl";
                    settingsControl.ID = id;

                    // settings image
                    Image settingsImage = new Image();
                    settingsImage.ID = id + "Image";
                    settingsImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SETTINGS,
                                                                    ImageFiles.EXT_PNG, true);
                    settingsImage.CssClass = "SmallIcon";
                    settingsControl.Controls.Add(settingsImage);

                    // settings control menu item container
                    Panel settingsControlMenuItemContainer = new Panel();
                    settingsControlMenuItemContainer.ID = "SettingsControlMenuItemContainer";

                    // logged in tag
                    LoggedInTag loggedInTag = new LoggedInTag();
                    loggedInTag.ID = id + "LoggedInTag";
                    settingsControlMenuItemContainer.Controls.Add(loggedInTag);

                    // logout link
                    string logoutLinkId = id + "LogoutLink";

                    Panel logoutLinkContainer = new Panel();
                    logoutLinkContainer.ID = logoutLinkId + "Container";

                    LinkButton logoutLink = new LinkButton();
                    logoutLink.ID = logoutLinkId + "Link";
                    logoutLink.Text = _GlobalResources.Logout;
                    logoutLink.Command += new CommandEventHandler(_LogoutLink_Command);

                    logoutLinkContainer.Controls.Add(logoutLink);
                    settingsControlMenuItemContainer.Controls.Add(logoutLinkContainer);

                    settingsControl.Controls.Add(settingsControlMenuItemContainer);
                }
                //else // if user is NOT logged in
                //{
                //    if (AsentiaSessionState.IdSiteUser == 0)
                //    {
                //        // id information
                //        string id = "SettingsControlForLogin";
                //        settingsControl.ID = id;

                //        // login image
                //        Image loginImage = new Image();
                //        loginImage.ID = id + "Image";
                //        loginImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                //                                                     ImageFiles.EXT_PNG);
                //        loginImage.CssClass = "SmallIcon";
                //        settingsControl.Controls.Add(loginImage);

                //        // set the global login "button" id property to the id of this login link
                //        LogInButtonId = settingsControl.ID;

                //        // set the LogInButtonControlExists property to true so that the pages know the login button exists
                //        LogInButtonControlExists = true;
                //    }
                //}

                this.MastheadColumnRight.Controls.Add(settingsControl);
            }
        }
        #endregion

        #region _LogoutLink_Command
        /// <summary>
        /// Logs the user out.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _LogoutLink_Command(object sender, CommandEventArgs e)
        {
            AsentiaAuthenticatedPage.UpdateUserSessionExpire(null);
            AsentiaSessionState.EndSession();
        }
        #endregion

        #region _PoweredByTag
        /// <summary>
        /// Control that displays the "Asentia" build and copyright information.
        /// </summary>
        private void _PoweredByTag()
        {
            // id information
            string id = "PoweredByTag";

            // declare container for control
            Panel poweredByTagContainer = new Panel();
            poweredByTagContainer.ID = id + "Container";

            // declare control and set its id
            Localize poweredByTag = new Localize();
            poweredByTag.ID = id;

            // build control
            poweredByTag.Text = String.Format(_GlobalResources.PoweredByAsentiaBuild, "1.00.0000", AsentiaSessionState.UserTimezoneCurrentLocalTime.Year.ToString());

            // attach control to container
            poweredByTagContainer.Controls.Add(poweredByTag);
            FooterColumnLeft.Controls.Add(poweredByTagContainer);
        }
        #endregion

        #region _FooterTag
        /// <summary>
        /// Control that displays the site's branding.
        /// </summary>
        private void _FooterTag()
        {
            // id information
            string id = "FooterTag";

            // declare container for control
            Panel siteBrandingTagContainer = new Panel();
            siteBrandingTagContainer.ID = id + "Container";

            // company name 
            HtmlGenericControl companyName = new HtmlGenericControl("p");
            companyName.ID = id + "CompanyName";
            companyName.InnerHtml = "Chetu";// GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_COMPANYNAME);

            // email
            HtmlGenericControl email = new HtmlGenericControl("p");
            email.ID = id + "Email";
            //email.InnerHtml = "<a href=\"mailto: " + GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_EMAIL) + "\">" + GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_EMAIL) + "</a>";
            email.InnerHtml = "<a href=\"mailto: " + "joe@icslearninggroup.com" + "\">" + "joe@icslearninggroup.com" + "</a>";

            // website
            HtmlGenericControl website = new HtmlGenericControl("p");
            website.ID = id + "Website";
            //website.InnerHtml = "<a href=\"" + GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_WEBSITE) + "\" target=\"_blank\">" + GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_WEBSITE) + "</a>";
            website.InnerHtml = "<a href=\"" + "http://www.icslearninggroup.com" + "\" target=\"_blank\">" + "http://www.icslearninggroup.com" + "</a>";

            // attach control to container
            siteBrandingTagContainer.Controls.Add(companyName);
            siteBrandingTagContainer.Controls.Add(email);
            siteBrandingTagContainer.Controls.Add(website);
            FooterColumnRight.Controls.Add(siteBrandingTagContainer);
        }
        #endregion

        #region _LanguageSelector_Change_Session
        /// <summary>
        /// This is the handler method for the "Session" language menu.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">arguments</param>
        private void _LanguageSelector_Change_Session(object sender, CommandEventArgs e)
        {
            string language = (string)e.CommandArgument;

            // change the user culture
            AsentiaSessionState.UserCulture = language;

            // if this session is a logged-in user, not the admin, change the default language for the user
            if (AsentiaSessionState.IdSiteUser > 1)
            {
                // update the user's default language
                AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.Account);

                try
                {
                    databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                    databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                    databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                    databaseObject.AddParameter("@callerLangString", language, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                    databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                    databaseObject.ExecuteNonQuery("[User.SetDefaultLanguage]", true);

                    DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    databaseObject.Dispose();
                }
            }

            // redirect back to the page with full url intact
            Context.Response.Redirect(Context.Request.RawUrl);
        }
        #endregion
        #endregion
        #endregion

        #region Overridden Methods

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(CustomerManager), "Asentia.CustomerManager.Pages._MasterPage.jquery-1.10.1.min.js");
            csm.RegisterClientScriptResource(typeof(CustomerManager), "Asentia.CustomerManager.Pages._MasterPage.jquery-ui.js");
            csm.RegisterClientScriptResource(typeof(CustomerManager), "Asentia.CustomerManager.Pages._MasterPage.CustomerManager.js");
        }
        #endregion
        #endregion
    }
}