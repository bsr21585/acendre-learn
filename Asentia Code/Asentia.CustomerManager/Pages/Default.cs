﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.CustomerManager.Pages
{
    public class Default : CustomerManagerAuthenticatedPage
    {
        #region Properties
        public Panel MainMenuContainer = new Panel();
        public Panel SessionStateInformation = new Panel(); // remove this
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // initialize customer manager administrator menu
            this.InitializeAdminMenu();
        }
        #endregion
    }
}
