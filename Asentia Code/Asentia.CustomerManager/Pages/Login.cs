﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using AjaxControlToolkit;

namespace Asentia.CustomerManager.Pages
{
    public class Login : CustomerManagerPage
    {
        #region Properties
        public Panel ContentContainer;
        public LoginForm LoginFormControl;
        #endregion

        #region OnPreInit
        protected override void OnPreInit(EventArgs e)
        {
            if (AsentiaSessionState.IdAccountUser > 0)
            { Response.Redirect("/"); }

            base.OnPreInit(e);
        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            LoginFormControl = new LoginForm();
            ContentContainer.Controls.Add(LoginFormControl);

            // set-up the login form control
            this.LoginFormControl.ShowRegisterLink = false; //added by Chetu team as per client suggestion
            this.LoginFormControl.FormLoginType = LoginFormLoginType.CustomerManager;
            this.LoginFormControl.LoginButton.Command += new CommandEventHandler(this._LoginButton_Command);
            this.LoginFormControl.ForgotPasswordModal.SubmitButton.Command += new CommandEventHandler(this._ForgotPassword_Command);
        }
        #endregion

        #region _LoginButton_Command
        /// <summary>
        /// Method to perform the user login.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">arguments</param>
        private void _LoginButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // make sure username and password are specified
                if (String.IsNullOrWhiteSpace(this.LoginFormControl.UsernameBox.Text)
                    || String.IsNullOrWhiteSpace(this.LoginFormControl.PasswordBox.Text))
                {
                    throw new AsentiaException(_GlobalResources.UsernameAndPasswordMustBeSpecified);
                }

                // authenticate login
                Library.Login.AuthenticateUser(this.LoginFormControl.UsernameBox.Text, this.LoginFormControl.PasswordBox.Text);

                // if we get here without throwing an exception, it means a successful login
                // redirect
                Response.Redirect("/");
            }
            catch (Exception ex)
            {
                // show the error
                this.LoginFormControl.DisplayFeedback(ex.Message, true, true);
            }
        }
        #endregion

        #region _ForgotPassword_Command
        /// <summary>
        /// Method to process the forgot password form submission from the modal.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">arguments</param>
        private void _ForgotPassword_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // make sure username is specified
                if (String.IsNullOrWhiteSpace(this.LoginFormControl.ForgotPasswordModalUsernameBox.Text))
                {
                    throw new AsentiaException(_GlobalResources.UsernameMustBeSpecified);
                }

                // reset the password
                string emailSentTo = Library.Login.ResetPassword(this.LoginFormControl.ForgotPasswordModalUsernameBox.Text);

                // disable the submit button and show the success feedback
                this.LoginFormControl.ForgotPasswordModal.DisableSubmitButton();
                this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(
                    String.Format(_GlobalResources.YourPasswordHasBeenSuccessfullyResetAndSentToEmail, emailSentTo),
                    false);
            }
            catch (Exception ex)
            {
                // show the error
                this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion
    }
}
