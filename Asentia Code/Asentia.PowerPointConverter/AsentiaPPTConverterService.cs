﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using Asentia.Common;

using NetOffice;
using PowerPoint = NetOffice.PowerPointApi;
using Word = NetOffice.WordApi;
using NetOffice.PowerPointApi.Enums;
using NetOffice.WordApi.Enums;
using NetOffice.OfficeApi.Enums;

namespace Asentia.PowerPointConverter
{      
    public partial class AsentiaPPTConverterService : ServiceBase
    {
        #region Private Properties
        private bool _StopService = true;

        private System.Timers.Timer _AsentiaJobTimer;
        private System.Timers.Timer _InquisiqJobTimer;

        private bool _IsRetrievingAsentiaJob = false;
        private bool _IsRetrievingInquisiqJob = false;
        #endregion

        #region Constructor
        public AsentiaPPTConverterService()
        {
            InitializeComponent();

            this._StopService = Convert.ToBoolean(ConfigurationManager.AppSettings["StopService"]);
        }
        #endregion

        #region OnStart
        protected override void OnStart(string[] args)
        {
            // Not sure if we're going to set MAX threads or just let the system handle it,
            // this will depend on unit testing.

            // Let's just do 1 thread for the moment.
            ThreadPool.SetMaxThreads(1, 1);

            // get the config values for the process flags, this tells us if we are to process Asentia and/or Inquisiq jobs
            bool processAsentiaJobsFlag = Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessAsentiaJobs"]);
            bool processInquisiqJobsFlag = Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessInquisiqJobs"]);

            // INITIALIZE AND SET TIMERS

            // Note that we set timers here statically instead of setting them via
            // the config file because we do not want any kind of outside control
            // for the timers, they're too sensitive.

            // Set the timer for Asentia jobs
            // This is the timer for processing PowerPoint jobs for each Asentia account database.
            //
            // TODO: Add condition for single database installation, probably key to
            // license number, because timer will not need to run every half second
            // for a single account database installation.
            if (processAsentiaJobsFlag)
            {
                this._AsentiaJobTimer = new System.Timers.Timer(500D);  // 500 milliseconds = 1/2 second
                this._AsentiaJobTimer.AutoReset = true;
                this._AsentiaJobTimer.Elapsed += new System.Timers.ElapsedEventHandler(this._AsentiaJobTimer_Elapsed);
                this._AsentiaJobTimer.Start();
            }
            else
            { this._AsentiaJobTimer = null; }

            // Set the timer for Inquisiq jobs
            // This is the timer for processing PowerPoint jobs for Inquisiq.
            if (processInquisiqJobsFlag)
            {
                this._InquisiqJobTimer = new System.Timers.Timer(5000);  // 5000 milliseconds = 5 seconds
                this._InquisiqJobTimer.AutoReset = true;
                this._InquisiqJobTimer.Elapsed += new System.Timers.ElapsedEventHandler(this._InquisiqJobTimer_Elapsed);
                this._InquisiqJobTimer.Start();
            }
            else
            { this._InquisiqJobTimer = null; }
        }
        #endregion

        #region OnStop
        protected override void OnStop()
        {
            // stop the Asentia job timer
            if (this._AsentiaJobTimer != null)
            {
                this._AsentiaJobTimer.Stop();
                this._AsentiaJobTimer = null;
            }

            // stop the Inquisiq job timer
            if (this._InquisiqJobTimer != null)
            {
                this._InquisiqJobTimer.Stop();
                this._InquisiqJobTimer = null;
            }
        }
        #endregion

        #region _AsentiaJobTimer_Elapsed
        /// <summary>
        /// The timer elapsed action for Asentia PowerPoint Converter jobs. This performs PowerPoint
        /// processing for a specific Asentia accounts database pulled from the account database queue.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AsentiaJobTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {            
            // if the service is not set to be stopped, proceed processing jobs
            if (!this._StopService)
            {
                int idAccount = 0;

                try
                {
                    // instansiate the AsentiaJobs class instead of using static 
                    // methods to keep this service thread safe
                    AsentiaJobs asentiaJobsInstance = new AsentiaJobs(this.ServiceName);

                    // accounts pull and entry point here
                    if (!this._IsRetrievingAsentiaJob)
                    {
                        // set the retrieving job flag
                        this._IsRetrievingAsentiaJob = true;

                        // get the next account to be processed and set the processing flag
                        idAccount = this._GetNextAccountIdForProcessing();

                        if (idAccount > 0)
                        { this._SetProcessingFlagForAccount(idAccount); }

                        // release the retrieving job flag
                        this._IsRetrievingAsentiaJob = false;

                        // process the jobs in the database
                        if (idAccount > 0)
                        {
                            // PowerPoint
                            asentiaJobsInstance.ProcessPowerPointJobs(idAccount);

                            // Word to PDF
                            asentiaJobsInstance.ProcessWordToPDFJobs(idAccount);

                            // release the processing flag for the account
                            this._ReleaseProcessingFlagForAccount(idAccount);
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteToLog(this.ServiceName, ex.Message + " | " + ex.StackTrace, true, true);

                    // release the retrieving job flag
                    this._IsRetrievingAsentiaJob = false;

                    // release the processing flag for the account
                    if (idAccount > 0)
                    { this._ReleaseProcessingFlagForAccount(idAccount); }
                }
                finally
                { }
            }
        }
        #endregion

        #region _InquisiqJobTimer_Elapsed
        /// <summary>
        /// The timer elapsed action for Inquisiq PowerPoint Converter jobs. This performs PowerPoint
        /// processing for the entire Inquisiq database specified in the config file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _InquisiqJobTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {            
            // if the service is not set to be stopped, proceed processing jobs
            if (!this._StopService)
            {
                int idInventoryItem = 0;

                try
                {
                    // instansiate the InquisiqJobs class instead of using static 
                    // methods to keep this service thread safe
                    InquisiqJobs inquisiqJobsInstance = new InquisiqJobs(this.ServiceName);

                    // accounts pull and entry point here
                    if (!this._IsRetrievingInquisiqJob)
                    {
                        // set the retrieving job flag
                        this._IsRetrievingInquisiqJob = true;

                        // get the next inventory item to be processed and set the processing flag
                        idInventoryItem = this._GetNextInquisiqInventoryItemForProcessing(inquisiqJobsInstance);

                        if (idInventoryItem > 0)
                        { this._SetProcessingFlagForInquisiqInventoryItem(idInventoryItem); }

                        // release the retrieving job flag
                        this._IsRetrievingInquisiqJob = false;

                        // process the jobs in the database
                        if (idInventoryItem > 0)
                        {
                            // PowerPoint
                            inquisiqJobsInstance.ProcessPowerPointJobs();

                            // release the processing flag for the inventory item
                            this._ReleaseProcessingFlagForInventoryItem(idInventoryItem);
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteToLog(this.ServiceName, ex.Message + " | " + ex.StackTrace, true, true);

                    // release the retrieving job flag
                    this._IsRetrievingInquisiqJob = false;

                    // release the processing flag for the inventory item
                    if (idInventoryItem > 0)
                    { this._ReleaseProcessingFlagForInventoryItem(idInventoryItem); }
                }
                finally
                { }
            }
        }
        #endregion

        #region _GetNextAccountIdForProcessing
        /// <summary>
        /// Gets the next account id for Asentia PowerPoint Converter job processing.
        /// </summary>
        /// <returns></returns>
        private int _GetNextAccountIdForProcessing()
        {
            int idAccount = 0;

            string rootWebConfigPath = ConfigurationManager.AppSettings["AsentiaRootFolder"];
            AsentiaDatabase customerManagerDatabaseObject = new AsentiaDatabase(rootWebConfigPath, DatabaseType.CustomerManagerDatabaseUsingWebConfigPath);

            customerManagerDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            customerManagerDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            customerManagerDatabaseObject.AddParameter("@idCallerAccount", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@callerLangString", DBNull.Value, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sqlDataReader = customerManagerDatabaseObject.ExecuteDataReader("[PowerPointJobProcessor.GetNextAccountForProcessing]", true);
                sqlDataReader.Read();

                if (sqlDataReader.HasRows)
                { idAccount = Convert.ToInt32(sqlDataReader["idAccount"]); }

                // close the SqlDataReader
                sqlDataReader.Close();

                return idAccount;
            }
            catch
            { throw; }
            finally
            { customerManagerDatabaseObject.Dispose(); }
        }
        #endregion

        #region _SetProcessingFlagForAccount
        /// <summary>
        /// Sets the processing flag for the account currently being processed.
        /// </summary>
        /// <param name="idAccount"></param>
        private void _SetProcessingFlagForAccount(int idAccount)
        {
            string rootWebConfigPath = ConfigurationManager.AppSettings["AsentiaRootFolder"];
            AsentiaDatabase customerManagerDatabaseObject = new AsentiaDatabase(rootWebConfigPath, DatabaseType.CustomerManagerDatabaseUsingWebConfigPath);

            customerManagerDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            customerManagerDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            customerManagerDatabaseObject.AddParameter("@idCallerAccount", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@callerLangString", DBNull.Value, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            customerManagerDatabaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                customerManagerDatabaseObject.ExecuteNonQuery("[PowerPointJobProcessor.SetProcessingFlagForAccount]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(customerManagerDatabaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = customerManagerDatabaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { customerManagerDatabaseObject.Dispose(); }
        }
        #endregion

        #region _ReleaseProcessingFlagForAccount
        /// <summary>
        /// Releases the processing flag for the account that was processed.
        /// </summary>
        /// <param name="idAccount"></param>
        private void _ReleaseProcessingFlagForAccount(int idAccount)
        {
            string rootWebConfigPath = ConfigurationManager.AppSettings["AsentiaRootFolder"];
            AsentiaDatabase customerManagerDatabaseObject = new AsentiaDatabase(rootWebConfigPath, DatabaseType.CustomerManagerDatabaseUsingWebConfigPath);

            customerManagerDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            customerManagerDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            customerManagerDatabaseObject.AddParameter("@idCallerAccount", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@callerLangString", DBNull.Value, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            customerManagerDatabaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                customerManagerDatabaseObject.ExecuteNonQuery("[PowerPointJobProcessor.ReleaseProcessingFlagForAccount]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(customerManagerDatabaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = customerManagerDatabaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { customerManagerDatabaseObject.Dispose(); }
        }
        #endregion

        #region _GetNextInquisiqInventoryItemForProcessing
        /// <summary>
        /// Gets the next inventory item id for Inquisiq PowerPoint Converter job processing.
        /// </summary>
        /// <returns></returns>
        private int _GetNextInquisiqInventoryItemForProcessing(InquisiqJobs inquisiqJobsInstance)
        {
            int idInventoryItem = 0;

            string inquisiqInventoryDBServer = ConfigurationManager.AppSettings["InquisiqInventoryDBServer"];
            string inquisiqInventoryDBName = ConfigurationManager.AppSettings["InquisiqInventoryDBName"];
            string inquisiqInventoryDBLogin = ConfigurationManager.AppSettings["InquisiqInventoryDBLogin"];
            string inquisiqInventoryDBPassword = ConfigurationManager.AppSettings["InquisiqInventoryDBPassword"];

            AsentiaDatabase inventoryDatabaseObject = new AsentiaDatabase(inquisiqInventoryDBServer, inquisiqInventoryDBName, inquisiqInventoryDBLogin, inquisiqInventoryDBPassword, "InquisiqPowerPointProcessor", 60);

            inventoryDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            inventoryDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            try
            {
                SqlDataReader sqlDataReader = inventoryDatabaseObject.ExecuteDataReader("[InquisiqPowerPointJobProcessor.GetNextInventoryItemForProcessing]", true);
                sqlDataReader.Read();

                if (sqlDataReader.HasRows)
                {
                    idInventoryItem = Convert.ToInt32(sqlDataReader["idInventoryItem"]);
                    inquisiqJobsInstance.InquisiqInstanceDBServer = sqlDataReader["databaseServer"].ToString();
                    inquisiqJobsInstance.InquisiqInstanceDBName = sqlDataReader["databaseName"].ToString();
                    inquisiqJobsInstance.InquisiqInstanceWarehousePath = sqlDataReader["warehousePath"].ToString();
                }

                // close the SqlDataReader
                sqlDataReader.Close();

                return idInventoryItem;
            }
            catch
            { throw; }
            finally
            { inventoryDatabaseObject.Dispose(); }
        }
        #endregion

        #region _SetProcessingFlagForInquisiqInventoryItem
        /// <summary>
        /// Sets the processing flag for the inventory item currently being processed.
        /// </summary>
        /// <param name="idAccount"></param>
        private void _SetProcessingFlagForInquisiqInventoryItem(int idInventoryItem)
        {
            string inquisiqInventoryDBServer = ConfigurationManager.AppSettings["InquisiqInventoryDBServer"];
            string inquisiqInventoryDBName = ConfigurationManager.AppSettings["InquisiqInventoryDBName"];
            string inquisiqInventoryDBLogin = ConfigurationManager.AppSettings["InquisiqInventoryDBLogin"];
            string inquisiqInventoryDBPassword = ConfigurationManager.AppSettings["InquisiqInventoryDBPassword"];

            AsentiaDatabase inventoryDatabaseObject = new AsentiaDatabase(inquisiqInventoryDBServer, inquisiqInventoryDBName, inquisiqInventoryDBLogin, inquisiqInventoryDBPassword, "InquisiqPowerPointProcessor", 60);

            inventoryDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            inventoryDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            inventoryDatabaseObject.AddParameter("@idInventoryItem", idInventoryItem, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                inventoryDatabaseObject.ExecuteNonQuery("[InquisiqPowerPointJobProcessor.SetProcessingFlagForInventoryItem]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(inventoryDatabaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = inventoryDatabaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { inventoryDatabaseObject.Dispose(); }
        }
        #endregion

        #region _ReleaseProcessingFlagForInventoryItem
        /// <summary>
        /// Releases the processing flag for the inventory item that was processed.
        /// </summary>
        /// <param name="idAccount"></param>
        private void _ReleaseProcessingFlagForInventoryItem(int idInventoryItem)
        {
            string inquisiqInventoryDBServer = ConfigurationManager.AppSettings["InquisiqInventoryDBServer"];
            string inquisiqInventoryDBName = ConfigurationManager.AppSettings["InquisiqInventoryDBName"];
            string inquisiqInventoryDBLogin = ConfigurationManager.AppSettings["InquisiqInventoryDBLogin"];
            string inquisiqInventoryDBPassword = ConfigurationManager.AppSettings["InquisiqInventoryDBPassword"];

            AsentiaDatabase inventoryDatabaseObject = new AsentiaDatabase(inquisiqInventoryDBServer, inquisiqInventoryDBName, inquisiqInventoryDBLogin, inquisiqInventoryDBPassword, "InquisiqPowerPointProcessor", 60);

            inventoryDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            inventoryDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            inventoryDatabaseObject.AddParameter("@idInventoryItem", idInventoryItem, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                inventoryDatabaseObject.ExecuteNonQuery("[InquisiqPowerPointJobProcessor.ReleaseProcessingFlagForInventoryItem]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(inventoryDatabaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = inventoryDatabaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { inventoryDatabaseObject.Dispose(); }
        }
        #endregion

        #region ConvertPowerPointFileToSlides
        /// <summary>
        /// Converts each slide of a .ppt or .pptx file to a PNG image and saves it to the specified location.
        /// 
        /// Note, this only converts static slides, no media inside of the PowerPoint will converted.
        /// </summary>
        /// <param name="pathToWarehouseFolder">
        /// this is the root of the package folder we created in the warehouse, 
        /// it's the source of the PowerPoint file and the root destination for saved slides
        /// </param>
        /// <param name="powerPointFilename">the name of the PowerPoint file</param>
        public static void ConvertPowerPointFileToSlides(string pathToWarehouseFolder, string powerPointFilename)
        {
            // append a \ to the end of the pathToWarehouseFolder if there is not already one
            if (!pathToWarehouseFolder.EndsWith("\\"))
            { pathToWarehouseFolder += "\\"; }

            // start powerpoint
            PowerPoint.Application powerPointApplication = new PowerPoint.Application();

            try
            {
                // create a utils instance, not needed, but helpful to keep the lines of code low
                PowerPoint.Tools.CommonUtils utils = new PowerPoint.Tools.CommonUtils(powerPointApplication);

                // open the powerpoint presentation
                PowerPoint.Presentation presentation = powerPointApplication.Presentations.Open(pathToWarehouseFolder + powerPointFilename, MsoTriState.msoTrue, MsoTriState.msoTrue, MsoTriState.msoFalse);

                // save the document
                presentation.SaveAs(pathToWarehouseFolder + "images", PowerPoint.Enums.PpSaveAsFileType.ppSaveAsPNG, MsoTriState.msoFalse);
            }
            catch (Exception ex)
            {
                throw ex.InnerException; // throw the inner exception, we will handle, log, and bury it in calling methods
            }
            finally
            {
                // close powerpoint and dispose reference
                powerPointApplication.Quit();
                powerPointApplication.Dispose();
            }
        }
        #endregion

        #region ConvertWordFileToPDF
        /// <summary>
        /// Converts a .doc or .docx file to a PDF file and saves it to the specified location.
        /// </summary>
        /// <param name="pathToWarehouseFolder">
        /// this is the root of the package folder we created in the warehouse, 
        /// it's the source of the Word file and the root destination for saved PDF
        /// </param>
        /// <param name="wordFilename">the name of the Word file</param>
        public static void ConvertWordFileToPDF(string pathToWarehouseFolder, string wordFilename)
        {
            // append a \ to the end of the pathToWarehouseFolder if there is not already one
            if (!pathToWarehouseFolder.EndsWith("\\"))
            { pathToWarehouseFolder += "\\"; }

            // start word
            Word.Application wordApplication = new Word.Application();

            try
            {
                // create a utils instance, not needed, but helpful to keep the lines of code low
                Word.Tools.CommonUtils utils = new Word.Tools.CommonUtils(wordApplication);

                // open the word document
                Word.Document document = wordApplication.Documents.Open(pathToWarehouseFolder + wordFilename, MsoTriState.msoTrue, MsoTriState.msoTrue, MsoTriState.msoFalse);

                // save the document
                string wordFilenameWithoutExtension = Path.GetFileNameWithoutExtension(wordFilename);
                document.SaveAs(pathToWarehouseFolder + wordFilenameWithoutExtension, Word.Enums.WdSaveFormat.wdFormatPDF, MsoTriState.msoFalse);
            }
            catch (Exception ex)
            {
                throw; // throw the exception, we will handle, log, and bury it in calling methods
            }
            finally
            {
                // close word and dispose reference
                wordApplication.Quit();
                wordApplication.Dispose();
            }
        }
        #endregion

        #region WriteToLog
        public static void WriteToLog(string serviceIdentifier, string message, bool isError, bool writeToSysEventLog)
        {
            // use a try, catch here; but do absolutely nothing in the catch
            // the reasons, 1) we do not ever want this service getting hung
            // up because of an exception, 2) inability to write to a log
            // indicates something fatal, and 3) we will discover "fatal"
            // issues through the lack of log entries
            try
            {
                if (writeToSysEventLog)
                {
                    EventLog applicationLog = new EventLog();
                    applicationLog.Source = serviceIdentifier;

                    if (isError)
                    { applicationLog.WriteEntry(message, EventLogEntryType.Error); }
                    else
                    { applicationLog.WriteEntry(message, EventLogEntryType.Information); }
                }
            }
            // just swallow the exception
            catch
            { ; }
        }
        #endregion
    }
}
