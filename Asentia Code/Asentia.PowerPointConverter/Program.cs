﻿using System;
using System.ServiceProcess;

namespace Asentia.PowerPointConverter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new AsentiaPPTConverterService() 
            };

            ServiceBase.Run(ServicesToRun);
        }
    }
}
