﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Text.RegularExpressions;
using Asentia.Common;

namespace Asentia.PowerPointConverter
{
    public class AsentiaJobs
    {
        #region Properties
        public string ServiceName = String.Empty;
        #endregion

        #region Private Properties
        private const int _NUMBER_OF_ITEMS_TO_PROCESS = 10;
        #endregion

        #region Constructor
        public AsentiaJobs(string serviceName)
        { this.ServiceName = serviceName; }
        #endregion

        #region ProcessPowerPointJobs
        /// <summary>
        /// Retrieves and processes a queue of PowerPoint Converter jobs.
        /// </summary>
        /// <param name="idAccount">the id of the account to process jobs for</param>
        public void ProcessPowerPointJobs(int idAccount)
        {
            string accountsPath = ConfigurationManager.AppSettings["AsentiaAccountsFolder"];
            string accountWebConfigPath = accountsPath + idAccount;

            if (Directory.Exists(accountWebConfigPath))
            {
                if (File.Exists(accountWebConfigPath + "\\web.config"))
                {
                    string asentiaWarehouseFolder = ConfigurationManager.AppSettings["AsentiaRootFolder"] + "warehouse\\";
                    DataTable currentQueueIds = new DataTable();
                    currentQueueIds.Columns.Add("id", typeof(int));

                    try
                    {
                        DataTable queueItemsToProcess = this._GetCurrentPowerPointQueueItems(accountWebConfigPath);

                        // only do work if there are items to process
                        if (queueItemsToProcess.Rows.Count > 0)
                        {
                            // get the ids of the items from the queue so we can set the processing flags
                            foreach (DataRow row in queueItemsToProcess.Rows)
                            { currentQueueIds.Rows.Add(Convert.ToInt32(row["idContentPackage"])); }

                            // set the processing flags for the items
                            this._SetProcessingFlagForPowerPointQueueItems(accountWebConfigPath, currentQueueIds);

                            foreach (DataRow row in queueItemsToProcess.Rows)
                            {
                                // content properties
                                int idContentPackage = Convert.ToInt32(row["idContentPackage"]);
                                string packageFolder = row["path"].ToString().Replace("/warehouse/", "");
                                string pptFilename = row["originalMediaFilename"].ToString();
                                string contentTitle = row["contentTitle"].ToString();

                                // content settings
                                bool enableAutoplay = Convert.ToBoolean(Convert.ToInt32(row["enableAutoplay"]));
                                bool allowRewind = Convert.ToBoolean(Convert.ToInt32(row["allowRewind"]));
                                bool allowFastForward = Convert.ToBoolean(Convert.ToInt32(row["allowFastForward"]));
                                bool allowNavigation = Convert.ToBoolean(Convert.ToInt32(row["allowNavigation"]));
                                bool allowResume = Convert.ToBoolean(Convert.ToInt32(row["allowResume"]));
                                double minProgressForCompletion = Convert.ToDouble(row["minProgressForCompletion"]);

                                // convert the PowerPoint file to slides
                                AsentiaPPTConverterService.ConvertPowerPointFileToSlides(asentiaWarehouseFolder + packageFolder, pptFilename);

                                // get the number of slides that were saved
                                int slideCount = Directory.GetFiles(asentiaWarehouseFolder + packageFolder + "\\images", "*.png", SearchOption.TopDirectoryOnly).Length;

                                // write the JS string array of slide files for the config file
                                string slideArrayStringForConfigJS = String.Empty;
                                int i;

                                for (i = 1; i <= slideCount; i++)
                                { slideArrayStringForConfigJS += "\"Slide" + i.ToString() + ".png\","; }

                                // remove the last , from the string
                                if (slideArrayStringForConfigJS.EndsWith(","))
                                { slideArrayStringForConfigJS = slideArrayStringForConfigJS.TrimEnd(','); }

                                // generate a manifest identifier
                                string manifestIdentifier = "_" + Guid.NewGuid().ToString();

                                // open the package's imsmanifest.xml, index.html, and /script/config.js files to replace placeholders with package specific information

                                // imsmanifest.xml
                                string imsManifestContent = String.Empty;

                                using (StreamReader reader = new StreamReader(asentiaWarehouseFolder + packageFolder + "\\imsmanifest.xml"))
                                {
                                    imsManifestContent = reader.ReadToEnd();
                                    reader.Close();
                                }

                                imsManifestContent = Regex.Replace(imsManifestContent, "##manifestIdentifier##", manifestIdentifier);
                                imsManifestContent = Regex.Replace(imsManifestContent, "##contentTitle##", contentTitle);

                                if (minProgressForCompletion == 0)
                                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>0</adlcp:completionThreshold>"); }
                                else if (minProgressForCompletion == 1)
                                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>1</adlcp:completionThreshold>"); }
                                else if (minProgressForCompletion > 0 && minProgressForCompletion < 1)
                                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>" + String.Format("{0:0.0}", minProgressForCompletion) + "</adlcp:completionThreshold>"); }
                                else
                                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", ""); }

                                File.Delete(asentiaWarehouseFolder + packageFolder + "\\imsmanifest.xml");

                                using (StreamWriter writer = new StreamWriter(asentiaWarehouseFolder + packageFolder + "\\imsmanifest.xml"))
                                {
                                    writer.Write(imsManifestContent);
                                    writer.Close();
                                }

                                // index.html
                                string indexContent = String.Empty;

                                using (StreamReader reader = new StreamReader(asentiaWarehouseFolder + packageFolder + "\\index.html"))
                                {
                                    indexContent = reader.ReadToEnd();
                                    reader.Close();
                                }

                                indexContent = Regex.Replace(indexContent, "##contentTitle##", contentTitle);

                                File.Delete(asentiaWarehouseFolder + packageFolder + "\\index.html");

                                using (StreamWriter writer = new StreamWriter(asentiaWarehouseFolder + packageFolder + "\\index.html"))
                                {
                                    writer.Write(indexContent);
                                    writer.Close();
                                }

                                // /script/config.js
                                string configContent = String.Empty;

                                using (StreamReader reader = new StreamReader(asentiaWarehouseFolder + packageFolder + "\\script\\config.js"))
                                {
                                    configContent = reader.ReadToEnd();
                                    reader.Close();
                                }

                                configContent = Regex.Replace(configContent, "##slides##", slideArrayStringForConfigJS);
                                configContent = Regex.Replace(configContent, "##enableAutoplay##", enableAutoplay.ToString().ToLower());
                                configContent = Regex.Replace(configContent, "##allowResume##", allowResume.ToString().ToLower());
                                configContent = Regex.Replace(configContent, "##allowFastForward##", allowFastForward.ToString().ToLower());
                                configContent = Regex.Replace(configContent, "##allowRewind##", allowRewind.ToString().ToLower());
                                configContent = Regex.Replace(configContent, "##allowNavigation##", allowNavigation.ToString().ToLower());
                                configContent = Regex.Replace(configContent, "##minProgressForCompletion##", minProgressForCompletion.ToString());

                                File.Delete(asentiaWarehouseFolder + packageFolder + "\\script\\config.js");

                                using (StreamWriter writer = new StreamWriter(asentiaWarehouseFolder + packageFolder + "\\script\\config.js"))
                                {
                                    writer.Write(configContent);
                                    writer.Close();
                                }

                                // get the size of the package in KB
                                int packageSizeInKb = Utility.CalculateDirectorySizeInKb(asentiaWarehouseFolder + packageFolder);

                                // update the processed package information
                                this._UpdateProcessedPowerPointQueueItem(accountWebConfigPath, idContentPackage, packageSizeInKb, imsManifestContent);
                            }
                        }
                    }
                    catch
                    { throw; }
                }
                else
                {
                    // throw new AsentiaException("Web.config file for account " + idAccount.ToString() + " does not exist.");
                }
            }
            else
            {
                // throw new AsentiaException("Directory for account " + idAccount.ToString() + " does not exist.");
            }
        }
        #endregion

        #region ProcessWordToPDFJobs
        /// <summary>
        /// Retrieves and processes a queue of Word to PDF Converter jobs.
        /// </summary>
        /// <param name="idAccount">the id of the account to process jobs for</param>
        public void ProcessWordToPDFJobs(int idAccount)
        {
            string accountsPath = ConfigurationManager.AppSettings["AsentiaAccountsFolder"];
            string accountWebConfigPath = accountsPath + idAccount;

            if (Directory.Exists(accountWebConfigPath))
            {
                if (File.Exists(accountWebConfigPath + "\\web.config"))
                {
                    string asentiaWarehouseFolder = ConfigurationManager.AppSettings["AsentiaRootFolder"] + "warehouse\\";
                    DataTable currentQueueIds = new DataTable();
                    currentQueueIds.Columns.Add("id", typeof(int));

                    try
                    {
                        DataTable queueItemsToProcess = this._GetCurrentWordToPDFQueueItems(accountWebConfigPath);

                        // only do work if there are items to process
                        if (queueItemsToProcess.Rows.Count > 0)
                        {
                            // get the ids of the items from the queue so we can set the processing flags
                            foreach (DataRow row in queueItemsToProcess.Rows)
                            { currentQueueIds.Rows.Add(Convert.ToInt32(row["idContentPackage"])); }

                            // set the processing flags for the items
                            this._SetProcessingFlagForWordToPDFQueueItems(accountWebConfigPath, currentQueueIds);

                            foreach (DataRow row in queueItemsToProcess.Rows)
                            {
                                // content properties
                                int idContentPackage = Convert.ToInt32(row["idContentPackage"]);
                                string packageFolder = row["path"].ToString().Replace("/warehouse/", "");
                                string wordFilename = row["originalMediaFilename"].ToString();
                                string contentTitle = row["contentTitle"].ToString();

                                // content settings                                
                                double minProgressForCompletion = Convert.ToDouble(row["minProgressForCompletion"]);

                                // convert the Word file to PDF
                                AsentiaPPTConverterService.ConvertWordFileToPDF(asentiaWarehouseFolder + packageFolder, wordFilename);

                                // get the word filename withoug extension so we can append .pdf to it and use it in the config file
                                string wordFilenameWithoutExtension = Path.GetFileNameWithoutExtension(wordFilename);

                                // generate a manifest identifier
                                string manifestIdentifier = "_" + Guid.NewGuid().ToString();

                                // open the package's imsmanifest.xml, viewer.html, and /script/config.js files to replace placeholders with package specific information

                                // imsmanifest.xml
                                string imsManifestContent = String.Empty;

                                using (StreamReader reader = new StreamReader(asentiaWarehouseFolder + packageFolder + "\\imsmanifest.xml"))
                                {
                                    imsManifestContent = reader.ReadToEnd();
                                    reader.Close();
                                }

                                imsManifestContent = Regex.Replace(imsManifestContent, "##manifestIdentifier##", manifestIdentifier);
                                imsManifestContent = Regex.Replace(imsManifestContent, "##contentTitle##", contentTitle);

                                if (minProgressForCompletion == 0)
                                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>0</adlcp:completionThreshold>"); }
                                else if (minProgressForCompletion == 1)
                                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>1</adlcp:completionThreshold>"); }
                                else if (minProgressForCompletion > 0 && minProgressForCompletion < 1)
                                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>" + String.Format("{0:0.0}", minProgressForCompletion) + "</adlcp:completionThreshold>"); }
                                else
                                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", ""); }

                                File.Delete(asentiaWarehouseFolder + packageFolder + "\\imsmanifest.xml");

                                using (StreamWriter writer = new StreamWriter(asentiaWarehouseFolder + packageFolder + "\\imsmanifest.xml"))
                                {
                                    writer.Write(imsManifestContent);
                                    writer.Close();
                                }

                                // viewer.html
                                string viewerContent = String.Empty;

                                using (StreamReader reader = new StreamReader(asentiaWarehouseFolder + packageFolder + "\\viewer.html"))
                                {
                                    viewerContent = reader.ReadToEnd();
                                    reader.Close();
                                }

                                viewerContent = Regex.Replace(viewerContent, "##contentTitle##", contentTitle);

                                File.Delete(asentiaWarehouseFolder + packageFolder + "\\viewer.html");

                                using (StreamWriter writer = new StreamWriter(asentiaWarehouseFolder + packageFolder + "\\viewer.html"))
                                {
                                    writer.Write(viewerContent);
                                    writer.Close();
                                }

                                // /script/config.js
                                string configContent = String.Empty;

                                using (StreamReader reader = new StreamReader(asentiaWarehouseFolder + packageFolder + "\\script\\config.js"))
                                {
                                    configContent = reader.ReadToEnd();
                                    reader.Close();
                                }

                                configContent = Regex.Replace(configContent, "##pdfFilename##", wordFilenameWithoutExtension + ".pdf");
                                configContent = Regex.Replace(configContent, "##minProgressForCompletion##", minProgressForCompletion.ToString());

                                File.Delete(asentiaWarehouseFolder + packageFolder + "\\script\\config.js");

                                using (StreamWriter writer = new StreamWriter(asentiaWarehouseFolder + packageFolder + "\\script\\config.js"))
                                {
                                    writer.Write(configContent);
                                    writer.Close();
                                }

                                // get the size of the package in KB
                                int packageSizeInKb = Utility.CalculateDirectorySizeInKb(asentiaWarehouseFolder + packageFolder);

                                // update the processed package information
                                this._UpdateProcessedWordToPDFQueueItem(accountWebConfigPath, idContentPackage, packageSizeInKb, imsManifestContent);
                            }
                        }
                    }
                    catch
                    { throw; }
                }
                else
                {
                    // throw new AsentiaException("Web.config file for account " + idAccount.ToString() + " does not exist.");
                }
            }
            else
            {
                // throw new AsentiaException("Directory for account " + idAccount.ToString() + " does not exist.");
            }
        }
        #endregion

        #region _GetCurrentPowerPointQueueItems
        /// <summary>
        /// Grabs a set of currently queued PowerPoint packages to be processed.
        /// </summary>
        /// <param name="accountWebConfigPath">full path to the account's web.config file</param>
        /// <returns></returns>
        private DataTable _GetCurrentPowerPointQueueItems(string accountWebConfigPath)
        {
            DataTable currentQueueItems = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", "en-US", SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); // this will always be null, for now
            databaseObject.AddParameter("@numItems", _NUMBER_OF_ITEMS_TO_PROCESS, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sqlDataReader = databaseObject.ExecuteDataReader("[PowerPointProcessor.GetCurrentQueueItems]", true);
                currentQueueItems.Load(sqlDataReader);
                sqlDataReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { databaseObject.Dispose(); }

            return currentQueueItems;
        }
        #endregion

        #region _SetProcessingFlagForPowerPointQueueItems
        /// <summary>
        /// Sets the processing flag for the PowerPoint packages that we are currently processing.
        /// </summary>
        /// <param name="accountWebConfigPath">full path to the account's web.config file</param>
        /// <param name="queueItems">DataTable of package ids to mark as processing</param>
        private void _SetProcessingFlagForPowerPointQueueItems(string accountWebConfigPath, DataTable queueItems)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", "en-US", SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@QueueItems", queueItems, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[PowerPointProcessor.SetProcessingFlagForQueueItems]", true);

                // get the return code
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { databaseObject.Dispose(); }
        }
        #endregion

        #region _UpdateProcessedPowerPointQueueItem
        /// <summary>
        /// Sets the processed flag for a PowerPoint package that has been processed. Also updates the 
        /// package size and manifest.
        /// </summary>
        /// <param name="accountWebConfigPath">full path to the account's web.config file</param>
        /// <param name="idContentPackage">id of the package to mark as processed</param>
        /// <param name="kb">size of the package</param>
        /// <param name="manifest">the package's manifest</param>
        private void _UpdateProcessedPowerPointQueueItem(string accountWebConfigPath, int idContentPackage, int kb, string manifest)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", "en-US", SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idContentPackage", idContentPackage, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@kb", kb, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@manifest", manifest, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[PowerPointProcessor.UpdateProcessedQueueItem]", true);

                // get the return code
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { databaseObject.Dispose(); }
        }
        #endregion

        #region _GetCurrentWordToPDFQueueItems
        /// <summary>
        /// Grabs a set of currently queued Word to PDF packages to be processed.
        /// </summary>
        /// <param name="accountWebConfigPath">full path to the account's web.config file</param>
        /// <returns></returns>
        private DataTable _GetCurrentWordToPDFQueueItems(string accountWebConfigPath)
        {
            DataTable currentQueueItems = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", "en-US", SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); // this will always be null, for now
            databaseObject.AddParameter("@numItems", _NUMBER_OF_ITEMS_TO_PROCESS, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sqlDataReader = databaseObject.ExecuteDataReader("[WordToPDFProcessor.GetCurrentQueueItems]", true);
                currentQueueItems.Load(sqlDataReader);
                sqlDataReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { databaseObject.Dispose(); }

            return currentQueueItems;
        }
        #endregion

        #region _SetProcessingFlagForWordToPDFQueueItems
        /// <summary>
        /// Sets the processing flag for the Word to PDF packages that we are currently processing.
        /// </summary>
        /// <param name="accountWebConfigPath">full path to the account's web.config file</param>
        /// <param name="queueItems">DataTable of package ids to mark as processing</param>
        private void _SetProcessingFlagForWordToPDFQueueItems(string accountWebConfigPath, DataTable queueItems)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", "en-US", SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@QueueItems", queueItems, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[WordToPDFProcessor.SetProcessingFlagForQueueItems]", true);

                // get the return code
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { databaseObject.Dispose(); }
        }
        #endregion

        #region _UpdateProcessedWordToPDFQueueItem
        /// <summary>
        /// Sets the processed flag for a Word to PDF package that has been processed. Also updates the 
        /// package size and manifest.
        /// </summary>
        /// <param name="accountWebConfigPath">full path to the account's web.config file</param>
        /// <param name="idContentPackage">id of the package to mark as processed</param>
        /// <param name="kb">size of the package</param>
        /// <param name="manifest">the package's manifest</param>
        private void _UpdateProcessedWordToPDFQueueItem(string accountWebConfigPath, int idContentPackage, int kb, string manifest)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", "en-US", SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idContentPackage", idContentPackage, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@kb", kb, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@manifest", manifest, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[WordToPDFProcessor.UpdateProcessedQueueItem]", true);

                // get the return code
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { databaseObject.Dispose(); }
        }
        #endregion
    }
}
