﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Threading;
using Asentia.Common;

namespace Asentia.PowerPointConverter
{
    public class InquisiqJobs
    {
        #region Properties
        public string ServiceName = String.Empty;

        public string InquisiqInstanceDBServer = String.Empty;
        public string InquisiqInstanceDBName = String.Empty;
        public string InquisiqInstanceWarehousePath = String.Empty;        
        #endregion

        #region Private Properties
        private const int _NUMBER_OF_ITEMS_TO_PROCESS = 10;
        #endregion

        #region Constructor
        public InquisiqJobs(string serviceName)
        { this.ServiceName = serviceName; }
        #endregion

        #region ProcessPowerPointJobs
        /// <summary>
        /// Retrieves and processes a queue of PowerPoint Converter jobs.
        /// </summary>
        public void ProcessPowerPointJobs()
        {    
            // only execute if there is a database server, name, and warehouse folder for the instance; DO NOT throw errors here, this should just die quietly if we dont have what we need
            if (!String.IsNullOrWhiteSpace(this.InquisiqInstanceDBServer) && !String.IsNullOrWhiteSpace(this.InquisiqInstanceDBName) && !String.IsNullOrWhiteSpace(this.InquisiqInstanceWarehousePath))
            {
                // append a \ to the end of the inquisiq instance warehouse path if there is not already one
                if (!this.InquisiqInstanceWarehousePath.EndsWith("\\"))
                { this.InquisiqInstanceWarehousePath += "\\"; }

                string currentQueueIds = String.Empty;

                try
                {
                    DataTable queueItemsToProcess = this._GetCurrentQueueItems();

                    // only do work if there are items to process
                    if (queueItemsToProcess.Rows.Count > 0)
                    {
                        // get the ids of the items from the queue so we can set the processing flags
                        foreach (DataRow row in queueItemsToProcess.Rows)
                        { currentQueueIds += row["idSCORMPackage"].ToString() + ","; }

                        // remove the last , from the string of ids
                        if (currentQueueIds.EndsWith(","))
                        { currentQueueIds = currentQueueIds.TrimEnd(','); }

                        // set the processing flags for the items
                        this._SetProcessingFlagForQueueItems(currentQueueIds);

                        foreach (DataRow row in queueItemsToProcess.Rows)
                        {
                            // content properties
                            int idSCORMPackage = Convert.ToInt32(row["idSCORMPackage"]);
                            
                            bool isShared = Convert.ToBoolean(row["isShared"]);
                            string portalPath = row["portalPath"].ToString();
                            string packagePath = row["path"].ToString();

                            string completePackageRelativePath = String.Empty;

                            if (isShared)
                            { completePackageRelativePath = "shared\\" + packagePath; }
                            else
                            { completePackageRelativePath = "sco\\" + portalPath + "\\" + packagePath; }

                            //string packageFolder = row["path"].ToString();
                            string pptFilename = row["originalMediaFilename"].ToString();
                            string contentTitle = row["contentTitle"].ToString();

                            // content settings
                            bool enableAutoplay = Convert.ToBoolean(Convert.ToInt32(row["enableAutoplay"]));
                            bool allowRewind = Convert.ToBoolean(Convert.ToInt32(row["allowRewind"]));
                            bool allowFastForward = Convert.ToBoolean(Convert.ToInt32(row["allowFastForward"]));
                            bool allowNavigation = Convert.ToBoolean(Convert.ToInt32(row["allowNavigation"]));
                            bool allowResume = Convert.ToBoolean(Convert.ToInt32(row["allowResume"]));
                            double minProgressForCompletion = Convert.ToDouble(row["minProgressForCompletion"]);

                            // convert the PowerPoint file to slides and return list of slide files created
                            AsentiaPPTConverterService.ConvertPowerPointFileToSlides(this.InquisiqInstanceWarehousePath + completePackageRelativePath, pptFilename);

                            // get the number of slides that were saved
                            int slideCount = Directory.GetFiles(this.InquisiqInstanceWarehousePath + completePackageRelativePath + "\\images", "*.png", SearchOption.TopDirectoryOnly).Length;

                            // write the JS string array of slide files for the config file
                            string slideArrayStringForConfigJS = String.Empty;
                            int i;

                            for (i = 1; i <= slideCount; i++)
                            { slideArrayStringForConfigJS += "\"Slide" + i.ToString() + ".png\","; }

                            // remove the last , from the string
                            if (slideArrayStringForConfigJS.EndsWith(","))
                            { slideArrayStringForConfigJS.TrimEnd(','); }

                            // generate a manifest identifier
                            string manifestIdentifier = "_" + Guid.NewGuid().ToString();

                            // open the package's imsmanifest.xml, index.html, and /script/config.js files to replace placeholders with package specific information

                            // imsmanifest.xml
                            string imsManifestContent = String.Empty;

                            using (StreamReader reader = new StreamReader(this.InquisiqInstanceWarehousePath + completePackageRelativePath + "\\imsmanifest.xml"))
                            {
                                imsManifestContent = reader.ReadToEnd();
                                reader.Close();
                            }

                            imsManifestContent = Regex.Replace(imsManifestContent, "##manifestIdentifier##", manifestIdentifier);
                            imsManifestContent = Regex.Replace(imsManifestContent, "##contentTitle##", contentTitle);

                            File.Delete(this.InquisiqInstanceWarehousePath + completePackageRelativePath + "\\imsmanifest.xml");

                            using (StreamWriter writer = new StreamWriter(this.InquisiqInstanceWarehousePath + completePackageRelativePath + "\\imsmanifest.xml"))
                            {
                                writer.Write(imsManifestContent);
                                writer.Close();
                            }

                            // index.html
                            string indexContent = String.Empty;

                            using (StreamReader reader = new StreamReader(this.InquisiqInstanceWarehousePath + completePackageRelativePath + "\\index.html"))
                            {
                                indexContent = reader.ReadToEnd();
                                reader.Close();
                            }

                            indexContent = Regex.Replace(indexContent, "##contentTitle##", contentTitle);

                            File.Delete(this.InquisiqInstanceWarehousePath + completePackageRelativePath + "\\index.html");

                            using (StreamWriter writer = new StreamWriter(this.InquisiqInstanceWarehousePath + completePackageRelativePath + "\\index.html"))
                            {
                                writer.Write(indexContent);
                                writer.Close();
                            }

                            // /script/config.js
                            string configContent = String.Empty;

                            using (StreamReader reader = new StreamReader(this.InquisiqInstanceWarehousePath + completePackageRelativePath + "\\script\\config.js"))
                            {
                                configContent = reader.ReadToEnd();
                                reader.Close();
                            }

                            configContent = Regex.Replace(configContent, "##slides##", slideArrayStringForConfigJS);
                            configContent = Regex.Replace(configContent, "##enableAutoplay##", enableAutoplay.ToString().ToLower());
                            configContent = Regex.Replace(configContent, "##allowResume##", allowResume.ToString().ToLower());
                            configContent = Regex.Replace(configContent, "##allowFastForward##", allowFastForward.ToString().ToLower());
                            configContent = Regex.Replace(configContent, "##allowRewind##", allowRewind.ToString().ToLower());
                            configContent = Regex.Replace(configContent, "##allowNavigation##", allowNavigation.ToString().ToLower());
                            configContent = Regex.Replace(configContent, "##minProgressForCompletion##", minProgressForCompletion.ToString());

                            File.Delete(this.InquisiqInstanceWarehousePath + completePackageRelativePath + "\\script\\config.js");

                            using (StreamWriter writer = new StreamWriter(this.InquisiqInstanceWarehousePath + completePackageRelativePath + "\\script\\config.js"))
                            {
                                writer.Write(configContent);
                                writer.Close();
                            }

                            // get the size of the package in KB
                            int packageSizeInKb = Utility.CalculateDirectorySizeInKb(this.InquisiqInstanceWarehousePath + completePackageRelativePath);

                            // update the processed package information
                            this._UpdateProcessedQueueItem(idSCORMPackage, packageSizeInKb, imsManifestContent, manifestIdentifier, "index.html");
                        }
                    }
                }
                catch
                { throw; }
            }
        }
        #endregion

        #region _GetCurrentQueueItems
        /// <summary>
        /// Grabs a set of currently queued PowerPoint packages to be processed.
        /// Note, if this is called, it means we have what we need to execute, so we do not need to check the Server, DB Name, or Warehouse Folder properties.
        /// </summary>
        /// <returns></returns>
        private DataTable _GetCurrentQueueItems()
        {
            string inquisiqDBLogin = ConfigurationManager.AppSettings["InquisiqInventoryDBLogin"];
            string inquisiqDBPassword = ConfigurationManager.AppSettings["InquisiqInventoryDBPassword"];

            DataTable currentQueueItems = new DataTable();

            // if somehow we do not have a server and database to connect to, exit
            if (String.IsNullOrWhiteSpace(this.InquisiqInstanceDBServer) || String.IsNullOrWhiteSpace(this.InquisiqInstanceDBName))
            { return currentQueueItems; }

            try
            {
                AsentiaDatabase databaseObject = new AsentiaDatabase(this.InquisiqInstanceDBServer, this.InquisiqInstanceDBName, inquisiqDBLogin, inquisiqDBPassword, "InquisiqPowerPointProcessor", 60);

                databaseObject.AddParameter("@numItems", _NUMBER_OF_ITEMS_TO_PROCESS, SqlDbType.Int, 4, ParameterDirection.Input);

                try
                {
                    SqlDataReader sqlDataReader = databaseObject.ExecuteDataReader("[powerPointProcessor.getCurrentQueueItems]", true);
                    currentQueueItems.Load(sqlDataReader);
                    sqlDataReader.Close();
                }
                catch
                { throw; }
                finally
                { databaseObject.Dispose(); }
            }
            catch
            { }

            return currentQueueItems;            
        }
        #endregion

        #region _SetProcessingFlagForQueueItems
        /// <summary>
        /// Sets the processing flag for the PowerPoint packages that we are currently processing.
        /// Note, if this is called, it means we have what we need to execute, so we do not need to check the Server, DB Name, or Warehouse Folder properties.
        /// </summary>
        /// <param name="idList">comma separated list of package ids to mark as processing</param>
        private void _SetProcessingFlagForQueueItems(string idList)
        {
            string inquisiqDBLogin = ConfigurationManager.AppSettings["InquisiqInventoryDBLogin"];
            string inquisiqDBPassword = ConfigurationManager.AppSettings["InquisiqInventoryDBPassword"];

            AsentiaDatabase databaseObject = new AsentiaDatabase(this.InquisiqInstanceDBServer, this.InquisiqInstanceDBName, inquisiqDBLogin, inquisiqDBPassword, "InquisiqPowerPointProcessor", 60);

            databaseObject.AddParameter("@idList", idList, SqlDbType.NVarChar, 2000, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[powerPointProcessor.setProcessingFlagForQueueItems]", true);
            }
            catch
            { throw; }
            finally
            { databaseObject.Dispose(); }
        }
        #endregion

        #region _UpdateProcessedQueueItem
        /// <summary>
        /// Sets the processed flag for a PowerPoint package that has been processed. Also updates the 
        /// package size, manifest, manifest identifier, and resource href.
        /// Note, if this is called, it means we have what we need to execute, so we do not need to check the Server, DB Name, or Warehouse Folder properties.
        /// </summary>
        /// <param name="idSCORMPackage">id of the package to mark as processed</param>
        /// <param name="kb">the size of the package</param>
        /// <param name="imsmanifest">the package's manifest</param>
        /// <param name="manifestIdentifier">the package's manifest identifier</param>
        /// <param name="resourceHref">the resource href</param>
        private void _UpdateProcessedQueueItem(int idSCORMPackage, int kb, string imsmanifest, string manifestIdentifier, string resourceHref)
        {
            string inquisiqDBLogin = ConfigurationManager.AppSettings["InquisiqInventoryDBLogin"];
            string inquisiqDBPassword = ConfigurationManager.AppSettings["InquisiqInventoryDBPassword"];

            AsentiaDatabase databaseObject = new AsentiaDatabase(this.InquisiqInstanceDBServer, this.InquisiqInstanceDBName, inquisiqDBLogin, inquisiqDBPassword, "InquisiqPowerPointProcessor", 60);

            databaseObject.AddParameter("@idSCORMPackage", idSCORMPackage, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@kb", kb, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@imsmanifest", imsmanifest, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@manifestIdentifier", manifestIdentifier, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@resourceHref", resourceHref, SqlDbType.NVarChar, 2000, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[powerPointProcessor.updateProcessedQueueItem]", true);
            }
            catch
            { throw; }
            finally
            { databaseObject.Dispose(); }
        }
        #endregion
    }
}
