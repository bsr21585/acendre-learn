﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.CustomerManager.Pages.Default" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">    
    <inq:CustomerManagerAdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageTitleContainer" runat="server" />
    <asp:Panel ID="PageFeedbackContainer" runat="server" />
    <asp:Panel ID="MainMenuContainer" runat="server" />
</asp:Content>