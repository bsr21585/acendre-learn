﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="ApplicationSettings" type="System.Configuration.NameValueFileSectionHandler" />
    <section name="AccountSettings" type="System.Configuration.NameValueFileSectionHandler" />
    <section name="SessionStateDatabaseSettings" type="System.Configuration.NameValueFileSectionHandler" />
    <section name="CustomerManagerDatabaseSettings" type="System.Configuration.NameValueFileSectionHandler" />
    <section name="EmailSettings" type="System.Configuration.NameValueFileSectionHandler" />
    <section name="LoginSettings" type="System.Configuration.NameValueFileSectionHandler" />
    <section name="AsentiaSettings" type="System.Configuration.NameValueFileSectionHandler" />
  </configSections>
  
  <ApplicationSettings>
    <add key="ApplicationName" value="Asentia Customer Manager" />
    <add key="MaximumFileUploadSizeInBytes" value="1073741824" />
    <!-- 1 GB -->
    <add key="Throw404OnNonAuthenticatedPages" value="false" />
    <add key="DefaultTimezone" value="Eastern Standard Time" />
    <add key="DefaultLanguage" value="en-US" />
    <add key="CustomerManagerSSOSecretKey" value="j583hU85L8sh39l2nfppdjg76s05L83PL8jjslqJV74T49h0gbicf78grf4bvf69" />
    
    <!-- THE KEYS BELOW ARE USED IN PORTAL CLONING, USUALLY BOTH VALUES WILL BE THE SAME, BUT COULD BE DIFFERENT IF WE NEEDED AT ANY GIVEN MOMENT -->
    <add key="AsentiaContentSourceRootPath" value="D:\Data\Sites\Asentia" />
    <add key="AsentiaContentDestinationRootPath" value="D:\Data\Sites\Asentia" />
  </ApplicationSettings>
  
  <AccountSettings>
    <add key="SessionTimeout" value="20" />
    <add key="BaseDomain" value="asentiadev01.icslearning.local" />
    <add key="AdministratorEmail" value="joseph.carpenski@icslearninggroup.com" />
  </AccountSettings>
  
  <SessionStateDatabaseSettings>
    <add key="Server" value="sql2014" />
    <add key="Name" value="Asentia-SessionState" />
    <add key="ConnectionTimeout" value="30" />
    <add key="CommandTimeout" value="60" />
    <add key="UseConnectionPooling" value="true" />
    <add key="MinPoolSize" value="10" />
    <add key="MaxPoolSize" value="200" />
    <add key="UseTrustedConnection" value="true" />
  </SessionStateDatabaseSettings>
  
  <CustomerManagerDatabaseSettings>
    <add key="Server" value="sql2014" />
    <add key="Name" value="Asentia-CustomerManager" />
    <add key="ConnectionTimeout" value="30" />
    <add key="CommandTimeout" value="60" />
    <add key="UseConnectionPooling" value="true" />
    <add key="MinPoolSize" value="10" />
    <add key="MaxPoolSize" value="200" />
    <add key="UseTrustedConnection" value="true" />
  </CustomerManagerDatabaseSettings>
  
  <EmailSettings>
    <add key="SystemEmailAddress" value="no-reply@icslearninggroup.com" />
    <add key="SmtpServer" value="xmail01.icslearning.local" />
    <add key="SmtpPort" value="25" />
    <add key="SmtpUsername" value="" />
    <add key="SmtpPassword" value="" />
    <add key="SmtpUseSsl" value="false" />
  </EmailSettings>

  <LoginSettings></LoginSettings>

  <AsentiaSettings></AsentiaSettings>

  <!-- DO NOT EDIT THIS SECTION UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING! -->

  <!-- BEGIN SYSTEM.WEB -->
  <system.web>

    <!--<httpCookies requireSSL="true" />-->
    
    <customErrors mode="Off" />
    
    <compilation debug="false" targetFramework="4.5" />

    <httpRuntime requestValidationMode="2.0" maxRequestLength="1048576" targetFramework="4.5" />
    
    <sessionState mode="Custom" cookieless="false" cookieName="AsentiaLMSCustomerManager" customProvider="AsentiaSessionStateProvider">
      <providers>
        <add name="AsentiaSessionStateProvider" type="Asentia.Common.AsentiaSessionStateProvider" />
      </providers>
    </sessionState>

    <pages clientIDMode="Static" validateRequest="true" controlRenderingCompatibilityVersion="4.0">
      <controls>
        <add tagPrefix="atk" assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" />
        <add tagPrefix="inq" assembly="Asentia.Controls" namespace="Asentia.Controls" />
      </controls>
    </pages>

    <webServices>
      <protocols>
        <add name="HttpPost" />
      </protocols>
    </webServices>

    <authentication mode="None" />

    <machineKey decryptionKey="F7622A345D1E299FD09B18135A5D4C65E5443C91D1C146FD" validation="SHA1" validationKey="6774159B5AB2CFAB2F153DEAB44465AADA9EB017431E2D6175FB6D28E205FD6FF344BB9F69BE66CB5D371C21ABD1670A5EACC4C3502A0E322C8C3A04E8289B0A" />

    <httpHandlers>
      <add verb="*" path="AjaxFileUploadHandler.axd" type="AjaxControlToolkit.AjaxFileUploadHandler, AjaxControlToolkit" />
      <add verb="*" path="CombineScriptsHandler.axd" type="AjaxControlToolkit.CombineScriptsHandler, AjaxControlToolkit" />
    </httpHandlers>
    
  </system.web>
  <!-- END SYSTEM.WEB -->

  <system.webServer>

    <validation validateIntegratedModeConfiguration="false" />
    <security>
      <requestFiltering>
        <requestLimits maxAllowedContentLength="1073741824" />
      </requestFiltering>
    </security>

    <handlers>
      <add name="AjaxFileUploadHandler" verb="*" path="AjaxFileUploadHandler.axd" type="AjaxControlToolkit.AjaxFileUploadHandler, AjaxControlToolkit" />
      <add name="CombineScriptsHandler" verb="*" path="CombineScriptsHandler.axd" type="AjaxControlToolkit.CombineScriptsHandler, AjaxControlToolkit" />
    </handlers>

    <!-- UNCOMMENT BELOW TO REWRITE HTTP TO HTTPS, URL REWRITE MODULE FOR IIS MUST BE INSTALLED FIRST -->
    <!--
    <rewrite>
      <rules>
        <clear />
        <rule name="HTTPS_REDIRECT" stopProcessing="true">
          <match url=".*" />
          <conditions>
            <add input="{HTTPS}" pattern="off" ignoreCase="true" />
          </conditions>
          <action type="Redirect" url="https://{HTTP_HOST}{REQUEST_URI}" redirectType="Permanent" appendQueryString="false" />
        </rule>
      </rules>
    </rewrite>
    -->

  </system.webServer>

  <system.serviceModel>

    <behaviors>
      <serviceBehaviors>
        <behavior name="">
          <serviceMetadata httpGetEnabled="true" />
          <serviceDebug includeExceptionDetailInFaults="false" />
        </behavior>
      </serviceBehaviors>
    </behaviors>

    <serviceHostingEnvironment multipleSiteBindingsEnabled="true" aspNetCompatibilityEnabled="true" />

  </system.serviceModel>

  <runtime>

    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">

      <dependentAssembly>
        <assemblyIdentity name="HtmlAgilityPack" publicKeyToken="bd319b19eaf3b43a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.4.9.0" newVersion="1.4.9.0" />
      </dependentAssembly>

      <dependentAssembly>
        <assemblyIdentity name="AjaxMin" publicKeyToken="21ef50ce11b5d80f" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.11.5295.12304" newVersion="5.11.5295.12304" />
      </dependentAssembly>

      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.OData" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.6.4.0" newVersion="5.6.4.0" />
      </dependentAssembly>

    </assemblyBinding>

  </runtime>
</configuration>