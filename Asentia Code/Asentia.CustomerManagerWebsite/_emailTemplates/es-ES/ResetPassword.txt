﻿<SUBJECT>Restablecer Contraseña

Hola ##user_fullname##,

Su contraseña se ha restablecido. Usted puede ingresar con la nueva contraseña y luego modificarla mediante el acceso a la información de su cuenta:

Login: ##user_login##
Contraseña: ##user_password##

No responda a este mensaje, ya que ningún destinatario haya sido designado.

¡Gracias!
El Equipo Asentia