IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'IDTable' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE IDTable AS TABLE (
	[id]	INT		NOT NULL
	)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'KeyValueTable' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE KeyValueTable AS TABLE (
	[key]		NVARCHAR(255)	NOT NULL,
	[value]		NVARCHAR(255)	NULL
	)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'IDTableWithOrdering' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE IDTableWithOrdering AS TABLE (
	[id]		INT		NOT NULL,
	[order]		INT		NOT NULL
	)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'IDTableWithScope' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE IDTableWithScope AS TABLE (
	[id]	INT				NOT NULL,
	[scope] NVARCHAR(MAX)	NULL
	)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'UserImport' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE UserImport AS TABLE (
	[firstName]				NVARCHAR(255)	NOT NULL,
	[middleName]			NVARCHAR(255)	NULL,
	[lastname]				NVARCHAR(255)	NOT NULL,
	[email]					NVARCHAR(255)	NULL,
	[username]				NVARCHAR(512)	NOT NULL,
	[password]				NVARCHAR(512)	NOT NULL,
	[mustchangePassword]	BIT				NULL,
	[timezoneString]		NVARCHAR(50)	NULL,
	[languageString]		NVARCHAR(10)	NULL,
	[dtExpires]				DATETIME		NULL,
	[isActive]				BIT				NOT NULL,
	[company]				NVARCHAR(255)	NULL,
	[address]				NVARCHAR(512)	NULL,
	[city]					NVARCHAR(255)	NULL,
	[province]				NVARCHAR(255)	NULL,
	[postalcode]			NVARCHAR(25)	NULL,
	[country]				NVARCHAR(50)	NULL,
	[phonePrimary]			NVARCHAR(25)	NULL,
	[phoneWork]				NVARCHAR(25)	NULL,
	[phoneFax]				NVARCHAR(25)	NULL,
	[phoneHome]				NVARCHAR(25)	NULL,
	[phoneMobile]			NVARCHAR(25)	NULL,
	[phonePager]			NVARCHAR(25)	NULL,
	[phoneOther]			NVARCHAR(25)	NULL,
	[employeeID]			NVARCHAR(255)	NULL,
	[jobTitle]				NVARCHAR(255)	NULL,
	[jobClass]				NVARCHAR(255)	NULL,
	[division]				NVARCHAR(255)	NULL,
	[region]				NVARCHAR(255)	NULL,
	[department]			NVARCHAR(255)	NULL,
	[dtHire]				DATETIME		NULL,
	[dtTerm]				DATETIME		NULL,
	[gender]				NVARCHAR(1)		NULL,
	[race]					NVARCHAR(64)	NULL,
	[dtDOB]					DATETIME		NULL,
	[field00]				NVARCHAR(255)	NULL,
	[field01]				NVARCHAR(255)	NULL,
	[field02]				NVARCHAR(255)	NULL,
	[field03]				NVARCHAR(255)	NULL,
	[field04]				NVARCHAR(255)	NULL,
	[field05]				NVARCHAR(255)	NULL,
	[field06]				NVARCHAR(255)	NULL,
	[field07]				NVARCHAR(255)	NULL,
	[field08]				NVARCHAR(255)	NULL,
	[field09]				NVARCHAR(255)	NULL,
	[field10]				NVARCHAR(255)	NULL,
	[field11]				NVARCHAR(255)	NULL,
	[field12]				NVARCHAR(255)	NULL,
	[field13]				NVARCHAR(255)	NULL,
	[field14]				NVARCHAR(255)	NULL,
	[field15]				NVARCHAR(255)	NULL,
	[field16]				NVARCHAR(255)	NULL,
	[field17]				NVARCHAR(255)	NULL,
	[field18]				NVARCHAR(255)	NULL,
	[field19]				NVARCHAR(255)	NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'ActivityImport' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE ActivityImport AS TABLE (
	[username]			NVARCHAR(255)	NOT NULL,
	[courseCode]		NVARCHAR(255)	NULL,
	[courseName]		NVARCHAR(255)	NULL,
	[credits]			FLOAT			NULL,
	[dtCompleted]		DATETIME		NOT NULL,
	[lessonName]		NVARCHAR(255)	NULL,
	[completionStatus]	NVARCHAR(10)	NULL,
	[successStatus]		NVARCHAR(7)		NULL,
	[scoreScaled]		FLOAT			NULL,
	[timestamp]			DATETIME		NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'CertificateImport' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE CertificateImport AS TABLE (
	[username]				NVARCHAR(255)	NOT NULL,
	[certificateCode]		NVARCHAR(25)	NOT NULL,
	[certificateAwardDate]	DATETIME		NOT NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'DateRange' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE DateRange AS TABLE (
	[dtStart]		DATETIME	NOT NULL,
	[dtEnd]			DATETIME	NOT NULL,
	[idTimezone]	INT			NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'EventLogItemObjects' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE EventLogItemObjects AS TABLE (
	[idSite]			INT			NOT NULL,
	[idObject]			INT			NOT NULL,
	[idObjectRelated]	INT			NOT NULL,
	[idObjectUser]		INT			NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'RuleTable' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE RuleTable AS TABLE (
	[idRule]		INT				NOT NULL,
	[idSite]		INT				NOT NULL,
	[userField]		NVARCHAR(25)	NULL,
	[operator]		NVARCHAR(25)	NOT NULL,
	[textValue]		NVARCHAR(255)	NULL,
	[dateValue]		DATETIME		NULL,
	[numValue]		INT				NULL,
	[bitValue]		BIT				NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'IDTableCourseWithOrdering' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE [IDTableCourseWithOrdering] AS TABLE (
	[idCatalog]		INT		NULL,
	[idCourse]		INT		NOT NULL,
	[order]			INT		NOT NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'UserNameTable' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE UserNameTable AS TABLE (
	[username]	NVARCHAR(255)	NOT NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'CourseCodeTable' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE CourseCodeTable AS TABLE (
	[courseCode]	NVARCHAR(255)	NOT NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'TinCanStatementTable' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE TinCanStatementTable AS TABLE (
	[statementId]				NVARCHAR(50)	NULL,
	[parentStatementId]			NVARCHAR(50)	NULL,
	[actor]						NVARCHAR(MAX)	NOT NULL,
	[verbId]					NVARCHAR(100)	NOT NULL,
	[verb]						NVARCHAR(200)	NOT NULL,
	[activityId]				NVARCHAR(300)	NULL,
	[mboxObject]				NVARCHAR(100)	NULL,
	[mboxSha1SumObject]			NVARCHAR(100)	NULL,
	[openIdObject]				NVARCHAR(100)	NULL,
	[accountObject]				NVARCHAR(100)	NULL,
	[mboxActor]					NVARCHAR(100)	NULL,
	[mboxSha1SumActor]			NVARCHAR(100)	NULL,
	[openIdActor]				NVARCHAR(100)	NULL,
	[accountActor]				NVARCHAR(100)	NULL,
	[mboxAuthority]				NVARCHAR(100)	NULL,
	[mboxSha1SumAuthority]		NVARCHAR(100)	NULL,
	[openIdAuthority]			NVARCHAR(100)	NULL,
	[accountAuthority]			NVARCHAR(100)	NULL,
	[mboxTeam]					NVARCHAR(100)	NULL,
	[mboxSha1SumTeam]			NVARCHAR(100)	NULL,
	[openIdTeam]				NVARCHAR(100)	NULL,
	[accountTeam]				NVARCHAR(100)	NULL,
	[mboxInstructor]			NVARCHAR(100)	NULL,
	[mboxSha1SumInstructor]		NVARCHAR(100)	NULL,
	[openIdInstructor]			NVARCHAR(100)	NULL,
	[accountInstructor]			NVARCHAR(100)	NULL,
	[object]					NVARCHAR(MAX)	NOT NULL,
	[registration]				NVARCHAR(50)	NULL,
	[statement]					NVARCHAR(MAX)	NOT NULL,
	[statementIdToBeVoided]		NVARCHAR(50)	NULL,
	[scoreScaled]				FLOAT     	    NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'TinCanContextActivitiesTable' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE TinCanContextActivitiesTable AS TABLE (
	[statementId]	NVARCHAR(50)	NOT NULL,
	[activityId]	NVARCHAR(100)	NOT NULL,
	[activityType]	NVARCHAR(10)	NOT NULL,
	[objectType]	NVARCHAR(30)	NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'StandupTrainingInstanceRoster' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE StandupTrainingInstanceRoster AS TABLE (
	idUser 				INT		NOT NULL,
	completionStatus 	INT		NULL,
	successStatus		INT		NULL,
	score				INT		NULL,
	isWaitList 			BIT		NOT NULL
)
GO

IF NOT EXISTS (SELECT 1 FROM sys.types WHERE name = N'ScoInteractionsTable' AND is_user_defined = 1 AND is_table_type = 1)
CREATE TYPE [ScoInteractionsTable] AS TABLE (
	id					nvarchar(255) NOT NULL,
	result				nvarchar(255) NULL,
	latency				float NULL
)
GO

