﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" Inherits="Asentia.CustomerManager.Pages.System.Database.Modify" %>



<%--<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadPlaceholder" runat="server">
  <%-- <script type="text/javascript" src="/_scripts/ckeditor/ckeditor.js"></script>
</asp:Content>--%>

<asp:content id="BreadcrumbContent" contentplaceholderid="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">    
    <inq:CustomerManagerAdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="DatabasePropertiesWrapperContainer" runat="server">
            <asp:Panel ID="DatabasePropertiesFeedbackContainer" runat="server" />
            <asp:Panel ID="DatabasePropertiesInstructionsPanel" runat="server" />
            <asp:Panel ID="DatabasePropertiesContainer" runat="server" />
            <asp:Panel ID="DatabasePropertiesActionsPanel" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:content>

<asp:content id="SideContent2" contentplaceholderid="SideContentPlaceholder2" runat="server">
</asp:content>
