﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.CustomerManager.Pages.System.License.Default" %>

<asp:content id="BreadcrumbContent" contentplaceholderid="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">    
    <inq:CustomerManagerAdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">
     <asp:Panel ID="PageContentContainer" runat="server">
    <asp:Panel ID="PageTitleContainer" runat="server" />
       <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
    <asp:Panel ID="PageFeedbackContainer" runat="server" />
         <asp:UpdatePanel ID="LicenseAgreementPanel" UpdateMode="Conditional" runat="server" />
            
        <asp:Panel ID="ActionsPanel" runat="server" />
  </asp:Panel>
</asp:content>
