﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.CustomerManager.Pages.ClonePortal.Default" %>

<asp:content id="BreadcrumbContent" contentplaceholderid="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">    
    <inq:CustomerManagerAdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />        
        <asp:Panel ID="UserFormWrapperContainer" runat="server">
         <asp:UpdatePanel runat="server" id="DataBaseFormUpdatePanel" >            
             <ContentTemplate>
                     <asp:Panel ID="PageFeedbackContainer" runat="server" />
                       <asp:Panel ID="PageInstructionsPanel" runat="server" />
                      <asp:Panel ID="DatabaseFormContainer" runat="server" />
                      <asp:Panel ID="ActionsPanel" runat="server" />
            </ContentTemplate>
         </asp:UpdatePanel>
        </asp:Panel>                   
    </asp:Panel>
</asp:content>
