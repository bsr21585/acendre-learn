﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.CustomerManager.Pages.Accounts.Sites.Default" %>


<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">    
    <inq:CustomerManagerAdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="ObjectOptionsPanel" runat="server" />
        <asp:UpdatePanel ID="SiteGridUpdatePanel" UpdateMode="Conditional" runat="server">
            <ContentTemplate></ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="GridActionsPanel" runat="server" />
    </asp:Panel>
</asp:Content>
