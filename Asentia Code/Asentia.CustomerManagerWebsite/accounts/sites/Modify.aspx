﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" Inherits="Asentia.CustomerManager.Pages.Accounts.Sites.Modify" %>



<%--<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadPlaceholder" runat="server">
  <%-- <script type="text/javascript" src="/_scripts/ckeditor/ckeditor.js"></script>
</asp:Content>--%>

<asp:content id="BreadcrumbContent" contentplaceholderid="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">    
    <inq:CustomerManagerAdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="SitePropertiesWrapperContainer" runat="server">
            <asp:Panel ID="SitePropertiesFeedbackContainer" runat="server" />
            <asp:Panel ID="SitePropertiesInstructionsPanel" runat="server" />
            <asp:Panel ID="SitePropertiesContainer" runat="server" />
            <asp:Panel ID="SitePropertiesActionsPanel" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:content>

<asp:content id="SideContent2" contentplaceholderid="SideContentPlaceholder2" runat="server">
</asp:content>
