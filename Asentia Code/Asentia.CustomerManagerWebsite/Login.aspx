﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.CustomerManager.Pages.Login" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageTitleContainer" runat="server" />
    <asp:Panel ID="ContentContainer" runat="server" />
    <asp:Panel ID="PageFeedbackContainer" runat="server" />
</asp:Content>