﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using Asentia.Common;

namespace Asentia.JobProcessor
{
    public partial class AsentiaService : ServiceBase
    {
        #region Private Properties
        private bool _StopService = true;

        private System.Timers.Timer _AccountJobTimer;
        private System.Timers.Timer _GlobalJobTimer;

        private bool _IsRetrievingJob = false;
        #endregion

        #region Constructor
        public AsentiaService()
        {
            InitializeComponent();

            this._StopService = Convert.ToBoolean(ConfigurationManager.AppSettings["StopService"]);
        }
        #endregion

        #region OnStart
        protected override void OnStart(string[] args)
        {
            // Not sure if we're going to set MAX threads or just let the system handle it,
            // this will depend on unit testing.

            // Let's just do 1 thread for the moment.
            ThreadPool.SetMaxThreads(1, 1);

            // Note that we set timers here statically instead of setting them via
            // the config file because we do not want any kind of outside control
            // for the timers, they're too sensitive.

            // Set the timer for account jobs
            // This is the timer for processing jobs for each account database.
            //
            // TODO: Add condition for single database installation, probably key to
            // license number, because timer will not need to run every half second
            // for a single account database installation.
            this._AccountJobTimer = new System.Timers.Timer(500D);  // 500 milliseconds = 1/2 second
            this._AccountJobTimer.AutoReset = true;
            this._AccountJobTimer.Elapsed += new System.Timers.ElapsedEventHandler(this._AccountJobTimer_Elapsed);
            this._AccountJobTimer.Start();

            // Set the timer for global jobs
            // This is the timer for processing jobs for the system, i.e. server pings,
            // session state cleanup, etc.
            // Generally these are system level jobs that don't need to be run as often
            // and don't need multiple threads, hence the slower timer.
            this._GlobalJobTimer = new System.Timers.Timer(600000D);  // 600000 milliseconds = 10 minutes
            this._GlobalJobTimer.AutoReset = true;
            this._GlobalJobTimer.Elapsed += new System.Timers.ElapsedEventHandler(this._GlobalJobTimer_Elapsed);
            this._GlobalJobTimer.Start();
        }
        #endregion

        #region OnStop
        protected override void OnStop()
        {
            this._AccountJobTimer.Stop();
            this._AccountJobTimer = null;

            this._GlobalJobTimer.Stop();
            this._GlobalJobTimer = null;
        }
        #endregion

        #region _AccountJobTimer_Elapsed
        /// <summary>
        /// The timer elapsed action for account jobs. This performs the email queue
        /// and report processor queue actions for the account databases.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AccountJobTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // if the service is not set to be stopped, proceed processing jobs
            if (!this._StopService)
            {
                int idAccount = 0;

                try
                {
                    // instansiate the AccountJobs class instead of using static 
                    // methods to keep this service thread safe
                    AccountJobs accountJobsInstance = new AccountJobs(this.ServiceName);

                    // accounts pull and entry point here
                    if (!this._IsRetrievingJob)
                    {
                        // set the retrieving job flag
                        this._IsRetrievingJob = true;

                        // get the next account to be processed and set the processing flag
                        idAccount = this._GetNextAccountIdForProcessing();

                        if (idAccount > 0)
                        { this._SetProcessingFlagForAccount(idAccount); }

                        // release the retrieving job flag
                        this._IsRetrievingJob = false;

                        // process the jobs in the database
                        if (idAccount > 0)
                        {
                            accountJobsInstance.ProcessDatabase(idAccount);


                            // release the processing flag for the account
                            this._ReleaseProcessingFlagForAccount(idAccount);
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteToLog(this.ServiceName, ex.Message + " | " + ex.StackTrace, true, true);

                    // release the retrieving job flag
                    this._IsRetrievingJob = false;

                    // release the processing flag for the account
                    if (idAccount > 0)
                    { this._ReleaseProcessingFlagForAccount(idAccount); }
                }
                finally
                { }
            }
        }
        #endregion

        #region _GlobalJobTimer_Elapsed
        /// <summary>
        /// The timer elapsed action for global jobs. This performs any actions that are
        /// global to the system.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _GlobalJobTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // if the service is not set to be stopped, proceed processing jobs
            if (!this._StopService)
            {
                try
                {
                    // instansiate the GlobalJobs class instead of using static 
                    // methods to keep this service thread safe
                    GlobalJobs globalJobsInstance = new GlobalJobs(this.ServiceName);

                    // process jobs
                    // globalJobsInstance.KeepApplicationPoolsAlive();
                }
                catch (Exception ex)
                { WriteToLog(this.ServiceName, ex.Message + " | " + ex.StackTrace, true, true); }
            }
        }
        #endregion

        #region _GetNextAccountIdForProcessing
        /// <summary>
        /// Gets the next account id for account job processing.
        /// </summary>
        /// <returns></returns>
        private int _GetNextAccountIdForProcessing()
        {
            int idAccount = 0;

            string rootWebConfigPath = ConfigurationManager.AppSettings["AsentiaRootFolder"];          
            AsentiaDatabase customerManagerDatabaseObject = new AsentiaDatabase(rootWebConfigPath, DatabaseType.CustomerManagerDatabaseUsingWebConfigPath);

            customerManagerDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            customerManagerDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            customerManagerDatabaseObject.AddParameter("@idCallerAccount", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@callerLangString", DBNull.Value, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sqlDataReader = customerManagerDatabaseObject.ExecuteDataReader("[JobProcessor.GetNextAccountForProcessing]", true);
                sqlDataReader.Read();

                if (sqlDataReader.HasRows)
                { idAccount = Convert.ToInt32(sqlDataReader["idAccount"]); }

                // close the SqlDataReader
                sqlDataReader.Close();

                return idAccount;
            }
            catch
            { throw; }
            finally
            { customerManagerDatabaseObject.Dispose(); }
        }
        #endregion

        #region _SetProcessingFlagForAccount
        /// <summary>
        /// Sets the processing flag for the account currently being processed.
        /// </summary>
        /// <param name="idAccount"></param>
        private void _SetProcessingFlagForAccount(int idAccount)
        {            
            string rootWebConfigPath = ConfigurationManager.AppSettings["AsentiaRootFolder"];            
            AsentiaDatabase customerManagerDatabaseObject = new AsentiaDatabase(rootWebConfigPath, DatabaseType.CustomerManagerDatabaseUsingWebConfigPath);

            customerManagerDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            customerManagerDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            customerManagerDatabaseObject.AddParameter("@idCallerAccount", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@callerLangString", DBNull.Value, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            customerManagerDatabaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                customerManagerDatabaseObject.ExecuteNonQuery("[JobProcessor.SetProcessingFlagForAccount]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(customerManagerDatabaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = customerManagerDatabaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { customerManagerDatabaseObject.Dispose(); }
        }
        #endregion

        #region _ReleaseProcessingFlagForAccount
        /// <summary>
        /// Releases the processing flag for the account that was processed.
        /// </summary>
        /// <param name="idAccount"></param>
        private void _ReleaseProcessingFlagForAccount(int idAccount)
        {
            string rootWebConfigPath = ConfigurationManager.AppSettings["AsentiaRootFolder"];
            AsentiaDatabase customerManagerDatabaseObject = new AsentiaDatabase(rootWebConfigPath, DatabaseType.CustomerManagerDatabaseUsingWebConfigPath);

            customerManagerDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            customerManagerDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            customerManagerDatabaseObject.AddParameter("@idCallerAccount", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@callerLangString", DBNull.Value, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            customerManagerDatabaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

            customerManagerDatabaseObject.AddParameter("@idAccount", idAccount, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                customerManagerDatabaseObject.ExecuteNonQuery("[JobProcessor.ReleaseProcessingFlagForAccount]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(customerManagerDatabaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = customerManagerDatabaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { customerManagerDatabaseObject.Dispose(); }
        }
        #endregion

        #region WriteToLog
        public static void WriteToLog(string serviceIdentifier, string message, bool isError, bool writeToSysEventLog)
        {
            // use a try, catch here; but do absolutely nothing in the catch
            // the reasons, 1) we do not ever want this service getting hung
            // up because of an exception, 2) inability to write to a log
            // indicates something fatal, and 3) we will discover "fatal"
            // issues through the lack of log entries
            try
            {
                if (writeToSysEventLog)
                {
                    EventLog applicationLog = new EventLog();
                    applicationLog.Source = serviceIdentifier;

                    if (isError)
                    { applicationLog.WriteEntry(message, EventLogEntryType.Error); }
                    else
                    { applicationLog.WriteEntry(message, EventLogEntryType.Information); }
                }
            }
            // just swallow the exception
            catch
            { ; }
        }
        #endregion
    }
}
