﻿using System;
using System.ServiceProcess;

namespace Asentia.JobProcessor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new AsentiaService() 
			};

            ServiceBase.Run(ServicesToRun);
        }
    }
}
