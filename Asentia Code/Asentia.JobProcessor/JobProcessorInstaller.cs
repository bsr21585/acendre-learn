﻿using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;


namespace Asentia.JobProcessor
{
    [RunInstaller(true)]
    public partial class JobProcessorInstaller : System.Configuration.Install.Installer
    {
        public JobProcessorInstaller()
        {
            ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            // Service Account Information
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            // Service Information
            serviceInstaller.DisplayName = "Asentia Job Processor";
            serviceInstaller.ServiceName = "AsentiaJobProcessorService";
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
