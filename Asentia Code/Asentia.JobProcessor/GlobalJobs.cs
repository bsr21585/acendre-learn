﻿using System;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using Asentia.Common;

namespace Asentia.JobProcessor
{
    public class GlobalJobs
    {
        #region Properties
        public string ServiceName = String.Empty;
        #endregion

        #region Constructor
        public GlobalJobs(string serviceName)
        { this.ServiceName = serviceName; }
        #endregion

        #region KeepApplicationPoolsAlive
        /// <summary>
        /// Pings the application to keep application pool alive.
        /// </summary>
        public void KeepApplicationPoolsAlive()
        {
            // get the server addresses to ping from the config
            string serversToPingKey = ConfigurationManager.AppSettings["ServersToPing"];
            string[] serversToPing = serversToPingKey.Split('|');

            for (int i = 0; i < serversToPing.Length; i++)
            {
                // ping it
                WebRequest request = WebRequest.Create("http://" + serversToPing[i]);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();

                // log it
                string logMessage = "Successfully pinged " + serversToPing[i] + ".";
                AsentiaService.WriteToLog(this.ServiceName, logMessage, false, true);
            }
        }
        #endregion
    }
}
