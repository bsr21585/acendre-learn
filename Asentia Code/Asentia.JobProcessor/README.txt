﻿TO INSTALL THE SERVICE, USE INSTRUCTIONS FROM MSDN

https://msdn.microsoft.com/en-us/library/sd8zc8ha(v=vs.110).aspx

COMMAND LINE:

cd C:\Windows\Microsoft.NET\Framework\v4.0.30319\
InstallUtil.exe "[Path to Asentia.JobProcessor.exe]"

Once service is installed, set it to automatic start, set the running as user to icslearning\asentialms
or whatever your IIS application pool is running as. Then start the service.

Note, the run as user must also have database access to all Asentia account databases.