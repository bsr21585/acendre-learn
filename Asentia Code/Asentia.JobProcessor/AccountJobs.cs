﻿using System;
using System.Web;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using Asentia.Common;
using Asentia.LMS.Library;
using Asentia.LRS.Library;

namespace Asentia.JobProcessor
{
    public class AccountJobs
    {
        #region Properties
        public string ServiceName = String.Empty;
        #endregion

        #region Constructor
        public AccountJobs(string serviceName)
        { this.ServiceName = serviceName; }
        #endregion

        #region ProcessDatabase
        /// <summary>
        /// Processes the email and report subscription queues for the specified account database.
        /// </summary>
        /// <param name="idAccount">the account id to process</param>
        public void ProcessDatabase(int idAccount)
        {
            string accountsPath = ConfigurationManager.AppSettings["AsentiaAccountsFolder"];
            string accountWebConfigPath = accountsPath + idAccount;

            if (Directory.Exists(accountWebConfigPath))
            {
                if (File.Exists(accountWebConfigPath + "\\web.config"))
                {
                    // load database object with this web config
                    AsentiaDatabase databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath);

                    // process the event email queue
                    ProcessEventEmailQueue(accountWebConfigPath);

                    // process the report subscription queue
                    ProcessReportSubscriptionQueue(accountWebConfigPath);

                    databaseObject.Dispose();
                }
                else
                {
                    // throw new AsentiaException("Web.config file for account " + idAccount.ToString() + " does not exist.");
                }
            }
            else
            {
                // throw new AsentiaException("Directory for account " + idAccount.ToString() + " does not exist.");
            }
        }
        #endregion

        #region ProcessEventEmailQueue
        /// <summary>
        /// Processes the event email queue for the system.
        /// </summary>
        /// <param name="accountWebConfigPath">path to the account database's web.config</param>
        public void ProcessEventEmailQueue(string accountWebConfigPath)
        {
            try
            {
                
                DataTable currentEmailQueue = EventEmailQueue.GetCurrentItems(1, "en-US", 1, null, 1000, accountWebConfigPath);

                foreach (DataRow queueItem in currentEmailQueue.Rows)
                {
                    
                    // get the data from the row
                    int idEventEmailQueue = Convert.ToInt32(queueItem["idEventEmailQueue"]);
                    int idSite = Convert.ToInt32(queueItem["idSite"]);
                    string hostname = queueItem["hostname"].ToString();
                    int idEventLog = Convert.ToInt32(queueItem["idEventLog"]);
                    int idEventType = Convert.ToInt32(queueItem["idEventType"]);
                    int idEventEmailNotification = Convert.ToInt32(queueItem["idEventEmailNotification"]);
                    int idEventTypeRecipient = Convert.ToInt32(queueItem["idEventTypeRecipient"]);
                    int idObject = Convert.ToInt32(queueItem["idObject"]);
                    int idObjectRelated = Convert.ToInt32(queueItem["idObjectRelated"]);
                    string objectType = queueItem["objectType"].ToString();
                    int? idObjectUser = null;
                    string objectUserFullName = queueItem["objectUserFullName"].ToString();
                    string objectUserFirstName = queueItem["objectUserFirstName"].ToString();
                    string objectUserLogin = queueItem["objectUserLogin"].ToString();
                    string objectUserEmail = queueItem["objectUserEmail"].ToString();
                    int idRecipient = Convert.ToInt32(queueItem["idRecipient"]);
                    string recipientLangString = queueItem["recipientLangString"].ToString();
                    string recipientFullName = queueItem["recipientFullName"].ToString();
                    string recipientFirstName = queueItem["recipientFirstName"].ToString();
                    string recipientLogin = queueItem["recipientLogin"].ToString();
                    string recipientEmail = queueItem["recipientEmail"].ToString();
                    int recipientTimezone = Convert.ToInt32(queueItem["recipientTimezone"]);
                    string recipientTimezoneDotNetName = queueItem["recipientTimezoneDotNetName"].ToString();
                    string from = queueItem["from"].ToString();
                    string copyTo = queueItem["copyTo"].ToString();
                    string systemFromAddressOverride = queueItem["systemFromAddressOverride"].ToString();
                    string systemSmtpServerNameOverride = queueItem["systemSmtpServerNameOverride"].ToString();
                    string systemSmtpServerPortOverrideStr = queueItem["systemSmtpServerPortOverride"].ToString();
                    string systemSmtpServerUseSslOverrideStr = queueItem["systemSmtpServerUseSslOverride"].ToString();
                    string systemSmtpServerUsernameOverride = queueItem["systemSmtpServerUsernameOverride"].ToString();
                    string systemSmtpServerPasswordOverride = queueItem["systemSmtpServerPasswordOverride"].ToString();
                    int? priority = null;
                    bool isHTMLBased = false;
                    int? attachmentType = null;
                    string calendarString = String.Empty;
                    DateTime dtEvent = Convert.ToDateTime(queueItem["dtEvent"]);
                    DateTime dtAction = Convert.ToDateTime(queueItem["dtAction"]);
                    DateTime dtActivation = Convert.ToDateTime(queueItem["dtActivation"]);
                    string objectInformation = queueItem["objectInformation"].ToString();
                    
                    if (!String.IsNullOrWhiteSpace(queueItem["idObjectUser"].ToString()))
                    { idObjectUser = Convert.ToInt32(queueItem["idObjectUser"]); }

                    if (!String.IsNullOrWhiteSpace(queueItem["priority"].ToString()))
                    { priority = Convert.ToInt32(queueItem["priority"]); }

                    if (!String.IsNullOrWhiteSpace(queueItem["isHTMLBased"].ToString()) && Convert.ToBoolean(queueItem["isHTMLBased"]))
                    { isHTMLBased = true; }

                    if (!String.IsNullOrWhiteSpace(queueItem["attachmentType"].ToString()))
                    { attachmentType = Convert.ToInt32(queueItem["attachmentType"]); }

                    int systemSmtpServerPortOverride = 0;

                    if (!String.IsNullOrWhiteSpace(systemSmtpServerPortOverrideStr))
                    { Int32.TryParse(systemSmtpServerPortOverrideStr, out systemSmtpServerPortOverride); }

                    bool systemSmtpServerUseSslOverride = false;

                    if (!String.IsNullOrWhiteSpace(systemSmtpServerUseSslOverrideStr))
                    { Boolean.TryParse(systemSmtpServerUseSslOverrideStr, out systemSmtpServerUseSslOverride); }

                    // instansiate an EventEmail
                    EventEmail eventEmail = new EventEmail(idEventEmailNotification, hostname, from, copyTo, systemFromAddressOverride, systemSmtpServerNameOverride, systemSmtpServerPortOverride, systemSmtpServerUseSslOverride, systemSmtpServerUsernameOverride, systemSmtpServerPasswordOverride, priority, isHTMLBased, recipientLangString, ConfigurationManager.AppSettings["AsentiaRootFolder"]);
                    
                    // fill in the common elements that do not need an object binding
                    CultureInfo culture = new CultureInfo(recipientLangString);
                    Config.WebConfigFilePath = ConfigurationManager.AppSettings["AsentiaRootFolder"];
                    AsentiaAESEncryption cryptoURL = new AsentiaAESEncryption();
                    string encryptedURL = WebUtility.UrlEncode(cryptoURL.Encrypt(idRecipient + "|" + recipientFullName));
                    
                    eventEmail.LmsUrl = "http://" + hostname + "." + Config.AccountSettings.BaseDomain;
                    eventEmail.LmsHostname = hostname;
                    
                    eventEmail.EventDate = TimeZoneInfo.ConvertTimeFromUtc(dtEvent, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern);
                    eventEmail.EventTime = TimeZoneInfo.ConvertTimeFromUtc(dtEvent, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortTimePattern);
                    eventEmail.EmailOptOutLink = "http://" + hostname + "." + Config.AccountSettings.BaseDomain + "/_util/EmailOptOut/?token=" + encryptedURL;

                    eventEmail.UserFullName = objectUserFullName;
                    eventEmail.UserFirstName = objectUserFirstName;
                    eventEmail.UserLogin = objectUserLogin;
                    eventEmail.UserEmail = objectUserEmail;

                    eventEmail.RecipientFullName = recipientFullName;
                    eventEmail.RecipientFirstName = recipientFirstName;
                    eventEmail.RecipientLogin = recipientLogin;
                    eventEmail.RecipientEmail = recipientEmail;

                    // if we have object information, load as xml and populate for placeholder replacement
                    if (!String.IsNullOrWhiteSpace(objectInformation))
                    {
                        bool isXmlLoaded = false;
                        XmlDocument objectInformationXml = new XmlDocument();

                        try
                        {
                            objectInformationXml.LoadXml(objectInformation);
                            isXmlLoaded = true;
                        }
                        catch // do nothing with any caught exception, just move along
                        { }

                        // fill in elements that need an object binding if we have the xml for the object information
                        if (isXmlLoaded)
                        {
                            // object name is common to all object types
                            string objectName = null;
                            objectName = objectInformationXml.SelectSingleNode("//objectName").InnerText;

                            int courseEstimatedLengthInMinutes;
                            string courseEstimatedLength = String.Empty;

                            switch (objectType)
                            {
                                case "user":
                                    eventEmail.ObjectName = objectName;
                                    eventEmail.UserRegistrationRejectionComments = objectInformationXml.SelectSingleNode("//rejectionComments").InnerText;

                                    break;
                                case "courseenrollment":
                                    eventEmail.ObjectName = objectName;
                                    eventEmail.EnrollmentCourseDescription = objectInformationXml.SelectSingleNode("//enrollmentDescription").InnerText;

                                    // enrollment due date
                                    DateTime dtDue;

                                    if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentDueDate").InnerText))
                                    {
                                        if (DateTime.TryParse(objectInformationXml.SelectSingleNode("//enrollmentDueDate").InnerText, out dtDue))
                                        { eventEmail.EnrollmentDueDate = TimeZoneInfo.ConvertTimeFromUtc(dtDue, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern); }
                                    }

                                    // enrollment expire date
                                    DateTime dtExpired;

                                    if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentExpireDate").InnerText))
                                    {
                                        if (DateTime.TryParse(objectInformationXml.SelectSingleNode("//enrollmentExpireDate").InnerText, out dtExpired))
                                        { eventEmail.EnrollmentExpireDate = TimeZoneInfo.ConvertTimeFromUtc(dtExpired, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern); }
                                    }

                                    // course estimated length in minutes                                    
                                    if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentEstCompletionTime").InnerText))
                                    {
                                        if (int.TryParse(objectInformationXml.SelectSingleNode("//enrollmentEstCompletionTime").InnerText, out courseEstimatedLengthInMinutes))
                                        {
                                            if (courseEstimatedLengthInMinutes < 60)
                                            { courseEstimatedLength = String.Format(_GlobalResources.XMinute_s, courseEstimatedLengthInMinutes.ToString()); }
                                            else
                                            {
                                                int estimatedLengthHours = courseEstimatedLengthInMinutes / 60;
                                                int estimatedLengthMinutes = courseEstimatedLengthInMinutes - (estimatedLengthHours * 60);

                                                if (estimatedLengthMinutes > 0)
                                                { courseEstimatedLength = String.Format(_GlobalResources.XHour_sAndXMinute_s, estimatedLengthHours, estimatedLengthMinutes); }
                                                else
                                                { courseEstimatedLength = String.Format(_GlobalResources.XHour_s, estimatedLengthHours); }
                                            }

                                            eventEmail.EnrollmentCourseEstCompletionTime = courseEstimatedLength;
                                        }
                                    }

                                    break;
                                case "courseenrollmentrequest":
                                    eventEmail.ObjectName = objectName;
                                    eventEmail.EnrollmentCourseDescription = objectInformationXml.SelectSingleNode("//enrollmentDescription").InnerText;
                                    eventEmail.EnrollmentRequestRejectionComments = objectInformationXml.SelectSingleNode("//rejectionComments").InnerText;

                                    // course estimated length in minutes                                    
                                    if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentEstCompletionTime").InnerText))
                                    {
                                        if (int.TryParse(objectInformationXml.SelectSingleNode("//enrollmentEstCompletionTime").InnerText, out courseEstimatedLengthInMinutes))
                                        {
                                            if (courseEstimatedLengthInMinutes < 60)
                                            { courseEstimatedLength = String.Format(_GlobalResources.XMinute_s, courseEstimatedLengthInMinutes.ToString()); }
                                            else
                                            {
                                                int estimatedLengthHours = courseEstimatedLengthInMinutes / 60;
                                                int estimatedLengthMinutes = courseEstimatedLengthInMinutes - (estimatedLengthHours * 60);

                                                if (estimatedLengthMinutes > 0)
                                                { courseEstimatedLength = String.Format(_GlobalResources.XHour_sAndXMinute_s, estimatedLengthHours, estimatedLengthMinutes); }
                                                else
                                                { courseEstimatedLength = String.Format(_GlobalResources.XHour_s, estimatedLengthHours); }
                                            }

                                            eventEmail.EnrollmentCourseEstCompletionTime = courseEstimatedLength;
                                        }
                                    }

                                    break;
                                case "lesson":
                                    eventEmail.ObjectName = objectName;

                                    break;
                                case "standuptraininginstance":
                                    eventEmail.ObjectName = objectName;

                                    // session start date
                                    DateTime dtSessionStart;

                                    if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//sessionDateTime").InnerText))
                                    {
                                        if (DateTime.TryParse(objectInformationXml.SelectSingleNode("//sessionDateTime").InnerText, out dtSessionStart))
                                        {
                                            if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//sessionTimezoneDotNetName").InnerText))
                                            {
                                                string sessionTimeZoneDotNetName = objectInformationXml.SelectSingleNode("//sessionTimezoneDotNetName").InnerText;
                                                eventEmail.StandupTrainingSessionMeetingTime = TimeZoneInfo.ConvertTimeFromUtc(dtSessionStart, TimeZoneInfo.FindSystemTimeZoneById(sessionTimeZoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern);
                                                eventEmail.StandupTrainingSessionMeetingTime += " @ " + TimeZoneInfo.ConvertTimeFromUtc(dtSessionStart, TimeZoneInfo.FindSystemTimeZoneById(sessionTimeZoneDotNetName)).ToString(culture.DateTimeFormat.ShortTimePattern);
                                            }
                                        }
                                    }

                                    // session timezone
                                    eventEmail.StandupTrainingSessionTimezone = objectInformationXml.SelectSingleNode("//sessionTimezoneFriendlyName").InnerText;

                                    // session location
                                    eventEmail.StandupTrainingSessionLocation = objectInformationXml.SelectSingleNode("//sessionLocation").InnerText;

                                    // session location description
                                    eventEmail.StandupTrainingSessionLocationDescription = objectInformationXml.SelectSingleNode("//sessionLocationDescription").InnerText;

                                    // session instructor fullname
                                    eventEmail.StandupTrainingSessionInstructorFullName = objectInformationXml.SelectSingleNode("//sessionInstructorFullName").InnerText;

                                    // session drop link
                                    // if the user is allowed to drop the ilt, populate the stand up training session drop link                                    
                                    bool isRestrictedDrop = Convert.ToBoolean(objectInformationXml.SelectSingleNode("//sessionIsRestrictedDrop").InnerText);
                                    if (!isRestrictedDrop)
                                    {
                                        AsentiaAESEncryption dropSessionCryptoURL = new AsentiaAESEncryption();
                                        string encryptedDropSessionURL = WebUtility.UrlEncode(dropSessionCryptoURL.Encrypt(idObjectRelated.ToString() + "|" + idRecipient.ToString() + "|" + idSite.ToString()));

                                        eventEmail.StandupTrainingSessionDropLink = "http://" + hostname + "." + Config.AccountSettings.BaseDomain + "/_util/DropSession/?token=" + encryptedDropSessionURL;
                                    }
                                    else
                                    {
                                        eventEmail.StandupTrainingSessionDropLink = "";
                                    }

                                    // if there is a calendar file attachment, attach it
                                    if (attachmentType == 0) // 0 is a calendar attachment
                                    {
                                        string meetingTimes = "<meetingTimes>" + objectInformationXml.SelectSingleNode("//sessionDateTimes").InnerText + "</meetingTimes>";
                                        string attachFilePath;

                                        XmlDocument meetingTimesXML = new XmlDocument();
                                        meetingTimesXML.LoadXml(meetingTimes);

                                        XmlNodeList sessionDateTimesList = meetingTimesXML.SelectNodes("/meetingTimes/meetingTime");
                                        DateTime dtStart;
                                        DateTime dtEnd; 
                                        int i = 1;

                                        foreach (XmlNode meetingTime in sessionDateTimesList)
                                        {

                                            DateTime.TryParse(meetingTime["dtStart"].InnerText, out dtStart);
                                            string dtStartString = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString("yyyyMMdd'T'HHmmss");

                                            DateTime.TryParse(meetingTime["dtEnd"].InnerText, out dtEnd);
                                            string dtEndString = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString("yyyyMMdd'T'HHmmss");

                                            //string fileName = Cryptography.GetHash("SessionCalendar_" + idSite.ToString() + "_" + idObjectRelated.ToString() + "_" + i.ToString() + "_" + DateTime.UtcNow.ToString("o"), Cryptography.HashType.SHA1) + ".ics";
                                            string fileName = Cryptography.GetHash("SessionCalendar_" + hostname + "_" + idSite.ToString() + "_" + idObjectRelated.ToString() + "_" + i.ToString(), Cryptography.HashType.SHA1) + ".ics";
                                            calendarString = Utility.GetICalendarFormatedString(dtStartString, dtEndString, eventEmail.ObjectName, eventEmail.StandupTrainingSessionLocation, eventEmail.StandupTrainingSessionLocationDescription);
                                            attachFilePath = ConfigurationManager.AppSettings["AsentiaRootFolder"] + SitePathConstants.DOWNLOAD + "\\" + fileName;

                                            // split the ICalendarContent to remove the new line characters
                                            string[] splitString = calendarString.Split(new string[] { @"\n" }, StringSplitOptions.RemoveEmptyEntries);

                                            // append each line separately to the calendar file
                                            StringBuilder sb = new StringBuilder();

                                            foreach (string s in splitString)
                                            { sb.AppendLine(s); }

                                            if (!File.Exists(attachFilePath))
                                            { File.WriteAllText(attachFilePath, sb.ToString()); }

                                            Attachment att = new Attachment(attachFilePath);
                                            eventEmail.Attachments.Add(att);
                                            i++;
                                        }                                        
                                    }

                                    break;
                                case "learningpathenrollment":
                                    eventEmail.ObjectName = objectName;
                                    eventEmail.EnrollmentLearningPathDescription = objectInformationXml.SelectSingleNode("//enrollmentDescription").InnerText;

                                    // learning path enrollment due date
                                    DateTime dtDueLPE;

                                    if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentDueDate").InnerText))
                                    {
                                        if (DateTime.TryParse(objectInformationXml.SelectSingleNode("//enrollmentDueDate").InnerText, out dtDueLPE))
                                        { eventEmail.EnrollmentDueDate = TimeZoneInfo.ConvertTimeFromUtc(dtDueLPE, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern); }
                                    }

                                    // learning path enrollment expire date
                                    DateTime dtExpiredLPE;

                                    if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentExpireDate").InnerText))
                                    {
                                        if (DateTime.TryParse(objectInformationXml.SelectSingleNode("//enrollmentExpireDate").InnerText, out dtExpiredLPE))
                                        { eventEmail.EnrollmentExpireDate = TimeZoneInfo.ConvertTimeFromUtc(dtExpiredLPE, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern); }
                                    }

                                    break;
                                case "certificate":
                                    eventEmail.ObjectName = objectName;

                                    break;
                                case "certification":
                                    eventEmail.ObjectName = objectName;
                                    eventEmail.CertificationDescription = objectInformationXml.SelectSingleNode("//certificationDescription").InnerText;

                                    break;
                                case "coursediscussionmessage":
                                case "groupdiscussionmessage":
                                    eventEmail.ObjectName = objectName;
                                    eventEmail.DiscussionMessage = objectInformationXml.SelectSingleNode("//discussionMessage").InnerText;

                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    try
                    {
                        // send the email
                        eventEmail.Send();

                        // update the sent status
                        EventEmailQueue.UpdateStatus(1, "en-US", 1, idEventEmailQueue, false, null, accountWebConfigPath);
                    }
                    // catch any exception and mark the status as unsent
                    catch (EmailTemplateException etex)
                    { EventEmailQueue.UpdateStatus(1, "en-US", 1, idEventEmailQueue, true, etex.Message, accountWebConfigPath); }
                    catch (AsentiaException aex)
                    { EventEmailQueue.UpdateStatus(1, "en-US", 1, idEventEmailQueue, true, aex.Message, accountWebConfigPath); }
                    catch (Exception ex)
                    { EventEmailQueue.UpdateStatus(1, "en-US", 1, idEventEmailQueue, true, ex.Message, accountWebConfigPath); }
                }
            }
            catch
            { throw; }
        }
        #endregion

        #region ProcessReportSubscriptionQueue
        /// <summary>
        /// Processes the report subscription queue for the system.
        /// </summary>
        /// <param name="accountWebConfigPath">path to the account database's web.config</param>
        public void ProcessReportSubscriptionQueue(string accountWebConfigPath)
        {
            try
            {
                DataTable currentReportSubscriptionQueue = ReportSubscriptionQueue.GetCurrentItems(1, "en-US", 1, null, 5, accountWebConfigPath);

                foreach (DataRow queueItem in currentReportSubscriptionQueue.Rows)
                {
                    // get the data from the row
                    int idReportSubscriptionQueue = Convert.ToInt32(queueItem["idReportSubscriptionQueue"]);
                    int idSite = Convert.ToInt32(queueItem["idSite"]);
                    string hostname = queueItem["hostname"].ToString();
                    string siteDefaultLanguageString = queueItem["siteDefaultLanguageString"].ToString();
                    int idReportSubscription = Convert.ToInt32(queueItem["idReportSubscription"]);
                    int idReport = Convert.ToInt32(queueItem["idReport"]);
                    int idDataset = Convert.ToInt32(queueItem["idDataset"]);
                    string fields = queueItem["fields"].ToString();
                    string filter = queueItem["filter"].ToString();
                    string order = queueItem["order"].ToString();
                    string reportName = queueItem["reportName"].ToString();
                    int idRecipient = Convert.ToInt32(queueItem["idRecipient"]);
                    string recipientSubscriptionToken = queueItem["recipientSubscriptionToken"].ToString();
                    string recipientLangString = queueItem["recipientLangString"].ToString();
                    string recipientFullName = queueItem["recipientFullName"].ToString();
                    string recipientFirstName = queueItem["recipientFirstName"].ToString();
                    string recipientLogin = queueItem["recipientLogin"].ToString();
                    string recipientEmail = queueItem["recipientEmail"].ToString();
                    int recipientTimezone = Convert.ToInt32(queueItem["recipientTimezone"]);
                    string recipientTimezoneDotNetName = queueItem["recipientTimezoneDotNetName"].ToString();
                    string systemFromAddressOverride = queueItem["systemFromAddressOverride"].ToString();
                    string systemSmtpServerNameOverride = queueItem["systemSmtpServerNameOverride"].ToString();
                    string systemSmtpServerPortOverrideStr = queueItem["systemSmtpServerPortOverride"].ToString();
                    string systemSmtpServerUseSslOverrideStr = queueItem["systemSmtpServerUseSslOverride"].ToString();
                    string systemSmtpServerUsernameOverride = queueItem["systemSmtpServerUsernameOverride"].ToString();
                    string systemSmtpServerPasswordOverride = queueItem["systemSmtpServerPasswordOverride"].ToString();
                    int? priority = null;
                    DateTime dtAction = Convert.ToDateTime(queueItem["dtAction"]);

                    if (!String.IsNullOrWhiteSpace(queueItem["priority"].ToString()))
                    { priority = Convert.ToInt32(queueItem["priority"]); }

                    int systemSmtpServerPortOverride = 0;
                    
                    if (!String.IsNullOrWhiteSpace(systemSmtpServerPortOverrideStr))
                    { Int32.TryParse(systemSmtpServerPortOverrideStr, out systemSmtpServerPortOverride); }

                    bool systemSmtpServerUseSslOverride = false;
                    
                    if (!String.IsNullOrWhiteSpace(systemSmtpServerUseSslOverrideStr))
                    { Boolean.TryParse(systemSmtpServerUseSslOverrideStr, out systemSmtpServerUseSslOverride); }

                    // instansiate an ReportSubscriptionEmail
                    ReportSubscriptionEmail reportSubscriptionEmail = new ReportSubscriptionEmail(priority, recipientLangString, systemFromAddressOverride, systemSmtpServerNameOverride, systemSmtpServerPortOverride, systemSmtpServerUseSslOverride, systemSmtpServerUsernameOverride, systemSmtpServerPasswordOverride, ConfigurationManager.AppSettings["AsentiaRootFolder"]);

                    // fill in the common elements that do not need an object binding
                    CultureInfo culture = new CultureInfo(recipientLangString);
                    Config.WebConfigFilePath = ConfigurationManager.AppSettings["AsentiaRootFolder"];

                    reportSubscriptionEmail.LmsUrl = "http://" + hostname + "." + Config.AccountSettings.BaseDomain;
                    reportSubscriptionEmail.LmsHostname = hostname;

                    reportSubscriptionEmail.EventDate = TimeZoneInfo.ConvertTimeFromUtc(dtAction, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern);
                    reportSubscriptionEmail.EventTime = TimeZoneInfo.ConvertTimeFromUtc(dtAction, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortTimePattern);

                    reportSubscriptionEmail.ObjectName = reportName;
                    reportSubscriptionEmail.RecipientId = idRecipient.ToString();
                    reportSubscriptionEmail.RecipientSubscriptionToken = recipientSubscriptionToken;

                    reportSubscriptionEmail.RecipientFullName = recipientFullName;
                    reportSubscriptionEmail.RecipientFirstName = recipientFirstName;
                    reportSubscriptionEmail.RecipientLogin = recipientLogin;
                    reportSubscriptionEmail.RecipientEmail = recipientEmail;

                    try
                    {
                        // build paths to the user account data file(s)
                        string defaultUserAccountDataPath = ConfigurationManager.AppSettings["AsentiaRootFolder"] + "_config\\default\\UserAccountData.xml";
                        string siteUserAccountDataPath = ConfigurationManager.AppSettings["AsentiaRootFolder"] + "_config\\" + hostname + "\\UserAccountData.xml";

                        // execute the report
                        DataTable reportDt = LRS.Library.Report.Run(idSite, recipientLangString, idRecipient, fields, filter, order, 0, idDataset, accountWebConfigPath, recipientTimezoneDotNetName, defaultUserAccountDataPath, siteUserAccountDataPath, siteDefaultLanguageString);

                        // remove columns that should not appear in the report file                        
                        if (reportDt.Columns.Contains("Data Lesson ID"))
                        { reportDt.Columns.Remove("Data Lesson ID"); }

                        if (reportDt.Columns.Contains("SCO Identifier"))
                        { reportDt.Columns.Remove("SCO Identifier"); }

                        // save report data to csv file
                        
                        // get the path of the file we're saving to
                        string fullFilePath = ConfigurationManager.AppSettings["AsentiaRootFolder"] + "warehouse\\" + hostname + "\\reportdata\\" + this._GenerateReportFileName(reportName) + ".csv";

                        // write the file
                        using (StreamWriter writer = new StreamWriter(fullFilePath, false, Encoding.UTF8))
                        {
                            List<string> headerValues = new List<string>();

                            // header
                            foreach (DataColumn column in reportDt.Columns)
                            { headerValues.Add(column.ColumnName); }

                            writer.WriteLine(String.Join(",", headerValues.ToArray()));

                            // rows
                            string[] items = null;

                            foreach (DataRow row in reportDt.Rows)
                            {
                                items = row.ItemArray.Select(o => "\"" + o.ToString().Replace("\"", "\"\"") + "\"").ToArray(); // linq statement, remove potentially
                                writer.WriteLine(String.Join(",", items));
                            }

                            // flush the writer
                            writer.Flush();
                        }

                        // attach the report file to email
                        if (File.Exists(fullFilePath))
                        {
                            Attachment reportAttachment = new Attachment(fullFilePath);
                            reportSubscriptionEmail.Attachments.Add(reportAttachment);
                        }
                        else
                        { throw new AsentiaException("Report subscription item could not be sent because the report file does not exist."); }
                        
                        // reset the web.config path because for some reason, something in the above code loses the path
                        Config.WebConfigFilePath = ConfigurationManager.AppSettings["AsentiaRootFolder"];

                        // send the email
                        reportSubscriptionEmail.Send();

                        // update the sent status
                        ReportSubscriptionQueue.UpdateStatus(1, "en-US", 1, idReportSubscriptionQueue, false, null, accountWebConfigPath);
                    }
                    // catch any exception and mark the status as unsent
                    catch (EmailTemplateException etex)
                    { ReportSubscriptionQueue.UpdateStatus(1, "en-US", 1, idReportSubscriptionQueue, true, etex.Message + " | " + etex.StackTrace, accountWebConfigPath); }
                    catch (AsentiaException aex)
                    { ReportSubscriptionQueue.UpdateStatus(1, "en-US", 1, idReportSubscriptionQueue, true, aex.Message + " | " + aex.StackTrace, accountWebConfigPath); }
                    catch (Exception ex)
                    { ReportSubscriptionQueue.UpdateStatus(1, "en-US", 1, idReportSubscriptionQueue, true, ex.Message + " | " + ex.StackTrace, accountWebConfigPath); }
                }
            }
            catch
            { throw; }
        }
        #endregion

        #region _GenerateReportFileName
        /// <summary>
        /// Generates a filename for the report file using the report title and current UTC timestamp.
        /// </summary>
        /// <param name="reportName">name of the report</param>
        /// <returns>filename</returns>
        private string _GenerateReportFileName(string reportName)
        {
            // clear the report name of any special characters that cannot be used in a filename
            StringBuilder sb = new StringBuilder();

            foreach (char c in reportName)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                { sb.Append(c); }
            }

            reportName = sb.ToString();

            return reportName + DateTime.UtcNow.ToString("_yyyy-MM-dd-HH-mm-ss");
        }
        #endregion
    }
}
