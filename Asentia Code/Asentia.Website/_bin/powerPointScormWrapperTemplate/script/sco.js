// Controls the SCORM functionality of the Asentia Image Slideshow wrapper SCO
function buildSuspendData()
{
	//builds and returns the suspend data string
	var suspendDataStr = "<sd>";

	//store the image index
	suspendDataStr += "<imgidx>";
	suspendDataStr += String(getCurrentImageIndex());
	suspendDataStr += "</imgidx>";
	
	//store the progress
	suspendDataStr += "<progress>";
	suspendDataStr += getProgressArrString();
	suspendDataStr += "</progress>";
	
	//store the time elapsed for the current slide
	suspendDataStr += "<tmcount>";
	suspendDataStr += String(getCurrentTimeCount());
	suspendDataStr += "</tmcount>";
	
	//close the suspend data tag
	suspendDataStr += "</sd>";
	
	return suspendDataStr;
}

function getLMSSuspendData()
{	
	if (slideshow.testMode)
		return "<sd><imgidx>2</imgidx><progress>1,1,1,0,0</progress><tmcount>0</tmcount></sd>";
	else
		return doLMSGetValue("cmi.suspend_data");
}

function setLMSSuspendData()
{	
	//send the suspend data to the LMS
	var currSuspendData = buildSuspendData();
	
	doLMSSetValue("cmi.suspend_data", currSuspendData);
}

function resumeLesson()
{
	//resumes the progress if there is suspend data available
	var currSuspendData = getLMSSuspendData();

	//only resume if bookmarking is enabled
	if (currSuspendData && useBookmarking)
	{
		//set that we are resuming
		slideshow.isResuming = true;
		
		//restore the slide index they were on
		slideshow.currentImageIndex = getImageIndexFromSuspendData(currSuspendData);
		
		//restore the progress array
		slideshow.progressArr = getProgressArrFromSuspendData(currSuspendData);
		
		//restore the slide timer count
		slideshow.timeCount = getTimeCountFromSuspendData(currSuspendData);
		
		//restore the highest progress measure reached
		restoreHighestProgressMeasureReachedOnResume();
		
	}
}

function restoreHighestProgressMeasureReachedOnResume()
{
	//restore the highestProgressMeasureReached from LMS progress_measure
	var restoredProgressMeasure = getProgressMeasureFromLMS();
	if (restoredProgressMeasure)
	{
		slideshow.highestProgressMeasureReached = Number(restoredProgressMeasure);
	}
}

function getImageIndexFromSuspendData(pSuspendData)
{
	//extract out the image index from suspend data string
	return Number($(pSuspendData).find("imgidx").html());
	
}

function getProgressArrFromSuspendData(pSuspendData)
{
	//extract out the progress string and return it as an array
	var progressStr = $(pSuspendData).find("progress").html();
	
	return progressStr.split(",");
}

function getTimeCountFromSuspendData(pSuspendData)
{
	//extract out the time elapsed on slide
	return Number($(pSuspendData).find("tmcount").html());
}

function setProgressMeasure()
{
	//set SCORM value to current user progress (0-1)
	var currProgress = String(getProgress());
	
	//DO NOT set the progress measure if the value in the LMS is already greater than this current number; avoids 'backward progress'
	var LMSProgressMeasure = doLMSGetValue("cmi.progress_measure");
	if (LMSProgressMeasure)
	{
		//be sure number isn't longer than 7 significant digits (required by SCORM for progress_measure)
		var currProgressNum = checkProgressMeasureLength(Number(currProgress));
		
		//compare the two values and record the new value only if it's greater than current SCORM progress measure valure
		if (currProgressNum > Number(LMSProgressMeasure))
		{	
			doLMSSetValue("cmi.progress_measure", String(currProgressNum));
		}
			
	}
	else
	{
		doLMSSetValue("cmi.progress_measure", String(currProgress));

	}
}

function getProgressMeasureFromLMS()
{
	return doLMSGetValue("cmi.progress_measure");
}

function setInitialCompletionStatus()
{
	if (!API) return "";
	if (blnFinishCalled) return "";
	
	//set 'incomplete' or 'completed' based on progress and if already set to completed (can't go from 'completed' to 'incomplete')
	var currentCompletionStatus = doLMSGetValue("cmi.completion_status");
	
	//if it's already completed, do nothing
	if (currentCompletionStatus == "completed")
		return;
	
	//if it's unknown, set to incomplete
	if (currentCompletionStatus == "unknown")
		doLMSSetValue("cmi.completion_status","incomplete");
}

function checkProgressMeasureLength(pMeasure)
{
	//SCORM value only accepts up to 7 significant digits; concatenate if there are more than that
	var pMeasureStr = String(pMeasure);
	
	if (pMeasureStr.length > 9)
	{
		pMeasureStr = pMeasureStr.substr(0,9);
		
		//return the new concatenated value as a number
		return Number(pMeasureStr);
	}
	
	//else return the original value
	return pMeasure;
}

function updateSCORMData()
{
	//send all necessary SCORM data to the LMS
	
	//update suspend data
	setLMSSuspendData();
	
	//set how far thru the slideshow they are
	setProgressMeasure();
	
}

function determineLessonCompletion()
{
	//compare internally stored progess_measure against completionThreshold to determine if lesson should be marked as completed
	if (slideshow.highestProgressMeasureReached >= slideshow.completionThreshold)
		setLessonCompleted();
}

function setLessonCompleted()
{
	if (!API) return "";
	if (blnFinishCalled) return "";
	
	//set completion_status to 'completed'
	var currentCompletionStatus = doLMSGetValue("cmi.completion_status");
	
	if (currentCompletionStatus != "completed")
		doLMSSetValue("cmi.completion_status","completed");
}

function onUserExit()
{
	//if they close the window, save their progress
	
	//if this has already been called, don't call it again
	if (slideshow.calledUserExit)
		return;
	
	//store the data to the LMS
	updateSCORMData();

	//always suspend the session so the data is restored next time
	doLMSSetValue("cmi.exit", "suspend");
	
	//set flag so this doesn't get double-called
	slideshow.calledUserExit = true;
}

