var SUPRESS_ALERTS = true;

var API = getAPI();

var lastSetValues = [];

var changeMadeSinceLastCommit = false;

var blnFinishCalled = false;


//automatically call initialize
doLMSInitialize();


function getAPI() {
	var theAPI = findAPI(window);
	if ((theAPI == null) && (window.opener != null) && (typeof(window.opener) != "undefined")) {
		theAPI = findAPI(window.opener);
	}
	if (theAPI == null) {
		if (!SUPRESS_ALERTS) {
			alert("LMS not found. Running in offline mode.");
		}
		
		console.log("LMS not found. Running in offline mode."); 
	}
	return theAPI;
}

function findAPI(win) {
	findAPITries = 0;
	while ((win.API_1484_11 == null) && (win.parent != null) && (win.parent != win)){
		findAPITries++;

		if (findAPITries > 500) {
			if (!SUPRESS_ALERTS) 
				alert("Error finding API -- too deeply nested.");
			return null;
		}
			win = win.parent;
	}

	return win.API_1484_11;
}

function doLMSInitialize() {
	if (!API) return "";

	value = API.Initialize("");
	
	if (doLMSGetLastError() != "0") {
		if (!SUPRESS_ALERTS) {
			alert(doLMSGetErrorString(doLMSGetLastError()));
		}
	}
	return value;
}

function doLMSTerminate() {
	if (!API) return "";

	if (blnFinishCalled) return "";

	//always suspend the session so the data is restored next time
	doLMSSetValue("cmi.exit", "suspend");
	//if we need to have the next sessions start over, set exit to 'normal'
	
	//Instruct the Rustici LMS to close the window on terminate
    //doLMSSetValue("adl.nav.request", "exitAll");

	value = API.Terminate("");
	if (doLMSGetLastError() != "0") {
		if (!SUPRESS_ALERTS) {
			alert(doLMSGetErrorString(doLMSGetLastError()));
		}
	}

	blnFinishCalled = true;
	
	return value;
}

function doLMSGetValue(name) {
	if (!API) return "";

	if (blnFinishCalled) return "";

	var value = API.GetValue(name);

	if (hasChanged(name, value))
		reportChange(name, value);
	
	return value;
}

function doLMSSetValue(name, value) {
	
	//alert(name + ": " + value);
	
	if (!API) return "";

	if (blnFinishCalled) return "true";

	value = String(value);

	//set the value only if it's changed since the last time it was set (or if it hasn't yet been set)
	if (hasChanged(name, value))
	{
		reportChange(name, value);

		var val = API.SetValue(name, value);
		
		return val;

	}
	else
		return "true";
}

	function hasChanged(item, value) {
		if (!lastSetValues[item]) {
			return true;
		} else if (lastSetValues[item] != value) {
			return true;
		}
		return false;
	}
	function reportChange(item, value) {
		lastSetValues[item] = value;
		changeMadeSinceLastCommit = true;
	}

function doLMSCommit() {
	if (!API) return "";

	if (blnFinishCalled) return "true";
	
	if (changeMadeSinceLastCommit)
	{
		changeMadeSinceLastCommit = false;

		return API.Commit("");
	}
	else
		return "true";
}

function doLMSGetLastError() {
	if (!API) return "";

	if (blnFinishCalled) return "";

	return API.GetLastError();
}

function doLMSGetErrorString(errorCode) {
	if (!API) return "";

	if (blnFinishCalled) return "";

	return API.GetErrorString(errorCode);
}

function doLMSGetDiagnostic() {
	if (!API) return "";

	if (blnFinishCalled) return "";

	return API.GetDiagnostic("");
}