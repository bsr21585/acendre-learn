//Controller for the Image Slideshow
// -- Authored by Jevon Pack: 6/21/2016 --

var slideshow = new Object();


$( document ).ready(function() {
	//currently used to initiate the slideshow; in production, this should be replaced with a direct call to initSlideshow()
	initSlideshow(imageArray);
});

function initSlideshow(fileArray)
{
	var ss = slideshow;
	
	//fileArray will be an array of image filename strings
	ss.testMode = false;
	ss.fileArray = fileArray;
	ss.autoplay = autoplay;
	ss.useBookmarking = useBookmarking;
	ss.imagePath = imagePath;
	ss.skinPath = "skins/" + skinFolder + "/";
	ss.currentImageIndex = 0;
	ss.timeCount = 0;
	ss.timeIntervalId;
	ss.slideDuration = slideDuration;
	ss.playState = ss.autoplay ? "playing" : "paused"; //will be "playing" or "paused"
	ss.progressArr = new Array();
	ss.allowForward  = allowForward;
	ss.allowBackward  = allowBackward;
	ss.allowNavigation = allowNavigation;
	ss.calledUserExit = false;
	ss.isResuming = false;
	ss.menuOpen = false;
	ss.completionThreshold = completionThreshold;
	ss.highestProgressMeasureReached = 0;
	
	//add navigation buttons to the footer
	addNavButtons();
	
	//fill the array that will track user progress (which slides they have viewed)
	initProgressArr();
	
	//listen for image load
	addImageLoadListener();
	
	//listen for errors loading images
	addImageErrorListener();
	
	//set 'unknown' status to 'incomplete'
	setInitialCompletionStatus();
	
	//resume: bring back SCORM data if continuing a previous session
	resumeLesson();
	
	//update the current slide
	updateSlide();
	
	//update numeric progress counter
	updateProgressCounter();
	
	//update progress
	updateProgressArr();
	
	//update graphic fill bar
	updateProgressBarFill();
	
	//update numerical time above progress bar
	updateProgressBarTimer();
	
	//set up the menu items
	initFloatingMenu();
	
	//update the floating menu
	updateFloatingMenu();
	
	//make any needed translations for the menu button
	updateMenuButtonTranslation();
	
	//update value showing the furthest user has reached in this lesson
	updateProgressMeasureReached();
	
	//do internal check of progress measure to check if lesson should be set as completed
	determineLessonCompletion();
	
	//save data when they exit the lesson (for desktop browsers)
	window.onbeforeunload = function(e) {
		onUserExit();
	};
	
	//save data when they exit the lesson (for mobile browsers)
	window.addEventListener("pagehide", function(evt){
            onUserExit();
        }, false);
}

function initProgressArr()
{
	//0 = not visited; 1 = visited
	var ss = slideshow;
	
	for (var i = 0; i < ss.fileArray.length; i++)
	{
		ss.progressArr[i] = 0;
	}
}

function updateSlide()
{
	//grab the image that should be displayed and load it
	var newImage = getImageFilenameByIndex(getCurrentImageIndex());

	updateImage(newImage);
}

function updateImage(imgFilename)
{
	//change the src of the on-screen image
	var newPath = slideshow.imagePath + imgFilename;

	$("#slideshowImage").attr("src", newPath);
}

function getImageFilenameByIndex(indexNum)
{
	//return the image filename by index given
	return slideshow.fileArray[indexNum];
}

function getCurrentImageIndex()
{
	return slideshow.currentImageIndex;
}

function addImageLoadListener()
{
	//add load listener
	$("#slideshowImage").bind("load", onImageLoad);
}

function addImageErrorListener()
{
	//add load listener
	$("#slideshowImage").bind("error", onImageError);
}

function onImageLoad()
{
	//start the slide timer after image has loaded
	startSlideTimer();

	//update the height of the sidemenu to match the height of the image
	//updateMenuHeight();
	
	//force the full table width to that of the main window contents to eliminate cross-browser issues
	updateTableWidth();
}

function onImageError()
{
	//hide the image tag
	showImageTag(false);
	
	//show the error box
	showErrorMsg(true);
}

function updateMenuHeight()
{
	//update the height of the sidemenu to match the height of the image
	
	//don't do anything if nonexistent or not visible
	if ($("#sideMenuContainer").length == 0 || $("#sideMenuContainer").css("display") == "none")
		return;
	
	var imgHeightStr = String($("#slideshowImage").height());
	
	if (imgHeightStr)
	{
		//append the units
		imgHeightStr += "px";
		
		$("#sideMenuContainer").css("height", imgHeightStr);
	}
}

function updateTableWidth()
{
	var imgWidthStr = String($("#slideshowImage").width());
	
	if (imgWidthStr)
	{
		//append the units
		imgWidthStr += "px";
		
		$("#slideshowTable").css("width", imgWidthStr);
	}
}

function startSlideTimer()
{
	//start the counter for how long this slide will display
	var ss = slideshow;
	
	//reset the time; if RESUMING SCORM data, keep the current time and reset the isResuming flag
	if (!ss.isResuming)
		resetSlideTimer();
	else
	{
		ss.isResuming = false;
	}
	
	//if the show is currently paused, DON'T start the timer
	if (ss.playState == "paused")
		return;
	
	//start the timer
	ss.timeIntervalId = setInterval(updateSlideTimer, 1000);
}

function updateSlideTimer()
{
	var ss = slideshow;
	
	//increment the time count
	incTimer();
	
	//update numerical time above progress bar
	updateProgressBarTimer();
	updateProgressBarFill();
	
	//check if count has met duration requirement
	if (ss.timeCount >= ss.slideDuration)
	{
		//reset timer so timeCount is already at 0 when new slide loads
		//endSlideTimer();
		resetSlideTimer();
		
		//move to next slide
		navigate(1);
	}
}

function pauseSlideTimer()
{
	//clear the timer interval but do not reset the timeCount
	try
	{
		clearInterval(slideshow.timeIntervalId);
	}
	catch(e){};
	
	slideshow.playState = "paused";
}

function resumeSlideTimer()
{
	//resumes the timer from a PAUSED state; carries on from the timeCount's previous spot
	slideshow.timeIntervalId = setInterval(updateSlideTimer, 1000);
	
	slideshow.playState = "playing";
}

function endSlideTimer()
{
	var ss = slideshow;
	
	//clear any current interval
	try
	{
		clearInterval(ss.timeIntervalId);
	}
	catch(e){};
}

function incTimer()
{
	//increment the time count
	slideshow.timeCount++;
	
	//console.log(slideshow.timeCount);
}

function resetSlideTimer()
{
	var ss = slideshow;
	
	//set the counter to 0
	ss.timeCount = 0;
	
	//clear any current interval
	try
	{
		clearInterval(ss.timeIntervalId);
	}
	catch(e){};
}

function getCurrentTimeCount()
{
	return slideshow.timeCount;	
}

function initFloatingMenu()
{
	//create the menu based on slides
	var ss = slideshow;
	var menuContents = "";
	
	//add a menu item for each page in the presentation
	for (var i = 0; i < ss.progressArr.length; i++)
	{
		var thisSlideNumStr = String(i+1);
		menuContents += "<div class='floatingMenuItem' id='floatingMenuItemSlide" + thisSlideNumStr + "' onclick='navigateTo(" + i + ")'>" + TRANS_SLIDE + " " + thisSlideNumStr + "</div>";
	}
	
	//insert the items in the menu
	$("#floatingMenuInset").html(menuContents);
}

function updateFloatingMenu()
{
	//update the menu to highlight the button for the current active slide
	var ss = slideshow;
	
	//remove current highlights
	clearFloatingMenuHighlights();
	
	//highlight button for active slide
	addFloatingMenuHighlight(String(ss.currentImageIndex + 1));
	
	//lock proper menu items if 'allowNavigation' is false
	updateLockedMenuItems();
}

function updateMenuButtonTranslation()
{
	//update translation for the menu button
	$("#menuButton").attr("value", TRANS_MENU);
}

function clearFloatingMenuHighlights()
{
	//clear all highlighted buttons from floating menu
	$(".floatingMenuItem").removeClass("floatingMenuItemHighlight");
}

function addFloatingMenuHighlight(pSlideNumStr)
{
	//highlight the given slide number button
	$("#floatingMenuItemSlide" + pSlideNumStr).addClass("floatingMenuItemHighlight");
}

function updateLockedMenuItems()
{
	//if 'allowNavigation' is false, don't allow user to skip ahead using the menu items
	var ss = slideshow;
	var progArr = ss.progressArr;
	
	//iterate thru each page
	for (var i = 0; i < progArr.length; i++)
	{
		var visitedStatus = progArr[i];
		
		//if this page has not been visited, lock the menu item
		if (visitedStatus == 0)
		{
			$("#floatingMenuItemSlide" + [i+1]).addClass("floatingMenuItemDisabled");
		}
		//if this page has been visited, unlock menu item
		else if (visitedStatus == 1)
		{
			$("#floatingMenuItemSlide" + [i+1]).removeClass("floatingMenuItemDisabled");
		}
	}
}

function navigate(navDirection)
{
	//accepts 1 (forward) and -1 (backward)
	var ss = slideshow;
	
	if (navDirection == 1)
	{
		//increment slide index (only if next slide exists)
		if (ss.fileArray[ss.currentImageIndex + 1])
		{
			ss.currentImageIndex++;
		
			onNavigate();
		}
		
	}
	else if (navDirection == -1)
	{
		//decrement slide index (only if previous slide exists)
		if (ss.currentImageIndex > 0)
		{
			ss.currentImageIndex--;
			
			onNavigate();
		}
	}
}

function navigateTo(pIndex)
{
	//nav to a specific index
	var ss = slideshow;
	
	//check that index is in range
	if (pIndex < 0 || pIndex > ss.progressArr.length - 1)
		return;
		
	//if 'allowNavigation' is false, menu item being clicked must be a page that was already visited; if not, return
	if (!ss.allowNavigation)
	{
		//if the page being clicked has NOT been visited yet, don't nav to it
		if (ss.progressArr[pIndex] == 0)
		{
			return;
		}
		
	}
	
	//update the slide index
	ss.currentImageIndex = pIndex;
	
	//do nav actions
	onNavigate();
	
}

function onNavigate()
{
	//functions to call when user navigates
	
	//end the current slide timer
	//endSlideTimer();
	resetSlideTimer();
	
	//make sure image is visible
	showImageTag(true);
	
	//hide err msg if visible
	showErrorMsg(false);
	
	//load the new slide
	updateSlide();
	
	//update numeric progress counter
	updateProgressCounter();
	
	//update progress
	updateProgressArr();
	
	//update progress bar
	updateProgressBarFill();
	
	//update numerical time above progress bar
	updateProgressBarTimer();
	
	//update the floating menu
	updateFloatingMenu();
	
	//update value showing the furthest user has reached in this lesson
	updateProgressMeasureReached();
	
	//do internal check of progress measure to check if lesson should be set as completed
	determineLessonCompletion();
	
	//update/save suspend data
	setLMSSuspendData();
}

function getProgress()
{
	//returns the amount completed as a value ranging from 0 to 1; i.e. .6 is %60 completed
	var ss = slideshow;
	var visitedCount = 0;
	var decPct = 0;
	
	//increment visitedCount for each page visited
	for (var i = 0; i < ss.progressArr.length; i++)
	{
		if (ss.progressArr[i] == 1)
			visitedCount++;
	}
	
	//compare pages visited to total pages
	decPct = visitedCount/ss.progressArr.length;
	
	return decPct
}

function getProgressArrString()
{
	//returns the progress array as a string, i.e.: "1,1,1,1,0,0,0"
	var ss = slideshow;
	
	return ss.progressArr.toString();
}

function updateProgressArr()
{
	//show that a slide is visited once user gets to that slide(0 = not visited; 1 = visited)
	var ss = slideshow;
	
	//if current slide is marked as unvisited, mark it as visited
	if (ss.progressArr[ss.currentImageIndex] == 0)
		ss.progressArr[ss.currentImageIndex] = 1;
}

function updateProgressCounter()
{
	//update the numeric progress display
	var progressStr = "";
	var slideNumStr = String(slideshow.currentImageIndex + 1);
	var totalSlidesStr = String(slideshow.fileArray.length);
	
	progressStr = slideNumStr + "/" + totalSlidesStr;
	
	$("#progressCounter").html(progressStr);
}

function updateProgressBarFill()
{
	//update the fill bar in the progress bar graph
	var ss = slideshow;
	
	//find how many secs have elapsed total at this point in the presentation
	var totalSecondsElapsed = ((ss.currentImageIndex) * slideDuration) + ss.timeCount;
	var totalSecondsOverall = ss.progressArr.length * slideDuration;
	
	//get completion %: current secs elapsed compared to total secs in presentation
	var completePct = (totalSecondsElapsed/totalSecondsOverall) * 100;
	var completePctStr = String(completePct) + "%";
	
	//find % based on current slide number: CAN DECIDE whether to show progress by time (above) or only by slide number (below)
	/*
	var completePct = ((ss.currentImageIndex + 1)/ss.progressArr.length) * 100;
	var completePctStr = String(completePct) + "%";
	*/
	
	$("#progressBarFill").css("width", completePctStr);
}

function updateProgressBarTimer()
{
	//update the numerical timer above the progress bar graph
	var ss = slideshow;
	var timerStr = "";
	var currTimeStr = "";
	var totalTimeStr = "";
	
	//get the current time (secs) in the presentation
	//console.log("ind: " + ss.currentImageIndex + " || " + "tc: " + ss.timeCount);
	var currTime = (ss.currentImageIndex * slideDuration) + ss.timeCount;
	currTimeStr = getTimeFormatFromSecs(currTime);
	
	//assemble the total time in the entire presentation
	var totalTime = getTotalShowSeconds();
	totalTimeStr = getTimeFormatFromSecs(totalTime);
	
	//piece the parts together
	timerStr = currTimeStr + "/" + totalTimeStr;
	
	//update the progress text field
	$("#progressBarTimer").html(timerStr);
	
}

function getTotalShowSeconds()
{
	//get the total amount of seconds that the slideshow is
	return slideshow.progressArr.length * slideDuration;
}

function getTimeFormatFromSecs(pSecs)
{
	//return string in format of MM:SS (minutes will not convert to hours i.e. microwave time, 72:32)
	var finalTimeStr = "";
	var minsStr = "";
	var secsStr = "";
	
	//first, determine the number of minutes
	var mins = Math.floor(pSecs / 60);
	
	//next detemine the number of seconds left over after the minutes are computed
	var secs = pSecs % 60;
	
	//build the string
	//if mins is a single digit number, append a 0 before it i.e. 06:44
	if (mins < 10)
		minsStr += "0";
		
	minsStr += String(mins);
	
	//add the seconds
	if (secs < 10)
		secsStr += "0";
		
	secsStr += String(secs);
	
	//final
	finalTimeStr = minsStr + ":" + secsStr;
	
	return finalTimeStr;
}

function updateProgressMeasureReached()
{
	//update internal progress measure value
	
	//get progress measure of current position (returns between 0-1)
	var progressPct = getProgress();
	
	//if current position is further than what last recorded value is, update the value
	if (progressPct > slideshow.highestProgressMeasureReached)
	{
		slideshow.highestProgressMeasureReached = progressPct;
	}
}

function togglePlayPause()
{
	//if playing, do pause; if paused, do play
	var ss = slideshow;
	
	if (ss.playState == "playing")
	{
		//trigger a pause
		doPause();
	}
	else if (ss.playState == "paused")
	{
		//trigger play
		doPlayFromPause();
	}
}

function doPause()
{	
	//pause the time counter
	pauseSlideTimer();
	
	//change play/pause icon to 'paused' graphic
	updatePlayPauseIcon("play");
}

function doPlayFromPause()
{	
	//resume the time counter
	resumeSlideTimer();
	
	//change play/pause icon to 'paused' graphic
	updatePlayPauseIcon("pause");
}

function updatePlayPauseIcon(stateStr)
{
	//accepts 'play' or 'pause' and changes the button graphic accordingly
	$("#playPauseButton").attr("src", slideshow.skinPath + stateStr + "_normal.png");
}

function menuButtonClickHandler()
{
	toggleFloatingMenu();
}

function toggleFloatingMenu()
{
	$("#floatingMenuContainer").slideToggle(500, onMenuAnimationEnd);
}

function onMenuAnimationEnd()
{
	//track whether the menu is open or not
	var ss = slideshow;
	
	if (ss.menuOpen)
		ss.menuOpen = false;
	else
		ss.menuOpen = true;
}

function showImageTag(flag)
{
	var currDisplay = $("#slideshowImage").css("display");
	
	if (flag)
	{
		//show it if hidden
		if (currDisplay == "none")
			$("#slideshowImage").css("display","block");
	}
	else
	{
		//hide it if visible
		if (currDisplay != "none")
			$("#slideshowImage").css("display","none");
	}
}

function showErrorMsg(flag)
{
	//show or hide the error msg when an image doesn't load correctly
	var currDisplay = $("#errorMsg").css("display");
	
	if (flag)
	{
		//show it if hidden
		if (currDisplay == "none")
		{
			$("#errorMsg").html(TRANS_ERROR);
			$("#errorMsg").css("display", "block");
		}
	}
	else
	{
		//hide it if visible
		if (currDisplay != "none")
			$("#errorMsg").css("display", "none");
	}
}

function addNavButtons()
{
	//add Previous, Play, and Next graphics/buttons
	
	//establish the path for the skin files
	var skinPath = slideshow.skinPath;
	
	//build code for the buttons
	var btnHtml = "";
	
	//back button
	//if allowBackward is disabled, disable the previous button
	if (slideshow.allowBackward)
		btnHtml += '<img id="backButton" class="navButton" src="' + skinPath + 'back_normal.png" onclick="navigate(-1)" alt="Back">';
	else
		btnHtml += '<img id="backButton" class="navButtonDisabled" src="' + skinPath + 'back_normal.png" alt="Back">';
	
	//play button
	btnHtml += '<img id="playPauseButton" class="navButton" src="' + skinPath + 'pause_normal.png" onclick="togglePlayPause();" alt="Play/Pause">';
	
	//next button
	//if allowForward is disabled, disable the next button
	if (slideshow.allowForward)
		btnHtml += '<img id="forwardButton" class="navButton" src="' + skinPath + 'forward_normal.png" onclick="navigate(1)" alt="Forward">';
	else
		btnHtml += '<img id="forwardButton" class="navButtonDisabled" src="' + skinPath + 'forward_normal.png" alt="Forward">';
	
	//add the code to the footer
	$("#footerSectionMiddle").html(btnHtml);
	
	//add listeners for nav buttons
	addNavButtonListeners();
}

function addNavButtonListeners()
{
	//button rollover highlights
	if (slideshow.allowBackward)
	{
		$("#backButton").bind("mouseover", backButtonMouseOver);
		$("#backButton").bind("mouseout", backButtonMouseOut);
	}
	
	if (slideshow.allowForward)
	{
		$("#forwardButton").bind("mouseover", forwardButtonMouseOver);
		$("#forwardButton").bind("mouseout", forwardButtonMouseOut);
	}
	
	$("#playPauseButton").bind("mouseover", playPauseButtonMouseOver);
	$("#playPauseButton").bind("mouseout", playPauseButtonMouseOut);
}

function backButtonMouseOver()
{
	$("#backButton").attr("src", slideshow.skinPath + "back_highlight.png");
}

function backButtonMouseOut()
{
	$("#backButton").attr("src", slideshow.skinPath + "back_normal.png");
}

function forwardButtonMouseOver()
{
	$("#forwardButton").attr("src", slideshow.skinPath + "forward_highlight.png");
}

function forwardButtonMouseOut()
{
	$("#forwardButton").attr("src", slideshow.skinPath + "forward_normal.png");
}

function playPauseButtonMouseOver()
{
	//highlight is dependent upon whether movie is playing or paused
	var ss = slideshow;
	
	//when show playing, pause button will be shown
	if (ss.playState == "playing")
		$("#playPauseButton").attr("src", slideshow.skinPath + "pause_highlight.png");
	//when show paused, play button will be shown
	else if (ss.playState == "paused")
		$("#playPauseButton").attr("src", slideshow.skinPath + "play_highlight.png");
}

function playPauseButtonMouseOut()
{
	//highlight is dependent upon whether movie is playing or paused
	var ss = slideshow;
	
	//when show playing, pause button will be shown
	if (ss.playState == "playing")
		$("#playPauseButton").attr("src", slideshow.skinPath + "pause_normal.png");
	//when show paused, play button will be shown
	else if (ss.playState == "paused")
		$("#playPauseButton").attr("src", slideshow.skinPath + "play_normal.png");
}