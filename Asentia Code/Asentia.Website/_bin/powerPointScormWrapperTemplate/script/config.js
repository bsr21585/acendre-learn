// contains all the customizable options for the Asentia SCORM Slideshow/PowerPoint Wrapper

// location to pull images from
var imagePath = "images/";

// array of image filenames to use
var imageArray = [##slides##];

// duration of each slide in seconds before it will move to next slide
var slideDuration = 10;

// skins folder where button images will be pulled; Note: Omit any slashes. Will always pull from skins/[skinFolder]/
var skinFolder = "default";

// whether the slideshow will play automatically or not
var autoplay = ##enableAutoplay##;

// whether the slideshow will resume in LMS environment
var useBookmarking = ##allowResume##;

// allow or disallow use of the Next button to skip ahead to the next slide
var allowForward = ##allowFastForward##;

// allow or disallow use of the Previous button to move to the previous slide
var allowBackward = ##allowRewind##;

// allow or disallow the user to skip ahead using the floating menu; if 'false', user can only click on slides they have already visited
var allowNavigation = ##allowNavigation##;

// for translation purposes
var TRANS_SLIDE = "Slide";
var TRANS_MENU = "Menu";
var TRANS_ERROR = "ERROR: There was a problem displaying this image. Please contact your system administrator for further assistance.";

// completionThreshold will be compared against progress measure so lesson can manage its own completion_status
var completionThreshold = ##minProgressForCompletion##;