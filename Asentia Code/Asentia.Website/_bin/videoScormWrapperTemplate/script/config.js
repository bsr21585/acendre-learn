// contains all the customizable options for the Asentia SCORM Video Wrapper

// path to the video
// a path to a video file, relative to root, i.e. "video/small.mp4" OR the full embed code from YouTube or Vimeo
var videoPath = "##videoPath##"; 

// specify the video type: "default" = local/remote video file; "vimeo" = streaming service; "wistia" = streaming service; "youtube" = streaming service
// NOTE: youtube currently does NOT TRACK PROGRESS. lesson will be marked completed on launch
var videoType = "##videoType##";

// allow or deny ability to seek/skip forward/backward in video
// (ONLY FOR NATIVE HTML5 VIDEO TAG - NOT APPLICABLE TO VIMEO/WISTA)
var allowSeekForward = ##allowFastForward##;
var allowSeekBackward = ##allowRewind##;

// turn bookmarking on or off
var useBookmarking = ##allowResume##;

// automatically play the video (video always autoplays on resume); not applicable to mobile devices
// (ONLY FOR NATIVE HTML5 VIDEO TAG - NOT APPLICABLE TO VIMEO/WISTA)
var autoplay = ##enableAutoplay##;

// use the built-in debug window
var useDebug = false;

// error message to appear if video fails to load
var videoFailMessage = "FATAL ERROR: There was a problem loading the video. Please contact your system administrator for further assistance.";

// completionThreshold will be compared against progress measure so lesson can manage its own completion_status
var completionThreshold = ##minProgressForCompletion##;