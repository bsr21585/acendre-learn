//Controller for the video page
// -- Authored by Jevon Pack: 8/27/2015 --

//global object for holding required video settings
var videoSettings = new Object();
var VIMEO = "vimeo";
var WISTIA = "wistia";
var YOUTUBE = "youtube";
var watchProgressInterval;
var nativeVideoPos; //keeps track of video position before user tries a seek

$( document ).ready(function() {
	//pulls videoPath from config js
	if (videoType == "default")
		initVideoControl([videoPath]);
	else if (videoType == VIMEO || videoType == WISTIA || videoType == YOUTUBE)
		initVideoControl([videoPath], videoType);
   
   //initVideoControl(["video/small.mp4"]);
   //initVideoControl(["<iframe src='https://player.vimeo.com/video/138129350' width='500' height='286' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"], "vimeo");
   //initVideoControl(["<iframe src='//fast.wistia.net/embed/iframe/etu9vjrt44' allowtransparency='true' frameborder='0' scrolling='no' class='wistia_embed' name='wistia_embed' allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width='640' height='360'></iframe><script src='//fast.wistia.net/assets/external/E-v1.js' async><\/script>"], "wistia");
});

function initVideoControl(videoFilesArr, embeddedPlayerName)
{
	//Syntax: initVideoControl arguments take an array of video files, or for embebbed players, an array with the embed code, and an argument telling which player it is, wistia or vimeo
	//videoFilesArr: accepts an array of filename string that will be used as source tags for the video i.e.: ["video/MyVideo.mp4", "video/MyVideo.3gp", "video/MyVideo.ogv"]
	//embeddedPlayerName (ONLY REQUIRED IF USING EMBEDDED PLAYER): accepts the name of the video embedding service: "Vimeo" or "Wistia"
	
	//store all default settings
	var vs = videoSettings;
	
	vs.source = ""; //video src being played
	vs.devMode = useDebug; //'true' sends traces and shows helpful debug info about the current video
	vs.autoplay = autoplay; //play video automatically at launch or not (not applicable for mobile since autoplay doesn't work)
	vs.finishedVideoResume = false;
	vs.jumpedToResumePosition = false;
	//Embedded video (Vimeo/Wistia) options
	vs.embeddedPlayerName = ""; //set to name of hosting service if using video from Vimeo or Wistia
	vs.embeddedPlayerOrigin = "*"; //the allowed origin of the embedded video
	vs.embeddedPlayerFrame = $(".videoHolder iframe"); //where the embedded video sits on the page
	vs.embeddedPlayheadPercent = 0;
	vs.completionThreshold = completionThreshold;
	vs.highestProgressMeasureReached = 0;
	
	if (embeddedPlayerName)
		vs.embeddedPlayerName = String(embeddedPlayerName).toLowerCase();
	
	//update src if embedded video
	try
	{
		if (vs.embeddedPlayerName == VIMEO || vs.embeddedPlayerName == WISTIA || vs.embeddedPlayerName == YOUTUBE)
			vs.source = $(videoFilesArr[0]).attr("src");
	}
	catch(e)
	{
		showVideoErrorBox();
	}
	
	//if it's an embedded player (Vimeo or Wistia), insert the embed code
	if (vs.embeddedPlayerName == VIMEO)
	{
		//add vimeo object members
		vs.vimeoIframe = null;
		vs.vimeoPlayer = null;
		
		insertFullscreenTagsOnParentIframes();
		
		try
		{
			embedVimeoFrame(videoFilesArr);
		}
		catch(e)
		{
			showVideoErrorBox();
		}
	}
	else if (vs.embeddedPlayerName == WISTIA)
	{
		insertFullscreenTagsOnParentIframes();
		
		try
		{
			embedWistiaFrame(videoFilesArr);
		}
		catch(e)
		{
			showVideoErrorBox();
		}
	}
	else if (vs.embeddedPlayerName == YOUTUBE)
	{
		//NOTE: YouTube currently does NOT TRACK PROGRESS. lesson will be marked completed on launch
		
		insertFullscreenTagsOnParentIframes();
		
		try
		{
			embedYouTubeFrame(videoFilesArr);
		}
		catch(e)
		{
			showVideoErrorBox();
		}
	}
	else if (!vs.embeddedPlayerName)
	{
		insertFullscreenTagsOnParentIframes();
		
		//insert the video into the page
		insertVideoTag(videoFilesArr);
	}
	
	//enable debug panel if needed
	enableDebugPanel(vs.devMode);
	
	//if not an hosting video service, add the normal event listeners
	if (vs.embeddedPlayerName == VIMEO)
		addVimeoAPI();
	if (vs.embeddedPlayerName == WISTIA)
		initWistiaAPIConnection();
	if (!vs.embeddedPlayerName)
		addVideoEventListeners();
	
	//loadProgress watcher only used for native html video 
	if (!vs.embeddedPlayerName)
		watchProgressInterval = setInterval(watchLoadProgress, 500);
	
	//set 'unknown' status to 'incomplete'
	setCompletionStatus();
	
	//lesson will resume if suspend data present
	resumeLesson();
	
	//if YouTube, autoset progress measure to 1 on launch and set lesson completed
	if (vs.embeddedPlayerName == YOUTUBE)
	{
		setProgressMeasureTo("1");
		setLessonCompleted();
	}
	
	//save data when they exit the lesson (for desktop browsers)
	window.onbeforeunload = function(e) {
		onUserExit();
	};
	
	//save data when they exit the lesson (for mobile browsers)
	window.addEventListener("pagehide", function(evt){
            onUserExit();
        }, false);
		
	addRightClickListener();
}

function insertFullscreenTagsOnParentIframes()
{
	//for embedded video (vimeo/youtube) to do fullscreen, all parent iframes must have the allowfullscreen attribute added to them
	try
	{
		var parentIframes = parent.document.getElementsByTagName('iframe');
		
		for (var i = 0; i < parentIframes.length; i++)
		{
			$(parentIframes[i]).attr("webkitallowfullscreen","");
			$(parentIframes[i]).attr("mozallowfullscreen","");
			$(parentIframes[i]).attr("allowfullscreen","");
		}
	}
	catch(e)
	{
		console.log("Failed adding fullscreen tags to parent iframe(s) for Vimeo: " + e.message);
	}
}

function insertVideoTag(filesArr)
{
	//put the video tag with source in the page
	var videoTagStr = getVideoTagHtml(filesArr);
	$(".videoHolder").append(videoTagStr);
}

function getVideoTagHtml(filesArr)
{
	//return blank string if nothing passed
	if (!filesArr)
		return "";
	
	//autoplay if: this is first load (no suspend data) or bookmarking is false (meaning video will always start from beginning)
	var autoplayAttr = "";
	if (videoSettings.autoplay)
	{
		if (!getLMSSuspendData() || !useBookmarking)
			autoplayAttr = " autoplay";
	}
	
	//return the HTML5 video tag as a string, with video filenames populated
	var tagHtml = "<video id='mainVideo' onloadstart='onVideoLoadStart()' controls" + autoplayAttr + ">";
	
	//add the source video from filesArr
	for (var i = 0; i < filesArr.length; i++)
	{
		tagHtml += "<source onerror='onVideoTagError()' src='";
		tagHtml += filesArr[i];
		tagHtml += "'>";
		//leaving out MIME type for more flexibility
	}
	
	
	//add the default 'support' message
	tagHtml += "Your browser does not support videos of this type.";
	
	//close the video tag
	tagHtml += "</video>";
	
	return tagHtml;
	
}

function embedVimeoFrame(filesArr)
{
	//vimeo embed code will be in index 0 of the files array
	var embedCode = filesArr[0];
	
	try
	{	
		$(".videoHolder").append(embedCode);
	}
	catch(e)
	{
		//show the error box popup
		showVideoErrorBox();
	}
}

function embedWistiaFrame(filesArr)
{
	//vimeo embed code will be in index 0 of the files array
	var embedCode = filesArr[0];
	
	try
	{
		$(".videoHolder").append(embedCode);
	}
	catch(e)
	{
		//show the error box popup
		showVideoErrorBox();
	}
}

function embedYouTubeFrame(filesArr)
{
	//vimeo embed code will be in index 0 of the files array
	var embedCode = filesArr[0];
	
	try
	{
		$(".videoHolder").append(embedCode);
	}
	catch(e)
	{
		//show the error box popup
		showVideoErrorBox();
	}
}

function watchLoadProgress()
{
	//monitor the progress of the load
	try
	{
		//console.log("watchLoadProgress: " + getVideoElement().buffered.end(0));
		
		//once it starts buffering, fill in the source and current position 
		if (getVideoElement().buffered.end(0) > 0)
		{
			//only update it once
			if (!videoSettings.source)
			{
				updateDebugVideoSource();
				updateDebugCurrentPosition();
				updateDebugTotalDuration();
				
				clearInterval(watchProgressInterval);
			}
			
			videoSettings.source = getVideoProperty("currentSrc");
			
		}
	}
	catch(e)
	{
		//console.log("video element not yet buffered...");
	};
	

}

function addVideoEventListeners()
{
	//adds various event listeners to video to allow for tracking
	var videoTag = getVideoElement();
	
	//when the video playhead time changes
	videoTag.ontimeupdate = function() {
		updateVideoTime();
		updateProgressMeasureReached();
	};
	
	//when the video playhead reaches the end
	videoTag.onended = function() {
		onVideoCompletedHandler();
	};
	
	//when video gets paused
	videoTag.onpause = function() {
		onPauseHandler();
	};
	
	//when video gets played
	videoTag.onplay = function() {
		onPlayHandler();
	};
	
	//when the video is playing after having been paused or stopped for buffering
	videoTag.onplaying = function() {
		//onPlayHandler();
	};
	
	//when the browser can start playing the video (only seems to work if attached directly to video tag on html page)
	videoTag.oncanplay = function() {
		//console.log("oncanplay fired");
		//onLoadedDataHandler()
	};
	
	videoTag.onprogress = function() {
		onProgressHandler();
	};
	
	//when user's moving the seek bar
	videoTag.onseeking = function() {
		onSeekHandler();
	}


}

function addVimeoAPI()
{
	//adds the 2019 Vimeo API player connection: https://developer.vimeo.com/player/sdk/basics
	
	//add the vimeo player JS script to the page
	var vimeoScriptElement = document.createElement("script");
	
	$("body").append(vimeoScriptElement);
	
	//listen for script onload so event listeners can be added afterward
	vimeoScriptElement.onload = function() {addVimeoEventListeners();};
	vimeoScriptElement.src = "//player.vimeo.com/api/player.js";
}

function addVimeoEventListeners()
{	
	try
	{
		//register a new vimeo player instance with the API
		videoSettings.vimeoIframe = document.querySelector("iframe");
		videoSettings.vimeoPlayer = new Vimeo.Player(videoSettings.vimeoIframe);
	}
	catch(e)
	{
		console.log("Vimeo Register API Error: " + e.message + "; Detail:");
		console.log(e);
	}
	
	//add the event listeners
	try
	{
		videoSettings.vimeoPlayer.ready().then(vimeoOnReady);
		
		videoSettings.vimeoPlayer.on('timeupdate', vimeoOnTimeUpdate);
		videoSettings.vimeoPlayer.on('pause', vimeoOnPause);
		videoSettings.vimeoPlayer.on('ended', vimeoOnEnded);
		videoSettings.vimeoPlayer.on('play', vimeoOnPlay);
		videoSettings.vimeoPlayer.on('seeked', vimeoOnSeeked);
	}
	catch(e)
	{
		console.log("Vimeo AddEventListener Error: " + e.message + "; Detail:");
		console.log(e);
	}
}

function vimeoOnReady()
{
	updateDebugVideoSource();
		
	//if lesson is resuming, seek to proper position
	//NOTE: THIS WON'T WORK ON MOBILE BECAUSE OF BANDWIDTH PLAY RESTRICTIONS; WILL HAVE TO WAIT UNTIL USER CLICKS PLAY TO DO THE SEEK
	if (videoSCOObj.lessonIsResuming)
		doVimeoSeekToOnResume();
	
	setEmbeddedPlayerDuration();
	setEmbeddedPlayerCurrentTime();
}

function vimeoOnTimeUpdate(data)
{
	updateDebugPlayPause("Playing");
	updateEmbeddedPlayerCurrentTime(data.seconds);
	updateVimeoPercentPlayed(data.percent);
	updateProgressMeasureReached();
}

function vimeoOnPause(data)
{
	updateDebugPlayPause("Paused");
}

function vimeoOnEnded(data)
{
	onVideoCompletedHandler();
}

function vimeoOnPlay(data)
{
	if (videoSCOObj.lessonIsResuming && videoSettings.finishedVideoResume == false)
		doVimeoSeekToOnResume();
}

function vimeoOnSeeked(data)
{
	//if it was resuming, tell it that seek/resume is done
	if (videoSettings.finishedVideoResume == false)
		videoSettings.finishedVideoResume = true;
}

function updateProgressMeasureReached()
{
	//update internal progress measure value
	
	//get progress measure of video's current position
	//pct comes back from 0-100, so multiply by .01 to get it between 0-1
	var progressPct = getPlayheadPercentage() * .01;
	
	//if current position is further than what last recorded value is, update the value
	if (progressPct > videoSettings.highestProgressMeasureReached)
	{
		videoSettings.highestProgressMeasureReached = progressPct;
	}
}

function initWistiaAPIConnection()
{
	//get the video identifier: i.e. src='//fast.wistia.net/embed/iframe/etu9vjrt44' identifier would be "etu9vjrt44"
	var sourceBreak = String(videoSettings.source).split("/");
	var sourceEnd = sourceBreak[sourceBreak.length - 1];
	videoSettings.wistiaSourceEnd = sourceEnd;
	//console.log("ID: " + sourceEnd);

	//get the video handle
	window._wq = window._wq || [];
	
	//this function will run when the video has data
	var targetObj = new Object();
	targetObj[sourceEnd] = function(video)
	{
		//console.log("This will run for embedded Wistia video. Video connected: ", video);
		
		//setup the reference to the wistia video and initial properties
		videoSettings.wistiaVideoRef = Wistia.api(videoSettings.wistiaSourceEnd);
		
		videoSettings.duration = getWistiaRef().duration();
		videoSettings.currentTime = getWistiaRef().time();
		
		//debug fxns
		updateDebugVideoSource();
		updateDebugTotalDuration();
		updateDebugCurrentPosition();
		
		//add listeners
		addWistiaEventListeners();
		
	}
	
	_wq.push(targetObj);

}

function addWistiaEventListeners()
{
	var wRef = getWistiaRef();
	//console.log("Ref established to video: " + wRef.name());
	
	wRef.bind("play", onWistiaPlay);
	wRef.bind("pause", onWistiaPause);
	wRef.bind("timechange", onWistiaTimeChange);
	wRef.bind("seek", onWistiaSeek);
	
	//add callback for when video is ready (ensures resume happens on load in LMS)
	wRef.ready(onWistiaReady);
	
}

function onWistiaReady()
{
	//for resuming
	if (videoSCOObj.lessonIsResuming && !videoSettings.finishedVideoResume)
	{
		doWistiaSeekOnResume();
		doWistiaPlay();
		
		videoSettings.finishedVideoResume = true;
	}
}

function onWistiaPlay()
{
	//for mobile, if lesson is resuming, can only seek to previous spot AFTER user presses play
	if (videoSCOObj.lessonIsResuming && !videoSettings.finishedVideoResume)
	{
		doWistiaSeekOnResume();
		videoSettings.finishedVideoResume = true;
	}
	
	updateDebugPlayPause("Playing");
}

function onWistiaPause()
{
	//update the embedded playhead percent
	updateWistiaPlayheadPercent();
	
	updateDebugPlayPause("Paused");
}

function onWistiaTimeChange(time)
{
	//time: the seconds the video is at
	videoSettings.currentTime = time;
	
	//update the embedded playhead percent
	updateWistiaPlayheadPercent();
	
	//check if video has reached the end
	checkWistiaVideoComplete();
	
	//update the internal progress measure value
	updateProgressMeasureReached();
	
	updateDebugCurrentPosition();
}

function onWistiaSeek()
{
	//if the lesson is resuming, signal that we successful seeked to former position
	if (videoSCOObj.lessonIsResuming && !videoSettings.finishedVideoResume)
	{
		videoSettings.finishedVideoResume = true;
	}
}

function doWistiaSeekOnResume()
{
	//non-mobile devices will automatically seek at this point
	//mobile devices won't be able to seek until after user presses play
	
	//seek here for non-mobile
	var wRef = getWistiaRef();
	var currSuspendData = getLMSSuspendData();
	
	if (currSuspendData)
	{
		//set the video to the last recorded position
		var lastPosition = getVideoTimeFromSuspendData(currSuspendData);
		
		doWistiaSeek(lastPosition);
	}
	
}

function doWistiaSeek(seekTime)
{
	var wRef = getWistiaRef();
	wRef.time(seekTime);
}

function doWistiaPlay()
{
	var wRef = getWistiaRef();
	wRef.play();
}

function updateWistiaPlayheadPercent()
{
	var wRef = getWistiaRef();
	
	//set the percent (from 0 to 1)
	//need to floor the duration because it gets rounded up to an amount that the playhead will never acutally reach
	var pct = Number(wRef.time()) / Math.floor(Number(wRef.duration()));
	
	//if it's greater than 1, set it to 1
	if (pct > 1)
		pct = 1;
	
	videoSettings.embeddedPlayheadPercent = pct;
	
	//console.log("wistia playhead pct: " + videoSettings.embeddedPlayheadPercent);
}

function checkWistiaVideoComplete()
{
	//check if the video has reached the end
	var wRef = getWistiaRef();
	var progressDecimal = videoSettings.embeddedPlayheadPercent;
	
	if (progressDecimal >= 1)
		onVideoCompletedHandler();
}

function getWistiaRef()
{
	//returns the regefence to the wistia video object
	return videoSettings.wistiaVideoRef;
}

function updateVideoTime()
{
	//update the current playhead position of the video
	videoSettings.currentTime = getVideoElement().currentTime;
	
	//if native html video not seeking, store the current position
	if (!getVideoElement().seeking)
		nativeVideoPos = getVideoElement().currentTime;
	
	//update debug panel if active
	updateDebugCurrentPosition();
}

function updateEmbeddedPlayerDuration()
{
	if (getPlayerType() == VIMEO)
	{
		videoSettings.embeddedPlayerDuration = getVideoElement();
	}
}

function updateEmbeddedPlayerCurrentTime(playerSecs)
{
	if (getPlayerType() == VIMEO)
	{
		videoSettings.currentTime = playerSecs;
		updateDebugCurrentPosition();
	}
}

function setEmbeddedPlayerCurrentTime()
{
	//use API to get embedded player time 
	if (getPlayerType() == VIMEO)
	{
		try
		{
			videoSettings.vimeoPlayer.getCurrentTime().then(function(seconds) {
				
				// `seconds` indicates the current playback position of the video
				videoSettings.currentTime = seconds;			
				updateDebugCurrentPosition();
				
				//console.log("vimeoPlayer getCurrentTime(): " + seconds)
			});
		}
		catch(e)
		{
			console.log("Vimeo setEmbeddedPlayerCurrentTime Error:");
			console.log(e);
		}

	}
}

function setEmbeddedPlayerDuration()
{
	//use API to get embedded player time
	if (getPlayerType() == VIMEO)
	{
		
		try
		{
			videoSettings.vimeoPlayer.getDuration().then(function(duration) {
				videoSettings.duration = duration;
				//console.log("vimeo getDuration: " + duration);
				
				updateDebugTotalDuration();
			});
		}
		catch(e)
		{
			console.log("setEmbeddedPlayerDuration Error Detail:");
			console.log(e);
		}
	}
}

function updateVimeoPercentPlayed(pctPlayed)
{
	//directly update the percentage the playhead is thru the video
	if (getPlayerType() == VIMEO)
	{
		videoSettings.embeddedPlayheadPercent = pctPlayed;
	}
	
}

function jumpToVideoPosition(timePos)
{
	//jumps to the specified time/position (in secs) in the video (HTML5 video tag, not embedded players)
	try
	{
		if (getPlayerType() == "default")
			getVideoElement().currentTime = timePos;
	}
	catch(e)
	{
		//console.log("ERROR: Tried jumping to invalid time position in video or method call failed");
	}
}

function doVimeoSeekTo(timeInSecs)
{
	if (getPlayerType() == VIMEO)
	{
		try
		{
			//console.log("try vimeo seekTo: " + String(timeInSecs));
			//alert("try vimeo seekTo: " + String(timeInSecs));
			
			videoSettings.vimeoPlayer.setCurrentTime(timeInSecs).then(function(seconds) {
				
				// `seconds` indicates the actual time that the player seeks to
				//console.log("vimeo setCurrentTime: " + seconds);
				
			}).catch(function(error) {
				
			  switch (error.name) {
				case 'RangeError':
					// The time is less than 0 or greater than the video's duration
					console.log("vimeoPlayer.setCurrentTime Error - The time is less than 0 or greater than the video's duration:");
					console.log(error.name);
					break;

				default:
					// Some other error occurred
					console.log("vimeoPlayer.setCurrentTime Error:");
					console.log(error.name);
					break;
			  }
			  
			});
		}
		catch(e)
		{
			console.log("doVimeoSeekTo Error:");
			console.log(e);
		}
	}
}

function doVimeoSeekToOnResume()
{
	//resumes the lesson/video progress if there is suspend data available
	var currSuspendData = getLMSSuspendData();
	
	if (currSuspendData)
	{
		//set the video to the last recorded position
		var lastPosition = getVideoTimeFromSuspendData(currSuspendData);
		//alert("vimeo - got time from suspend data: " + lastPosition);
		
		doVimeoSeekTo(lastPosition);
	}
}

function vimeoSeekToPreviousSpotForMobileResume()
{
	if (getPlayerType() == VIMEO)
	{
		doVimeoSeekToOnResume()
	}
}

function getPlayheadPercentage()
{
	//returns the percentage that the playhead is currently thru the video
	//NOTE: does not return the actual amount of the video that they've watched
	if (getPlayerType() == VIMEO)
	{
		//since vimeo total duration rounds up (5.586 goes to 6), use API percentage instead of current time to get the pct
		var pct = videoSettings.embeddedPlayheadPercent * 100;
	}
	else if (getPlayerType() == WISTIA)
	{
		var pct = videoSettings.embeddedPlayheadPercent * 100;
	}
	else
	{
		var vidCurrPos = Number(getVideoProperty("currentTime"));
		var vidDuration = Number(getVideoProperty("duration"));
	
		var pct = (vidCurrPos/vidDuration) * 100;
	}
	
	return pct;
}

function doVideoPause()
{
	getVideoElement().pause();
}

function doVideoPlay()
{
	getVideoElement().play();
}

function onLoadedDataHandler()
{
	//videoSettings.source = getVideoElement().src;
}

function onVideoLoadStart()
{
	//when browser starts looking for video (first event)
	//console.log("video onloadstart fired: " + getVideoProperty("duration") + " || " +  getVideoProperty("currentSrc"));
}

function onVideoTagError()
{
	//fires when there's an error loading the HTML5 video
	showVideoErrorBox();
	
}

function showVideoErrorBox()
{
	//hide the video tag
	$(".videoHolder").css("display","none");
	
	//show the alert box
	$("#errorBox").css("display","block");
	
	//insert the custom message
	$("#errorBox").html(videoFailMessage);
}

function onPlayHandler()
{
	updateDebugPlayPause("Playing");
}

function onPauseHandler()
{
	updateDebugPlayPause("Paused");
}

function onProgressHandler()
{
	//console.log("ONPROGRESS");
}

function onVideoCompletedHandler()
{
	//set video to completed (progress measure will be 1, which in turn completes the lesson)
	setProgressMeasure();
	
	//in the event the lesson somehow doesn't get set to completed from progress measure being '1', set lesson to completed if not already
	setLessonCompleted();
	
	//once video reaches end of playhead
	updateDebugHasReachedEnd();
}

function onSeekHandler()
{
	//for native html5 video
	if (videoSettings.embeddedPlayerName == "")
	{
		var delta = getVideoElement().currentTime - nativeVideoPos;
		
		//**FOR FUTURE REFERENCE**: BELOW WILL NEED TO BE ADDED TO WISTIA/VIMEO WHEN THOSE PLATFORMS ARE ULTIMATELY USED
		
		//allow forward seeking?
		if (!allowSeekForward & delta > 0.01)
		{
			getVideoElement().currentTime = nativeVideoPos;
		}
		
		//allow backward seeking?
		if (!allowSeekBackward & delta < -0.01)
		{
			getVideoElement().currentTime = nativeVideoPos;
		}
		
	}
}

function getVideoProperty(propName)
{
	//return specified html5 video DOM property
	if (getPlayerType() == VIMEO || getPlayerType() == WISTIA)
	{
		//convert for vimeo properties
		switch(propName)
		{
			case "currentTime":
			//NOTE: Vimeo ROUNDS UP on duration, so video might end at 5.568, but total duration may report 6; this discrepancy causes issues with progress_measure bc it will never reach 1, i.e. completed:JP FIX THIS (if hasReachedEnd, return the totalDuration instead of current time) 
				return videoSettings.currentTime;
				break;
			case "duration":
				return videoSettings.duration;
				break;
			case "currentSrc":
				return videoSettings.source;
				break;
			default:
				return;
				break;
		}
	}
	else if (getPlayerType() == YOUTUBE)
	{
		//no actions for youtube yet
		return "";
	}
	else
		//normal HTML5 video tag property
		return getVideoElement()[propName];
}

function getPlayerType()
{
	//default (html5), vimeo, wistia...
	if (videoSettings.embeddedPlayerName == VIMEO)
		return VIMEO;
	else if (videoSettings.embeddedPlayerName == WISTIA)
		return WISTIA;
	else if (videoSettings.embeddedPlayerName == YOUTUBE)
		return YOUTUBE;
		
	return "default";
}

function getVideoElement()
{
	if (getPlayerType() == VIMEO || getPlayerType() == WISTIA)
		return videoSettings.embeddedPlayerFrame;
	else
		return $("#mainVideo")[0];
}

function addRightClickListener()
{
	//prevent right-click on the page
	$("html").attr("oncontextmenu","return false;");
}



