// -- DEBUG PANEL FUNCTIONS --
function enableDebugPanel(flag) {
    //hides/undhides the debug panel
    if (flag) {
        $("#videoInfo").css("display", "block");
    }
    else {
        $("#videoInfo").css("display", "none");
    }
}

function updateDebugCurrentPosition() {
    if (videoSettings.devMode)
        $("#dbgCurrentPosition").html("Current position: " + getVideoProperty("currentTime"));
}

function updateDebugVideoSource() {
    if (videoSettings.devMode)
        $("#dbgVideoSource").html("Video source: " + getVideoProperty("currentSrc"));

}


function updateDebugPlayPause(commandStr) {
    //console.log("updated debug play/pause text");
    if (videoSettings.devMode) {
        //only update text if it has changed
        var oldState = $("#dbgPlayPause").html();
        var newState = "Play/pause state: " + commandStr;

        if (newState != oldState)
            $("#dbgPlayPause").html("Play/pause state: " + commandStr);
    }
}

function updateDebugTotalDuration() {
    if (videoSettings.devMode)
        $("#dbgTotalDuration").html("Total duration: " + getVideoProperty("duration"));
}

function updateDebugHasReachedEnd() {
    if (videoSettings.devMode)
        $("#dbgHasReachedEnd").html("Has reached end: True");
}