// Controls the SCORM functionality of the Asentia SCORM Video wrapper SCO
var videoSCOObj = new Object();
videoSCOObj.SCOTestMode = false; //adds stand-in suspend data to mimic a lesson resume
videoSCOObj.lessonIsResuming = false;
videoSCOObj.resumeInterval;
videoSCOObj.resumeSuspendData;
videoSCOObj.calledUserExit = false;

function getLMSSuspendData() {
    if (videoSCOObj.SCOTestMode)
        return "<sd><ct>10</ct></sd>";
    else
        return doLMSGetValue("cmi.suspend_data");
}

function setLMSSuspendData() {
    //send the suspend data to the LMS
    var currSuspendData = buildSuspendData();

    doLMSSetValue("cmi.suspend_data", currSuspendData);
}

function buildSuspendData() {
    //builds and returns the suspend data string
    var suspendDataStr = "<sd>";

    //add the current time of the video
    suspendDataStr += "<ct>";

    if (videoSCOObj.SCOTestMode)
        suspendDataStr += "8";
    else
        suspendDataStr += String(getVideoProperty("currentTime"));

    //close the current time tag
    suspendDataStr += "</ct>";

    //close the suspend data tag
    suspendDataStr += "</sd>";

    return suspendDataStr;
}

function resumeLesson() {
    //resumes the lesson/video progress if there is suspend data available
    var currSuspendData = getLMSSuspendData();

    //only resume if bookmarking is enabled
    if (currSuspendData && useBookmarking) {
        //set the video to the last recorded position
        //var lastPosition = getVideoTimeFromSuspendData(currSuspendData);

        //jumpToVideoPosition(lastPosition);

        //let it know that this is a resume
        videoSCOObj.lessonIsResuming = true;

        //restore the highestProgressMeasureReached from LMS progress_measure
        restoreHighestProgressMeasureReachedOnResume();

        //by default, play video automatically if resuming (when enough data is available to start playing)
        videoSCOObj.resumeInterval = setInterval(resumeIntervalHandler, 500);
    }
}

function restoreHighestProgressMeasureReachedOnResume() {
    //restore the highestProgressMeasureReached from LMS progress_measure
    var restoredProgressMeasure = getProgressMeasureFromLMS();
    if (restoredProgressMeasure) {
        videoSettings.highestProgressMeasureReached = Number(restoredProgressMeasure);
    }
}

function determineLessonCompletion() {
    //compare internally stored progess_measure against completionThreshold to determine if lesson should be marked as completed
    if (videoSettings.highestProgressMeasureReached >= videoSettings.completionThreshold)
        setLessonCompleted();
}

function resumeIntervalHandler() {
    //if the video is still waiting to resume, continue trying to resume it...
    //b/c on mobile devices, we won't be able to access the video until user manually plays it
    if (!videoSettings.finishedVideoResume) {
        //only request the suspend data from the LMS once to avoid 'get value' call overload
        if (!videoSCOObj.resumeSuspendData)
            videoSCOObj.resumeSuspendData = getLMSSuspendData();

        //var currSuspendData = getLMSSuspendData();
        var currSuspendData = videoSCOObj.resumeSuspendData;

        if (getPlayerType() == VIMEO) {
            //For non-mobile, resume is handled in OnReady event
            //For mobile, resume is handled after user clicks play (since mobile doesn't support autoplay)
            //So, clear this interval
            clearInterval(videoSCOObj.resumeInterval);
        }
        else if (getPlayerType() == WISTIA) {
            //For non-mobile, resume is handled when video data has been received
            //For mobile, resume is handled after user clicks play (since mobile doesn't support autoplay)
            clearInterval(videoSCOObj.resumeInterval);

            //for resuming
            if (videoSCOObj.lessonIsResuming && !videoSettings.finishedVideoResume) {
                //see if the video is ready
                var wRef = getWistiaRef();

                if (wRef.ready() && wRef.hasData()) {
                    doWistiaSeekOnResume();
                    //clearInterval(videoSCOObj.resumeInterval);
                }
            }

        }
        else {
            if (videoSettings.debugMode) {
                if ($("#rs").length == 0)
                    $("#videoInfo").append("<p id='rs'></p>");
                $("#rs").html("");
                $("#rs").append("Ready state: " + String(getVideoProperty("readyState")))
            }

            if (Number(getVideoProperty("readyState")) > 2 && !videoSettings.jumpedToResumePosition) {
                //console.log("got to > rs2");
                //set the video to the last recorded position
                var lastPosition = getVideoTimeFromSuspendData(currSuspendData);
                jumpToVideoPosition(lastPosition);

                //signal that video has seeked to its former position
                videoSettings.jumpedToResumePosition = true;


            }


            //if enough data is available to start playing, play() and clear interval (Note: this won't work < IE9)
            if (getVideoProperty("readyState") == 4) {
                //console.log("got to rs4");
                //set the video to the last recorded position
                //var lastPosition = getVideoTimeFromSuspendData(currSuspendData);
                //jumpToVideoPosition(lastPosition);

                //after jumping to position, play the video (this is for desktop; can't manually invoke the html video to play on mobile devices)
                doVideoPlay();

                //signal that video has resumed
                videoSettings.finishedVideoResume = true;

                clearInterval(videoSCOObj.resumeInterval);
            }

            //if readyState isn't reported, clear the interval automatically (< IE9 safeguard)
            if (getVideoProperty("readyState") == undefined)
                clearInterval(videoSCOObj.resumeInterval);
        }
    }


}

function resumeIntervalHandlerOrig() {
    //if enough data is available to start playing, play() and clear interval (Note: this won't work < IE9)
    if (getVideoProperty("readyState") == 4) {
        doVideoPlay();
        clearInterval(videoSCOObj.resumeInterval);
    }

    //if readyState isn't reported, clear the interval automatically (< IE9 safeguard)
    if (getVideoProperty("readyState") == undefined)
        clearInterval(videoSCOObj.resumeInterval);
}

function getVideoTimeFromSuspendData(suspendDataStr) {
    //return the time/progress of the video as a number (secs) from suspend data string
    var sD = suspendDataStr;

    var videoTime = Number($(sD).find("ct").html());

    return videoTime;
}

function updateSCORMData() {
    //send all necessary SCORM data to the LMS

    //update suspend data
    setLMSSuspendData();

    //set how far thru the video they are
    setProgressMeasure();

    //check if lesson should be set to completed
    determineLessonCompletion();

}

function setProgressMeasure() {
    //set the progress measure (what number [0-1] represents how far they are in the lesson)

    //pct comes back from 0-100, so multiply by .01 to get it between 0-1
    var progressPct = getPlayheadPercentage() * .01;

    //LMS value must be a real number with a precision of seven significant digits: 0.1234567
    //truncate if larger than needed
    var pctStr = String(progressPct);
    if (pctStr.length > 9)
        pctStr = pctStr.substr(0, 9);


    //DO NOT set the progress measure if the value in the LMS is already greater than this current number; avoids 'backward progress'
    var LMSProgressMeasure = getProgressMeasureFromLMS();
    if (LMSProgressMeasure) {
        //compare the two values and record the new value only if it's greater than current SCORM progress measure valure
        if (Number(pctStr) > Number(LMSProgressMeasure)) {
            //doLMSSetValue("cmi.progress_measure", pctStr);
            setProgressMeasureTo(pctStr);

            //for testing
            //alert("SET MEASURE (IF ALREADY EXISTING): " + pctStr);
        }

    }
    else {
        //doLMSSetValue("cmi.progress_measure", pctStr);
        setProgressMeasureTo(pctStr);

        //for testing
        //alert("SET MEASURE (IF NOT ALREADY EXISTING): " + pctStr);
    }
}

function setProgressMeasureTo(pProgMeasure) {
    //directly set progress measure to a value
    doLMSSetValue("cmi.progress_measure", String(pProgMeasure));
}

function getProgressMeasureFromLMS() {
    return doLMSGetValue("cmi.progress_measure");
}

function setCompletionStatus() {
    if (!API) return "";
    if (blnFinishCalled) return "";

    //set 'incomplete' or 'completed' based on progress and if already set to completed (can't go from 'completed' to 'incomplete')
    var currentCompletionStatus = doLMSGetValue("cmi.completion_status");

    //if it's already completed, do nothing
    if (currentCompletionStatus == "completed")
        return;

    //if it's unknown, set to incomplete
    if (currentCompletionStatus == "unknown")
        doLMSSetValue("cmi.completion_status", "incomplete");

    //lesson will have already be set to completed if video has reached the end

    //NOTE: if adding feature to force user watch ALL PARTS of video before "completed" granted, add that here
}

function setLessonCompleted() {
    if (!API) return "";
    if (blnFinishCalled) return "";

    //set completion_status to 'completed'
    var currentCompletionStatus = doLMSGetValue("cmi.completion_status");

    if (currentCompletionStatus != "completed")
        doLMSSetValue("cmi.completion_status", "completed");
}

function onUserExit() {
    //if they close the window, save their progress
    //console.log("User exit: " + getVideoProperty("currentTime"));

    //if this has already been called, don't call it again
    if (videoSCOObj.calledUserExit)
        return;

    //store the data to the LMS
    updateSCORMData();

    //always suspend the session so the data is restored next time
    doLMSSetValue("cmi.exit", "suspend");

    //set flag so this doesn't get double-called
    videoSCOObj.calledUserExit = true;
}