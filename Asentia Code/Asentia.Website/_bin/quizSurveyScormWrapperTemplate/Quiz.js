function Quiz(j, nodeRoot, dictionary, SCORM) {

    // event listeners for the confirm modals
    window._Quiz = this;
    window.addEventListener("message", function (e) {
        switch (e.data) {
            case "OK":
                window._Quiz.ConfirmOK();
                window._Quiz.ConfirmClose();                
                break;
            case "Cancel":
                window._Quiz.ConfirmCancel();
                window._Quiz.ConfirmClose();
                break;
            case "AlertAck":
                window._Quiz.ConfirmClose();
                break;
        }
    }, false);

    this.SCORM = SCORM;
    this.dataModel = j;
    this.nodeRoot = nodeRoot;
    nodeRoot.object = this;
    nodeRoot.id = this.dataModel.ObjectId;
    nodeRoot.className = this.dataModel.ObjectType;
    this.Questions = [];
    this.Location = "";

    //get the user language from the SCORM API

    this.DefaultLanguage = this.dataModel.DefaultLanguage;

    this.DICTIONARY = dictionary; // this is the translation dictionary
    this.Language = this.DICTIONARY.ActiveLanguage; // language as set by default on page.

    if (this.SCORM.ObjectName == "SCORMHandler") {
        this.DICTIONARY.SetActiveLanguage(this.SCORM.GetValue("cmi.learner_preference.language"));
        this.Language = this.DICTIONARY.ActiveLanguage;
    }

    this.nodeRoot.className = this.nodeRoot.className + " " + this.Language;

    this.CurrentQuestionIdx = 0;
    this.operatingWidth = 0;

    this.TYPES = {
        QUIZ: "quiz",
        SURVEY: "survey"
    }

    this.MODES = {
        ACTIVE: "active",
        REVIEW: "review"
    }

    // protect against window closure

    //window.onbeforeunload = function(){
    //	return "";
    //}

    this.Mode = this.MODES.ACTIVE;

    // quiz title
    var tt = "";

    for (var i = 0; i < this.dataModel.Titles.length; i++) {
        if ((this.dataModel.Titles[i].Language == this.Language) || (this.dataModel.Titles[i].Language == this.DefaultLanguage && tt == "")) {
            if (this.dataModel.Titles[i].Value != "") {
                tt = this.dataModel.Titles[i].Value;
            }
        }
    }

    var t = document.createElement("div");
    t.className = "title";
    t.appendChild(document.createTextNode(tt));

    // close lesson button
    this.CloseLessonButton = document.createElement("img");
    this.CloseLessonButton.setAttribute("src", "./_images/exit.png");
    this.CloseLessonButton.setAttribute("title", this.DICTIONARY.Get("exit"));
    this.CloseLessonButton.setAttribute("alt", this.DICTIONARY.Get("exit"));
    this.CloseLessonButton.className = "CloseLessonButton";
    this.CloseLessonButton.parent = this;
    this.CloseLessonButton.disabled = false;
    this.CloseLessonButton.onclick = function () {
        this.parent.Exit();
    }

    this.nodeRoot.appendChild(this.CloseLessonButton);
    this.nodeRoot.appendChild(t);

    // build the questions/containers

    this.nodeQuestionsContainer = document.createElement("div")
    this.nodeQuestionsContainer.id = "questionsContainer";
    this.nodeQuestionsContainer.className = "questionsContainer";

    for (var i in this.dataModel.Questions) {

        //instantiate the question node
        var nodeQuestion = document.createElement("div");

        //build the question object
        this.Questions.push(new Question(this.dataModel.Questions[i], nodeQuestion, this));

        // let the question know what it's index is
        //this.Questions[this.Questions.length - 1].Index = this.Questions.length - 1;

        //surveys are all on one page
        if (this.dataModel.ObjectType == this.TYPES.SURVEY) {
            // attach every question
            this.nodeQuestionsContainer.appendChild(nodeQuestion);
            // page by page questions attached after navigation
        }

    }

    // append to the root
    this.nodeRoot.appendChild(this.nodeQuestionsContainer);

    // create the Results Page, do not append yet
    var nodeResults = document.createElement("div");
    this.Results = new Results(nodeResults, this);

    //append results container
    this.nodeResultsContainer = document.createElement("div");
    this.nodeResultsContainer.className = "resultsContainer";
    this.nodeRoot.appendChild(this.nodeResultsContainer);

    //navigation

    if (this.dataModel.ObjectType == this.TYPES.QUIZ) { // quizzes are multi page; add navigation

        // Quiz

        this.NavigationContainer = document.createElement("div");
        this.NavigationContainer.className = "navigationContainer";

        this.NextButton = document.createElement("button");
        this.NextButton.parent = this;

        // disabled next button if quiz requires each question answered before continueing
        if (this.dataModel.RequireAnswerToContinue == true) {
            this.NextButton.disabled = true;
        }

        //set the onclick function // PAY ATTENTION to scoping.
        this.NextButton.onclick = function () {
            if (this.parent.Mode == this.parent.MODES.ACTIVE) {
                if (!this.parent.Questions[this.parent.CurrentQuestionIdx].IsSubmitted) {
                    this.parent.ConfirmOK = function () {
                        this.DisplayNextQuestion();
                    }
                    this.parent.Confirm(this.parent.DICTIONARY.Get("no answer continue head"), this.parent.DICTIONARY.Get("no answer continue message"));
                } else {
                    this.parent.DisplayNextQuestion();
                }
            } else {
                this.parent.DisplayNextQuestion();
            }
        }

        this.NextButton.appendChild(document.createTextNode(this.DICTIONARY.Get("next")));

        this.BackButton = document.createElement("button");
        this.BackButton.parent = this;
        this.BackButton.disabled = true; // page one starts disabled
        this.BackButton.onclick = function () {
            this.parent.DisplayPreviousQuestion();
        }
        this.BackButton.appendChild(document.createTextNode(this.DICTIONARY.Get("back")));

        this.DoneButton = document.createElement("button");
        this.DoneButton.parent = this;
        this.DoneButton.disabled = false;
        this.DoneButton.onclick = function () {
            if (this.parent.Mode == this.parent.MODES.ACTIVE) {
                this.parent.ConfirmOK = function () {
                    this.CalculateResults();
                    this.ShowResults();
                }

                // check that all questions answered
                var ua = false; //unanswered exists
                var st = "";
                for (var i = 0; i < this.parent.Questions.length; i++) {
                    if (this.parent.Questions[i].IsSubmitted != true) {
                        ua = true;
                    }
                }
                if (ua) {
                    st = "end " + this.parent.dataModel.ObjectType + " ua message";
                } else {
                    st = "end " + this.parent.dataModel.ObjectType + " message";
                }
                this.parent.Confirm(this.parent.DICTIONARY.Get("end " + this.parent.dataModel.ObjectType + " head"), this.parent.DICTIONARY.Get(st));
            } else {
                this.parent.ShowResults();
            }
        }

        this.DoneButton.appendChild(document.createTextNode(this.DICTIONARY.Get("done")));

        this.NavigationContainer.appendChild(this.BackButton);
        this.NavigationContainer.appendChild(this.NextButton);
        this.NavigationContainer.appendChild(this.DoneButton);

        this.nodeRoot.appendChild(this.NavigationContainer);

        // now attach the first question
        this.DisplayQuestion(0);

    } else { // SURVEY

        this.NavigationContainer = document.createElement("div");
        this.NavigationContainer.className = "navigationContainer";

        this.DoneButton = document.createElement("button");
        this.DoneButton.parent = this;
        this.DoneButton.disabled = false;
        this.DoneButton.onclick = function () {
            if (this.parent.Mode == this.parent.MODES.ACTIVE) {
                this.parent.ConfirmOK = function () {
                    this.EvaluateAllQuestions();
                    this.CalculateResults();
                    this.ShowResults();
                }
                // check that all questions have inputs filled
                if (!this.parent.IsAllQuestionsInput()) { // missing responses
                    this.parent.Confirm(this.parent.DICTIONARY.Get("submit survey head"), this.parent.DICTIONARY.Get("submit survey ua message"));
                } else { // all responses filled
                    this.parent.Confirm(this.parent.DICTIONARY.Get("submit survey head"), this.parent.DICTIONARY.Get("submit survey message"));
                }
            } else {
                this.parent.ShowResults();
            }
        }
        this.DoneButton.appendChild(document.createTextNode(this.DICTIONARY.Get("submit")));

        this.NavigationContainer.appendChild(this.DoneButton);

        this.nodeRoot.appendChild(this.NavigationContainer);
    }

    this.GetOperatingWidth();

    // SCORM

    if (this.SCORM.ObjectName == "SCORMHandler") {

        switch (this.SCORM.GetValue("cmi.entry")) {
            case "resume":

                // cmi.exit
                if (this.dataModel.AllowBookmarking) {
                    this.SCORM.SetValue("cmi.exit", "suspend");
                } else {
                    this.SCORM.SetValue("cmi.exit", "");
                }

                this.RestoreStateFromSCORMAPI();

                break;
            default: // clean attempt

                //cmi.exit
                if (this.dataModel.AllowBookmarking) {
                    this.SCORM.SetValue("cmi.exit", "suspend");
                } else {
                    this.SCORM.SetValue("cmi.exit", "");
                }

                this.SCORM.SetValue("cmi.success_status", "unknown");
                this.SCORM.SetValue("cmi.completion_status", "incomplete");
                break;
        }

    }

    if (this.Mode == this.MODES.REVIEW) {

        this.DoneButton.removeChild(this.DoneButton.firstChild);
        this.DoneButton.appendChild(document.createTextNode(this.DICTIONARY.Get("back to summary")));

    }

}

/*

METHOD: Quiz.GetOperatingWidth()
Gets the actual width of the Question display.

*/
Quiz.prototype.GetOperatingWidth = function () {

    // get the width
    this.operatingWidth = this.nodeQuestionsContainer.childNodes[0].offsetWidth;

}

/*

METHOD: Quiz.DisplayQuestion(idx)
Displays onscreen the Question with index number idx
by attaching it to the DOM

*/
Quiz.prototype.DisplayQuestion = function (idx) {

    //

    this.CurrentQuestionIdx = idx;

    // record the location
    if (this.dataModel.ObjectType == this.TYPES.SURVEY) {
        this.Location = "questions";
    } else {
        this.Location = "question_" + idx;
    }

    // set the SCORM location
    try {
        this.SCORM.SetValue("cmi.location", this.Location);
    } catch (e) { }

    // attach it to the DOM so it displays
    this.nodeQuestionsContainer.appendChild(this.Questions[idx].nodeRoot);

    // back button
    if (idx <= 0) {
        if (this.BackButton) {
            this.BackButton.disabled = true;
        }
    } else {
        if (this.BackButton) {
            this.BackButton.disabled = false;
        }
    }

    // next button
    if (idx >= this.Questions.length - 1) {
        if (this.NextButton) {
            this.NextButton.disabled = true;
        }
    } else {
        if (this.NextButton) {
            this.NextButton.disabled = false;
        }
    }

    // disable the next button if necessary (ACTIVE mode only, not REVIEW)
    if (this.Mode == this.MODES.ACTIVE) {
        if ((this.Questions[this.CurrentQuestionIdx].LearnerResult == this.Questions[this.CurrentQuestionIdx].RESPONSE_STATES.NOT_ANSWERED) && (this.dataModel.RequireAnswerToContinue == true)) {
            if (this.NextButton) {
                this.NextButton.disabled = true;
            }
        }
    }

}

/*

METHOD: Quiz.RemoveCurrentQuestion()
Hides from the screen the currently displayed Question 
by removing it from the DOM

*/
Quiz.prototype.RemoveCurrentQuestion = function () {

    this.nodeQuestionsContainer.removeChild(this.nodeQuestionsContainer.childNodes[0]);

}

/*

METHOD: Quiz.DisplayNextQuestion()
Activates the next question in line by hiding the current question
and then displaying the next question.

*/
Quiz.prototype.DisplayNextQuestion = function () {

    if (this.CurrentQuestionIdx >= this.Questions.length - 1) {
        return;
    }

    this.RemoveCurrentQuestion();
    this.DisplayQuestion(this.CurrentQuestionIdx + 1);

}

/*

METHOD: Quiz.DisplayPreviousQuestion()
Activates the previous question in line by hiding the current question
and then displaying the previous question.

*/
Quiz.prototype.DisplayPreviousQuestion = function () {

    if (this.CurrentQuestionIdx == 0) {
        return;
    }

    this.RemoveCurrentQuestion();
    this.DisplayQuestion(this.CurrentQuestionIdx - 1);

}

/*

METHOD: Quiz.ShowResults()
Displays the Results page

*/
Quiz.prototype.ShowResults = function () {

    // reset the index
    this.CurrentQuestionIdx = -1;

    // record the location
    if (this.dataModel.ObjectType == this.TYPES.QUIZ) {
        this.Location = "results";
    } else {
        this.Location = "results_0";
    }

    // set the SCORM location

    try {
        this.SCORM.SetValue("cmi.location", this.Location);
    } catch (e) { }

    // save state

    this.SaveStateToSCORMAPI();

    // remove question containers

    if (this.dataModel.ObjectType == this.TYPES.SURVEY) { // single page; remove all question containers
        for (var i = 1; i <= this.Questions.length; i++) {
            try {
                this.nodeQuestionsContainer.removeChild(this.nodeQuestionsContainer.childNodes[0]);
            } catch (e) { }
        }
    } else {
        //multi-page, remove only the active question
        this.RemoveCurrentQuestion();
    }

    // fill in the text values

    this.Results.TotalCount.innerHTML = this.RawScorePossible().toString();
    this.Results.CorrectCount.innerHTML = this.RawScoreEarned().toString();
    this.Results.pct.innerHTML = this.PercentageCaption(this.ScaledScoreEarned());
    this.Results.ScaledPassingScore.innerHTML = this.PercentageCaption(this.dataModel.MinimumPassingScore);

    // Image

    if (this.ScaledScoreEarned() >= this.dataModel.MinimumPassingScore) {
        this.Results.ResultsImage.setAttribute("src", "./_images/correct.png");
        this.Results.ResultsImage.className = "pass";
        this.Results.SummaryBrief.innerHTML = this.DICTIONARY.Get("congratulations-passed");
    } else {
        this.Results.ResultsImage.className = "fail";
        this.Results.SummaryBrief.innerHTML = this.DICTIONARY.Get("sorry-failed");
    }

    // attach it to the DOM so it displays
    this.nodeResultsContainer.appendChild(this.Results.nodeRoot);

    // buttons disabled
    this.DoneButton.disabled = true;

    if (this.BackButton)
        this.BackButton.disabled = true;
    if (this.NextButton)
        this.NextButton.disabled = true;

    //remove the navigation
    //this.nodeRoot.removeChild(this.NavigationContainer);
}

Quiz.prototype.HideResults = function () {

    this.nodeResultsContainer.removeChild(this.Results.nodeRoot);

}

/*

METHOD: Quiz.CalculateResults()
Processes the quiz to generate results.

*/
Quiz.prototype.CalculateResults = function () {

    //if already tallied, exit
    if (this.Results.IsFinal)
        return true;

    //submit all unsubmitted questions
    for (var i = 0; i < this.Questions.length; i++) {

        if (this.Questions[i].LearnerResult == this.Questions[i].RESPONSE_STATES.CORRECT) {
            //do nothing
        } else {
            //mark wrong
            this.Questions[i].RESPONSE_STATES.WRONG;
            //revert inputs
            this.Questions[i].SetInputsFromLearnerResponseValue(this.Questions[i].LearnerResponse);
            //commit
            if (this.Questions[i].IsSubmitted) {
                //submission exists
                this.Questions[i].DoFeedback(true);
            } else {
                //never submitted
                this.Questions[i].IsSubmitted = true;
                this.Questions[i].Evaluate();
                this.Questions[i].DoFeedback(true);
            }
        }

        // disable all inputs
        this.Questions[i].DisableAll();
    }

    // post SCORM values

    try {
        switch (this.dataModel.ObjectType) {
            case this.TYPES.SURVEY:

                // no scoring, set suspend for review
                this.SCORM.SetValue("cmi.completion_status", "completed");
                this.SCORM.SetValue("cmi.success_status", "unknown"); // no pass, no fail.
                this.SCORM.SetValue("cmi.exit", "suspend");
                break;

            case this.TYPES.QUIZ:

                this.SCORM.SetValue("cmi.score.min", "0");
                this.SCORM.SetValue("cmi.score.max", this.RawScorePossible().toString());
                this.SCORM.SetValue("cmi.score.raw", this.RawScoreEarned().toString());
                this.SCORM.SetValue("cmi.score.scaled", this.ScaledScoreEarned().toString());

                this.SCORM.SetValue("cmi.completion_status", "completed");

                if (this.ScaledScoreEarned() >= this.dataModel.MinimumPassingScore) {
                    this.SCORM.SetValue("cmi.success_status", "passed");

                    // when passed, set resume so review is possible (even if 'no bookmarking' is configured)
                    this.SCORM.SetValue("cmi.exit", "suspend");
                } else {
                    this.SCORM.SetValue("cmi.success_status", "failed");

                    // when failed, start new attempt on next launch
                    this.SCORM.SetValue("cmi.exit", "");
                }
                break;

        }

    } catch (e) { }

    // save state

    this.SaveStateToSCORMAPI();

    // mark final

    this.Results.IsFinal = true;

    // set to review

    this.Mode = this.MODES.REVIEW;

}

Quiz.prototype.EvaluateAllQuestions = function () {

    for (var i = 0; i < this.Questions.length; i++) {
        this.Questions[i].Evaluate();
    }

    return true;

}

Quiz.prototype.IsAllQuestionsInput = function () {

    for (var i = 0; i < this.Questions.length; i++) {
        if (this.Questions[i].IsNothingInput()) {
            return false;
        }
    }

    return true; // all questions have inputs filled

}

Quiz.prototype.RawScorePossible = function () {

    var raw = 0;

    for (var i = 0; i < this.Questions.length; i++) {
        raw = raw + this.Questions[i].RawScorePossible();
    }

    return raw;
}

Quiz.prototype.RawScoreEarned = function () {

    var raw = 0;

    for (var i = 0; i < this.Questions.length; i++) {
        raw = raw + this.Questions[i].RawScoreEarned();
    }

    return raw;
}

Quiz.prototype.ScaledScoreEarned = function () {

    var pct = 0;

    try {
        pct = Math.round(this.RawScoreEarned() / this.RawScorePossible() * 100, 2) / 100;
    } catch (e) { }

    return pct;
}

Quiz.prototype.PercentageCaption = function (p) {

    var pct = 0;

    try {
        pct = Math.round(p * 100);
        pct = pct.toString() + "%";
    } catch (e) { }

    return pct;

}

Quiz.prototype.Exit = function () {

    // set the OK action

    this.ConfirmOK = function () {
        this.SCORM.Terminate();
        window.parent.courseLaunchController.processLMSNavRequest('exitAll');
    }

    if (!this.Results.IsFinal && !this.dataModel.AllowBookmarking) { // if not finalized AND not set to resume, warn of starting over on relaunch
        this.Confirm(this.DICTIONARY.Get("exit resume warning head"), this.DICTIONARY.Get("exit resume warning message"));
    } else {
        this.Confirm(this.DICTIONARY.Get("exit continue head"), this.DICTIONARY.Get("exit continue message"));
    }

}

Quiz.prototype.RestoreStateFromSCORMAPI = function () {

    var s = this.SCORM.GetValue("cmi.suspend_data");

    try {
        var state = JSON.parse(s);
    } catch (e) {
        return;
    }

    this.Mode = state.Mode;
    this.CurrentQuestionIdx = state.CurrentQuestionIdx;
    this.Location = this.SCORM.GetValue("cmi.location"); // use this location since it does not require a large state change save to persist
    this.Results.IsFinal = state.IsFinal;

    // restore questions

    for (var i = 0; i < this.Questions.length; i++) {

        this.Questions[i].RestoreStateFromObject(state.Interactions[this.Questions[i].dataModel.QuestionId]);

    }

    // resume location

    switch (this.Location) {
        case "results":
        case "results_0":
            this.ShowResults();
            break;
        case "questions":
            // nothing, all questions would already be displayed
            break;
        default:
            this.RemoveCurrentQuestion();
            this.DisplayQuestion(Number(this.Location.replace("question_", "")));
            break;
    }

}

/*

Saves the state of the quiz to JSON in the suspend_data parameter.

*/
Quiz.prototype.SaveStateToSCORMAPI = function () {

    var state = new Object;
    state.Interactions = {};

    state.Mode = this.Mode;
    state.CurrentQuestionIdx = this.CurrentQuestionIdx;
    state.Location = this.Location;
    state.IsFinal = this.Results.IsFinal;

    for (var i = 0; i < this.Questions.length; i++) {

        state.Interactions[this.Questions[i].dataModel.QuestionId] = this.Questions[i].SaveStateToObject();

    }

    this.SCORM.SetValue("cmi.suspend_data", JSON.stringify(state));

}

Quiz.prototype.Review = function () {

    //safety
    if (this.dataModel.AllowReview != true)
        return true;    

    this.HideResults();

    // display question(s)
    if (this.dataModel.ObjectType == this.TYPES.QUIZ) {
        // display the first question
        this.DisplayQuestion(0);
    } else {
        // display all questions (single page)
        for (var i = 0; i < this.Questions.length; i++) {
            this.DisplayQuestion(i);
        }
    }

    // remove the popin class from the feedback images
    for (var i = 0; i < this.Questions.length; i++) {
        this.Questions[i].Feedback.Image.className = "";
    }

    this.DoneButton.removeChild(this.DoneButton.childNodes[0]);
    this.DoneButton.appendChild(document.createTextNode(this.DICTIONARY.Get("back to summary")));
    this.DoneButton.disabled = false;
}

Quiz.prototype.ConfirmOK = function () {

}

Quiz.prototype.ConfirmCancel = function () {

}

Quiz.prototype.ConfirmClose = function () {
    if (this.FramedWindow) {
        $.fancybox.close();
    }
    this.FramedWindow = null;
}

Quiz.prototype.Confirm = function (h, m) {
    $.fancybox.open({
        type: "iframe",
        href: "./confirm.html?ok=" + encodeURIComponent(this.DICTIONARY.Get("ok")) + "&cancel=" + encodeURIComponent(this.DICTIONARY.Get("cancel")) + "&heading=" + encodeURIComponent(h) + "&message=" + encodeURIComponent(m),
        width: 600,
        height: 200,
        maxHeight: 200,
        modal: false
    });

    // assign the window
    this.FramedWindow = $("iframe").get(0);

}

Quiz.prototype.Alert = function (h, m) {
    $.fancybox.open({
        type: "iframe",
        href: "./alert.html?ok=" + encodeURIComponent(this.DICTIONARY.Get("ok")) + "&cancel=" + encodeURIComponent(this.DICTIONARY.Get("cancel")) + "&heading=" + encodeURIComponent(h) + "&message=" + encodeURIComponent(m),
        width: 600,
        height: 200,
        maxHeight: 200,
        modal: false
    });

    // assign the window
    this.FramedWindow = $("iframe").get(0);

}

Quiz.prototype.AlertClose = function () {
    if (this.FramedWindow) {
        $.fancybox.close();
    }
    this.FramedWindow = null;
}


/*

QUESTION OBJECT

*/
Question = function (j, nodeRoot, quiz) {

    this.FEEDBACK_MODES = {
        FINAL: "final",
        EVERY: "every"
    }

    this.QUESTION_TYPES = {
        FILL_IN: "fill-in",
        TRUE_FALSE: "true-false",
        MC_MULTIPLE: "multiple-choice-ms",
        MC_SINGLE: "multiple-choice-ss",
        LIKERT: "likert",
        ESSAY: "long-fill-in"
    }

    this.RESPONSE_STATES = {
        NOT_ANSWERED: "not attempted",
        CORRECT: "correct",
        WRONG: "incorrect",
        PARTIAL: "partial",
        NEUTRAL: "neutral"
    }

    this.dataModel = j;
    this.nodeRoot = nodeRoot;
    nodeRoot.object = this;
    nodeRoot.id = this.dataModel.QuestionId;
    nodeRoot.className = "questionContainer";
    this.Quiz = quiz; //Quiz object

    // get the index of this question
    this.Index = this.Quiz.Questions.length;

    this.Choices = [];

    // changable variables
    this.AttemptsUsed = 0;
    this.IsSubmitted = false;
    this.LearnerResponse = "";
    this.LearnerResult = this.RESPONSE_STATES.NOT_ANSWERED;
    this.AllDisabled = false;

    this.CorrectChoiceCount = 0;
    this.QuestionDetails = null;
    var defaultIdx = -1;

    // get the details in proper language (default if language not found)

    for (var i = 0; i < this.dataModel.QuestionDetails.length; i++) {
        if (this.dataModel.QuestionDetails[i].Language == this.Quiz.DefaultLanguage) {
            defaultIdx = i;
        }

        if (this.dataModel.QuestionDetails[i].Language == this.Quiz.Language) {
            this.QuestionDetails = this.dataModel.QuestionDetails[i];
        }
    }

    if (this.QuestionDetails == null) {
        this.QuestionDetails = this.dataModel.QuestionDetails[defaultIdx];
    }

    // question text

    var heading = document.createElement("div");
    heading.className = "heading";
    heading.appendChild(document.createTextNode(this.Quiz.DICTIONARY.Get("question") + " #" + String(this.Quiz.Questions.length + 1)))

    // add the page number now that we know the total
    var pn = document.createElement("span");
    pn.appendChild(document.createTextNode(String(this.Quiz.Questions.length + 1) + " / " + this.Quiz.dataModel.Questions.length));

    heading.appendChild(pn);
    nodeRoot.appendChild(heading);

    var text = document.createElement("div");
    text.innerHTML = this.QuestionDetails.QuestionHtml;
    nodeRoot.appendChild(text);

    // feedback block

    this.Feedback = new Feedback(document.createElement("div"), this);
    nodeRoot.appendChild(this.Feedback.nodeRoot);

    // choices

    this.nodeChoicesContainer = document.createElement("div")
    this.nodeChoicesContainer.id = "";

    switch (this.dataModel.QuestionType) {
        case this.QUESTION_TYPES.LIKERT:
            this.nodeChoicesContainer.className = "choicesContainer likertContainer";
            break;
        case this.QUESTION_TYPES.ESSAY:
            this.nodeChoicesContainer.className = "choicesContainer essayContainer";
            break;
        default:
            this.nodeChoicesContainer.className = "choicesContainer";
            break;
    }

    switch (this.dataModel.QuestionType) {
        case this.QUESTION_TYPES.MC_MULTIPLE: //do not reorder these switch options
            this.IsMultipleAnswer = true;
        case this.QUESTION_TYPES.LIKERT: //do not reorder these switch options
        case this.QUESTION_TYPES.TRUE_FALSE: //do not reorder these switch options
        case this.QUESTION_TYPES.MC_SINGLE: //do not reorder these switch options

            // build the choices

            for (var i in this.QuestionDetails.Choices) {

                //create the choice node
                var nodeChoice = document.createElement("div");
                nodeChoice.className = "choiceContainer";

                //build the choice object
                this.Choices.push(new Choice(this.QuestionDetails.Choices[i], nodeChoice, this));

                //attach the choice
                this.nodeChoicesContainer.appendChild(nodeChoice);

                // tally the number of correct options
                if (this.Choices[this.Choices.length - 1].dataModel.IsCorrect) {
                    this.CorrectChoiceCount++;
                }
            }

            // attach the choices collection
            this.nodeRoot.appendChild(this.nodeChoicesContainer);
            break;

        case this.QUESTION_TYPES.FILL_IN:

            //create the choice node, this is really not used on page for a fill-in
            var nodeChoice = document.createElement("div");
            nodeChoice.className = "choiceContainer";

            // build the choices
            for (var i in this.QuestionDetails.Choices) {
                //build the choice object
                this.Choices.push(new Choice(this.QuestionDetails.Choices[i], nodeChoice, this));
            }

            // attach the text input node
            this.nodeRoot.appendChild(new TextInteraction(this));
            break;

        case this.QUESTION_TYPES.ESSAY:

            //attach the choice
            this.nodeChoicesContainer.appendChild(new EssayInteraction(this));

            // attach the choices collection
            this.nodeRoot.appendChild(this.nodeChoicesContainer);
            break;
    }

    if (this.Quiz.dataModel.ObjectType == this.Quiz.TYPES.QUIZ) { // submit only attached to quizzes

        // submit navigation
        this.nodeSubmitContainer = document.createElement("div");
        this.nodeSubmitContainer.className = "submitContainer";

        // attach the submit button
        this.SubmitButton = new SubmitInteraction("", this.Quiz.DICTIONARY.Get("submit"), this);
        this.nodeSubmitContainer.appendChild(this.SubmitButton);

        // attach the submit
        this.nodeRoot.appendChild(this.nodeSubmitContainer);

    }

}

Question.prototype.GetLearnerReponseFromInputs = function () {

    var r = "";

    switch (this.dataModel.QuestionType) {
        case this.QUESTION_TYPES.MC_MULTIPLE: //do not reorder these switch options
        case this.QUESTION_TYPES.LIKERT: //do not reorder these switch options
        case this.QUESTION_TYPES.TRUE_FALSE: //do not reorder these switch options
        case this.QUESTION_TYPES.MC_SINGLE: //do not reorder these switch options
            for (var i = 0; i < this.Choices.length; i++) {
                if (this.Choices[i].nodeRoot.childNodes[0].checked) {
                    r = r + "|" + this.Choices[i].nodeRoot.childNodes[1].innerHTML;
                }
            }
            return r.substring(1);
            break;
        case this.QUESTION_TYPES.ESSAY: //do not reorder these switch options
        case this.QUESTION_TYPES.FILL_IN: //do not reorder these switch options
            return this.TextInput.value;
            break;
    }
}

Question.prototype.DoFeedback = function (doAsFinal) {

    // no feedback on surveys

    if (this.Quiz.dataModel.ObjectType == this.Quiz.TYPES.SURVEY) {
        return true;
    }

    // if last attempt or answered correctly or quiz finalized, DISABLE 
    if ((this.AttemptsUsed >= this.dataModel.MaxAttempts) || (this.LearnerResult == this.RESPONSE_STATES.CORRECT) || doAsFinal == true) {

        // disable all inputs
        this.DisableAll();

        // feedback
        switch (this.LearnerResult) {
            case this.RESPONSE_STATES.CORRECT:

                if (this.QuestionDetails.CorrectFeedback != null) {
                    this.Feedback.Show("", this.QuestionDetails.CorrectFeedback, this.LearnerResult);
                }

                break;
            case this.RESPONSE_STATES.WRONG:

                if (this.QuestionDetails.WrongFeedback != null) {
                    this.Feedback.Show("", this.QuestionDetails.WrongFeedback, this.LearnerResult);
                }

                break;
            case this.RESPONSE_STATES.PARTIAL:
                if (this.QuestionDetails.PartialFeedback != null) {
                    this.Feedback.Show("", this.QuestionDetails.PartialFeedback, this.LearnerResult);
                } else {
                    if (this.QuestionDetails.WrongFeedback != null) {
                        this.Feedback.Show("", this.QuestionDetails.WrongFeedback, this.RESPONSE_STATES.WRONG);
                    }
                }
                break;
        }

    } else {

        // can only be wrong here
        switch (this.LearnerResult) {
            case this.RESPONSE_STATES.WRONG:

                if (this.QuestionDetails.TryAgainWrongFeedback != null) {
                    this.Feedback.Show("", this.QuestionDetails.TryAgainWrongFeedback, this.LearnerResult);
                } else {
                    this.Feedback.Show("", this.QuestionDetails.WrongFeedback, this.RESPONSE_STATES.WRONG);
                }

                break;
            case this.RESPONSE_STATES.PARTIAL:
                if (this.QuestionDetails.TryAgainPartialFeedback != null) {
                    this.Feedback.Show("", this.QuestionDetails.TryAgainPartialFeedback, this.LearnerResult);
                } else if (this.QuestionDetails.PartialFeedback != null) {
                    this.Feedback.Show("", this.QuestionDetails.PartialFeedback, this.LearnerResult);
                } else {
                    this.Feedback.Show("", this.QuestionDetails.WrongFeedback, this.RESPONSE_STATES.WRONG);
                }
                break;
        }
    }
}

/*

returns TRUE if the user has not clicked any input/interaction for the question
returns FALSE if the user has clicked input

*/
Question.prototype.IsNothingInput = function () {

    var b = true;

    switch (this.dataModel.QuestionType) {
        case this.QUESTION_TYPES.ESSAY:
        case this.QUESTION_TYPES.FILL_IN:
            if (this.TextInput.value.length > 0) {
                b = false;
            }
            break;
        case this.QUESTION_TYPES.LIKERT:
        case this.QUESTION_TYPES.TRUE_FALSE:
        case this.QUESTION_TYPES.MC_MULTIPLE:
        case this.QUESTION_TYPES.MC_SINGLE:
            for (var i = 0; i < this.Choices.length; i++) {
                if (this.Choices[i].nodeRoot.childNodes[0].object.IsChecked()) {
                    b = false;
                }
            }
            break;
    }

    return b;
}

/*

We need this to avoid the circular structure error if we JSON the native object.

*/

Question.prototype.SaveStateToObject = function () {

    var o = new Object;

    o.AttemptsUsed = this.AttemptsUsed;
    o.LearnerResponse = this.LearnerResponse;
    o.LearnerResult = this.LearnerResult;
    o.IsSubmitted = this.IsSubmitted;
    o.AllDisabled = this.AllDisabled;

    return o;

}

Question.prototype.RestoreStateFromObject = function (object) {

    try { this.AttemptsUsed = object.AttemptsUsed; } catch (e) { }
    try { this.LearnerResponse = object.LearnerResponse; } catch (e) { }
    try { this.LearnerResult = object.LearnerResult; } catch (e) { }
    try { this.IsSubmitted = object.IsSubmitted; } catch (e) { }
    try { this.AllDisabled = object.AllDisabled; } catch (e) { }

    // restore learner response

    this.SetInputsFromLearnerResponseValue(this.LearnerResponse);

    // feedback

    this.DoFeedback();

    // disable inputs

    if (this.AllDisabled) {
        this.DisableAll();
    }

}

Question.prototype.PostToSCORMAPI = function () {

    // find the scorm index

    var idx = 0;
    var i = 0;

    if (this.Quiz.SCORM.JournalingOn) {

        idx = this.Quiz.SCORM.GetValue("cmi.interactions._count");

    } else {

        // search for the latest matching interaction identifier
        for (i = this.Quiz.SCORM.GetValue("cmi.interactions._count") ; i >= 0; i--) {
            if (this.Quiz.SCORM.GetValue("cmi.interactions." + i + ".id") == this.dataModel.QuestionId) {
                if (idx == 0) { // set it only once
                    idx = i;
                }
            }
        }

        // if not found, append
        if (idx == 0) {
            idx = this.Quiz.SCORM.GetValue("cmi.interactions._count");
        }

    }

    // identifier
    this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".id", this.dataModel.QuestionId);

    //type
    switch (this.dataModel.QuestionType) {
        case this.QUESTION_TYPES.MC_MULTIPLE:
        case this.QUESTION_TYPES.MC_SINGLE:
            this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".type", "choice"); // multiple choice
            break;
        default:
            this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".type", this.dataModel.QuestionType);
            break;
    }

    //description
    this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".description", this.dataModel.QuestionDescription);

    //timestamp
    //this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".timestamp", this.dataModel.QuestionId);

    //correct_responses

    //weighting
    //this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".weighting", this.dataModel.QuestionId);

    //learner_response
    this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".learner_response", this.LearnerResponse);

    //result
    switch (this.LearnerResult) {
        case this.RESPONSE_STATES.CORRECT:
            this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".result", this.RESPONSE_STATES.CORRECT);
            break;
        case this.RESPONSE_STATES.NEUTRAL:
            this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".result", this.RESPONSE_STATES.NEUTRAL);
            break;
        default:
            this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".result", this.RESPONSE_STATES.WRONG);
            break;
    }

    //latency
    //this.Quiz.SCORM.SetValue("cmi.interactions." + idx + ".latency", this.dataModel.QuestionId);

    // suspend data - (attempts)
    var o = new Object;
    o.AttemptsUsed = this.AttemptsUsed;

    try {
        var suspend = JSON.parse(this.Quiz.SCORM.GetValue("cmi.suspend_data"));
        // if the API is not connected, false is returned so lets check for that
        if (suspend == false) {
            suspend = new Object;
            suspend.Interactions = [];
        }
    } catch (e) {
        var suspend = new Object;
        suspend.Interactions = [];
    }

    suspend.Interactions[idx] = o;

    this.Quiz.SCORM.SetValue("cmi.suspend_data", JSON.stringify(suspend));

    // commit

    this.Quiz.SCORM.Commit();

}

Question.prototype.LoadStateFromSCORMAPI = function () {


}

Question.prototype.Evaluate = function () {

    // store the learner response
    this.LearnerResponse = this.GetLearnerReponseFromInputs();

    switch (this.dataModel.QuestionType) {
        case this.QUESTION_TYPES.ESSAY:
            this.LearnerResult = this.EvaluateEssay();
            break;
        case this.QUESTION_TYPES.FILL_IN:
            this.LearnerResult = this.EvaluateFillIn();
            break;
        case this.QUESTION_TYPES.LIKERT:
            this.LearnerResult = this.EvaluateLikert();
            break;
        case this.QUESTION_TYPES.MC_MULTIPLE:
        case this.QUESTION_TYPES.TRUE_FALSE:
        case this.QUESTION_TYPES.MC_SINGLE:
            this.LearnerResult = this.EvaluateMultipleChoice();
            break;
    }

    // SCORM
    this.PostToSCORMAPI();
    this.Quiz.SaveStateToSCORMAPI();

}

Question.prototype.EvaluateEssay = function () {

    // always neutral
    return this.RESPONSE_STATES.NEUTRAL;

}

Question.prototype.EvaluateFillIn = function () {

    // if survey, result is always neutral
    if (this.Quiz.dataModel.ObjectType == this.Quiz.TYPES.SURVEY) {
        return this.RESPONSE_STATES.NEUTRAL;
    }

    var strippedInput = this.TextInput.value.replace(/[^0-9a-z]/gi, '');
    var strippedAnswer;

    for (var i = 0; i < this.Choices.length; i++) {
        if (this.Choices[i].dataModel.IsCorrect) {
            strippedAnswer = this.Choices[i].dataModel.Text.replace(/[^0-9a-z]/gi, '');
            if (strippedAnswer == strippedInput) {
                return this.RESPONSE_STATES.CORRECT;
            }
        }
    }

    return this.RESPONSE_STATES.WRONG;

}

Question.prototype.EvaluateLikert = function () {

    // if survey, result is always neutral
    if (this.Quiz.dataModel.ObjectType == this.Quiz.TYPES.SURVEY) {
        return this.RESPONSE_STATES.NEUTRAL;
    }

    var c = 0; //checked items

    // radio, checkboxes
    for (var i = 0; i < this.Choices.length; i++) {
        if (this.Choices[i].nodeRoot.childNodes[0].checked) {
            c++;
        }
    }

    // likert is always correct assuming  something is checked
    if (c > 0) {
        return this.RESPONSE_STATES.CORRECT;
    } else { // ANY wrong checks, we end up here.
        return this.RESPONSE_STATES.WRONG;
    }

}

Question.prototype.EvaluateMultipleChoice = function () {

    // if survey, result is always neutral
    if (this.Quiz.dataModel.ObjectType == this.Quiz.TYPES.SURVEY) {
        return this.RESPONSE_STATES.NEUTRAL;
    }

    var s = [];
    var c = 0; //correctly checked items
    var w = 0; //wrongly checked items

    // radio, checkboxes
    for (var i = 0; i < this.Choices.length; i++) {
        if (this.Choices[i].nodeRoot.childNodes[0].checked) {
            if (this.Choices[i].dataModel.IsCorrect == this.Choices[i].nodeRoot.childNodes[0].checked) {
                c++;
            } else {
                w++; // cannot check any wrong items
            }
        }
    }

    if (this.dataModel.CorrectAnswer_sToMatch == null) {
        this.dataModel.CorrectAnswer_sToMatch = this.CorrectChoiceCount;
    }

    // did we match the minimum required
    if (c >= this.dataModel.CorrectAnswer_sToMatch && w == 0) {
        return this.RESPONSE_STATES.CORRECT;
    } else { // ANY wrong checks, we end up here.
        if (c > 0) {
            return this.RESPONSE_STATES.PARTIAL;
        } else {
            return this.RESPONSE_STATES.WRONG;
        }
    }

}

Question.prototype.DisplayCorrectResponses = function () {

}

Question.prototype.ShowQuestionFeedback = function () {

}

Question.prototype.DisplayResult = function () {

}

Question.prototype.ClearFeedback = function () {
}

Question.prototype.ClearCorrectResponses = function () {
}

Question.prototype.ClearResult = function () {
}

Question.prototype.StartNewAttempt = function () {

    this.IsSubmitted = false;

    this.Feedback.Hide();
    this.ClearCorrectResponses();
    this.ClearResult();

}

Question.prototype.RemoveSubmitContainer = function () {

    for (var i = 0; i < this.nodeRoot.childNodes.length; i++) {
        if (this.nodeRoot.childNodes[i].className == "submitContainer") {
            this.nodeRoot.removeChild(this.nodeRoot.childNodes[i]);
        }
    }

}

/*

reverts the interface to the answer as provided by the argument

*/

Question.prototype.SetInputsFromLearnerResponseValue = function (str) {

    switch (this.dataModel.QuestionType) {
        case this.QUESTION_TYPES.MC_MULTIPLE: //do not reorder these switch options
        case this.QUESTION_TYPES.LIKERT: //do not reorder these switch options
        case this.QUESTION_TYPES.TRUE_FALSE: //do not reorder these switch options
        case this.QUESTION_TYPES.MC_SINGLE: //do not reorder these switch options

            var a = str.split("|");
            var b = false;

            for (var i = 0; i < this.Choices.length; i++) {
                b = false;
                for (var j = 0; j < a.length; j++) {
                    if (this.Choices[i].nodeRoot.childNodes[1].innerHTML == a[j]) {
                        b = true;
                    }
                }
                this.Choices[i].nodeRoot.childNodes[0].checked = b;
            }

            break;

        case this.QUESTION_TYPES.ESSAY: //do not reorder these switch options
        case this.QUESTION_TYPES.FILL_IN: //do not reorder these switch options
            this.TextInput.value = str;
            break;
    }

}

Question.prototype.DisableAll = function () {

    // disable all choices
    switch (this.dataModel.QuestionType) {
        case this.QUESTION_TYPES.LIKERT:
        case this.QUESTION_TYPES.MC_MULTIPLE:
        case this.QUESTION_TYPES.MC_SINGLE:
        case this.QUESTION_TYPES.TRUE_FALSE:
            for (var i = 0; i < this.Choices.length; i++) {
                this.Choices[i].nodeRoot.childNodes[0].disabled = true;
            }
            break;
        case this.QUESTION_TYPES.ESSAY:
        case this.QUESTION_TYPES.FILL_IN:
            this.TextInput.disabled = true;
            break;
    }

    // disable the submit
    try {
        this.SubmitButton.disabled = true;
    } catch (e) { }

    // 

    this.AllDisabled = true;

}
Question.prototype.ClearAll = function () {

    // clear all choices
    switch (this.dataModel.QuestionType) {
        case this.QUESTION_TYPES.LIKERT:
        case this.QUESTION_TYPES.MC_MULTIPLE:
        case this.QUESTION_TYPES.MC_SINGLE:
        case this.QUESTION_TYPES.TRUE_FALSE:
            for (var i = 0; i < this.Choices.length; i++) {
                this.Choices[i].nodeRoot.childNodes[0].checked = false;
            }
            break;
        case this.QUESTION_TYPES.ESSAY:
        case this.QUESTION_TYPES.FILL_IN:
            this.TextInput.value = null;
            break;
    }
}

Question.prototype.RawScorePossible = function () {

    if (this.dataModel.QuestionType == this.QUESTION_TYPES.LIKERT) {
        return 0;
    } else {
        return 1;
    }

}

Question.prototype.RawScoreEarned = function () {

    if (this.RawScorePossible() == 0) {
        return 0;
    }

    if (this.LearnerResult == this.RESPONSE_STATES.CORRECT) {
        return 1;
    } else {
        return 0;
    }

}



/*

CHOICE OBJECT

*/

Choice = function (j, nodeRoot, question) {
    this.dataModel = j;
    this.nodeRoot = nodeRoot;
    nodeRoot.object = this;
    this.Question = question;

    //build the input
    switch (this.Question.dataModel.QuestionType) {
        case this.Question.QUESTION_TYPES.FILL_IN:
            nodeRoot.appendChild(new TextInteraction(this.Question.dataModel.QuestionId + "_" + this.Question.Choices.length, "", this.Question, this));
            break;
        case this.Question.QUESTION_TYPES.TRUE_FALSE:
        case this.Question.QUESTION_TYPES.MC_MULTIPLE:
        case this.Question.QUESTION_TYPES.MC_SINGLE:
            if (this.Question.IsMultipleAnswer == true) {
                nodeRoot.appendChild(new CheckInteraction(this.Question.dataModel.QuestionId + "_" + this.Question.Choices.length, "", this.Question, this));
            } else {
                nodeRoot.appendChild(new RadioInteraction(this.Question.dataModel.QuestionId + "_" + this.Question.Choices.length, "", this.Question, this));
            }
            break;
        case this.Question.QUESTION_TYPES.LIKERT:
            nodeRoot.appendChild(new RadioInteraction(this.Question.dataModel.QuestionId + "_" + this.Question.Choices.length, "", this.Question, this));
            break;
    }

    //build the text label
    var label = document.createElement("label");
    label.setAttribute("for", this.Question.dataModel.QuestionId + "_" + this.Question.Choices.length);
    label.appendChild(document.createTextNode(this.dataModel.Text));

    nodeRoot.appendChild(label);
}


RadioInteraction = function (id, value, question, choice) {

    this.Question = question;
    this.Choice = choice;
    this.Input = document.createElement("input")

    this.Input.type = "radio";
    this.Input.id = id;
    this.Input.name = this.Question.dataModel.QuestionId;
    this.Input.object = this;
    this.Input.onclick = function () {
        this.object.Question.StartNewAttempt();
    }

    return this.Input;
}

RadioInteraction.prototype.IsChecked = function () {
    return this.Input.checked;
}

CheckInteraction = function (id, value, question, choice) {

    this.Question = question;
    this.Choice = choice;
    this.Input = document.createElement("input")

    this.Input.type = "checkbox";
    this.Input.id = id;
    this.Input.object = this;
    this.Input.onclick = function () {
        this.object.Question.StartNewAttempt();
    }

    return this.Input;
}

CheckInteraction.prototype.IsChecked = function () {
    return this.Input.checked;
}

EssayInteraction = function (question) {

    this.Question = question;

    var i = document.createElement("textarea");
    i.object = this;
    i.setAttribute("maxlength", "1024");
    i.setAttribute("rows", "5");
    i.setAttribute("style", "width:100%;");
    i.className = "essay";
    //i.setAttribute("placeholder", this.Question.Quiz.DICTIONARY.Get("essay placeholder"));
    i.onchange = function () {
        this.object.Question.StartNewAttempt();
    }
    i.onfocus = function () {
        this.object.Question.StartNewAttempt();
    }

    this.Question.TextInput = i;
    return i;

}

TextInteraction = function (question) {

    this.Question = question;

    //determine the max characters, size of the input
    var l = 0;
    for (var i in this.Question.Choices) {
        if (this.Question.Choices[i].dataModel.Text.length > l) {
            l = this.Question.Choices[i].dataModel.Text.length;
        }
    }

    // size the input appropriately
    l = (parseInt(l / 5) + 1) * 5;
    var s = "";

    switch (l) {
        case 0:
        case 5:
        case 10:
            s = "175px"
            break;
        case 15:
        case 20:
            s = "275px"
            break;
        case 25:
        case 30:
            s = "375px"
            break;
        case 35:
        case 40:
        default:
            s = "475px"
            break;
    }

    var i = document.createElement("input");
    i.type = "text";
    i.object = this;
    i.setAttribute("maxlength", l.toString());
    i.setAttribute("style", "width:" + s + ";");
    i.onchange = function () {
        this.object.Question.StartNewAttempt();
    }
    i.onfocus = function () {
        this.object.Question.StartNewAttempt();
    }

    this.Question.TextInput = i;
    return i;

}

SubmitInteraction = function (id, caption, parent) {

    this.Question = parent;

    var s = document.createElement("button");
    s.appendChild(document.createTextNode(caption));
    s.object = this;
    s.onclick = function () {
        if (this.object.Question.IsNothingInput()) {
            if (this.object.Question.dataModel.QuestionType == this.object.Question.QUESTION_TYPES.LIKERT) {
                this.object.Question.Quiz.Alert(this.object.Question.Quiz.DICTIONARY.Get("no answer likert head"), this.object.Question.Quiz.DICTIONARY.Get("no answer likert message"));
            } else {
                //this.object.Question.Quiz.CurrentQuestionIdx = this.object.Question.Index;
                this.object.Question.Quiz.ConfirmOK = function () {
                    this.Questions[this.CurrentQuestionIdx].AttemptsUsed++;
                    this.Questions[this.CurrentQuestionIdx].IsSubmitted = true;
                    this.Questions[this.CurrentQuestionIdx].Evaluate();
                    this.Questions[this.CurrentQuestionIdx].DoFeedback();
                }
                this.object.Question.Quiz.Confirm(this.object.Question.Quiz.DICTIONARY.Get("no answer submit head"), this.object.Question.Quiz.DICTIONARY.Get("no answer submit message"));
            }
        } else {
            this.object.Question.AttemptsUsed++;
            this.object.Question.IsSubmitted = true;
            this.object.Question.Evaluate();
            this.object.Question.DoFeedback();
            // unlock the next button
            try {
                if (this.object.Question.Quiz.CurrentQuestionIdx < this.object.Question.Quiz.Questions.length - 1) {
                    this.object.Question.Quiz.NextButton.disabled = false;
                }
            } catch (e) { }
        }
    }


    return s;
}

Feedback = function (nodeRoot, question) {

    this.Question = question;
    this.nodeRoot = nodeRoot;
    nodeRoot.className = "feedbackContainer off";

    this.MODES = {
        CORRECT: "correct",
        WRONG: "incorrect",
        PARTIAL: "partial",
        NOT_ANSWERED: "not attempted"

    }

    var img = document.createElement("img");
    nodeRoot.appendChild(img);
    this.Image = img;
    var title = document.createElement("div");
    title.className = "title";
    nodeRoot.appendChild(title);
    this.Title = title;
    var text = document.createElement("div");
    text.className = "text";
    nodeRoot.appendChild(text);
    this.Text = text;

}

Feedback.prototype.Show = function (title, text, mode) {

    this.nodeRoot.className = "feedbackContainer " + mode.replace(" ", "");

    // image
    this.Image.className = "staged";

    switch (mode) {
        case this.Question.RESPONSE_STATES.CORRECT:
            this.Image.setAttribute("src", "./_images/correct.png");
            break;
        case this.Question.RESPONSE_STATES.WRONG:
            this.Image.setAttribute("src", "./_images/wrong.png");
            break;
        case this.Question.RESPONSE_STATES.PARTIAL:
            this.Image.setAttribute("src", "./_images/partial.png");
            break;
    }

    try { this.Title.removeChild(this.Title.childNodes[0]); } catch (e) { }
    try { this.Text.removeChild(this.Text.childNodes[0]); } catch (e) { }

    this.Image.className = "popin";
    this.Image.id = "fbimg_" + this.Question.nodeRoot.id;

    if (title == "") {
        this.Title.setAttribute("style", "display:none;");
    } else {
        this.Title.setAttribute("style", "");
    }
    //this.Title.appendChild(document.createTextNode(title));

    this.Text.appendChild(document.createTextNode(text));

    // remove the class animation
    setTimeout("try{document.getElementById(\"fbimg_" + this.Question.nodeRoot.id + "\").className = \"\"}catch(e){};", 250);

}

Feedback.prototype.Hide = function () {

    this.nodeRoot.className = this.nodeRoot.className + " hide";

    //try{this.Title.removeChild(this.Title.childNodes[0]);} catch(e){}
    //try{this.Text.removeChild(this.Text.childNodes[0]);} catch(e){}

}

/*

RESULTS OBJECT 

*/

Results = function (nodeRoot, quiz) {

    this.nodeRoot = nodeRoot;
    nodeRoot.object = this;
    nodeRoot.className = "results";
    this.Quiz = quiz; //Quiz object

    this.IsFinal = false; // true when results have been tallied.

    // heading

    var heading = document.createElement("div");
    heading.className = "heading";
    heading.appendChild(document.createTextNode(this.Quiz.DICTIONARY.Get("summary")));

    nodeRoot.appendChild(heading);

    // summary text
    var defaultSummaryIdx;
    var summaryString;

    for (var i = 0; i < this.Quiz.dataModel.Summaries.length; i++) {
        if ((this.Quiz.dataModel.Summaries[i].Language == this.Quiz.Language) || (this.Quiz.dataModel.Summaries[i].Language == this.DefaultLanguage && summaryString == "")) {
            if (this.Quiz.dataModel.Summaries[i].Value != "") {
                summaryString = this.Quiz.dataModel.Summaries[i].Value;
            } else {
                summaryString = this.Quiz.DICTIONARY.Get("summary text");
            }
        }
    }

    var summaryText = document.createElement("div");
    summaryText.className = "summaryText";
    summaryText.appendChild(document.createTextNode(summaryString));

    // stats container 
    var statsContainer = document.createElement("div");
    statsContainer.className = "statsContainer";

    //img
    this.ResultsImage = document.createElement("img");
    this.ResultsImage.className = "";
    this.ResultsImage.setAttribute("src", "./_images/wrong.png");
    statsContainer.appendChild(this.ResultsImage);

    //your score
    var score = document.createElement("div");
    score.className = "your";
    score.appendChild(document.createTextNode(this.Quiz.DICTIONARY.Get("your score")));
    statsContainer.appendChild(score);

    //pct
    this.pct = document.createElement("div");
    this.pct.className = "pct";
    statsContainer.appendChild(this.pct);

    //counter
    var count = document.createElement("div");

    this.CorrectCount = document.createElement("span");
    this.TotalCount = document.createElement("span");

    count.appendChild(this.CorrectCount);
    count.appendChild(document.createTextNode("/"));
    count.appendChild(this.TotalCount);
    count.appendChild(document.createTextNode(" " + this.Quiz.DICTIONARY.Get("correct")));

    count.className = "raw";
    statsContainer.appendChild(count);

    //passing
    this.ScaledPassingScore = document.createElement("span");

    var sps = document.createElement("div");
    sps.appendChild(this.ScaledPassingScore);
    sps.appendChild(document.createTextNode(" " + this.Quiz.DICTIONARY.Get("required to pass").toLowerCase()));
    sps.className = "sps";
    statsContainer.appendChild(sps);

    //congrats
    var summaryContainer = document.createElement("div");
    summaryContainer.className = "summaryContainer";

    this.SummaryBrief = document.createElement("span");
    this.SummaryBrief.className = "summaryBrief";

    this.SummaryLong = document.createElement("span");
    this.SummaryLong.className = "summaryLong";

    summaryContainer.appendChild(this.SummaryBrief);
    summaryContainer.appendChild(this.SummaryLong);

    statsContainer.appendChild(summaryContainer);

    switch (this.Quiz.dataModel.ObjectType) {
        case this.Quiz.TYPES.SURVEY:
            nodeRoot.appendChild(summaryText);
            break;
        case this.Quiz.TYPES.QUIZ:
            if (this.Quiz.dataModel.ShowFinalScore) {
                nodeRoot.appendChild(statsContainer);
            } else {
                nodeRoot.appendChild(summaryText);
            }
            break;
    }

    // navigation
    var summaryNavContainer = document.createElement("div");
    summaryNavContainer.className = "summaryNavigationContainer";

    this.ReviewButton = document.createElement("button");
    this.ReviewButton.parent = this;
    this.ReviewButton.onclick = function () {
        this.parent.Quiz.Review();
    }
    this.ReviewButton.appendChild(document.createTextNode(this.Quiz.DICTIONARY.Get("review")));

    this.ExitButton = document.createElement("button");
    this.ExitButton.parent = this;
    this.ExitButton.onclick = function () {
        this.parent.Quiz.Exit();
    }
    this.ExitButton.appendChild(document.createTextNode(this.Quiz.DICTIONARY.Get("close")));


    if (this.Quiz.dataModel.AllowReview == true)
        summaryNavContainer.appendChild(this.ReviewButton);

    summaryNavContainer.appendChild(this.ExitButton);

    nodeRoot.appendChild(summaryNavContainer);



}

/*

LANGUAGE OBJECT

*/

//LANGUAGE["en-US"]["tag"];

Dictionary = function (l) {

    this.ActiveLanguage = l;

    this.Phrases = {
        "default": {
            "ok": "OK",
            "cancel": "Cancel",
            "submit": "Submit",
            "submitted": "Submitted",
            "next": "Next",
            "back": "Back",
            "question": "Question",
            "review": "Review",
            "close": "Close",
            "exit": "Exit",
            "done": "Done",
            "your score": "Your Score:",
            "required to pass": "Required to pass.",
            "correct": "Correct",
            "congratulations-passed": "Congratulations, you passed!",
            "sorry-failed": "So sorry. You did not pass.",
            "results": "Results",
            "summary": "Summary",
            "summary text": "Thank you for your time. We appreciate you answering these questions.",
            "back to summary": "Summary",
            "exit continue head": "Exit?",
            "exit continue message": "Click 'OK' to exit.",
            "exit resume warning head": "Exit?",
            "exit resume warning message": "Are you sure? You have not finished and will have to restart this quiz from the beginning.",
            "no answer submit head": "Submit?",
            "no answer submit message": "You haven't entered an answer. Are you sure you want to submit?",
            "no answer likert head": "Please select a response.",
            "no answer likert message": "This question requires a response.",
            "no answer continue head": "Continue?",
            "no answer continue message": "You haven't submitted your answer yet. Are you sure you want to continue?",
            "end quiz ua message": "You have not answered all questions! This will END your quiz! Are you sure you want to continue?",
            "end quiz head": "End Quiz?",
            "end quiz message": "This will END your quiz! Are you sure you want to continue?",
            "end survey ua message": "You have not answered all questions! This will END your survey! Are you sure you want to continue?",
            "end survey head": "End Survey?",
            "end survey message": "This will END your survey! Are you sure you want to continue?",
            "submit survey ua message": "You have not answered all questions! Are you sure you want to continue?",
            "submit survey head": "Submit Survey?",
            "submit survey message": "This will submit your survey. Are you sure you want to continue?"
        },
        "en-AU": {
            "ok": "OK",
            "cancel": "Cancel",
            "submit": "Submit",
            "submitted": "Submitted",
            "next": "Next",
            "back": "Back",
            "question": "Question",
            "review": "Review",
            "close": "Close",
            "exit": "Exit",
            "done": "Done",
            "your score": "Your Score:",
            "required to pass": "Required to pass.",
            "correct": "Correct",
            "congratulations-passed": "Congratulations, you passed!",
            "sorry-failed": "So sorry. You did not pass.",
            "results": "Results",
            "summary": "Summary",
            "summary text": "Thank you for your time. We appreciate you answering these questions.",
            "back to summary": "Summary",
            "exit continue head": "Exit?",
            "exit continue message": "Click 'OK' to exit.",
            "exit resume warning head": "Exit?",
            "exit resume warning message": "Are you sure? You have not finished and will have to restart this quiz from the beginning.",
            "no answer submit head": "Submit?",
            "no answer submit message": "You haven't entered an answer. Are you sure you want to submit?",
            "no answer likert head": "Please select a response.",
            "no answer likert message": "This question requires a response.",
            "no answer continue head": "Continue?",
            "no answer continue message": "You haven't submitted your answer yet. Are you sure you want to continue?",
            "end quiz ua message": "You have not answered all questions! This will END your quiz! Are you sure you want to continue?",
            "end quiz head": "End Quiz?",
            "end quiz message": "This will END your quiz! Are you sure you want to continue?",
            "end survey ua message": "You have not answered all questions! This will END your survey! Are you sure you want to continue?",
            "end survey head": "End Survey?",
            "end survey message": "This will END your survey! Are you sure you want to continue?",
            "submit survey ua message": "You have not answered all questions! Are you sure you want to continue?",
            "submit survey head": "Submit Survey?",
            "submit survey message": "This will submit your survey. Are you sure you want to continue?"
        },
        "en-GB": {
            "ok": "OK",
            "cancel": "Cancel",
            "submit": "Submit",
            "submitted": "Submitted",
            "next": "Next",
            "back": "Back",
            "question": "Question",
            "review": "Review",
            "close": "Close",
            "exit": "Exit",
            "done": "Done",
            "your score": "Your Score:",
            "required to pass": "Required to pass.",
            "correct": "Correct",
            "congratulations-passed": "Congratulations, you passed!",
            "sorry-failed": "So sorry. You did not pass.",
            "results": "Results",
            "summary": "Summary",
            "summary text": "Thank you for your time. We appreciate you answering these questions.",
            "back to summary": "Summary",
            "exit continue head": "Exit?",
            "exit continue message": "Click 'OK' to exit.",
            "exit resume warning head": "Exit?",
            "exit resume warning message": "Are you sure? You have not finished and will have to restart this quiz from the beginning.",
            "no answer submit head": "Submit?",
            "no answer submit message": "You haven't entered an answer. Are you sure you want to submit?",
            "no answer likert head": "Please select a response.",
            "no answer likert message": "This question requires a response.",
            "no answer continue head": "Continue?",
            "no answer continue message": "You haven't submitted your answer yet. Are you sure you want to continue?",
            "end quiz ua message": "You have not answered all questions! This will END your quiz! Are you sure you want to continue?",
            "end quiz head": "End Quiz?",
            "end quiz message": "This will END your quiz! Are you sure you want to continue?",
            "end survey ua message": "You have not answered all questions! This will END your survey! Are you sure you want to continue?",
            "end survey head": "End Survey?",
            "end survey message": "This will END your survey! Are you sure you want to continue?",
            "submit survey ua message": "You have not answered all questions! Are you sure you want to continue?",
            "submit survey head": "Submit Survey?",
            "submit survey message": "This will submit your survey. Are you sure you want to continue?"
        },
        "en-US": {
            "ok": "OK",
            "cancel": "Cancel",
            "submit": "Submit",
            "submitted": "Submitted",
            "next": "Next",
            "back": "Back",
            "question": "Question",
            "review": "Review",
            "close": "Close",
            "exit": "Exit",
            "done": "Done",
            "your score": "Your Score:",
            "required to pass": "Required to pass.",
            "correct": "Correct",
            "congratulations-passed": "Congratulations, you passed!",
            "sorry-failed": "So sorry. You did not pass.",
            "results": "Results",
            "summary": "Summary",
            "summary text": "Thank you for your time. We appreciate you answering these questions.",
            "back to summary": "Summary",
            "exit continue head": "Exit?",
            "exit continue message": "Click 'OK' to exit.",
            "exit resume warning head": "Exit?",
            "exit resume warning message": "Are you sure? You have not finished and will have to restart this quiz from the beginning.",
            "no answer submit head": "Submit?",
            "no answer submit message": "You haven't entered an answer. Are you sure you want to submit?",
            "no answer likert head": "Please select a response.",
            "no answer likert message": "This question requires a response.",
            "no answer continue head": "Continue?",
            "no answer continue message": "You haven't submitted your answer yet. Are you sure you want to continue?",
            "end quiz ua message": "You have not answered all questions! This will END your quiz! Are you sure you want to continue?",
            "end quiz head": "End Quiz?",
            "end quiz message": "This will END your quiz! Are you sure you want to continue?",
            "end survey ua message": "You have not answered all questions! This will END your survey! Are you sure you want to continue?",
            "end survey head": "End Survey?",
            "end survey message": "This will END your survey! Are you sure you want to continue?",
            "submit survey ua message": "You have not answered all questions! Are you sure you want to continue?",
            "submit survey head": "Submit Survey?",
            "submit survey message": "This will submit your survey. Are you sure you want to continue?"
        },
        "es": {
            "ok": "Aceptar",
            "cancel": "Cancelar",
            "submit": "Enviar",
            "submitted": "Presentada",
            "next": "Siguiente",
            "back": "Espalda",
            "question": "Pregunta",
            "review": "Revisión",
            "close": "Cerca",
            "exit": "Salida",
            "done": "Hecho",
            "your score": "Tu Puntuación:",
            "required to pass": "Requerido para pasar.",
            "correct": "Correcto",
            "congratulations-passed": "¡Felicidades, pasaste!",
            "sorry-failed": "Lo siento mucho. No pasaste.",
            "results": "Resultados",
            "summary": "Resumen",
            "summary text": "Gracias por tu tiempo. Le agradecemos que responda estas preguntas.",
            "back to summary": "Resumen",
            "exit continue head": "¿Salida?",
            "exit continue message": "Haga clic en 'Aceptar' para salir.",
            "exit resume warning head": "¿Salida?",
            "exit resume warning message": "¿Estás seguro? No ha terminado y tendrá que reiniciar esta prueba desde el principio.",
            "no answer submit head": "¿Enviar?",
            "no answer submit message": "No has ingresado una respuesta. ¿Estás seguro que quieres enviar?",
            "no answer likert head": "Por favor seleccione una respuesta.",
            "no answer likert message": "Esta pregunta requiere una respuesta.",
            "no answer continue head": "¿Continuar?",
            "no answer continue message": "Aún no ha enviado su respuesta. Estás seguro de que quieres continuar?",
            "end quiz ua message": "¡No has respondido todas las preguntas! ¡Esto terminará tu prueba! Estás seguro de que quieres continuar?",
            "end quiz head": "Terminar cuestionario?",
            "end quiz message": "¡Esto terminará tu prueba! Estás seguro de que quieres continuar?",
            "end survey ua message": "¡No has respondido todas las preguntas! ¡Esto terminará su encuesta! Estás seguro de que quieres continuar?",
            "end survey head": "Terminar encuesta?",
            "end survey message": "¡Esto terminará su encuesta! Estás seguro de que quieres continuar?",
            "submit survey ua message": "¡No has contestado todas las preguntas! Estás seguro de que quieres continuar?",
            "submit survey head": "¿Enviar Encuesta?",
            "submit survey message": "Esto enviará su encuesta. Estás seguro de que quieres continuar?"
        },
        "es-419": {
            "ok": "Aceptar",
            "cancel": "Cancelar",
            "submit": "Submit",
            "submitted": "Submitted",
            "next": "Next",
            "back": "Back",
            "question": "Question",
            "review": "Review",
            "close": "Close",
            "exit": "Exit",
            "done": "Done",
            "your score": "Your Score:",
            "required to pass": "Required to pass.",
            "correct": "Correct",
            "congratulations-passed": "Congratulations, you passed!",
            "sorry-failed": "So sorry. You did not pass.",
            "results": "Results",
            "summary": "Summary",
            "summary text": "Thank you for your time. We appreciate you answering these questions.",
            "back to summary": "Summary",
            "exit continue head": "Exit?",
            "exit continue message": "Click 'OK' to exit.",
            "exit resume warning head": "Exit?",
            "exit resume warning message": "Are you sure? You have not finished and will have to restart this quiz from the beginning.",
            "no answer submit head": "Submit?",
            "no answer submit message": "You haven't entered an answer. Are you sure you want to submit?",
            "no answer likert head": "Please select a response.",
            "no answer likert message": "This question requires a response.",
            "no answer continue head": "Continue?",
            "no answer continue message": "You haven't submitted your answer yet. Are you sure you want to continue?",
            "end quiz ua message": "You have not answered all questions! This will END your quiz! Are you sure you want to continue?",
            "end quiz head": "End Quiz?",
            "end quiz message": "This will END your quiz! Are you sure you want to continue?",
            "end survey ua message": "You have not answered all questions! This will END your survey! Are you sure you want to continue?",
            "end survey head": "End Survey?",
            "end survey message": "This will END your survey! Are you sure you want to continue?",
            "submit survey ua message": "¡No has contestado todas las preguntas! Estás seguro de que quieres continuar?",
            "submit survey head": "¿Enviar Encuesta?",
            "submit survey message": "Esto enviará su encuesta. Estás seguro de que quieres continuar?"
        },
        "de": {
            "ok": "OK",
            "cancel": "Stornieren",
            "submit": "Absenden",
            "submitted": "Eingereicht",
            "next": "Nächster",
            "back": "Zurück",
            "question": "Anfrage",
            "review": "Rezension",
            "close": "Schließen",
            "exit": "Ausgang",
            "done": "Erledigt",
            "your score": "Ihr Ergebnis:",
            "required to pass": "Muss übergeben werden.",
            "correct": "Richtig",
            "congratulations-passed": "Glückwunsch, du hast bestanden!",
            "sorry-failed": "So leid. Du hast nicht bestanden.",
            "results": "Ergebnisse",
            "summary": "Zusammenfassung",
            "summary text": "Vielen Dank für Ihre Zeit. Wir freuen uns, dass Sie diese Fragen beantwortet haben.",
            "back to summary": "Zusammenfassung",
            "exit continue head": "Ausgang?",
            "exit continue message": "Klicken Sie zum Beenden auf 'OK'.",
            "exit resume warning head": "Ausgang?",
            "exit resume warning message": "Bist du sicher? Sie sind noch nicht fertig und müssen dieses Quiz von Anfang an neu starten.",
            "no answer submit head": "Einreichen?",
            "no answer submit message": "Sie haben keine Antwort eingegeben. Sind Sie sicher, dass Sie einreichen möchten?",
            "no answer likert head": "Bitte wählen Sie eine Antwort aus.",
            "no answer likert message": "Diese Frage erfordert eine Antwort.",
            "no answer continue head": "Fortsetzen?",
            "no answer continue message": "Sie haben Ihre Antwort noch nicht eingereicht. Bist du dir sicher, dass du weitermachen willst?",
            "end quiz ua message": "Du hast nicht alle Fragen beantwortet! Dies wird dein Quiz beenden! Bist du dir sicher, dass du weitermachen willst?",
            "end quiz head": "Ende Quiz?",
            "end quiz message": "Dies wird dein Quiz beenden! Bist du dir sicher, dass du weitermachen willst?",
            "end survey ua message": "Du hast nicht alle Fragen beantwortet! Dies beendet Ihre Umfrage! Bist du dir sicher, dass du weitermachen willst?",
            "end survey head": "Umfrage beenden?",
            "end survey message": "Dies beendet Ihre Umfrage! Bist du dir sicher, dass du weitermachen willst?",
            "submit survey ua message": "Sie haben nicht alle Fragen beantwortet! Sind Sie sicher, dass Sie fortfahren möchten?",
            "submit survey head": "Umfrage abschicken?",
            "submit survey message": "Dies wird Ihre Umfrage einreichen. Sind Sie sicher, dass Sie fortfahren möchten?"
        },
        "it": {
            "ok": "OK",
            "cancel": "Annulla",
            "submit": "Invia",
            "submitted": "Inserito",
            "next": "Il prossimo",
            "back": "Indietro",
            "question": "Domanda",
            "review": "Revisione",
            "close": "Vicino",
            "exit": "Uscita",
            "done": "Fatto",
            "your score": "Il tuo punteggio:",
            "required to pass": "Richiesto per passare.",
            "correct": "Corretta",
            "congratulations-passed": "Congratulazioni, sei passato!",
            "sorry-failed": "Mi dispiace. Non avete superato.",
            "results": "Risultati",
            "summary": "Sommario",
            "summary text": "Grazie per il tuo tempo. Ti ringraziamo per aver risposto a queste domande.",
            "back to summary": "Sommario",
            "exit continue head": "Uscita?",
            "exit continue message": "Fai clic su 'OK' per uscire.",
            "exit resume warning head": "Uscita?",
            "exit resume warning message": "Sei sicuro? Non hai finito e dovrai riavviare questo quiz dall'inizio.",
            "no answer submit head": "Invia?",
            "no answer submit message": "Non hai inserito una risposta. Sei sicuro di voler inviare?",
            "no answer likert head": "Si prega di selezionare una risposta.",
            "no answer likert message": "Questa domanda richiede una risposta.",
            "no answer continue head": "Continua?",
            "no answer continue message": "Non hai ancora inviato la tua risposta. Sei sicuro di voler continuare?",
            "end quiz ua message": "Non hai risposto a tutte le domande! Questo finirà il tuo quiz! Sei sicuro di voler continuare?",
            "end quiz head": "Fine Quiz?",
            "end quiz message": "Questo finirà il tuo quiz! Sei sicuro di voler continuare?",
            "end survey ua message": "Non hai risposto a tutte le domande! Questo finirà il tuo sondaggio! Sei sicuro di voler continuare?",
            "end survey head": "Fine sondaggio?",
            "end survey message": "Questo finirà il tuo sondaggio! Sei sicuro di voler continuare?",
            "submit survey ua message": "Non hai risposto a tutte le domande! Sei sicuro di voler continuare?",
            "submit survey head": "Invia sondaggio?",
            "submit survey message": "Questo invierà il tuo sondaggio. Sei sicuro di voler continuare?"
        },
        "fr": {
            "ok": "OK",
            "cancel": "Annuler",
            "submit": "Envoyer",
            "submitted": "Soumis",
            "next": "Prochain",
            "back": "Verso",
            "question": "Question",
            "review": "La revue",
            "close": "Fermer",
            "exit": "Sortie",
            "done": "Terminé",
            "your score": "Ton score:",
            "required to pass": "Obligatoire pour passer.",
            "correct": "Correct",
            "congratulations-passed": "Félicitations, vous avez réussi!",
            "sorry-failed": "Désolé. Tu n'es pas passé.",
            "results": "Résultats",
            "summary": "Résumé",
            "summary text": "Merci pour votre temps. Nous apprécions que vous répondiez à ces questions.",
            "back to summary": "Résumé",
            "exit continue head": "Sortie?",
            "exit continue message": "Cliquez sur 'OK' pour quitter.",
            "exit resume warning head": "Sortie?",
            "exit resume warning message": "Êtes-vous sûr? Vous n'avez pas fini et devrez recommencer ce quiz depuis le début.",
            "no answer submit head": "Envoyer?",
            "no answer submit message": "Vous n'avez pas entré de réponse. Êtes-vous sûr de vouloir soumettre?",
            "no answer likert head": "Veuillez sélectionner une réponse",
            "no answer likert message": "Cette question nécessite une réponse.",
            "no answer continue head": "Continuer?",
            "no answer continue message": "Vous n'avez pas encore envoyé votre réponse. Es-tu sur de vouloir continuer?",
            "end quiz ua message": "Vous n'avez pas répondu à toutes les questions! Cela va mettre fin à votre quiz! Es-tu sur de vouloir continuer?",
            "end quiz head": "Fin du quiz?",
            "end quiz message": "Cela va mettre fin à votre quiz! Es-tu sur de vouloir continuer?",
            "end survey ua message": "Vous n'avez pas répondu à toutes les questions! Cela va mettre fin à votre enquête! Es-tu sur de vouloir continuer?",
            "end survey head": "Fin de l'enquête?",
            "end survey message": "Cela va mettre fin à votre enquête! Es-tu sur de vouloir continuer?",
            "submit survey ua message": "Vous n'avez pas répondu à toutes les questions! Es-tu sur de vouloir continuer?",
            "submit survey head": "Soumettez l'enquête?",
            "submit survey message": "Ceci soumettra votre sondage. Es-tu sur de vouloir continuer?"
        },
        "fr-CA": {
            "ok": "OK",
            "cancel": "Annuler",
            "submit": "Envoyer",
            "submitted": "Soumis",
            "next": "Prochain",
            "back": "Verso",
            "question": "Question",
            "review": "La revue",
            "close": "Fermer",
            "exit": "Sortie",
            "done": "Terminé",
            "your score": "Ton score:",
            "required to pass": "Obligatoire pour passer.",
            "correct": "Correct",
            "congratulations-passed": "Félicitations, vous avez réussi!",
            "sorry-failed": "Désolé. Tu n'es pas passé.",
            "results": "Résultats",
            "summary": "Résumé",
            "summary text": "Merci pour votre temps. Nous apprécions que vous répondiez à ces questions.",
            "back to summary": "Résumé",
            "exit continue head": "Sortie?",
            "exit continue message": "Cliquez sur 'OK' pour quitter.",
            "exit resume warning head": "Sortie?",
            "exit resume warning message": "Êtes-vous sûr? Vous n'avez pas fini et devrez recommencer ce quiz depuis le début.",
            "no answer submit head": "Envoyer?",
            "no answer submit message": "Vous n'avez pas entré de réponse. Êtes-vous sûr de vouloir soumettre?",
            "no answer likert head": "Veuillez sélectionner une réponse",
            "no answer likert message": "Cette question nécessite une réponse.",
            "no answer continue head": "Continuer?",
            "no answer continue message": "Vous n'avez pas encore envoyé votre réponse. Es-tu sur de vouloir continuer?",
            "end quiz ua message": "Vous n'avez pas répondu à toutes les questions! Cela va mettre fin à votre quiz! Es-tu sur de vouloir continuer?",
            "end quiz head": "Fin du quiz?",
            "end quiz message": "Cela va mettre fin à votre quiz! Es-tu sur de vouloir continuer?",
            "end survey ua message": "Vous n'avez pas répondu à toutes les questions! Cela va mettre fin à votre enquête! Es-tu sur de vouloir continuer?",
            "end survey head": "Fin de l'enquête?",
            "end survey message": "Cela va mettre fin à votre enquête! Es-tu sur de vouloir continuer?",
            "submit survey ua message": "Vous n'avez pas répondu à toutes les questions! Es-tu sur de vouloir continuer?",
            "submit survey head": "Soumettez l'enquête?",
            "submit survey message": "Ceci soumettra votre sondage. Es-tu sur de vouloir continuer?"
        },
        "ja": {
            "ok": "OK",
            "cancel": "Cancel",
            "submit": "Submit",
            "submitted": "Submitted",
            "next": "Next",
            "back": "Back",
            "question": "Question",
            "review": "Review",
            "close": "Close",
            "exit": "Exit",
            "done": "Done",
            "your score": "Your Score:",
            "required to pass": "Required to pass.",
            "correct": "Correct",
            "congratulations-passed": "Congratulations, you passed!",
            "sorry-failed": "So sorry. You did not pass.",
            "results": "Results",
            "summary": "Summary",
            "summary text": "Thank you for your time. We appreciate you answering these questions.",
            "back to summary": "Summary",
            "exit continue head": "Exit?",
            "exit continue message": "Click 'OK' to exit.",
            "exit resume warning head": "Exit?",
            "exit resume warning message": "Are you sure? You have not finished and will have to restart this quiz from the beginning.",
            "no answer submit head": "Submit?",
            "no answer submit message": "You haven't entered an answer. Are you sure you want to submit?",
            "no answer likert head": "Please select a response.",
            "no answer likert message": "This question requires a response.",
            "no answer continue head": "Continue?",
            "no answer continue message": "You haven't submitted your answer yet. Are you sure you want to continue?",
            "end quiz ua message": "You have not answered all questions! This will END your quiz! Are you sure you want to continue?",
            "end quiz head": "End Quiz?",
            "end quiz message": "This will END your quiz! Are you sure you want to continue?",
            "end survey ua message": "You have not answered all questions! This will END your survey! Are you sure you want to continue?",
            "end survey head": "End Survey?",
            "end survey message": "This will END your survey! Are you sure you want to continue?",
            "submit survey ua message": "You have not answered all questions! Are you sure you want to continue?",
            "submit survey head": "Submit Survey?",
            "submit survey message": "This will submit your survey. Are you sure you want to continue?"
        },
        "ru": {
            "ok": "OK",
            "cancel": "Cancel",
            "submit": "Submit",
            "submitted": "Submitted",
            "next": "Next",
            "back": "Back",
            "question": "Question",
            "review": "Review",
            "close": "Close",
            "exit": "Exit",
            "done": "Done",
            "your score": "Your Score:",
            "required to pass": "Required to pass.",
            "correct": "Correct",
            "congratulations-passed": "Congratulations, you passed!",
            "sorry-failed": "So sorry. You did not pass.",
            "results": "Results",
            "summary": "Summary",
            "summary text": "Thank you for your time. We appreciate you answering these questions.",
            "back to summary": "Summary",
            "exit continue head": "Exit?",
            "exit continue message": "Click 'OK' to exit.",
            "exit resume warning head": "Exit?",
            "exit resume warning message": "Are you sure? You have not finished and will have to restart this quiz from the beginning.",
            "no answer submit head": "Submit?",
            "no answer submit message": "You haven't entered an answer. Are you sure you want to submit?",
            "no answer likert head": "Please select a response.",
            "no answer likert message": "This question requires a response.",
            "no answer continue head": "Continue?",
            "no answer continue message": "You haven't submitted your answer yet. Are you sure you want to continue?",
            "end quiz ua message": "You have not answered all questions! This will END your quiz! Are you sure you want to continue?",
            "end quiz head": "End Quiz?",
            "end quiz message": "This will END your quiz! Are you sure you want to continue?",
            "end survey ua message": "You have not answered all questions! This will END your survey! Are you sure you want to continue?",
            "end survey head": "End Survey?",
            "end survey message": "This will END your survey! Are you sure you want to continue?",
            "submit survey ua message": "You have not answered all questions! Are you sure you want to continue?",
            "submit survey head": "Submit Survey?",
            "submit survey message": "This will submit your survey. Are you sure you want to continue?"
        },
        "pt-BR": {
            "ok": "OK",
            "cancel": "Cancel",
            "submit": "Submit",
            "submitted": "Submitted",
            "next": "Next",
            "back": "Back",
            "question": "Question",
            "review": "Review",
            "close": "Close",
            "exit": "Exit",
            "done": "Done",
            "your score": "Your Score:",
            "required to pass": "Required to pass.",
            "correct": "Correct",
            "congratulations-passed": "Congratulations, you passed!",
            "sorry-failed": "So sorry. You did not pass.",
            "results": "Results",
            "summary": "Summary",
            "summary text": "Thank you for your time. We appreciate you answering these questions.",
            "back to summary": "Summary",
            "exit continue head": "Exit?",
            "exit continue message": "Click 'OK' to exit.",
            "exit resume warning head": "Exit?",
            "exit resume warning message": "Are you sure? You have not finished and will have to restart this quiz from the beginning.",
            "no answer submit head": "Submit?",
            "no answer submit message": "You haven't entered an answer. Are you sure you want to submit?",
            "no answer likert head": "Please select a response.",
            "no answer likert message": "This question requires a response.",
            "no answer continue head": "Continue?",
            "no answer continue message": "You haven't submitted your answer yet. Are you sure you want to continue?",
            "end quiz ua message": "You have not answered all questions! This will END your quiz! Are you sure you want to continue?",
            "end quiz head": "End Quiz?",
            "end quiz message": "This will END your quiz! Are you sure you want to continue?",
            "end survey ua message": "You have not answered all questions! This will END your survey! Are you sure you want to continue?",
            "end survey head": "End Survey?",
            "end survey message": "This will END your survey! Are you sure you want to continue?",
            "submit survey ua message": "You have not answered all questions! Are you sure you want to continue?",
            "submit survey head": "Submit Survey?",
            "submit survey message": "This will submit your survey. Are you sure you want to continue?"
        },
        "zh-CN": {
            "ok": "好",
            "cancel": "取消",
            "submit": "提交",
            "submitted": "已提交",
            "next": "下一个",
            "back": "返回",
            "question": "问题",
            "review": "复习",
            "close": "关闭",
            "exit": "退出",
            "done": "已完成",
            "your score": "你的分数:",
            "required to pass": "合格必备。",
            "correct": "正确",
            "congratulations-passed": "恭喜!及格通过。",
            "sorry-failed": "非常抱歉，你没有通过。",
            "results": "结果",
            "summary": "概要",
            "summary text": "感谢您的时间。 我们感谢您回答这些问题。",
            "back to summary": "概要",
            "exit continue head": "退出?",
            "exit continue message": "点击'确定'退出。",
            "exit resume warning head": "退出?",
            "exit resume warning message": "确定？您尚未完成，退出后必须从头开始重新测验。",
            "no answer submit head": "提交?",
            "no answer submit message": "你还没有输入答案。 确定要提交吗？",
            "no answer likert head": "请选择一个答案。",
            "no answer likert message": "这个问题需要回复。",
            "no answer continue head": "继续?",
            "no answer continue message": "你还没有提交你的答案。 确定要继续吗？",
            "end quiz ua message": "你还没有回答所有问题！ 这会结束您的测验！ 确定要继续吗？",
            "end quiz head": "结束测验?",
            "end quiz message": "这会结束您的测验！ 确定要继续吗?",
            "end survey ua message": "你还没有回答所有问题！ 这会结束您的调查！ 确定要继续吗？",
            "end survey head": "结束调查?",
            "end survey message": "这会结束你的调查！ 确定要继续吗？",
            "submit survey ua message": "你还没有回答所有问题！ 你确定你要继续吗？",
            "submit survey head": "提交调查？",
            "submit survey message": "这将提交您的调查。 你确定你要继续吗？"
        }
    };

    // assign redundancies

    this.Phrases["es-ES"] = this.Phrases["es"];
    this.Phrases["es-MX"] = this.Phrases["es"];
    this.Phrases["it-IT"] = this.Phrases["it"];
    this.Phrases["fr-CA"] = this.Phrases["fr"];
    this.Phrases["fr-FR"] = this.Phrases["fr"];
    this.Phrases["de-DE"] = this.Phrases["de"];
    this.Phrases["ru-RU"] = this.Phrases["ru"];
    this.Phrases["ja-JP"] = this.Phrases["ja"];
    this.Phrases["it-IT"] = this.Phrases["it"];
    this.Phrases["nl-NL"] = this.Phrases["nl"];

    this.SetActiveLanguage(l);

}

Dictionary.prototype.SetActiveLanguage = function (l) {

    try {
        var x = this.Phrases[l]["close"]; // grab a random phrase
        this.ActiveLanguage = l;
    } catch (e) {
        try {
            var x = this.Phrases[this.ActiveLanguage]["close"];
            // do nothing, we keep the existing
        } catch (e) {
            // default
            this.ActiveLanguage = "en-US";
        }
    }
}

Dictionary.prototype.Get = function (s) {

    if (!this.Phrases[this.ActiveLanguage][s]) {
        if (!this.Phrases["default"][s]) {
            "[_" + s + "-]";
        } else {
            return this.Phrases["default"][s];
        }
    } else {
        return this.Phrases[this.ActiveLanguage][s];
    }

}

