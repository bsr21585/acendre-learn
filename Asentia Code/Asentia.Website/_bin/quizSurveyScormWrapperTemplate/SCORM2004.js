SCORM2004 = function(window, journaling){
	
	this.window = window;
	this.API = null;
	this.ObjectName = "SCORMHandler";
	
	this.STATES = {
		NOT_INITIALIZED 	: "not-initialized",
		INITIALIZING 		: "initializing",
		INITIALIZED 		: "initialized",
		TERMINATING 		: "terminating",
		TERMINATED 			: "terminated",
		ERROR				: "error"
	}
	
	this.state = this.STATES.NOT_INITIALIZED;
	
	// journaling mode OFF by default
	
	try{
		this.JournalingOn = journaling;
	} catch(e){
		this.JournalingOn = false;
	}
	
	this.result = "";
	this.errorCode = "";
	this.errorDescription = "";
}

SCORM2004.prototype.Initialize = function(){
	
	// already initialized
	
	if (this.state == this.STATES.INITIALIZED){
		return true;
	}
	
	// set state
	
	this.state = this.STATES.INITIALIZING;
	
	//find the api
	
	if (!this.FindAPI()){
		return false;
	}
	
	// initialize the api
	
	this.result = this.API.Initialize("") + "";
	
	// get results
	
	if (this.result != "true"){
		this.GetLastActionResult();
		this.state = this.STATES.ERROR;
	} else {
		this.state = this.STATES.INITIALIZED;
	}
	
}

SCORM2004.prototype.FindAPI = function(){
	
	if ((this.window.parent != null) && (this.window.parent != this.window)) 
	{ 
		this.API = this.TraverseParentsForAPI(this.window); 
	} 
	
	if ((this.API == null) && (this.window.top.opener != null))
	{ 
		this.API = this.TraverseParentsForAPI(this.window.top.opener); 
	} 
	
	// not found
	if (this.API == null){
		this.state = this.STATES.ERROR;
		return false;
	}
	
	return true;
}

SCORM2004.prototype.TraverseParentsForAPI = function(window){
	
	var i = 0;
	var j = 50; 
	
	while ((window.API_1484_11 == null || window.API_1484_11 == undefined) && 
			(window.parent != null) && (window.parent != window) && 
			(i <= j) 
		  )
	{ 
		i++; 
		window = window.parent;
	} 
	
	return window.API_1484_11; 
	
}

SCORM2004.prototype.Terminate = function(){
	
	if(this.state != this.STATES.INITIALIZED){
		return true;
	}
	
	this.state = this.STATES.TERMINATING;
	
	var result = this.API.Terminate("") + "";
	
	this.state = this.STATES.TERMINATED;
	
}

SCORM2004.prototype.GetValue = function(cmi_element){
	
	// ensure initialization
	
    if (!this.Initialize()) {
        return false;
    }
	
	// call
	
	var result = this.API.GetValue(cmi_element) + "";
	
	// get last error, validation
	
	return result;
	
}

SCORM2004.prototype.SetValue = function(cmi_element, value){
	
	// ensure initialization
	
    if (!this.Initialize()) {
        return false;
    }
	
	// call
	
	this.result = this.API.SetValue(cmi_element + "", value + "") + "";
	
	// get results
	
	if (this.result != "true"){
		this.GetLastActionResult();
		return false;
	}
	
	return true;
	
}

SCORM2004.prototype.Commit = function(){
	
	// ensure initialization
	
    if (!this.Initialize()) {
        return false;
    }
	
	// call
	
	this.result = this.API.Commit("") + "";
	
	// get results
	
	if (this.result != "true"){
		this.GetLastActionResult();
		return false;
	}
	
	return true;
	
}

SCORM2004.prototype.GetLastError = function(){
}

SCORM2004.prototype.GetErrorString = function(){
}

SCORM2004.prototype.GetDiagnostic = function(){
}

SCORM2004.prototype.GetLastActionResult = function(){
}
