//SCORM-Related Lesson Functions

var testMode = false;

var SCOItems = new Object();

var checkPagesInterval;
var resumePageInterval;

//this will be inited by the Viewer once the PDF loads
function initSCORMItems()
{
	var so = SCOItems;
	
	//keep track of what pages are viewed
	so.progressArr = new Array();
	so.lessonSetCompleted = false;
	so.foundPages = false;
	
	so.isResuming = false;
	so.completedResume = false;
	
	//set lesson to 'incomplete' if 'not attempted'
	setLessonIncomplete();
	
	//check to see if there's LMS suspend data (saved progress array data) to bring back
	tryLessonResume(); //JP START HERE AND BUILDSUSPENDDATA; NEED TO SYNC UP RESTORING PROGRESS AND LAST VISITED PAGE ON RESUME
	
	//build progress array based on number pages in the PDF
	initProgressArray();
	
	//set lesson completed if it needs to be
	updateLessonCompletion();
}

function tryLessonResume()
{
	//if there is LMS suspend data available, grab the data and restore it to local vars
	var restoredData = getLMSSuspendData();
	
	if (restoredData)
	{
		SCOItems.isResuming = true;
		//SCOItems.progressArr = String(restoredData).split(",");
		SCOItems.progressArr = getProgressStrFromSuspendDataStr(restoredData).split(",");
		
		//once the viewer can access the current page, manually navigate to the last page recorded in suspend data
		if (getCurrentPdfPageNumber())
		{
			tryResumePage();
		}
		else
		{
			//set an interval to do the resume when the proper data is available
			resumePageInterval = setInterval(tryResumePage, 500);
		}
	}
}

function tryResumePage()
{
	//attempt to navigate to last recorded page number in suspend data
	if (getCurrentPdfPageNumber())
	{
		clearInterval(resumePageInterval);
		
		var lastRecordedPage = getCurrentPageFromSuspendData();
		
		if (lastRecordedPage)
		{
			jumpToPdfPage(lastRecordedPage);
			//signal that the resume is completed
			SCOItems.completedResume = true;
		}
	}
}

function buildSuspendData()
{
	var suspendData = "<sd>";
	
	//add the progress string
	suspendData += "<pr>";
	suspendData += getProgressStr();
	suspendData += "</pr>";
	
	//save the page they are on if that information is currently available
	if (getCurrentPdfPageNumber())
	{
		//cp is current page
		suspendData += "<cp>";
		suspendData += String(getCurrentPdfPageNumber());
		suspendData += "</cp>";
	}
	
	suspendData += "</sd>";
	
	return suspendData;
}

function setLMSSuspendData()
{
	//if lesson is in process of resuming, don't overwrite the current suspend data because we'll need to retrieve it first
	if (SCOItems.isResuming && !SCOItems.completedResume)
		return;
		
	//send the suspend data to the LMS
	var currSuspendData = buildSuspendData();

	doLMSSetValue("cmi.suspend_data", currSuspendData);
}

function getLMSSuspendData()
{	
	if (testMode)
		return "<sd><pr>1,1,1</pr><cp>3</cp></sd>";
	
	return doLMSGetValue("cmi.suspend_data");
}

function initProgressArray()
{	
	//if the progress array already exists (was populated from suspend data), no need to re-init it from scratch
	if (SCOItems.progressArr.length > 0)
		return;
	
	//get total num pages in this PDF
	var totalPages = Number(PDFViewerApplication.pagesCount);
	
	console.log("Trying to build progress array. Num pages: " + String(totalPages));
	
	if (!totalPages)
	{
		console.log("totalPages not found. Starting interval");
		checkPagesInterval = setInterval(checkPages, 500);	
	}
	else
	{
		buildProgressArray();
		
		//update SCORM data after progress array is constructed
		updateSCORMData();
	}
	
}

function buildProgressArray()
{
	var soa = SCOItems.progressArr;
	
	//get total num pages in this PDF
	var totalPages = Number(PDFViewerApplication.pagesCount);
	
	//populate the progress array
	for (var i = 0; i < totalPages; i++)
	{
		soa.push(0);
	}
}

function checkPages()
{
	var tp = Number(PDFViewerApplication.pagesCount);
	
	console.log("doing interval totalPages check. Found: " + String(tp))
	
	if (tp)
	{
		clearInterval(checkPagesInterval);
		
		//update boolean that the pages were found
		SCOItems.foundPages = true;
		
		//create the progress array
		buildProgressArray();
		
		//update SCORM data after progress array is constructed
		updateSCORMData();
	}
}

function updateProgressArray()
{
	//use the page number to update the progress array: 0 = unvisited, 1 = visited
	try
	{	
		var currPageNum = Number(getCurrentPdfPageNumber());
		console.log("Update progress array using current page num: " + currPageNum.toString());
		var currPageIndex = currPageNum - 1;

		//update the page index visited to 1 if it is 0
		if (SCOItems.progressArr[currPageIndex] == 0)
			SCOItems.progressArr[currPageIndex] = 1;
	}
	catch(e)
	{
		console.log("Error setting internal progress array: " + e.message);
	};
}

function jumpToPdfPage(pageNum)
{
	PDFViewerApplication.page = (Number(pageNum) | 0);
	console.log("jump to page " + String(pageNum));
}

function getCurrentPdfPageNumber()
{
	return PDFViewerApplication.page;
}

function getCurrentPageFromSuspendData()
{
	var currSd = getLMSSuspendData();
	var currentPage;
	
	if (currSd)
	{
		//is suspend data exists, extract the 'cp' tag data
		//currentPage = $(currSd).find("cp").val();
		currentPage = currSd.split("<cp>")[1].split("</cp>")[0];
	}
	
	return currentPage;
}

function getProgressDecimal()
{
	//return progress as a decimal: i.e. 30% done = .3
	var soa = SCOItems.progressArr;
	var numPagesVisited = 0;
	var pDec;
	
	for (var i = 0; i < soa.length; i++)
	{
		if (soa[i] == 1)
			numPagesVisited++;
	}
	
	pDec = numPagesVisited/soa.length;
	
	return pDec;
}

function getProgressStr()
{
	//get progress array as a string
	return SCOItems.progressArr.toString();
}

function getProgressStrFromSuspendDataStr(dataStr)
{
	//extract the progress section from the full suspend data str: i.e. from "<sd><pr>1,1,1</pr><cp>3</cp></sd>" we'd get "1,1,1"
	var progressStr = "";
	
	//progress will be in the <pr></pr> tag
	if (dataStr)
		progressStr = $(dataStr).find("pr").text();
	
	return progressStr;
}

function updateSCORMData()
{
	updateProgressArray();
	updateProgressMeasure();
	updateLessonCompletion();
	setLMSSuspendData();
}

function setLessonIncomplete()
{
	if (!API) return "";
	if (blnFinishCalled) return "";
	
	//set lesson 'incomplete' if 'not attempted' or 'unknown'
	var currentCompletionStatus = doLMSGetValue("cmi.completion_status");
	
	if (currentCompletionStatus == "not attempted" || currentCompletionStatus == "unknown")
		doLMSSetValue("cmi.completion_status","incomplete");
}

function setLessonCompleted()
{
	if (!API) return "";
	if (blnFinishCalled) return "";
	
	//set lesson 'incomplete' if 'not attempted'
	var currentCompletionStatus = doLMSGetValue("cmi.completion_status");
	
	if (currentCompletionStatus != "completed")
	{
		doLMSSetValue("cmi.completion_status","completed");
		SCOItems.lessonSetCompleted = true;	
	}
	else if (currentCompletionStatus == "completed")
	{
		//if on a resumed lesson, this internal var will need to be set again to signal previous completion
		if (!SCOItems.lessonSetCompleted)
			SCOItems.lessonSetCompleted = true;
	}
}

function updateLessonCompletion()
{
	//set lesson completed if threshold met
	if (getProgressDecimal() >= completionThreshold && !SCOItems.lessonSetCompleted)
		setLessonCompleted();
}

function updateProgressMeasure()
{
	var currProgressMeasure = getLMSProgressMeasure();
	
	//if progress measure already exists, weigh current measure vs what's already been recorded
	if (currProgressMeasure)
	{
		if (getProgressDecimal() > Number(currProgressMeasure))
			setLMSProgressMeasure();
	}
	else
	{
		//if no progress measure is recorded yet, save the new progress measure
		setLMSProgressMeasure();
	}
}

function getLMSProgressMeasure()
{
	return doLMSGetValue("cmi.progress_measure");
}

function setLMSProgressMeasure()
{
	//set the progress measure (to 7 signigicant digits)
	var currProgressMeasure = getProgressDecimal();
	
	currProgressMeasure = checkProgressMeasureLength(currProgressMeasure);
	
	//set the value
	if (currProgressMeasure || currProgressMeasure == 0)
		doLMSSetValue("cmi.progress_measure", String(currProgressMeasure));
}

function checkProgressMeasureLength(pMeasure)
{
	//SCORM value only accepts up to 7 significant digits; concatenate if there are more than that
	var pMeasureStr = String(pMeasure);
	
	if (pMeasureStr.length > 9)
	{
		pMeasureStr = pMeasureStr.substr(0,9);
		
		//return the new concatenated value as a number
		return Number(pMeasureStr);
	}
	
	//else return the original value
	return pMeasure;
}

function onUserExit()
{
	doLMSSetValue("cmi.exit","suspend");
}