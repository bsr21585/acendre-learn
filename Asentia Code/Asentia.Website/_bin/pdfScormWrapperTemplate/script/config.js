// contains all the customizable options for the Asentia SCORM PDF Wrapper

// the name of the PDF file
var pdfFilename = "##pdfFilename##";

// completionThreshold will be compared against progress measure so lesson can manage its own completion_status
var completionThreshold = ##minProgressForCompletion##;