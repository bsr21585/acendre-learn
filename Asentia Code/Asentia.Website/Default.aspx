﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.UMS.Pages.Default" %>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Literal ID="HomePageContent" runat="server" />
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>