﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.QuizzesAndSurveys.Default" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceholderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content id="SideContent1" ContentPlaceholderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceholderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="ObjectOptionsPanel" runat="server"  />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="QuizSurveyFormContentWrapperContainer" runat="server">
            <asp:UpdatePanel ID="QuizSurveyGridUpdatePanel" runat="server">
                <ContentTemplate>
                    <inq:Grid ID="QuizSurveyGrid" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:Panel ID="ActionsPanel" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceholderID="SideContentPlaceholder2" runat="server">
</asp:Content>