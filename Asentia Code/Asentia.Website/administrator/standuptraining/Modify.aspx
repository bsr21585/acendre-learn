﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.StandupTraining.Modify" ValidateRequest="false" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadPlaceholder" runat="server">
    <script type="text/javascript" src="/_scripts/ckeditor/ckeditor.js"></script>
</asp:Content>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="StandupTrainingPropertiesFormContentWrapperContainer" runat="server">
            <asp:Panel ID="StandupTrainingObjectMenuContainer" runat="server" />
            <asp:Panel ID="StandupTrainingPropertiesWrapperContainer" runat="server">
                <asp:Panel ID="StandupTrainingPropertiesInstructionsPanel" runat="server" />
                <asp:Panel ID="StandupTrainingPropertiesFeedbackContainer" runat="server" />
                <asp:Panel ID="StandupTrainingPropertiesContainer" runat="server" />
                <asp:Panel ID="StandupTrainingPropertiesActionsPanel" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>