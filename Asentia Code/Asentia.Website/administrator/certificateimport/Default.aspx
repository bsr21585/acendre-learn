﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.CertificateImport.Default" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadPlaceholder" runat="server">
</asp:Content>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="ObjectOptionsPanel" runat="server" />
        <asp:Panel ID="CertificateImportFormContentWrapperContainer" runat="server">
            <asp:Panel ID="UserObjectMenuContainer" runat="server" />
            <asp:Panel ID="CertificateImportPageWrapperContainer" runat="server">
                <asp:Panel ID="TabContainer" runat="server" />  
                    <asp:Panel ID="TabContentWrapperPanel" runat="server">  
                    <asp:UpdatePanel ID="CertificateImportUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="PageFeedbackContainer" runat="server" />  
                            <asp:Panel ID="ErrorTablePanel" runat="server" />        
                            <asp:Panel ID="BatchFileUploadPanel" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>             
             
                    <asp:UpdatePanel ID="CertificateImportGridUpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="ManageCertificateFeedbackContainer" runat="server" /> 
                            <asp:Panel ID="ManagementInstructionPanel" runat="server" /> 
                            <asp:Panel ID="CertificateImportGridPanel" runat="server"> 
                                <inq:Grid ID="CertificateImportGrid" runat="server" />
                            </asp:Panel> 
                            <asp:Panel ID="ActionsPanel" runat="server" />   
                        </ContentTemplate>
                    </asp:UpdatePanel>                 
                </asp:Panel> 
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>    
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>