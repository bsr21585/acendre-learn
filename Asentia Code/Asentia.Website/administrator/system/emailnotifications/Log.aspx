﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.System.EmailNotifications.Log" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
    <asp:Panel ID="PageTitleContainer" runat="server" />
    <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
    <asp:Panel ID="TabContainer" runat="server" />  
        <asp:UpdatePanel runat="server" ID="SentGridUpdatePanel">
          <ContentTemplate>
                <inq:Grid ID="SentGrid" runat="server" />
                <asp:Panel ID="GridContainer" runat="server" />
         </ContentTemplate>
        </asp:UpdatePanel> 
         <asp:UpdatePanel runat="server" id="FailedGridUpdatePanel">
            <ContentTemplate>
                <asp:Panel ID="FailedFeedbackContainer" runat="server" /> 
                <inq:Grid ID="FailedGrid" runat="server" />
                <asp:Panel ID="ActionsPanel" runat="server" />    
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>    
</asp:content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>
