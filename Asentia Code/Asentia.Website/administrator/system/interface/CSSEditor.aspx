﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.UMS.Pages.Administrator.System.Interface.CSSEditor" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadPlaceholder" runat="server">
    <script type="text/javascript" src="/_scripts/aceeditor/ace.js"></script>
    <script type="text/javascript" src="/_scripts/aceeditor/ext-language_tools.js"></script>
</asp:Content>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageInstructionsPanel" runat="server" />
        <asp:Panel ID="CSSEditorFormContentWrapperContainer" runat="server" >
            <asp:UpdatePanel ID="CSSFileUpdatePanel" updatemode="Conditional" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="PageFeedbackContainer" runat="server" />
                    <inq:CSSFileEditor ID="CSSEditorControl" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>