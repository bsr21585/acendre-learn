﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.UMS.Pages.Administrator.System.Interface.ImageEditor" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />   
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageInstructionsPanel" runat="server" />
        <asp:Panel ID="ImageEditorFormContentWrapperContainer" runat="server" >
            <asp:UpdatePanel ID="ImageFileUpdatePanel" updatemode="Conditional" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="PageFeedbackContainer" runat="server" />
                    <asp:Panel ID="ImageFileTreeContainer" runat="server">
                        <asp:Panel ID="ImageFileTreePanel" runat="server">
                            <asp:TreeView ID="ImageFileTree" runat="server" CausesValidation="false" />
                        </asp:Panel>
                    </asp:Panel>
                 
                    <asp:Panel ID="ImageFileEditWrapperContainer" runat="server">
                        <asp:Panel ID="ImageFileEditContainer" runat="server">
                            <asp:Panel ID="ImageEditorEditorLabelPanel" runat="server">
				            </asp:Panel>
                            <asp:Panel ID="ImageEditorEditingPanel" runat="server">
                                <asp:Panel ID="ImageFilePreviewContainer" runat="server">
                                    <asp:Image ID="SelectedImage" runat="server"/>
                                    <asp:HiddenField ID="SelectedImageURL" runat="server" />
                                </asp:Panel>
                                <asp:Panel ID="ImageSelectionPanel" runat="server"></asp:Panel>
                            </asp:Panel>
                            <asp:Panel ID="ActionsPanel" runat="server" />
                        </asp:Panel>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>