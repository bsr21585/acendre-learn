﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.UMS.Pages.Administrator.System.xAPIEndpoints.Modify" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="atk" %>

<asp:content id="BreadcrumbContent" contentplaceholderid="BreadcrumbContentPlaceholder"
    runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:content>

<asp:content id="SideContent1" contentplaceholderid="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:content>

<asp:content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageInstructionsPanel" runat="server" />
        <div class="TokenModify">
            <asp:Panel ID="PageFeedbackContainer" runat="server" />
            <asp:Panel ID="xApiFormContainer" runat="server" />
            <asp:Panel ID="ActionsPanel" runat="server" />
        </div>
    </asp:Panel>
</asp:content>
<asp:content id="SideContent2" contentplaceholderid="SideContentPlaceholder2" runat="server">
</asp:content>
