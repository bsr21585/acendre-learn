﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.System.Ecommerce.Default" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadPlaceholder" runat="server">    
    <script type="text/javascript" src="/_scripts/ckeditor/ckeditor.js"></script>
</asp:Content>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="EcommerceSettingsFormContentWrapperContainer" runat="server">
            <asp:Panel ID="EcommerceSettingsWrapperContainer" runat="server">
                <asp:Panel ID="PageInstructionsPanel" runat="server" />
                <asp:Panel ID="EcommerceSettingsFormContainer" runat="server" />
            </asp:Panel>
            <asp:Panel ID="ActionsPanel" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>