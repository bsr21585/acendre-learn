﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.System.Logs.EmailLog" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="LogFormContentWrapperContainer" runat="server">
            <asp:Panel ID="LogWrapperContainer" runat="server">
                <asp:Panel ID="TabContainer" runat="server" />  
                    <asp:Panel ID="TabContentWrapperPanel" runat="server" > 
                        <asp:UpdatePanel ID="SentGridUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="SentGridPanel" runat="server"> 
                                    <inq:Grid ID="SentGrid" runat="server" />
                                </asp:Panel> 
                             </ContentTemplate>
                        </asp:UpdatePanel> 

                        <asp:UpdatePanel ID="FailedGridUpdatePanel" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="FailedFeedbackContainer" runat="server" />
                                <asp:Panel ID="FailedGridPanel" runat="server"> 
                                    <inq:Grid ID="FailedGrid" runat="server" />
                                </asp:Panel> 
                                <asp:Panel ID="ActionsPanel" runat="server" />   
                             </ContentTemplate>
                        </asp:UpdatePanel> 
                    </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>    
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>