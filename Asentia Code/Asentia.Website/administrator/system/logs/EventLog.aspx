﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.System.Logs.EventLog" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="LogFormContentWrapperContainer" runat="server">
            <asp:Panel ID="LogWrapperContainer" runat="server">
                <asp:UpdatePanel runat="server" id="EventLogGridUpdatePanel" updatemode="Conditional">
                    <ContentTemplate>
                        <inq:Grid ID="EventLogGrid" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <asp:Panel ID="ActionsPanel" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>