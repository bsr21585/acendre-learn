﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.Courses.EmailNotifications.Modify" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadPlaceholder" runat="server">
    <script type="text/javascript" src="/_scripts/ckeditor/ckeditor.js"></script>
</asp:Content>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="EmailNotificationPropertiesFormContentWrapperContainer" runat="server">
            <asp:Panel ID="CourseObjectMenuContainer" runat="server" />
            <asp:Panel ID="EmailNotificationPropertiesWrapperContainer" runat="server">
                <asp:Panel ID="EmailNotificationPropertiesFeedbackContainer" runat="server" />
                <asp:Panel ID="EmailNotificationPropertiesInstructionsPanel" runat="server" />
                <asp:Panel ID="EmailNotificationPropertiesContainer" runat="server" />
                <asp:Panel ID="EmailNotificationPropertiesActionsPanel" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>