﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.Packages.Default" %>

<asp:Content id="BreadcrumbContent" contentplaceholderid="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content id="SideContent1" contentplaceholderid="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceholderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="ObjectOptionsPanel" runat="server"  />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="ContentPackagesFormContentWrapperContainer" runat="server">
            <asp:Panel ID="GridAnalyticPanel" runat="server" />
            <asp:UpdatePanel ID="PackageGridUpdatePanel" runat="server">
                <ContentTemplate>
                    <inq:Grid ID="PackageGrid" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:Panel ID="ActionsPanel" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceholderID="SideContentPlaceholder2" runat="server">
</asp:Content>