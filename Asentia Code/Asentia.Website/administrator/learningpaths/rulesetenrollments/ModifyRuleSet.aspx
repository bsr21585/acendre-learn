﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.LearningPaths.RulesetEnrollments.ModifyRuleSet" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="LearningPathAccordionMenuContainer" runat="server" />
        <asp:Panel ID="RuleSetPropertiesWrapperContainer" runat="server">
            <asp:Panel ID="RuleSetPropertiesFeedbackContainer" runat="server" />
            <asp:Panel ID="ObjectLanguageSelectorContainer" runat="server" />
            <asp:Panel ID="RuleSetPropertiesInstructionsPanel" runat="server" />
            <asp:Panel ID="RuleSetPropertiesContainer" runat="server" />
            <asp:Panel ID="RuleSetPropertiesActionsPanel" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>