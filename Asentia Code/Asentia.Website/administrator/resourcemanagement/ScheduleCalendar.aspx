﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.ResourceManagement.ScheduleCalendar" %>

<asp:content id="BreadcrumbContent" contentplaceholderid="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:content>

<asp:content id="SideContent1" contentplaceholderid="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:content>

<asp:content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="ObjectOptionsPanel" runat="server" />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="ResourceCalendarFormContentWrapperContainer" runat="server">
            <asp:Panel ID="ResourceCalendarWrapperContainer" runat="server" >
                <asp:Panel ID="MaintenanceScheduleContainer" runat="server" />
                <asp:Panel ID="OutSideUseScheduleContainer" runat="server" />
                <asp:Panel ID="UserActionsContainer" runat="server" />
            </asp:Panel>
            <asp:Panel ID="ActionsPanel" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:content>

<asp:content id="SideContent2" contentplaceholderid="SideContentPlaceholder2" runat="server">
</asp:content>







