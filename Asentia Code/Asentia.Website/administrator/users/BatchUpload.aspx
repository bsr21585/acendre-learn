﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.UMS.Pages.Administrator.Users.BatchUpload" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="ObjectOptionsPanel" runat="server" />
        <asp:Panel ID="UserBatchUploadWrapperPanel" runat="server">
            <asp:UpdatePanel ID="UserFormUpdatePanel" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="PanelUploadContainer" runat="server">
                        <asp:Panel ID="PageFeedbackContainer" runat="server" />    
                        <asp:Panel ID="ErrorTablePanel" runat="server" />     
                        <asp:Panel ID="BatchFileUploadPanel" runat="server" />
                    </asp:Panel>                
                </ContentTemplate>
            </asp:UpdatePanel>
         </asp:Panel>  
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>
