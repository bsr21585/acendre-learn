﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.Users.Certifications.View" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="CertificationFormContentWrapperContainer" runat="server">
            <asp:Panel ID="UserObjectMenuContainer" runat="server" />
            <asp:Panel ID="CertificationFormWrapperContainer" runat="server">
                <asp:Panel ID="PageInstructionsPanel" runat="server" />                
                <asp:Panel ID="PageFeedbackContainer" runat="server" />
                <asp:Panel ID="CertificationFormContainer" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>