﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.Roles.ModifyRuleSet" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="GroupActionsHeaderContainer" runat="server" />
        <asp:Panel ID="GroupActionsContainer" runat="server" />
        <asp:Panel ID="PropertiesHeaderContainer" runat="server" />
        <asp:Panel ID="PropertiesWrapperContainer" runat="server">
            <asp:Panel ID="PropertiesInstructionsPanel" runat="server" />
            <asp:Panel ID="ObjectLanguageSelectorContainer" runat="server" />
            <asp:Panel ID="PropertiesFeedbackContainer" runat="server" />
            <asp:Panel ID="PropertiesContainer" runat="server" />
            <asp:Panel ID="PropertiesActionsPanel" runat="server" />
        </asp:Panel>
        <asp:Panel ID="GroupMembershipHeaderContainer" runat="server" />
        <asp:Panel ID="GroupMembershipContainer" runat="server" />
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>