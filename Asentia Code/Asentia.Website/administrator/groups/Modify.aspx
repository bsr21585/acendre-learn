﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.UMS.Pages.Administrator.Groups.Modify" %>

<asp:Content ID="HeadContent" ContentPlaceholderID="HeadPlaceholder" runat="server">
    <script type="text/javascript" src="/_scripts/ckeditor/ckeditor.js"></script>
</asp:Content>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="GroupModifyFormContentWrapperContainer" runat="server">
            <asp:Panel ID="GroupObjectMenuContainer" runat="server" />
            <asp:Panel ID="GroupModifyWrapperContainer" runat="server">
                <asp:Panel ID="GroupPropertiesFeedbackContainer" runat="server" />
                <asp:Panel ID="GroupPropertiesInstructionsPanel" runat="server" />
                <asp:Panel ID="GroupPropertiesContainer" runat="server" />
                <asp:Panel ID="GroupPropertiesActionsPanel" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>