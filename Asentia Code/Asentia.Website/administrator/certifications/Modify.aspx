﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.Certifications.Modify"  ValidateRequest="false" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="CertificationPropertiesFormContentWrapperContainer" runat="server">
            <asp:Panel ID="CertificationObjectMenuContainer" runat="server" />
            <asp:Panel ID="CertificationPropertiesWrapperContainer" runat="server">
                <asp:Panel ID="CertificationPropertiesInstructionsPanel" runat="server" />
                <asp:Panel ID="CertificationPropertiesContainer" runat="server">
                    <asp:Panel ID="CertificationFormContainer" runat="server">
                        <asp:Panel ID="CertificationAsentiaModalContainer" runat="server">
                            <asp:Panel ID="CertificationAsentiaJsModal" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="CertificationAsentiaModalAlertContainer" runat="server">
                            <asp:Panel ID="CertificationAsentiaJsModalAlert" runat="server" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="CertificationPropertiesActionsPanel" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>