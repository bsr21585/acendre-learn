﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Administrator.Certifications.EmailNotifications.Default" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="ObjectOptionsPanel" runat="server" />
        <asp:Panel ID="EmailNotificationsFormContentWrapperContainer" runat="server">
            <asp:Panel ID="CertificationObjectMenuContainer" runat="server" />
            <asp:Panel ID="EmailNotificationsWrapperContainer" runat="server">
                <asp:Panel ID="PageFeedbackContainer" runat="server" />
                <asp:UpdatePanel ID="EmailNotificationGridUpdatePanel" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <inq:Grid ID="EmailNotificationGrid" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Panel ID="ActionsPanel" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>