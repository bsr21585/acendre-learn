﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Dashboard.LearningPathEnrollment" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceholderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content id="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="LearningPathEnrollmentFormContentWrapperContainer" runat="server"> 
            <asp:Panel ID="LearningPathEnrollmentWrapperContainer" runat="server">
                <asp:Panel ID="LearningPathDetailsContainer" runat="server" />
                <asp:UpdatePanel runat="server" ID="LearningPathEnrollmentCoursesGridUpdatePanel" UpdateMode="Conditional">
                    <ContentTemplate>
                        <inq:Grid ID="LearningPathEnrollmentCoursesGrid" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>