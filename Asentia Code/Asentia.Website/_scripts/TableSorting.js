﻿/*

Note from Daniel Ko
 
   This file should be consistent with the file  Asentia.Common.TableSorting.js

   Willmaster Table Sort
   Version 1.0
   July 3, 2011

   Will Bontrager
   http://www.willmaster.com/
   Copyright 2011 Will Bontrager Software, LLC

   This software is provided "AS IS," without 
   any warranty of any kind, without even any 
   implied warranty such as merchantability 
   or fitness for a particular purpose.
   Will Bontrager Software, LLC grants 
   you a royalty free license to use or 
   modify this software provided this 
   notice appears on all copies. 
*/
//
// One placed to customize - The id value of the table tag.
// Customized the method by Daniel Ko 5/25/2016

var TableIDvalue;

//
//////////////////////////////////////
var TableLastSortedColumn = -1;
var ascIcon = "&#x25B2;";
var descIcon = "&#x25BC;";
var sortIcon = ascIcon;

function SortTable() {
    var sortColumn = parseInt(arguments[0]);
    var type = arguments.length > 1 ? arguments[1] : 'T';
    var dateformat = arguments.length > 2 ? arguments[2] : '';
    var table = document.getElementById(TableIDvalue);

    if (table.getElementsByTagName('thead').length == 0) {
        // table doesn't have a tHead. Since it should have, create one and
        // put the first table row in it.
        the = document.createElement('thead');
        the.appendChild(table.rows[0]);
        table.insertBefore(the, table.firstChild);
    }
    // Safari doesn't support table.tHead, sigh
    if (table.tHead == null) table.tHead = table.getElementsByTagName('thead')[0];

    if (table.tHead.rows.length != 1) return; // can't cope with two header rows

    var tbody = table.getElementsByTagName("tbody")[0];
    var rows = tbody.getElementsByTagName("tr");
    var rowhead = table.tHead.rows[0];
    var arrayOfRows = new Array();

    type = type.toUpperCase();
    dateformat = dateformat.toLowerCase();
    for (var i = 0, len = rows.length; i < len; i++) {
        arrayOfRows[i] = new Object;
        arrayOfRows[i].oldIndex = i;
        var celltext = rows[i].getElementsByTagName("td")[sortColumn].innerHTML.replace(/<[^>]*>/g, "");
        if (type == 'D') { arrayOfRows[i].value = GetDateSortingKey('mmddyyyy', celltext); }
        else {
            var re = type == "N" ? /[^\.\-\+\d]/g : /[^a-zA-Z0-9]/g;
            arrayOfRows[i].value = celltext.replace(re, "").substr(0, 25).toLowerCase();
        }
    }

    sortIcon = ((sortColumn == TableLastSortedColumn) && (sortIcon == ascIcon)) ? descIcon : ascIcon;

    for (var i = 0; i < rowhead.cells.length; i++) {
        rowhead.cells[i].innerHTML = (rowhead.cells[i].innerHTML.substring(0, rowhead.cells[i].innerHTML.length - 1)).trim();
    }
    rowhead.cells[sortColumn].innerHTML += sortIcon;
    if (sortColumn == TableLastSortedColumn) { arrayOfRows.reverse(); }
    else {
        TableLastSortedColumn = sortColumn;
        switch (type) {
            case "N": arrayOfRows.sort(CompareRowOfNumbers); break;
            case "D": arrayOfRows.sort(CompareRowOfNumbers); break;
            default: arrayOfRows.sort(CompareRowOfText);
        }
    }
    var newTableBody = document.createElement("tbody");
    for (var i = 0, len = arrayOfRows.length; i < len; i++) {
        newTableBody.appendChild(rows[arrayOfRows[i].oldIndex].cloneNode(true));
    }
    table.replaceChild(newTableBody, tbody);
    $("table#UserTableTranscriptFullView tr:even").removeClass().addClass("GridDataRowAlternate");
    $("table#UserTableTranscriptFullView tr:odd").removeClass().addClass("GridDataRow");
    $("table#UserTableTranscriptFullView tr:first").removeClass().addClass("GridHeaderRow");

} // function SortTable()

function CompareRowOfText(a, b) {
    var aval = a.value;
    var bval = b.value;
    return (aval == bval ? 0 : (aval > bval ? 1 : -1));
} // function CompareRowOfText()

function CompareRowOfNumbers(a, b) {
    var aval = /\d/.test(a.value) ? parseFloat(a.value) : 0;
    var bval = /\d/.test(b.value) ? parseFloat(b.value) : 0;
    return (aval == bval ? 0 : (aval > bval ? 1 : -1));
} // function CompareRowOfNumbers()

function GetDateSortingKey(format, text) {
    if (format.length < 1) { return ""; }
    format = format.toLowerCase();
    text = text.toLowerCase();
    text = text.replace(/^[^a-z0-9]*/, "", text);
    text = text.replace(/[^a-z0-9]*$/, "", text);
    if (text.length < 1) { return ""; }
    text = text.replace(/[^a-z0-9]+/g, ",", text);
    var date = text.split(",");
    if (date.length < 3) { return ""; }

    var d = 0, m = 0, y = 0, h = 0, min = 0, s = 0;
    m = date[0];
    d = date[1];
    y = date[2];
    h = date[3];
    min = date[4];
    s = date[5];

    if (m < 10) { m = "0" + m; }
    if (d < 10) { d = "0" + d; }
    if (date[6] == 'pm' || date[6] == 'PM') { h = parseInt(h) + 12; }
    if (h < 10) { h = "0" + h; }
    if ((date[6] == 'am' || date[6] == 'AM') && (h == 12)) { h = "00"; }

    return String(y) + String(m) + String(d) + String(h) + String(min) + String(s);
}