﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LRS.Pages.Reporting.Reports.Default" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:content id="SideContent1" contentplaceholderid="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:content>

<asp:content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">    
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="ReportFormContentWrapperContainer" runat="server">
            <asp:Panel ID="ReportAccordionMenuContainer" runat="server" />
            <asp:Panel ID="ReportPropertiesWrapperContainer" runat="server">
                <asp:Panel ID="ReportFeedbackContainer" runat="server" />
                <asp:Panel ID="TabContainer" runat="server" />    
                <asp:Panel ID="TabContentWrapperPanel" runat="server">    

                    <asp:UpdatePanel ID="MySavedReportsContainer" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="SavedReportsFeedbackContainer" runat="server" />
                                <asp:Panel ID="MySavedReportsInstructionsPanel" runat="server" /> 
                                <inq:Grid ID="MySavedReportsGrid" runat="server" />
                                <asp:Panel ID="MySavedReportsActionsPanel" runat="server" />              
                            </ContentTemplate>                            
                    </asp:UpdatePanel>                         
                
                    <asp:UpdatePanel ID="PublicReportsContainer" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="PublicReportsFeedbackContainer" runat="server" /> 
                                <asp:Panel ID="PublicReportsInstructionsPanel" runat="server" />
                                <inq:Grid ID="PublicReportsGrid" runat="server" />
                                <asp:Panel ID="PublicReportsActionsPanel" runat="server" />                 
                            </ContentTemplate>
                    </asp:UpdatePanel> 
                
                    <asp:UpdatePanel runat="server" ID="MySubscriptionsContainer">                
                            <ContentTemplate>
                                <asp:Panel ID="MySubscriptionsFeedbackContainer" runat="server" /> 
                                <asp:Panel ID="MySubscriptionsInstructionsPanel" runat="server" /> 
                                <inq:Grid ID="ReportSubscriptionGrid" runat="server" />
                                <asp:Panel ID="MySubscriptionsReportActionsPanel" runat="server" />
                            </ContentTemplate>
                    </asp:UpdatePanel>                  
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
     </asp:Panel>
</asp:Content>


<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>

