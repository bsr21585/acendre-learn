﻿<% @ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LRS.Pages.Reporting.Reports.Modify" EnableEventValidation="false" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:ScriptManager AsyncPostBackTimeout="3000" />
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="ObjectOptionsPanel" runat="server" />                        
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="ReportFormContentWrapperContainer" runat="server">
            <asp:Panel ID="ReportWrapperContainer" runat="server">
                <asp:Panel ID="PageInstructionsPanel" runat="server" />
                <asp:Panel ID="ReportFormContainer" runat="server" />
                <asp:Panel ID="ReportTypeModal" runat="server" /> 
            </asp:Panel>
            <asp:Panel ID="ActionsPanel" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>