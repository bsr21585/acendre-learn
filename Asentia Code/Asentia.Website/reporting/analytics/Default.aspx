﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LRS.Pages.Reporting.Analytics.Default" %>

<asp:content id="BreadcrumbContent" contentplaceholderid="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:content>


<asp:content id="SideContent1" contentplaceholderid="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:content>

<asp:content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">    
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="NewAnalyticContainer" runat="server">   
            <asp:Panel ID="NewAnalyticInstructionsPanel" runat="server" />     
        </asp:Panel>
            <asp:Panel ID="MySavedAnalyticsContainer" runat="server">
             <asp:Panel ID="MySavedAnalyticsInstructionsPanel" runat="server" />     
        </asp:Panel>
              <asp:Panel ID="PublicAnalyticsContainer" runat="server">
             <asp:Panel ID="PublicAnalyticsInstructionsPanel" runat="server" />     
        </asp:Panel>
     </asp:Panel>

</asp:content>


<asp:content id="SideContent2" contentplaceholderid="SideContentPlaceholder2" runat="server">
</asp:content>
