﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LRS.Pages.Reporting.Analytics.Modify" EnableEventValidation="false" %>

<asp:content id="BreadcrumbContent" contentplaceholderid="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:content>

<asp:content id="SideContent1" contentplaceholderid="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:content>

<asp:content id="PageContent" contentplaceholderid="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
    <asp:Panel ID="PageTitleContainer" runat="server" />
    <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
    <asp:Panel ID="PageFeedbackContainer" runat="server" />
    <asp:Panel ID="AnalyticTitlePanel" runat="server" />
         <div class="AnalyticModify">
                     <asp:Panel ID="PageInstructionsPanel" runat="server" />
                    <asp:Panel ID="AnalyticFormContainer" runat="server" />
                   <asp:Panel ID="ActionsPanel" runat="server" />
             </div>
    </asp:Panel>
</asp:content>


<asp:content id="SideContent2" contentplaceholderid="SideContentPlaceholder2" runat="server">
</asp:content>

