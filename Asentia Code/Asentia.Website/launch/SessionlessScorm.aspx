﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Launch.SessionlessScorm" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>    
</head>
<body>
    <form id="form1" runat="server">
        <atk:ToolkitScriptManager ID="asm" runat="server" ScriptMode="Release" EnablePartialRendering="true" EnableScriptLocalization="true" EnableScriptGlobalization="true" />
        <div>
            <script src="WidgetAPI.js" type="text/javascript"></script>     
            <asp:Panel ID="LauncherContainer" runat="server" />
        </div>
    </form>
</body>
</html>