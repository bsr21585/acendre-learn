﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.Launch.TinCan" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body onload="OnLoad()">
    <form id="form1" runat="server">
        <atk:ToolkitScriptManager ID="Asm" runat="server" ScriptMode="Release" EnablePartialRendering="true" EnableScriptLocalization="true" EnableScriptGlobalization="true" />        
        <asp:Panel ID="PageContainer" runat="server">
            <asp:Panel ID="ContentHeaderAndNavigationContainer" runat="server" />
            <asp:Panel ID="LessonContentFrameContainer" runat="server" />
        </asp:Panel>
    </form>
</body>
</html>