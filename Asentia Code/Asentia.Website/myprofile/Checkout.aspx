﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Asentia.LMS.Pages.MyProfile.Checkout" %>

<asp:Content ID="BreadcrumbContent" ContentPlaceHolderID="BreadcrumbContentPlaceholder" runat="server">
    <asp:Panel ID="PageBreadcrumbContainer" runat="server" />
</asp:Content>

<asp:Content ID="SideContent1" ContentPlaceHolderID="SideContentPlaceholder1" runat="server">
    <inq:AdministratorMenu ID="AdminMenu" runat="server" />
</asp:Content>

<asp:Content id="PageContent" ContentPlaceHolderID="PageContentPlaceholder" runat="server">
    <asp:Panel ID="PageContentContainer" runat="server">
        <asp:Panel ID="PageTitleContainer" runat="server" />
        <asp:Panel ID="FullDropDownBreadcrumbContainer" runat="server" />
        <asp:Panel ID="ObjectOptionsPanel" runat="server" />
        <asp:Panel ID="PageFeedbackContainer" runat="server" />
        <asp:Panel ID="CheckoutPageContentWrapperContainer" runat="server">
            <asp:Panel ID="CheckoutPageContentContainer" runat="server">
                <asp:Panel ID="CheckoutFormWrapperContainer" runat="server">
                    <inq:Grid ID="MyCartGrid" runat="server" />
                    <asp:Panel ID="PurchaseTotalContainer" runat="server" />
                    <asp:Panel ID="CheckoutFormContainer" runat="server" />
                    <asp:Panel ID="CheckoutFormActionsPanel" runat="server" />                    
                </asp:Panel>                
                <asp:Panel ID="CheckoutFormProcessingContainer" runat="server" />
                <asp:Panel ID="CheckoutReceiptContainer" runat="server" />                
            </asp:Panel>        
        </asp:Panel>
    </asp:Panel>
</asp:Content>

<asp:Content ID="SideContent2" ContentPlaceHolderID="SideContentPlaceholder2" runat="server">
</asp:Content>