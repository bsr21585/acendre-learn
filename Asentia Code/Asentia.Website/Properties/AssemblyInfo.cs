﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Asentia.Website")]
[assembly: AssemblyDescription("Website files for the Asentia product suite.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ICS Learning Group")]
[assembly: AssemblyProduct("Asentia.Website")]
[assembly: AssemblyCopyright("Copyright © ICS Learning Group 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0a21c225-af12-43ad-8667-10c8be608176")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.X.*")]
[assembly: AssemblyVersion("1.16.*")]
