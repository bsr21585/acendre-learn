﻿ManifestDataLoader.GET_DATA_URL = GLOBAL.WEB_SERVICES.ManifestDataLoaderURL;

function ManifestDataLoader(parent, courseId) {
    
    this.courseLaunchController = parent;

    this.courseId = courseId;
    
	this.jsonLoader = new JSONLoader("ManifestDataLoader");
	
	//on successful load, store a reference to the XML, store the first Save Token, then propogate the event up
	this.jsonLoader.addEventListener(
		Event.LOADED,
		function (e) {
		    this.dispatchEvent(new Event(Event.TRACE, "ManifestDataLoader", "JSON response received; processing..."));

		    //store a reference to the response xml in this instance.
		    this.resultsJSON = e.target;

		    //propogate up the LOADED event
		    this.dispatchEvent(e);
		},
		this);
	
	//on other events, just propogate the event up for now
	this.jsonLoader.addEventListener(Event.FATAL_ERROR, this.dispatchEvent, this);
	this.jsonLoader.addEventListener(Event.ERROR, this.dispatchEvent, this);
	this.jsonLoader.addEventListener(Event.TRACE, this.dispatchEvent, this);
}

ManifestDataLoader.inherits(EventDispatcher);


//PUBLIC

/**
 * Call this to have alerts and confirms open from another window besides this one.
 */
ManifestDataLoader.method('setAlertConfirmOwner', function (windowHandle) {
    //alert('windowHandle' + windowHandle);
    this.jsonLoader.setAlertConfirmOwner(windowHandle);
});

ManifestDataLoader.method('load', function () {
    //alert('ManifestDataLoader.js: load funcion');
    this.jsonLoader.sendAndLoad(
		ManifestDataLoader.GET_DATA_URL,
        '{"contentFolder":"' + GLOBAL.COURSE_INFO.ContentFolder + '"}',
        'application/json; charset=utf-8');
});

/**
 * Returns a reference to the XML DOM that was loaded.
 */
ManifestDataLoader.method('getJSON', function () {

    return this.resultsJSON;
});

/**
 * Aborts the current JSONLoader.
 */
ManifestDataLoader.method('abort', function()
{
	this.jsonLoader.abort();
});
