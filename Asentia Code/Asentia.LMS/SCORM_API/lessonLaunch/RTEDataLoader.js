﻿RTEDataLoader.GET_DATA_URL = GLOBAL.WEB_SERVICES.RTEDataLoaderURL;

function RTEDataLoader(parent, enrollmentIdentifier, scoIdentifier)
{
    this.courseLaunchController = parent;

	this.enrollmentIdentifier = enrollmentIdentifier;

	this.scoIdentifier = scoIdentifier;
	
	this.jsonLoader = new JSONLoader("RTEDataLoader");
	
	//on successful load, store a reference to the XML, store the first Save Token, then propogate the event up
	this.jsonLoader.addEventListener(
		Event.LOADED,
		function (e) {
		    this.dispatchEvent(new Event(Event.TRACE, "RTEDataLoader", "JSON response received for SCO (" + this.scoIdentifier + "); processing..."));

		    //store a reference to the response xml in this instance.
		    this.resultsJSON = e.target;

		    var jsonResponse = JSON.parse(this.resultsJSON);
		    
		    //if the response XML came back in an invalid format, dispatch a fatal error and return.
		    //if (jsonResponse.d == "fail")
		    //{
		    //	return;
		    //}
		    //otherwise check for status items and dispatch them as errors. If any are fatal errors, return, otherwise, propogate up the LOADED event.
		    //else
		    //{					
		    //propogate up the LOADED event
		    this.dispatchEvent(e);
		    //}
		},
		this);
	
	//on other events, just propogate the event up for now
	this.jsonLoader.addEventListener(Event.FATAL_ERROR, this.dispatchEvent, this);
	this.jsonLoader.addEventListener(Event.ERROR, this.dispatchEvent, this);
	this.jsonLoader.addEventListener(Event.TRACE, this.dispatchEvent, this);
}

RTEDataLoader.inherits(EventDispatcher);


//PUBLIC

/**
 * Call this to have alerts and confirms open from another window besides this one.
 */
RTEDataLoader.method('setAlertConfirmOwner', function(windowHandle)
{
	this.jsonLoader.setAlertConfirmOwner(windowHandle);
});


RTEDataLoader.method('load', function () {
    this.jsonLoader.sendAndLoad(
		RTEDataLoader.GET_DATA_URL,
        '{"learnerId":"' + GLOBAL.COURSE_INFO.LearnerId + '","lessonIdentifier":"' + this.enrollmentIdentifier + '", "scoIdentifier":"' + this.scoIdentifier + '"}',
        'application/json; charset=utf-8');
});

/**
 * Returns a reference to the XML DOM that was loaded.
 */
RTEDataLoader.method('getJSON', function()
{
	return this.resultsJSON;
});

/**
 * Aborts the current JSONLoader.
 */
RTEDataLoader.method('abort', function()
{
	this.jsonLoader.abort();
});
