﻿RTEDataSaver.SET_DATA_URL = GLOBAL.WEB_SERVICES.RTEDataSaverURL;

function RTEDataSaver(parent, enrollmentIdentifier) {
    this.courseLaunchController = parent;

    this.scoIdentifier = null;

    this.enrollmentIdentifier = enrollmentIdentifier;

    this.resultsJSON = null; //this will be the XML DOM of the resulting data once it has loaded.

    this.jsonLoader = new JSONLoader("RTEDataSaver");

    //on successful save, store a reference to the XML, store the next Save Token, then propogate the event up
    this.jsonLoader.addEventListener(
		Event.LOADED,
		function (e) {
		    this.dispatchEvent(new Event(Event.TRACE, "RTEDataSaver", "JSON response received for SCO (" + this.scoIdentifier + "); processing..."));

		    //store a reference to the response xml in this instance.
		    this.resultsJSON = e.target;

		    var jsonResponse = JSON.parse(this.resultsJSON);

		    //if the response XML came back in an invalid format, dispatch a fatal error and return.
		    if (jsonResponse.d == "fail") {
		        return;
		    }
		        //otherwise check for status items and dispatch them as errors. If any are fatal errors, return, otherwise, propogate up the LOADED event.
		    else {
		        //propogate up the LOADED event
		        if (this.scoIdentifier == "global_data") {
		            this.dispatchEvent(new Event(Event.GLOBAL_DATA_SAVED, e.target, "GLOBAL DATA SAVED"));
		        }
		        else if (this.scoIdentifier == "|__NonLeafDataItems__|") {
		            this.dispatchEvent(new Event(Event.NON_LEAF_DATA_ITEMS_SAVED, e.target, "NON-LEAF DATA ITEMS SAVED"));
                }
		        else {
		            this.dispatchEvent(new Event(Event.SCO_DATA_SAVED, e.target, "SCO DATA SAVED FOR: " + this.scoIdentifier));
                }
		    }
		},
		this);

    //on other events, just propogate the event up for now
    this.jsonLoader.addEventListener(Event.FATAL_ERROR, this.dispatchEvent, this);
    this.jsonLoader.addEventListener(Event.ERROR, this.dispatchEvent, this);
    this.jsonLoader.addEventListener(Event.TRACE, this.dispatchEvent, this);
}

RTEDataSaver.inherits(EventDispatcher);


//PUBLIC

/**
* Call this to have alerts and confirms open from another window besides this one.
*/
RTEDataSaver.method('setAlertConfirmOwner', function (windowHandle) {
    this.jsonLoader.setAlertConfirmOwner(windowHandle);
});

/**
* Called by RTEDataLoader and RTEDataSaver to extract the lesson-data-save token from
* a standard XML Response formatted like:
*
* <serv_response>
* 		<head>
* 			<status>success</status>
* 		</head>
* 		<body>
* 			<token>TOKEN-IS-HERE</token>
* 		</body>
* </serv_response>
*/

//function commented by chetu as it has not been used anywhere
//RTEDataSaver.method('extractSaveToken', function (xmlDom) {
//    var tokenNode = XmlHelper.xpath(xmlDom, "/serv_response/body/token/text()")[0];

//    this.setSaveToken(tokenNode.nodeValue);
//});

/**
* The saveToken is set by the RTEDataLoader when it completes its load process.
* The token will either be 1) a string, which implies data can be saved to the database 
* during run-time using this token, or 2) an empty-string which implies data will not 
* be saved to the database.
*/

RTEDataSaver.method('setSaveToken', function (saveToken) {
    this.saveToken = saveToken;
});

/**
* Called by the active SCORM API to save the state of the lesson.
*
* @param xmlToPost string of XML.
* @param saveToDatabase boolean if true, data will be saved to the database, if false, will be saved to an XML file.
*/

RTEDataSaver.method('save', function (scoIdentifier, jsonToPost, schemaVersion, saveToDB) {
    
    if (saveToDB == undefined) {
        saveToDB = false;
    }

    this.scoIdentifier = scoIdentifier
   
    this.jsonLoader.sendAndLoad(
		RTEDataSaver.SET_DATA_URL,
        '{"learnerId":"' + GLOBAL.COURSE_INFO.LearnerId + '","lessonIdentifier":"' + this.enrollmentIdentifier + '", "scoIdentifier":"' + this.scoIdentifier + '", "jsonString":"' + jsonToPost + '", "schemaVersion":"' + schemaVersion + '", "launchType":"' + LaunchType + '", "idDataSco":"' + DataScoId + '", "saveToDB":"' + saveToDB + '"}',        
		'application/json; charset=utf-8');
});

/**
* Returns a reference to the XML DOM that was loaded.
*/
RTEDataSaver.method('getJSON', function () {
    return this.resultsJSON;
});

/**
* Aborts the current JSONLoader.
*/
RTEDataSaver.method('abort', function () {
    this.jsonLoader.abort();
});
