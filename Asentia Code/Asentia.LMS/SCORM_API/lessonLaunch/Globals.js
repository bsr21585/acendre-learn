﻿GLOBAL.COURSE_INFO = {
    ManifestId: "",
    ContentFolder: "",
    PersistedDataFileName: "",
    ReturnUrl: "index.aspx",
    LearnerName: "",
    LearnerId: "",
    EnrollmentIdentifier: ""
}

GLOBAL.WEB_SERVICES = {
        ManifestDataLoaderURL: ("/_util/scormservices.asmx/RetrieveManifestObject"),
        RTEDataLoaderURL: ("/_util/scormservices.asmx/RetrieveRTEData"),
        RTEDataSaverURL: ("/_util/scormservices.asmx/SaveRTEData"),
        KeepAliveSessionURL: ("/_util/scormservices.asmx/KeepAliveSession")
    }

function GLOBAL() {

}


