﻿CourseLaunchController.PAGE_MESSAGES = {
    LAUNCHING: ["Launching Courseware...", "Do <strong>not</strong> close this window.<br />Do <strong>not</strong> click the 'Back' button on your browser.<br />Do <strong>not</strong> click the 'Refresh' button on your browser."],
    POPUP_BLOCKED: ["Courseware Blocked.", null, "Your popup-blocker may have prevented the lesson from launching.<br />Be sure to set your browser to allow popup windows from this site.<br /><br /><a href='javascript:lessonLaunchController.lessonWindow.open();'>Click here to try it again.</a>"],
    RUNNING: ["Running Courseware.", null, ""],
    PROCESSING: ["Processing Data...Please Wait.", ""],
    FATAL_ERROR: ["A Fatal Error Ocurred.", "Please notify the administrator, then <a href='javascript:lessonLaunchController.exit()'>click here to go back.</a>"],
    DONE: ["Data Saved.", "<a href='javascript:lessonLaunchController.exit()'>Click here to go back to your 'My Account' screen.</a>"]
};

CourseLaunchController.PREMATURE_UNLOAD_MSG = "WARNING!\r\n\r\nIf you navigate away from this window, your LMS session will end and data loss will result. Please use the controls in the navigation menu to navigate back to the LMS.";

var scorm12Instance = null;

function CourseLaunchController(courseId,
								returnURL, dataMode) {
    //
    // -------------------------------------------------------------------------------
    // Perform all necessary "pre-load" operations before starting to load the course.
    // These operations include setting up LMS UI elements (nav controls, and logger
    // container), instantiating objects (loaders, savers, and APIs), and loading 
    // manifest as well as persisted data.
    // -------------------------------------------------------------------------------

    // keep a reference to this instance of CourseLaunchController
    var thisInstance = this;

    // isRunning shall be true until an API dispatches the FINISH_COMPLETED event
    this.isRunning = true;

    // save the return url here for when returning to the lesson listing page
    this.returnURL = returnURL;

    //global cmi entry value
    this.globalCmiEntry = null;

    // store a null reference to which API is active, to be set when an API is initialized by a SCO by calling this.setActiveApi.
    this.activeApi = null;
    // instantiate the objects used
    this.rteDataLoader = new RTEDataLoader(this,
                                                GLOBAL.COURSE_INFO.EnrollmentIdentifier,
                                                ""); // change to individual sco's
    this.rteDataSaver = new RTEDataSaver(this,
                                               GLOBAL.COURSE_INFO.EnrollmentIdentifier);  // change to individual sco's
    this.globalDataSaver = new RTEDataSaver(this,
                                               GLOBAL.COURSE_INFO.EnrollmentIdentifier);
    this.scorm12Api = new Scorm12Api(this);
    scorm12Instance = this.scorm12Api; //api instance variable for using in the set interval function, where this keyword is not accessible. 

    this.scorm2004Api = new Scorm2004Api(this);
    this.manifestDataLoader = new ManifestDataLoader(this, courseId);

    this.serverSessionPersister = new ServerSessionPersister();

    // WIDGET
    // create an object for injected learner persisted data and initialize with null values
    this.injectedLearnerPersistedData = {
        mode: "",
        entry: "",
        completion_status: "",
        success_status: "",
        score: {
            min: "",
            max: "",
            raw: "",
            scaled: ""
        }
    }

    this.injectedLearnerPersistedData.mode = null;
    this.injectedLearnerPersistedData.entry = null;
    this.injectedLearnerPersistedData.completion_status = null;
    this.injectedLearnerPersistedData.success_status = null;
    this.injectedLearnerPersistedData.score.min = null;
    this.injectedLearnerPersistedData.score.max = null;
    this.injectedLearnerPersistedData.score.raw = null;
    this.injectedLearnerPersistedData.score.scaled = null;

    //// have the logger monitor the events dispatched from the objects   //y1 commented 
    this.logger = new Logger(this.scorm2004Api,
                             this.scorm12Api,
                             this.manifestDataLoader,
    					     this.rteDataLoader,
    					     this.rteDataSaver,
                             this.globalDataSaver,
    						 this);

    // attach the logger to the container div on the page within which it will output
    this.logger.attachToContainer(document.getElementById("LoggerContainer"));

    // ------------------------------------------------------------------------------
    // The following event listeners handle the bult of the course launch work.
    // That is because we have to load the manifest and and RTE data before doing
    // anything else. These "LOADED" events are fired when those server-side calls
    // are complete, and perform the following actions: parse the manifest JSON data
    // into the scorm2004.imsManifest object, build the tree menu, load all persisted
    // RTE data, and finally perfrom actions to evaluate SCORM SN rules and identify
    // an item for delivery.
    // ------------------------------------------------------------------------------

    // "LOADED" event listener for manifestDataLoader
    this.manifestDataLoader.addEventListener(
                   Event.LOADED,
                   function (loaderEvent) {
                       // JSON data that is returned from a .NET web-service is contained
                       // within it's own object "d", therefore, we need to parse the data
                       // that was returned, then parse object.d to obtain our JSON data
                       var jsonObject = JSON.parse(this.manifestDataLoader.getJSON());
                       this.scorm2004Api.imsManifest = JSON.parse(jsonObject.d);
                       this.scorm12Api.imsManifest = JSON.parse(jsonObject.d);
                       var schemaVersion = this.scorm2004Api.imsManifest.schemaversion;
                       this.scorm12Api.schemaversion = this.scorm12Api.imsManifest.schemaversion;
                       this.scorm2004Api.schemaversion = this.scorm2004Api.imsManifest.schemaversion;

                       // use the first (and for now we assume only) organization to build the activity
                       // tree; the HTML for this tree is already built server-side by the manifest
                       // loader web-service
                       if (this.scorm2004Api.imsManifest.organizations.length > 0) {
                           this.buildTreeMenu(this.scorm2004Api.imsManifest.organizations[0].treeHTML);
                       }
                       if (schemaVersion.indexOf("1.2") == -1) {

                           // fill Global objectives from manifest file
                           this.FillGlobalObjectives();

                           // create Dummy Primary Objectives so they can be used when no primary objective is found
                           this.CreateDummyPrimaryObjectives();

                           // initialize data model for each item in the manifest
                           for (var scoIndex = 0; scoIndex < this.scorm2004Api.imsManifest.organizations[0].items.length; scoIndex++) {

                               this.scorm2004Api.courseDataModel[scoIndex] = new Scorm2004DataModel();
                               this.scorm2004Api.courseDataModel[scoIndex].scoIdentifier = this.scorm2004Api.imsManifest.organizations[0].items[scoIndex].identifier;

                               this.scorm2004Api.courseDataModel[scoIndex].cmi.learner_id.initialize(GLOBAL.COURSE_INFO.LearnerId);
                               this.scorm2004Api.courseDataModel[scoIndex].cmi.learner_name.initialize(GLOBAL.COURSE_INFO.LearnerName);

                               // initialize the mode data model element with value, as set from scorm launcher page
                               this.scorm2004Api.courseDataModel[scoIndex].cmi.mode.initialize(CmiMode);
                               var attemptAbsoluteDurationLimit = this.scorm2004Api.scorm2004ADLNav.GetSequencingElementsAttributeValue(scoIndex, false, "limitConditions", "attemptAbsoluteDurationLimit", null);

                               if (!isNullOrUndefined(attemptAbsoluteDurationLimit)) {
                                   //this.scorm2004Api.courseDataModel[scoIndex].cmi.max_time_allowed.initialize(this.scorm2004Api.msToTimeInterval(attemptAbsoluteDurationLimit * 1000));
                                   this.scorm2004Api.courseDataModel[scoIndex].cmi.max_time_allowed.initialize(attemptAbsoluteDurationLimit);
                               }

                               if (this.scorm2004Api.scorm2004ADLNav.IsPrimaryObjectiveSatisfiedByMeasure(scoIndex)) {
                                   var primaryObjective = this.scorm2004Api.scorm2004ADLNav.GetRolledupObjectiveForItem(scoIndex);
                                   this.scorm2004Api.courseDataModel[scoIndex].cmi.scaled_passing_score.initialize(this.scorm2004Api.scorm2004ADLNav.GetObjectiveMinNormalizedMeasure(scoIndex, primaryObjective.objectiveId));
                               }

                               // initialize the launch_data cmi data modal element
                               if (this.scorm2004Api.imsManifest.organizations[0].items[scoIndex].adlcpDataFromLMS != null) {
                                   this.scorm2004Api.courseDataModel[scoIndex].cmi.launch_data.initialize(this.scorm2004Api.imsManifest.organizations[0].items[scoIndex].adlcpDataFromLMS);
                               }

                               // initialize the time_limit_action cmi data modal element
                               if (this.scorm2004Api.imsManifest.organizations[0].items[scoIndex].adlcpTimeLimitAction != null) {
                                   this.scorm2004Api.courseDataModel[scoIndex].cmi.time_limit_action.initialize(this.scorm2004Api.imsManifest.organizations[0].items[scoIndex].adlcpTimeLimitAction);
                               }

                               // initialize the completion_threshold cmi data modal element
                               if (this.scorm2004Api.imsManifest.organizations[0].items[scoIndex].adlcpCompletionThreshold != null
                                   && this.scorm2004Api.imsManifest.organizations[0].items[scoIndex].adlcpCompletionThreshold.completionThreshold != null) {
                                   this.scorm2004Api.courseDataModel[scoIndex].cmi.completion_threshold.initialize(this.scorm2004Api.imsManifest.organizations[0].items[scoIndex].adlcpCompletionThreshold.completionThreshold);
                               }
                           }

                           this.SetAvailableChildren();
                           this.scorm2004Api.scorm2004ADLNav.ReorderChildren();

                           // "LOADED" event listener for rteDataLoader
                           this.rteDataLoader.addEventListener(
                         Event.LOADED,
                         function (loaderEvent) {

                             // load persisted data if the data mode value is 'Load', not 'Wait'
                             if (dataMode == "Load") {
                                 this.loadPersistedData();
                             }

                             // initialzation of objectives for each activity
                             this.scorm2004Api.SetObjectivesInDataModel();

                             var imsManifestOrganization = this.scorm2004Api.imsManifest.organizations[0];

                             // loop through the items of the organization to find the first deliverable
                             // sco (the first one with an identifierref)
                             var itemCounter;
                             for (itemCounter = 0; itemCounter < imsManifestOrganization.items.length; itemCounter++) {

                                 if (imsManifestOrganization.items[itemCounter].identifier == this.scorm2004Api.activeSCOIdentifier) {
                                     sessionStarted = true;
                                     this.findActiveScoIndex();
                                     this.deliverSCO();
                                     break;
                                 }
                             }

                             if (itemCounter == imsManifestOrganization.items.length) {
                                 // if there is no active sco, then find the first available
                                 // otherwise grab the last active one
                                 DeliverFirstLeaf = this.scorm2004Api.scorm2004ADLNav.CheckForFlow();
                                 if (DeliverFirstLeaf) {
                                     this.processLMSNavRequest("start");
                                 }
                                 else {
                                     // this is a user instruction page url that will open when no activity 
                                     // will be launched by default
                                     var href = "../launch/UserInstructionPage.html?pname=" + PackageName;

                                     // deliver the sco
                                     SetContentFrameUrl(href);

                                     // hide the loader popup
                                     HideLoadAndSaveContentModalPopup();

                                 }

                                 // if no active SCO is found, still need to update the activity tree SCO status icon class
                                 this.scorm2004Api.scorm2004ADLNav.UpdateStateDisplay();
                             }

                             // evaluate the LMS UI button states
                             this.evaluateLMSUIButtonStates();

                             // extra Added to set the current Active SCO isActive as True
                             if (this.scorm2004Api.activeSCOIndex != null) {
                                 this.scorm2004Api.scorm2004ADLNav.SetIsActiveToTrueForActivityProvidedAndFalseForOthers(this.scorm2004Api.activeSCOIndex);
                                 // added to increment Attempt count to 1 for the first delivery SCO
                                 this.scorm2004Api.courseDataModel[this.scorm2004Api.activeSCOIndex].cmi.activityAttemptCount.value += 1;
                                 this.scorm2004Api.scorm2004ADLNav.SetCurrentActivity(this.scorm2004Api.activeSCOIdentifier);
                                 this.scorm2004Api.scorm2004ADLNav.UpdateActivitiesStatus();
                             }
                             this.scorm2004Api.scorm2004ADLNav.CheckMenu();

                             // dispatch the event that this whole thing was loaded
                             this.dispatchEvent(new Event(Event.COURSE_LAUNCH_CONTROLLER_READY, "COURSE LAUNCH CONTROLLER IS READY")); // WIDGET
                         },
                         this);
                           // trigger the first rte data load
                           this.rteDataLoader.load();
                       }
                       else if (schemaVersion.indexOf("1.2") > -1) {

                           var manifestItems = null;

                           // initialize data models and grab persisted RTE data for each

                           // the first file name is the global data

                           // initialize data model for each item in the manifest
                           var manifestOrganizations = this.scorm12Api.imsManifest.organizations[0];
                           if (manifestOrganizations != null && manifestOrganizations != undefined) {
                               // initialize data model for each item in the manifest
                               manifestItems = this.scorm12Api.imsManifest.organizations[0].items;
                           }

                           // if manifest  items are not null
                           if (manifestItems != null && manifestItems != undefined) {

                               for (var i = 0; i < manifestItems.length; i++) {
                                   this.scorm12Api.courseDataModel[i] = new Scorm12DataModel();
                                   this.scorm12Api.courseDataModel[i].scoIdentifier = this.scorm12Api.imsManifest.organizations[0].items[i].identifier;
                                   this.scorm12Api.courseDataModel[i].cmi.core.student_id.initialize(GLOBAL.COURSE_INFO.LearnerId);
                                   this.scorm12Api.courseDataModel[i].cmi.core.student_name.initialize(GLOBAL.COURSE_INFO.LearnerName);
                                   this.scorm12Api.courseDataModel[i].cmi.core.lesson_mode.initialize(CmiMode); /// YP EDIT FOR CMI.MODE

                                   // initialize the launch_data cmi data modal element
                                   if (this.scorm12Api.imsManifest.organizations[0].items[i].adlcpDataFromLMS != null) {
                                       this.scorm12Api.courseDataModel[i].cmi.launch_data.initialize(this.scorm12Api.imsManifest.organizations[0].items[i].adlcpDataFromLMS); /// YP EDIT FOR CMI.MODE
                                   }
                               }

                               // "LOADED" event listener for rteDataLoader
                               this.rteDataLoader.addEventListener(
                             Event.LOADED,
                             function (loaderEvent) {

                                 // load persisted data for scorm 1.2 package activities
                                 if (dataMode == "Load") {
                                     this.loadPersistedData();
                                 }

                                 // if there is no active sco, then find the first available
                                 // otherwise grab the last active one
                                 if (this.scorm12Api.activeSCOIdentifier == null) {
                                     this.SearchDeliverableActivityForScorm12();
                                 }
                                 else {
                                     this.SearchDeliverableActivityForScorm12(this.scorm12Api.activeSCOIdentifier);
                                 }
                                 // evaluate the LMS UI button states
                                 this.EnableTreeItems();
                                 this.evaluateLMSUIButtonStatesForSco12();
                                 this.scorm12Api.UpdateStateDisplay();

                                 // dispatch the event that this whole thing was loaded
                                 this.dispatchEvent(new Event(Event.COURSE_LAUNCH_CONTROLLER_READY, "COURSE LAUNCH CONTROLLER IS READY")); // WIDGET
                             },
                             this);
                               // trigger the first rte data load                               
                               this.rteDataLoader.load();
                           }
                               // else create the default structure with default values of the data modal element
                           else {

                               this.scorm12Api.courseDataModel[0] = new Scorm12DataModel();
                               this.scorm12Api.courseDataModel[0].scoIdentifier = "DefaultIdentifierName";
                               rteFileNames[1] = "DefaultIdentifierName";
                               this.scorm12Api.courseDataModel[0].cmi.core.student_id.initialize(GLOBAL.COURSE_INFO.LearnerId);
                               this.scorm12Api.courseDataModel[0].cmi.core.student_name.initialize(GLOBAL.COURSE_INFO.LearnerName);

                               // "LOADED" event listener for rteDataLoader
                               this.rteDataLoader.addEventListener(
                             Event.LOADED,
                             function (loaderEvent) {

                                 var retrivedJson = JSON.parse(this.rteDataLoader.getJSON())

                                 if (retrivedJson.d != "fail") {

                                     var jsonKeyValueData = JSON.parse(retrivedJson.d);

                                     while (rteFileNames.length != 0) {
                                         var fileIdentifier = rteFileNames.shift();

                                         if (fileIdentifier == "global_data") {
                                             for (var i = 0; i < jsonKeyValueData.length; i++) {
                                                 if (jsonKeyValueData[i].Key == fileIdentifier) {
                                                     this.populatePersistedDataGlobal(fileIdentifier, jsonKeyValueData[i].Value);
                                                 }
                                             }
                                         }
                                         else {
                                             for (var i = 0; i < jsonKeyValueData.length; i++) {
                                                 if (isActiveSCOIdentifierFound && jsonKeyValueData[i].Key == fileIdentifier) {
                                                     this.populatePersistedDataSco(fileIdentifier, jsonKeyValueData[i].Value);
                                                 }
                                             }
                                         }
                                     }
                                 }

                                 // if there is no active sco, then find the first available
                                 // otherwise grab the last active one
                                 if (this.scorm12Api.activeSCOIdentifier == null) {
                                     this.SearchDeliverableActivityForScorm12();
                                 }
                                 else {
                                     this.SearchDeliverableActivityForScorm12(this.scorm12Api.activeSCOIdentifier);
                                 }

                                 // evaluate the LMS UI button states
                                 this.EnableTreeItems();
                                 this.evaluateLMSUIButtonStatesForSco12();
                                 this.scorm12Api.UpdateStateDisplay();

                                 // dispatch the event that this whole thing was loaded
                                 this.dispatchEvent(new Event(Event.COURSE_LAUNCH_CONTROLLER_READY, "COURSE LAUNCH CONTROLLER IS READY")); // WIDGET
                             },
                             this);
                               // trigger the first rte data load                               
                               this.rteDataLoader.load();
                           }
                       }
                   },
                   this);


    // -------------------------------------------------------------------------------------
    // Method to load the packages persisted global and SCO's data from json files to SCO data model
    // This method can be called at the time of launching the package and also externally at any 
    // time to fill the data in model from json files
    // -------------------------------------------------------------------------------------    
    CourseLaunchController.method('loadPersistedData', function () {

        var rteFileNames = new Array();
        rteFileNames[0] = "global_data";

        // WIDGET
        // get global cmi mode
        if (this.injectedLearnerPersistedData.mode != null) {
            CmiMode = this.injectedLearnerPersistedData;
        }

        if (this.scorm2004Api.schemaversion.indexOf("1.2") == -1) {

            // WIDGET
            // set global entry to the value of injectedLearnerPersistedData.entry if it is not null
            if (this.injectedLearnerPersistedData.entry != null) {
                this.globalCmlEntry = this.injectedLearnerPersistedData.entry;
                this.scorm2004Api.entry = this.injectedLearnerPersistedData.entry;
            }

            // if entry is ab-initio, just return, do not load anything
            if (this.globalCmlEntry == "ab-initio") {
                return;
            }

            var retrivedJson = JSON.parse(this.rteDataLoader.getJSON());

            if (retrivedJson.d != "fail") {

                for (var scoIndex = 0; scoIndex < this.scorm2004Api.imsManifest.organizations[0].items.length; scoIndex++) {
                    rteFileNames[scoIndex + 1] = this.scorm2004Api.imsManifest.organizations[0].items[scoIndex].identifier;
                }

                var jsonKeyValueData = JSON.parse(retrivedJson.d);

                while (rteFileNames.length != 0) {
                    var fileIdentifier = rteFileNames.shift();

                    if (fileIdentifier == "global_data") {
                        for (var i = 0; i < jsonKeyValueData.length; i++) {
                            if (jsonKeyValueData[i].Key == fileIdentifier) {
                                this.populatePersistedDataGlobal(fileIdentifier, jsonKeyValueData[i].Value);
                            }
                        }
                    }
                    else {
                        for (var i = 0; i < jsonKeyValueData.length; i++) {
                            if (isActiveSCOIdentifierFound && jsonKeyValueData[i].Key == fileIdentifier) {
                                this.populatePersistedDataSco(fileIdentifier, jsonKeyValueData[i].Value);
                            }
                        }
                    }
                }
            }
        }
        else if (this.scorm2004Api.schemaversion.indexOf("1.2") > -1) {

            // WIDGET
            // set global entry to the value of injectedLearnerPersistedData.entry if it is not null
            if (this.injectedLearnerPersistedData.entry != null) {
                this.globalCmlEntry = this.injectedLearnerPersistedData.entry;
                this.scorm12Api.entry = this.injectedLearnerPersistedData.entry;
            }

            // if entry is ab-initio, just return, do not load anything
            if (this.globalCmlEntry == "ab-initio") {
                return;
            }

            var retrivedJson = JSON.parse(this.rteDataLoader.getJSON());

            if (retrivedJson.d != "fail") {

                for (var scoIndex = 0; scoIndex < this.scorm12Api.imsManifest.organizations[0].items.length; scoIndex++) {
                    rteFileNames[scoIndex + 1] = this.scorm12Api.imsManifest.organizations[0].items[scoIndex].identifier;
                }

                var jsonKeyValueData = JSON.parse(retrivedJson.d);

                if (jsonKeyValueData != "fail" && jsonKeyValueData != null && jsonKeyValueData != undefined) {

                    while (rteFileNames.length != 0) {
                        var fileIdentifier = rteFileNames.shift();

                        if (fileIdentifier == "global_data") {
                            for (var counterI = 0; counterI < jsonKeyValueData.length; counterI++) {
                                if (jsonKeyValueData[counterI].Key == fileIdentifier) {
                                    this.populatePersistedDataGlobal(fileIdentifier, jsonKeyValueData[counterI].Value);
                                }
                            }
                        }
                        else {
                            for (var counterJ = 0; counterJ < jsonKeyValueData.length; counterJ++) {
                                if (isActiveSCOIdentifierFound && jsonKeyValueData[counterJ].Key == fileIdentifier) {
                                    this.populatePersistedDataSco(fileIdentifier, jsonKeyValueData[counterJ].Value);
                                }
                            }
                        }
                    }
                }
            }
        }
    });


    // -------------------------------------------------------------------------------------
    // This is the real entry point for course launch, the call to manifestDataLoader.load()
    // sets it all off. The "main" functionality of course launch happens within the event
    // listeners for the mainfestDataLoader.load() and rteDataLoader.load() events.
    // -------------------------------------------------------------------------------------

    // Dispatch the first trace event
    this.dispatchEvent(
    new Event(
        Event.TRACE,
        "CourseLaunchController",
        "Launching \"<b>Course</b>\" from " + window.location.href));

    // call the manifest data loader - this is the beginning of our course launch, other
    // things will be called once the manifest and all RTE data loads (see the event listeners above)
    this.manifestDataLoader.load();

    // update the message on the page - TO DO LATER
    //this.updateMessageOnPage(CourseLaunchController.PAGE_MESSAGES.LAUNCHING);

    // start the session persister to keep the server session alive - TO DO LATER
    //this.serverSessionPersister.start();

    // handle all Fatal Errors here
    //this.scorm12Api.addEventListener(Event.FATAL_ERROR, this.onFatalError, this);
    this.scorm2004Api.addEventListener(Event.FATAL_ERROR, this.onFatalError, this);
    //this.serverSessionPersister.addEventListener(Event.FATAL_ERROR, this.onFatalError, this);
    this.rteDataLoader.addEventListener(Event.FATAL_ERROR, this.onFatalError, this);
    this.rteDataSaver.addEventListener(Event.FATAL_ERROR, this.onFatalError, this);
    this.manifestDataLoader.addEventListener(Event.FATAL_ERROR, this.onFatalError, this);

    // handle the Finish-Started event here.
    //this.scorm12Api.addEventListener(Event.FINISH_STARTED, this.onApiFinishStarted, this);
    this.scorm2004Api.addEventListener(Event.FINISH_STARTED, this.onApiFinishStarted, this);

    // handle the Finish-Completed event here.
    this.scorm12Api.addEventListener(Event.FINISH_COMPLETED, this.onApiFinishCompleted, this);
    this.scorm2004Api.addEventListener(Event.FINISH_COMPLETED, this.onApiFinishCompleted, this);
    
    // on-before-unload, if the user tries to close the LMS window, warn them about data loss
    window.onbeforeunload = function () {
        if (thisInstance.isRunning && this.CmiMode != "review") {
            return CourseLaunchController.PREMATURE_UNLOAD_MSG;
        }
    }

    // on-unload, close the lesson window (if it's not already closed)
    //window.onunload = function () {
    //    if (this.isRunning) {
    // abort all objects so XHRs stop loading, and so the active SCORM API no longer accepts
    // future method calls (in case a SCO tries to call something duruing an on-load event which would cause JS errors becaue THIS window is unloaded)
    //         thisInstance.abortAll();
    //     }
    // }
};

CourseLaunchController.inherits(EventDispatcher);

//PUBLIC

/**
* Returns a reference to the SCORM 1.2 API.
*/
CourseLaunchController.method('getScorm12Api', function () {
    return this.scorm12Api;
});

/**
* Returns a reference to the SCORM 2004 API.
*/
CourseLaunchController.method('getScorm2004Api', function () {
    return this.scorm2004Api;
});

/**
* Sets a reference to the active API. This is used when the lesson window is closed to determine
* if the active API needs to be finished (if the SCO forgot to do that).
*/
CourseLaunchController.method('setActiveApi', function (activeApi) {
    this.activeApi = activeApi;
});

/**
* Returns a reference to the RTEDataLoader.
*/
CourseLaunchController.method('getRteDataLoader', function () {
    return this.rteDataLoader;
});

/**
* Returns a reference to the RTEDataSaver.
*/
CourseLaunchController.method('getRteDataSaver', function () {
    return this.rteDataSaver;
});

/**
* Returns a reference to the RTEDataSaver.
*/
CourseLaunchController.method('getGlobalDataSaver', function () {
    return this.globalDataSaver;
});

/**
* Updates the message on the page, consisting of a headline, and two text blocks: a warning/message div, and popup block message div.
*
* @param messageChoice array - one of the items in the CourseLaunchController.PAGE_MESSAGES set. 
* The second and third items in the array will be ignored if set to null or undefined.
*/
CourseLaunchController.method('updateMessageOnPage', function (messageChoice) {
    var headline = messageChoice[0];
    var warningText = messageChoice[1];
    var popupBlockText = messageChoice[2];

    //alert(headline);

    //if (warningText)
    //    document.getElementById("warning").innerHTML = warningText;
});



//PRIVATE

/**
* Builds a tree view menu of the organization being delivered to the learner.
*
* @param treeHTML - the HTML for the organization's tree
*/

CourseLaunchController.method('buildTreeMenu', function (treeHTML) {
    // set the innerHTML of the tree container to treeHTML    
    document.getElementById("ActivityTreeContainer").innerHTML = treeHTML;
});

/**
* Processes the loaded RTE Data into the appropriate data model
*/
CourseLaunchController.method('populatePersistedDataSco', function (scoIdentifier, jsonData) {

    // WIDGET
    // if the global entry value is "ab-initio", skip loading sco persisted data, just return
    if (this.globalCmiEntry == "ab-initio") {        
        return;
    }

    var persistedData = JSON.parse(jsonData);

    if (this.scorm12Api.schemaversion != "1.2") {

        var scoDataModelIndex = null;

        for (var i = 0; i < this.scorm2004Api.courseDataModel.length; i++) {
            if (this.scorm2004Api.courseDataModel[i].scoIdentifier == scoIdentifier) {
                scoDataModelIndex = i;
                break;
            }
        }

        if (scoDataModelIndex == null) {
            return;
        }

        var dataModel = this.scorm2004Api.courseDataModel[scoDataModelIndex].cmi;

        // lets get down to business
        // start with all the top-level text nodes - if value is null, the initialize will not happen        
        //dataModel.completion_threshold.initialize(persistedData.completion_threshold);        

        // WIDGET
        // populate entry with injected data if there is injected data and this is single-sco; otherwise, use persisted data
        if (this.injectedLearnerPersistedData.entry != null && this.scorm2004Api.courseDataModel.length == 2) {
            dataModel.entry.initialize(this.injectedLearnerPersistedData.entry);
        }
        else {
            dataModel.entry.initialize(persistedData.entry);
        }

        dataModel.exit.initialize(persistedData.exit);

        //time limit action
        dataModel.time_limit_action.initialize(persistedData.time_limit_action);
        dataModel.location.initialize(persistedData.location);
        dataModel.progress_measure.initialize(persistedData.progress_measure);

        // WIDGET
        // populate completion with injected data if there is injected data and this is single-sco; otherwise, use persisted data
        if (this.injectedLearnerPersistedData.completion_status != null && this.scorm2004Api.courseDataModel.length == 2) {
            dataModel.completion_status.initialize(this.injectedLearnerPersistedData.completion_status);
        }
        else {
            dataModel.completion_status.initialize(persistedData.completion_status);
        }

        // WIDGET
        // populate success with injected data if there is injected data and this is single-sco; otherwise, use persisted data
        if (this.injectedLearnerPersistedData.success_status != null && this.scorm2004Api.courseDataModel.length == 2) {
            dataModel.success_status.initialize(this.injectedLearnerPersistedData.success_status);
        }
        else {
            dataModel.success_status.initialize(persistedData.success_status);
        }

        // WIDGET
        // populate mode with injected data if there is injected data and we do not already have a completed/passed or 
        // completed/unknown for the sco; if we have completed/passed or completed/unknown, set mode to "review"
        if (this.injectedLearnerPersistedData.mode != null) {
            if (dataModel.completion_status.value == "completed" && (dataModel.success_status.value == "unknown" || dataModel.success_status.value == "passed")) {
                dataModel.mode.initialize("review");
            }
            else {
                dataModel.mode.initialize(this.injectedLearnerPersistedData.mode);
            }
        }

        dataModel.successStatusChangedDuringRuntime.initialize(persistedData.successStatusChangedDuringRuntime);

        // if suspend data value is saved as json object then replace single quote with double quote to make it parsable
        if (persistedData.suspend_data != null) {
            dataModel.suspend_data.initialize(persistedData.suspend_data.replace(new RegExp("'", 'g'), '"'));
        }
        else {
            dataModel.suspend_data.initialize(persistedData.suspend_data);
        }

        dataModel.max_time_allowed.initialize(persistedData.max_time_allowed);

        // handle the total_time which is delivered in seconds
        this.scorm2004Api.lastLaunchTotalTimeInMS = Number(persistedData.total_time) * 1000;
        // store the data model element as a TimeInterval
        dataModel.total_time.initialize(this.scorm2004Api.msToTimeInterval(this.scorm2004Api.lastLaunchTotalTimeInMS));

        // this.traceNote("Previous time in this lesson: " + Number(persistedData.total_time) + " seconds");
        // added code START
        dataModel.isActive.initialize(persistedData.isActive);
        dataModel.isSuspended.initialize(persistedData.isSuspended);
        dataModel.attemptCompletionAmount.initialize(persistedData.attemptCompletionAmount);
        dataModel.objectiveProgressStatus.initialize(persistedData.objectiveProgressStatus);
        dataModel.objectiveSatisfiedStatus.initialize(persistedData.objectiveSatisfiedStatus);
        dataModel.objectiveMeasureStatus.initialize(persistedData.objectiveMeasureStatus);
        dataModel.objectiveNormalizedMeasure.initialize(persistedData.objectiveNormalizedMeasure);
        dataModel.attemptProgressStatus.initialize(persistedData.attemptProgressStatus);
        dataModel.attemptCompletionStatus.initialize(persistedData.attemptCompletionStatus);
        dataModel.activityProgressStatus.initialize(persistedData.activityProgressStatus);

        // if the cmi.entry is resume then dont increment the activity attempt count
        if (persistedData.entry == "resume" && persistedData.activityAttemptCount > 0) {
            dataModel.activityAttemptCount.initialize(persistedData.activityAttemptCount - 1);
        }
        else {
            dataModel.activityAttemptCount.initialize(persistedData.activityAttemptCount);
        }

        dataModel.activityEffectiveAttemptCount.initialize(persistedData.activityEffectiveAttemptCount);
        // end
        // score

        // WIDGET
        // populate score with injected data if there is injected data and this is single-sco; otherwise, use persisted data
        if (this.injectedLearnerPersistedData.score.min != null && this.injectedLearnerPersistedData.score.max != null && this.injectedLearnerPersistedData.score.raw != null && this.injectedLearnerPersistedData.score.scaled != null && this.scorm2004Api.courseDataModel.length == 2) {
            dataModel.score.max.initialize(this.injectedLearnerPersistedData.score.max);
            dataModel.score.min.initialize(this.injectedLearnerPersistedData.score.min);
            dataModel.score.raw.initialize(this.injectedLearnerPersistedData.score.raw);
            dataModel.score.scaled.initialize(this.injectedLearnerPersistedData.score.scaled);
        }
        else {            
            dataModel.score.max.initialize(persistedData.score.max);
            dataModel.score.min.initialize(persistedData.score.min);
            dataModel.score.raw.initialize(persistedData.score.raw);
            dataModel.score.scaled.initialize(persistedData.score.scaled);
        }

        // get interactions
        if (persistedData.interactions.length == null) {
            interactionsLength = 0;
        }
        else {
            interactionsLength = persistedData.interactions.length;
        }

        if (interactionsLength > 0) {

            for (var x = 0; x < persistedData.interactions.length; x++) {
                var interactionObject = persistedData.interactions[x];

                var objInteraction = new Scorm2004Interaction();

                objInteraction.id.initialize(interactionObject.id);
                objInteraction.type.initialize(interactionObject.type);
                objInteraction.timestamp.initialize(interactionObject.timestamp);
                objInteraction.weighting.initialize(interactionObject.weighting);
                objInteraction.learner_response.initialize(interactionObject.learner_response);
                objInteraction.result.initialize(interactionObject.result);
                objInteraction.description.initialize(interactionObject.description);

                // latency arrives as seconds, so convert to TimeInterval
                var latencyAsSeconds = interactionObject.latency;
                if (!isNaN(latencyAsSeconds)) {
                    latencyAsSeconds = Number(latencyAsSeconds);
                    var latencyAsTimeInterval = this.scorm2004Api.msToTimeInterval(latencyAsSeconds * 1000);

                    objInteraction.latency.initialize(latencyAsTimeInterval);
                }

                if (interactionObject.objectives.length == null) {
                    interactionObjectivesLength = 0;
                }
                else {
                    interactionObjectivesLength = interactionObject.objectives.length;
                }

                if (interactionObjectivesLength > 0) {

                    for (var y = 0; y < interactionObject.objectives.length; y++) {
                        var interactionObjectivesObject = interactionObject.objectives[y];

                        var objInteractionObjective = new Scorm2004InteractionObjective();

                        objInteractionObjective.id.initialize(interactionObjectivesObject.id);

                        this.scorm2004Api.appendToCollection(objInteraction.objectives, objInteractionObjective);
                    }
                }

                if (interactionObject.correct_responses.length == null) {
                    interactionsCorrectResponsesLength = 0;
                }
                else {
                    interactionsCorrectResponsesLength = interactionObject.correct_responses.length;
                }

                if (interactionsCorrectResponsesLength > 0) {

                    for (var y = 0; y < interactionObject.correct_responses.length; y++) {
                        var correctResponseObject = interactionObject.correct_responses[y];

                        var objCorrectResponse = new Scorm2004InteractionCorrectResponse();

                        objCorrectResponse.pattern.initialize(correctResponseObject.pattern);

                        this.scorm2004Api.appendToCollection(objInteraction.correct_responses, objCorrectResponse);
                    }
                }

                this.scorm2004Api.appendToCollection(dataModel.interactions, objInteraction);
            }
        }

        // get objectives

        if (persistedData.objectives.length == null) {
            objectivesLength = 0;
        }
        else {
            objectivesLength = persistedData.objectives.length;
        }

        if (objectivesLength > 0) {

            for (var x = 0; x < persistedData.objectives.length; x++) {
                var objectiveObject = persistedData.objectives[x];

                var objObjective = new Scorm2004Objective();

                objObjective.id.initialize(objectiveObject.id);
                objObjective.success_status.initialize(objectiveObject.success_status);
                objObjective.completion_status.initialize(objectiveObject.completion_status);
                objObjective.description.initialize(objectiveObject.description);

                objObjective.score.max.initialize(objectiveObject.score.max);
                objObjective.score.min.initialize(objectiveObject.score.min);
                objObjective.score.raw.initialize(objectiveObject.score.raw);
                objObjective.score.scaled.initialize(objectiveObject.score.scaled);
                // extra code added
                objObjective.progress_measure.initialize(objectiveObject.progress_measure);

                // extra code added  START
                objObjective.objectiveProgressStatus.initialize(objectiveObject.objectiveProgressStatus);
                objObjective.objectiveSatisfiedStatus.initialize(objectiveObject.objectiveSatisfiedStatus);
                objObjective.objectiveMeasureStatus.initialize(objectiveObject.objectiveMeasureStatus);
                objObjective.objectiveNormalizedMeasure.initialize(objectiveObject.objectiveNormalizedMeasure);
                objObjective.attemptProgressStatus.initialize(objectiveObject.attemptProgressStatus);
                objObjective.attemptCompletionStatus.initialize(objectiveObject.attemptCompletionStatus);
                objObjective.objectiveContributestoRollup.initialize(objectiveObject.objectiveContributestoRollup);

                // end
                this.scorm2004Api.appendToCollection(dataModel.objectives, objObjective);

            }
        }
    }
    else {
        var scoDataModelIndex = null;

        if (scoIdentifier != "DefaultIdentifierName") {
            for (var i = 0; i < this.scorm12Api.courseDataModel.length; i++) {
                var items = this.scorm12Api.imsManifest.organizations[0].items;
                if (this.scorm12Api.courseDataModel[i].scoIdentifier == scoIdentifier && items[i].identifierref != undefined && items[i].identifierref != null) {
                    scoDataModelIndex = i;
                }
            }
        }
        else {
            scoDataModelIndex = 0;
        }

        if (scoDataModelIndex == null) {
            return;
        }

        var dataModel = this.scorm12Api.courseDataModel[scoDataModelIndex].cmi;

        // start with all the top-level text nodes - if value is null, the initialize will not happen       

        // WIDGET
        // populate entry with injected data if there is injected data and this is single-sco; otherwise, use persisted data
        if (this.injectedLearnerPersistedData.entry != null && this.scorm12Api.courseDataModel.length == 2) {
            dataModel.core.entry.initialize(this.injectedLearnerPersistedData.entry);
        }
        else {
            dataModel.core.entry.initialize(persistedData.entry);
        }
        
        dataModel.core.exit.initialize(persistedData.exit);
        dataModel.core.student_id.initialize(persistedData.learner_id);
        dataModel.core.student_name.initialize(persistedData.learner_name);
        dataModel.core.lesson_location.initialize(persistedData.lesson_location);        

        // if suspend data value is saved as json object then replace single quote with double quote to make it parsable
        if (persistedData.suspend_data != null) {
            dataModel.suspend_data.initialize(persistedData.suspend_data.replace(new RegExp("'", 'g'), '"'));
        }
        else {
            dataModel.suspend_data.initialize(persistedData.suspend_data);
        }
        
        // WIDGET
        // populate status with injected data if there is injected data and this is single-sco; otherwise, use persisted data
        if ((this.injectedLearnerPersistedData.completion_status != null || this.injectedLearnerPersistedData.success_status != null) && this.scorm12Api.courseDataModel.length == 2) {
            if (this.injectedLearnerPersistedData.success_status == "passed" || this.injectedLearnerPersistedData.success_status == "failed") {
                dataModel.core.lesson_status.initialize(this.injectedLearnerPersistedData.success_status);
            }
            else {
                dataModel.core.lesson_status.initialize(this.injectedLearnerPersistedData.completion_status);
            }
        }
        else {
            dataModel.core.lesson_status.initialize(persistedData.lesson_status);
        }

        // WIDGET
        // populate mode with injected data if there is injected data and we do not already have a completed or passed
        // if we have completed or passed, set mode to "review"
        if (this.injectedLearnerPersistedData.mode != null) {
            if (dataModel.core.lesson_status.value == "completed" || dataModel.core.lesson_status.value == "passed") {
                dataModel.core.lesson_mode.initialize("review");
            }
            else {
                dataModel.core.lesson_mode.initialize(this.injectedLearnerPersistedData.mode);
            }
        }
        
        // handle the total_time which is delivered in seconds
        this.scorm12Api.lastLaunchTotalTimeInMS = Number(persistedData.total_time) * 1000;
        // store the data model element as a TimeInterval
        dataModel.core.total_time.initialize(this.scorm12Api.msToTimeInterval(this.scorm12Api.lastLaunchTotalTimeInMS));        

        // score        

        // WIDGET
        // populate score with injected data if there is injected data and this is single-sco; otherwise, use persisted data
        if (this.injectedLearnerPersistedData.score.min != null && this.injectedLearnerPersistedData.score.max != null && this.injectedLearnerPersistedData.score.raw != null && this.injectedLearnerPersistedData.score.scaled != null && this.scorm12Api.courseDataModel.length == 2) {
            dataModel.core.score.max.initialize(this.injectedLearnerPersistedData.score.max);
            dataModel.core.score.min.initialize(this.injectedLearnerPersistedData.score.min);
            dataModel.core.score.raw.initialize(this.injectedLearnerPersistedData.score.raw);
        }
        else {            
            dataModel.core.score.max.initialize(persistedData.score.max) ;
            dataModel.core.score.min.initialize(persistedData.score.min);
            dataModel.core.score.raw.initialize(persistedData.score.raw);
        }

        // get interactions
        if (persistedData.interactions.length == null) {
            interactionsLength = 0;
        }
        else {
            // bellow if condition is for handling the previously launched old scorm 1.2 packages,those saved json 
            // with default "interaction" as its first item due to early implementation, and like wise we need to  
            // handle the correct responce and objectives arrays first item.
            if (persistedData.interactions[0] == "interaction") {
                persistedData.interactions.shift();
            }

            interactionsLength = persistedData.interactions.length;
        }

        if (interactionsLength > 0) {

            for (var x = 0; x < persistedData.interactions.length; x++) {
                var interactionObject = persistedData.interactions[x];

                var objInteraction = new Scorm12Interaction();

                objInteraction.id.initialize(interactionObject.id);
                objInteraction.type.initialize(interactionObject.type);
                objInteraction.time.initialize(interactionObject.timestamp);
                objInteraction.weighting.initialize(interactionObject.weighting);
                objInteraction.student_response.initialize(interactionObject.student_response);
                objInteraction.result.initialize(interactionObject.result);

                // latency arrives as seconds, so convert to TimeInterval
                var latencyAsSeconds = interactionObject.latency;
                if (!isNaN(latencyAsSeconds)) {
                    latencyAsSeconds = Number(latencyAsSeconds);
                    var latencyAsTimeInterval = this.scorm12Api.msToTimeInterval(latencyAsSeconds * 1000);

                    objInteraction.latency.initialize(latencyAsTimeInterval);
                }

                if (interactionObject.objectives.length == null) {
                    interactionObjectivesLength = 0;
                }
                else {
                    interactionObjectivesLength = interactionObject.objectives.length;
                }

                if (interactionObjectivesLength > 0) {

                    for (var y = 0; y < interactionObject.objectives.length; y++) {
                        var interactionObjectivesObject = interactionObject.objectives[y];

                        var objInteractionObjective = new Scorm12InteractionObjective();

                        objInteractionObjective.id.initialize(interactionObjectivesObject.id);

                        this.scorm12Api.appendToCollection(objInteraction.objectives, objInteractionObjective);
                    }
                }

                if (interactionObject.correct_responses.length == null) {
                    interactionsCorrectResponsesLength = 0;
                }
                else {
                    if (interactionObject.correct_responses[0] == "correct_response") {
                        interactionObject.correct_responses.shift();
                    }

                    interactionsCorrectResponsesLength = interactionObject.correct_responses.length;
                }

                if (interactionsCorrectResponsesLength > 0) {

                    for (var y = 0; y < interactionObject.correct_responses.length; y++) {
                        var correctResponseObject = interactionObject.correct_responses[y];

                        var objCorrectResponse = new Scorm12InteractionCorrectResponse();

                        objCorrectResponse.pattern.initialize(correctResponseObject.pattern);

                        this.scorm12Api.appendToCollection(objInteraction.correct_responses, objCorrectResponse);
                    }
                }
                this.scorm12Api.appendToCollection(dataModel.interactions, objInteraction);
            }
        }
    }
});

/**
* Processes the loaded RTE Data into the appropriate data model
*/
var isActiveSCOIdentifierFound = false;

CourseLaunchController.method('populatePersistedDataGlobal', function (scoIdentifier, jsonData) {

    if (this.scorm12Api.schemaversion != "1.2") {
        var persistedData = JSON.parse(jsonData);
        var imsManifestOrganization = this.scorm2004Api.imsManifest.organizations[0];
        var itemCounter;
        for (itemCounter = 0; itemCounter < imsManifestOrganization.items.length; itemCounter++) {

            if (imsManifestOrganization.items[itemCounter].identifier == persistedData.activeSCOIdentifier) {
                isActiveSCOIdentifierFound = true;
                break;
            }
        }
        // saved sco identifier Item not found in manifest, hence returning
        if (!isActiveSCOIdentifierFound) {
            return;
        }

        // WIDGET
        // if persisted data entry is "ab-initio" and there has not been an injected entry value, set global entry to "ab-initio" and exit
        // setting value here will prevent the persisted data from being loaded for individual sco's
        if (persistedData.entry == "ab-initio" && this.injectedLearnerPersistedData.entry == null) {
            this.globalCmiEntry = "ab-initio";
            return;
        }

        this.scorm2004Api.activeSCOIdentifier = persistedData.activeSCOIdentifier;
        this.scorm2004Api.activeSCOIndex = persistedData.activeSCOIndex;
        this.scorm2004Api.previousSCOIdentifier = persistedData.previousSCOIdentifier;
        this.scorm2004Api.nextSCOIdentifier = persistedData.nextSCOIdentifier;

        // WIDGET
        // populate completion with either injected data or persisted data
        if (this.injectedLearnerPersistedData.completion_status != null) {
            this.scorm2004Api.completion_status = this.injectedLearnerPersistedData.completion_status;
        }
        else {
            this.scorm2004Api.completion_status = persistedData.completion_status;
        }

        // WIDGET
        // populate success with either injected data or persisted data
        if (this.injectedLearnerPersistedData.success_status != null) {
            this.scorm2004Api.success_status = this.injectedLearnerPersistedData.success_status;
        }
        else {
            this.scorm2004Api.success_status = persistedData.success_status;
        }

        // extra code added
        this.scorm2004Api.actualAttemptCount = persistedData.actualAttemptCount;
        this.scorm2004Api.effectiveAttemptCount = persistedData.effectiveAttemptCount;
        this.scorm2004Api.total_time = Number(persistedData.total_time == null ? 0 : persistedData.total_time); // persistedData.total_time; // this.scorm2004Api.msToTimeInterval(persistedData.total_time);   

        var activityAttemptCount = persistedData.activityAttemptCount;
        var availableChildrens = persistedData.availableChildrens;

        for (var index = 0; index < this.scorm2004Api.courseDataModel.length; index++) {
            this.scorm2004Api.courseDataModel[index].cmi.activityAttemptCount.value = activityAttemptCount[index];
            this.scorm2004Api.courseDataModel[index].cmi.availableChildren = availableChildrens[index];
        }

        var primaryObjectives = persistedData.primaryObjectives;

        if (primaryObjectives != null && primaryObjectives.length > 0) {
            for (var index = 0; index < primaryObjectives.length; index++) {

                var o = this.scorm2004Api.primaryObjectives[index];

                o.completion_status.value = primaryObjectives[index].completion_status;
                o.success_status.value = primaryObjectives[index].success_status;
                o.progress_measure.value = primaryObjectives[index].progress_measure;
                primaryObjectives[index].description = o.description.value;
                // extra code added  START
                o.objectiveProgressStatus.value = primaryObjectives[index].objectiveProgressStatus;
                o.objectiveSatisfiedStatus.value = primaryObjectives[index].objectiveSatisfiedStatus;
                o.objectiveMeasureStatus.value = primaryObjectives[index].objectiveMeasureStatus;
                o.objectiveNormalizedMeasure.value = primaryObjectives[index].objectiveNormalizedMeasure;
                o.attemptProgressStatus.value = primaryObjectives[index].attemptProgressStatus;
                o.attemptCompletionStatus.value = primaryObjectives[index].attemptCompletionStatus;
                o.objectiveContributestoRollup.value = primaryObjectives[index].objectiveContributestoRollup;
                o.attemptCompletionAmountStatus.value = primaryObjectives[index].attemptCompletionAmountStatus;
                // end

                // WIDGET
                // populate score with either injected data or persisted data
                if (this.injectedLearnerPersistedData.score.min != null && this.injectedLearnerPersistedData.score.max != null && this.injectedLearnerPersistedData.score.raw != null && this.injectedLearnerPersistedData.score.scaled != null) {
                    o.score =
                    {
                        max: this.injectedLearnerPersistedData.score.max,
                        min: this.injectedLearnerPersistedData.score.min,
                        raw: this.injectedLearnerPersistedData.score.raw,
                        scaled: this.injectedLearnerPersistedData.score.scaled
                    };
                }
                else {
                    o.score =
                    {
                        max: primaryObjectives[index].score.max,
                        min: primaryObjectives[index].score.min,
                        raw: primaryObjectives[index].score.raw,
                        scaled: primaryObjectives[index].score.scaled
                    };
                }
            }
        }


        var globalObjectives = persistedData.globalObjectives;

        if (globalObjectives != null && globalObjectives.length > 0) {
            for (var index = 0; index < globalObjectives.length; index++) {

                var o = this.scorm2004Api.globalObjectives[index];

                o.completion_status.value = globalObjectives[index].completion_status;
                o.success_status.value = globalObjectives[index].success_status;
                o.progress_measure.value = globalObjectives[index].progress_measure;
                globalObjectives[index].description = o.description.value;
                // extra code added START
                o.objectiveProgressStatus.value = globalObjectives[index].objectiveProgressStatus;
                o.objectiveSatisfiedStatus.value = globalObjectives[index].objectiveSatisfiedStatus;
                o.objectiveMeasureStatus.value = globalObjectives[index].objectiveMeasureStatus;
                o.objectiveNormalizedMeasure.value = globalObjectives[index].objectiveNormalizedMeasure;
                o.attemptProgressStatus.value = globalObjectives[index].attemptProgressStatus;
                o.attemptCompletionStatus.value = globalObjectives[index].attemptCompletionStatus;
                o.objectiveContributestoRollup.value = globalObjectives[index].objectiveContributestoRollup;
                o.attemptCompletionAmountStatus.value = globalObjectives[index].attemptCompletionAmountStatus;
                // end

                // WIDGET
                // populate score with either injected data or persisted data
                if (this.injectedLearnerPersistedData.score.min != null && this.injectedLearnerPersistedData.score.max != null && this.injectedLearnerPersistedData.score.raw != null && this.injectedLearnerPersistedData.score.scaled != null) {
                    o.score =
                    {
                        max: this.injectedLearnerPersistedData.score.max,
                        min: this.injectedLearnerPersistedData.score.min,
                        raw: this.injectedLearnerPersistedData.score.raw,
                        scaled: this.injectedLearnerPersistedData.score.scaled
                    };
                }
                else {
                    o.score =
                    {
                        max: globalObjectives[index].score.max,
                        min: globalObjectives[index].score.min,
                        raw: globalObjectives[index].score.raw,
                        scaled: globalObjectives[index].score.scaled
                    };
                }
            }
        }
    }
    else {
        var persistedData = JSON.parse(jsonData);
        var imsManifestOrganization = this.scorm12Api.imsManifest.organizations[0];

        for (var itemCounter = 0; itemCounter < imsManifestOrganization.items.length; itemCounter++) {

            if (imsManifestOrganization.items[itemCounter].identifier == persistedData.activeSCOIdentifier) {
                isActiveSCOIdentifierFound = true;
                break;
            }
        }
        // saved sco identifier Item not found in manifest, hence returning
        if (!isActiveSCOIdentifierFound) {
            return;
        }

        // WIDGET
        // if persisted data entry is "ab-initio" and there has not been an injected entry value, set global entry to "ab-initio" and exit
        // setting value here will prevent the persisted data from being loaded for individual sco's
        if (persistedData.entry == "ab-initio" && this.injectedLearnerPersistedData.entry == null) {
            this.globalCmiEntry = "ab-initio";
            return;
        }

        this.scorm12Api.activeSCOIdentifier = persistedData.activeSCOIdentifier;
        this.scorm12Api.activeSCOIndex = persistedData.activeSCOIndex;
        this.scorm12Api.previousSCOIdentifier = persistedData.previousSCOIdentifier;
        this.scorm12Api.nextSCOIdentifier = persistedData.nextSCOIdentifier;  

        // WIDGET
        // populate completion with either injected data or persisted data
        if (this.injectedLearnerPersistedData.completion_status != null) {
            this.scorm12Api.completion_status = this.injectedLearnerPersistedData.completion_status;
        }
        else {
            this.scorm12Api.completion_status = persistedData.completion_status;
        }

        // WIDGET
        // populate success with either injected data or persisted data
        if (this.injectedLearnerPersistedData.success_status != null) {
            this.scorm12Api.success_status = this.injectedLearnerPersistedData.success_status;
        }
        else {
            this.scorm12Api.success_status = persistedData.success_status;
        }

        this.scorm12Api.total_time = Number(persistedData.total_time == null ? 0 : persistedData.total_time);
    }
});

/**
* Finds the active SCO's index in the activity tree, sets its identifier 
* As the active sco's identifier, and its index in the API's manifest array
* As the active sco's index. Also, sets the previous and next sco's identifiers
* In the API. This method is used on the user's first experience of an activity
* Tree. This data is already presisted from the data save, but we refresh in
* Case of a change in the manifest
*/
CourseLaunchController.method('findActiveScoIndex', function () {
    // shorthand variable for manifest organization information from the API
    var imsManifestOrganization = this.scorm2004Api.imsManifest.organizations[0];

    // loop through the items of the organization to find the first deliverable
    // sco (the first one with an identifierref)
    for (var i = 0; i < imsManifestOrganization.items.length; i++) {
        if (imsManifestOrganization.items[i].identifier == this.scorm2004Api.activeSCOIdentifier) {
            // set this as the active sco
            this.scorm2004Api.activeSCOIdentifier = imsManifestOrganization.items[i].identifier;
            this.scorm2004Api.activeSCOIndex = i;
            this.scorm2004Api.nextSCOIdentifier = null;
            this.scorm2004Api.previousSCOIdentifier = null;

            // find the logically next item
            if (i + 1 < imsManifestOrganization.items.length) {
                for (var j = i + 1; j < imsManifestOrganization.items.length; j++) {
                    if (imsManifestOrganization.items[j].isvisible == true) {
                        this.scorm2004Api.nextSCOIdentifier = imsManifestOrganization.items[j].identifier;
                        break;
                    }
                }
            }

            // find the logically previous item
            if (i - 1 >= 0) {
                for (var j = i - 1; j >= 0; j--) {
                    if (imsManifestOrganization.items[j].isvisible == true) {
                        this.scorm2004Api.previousSCOIdentifier = imsManifestOrganization.items[j].identifier;
                        break;
                    }
                }
            }

            break;
        }
    }
});

/**
* Handles the states of all LMS UI provided navigation buttons. The display
* Of these buttons can be controlled through the manifest (at the item level)
* To show/hide them. Additionally, we can enable/disable them based on what
* The user is currently doing, and whether or not a specific navigation request
* Would make sense logically.
* More rules should go here for VALID REQUESTS (adl.nav.request.valid)
*/
CourseLaunchController.method('evaluateLMSUIButtonStates', function () {
    // shorthand variables for the LMS UI buttons
    var btnPrevious = document.getElementById("BtnPrevious");
    var btnContinue = document.getElementById("BtnContinue");
    var btnExitAll = document.getElementById("BtnExitAll");
    //var btnExitThisSCO = document.getElementById("BtnExitThisSCO");
    //var btnAbandon = document.getElementById("BtnAbandon");
    //var btnAbandonAll = document.getElementById("BtnAbandonAll");
    //var btnSuspendAll = document.getElementById("BtnSuspendAll");

    // shorthand variables for actice, previous, next SCO, and
    // manifest organization information from the API
    var activeSCOIdentifier = this.scorm2004Api.activeSCOIdentifier;
    var activeSCOIndex = this.scorm2004Api.activeSCOIndex;
    var previousSCOIdentifier = this.scorm2004Api.previousSCOIdentifier;
    var nextSCOIdentifier = this.scorm2004Api.nextSCOIdentifier;
    var imsManifestOrganization = this.scorm2004Api.imsManifest.organizations[0];

    // determine hidden states for all LMS UI elements based on the 
    // adlnav presentation information in the manifest for the active sco
    if (activeSCOIndex > 0) {
        var curAdlnavPresentation = imsManifestOrganization.items[activeSCOIndex].adlnavPresentation;

        if (curAdlnavPresentation != null) {
            for (var i = 0; i < curAdlnavPresentation.hideLMSUI.length; i++) {
                var elementToHide = curAdlnavPresentation.hideLMSUI[i];

                switch (elementToHide) {
                    case "previous":
                        btnPrevious.style.display = "none";
                        break;
                    case "continue":
                        btnContinue.style.display = "none";
                        break;
                    //case "exit":
                        //btnExitThisSCO.style.display = "none";
                        //break;
                    case "exitAll":
                        btnExitAll.style.display = "none";
                        break;
                    //case "abandon":
                        //btnAbandon.style.display = "none";
                        //break;
                    //case "abandonAll":
                        //btnAbandonAll.style.display = "none";
                        //break;
                    //case "suspendAll":
                        //btnSuspendAll.style.display = "none";
                        //break;
                    default:
                        break;
                }
            }
        }
    }


    // exit - only if there is an active sco
    if (activeSCOIdentifier != null) {
        btnExitAll.disabled = false;
        btnExitAll.style.cursor = "pointer";
    }

    // exit sco
    //btnExitThisSCO.disabled = false;
    //btnExitThisSCO.style.cursor = "pointer";

    // abandon - only if there is an active sco
    //if (activeSCOIdentifier != null) {
    //    btnAbandon.disabled = false;
    //    btnAbandon.style.cursor = "pointer";
    //}

    // abandon all - always
    //btnAbandonAll.disabled = false;
    //btnAbandonAll.style.cursor = "pointer";

    // suspend all - always
    //btnSuspendAll.disabled = false;
    //btnSuspendAll.style.cursor = "pointer";

    // for single SCO package disable previous and continue buttons
    if (imsManifestOrganization.items.length <= 2) {
        btnPrevious.style.display = "none";
        btnContinue.style.display = "none";

        //btnExitThisSCO.disabled = true;
        //btnExitThisSCO.style.display = "none";
    }    
});

/**
* Handles the states of all LMS UI provided navigation buttons. The display
* Of these buttons can be controlled through the manifest (at the item level)
* To show/hide them for scorm 1.2.
*/
CourseLaunchController.method('evaluateLMSUIButtonStatesForSco12', function () {
    try {
        var imsManifest = this.scorm12Api.imsManifest;
        var imsManifestOrganization = this.scorm12Api.imsManifest.organizations[0];
        var imsManifestResources = this.scorm12Api.imsManifest.resources;
        var itemIdentifier = null;
        var itemIdentifierref = null;

        // shorthand variables for the LMS UI buttons        
        var btnPrevious = document.getElementById("BtnPrevious");
        var btnContinue = document.getElementById("BtnContinue");
        var btnExitAll = document.getElementById("BtnExitAll");
        //var btnExitThisSCO = document.getElementById("BtnExitThisSCO");
        //var btnAbandon = document.getElementById("BtnAbandon");
        //var btnAbandonAll = document.getElementById("BtnAbandonAll");
        //var btnSuspendAll = document.getElementById("BtnSuspendAll");
        
        if (imsManifestOrganization != undefined && imsManifestOrganization.items.length > 0) {

            // shorthand variables for actice, previous, next SCO, and
            // manifest organization information from the API
            var activeSCOIdentifier = this.scorm12Api.activeSCOIdentifier;
            var activeSCOIndex = this.scorm12Api.activeSCOIndex;
            var previousSCOIndex = this.findPreviousSCOIndex();
            var nextSCOIndex = this.findNextSCOIndex();

            // show and hide next/previous button, if next and previous SCO is not available.
            if (previousSCOIndex != null && previousSCOIndex != undefined) {
                btnPrevious.style.cursor = "pointer";
                btnPrevious.style.display = "inline";
            }
            else {
                btnPrevious.style.cursor = "default";
                btnPrevious.style.display = "none";
            }

            // continue      
            if (nextSCOIndex != null && nextSCOIndex != undefined) {
                btnContinue.style.cursor = "pointer";
                btnContinue.style.display = "inline";
            }
            else {
                btnContinue.style.cursor = "default";
                btnContinue.style.display = "none";
            }
        }

        // exit all button
        btnExitAll.disabled = false;       
        btnExitAll.style.cursor = "pointer";
    }
    catch (e) {
        alert("error : " + e)
    }
});

/**
 * Function to find the previous available activity for scorm 1.2.
 */
CourseLaunchController.method('findPreviousSCOIndex', function () {
    var imsManifestOrganization = this.scorm12Api.imsManifest.organizations[0];
    var PreviousSCOIndex = null;
    if (this.scorm12Api.activeSCOIndex != null && this.scorm12Api.activeSCOIndex != undefined) {
        for (var i = this.scorm12Api.activeSCOIndex - 1; i >= 0; i--) {
            if (imsManifestOrganization.items[i].identifier != null && imsManifestOrganization.items[i].identifierref != null) {
                PreviousSCOIndex = i;
                return PreviousSCOIndex;
            }
        }
    }
    return PreviousSCOIndex;
});

/**
 * Function to find the next available activity for scorm 1.2.
 */
CourseLaunchController.method('findNextSCOIndex', function () {
    var imsManifestOrganization = this.scorm12Api.imsManifest.organizations[0];
    var NextSCOIndex = null;
    if (this.scorm12Api.activeSCOIndex != null && this.scorm12Api.activeSCOIndex != undefined) {
        for (var i = this.scorm12Api.activeSCOIndex + 1; i < imsManifestOrganization.items.length; i++) {
            if (imsManifestOrganization.items[i].identifier != null && imsManifestOrganization.items[i].identifierref != null) {
                NextSCOIndex = i;
                return NextSCOIndex;
            }
        }
    }
    return NextSCOIndex;
});

/**
* Processes lms initated nav requests
*/
CourseLaunchController.method('processLMSNavRequest', function (navRequest) {
    try {

        // used to close SCOTreeMenuContainer on scorm launcher page
        CloseContentMenu(true);
        
        if (this.scorm2004Api.imsManifest.schemaversion != "1.2") {
            this.scorm2004Api.SetValue('adl.nav.request', navRequest);
            this.scorm2004Api.scorm2004ADLNav.CheckMenu();
            this.evaluateLMSUIButtonStates();
        }
        else {
            if (navRequest == "exitAll") {
                var href = GLOBAL.COURSE_INFO.ReturnUrl;
                SetIntervalBeforeExit(href, this);

                // display the loader modal popup
                ShowLoaderModalPopup(SavingContentStr);

                var url = "/launch/ContentPlaceHolder.html";
                SetContentFrameUrl(url);
            }
            else {
                this.SearchDeliverableActivityForScorm12(navRequest);
            }
        }
    }
    catch (e) {
        // doing nothing here. just making sure that that functions called below are executed even if some exception occurs
    }
});

/*
* This method is used to return the return url page when global data save is completed
* On exit LMS button click for scorm 1.2.
*/
function SetIntervalBeforeExit(url, courseLaunchController) {
    var timerCount = 0;
    var interval =
        setInterval(function () {
            timerCount++;

            // bellow Commented "if" condition was applied for some reason and was working fine for many scorm packages tested earlier but when 
            // tested for the newly created packages then failed to save the global data file as it satisfied the first condition much before 
            // the asinc SaveRTEdata in service complete saving the global json file and thus resulted in fail to save the global file along with database entry 
            // and thus need to remove this condition however need to be tested again against the previously tested packages.

            /*
            if (scorm12Instance.activeState == Scorm12Api.STATE.NotInitialized || scorm12Instance.globalSaveCompleted || timerCount == 10) {
            */

            if (scorm12Instance.globalSaveCompleted || timerCount >= 10) {

                // check if the redirecting to url without termination, then terminate first and then redirect 
                if (timerCount >= 10 && scorm12Instance.activeState == 1) {  //active state 1 is for running

                    timerCount = 0;
                    scorm12Instance.LMSFinish("");
                }
                else {
                    clearInterval(interval);

                    // if we are exiting, dispatch exit event and finish completed event
                    courseLaunchController.dispatchEvent(new Event(Event.DATA_SAVED_FOR_EXIT, "DATA SAVED FOR EXIT")); // WIDGET
                    scorm12Instance.dispatchEvent(new Event(Event.FINISH_COMPLETED, "Scorm12Api"));

                    if (url != null) {
                        window.location.href = url;
                    }
                    else {
                        HideLoadAndSaveContentModalPopup();
                    }
                }
            }
        }, 1000);
}

/*
* This method is used to load the destination resource page after global data save is completed
* On click of the previous/next lms buttons or tree activity link for scorm 1.2.
*/
function SetIntervalWhenGlobalSaveCompletedForScorm12(url, activeIndex, identifier) {
    var i = 0;
    var interval =
        setInterval(function () {
            i++;

            if (scorm12Instance.activeState == Scorm12Api.STATE.NotInitialized || scorm12Instance.globalSaveCompleted || i == 10) {
                clearInterval(interval);

                SetContentFrameUrl(url);

                // hide the loader modal popup
                HideLoadAndSaveContentModalPopup();

                scorm12Instance.activeSCOIndex = activeIndex;
                scorm12Instance.activeSCOIdentifier = identifier;
                scorm12Instance.courseLaunchController.EnableTreeItems();
                scorm12Instance.courseLaunchController.evaluateLMSUIButtonStatesForSco12();
            }
        }, 500);
}


/**
* Builds the URL to the active SCO, and then delivers it.
* This assumes that our SCO to be delivered has already been
* Made active.
*/
CourseLaunchController.method('deliverSCO', function () {
    // shorthand variables for manifest organization information from the API
    // and the index of the active sco (the one to be delivered)
    var imsManifest = this.scorm2004Api.imsManifest;
    var imsManifestOrganization = this.scorm2004Api.imsManifest.organizations[0];
    var imsManifestResources = this.scorm2004Api.imsManifest.resources;
    var activeSCOIndex = this.scorm2004Api.activeSCOIndex;
    if (activeSCOIndex != null) {
        // initalize the xmlBase's as empty strings
        var manifestXmlBase = "";
        var resourcesXmlBase = "";
        var resourceXmlBase = "";

        // get the item identifier and identifierref of the active sco and
        // initialize the resourceHref variable as an empty string
        var itemIdentifier = imsManifestOrganization.items[activeSCOIndex].identifier;
        var itemIdentifierref = imsManifestOrganization.items[activeSCOIndex].identifierref;
        var resourceHref = "";

        // if the manifest root has an xml base, grab it
        if (imsManifest.xmlBase != null) {
            manifestXmlBase = imsManifest.xmlBase;
        }

        // if the resources root has an xml base, grab it
        if (imsManifest.resourcesXmlBase != null) {
            resourcesXmlBase = imsManifest.resourcesXmlBase;
        }

        // loop through the resources to find the resource who's identifier
        // matches the item's identifierref
        for (var i = 0; i < imsManifestResources.length; i++) {
            if (itemIdentifierref == imsManifestResources[i].identifier) {
                resourceHref = imsManifestResources[i].href;

                // if this resource has an xml base, grab it
                if (imsManifestResources[i].xmlBase != null) {
                    resourceXmlBase = imsManifestResources[i].xmlBase;
                }
            }
        }

        // if there is no href for this resource, exit
        // to do, throw a fatal error
        if (resourceHref == "") {
            return;
        }

        // initialize the href with the content location, and xml bases    
        var href = GLOBAL.COURSE_INFO.ContentFolder + manifestXmlBase + resourcesXmlBase + resourceXmlBase + resourceHref;

        // THIS IS CAUSING PROBLEMS - 8/23/2018 JC
        //href = href.replace('//', '/');

        // grab the parameters from the item
        var parameters = imsManifestOrganization.items[activeSCOIndex].parameters;

        // logic to add any parameters to the url
        if (parameters != null) {
            // while ? or & is the first character of the parameters string, remove it
            while (parameters.indexOf("?") == 0 || parameters.indexOf("&") == 0) {
                parameters = parameters.substring(1);
            }

            // if the first character of the parameter is a #
            if (parameters.indexOf("#") == 0) {
                // if there is a # in the href, ignore the parameters           
                // else, append the parameters to the href
                if (href.indexOf("#") >= 0) {
                }
                else {
                    href = href + parameters;
                }


                // deliver the sco, and exit
                SetContentFrameUrl(href);

                // hide the loader modal popup before launching the content
                HideLoadAndSaveContentModalPopup();
                return;
            }

            // if there is a ? in the href, append a &
            // else, append a ?
            if (href.indexOf("?") >= 0) {
                href = href + "&";
            }
            else {
                href = href + "?";
            }

            // append the parameters to the href
            href = href + parameters;
        }
        // deliver the sco
        setTimeout(function () {

            SetContentFrameUrl(href);

            // hide the loader modal popup before launching the content
            HideLoadAndSaveContentModalPopup();
        }, 1000);
    }
});

/**
* When the SCORM API starts the Finish/Terminate process, the lesson window may close (if it hasn't already), 
* Since the lesson does not need to be displayed.
*/
//
CourseLaunchController.method('onApiFinishStarted', function () {
    this.updateMessageOnPage(CourseLaunchController.PAGE_MESSAGES.PROCESSING);

    this.apiFinishStarted = true;
});

/**
* When the SCORM API's Finish/Terminate process completes, we are done, so redirect to the launch page.
*/
//
CourseLaunchController.method('onApiFinishCompleted', function () {    
    // abort all worker objects, particularly to stop the ServerSessionPersister stops.
    this.abortAll();

    // set the isRunning flag to false so this window will not warn that data may be lost, since at this point all data is saved.
    this.isRunning = false;

    this.updateMessageOnPage(CourseLaunchController.PAGE_MESSAGES.DONE);

    this.dispatchEvent(new Event(Event.TRACE, "CourseLaunchController", "Run-time has completed successfully."));

    // here we would add an automatic redirect under normal circumstances.
});

/**
* Handles Fatal Errors that are dispatched. Currently this does the exact same thing as onApiFinishCompleted does, but these
* Are kept separate in case changes will need to be made.
*/
CourseLaunchController.method('onFatalError', function () {
    // abort all worker objects, particularly to stop the ServerSessionPersister stops.
    this.abortAll();

    // set the isRunning flag to false so this window will not warn that data may be lost, since at this point a Fatal Error happened.
    this.isRunning = false;

    // update the message on the page
    this.updateMessageOnPage(CourseLaunchController.PAGE_MESSAGES.FATAL_ERROR);

    this.dispatchEvent(new Event(Event.TRACE, "CourseLaunchController", "Run-time has ended with a fatal error."));
});

/**
* Aborts all objects from processing and mark the APIs as aborted so they do not accept any 
* Subsequent calls from the lesson. This is mainly to prevent any JavaScript errors that might
* Appear when the launch page redirects if XHRs are still in progress or if the SCO still tries 
* To make calls to the SCORM API.
*/
CourseLaunchController.method('abortAll', function () {
    this.rteDataLoader.abort();
    this.rteDataSaver.abort();
    //this.serverSessionPersister.abort();

    if (this.activeApi)
        this.activeApi.abort();
});

/**
* Called to redirect back to the return URL.
*/
CourseLaunchController.method('exit', function () {
    //alert("exit button clicked" + this.returnURL);
    //window.location = this.returnURL;    
    // window.open('', '_self').close();
});

/*
* FillGlobalObjectives()
* This method is used to Fill Global objectives from manifest file
*/
CourseLaunchController.prototype.FillGlobalObjectives = function () {
    var globalObjectives = new Array();
    var manifestItems = this.scorm2004Api.imsManifest.organizations[0].items;
    var alreadyExists = false;
    for (var itemIndex = 1; itemIndex < manifestItems.length; itemIndex++) {
        var itemObjectives = this.scorm2004Api.scorm2004ADLNav.GetItemObjectivesFromManifest(itemIndex, this.scorm2004Api.scorm2004ADLNav.cEnum.ObjectiveTypes.all);

        var adlseqItemObjectives = this.scorm2004Api.scorm2004ADLNav.GetAdlSeqItemObjectives(itemIndex);

        for (var objectiveIndex = 0; objectiveIndex < itemObjectives.length; objectiveIndex++) {
            var mapInfo = itemObjectives[objectiveIndex].mapInfo;

            if (mapInfo != null && mapInfo.length > 0) {
                for (var k = 0; k < mapInfo.length; k++) {
                    alreadyExists = false;
                    for (var l = 0; l < globalObjectives.length; l++) {
                        if (globalObjectives[l].id.value == mapInfo[k].targetObjectiveId) {
                            alreadyExists = true;
                            break;
                        }
                    }

                    if (!alreadyExists) {
                        var scorm2004Objective = new Scorm2004Objective();

                        scorm2004Objective.id.value = mapInfo[k].targetObjectiveId;
                        globalObjectives.push(scorm2004Objective);
                    }
                }
            }
        }

        for (var adlObjectiveIndex = 0; adlObjectiveIndex < adlseqItemObjectives.length; adlObjectiveIndex++) {
            var mapInfo = adlseqItemObjectives[adlObjectiveIndex].mapInfo;

            if (mapInfo != null && mapInfo.length > 0) {
                for (var k = 0; k < mapInfo.length; k++) {
                    alreadyExists = false;
                    for (var l = 0; l < globalObjectives.length; l++) {
                        if (globalObjectives[l].id.value == mapInfo[k].targetObjectiveId) {
                            alreadyExists = true;
                            break;
                        }
                    }

                    if (!alreadyExists) {
                        var scorm2004Objective = new Scorm2004Objective();

                        scorm2004Objective.id.value = mapInfo[k].targetObjectiveId;
                        globalObjectives.push(scorm2004Objective);
                    }
                }
            }
        }
    }

    this.scorm2004Api.globalObjectives = globalObjectives;
}


/*
* CreateDummyPrimaryObjectives()
* This method is used to Create Dummy Primary Objectives so they can be used when no primary objective is found
*/
CourseLaunchController.prototype.CreateDummyPrimaryObjectives = function () {
    try {
        var primaryObjectives = new Array();
        var manifestItems = this.scorm2004Api.imsManifest.organizations[0].items;

        for (var i = 0; i < manifestItems.length; i++) {

            var scorm2004Objective = new Scorm2004Objective();

            scorm2004Objective.id.value = "";
            scorm2004Objective.objectiveContributestoRollup.value = true;
            primaryObjectives.push(scorm2004Objective);
        }

        this.scorm2004Api.primaryObjectives = primaryObjectives;
    }
    catch (e) {
        this.scorm2004Api.traceError("Error caught in  CreateDummyPrimaryObjectives", "", 1);
    }
}

/*
* SetAvailableChildren()
* This method is used to Create Dummy Primary Objectives so they can be used when no primary objective is found
*/
CourseLaunchController.prototype.SetAvailableChildren = function () {
    try {
        var items = this.scorm2004Api.courseDataModel;
        for (var index = 0; index < items.length; index++) {
            items[index].cmi.availableChildren = this.scorm2004Api.scorm2004ADLNav.GetAvailableChildren(index);
        }
    }
    catch (e) {
        this.scorm2004Api.traceError("Error caught in  SetAvailableChildren", "", 1);
    }
}

/*
* This method is enable/disable the activity tree links for scorm 1.2 only.
*/
CourseLaunchController.prototype.EnableTreeItems = function () {
    try {
        var identifierPrefix = "__leaf__";
        var identifier;

        for (var index = 0; index < this.scorm12Api.imsManifest.organizations[0].items.length; index++) {

            var currentItem = this.scorm12Api.imsManifest.organizations[0].items[index];

            identifier = currentItem.identifier;

            var liId = document.getElementById((identifierPrefix + identifier));

            if (liId != null) {
                liId.attributes['itemIndex'] = index;
                if (index == this.scorm12Api.activeSCOIndex) {
                    liId.className = "SCOTreeMenuItemSelected";

                    liId.onclick = function () {
                        courseLaunchController.processLMSNavRequest(this.id.replace("__leaf__", ""));
                    }
                }
                else {
                    liId.className = "SCOTreeMenuItemEnabled";

                    liId.onclick = function () {
                        courseLaunchController.processLMSNavRequest(this.id.replace("__leaf__", ""));
                    }
                }

                if (this.isIdentifierRefExist(index) == false) {                    
                    liId.style.cursor = "default";
                    liId.className = "";

                    liId.onclick = function () { return false; };
                }
            }

        }
    }
    catch (e) {
        this.scorm12Api.traceError("Error caught in  EnableTreeItems", "", 1);
    }
}

/*
* This method checkes for the identifier ref value of any item in the manifest and returns true/false
* for scorm 1.2 .
*/
CourseLaunchController.prototype.isIdentifierRefExist = function (index) {

    if (this.scorm12Api.imsManifest.organizations[0].items[index].identifierref == null || this.scorm12Api.imsManifest.organizations[0].items[index].identifierref == undefined) {
        return false;
    }
    else {
        return true;
    }
}

/*
* This method takes item identifier as parameter and returns the index position of any activity in the manifest
* for scorm 1.2 .
*/
CourseLaunchController.prototype.GetItemIndex = function (itemIdentifier) {

    var index = 0;
    while (index < this.scorm12Api.imsManifest.organizations[0].items.length) {
        if (itemIdentifier == this.scorm12Api.imsManifest.organizations[0].items[index].identifier) {
            return index;
        }
        index++;
    }

}

/*
* This method takes navigation reuest as parameter and launche item according to the navigation 
* Request, if parameter value is null then this method launches the first available item/resource from the manifest
* for scorm 1.2 .
*/
CourseLaunchController.prototype.SearchDeliverableActivityForScorm12 = function (navRequest) {
    try {

        // shorthand variables for manifest organization information from the API
        // and the index of the active sco (the one to be delivered)
        var imsManifest = this.scorm12Api.imsManifest;
        var imsManifestOrganization = this.scorm12Api.imsManifest.organizations[0];
        var imsManifestResources = this.scorm12Api.imsManifest.resources;
        var activeIndex = null;
        var itemIdentifier = null;
        var itemIdentifierref = null;
        // initalize the xmlBase's as empty strings
        var manifestXmlBase = "";
        var resourcesXmlBase = "";
        var resourceXmlBase = "";

        // if the manifest root has an xml base, grab it
        if (imsManifest.xmlBase != null) {
            manifestXmlBase = imsManifest.xmlBase;
        }

        // if the resources root has an xml base, grab it
        if (imsManifest.resourcesXmlBase != null) {
            resourcesXmlBase = imsManifest.resourcesXmlBase;
        }

        var resourceHref = "";

        if (imsManifestOrganization != undefined) {
            // loop through the resources to find the resource who's identifier
            // matches the item's identifierref
            if (this.scorm12Api.activeSCOIndex == null) {
                for (j = 0; j <= imsManifestOrganization.items.length - 1; j++) {
                    activeIndex = j;
                    // get the item identifier and identifierref of the active sco and
                    // initialize the resourceHref variable as an empty string
                    itemIdentifier = imsManifestOrganization.items[activeIndex].identifier;
                    itemIdentifierref = imsManifestOrganization.items[activeIndex].identifierref;
                    if (itemIdentifier != null && itemIdentifierref != null) {
                        break;
                    }
                }
            }
            else {
                // get the item identifier and identifierref of the active sco and
                // initialize the resourceHref variable as an empty string
                if (navRequest != null && navRequest != undefined) {
                    if (navRequest == "previous") {
                        activeIndex = this.findPreviousSCOIndex();
                        itemIdentifier = imsManifestOrganization.items[activeIndex].identifier;
                        itemIdentifierref = imsManifestOrganization.items[activeIndex].identifierref;
                    }
                    else if (navRequest == "continue") {
                        activeIndex = this.findNextSCOIndex();
                        itemIdentifier = imsManifestOrganization.items[activeIndex].identifier
                        itemIdentifierref = imsManifestOrganization.items[activeIndex].identifierref;
                    }
                    else {
                        activeIndex = this.GetItemIndex(navRequest);
                        itemIdentifier = navRequest;
                        itemIdentifierref = imsManifestOrganization.items[activeIndex].identifierref;
                    }
                }
            }
            for (var i = 0; i < imsManifestResources.length; i++) {
                if (itemIdentifierref == imsManifestResources[i].identifier) {
                    resourceHref = imsManifestResources[i].href;
                    // if this resource has an xml base, grab it
                    if (imsManifestResources[i].xmlBase != null) {
                        resourceXmlBase = imsManifestResources[i].xmlBase;
                    }
                    break;
                }
            }
        }
        else {
            resourceHref = imsManifestResources[0].href;
        }
        // if there is no href for this resource, exit
        // to do, throw a fatal error
        if (resourceHref == "") {
            return;
        }
        ////
        var href;
        if (resourceHref.indexOf("www.") > -1) {
            // initialize the href with the content location, and xml bases    
            href = resourceHref;
        }
        else {
            href = GLOBAL.COURSE_INFO.ContentFolder + manifestXmlBase + resourcesXmlBase + resourceXmlBase + resourceHref;

            // THIS IS CAUSING PROBLEMS - 8/23/2018 JC
            // href = href.replace('//', '/');
        }


        if (imsManifestOrganization != undefined) {
            // grab the parameters from the item
            var parameters = imsManifestOrganization.items[activeIndex].parameters;

            // logic to add any parameters to the url
            if (parameters != null) {
                // while ? or & is the first character of the parameters string, remove it
                while (parameters.indexOf("?") == 0 || parameters.indexOf("&") == 0) {
                    parameters = parameters.substring(1);
                }

                // if the first character of the parameter is a #
                if (parameters.indexOf("#") == 0) {
                    // if there is a # in the href, ignore the parameters           
                    // else, append the parameters to the href
                    if (href.indexOf("#") >= 0) {
                    }
                    else {
                        href = href + parameters;
                    }

                    // deliver the sco, and exit
                    SetContentFrameUrl(href);

                    // hide the loader modal popup before launching the content
                    HideLoadAndSaveContentModalPopup();

                    return;
                }

                // if there is a ? in the href, append a &
                // else, append a ?
                if (href.indexOf("?") >= 0) {
                    href = href + "&";
                }
                else {
                    href = href + "?";
                }

                // append the parameters to the href
                href = href + parameters;
            }
        }
        // deliver the sco
        if (navRequest != null && navRequest != undefined) {
            this.scorm12Api.globalSaveCompleted = false;
            SetIntervalWhenGlobalSaveCompletedForScorm12(href, activeIndex, itemIdentifier);

            if (navRequest == "exitAll") {
                // display the loader modal popup
                ShowLoaderModalPopup(SavingContentStr);
            }
            else {
                // display the loader modal popup
                ShowLoaderModalPopup(LoadingContentStr);
            }

            var url = "/launch/ContentPlaceHolder.html";
            SetContentFrameUrl(url);
        }
        else {
            setTimeout(function () {
                SetContentFrameUrl(href);
                // hide the loader modal popup before launching the content
                HideLoadAndSaveContentModalPopup();
            }, 1000);

            this.scorm12Api.activeSCOIndex = activeIndex;
            this.scorm12Api.activeSCOIdentifier = itemIdentifier;
            this.EnableTreeItems();
            this.evaluateLMSUIButtonStatesForSco12();
        }
    }
    catch (e) {
        this.scorm12Api.traceError("Error caught in  SearchDeliverableActivityForScorm12", "", 1);
    }
}
