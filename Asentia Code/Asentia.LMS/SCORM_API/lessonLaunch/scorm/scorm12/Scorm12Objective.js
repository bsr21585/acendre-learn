﻿/**
 * Scorm12Objective
 *
 * Public Properties:
 *   correct_responses._count
 *   id
 *   score._children
 *   score.max
 *   score.min
 *   score.raw
 *   status
 */
function Scorm12Objective()
{
	//use shorthand references for convenience below
	var a = Scorm12DataModel.ACCESS;
	var t = Scorm12DataModel.DATA_TYPE;
	
	this.id		= new Scorm12DataItem(this,	a.ReadWrite,	t.CMIIdentifier);
	this.score =
	{
		_children	: new Scorm12DataItem(this,	a.ReadOnly,	t.CMIString255),
		max			: new Scorm12DataItem(this,	a.ReadWrite,	t.CMIDecimal,		"0:100"),
		min			: new Scorm12DataItem(this,	a.ReadWrite,	t.CMIDecimal,		"0:100"),
		raw			: new Scorm12DataItem(this,	a.ReadWrite,	t.CMIDecimal,		"0:100")
	}
	this.status	= new Scorm12DataItem(this,	a.ReadWrite,	t.CMIVocabulary,	t.CMIVocabulary.Status);
	
	//initialize values
	this.score._children.initialize(Scorm12DataModel.getChildPropertyNames(this.score), true);
	this.status.initialize("not attempted", true);
}
