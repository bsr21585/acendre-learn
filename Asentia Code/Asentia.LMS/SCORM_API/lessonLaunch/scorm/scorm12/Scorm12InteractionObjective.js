﻿/**
 * Scorm12InteractionObjective
 *
 * Public Properties:
 *   id
 */
function Scorm12InteractionObjective()
{
	//use shorthand references for convenience below
	var a = Scorm12DataModel.ACCESS;
	var t = Scorm12DataModel.DATA_TYPE;
	
	this.id = new Scorm12DataItem(this, a.WriteOnly, t.CMIIdentifier);
}
