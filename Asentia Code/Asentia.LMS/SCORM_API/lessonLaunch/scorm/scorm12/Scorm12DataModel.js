﻿/**
 * Scorm12DataModel
 *
 * Public Properties:
 * cmi. :
 *   core. :
 *      _children
 *      credit
 *      entry
 *      exit
 *      lesson_location
 *      lesson_mode
 *      lesson_status
 *      score._children
 *      score.max
 *      score.min
 *      score.raw
 *      session_time
 *      student_id
 *      student_name
 *      total_time
 *   interactions. :
 *      correct_responses._count
 *      correct_responses.n.pattern
 *      id
 *      latency
 *      objectives._count
 *      objectives.n.id
 *      result
 *      student_response
 *      time
 *      type
 *      weighting
 *   launch_data
 *   objectives. :
 *      _children
 *      _count
 *      n.id
 *      n.score._children
 *      n.score.max
 *      n.score.min
 *      n.score.raw
 *      n.status
 *   suspend_data
 *   student_data. :
 *      _children
 *      mastery_score
 *      max_time_allowed
 *      time_limit_action
 * 
 * Private Properties:
 *   interactions.__array
 *   interactions.__itemType
 *   objectives.__array
 *   objectives.__itemType
 * 
 * Private Methods:
 *   getChildPropertyNames()
 */

//STATIC CONSTANTS

Scorm12DataModel.ACCESS =
{
	ReadOnly	: 0,
	WriteOnly	: 1,
	ReadWrite	: 2
};

Scorm12DataModel.DATA_TYPE =
{
	CMIDecimal		: 0,
	CMIFeedback		: 1,//no validation for this
	CMIIdentifier	: 2,
	CMIInteger		: 3,
	CMIString255	: 4,
	CMIString4096	: 5,
	CMITime			: 6,
	CMITimespan		: 7,
	CMIVocabulary	:
	{
		Credit				: "credit|no-credit",
		Exit				: "time-out|suspend|logout|",
		Entry				: "ab-initio|resume|",
		Interaction			: "true-false|choice|fill-in|matching|performance|likert|sequencing|numeric",
		Mode				: "normal|review|browse",
		Result				: "correct|wrong|unanticipated|neutral|[CMIDecimal]",
		Status				: "passed|completed|failed|incomplete|browsed|not attempted",
		Time_Limit_Action	: "exit,message|exit,no message|continue,message|continue,no message"
	}
};


//CONSTRUCTOR

function Scorm12DataModel()
{
	//use shorthand references for convenience below
	var a = Scorm12DataModel.ACCESS;
	var t = Scorm12DataModel.DATA_TYPE;
	
	this.cmi = {};
	
	//declare the required-to-be-here, but not-currently-implemented data model elements
	this.cmi.comments = new Scorm12DataItem(this.cmi);
	
	//declare the implemented data model elements
	this.cmi.comments_from_lms	= new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMIString4096);
	this.cmi.core =
	{
		_children		: new Scorm12DataItem(this.cmi,	a.ReadOnly,		t.CMIString255),
		credit			: new Scorm12DataItem(this.cmi,	a.ReadOnly,		t.CMIVocabulary,	t.CMIVocabulary.Credit),
		entry			: new Scorm12DataItem(this.cmi,	a.ReadOnly,		t.CMIVocabulary,	t.CMIVocabulary.Entry),
		exit			: new Scorm12DataItem(this.cmi,	a.WriteOnly,	t.CMIVocabulary,	t.CMIVocabulary.Exit),
		lesson_location	: new Scorm12DataItem(this.cmi,	a.ReadWrite,	t.CMIString255),
		lesson_mode		: new Scorm12DataItem(this.cmi,	a.ReadOnly,		t.CMIVocabulary,	t.CMIVocabulary.Mode),
		lesson_status	: new Scorm12DataItem(this.cmi,	a.ReadWrite,	t.CMIVocabulary,	t.CMIVocabulary.Status),
		student_id		: new Scorm12DataItem(this.cmi,	a.ReadOnly,		t.CMIIdentifier),
		student_name	: new Scorm12DataItem(this.cmi,	a.ReadOnly,		t.CMIString255),
		score 			: 
		{
			_children		: new Scorm12DataItem(this.cmi,	a.ReadOnly,		t.CMIString255),
			max				: new Scorm12DataItem(this.cmi,	a.ReadWrite,	t.CMIDecimal,	"0:100"),
			min				: new Scorm12DataItem(this.cmi,	a.ReadWrite,	t.CMIDecimal,	"0:100"),
			raw				: new Scorm12DataItem(this.cmi,	a.ReadWrite,	t.CMIDecimal,	"0:100")
		},
		session_time	: new Scorm12DataItem(this.cmi,	a.WriteOnly,	t.CMITimespan), 
		total_time		: new Scorm12DataItem(this.cmi,	a.ReadOnly,		t.CMITimespan)
	};
	this.cmi.interactions =
	{
		__array			: new Array(),
		__itemType		: "Scorm12Interaction()",
		_children		: new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMIString255),
		_count			: new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMIInteger)
	};
	this.cmi.launch_data	= new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMIString4096);
	this.cmi.objectives =
	{
		__array			: new Array(),
		__itemType		: "Scorm12Objective()",
		_children		: new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMIString255),
		_count			: new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMIInteger)
	};
	this.cmi.suspend_data	= new Scorm12DataItem(this.cmi,	a.ReadWrite,	t.CMIString4096);
	this.cmi.student_data =
	{
		_children			: new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMIString255),
		mastery_score		: new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMIDecimal,	"0:100"),
		max_time_allowed	: new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMITimespan),
		time_limit_action 	: new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMIVocabulary,	t.CMIVocabulary.Time_Limit_Action)
	};
	this.cmi.student_preference =
	{
		_children			: new Scorm12DataItem(this.cmi,	a.ReadOnly,	t.CMIString255),
		audio				: new Scorm12DataItem(this.cmi),
		language			: new Scorm12DataItem(this.cmi),
		speed				: new Scorm12DataItem(this.cmi),
		text				: new Scorm12DataItem(this.cmi)
	};
	
	//set the simple _children property values
	this.cmi.core._children.initialize					(Scorm12DataModel.getChildPropertyNames(this.cmi.core));
	this.cmi.core.score._children.initialize			(Scorm12DataModel.getChildPropertyNames(this.cmi.core.score));
	this.cmi.student_data._children.initialize			(Scorm12DataModel.getChildPropertyNames(this.cmi.student_data));
	this.cmi.student_preference._children.initialize(Scorm12DataModel.getChildPropertyNames(this.cmi.student_preference));
		
	//set the _children properties of the collection objects (objs & ints) which are actually the children of the signular types
	this.cmi.objectives._children.initialize	(Scorm12DataModel.getChildPropertyNames(new Scorm12Objective()));
	this.cmi.interactions._children.initialize	(Scorm12DataModel.getChildPropertyNames(new Scorm12Interaction()));
	
	//initialize array _count properties to "0"
	this.cmi.interactions._count.initialize	("0", true);
	this.cmi.objectives._count.initialize	("0", true);
	
	//set default values
	//set session time but don't mark it as initialized
	this.cmi.core.session_time.value = "00:00:00"; //REQ_80.4

    //Added by chetu
	this.cmi.core.lesson_mode.initialize("normal");
}


//STATIC METHODS

/**
 * Returns a comma-delimited list of all of the specified dataElement's 
 * standard child property names, excluding the property called "_children". 
 * A non-standard child property starts with double underscores and will be
 * excluded as well.
 *
 * @param dataElement is not a Scorm12DataItem but is an Object in the data model
 * 			such as "[datamodel].core" that has a set of child property names 
 *			like "credit", "entry", "exit, etc.
 */
Scorm12DataModel.getChildPropertyNames = function(dataElement)
{
	var keys = "";
	var key;
	
	if (typeof dataElement == "object")
	{
		for (key in dataElement)
		{
			if (key != "_children" && key.substr(0, 2) != "__")
				keys += key + ",";
		}
		
		//strip the last comma
		if (keys.length > 0)
			keys = keys.substr(0, keys.length - 1);
	}
	
	return keys;
}
