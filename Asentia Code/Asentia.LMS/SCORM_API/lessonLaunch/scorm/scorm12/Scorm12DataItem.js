﻿/**
 * Scorm12DataItem
 * 
 * Constructor:
 *   new Scorm12DataItem([parent, accessLevel, dataType[, constraints]]);
 *   
 *   When instantiated with NO arguments, the element becomes a 'not implemented by LMS' element
 *
 * Public Properties:
 *   isImplementedByLMS
 *   isInitialized
 *   value
 *   accessLevel
 *   dataType
 *   constraints
 *   
 * Public Methods:
 *   initialize(value)
 */
function Scorm12DataItem(parent, accessLevel, dataType, constraints)
{
	this.parent				= parent;
	this.isImplementedByLMS	= (arguments.length > 1) ? true : false;
	this.isInitialized		= (arguments.length > 1) ? false : true; //initialize the non-implemented items
	this.value				= "";
	this.pendingValue		= "";
	this.accessLevel		= accessLevel;
	this.dataType			= dataType;
	this.constraints		= constraints;
}

//PUBLIC METHODS

/**
 * Initlaizes the value of this data item. If "dontFlagAsPropertySet" is true, 
 * the parent Data Model will not be notified that at least one property has been set.
 */
Scorm12DataItem.method('initialize', function(value, dontFlagAsPropertySet)
{
	//don't initialize if value is null	
	if (value == null)
		return;
	
	this.value = value;
	this.isInitialized = true;
	this.pendingValue = "";
	
	//set that at least one property in the containing object was initialized
	//i.e. if this is an interaction,  interaction.atLeastOnePropertySet = true
	if (!dontFlagAsPropertySet && !this.parent.atLeastOnePropertySet)
		this.parent.atLeastOnePropertySet = true;
});
