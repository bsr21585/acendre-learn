﻿/**
 * Scorm12Interaction
 *
 * Public Properties:
 *   correct_responses._count
 *   correct_responses.n.pattern
 *   id
 *   latency
 *   objectives._count
 *   objectives.n.id
 *   result
 *   student_response
 *   time
 *   type
 *   weighting
 *	
 * Private Properties:
 *	 correct_responses.__array
 *	 correct_responses.__itemType
 *   objectives.__array
 *   objectives.__itemType
 */
function Scorm12Interaction()
{
	//use shorthand references for convenience below
	var a = Scorm12DataModel.ACCESS;
	var t = Scorm12DataModel.DATA_TYPE;
	
	this.correct_responses =
	{
		__array			: new Array(),
		__itemType		: "Scorm12InteractionCorrectResponse()",
		_count			: new Scorm12DataItem(this,	a.ReadOnly,	t.CMIInteger)
	};
	this.id					= new Scorm12DataItem(this,	a.WriteOnly,	t.CMIIdentifier);
	this.latency			= new Scorm12DataItem(this,	a.WriteOnly,	t.CMITimespan);
	this.objectives =
	{
		__array			: new Array(),
		__itemType		: "Scorm12InteractionObjective()",
		_count			: new Scorm12DataItem(this,	a.ReadOnly,	t.CMIInteger)
	};
	this.result				= new Scorm12DataItem(this,	a.WriteOnly,	t.CMIVocabulary,	t.CMIVocabulary.Result);
	this.student_response	= new Scorm12DataItem(this,	a.WriteOnly,	t.CMIFeedback);
	this.time				= new Scorm12DataItem(this,	a.WriteOnly,	t.CMITime);
	this.type				= new Scorm12DataItem(this,	a.WriteOnly,	t.CMIVocabulary,	t.CMIVocabulary.Interaction);
	this.weighting			= new Scorm12DataItem(this,	a.WriteOnly,	t.CMIDecimal);
	
	//initialize array _count properties to "0"
	this.correct_responses._count.initialize	("0", true);
	this.objectives._count.initialize			("0", true);
}
