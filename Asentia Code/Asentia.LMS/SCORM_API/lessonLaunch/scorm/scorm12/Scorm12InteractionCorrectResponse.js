﻿/**
 * Scorm12InteractionCorrectResponse
 *
 * Public Properties:
 *   pattern
 */
function Scorm12InteractionCorrectResponse()
{
	this.pattern = new Scorm12DataItem(this, Scorm12DataModel.ACCESS.WriteOnly, Scorm12DataModel.DATA_TYPE.CMIFeedback);
}
