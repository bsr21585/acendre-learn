﻿/**
 * Scorm12Api
 * 
 * Public Properties:
 *    version
 * 
 * Public SCORM 1.2 Methods:
 *    LMSInitialize("")
 *    LMSFinish("")
 *    LMSCommit("")
 *    LMSGetValue(strDataElementName)
 *    LMSSetValue(strDataElementName, strDataElementValue)
 *    LMSGetLastError()
 *    LMSGetErrorString(pstrErrorCode)
 *    LMSGetDiagnostic(pstrParam)
 */

//uses util.EventDispatcher
//uses util.Function
//uses util.JSONLoader
//uses scorm.scorm12.Scorm12DataItem
//uses scorm.scorm12.Scorm12DataModel
//uses scorm.scorm12.Scorm12Interaction
//uses scorm.scorm12.Scorm12InteractionCorrectResponse
//uses scorm.scorm12.Scorm12InteractionObjective
//uses scorm.scorm12.Scorm12Objective



//STATIC CONFIG CONSTANTS

Scorm12Api.MAX_TIME_ALLOWED_INTERVAL_SECONDS = 5; //number of seconds between polling for the max-time-allowed (if set by the lesson)

Scorm12Api.TEST_SUITE_MODE = false; //true : when testing...adjusts for real world issues and test suite bugs.



//STATIC CONSTANTS (error code numbers are spec REQ's)

Scorm12Api.ERRORS =
{
    None: { code: "0", descr: "No error" },
    GeneralException: { code: "101", descr: "General exception" },
    InvalidArgument: { code: "201", descr: "Invalid argument error" },
    CannotHaveChildren: { code: "202", descr: "Element cannot have children" },
    CannotHaveCount: { code: "203", descr: "Element not an array - Cannot have count" }, //not per spec(2.1.1.2, 10.2), but required by test suite
    NotInitialized: { code: "301", descr: "Not initialized" },
    DataElementNotImp: { code: "401", descr: "Not implemented error" },
    InvalidSetValue: { code: "402", descr: "Invalid set value, element is a keyword" },
    DataElementReadOnly: { code: "403", descr: "Element is read only" },
    DataElementWriteOnly: { code: "404", descr: "Element is write only" },
    DataValueTypeMismatch: { code: "405", descr: "Incorrect Data Type" }
};

Scorm12Api.STATE =
{
    NotInitialized: 0,
    Running: 1,
    FinishStarted: 2,
    Finished: 3
};

Scorm12Api.DATA_STATE =
{
    Unsaved: 0,
    Saved: 1
};

Scorm12Api.COMMIT_STATE =
{
    NotCommitted: 0,
    Committing: 1,
    Committed: 2
};

Scorm12Api.RETURN_VALUE =
{
    True: "true",
    False: "false",
    Empty: ""
};



//CONSTRUCTOR 

function Scorm12Api(courseLaunchController) {
    //reference the launch controller
    this.courseLaunchController = courseLaunchController;

    this.alertAndConfirmWindow = window; //override this by calling setAlertConfirmOwner(someOtherWindow)

    //data model
    this.dataModel = new Scorm12DataModel();
    this.imsManifest = null;
    this.activeSCOIndex = null;
    this.activeSCOIdentifier = null;
    this.previousSCOIdentifier = null;
    this.nextSCOIdentifier = null;
    this.entry = "ab-initio";
    this.schemaversion = null;
    this.total_time = null;
    this.courseDataModel = new Array();
    this.completion_status = "unknown";
    this.success_status = "unknown";
    this.score =
            {
                max: null,
                min: null,
                raw: null,
            };


    //intialize the state variables
    this.activeState = Scorm12Api.STATE.NotInitialized;
    this.activeCommitState = Scorm12Api.COMMIT_STATE.NotCommitted;
    this.activeDataState = Scorm12Api.DATA_STATE.Saved;

    //initialize the error variable (REQ_3.1)
    this.activeError = Scorm12Api.ERRORS.None;

    //start the session timer
    this.sessionTimeStart = new Date();
    this.lastLaunchTotalTimeInMS = 0; //this is initialized in deserialze();
    this.timeoutDateTime = null;

    this.queueCommitOnSaverResponse = false;
    this.queueFinishOnSaverResponse = false;
    this.onFinishCompletedCalled = false;
    this.globalDataSaveInvoked = false; // this helps ensure that the global data save only happens one time
    this.globalSaveCompleted = false;

    this.callDatabaseSaveOnNextCommit = false;

    //public property "version" per SCORM 1.2 requirement.
    this.version = "Scorm12Api";
}

Scorm12Api.inherits(EventDispatcher);



//PUBLIC METHODS

/**
 * Call this to have alerts and confirms open from another window besides this one.
 */
Scorm12Api.method('setAlertConfirmOwner', function (windowHandle) {
    this.alertAndConfirmWindow = windowHandle;
});

/**
 * Called by the LessonLaunchController to find out if this API is either in the process of finishing or has already finished.
 */
Scorm12Api.method('isFinishingOrFinished', function () {
    return (this.activeState == Scorm12Api.STATE.FinishStarted || this.activeState == Scorm12Api.STATE.Finished);
});

/**
 * Called by the LessonLaunchController to save the data if it has not already been saved.
 * Simply calls LMSFinish.
 */
Scorm12Api.method('finishApi', function () {
    this.LMSFinish("");
});

/**
 * Called by the LessonLaunchController to prevent any subsequent public methods from being processed or even tracing events.
 */
Scorm12Api.method('abort', function () {
    this.isAborted = true;
});

/**
 * LMSInitialize("") 1_2_REQ_5
 *
 * @param pstrEmpty must be be empty-string.
 */
Scorm12Api.prototype.LMSInitialize = function (pstrEmpty) {
    if (this.isAborted)
        return;

    var methodName = "LMSInitialize";

    this.traceCall(methodName, pstrEmpty);

    //tell the launch controller that the SCORM 1.2 API is the active API.
    this.courseLaunchController.setActiveApi(this);

    try {
        //API can only be initialized once 1_2_REQ_5.6
        if (this.activeState != Scorm12Api.STATE.NotInitialized) {
            this.setActiveError(Scorm12Api.ERRORS.GeneralException, "LMSInitialize already called and cannot be called again.", 0);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        //pstrEmpty must be an empty string 1_2_REQ_5.1, 1_2_REQ_5.5
        //if (typeof pstrEmpty != "string" || pstrEmpty.length != 0) {
        //    this.setActiveError(Scorm12Api.ERRORS.InvalidArgument, "Expected argument is empty-string. Argument value is '" + pstrEmpty + "'. Argument type is " + (typeof pstrEmpty) + ".", 1);
        //    return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        //}

        //set the state to initialized (running)
        this.activeState = Scorm12Api.STATE.Running;

        //deserialize the data model based on the Lesson Data Loader's XML DOM
        //this.deserialize();

        //begin the session timer
        this.sessionTimeStart = new Date();

        //if a maximum time was set, begin the timer
        if (this.dataModel.cmi.student_data.max_time_allowed.isInitialized) {
            //calculate when the timeout should occurr
            var startTimeInMs = Number(this.sessionTimeStart);
            var timeoutInMs = this.timeIntervalToMs(this.courseDataModel[this.activeSCOIndex].cmi.student_data.max_time_allowed.value) - this.lastLaunchTotalTimeInMS;

            //if negative interval for some reason, timeout immediately
            if (timeoutInMs < 0)
                timeoutInMs = 0;

            //store the timeout date/time (not used for any purpose)
            this.timeoutDateTime = new Date(startTimeInMs + timeoutInMs);

            this.traceNote("Timeout at " + this.timeoutDateTime + "(time limit: " + this.courseDataModel[this.activeSCOIndex].cmi.student_data.max_time_allowed.value + "; total time: " + (this.lastLaunchTotalTimeInMS / 1000) + "s; remaining time limit: " + (timeoutInMs / 1000) + "s)");

            //start the time-out stop-watch - check every second to see if the time limit has been reached

            var thisInstance = this;
            this.checkTimeoutInterval = window.setInterval(
				function () {
				    thisInstance.checkTimeout();
				},
				Scorm12Api.MAX_TIME_ALLOWED_INTERVAL_SECONDS * 1000
			);
        }

        //LMSInitialize successful, reset error, Return true 1_2_REQ_5.2, 1_2_REQ_5.3
        this.setActiveError(Scorm12Api.ERRORS.None);
        return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.True);
    }
    catch (e) {
        //1_2_REQ_4.4
        this.traceNote("Caught an error in LMSInitialize: " + e);
        this.setActiveError(Scorm12Api.ERRORS.GeneralException, "", 1);
        return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
    }
};


/**
 * LMSFinish("") 1_2_REQ_6
 *
 * @param pstrEmpty must be be empty-string.
 */
Scorm12Api.prototype.LMSFinish = function (pstrEmpty) {    
    if (this.isAborted)
        return;

    var methodName = "LMSFinish";

    this.traceCall(methodName, pstrEmpty);

    try {
        //Reset the globalSaveCompleted flag
        this.globalSaveCompleted = false;

        //make sure only 1 Finish process happens simultaneously
        if (this.activeState != Scorm12Api.STATE.FinishStarted) {
            //API must not be finished 1_2_REQ_6.8
            if (this.activeState == Scorm12Api.STATE.Finished) {
                this.setActiveError(Scorm12Api.ERRORS.GeneralException, "LMSFinish already called and cannot be called again.", 0);
                return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
            }

            //API must have been initialized 1_2_REQ_6.6
            if (this.activeState == Scorm12Api.STATE.NotInitialized) {
                this.setActiveError(Scorm12Api.ERRORS.NotInitialized, "API has not been initialized.", 1);
                return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
            }

            pstrEmpty = pstrEmpty + "";

            //pstrEmpty must be an empty string 1_2_REQ_6.1, 1_2_REQ_6.5
            if (typeof pstrEmpty != "string" || pstrEmpty.length != 0) {
                this.setActiveError(Scorm12Api.ERRORS.InvalidArgument, "Expected argument is empty string. Argument value is '" + pstrEmpty + "'. Argument type is " + (typeof pstrEmpty) + ".", 1);
                return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
            }

            //set the state to Finish Started
            this.activeState = Scorm12Api.STATE.FinishStarted;

            //dispatch the FINISH_STARTED event so the controller can close the SCO window while it is processing
            this.dispatchEvent(new Event(Event.FINISH_STARTED, "Scorm12Api"));

            //if the RTEDataSaver is still processing, queue LMSFinish for when it returns
            if (this.activeCommitState == Scorm12Api.COMMIT_STATE.Committing) {
                this.queueFinishOnSaverResponse = true;

                this.traceNote("LMSFinish has been queued until the RTEDataSaver completes the pending LMSCommit.");
                this.setActiveError(Scorm12Api.ERRORS.None);
                return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.True);
            }

            //If the call to here was a result of a queued LMSFinish, trace this note and clear the queue flag
            if (this.queueFinishOnSaverResponse) {
                this.traceNote("This call resulted from a queued LMSFinish call.");
                this.queueFinishOnSaverResponse = false;

                if (this.queueCommitOnSaverResponse) {
                    this.traceNote("Queued LMSCommit call will be bypassed.");
                    this.queueCommitOnSaverResponse = false;
                }
            }

            //if LMSFinish was called as a result of a timeout...
            if (this.finishFromTimeout) {
                this.traceNote("LMSFinish triggerd by timeout.");
            }

            //LMSCommit 1_2_REQ_6.7
            if (this.LMSCommit("") == Scorm12Api.RETURN_VALUE.False) {
                //Return general exception
                this.setActiveError(Scorm12Api.ERRORS.GeneralException, "LMSCommit call made by LMSFinish returned false.", 0);
                this.activeState = Scorm12Api.STATE.Running;
                return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
            }
        }

        //Finish successful, reset error, Return true 1_2_REQ_6.3
        this.activeState = Scorm12Api.STATE.NotInitialized;
        this.setActiveError(Scorm12Api.ERRORS.None);
        return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.True);
    }
    catch (e) {
        //1_2_REQ_6.4
        this.traceNote("Caught an error in LMSFinish: " + e);
        this.setActiveError(Scorm12Api.ERRORS.GeneralException, "", 1);
        return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
    }
};

/**
 * LMSCommit("") 1_2_REQ_7
 *
 * @param pstrEmpty must be be empty-string.
 */
Scorm12Api.prototype.LMSCommit = function (pstrEmpty) {
    if (this.isAborted)
        return;

    var methodName = "LMSCommit";

    this.traceCall(methodName, pstrEmpty);

    try {
        //API must not be finished 1_2_REQ_6.8
        if (this.activeState == Scorm12Api.STATE.Finished) {
            this.setActiveError(Scorm12Api.ERRORS.GeneralException, "LMSFinish already called so LMSCommit cannot be called.", 0);            
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        //API must have been initilaized 1_2_REQ_7.6
        if (this.activeState == Scorm12Api.STATE.NotInitialized) {
            this.setActiveError(Scorm12Api.ERRORS.NotInitialized, "LMSInitialize has not been called so LMSCommit cannot be called.", 1);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        pstrEmpty = pstrEmpty + "";

        //pstrEmpty must be an empty string 1_2_REQ_7.1, 1_2_REQ_7.5
        if (typeof pstrEmpty != "string" || pstrEmpty.length != 0) {
            this.setActiveError(Scorm12Api.ERRORS.InvalidArgument, "Expected argument is empty string. Argument value is '" + pstrEmpty + "'. Argument type is " + (typeof pstrEmpty) + ".", 1);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        // if in browse or review mode, bypass commit
        if (this.courseDataModel[this.activeSCOIndex].cmi.core.lesson_mode.value == "browse" || this.courseDataModel[this.activeSCOIndex].cmi.core.lesson_mode.value == "review") {
            this.traceNote("Bypassing commit because mode is \"" + this.courseDataModel[this.activeSCOIndex].cmi.core.lesson_mode.value + "\".");
            this.setActiveError(Scorm12Api.ERRORS.None);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.True);
        }

        //Don't commit if LMSSetData hasn't been called since last Commit
        if (this.activeState != Scorm12Api.STATE.FinishStarted && this.activeDataState == Scorm12Api.DATA_STATE.Saved) {
            this.queueCommitOnSaverResponse = false;

            this.traceNote("Bypassing commit because no data has been set since the last call to LMSCommit.");
            this.setActiveError(Scorm12Api.ERRORS.None);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.True);
        }

        //If currently commiting, queue up a commit for when the RTEDataSaver completes the previous Commit.
        if (this.activeCommitState == Scorm12Api.COMMIT_STATE.Committing) {
            this.queueCommitOnSaverResponse = true;

            this.traceNote("LMSCommit has been queued until the RTEDataSaver completes the pending LMSCommit.");
            this.setActiveError(Scorm12Api.ERRORS.None);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.True);
        }

        //If the call to here was a result of a queued Commit, trace this note and clear the queue flag
        if (this.queueCommitOnSaverResponse) {
            this.traceNote("This call resulted from a queued LMSCommit call.");
            this.queueCommitOnSaverResponse = false;
        }

        //evaluate overrides
        this.evaluateOverrides("cmi.core.lesson_status");

        //lock the commit by setting the commit state
        this.activeCommitState = Scorm12Api.COMMIT_STATE.Committing;

        //serialize to the RTEDataSaver
        //save to the database only if this Commit call was called by Finish; save to XML file otherwise.
        var saveToDatabase = (this.activeState == Scorm12Api.STATE.FinishStarted);

        // override saveToDB if callDatabaseSaveOnNextCommit is true
        if (this.callDatabaseSaveOnNextCommit) {
            saveToDatabase = true;
        }

        this.serialize(saveToDatabase);

        this.setActiveError(Scorm12Api.ERRORS.None);

        // after we're done, reset callDatabaseSaveOnNextCommit to false
        this.callDatabaseSaveOnNextCommit = false;

        return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.True);
    }
    catch (e) {
        //1_2_REQ_7.3
        this.traceNote("Caught an error in LMSCommit: " + e);
        this.setActiveError(Scorm12Api.ERRORS.GeneralException, "", 1);
        return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
    }
};

/**
 * LMSGetValue(pstrDataElementName) 1_2_REQ_8
 *
 * @param pstrDataElementName string of the SCORM 1.2 data model element name
 * @return string of the data element's value
 */
Scorm12Api.prototype.LMSGetValue = function (pstrDataElementName) {
    if (this.isAborted)
        return;

    var methodName = "LMSGetValue";

    this.traceCall(methodName, pstrDataElementName);

    try {
        //API must not be finished 1_2_REQ_6.8
        if (this.activeState == Scorm12Api.STATE.Finished || this.activeState == Scorm12Api.STATE.FinishStarted) {
            this.setActiveError(Scorm12Api.ERRORS.GeneralException, "LMSFinish already called so LMSGetValue cannot be called.", 0);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.Empty);
        }

        //API must have been initialized 1_2_REQ_8.8
        if (this.activeState == Scorm12Api.STATE.NotInitialized) {
            this.setActiveError(Scorm12Api.ERRORS.NotInitialized, "LMSInitialize has not been called so LMSGetValue cannot be called.", 1);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.Empty);
        }

        //pstrDataElementName must be a defined data model element name 1_2_REQ_8.2.3
        if (this.isValidDataElementName(pstrDataElementName)) {
            //data element is defined so get the element for further validation
            var objDataElement = this.getDataElement(pstrDataElementName);
        }
        else if (this.isValidArrayDataElementName(pstrDataElementName)) {
            if (!this.isArrayDataElementIndexInRange(pstrDataElementName)) {
                this.setActiveError(Scorm12Api.ERRORS.InvalidArgument, "Index out of range.", 1);
                return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.Empty);
            }
            else {
                //array data element is defined and index is in range, so get the element for further validation
                var objDataElement = this.getArrayDataElement(pstrDataElementName, false);
            }
        }
        else {
            this.setActiveError(Scorm12Api.ERRORS.InvalidArgument, "Unknown data model element.", 1);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.Empty);
        }

        //pstrDataElementName must be a data model element implemented by the LMS 1_2_REQ_8.2.3
        if (!objDataElement.isImplementedByLMS) {
            this.setActiveError(Scorm12Api.ERRORS.DataElementNotImp, "LMSGetValue attempted to read a data element that is either not implemeted or is not defined in the SCORM 1.2 data model.", 0);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.Empty);
        }

        //pstrDataElementName must not be write-only 1_2_REQ_8.2.2
        if (objDataElement.accessLevel == Scorm12DataModel.ACCESS.WriteOnly) {
            this.setActiveError(Scorm12Api.ERRORS.DataElementWriteOnly, "", 0);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.Empty);
        }

        //exception: accumulate cmi.core.total_time when it is requested - REQ 1.9.4
        if (pstrDataElementName == "cmi.core.total_time") {
            objDataElement.initialize(this.msToTimeInterval(this.getTotalTimeInSeconds() * 1000));

            //TO DO:
            //this.reportToAPIInspector("cmi.core.total_time", this.dataModel.cmi.core.total_time.value);
        }

        //GetValue successful, reset error, return the value 1_2_REQ_8.2.1, 1_2_REQ_8.3, 1_2_REQ_8.6
        this.setActiveError(Scorm12Api.ERRORS.None);
        return this.traceReturn(methodName, objDataElement.value);
    }
    catch (e) {
        //1_2_REQ_8.7
        this.traceNote("Caught an error in LMSGetValue: " + e);
        this.setActiveError(Scorm12Api.ERRORS.GeneralException, "", 1);
        return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.Empty);
    }
};

/**
 * LMSSetValue(pstrDataElementName) 1_2_REQ_9
 * @param pstrDataElementName string of the SCORM 1.2 data model element name
 * @param pstrDataElementValue value that element should be set to
 * @return "true" if successful, "false" if not.
 */
Scorm12Api.prototype.LMSSetValue = function (pstrDataElementName, pstrDataElementValue) {
    if (this.isAborted)
        return;

    var methodName = "LMSSetValue";

    this.traceCall(methodName, pstrDataElementName, pstrDataElementValue);

    try {
        //API must not be finished 1_2_REQ_6.8
        if (this.activeState == Scorm12Api.STATE.Finished || this.activeState == Scorm12Api.STATE.FinishStarted) {
            this.setActiveError(Scorm12Api.ERRORS.GeneralException, "LMSFinish already called so LMSSetValue cannot be called.", 0);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        //API must have been initialized 1_2_REQ_9.3.12
        if (this.activeState == Scorm12Api.STATE.NotInitialized) {
            this.setActiveError(Scorm12Api.ERRORS.NotInitialized, "LMSInitialize has not been called so LMSSetValue cannot be called.", 1);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        var blnIsArrayElement = false;

        //pstrDataElementName must be a defined data model element name 1_2_REQ_9.3.3
        if (this.isValidDataElementName(pstrDataElementName)) {
            //data element is defined so get the element for further validation
            var objDataElement = this.getDataElement(pstrDataElementName);
        }
        else if (this.isValidArrayDataElementName(pstrDataElementName)) {
            if (!this.isArrayDataElementIndexInRange(pstrDataElementName)) {
                this.setActiveError(Scorm12Api.ERRORS.InvalidArgument, "Index out of range.", 1);
                return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
            }
            else {
                //array data element is defined and index is in range, so get the element for further validation
                var objDataElement = this.getArrayDataElement(pstrDataElementName, true);

                //set the blnIsArrayElement flag so in case validation errors occurr, we'll remove the element we added as a result of the above line
                blnIsArrayElement = true;
            }
        }
        else {
            //data element is not defined
            this.setActiveError(Scorm12Api.ERRORS.InvalidArgument, "Unknown data model element.", 1);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        //pstrDataElementName must be a data model element implemented by the LMS 1_2_REQ_9.3.3
        if (!objDataElement.isImplementedByLMS) {
            this.setActiveError(Scorm12Api.ERRORS.DataElementNotImp, "LMSSetValue attempted to write to a data element that is either not implemeted or is not defined in the SCORM 1.2 data model.", 1);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        //pstrDataElementName must not be read-only 1_2_REQ_9.3.2
        if (objDataElement.accessLevel == Scorm12DataModel.ACCESS.ReadOnly) {
            this.setActiveError(Scorm12Api.ERRORS.DataElementReadOnly, "", 0);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        if (Scorm12Api.TEST_SUITE_MODE)
            objDataElement.pendingValue = pstrDataElementValue;
        else
            objDataElement.pendingValue = String(pstrDataElementValue);

        //pstrDataElementValue must be valid data type for element pstrDataElementName 1_2_REQ_9.3.7
        if (!this.isDataValueTypeValid(objDataElement)) {
            objDataElement.pendingValue = null;

            if (blnIsArrayElement)
                this.undoArrayElementAppend(objDataElement);

            this.setActiveError(Scorm12Api.ERRORS.InvalidArgument, "Invalid data type.", 1);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        //pstrDataElementValue must be in the valid value range for element pstrDataElementName 1_2_REQ_9.3.7
        if (!this.isDataValueInRange(objDataElement, pstrDataElementName)) {
            objDataElement.pendingValue = null;

            if (blnIsArrayElement)
                this.undoArrayElementAppend(objDataElement);

            this.setActiveError(Scorm12Api.ERRORS.InvalidArgument, "Data value is out of range.", 1);
            return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
        }

        //set the element's value & mark the element as 'initialized'
        var previousValue = objDataElement.value;
        objDataElement.initialize(objDataElement.pendingValue);
        objDataElement.pendingValue = previousValue;

        //evaluate any relations for this
        this.evaluateOverrides(pstrDataElementName);

        objDataElement.pendingValue = null;

        //TO DO:
        //this.reportToAPIInspector(pstrDataElementName, objDataElement.value);

        //set the data state to Unsaved
        this.activeDataState = Scorm12Api.DATA_STATE.Unsaved;

        //update the status of the activity's check box in the activity tree
        ManageTreeButtonsAndSCOStatus();

        // fill the updated over all package values in global data model
        this.fillGlobalModelValuesForScorm12();
        
        // if this is a single sco package, and status is just changing to "completed" or "passed", then set the flag to save to database on next commit        
        if(this.courseDataModel.length == 2 && pstrDataElementName == "cmi.core.lesson_status" && (previousValue == "unknown" || previousValue == "incomplete") && (objDataElement.value == "completed" || objDataElement.value == "passed")) {
            this.callDatabaseSaveOnNextCommit = true;
        }

        //SetValue successful, reset error, return true 1_2_REQ_9.2, 1_2_REQ_9.3.10
        this.setActiveError(Scorm12Api.ERRORS.None);
        return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.True);
    }
    catch (e) {
        //1_2_REQ_9.2, 1_2_REQ_9.3.11
        this.traceNote("Caught an error in LMSSetValue: " + e);
        this.setActiveError(Scorm12Api.ERRORS.GeneralException, "", 1);
        return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.False);
    }
};

/**
 * LMSGetLastError() 1_2_REQ_10
 */
Scorm12Api.prototype.LMSGetLastError = function () {
    if (this.isAborted)
        return;

    var methodName = "LMSGetLastError";

    this.traceCall(methodName);

    return this.traceReturn(methodName, this.activeError.code);
};

/**
 * LMSGetErrorString(pstrErrorCode) 1_2_REQ_11
 */
Scorm12Api.prototype.LMSGetErrorString = function (pstrErrorCode) {
    if (this.isAborted)
        return;

    pstrErrorCode = String(pstrErrorCode);

    var methodName = "LMSGetErrorString";

    this.traceCall(methodName, pstrErrorCode);

    var errorString = "";

    //return the description of the specified error code 1_2_REQ_11.2
    for (var errItem in Scorm12Api.ERRORS) {
        if (Scorm12Api.ERRORS[errItem].code == pstrErrorCode) {
            return this.traceReturn(methodName, Scorm12Api.ERRORS[errItem].descr);
        }
    }

    //If unknown error code, return empty string
    return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.Empty);
};

/**
 * LMSGetDiagnostic(pstrParam) 1_2_REQ_12
 * Implemeneted but not used for anything yet.
 */
Scorm12Api.prototype.LMSGetDiagnostic = function (pstrParam) {
    if (this.isAborted)
        return;

    var methodName = "LMSGetDiagnostic";

    this.traceCall(methodName, pstrParam);

    return this.traceReturn(methodName, Scorm12Api.RETURN_VALUE.Empty);
};



//PRIVATE METHODS

/**
 * Called to format a Scorm API method call then dispatch it as a trace.
 * Arguments are: (functionName[,arg1,...,argN])
 * Arguments which are not strings are made red and bold.
 */
Scorm12Api.prototype.traceCall = function (functionName) {
    var line = "<b>" + functionName + "</b> (";

    for (var i = 1; i < arguments.length; i++) {
        //all arguments should be of type String, so if not, show them as red/bold/not-in-quotes.
        if (typeof arguments[i] == "string")
            line += '"<span style="color: #009900">' + StringHelper.escapeTags(arguments[i]) + '</span>"';
        else
            line += '<span style="color: #FF0000"><b>' + arguments[i] + '</b></span>';

        if (i < arguments.length - 1)
            line += ", ";
    }

    line += ")";

    this.dispatchEvent(new Event(Event.TRACE, "Scorm12Api", line));
};

/**
 * Called to dispatch a TRACE event. 
 * @param functionName is the name of the function
 * @param returnVal is one of the items in the Scorm12Api.RETURN_VALUE set
 * @return returnVal - the same as the input.
 */
Scorm12Api.prototype.traceReturn = function (functionName, returnVal) {
    var line = "return " + functionName + ": ";

    //all arguments should be of type String, so if not, show them as red/bold/not-in-quotes.
    if (typeof returnVal == "string")
        line += '"' + StringHelper.escapeTags(returnVal) + '"';
    else
        line += '<span color="#FF0000"><b>' + returnVal + '</b></span>';

    this.dispatchEvent(new Event(Event.TRACE, "Scorm12Api", line));

    return returnVal;
};

/**
 * Called to dispatch a generic TRACE event.
 */
Scorm12Api.prototype.traceNote = function (line) {
    this.dispatchEvent(new Event(Event.TRACE, "Scorm12Api", line));
};

/**
 * Sets the current error in the API, and dispatches an error event for logging.
 * @param err - any item within the set of Scorm12Api.ERRORS
 * @param details - a string of details to be included in the dispatched event
 * @param warningOrError - 0 = warning; 1 = error. This determined the type of event dispatched.
 */
Scorm12Api.prototype.setActiveError = function (err, details, warningOrError) {
    this.activeError = err;

    //don't bother dispatching an event if the error set is "None" for no-error.
    if (err != Scorm12Api.ERRORS.None) {
        this.traceError(err.code + " - " + err.descr + ". " + details, "Scorm12Api", warningOrError);
    }
};

/**
 * Dispatches an error/warning event for logging purposes.
 * @param details - a string of details to be included in the dispatched event
 * @param source - a string representing the location of the error
 * @param warningOrError - 0 = warning; 1 = error. This determined the type of event dispatched.
 */
Scorm12Api.prototype.traceError = function (details, source, warningOrError) {
    this.dispatchEvent(new Event((warningOrError == 1 ? Event.ERROR : Event.WARNING), source, details));
}

/**
 * Does an alert via the configured window, or if that fails, just a regular alert.
 */
Scorm12Api.prototype.doJSAlert = function (str) {
    try {
        this.alertAndConfirmWindow.alert(str);
    }
    catch (e) {
        alert(str);
    }
};

/**
 * Called by LMSInitialize to restore the previous state based on the XML that was loaded by the RTEDataLoader.
 * This is not called if the RTEDataLoader has any type of HTTP or XML Status error -- those are handled by the RTEDataSaver.
 */
Scorm12Api.prototype.deserialize = function () {
    /*
	The format of a successful response is:
	
	<serv_response>
		<head>
			<status>success</status>  this is checked by the RTEDataLoader.
		</head>
		<body>
			<token>asdfa23324dsf</token>  this is extracted by the RTEDataLoader.
			<cmi>
				<completion_status>status</completion_status>  		if not null, maps to cmi.core.lesson_status
				<entry>entry</entry> (present only if initialized) 	maps to cmi.core.entry
				<interactions>
					<interaction>
						<id></id>
						<type></type>
						<objectives>
							<objective>
								<id></id>
							</objective>
						</objectives>
						<timestamp></timestamp>					maps to time
						<correct_responses>
							<correct_response>
								<pattern></pattern>
							</correct_response>
						</correct_responses>
						<weighting></weighting>
						<learner_response></learner_response>	maps to student_response
						<result></result>
						<latency></latency>
					</interaction>
				</interactions>
				<launch_data></launch_data>
				<learner_id></learner_id>						maps to core.student_id
				<learner_name></learner_name>					maps to core.student_name
				<location></location>							maps to core.lesson_location
				<max_time_allowed></max_time_allowed>			maps to student_data.max_time_allowed
				<mode></mode>									maps to core.lesson_mode
				<credit></credit>								maps to core.credit
				<objectives>
					<objective>
						<id></id>
						<score>
							<raw></raw>
							<min></min>
							<max></max>
						</score>
						<success_status></success_status>		if not null, maps to status
						<completion_status></completion_status>	if not null, maps to status
					</objective>
				</objectives>
				<score>
					<raw></raw>									maps to core.score.raw
					<min></min>									maps to core.score.min
					<max></max>									maps to core.score.max
				</score>
				<success_status></success_status>				maps to core.lesson_status
				<suspend_data></suspend_data>
				<total_time></total_time>						core.total_time
			</cmi>
		</body>
	</serv_response>
	
	This function would NOT be called if the XML was a failed response, because the RTEDataLoader handles that now.
	*/

    this.activeDataModel = null;
    this.activeDataModel = new Scorm2004DataModel();

    var c = this.activeDataModel.cmi;
    var pc = this.courseDataModel[this.activeSCOIndex].cmi;

    c.launch_data.initialize(pc.launch_data.value);    
    c.suspend_data.initialize(pc.suspend_data.value);

    //start with all the top-level text nodes - if value is null, the initialize will not happen

    c.core.credit.initialize(pc.core.credit.value);
    c.core.entry.initialize(pc.core.entry.value);
    c.core.exit.initialize(pc.core.exit.value);

    //note: <location> is a tag that is not allowed in the XMLHTTPRequest object (for some dumb reason)
    c.core.lesson_location.initialize(pc.core.lesson_location.value);


    c.core.lesson_mode.initialize(pc.core.lesson_mode.value);

    //status comes in as success_status and compeletion_status, but these need to be converted to just lesson_status for SCORM 1.2
    var completionStatus = pc.completion_status.value;
    var successStatus = pc.success_status.value;

    if (completionStatus != "unknown")
        c.core.lesson_status.initialize(completionStatus);
    else if (successStatus != "unknown")
        c.core.lesson_status.initialize(successStatus);
    else
        c.core.lesson_status.initialize("not attempted");

    c.core.student_id.initialize(pc.core.student_id);
    c.core.student_name.initialize(pc.core.student_name);

    //mastery score comes in between 0 and 1 but needs to be converted to 0 and 100.
    var masteryScore = pc.scaled_passing_score.value;
    if (masteryScore && masteryScore.length > 0) {
        masteryScore = String(Number(masteryScore) * 100);
        c.student_data.mastery_score.initialize(masteryScore);
    }

    c.student_data.time_limit_action.initialize(pc.student_data.time_limit_action.value);

    //handle the max_time_allowed which is delivered in seconds
    var maxTime = pc.student_data.max_time_allowed.value;
    if (maxTime != null)
        c.student_data.max_time_allowed.initialize(this.msToTimeInterval(Number(maxTime) * 1000));

    //handle the total_time which is delivered in seconds
    this.lastLaunchTotalTimeInMS = Number(pc.core.total_time.value) * 1000;
    //store the data model element as a TimeInterval
    c.core.total_time.initialize(this.msToTimeInterval(this.lastLaunchTotalTimeInMS));

    this.traceNote("Previous time in this lesson: " + Number(pc.core.total_time.value) + " seconds");

    //score
    //var scoreNode = XmlHelper.getNode(cmiNode, "score");
    c.core.score.max.initialize(pc.core.score.max.value);
    c.core.score.min.initialize(pc.core.score.min.value);
    c.core.score.raw.initialize(pc.core.score.raw.value);

    // [ ignore interactions since they are write-only in SCORM 1.2 ]

    //get objectives - use XPath because there may also be an <objectives> node within an <interaction>

    var arrObjectives = pc.objectives.__array

    for (var x = 0; x < arrObjectives.length; x++) {
        var objectiveNode = arrObjectives[x];

        var objObjective = new Scorm12Objective();

        objObjective.id.initialize(objectiveNode.id);

        var objectiveStatus = objectiveNode.completion_status;
        if (objectiveStatus != null)
            objObjective.status.initialize(objectiveStatus);
        else
            objObjective.status.initialize(objectiveNode.success_status);

        objObjective.score.max.initialize(objectiveNode.score.max);
        objObjective.score.min.initialize(objectiveNode.score.min);
        objObjective.score.raw.initialize(objectiveNode.score.raw);

        this.appendToCollection(c.objectives, objObjective);
    }
};

/**
 * Helper function to append an object to a collection.
 */
Scorm12Api.prototype.appendToCollection = function (collection, obj) {
    var arr = collection.__array;

    arr[arr.length] = obj;

    collection._count.initialize(String(arr.length));
};

/**
 * Called by LMSCommit to save the current state to the RTEDataSaver.
 * 
 * @param saveToDatabase boolean if true, data will be saved to the database, if false, will be saved to an json file.
 */
Scorm12Api.prototype.serialize = function (saveToDatabase) {    
    var c;
    c = this.courseDataModel[this.activeSCOIndex].cmi;
    var totalTimeInS = this.getTotalTimeInSeconds();

    //if finishing from timeout, set the total time to the max time allowed
    if (this.finishFromTimeout) {
        c.core.total_time.initialize(c.student_data.max_time_allowed.value);
        totalTimeInS = this.timeIntervalToMs(c.student_data.max_time_allowed.value) / 1000;
    }
        //otherwise use the actual total time
    else
        c.core.total_time.initialize(this.msToTimeInterval(totalTimeInS * 1000));

    this.traceNote("Total time in this lesson: " + totalTimeInS + " seconds");

    /*
	TO DO:
	this.reportToAPIInspector("cmi.core.total_time", 	c.core.total_time.value);
	this.reportToAPIInspector("cmi.core.session_time",	c.core.session_time.value);
	*/

    //for simplicity, build an intermediary object, then translate the object to an XML string, 
    //which will exclude tags for any properties that have a null value
    var cmiXML =
	{
	    credit: c.core.credit.value,
	    lesson_mode: c.core.lesson_mode.value,
	    exit: c.core.exit.value,

	    //added by chetu
	    entry: c.core.entry.value,
	    student_id: c.core.student_id.value,
	    student_name: c.core.student_name.value,
	    launch_data: c.launch_data.value,
	    lesson_status: c.core.lesson_status.value,
	    suspend_data: c.suspend_data.value.replace(new RegExp('"', 'g'), '\''),
	    lesson_location: c.core.lesson_location.value,
	    score:
		{
		    max: c.core.score.max.value,
		    min: c.core.score.min.value,
		    raw: c.core.score.raw.value
		},
	    //suspend_data: c.suspend_data.value,
	    total_time: totalTimeInS
	};
    
    //interactions
    var arr;
    arr = cmiXML.interactions = new Array();

    for (var x = 0; x < c.interactions.__array.length; x++) {

        var interaction = arr[x] = new Object();
        var i = c.interactions.__array[x];

        interaction.id = i.id.value;
        interaction.type = i.type.value;
        interaction.timestamp = i.time.value;

        //expand time to a timestamp by including today's date
        interaction.timestampConverted = this.expandDateTime(i.time.value);

        interaction.weighting = i.weighting.value;
        interaction.student_response = i.student_response.value;
        interaction.result = i.result.value;

        //latency needs to be delivered as seconds, so convert TimeInterval to seconds
        var latencyAsTimeInterval = i.latency.value;
        if (latencyAsTimeInterval != null)
            interaction.latency = (this.timeIntervalToMs(latencyAsTimeInterval) / 1000);

        interaction.objectives = new Array();

        for (var y = 0; y < i.objectives.__array.length; y++) {
            var objective = interaction.objectives[y] = new Object();
            var o = i.objectives.__array[y];
            objective.id = o.id.value;
        }

        interaction.correct_responses = new Array();

        for (var y = 0; y < i.correct_responses.__array.length; y++) {
            var correct_response = interaction.correct_responses[y] = new Object();
            var cr = i.correct_responses.__array[y];
            correct_response.pattern = cr.pattern.value;
        }
    }

    // replaceing old ids with new  ids of cmiXML.interactions
    var duplicateInterationIndex = [];
    var uniqueInteractionId = [];
    for (var i = cmiXML.interactions.length - 1; i >= 0; i--) {
        if (uniqueInteractionId.indexOf(cmiXML.interactions[i].id) > -1) {
            duplicateInterationIndex.push(i);
        }
        else {
            uniqueInteractionId.push(cmiXML.interactions[i].id);
        }
    }

    for (var i = 0; i < duplicateInterationIndex.length; i++) {
        cmiXML.interactions.splice(duplicateInterationIndex[i], 1);
    }


    //objectives
    arr = cmiXML.objectives = new Array();
    for (var x = 0; x < c.objectives.__array.length; x++) {
        var objective = arr[x] = new Object();
        var o = c.objectives.__array[x];

        objective.id = o.id.value;
        objective.completion_status = "unknown";
        objective.success_status = "unknown";
        objective.score =
		{
		    max: o.score.max.value,
		    min: o.score.min.value,
		    raw: o.score.raw.value
		};

        if (o.status.value == "passed" || o.status.value == "failed")
            objective.success_status = o.status.value;
        else
            objective.completion_status = o.status.value;
    }

    var cmiJSONString = JSON.stringify(cmiXML).replace(new RegExp('\"', 'g'), '\\"');
    //save

    var saver = this.courseLaunchController.getRteDataSaver();
    saver.addEventListener(Event.SCO_DATA_SAVED,
                           function (e) {
                               this.onSerializeLoaded(e);

                               // only call saveGlobal if it has not already been invoked and we are saving to database
                               if (!this.globalDataSaveInvoked) {
                                   this.saveGlobal(saveToDatabase);
                               }                               
                           },
                           this); //this listener gets added only once on the first addEventListener call
    saver.save(this.activeSCOIdentifier != null && this.activeSCOIdentifier != undefined ? this.activeSCOIdentifier : "DefaultIdentifierName", cmiJSONString, this.schemaversion, saveToDatabase);

}


/**
* This is called once the RTEDataSaver has completed SUCCESSFULLY. It runs in the correct scope of this-instance.
* This is not called if the saver has any type of HTTP or XML Status error -- those are handled by the RTEDataSaver.
* 
* @param e the event object returned by the RTEDataSaver which contains the resulting XML DOM object in the "target" property.
*/

Scorm12Api.prototype.saveGlobal = function (saveToDatabase) {

    // set the global data save invoked flag
    if (saveToDatabase) {
        this.globalDataSaveInvoked = true;
    }
    


    //fill the updated over all package values in global data model
    this.fillGlobalModelValuesForScorm12();

    // save global
    var globalXML =
        {
            activeSCOIdentifier: this.activeSCOIdentifier,
            activeSCOIndex: this.activeSCOIndex,
            previousSCOIdentifier: this.previousSCOIdentifier,
            nextSCOIdentifier: this.nextSCOIdentifier,
            entry: this.entry,
            completion_status: this.completion_status,
            success_status: this.success_status,
            total_time: this.total_time,
            score: this.score
        };

    //convert the Object to a string of JSON
    var globalJSONString = JSON.stringify(globalXML).replace(new RegExp('\"', 'g'), '\\"');

    //save
    var globalSaver = this.courseLaunchController.getGlobalDataSaver();
    globalSaver.addEventListener(Event.GLOBAL_DATA_SAVED,
                                 function (e) {
                                     this.traceNote("Global save process is complete.");
                                     this.globalSaveCompleted = true;
                                 },
                                 this); //this listener gets added only once on the first addEventListener call

    globalSaver.save("global_data", globalJSONString, this.schemaversion, saveToDatabase);
};


/**
 * This is called for computing the updated global values and fill those values into the 
 * global attributes attched with api instance.
 */
Scorm12Api.prototype.fillGlobalModelValuesForScorm12 = function (e) {

    // set the success status and completion status
    this.success_status = this.ComputeGlobalStatus("SuccessStatus");
    this.completion_status = this.ComputeGlobalStatus("CompletionStatus");    
   
    // fill global total time 
    this.total_time = 0;
    for (var index = 0; index < this.courseDataModel.length; index++) {
        this.total_time += (this.timeIntervalToMs(this.courseDataModel[index].cmi.core.total_time.value));
    }

    // convert total time from ms to seconds 
    this.total_time = this.total_time / 1000;
    
    var scoreMaxValue = 0;
    var scoreMinValue = 0;
    var scoreRawValue = 0;
    var count = 0;

    for (var index = 1; index < this.courseDataModel.length; index++) {

        if (this.courseDataModel[index].cmi.core != null && this.courseDataModel[index].cmi.core.score != null && this.courseDataModel[index].cmi.core.score != undefined && this.courseDataModel[index].cmi.core.score.raw.value != null && this.courseDataModel[index].cmi.core.score.raw.value != '') {
            count = count + 1;
            scoreMaxValue = scoreMaxValue + this.courseDataModel[index].cmi.core.score.max.value != null ? this.courseDataModel[index].cmi.core.score.max.value : 0;
            scoreMinValue = scoreMinValue + this.courseDataModel[index].cmi.core.score.min.value != null ? this.courseDataModel[index].cmi.core.score.min.value : 0;
            scoreRawValue = scoreRawValue + this.courseDataModel[index].cmi.core.score.raw.value != null ? this.courseDataModel[index].cmi.core.score.raw.value : 0;
        }
    }

    if (count == 0) {
        scoreMaxValue = null;
        scoreMinValue = null;
        scoreRawValue = null;
        scoreRawValue = null;
    }
    else {
        scoreMaxValue = scoreMaxValue / count;
        scoreMinValue = scoreMinValue / count;
        scoreRawValue = scoreRawValue / count;
        scoreRawValue = (scoreMaxValue != '' && scoreMaxValue != 0) ? (scoreRawValue / scoreMaxValue) : scoreRawValue / 100;
    }

    // fill globle score
    this.score =
           {
               max: scoreMaxValue / 100,
               min: scoreMinValue / 100,
               raw: scoreRawValue
           };

    this.entry = "resume";
}

/**
 * This is called for computing the completion status and success status of the organization node 
 * through the lesson status of the dfferent SCO's.
 * Logic mentioned bellow is as per the discussion in the client meeting.
 * @param statusToReturn ,the static string that decides which status value needed for return.
 */
Scorm12Api.prototype.ComputeGlobalStatus = function (statusToReturn) {
    try {
        /*LOGIC:
        ======================================================================================
        SCORM 1.2 COMPLETION STATUS            COMPLETION STATUS    SUCCESS STATUS
        completed                                completed        		unknown -- condition 1
        incomplete                               incomplete       		unknown -- condition 2 
        passed                                   completed        		passed  -- condition 3
        failed                                   incomplete       		failed  -- condition 4

        MULTI SCO       
        if any SCO sets passed and all others completed, then use passed case above.
        if any SCO sets failed, then use failed case above.
        if all SCOs set completed, then use completed case above.
        otherwise, use incomplete case above. 
        ======================================================================================
        */

        var imsManifestOrganization = this.imsManifest.organizations[0];
        var len = imsManifestOrganization.items.length;
        var activeIndex = null;
        var itemsCount = len;

        var passedItems = 0;
        var completedItems = 0;
        var failedItems = 0;
        var completionStatus = 'unknown';
        var successStatus = 'unknown';

        for (var i = 0; i < len; i++) {
            activeIndex = i;
            itemIdentifier = imsManifestOrganization.items[activeIndex].identifier;
            itemIdentifierref = imsManifestOrganization.items[activeIndex].identifierref;
            isVisibleItem = imsManifestOrganization.items[activeIndex].isvisible;

            if (itemIdentifier != null && itemIdentifierref != null && isVisibleItem) {
                var lessonStatus = this.courseDataModel[activeIndex].cmi.core.lesson_status.value;

                if (lessonStatus == "passed") {
                    passedItems = passedItems + 1;
                }
                else if (lessonStatus == "completed") {
                    completedItems = completedItems + 1;
                }
                else if (lessonStatus == "failed") {
                    failedItems = failedItems + 1;
                }
            }
            else {
                itemsCount = itemsCount - 1;
            }
        }

        //If any SCO sets passed and all others completed, then use passed case above.
        if (passedItems > 0 && passedItems + completedItems == itemsCount) {
            //From condition 3
            completionStatus = "completed";
            successStatus = "passed";
        }

            //if any SCO sets failed, then use failed case above.
        else if (failedItems > 0) {
            //From condition 4
            completionStatus = "incomplete";
            successStatus = "failed";
        }

            //if all SCOs set completed, then use completed case above.
        else if (completedItems == itemsCount) {
            //From condition 1
            completionStatus = "completed";
            successStatus = "unknown";
        }
            //otherwise, use incomplete case above.
        else {
            //From condition 2
            completionStatus = "incomplete";
            successStatus = "unknown";
        }

        if (statusToReturn == "CompletionStatus") {
            return completionStatus;
        }
        else if (statusToReturn == "SuccessStatus") {
            return successStatus;
        }
        else {
            return null;
        }
    }
    catch (e) {
        this.traceError("Error caught in ComputeGlobalStatus: " + e, "", 1);
    }
}

/**
 * This is called once the RTEDataSaver has completed SUCCESSFULLY. It runs in the correct scope of this-instance.
 * This is not called if the saver has any type of HTTP or XML Status error -- those are handled by the RTEDataSaver.
 * 
 * @param e the event object returned by the RTEDataSaver which contains the resulting XML DOM object in the "target" property.
 */
Scorm12Api.prototype.onSerializeLoaded = function (e) {      
    try {
        /*
        The format of a successful response is:
        
        <serv_response>
            <head>
                <status>success</status>
            </head>
            <body>
                <token>sdf676sd87f68s7d</token>  this is extracted by the RTEDataSaver.
            </body>
        </serv_response>

        This function would NOT be called if the XML was a failed response, because the RTEDataSaver handles that now.
        */
        this.traceNote("LMSCommit process is complete.");

        //unlock commmit by changing the commit state
        this.activeCommitState = Scorm12Api.COMMIT_STATE.Committed;

        if (this.queueFinishOnSaverResponse) {
            //state is currently be FinishStarted to lock any calls to LMSGetValue/LMSSetValue/LMSCommit,
            //but revert the state to Running to LMSFinish can now will proceed.
            this.activeState = Scorm12Api.STATE.Running;
            this.LMSFinish("");
            return;
        }
        else if (this.queueCommitOnSaverResponse) {
            this.LMSCommit("");
            return;
        }

        //if this call to Commit was called by LMSFinish, dispatch the onFinishCompleted event
        if (this.activeState == Scorm12Api.STATE.FinishStarted) {
            this.activeState = Scorm12Api.STATE.Finished;

            this.dispatchEvent(new Event(Event.FINISH_COMPLETED, "Scorm12Api"));
        }
    }
    catch (e) {
        this.traceError("Error caught in onSerializeLoaded: " + e, "", 1);
    }
};


/**
 * Returns true if the name is a string, starts with "cmi", and exists in the data model.
 */
Scorm12Api.prototype.isValidDataElementName = function (pstrDataElementName) {
    //make sure the parameter is a string
    if (typeof pstrDataElementName != "string")
        return false;

    //make sure pstrDataElementName begins with "cmi"
    if (pstrDataElementName.substr(0, 3) != "cmi")
        return false;

    //make sure pstrDataElementName is a valid data model element
    if (!this.getDataElement(pstrDataElementName))
        return false;

    return true;
};

/**
 * Returns the corresponding element in the data model, or false if it doesn't exist.
 * Note - for "array" elements like cmi.objectives.1.id, you must use getArrayDataElement()
 */
Scorm12Api.prototype.getDataElement = function (pstrDataElementName) {
    try {
        if (this.activeSCOIndex != null) {
            return eval("this.courseDataModel[" + this.activeSCOIndex + "]." + pstrDataElementName);
        }
        else {
            return eval("this.courseDataModel[0]." + pstrDataElementName);
        }
    }
    catch (e) {
        return false;
    }
};

/**
 * Returns true if the data element name is a valid array data element, i.e. cmi.objectives.1.id
 */
Scorm12Api.prototype.isValidArrayDataElementName = function (pstrDataElementName) {
    try {
        //check if there is at least one array index notation within the name (cmi.abcd.5.asdf)
        var pattern = /.[0-9]+./;
        if (!pattern.test(pstrDataElementName))
            return false;

        //loop through each part and validate each
        /*
            cmi.interactions.5.objectives.3.id
            1. validate cmi.interactions in "this"
            2. get itemType of cmi.interactions 
            3. validate objectives in last itemType
            4. get itemType of objectives
            5. validate id in last itemType
        */

        var arr = pstrDataElementName.split(".");
        var container = this.courseDataModel[this.activeSCOIndex];
        var propertyPart = "";

        for (var i = 0; i < arr.length; i++) {
            //if part is not not an array index, build the property part string
            if (isNaN(arr[i])) {
                propertyPart += arr[i];
                if (i < arr.length - 1 && isNaN(arr[i + 1])) {
                    propertyPart += ".";
                }
            }

            //if part is numeric or is the last part, evaluate it to make sure it exists
            if (!isNaN(arr[i])) {
                if (!eval("container." + propertyPart))
                    return false;

                //get the next object container
                var itemType = eval("container." + propertyPart + ".__itemType");
                container = eval("new " + itemType);

                //reset the propertyPart string
                propertyPart = "";

                //validate the last property
            }
            else if (i == arr.length - 1) {
                if (!eval("container." + propertyPart))
                    return false;
            }
        }

        return true;
    }
    catch (e) {
        this.traceNote("Caught an error in isValidArrayDataElementName():" + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
 * Returns true if index is > 0 and <= count. 
 * Assumes pstrDataElementName is a valid array data element name.
 */
Scorm12Api.prototype.isArrayDataElementIndexInRange = function (pstrDataElementName) {
    try {
        var arr = pstrDataElementName.split(".");
        var container = this.courseDataModel[this.activeSCOIndex];
        var propertyPart = "";
        var index;

        for (var i = 0; i < arr.length; i++) {
            //if part is not not an array index, build the property part string
            if (isNaN(arr[i])) {
                propertyPart += arr[i];
                if (i < arr.length - 1 && isNaN(arr[i + 1]))
                    propertyPart += ".";
            }

            //if part is numeric, build the container
            if (!isNaN(arr[i])) {
                container = eval("container." + propertyPart);

                index = Number(arr[i]);

                if (index < 0 || index > Number(container._count.value))
                    return false;
                else if (index == Number(container._count.value))
                    container = eval("new " + container.__itemType);
                else
                    container = container.__array[index];
                propertyPart = "";
            }
        }

        return true;
    }
    catch (e) {
        this.traceNote("Caught an error in isArrayDataElementIndexInRange():" + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
 * @param pstrDataElementName i.e. "objectives.5.score.raw"
 * @param blnAppendNewElement:
 *	if blnAppendNewElement is true AND if current _count is equal to the requested index, then the count is incremented by 1 and a new element is appended to the array
 *	else if blnAppendNewElement is false AND if current _count is equal to the requested index, then the count stays the same and a temporary element is returned
 *	else return the existing element
 */
Scorm12Api.prototype.getArrayDataElement = function (pstrDataElementName, blnAppendNewElement) {
    try {
        var arr = pstrDataElementName.split(".");
        var container = this.courseDataModel[this.activeSCOIndex];
        var newContainer;
        var propertyPart = "";
        var index;

        for (var i = 0; i < arr.length; i++) {
            //if part is not not an array index, build the property part string
            if (isNaN(arr[i])) {
                propertyPart += arr[i];
                if (i < arr.length - 1 && isNaN(arr[i + 1]))
                    propertyPart += ".";
            }

            //if part is numeric, build the container
            if (!isNaN(arr[i])) {
                container = eval("container." + propertyPart);

                index = Number(arr[i]);

                if (index < 0 || index > Number(container._count.value))
                    return false;

                else if (Number(container._count.value) == index) {
                    if (blnAppendNewElement) {
                        //create the new index & update the _count property
                        container.__array[index] = eval("new " + container.__itemType);
                        container._count.initialize(String(container.__array.length));

                        //store a reference to the the parent and a flag atLeastOnePropertySet in case a rollback needs to happen due to attempting to set invalid data
                        container.__array[index].parent = container;

                        container = container.__array[index]
                    }
                    else
                        container = eval("new " + container.__itemType);
                }
                else
                    container = container.__array[index]

                propertyPart = "";
            }

                //if part is the last part, return it
            else if (i == arr.length - 1)
                return eval("container." + propertyPart);
        }
    }
    catch (e) {
        this.traceNote("Caught an error in getArrayDataElement():" + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
 * Removes the last array element (assumed to be pobjDataElement) from the parent
 * collection's array ONLY IF no other properties in the parent have been previously set successfully
 * 
 * i.e. if the object is interactions.5.latency, but no other properties of the 5th interaction have 
 *	      been set yet (like interactions.5.id), then the 5th interaction will be discarded and the highest
 *		  index will become 4 (meaning 5 total interactions since it's zero-based)
 */
Scorm12Api.prototype.undoArrayElementAppend = function (pobjDataElement) {
    try {
        var parentObject = pobjDataElement.parent; //i.e. instance of Interaction()

        if (!parentObject.atLeastOnePropertySet) {
            //this.traceNote("atLeastOnePropertySet != true, so undoing");
            var collectionObject = parentObject.parent; //i.e. interactions
            var arr = collectionObject.__array;
            arr.splice(arr.length - 1, 1);
            collectionObject._count.value = String(arr.length);
        }
    }
    catch (e) {
        this.traceNote("Caught an error in undoArrayElementAppend():" + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
}

/**
 * Returns true if the data type of pobjDataElement is valid according to 
 * the type of element pobjDataElement.pendingValue.
 * @param pobjDataElement - the data element to test
 * @param pobjDataType - the optional data type to use; if not defined, uses pobjDataElement.dataType
 */
Scorm12Api.prototype.isDataValueTypeValid = function (pobjDataElement, pobjDataType) {
    try {
        var objDataType = pobjDataElement.dataType;

        if (typeof (pobjDataType) != "undefined")
            objDataType = pobjDataType;

        //convert decimal and integer to string when not using the test suite
        switch (objDataType) {
            case Scorm12DataModel.DATA_TYPE.CMIDecimal:
            case Scorm12DataModel.DATA_TYPE.CMIInteger:
                if (!Scorm12Api.TEST_SUITE_MODE)
                    pobjDataElement.pendingValue = pobjDataElement.pendingValue.toString();
                break;
            default:
                //other types - no integers allowed
                if ((typeof pobjDataElement.pendingValue != "string") && (typeof pobjDataElement.pendingValue != "object"))
                    return false;
        }

        switch (objDataType) {
            //CMIDecimal
            case Scorm12DataModel.DATA_TYPE.CMIDecimal:
                var pattern = /^(|(|-)([0-9]*)(|\.)(|[0-9]*))$/;

                return pattern.test(pobjDataElement.pendingValue);


                //CMIFeedback - no validation for this datatype right now
            case Scorm12DataModel.DATA_TYPE.CMIFeedback:
                return true;

                //CMIIdentifier
            case Scorm12DataModel.DATA_TYPE.CMIIdentifier:
                if (pobjDataElement.pendingValue.length > 255) {
                    this.traceError("CMIIdentifier length limit is 255. Value length is " + pobjDataElement.pendingValue.length + ".");
                    return false;
                }

                if (pobjDataElement.pendingValue.length <= 255 && pobjDataElement.pendingValue.length > 0) { //0 to 255
                    for (var i = 0; i < pobjDataElement.pendingValue.length; i++) { //loop for whitespace
                        if (pobjDataElement.pendingValue.charCodeAt(i) <= 32)
                            return false
                    }
                    return true;
                }
                return false;

                //CMIInteger
            case Scorm12DataModel.DATA_TYPE.CMIInteger:
                var pattern = /^(|(|-)([0-9]{1,5}))$/;
                if (pattern.test(pobjDataElement.pendingValue))
                    if (Number(pobjDataElement.pendingValue) >= -32768 && Number(pobjDataElement.pendingValue) <= 32768)
                        return true;
                    else
                        this.traceError("CMIInteger must be between -32768 and 32768.");

                return false;

                //CMIString255
            case Scorm12DataModel.DATA_TYPE.CMIString255:
                if (pobjDataElement.pendingValue.length > 255) {
                    this.traceError("CMIString255 length limit is 255. Value length is " + pobjDataElement.pendingValue.length + ".");
                    return false;
                }

                return true;

                //CMIString4096
            case Scorm12DataModel.DATA_TYPE.CMIString4096:
                if (pobjDataElement.pendingValue.length > 4096) {
                    this.traceError("CMIString4096 length limit is 4096. Value length is " + pobjDataElement.pendingValue.length + ".");
                    return false;
                }

                return true;

                //CMITime
            case Scorm12DataModel.DATA_TYPE.CMITime:
                var pattern = /^([0-9]{2})(:)([0-9]{2})(:)([0-9]{2})(|(.)([0-9]{1,2}))$/;

                if (!pattern.test(pobjDataElement.pendingValue))
                    return false;
                else if (Number(pobjDataElement.pendingValue.split(":")[0]) > 23)
                    return false;
                else if (Number(pobjDataElement.pendingValue.split(":")[1]) > 59)
                    return false;
                else if (Number(pobjDataElement.pendingValue.split(":")[2]) > 59)
                    return false;

                return true;

                //CMITimespan
            case Scorm12DataModel.DATA_TYPE.CMITimespan:
                var pattern = /^([0-9]{2,4})(:)([0-9]{2})(:)([0-9]{2,7})(|(.)([0-9]{1,2}))$/;

                //Note: The SCORM spec lists seconds have only a "minimum of 2 digits" -- no maximum. Lets assume a maximum of 7 digits.

                return (pattern.test(pobjDataElement.pendingValue));

                //CMIVocabulary
            case Scorm12DataModel.DATA_TYPE.CMIVocabulary:
                var arrList = pobjDataElement.constraints.split("|");
                var x;
                var vocabValue;
                for (var x = 0; x < arrList.length; x++) {
                    //if vocabulary value includes another data type in brackets, validate against that type
                    vocabValue = arrList[x];
                    if (vocabValue.indexOf("[") == 0) {
                        var dataTypeName = vocabValue.substring(1, vocabValue.length - 1);
                        var dataType = eval("Scorm12DataModel.DATA_TYPE." + dataTypeName);

                        if (this.isDataValueTypeValid(pobjDataElement, dataType))
                            return true;
                    }
                        //else validate against explicit vocab value
                    else if (pobjDataElement.pendingValue == vocabValue)
                        return true;
                }

                return false;
        }

        return true;
    }
    catch (e) {
        this.traceNote("Caught an error in isDataValueTypeValid():" + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
 * Returns true if the data of pobjDataElement is in the correct range of values according to its defined constraints.
 * @param pobjDataElement the data element to test
 * @param pstrDataElementName the name of the data element for some very specific tests
 */
Scorm12Api.prototype.isDataValueInRange = function (pobjDataElement, pstrDataElementName) {
    try {
        var blnInRange = false;
        var constraints = "";
        var arrConstraints;

        switch (pobjDataElement.dataType) {
            //CMIDecimal - low:high
            case Scorm12DataModel.DATA_TYPE.CMIDecimal:

                constraints = pobjDataElement.constraints;

                var numValue = Number(pobjDataElement.pendingValue);

                if (typeof constraints == "string" && constraints.length > 0) {
                    arrConstraints = constraints.split(":");
                    low = Number(arrConstraints[0]);
                    high = Number(arrConstraints[1]);

                    return (numValue >= low && numValue <= high);
                }
                break;

            case Scorm12DataModel.DATA_TYPE.CMIVocabulary:
                if (pobjDataElement.constraints == Scorm12DataModel.DATA_TYPE.CMIVocabulary.Status) {
                    //don't allow lesson_status to be set to "not attempted"
                    if (pstrDataElementName == "cmi.core.lesson_status" && pobjDataElement.pendingValue == "not attempted")
                        return false;
                }
                break;
        }

        return true;
    }
    catch (e) {
        this.traceNote("Caught an error in isDataValueInRange():" + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
 * If the data element is the lesson status or raw score, if mastery score
 * has been set, over-rides the lesson status based on the score.
 */
Scorm12Api.prototype.evaluateOverrides = function (pstrDataElementName) {
    var blnOverrideMade = false;

    try {
        //var c = this.dataModel.cmi;
        var c = this.courseDataModel[this.activeSCOIndex].cmi;

        //REQ 1.6.6
        if (pstrDataElementName == "cmi.core.lesson_status" || pstrDataElementName == "cmi.core.score.raw") {
            //if there's a mastery score and status isn't incomplete...
            if (c.student_data.mastery_score.isInitialized && c.core.lesson_status.value != "incomplete") {
                //if the master score is > 0 and either an empty score or a score < the mastery score...
                if (Number(c.student_data.mastery_score.value) > 0
                    && (
                        c.core.score.raw.value == null
                        || c.core.score.raw.value == ""
                        || Number(c.core.score.raw.value) < Number(c.student_data.mastery_score.value)
                        )
                    ) {
                    if (c.core.lesson_status.value != "failed") {
                        c.core.lesson_status.initialize("failed");
                        blnOverrideMade = true;
                    }
                }

                    //user has a score >= the mastery score...
                else {
                    //if no-credit and browse, mark status as browsed.
                    if (c.core.credit.value == "no-credit" && c.core.lesson_mode.value == "browse") {
                        if (c.core.lesson_status.value != "browsed") {
                            c.core.lesson_status.initialize("browsed");
                            blnOverrideMade = true;
                        }
                    }

                        //if credit or (passed, failed, or incomplete), mark status as passed.
                    else {
                        if (c.core.lesson_status.value != "passed") {
                            c.core.lesson_status.initialize("passed");
                            blnOverrideMade = true;
                        }
                    }
                }

                if (blnOverrideMade)
                    this.traceNote('"cmi.core.lesson_status" was overridden as "' + c.core.lesson_status.value + '". (mastery_score is "' + c.student_data.mastery_score.value + '", score.raw is "' + c.core.score.raw.value + '", and credit is "' + c.core.credit.value + '")');
            }

            /*
            TO DO:
            if (blnOverrideMade)
                this.reportToAPIInspector("cmi.core.lesson_status", c.core.lesson_status.value);
            */
        }
    }
    catch (e) {
        this.traceNote("Caught an error in evaluateOverrides(): " + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
 * Returns the specified number of milliseconds to a SCORM 1.2 Time Interval format.
 * The Time Interval format is: HHHH:MM:SS.SS
 */
Scorm12Api.prototype.msToTimeInterval = function (pintMilliseconds) {
    var subsecs, seconds, minutes, hours;

    subsecs = Math.floor(pintMilliseconds / 10) % 100;
    seconds = Math.floor(pintMilliseconds / 1000);
    minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    hours = Math.floor(minutes / 60);
    minutes = minutes % 60;

    if (seconds < 10)
        seconds = "0" + seconds;

    if (minutes < 10)
        minutes = "0" + minutes;

    if (hours < 10)
        hours = "0" + hours;

    return hours + ":" + minutes + ":" + seconds + "." + subsecs;
}

/**
 * Returns the specified SCORM 1.2 Time Interval format converted to milliseconds.
 * The Time Interval format is: HHHH:MM:SS.SS
 */
Scorm12Api.prototype.timeIntervalToMs = function (pstrTimeInterval) {
    var seconds, minutes, hours;
    var milliseconds;

    if (pstrTimeInterval == "" || pstrTimeInterval == 0)
        return 0;

    var arrTimeInterval = pstrTimeInterval.split(":");
    seconds = Number(arrTimeInterval[2]);
    minutes = Number(arrTimeInterval[1]);
    hours = Number(arrTimeInterval[0]);

    milliseconds = (hours * 60 * 60 * 1000) + (minutes * 60 * 1000) + (seconds * 1000);

    return milliseconds;
}

/**
 * Returns the total time the learner has been in this lesson... REQ_76.4
 */
Scorm12Api.prototype.getTotalTimeInSeconds = function () {
    var totalTimeInMS = 0;
    var totalSessionTimeInMS = 0;

    //if the sco has reported session time, use that
    //if (this.courseDataModel[this.activeSCOIndex].cmi.core.session_time.isInitialized)
        //totalSessionTimeInMS = this.timeIntervalToMs(this.courseDataModel[this.activeSCOIndex].cmi.core.session_time.value);

        //else calulate the session time
    //else {
        totalSessionTimeInMS = (new Date()) - this.sessionTimeStart;

        //update the session time value but don't mark the element as initialized
        this.courseDataModel[this.activeSCOIndex].cmi.core.session_time.value = this.msToTimeInterval(totalSessionTimeInMS);
    //}

    totalTimeInMS = this.lastLaunchTotalTimeInMS + totalSessionTimeInMS;

    return totalTimeInMS / 1000;
}

/**
 * If the current date/time has reached the time-out date/time, time out
 */
Scorm12Api.prototype.checkTimeout = function () {
    var now = new Date();

    if (now >= this.timeoutDateTime) {
        window.clearTimeout(this.checkTimeoutInterval);
        this.timeout();
    }
}

/**
 * Called by checkTimeout if the session time limit has been exceeded.
 */

Scorm12Api.prototype.timeout = function () {
    if (this.activeState != Scorm12Api.STATE.Running)
        return;

    var c = this.dataModel.cmi;

    switch (c.student_data.time_limit_action.value) {
        case "exit,message":
            //message
            this.doJSAlert("You have exceeded the allowed time limit for this lesson. This lesson will now close.");

            //set exit to time-out
            c.core.exit.initialize("time-out");
            this.traceNote("Timed out. Setting cmi.core.exit to 'time-out'.");
            //this.reportToAPIInspector("cmi.core.exit", c.core.exit.value);

            //call LMSFinish
            this.traceNote("time_limit_action is 'exit,message', so calling LMSFinish.");
            this.finishFromTimeout = true;
            this.LMSFinish("");

            break;

        case "exit,no message":
            //set exit to time-out
            c.core.exit.initialize("time-out");
            this.traceNote("Timed out. Setting cmi.core.exit to 'time-out'.");
            //this.reportToAPIInspector("cmi.core.exit", c.core.exit.value);

            //call LMSFinish
            this.traceNote("time_limit_action is 'exit,no message', so calling LMSFinish.");
            this.finishFromTimeout = true;
            this.LMSFinish("");

            break;

        case "continue,message":
            this.doJSAlert("You have exceeded the allowed time limit for this lesson, however you may continue working.");
            this.traceNote("Timed out, but time_limit_action is 'continue,message', so learner is allowed to continue.");
            break;

        case "continue,no message":
            this.traceNote("Timed out, but time_limit_action is 'continue,no message', so learner is allowed to continue.");
            break;

        default:
            this.traceNote("Timed out, but time_limit_action is an unknown value, so doing nothing.");
            break;
    }
};

/**
 * Converts valid SCORM 1.2 time (CMITime datatype) to a full timestamp
 * by pre-pending today's date onto a valid CMITime HH:MM:SS.ss
 *
 * @param strTime "HH:MM:SS.ss" or null
 * @return "MM-DD-YYYY HH:MM:SS" (where date is today's date) or null
 */
Scorm12Api.prototype.expandDateTime = function (strTime) {
    if (strTime == null)
        return null;

    var now = new Date();

    var day = String(now.getDate());
    var month = String(now.getMonth() + 1);
    var year = String(now.getFullYear());

    if (day.length == 1)
        day = "0" + day;
    if (month.length == 1)
        month = "0" + month;

    var expandedDateTime = month + "-" + day + "-" + year + " " + this.expandTime(strTime);

    return expandedDateTime;
}

/**
 * Helper function used by expandDateTime() to expand a valid
 * time portion of a SCORM 1.2 "time" CMITime type to HH:MM:SS.s.
 * Note: the partial seconds are truncated
 * assume strTime is not null
 */
Scorm12Api.prototype.expandTime = function (strTime) {
    return strTime.split(".")[0];
}

/**
 * Method is used to update the activity status and mark it checked or unchecked according to 
 * its completion or success status.
 */
Scorm12Api.prototype.UpdateStateDisplay = function () {
    try {
        var organizations = this.imsManifest.organizations[0];
        if (organizations != undefined && organizations != null && organizations.items != null && organizations != undefined) {
            var items = organizations.items;
            var courseDataModel = this.courseDataModel;
            for (var index = 0; index < courseDataModel.length; index++) {

                var prefix_img_status = "img_status_";
                var scoIdentifier = courseDataModel[index].scoIdentifier;
                var chkBox_status = document.getElementById((prefix_img_status + scoIdentifier));
                chkBox_status.checked = false;
                var success_status = null;

                if (index == 0) {
                    //SCORM 1.2 COMPLETION STATUS            COMPLETION STATUS    SUCCESS STATUS
                    //completed                                completed        		unknown -- condition 1
                    //incomplete                               incomplete       		unknown -- condition 2 
                    //passed                                   completed        		passed  -- condition 3
                    //failed                                   incomplete       		failed  -- condition 4

                    success_status = this.ComputeGlobalStatus("CompletionStatus");
                    success_status = this.ComputeGlobalStatus("SuccessStatus") != 'unknown' ? this.ComputeGlobalStatus("SuccessStatus") : success_status;
                }

                else if (items[index].identifier != undefined && items[index].identifier != null && items[index].identifierref != undefined && items[index].identifierref != undefined) {
                    var lessonStatus = courseDataModel[index].cmi.core.lesson_status.value;
                    if (lessonStatus != "") {
                        success_status = lessonStatus;
                    }
                }

                if (chkBox_status != null) {

                    // set "not completed style" by adding "DimIcon" class
                    chkBox_status.classList.add("DimIcon");

                    if (success_status == "passed") {
                        // remove "DimIcon" class for "completed style"
                        chkBox_status.classList.remove("DimIcon");

                        chkBox_status.title = "passed";
                    }
                    else if (success_status == "failed") {
                        chkBox_status.title = "failed";
                    }
                    else if (success_status == "completed") {
                        // remove "DimIcon" class for "completed style"
                        chkBox_status.classList.remove("DimIcon");

                        chkBox_status.title = "completed";
                    }
                    else if (success_status == "incomplete") {
                        chkBox_status.title = "incomplete";
                    }
                    else {
                        chkBox_status.title = "unknown";
                    }
                }
            }
            //update the over all package status elements values
            this.fillGlobalModelValuesForScorm12();

        }
    }
    catch (e) {
        this.traceError("Error caught in UpdateStateDisplay", "", 1);
        throw e;
    }
}