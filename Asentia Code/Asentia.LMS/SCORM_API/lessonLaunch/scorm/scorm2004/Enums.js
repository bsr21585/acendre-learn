﻿

/**
* NavigationRequest.
* CEnum for NavigationRequest.
*/
CEnum.NavigationRequest =
{
    Start: "start",
    Previous: "previous",
    Continue: "continue",
    Exit: "exit",
    ExitAll: "exitAll",
    SuspendAll: "suspendAll",
    Abandon: "abandon",
    AbandonAll: "abandonAll",
    Valid: "valid",
    NotValid: "notvalid",
    Jump: "jump"
};

/**
* SequencingRequest.
* CEnum for SequencingRequest.
*/
CEnum.SequencingRequest =
{
    NotAvailable: "notAvailable",
    Previous: "previous",
    Continue: "continue",
    Start: "start",
    ResumeAll: "resumeAll",
    Valid: "valid",
    NotValid: "notvalid",
    Exit: "exit",
    ExitAll: "exitAll",
    Retry: "retry",
    Choice: "choice",
    Jump: "jump"
};

/**
* DeliveryRequest.
* CEnum for DeliveryRequest.
*/
CEnum.DeliveryRequest =
{
    Valid: "valid",
    NotValid: "notvalid"
};

/**
* TerminationRequest.
* CEnum for TerminationRequest.
*/
CEnum.TerminationRequest =
{
    NotAvailable:"notAvailable",
    Valid: "valid",
    NotValid: "notvalid",
    Exit: "exit",
    ExitAll: "exitAll",
    SuspendAll: "suspendAll",
    Abandon: "abandon",
    AbandonAll: "abandonAll"
};

/**
* Sequencing Condition Rules and RuleAction.
* CEnum for Sequencing Condition Rules.
*/
CEnum.SequencingConditionRules =
{
    PreConditionRule: "preConditionRule",
    PostConditionRule: "postConditionRule",
    ExitConditionRule: "exitConditionRule",
    PreConditionRuleAction:
 {
     Skip: "skip",                            //skip|disabled|hiddenFromChoice|stopForwardTraversal
     Disabled: "disabled",
     HiddenFromChoice: "hiddenFromChoice",
     StopForwardTraversal: "stopForwardTraversal"
 },
    PostConditionRuleAction:
 {
     ExitParent: "exitParent",               //"exitParent|exitAll|retry|retryAll|continue|previous"
     ExitAll: "exitAll",
     Retry: "retry",
     RetryAll: "retryAll",
     Continue: "continue",
     Previous: "previous"
 },

    ExitConditionRuleAction:
 {
     //"exit""
     Exit: "exit"
 }
};



/**
* Sequencing ControlMode Attribute Value.
* CEnum for Sequencing ControlMode Attribute Value.
*/
CEnum.ControlModeAttributeValue =
{
    Choice: "choice",
    ChoiceExit: "choiceExit",
    Flow: "flow",
    ForwardOnly: "forwardOnly",
    UseCurrentAttemptObjectiveInfo: "useCurrentAttemptObjectiveInfo",
    UseCurrentAttemptProgressInfo: "useCurrentAttemptProgressInfo"
};

/**
* Sequencing Condition Rules and RuleAction.
* CEnum for Sequencing Condition Rules
*/
CEnum.CompletionStatus =
{
    Completed: "completed",              //"completed|incomplete|not attempted|unknown"
    Incomplete: "incomplete",
    NotAttempted: "not attempted",
    Unknown: "unknown"
};


/**
* End Attempt Process Values.
* CEnum for End Attempt Process.
*/
CEnum.EndAttemptProcess =
{
    Unknown: "unknown",
    Failed: "failed",
    Passed: "passed",
    Incomplete: "incomplete",
    Completed: "completed",
    NotAttempted: "not attempted"
};

/**
* Randomize Children Process Values.
* CEnum for Randomize Children Process.
*/
CEnum.RandomizeChildrenProcess =
{
    SelectCount: "selectCount",
    ReorderChildren: "reorderChildren",
    SelectionTiming:"selectionTiming",
    RandomizationControls: "randomizationControls",
    RandomizationTiming: "randomizationTiming",
    Once: "once",
    Never: "naver",
    OnEachNewAttempt: "onEachNewAttempt"
};


/**
* Limit Conditions Values Values.
* CEnum for Limit Conditions Values.
*/
CEnum.LimitConditionsValues =
{
    AttemptAbsoluteDurationLimit: "attemptAbsoluteDurationLimit",
    AttemptExperiencedDurationLimit: "attemptExperiencedDurationLimit",
    ActivityAbsoluteDurationLimit: "activityAbsoluteDurationLimit",
    ActivityExperiencedDurationLimit: "activityExperiencedDurationLimit",
    AttemptLimit: "attemptLimit",
    BeginTimeLimit: "beginTimeLimit",
    EndTimeLimit: "endTimeLimit"

};

/**
* adlcpCompletionThreshold attribute Values.
* CEnum for adlcpCompletionThreshold Values.
*/
CEnum.AdlcpCompletionThreshold =
{
    CompletedByMeasure: "completedByMeasure",
    MinProgressMeasure: "minProgressMeasure",
    ProgressWeight: "progressWeight"
};

/**
* adlcpCompletionThreshold attribute Values.
* CEnum for adlcpCompletionThreshold Values.
*/
CEnum.RollupAction =
{
    Satisfied: "satisfied",
    NotSatisfied: "notSatisfied",
    Incomplete: "incomplete",
    Completed: "completed"
};

CEnum.ObjectiveTypes = { all: 'all', primary: 'primary', objective: 'objective' }
CEnum.GetObjective = { index: 'index', item: 'item' }
CEnum.ActivityType = { all: "all", leaf: "leaf" }
CEnum.RuleConditions = { satisfied: 'satisfied', objectiveStatusKnown: 'objectiveStatusKnown', objectiveMeasureKnown: 'objectiveMeasureKnown', objectiveMeasureGreaterThan: 'objectiveMeasureGreaterThan', objectiveMeasureLessThan: 'objectiveMeasureLessThan', completed: 'completed', activityProgressKnown: 'activityProgressKnown', attempted: 'attempted', attemptLimitExceeded: 'attemptLimitExceeded', timeLimitExceeded: 'timeLimitExceeded', outsideAvailableTimeRange: 'outsideAvailableTimeRange', always: 'always' }
CEnum.Rollupconditions = { satisfied: 'satisfied', objectiveStatusKnown: 'objectiveStatusKnown', objectiveMeasureKnown: 'objectiveMeasureKnown', completed: 'completed', activityProgressKnown: 'activityProgressKnown', attempted: 'attempted', attemptLimitExceeded: 'attemptLimitExceeded', never: 'never' }

CEnum.RuleConditionArray = [CEnum.RuleConditions.satisfied
                            , CEnum.RuleConditions.objectiveStatusKnown
                            , CEnum.RuleConditions.objectiveMeasureKnown
                            , CEnum.RuleConditions.objectiveMeasureGreaterThan
                            , CEnum.RuleConditions.objectiveMeasureLessThan
                            , CEnum.RuleConditions.completed
                            , CEnum.RuleConditions.activityProgressKnown
                            , CEnum.RuleConditions.attempted
                            , CEnum.RuleConditions.attemptLimitExceeded
                            , CEnum.RuleConditions.timeLimitExceeded
                            , CEnum.RuleConditions.outsideAvailableTimeRange
                            , CEnum.RuleConditions.always
                            ];
CEnum.CheckObjectiveForFollowingRuleConditions = [CEnum.RuleConditions.satisfied
                                                , CEnum.RuleConditions.objectiveStatusKnown
                                                , CEnum.RuleConditions.objectiveMeasureKnown
                                                , CEnum.RuleConditions.objectiveMeasureGreaterThan
                                                , CEnum.RuleConditions.objectiveMeasureLessThan
                                                 ]

CEnum.CheckObjectiveForFollowingRollupConditions = [CEnum.Rollupconditions.satisfied
                                                  , CEnum.Rollupconditions.objectiveStatusKnown
                                                  , CEnum.Rollupconditions.objectiveMeasureKnown
]

CEnum.RollupObjectiveStatusElement = {
    objectiveProgressStatus: 'objectiveProgressStatus',
    objectiveSatisfiedStatus: 'objectiveSatisfiedStatus',
    objectiveMeasureStatus: 'objectiveMeasureStatus',
    objectiveNormalizedMeasure: 'objectiveNormalizedMeasure',
    attemptProgressStatus: 'attemptProgressStatus',
    attemptCompletionStatus: 'attemptCompletionStatus',
    attemptCompletionAmount: 'attemptCompletionAmount',
    attemptCompletionAmountStatus: 'attemptCompletionAmountStatus'
}
//CONSTRUCTOR

function CEnum() {
    this.NavigationRequest = CEnum.NavigationRequest;
    this.SequencingRequest = CEnum.SequencingRequest;
    this.DeliveryRequest = CEnum.DeliveryRequest;
    this.TerminationRequest = CEnum.TerminationRequest;
    this.SequencingConditionRules = CEnum.SequencingConditionRules;
    this.ControlModeAttributeValue = CEnum.ControlModeAttributeValue;
    this.CompletionStatus = CEnum.CompletionStatus;
    this.RandomizeChildrenProcess = CEnum.RandomizeChildrenProcess;
    this.LimitConditionsValues = CEnum.LimitConditionsValues;
    this.EndAttemptProcess = CEnum.EndAttemptProcess;
    this.AdlcpCompletionThreshold = CEnum.AdlcpCompletionThreshold;
    this.RollupAction = CEnum.RollupAction;
    this.ObjectiveTypes = CEnum.ObjectiveTypes;
    this.GetObjective = CEnum.GetObjective;
    this.ActivityType = CEnum.ActivityType;
    this.RuleConditions = CEnum.RuleConditions;
    this.RollupConditions = CEnum.Rollupconditions;
    this.CheckObjectiveForFollowingRuleConditions = CEnum.CheckObjectiveForFollowingRuleConditions;
    this.CheckObjectiveForFollowingRollupConditions = CEnum.CheckObjectiveForFollowingRollupConditions;
    this.RollupObjectiveStatusElement = CEnum.RollupObjectiveStatusElement;
}

