﻿/**
* Custom indexOf function implemented as in some browsers. Array.indexOf function is not available
*/
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
        for (var index = (start || 0), counter = this.length; index < counter; index++) {
            if (this[index] === obj) { return index; }
        }
        return -1;
    }
}

/**
* Scorm2004ADLNavChoice
* This data model element is not yet used, but when we add SCORM 2004 Sequencing support 
* we will use this as a starting point for this type of data.
* Public Properties:
*   value
*/
var scormApiInstance = null;
function Scorm2004ADLNavChoice(scorm2004Api) {
    this.scormApi = scorm2004Api;
    scormApiInstance = scorm2004Api;
    var access = Scorm2004DataModel.ACCESS;
    var type = Scorm2004DataModel.DATA_TYPE;
    this.value = new Scorm2004DataItem(this, access.ReadOnly, type.Vocabulary, type.Vocabulary.NavRequestValid); //REQ_50
    this.cEnum = new CEnum();
};

Scorm2004ADLNavChoice.prototype.currentActivity = null;
Scorm2004ADLNavChoice.prototype.currentActivityIndex = -1;
Scorm2004ADLNavChoice.prototype.suspendedActivity = null;
Scorm2004ADLNavChoice.prototype.suspendedActivityIndex = -1;
var TerminationRequest;
var SequencingRequest;
var Attempt_Progress_Status;
var Attempt_Completion_Status;

/**
* GetParentIdentifier( SCOIndex) 
* This  Function returns the parent identifier of the activity passed as parameter.
* @param scormApi instance of the Scorm2004Api.js 
*/
Scorm2004ADLNavChoice.prototype.GetParentIdentifier = function (SCOIndex) {
    var parentIdentifier = null;
    parentIdentifier = this.scormApi.imsManifest.organizations[0].items[SCOIndex].parentIdentifier;
    return parentIdentifier;
}
/**
* ExitConditionValues
* This objects is used to retrieve values from pseudo code.
*/
function ExitConditionValues() {
    this.value = null;
    this.identifiedActivity = null;
    this.deliverable = null;
    this.exception = null;
    this.nextActivity = null;
    this.endSequencingSession = null;
    this.terminationRequest = null;
    this.sequencingRequest = null;
    this.deliveryRequest = null;
    this.traversalDirection = null;
    this.navigationRequest = null;
    this.reachable = null;
    this.sequencingRulesCheckProcess = null;
    this.targetActivity = null;
    this.objectiveProgressStatus = null;
    this.objectiveSatisfiedStatus = null;
    this.objectiveMeasureStatus = null;
    this.objectiveNormalizedMeasure = 0.0;
    this.attemptProgressStatus = null;
    this.attemptCompletionStatus = null;
    this.attemptCompletionAmount = null;
    this.activityProgressStatus = null;
    this.action = null;
}


/**
* GetItemIndex( item) 
* This  Function returns item index of item activity passed as parameter.
* @param scormApi instance of the Scorm2004Api.js
* @param item is the identifier of activity.
*/
Scorm2004ADLNavChoice.prototype.GetItemIndex = function (item) {

    var index = 0;
    while (index < this.scormApi.imsManifest.organizations[0].items.length) {
        if (item == this.scormApi.imsManifest.organizations[0].items[index].identifier) {
            return index;
        }
        index++;
    }
}

/**
* Sequencing Exit Action Rules Subprocess [TB.2.1] -- 180
* SequencingExitActionRuleSubProcess(TerminationRequest) --[TB.2.1]
* This function is used to apply the Sequencing Exit Action Rule SubProcess on the current activity.
* @param scormApi instance of the Scorm2004Api.js 
**/
Scorm2004ADLNavChoice.prototype.SequencingExitActionRuleSubProcess = function (currentActivityIndex) {
    this.scormApi.DebuggerTraceCall('SequencingExitActionRuleSubProcess');
    try {

        var returnedResultSet = new ExitConditionValues();
        var resultSet = new ExitConditionValues();
        var sortedResult = new Array();

        sortedResult = this.GetItemNamesHierarchyFromRoot(this.GetItemIndex(this.GetParentIdentifier(currentActivityIndex)), 0, true, true, false);

        var exit_target = undefined;

        for (index = 0; index < sortedResult.length; index++) {

            returnedResultSet = this.SequencingRulesCheckProcess(this.GetItemIndex(sortedResult[index]), [this.cEnum.SequencingConditionRules.ExitConditionRuleAction.Exit], this.cEnum.SequencingConditionRules.ExitConditionRule);
            if (returnedResultSet.action != null) {
                // set the exit target to the activity
                exit_target = sortedResult[index];
                break;
            }
        }

        if (exit_target != undefined) {

            this.TerminateDescendentAttemptsProcess(exit_target);

            this.EndAttemptProcess(exit_target);

            // set the Current Activity to the exit target
            this.SetCurrentActivity(exit_target);

        }
        return;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SequencingExitActionRuleSubProcess", "", 1);
        throw e;
    }
}
/**
* Sequencing Post Condition Rules Subprocess [TB.2.2]
* SequencingPostConditionRulesSubprocess(TerminationRequest)
* This function is used to apply the sequencing post action rule subProcess on the current activity.
* @param scormApi instance of the Scorm2004Api.js 
* @param TerminationRequest string represent adl nav request value
* @return termination request or sequencing request.
*/
Scorm2004ADLNavChoice.prototype.SequencingPostConditionRulesSubprocess = function (currentActivityIndex) {
    this.scormApi.DebuggerTraceCall('SequencingPostConditionRulesSubprocess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();
        var data = this.scormApi.courseDataModel;
        var IsActivityIsActive = data[currentActivityIndex].cmi.isActive.value;
        var IsSuspended = data[currentActivityIndex].cmi.isSuspended.value;

        if (IsSuspended) {
            return resultSet;
        }

        returnedResultSet = this.SequencingRulesCheckProcess(currentActivityIndex,
                                                            [
                                                                  this.cEnum.SequencingConditionRules.PostConditionRuleAction.ExitParent
                                                                , this.cEnum.SequencingConditionRules.PostConditionRuleAction.ExitAll
                                                                , this.cEnum.SequencingConditionRules.PostConditionRuleAction.Retry
                                                                , this.cEnum.SequencingConditionRules.PostConditionRuleAction.RetryAll
                                                                , this.cEnum.SequencingConditionRules.PostConditionRuleAction.Continue
                                                                , this.cEnum.SequencingConditionRules.PostConditionRuleAction.Previous
                                                            ], this.cEnum.SequencingConditionRules.PostConditionRule);

        // pseudo code 3
        if (returnedResultSet.action != null) {

            // if the Sequencing Rules Check Process returned Retry, Continue, Or Previous Then

            // pseudo code 3.1
            if (returnedResultSet.action == this.cEnum.SequencingConditionRules.PostConditionRuleAction.Retry
            	|| returnedResultSet.action == this.cEnum.SequencingConditionRules.PostConditionRuleAction.Continue
	        || returnedResultSet.action == this.cEnum.SequencingConditionRules.PostConditionRuleAction.Previous) {

                resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                resultSet.sequencingRequest = returnedResultSet.action;
                return resultSet;
            }

            // pseudo code 3.2
            if (returnedResultSet.action == this.cEnum.SequencingConditionRules.PostConditionRuleAction.ExitParent
	        || returnedResultSet.action == this.cEnum.SequencingConditionRules.PostConditionRuleAction.ExitAll) {
                resultSet.terminationRequest = returnedResultSet.action;
                resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                return resultSet;
            }

            // pseudo code 3.3
            if (returnedResultSet.action == this.cEnum.SequencingConditionRules.PostConditionRuleAction.RetryAll) {
                resultSet.terminationRequest = this.cEnum.TerminationRequest.ExitAll;
                resultSet.sequencingRequest = this.cEnum.SequencingConditionRules.PostConditionRuleAction.Retry;
                return resultSet;
            }
        }

        resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
        resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
        return resultSet;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SequencingPostConditionRulesSubprocess", "", 1);
        throw e;
    }
}

/** -204
* EndAttemptProcess() End Attempt Process [UP.4]
* This  Function ensures that state of the terminating (“exiting”) activity is up to date.
* @param scormApi instance of the Scorm2004Api.js 
*/
Scorm2004ADLNavChoice.prototype.EndAttemptProcess = function (itemIdentifier) {
    this.scormApi.DebuggerTraceCall('EndAttemptProcess');
    try {
        var itemIndex = this.GetItemIndex(itemIdentifier);
        var returnResultSet = new ExitConditionValues();
        // if the activity is a leaf Then  
        if (this.IsLeafActivity(itemIdentifier)) {

            var data = this.scormApi.courseDataModel;
            var IsActivityIsActive = data[itemIndex].cmi.isActive.value;
            var IsSuspended = data[itemIndex].cmi.isSuspended.value;

            if (this.GetSequencingElementsAttributeValue(itemIndex, false, "deliveryControls", "tracked", true)) {

                // this.RunSetrteForItemAndItsObjectives(itemIndex);

                // if the Activity is Suspended for the activity is False
                if (!IsSuspended) {
                    // if the Completion Set by Content for the activity is False  
                    if (!this.GetSequencingElementsAttributeValue(itemIndex, false, "deliveryControls", "completionSetByContent", false)) {
                        if (!this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus)) {
                            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, true);
                            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, true);
                        }
                    }

                    // if Objective Set by Content for the activity is False
                    if (!this.GetSequencingElementsAttributeValue(itemIndex, false, "deliveryControls", "objectiveSetByContent", false)) {

                        var result = this.GetPrimaryObjectiveFromCDM(itemIndex);
                        var itemCmi = this.GetItemCmi(itemIndex);
                        var primaryObjective = this.GetPrimaryObjectiveFromCDM(itemIndex);

                        if (result != null) {

                            if (!result.objectiveProgressStatus.value && !itemCmi.successStatusChangedDuringRuntime.value && !primaryObjective.successStatusChangedDuringRuntime.value) {
                                result.objectiveProgressStatus.value = true;
                                result.objectiveSatisfiedStatus.value = true;

                            }
                        }
                    }
                }
            }
        }
        else {
            var AvailableChildren = new Array();
            AvailableChildren = this.scormApi.courseDataModel[itemIndex].cmi.availableChildren;
            var IsSuspended = false;
            for (index = 0; index < AvailableChildren.length; index++) {

                var itemIdentifier = AvailableChildren[index];
                var childItemIndex = this.GetItemIndex(itemIdentifier);
                var IsSuspended = this.scormApi.courseDataModel[childItemIndex].cmi.isSuspended.value;

                if (IsSuspended) {
                    break;
                }
            }

            // if the activity includes any child activity whose Activity is Suspended attribute is True
            if (IsSuspended) {
                // set the Activity is Suspended for the activity to True
                this.scormApi.courseDataModel[itemIndex].cmi.isSuspended.value = true;
            }
            else {
                // set the Activity is Suspended for the activity to False
                this.scormApi.courseDataModel[itemIndex].cmi.isSuspended.value = false;
            }
        }
        // set the Activity is Active for the activity to False
        this.scormApi.courseDataModel[itemIndex].cmi.isActive.value = false;

        // apply the Overall Rollup Process to the activity
        this.OverallRollupProcess(itemIndex);
        this.UpdateActivitiesStatus();
    }
    catch (e) {
        this.scormApi.traceError("Error caught in EndAttemptProcess", "", 1);
        throw e;
    }
}


/**
* Termination Request Process [TB.2.3]
* TerminationRequestProcess(TerminationRequest)
* This  Function ensures that state of the terminating (“exiting”) activity is up to date.
* @param scormApi                   instance of the Scorm2004Api.js 
* @param TerminationRequest         is of termination request type.
*/
Scorm2004ADLNavChoice.prototype.TerminationRequestProcess = function (TerminationRequest) {
    this.scormApi.DebuggerTraceCall('TerminationRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();
        var data = this.scormApi.courseDataModel;
        var IsActivityIsActive = data[this.GetItemIndex(this.currentActivity)].cmi.isActive.value;
        var IsAcivityIsSuspended = data[this.GetItemIndex(this.currentActivity)].cmi.isSuspended.value;

        if (this.currentActivity == null) {
            resultSet.terminationRequest = this.cEnum.TerminationRequest.NotValid;
            resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
            resultSet.exception = Scorm2004Api.ERRORS.TB_2_3_1;
            return resultSet;
        }

        if ((TerminationRequest == this.cEnum.TerminationRequest.Exit || TerminationRequest == this.cEnum.TerminationRequest.Abandon) && !IsActivityIsActive) {
            resultSet.terminationRequest = this.cEnum.TerminationRequest.NotValid;
            resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
            resultSet.exception = Scorm2004Api.ERRORS.TB_2_3_2;
            return resultSet;
        }

        switch (TerminationRequest) {
            // 3. Case: termination request is Exit
            case this.cEnum.TerminationRequest.Exit:
                {
                    this.EndAttemptProcess(this.currentActivity);

                    this.SequencingExitActionRuleSubProcess(this.GetItemIndex(this.currentActivity));

                    var processed_exit;

                    do {
                        processed_exit = false;

                        returnedResultSet = this.SequencingPostConditionRulesSubprocess(this.currentActivityIndex);

                        // 3.3.3. If the Sequencing Post Condition Rule Subprocess returned a termination request of Exit All Then
                        if (returnedResultSet.terminationRequest == this.cEnum.SequencingRequest.ExitAll) {

                            returnedResultSet = this.TerminationRequestProcess(this.cEnum.TerminationRequest.ExitAll);
                            break;

                        }
                        // 3.3.4. If the Sequencing Post Condition Rule Subprocess returned a termination request of Exit Parent Then
                        if (returnedResultSet.terminationRequest == this.cEnum.SequencingConditionRules.PostConditionRuleAction.ExitParent) {

                            if (!this.IsRootActivity(this.currentActivity)) {
                                // set the Current Activity to the parent of the Current Activity 
                                this.SetCurrentActivity(this.GetParentIdentifier(this.GetItemIndex(this.currentActivity)));
                                this.EndAttemptProcess(this.currentActivity);

                                processed_exit = true;
                            }
                            else {
                                resultSet.terminationRequest = this.cEnum.TerminationRequest.NotValid;
                                resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                                resultSet.exception = Scorm2004Api.ERRORS.TB_2_3_4;
                                return resultSet;
                            }
                        }
                        else {
                            // 3.3.5.1. If the Current Activity is the Root of the Activity Tree And
                            // the sequencing request returned by the Sequencing Post
                            // condition Rule Subprocess is Not Retry Then
                            if (this.IsRootActivity(this.currentActivity) && returnedResultSet.sequencingRequest != this.cEnum.SequencingRequest.Retry) {
                                resultSet.terminationRequest = this.cEnum.TerminationRequest.Valid;
                                resultSet.sequencingRequest = this.cEnum.SequencingRequest.Exit;
                                return resultSet;
                            }
                        }
                    } while (processed_exit !== false);

                    resultSet.terminationRequest = this.cEnum.TerminationRequest.Valid;
                    resultSet.sequencingRequest = returnedResultSet.sequencingRequest;
                    return resultSet;
                }
            case this.cEnum.TerminationRequest.ExitAll:
                {
                    if (IsActivityIsActive) {
                        this.EndAttemptProcess(this.currentActivity);
                    }

                    this.TerminateDescendentAttemptsProcess(this.GetItemIdentifier(0));
                    this.EndAttemptProcess(this.GetItemIdentifier(0));
                    this.SetCurrentActivity(this.scormApi.imsManifest.organizations[0].items[0].identifier);
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.Valid;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Exit;
                    return resultSet;

                }

            case this.cEnum.TerminationRequest.SuspendAll:
                {
                    if (IsActivityIsActive || IsAcivityIsSuspended) {
                        this.OverallRollupProcess(this.GetItemIndex(this.currentActivity));
                        this.SetSuspendedActivity(this.currentActivity);
                    }
                    else {
                        if (!this.IsRootActivity(this.currentActivity)) {
                            this.SetSuspendedActivity(this.GetParentIdentifier(this.currentActivity));
                        }
                        else {
                            resultSet.terminationRequest = this.cEnum.TerminationRequest.NotValid;
                            resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                            resultSet.exception = Scorm2004Api.ERRORS.TB_2_3_3;
                            return resultSet;
                        }
                    }

                    // form the activity path as the ordered series of all activities from the Suspended Activity to the root of the activity tree, inclusive
                    var sortedResult = new Array();
                    sortedResult = this.GetItemNamesHierarchyFromRoot(this.suspendedActivityIndex, 0, true, true, false);
                    if (sortedResult.length == 0) {
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.NotValid;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                        resultSet.exception = Scorm2004Api.ERRORS.TB_2_3_5;
                        return resultSet;
                    }
                    for (index = 0; index < sortedResult.length; index++) {
                        var itemIndex = this.GetItemIndex(sortedResult[index]);
                        data[itemIndex].cmi.isActive.value = false;
                        data[itemIndex].cmi.isSuspended.value = true;

                    }
                    // set the Current Activity to the root of the activity tree
                    this.SetCurrentActivity(this.GetItemIdentifier(0));
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.Valid;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Exit;
                    return resultSet;
                }

            case this.cEnum.TerminationRequest.Abandon:
                {
                    // set Activity is Active for the Current Activity to False
                    data[this.currentActivityIndex].cmi.isActive.value = false;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.Valid;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    return resultSet;
                }

            case this.cEnum.TerminationRequest.AbandonAll:
                {
                    // form the activity path as the ordered series of all activities from the Current Activity to the root of the activity tree, inclusive
                    var ActivityPath = new Array();
                    ActivityPath = this.GetItemNamesHierarchyFromRoot(this.currentActivityIndex, 0, true, true, false);
                    if (ActivityPath.length == 0) {
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.NotValid;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                        resultSet.exception = Scorm2004Api.ERRORS.TB_2_3_6;
                        return resultSet;
                    }
                    for (index = 0; index < ActivityPath.length; index++) {

                        data[this.GetItemIndex(ActivityPath[index])].cmi.isActive.value = false;
                    }
                    // set the Current Activity to the root of the activity tree
                    this.SetCurrentActivity(this.GetItemIdentifier(0));
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.Valid;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Exit;
                    return resultSet;
                }
                resultSet.terminationRequest = this.cEnum.TerminationRequest.NotValid;
                resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                resultSet.exception = Scorm2004Api.ERRORS.TB_2_3_7;
                return resultSet;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in TerminationRequestProcess", "", 1);
        throw e;
    }
}

/**
* IsLeafActivity() 
* This Function is used to check whether the currently active SCO is a Leaf Activity or not.
* @param scormApi instance of the Scorm2004Api.js 
* @return returnVal - true or false.
*/
Scorm2004ADLNavChoice.prototype.IsLeafActivity = function (itemIdentifier) {
    this.scormApi.DebuggerTraceCall('IsLeafActivity');
    try {
        for (var index = 0; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {

            if (itemIdentifier == this.scormApi.imsManifest.organizations[0].items[index].parentIdentifier) {
                return false;
            }
        }
        return true;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in IsLeafActivity", "", 1);
        throw e;
    }
}

/**
* ControlModeAttributeValue( controlModeAttribute) 
* This Function is used to get the control mode attribute value.
* @param controlModeAttribute string represent the control mode attribute.
* @return returnVal - true or false.
*/
Scorm2004ADLNavChoice.prototype.ControlModeAttributeValue = function (itemIndex, controlModeAttribute, ofParent) {
    this.scormApi.DebuggerTraceCall('ControlModeAttributeValue');
    try {
        var result = this.GetControlModeAttributeValue(itemIndex, controlModeAttribute, ofParent);

        if (result == undefined) {
            switch (controlModeAttribute) {
                case this.cEnum.ControlModeAttributeValue.Choice:
                    return true;
                    break;
                case this.cEnum.ControlModeAttributeValue.ChoiceExit:
                    return true;
                    break;
                case this.cEnum.ControlModeAttributeValue.Flow:
                    return false;
                    break;

                case this.cEnum.ControlModeAttributeValue.ForwardOnly:
                    return false;
                    break;
                case this.cEnum.ControlModeAttributeValue.UseCurrentAttemptObjectiveInfo:
                    return true;
                    break;
                case this.cEnum.ControlModeAttributeValue.UseCurrentAttemptProgressInfo:
                    return true;
                    break;
            }
        }
        else {
            return result;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ControlModeAttributeValue", "", 1);
        throw e;
    }
}

/**
* GetControlModeAttributeValue(controlModeAttribute) 
* This Function is used to check whether the currently active SCO is a Leaf Activity or not.
* @param scormApi instance of the Scorm2004Api.js 
* @return returnVal - true or false.
*/
Scorm2004ADLNavChoice.prototype.GetControlModeAttributeValue = function (itemIndex, controlModeAttribute, ofParent) {
    this.scormApi.DebuggerTraceCall('GetControlModeAttributeValue');
    try {
        var parentIdentifier = null;
        var controlMode;
        var imsssSequencing;
        var itemIndex;
        var tempIndex = itemIndex;

        if (ofParent) {

            parentIdentifier = this.scormApi.imsManifest.organizations[0].items[tempIndex].parentIdentifier;
        }
        else {
            parentIdentifier = this.scormApi.imsManifest.organizations[0].items[itemIndex].identifier;
        }


        if (parentIdentifier != null) {
            for (var index = 0; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {
                if (this.scormApi.imsManifest.organizations[0].items[index].identifier == parentIdentifier) {
                    imsssSequencing = this.scormApi.imsManifest.organizations[0].items[index].imsssSequencing;
                    if (imsssSequencing != null) {
                        controlMode = imsssSequencing.controlMode;
                    }
                    break;
                }
            }
        }
        else {
            imsssSequencing = this.scormApi.imsManifest.organizations[0].imsssSequencing;
            if (imsssSequencing != null) {
                controlMode = imsssSequencing.controlMode;
            }
        }

        if (controlMode != null) {
            switch (controlModeAttribute) {
                case this.cEnum.ControlModeAttributeValue.Choice:
                    return controlMode.choice;
                    break;
                case this.cEnum.ControlModeAttributeValue.ChoiceExit:
                    return controlMode.choiceExit;
                    break;
                case this.cEnum.ControlModeAttributeValue.Flow:
                    return controlMode.flow;
                    break;
                case this.cEnum.ControlModeAttributeValue.ForwardOnly:
                    return controlMode.forwardOnly;
                    break;
                case this.cEnum.ControlModeAttributeValue.UseCurrentAttemptObjectiveInfo:
                    return controlMode.useCurrentAttemptObjectiveInfo;
                    break;
                case this.cEnum.ControlModeAttributeValue.UseCurrentAttemptProgressInfo:
                    return controlMode.useCurrentAttemptProgressInfo;
                    break;
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetControlModeAttributeValue", "", 1);
        throw e;
    }
}

/**
* Sequencing Request Process [SB.2.12]
* SequencingRequestProcess( requestType, targetActivity) 
* This Function is used to process all types of sequencing request.
* @param scormApi instance of the Scorm2004Api.js 
* @param requestType string represent adl nav request value
* @param targetActivity string represents the target activity in case of only choice request type otherwise it is null.
*/
Scorm2004ADLNavChoice.prototype.SequencingRequestProcess = function (requestType, targetActivity) {
    this.scormApi.DebuggerTraceCall('SequencingRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        switch (requestType) {

            // pseudo code point 1                                                                                                                                                                                                                                                                                                                                                                                      
            case this.cEnum.SequencingRequest.Start:

                returnedResultSet = this.StartSequencingRequestProcess();

                if (returnedResultSet.exception != null) {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotValid;
                    resultSet.exception = returnedResultSet.exception;
                    return resultSet;
                }
                else {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Valid;
                    resultSet.deliveryRequest = returnedResultSet.deliveryRequest;

                    resultSet.endSequencingSession = returnedResultSet.endSequencingSession;
                    return resultSet;
                }
                break;

                // pseudo code point 2                                                                                                                                                                                                                                                                                                                                                                                      
            case this.cEnum.SequencingRequest.ResumeAll:

                returnedResultSet = this.ResumeAllSequencingRequestProcess();

                if (returnedResultSet.exception != null) {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotValid;
                    resultSet.exception = returnedResultSet.exception;
                    return resultSet;
                }
                else {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Valid;
                    resultSet.deliveryRequest = returnedResultSet.deliveryRequest;

                    return resultSet;
                }
                break;

                // pseudo code point 3                                                                                                                                                                                                                                                                                                                                                                                      
            case this.cEnum.SequencingRequest.Exit:

                returnedResultSet = this.ExitSequencingRequestProcess();

                if (returnedResultSet.exception != null) {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotValid;
                    resultSet.exception = returnedResultSet.exception;
                    return resultSet;
                }
                else {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Valid;
                    resultSet.endSequencingSession = returnedResultSet.endSequencingSession;
                    return resultSet;
                }
                break;

                // pseudo code point 4                                                                                                                                                                                                                                                                                                                                                                                      
            case this.cEnum.SequencingRequest.Retry:

                returnedResultSet = this.RetrySequencingRequestProcess();

                if (returnedResultSet.exception != null) {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotValid;
                    resultSet.exception = returnedResultSet.exception;
                    return resultSet;
                }
                else {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Valid;
                    resultSet.deliveryRequest = returnedResultSet.deliveryRequest;

                    return resultSet;
                }
                break;

                // pseudo code point 5                                                                                                                                                                                                                                                                                                                                                                                      
            case this.cEnum.SequencingRequest.Continue:

                returnedResultSet = this.ContinueSequencingRequestProcess();

                if (returnedResultSet.exception != null) {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotValid;
                    resultSet.exception = returnedResultSet.exception;
                    return resultSet;
                }
                else {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Valid;
                    resultSet.deliveryRequest = returnedResultSet.deliveryRequest;
                    resultSet.deliverable = resultSet.deliveryRequest; //extra valued added
                    resultSet.endSequencingSession = returnedResultSet.endSequencingSession;
                    return resultSet;
                }
                break;

                // pseudo code point 6                                                                                                                                                                                                                                                                                                                                                                                      
            case this.cEnum.SequencingRequest.Previous:

                returnedResultSet = this.PreviousSequencingRequestProcess();

                if (returnedResultSet.exception != null) {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotValid;
                    resultSet.exception = returnedResultSet.exception;
                    return resultSet;
                }
                else {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Valid;
                    resultSet.deliveryRequest = returnedResultSet.deliveryRequest;

                    return resultSet;
                }
                break;

                // pseudo code point 7                                                                                                                                                                                                                                                                                                                                                                                      
            case this.cEnum.SequencingRequest.Choice:

                returnedResultSet = this.ChoiceSequencingRequestProcess(targetActivity);

                if (returnedResultSet.exception != null) {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotValid;
                    resultSet.exception = returnedResultSet.exception;
                    return resultSet;
                }
                else {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Valid;
                    resultSet.deliveryRequest = returnedResultSet.deliveryRequest;

                    return resultSet;
                }
                break;

                // pseudo code point 8                                                                                                                                                                                                                                                                                                                                                                                      
            case this.cEnum.SequencingRequest.Jump:

                returnedResultSet = this.JumpSequencingRequestProcess(targetActivity);

                if (returnedResultSet.exception != null) {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotValid;
                    resultSet.exception = returnedResultSet.exception;
                    return resultSet;
                }
                else {
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Valid;
                    resultSet.deliveryRequest = returnedResultSet.deliveryRequest;

                    return resultSet;
                }
                break;

            default:
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_12_1, "", 1);
                resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotValid;
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_12_1;
                return resultSet;
                break;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SequencingRequestProcess", "", 1);
        throw e;
    }
}


/**
* GetItemNamesHierarchyFromRoot( toItemIndex, fromItemIndex, includeToItem, includeFromItem, includeOnlyVisible) 
* This Function is used to get array of activities between 2 activities.
* @param scormApi instance of the Scorm2004Api.js 
* @param toItemIndex            toitem index till where list should made
* @param fromItemIndex          from Item index from where list should start
* @param includeToItem          should toItem be included in list
* @param includeFromItem        should From Item be included in list
* @param includeOnlyVisible     should we consider only visible items--need to be removed in future
*/
Scorm2004ADLNavChoice.prototype.GetItemNamesHierarchyFromRoot = function (toItemIndex, fromItemIndex, includeToItem, includeFromItem, includeOnlyVisible) {
    this.scormApi.DebuggerTraceCall('GetItemNamesHierarchyFromRoot');
    try {

        var result = new Array();
        var sortedResult = new Array();
        var tempIndex;

        if (fromItemIndex > toItemIndex) {
            var temp = fromItemIndex;
            fromItemIndex = toItemIndex;
            toItemIndex = temp;
        }

        tempIndex = toItemIndex;
        while (tempIndex >= fromItemIndex) {
            result.push(this.scormApi.imsManifest.organizations[0].items[tempIndex].identifier);
            if (this.scormApi.imsManifest.organizations[0].items[tempIndex].parentIdentifier == null) {
                break;
            }
            tempIndex = this.GetItemIndex(this.scormApi.imsManifest.organizations[0].items[tempIndex].parentIdentifier);
        }

        for (var index = 0; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {
            if (result.indexOf(this.scormApi.imsManifest.organizations[0].items[index].identifier) > -1) {
                sortedResult.push(this.scormApi.imsManifest.organizations[0].items[index].identifier);
            }
        }

        if (sortedResult.length > 0) {

            if (!includeToItem) {
                sortedResult.pop();
            }

            if (!includeFromItem) {
                sortedResult.shift();
            }
        }
        return sortedResult;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetItemNamesHierarchyFromRoot", "", 1);
        throw e;
    }
}

/**
* GetAvailableChildren( itemIndex) 
* This Function is used to get available children of an item
* @param itemIndex item index for which children to be fetched
*/
Scorm2004ADLNavChoice.prototype.GetAvailableChildren = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('GetAvailableChildren');
    try {
        var result = new Array();
        var identifier;
        if (isNullOrUndefined(itemIndex)) {
            identifier = null;
        }
        else {
            identifier = this.scormApi.imsManifest.organizations[0].items[itemIndex].identifier;
        }

        for (var index = 0; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {
            if (this.scormApi.imsManifest.organizations[0].items[index].parentIdentifier == identifier) {
                result.push(this.scormApi.imsManifest.organizations[0].items[index].identifier);
            }
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetAvailableChildren", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.GetCommonAncestor1 = function (firstItemIndex, secondItemIndex) {

    var firstItemIdentifier = this.scormApi.imsManifest.organizations[0].items[firstItemIndex].identifier;
    var secondItemIdentifier = this.scormApi.imsManifest.organizations[0].items[secondItemIndex].identifier;
    var organizationIdentifier = this.scormApi.imsManifest.organizations[0].identifier;

    if (firstItemIndex == null || secondItemIndex == null || firstItemIndex == 0 || secondItemIndex == 0 ||
        firstItemIdentifier == organizationIdentifier || secondItemIdentifier == organizationIdentifier) {
        return organizationIdentifier;
    }

    var _currentNodeTemp = firstItemIdentifier;

    while (_currentNodeTemp !== organizationIdentifier) {

        var _targetNodeTemp = secondItemIdentifier;

        while (_targetNodeTemp !== organizationIdentifier) {
            if (_currentNodeTemp === _targetNodeTemp) {
                return _currentNodeTemp;
            }

            var index = this.GetItemIndex(_targetNodeTemp);
            _targetNodeTemp = this.scormApi.imsManifest.organizations[0].items[index].parentIdentifier;
        }

        var index = this.GetItemIndex(_currentNodeTemp);
        _currentNodeTemp = this.scormApi.imsManifest.organizations[0].items[index].parentIdentifier;
    }
    return organizationIdentifier;
}


/**
* GetCommonAncestor( firstItemIndex, secondItemIndex) // s_pool 3169
* This Function is used to get common ancestor identifier of 2 items.
* @param firstItemIndex         first item index
* @param secondItemIndex        second item index
*/
Scorm2004ADLNavChoice.prototype.GetCommonAncestor = function (firstItemIndex, secondItemIndex) {
    this.scormApi.DebuggerTraceCall('GetCommonAncestor');
    try {
        var firstParentIdentifier;
        var firstParentLevel;
        var secondParentIdentifier;
        var secondParentLevel;
        var commonAncestorItem;
        var organizationIdentifier = this.scormApi.imsManifest.organizations[0].identifier;

        firstParentIdentifier = this.scormApi.imsManifest.organizations[0].items[firstItemIndex].parentIdentifier;
        secondParentIdentifier = this.scormApi.imsManifest.organizations[0].items[secondItemIndex].parentIdentifier;

        if (firstParentIdentifier == null || firstParentIdentifier == organizationIdentifier || secondParentIdentifier == organizationIdentifier || secondParentIdentifier == null) {
            return organizationIdentifier;
        }

        firstItemIndex = this.GetItemIndex(firstParentIdentifier);
        secondItemIndex = this.GetItemIndex(secondParentIdentifier);


        firstParentLevel = this.scormApi.imsManifest.organizations[0].items[firstItemIndex].level;
        secondParentLevel = this.scormApi.imsManifest.organizations[0].items[secondItemIndex].level;

        if (firstParentIdentifier == secondParentIdentifier) {
            commonAncestorItem = firstParentIdentifier;
            return commonAncestorItem;
        }
        else if (firstParentLevel > secondParentLevel) {
            while (firstParentLevel > secondParentLevel) {
                firstParentIdentifier = this.scormApi.imsManifest.organizations[0].items[firstItemIndex].parentIdentifier;
                firstItemIndex = this.GetItemIndex(firstParentIdentifier);
                firstParentLevel = this.scormApi.imsManifest.organizations[0].items[firstItemIndex].level;
            }
        }
        else {
            while (secondParentLevel > firstParentLevel) {
                secondParentIdentifier = this.scormApi.imsManifest.organizations[0].items[secondItemIndex].parentIdentifier;
                secondItemIndex = this.GetItemIndex(secondParentIdentifier);
                secondParentLevel = this.scormApi.imsManifest.organizations[0].items[secondItemIndex].level;
            }
        }

        if (firstParentIdentifier == secondParentIdentifier) {
            commonAncestorItem = firstParentIdentifier;
            return commonAncestorItem;
        }
        else {
            while (firstParentIdentifier != secondParentIdentifier) {
                firstParentIdentifier = this.scormApi.imsManifest.organizations[0].items[firstItemIndex].parentIdentifier;
                firstItemIndex = this.GetItemIndex(firstParentIdentifier);

                secondParentIdentifier = this.scormApi.imsManifest.organizations[0].items[secondItemIndex].parentIdentifier;
                secondItemIndex = this.GetItemIndex(secondParentIdentifier);
            }
        }

        if (firstParentIdentifier == null || secondParentIdentifier == null) {
            return organizationIdentifier;
        }
        return firstParentIdentifier;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetCommonAncestor", "", 1);
        throw e;
    }
}

/**
* GetSequencingElementsAttributeValue( itemIndex, ofParent, elementName, attributeName, defaultValue) 
* This Function is used to get common ancestor identifier of 2 items.
* @param itemIndex              item index
* @param ofParent               should we fetch value of Parent of item passed as parameter
* @param elementName            name of xml element
* @param attributeName          name of attribute for which value should be fetched
* @param defaultValue           default value of the attribute
*/
Scorm2004ADLNavChoice.prototype.GetSequencingElementsAttributeValue = function (itemIndex, ofParent, elementName, attributeName, defaultValue) {
    this.scormApi.DebuggerTraceCall('GetSequencingElementsAttributeValue');
    try {
        var result = defaultValue;
        var parentIdentifier;

        if (ofParent) {
            parentIdentifier = this.scormApi.imsManifest.organizations[0].items[itemIndex].parentIdentifier;
        }
        else {
            parentIdentifier = this.scormApi.imsManifest.organizations[0].items[itemIndex].identifier;
        }

        var imsssSequencing;

        if (parentIdentifier != null) {
            itemIndex = this.GetItemIndex(parentIdentifier);
            imsssSequencing = this.scormApi.imsManifest.organizations[0].items[itemIndex].imsssSequencing;
        }
        else {
            imsssSequencing = this.scormApi.imsManifest.organizations[0].imsssSequencing;
        }

        if (imsssSequencing != null) {
            var element = imsssSequencing[elementName];

            if (element != null) {
                return result = element[attributeName];
            }

            else if (imsssSequencing["idRef"] != null) {
                element = this.FetchSequencingAttributeFromSubnode(imsssSequencing, elementName);
                if (element != null) {

                    if (element[attributeName] != null) {
                        result = element[attributeName];
                    }
                }
            }
        }
        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetSequencingElementsAttributeValue", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.FetchSequencingAttributeFromSubnode = function (imsssSequencing, elementName) {
    this.scormApi.DebuggerTraceCall('FetchSequencingAttributeFromSubnode');

    var result = null;

    if (imsssSequencing["idRef"] != null) {

        var sequencingCollection = this.scormApi.imsManifest.sequencingCollection;

        for (var index = 0; index < sequencingCollection.length; index++) {

            if (imsssSequencing["idRef"] == sequencingCollection[index].id) {

                var element = sequencingCollection[index][elementName];
                if (element != null) {
                    result = element;
                }

            }
        }
    }
    return result;
}

/**
* IsRootActivity( SCOIndex) 
* This Function is used to check if the item passed is root of the activity tree or not.
* @param SCOIndex               item index
*/
Scorm2004ADLNavChoice.prototype.IsRootActivity = function (itemIdentifier) {
    this.scormApi.DebuggerTraceCall('IsRootActivity');
    try {
        if (this.scormApi.imsManifest.organizations[0].identifier == itemIdentifier) {
            return true;
        }
        else {
            false;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in IsRootActivity", "", 1);
        throw e;
    }
}


/**
* Sequencing Rules Check Process [UP.2]
* SequencingRulesCheckProcess( itemIndex, activityAction, conditionRules)
* This Function is used to For an activity and a set of Rule Actions; returns the action to apply or Nil.
* @param itemIndex              item index 
* @param activityAction         string type activity of corresponding  ConditionRules like for preConditionRules activityAction as "disabled|skip" etc.
* @param conditionRules         string type   ConditionRules like for "preConditionRules|postConditions|exitConditins" .
*/
Scorm2004ADLNavChoice.prototype.SequencingRulesCheckProcess = function (itemIndex, activityAction, conditionRules) {
    this.scormApi.DebuggerTraceCall('SequencingRulesCheckProcess');
    try {
        var resultSet = new ExitConditionValues();

        var imsssSequencing = this.scormApi.imsManifest.organizations[0].items[itemIndex].imsssSequencing;

        if (imsssSequencing != null) {
            // if the activity includes Sequencing Rules with any of the specified Rule Actions      
            // initialize rules list by selecting the set of Sequencing Rules for the activity that have any of the specified Rule Actions, maintaining original rule ordering
            var orderedSequencingRules = new Array();
            // created instance ConditionRules dynamically
            var condition_Rule = this.GetSequencingRules(imsssSequencing, conditionRules);

            if (condition_Rule != null) {
                for (var index = 0; index < condition_Rule.length; index++) {

                    var data = this.GetInstanceAttributeValue(condition_Rule[index], "ruleAction");

                    if (activityAction.indexOf(data) > -1) {
                        orderedSequencingRules.push(condition_Rule[index]);
                    }
                }

                // for each rule in the rules list
                if (orderedSequencingRules.length > 0) {
                    // if the activity includes Sequencing Rules with any of the specified Rule Actions

                    for (var index = 0; index < orderedSequencingRules.length; index++) {
                        // apply the Sequencing Rule Check Subprocess to the activity and the rule             
                        returnedResultSet = this.SequencingRuleCheckSubprocess(itemIndex, orderedSequencingRules[index]);
                        // stop at the first rule  that evaluates to true - perform the associated action.
                        if (returnedResultSet.value) {
                            // exit Sequencing Rules Check Process (Action: Rule Action for the rule)
                            resultSet.action = orderedSequencingRules[index].ruleAction;
                            return resultSet;
                        }
                    }
                }
            }
        }
        return resultSet;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SequencingRulesCheckProcess", "", 1);
        throw e;
    }
}

/**
* Sequencing Rule Check Subprocess [UP.2.1]
* SequencingRuleCheckSubprocess(imsssSequencing, conditionType, ruleAction)
* This Function is used to For an activity and a set of Rule Actions; returns the action to apply or Nil.
* @param SCOIndex               item index
*/
Scorm2004ADLNavChoice.prototype.SequencingRuleCheckSubprocess = function (itemIndex, sequencingRule) {
    this.scormApi.DebuggerTraceCall('SequencingRuleCheckSubprocess');
    try {
        var resultSet = new ExitConditionValues();
        resultSet.value = false;
        var ruleBag = new Array();
        var ruleActionsResultArray = new Array();
        var ruleConditions = sequencingRule.ruleConditions;
        var conditionCombination = sequencingRule.conditionCombination;
        var ruleAction = sequencingRule.ruleAction;

        if (ruleConditions != null) {
            // 2. for each Rule Condition for the Sequencing Rule for the activity
            for (var index = 0; index < ruleConditions.length; index++) {
                var result = false;
                var conditionOperator = ruleConditions[index].conditionOperator;

                result = this.EvaluateRuleCondition(itemIndex, ruleConditions[index].referencedObjective, ruleConditions[index].condition, ruleConditions[index].measureThreshold);

                if (conditionOperator != null && conditionOperator == "not") {
                    result = !result;
                }

                ruleBag.push(result);
            }
        }

        // 3. if the rule condition bag is Empty 

        if (ruleBag.length == 0) {
            resultSet.value = "unknown";
            return resultSet;
        }

        // apply the Rule Combination for the Sequencing Rule to the rule condition bag to produce a single combined rule evaluation
        var trueCountInArray = 0;
        for (var index in ruleBag) {
            if (ruleBag[index] == true) {
                trueCountInArray++;
            }
        }

        if (conditionCombination == "all" && ruleBag.length == trueCountInArray) {
            resultSet.value = true;

        }
        else if (conditionCombination == "any" && ruleBag.indexOf(true) > -1) {
            resultSet.value = true;
        }

        return resultSet;
        // TO DO: Exit Sequencing Rule Check Subprocess (Result: the value of rule evaluation)
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SequencingRuleCheckSubprocess", "", 1);
        throw e;
    }
}

/**
* GetInstanceAttributeValue() 
* This Function is used to dynmaically take object and property and return object instance and value 
* @param object               instance of any object type
* @param propertyName          propertyName of specific object
* @return                     object instance or  property value of object inatance
*/
Scorm2004ADLNavChoice.prototype.GetInstanceAttributeValue = function (object, propertyName) {

    this.scormApi.DebuggerTraceCall('GetInstanceAttributeValue');
    try {
        if (propertyName == this.cEnum.SequencingConditionRules.PreConditionRule
        || propertyName == this.cEnum.SequencingConditionRules.PostConditionRule
        || propertyName == this.cEnum.SequencingConditionRules.ExitConditionRule) {
            propertyName = propertyName + "s";
        }

        return object[propertyName];
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetInstanceAttributeValue", "", 1);
        throw e;
    }
}

/**
* GetSequencingRules() 
* This Function is used to dynmaically take object and property and return object instance and value 
* @param imsssSequencing      imsssSequencing object
* @param elementName          specific attribute name
* @return                     array of sequencing object with elementname.
*/
Scorm2004ADLNavChoice.prototype.GetSequencingRules = function (imsssSequencing, elementName) {

    this.scormApi.DebuggerTraceCall('GetSequencingRules');
    try {

        var result = new Array();
        var tempResult = new Array();
        if (elementName == this.cEnum.SequencingConditionRules.PreConditionRule
        || elementName == this.cEnum.SequencingConditionRules.PostConditionRule
        || elementName == this.cEnum.SequencingConditionRules.ExitConditionRule) {
            elementName = elementName + "s";
        }

        if (imsssSequencing[elementName] != null && imsssSequencing[elementName].length > 0) {
            tempResult = imsssSequencing[elementName];

            for (var index = 0; index < tempResult.length; index++) {
                result.push(tempResult[index]);
            }
        }

        if (imsssSequencing["idRef"] != null) {
            tempResult = this.FetchSequencingAttributeFromSubnode(imsssSequencing, elementName);

            if (tempResult != null && tempResult.length > 0) {
                for (var index = 0; index < tempResult.length; index++) {
                    result.push(tempResult[index]);
                }
            }
        }

        return result;

    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetSequencingRules", "", 1);
        throw e;
    }
}


/**
* Flow Subprocess [SB.2.3]
* FlowSubprocess( traversalDirection, considerChildren) 
* @param scormApi           instance of the Scorm2004Api.js 
* @traversalDirection                traversalDirection
* @considerChildren         considerChildren
*/
Scorm2004ADLNavChoice.prototype.FlowSubprocess = function (itemIdentifier, traversalDirection, considerChildren) {
    this.scormApi.DebuggerTraceCall('FlowSubprocess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        // pseudo Code Point 1
        var candidateActivity = itemIdentifier;

        // pseudo Code Point 2
        returnedResultSet = this.FlowTreeTraversalSubprocess(itemIdentifier, traversalDirection, 'n/a', considerChildren);

        // pseudo code point 3
        if (returnedResultSet.identifiedActivity == null) {
            resultSet.identifiedActivity = itemIdentifier;
            resultSet.deliverable = false;
            resultSet.endSequencingSession = returnedResultSet.endSequencingSession;
            resultSet.exception = returnedResultSet.exception;
            resultSet.value = false;
            return resultSet;
        }
            // pseudo code point 4
        else {

            // pseudo code point 4.1
            candidateActivity = returnedResultSet.identifiedActivity;

            // pseudo code point 4.2
            returnedResultSet = this.FlowActivityTraversalSubprocess(candidateActivity, traversalDirection, 'n/a');

            // pseudo code point 4.3
            resultSet.nextActivity = returnedResultSet.nextActivity;
            resultSet.identifiedActivity = resultSet.nextActivity;
            resultSet.deliverable = returnedResultSet.deliverable;
            resultSet.endSequencingSession = returnedResultSet.endSequencingSession;
            resultSet.exception = returnedResultSet.exception;
            resultSet.value = true;
            return resultSet;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in FlowSubprocess", "", 1);
        throw e;
    }
}



/**
* Flow Tree Traversal Subprocess [SB.2.1]
* FlowTreeTraversalSubprocess( itemIdentifier, traversalDirection, previousTraversalDirection, considerChildren)
* @param scormApi                       instance of the Scorm2004Api.js 
* @param itemIdentifier                 itemIdentifier
* @param traversalDirection             traversalDirection
* @param previousTraversalDirection     previousTraversalDirection
* @param considerChildren               considerChildren
*/
Scorm2004ADLNavChoice.prototype.FlowTreeTraversalSubprocess = function (itemIdentifier, traversalDirection, previousTraversalDirection, considerChildren) {

    this.scormApi.DebuggerTraceCall('FlowTreeTraversalSubprocess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        // 1 set reversed direction to False
        var reversedDirection = false;

        // 2 if (previous traversal direction is Defined And is Backward) And the activity is the last activity in the activity’s parent’s list of Available Children Then
        var parentOfActivity = this.GetParentIdentifier(this.GetItemIndex(itemIdentifier));
        var parentIndexOfActivity = this.GetItemIndex(parentOfActivity);
        var availableChildren = this.GetItemIndex(itemIdentifier) == 0 ? this.scormApi.courseDataModel[0].cmi.availableChildren : this.scormApi.courseDataModel[parentIndexOfActivity].cmi.availableChildren;
        if ((previousTraversalDirection != null && previousTraversalDirection == "backward") && availableChildren.length > 0 && itemIdentifier == availableChildren[availableChildren.length - 1]) {
            traversalDirection = "backward";
            itemIdentifier = availableChildren[0];
            reversedDirection = true;
        }


        // pseudo code point 3
        if (traversalDirection == "forward") {

            // pseudo code point 3.1
            if (itemIdentifier == this.scormApi.imsManifest.organizations[0].items[this.scormApi.imsManifest.organizations[0].items.length - 1].identifier
            || (this.IsRootActivity(itemIdentifier) && !considerChildren)) {

                // need to apply Terminate Descendent Attempt Process to the root of the activity tree
                this.TerminateDescendentAttemptsProcess(this.GetItemIdentifier(0));
                resultSet.endSequencingSession = true;
                return resultSet;
            }

            // pseudo code point 3.2
            var parentOfActivity = this.GetParentIdentifier(this.GetItemIndex(itemIdentifier))
            var parentIndexOfActivity = this.GetItemIndex(parentOfActivity);
            var availableChildren = this.GetItemIndex(itemIdentifier) == 0 ? this.scormApi.courseDataModel[0].cmi.availableChildren : this.scormApi.courseDataModel[parentIndexOfActivity].cmi.availableChildren;
            if (this.IsLeafActivity(this.scormApi.itemIdentifier) || !considerChildren) {

                // pseudo code point 3.2.1
                if (availableChildren.length > 0 && itemIdentifier == availableChildren[availableChildren.length - 1]) {
                    returnedResultSet = this.FlowTreeTraversalSubprocess(this.scormApi.imsManifest.organizations[0].items[this.GetItemIndex(itemIdentifier)].parentIdentifier, "forward", "n/a", false);
                    return returnedResultSet;
                }
                    // pseudo code point 3.2.2
                else if (availableChildren.length > 0) {
                    resultSet.nextActivity = availableChildren[availableChildren.indexOf(itemIdentifier) + 1];
                    resultSet.identifiedActivity = resultSet.nextActivity; //extra value added
                    resultSet.traversalDirection = traversalDirection;
                    return resultSet;
                }
            }
                // pseudo code point 3.3
            else {
                var availableChildren = this.scormApi.courseDataModel[this.GetItemIndex(itemIdentifier)].cmi.availableChildren;
                // pseudo code point 3.3.1
                if (availableChildren.length > 0) {
                    resultSet.nextActivity = availableChildren[0];
                    resultSet.identifiedActivity = resultSet.nextActivity; //extra value added
                    resultSet.traversalDirection = traversalDirection;
                    return resultSet;
                }
                    // pseudo code point 3.3.2
                else {
                    this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_1_2, "", 1);
                    return;
                }
            }
        }

        // pseudo code point 4
        if (traversalDirection == "backward") {

            // pseudo code point 4.1
            if (this.IsRootActivity(itemIdentifier)) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_1_3, "", 1);
                return;
            }

            // pseudo code point 4.2
            if (this.IsLeafActivity(itemIdentifier) || !considerChildren) {
                if (!reversedDirection) {
                    if (this.GetSequencingElementsAttributeValue(this.GetItemIndex(itemIdentifier), true, "controlMode", "forwardOnly", false)) {
                        this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_1_4, "", 1);
                        return;
                    }
                }

                var parentOfActivity = this.GetParentIdentifier(this.GetItemIndex(itemIdentifier))
                var parentIndexOfActivity = this.GetItemIndex(parentOfActivity);
                var availableChildren = this.GetItemIndex(itemIdentifier) == 0 ? this.scormApi.courseDataModel[0].cmi.availableChildren : this.scormApi.courseDataModel[parentIndexOfActivity].cmi.availableChildren;

                // pseudo code point 4.2.2
                if (availableChildren.length > 0 && itemIdentifier == availableChildren[0]) {
                    returnedResultSet = this.FlowTreeTraversalSubprocess(this.scormApi.imsManifest.organizations[0].items[this.GetItemIndex(itemIdentifier)].parentIdentifier, "backward", "n/a", false);
                    return returnedResultSet;
                }
                    // pseudo code point 4.2.3
                else if (availableChildren.length > 0) {
                    resultSet.nextActivity = availableChildren[availableChildren.indexOf(itemIdentifier) - 1];
                    resultSet.identifiedActivity = resultSet.nextActivity; //extra value added
                    resultSet.traversalDirection = traversalDirection;
                    return resultSet;
                }
            }
                // pseudo code point 4.3
            else {
                var availableChildren = this.scormApi.courseDataModel[this.GetItemIndex(itemIdentifier)].cmi.availableChildren;
                // pseudo code point 4.3.1
                if (availableChildren.length > 0) {
                    if (this.GetSequencingElementsAttributeValue(this.GetItemIndex(itemIdentifier), false, "controlMode", "forwardOnly", false)) {
                        resultSet.nextActivity = availableChildren[0];
                        resultSet.identifiedActivity = resultSet.nextActivity; //extra value added
                        resultSet.traversalDirection = "forward";
                        return resultSet;
                    }
                    else {
                        resultSet.nextActivity = availableChildren[availableChildren.length - 1];
                        resultSet.identifiedActivity = resultSet.nextActivity; //extra value added
                        resultSet.traversalDirection = "backward";
                        return resultSet;
                    }
                }
                    // pseudo code point 4.3.2
                else {
                    this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_1_2, "", 1);
                    return;
                }
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in FlowTreeTraversalSubprocess", "", 1);
        throw e;
    }

}



/**
* Flow Activity Traversal Subprocess [SB.2.2]
* FlowActivityTraversalSubprocess( itemIdentifier, traversalDirection, previousTraversalDirection)
* @param scormApi                       instance of the Scorm2004Api.js 
* @param itemIdentifier                 itemIdentifier
* @param traversalDirection             traversalDirection
* @param previousTraversalDirection     previousTraversalDirection
*/
Scorm2004ADLNavChoice.prototype.FlowActivityTraversalSubprocess = function (itemIdentifier, traversalDirection, previousTraversalDirection) {

    this.scormApi.DebuggerTraceCall('FlowActivityTraversalSubprocess');
    try {
        var resultSet = new ExitConditionValues();
        var returnSequencingRulesCheckProcess = new ExitConditionValues();
        var returnFlowTreeTraversalSubprocess = new ExitConditionValues();
        var returnFlowActivityTraversalSubprocess = new ExitConditionValues();

        var itemIndex = this.GetItemIndex(itemIdentifier);
        var parentIdetifier = this.GetParentIdentifier(itemIndex);
        var parentIndex = this.GetItemIndex(parentIdetifier);

        // pseudo code point 1
        if (!this.GetSequencingElementsAttributeValue(parentIndex, true, "controlMode", "flow", false)) {
            resultSet.deliverable = false;
            resultSet.nextActivity = itemIdentifier;
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_1_1;
            return resultSet;
        }

        // pseudo code point 2
        returnSequencingRulesCheckProcess = this.SequencingRulesCheckProcess(itemIndex, [this.cEnum.SequencingConditionRules.PreConditionRuleAction.Skip], this.cEnum.SequencingConditionRules.PreConditionRule);

        // pseudo code point 3
        if (returnSequencingRulesCheckProcess.action != null) {

            // pseudo code point 3.1
            returnFlowTreeTraversalSubprocess = this.FlowTreeTraversalSubprocess(itemIdentifier, traversalDirection, previousTraversalDirection, false);

            // pseudo code point 3.2
            if (returnFlowTreeTraversalSubprocess.identifiedActivity == null) {
                resultSet.deliverable = false;
                resultSet.nextActivity = itemIdentifier;
                resultSet.endSequencingSession = returnFlowTreeTraversalSubprocess.endSequencingSession;
                resultSet.exception = returnFlowTreeTraversalSubprocess.exception;
                return resultSet;
            }
                // pseudo code point 3.3
            else {
                // pseudo code point 3.3.1
                if (previousTraversalDirection == "backward" && returnFlowTreeTraversalSubprocess.traversalDirection == "backward") {

                    returnFlowActivityTraversalSubprocess = this.FlowActivityTraversalSubprocess(returnFlowTreeTraversalSubprocess.identifiedActivity, traversalDirection, 'n/a');
                }
                    // pseudo code point 3.3.2
                else {
                    returnFlowActivityTraversalSubprocess = this.FlowActivityTraversalSubprocess(returnFlowTreeTraversalSubprocess.identifiedActivity, traversalDirection, previousTraversalDirection);
                }

                return returnFlowActivityTraversalSubprocess;
            }
        }

        // pseudo code point 4 & 5
        if (this.CheckActivityProcess(itemIdentifier)) {
            resultSet.deliverable = false;
            resultSet.nextActivity = itemIdentifier;
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_1_2;
            return resultSet;
        }

        // pseudo code point 6
        if (!this.IsLeafActivity(itemIdentifier)) {
            // pseudo code point 6.1
            returnFlowTreeTraversalSubprocess = this.FlowTreeTraversalSubprocess(itemIdentifier, traversalDirection, 'n/a', true);

            // pseudo code point 6.2
            if (returnFlowTreeTraversalSubprocess.identifiedActivity == null) {
                resultSet.deliverable = false;
                resultSet.nextActivity = itemIdentifier;
                resultSet.endSequencingSession = returnFlowTreeTraversalSubprocess.endSequencingSession;
                resultSet.exception = returnFlowTreeTraversalSubprocess.exception;
                return resultSet;
            }
                // pseudo code point 6.3
            else {
                // pseudo code point 6.3.1
                if (traversalDirection == "backward" && returnFlowTreeTraversalSubprocess.traversalDirection == "forward") {
                    returnFlowTreeTraversalSubprocess = this.FlowActivityTraversalSubprocess(returnFlowTreeTraversalSubprocess.identifiedActivity, "forward", "backward");
                }
                    // pseudo code point 6.3.2
                else {
                    returnFlowTreeTraversalSubprocess = this.FlowActivityTraversalSubprocess(returnFlowTreeTraversalSubprocess.identifiedActivity, traversalDirection, 'n/a');
                }

                return returnFlowTreeTraversalSubprocess;
            }
        }

        resultSet.deliverable = true;
        resultSet.nextActivity = itemIdentifier;
        return resultSet;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in FlowActivityTraversalSubprocess", "", 1);
        throw e;
    }
}

/**
* Delivery Request Process [DB.1.1]
* DeliveryRequestProcess(deliverableActivity) 
* This Function is used to  if the activity identified by a delivery request can be delivered.
* @param deliverableActivity               activity specified by the delivery request
*/
Scorm2004ADLNavChoice.prototype.DeliveryRequestProcess = function (deliverableActivity) {
    this.scormApi.DebuggerTraceCall('DeliveryRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        // if the activity specified by the delivery request is not a leaf (Can only deliver leaf activities.)    
        if (!this.IsLeafActivity(deliverableActivity)) {
            resultSet.deliveryRequest = this.cEnum.DeliveryRequest.NotValid;
            resultSet.exception = Scorm2004Api.ERRORS.DB_1_1_1;
            return resultSet;
        }
        // form the activity path as the ordered series of activities from the root of the activity tree to the activity specified in the delivery request, inclusive.
        var sortedResult = new Array();
        sortedResult = this.GetItemNamesHierarchyFromRoot(this.GetItemIndex(deliverableActivity), 0, true, true, false);

        // if the activity path is Empty 
        if (sortedResult.length == 0) {
            resultSet.deliveryRequest = this.cEnum.DeliveryRequest.NotValid;
            resultSet.exception = Scorm2004Api.ERRORS.DB_1_1_2;
            return resultSet;
        }
        // for each activity in the activity path
        for (index = 0; index < sortedResult.length; index++) {
            // apply Check Check Activity Process ,,If the Check Activity Process returned True
            if (this.CheckActivityProcess(sortedResult[index])) {
                resultSet.deliveryRequest = this.cEnum.DeliveryRequest.NotValid;
                resultSet.exception = Scorm2004Api.ERRORS.DB_1_1_3;

                // show the SCO unavailable instruction to the user
                ShowUserInstruction();
                return resultSet;
            }
        }
        // exit Delivery Request Process (Delivery Request: Valid; Exception: n/a)
        resultSet.deliveryRequest = this.cEnum.DeliveryRequest.Valid;
        return resultSet;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in DeliveryRequestProcess", "", 1);
        throw e;
    }
}


/**
* Check Activity Process [UP.5]
* CheckActivityProcess(deliverableActivity) 
* This Function is used to For a delivery request; returns the validity of the delivery request; may return an exception code.
* @param scormApi                  instance of the Scorm2004Api.js 
* @param deliverableActivity        activity specified by the delivery request
*/
Scorm2004ADLNavChoice.prototype.CheckActivityProcess = function (deliverableActivity) {
    this.scormApi.DebuggerTraceCall('CheckActivityProcess');

    // apply the Sequencing Rules Check Process to the activity and the Disabled sequencing rules   
    var itemIndex = this.GetItemIndex(deliverableActivity);

    var returnedResultSet = new ExitConditionValues();
    try {
        returnedResultSet = this.SequencingRulesCheckProcess(itemIndex, [this.cEnum.SequencingConditionRules.PreConditionRuleAction.Disabled], this.cEnum.SequencingConditionRules.PreConditionRule);

        // if the Sequencing Rules Check Process does not return Nil 
        if (returnedResultSet.action != null) {
            return true;
        }

        // limit Conditions Check Process
        // if the Limit Conditions Check Process returns True 
        if (this.LimitConditionsCheckProcess(deliverableActivity)) {
            return true;
        }
        return false;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in CheckActivityProcess", "", 1);
        throw e;
    }
}

/**
* Limit Conditions Check Process [UP.1]
* LimitConditionsCheckProcess(deliverableActivity) 
* This Function is used to For a delivery request; returns the validity of the delivery request; may return an exception code.
* @param scormApi                  instance of the Scorm2004Api.js 
* @param deliverableActivity        activity specified by the delivery request
* Please Note : All LimitConditionsValues are not getting declared and parsed into existing Manifestdatamodel ?????
* To get Limit Conditions other values we need to add extra property for limit conditions in manifestdatamodel for limitconditions and parsed accordingly as it is being used for pseudo code
*/
Scorm2004ADLNavChoice.prototype.LimitConditionsCheckProcess = function (deliverableActivity) {
    this.scormApi.DebuggerTraceCall('LimitConditionsCheckProcess');
    try {

        var itemIndex = this.GetItemIndex(deliverableActivity);
        var data = this.scormApi.courseDataModel;
        var IsAcivityIsActive = data[itemIndex].cmi.isActive.value;
        var IsSuspended = data[itemIndex].cmi.isSuspended.value;


        // if Tracked for the activity is False Then
        if (this.GetSequencingElementsAttributeValue(itemIndex, false, "deliveryControls", "tracked", true)) {
            // exit Limit Conditions Check Process (Limit Condition Violated:False)
            return false;
        }
        // if the Activity is Active for the activity is True Or the Activity is Suspended for the activity is True Then
        if (IsAcivityIsActive == true || IsSuspended == true) {
            return false;
        }
        // if the Limit Condition Attempt Control for the activity is True 
        var attemptLimit = this.GetSequencingElementsAttributeValue(itemIndex, false, "limitConditions", "attemptLimit", 0);
        var attemptCount = attemptLimit != null && attemptLimit > 0 ? true : false; // Reference from S_Pool
        if (attemptCount) {

            // this.SetRTEDataModelElement(itemIndex, null);

            // if the "Activity Progress Status" for the activity is True And the "Activity Attempt Count" for the activity is greater than or equal (>=)
            // to the Limit Condition Attempt Limit for the activity
            var activityProgressStatus = this.scormApi.courseDataModel[itemIndex].cmi.activityProgressStatus.value;
            var activityAttemptCount = this.scormApi.courseDataModel[itemIndex].cmi.activityAttemptCount.value;
            if (activityProgressStatus && (activityAttemptCount >= attemptLimit)) {
                return true;
            }
        }

        // if the Limit Condition Activity Absolute Duration Control for the activity is True
        // if the Activity Progress Status for the activity is True And 
        // the Activity Absolute Duration for the activity is greater than or equal (>=) to 
        // limit Condition Activity Absolute Duration Limit for the activity
        var activityAbsoluteDuration = this.scormApi.timeIntervalToMs(this.scormApi.courseDataModel[itemIndex].cmi.total_time.value) / 1000;

        if ((this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus)) && (activityAbsoluteDuration >= this.GetSequencingElementsAttributeValue(itemIndex, false, "limitConditions", "attemptAbsoluteDurationLimit", 0))) {
            return true;
        }
        // if the Limit Condition Activity Experienced Duration Control for the activity is True 

        var ExperiencedDurationfortheactivity; // TODO: need to get this value

        // experienced Duration for the activity is greater than or equal (>=) to the Limit Condition Activity 
        // experienced Duration Limit for the activity Then*

        if (ExperiencedDurationfortheactivity >= this.GetSequencingElementsAttributeValue(itemIndex, false, "limitConditions", "activityExperiencedDurationLimit", 0)) {
            return true;
        }

        // if the Limit Condition Begin Time Limit Control for the activity is True 
        // this.scormApi.imsManifest.organizations[0].items[itemIndex].imsssSequencing.limitConditions.beginTimeLimit


        // if the current time point is before the Limit Condition Begin Time Limit for the activity Then

        var currenttimepoint; // TODO: need to get this value
        if (currenttimepoint < this.GetSequencingElementsAttributeValue(itemIndex, false, "limitConditions", "beginTimeLimit", 0)) {
            return true;
        }

        // the Limit Condition End Time Limit Control for the activity is True  
        // if the current time point is after the Limit Condition End Time Limit for the activity Then
        var currenttimepoint; // TODO: need to get this value
        if (currenttimepoint > this.GetSequencingElementsAttributeValue(itemIndex, false, "limitConditions", "endTimeLimit", 0)) {
            return true;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in LimitConditionsCheckProcess", "", 1);
        throw e;
    }
}

/**
* Overall Sequencing Process [OP.1]
* OverallSequencingProcess(navigationRequest) 
* This Function is details with Content Delivery Environment Process DB.2; Delivery Request Process DB.1.3; Navigation
* Request Process NB.2.1; Sequencing Request Process SB.2.12; Termination Request Process TB.2.3
* @param navigationRequest        string represents navigation request process.
*/
Scorm2004ADLNavChoice.prototype.OverallSequencingProcess = function (navigationRequest, currentActivity) {
    this.scormApi.DebuggerTraceCall('OverallSequencingProcess');
    try {

        // loop - Wait for a navigation request   
        //var sequencingRequest = null;
        var navResultSet = new ExitConditionValues();

        // 1.1 apply the Navigation Request Process to the navigation request
        navResultSet = this.NavigationRequestProcess(navigationRequest, currentActivity);

        // 1.1. apply the Navigation Request Process to the navigation request
        if (navResultSet.navigationRequest == this.cEnum.NavigationRequest.NotValid) {

            // handle the navigation request exception 
            if (navResultSet.exception != null) {
                this.scormApi.setActiveError(navResultSet.exception, "", 1);
            }
            // continue Loop - wait for the next navigation request
            return;
        }
        var terminationResultSet = new ExitConditionValues();
        terminationResultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
        terminationResultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;

        var sequencingResultSet = new ExitConditionValues();
        sequencingResultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
        sequencingResultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;

        var deliveryResultSet = new ExitConditionValues();
        // 1.3. if there is a termination request Then
        if (navResultSet.terminationRequest != this.cEnum.TerminationRequest.NotAvailable) {
            terminationResultSet = this.TerminationRequestProcess(navResultSet.terminationRequest);

            // 1.3.2. if the Termination Request Process returned termination request Not Valid Then
            if (terminationResultSet.terminationRequest == this.cEnum.TerminationRequest.NotValid) {

                // handle the termination request exception 
                if (terminationResultSet.exception != null) {
                    this.scormApi.setActiveError(terminationResultSet.exception, "", 1);
                }
                // continue Loop - wait for the next navigation request
                return;
            }

            // 1.3.3. if Termination Request Process returned a sequencing request
            if (terminationResultSet.sequencingRequest != this.cEnum.SequencingRequest.NotAvailable
                && terminationResultSet.sequencingRequest != null) {
                // replace any pending sequencing request by the sequencing request returned by the Termination Request Process       
                //this.TerminationRequestProcess(terminationResultSet.terminationRequest);
                navResultSet.sequencingRequest = terminationResultSet.sequencingRequest;
                //this.SequencingRequestProcess(terminationResultSet.sequencingRequest, null);
            }
        }

        // 1.4. if there is a sequencing request        
        if (navResultSet.sequencingRequest != this.cEnum.SequencingRequest.NotAvailable
            && navResultSet.sequencingRequest != null) {

            // then 1.4.1. Apply the Sequencing Request Process
            if (navResultSet.targetActivity != null) {
                sequencingResultSet = this.SequencingRequestProcess(navResultSet.sequencingRequest, navResultSet.targetActivity);
            }
            else {
                sequencingResultSet = this.SequencingRequestProcess(navResultSet.sequencingRequest, currentActivity);
            }
            // 1.4.2. if the Sequencing Request Process returned sequencing request Not Valid
            if (sequencingResultSet.sequencingRequest == this.cEnum.SequencingRequest.NotValid) {

                // handle the sequencing request exception
                this.scormApi.setActiveError(sequencingResultSet.exception, "", 1);
                // continue Loop - wait for the next navigation request
                return;
            }

            // 1.4.3. if the Sequencing Request Process returned a request to end the sequencing session Then
            // if the Sequencing Request Process returned a request to end the sequencing session ==true expecting True|false as retun for  endSequencingSession
            if (sequencingResultSet.endSequencingSession != null && sequencingResultSet.endSequencingSession == true) {
                // exit: Overall Sequencing Process - the sequencing session has terminated; return control to LTS 
                this.scormApi.globalSaveCompleted = false;

                if (this.scormApi.activeState == Scorm2004Api.STATE.NotInitialized) {
                    this.scormApi.saveGlobal();
                }

                if (navigationRequest == 'exit') {
                    //fire an exitAll call if exit is called on a single-SCO package
                    if (this.scormApi.imsManifest.organizations[0].items.length <= 2) {
                        ReturnWhenGlobalSaveCompleted(true);
                    }
                    else
                    {
                        ReturnWhenGlobalSaveCompleted(false);
                    }

                }
                else {
                    ReturnWhenGlobalSaveCompleted(true);
                }


                // display the loader modal popup
                ShowLoaderModalPopup(SavingContentStr);

                var href = "../../../../launch/ContentPlaceHolder.html";
                SetContentFrameUrl(href);

                return;
            }

            // 1.4.4. if the Sequencing Request Process did not identify an activity for delivery Then
            if (sequencingResultSet.deliveryRequest == null) {
                // continue Loop - wait for the next navigation request
                return;
            }

            // delivery request is for the activity identified by the Sequencing Request Process
            // 1.5. if there is a delivery request Then

            deliveryResultSet = this.DeliveryRequestProcess(sequencingResultSet.deliveryRequest);
            if (deliveryResultSet.deliveryRequest != null) {

                // 1.5.2. if the Delivery Request Process returned delivery request Not Valid Then
                if (deliveryResultSet.deliveryRequest == this.cEnum.DeliveryRequest.NotValid) {
                    // handle the delivery request exception
                    // continue Loop - wait for the next navigation request
                    return;
                }

                // 1.5.3. apply the Content Delivery Environment Process to the delivery request 
                this.ContentDeliveryEnvironmentProcess(sequencingResultSet.deliveryRequest);
            }
        }

        // extra condition added  to set current activity's isActive status back to true if no activity is delivered coz Exit navigation request sets isActive status as false when any navigation request is processed
        if (this.currentActivityIndex > -1) {
            this.scormApi.courseDataModel[this.currentActivityIndex].cmi.isActive.value = true;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in OverallSequencingProcess", "", 1);
        throw e;
    }
}

/*
This method is used to return the page to courselist page when global data save is completed
*/
function ReturnWhenGlobalSaveCompleted(returnToLMS) {
    scormApiInstance.DebuggerTraceCall('ReturnWhenGlobalSaveCompleted');
    try {
        var timerCount = 0;
        var interval =
            setInterval(function () {
                timerCount++;

                // either global data save call finished or time exceeds 5 seconds and global data is not saved, then saving it from our end.
                if (scormApiInstance.saveRTECallFinished || timerCount >= 10) {

                    // check if the redirecting to url without termination, then terminate first and then redirect 
                    if ((!scormApiInstance.globalSaved || scormApiInstance.globalSaved == undefined) && scormApiInstance.activeState == 1) {

                        // after waiting 5 second save the data.
                        if (timerCount >= 10) {
                            timerCount = 0;
                            clearInterval(interval);
                            scormApiInstance.Terminate("");
                        }
                    }
                        // check if global data is saved and activity is not in active state 
                    else if ((scormApiInstance.globalSaved || scormApiInstance.globalSaved == undefined) && scormApiInstance.activeState != 1) {
                        clearInterval(interval);
                        scormApiInstance.SaveNonLeafItemsJson(returnToLMS);
                    }
                }
            }, 500);
    }
    catch (e) {
        scormApiInstance.traceError("Error caught in ReturnWhenGlobalSaveCompleted", "", 1);
        throw e;
    }
}

/**
* Navigation Request Process [NB.2.1]
* LimitConditionsCheckProcess(deliverableActivity) 
* This Function is used to For a navigation request and possibly a specified activity, returns the validity of the navigation request;
* @param scormApi                  instance of the Scorm2004Api.js 
* @param navigationRequest       string navigation request
*/
Scorm2004ADLNavChoice.prototype.NavigationRequestProcess = function (navigationRequest, specifiedActivity) {
    this.scormApi.DebuggerTraceCall('NavigationRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        switch (navigationRequest) {

            case this.cEnum.NavigationRequest.Start:
                if (this.currentActivity == null) {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Start;
                    return resultSet;
                }
                else {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_1;
                }
                break;

            case this.cEnum.NavigationRequest.Previous:
                if (this.currentActivity == null) {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_2;
                    return resultSet;
                }
                if (!this.IsRootActivity(this.currentActivity)) {
                    if (this.GetSequencingElementsAttributeValue(this.GetItemIndex(this.currentActivity), true, "controlMode", "flow", false) && !this.GetSequencingElementsAttributeValue(this.GetItemIndex(this.currentActivity), true, "controlMode", "forwardOnly", false)) {
                        if (this.scormApi.courseDataModel[this.GetItemIndex(this.currentActivity)].cmi.isActive.value) {
                            resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                            resultSet.terminationRequest = this.cEnum.TerminationRequest.Exit;
                            resultSet.sequencingRequest = this.cEnum.SequencingRequest.Previous;
                            return resultSet;
                        }
                        else {
                            // exit Navigation Request Process                         
                            resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                            resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                            resultSet.sequencingRequest = this.cEnum.SequencingRequest.Previous;
                            return resultSet;
                        }
                    }
                    else {
                        resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                        resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_5;
                        return resultSet;
                    }
                }
                else {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_6;
                    return resultSet;
                }
                break;
            case this.cEnum.NavigationRequest.Continue:
                // 3.1. make sure the sequencing session has already begun 
                if (this.currentActivity == null) {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_2;
                    return resultSet;
                }
                if (!this.IsRootActivity(this.currentActivity) && this.GetSequencingElementsAttributeValue(this.GetItemIndex(this.currentActivity), true, "controlMode", "flow", false)) {
                    if (this.scormApi.courseDataModel[this.currentActivityIndex].cmi.isActive.value) {
                        resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.Exit;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.Continue;
                        return resultSet;
                    }
                    else {
                        resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.Continue;
                        return resultSet;
                    }
                }
                else {
                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_4;
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    return resultSet;
                }
                break;

            case this.cEnum.NavigationRequest.Exit:
                if (this.currentActivity != null) {
                    if (this.scormApi.courseDataModel[this.currentActivityIndex].cmi.isActive.value) {
                        resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.Exit;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.Exit;
                        return resultSet;
                    }
                    else {
                        resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                        resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_12;
                        return resultSet;
                    }
                }
                else {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_2;
                    return resultSet;
                }
                break;

            case this.cEnum.NavigationRequest.ExitAll:
                if (this.currentActivity != null) {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.ExitAll;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Exit;
                    return resultSet;
                }
                else {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_2;
                    return resultSet;
                }
                break;

            case this.cEnum.NavigationRequest.Abandon:
                if (this.currentActivity != null) {
                    if (this.scormApi.courseDataModel[this.currentActivityIndex].cmi.isActive.value) {
                        resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.Abandon;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.Exit;
                        return resultSet;
                    }
                    else {
                        resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                        resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_12;
                        this.scormApi.setActiveError(Scorm2004Api.ERRORS.NB_2_1_12, "", 1);
                        return resultSet;
                    }
                }
                else {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_2;
                    this.scormApi.setActiveError(Scorm2004Api.ERRORS.NB_2_1_2, "", 1);
                    return resultSet;
                }
                break;

            case this.cEnum.NavigationRequest.AbandonAll:
                if (this.currentActivity != null) {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.AbandonAll;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Exit;
                    return resultSet;
                }
                else {
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_2;
                    return resultSet;
                }
                break;

            case this.cEnum.NavigationRequest.SuspendAll:
                // make sure the sequencing session has already begun. -- 12.1 -page 179
                if (this.currentActivity != null) {
                    // exit Navigation Request Process 
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.SuspendAll;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.Exit;
                    return resultSet;
                }
                else {
                    // exit Navigation Request Process           
                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                    this.scormApi.setActiveError(Scorm2004Api.ERRORS.NB_2_1_2, "", 1);
                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_2;
                    return resultSet;
                }
                break;

            default:

                if (navigationRequest.indexOf("jump") > 8) {
                    var specifiedActivity = navigationRequest.replace("}jump", "");
                    specifiedActivity = specifiedActivity.replace("{target=", "");
                    var specifiedActivityIndex = this.GetItemIndex(specifiedActivity);

                    // pseudo code 13.1
                    // if the activity specified by the Jump navigation request exists within the activity tree 
                    // and Available Children for the parent of the activity contains the activity

                    // note : We dint added second condition because it is always true.
                    if (specifiedActivityIndex != undefined && true) {
                        resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.Exit;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.Jump;
                        resultSet.targetActivity = specifiedActivity;
                        return resultSet;
                    }
                    else {
                        resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                        resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_11;
                        return resultSet;
                    }

                }
                else if (navigationRequest.indexOf("choice") > 8) {
                    specifiedActivity = navigationRequest.replace("}choice", "");
                    specifiedActivity = specifiedActivity.replace("{target=", "");
                    var specifiedActivityIndex = this.GetItemIndex(specifiedActivity);

                    // pseudo code point 7.1
                    // specified activity will always be in the activity tree
                    if (specifiedActivityIndex != undefined) {

                        // pseudo code point 7.1.1
                        if (this.IsRootActivity(specifiedActivity) || this.GetSequencingElementsAttributeValue(specifiedActivityIndex, true, "controlMode", "choice", true)) {

                            // pseudo code point 7.1.1.1
                            if (this.currentActivity == null) {
                                resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                                resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                                resultSet.sequencingRequest = this.cEnum.SequencingRequest.Choice;
                                resultSet.targetActivity = specifiedActivity;
                                return resultSet;
                            }

                            // pseudo code point 7.1.1.2
                            if (this.GetItemLevel(specifiedActivityIndex) != this.GetItemLevel(this.currentActivityIndex)) {

                                // pseudo code point 7.1.1.2.1
                                var commonAncestor = this.GetCommonAncestor(specifiedActivityIndex, this.currentActivityIndex);
                                var commonAncestorIndex = this.GetItemIndex(commonAncestor);

                                var activityPath;
                                // pseudo code point 7.1.1.2.2
                                if (commonAncestorIndex == this.currentActivityIndex) {
                                    activityPath = this.GetItemNamesHierarchyFromRoot(this.currentActivityIndex, commonAncestorIndex, true, true, false);
                                }
                                else {
                                    activityPath = this.GetItemNamesHierarchyFromRoot(this.currentActivityIndex, commonAncestorIndex, true, false, false);
                                }
                                // pseudo code point 7.1.1.2.3
                                if (activityPath.length > 0) {
                                    for (var index = 0; index < activityPath.length; index++) {
                                        var loopActivityIndex = this.GetItemIndex(activityPath[index]);

                                        if (this.scormApi.courseDataModel[loopActivityIndex].cmi.isActive.value && !this.GetSequencingElementsAttributeValue(loopActivityIndex, false, "controlMode", this.cEnum.ControlModeAttributeValue.ChoiceExit, true)) {
                                            resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                                            resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                                            resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                                            resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_8;
                                            return resultSet;
                                        }
                                    }
                                }
                                    // pseudo code point 7.1.1.2.4
                                else {
                                    resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                                    resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                                    resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                                    resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_9;
                                    return resultSet;
                                }
                            }

                            // pseudo code point 7.1.1.3
                            if (this.scormApi.courseDataModel[this.currentActivityIndex].cmi.isActive.value && !this.GetSequencingElementsAttributeValue(this.currentActivityIndex, false, "controlMode", this.cEnum.ControlModeAttributeValue.ChoiceExit, true)) {
                                resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                                resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                                resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                                resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_8;
                                return resultSet;
                            }

                            // pseudo code point 7.1.1.4
                            if (this.scormApi.courseDataModel[this.currentActivityIndex].cmi.isActive.value) {
                                resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                                resultSet.terminationRequest = this.cEnum.TerminationRequest.Exit
                                resultSet.sequencingRequest = this.cEnum.SequencingRequest.Choice;
                                resultSet.targetActivity = specifiedActivity;
                                return resultSet;
                            }
                                // pseudo code point 7.1.1.5
                            else {
                                resultSet.navigationRequest = this.cEnum.NavigationRequest.Valid;
                                resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                                resultSet.sequencingRequest = this.cEnum.SequencingRequest.Choice;
                                resultSet.targetActivity = specifiedActivity;
                                return resultSet;
                            }
                        }
                            // pseudo code point 7.1.2
                        else {
                            resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                            resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                            resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                            resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_10;
                            return resultSet;
                        }
                    }
                        // pseudo code point 7.2
                    else {
                        resultSet.navigationRequest = this.cEnum.NavigationRequest.NotValid;
                        resultSet.terminationRequest = this.cEnum.TerminationRequest.NotAvailable;
                        resultSet.sequencingRequest = this.cEnum.SequencingRequest.NotAvailable;
                        resultSet.exception = Scorm2004Api.ERRORS.NB_2_1_11;
                        return resultSet;
                    }
                }
                break;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in NavigationRequestProcess", "", 1);
        throw e;
    }
}


/*
* Content Delivery Environment Process [DB.2]
* ContentDeliveryEnvironmentProcess(targetActivity) 
* This Function is used to For a navigation request and possibly a specified activity, returns the validity of the navigation request;
* @param scormApi                  instance of the Scorm2004Api.js 
* @param targetActivity       string deliverableActivity 
*/
Scorm2004ADLNavChoice.prototype.ContentDeliveryEnvironmentProcess = function (deliverableActivity) {   
    this.scormApi.DebuggerTraceCall('ContentDeliveryEnvironmentProcess');

    try {
        this.currentActivityIndex = this.currentActivityIndex < 0 ? 0 : this.currentActivityIndex;

        var itemIndex = this.GetItemIndex(deliverableActivity);
        var data = this.scormApi.courseDataModel;
        var IsAcivityIsActive = data[this.currentActivityIndex].cmi.isActive.value;

        // if the Activity is Active for the Current Activity is True Then
        if (IsAcivityIsActive) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.DB_2_1, "", 1);
            return;
        }
        // if the activity identified for delivery is not equal to the Suspended Activity Then
        if (deliverableActivity != this.suspendedActivity) {
            this.ClearSuspendedActivitySubprocess(deliverableActivity);
        }

        this.TerminateDescendentAttemptsProcess(deliverableActivity);
        // form the activity path as the ordered series of activities from the root of the activity tree to the activity identified for delivery, inclusive
        var sortedResult = new Array();
        sortedResult = this.GetItemNamesHierarchyFromRoot(this.GetItemIndex(deliverableActivity), 0, true, true, false);
        // for each activity in the activity path
        for (index = 0; index < sortedResult.length; index++) {
            itemIndex = this.GetItemIndex(sortedResult[index]);
            // if Activity is Active for the activity is False Then
            if (!this.scormApi.courseDataModel[itemIndex].cmi.isActive.value) {
                // if Tracked for the activity is True Then
                if (this.GetSequencingElementsAttributeValue(itemIndex, false, "deliveryControls", "tracked", true)) {
                    // if Activity is Suspended for the activity is True
                    if (this.scormApi.courseDataModel[itemIndex].cmi.isSuspended.value) {
                        // set Activity is Suspended for the activity to False 
                        this.scormApi.courseDataModel[itemIndex].cmi.isSuspended.value = false;
                    }
                    else {
                        // JC EDIT: 7-11-2018, DO NOT increment the attempt here, it is already done when the data is initialized/loaded
                        // increment the Activity Attempt Count for the activity
                        //this.scormApi.courseDataModel[itemIndex].cmi.activityAttemptCount.value += 1;                        

                        // if Activity Attempt Count for the activity is equal to One (1) Then //TODO: pending checking
                        /// TODO: Handling completed by taking reference from S_Pool
                        if (this.scormApi.courseDataModel[itemIndex].cmi.activityAttemptCount.value == 1) {
                            // set Activity Progress Status for the activity to True ////TODO: pending 
                            this.scormApi.courseDataModel[itemIndex].cmi.activityProgressStatus.value = true;
                        }
                        // initialize Objective Progress Information and Attempt Progress Information required for the new attempt // implemented in api initialize with default values
                        // this.InitializeObjectiveProgressInformationAndAttemptProgInformation(itemIndex);
                    }
                }
                this.scormApi.courseDataModel[itemIndex].cmi.isActive.value = true;
            }
        }

        // set Activity is Active for the activity to True 
        this.SetIsActiveToTrueForActivityProvidedAndFalseForOthers(this.GetItemIndex(deliverableActivity));

        // 6. set Current Activity to the activity identified for delivery      
        this.SetCurrentActivity(deliverableActivity);
        this.scormApi.activeSCOIdentifier = this.currentActivity;
        this.scormApi.activeSCOIndex = this.currentActivityIndex;
        this.scormApi.courseLaunchController.findActiveScoIndex();

        // 7. set Suspended Activity to undefined
        this.SetSuspendedActivity(null);
        this.scormApi.courseDataModel[this.currentActivityIndex].cmi.isSuspended.value = false;

        // evaluate the LMS UI button states
        this.scormApi.courseLaunchController.evaluateLMSUIButtonStates();

        this.UpdateObjectiveStatus(this.currentActivityIndex);

        // 8. once the delivery of the activity’s content resources and auxiliary resources begins
        this.scormApi.courseLaunchController.deliverSCO();

        // extra code added method added to enable/disable the activity tree items based on the organizations control mode values
        this.CheckMenu();

        // 8.1
        // if Tracked for the activity identified for delivery is False Then
        if (!this.GetSequencingElementsAttributeValue(itemIndex, false, "deliveryControls", "tracked", true)) {
            // 8.1.1 the Objective and Attempt Progress information for the activity should not be recorded during delivery 
            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, false);
            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, false);
            // 8.1.2 the delivery environment begins tracking the Attempt Absolute Duration and the Attempt Experienced Duration
        }

        // 9. exit Content Delivery Environment Process (Exception: n/a) 
        return;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ContentDeliveryEnvironmentProcess", "", 1);
        throw e;
    }
}

/*
* Clear Suspended Activity Subprocess [DB.2.1]
* ClearSuspendedActivitySubprocess(targetActivity) 
* This Function is used to For a navigation request and possibly a specified activity, returns the validity of the navigation request;
* @param scormApi           instance of the Scorm2004Api.js 
* @param targetActivity     string deliverableActivity 
*/
Scorm2004ADLNavChoice.prototype.UpdateObjectiveStatus = function (itemIndex) {

    try {
        var itemCmi = this.GetItemCmi(itemIndex);
        for (var count = 0; count < itemCmi.objectives.__array.length ; count++) {

            var objective = itemCmi.objectives.__array[count];

            if (this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, null, objective)) {
                var objectiveSatisfiedStatus = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, null, objective);
                this.SetPrimaryObjectiveSatusValues(itemIndex, 'success_status', objectiveSatisfiedStatus ? 'passed' : 'failed', objective);
            }

            if (this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveMeasureStatus, null, objective)) {
                var objectiveNormalizedMeasure = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveNormalizedMeasure, null, objective);
                this.SetPrimaryObjectiveSatusValues(itemIndex, 'score.scaled', objectiveNormalizedMeasure, objective);
            }

            if (this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, null, objective)) {
                var attemptCompletionStatus = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, null, objective);
                this.SetPrimaryObjectiveSatusValues(itemIndex, 'completion_status', attemptCompletionStatus ? 'completed' : 'incomplete', objective);
            }

            if (this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmountStatus, null, objective)) {
                var attemptCompletionAmount = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmount, null, objective);
                this.SetPrimaryObjectiveSatusValues(itemIndex, 'progress_measure', attemptCompletionAmount, objective);
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in UpdateObjectiveStatus", "", 1);
        throw e;
    }
}
/*
* Clear Suspended Activity Subprocess [DB.2.1]
* ClearSuspendedActivitySubprocess(targetActivity) 
* This Function is used to For a navigation request and possibly a specified activity, returns the validity of the navigation request;
* @param scormApi           instance of the Scorm2004Api.js 
* @param targetActivity     string deliverableActivity 
*/
Scorm2004ADLNavChoice.prototype.ClearSuspendedActivitySubprocess = function (deliverableActivity) {
    this.scormApi.DebuggerTraceCall('ClearSuspendedActivitySubprocess');
    try {
        // if the Suspended Activity is Defined Then
        // 1. if the Suspended Activity is Defined Then
        if (this.suspendedActivity != null) {

            // find the common ancestor of the identified activity and the Suspended Activity
            var itemIndex = this.GetItemIndex(deliverableActivity);

            // 1.1. find the common ancestor of the identified activity and the Suspended Activity
            var commonAncestor = this.GetCommonAncestor(this.suspendedActivityIndex, itemIndex);

            // 1.2. form an activity path as the ordered series of activities from the Suspended Activity to the common ancestor, inclusive
            var itemHierarchy = this.GetItemNamesHierarchyFromRoot(commonAncestor, this.suspendedActivityIndex, true, true, false);

            // 1.3 if the activity path is Not Empty Then
            if (itemHierarchy.length > 0) {
                // 1.3.1.1. if the activity is a leaf Then
                for (var index = 0; index < itemHierarchy.length; index++) {
                    itemIndex = this.GetItemIndex(itemHierarchy[index]);

                    if (this.IsLeafActivity(itemHierarchy[index])) {
                        // 1.3.1.1.1. set Activity is Suspended for the activity to False
                        this.scormApi.courseDataModel[itemIndex].cmi.isSuspended.value = false;
                    }
                    else {
                        var availableChildren = this.scormApi.courseDataModel[itemIndex].cmi.availableChildren;
                        var suspendedActivityExistsInChildren = false;
                        for (var counter = 0; counter < availableChildren.length; counter++) {
                            if (this.scormApi.courseDataModel[this.GetItemIndex(availableChildren[counter])].cmi.isSuspended.value) {
                                suspendedActivityExistsInChildren = true;
                                break;
                            }
                        }
                        // 1.3.1.2.1. if the activity does not include any child activity whose Activity is Suspended attribute is True Then
                        if (!suspendedActivityExistsInChildren) {
                            // 1.3.1.2.1.1. set Activity is Suspended for the activity to False  
                            this.scormApi.courseDataModel[itemIndex].cmi.isSuspended.value = false;
                        }
                    }
                }
                // 1.4. set Suspended Activity to Undefined
                this.SetSuspendedActivity(null);
            }
            // 2. exit Clear Suspended Activity Subprocess
            return;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ClearSuspendedActivitySubprocess", "", 1);
        throw e;
    }
}

/**
* Start Sequencing Request Process [SB.2.5]
* StartSequencingRequestProcess()
* This Function is used to process Start sequencing request.
* @param scormApi instance of the Scorm2004Api.js 
*/
Scorm2004ADLNavChoice.prototype.StartSequencingRequestProcess = function () {
    this.scormApi.DebuggerTraceCall('StartSequencingRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        // pseudo code point 1
        if (this.currentActivity != null) {
            resultSet.deliveryRequest = 'n/a';
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_5_1;
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_5_1, "", 1);
            return resultSet;
        }

        // pseudo code point 2
        if (this.scormApi.imsManifest.organizations[0].items == null || this.scormApi.imsManifest.organizations[0].items.length == 0) {
            resultSet.deliveryRequest = this.scormApi.imsManifest.organizations[0].identifier;
            resultSet.deliverable = true; // extra code added 
            return resultSet;
        }
            // pseudo code point 3
        else {
            // pseudo code point 3.1
            returnedResultSet = this.FlowSubprocess(this.scormApi.imsManifest.organizations[0].identifier, "forward", true);

            // pseudo code point 3.2
            if (!returnedResultSet.value) {
                resultSet.endSequencingSession = returnedResultSet.endSequencingSession;
                resultSet.deliverable = returnedResultSet.deliverable; // extra code added 
                resultSet.exception = returnedResultSet.exception;
                return resultSet;
            }
                // pseudo code point 3.3
            else {
                resultSet.deliveryRequest = returnedResultSet.identifiedActivity;
                resultSet.deliverable = returnedResultSet.deliverable; // extra code added 
                return resultSet;
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in StartSequencingRequestProcess", "", 1);
        throw e;
    }
}

/**
* Resume All Sequencing Request Process [SB.2.6]
* ResumeAllSequencingRequestProcess()
* This Function is used to process Resume All sequencing request.
* @param scormApi instance of the Scorm2004Api.js 
*/
Scorm2004ADLNavChoice.prototype.ResumeAllSequencingRequestProcess = function () {
    this.scormApi.DebuggerTraceCall('ResumeAllSequencingRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        // pseudo code point 1
        if (this.currentActivity != null) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_6_1, "", 1);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_6_1;
            return resultSet;
        }

        // pseudo code point 2
        if (this.suspendedActivity == null) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_6_2, "", 1);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_6_2;
            return resultSet;
        }

        // pseudo code point 3
        resultSet.deliveryRequest = this.suspendedActivity;
        return resultSet;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ResumeAllSequencingRequestProcess", "", 1);
        throw e;
    }
}


/**
* Exit Sequencing Request Process [SB.2.11]
* ExitSequencingRequestProcess()
* This Function is used to process Exit Sequencing Request.
* @param scormApi instance of the Scorm2004Api.js 
*/
Scorm2004ADLNavChoice.prototype.ExitSequencingRequestProcess = function () {
    this.scormApi.DebuggerTraceCall('ExitSequencingRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();
        var endSequencingSession = false;

        resultSet.endSequencingSession = endSequencingSession;
        // exit Sequencing Request Process

        // pseudo code point 1
        if (this.currentActivity == null) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_11_1, "", 1);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_11_1;
            return resultSet;
        }

        // pseudo code point 2
        if (this.scormApi.courseDataModel[this.currentActivityIndex].cmi.isActive.value) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_11_2, "", 1);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_11_2;
            return resultSet;
        }

        // pseudo code point 3
        if (this.IsRootActivity(this.currentActivity) || this.scormApi.courseDataModel.length == 2) {
            resultSet.endSequencingSession = true;
            return resultSet;
        }
        return resultSet;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ExitSequencingRequestProcess", "", 1);
        throw e;
    }
}


/**
* Retry Sequencing Request Process [SB.2.10]
* RetrySequencingRequestProcess()
* This Function is used to process Retry Sequencing Request.
* @param scormApi instance of the Scorm2004Api.js 
*/
Scorm2004ADLNavChoice.prototype.RetrySequencingRequestProcess = function () {
    this.scormApi.DebuggerTraceCall('RetrySequencingRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        // pseudo code point 1
        if (this.currentActivity == null) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_10_1, "", 1);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_10_1;
            return resultSet;
        }

        // pseudo code point 2
        if (this.scormApi.courseDataModel[this.currentActivityIndex].cmi.isActive.value || this.scormApi.courseDataModel[this.currentActivityIndex].cmi.isSuspended.value) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_10_2, "", 1);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_10_2;
            return resultSet;
        }

        // pseudo code point 3
        if (!this.IsLeafActivity(this.currentActivity)) {

            // the bellow function was called before s_Pool refactoring but now has been comentaed 
            // as to full fill the scorm pseudo code specification

            //this.InitializeForNewAttempt(this.currentActivityIndex, true, true);

            returnedResultSet = this.FlowSubprocess(this.currentActivity, "forward", true);

            if (!returnedResultSet.value) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_10_3, "", 1);
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_10_3;
                return resultSet;
            }
            else {
                resultSet.deliveryRequest = returnedResultSet.identifiedActivity;
                return resultSet;
            }
        }
            // pseudo code point 4
        else {
            resultSet.deliveryRequest = this.currentActivity;
            return resultSet;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in RetrySequencingRequestProcess", "", 1);
        throw e;
    }
}


/**
* Continue Sequencing Request Process [SB.2.7]
* ContinueSequencingRequestProcess()
* This Function is used to process Continue Sequencing Request.
* @param scormApi instance of the Scorm2004Api.js 
*/
Scorm2004ADLNavChoice.prototype.ContinueSequencingRequestProcess = function () {
    this.scormApi.DebuggerTraceCall('ContinueSequencingRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        // continue Sequencing Request Process
        // pseudo code point 1
        if (this.currentActivity == null) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_7_1, "", 1);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_7_1;
            return resultSet;
        }

        // pseudo code point 2 //checking if current activity is not the root activity 
        if (!this.IsRootActivity(this.currentActivity)) {

            // pseudo code point 2.1 //checking if Sequencing ControlMode Flow for the parent of the Current Activity is False 
            if (!this.GetSequencingElementsAttributeValue(this.currentActivityIndex, true, "controlMode", "flow", false)) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_7_2, "", 1);
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_7_2; //hard coded temporarily
                return resultSet;
            }
        }

        // pseudo code point 3 

        returnedResultSet = this.FlowSubprocess(this.currentActivity, "forward", false);

        // pseudo code point 4
        if (!returnedResultSet.value) {
            resultSet.endSequencingSession = returnedResultSet.endSequencingSession;
            resultSet.exception = returnedResultSet.exception;
            return resultSet;
        }
            // pseudo code point 5
        else {
            resultSet.deliveryRequest = returnedResultSet.identifiedActivity;
            resultSet.deliverable = resultSet.deliveryRequest; //extra value added
            return resultSet;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ContinueSequencingRequestProcess", "", 1);
        throw e;
    }
}

/**
* Previous Sequencing Request Process [SB.2.8]
* PreviousSequencingRequestProcess()
* This Function is used to process Previous Sequencing Request.
* @param scormApi instance of the Scorm2004Api.js 
*/
Scorm2004ADLNavChoice.prototype.PreviousSequencingRequestProcess = function () {
    this.scormApi.DebuggerTraceCall('PreviousSequencingRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        // pseudo code point 1
        if (this.currentActivity == null) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_8_1, "", 1);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_8_1;
            return resultSet;
        }

        // pseudo code point 2 //checking if current activity is not the root activity 
        if (!this.IsRootActivity(this.currentActivity)) {

            // pseudo code point 2.1 //checking if Sequencing ControlMode Flow for the parent of the Current Activity is False 
            if (!this.GetSequencingElementsAttributeValue(this.currentActivityIndex, true, "controlMode", "flow", false)) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_8_2, "", 1);
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_8_2;
                return resultSet;
            }
        }


        // pseudo code point 3
        returnedResultSet = this.FlowSubprocess(this.currentActivity, "backward", false);

        // pseudo code point 4
        if (!returnedResultSet.value) {
            resultSet.exception = returnedResultSet.exception;
            return resultSet;
        }
            // pseudo code point 5
        else {
            resultSet.deliveryRequest = returnedResultSet.identifiedActivity;
            return resultSet;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in PreviousSequencingRequestProcess", "", 1);
        throw e;
    }
}


/**
* Choice Sequencing Request Process [SB.2.9]
* ChoiceSequencingRequestProcess()
* This Function is used to process Choice Sequencing Request.
* @param scormApi instance of the Scorm2004Api.js 
*/
Scorm2004ADLNavChoice.prototype.ChoiceSequencingRequestProcess = function (targetActivity) {
    this.scormApi.DebuggerTraceCall('ChoiceSequencingRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        var targetActivityIndex = this.GetItemIndex(targetActivity);

        // pseudo code point 1
        if (targetActivity == null) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_1, "", 1);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_1;
            return resultSet;
        }

        // pseudo code point 2
        var itemIndex = targetActivityIndex;
        var itemHierarchy = this.GetItemNamesHierarchyFromRoot(itemIndex, 0, true, true, false);

        for (var index = 0; index < itemHierarchy.length; index++) {
            itemIndex = this.GetItemIndex(itemHierarchy[index]);

            var parentIdetifier = this.GetParentIdentifier(itemIndex);

            if (parentIdetifier != null) {
                itemIndex = this.GetItemIndex(parentIdetifier);
                var availableChildren = this.scormApi.courseDataModel[itemIndex].cmi.availableChildren;

                if (availableChildren.indexOf(itemHierarchy[index]) < 0) {
                    this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_2, "", 1);
                    resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_2;
                    return resultSet;
                }

            }

            // pseudo code point 3.2
            returnedResultSet = this.SequencingRulesCheckProcess(itemIndex, [this.cEnum.SequencingConditionRules.PreConditionRuleAction.HiddenFromChoice], this.cEnum.SequencingConditionRules.PreConditionRule);

            // pseudo code point 3.3
            if (returnedResultSet.action != null) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_3, "", 1);
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_3;
                return resultSet;
            }
        }

        // pseudo code point 4
        if (!this.IsRootActivity(targetActivity)) {

            // pseudo code point 4.1
            if (!this.GetSequencingElementsAttributeValue(targetActivityIndex, true, "controlMode", "choice", true)) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_4, "", 1);
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_4;
                return resultSet;
            }
        }

        // pseudo code point 5
        var commonAncestorItemIdentifier
        if (this.currentActivity != null) {
            commonAncestorItemIdentifier = this.GetCommonAncestor(this.currentActivityIndex, targetActivityIndex);
        }
            // pseudo code point 6
        else {
            commonAncestorItemIdentifier = this.scormApi.imsManifest.organizations[0].identifier;
        }

        // pseudo code point 7
        if (this.currentActivityIndex == targetActivityIndex) {
            //return resultSet;
        }

            // pseudo code point 8
        else if (this.currentActivity != null && this.GetParentIdentifier(this.currentActivityIndex) == this.GetParentIdentifier(targetActivityIndex)) {

            // pseudo code point 8.1
            var activityList = new Array();
            var loopFrom;
            var loopTo;
            if (this.currentActivityIndex >= targetActivityIndex) {
                loopFrom = targetActivityIndex + 1;
                loopTo = this.currentActivityIndex + 1;
            }
            else {
                loopFrom = this.currentActivityIndex;
                loopTo = targetActivityIndex;
            }

            for (var index = loopFrom; index < loopTo; index++) {
                activityList.push(this.scormApi.imsManifest.organizations[0].items[index].identifier);
            }

            // pseudo code point 8.2
            if (activityList.length == 0) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_5, "", 1);
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_5;
                return resultSet;
            }

            var traverseDirection;

            // pseudo Code point 8.3
            if (targetActivityIndex > this.currentActivityIndex) {
                traverseDirection = "forward";
            }
                // pseudo Code point 8.4
            else {
                traverseDirection = "backward";
            }

            // pseudo Code point 8.5
            for (var index = 0; index < activityList.length; index++) {
                returnedResultSet = this.ChoiceActivityTraversalSubprocess(activityList[index], traverseDirection);
                if (!returnedResultSet.reachable) {
                    resultSet.exception = returnedResultSet.exception;
                    return resultSet
                }
            }
        }

            // pseudo code point 9

        else if (this.currentActivity == commonAncestorItemIdentifier || this.currentActivity == null) {

            var commonAncestorItemIdentifierIndex = this.GetItemIndex(commonAncestorItemIdentifier);
            commonAncestorItemIdentifierIndex = commonAncestorItemIdentifierIndex == undefined ? 0 : commonAncestorItemIdentifierIndex;

            // pseudo code point 9.1
            var activityList = this.GetItemNamesHierarchyFromRoot(targetActivityIndex, commonAncestorItemIdentifierIndex, false, true, false);

            // pseudo code point 9.2
            if (activityList.length == 0) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_5, "", 1);
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_5;
                return resultSet;
            }

            // pseudo code point 9.3
            for (var index = 0; index < activityList.length; index++) {

                // pseudo code point 9.3.1
                var returnedResultSet = this.ChoiceActivityTraversalSubprocess(activityList[index], "forward");

                // pseudo code point 9.3.2
                if (!returnedResultSet.reachable) {
                    resultSet.exception = returnedResultSet.exception;
                    return resultSet;
                }

                var preventActivation = this.GetSequencingElementsAttributeValue(this.GetItemIndex(activityList[index]), false, "constrainedChoiceConsiderations", "preventActivation", false);

                // pseudo code point 9.3.3
                if (!this.scormApi.courseDataModel[this.GetItemIndex(activityList[index])].cmi.isActive.value && (activityList[index] != commonAncestorItemIdentifier && preventActivation)) {
                    this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_6, "", 1);
                    resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_6;
                    return resultSet;
                }
            }
        }

            // pseudo code point 10
        else if (targetActivity == commonAncestorItemIdentifier) {

            // pseudo code point 10.1
            var activityList = this.GetItemNamesHierarchyFromRoot(this.currentActivityIndex, targetActivityIndex, true, true, false);

            // pseudo code point 10.2
            if (activityList.length == 0) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_5, "", 1);
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_5;
                return resultSet;
            }

            // pseudo code point 10.3
            for (var index = 0; index < activityList.length; index++) {
                if (index != activityList.length - 1) {
                    if (!this.GetSequencingElementsAttributeValue(this.GetItemIndex(activityList[index]), false, "controlMode", "choiceExit", true)) {
                        this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_7, "", 1);
                        resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_7;
                        return resultSet;
                    }
                }
            }
        }

            // pseudo code point 11
        else if (commonAncestorItemIdentifierIndex > targetActivityIndex
            && this.scormApi.imsManifest.organizations[0].items[commonAncestorItemIdentifierIndex].level == this.scormApi.imsManifest.organizations[0].items[targetActivityIndex].level) {

            // pseudo code 11.1
            var activityList = this.GetItemNamesHierarchyFromRoot(this.currentActivityIndex, commonAncestorItemIdentifierIndex, true, false, false);

            // pseudo code 11.2
            if (activityList.length == 0) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_5, "", 1);
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_5;
                return resultSet;
            }

            // pseudo code 11.3
            var constrainedActivity = undefined;

            // pseudo code 11.4
            for (var index = 0; index < activityList.length; index++) {
                if (!this.GetSequencingElementsAttributeValue(this.GetItemIndex(activityList[index]), false, "controlMode", "choiceExit", true)) {
                    this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_7, "", 1);
                    resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_7;
                    return resultSet;
                }

                if (constrainedActivity == undefined) {
                    if (this.GetSequencingElementsAttributeValue(this.GetItemIndex(activityList[index]), false, "constrainedChoiceConsiderations", "constrainChoice", false)) {
                        constrainedActivity = activityList[index];
                    }
                }
            }

            // pseudo code 11.5
            if (constrainedActivity != undefined) {

                // pseudo code 11.5.1
                if (this.GetItemIndex(targetActivity) > this.GetItemIndex(constrainedActivity)) {
                    traverseDirection = "forward";
                }
                    // pseudo code 11.5.2
                else {
                    traverseDirection = "backward";
                }

                // pseudo code 11.5.3
                returnedResultSet = this.ChoiceFlowSubprocess(constrainedActivity, traverseDirection);

                // pseudo code 11.5.4
                var activityToConsider = returnedResultSet.identifiedActivity;

                // pseudo code 11.5.5
                var availableChildren = this.scormApi.courseDataModel[this.GetItemIndex(activityToConsider)].cmi.availableChildren;

                if ((availableChildren.indexOf(targetActivity) < 0) && targetActivity != activityToConsider && targetActivity != constrainedActivity) {
                    resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_8;
                    return resultSet;
                }
            }

            // pseudo code point 11.6
            activityList = this.GetItemNamesHierarchyFromRoot(targetActivityIndex, commonAncestorItemIdentifierIndex, false, true, false);

            // pseudo code point 11.7
            if (activityList.length == 0) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_5, "", 1);
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_5;
                return resultSet;
            }

            // pseudo code point 11.8
            if (targetActivityIndex > this.currentActivityIndex) {

                // pseudo code point 11.8.1
                for (var index = 0; index < activityList.length; index++) {

                    // pseudo code point 11.8.1.1
                    var returnedResultSet = this.ChoiceActivityTraversalSubprocess(activityList[index], "forward");

                    // pseudo code point 11.8.1.2
                    if (!returnedResultSet.reachable) {
                        resultSet.exception = returnedResultSet.exception;
                        return resultSet;
                    }

                    var preventActivation = this.GetSequencingElementsAttributeValue(this.GetItemIndex(activityList[index]), false, "constrainedChoiceConsiderations", "preventActivation", false);

                    // pseudo code point 11.8.1.3
                    if (!this.scormApi.courseDataModel[this.GetItemIndex(activityList[index])].cmi.isActive.value && (activityList[index] != commonAncestorItemIdentifier && preventActivation)) {
                        this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_6, "", 1);
                        resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_6;
                        return resultSet;
                    }
                }
            }

                // pseudo code point 11.9
            else {
                activityList.push(targetActivity);
                for (var index = 0; index < activityList.length; index++) {

                    var preventActivation = this.GetSequencingElementsAttributeValue(this.GetItemIndex(activityList[index]), false, "constrainedChoiceConsiderations", "preventActivation", false);
                    var isActive = this.scormApi.courseDataModel[this.GetItemIndex(activityList[index])].cmi.isActive.value;

                    if (!isActive && (activityList[index] != commonAncestorItemIdentifier && preventActivation)) {
                        this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_9_6, "", 1);
                        resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_6;
                        return resultSet;
                    }
                }
            }
        }

        // pseudo code point 12
        if (this.IsLeafActivity(targetActivity)) {
            resultSet.deliveryRequest = targetActivity;
            return resultSet;
        }

        // pseudo code point 13
        returnedResultSet = this.FlowSubprocess(targetActivity, "forward", true);

        // pseudo code point 14
        if (!returnedResultSet.value) {
            this.TerminateDescendentAttemptsProcess(commonAncestorItemIdentifier);
            this.EndAttemptProcess(commonAncestorItemIdentifier);
            this.SetCurrentActivity(targetActivity);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_9_9;
            return resultSet;
        }
            // pseudo code point 15
        else {
            resultSet.deliveryRequest = returnedResultSet.identifiedActivity;
            return resultSet;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ChoiceSequencingRequestProcess", "", 1);
        throw e;
    }
}


/**
* Jump Sequencing Request Process [SB.2.13]
* JumpSequencingRequestProcess()
* This Function is used to process Jump Sequencing Request.
* @param scormApi           instance of the Scorm2004Api.js 
* @param targetActivity     targetActivity
*/
Scorm2004ADLNavChoice.prototype.JumpSequencingRequestProcess = function (targetActivity) {
    this.scormApi.DebuggerTraceCall('JumpSequencingRequestProcess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        if (this.currentActivity == null) {
            this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_13_1, "", 1);
            resultSet.exception = Scorm2004Api.ERRORS.SB_2_13_1;
            return resultSet;
        }

        resultSet.deliveryRequest = targetActivity;
        return resultSet;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in JumpSequencingRequestProcess", "", 1);
        throw e;
    }
}


/**
* Terminate Descendent Attempts Process [UP.3]
* TerminateDescendentAttemptsProcess(itemIdentifier)
* This Function is used to process Jump Sequencing Request.
* @param scormApi           instance of the Scorm2004Api.js 
* @param itemIdentifier     itemIdentifier
*/
Scorm2004ADLNavChoice.prototype.TerminateDescendentAttemptsProcess = function (itemIdentifier) {
    this.scormApi.DebuggerTraceCall('TerminateDescendentAttemptsProcess');
    try {
        // pseudo code point 1
        var commonAncestorItemIdentifier;
        if (this.currentActivity != null) {
            commonAncestorItemIdentifier = this.GetCommonAncestor(this.currentActivityIndex, this.GetItemIndex(itemIdentifier));
        }

        // pseudo code point 2
        var activityList = this.GetItemNamesHierarchyFromRoot(this.currentActivityIndex, this.GetItemIndex(commonAncestorItemIdentifier), false, false, false);

        // pseudo code point 3
        if (activityList.length > 0) {
            // pseudo code point 3.1
            for (var index = 0; index < activityList.length; index++) {
                // pseudo code point 3.1.1
                this.EndAttemptProcess(activityList[index]);
            }
        }
        return;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in TerminateDescendentAttemptsProcess", "", 1);
        throw e;
    }
}


/**
* Choice Activity Traversal Subprocess [SB.2.4]
* ChoiceActivityTraversalSubprocess(itemIdentifier,traversalDirection)
* This Function is used to process Jump Sequencing Request.
* @param itemIdentifier         itemIdentifier
* @param traversalDirection     traversalDirection
*/
Scorm2004ADLNavChoice.prototype.ChoiceActivityTraversalSubprocess = function (itemIdentifier, traversalDirection) {
    this.scormApi.DebuggerTraceCall('ChoiceActivityTraversalSubprocess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        // pseudo code point 1
        if (traversalDirection == "forward") {

            // pseudo code point 1.1
            returnedResultSet = this.SequencingRulesCheckProcess(this.GetItemIndex(itemIdentifier), [this.cEnum.SequencingConditionRules.PreConditionRuleAction.StopForwardTraversal], this.cEnum.SequencingConditionRules.PreConditionRule);

            // pseudo code point 1.2
            if (returnedResultSet.action != null) {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_4_1, "", 1);
                resultSet.reachable = false;
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_4_1;
                return resultSet;
            }
            resultSet.reachable = true;
            return resultSet;
        }

        // pseudo code point 2
        if (traversalDirection == "backward") {

            // pseudo code point 2.1
            if (itemIdentifier != this.scormApi.imsManifest.organizations[0].identifier) {
                if (this.GetSequencingElementsAttributeValue(this.GetItemIndex(itemIdentifier), true, "controlMode", "forwardOnly", false)) {
                    this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_4_2, "", 1);
                    resultSet.reachable = false;
                    resultSet.exception = Scorm2004Api.ERRORS.SB_2_4_2;
                    return resultSet;
                }
            }
                // pseudo code point 2.2
            else {
                this.scormApi.setActiveError(Scorm2004Api.ERRORS.SB_2_4_3, "", 1);
                resultSet.reachable = false;
                resultSet.exception = Scorm2004Api.ERRORS.SB_2_4_3;
                return resultSet;
            }

            // pseudo code point 2.3
            resultSet.reachable = true;
            return resultSet;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ChoiceActivityTraversalSubprocess", "", 1);
        throw e;
    }
}



/**
* Choice Flow Tree Traversal Subprocess [SB.2.9.2]
* ChoiceFlowTreeTraversalSubprocess(itemIdentifier,traversalDirection)
* This Function is used to process Jump Sequencing Request.
* @param itemIdentifier         itemIdentifier
* @param traversalDirection     traversalDirection
*/
Scorm2004ADLNavChoice.prototype.ChoiceFlowTreeTraversalSubprocess = function (itemIdentifier, traversalDirection) {
    this.scormApi.DebuggerTraceCall('ChoiceFlowTreeTraversalSubprocess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();
        var itemIndex = this.GetItemIndex(itemIdentifier);
        var availableChildren = this.scormApi.courseDataModel[itemIndex].cmi.availableChildren;

        // pseudo code point 1
        if (traversalDirection == "forward") {

            // pseudo code point 1.1
            if (this.IsRootActivity(itemIdentifier)
            || itemIdentifier == this.scormApi.imsManifest.organizations[0].items[this.scormApi.imsManifest.organizations[0].items.length - 1].identifier) {
                return resultSet;
            }

            // pseudo code point 1.2
            if (availableChildren.length > 0 && itemIdentifier == availableChildren[availableChildren.length - 1]) {
                returnedResultSet = this.ChoiceFlowTreeTraversalSubprocess(this.scormApi.imsManifest.organizations[0].items[itemIndex].parentIdentifier, "forward");
                resultSet.nextActivity = returnedResultSet.nextActivity;
                return resultSet;
            }
                // pseudo code point 1.3
            else if (availableChildren.length > 0) {
                resultSet.nextActivity = availableChildren[availableChildren.indexOf(itemIdentifier) + 1];
                return resultSet;
            }
        }

        // pseudo code point 2
        if (traversalDirection == "backward") {

            // pseudo code point 2.1
            if (this.IsRootActivity(itemIdentifier)) {
                return resultSet;
            }

            // pseudo code point 2.2
            if (availableChildren.length > 0 && itemIdentifier == availableChildren[0]) {
                returnedResultSet = this.ChoiceFlowTreeTraversalSubprocess(this.scormApi.imsManifest.organizations[0].items[itemIndex].parentIdentifier, "backward");
                resultSet.nextActivity = returnedResultSet.nextActivity;
                return resultSet;
            }
                // pseudo code point 2.3
            else if (availableChildren.length > 0) {
                resultSet.nextActivity = availableChildren[availableChildren.indexOf(itemIdentifier) - 1];
                return resultSet;
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ChoiceFlowTreeTraversalSubprocess", "", 1);
        throw e;
    }
}


/**
* Choice Flow Subprocess [SB.2.9.1]
* ChoiceFlowSubprocess(itemIdentifier,traversalDirection)
* This Function is used to process Jump Sequencing Request.
* @param itemIdentifier         itemIdentifier
* @param traversalDirection     traversalDirection
*/
Scorm2004ADLNavChoice.prototype.ChoiceFlowSubprocess = function (itemIdentifier, traversalDirection) {
    this.scormApi.DebuggerTraceCall('ChoiceFlowSubprocess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        // pseudo code point 1
        returnedResultSet = this.ChoiceFlowTreeTraversalSubprocess(itemIdentifier, traversalDirection);

        // pseudo code point 2
        if (returnedResultSet.nextActivity == null) {
            resultSet.identifiedActivity = itemIdentifier;
            return resultSet;
        }
            // pseudo code point 3
        else {
            resultSet.identifiedActivity = returnedResultSet.nextActivity;
            return resultSet;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ChoiceFlowSubprocess", "", 1);
        throw e;
    }
}

/**
* Overall Rollup Process [RB.1.5]
* OverallRollupProcess() 
* This Function is used for an activity; may change the tracking information for the activity and its ancestors.
* @param scormApi               instance of the Scorm2004Api.js.
*/
Scorm2004ADLNavChoice.prototype.OverallRollupProcess = function (itemIndex) {

    this.scormApi.DebuggerTraceCall('OverallRollupProcess');
    try {
        var sortedResult = new Array();
        //var current_activity = this.GetCurrentActivity(); //this.scormApi.activeSCOindex
        sortedResult = this.GetItemNamesHierarchyFromRoot(itemIndex, 0, true, true, false);
        var reverseResult = new Array();
        reverseResult = sortedResult.reverse();

        if (reverseResult.length === 0) {
            return;
        }
        for (var index = 0; index < reverseResult.length; index++) {
            var itemIdentifier = reverseResult[index];
            if (!this.IsLeafActivity(itemIdentifier)) {

                this.MeasureRollupProcess(this.GetItemIndex(itemIdentifier));
                this.CompletionMeasureRollupProcess(this.GetItemIndex(itemIdentifier));
            }
            itemIndex = this.GetItemIndex(itemIdentifier);
            // apply the appropriate Objective Rollup Process to the activity        

            var rolledUpObjective = this.GetRolledupObjectiveForItem(itemIndex);
            var satisfiedByMeasure = false;
            if (rolledUpObjective != null && rolledUpObjective.satisfiedByMeasure) {
                satisfiedByMeasure = true;
            }

            if (satisfiedByMeasure) {
                this.ObjectiveRollupUsingMeasureProcess(itemIndex);
            }
            else {
                this.ObjectiveRollupUsingRulesProcess(itemIndex);
            }

            // 3.3. apply the appropriate Activity Progress Rollup Process to the activity
            var completedByMeasure = this.GetActivityElementsAttributeValue(itemIndex, false, "adlcpCompletionThreshold", "completedByMeasure", false);

            if (completedByMeasure) {
                this.ActivityProgressRollupUsingMeasureProcess(itemIndex);
            }
            else {
                this.ActivityProgressRollupUsingRulesProcess(itemIndex);
            }
        }
        return;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in OverallRollupProcess", "", 1);
        throw e;
    }
}

/**
* Measure Rollup Process [RB.1.1 a]  -- p.no. 186
* MeasureRollupProcess() 
* This Function is used for an activity; may change the Objective Information for the activity.
* @param scormApi               instance of the Scorm2004Api.js.
* @param activityIndex          index of the activity on which measure rollup process to be applied.
*/
Scorm2004ADLNavChoice.prototype.MeasureRollupProcess = function (activityIndex) {
    this.scormApi.DebuggerTraceCall('MeasureRollupProcess');
    try {
        var total_weighted_measure = 0.0;
        var valid_data = false;
        var counted_measures = 0.0;
        var target_objective = null;

        var data = this.scormApi.courseDataModel;

        var itemObjectives = this.GetItemObjectivesFromManifest(activityIndex, this.cEnum.ObjectiveTypes.all);

        for (var index = 0; index < itemObjectives.length; index++) {
            if (itemObjectives[index].objectiveContributesToRollup) {
                target_objective = itemObjectives[index];
                break;
            }
        }

        if (target_objective != null) {

            var ChildrenArray = new Array();
            ChildrenArray = this.scormApi.courseDataModel[activityIndex].cmi.availableChildren;;

            for (var counter = 0; counter < ChildrenArray.length; counter++) {

                var childrenActivity = ChildrenArray[counter];
                var childrenActivityIndex = this.GetItemIndex(childrenActivity);

                // 6.1.1. if Tracked for the child is True Then
                if (this.GetSequencingElementsAttributeValue(childrenActivityIndex, false, "deliveryControls", "tracked", true)) {

                    var rolledup_objective = null;

                    var itemObjectives = this.GetItemObjectivesFromManifest(childrenActivityIndex, this.cEnum.ObjectiveTypes.all);

                    for (var index = 0; index < itemObjectives.length; index++) {
                        if (itemObjectives[index].objectiveContributesToRollup) {
                            rolledup_objective = itemObjectives[index];
                            break;
                        }
                    }

                    // 6.1.1.3. If rolled-up objective is Defined Then
                    if (rolledup_objective != null) {

                        // increment counted measures by the "Rollup Objective Measure Weight" for the child
                        counted_measures += this.GetSequencingElementsAttributeValue(childrenActivityIndex, false, "rollupRules", "objectiveMeasureWeight", 0);

                        if (this.GetPrimaryObjectiveSatusValues(childrenActivityIndex, this.cEnum.RollupObjectiveStatusElement.objectiveMeasureStatus, null)) {

                            // add the product of "Objective Normalized Measure" for the rolled-up objective multiplied by the "Rollup Objective Measure Weight" for the child to the total weighted measure
                            var objectiveNormalizedMeasure = this.GetPrimaryObjectiveSatusValues(activityIndex, this.cEnum.RollupObjectiveStatusElement.objectiveNormalizedMeasure, null);
                            var rollupObjectiveMeasureWeight = this.GetSequencingElementsAttributeValue(childrenActivityIndex, false, "rollupRules", "objectiveMeasureWeight", 0);

                            total_weighted_measure += objectiveNormalizedMeasure * rollupObjectiveMeasureWeight;
                            valid_data = true;
                        }
                    }
                    else {
                        return;
                    }
                }
            }
            if (!valid_data) {
                // set the "Objective Measure Status" for the target objective to False     
                this.SetPrimaryObjectiveSatusValues(activityIndex, this.cEnum.RollupObjectiveStatusElement.objectiveMeasureStatus, false);
            }
            else {
                if (counted_measures > 0.0) {

                    // set the Objective Measure Status for the target objective to True
                    this.SetPrimaryObjectiveSatusValues(activityIndex, this.cEnum.RollupObjectiveStatusElement.objectiveMeasureStatus, true);

                    // set the Objective Normalized Measure for the target objective to the total weighted measure divided by counted measures
                    var val = total_weighted_measure / counted_measures;
                    this.SetPrimaryObjectiveSatusValues(activityIndex, this.cEnum.RollupObjectiveStatusElement.objectiveNormalizedMeasure, val);
                }
                else {
                    // set the Objective Measure Status for the target objective to False
                    this.SetPrimaryObjectiveSatusValues(activityIndex, this.cEnum.RollupObjectiveStatusElement.objectiveMeasureStatus, false);
                }
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in MeasureRollupProcess", "", 1);
        throw e;
    }
}


/**
* Completion Measure Rollup Process [RB.1.1 b]-- p.No. 188
* CompletionMeasureRollupProcess() 
* This Function is used for an activity; may change the Objective Information for the activity.
* @param scormApi               instance of the Scorm2004Api.js.
* @param activityIndex          index of the activity on which measure rollup process to be applied.
*/
Scorm2004ADLNavChoice.prototype.CompletionMeasureRollupProcess = function (activityIndex) {
    this.scormApi.DebuggerTraceCall('CompletionMeasureRollupProcess');
    try {
        var total_weighted_measure = 0.0;
        var valid_data = false;
        var counted_measures = 0.0

        var ChildrenArray = new Array();
        ChildrenArray = this.scormApi.courseDataModel[activityIndex].cmi.availableChildren;;
        for (index = 0; index < ChildrenArray.length; index++) {
            var childrenActivity = ChildrenArray[index];
            var childrenActivityIndex = this.GetItemIndex(childrenActivity);

            if (this.GetSequencingElementsAttributeValue(childrenActivityIndex, false, "deliveryControls", "tracked", true)) {

                // increment counted measures by the adlcp:progressWeight for the child
                counted_measures += this.GetCompletionThresholdAttributeValue(childrenActivityIndex, this.cEnum.AdlcpCompletionThreshold.ProgressWeight, Scorm2004ADLNavChoice.ItemOrOrganization.Item);

                // if the Attempt Completion Amount Status is True
                // if (this.scormApi.courseDataModel[childrenActivityIndex].cmi.attemptCompletionAmountStatus.value == true) {
                if (this.GetPrimaryObjectiveSatusValues(childrenActivityIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmountStatus) == true) {

                    // add the product of Attempt Completion Amount multiplied by the adlcp:progressWeight to the total weighted measure
                    // var attemptCompletionAmount = this.scormApi.courseDataModel[childrenActivityIndex].cmi.attemptCompletionAmount.value;
                    var attemptCompletionAmount = this.GetPrimaryObjectiveSatusValues(childrenActivityIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmount);
                    var progressWeight = this.GetCompletionThresholdAttributeValue(childrenActivityIndex, this.cEnum.AdlcpCompletionThreshold.ProgressWeight, Scorm2004ADLNavChoice.ItemOrOrganization.Item);
                    total_weighted_measure += attemptCompletionAmount * progressWeight;

                    valid_data = true;

                }
            }
        }
        if (valid_data == false) {

            this.SetPrimaryObjectiveSatusValues(activityIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmountStatus, false);
        }
        else {
            if (counted_measures > 0.0) {
                // set the Attempt Completion Amount Status to True

                this.SetPrimaryObjectiveSatusValues(activityIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmountStatus, true);
                // set the Attempt Completion Amount to the total weighted measure divided by counted measures

                this.SetPrimaryObjectiveSatusValues(activityIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmount, total_weighted_measure / counted_measures);
            }
            else {
                // TODO: Set the Attempt Completion Amount Status for the target objective to False            
                var objective = this.GetRolledupObjectiveForItem(activityIndex);
                var objectiveCmi = this.GetItemObjectivesIndexFromCourseDataModel(activityIndex, objective.objectiveId);
                this.scormApi.courseDataModel[activityIndex].cmi.objectives.__array[objectiveCmi].attemptCompletionAmount.value = false; //we considered target objective to the rollup objective, it seems that they mean it.
            }
        }
        return;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in CompletionMeasureRollupProcess", "", 1);
        throw e;
    }
}


/**
* -------------------------------------------- 4.6.5. Objective Rollup Process-- p.No. 123-----------------------------------------
* Objective Rollup Using Measure Process [RB.1.2 a]-- p.No. 189
* Objective Rollup Using Rules Process [RB.1.2 b]-- p.No. 191
* ObjectiveRollupUsingMeasureProcess() 
* This Function is used to set the cluster’s rolled-up objective status to “unknown”, “satisfied” or “not satisfied”.
* @param scormApi               instance of the Scorm2004Api.js.
* @param activityIndex          index of the activity on which measure rollup process to be applied.
*/

Scorm2004ADLNavChoice.prototype.ObjectiveRollupUsingMeasureProcess = function (itemIndex) {

    this.scormApi.DebuggerTraceCall('ObjectiveRollupUsingMeasureProcess');
    try {
        var target_objective = undefined;
        var data = this.scormApi.courseDataModel;
        var IsActivityIsActive = data[itemIndex].cmi.isActive.value;
        var IsSuspended = data[itemIndex].cmi.isSuspended.value;

        target_objective = this.GetPrimaryObjectiveFromCDM(itemIndex);

        if (target_objective != null || target_objective != undefined) {

            // if Objective Satisfied by Measure for the target objective is True
            if (this.IsObjectiveSatisfiedByMeasure(itemIndex, target_objective.id.value)) {

                var ObjectiveMeasureStatus = target_objective.objectiveMeasureStatus.value;

                // if the Objective Measure Status for the target objective is False
                if (!ObjectiveMeasureStatus) {
                    // set the Objective Progress Status for the target objective to false
                    target_objective.objectiveProgressStatus.value = false;
                }
                else {

                    // activity is Active for the activity is False || Activity is Active for the activity is True && adlseq:measureSatisfactionIfActive for the activity is True
                    if (!IsActivityIsActive || (IsActivityIsActive == true && this.MeasureSatisfactionIfActive(itemIndex))) {

                        // if the Objective Normalized Measure for the target objective is greater than or equal (>=) to the Objective Minimum Satisfied Normalized Measure for the target objective Then*/
                        var objectiveNormalizedMeasure = target_objective.objectiveNormalizedMeasure.value;
                        var objectiveMinimumSatisfiedNormalizedMeasure = this.GetObjectiveMinNormalizedMeasure(itemIndex, target_objective.id.value);

                        if (objectiveMinimumSatisfiedNormalizedMeasure != null && objectiveNormalizedMeasure != null
                        && objectiveNormalizedMeasure >= objectiveMinimumSatisfiedNormalizedMeasure) {
                            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, true);
                            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveSatisfiedStatus, true);
                        }
                        else {
                            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, true);
                            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveSatisfiedStatus, false);
                        }

                    }
                    else {
                        this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, false);
                    }
                }
            }
            return;
        }
        else {
            return;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ObjectiveRollupUsingMeasureProcess", "", 1);
        throw e;
    }
}

/**
* -------------------------------------------- 4.6.5. Objective Rollup Process-- p.No. 123-----------------------------------------
* Objective Rollup Using Measure Process [RB.1.2 a]-- p.No. 189
* Objective Rollup Using Rules Process [RB.1.2 b]-- p.No. 191
* ObjectiveRollupUsingRulesProcess() 
* This Function is used to set the cluster’s rolled-up objective status to “unknown”, “satisfied” or “not satisfied”.
* @param scormApi               instance of the Scorm2004Api.js.
* @param activityIndex          index of the activity on which measure rollup process to be applied.
*/

Scorm2004ADLNavChoice.prototype.ObjectiveRollupUsingRulesProcess = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('ObjectiveRollupUsingRulesProcess');
    try {
        var satisfiedActionExists = this.RollupActionExists(itemIndex, this.cEnum.RollupAction.Satisfied);
        var notSatisfiedActionExists = this.RollupActionExists(itemIndex, this.cEnum.RollupAction.NotSatisfied);
        var satisfiedActionRollupRule = null;
        var notSatisfiedActionRollupRule = null;

        // if the activity does not include Rollup Rules with the Not Satisfied rollup action And the activity does not include Rollup Rules with the Satisfied rollup action
        if (!satisfiedActionExists && !notSatisfiedActionExists) {

            // 1. apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Satisfied; and a Rollup Action of Satisfied
            satisfiedActionRollupRule = new RollupRuleDataModel();
            satisfiedActionRollupRule.childActivitySet = "all";
            satisfiedActionRollupRule.rollupConditions = new Array();
            var rollupCondition = new RollupConditionDataModel();
            rollupCondition.condition = "satisfied";
            satisfiedActionRollupRule.rollupConditions.push(rollupCondition);
            satisfiedActionRollupRule.rollupAction = "satisfied";

            // apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Objective Status Known; and a Rollup Action of Not Satisfied
            notSatisfiedActionRollupRule = new RollupRuleDataModel();
            notSatisfiedActionRollupRule.childActivitySet = "all";
            notSatisfiedActionRollupRule.rollupConditions = new Array();
            var rollupCondition = new RollupConditionDataModel();
            rollupCondition.condition = "objectiveStatusKnown";
            notSatisfiedActionRollupRule.rollupConditions.push(rollupCondition);
            notSatisfiedActionRollupRule.rollupAction = "notSatisfied";
        }

        var target_objective = undefined;
        target_objective = this.GetPrimaryObjectiveFromCDM(itemIndex);

        if (target_objective != undefined) {

            // apply the Rollup Rule Check Subprocess to the activity and the Not Satisfied rollup action
            var rollupRules = null;
            if (notSatisfiedActionRollupRule != null) {
                rollupRules = new Array();
                rollupRules.push(notSatisfiedActionRollupRule);
            }
            var result = this.RollUpRuleCheckSubprocess(itemIndex, "notSatisfied", rollupRules);

            // if the Rollup Rule Check Subprocess returned True Then
            if (result) {
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, true);
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveSatisfiedStatus, false);
            }

            // apply the Rollup Rule Check Subprocess to the activity and the Satisfied rollup action
            var rollupRules = null;
            if (satisfiedActionRollupRule != null) {
                rollupRules = new Array();
                rollupRules.push(satisfiedActionRollupRule);
            }
            var result = this.RollUpRuleCheckSubprocess(itemIndex, "satisfied", rollupRules);

            // if the Rollup Rule Check Subprocess returned True Then
            if (result) {
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, true);
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveSatisfiedStatus, true);
            }
            return;
        }
        else {
            return;
        }


    }
    catch (e) {
        this.scormApi.traceError("Error caught in ObjectiveRollupUsingRulesProcess", "", 1);
        throw e;
    }
}

/**
* GetItemObjectivesFromManifest(itemIdentifier, objectiveTypes) 
* This Function is used to get objectives from manifest data model for a certain item(all or primary or other)
* @param itemIdentifier              itemIdentifier
* @param objectiveTypes              objectiveTypes
*/
Scorm2004ADLNavChoice.prototype.GetItemObjectivesFromManifest = function (itemIndex, objectiveTypes) {
    this.scormApi.DebuggerTraceCall('GetItemObjectivesFromManifest');
    try {
        var result = new Array();

        var imsssSequencing = this.scormApi.imsManifest.organizations[0].items[itemIndex].imsssSequencing;
        if (imsssSequencing != null) {
            var imsssObjectives = imsssSequencing.imsssObjectives;
            if (imsssObjectives != null) {
                var primaryObjective = imsssObjectives.primaryObjective;
                var objectives = imsssObjectives.objectives;
                if (primaryObjective != null
                    && primaryObjective.objectiveId != null
                    && primaryObjective.objectiveId.length > 0
                    && (objectiveTypes == this.cEnum.ObjectiveTypes.all || objectiveTypes == this.cEnum.ObjectiveTypes.primary)) {

                    // below block added to handle adlseq objecctives as in some package mapinfo object is on adlseq objectveis with same id
                    var adlseqobjectivesFromManifest = this.GetAdlSeqItemObjectives(itemIndex);
                    var adlseqobjectiveWithId = this.GetObjectiveWithId(primaryObjective.objectiveId, adlseqobjectivesFromManifest, this.cEnum.GetObjective.item);
                    if (adlseqobjectiveWithId != null) {
                        adlseqobjectiveWithId.objectiveContributesToRollup = true;
                        result.push(adlseqobjectiveWithId);
                    }
                    else {
                        result.push(primaryObjective);
                    }
                }
                if (objectives != null && (objectiveTypes == this.cEnum.ObjectiveTypes.all || objectiveTypes == this.cEnum.ObjectiveTypes.objective)) {
                    for (var counter = 0; counter < objectives.length; counter++) {

                        // below block added to handle adlseq objecctives as in some package mapinfo object is on adlseq objectveis with same id
                        var adlseqobjectivesFromManifest = this.GetAdlSeqItemObjectives(itemIndex);
                        var adlseqobjectiveWithId = this.GetObjectiveWithId(objectives[counter].objectiveId, adlseqobjectivesFromManifest, this.cEnum.GetObjective.item);
                        if (adlseqobjectiveWithId != null) {
                            result.push(adlseqobjectiveWithId);
                        }
                        else {
                            result.push(objectives[counter]);
                        }

                    }
                }
            }
            if (imsssSequencing["idRef"] != null) {

                var sequencingCollection = this.scormApi.imsManifest.sequencingCollection;

                if (sequencingCollection != null && sequencingCollection.length > 0) {
                    for (var index = 0; index < sequencingCollection.length; index++) {
                        if (sequencingCollection[index].id == imsssSequencing.idRef) {

                            var imsssObjectives = sequencingCollection[index].imsssObjectives;
                            if (imsssObjectives != null) {

                                var primaryObjective = imsssObjectives.primaryObjective;

                                if (primaryObjective != null
                                     && primaryObjective.objectiveId != null
                                     && primaryObjective.objectiveId.length > 0
                                     && (objectiveTypes == this.cEnum.ObjectiveTypes.all || objectiveTypes == this.cEnum.ObjectiveTypes.primary)) {

                                    // below block added to handle adlseq objecctives as in some package mapinfo object is on adlseq objectveis with same id
                                    var adlseqobjectivesFromManifest = this.GetAdlSeqItemObjectives(itemIndex);
                                    var adlseqobjectiveWithId = this.GetObjectiveWithId(primaryObjective.objectiveId, adlseqobjectivesFromManifest, this.cEnum.GetObjective.item);
                                    if (adlseqobjectiveWithId != null) {
                                        adlseqobjectiveWithId.objectiveContributesToRollup = true;
                                        result.push(adlseqobjectiveWithId);
                                    }
                                    else {
                                        result.push(primaryObjective);
                                    }
                                }

                                var objectives = imsssObjectives.objectives;

                                if (objectives != null && (objectiveTypes == this.cEnum.ObjectiveTypes.all || objectiveTypes == this.cEnum.ObjectiveTypes.objective)) {
                                    for (var counter = 0; counter < objectives.length; counter++) {
                                        if (objectives[counter] == null) {

                                            // below block added to handle adlseq objecctives as in some package mapinfo object is on adlseq objectveis with same id
                                            var adlseqobjectivesFromManifest = this.GetAdlSeqItemObjectives(itemIndex);
                                            var adlseqobjectiveWithId = this.GetObjectiveWithId(objectives[counter].objectiveId, adlseqobjectivesFromManifest, this.cEnum.GetObjective.item);
                                            if (adlseqobjectiveWithId != null) {
                                                result.push(adlseqobjectiveWithId);
                                            }
                                            else {
                                                result.push(objectives[counter]);
                                            }
                                        }

                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetItemObjectivesFromManifest", "", 1);
        throw e;
    }
}

/**
* GetItemObjectivesFromCourseDataModel(itemIdentifier, objectiveTypes) 
* This Function is used to get objectives from course data model for a certain item
* @param itemIdentifier              itemIdentifier
*/
Scorm2004ADLNavChoice.prototype.GetItemObjectivesFromCourseDataModel = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('GetItemObjectivesFromCourseDataModel');
    try {
        var result = new Array();

        var objectives = this.scormApi.courseDataModel[itemIndex].cmi.objectives;
        if (objectives != null) {
            for (var counter = 0; counter < this.scormApi.courseDataModel[itemIndex].cmi.objectives.__array.length; counter++) {

                result.push(this.scormApi.courseDataModel[itemIndex].cmi.objectives.__array[counter])
            }
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetItemObjectivesFromCourseDataModel", "", 1);
        throw e;
    }
}

/**
* GetObjectiveWithId(objectiveId, objectiveArray, getObjective) 
* This Function is used to get a certain objective from the list of objectives provided and returns the object or the index of it
* @param objectiveId              objectiveId
* @param objectiveArray           objectiveArray
* @param getObjective             getObjective
*/
Scorm2004ADLNavChoice.prototype.GetObjectiveWithId = function (objectiveId, objectiveArray, getObjective) {
    this.scormApi.DebuggerTraceCall('GetObjectiveWithId');
    try {
        for (var counter = 0; counter < objectiveArray.length; counter++) {

            var objectiveIdTemp = objectiveArray[counter].id == null ? objectiveArray[counter].objectiveId : objectiveArray[counter].id.value;
            if (objectiveId == objectiveIdTemp) {
                if (getObjective == this.cEnum.GetObjective.index) {
                    return counter;
                }
                else if (getObjective == this.cEnum.GetObjective.item) {
                    return objectiveArray[counter];
                }
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetObjectiveWithId", "", 1);
        throw e;
    }
}


/**
* ValidateSequencingRules(itemIndex, ruleType) 
* This Function is used evaluate sequencing rules of certain type and provides all actions to be applied in an array
* @param itemIndex              item index for which children to be fetched
* @param ruleType               ruleType
*/
Scorm2004ADLNavChoice.prototype.ValidateSequencingRules = function (itemIndex, ruleType) {
    this.scormApi.DebuggerTraceCall('ValidateSequencingRules');
    try {
        var ruleActionsResultArray = new Array();
        var imsssSequencing = this.scormApi.imsManifest.organizations[0].items[itemIndex].imsssSequencing;

        if (imsssSequencing != null) {

            var rules;
            if (ruleType == "preConditionRules") {
                rules = imsssSequencing.preConditionRules;
            }
            else if (ruleType == "postConditionRules") {
                rules = imsssSequencing.postConditionRules;
            }
            else if (ruleType == "exitConditionRules") {
                rules = imsssSequencing.exitConditionRules;
            }

            if (rules != null) {

                for (var index = 0; index < rules.length; index++) {

                    var conditionCombination = rules[index].conditionCombination;
                    var ruleConditions = rules[index].ruleConditions;
                    var ruleAction = rules[index].ruleAction;
                    var ruleBag = new Array();

                    if (ruleConditions != null) {

                        for (var counter = 0; counter < ruleConditions.length; counter++) {
                            var result = false;
                            var conditionOperator = ruleConditions[counter].conditionOperator;

                            var returnedResult = this.EvaluateRuleCondition(itemIndex, ruleConditions[counter].referencedObjective, ruleConditions[counter].condition);

                            if (conditionOperator != null && conditionOperator == "not") {
                                returnedResult = returnedResult == 'unknown' ? false : returnedResult;
                                result = !returnedResult;
                            }
                            else {
                                result = returnedResult;
                            }

                            ruleBag.push(result);
                        }
                    }

                    var trueCountInArray = 0;
                    for (var item in ruleBag) {
                        if (ruleBag[item] == true) {
                            trueCountInArray++;
                        }
                    }

                    if (conditionCombination == "all") {
                        if (trueCountInArray == ruleBag.length) {
                            ruleActionsResultArray.push(ruleAction);
                        }
                    }
                    else if (conditionCombination == "any") {
                        if (trueCountInArray > 0) {
                            ruleActionsResultArray.push(ruleAction);
                        }
                    }
                }
            }
        }

        return ruleActionsResultArray;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ValidateSequencingRules", "", 1);
        throw e;
    }
}

/**
* CheckMenu() 
* This Function is used to enable/disable activity tree menu item and continue/previous buttons as per sequencing rules and control choice values
*/
Scorm2004ADLNavChoice.prototype.CheckMenu = function () {
    this.scormApi.DebuggerTraceCall("CheckMenu");
    try {
        var firstLeafActivityIndex = -1;
        var lastLeafActivityIndex = -1;
        var leafActivityCounter = 0;
        var BtnPrevious = document.getElementById("BtnPrevious");
        var BtnContinue = document.getElementById("BtnContinue");

        if (BtnPrevious != null) {
            BtnPrevious.style.cursor = "pointer";
            BtnPrevious.classList.remove("DimIcon");
            BtnPrevious.disabled = false;
        }

        if (BtnContinue != null) {
            BtnContinue.style.cursor = "pointer";
            BtnContinue.classList.remove("DimIcon");
            BtnContinue.disabled = false;
        }

        var identifierPrefix = "__leaf__";
        var identifier;

        for (var index = 0; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {

            var currentItem = this.scormApi.imsManifest.organizations[0].items[index];


            identifier = currentItem.identifier;

            var isLeafActivity = this.IsLeafActivity(identifier);

            if (isLeafActivity) {

                if (leafActivityCounter == 0) {
                    firstLeafActivityIndex = index;
                    leafActivityCounter++;
                }
                lastLeafActivityIndex = index;
            }

            var liId = document.getElementById((identifierPrefix + identifier));

            if (liId != null) {

                if (index == this.scormApi.activeSCOIndex) {
                    liId.className = "SCOTreeMenuItemSelected";

                    liId.onclick = function () {
                        courseLaunchController.processLMSNavRequest("{target=" + this.id.replace("__leaf__", "") + "}choice");
                    }
                }
                else {
                    liId.className = "SCOTreeMenuItemEnabled";

                    liId.onclick = function () {
                        courseLaunchController.processLMSNavRequest("{target=" + this.id.replace("__leaf__", "") + "}choice");
                    }
                }

                if (this.GetSequencingElementsAttributeValue(index, true, "controlMode", "choice", true)) {

                    var actions = this.ValidateSequencingRules(index, "preConditionRules");

                    if (actions.indexOf("disabled") > -1 || actions.indexOf("skip") > -1) {
                        liId.style.cursor = "default";
                        liId.className = "";
                        liId.onclick = function () { return false; };
                    }

                    if (actions.indexOf("hiddenFromChoice") > -1) {
                        liId.style.display = "none";
                        liId.onclick = function () { return false; };
                        document.getElementById('img_status_' + identifier).style.display = 'none';
                    }
                    else {
                        document.getElementById('img_status_' + identifier).style.display = 'inline';
                    }
                }
                else {
                    liId.style.cursor = "default";
                    liId.onclick = function () { return false; };
                }

                if (index == this.scormApi.activeSCOIndex) {
                    liId.className = "SCOTreeMenuItemSelected";
                }
            }

        }

        if (this.scormApi.activeSCOIndex != null) {
            var organizationControlModeFlow = this.GetSequencingElementsAttributeValue(this.scormApi.activeSCOIndex, true, "controlMode", "flow", false);

            if (!organizationControlModeFlow) {
                if (BtnPrevious != null) {
                    BtnPrevious.style.cursor = "default";
                    BtnPrevious.classList.add("DimIcon");
                    BtnPrevious.disabled = true;
                }

                if (BtnContinue != null) {
                    BtnContinue.style.cursor = "default";
                    BtnContinue.classList.add("DimIcon");
                    BtnContinue.disabled = true;
                }
            }

            this.SetStopTraversalRule();
            this.ApplyConstrainChoice();
            this.ApplyPreventActivation();
            this.DisableTreeWithSingleActivity();
        }

    }
    catch (e) {
        this.scormApi.traceError("Error caught in CheckMenu ", e.toString(), 1);
        throw e;
    }
}

/**
* GetSameLevelActivities() 
* This Function is used to get all activities,those are at the same level as activity with itemIndex index value.
*/
Scorm2004ADLNavChoice.prototype.GetSameLevelActivities = function (itemIndex) {

    this.scormApi.DebuggerTraceCall('GetSameLevelActivities');
    try {
        var result = new Array();
        var Activity = null;
        if (isNullOrUndefined(itemIndex)) {
            return null;
        }
        else {
            Activity = this.scormApi.imsManifest.organizations[0].items[itemIndex];
        }

        for (var index = 0; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {
            if (Activity.level == this.scormApi.imsManifest.organizations[0].items[index].level) {
                result.push(this.scormApi.imsManifest.organizations[0].items[index]);
            }
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetSameLevelActivities", "", 1);
        throw e;
    }
}



/**
* ApplyConstrainChoice() 
* This Function is used to make activities disable for choice,whose parent prevent activation attribute is true.
*/
Scorm2004ADLNavChoice.prototype.ApplyConstrainChoice = function () {
    this.scormApi.DebuggerTraceCall('ApplyConstrainChoice');
    try {

        for (var index = 0; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {
            var constrainChoice = this.GetSequencingElementsAttributeValue(index, false, "adlseqConstrainedChoiceConsiderations", "constrainChoice", false);

            Activity = this.scormApi.imsManifest.organizations[0].items[index];
            var childrens = this.scormApi.courseDataModel[index].cmi.availableChildren;
            var flag = false;

            if (!this.IsLeafActivity(Activity.identifier)) {
                if (this.currentActivityIndex >= this.GetItemIndex(Activity.identifier) && this.currentActivityIndex <= this.GetItemIndex(childrens[childrens.length - 1])) {
                    flag = true;
                }
            }

            if (flag) {
                var logicallyNext = null;
                var logicallyPrevious = null;
                var LogicallyNextAndPrev = new Array();
                var identifierPrefix = "__leaf__";

                if (constrainChoice) {
                    var sameLevelActivities = this.GetSameLevelActivities(index);
                    for (var counter = 0; counter < sameLevelActivities.length; counter++) {
                        if (Activity.identifier == sameLevelActivities[counter].identifier) {
                            if (counter < sameLevelActivities.length - 1) {
                                logicallyNext = sameLevelActivities[counter + 1];
                                LogicallyNextAndPrev.push(logicallyNext);
                            }
                            LogicallyNextAndPrev.push(sameLevelActivities[counter]);

                            if (counter > 0) {
                                logicallyPrevious = sameLevelActivities[counter - 1];
                                LogicallyNextAndPrev.push(logicallyPrevious);
                            }
                        }
                    }

                    var allItem = this.scormApi.imsManifest.organizations[0].items;

                    for (var j = 0; j < allItem.length; j++) {

                        identifier = allItem[j].identifier;
                        var liId = document.getElementById((identifierPrefix + identifier));

                        if (liId != null) {
                            liId.style.cursor = "default";
                            liId.onclick = function () { return false; };
                        }
                    }

                    for (var k = 0; k < LogicallyNextAndPrev.length; k++) {
                        identifier = LogicallyNextAndPrev[k].identifier;
                        this.SetActivityEnable(identifier);
                    }

                }
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ApplyConstrainChoice ", e.toString(), 1);
        throw e;
    }
}


/**
* SetActivityEnable(identifier) 
* This Function is used to make activities disable for choice,whose parent prevent activation attribute is true.
* @param identifier          identifier that is to disable for choice navigation in activity tree along with descendents.
*/

Scorm2004ADLNavChoice.prototype.SetActivityEnable = function (identifier) {
    this.scormApi.DebuggerTraceCall('SetActivityEnable');
    var identifierPrefix = "__leaf__";
    var liId = document.getElementById((identifierPrefix + identifier));
    if (this.GetSequencingElementsAttributeValue(this.GetItemIndex(identifier), true, "controlMode", "choice", true)) {
        if (liId != null) {
            liId.className = "SCOTreeMenuItemEnabled";
            liId.style.cursor = "pointer";
            liId.onclick = function () {
                courseLaunchController.processLMSNavRequest("{target=" + this.id.replace("__leaf__", "") + "}choice");
            }
        }
    }
    if (!this.IsLeafActivity(identifier)) {
        var childrens = this.scormApi.courseDataModel[this.GetItemIndex(identifier)].cmi.availableChildren;
        for (var counter = 0; counter < childrens.length; counter++) {
            identifier = childrens[counter];
            this.SetActivityEnable(childrens[counter]);
        }
    }
}


/**
* ApplyPreventActivation() 
* This Function is used to make activities disable for choice,whose parent prevent activation attribute is true.
*/

Scorm2004ADLNavChoice.prototype.ApplyPreventActivation = function () {
    this.scormApi.DebuggerTraceCall('ApplyPreventActivation');
    try {

        for (index = 0; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {
            if (this.GetSequencingElementsAttributeValue(index, true, "controlMode", "choice", true)) {
                var preventActivation = this.GetSequencingElementsAttributeValue(index, false, "adlseqConstrainedChoiceConsiderations", "preventActivation", false);
                Activity = this.scormApi.imsManifest.organizations[0].items[index];
                var childrens = this.scormApi.courseDataModel[index].cmi.availableChildren;
                if (preventActivation) {
                    if (this.currentActivityIndex < this.GetItemIndex(Activity.identifier) || this.currentActivityIndex > this.GetItemIndex(childrens[childrens.length - 1])) {
                        identifierPrefix = "__leaf__";

                        for (counter = 0; counter < childrens.length; counter++) {

                            identifier = childrens[counter];;
                            var liId = document.getElementById((identifierPrefix + identifier));

                            if (liId != null) {
                                liId.style.cursor = "default";
                                liId.onclick = function () { return false; };
                            }

                        }
                    }
                }
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ApplyPreventActivation ", e.toString(), 1);
        throw e;
    }
}

/**
* SetStopTraversalRule() 
* This Function is used to make all next activities disable, if any item has StopTraversalRule attribute true.
*/

Scorm2004ADLNavChoice.prototype.SetStopTraversalRule = function () {
    this.scormApi.DebuggerTraceCall('SetStopTraversalRule');
    try {

        if (this.currentActivityIndex == -1 && this.scormApi.activeSCOIndex != null) {
            this.currentActivityIndex = this.scormApi.activeSCOIndex;
        }

        for (index = this.currentActivityIndex; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {
            if (this.GetSequencingElementsAttributeValue(index, true, "controlMode", "choice", true)) {
                var actions = this.ValidateSequencingRules(index, "preConditionRules");
                Activity = this.scormApi.imsManifest.organizations[0].items[index];

                if (actions.indexOf("stopForwardTraversal") > -1) {
                    identifierPrefix = "__leaf__";

                    for (counter = index; counter < this.scormApi.imsManifest.organizations[0].items.length; counter++) {

                        var currentItem = this.scormApi.imsManifest.organizations[0].items[counter];

                        if (currentItem != Activity) {

                            identifier = currentItem.identifier;

                            var liId = document.getElementById((identifierPrefix + identifier));

                            if (liId != null) {
                                liId.style.cursor = "default";
                                liId.onclick = function () { return false; };
                            }
                        }
                    }
                    break;
                }
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SetStopTraversalRule ", e.toString(), 1);
        throw e;
    }
}

/**
* GetItemIdentifier( itemIndex) 
* This Function is used to get the identifier name for the item index value passed as parameter.
* @param itemIndex              item index for which identifier name is to retrive.
*/
Scorm2004ADLNavChoice.prototype.GetItemIdentifier = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('GetItemIdentifier');
    var identifier = this.scormApi.imsManifest.organizations[0].items[itemIndex].identifier;
    return identifier;
}

/**
* GetItemIdentifierRef( itemIndex) 
* This Function is used to get the identifierref for the item index value passed as parameter.
* This is used to help us determine if the item is a leaf or non-leaf item.
* @param itemIndex              item index for which identifier name is to retrive.
*/
Scorm2004ADLNavChoice.prototype.GetItemIdentifierRef = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('GetItemIdentifierRef');
    var identifierref = this.scormApi.imsManifest.organizations[0].items[itemIndex].identifierref;
    return identifierref;
}

/**
* GetItemIdentifier( itemIndex) 
* This function will take the objective Id from the ImsManifest and returns the corresponding objective Index from the course data model.
* @param itemIndex              item index for which objective index value is to retrive.
* @param objectiveId            objectiveId for which index value is to retrive.
*/
Scorm2004ADLNavChoice.prototype.GetItemObjectivesIndexFromCourseDataModel = function (itemIndex, objectiveId) {
    this.scormApi.DebuggerTraceCall('GetItemObjectivesIndexFromCourseDataModel');
    try {
        var result = null;
        if (itemIndex == null || itemIndex == undefined) {
            return result;
        }

        var allobjectives = this.GetItemObjectivesFromCourseDataModel(itemIndex);
        for (index = 0; index < allobjectives.length; index++) {
            if (objectiveId == allobjectives[index].id.value) {
                return index;
            }
        }
        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetItemObjectivesIndexFromCourseDataModel", "", 1);
        throw e;
    }
}


Scorm2004ADLNavChoice.ItemOrOrganization = { Item: 'Item', Organization: 'Organization' };
/*
* GetCompletionThresholdAttributeValue() 
* This Function is used to retrive the attribute value of adlcpCompletionThreshold node from the imsManifest file for the activity or objective.
* @param scormApi               instance of the Scorm2004Api.js.
* @param attribute              is the attribute of which the value is to retrive.
* @param itemIndex              index of the activity on which measure rollup process to be applied.
*/
Scorm2004ADLNavChoice.prototype.GetCompletionThresholdAttributeValue = function (itemIndex, attribute, ItemOrOrganization) {
    this.scormApi.DebuggerTraceCall('GetCompletionThresholdAttributeValue');
    try {
        var result;
        if (ItemOrOrganization == Scorm2004ADLNavChoice.ItemOrOrganization.Item) {
            var adlcpCompletionThreshold = this.scormApi.imsManifest.organizations[0].items[itemIndex].adlcpCompletionThreshold;

            if (adlcpCompletionThreshold != null && adlcpCompletionThreshold[attribute] != null) {
                switch (attribute) {
                    case this.cEnum.AdlcpCompletionThreshold.CompletedByMeasure:
                        result = adlcpCompletionThreshold.completedByMeasure;
                        break;
                    case this.cEnum.AdlcpCompletionThreshold.MinProgressMeasure:
                        result = adlcpCompletionThreshold.minProgressMeasure;
                        break;
                    case this.cEnum.AdlcpCompletionThreshold.ProgressWeight:
                        result = adlcpCompletionThreshold.progressWeight;
                        break;
                }
            }
            else {
                switch (attribute) {
                    case this.cEnum.AdlcpCompletionThreshold.CompletedByMeasure:
                        result = false;
                        break;
                    case this.cEnum.AdlcpCompletionThreshold.MinProgressMeasure:
                        result = 1.0;
                        break;
                    case this.cEnum.AdlcpCompletionThreshold.ProgressWeight:
                        result = 1.0;
                        break;
                }
            }


        }

        else if (ItemOrOrganization == Scorm2004ADLNavChoice.ItemOrOrganization.Organization) {
            var adlcpCompletionThreshold = this.scormApi.imsManifest.organizations[0].adlcpCompletionThreshold;

            if (adlcpCompletionThreshold != null && adlcpCompletionThreshold[attribute] != null) {
                switch (attribute) {
                    case this.cEnum.AdlcpCompletionThreshold.CompletedByMeasure:
                        result = adlcpCompletionThreshold.completedByMeasure;
                        break;
                    case this.cEnum.AdlcpCompletionThreshold.MinProgressMeasure:
                        result = adlcpCompletionThreshold.minProgressMeasure;
                        break;
                    case this.cEnum.AdlcpCompletionThreshold.ProgressWeight:
                        result = adlcpCompletionThreshold.progressWeight;
                        break;
                }
            }
            else {
                switch (attribute) {
                    case this.cEnum.AdlcpCompletionThreshold.CompletedByMeasure:
                        result = false;
                        break;
                    case this.cEnum.AdlcpCompletionThreshold.MinProgressMeasure:
                        result = 1.0;
                        break;
                    case this.cEnum.AdlcpCompletionThreshold.ProgressWeight:
                        result = 1.0;
                        break;
                }
            }
        }
        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetCompletionThresholdAttributeValue", "", 1);
        throw e;
    }
}

/**
* GetSuspendedActivities(activityType)
* This function is used to get suspended activities(from all activities or from leaf activities)
* @param activityType                     activityType
*/
Scorm2004ADLNavChoice.prototype.GetSuspendedActivities = function (activityType) {
    this.scormApi.DebuggerTraceCall('GetSuspendedActivities');
    try {
        var result = new Array();
        for (var index = 0; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {

            var currentItem = this.scormApi.courseDataModel[index];
            var isLeaf = this.IsLeafActivity(currentItem.scoIdentifier);

            if (activityType == this.cEnum.ActivityType.all && currentItem.cmi.isSuspended.value) {
                result.push(currentItem);
            }
            else if (activityType == this.cEnum.ActivityType.leaf && isLeaf && currentItem.cmi.isSuspended.value) {
                result.push(currentItem);
                break;
            }
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetSuspendedActivities", "", 1);
        throw e;
    }
}


/**
* Rollup Rule Check Subprocess [RB.1.4]
* RollUpRuleCheckSubprocess(itemIndex, rollupAction, useDummyData)
* For an activity and a Rollup Action; returns True if the action applies
* @param itemIndex                     itemIndex
* @param rollupAction                  rollupAction
* @param useDummyData                  useDummyData
*/
Scorm2004ADLNavChoice.prototype.RollUpRuleCheckSubprocess = function (itemIndex, rollupAction, useDummyData) {
    this.scormApi.DebuggerTraceCall('RollUpRuleCheckSubprocess');
    try {
        var resultSet = new ExitConditionValues();
        var returnedResultSet = new ExitConditionValues();

        var rollupRules;
        if (useDummyData != null) {
            rollupRules = useDummyData;
        }
        else {
            rollupRules = this.GetItemRollUpRules(itemIndex);
        }

        var rollupRulesWithSpecifiedActions = new Array();
        var specifiedActionExists = false;
        if (rollupRules.length > 0) {
            for (var index = 0; index < rollupRules.length; index++) {
                if (rollupAction == rollupRules[index].rollupAction) {
                    rollupRulesWithSpecifiedActions.push(rollupRules[index]);
                    specifiedActionExists = true;
                }
            }
        }

        // pseudo code point 1
        if (specifiedActionExists) {

            // pseudo code point 1.1
            // already done above

            // pseudo code point 1.2
            for (var index = 0; index < rollupRulesWithSpecifiedActions.length; index++) {
                var contributingChildren = new Array();

                var availableChildren = this.scormApi.courseDataModel[itemIndex].cmi.availableChildren

                // pseudo code point 1.2.2
                for (var counter = 0; counter < availableChildren.length; counter++) {
                    var childIndex = this.GetItemIndex(availableChildren[counter]);

                    // pseudo code point 1.2.2.1
                    if (this.GetSequencingElementsAttributeValue(childIndex, false, "deliveryControls", "tracked", true)) {

                        // pseudo code point 1.2.2.1.1
                        var result1 = this.CheckChildForRollupSubprocess(childIndex, rollupAction);

                        // pseudo code point 1.2.2.1.2
                        if (result1) {

                            // pseudo code point 1.2.2.1.2.1
                            var result2 = this.EvaluateRollupConditionsSubprocess(childIndex, rollupRulesWithSpecifiedActions[index].conditionCombination, rollupRulesWithSpecifiedActions[index].rollupConditions);

                            // pseudo code point 1.2.2.1.2.2
                            if (result2 == "unknown") {
                                contributingChildren.push("unknown");
                            }

                                // pseudo code point 1.2.2.1.2.3
                            else {
                                // pseudo code point 1.2.2.1.2.3.1
                                if (result2) {
                                    contributingChildren.push(true);
                                }
                                    // pseudo code point 1.2.2.1.2.3.2
                                else {
                                    contributingChildren.push(false);
                                }
                            }
                        }

                    }
                }

                // pseudo code point 1.2.3
                var statusChange = false;

                // pseudo code point 1.2.4
                if (contributingChildren.length == 0) {
                    break;
                }

                var trueCountInArray = 0;
                for (var contributingChildrenItem in contributingChildren) {
                    if (contributingChildren[contributingChildrenItem] == true) {
                        trueCountInArray++;
                    }
                }

                // pseudo code point 1.2.5
                if (rollupRulesWithSpecifiedActions[index].childActivitySet == "all") {
                    if (contributingChildren.indexOf(false) < 0 && contributingChildren.indexOf("unknown") < 0) {
                        statusChange = true;
                    }
                }

                    // pseudo code point 1.2.6
                else if (rollupRulesWithSpecifiedActions[index].childActivitySet == "any") {
                    if (contributingChildren.indexOf(true) > -1) {
                        statusChange = true;
                    }
                }

                    // pseudo code point 1.2.7
                else if (rollupRulesWithSpecifiedActions[index].childActivitySet == "none") {
                    if (contributingChildren.indexOf(true) < 0 && contributingChildren.indexOf("unknown") < 0) {
                        statusChange = true;
                    }
                }

                    // pseudo code point 1.2.8
                else if (rollupRulesWithSpecifiedActions[index].childActivitySet == "atLeastCount") {
                    if (trueCountInArray >= rollupRulesWithSpecifiedActions[index].minimumCount) {
                        statusChange = true;
                    }

                }

                    // pseudo code point 1.2.9
                else if (rollupRulesWithSpecifiedActions[index].childActivitySet == "atLeastPercent") {
                    if ((trueCountInArray / contributingChildren.length) >= rollupRulesWithSpecifiedActions[index].minimumPercent) {
                        statusChange = true;
                    }
                }

                // pseudo code point 1.2.10
                if (statusChange) {
                    return true;
                }

            }
        }

        // pseudo code point 2
        return false;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in RollUpRuleCheckSubprocess", "", 1);
        throw e;
    }
}

/**
* Check Child for Rollup Subprocess [RB.1.4.2]
* CheckChildForRollupSubprocess(itemIndex, rollupAction) 
* For an activity and a Rollup Action; returns True if the activity is included in rollup
* @param itemIndex                     itemIndex
* @param rollupAction                  rollupAction
*/
Scorm2004ADLNavChoice.prototype.CheckChildForRollupSubprocess = function (itemIndex, rollupAction) {
    this.scormApi.DebuggerTraceCall('CheckChildForRollupSubprocess');
    try {
        // pseudo code point 1
        var included = false;

        // pseudo code point 2
        if (rollupAction == "satisfied" || rollupAction == "notSatisfied") {

            // pseudo code point 2.1
            if (this.GetSequencingElementsAttributeValue(itemIndex, false, "rollupRules", "rollupObjectiveSatisfied", true)) {

                // pseudo code point 2.1.1
                included = true;

                // pseudo code point 2.1.2
                if ((rollupAction == "satisfied" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForSatisfied", "always") == "ifNotSuspended")
                || (rollupAction == "notSatisfied" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForNotSatisfied", "always") == "ifNotSuspended")) {

                    // pseudo code point 2.1.2.1
                    if (!this.scormApi.courseDataModel[itemIndex].cmi.activityProgressStatus.value || (this.scormApi.courseDataModel[itemIndex].cmi.activityAttemptCount.value > 0 && this.scormApi.courseDataModel[itemIndex].cmi.isSuspended.value)) {
                        included = false;
                    }
                }
                    // pseudo code point 2.1.3
                else {

                    // pseudo code point 2.1.3.1
                    if ((rollupAction == "satisfied" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForSatisfied", "always") == "ifAttempted")
                || (rollupAction == "notSatisfied" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForNotSatisfied", "always") == "ifAttempted")) {

                        // pseudo code point 2.1.3.1
                        if (!this.scormApi.courseDataModel[itemIndex].cmi.activityProgressStatus.value || this.scormApi.courseDataModel[itemIndex].cmi.activityAttemptCount.value == 0) {
                            included = false;
                        }
                    }
                        // pseudo code point 2.1.3.2
                    else {

                        // pseudo code point 2.1.3.2.1
                        if ((rollupAction == "satisfied" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForSatisfied", "always") == "ifNotSkipped")
                        || (rollupAction == "notSatisfied" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForNotSatisfied", "always") == "ifNotSkipped")) {

                            // pseudo code point 2.1.3.2.1.1
                            var returnedResultSet = this.SequencingRulesCheckProcess(itemIndex, [this.cEnum.SequencingConditionRules.PreConditionRuleAction.Skip], this.cEnum.SequencingConditionRules.PreConditionRule);

                            // pseudo code point 2.1.3.2.1.2
                            if (returnedResultSet.action != null) {
                                included = false;
                            }
                        }
                    }
                }
            }
        }

        // pseudo code point 3
        if (rollupAction == "completed" || rollupAction == "incomplete") {

            // pseudo code point 3.1
            if (this.GetSequencingElementsAttributeValue(itemIndex, false, "rollupRules", "rollupProgressCompletion", true)) {

                // pseudo code point 3.1.1
                included = true;

                // pseudo code point 3.1.2
                if ((rollupAction == "completed" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForCompleted", "always") == "ifNotSuspended")
                || (rollupAction == "incomplete" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForIncomplete", "always") == "ifNotSuspended")) {

                    // pseudo code point 3.1.2.1
                    if (!this.scormApi.courseDataModel[itemIndex].cmi.activityProgressStatus.value || (this.scormApi.courseDataModel[itemIndex].cmi.activityAttemptCount.value > 0 && this.scormApi.courseDataModel[itemIndex].cmi.isSuspended.value)) {
                        included = false;
                    }
                }
                    // pseudo code point 3.1.3
                else {

                    // pseudo code point 3.1.3.1
                    if ((rollupAction == "completed" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForCompleted", "always") == "ifAttempted")
                    || (rollupAction == "incomplete" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForIncomplete", "always") == "ifAttempted")) {

                        // pseudo code point 3.1.3.1.1
                        if (!this.scormApi.courseDataModel[itemIndex].cmi.activityProgressStatus.value || this.scormApi.courseDataModel[itemIndex].cmi.activityAttemptCount.value == 0) {
                            included = false;
                        }
                    }
                        // pseudo code point 3.1.3.2
                    else {

                        // pseudo code point 3.1.3.2.1
                        if ((rollupAction == "completed" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForCompleted", "always") == "ifNotSkipped")
                        || (rollupAction == "incomplete" && this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "requiredForIncomplete", "always") == "ifNotSkipped")) {

                            // pseudo code point 3.1.3.2.1.1
                            var returnedResultSet = this.SequencingRulesCheckProcess(itemIndex, [this.cEnum.SequencingConditionRules.PreConditionRuleAction.Skip], this.cEnum.SequencingConditionRules.PreConditionRule);

                            // pseudo code point 3.1.3.2.1.2
                            if (returnedResultSet.action != null) {
                                included = false;
                            }
                        }
                    }
                }
            }
        }

        // pseudo code point 4
        return included;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in CheckChildForRollupSubprocess", "", 1);
        throw e;
    }
}

/**
* Evaluate Rollup Conditions Subprocess [RB.1.4.1]
* EvaluateRollupConditionsSubprocess(itemIndex, conditionCombination, rollupConditions) 
* This pseudo code evaluates the roll up condition
* @param itemIndex                     itemIndex
* @param conditionCombination          conditionCombination
* @param rollupConditions              rollupConditions
*/
Scorm2004ADLNavChoice.prototype.EvaluateRollupConditionsSubprocess = function (itemIndex, conditionCombination, rollupConditions) {

    this.scormApi.DebuggerTraceCall('EvaluateRollupConditionsSubprocess');
    try {
        // pseudo code point 1
        var rollupConditionBag = new Array();

        // pseudo code point 2
        for (var index = 0; index < rollupConditions.length; index++) {

            // pseudo code point 2.1
            var result = this.EvaluateRuleCondition(itemIndex, "", rollupConditions[index].condition);

            // pseudo code point 2.2
            if (rollupConditions[index].condition != null && rollupConditions[index].conditionOperator == "not" && typeof result == "boolean") {
                result = !result;
            }

            // pseudo code point 2.3
            rollupConditionBag.push(result);

        }

        // pseudo code point 3
        if (rollupConditionBag.length == 0) {
            return "unknown";
        }

        var result = false;

        var trueCountInArray = 0;
        for (var index in rollupConditionBag) {
            if (rollupConditionBag[index] == true) {
                trueCountInArray++;
            }
        }

        // pseudo code point 4
        if (conditionCombination == "all") {
            if (trueCountInArray == rollupConditionBag.length) {
                result = true;
            }
            else if (rollupConditionBag.indexOf("unknown") > -1) {
                result = "unknown";
            }
            else {
                result = false;
            }
        }
        else if (conditionCombination == "any") {
            result = true;
            if (trueCountInArray > 0) {
                result = true;
            }
            else if (rollupConditionBag.indexOf("unknown") > -1) {
                result = "unknown";
            }
            else {
                result = false;
            }
        }

        // pseudo code point 5
        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in EvaluateRollupConditionsSubprocess", "", 1);
        throw e;
    }
}

/**
* GetItemRollUpRules(itemIndex) 
* This function is used to fetch roll up rules of an item
* @param itemIndex              itemIndex
*/
Scorm2004ADLNavChoice.prototype.GetItemRollUpRules = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('GetItemRollUpRules');
    try {
        var result = new Array();
        var imsssSequencing = this.scormApi.imsManifest.organizations[0].items[itemIndex].imsssSequencing;

        if (imsssSequencing != null) {

            var rollupRulesItem = imsssSequencing.rollupRules;

            if (rollupRulesItem != null) {
                var rollupRules = rollupRulesItem.rollupRules;

                if (rollupRules != null && rollupRules.length > 0) {
                    for (var index = 0; index < rollupRules.length; index++) {
                        result.push(rollupRules[index]);
                    }
                }
            }

            if (imsssSequencing["idRef"] != null) {
                var rollupRulesItem = this.FetchSequencingAttributeFromSubnode(imsssSequencing, "rollupRules");

                if (rollupRulesItem != null) {
                    var rollupRules = rollupRulesItem.rollupRules;

                    if (rollupRules != null && rollupRules.length > 0) {
                        for (var index = 0; index < rollupRules.length; index++) {
                            result.push(rollupRules[index]);
                        }
                    }
                }
            }
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetItemRollUpRules", "", 1);
        throw e;
    }
}

/**
* GetRolledupObjectiveForItem(itemIndex) 
* This function will provide the rolled up objective of an item
* @param itemIndex              itemIndex
*/
Scorm2004ADLNavChoice.prototype.GetRolledupObjectiveForItem = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('GetRolledupObjectiveForItem');
    try {

        var itemObjectives = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);

        for (var index = 0; index < itemObjectives.length; index++) {
            if (itemObjectives[index].objectiveContributesToRollup) {
                return itemObjectives[index];
            }
        }
        return null;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetRolledupObjectiveForItem", "", 1);
        throw e;
    }
}

/**
* SetIsActiveToTrueForActivityProvidedAndFalseForOthers( itemIndex) 
* This function will update the isActive status to true of an activity and all its ancestors and false for all others
* @param itemIndex              itemIndex
*/
Scorm2004ADLNavChoice.prototype.SetIsActiveToTrueForActivityProvidedAndFalseForOthers = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('SetIsActiveToTrueForActivityProvidedAndFalseForOthers');
    try {
        var activeItemHierarchy = this.GetItemNamesHierarchyFromRoot(itemIndex, 0, true, true, false);

        for (var index = 0; index < this.scormApi.imsManifest.organizations[0].items.length; index++) {

            if (activeItemHierarchy.indexOf(this.scormApi.imsManifest.organizations[0].items[index].identifier) > -1) {
                this.scormApi.courseDataModel[index].cmi.isActive.value = true;
            }
            else {
                this.scormApi.courseDataModel[index].cmi.isActive.value = false;
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SetIsActiveToTrueForActivityProvidedAndFalseForOthers", "", 1);
        throw e;
    }
}


/**
* Activity Progress Rollup Using Measure Process [RB.1.3 a]- P.No.  192 
* ActivityProgressRollupUsingMeasureProcess( itemIndex) 
* This function will take the itemIndex from the ImsManifest and returns the corresponding objective Index from the course data model.
* @param itemIndex              item index for which objective index value is to retrive.
*/
Scorm2004ADLNavChoice.prototype.ActivityProgressRollupUsingMeasureProcess = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('ActivityProgressRollupUsingMeasureProcess');
    try {
        // 1. set Attempt Progress Status to False
        this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, false);

        // 2. set Attempt Completion Status to False
        this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, false);

        // 3. if adlcp:completedbyMeasure for the target activity is True Then
        var completedByMeasure = this.GetCompletionThresholdAttributeValue(itemIndex, this.cEnum.AdlcpCompletionThreshold.CompletedByMeasure, Scorm2004ADLNavChoice.ItemOrOrganization.Item);
        if (completedByMeasure) {
            // 3.1. if the Attempt Completion Amount Status is False Then
            if (!this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmountStatus)) {
                // 3.1.1. set the Attempt Completion Status to False
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, false);
            }
                // else
            else {
                var minProgressMeasure = this.GetCompletionThresholdAttributeValue(itemIndex, this.cEnum.AdlcpCompletionThreshold.MinProgressMeasure, Scorm2004ADLNavChoice.ItemOrOrganization.Item);

                var attemptCompletionAmount = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmount);
                // 3.2.1. if the Attempt Completion Amount is greater than or equal (>=) to the adlcp:minProgressMeasure Then

                if (attemptCompletionAmount >= minProgressMeasure) {
                    // 3.2.1.1. set the Attempt Progress Status True
                    this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, true);

                    // 3.2.1.2. set the Attempt Completion Status to True
                    this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, true);
                }
                    // 3.2.2. else
                else {
                    // 3.2.2.1. set the Attempt Progress Status True
                    this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, true);

                    // 3.2.2.2. set the Attempt Completion Status to False
                    this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, false);

                    // end If
                }
            }
        }
            // 4. else
        else {
            // 4.1. set the Attempt Progress Status False
            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, false);

            // end If
        }
        // 5. exit Activity Progress Rollup Using Measure Process
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ActivityProgressRollupUsingMeasureProcess", "", 1);
        throw e;
    }
}


/**
* Activity Progress Rollup Using Rules Process [RB.1.3 b]- P.No.  193 
* ActivityProgressRollupUsingRulesProcess( itemIndex) 
* This function will take the itemIndex of activity from the ImsManifest and may change the Attempt Information for the activity:.
* @param itemIndex              item index of activity from the ImsManifest.
*/
Scorm2004ADLNavChoice.prototype.ActivityProgressRollupUsingRulesProcess = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('ActivityProgressRollupUsingRulesProcess');
    try {

        var completedActionExists = this.RollupActionExists(itemIndex, this.cEnum.RollupAction.Completed);
        var incompleteActionExists = this.RollupActionExists(itemIndex, this.cEnum.RollupAction.Incomplete);
        var completedActionRollupRule = null;
        var incompleteActionRollupRule = null;

        // 1. if the activity does not include Rollup Rules with the Incomplete rollup action And the activity does not include Rollup Rules with the Completed rollup action Then    
        if (!completedActionExists && !incompleteActionExists) {

            // apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Completed; and a Rollup Action of Completed
            completedActionRollupRule = new RollupRuleDataModel();
            completedActionRollupRule.childActivitySet = "all";
            completedActionRollupRule.rollupConditions = new Array();
            var rollupCondition = new RollupConditionDataModel();
            rollupCondition.condition = "completed";
            completedActionRollupRule.rollupConditions.push(rollupCondition);
            completedActionRollupRule.rollupAction = "completed";

            // apply a Rollup Rule to the activity with a Rollup Child Activity Set of All; a Rollup Condition of Activity Progress Known; and a Rollup Action of Incomplete
            incompleteActionRollupRule = new RollupRuleDataModel();
            incompleteActionRollupRule.childActivitySet = "all";
            incompleteActionRollupRule.rollupConditions = new Array();
            var rollupCondition = new RollupConditionDataModel();
            rollupCondition.condition = "activityProgressKnown";
            incompleteActionRollupRule.rollupConditions.push(rollupCondition);
            incompleteActionRollupRule.rollupAction = "incomplete";

        }

        // 2. apply the Rollup Rule Check Subprocess to the activity and the Incomplete rollup action
        var rollupRules = null;
        if (incompleteActionRollupRule != null) {
            rollupRules = new Array();
            rollupRules.push(incompleteActionRollupRule);
        }
        var result = this.RollUpRuleCheckSubprocess(itemIndex, "incomplete", rollupRules);

        // 3. if the Rollup Rule Check Subprocess returned True Then
        if (result) {
            // 3.1. set the Attempt Progress Status for the activity to True
            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, true);

            // 3.2. set the Attempt Completion Status for the activity to False
            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, false);

        }

        // 4. apply the Rollup Rule Check Subprocess to the activity and the Completed rollup action
        var rollupRules = null;
        if (completedActionRollupRule != null) {
            rollupRules = new Array();
            rollupRules.push(completedActionRollupRule);
        }
        var result = this.RollUpRuleCheckSubprocess(itemIndex, "completed", rollupRules);

        // 5. if the Rollup Rule Check Subprocess returned True Then
        if (result) {
            // 5.1. set the Attempt Progress Status for the activity to True
            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, true);

            // 5.2. set the Attempt Completion Status for the activity to True
            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, true);
        }
        // 6. exit Activity Progress Rollup Process
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ActivityProgressRollupUsingRulesProcess", "", 1);
        throw e;
    }
}

/**
* RollupActionExists(itemIndex,rollupAction)
* This function will return whether the roll up action is complete or incomplete
* @param itemIndex              itemIndex
* @param rollupAction           rollupAction
*/
Scorm2004ADLNavChoice.prototype.RollupActionExists = function (itemIndex, rollupAction) {
    this.scormApi.DebuggerTraceCall('RollupActionExists');
    try {
        var result = false;
        var imsssSequencing = this.scormApi.imsManifest.organizations[0].items[itemIndex].imsssSequencing;

        if (imsssSequencing != null) {

            var rollupRulesItem = imsssSequencing.rollupRules;

            if (rollupRulesItem != null) {
                var rollupRules = rollupRulesItem.rollupRules;

                if (rollupRules != null && rollupRules.length > 0) {
                    for (var index = 0; index < rollupRules.length; index++) {
                        if (rollupRules[index].rollupAction == rollupAction) {
                            result = true;
                            break;
                        }
                    }
                }
            }

            if (imsssSequencing["idRef"] != null) {

                var rollupRulesItem = this.FetchSequencingAttributeFromSubnode(imsssSequencing, "rollupRules");

                if (rollupRulesItem != null) {
                    var rollupRules = rollupRulesItem.rollupRules;

                    if (rollupRules != null && rollupRules.length > 0) {
                        for (var index = 0; index < rollupRules.length; index++) {
                            if (rollupRules[index].rollupAction == rollupAction) {
                                result = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in RollupActionExists", "", 1);
        throw e;
    }
}


function RollupRuleDataModel() {
    this.childActivitySet = "all";
    this.conditionCombination = "any";
    this.rollupAction = null;
    this.rollupConditions = null;
}

function RollupConditionDataModel() {
    this.condition = null;
    this.conditionOperator = "noOp";
}



/**
* SetGlobalactualAttemptCount( defaultvalue) 
* This function to update  actualAttemptCount value based on overall activity tree completion status
* @param defaultvalue            default value  .
*/
Scorm2004ADLNavChoice.prototype.SetGlobalActualAttemptCount = function (defaultvalue) {
    
    this.scormApi.DebuggerTraceCall('SetGlobalActualAttemptCount');

    try {
        var returnVal = defaultvalue;        
        var count = this.scormApi.courseDataModel.length;

        if (count > 0) {
            for (var index = 0; index < this.scormApi.courseDataModel.length; index++) {
                var pc = this.scormApi.courseDataModel[index].cmi;

                if (pc.activityAttemptCount.value > returnVal) {
                    returnVal = pc.activityAttemptCount.value;
                }
            }
        }

        return returnVal;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SetGlobalActualAttemptCount", "", 1);
        throw e;
    }
}


/**
* SetGlobaleffectiveAttemptCount( defaultvalue) 
* This function to update  actualAttemptCount value based on overall activity tree completion status
* @param defaultvalue            default value  .
*/
Scorm2004ADLNavChoice.prototype.SetGlobalEffectiveAttemptCount = function (defaultvalue) {
    this.scormApi.DebuggerTraceCall('SetGlobalEffectiveAttemptCount');

    try {
        var returnVal = defaultvalue;
        var count = this.scormApi.courseDataModel.length;

        if (count > 0) {
            for (var index = 0; index < this.scormApi.courseDataModel.length; index++) {
                var pc = this.scormApi.courseDataModel[index].cmi;

                if (pc.activityEffectiveAttemptCount.value > returnVal) {
                    returnVal = pc.activityEffectiveAttemptCount.value;
                }                
            }
        }

        return returnVal;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SetGlobalEffectiveAttemptCount", "", 1);
        throw e;
    }
}

/**
* UpdateActivitiesStatus() 
* This function is used to update the activities status of all parents in the activity tree as per status of their respective child activities
*/
Scorm2004ADLNavChoice.prototype.UpdateActivitiesStatus = function () {
    this.scormApi.DebuggerTraceCall('UpdateActivitiesStatus');
    try {

        var items = this.scormApi.imsManifest.organizations[0].items;
        var courseDataModel = this.scormApi.courseDataModel;

        if (this.currentActivity == null || this.currentActivity == undefined) {
            return;
        }

        for (var index = 0; index < items.length; index++) {

            courseDataModel[index].cmi.success_status.value = this.ComputeSuccessStatus(index);
            courseDataModel[index].cmi.completion_status.value = this.ComputeCompletionStatus(index);
        }
        this.UpdateStateDisplay();
    }
    catch (e) {
        this.scormApi.traceError("Error caught in UpdateActivitiesStatus", "", 1);
        throw e;
    }
}

/**
 * Method is used to update the activity status and mark it checked or unchecked according to 
 * Its completion or success status.
 */
Scorm2004ADLNavChoice.prototype.UpdateStateDisplay = function () {
    this.scormApi.DebuggerTraceCall('UpdateStateDisplay');
    try {
        for (var itemIndexCounter = this.scormApi.courseDataModel.length - 1; itemIndexCounter >= 0; itemIndexCounter--) {
            this.OverallRollupProcess(itemIndexCounter);
        }
        // check if activeSCOIndex exists then only call fillGlobalModelValues
        // becuase for some content packages, no launchable activity is set at the time of fresh launch
        if (!isNullOrUndefined(this.scormApi.activeSCOIndex)) {
            //update the over all package attribute values
            this.scormApi.fillGlobalModelValues();
        }

        var items = this.scormApi.imsManifest.organizations[0].items;
        var courseDataModel = this.scormApi.courseDataModel;
        var prefix_img_status = "img_status_";

        for (var index = 0; index < courseDataModel.length; index++) {

            var scoIdentifier = courseDataModel[index].scoIdentifier;
            var chkBox_status = document.getElementById((prefix_img_status + scoIdentifier));

            var completion_status = this.ComputeCompletionStatus(index);
            var success_status = this.ComputeSuccessStatus(index)

            if (chkBox_status != null) {

                chkBox_status.classList.add("DimIcon");

                if (success_status == "passed") {
                    // remove "DimIcon" class for "completed style"
                    chkBox_status.classList.remove("DimIcon");

                    chkBox_status.title = "passed";
                }
                else if (success_status == "failed") {
                    chkBox_status.title = "failed";
                }
                else if (completion_status == "completed") {
                    // remove "DimIcon" class for "completed style"
                    chkBox_status.classList.remove("DimIcon");

                    chkBox_status.title = "completed";
                }
                else if (completion_status == "incomplete") {
                    chkBox_status.title = "incomplete";
                }
                else {
                    chkBox_status.title = "unknown";
                }
            }
        }

        // if the package is single sco package then update package status as completed/passed
        //this.UpdateSingleSCOorganizationStatus();
    }
    catch (e) {
        this.scormApi.traceError("Error caught in UpdateStateDisplay", "", 1);
        throw e;
    }
}

/**
* UpdateOrganizationStatus() 
* This function is used to set the status of the organization as completed/passed.
*/
Scorm2004ADLNavChoice.prototype.UpdateSingleSCOorganizationStatus = function () {

    this.scormApi.DebuggerTraceCall('UpdateSingleSCOorganizationStatus');
    try {
        // no  of nodes in the activity tree
        var itemsLength = this.scormApi.imsManifest.organizations[0].items.length;

        if (itemsLength == 2) {
            // prefix
            var prefix_img_status = "img_status_";

            var courseDataModel = this.scormApi.courseDataModel;
            var scoIdentifier = courseDataModel[0].scoIdentifier;

            var chkBox_status = document.getElementById((prefix_img_status + scoIdentifier));

            if (chkBox_status != null) {

                chkBox_status.checked = false;
                var completion_status = this.ComputeCompletionStatus(1);
                var success_status = this.ComputeSuccessStatus(1);

                if (completion_status == "completed") {
                    chkBox_status.checked = true;
                    chkBox_status.title = "completed";
                }

                if (success_status == "passed") {
                    chkBox_status.checked = true;
                    chkBox_status.title = "passed";
                }
            }
        }
    } catch (e) {
        this.scormApi.traceError("Error caught in UpdateSingleSCOorganizationStatus", "", 1);
        throw e;
    }
}


Scorm2004ADLNavChoice.prototype.ComputeSuccessStatus = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('ComputeSuccessStatus');
    try {

        // this if condition is to set success status for root of single SCO package 
        // as a single SCO package has length of, two first is root and second is leaf activity.
        // if (this.scormApi.imsManifest.organizations[0].items.length == 2) {
        //    return this.scormApi.courseDataModel[1].cmi.success_status.value;
        //}

        var itemCmi = this.GetItemCmi(itemIndex);

        if (itemCmi.successStatusChangedDuringRuntime.value == false) {
            var isSatisfied = this.EvaluateRuleCondition(itemIndex, null, "satisfied");

            if (typeof isSatisfied == "boolean" && isSatisfied) {
                return "passed";
            }
            else if (typeof isSatisfied == "boolean" && !isSatisfied) {
                return "failed";
            }
            else {
                return "unknown";
            }
        }
        else {
            return itemCmi.success_status.value;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ComputeSuccessStatus", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.ComputeCompletionStatus = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('ComputeCompletionStatus');
    try {
        // status for root activity of single SCO package, length 2 dipicts first item as root/organization with one only activity
        // if (this.scormApi.imsManifest.organizations[0].items.length == 2) {
        //    return this.scormApi.courseDataModel[1].cmi.completion_status.value;
        //}

        var itemCmi = this.GetItemCmi(itemIndex);

        if (itemCmi.completionStatusChangedDuringRuntime.value == false) {

            var completedValue = this.EvaluateRuleCondition(itemIndex, null, "completed");

            if (typeof completedValue == "boolean" && completedValue) {
                return "completed";
            }
            else if (typeof completedValue == "boolean" && !completedValue) {
                return "incomplete";
            }
            else {
                return "unknown";
            }
        }
        else {
            return itemCmi.completion_status.value;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ComputeCompletionStatus", "", 1);
        throw e;
    }
}

/**
* GetItemLevel(itemIndex) 
* This function is used to get the level of the item
* @param itemIndex            itemIndex
*/
Scorm2004ADLNavChoice.prototype.GetItemLevel = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('GetItemLevel');
    try {
        return this.scormApi.imsManifest.organizations[0].items[itemIndex].level;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetItemLevel", "", 1);
        throw e;
    }
}

/**
* SetCurrentActivity(itemIdentifier) 
* This function is used to set currentActivity and currentActivityIndex
* @param itemIdentifier            itemIdentifier
*/
Scorm2004ADLNavChoice.prototype.SetCurrentActivity = function (itemIdentifier) {
    this.scormApi.DebuggerTraceCall('SetCurrentActivity');
    try {
        this.currentActivity = itemIdentifier;
        if (itemIdentifier == null) {
            this.currentActivityIndex = -1;
        }
        else {
            this.currentActivityIndex = this.GetItemIndex(itemIdentifier);
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SetCurrentActivity", "", 1);
        throw e;
    }
}

/**
* SetSuspendedActivity(itemIdentifier) 
* This function is used to set suspendedActivity and suspendedActivityIndex
* @param itemIdentifier            itemIdentifier
*/
Scorm2004ADLNavChoice.prototype.SetSuspendedActivity = function (itemIdentifier) {
    this.scormApi.DebuggerTraceCall('SetSuspendedActivity');
    try {
        this.suspendedActivity = itemIdentifier;

        if (itemIdentifier == null) {
            this.suspendedActivityIndex = -1;
        }
        else {
            this.suspendedActivityIndex = this.GetItemIndex(itemIdentifier);
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SetSuspendedActivity", "", 1);
        throw e;
    }
}

/**
* SetGlobalGrandTotalTime( defaultvalue) 
* This function to update  total_time value based on overall activity tree total_time.
* @param defaultvalue            default value  .
*/
Scorm2004ADLNavChoice.prototype.SetGlobalGrandTotalTime = function (defaultvalue) {
    this.scormApi.DebuggerTraceCall('SetGlobalGrandTotalTime');
    try {
        defaultvalue = defaultvalue + this.scormApi.getTotalTimeInSeconds();
        return defaultvalue;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SetGlobalGrandTotalTime", "", 1);
        throw e;
    }
}

/**
* CheckForFlow() 
* This function will return the control mode flow value.
*/

Scorm2004ADLNavChoice.prototype.CheckForFlow = function () {
    this.scormApi.DebuggerTraceCall('CheckForFlow');
    var organizationControlModeFlow = this.GetSequencingElementsAttributeValue(0, false, "controlMode", "flow", false);
    var items = this.scormApi.imsManifest.organizations[0].items;

    if (organizationControlModeFlow || items.length == 2) {
        return true;
    }
    else {
        return false;
    }
}


/**
* GetGlobalObjective(objectiveId) 
* This Function is used to get Global Objective with a specific id
* @param objectiveId            instance of the Scorm2004Api.js.
*/
Scorm2004ADLNavChoice.prototype.GetGlobalObjective = function (objectiveId) {

    this.scormApi.DebuggerTraceCall('GetGlobalObjective');
    try {
        for (var index = 0; index < this.scormApi.globalObjectives.length; index++) {
            if (this.scormApi.globalObjectives[index].id.value == objectiveId) {
                return this.scormApi.globalObjectives[index];
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetGlobalObjective", "", 1);
        throw e;
    }
}


/**
* InitializeObjectives(itemIndex)
* This Function is used to get Global Objective with a specific id
* @param activity            activity for which objectives to be initialized
*/
Scorm2004ADLNavChoice.prototype.InitializeObjectives = function (itemIndex) {

    this.scormApi.DebuggerTraceCall('InitializeObjectives');
    try {

        var itemObjectives = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);

        for (var index = 0; index < itemObjectives.length; index++) {

            var mapInfo = itemObjectives[index].mapInfo;

            this.FillObjectiveValue(itemIndex, index, mapInfo);

            var adlSeqObjective = this.GetAdlSeqObjectiveForId(itemIndex, itemObjectives[index].objectiveId);
            if (adlSeqObjective != null) {
                mapInfo = adlSeqObjective.mapInfo;
                this.FillObjectiveValue(itemIndex, index, mapInfo);
            }
        }

    }
    catch (e) {
        this.scormApi.traceError("Error caught in InitializeObjectives", "", 1);
        throw e;
    }
}


Scorm2004ADLNavChoice.prototype.FillObjectiveValue = function (itemIndex, objectiveIndex, mapInfo) {
    this.scormApi.DebuggerTraceCall('FillObjectiveValue');
    try {

        var primaryObjective = this.GetPrimaryObjectiveFromCDM(itemIndex);
        var IsSettingPrimaryObjective = false;

        if (primaryObjective != null && objectiveIndex == 0) {
            IsSettingPrimaryObjective = true;
        }

        if (mapInfo != null && mapInfo.length > 0) {

            for (var counter = 0; counter < mapInfo.length; counter++) {

                var globalObjective = this.GetGlobalObjective(mapInfo[counter].targetObjectiveId);

                // table 4.9.2a Scorm SN book

                // 1
                this.scormApi.InitilzeObjectiveIfNotInitialized(itemIndex);

                var courseDataModelObjective = this.scormApi.courseDataModel[itemIndex].cmi.objectives.__array[objectiveIndex];

                // 2 read Satisfied Status
                if (mapInfo[counter].readSatisfiedStatus != undefined && mapInfo[counter].readSatisfiedStatus == true) {
                    var successStatus;
                    if (globalObjective.objectiveProgressStatus.value && !globalObjective.objectiveSatisfiedStatus.value) {
                        successStatus = "failed";
                    }
                    else if (globalObjective.objectiveProgressStatus.value && globalObjective.objectiveSatisfiedStatus.value) {
                        successStatus = "passed";
                    }
                    else {
                        successStatus = "unknown";
                    }
                    if (successStatus != null && successStatus != undefined) {
                        courseDataModelObjective.success_status.initialize(successStatus);

                        if (IsSettingPrimaryObjective) {
                            this.scormApi.courseDataModel[itemIndex].cmi.success_status.initialize(successStatus);
                        }
                    }
                }

                // 3
                if (mapInfo[counter].readNormalizedMeasure != undefined && mapInfo[counter].readNormalizedMeasure == true) {
                    if (globalObjective.objectiveMeasureStatus.value) {
                        var value = globalObjective.objectiveNormalizedMeasure.value;
                        courseDataModelObjective.score.scaled.initialize(value);

                        if (IsSettingPrimaryObjective) {
                            this.scormApi.courseDataModel[itemIndex].cmi.score.scaled.initialize(value);
                        }
                    }
                }
                // 4 read Completion Status
                if (mapInfo[counter].readCompletionStatus != undefined && mapInfo[counter].readCompletionStatus == true) {

                    var successStatus;
                    if (globalObjective.attemptProgressStatus.value && !globalObjective.attemptCompletionStatus.value) {
                        successStatus = "incomplete";
                    }
                    else if (globalObjective.attemptProgressStatus.value && globalObjective.attemptCompletionStatus.value) {
                        successStatus = "completed";
                    }
                    else {
                        successStatus = "unknown";
                    }
                    courseDataModelObjective.completion_status.initialize(successStatus);
                    courseDataModelObjective.attemptProgressStatus.initialize(globalObjective.attemptProgressStatus.value);
                    courseDataModelObjective.attemptCompletionStatus.initialize(globalObjective.attemptCompletionStatus.value);

                    if (IsSettingPrimaryObjective) {
                        this.scormApi.courseDataModel[itemIndex].cmi.completion_status.initialize(successStatus);
                        //this.scormApi.courseDataModel[itemIndex].cmi.attemptProgressStatus.initialize(globalObjective.attemptProgressStatus.value);
                        this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, globalObjective.attemptProgressStatus.value);
                        //this.scormApi.courseDataModel[itemIndex].cmi.attemptCompletionStatus.initialize(globalObjective.attemptCompletionStatus.value);
                        this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, globalObjective.attemptCompletionStatus.value);

                    }
                }
                // 5 read Progress Measure
                if (mapInfo[counter].readProgressMeasure != undefined && mapInfo[counter].readProgressMeasure == true) {

                    if (!isNullOrUndefined(globalObjective.attemptCompletionAmount.value)) {

                        var value = globalObjective.attemptCompletionAmount.value;
                        courseDataModelObjective.progress_measure.initialize(value);
                        // courseDataModelObjective.completion_status.value = value;
                    }
                }
                // read Raw Score 

                if (mapInfo[counter].readRawScore != undefined && mapInfo[counter].readRawScore == true) {
                    if (!isNullOrUndefined(globalObjective.score.raw) && !isNullOrUndefined(globalObjective.score.raw.value)) {

                        var value = globalObjective.score.raw.value;
                        courseDataModelObjective.score.raw.initialize(value);
                    }
                }
                // read Min Score 

                if (mapInfo[counter].readMinScore != undefined && mapInfo[counter].readMinScore == true) {
                    if (!isNullOrUndefined(globalObjective.score.min) && !isNullOrUndefined(globalObjective.score.min.value)) {
                        var value = globalObjective.score.min.value;
                        courseDataModelObjective.score.min.initialize(value);
                    }
                }
                // read Max Score 
                if (mapInfo[counter].readMaxScore != undefined && mapInfo[counter].readMaxScore == true) {
                    if (!isNullOrUndefined(globalObjective.score.max) && !isNullOrUndefined(globalObjective.score.max.value)) {
                        var value = globalObjective.score.max.value;
                        courseDataModelObjective.score.max.initialize(value);
                    }
                }

                //this.SetRTEDataModelElement(itemIndex, objectiveIndex);
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in FillObjectiveValue", "", 1);
        throw e;
    }

}

/**
* GetAdlSeqItemObjectives(itemIdentifier, objectiveTypes) 
* This Function is used to get objectives from manifest data model for a certain item(all or primary or other)
* @param itemIdentifier              itemIdentifier
* @param objectiveTypes              objectiveTypes
*/
Scorm2004ADLNavChoice.prototype.GetAdlSeqItemObjectives = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('GetAdlSeqItemObjectives');
    try {
        var result = new Array();

        var imsssSequencing = this.scormApi.imsManifest.organizations[0].items[itemIndex].imsssSequencing;
        if (imsssSequencing != null) {
            var adlseqObjectives = imsssSequencing.adlseqObjectives;
            if (adlseqObjectives != null) {
                var objectives = adlseqObjectives.objectives;

                if (objectives != null) {
                    for (var counter = 0; counter < objectives.length; counter++) {
                        result.push(objectives[counter]);
                    }
                }
            }

            if (imsssSequencing.idRef != null) {

                var sequencingCollection = this.scormApi.imsManifest.sequencingCollection;
                if (sequencingCollection != null && sequencingCollection.length > 0) {
                    for (var index = 0; index < sequencingCollection.length; index++) {
                        if (sequencingCollection[index].id == imsssSequencing.idRef) {

                            var adlseqObjectives = sequencingCollection[index].adlseqObjectives;

                            if (adlseqObjectives != null) {
                                var objectives = adlseqObjectives.objectives;

                                if (objectives != null) {
                                    for (var counter = 0; counter < objectives.length; counter++) {
                                        result.push(objectives[counter]);
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetAdlSeqItemObjectives", "", 1);
        throw e;
    }
}

/**
* GetAdlSeqItemObjectiveForId(itemIndex, objectiveId) 
* This Function is used to get adlseq objective with a specified objective id for a specified item
* @param itemIndex               itemIndex
* @param objectiveId              objectiveId
*/
Scorm2004ADLNavChoice.prototype.GetAdlSeqObjectiveForId = function (itemIndex, objectiveId) {
    this.scormApi.DebuggerTraceCall('GetAdlSeqObjectiveForId');
    try {
        var result = null;

        var itemsAdlSeqObjecivesFromManifest = this.GetAdlSeqItemObjectives(itemIndex);

        if (itemsAdlSeqObjecivesFromManifest != null) {
            for (var index = 0; index < itemsAdlSeqObjecivesFromManifest.length; index++) {

                if (itemsAdlSeqObjecivesFromManifest[index].objectiveId == objectiveId) {
                    result = itemsAdlSeqObjecivesFromManifest[index];
                    break;
                }
            }
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetAdlSeqObjectiveForId", "", 1);
        throw e;
    }
}

/**
* GetActivityElementsAttributeValue( itemIndex, ofParent, elementName, attributeName, defaultValue) 
* This Function is used to get activities element attribute value.
* @param itemIndex              item index
* @param ofParent               should we fetch value of Parent of item passed as parameter
* @param elementName            name of xml element
* @param attributeName          name of attribute for which value should be fetched
* @param defaultValue           default value of the attribute
*/
Scorm2004ADLNavChoice.prototype.GetActivityElementsAttributeValue = function (itemIndex, ofParent, elementName, attributeName, defaultValue) {
    this.scormApi.DebuggerTraceCall('GetActivityElementsAttributeValue');
    try {
        var result = defaultValue;
        var parentIdentifier;

        if (ofParent) {
            parentIdentifier = this.scormApi.imsManifest.organizations[0].items[itemIndex].parentIdentifier;
        }
        else {
            parentIdentifier = this.scormApi.imsManifest.organizations[0].items[itemIndex].identifier;
        }

        if (parentIdentifier != null) {
            itemIndex = this.GetItemIndex(parentIdentifier);
        }
        else {
            itemIndex = 0;
        }

        var element = this.scormApi.imsManifest.organizations[0].items[itemIndex][elementName];

        if (element != null) {
            result = element[attributeName];
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetActivityElementsAttributeValue", e.toString(), 1);
        throw e;
    }
}

/**
* Select Children Process [SR.1]
* SelectChildrenProcess(itemIndex) 
* This Function is used for an activity; may change the Available Children for the activity
* @param itemIndex              itemIndex
*/
Scorm2004ADLNavChoice.prototype.SelectChildrenProcess = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('SelectChildrenProcess');
    try {
        var data = this.scormApi.courseDataModel;
        var IsActivityIsActive = data[itemIndex].cmi.isActive.value;
        var IsSuspended = data[itemIndex].cmi.isSuspended.value;

        // if the activity does not have children Then
        if (this.IsLeafActivity(this.GetItemIdentifier(itemIndex))) {
            // exit Select Children Process
            return;
        }
        // if Activity is Suspended for the activity is True Or the Activity is Active for the activity is True Then
        if (IsSuspended || IsActivityIsActive) {
            // exit Select Children Process
            return;
        }
        var selectionTiming = this.GetSequencingElementsAttributeValue(itemIndex, false, this.cEnum.RandomizeChildrenProcess.RandomizationControls, this.cEnum.RandomizeChildrenProcess.SelectionTiming, this.cEnum.RandomizeChildrenProcess.Never);

        switch (selectionTiming) {

            case this.cEnum.RandomizeChildrenProcess.Once:
                // 4.1. if the Activity Progress Status for the activity is False Then
                if (this.scormApi.courseDataModel[itemIndex].cmi.activityProgressStatus.value == false) {
                    // 4.1.1. if the Selection Count Status for the activity is True Then
                    if (this.GetSelectionCountStatus(itemIndex)) {
                        // 4.1.1.1. initialize child list as an Empty ordered list
                        var childList = new Array();
                        // 4.1.1.2. iterate Selection Count, for the activity, times
                        var childrens = this.scormApi.courseDataModel[itemIndex].cmi.availableChildren;
                        var selectCount = this.GetSequencingElementsAttributeValue(itemIndex, false, this.cEnum.RandomizeChildrenProcess.RandomizationControls, this.cEnum.RandomizeChildrenProcess.SelectCount, 0);
                        for (var index = 0; index < selectCount; index++) {
                            // 4.1.1.2.1. randomly select (without replacement) an activity from the children of the activity                        
                            var randomNumber = Math.floor(Math.random() * (childrens.length - 0));
                            // 4.1.1.2.2. add the selected activity to the child list, retaining the original (from the activity) relative order of activities End Iterate
                            childList.push(childrens[randomNumber]);
                            childrens.splice(randomNumber, 1);
                        }
                        // 4.1.1.3. set Available Children for the activity to the child list
                        this.scormApi.courseDataModel[itemIndex].cmi.availableChildren = childList;
                    }
                }
                break;
            case this.cEnum.RandomizeChildrenProcess.Naver:
                // exit Select Children Process
                return;
                break;
            case this.cEnum.RandomizeChildrenProcess.OnEachNewAttempt:
                // exit Select Children Process
                return;
                break;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SelectChildrenProcess", "", 1);
        throw e;
    }
}

/**
* Randomize Children Process [SR.2]
* SelectChildrenProcess(itemIndex) 
* This Function is used for an activity; may change the Available Children for the activity.
* @param itemIndex              itemIndex
*/
Scorm2004ADLNavChoice.prototype.RandomizeChildrenProcess = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('RandomizeChildrenProcess');
    try {

        var data = this.scormApi.courseDataModel;
        var IsActivityIsActive = data[itemIndex].cmi.isActive.value;
        var IsSuspended = data[itemIndex].cmi.isSuspended.value;

        // if the activity does not have children Then
        if (this.IsLeafActivity(this.GetItemIdentifier(itemIndex))) {
            // exit Randomize Children Process
            return;
        }

        // if Activity is Suspended for the activity is True Or the Activity is Active for the activity is True Then
        if (IsSuspended || IsActivityIsActive) {
            // exit Randomize Children Process
            return;
        }
        var reorderChildren = this.GetSequencingElementsAttributeValue(itemIndex, false, this.cEnum.RandomizeChildrenProcess.RandomizationControls, this.cEnum.RandomizeChildrenProcess.ReorderChildren, false);
        var randomizationTiming = this.GetSequencingElementsAttributeValue(itemIndex, false, this.cEnum.RandomizeChildrenProcess.RandomizationControls, this.cEnum.RandomizeChildrenProcess.RandomizationTiming, this.cEnum.RandomizeChildrenProcess.Never);

        switch (randomizationTiming) {

            case this.cEnum.RandomizeChildrenProcess.Never:
                // exit Randomize Children Process
                return;
                break;
            case this.cEnum.RandomizeChildrenProcess.Once:
                // 4.1 if the Activity Progress Status for the activity is False Then
                var activityProgressStatus = this.scormApi.courseDataModel[itemIndex].cmi.activityProgressStatus.value;
                if (!activityProgressStatus) {
                    // 4.1.1. if Randomize Children for the activity is True Then
                    if (reorderChildren) {
                        // 4.1.1.1. randomly reorder the activities contained in Available Children for the activity
                        this.scormApi.courseDataModel[itemIndex].cmi.availableChildren.sort(function () { return 0.5 - Math.random() });
                    }
                }
                break;

                // 4.2. exit Randomize Children Process                                                                                                                 
                return;

                // 5. Case: the Randomization Timing for the activity is On Each New Attempt
            case this.cEnum.RandomizeChildrenProcess.OnEachNewAttempt:
                // 5.1. if Randomize Children for the activity is True Then
                if (reorderChildren) {
                    // 5.1.1. randomly reorder the activities contained in Available Children for the activity
                    this.scormApi.courseDataModel[itemIndex].cmi.availableChildren.sort(function () { return 0.5 - Math.random() });
                }
                break;
                // exit Randomize Children Process                                                                                                               
                return;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in RandomizeChildrenProcess", "", 1);
        throw e;
    }
}

/**
* Randomize Children Process [SR.2]
* SelectChildrenProcess(itemIndex) 
* This Function is used for an activity; may change the Available Children for the activity.
* @param itemIndex              itemIndex
*/
Scorm2004ADLNavChoice.prototype.GetSelectionCountStatus = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('GetSelectionCountStatus');
    try {
        var selectCount = this.GetSequencingElementsAttributeValue(itemIndex, false, this.cEnum.RandomizeChildrenProcess.RandomizationControls, this.cEnum.RandomizeChildrenProcess.SelectCount, 0);
        var selectionTiming = this.GetSequencingElementsAttributeValue(itemIndex, false, this.cEnum.RandomizeChildrenProcess.RandomizationControls, this.cEnum.RandomizeChildrenProcess.SelectionTiming, this.cEnum.RandomizeChildrenProcess.Never);
        // condition derived from http://scorm.com/scorm-explained/technical-scorm/sequencing/sequencing-definition-model      
        if (selectCount > 0 && selectionTiming != this.cEnum.RandomizeChildrenProcess.Never) {
            return true;
        }
        else {
            return false;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetSelectionCountStatus", "", 1);
        throw e;
    }
}


/**
* ReorderChildren() 
*/
Scorm2004ADLNavChoice.prototype.ReorderChildren = function () {
    this.scormApi.DebuggerTraceCall('ReorderChildren');
    try {
        var items = this.scormApi.courseDataModel.length;
        for (var index = 0; index < items; index++) {
            this.SelectChildrenProcess(index);
            this.RandomizeChildrenProcess(index);
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ReorderChildren", "", 1);
        throw e;
    }
}


/**
* GetPrimaryObjectiveFromCDM() 
*/
Scorm2004ADLNavChoice.prototype.GetPrimaryObjectiveFromCDM = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('GetPrimaryObjectiveFromCDM');
    try {

        if (this.GetRolledupObjectiveForItem(itemIndex) != null) {
            return this.scormApi.courseDataModel[itemIndex].cmi.objectives.__array[0];
        }
        else {
            return this.scormApi.primaryObjectives[itemIndex];
        }

    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetPrimaryObjectiveFromCDM", "", 1);
        throw e;
    }
}

/*
* IsObjectiveSatisfiedByMeasure
* Method to check if objective is satisfied by measure
*/
Scorm2004ADLNavChoice.prototype.IsObjectiveSatisfiedByMeasure = function (itemIndex, objectiveId) {
    this.scormApi.DebuggerTraceCall('IsObjectiveSatisfiedByMeasure');
    try {
        var itemObjectives = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);

        var objectiveWithId = this.GetObjectiveWithId(objectiveId, itemObjectives, this.cEnum.GetObjective.item);

        if (objectiveWithId != null) {
            return objectiveWithId.satisfiedByMeasure;
        }
        else {
            return false;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in IsObjectiveSatisfiedByMeasure", "", 1);
        throw e;
    }
}

/*
* IsPrimaryObjectiveSatisfiedByMeasure
* Method to check if objective is satisfied by measure
*/
Scorm2004ADLNavChoice.prototype.IsPrimaryObjectiveSatisfiedByMeasure = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('IsPrimaryObjectiveSatisfiedByMeasure');
    try {
        var result = false;

        // getting primary objective
        var rolledupObjective = this.GetRolledupObjectiveForItem(itemIndex);

        if (rolledupObjective != null) {
            var itemObjectives = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);

            var objectiveWithId = this.GetObjectiveWithId(rolledupObjective.objectiveId, itemObjectives, this.cEnum.GetObjective.item);

            if (objectiveWithId != null) {
                result = objectiveWithId.satisfiedByMeasure;
            }
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in IsPrimaryObjectiveSatisfiedByMeasure", "", 1);
        throw e;
    }
}

/*
* IsObjectiveSatisfiedByMeasure
* Method to get objective minimum normalized measure
*/
Scorm2004ADLNavChoice.prototype.GetObjectiveMinNormalizedMeasure = function (itemIndex, objectiveId) {
    this.scormApi.DebuggerTraceCall('GetObjectiveMinNormalizedMeasure');
    try {
        var itemObjectives = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);

        var objectiveWithId = this.GetObjectiveWithId(objectiveId, itemObjectives, this.cEnum.GetObjective.item);

        if (objectiveWithId != null) {
            return objectiveWithId.minNormalizedMeasure == undefined ? 1.0 : objectiveWithId.minNormalizedMeasure;
        }
        else {
            return 1.0;
        }

    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetObjectiveMinNormalizedMeasure", "", 1);
        throw e;
    }
}

/*
* DisableTreeWithSingleActivity()
* Method to disable table of content for single activity in activity tree.
*/
Scorm2004ADLNavChoice.prototype.DisableTreeWithSingleActivity = function () {
    this.scormApi.DebuggerTraceCall('DisableTreeWithSingleActivity');
    try {
        var itemsLength = this.scormApi.imsManifest.organizations[0].items.length;

        if (itemsLength == 2) {
            var identifierPrefix = "__leaf__";
            for (counter = 0; counter < itemsLength; counter++) {

                var currentItem = this.scormApi.imsManifest.organizations[0].items[counter];

                identifier = currentItem.identifier;

                var liId = document.getElementById((identifierPrefix + identifier));

                if (liId != null) {
                    liId.style.cursor = "default";
                    liId.onclick = function () { return false; };
                }

            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in DisableTreeWithSingleActivity", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.IsActivityTracked = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('IsActivityTracked');
    return this.GetSequencingElementsAttributeValue(itemIndex, false, "deliveryControls", "tracked", true)
}

Scorm2004ADLNavChoice.prototype.IsActivityCompletedByMeasure = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('IsActivityCompletedByMeasure');
    return this.GetActivityElementsAttributeValue(itemIndex, false, "adlcpCompletionThreshold", "completedByMeasure", false);
}

Scorm2004ADLNavChoice.prototype.IsObjectiveSatisfiedByMeasure = function (itemIndex, objectiveId) {
    this.scormApi.DebuggerTraceCall('IsObjectiveSatisfiedByMeasure');

    var itemObjectives = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);

    var objectiveWithId = this.GetObjectiveWithId(objectiveId, itemObjectives, this.cEnum.GetObjective.item);

    if (objectiveWithId != null) {
        return objectiveWithId.satisfiedByMeasure;
    }
    else {
        return false;
    }
}

Scorm2004ADLNavChoice.prototype.GetObjectiveMinNormalizedMeasure = function (itemIndex, objectiveId) {

    this.scormApi.DebuggerTraceCall('GetObjectiveMinNormalizedMeasure');
    var itemObjectives = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);

    var objectiveWithId = this.GetObjectiveWithId(objectiveId, itemObjectives, this.cEnum.GetObjective.item);

    if (objectiveWithId != null) {
        return objectiveWithId.minNormalizedMeasure == undefined ? 1.0 : objectiveWithId.minNormalizedMeasure;
    }
    else {
        return 1.0;
    }
}

Scorm2004ADLNavChoice.prototype.FindObjectiveWithId = function (itemIndex, objectiveId) {

    this.scormApi.DebuggerTraceCall('FindObjectiveWithId');
    var itemObjectives = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);
    var objectiveWithId = this.GetObjectiveWithId(objectiveId, itemObjectives, this.cEnum.GetObjective.item);
    return objectiveWithId;

}

Scorm2004ADLNavChoice.prototype.GetAttemptProgressStatus = function (itemIndex, objectiveId) {
    this.scormApi.DebuggerTraceCall('GetAttemptProgressStatus');
    try {
        var objective = this.FindObjectiveWithIdFromCDM(itemIndex, objectiveId);

        var attemptProgressStatus = objective.attemptProgressStatus.value;

        var maps = this.GetObjectiveMaps(itemIndex, objectiveId);

        var globalObjective;
        for (var i = 0; i < maps.length; i++) {
            if (maps[i].readCompletionStatus) {
                globalObjective = this.GetGlobalObjective(maps[i].targetObjectiveId);
                if (globalObjective != null) {
                    attemptProgressStatus = globalObjective.attemptProgressStatus.value;
                }
            }
        }

        return attemptProgressStatus;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetAttemptProgressStatus", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.GetAttemptCompletionStatus = function (itemIndex, objectiveId) {
    this.scormApi.DebuggerTraceCall('GetAttemptCompletionStatus');
    try {
        var objective = this.FindObjectiveWithIdFromCDM(itemIndex, objectiveId);

        var attemptCompletionStatus = objective.attemptCompletionStatus.value;

        var maps = this.GetObjectiveMapsWithCondition(itemIndex, objectiveId, "readCompletionStatus", true);

        var globalObjective;
        for (var i = 0; i < maps.length; i++) {
            if (maps[i].readCompletionStatus) {
                globalObjective = this.GetGlobalObjective(maps[i].targetObjectiveId);
                if (globalObjective != null) {
                    attemptCompletionStatus = globalObjective.attemptCompletionStatus.value;
                }
            }
        }

        return attemptCompletionStatus;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetAttemptCompletionStatus", "", 1);
        throw e;
    }
}


Scorm2004ADLNavChoice.prototype.GetMeasureStatus = function (itemIndex, objectiveId) {
    this.scormApi.DebuggerTraceCall('GetMeasureStatus');
    try {
        var objective = this.FindObjectiveWithIdFromCDM(itemIndex, objectiveId);

        var objectiveMeasureStatus = objective.objectiveMeasureStatus.value;

        var maps = this.GetObjectiveMaps(itemIndex, objectiveId);

        var globalObjective;
        for (var i = 0; i < maps.length; i++) {
            if (maps[i].readNormalizedMeasure) {
                globalObjective = this.GetGlobalObjective(maps[i].targetObjectiveId);
                if (globalObjective != null) {
                    objectiveMeasureStatus = globalObjective.objectiveMeasureStatus.value;
                }
            }
        }

        return objectiveMeasureStatus;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetMeasureStatus", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.GetNormalizedMeasure = function (itemIndex, objectiveId) {
    this.scormApi.DebuggerTraceCall('GetNormalizedMeasure');
    try {
        var objective = this.FindObjectiveWithIdFromCDM(itemIndex, objectiveId);

        var objectiveMeasureStatus = objective.objectiveMeasureStatus.value;
        var objectiveNormalizedMeasure = objective.objectiveNormalizedMeasure.value;

        var maps = this.GetObjectiveMaps(itemIndex, objectiveId);

        var globalObjective;
        for (var i = 0; i < maps.length; i++) {
            if (maps[i].readNormalizedMeasure) {
                globalObjective = this.GetGlobalObjective(maps[i].targetObjectiveId);
                if (globalObjective != null && globalObjective.objectiveMeasureStatus.value == true) {
                    objectiveNormalizedMeasure = globalObjective.objectiveNormalizedMeasure.value;
                }
            }
        }

        return objectiveNormalizedMeasure;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetNormalizedMeasure", "", 1);
        throw e;
    }
}


Scorm2004ADLNavChoice.prototype.GetObjectiveMaps = function (itemIndex, objectiveId) {

    this.scormApi.DebuggerTraceCall('GetObjectiveMaps');
    try {
        var result = new Array();

        var objective = this.FindObjectiveWithId(itemIndex, objectiveId);
        var mapInfo;

        if (!isNullOrUndefined(objective)) {
            mapInfo = objective.mapInfo;
        }

        if (mapInfo != null && mapInfo.length > 0) {
            for (var index = 0; index < mapInfo.length; index++) {
                result.push(mapInfo[index]);
            }
        }

        var adlseqObjective = this.GetAdlSeqObjectiveForId(itemIndex, objectiveId);
        var mapInfo;

        if (!isNullOrUndefined(adlseqObjective)) {
            mapInfo = adlseqObjective.mapInfo;
        }

        if (mapInfo != null && mapInfo.length > 0) {
            for (var index = 0; index < mapInfo.length; index++) {
                result.push(mapInfo[index]);
            }
        }

        return result;

    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetObjectiveMaps", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.GetObjectiveMapsWithCondition = function (itemIndex, objectiveId, attribute, value) {

    this.scormApi.DebuggerTraceCall('GetObjectiveMapsWithCondition');
    try {
        var mapInfo = this.GetObjectiveMaps(itemIndex, objectiveId);
        var result = new Array();

        for (var index = 0; index < mapInfo.length; index++) {
            if (mapInfo[index][attribute] != undefined && mapInfo[index][attribute] != null && mapInfo[index][attribute] == value) {
                result.push(mapInfo[index]);
            }
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetObjectiveMapsWithCondition", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.MeasureSatisfactionIfActive = function (itemIndex) {

    this.scormApi.DebuggerTraceCall('MeasureSatisfactionIfActive');
    return this.GetSequencingElementsAttributeValue(itemIndex, false, "adlseqRollupConsiderations", "measureSatisfactionIfActive", true);
}

Scorm2004ADLNavChoice.prototype.GetItemCmi = function (itemIndex) {

    this.scormApi.DebuggerTraceCall('GetItemCmi');
    return this.scormApi.courseDataModel[itemIndex].cmi;
}

Scorm2004ADLNavChoice.prototype.FindObjectiveWithIdFromCDM = function (itemIndex, objectiveId) {
    this.scormApi.DebuggerTraceCall('FindObjectiveWithIdFromCDM');
    try {
        var objective = null;

        if (objectiveId == "" || objectiveId == null || objectiveId == undefined) {
            objective = this.GetPrimaryObjectiveFromCDM(itemIndex);
        }
        else {
            var cdmObjectives = this.scormApi.courseDataModel[itemIndex].cmi.objectives.__array;

            for (var index = 0; index < cdmObjectives.length; index++) {
                if (cdmObjectives[index].id.value == objectiveId) {
                    objective = cdmObjectives[index];
                    break;
                }
            }
        }

        return objective;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in FindObjectiveWithIdFromCDM", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.GetObjectiveWithIDFromCDM = function (itemIndex, refObjectiveId) {
    this.scormApi.DebuggerTraceCall('GetObjectiveWithIDFromCDM');
    try {
        var objective = null;


        objective = this.GetPrimaryObjectiveFromCDM(itemIndex);

        if (refObjectiveId != "" && !isNullOrUndefined(refObjectiveId)) {

            var cdmObjectives = this.scormApi.courseDataModel[itemIndex].cmi.objectives.__array;

            for (var index = 0; index < cdmObjectives.length; index++) {
                if (cdmObjectives[index].id.value == refObjectiveId) {
                    objective = cdmObjectives[index];
                    break;
                }
            }
        }
        return objective;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetObjectiveWithIDFromCDM", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.GetProgressStatus = function (itemIndex, objectiveId) {

    this.scormApi.DebuggerTraceCall('GetProgressStatus');
    try {

        var objective = this.FindObjectiveWithIdFromCDM(itemIndex, objectiveId);

        var objectiveProgressStatus = objective.objectiveProgressStatus.value;


        var itemCmi = this.GetItemCmi(itemIndex);
        var satisfiedByMeasure = this.IsObjectiveSatisfiedByMeasure(itemIndex, objectiveId);

        var maps = this.GetObjectiveMaps(itemIndex, objectiveId);

        for (var i = 0; i < maps.length; i++) {
            if (maps[i].readSatisfiedStatus == true) {
                var globalObjective = this.GetGlobalObjective(maps[i].targetObjectiveId);
                if (globalObjective != null) {
                    objectiveProgressStatus = globalObjective.objectiveProgressStatus.value;
                }
            }
        }

        if (satisfiedByMeasure && (!itemCmi.isActive.value || this.MeasureSatisfactionIfActive(itemIndex))) {
            if (this.GetMeasureStatus(itemIndex, objectiveId) == true) {
                objectiveProgressStatus = true;
            } else {
                objectiveProgressStatus = false;
            }
        }

        return objectiveProgressStatus;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetProgressStatus", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.GetSatisfiedStatus = function (itemIndex, objectiveId) {

    this.scormApi.DebuggerTraceCall('GetSatisfiedStatus');
    try {

        var objective = this.FindObjectiveWithIdFromCDM(itemIndex, objectiveId);

        var objectiveProgressStatus = objective.objectiveProgressStatus.value;
        var objectiveSatiswfiedStatus = objective.objectiveSatisfiedStatus.value;

        var itemCmi = this.GetItemCmi(itemIndex);
        var satisfiedByMeasure = this.IsObjectiveSatisfiedByMeasure(itemIndex, objectiveId);

        var maps = this.GetObjectiveMaps(itemIndex, objectiveId);

        for (var i = 0; i < maps.length; i++) {
            if (maps[i].readSatisfiedStatus == true) {
                var globalObjective = this.GetGlobalObjective(maps[i].targetObjectiveId);
                if (globalObjective != null && globalObjective.objectiveProgressStatus.value == true) {
                    objectiveSatisfiedStatus = globalObjective.objectiveSatisfiedStatus.value;
                }
            }
        }

        if (satisfiedByMeasure && (!itemCmi.isActive.value || this.MeasureSatisfactionIfActive(itemIndex))) {

            if (this.GetMeasureStatus(itemIndex, objectiveId) == true) {

                if (this.GetNormalizedMeasure(_29f, _2a0) >= this.GetObjectiveMinNormalizedMeasure(itemIndex, objectiveId)) {
                    objectiveSatisfiedStatus = true;
                } else {
                    objectiveSatisfiedStatus = false;
                }
            }
        }

        return objectiveSatisfiedStatus;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in GetSatisfiedStatus", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.EvaluateRuleCondition = function (itemIndex, refObjectiveId, condition, measureThresholdValue) {

    this.scormApi.DebuggerTraceCall('EvaluateRuleCondition');
    try {
        var result = false;

        if (this.IsActivityTracked(itemIndex) == false) {
            result = "unknown";
            return result;
        }

        var activityAttemptCount = this.scormApi.courseDataModel[itemIndex].cmi.activityAttemptCount.value;
        var activityProgressStatus = this.scormApi.courseDataModel[itemIndex].cmi.activityProgressStatus.value;
        var attemptProgressStatus = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, refObjectiveId);
        var attemptCompletionStatus = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, refObjectiveId);
        var objectiveMeasureStatus = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveMeasureStatus, refObjectiveId);
        var objectiveProgressStatus = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, refObjectiveId);
        var objectiveSatisfiedStatus = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveSatisfiedStatus, refObjectiveId);
        var objectiveNormalizedMeasure = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveNormalizedMeasure, refObjectiveId);
        var attemptCompletionAmountStatus = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmountStatus, refObjectiveId);
        var measureThreshold = -1.0000;

        if (!isNullOrUndefined(measureThresholdValue)) {
            measureThreshold = measureThresholdValue;
        }

        switch (condition) {
            case "satisfied":
                result = (objectiveProgressStatus && objectiveSatisfiedStatus) ? true : false;

                if (!objectiveProgressStatus) {
                    result = 'unknown';
                }
                break;

            case "objectiveStatusKnown":
                result = objectiveProgressStatus ? true : false;
                break;

            case "objectiveMeasureKnown":
                result = objectiveMeasureStatus ? true : false;
                break;

            case "objectiveMeasureGreaterThan":
                result = (objectiveMeasureStatus && objectiveNormalizedMeasure > parseFloat(measureThreshold)) ? true : false;
                if (!objectiveMeasureStatus) {
                    result = 'unknown';
                }
                break;

            case "objectiveMeasureLessThan":
                result = (objectiveMeasureStatus && objectiveNormalizedMeasure < parseFloat(measureThreshold)) ? true : false;
                if (!objectiveMeasureStatus) {
                    result = 'unknown';
                }
                break;

            case "completed":
                result = (attemptProgressStatus && attemptCompletionStatus) ? true : false;
                if (this.IsActivityCompletedByMeasure(itemIndex) && !attemptCompletionAmountStatus) {
                    result = 'unknown';
                }
                if (!attemptProgressStatus) {
                    result = 'unknown';
                }
                break;

            case "activityProgressKnown":
                result = (activityProgressStatus && attemptProgressStatus) ? true : false;
                break;

            case "attempted":
                result = (activityProgressStatus && activityAttemptCount > 0) ? true : false;
                if (!activityProgressStatus) {
                    result = 'unknown';
                }
                break;

            case "attemptLimitExceeded":
                var activityAttemptLimit = this.GetSequencingElementsAttributeValue(itemIndex, false, "limitConditions", "attemptLimit", 0);
                var activityAttemptControl = activityAttemptLimit != null && activityAttemptLimit > 0 ? true : false; // Reference from S_Pool

                result = (activityProgressStatus && activityAttemptControl && activityAttemptCount >= activityAttemptLimit) ? true : false;
                if (!activityProgressStatus) {
                    result = 'unknown';
                }
                break;

                // condition added by taking reference from S_pool (July 12, 2016)
            case "timeLimitExceeded":
                result = false;
                break;

                // condition added by taking reference from S_pool (July 12, 2016)
            case "outsideAvailableTimeRange":
                result = false;
                break;

            case "always":
                result = true;
                break;

            case "never":
                result = true;
                break;
        }

        return result;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in EvaluateRuleCondition", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.InitializeForNewAttempt = function (itemIndex, resetObjectiveStatus, resetActivityStatus) {
    this.scormApi.DebuggerTraceCall('InitializeForNewAttempt');
    try {

        var cdmObjectives = new Array();
        if (this.GetRolledupObjectiveForItem(itemIndex) == null) {
            cdmObjectives.push(this.scormApi.primaryObjectives[itemIndex]);

            otherObjective = this.GetItemObjectivesFromCourseDataModel(itemIndex);

            for (var index = 0; index < otherObjective.length; index++) {
                cdmObjectives.push(otherObjective[index]);
            }
        }
        else {
            cdmObjectives = this.GetItemObjectivesFromCourseDataModel(itemIndex);
        }

        if (resetObjectiveStatus /*&& this.LearningObject.SequencingData.UseCurrentAttemptObjectiveInformation === true*/) {
            resetObjectiveStatus = true;
            for (var index = 0; index < cdmObjectives.length; index++) {
                this.ResetAttemptStateOfObjective(cdmObjectives[index]);
            }
        } else {
            resetObjectiveStatus = false;
        }
        if (resetActivityStatus/* && this.LearningObject.SequencingData.UseCurrentAttemptProgressInformation === true*/) {
            resetActivityStatus = true;
            this.ResetAttemptStateOfActivity(itemIndex);
        } else {
            resetActivityStatus = false;
        }
        var availableChildren = this.scormApi.courseDataModel[itemIndex].cmi.availableChildren;
        for (var index = 0; index < availableChildren.length; index++) {
            this.InitializeForNewAttempt(this.GetItemIndex(availableChildren[index]), resetObjectiveStatus, resetActivityStatus);
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in InitializeForNewAttempt", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.ResetAttemptStateOfActivity = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('ResetAttemptStateOfActivity');
    try {
        var itemCmi = this.GetItemCmi(itemIndex);
        itemCmi.attemptProgressStatus.initialize(false);
        itemCmi.attemptCompletionAmountStatus.initialize(false);
        itemCmi.attemptCompletionAmount = 0;
        itemCmi.attemptCompletionStatus.initialize(false);
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ResetAttemptStateOfActivity", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.ResetAttemptStateOfObjective = function (cdmObjective) {
    this.scormApi.DebuggerTraceCall('ResetAttemptStateOfObjective');
    try {
        cdmObjective.objectiveProgressStatus.initialize(false);
        cdmObjective.objectiveSatisfiedStatus.initialize(false);
        cdmObjective.objectiveMeasureStatus.initialize(false);
        cdmObjective.objectiveNormalizedMeasure.initialize(0.0);
        cdmObjective.score.raw.initialize(null);
        cdmObjective.score.min.initialize(null);
        cdmObjective.score.max.initialize(null);
        cdmObjective.completion_status.initialize(false);
        cdmObjective.progress_measure.initialize(null);
    }
    catch (e) {
        this.scormApi.traceError("Error caught in ResetAttemptStateOfObjective", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.TrackChangesDuringRuntime = function (itemIndex, elementChanged) {
    this.scormApi.DebuggerTraceCall('TrackChangesDuringRuntime');
    try {
        var itemCmi = this.GetItemCmi(itemIndex);
        var primaryObjective = this.GetPrimaryObjectiveFromCDM(itemIndex);

        switch (elementChanged) {
            case "cmi.success_status":
                itemCmi.successStatusChangedDuringRuntime.value = true;
                primaryObjective.successStatusChangedDuringRuntime.value = true;
                break;
            case "cmi.completion_status":
                itemCmi.completionStatusChangedDuringRuntime.value = true;
                primaryObjective.completionStatusChangedDuringRuntime.value = true;
                break;
            case "cmi.progress_measure":
                itemCmi.progressMeasureChangedDuringRuntime.value = true;
                primaryObjective.progressMeasureChangedDuringRuntime.value = true;
                break;
        }

    }
    catch (e) {
        this.scormApi.traceError("Error caught in TrackChangesDuringRuntime", "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.RunSetrteForItemAndItsObjectives = function (itemIndex) {
    this.scormApi.DebuggerTraceCall('RunSetrteForItemAndItsObjectives');
    try {
        var objectives = this.GetItemObjectivesFromCourseDataModel(itemIndex);

        for (var index = 0; index < objectives.length; index++) {
            this.SetRTEDataModelElement(itemIndex, index);
        }

        this.SetRTEDataModelElement(itemIndex, null);
    }
    catch (e) {
        this.scormApi.traceError("Error caught in RunSetrteForItemAndItsObjectives", "", 1);
        throw e;
    }
}


/**
* InitializeObjectiveProgressInformationAndAttemptProgInformation(itemIndex)
* This Function is used to get Global Objective with a specific id
* @param activity            activity for which objectives to be initialized
*/
Scorm2004ADLNavChoice.prototype.InitializeObjectiveProgressInformationAndAttemptProgInformation = function (itemIndex) {

    this.scormApi.DebuggerTraceCall('InitializeObjectives');
    try {
        this.InitializeObjectives(itemIndex);
        var primaryObjective = this.GetPrimaryObjectiveFromCDM(itemIndex);

        // table 4.2.1.2a: Objective Progress Information
        primaryObjective.objectiveProgressStatus.value = false;
        primaryObjective.objectiveSatisfiedStatus.value = false;
        primaryObjective.objectiveMeasureStatus.value = false;
        primaryObjective.objectiveNormalizedMeasure.value = 0.0;

        // table 4.2.1.4a: Attempt Progress Information
        primaryObjective.attemptProgressStatus.value = false;
        primaryObjective.attemptCompletionStatus.value = false;
        primaryObjective.attemptCompletionAmountStatus.value = false;
        primaryObjective.attemptCompletionAmount.value = 0.0;
        primaryObjective.attemptAbsoluteDuration.value = 0.0;
        primaryObjective.attemptExperiencedDuration.value = 0.0;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in InitializeObjectives", "", 1);
        throw e;
    }
}


/**
* SetPrimaryObjectiveSatusValues(itemIndex, dataModelElement, value)
* This Function is used to set the primary objective status value and transfer the values to Global Objective with a specific id.
* @param itemIndex            activity for which objectives to be initialized
* @param dataModelElement     dataModelElement of which value for objectives to be initialized
* @param value                objectives status value to be initialized
*/
Scorm2004ADLNavChoice.prototype.SetPrimaryObjectiveSatusValues = function (itemIndex, dataModelElement, value, objectiveToUpdate) {

    this.scormApi.DebuggerTraceCall('SetPrimaryObjectiveSatusValues');
    try {

        var primaryObjective = this.GetPrimaryObjectiveFromCDM(itemIndex);

        if (objectiveToUpdate != null) {
            primaryObjective = objectiveToUpdate;
        }
        switch (dataModelElement) {
            case "objectiveProgressStatus":
                primaryObjective.objectiveProgressStatus.value = value;
                break;

            case "objectiveSatisfiedStatus":
                primaryObjective.objectiveSatisfiedStatus.value = value;
                break;

            case "objectiveMeasureStatus":
                primaryObjective.objectiveMeasureStatus.value = value;
                break;

            case "objectiveNormalizedMeasure":
                primaryObjective.objectiveNormalizedMeasure.value = value;
                break;

            case "attemptProgressStatus":
                primaryObjective.attemptProgressStatus.value = value;
                break;

            case "attemptCompletionStatus":
                primaryObjective.attemptCompletionStatus.value = value;
                break;

            case "attemptCompletionAmount":
                primaryObjective.attemptCompletionAmount.value = value;
                break;

                // this Status updation will not cause a transfer of data to global objective
            case "attemptCompletionAmountStatus":
                primaryObjective.attemptCompletionAmountStatus.value = value;
                break;
            case "score.scaled":
                primaryObjective.score.scaled.value = value;
                break;

            case "score.min":
                primaryObjective.score.min.value = value;
                break;

            case "score.max":
                primaryObjective.score.max.value = value;
                break;

            case "score.raw":
                primaryObjective.score.raw.value = value;
                break;

            case "progress_measure":
                primaryObjective.progress_measure.value = value;
                break;

            case "completion_status":
                primaryObjective.completion_status.value = value;
                break;

            case "success_status":
                primaryObjective.success_status.value = value;
                break;
        }

        // update globale objectives, reference taken from S_pool
        this.UpdateGlobalObjectiveNew(itemIndex, primaryObjective);
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SetPrimaryObjectiveSatusValues", "", 1);
        throw e;
    }
}

/**
* SetPrimaryObjectiveSatusValues(itemIndex, dataModelElement, value)
* This Function is used to Get the primary objective status value and transfer the values to Global Objective with a specific id.
* @param itemIndex            activity for which objectives to be initialized
* @param dataModelElement     dataModelElement of which value for objectives to be initialized
* @param value                objectives status value to be initialized
*/
Scorm2004ADLNavChoice.prototype.GetPrimaryObjectiveSatusValues = function (itemIndex, dataModelElement, refObjectiveId, objectiveObjectToUpdate) {

    this.scormApi.DebuggerTraceCall('GetPrimaryObjectiveSatusValues');
    try {

        var primaryObjective = this.GetObjectiveWithIDFromCDM(itemIndex, refObjectiveId); // this.GetPrimaryObjectiveFromCDM(itemIndex);

        if (objectiveObjectToUpdate != null) {
            primaryObjective = objectiveObjectToUpdate;
        }
        var value = null;

        switch (dataModelElement) {
            case "objectiveProgressStatus":
                value = primaryObjective.objectiveProgressStatus.value;
                break;

            case "objectiveSatisfiedStatus":
                value = primaryObjective.objectiveSatisfiedStatus.value;
                break;

            case "objectiveMeasureStatus":
                value = primaryObjective.objectiveMeasureStatus.value;
                break;

            case "objectiveNormalizedMeasure":
                value = primaryObjective.objectiveNormalizedMeasure.value;
                break;

            case "attemptProgressStatus":
                value = primaryObjective.attemptProgressStatus.value;
                break;

            case "attemptCompletionStatus":
                value = primaryObjective.attemptCompletionStatus.value;
                break;

            case "attemptCompletionAmount":
                value = primaryObjective.attemptCompletionAmount.value;
                break;

                // this Status updation will not cause a transfer of data to global objective
            case "attemptCompletionAmountStatus":
                value = primaryObjective.attemptCompletionAmountStatus.value;
                break;

            case "score.scaled":
                value = primaryObjective.score.scaled.value;
                break;

            case "score.min":
                value = primaryObjective.score.min.value;
                break;

            case "score.max":
                value = primaryObjective.score.max.value;
                break;

            case "score.raw":
                value = primaryObjective.score.raw.value;
                break;

            case "progress_measure":
                value = primaryObjective.progress_measure.value;
                break;
        }

        return value;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in SetPrimaryObjectiveSatusValues", "", 1);
        throw e;
    }
}

/**
* UpdateGlobalObjectiveNew(itemIndex)
* This Function is used to Get the primary objective status value and transfer the values to Global Objective with a specific id.
* @param itemIndex            activity Item index
* @param objectiveIndex       objective Index of an item
*/
Scorm2004ADLNavChoice.prototype.UpdateGlobalObjectiveNew = function (itemIndex, itemObjective) {

    this.scormApi.DebuggerTraceCall('UpdateGlobalObjectiveNew');
    try {

        var tracked = this.GetSequencingElementsAttributeValue(itemIndex, false, "deliveryControls", "tracked", true);

        var mapInfos = this.GetItemObjectivesMapInfoFromManifest(itemIndex, itemObjective.id.value);

        if (itemObjective == null || this.scormApi.globalObjectives == null || !tracked || mapInfos == null) {
            return;
        }

        for (var i = 0; i < mapInfos.length; i++) {

            var currentMapInfo = mapInfos[i];

            var globalObjectveIdFromManifest = currentMapInfo.targetObjectiveId;

            var globalObjectiveToUpdateFromCDM = this.GetGlobalObjectiveWithIdFromCDM(globalObjectveIdFromManifest);

            if (isNullOrUndefined(globalObjectiveToUpdateFromCDM)) {
                continue;
            }

            var $5 = currentMapInfo.writeSatisfiedStatus;
            if ($5) {
                globalObjectiveToUpdateFromCDM.objectiveProgressStatus.value = itemObjective.objectiveProgressStatus.value;
                globalObjectiveToUpdateFromCDM.objectiveSatisfiedStatus.value = itemObjective.objectiveSatisfiedStatus.value;
            }
            var $6 = currentMapInfo.writeNormalizedMeasure;
            if ($6) {
                globalObjectiveToUpdateFromCDM.objectiveMeasureStatus.value = itemObjective.objectiveMeasureStatus.value;
                globalObjectiveToUpdateFromCDM.objectiveNormalizedMeasure.value = itemObjective.objectiveNormalizedMeasure.value;
            }
            var $7 = currentMapInfo.writeRawScore;
            if ($7) {
                globalObjectiveToUpdateFromCDM.score.raw.value = itemObjective.score.raw.value;
            }
            var $8 = currentMapInfo.writeMinScore;
            if ($8) {
                globalObjectiveToUpdateFromCDM.score.min.value = itemObjective.score.min.value;
            }
            var $9 = currentMapInfo.writeMaxScore;
            if ($9) {
                globalObjectiveToUpdateFromCDM.score.max.value = itemObjective.score.max.value;
            }
            var $A = currentMapInfo.writeProgressMeasure;
            if ($A) {
                globalObjectiveToUpdateFromCDM.progressMeasureStatus.value = itemObjective.attemptCompletionAmountStatus.value;
                globalObjectiveToUpdateFromCDM.progressMeasure.value = itemObjective.attemptCompletionAmount.value;
            }
            var $B = currentMapInfo.writeCompletionStatus;
            if ($B) {
                globalObjectiveToUpdateFromCDM.attemptProgressStatus.value = itemObjective.attemptProgressStatus.value;
                globalObjectiveToUpdateFromCDM.completion_status.value = itemObjective.attemptCompletionStatus.value;
            }

            for (var cdmItem = 0; cdmItem < this.scormApi.courseDataModel.length; cdmItem++) {

                // if (cdmItem != itemIndex) {

                if (!this.GetSequencingElementsAttributeValue(cdmItem, false, "deliveryControls", "tracked", true)) {

                    continue;
                }
                var itemCmi = this.GetItemCmi(cdmItem);
                var itemObjectives = this.GetItemObjectivesFromManifest(cdmItem, this.cEnum.ObjectiveTypes.all);

                if (itemObjectives != null && itemObjectives.length > 0) {

                    for (var ojectiveCounter = 0; ojectiveCounter < itemObjectives.length; ojectiveCounter++) {

                        if (cdmItem == itemIndex && itemObjective.id.value == itemObjectives[ojectiveCounter].objectiveId) {
                            continue;
                        }

                        var mapInfos2 = itemObjectives[ojectiveCounter].mapInfo;

                        if (mapInfos2 != null && mapInfos2.length > 0) {

                            for (var mapInfoCounter = 0; mapInfoCounter < mapInfos2.length; mapInfoCounter++) {

                                if (mapInfos2[mapInfoCounter].targetObjectiveId == globalObjectveIdFromManifest) {

                                    var returnedItem = this.GetItemObjectivesFromCourseDataModelWithId(cdmItem, itemObjectives[ojectiveCounter].objectiveId);

                                    if (returnedItem != null) {

                                        var $10 = false;
                                        if ($5 && mapInfos2[mapInfoCounter].readSatisfiedStatus) {
                                            $10 = true;
                                            itemCmi.activityProgressStatus.value = true;
                                            if (globalObjectiveToUpdateFromCDM.objectiveProgressStatus.value) {
                                                //returnedItem.objectiveSatisfiedStatusFromGlobal.value = true;
                                                returnedItem.objectiveProgressStatus.value = globalObjectiveToUpdateFromCDM.objectiveProgressStatus.value;
                                                returnedItem.objectiveSatisfiedStatus.value = globalObjectiveToUpdateFromCDM.objectiveSatisfiedStatus.value;
                                            } else {
                                                returnedItem.objectiveProgressStatus.value = false;
                                                returnedItem.objectiveSatisfiedStatus.value = false;
                                            }
                                        }
                                        if ($6 && mapInfos2[mapInfoCounter].readNormalizedMeasure) {
                                            $10 = true;
                                            itemCmi.activityProgressStatus.value = true;

                                            if (globalObjectiveToUpdateFromCDM.objectiveMeasureStatus.value) {
                                                //returnedItem.objectiveMeasureStatusFromGlobal.value = true;
                                                returnedItem.objectiveMeasureStatus.value = globalObjectiveToUpdateFromCDM.objectiveMeasureStatus.value;
                                                returnedItem.objectiveNormalizedMeasure.value = globalObjectiveToUpdateFromCDM.objectiveNormalizedMeasure.value;
                                            } else {
                                                returnedItem.objectiveMeasureStatus.value = false;
                                                returnedItem.objectiveNormalizedMeasure.value = 0;
                                            }
                                        }

                                        if ($7 && mapInfos2[mapInfoCounter].readRawScore) {
                                            $10 = true;
                                            returnedItem.score.raw.value = globalObjectiveToUpdateFromCDM.score.raw.value;
                                        }
                                        if ($8 && mapInfos2[mapInfoCounter].readMinScore) {
                                            $10 = true;
                                            returnedItem.score.min.value = globalObjectiveToUpdateFromCDM.score.min.value;
                                        }
                                        if ($9 && mapInfos2[mapInfoCounter].readMaxScore) {
                                            $10 = true;
                                            returnedItem.score.max.value = globalObjectiveToUpdateFromCDM.score.max.value;
                                        }
                                        if ($A && mapInfos2[mapInfoCounter].readProgressMeasure) {
                                            $10 = true;
                                            //returnedItem.attemptCompletionAmountFromGlobal = true;
                                            returnedItem.attemptCompletionAmountStatus.value = globalObjectiveToUpdateFromCDM.progressMeasureStatus.value;
                                            returnedItem.attemptCompletionAmount.value = globalObjectiveToUpdateFromCDM.progressMeasure.value;
                                        }
                                        if ($B && mapInfos2[mapInfoCounter].readCompletionStatus) {
                                            $10 = true;
                                            itemCmi.activityProgressStatus.value = true;
                                            //returnedItem.attemptCompletionStatusFromGlobal.value = true;
                                            returnedItem.attemptCompletionStatus.value = globalObjectiveToUpdateFromCDM.completion_status.value;
                                            returnedItem.attemptProgressStatus.value = globalObjectiveToUpdateFromCDM.attemptProgressStatus.value;
                                        }
                                        // if ($10 && !$0.contains($D.getSequencing().getActivityTreeNode())) {
                                        //    $0.add($D.getSequencing().getActivityTreeNode());
                                        //}

                                    }

                                }
                            }
                        }

                    }
                }
                //}
            }


        }

        if (this.courseDataModelObjective != undefined) {
            var itemsAllObjectivesFromManifest = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);
            var manifestDataModelObjective = this.GetObjectiveWithId(this.courseDataModelObjective.id.value, itemsAllObjectivesFromManifest, this.cEnum.GetObjective.item);

            var mapInfo = manifestDataModelObjective == null ? null : manifestDataModelObjective.mapInfo;



            if (tracked && mapInfo != null && mapInfo.length > 0) {
                for (var index = 0; index < mapInfo.length; index++) {

                    var value = null;

                    if (mapInfo[index].writeNormalizedMeasure) {

                        var globalObjective = this.GetGlobalObjective(mapInfo[index].targetObjectiveId);

                        value = this.courseDataModelObjective.objectiveMeasureStatus.value;
                        globalObjective.objectiveMeasureStatus.value = value;

                        value = this.courseDataModelObjective.objectiveNormalizedMeasure.value;
                        globalObjective.objectiveNormalizedMeasure.value = value;
                    }

                    if (mapInfo[index].writeSatisfiedStatus) {

                        var globalObjective = this.GetGlobalObjective(mapInfo[index].targetObjectiveId);

                        value = this.courseDataModelObjective.objectiveProgressStatus.value;
                        globalObjective.objectiveProgressStatus.value = value;

                        value = this.courseDataModelObjective.objectiveSatisfiedStatus.value;
                        globalObjective.objectiveSatisfiedStatus.value = value;
                    }
                }
            }

            var itemsAdlSeqObjecivesFromManifest = this.GetAdlSeqItemObjectives(itemIndex);
            var relatedAdlSeqObjective = null;
            if (itemsAdlSeqObjecivesFromManifest != null && itemsAdlSeqObjecivesFromManifest.length > 0) {
                for (var index = 0; index < itemsAdlSeqObjecivesFromManifest.length; index++) {
                    if (itemsAdlSeqObjecivesFromManifest[index].objectiveId == this.courseDataModelObjective.id.value) {
                        relatedAdlSeqObjective = itemsAdlSeqObjecivesFromManifest[index];
                        break;
                    }
                }
            }

            var adlseqMapInfo = null;

            if (relatedAdlSeqObjective != null) {
                var adlseqMapInfo = relatedAdlSeqObjective == null ? null : relatedAdlSeqObjective.mapInfo;
            }

            if (tracked && adlseqMapInfo != null && adlseqMapInfo.length > 0) {
                for (var index = 0; index < adlseqMapInfo.length; index++) {

                    var value = null;
                    var globalObjective = this.GetGlobalObjective(adlseqMapInfo[index].targetObjectiveId);

                    if (adlseqMapInfo[index].writeCompletionStatus) {

                        value = this.courseDataModelObjective.completion_status.value;
                        globalObjective.completion_status.value = value;

                        if (value == "completed") {
                            globalObjective.attemptProgressStatus.value = true;
                            globalObjective.attemptCompletionStatus.value = true;
                        }
                        else if (value == "incomplete" || value == "not attempted") {
                            globalObjective.attemptProgressStatus.value = true;
                            globalObjective.attemptCompletionStatus.value = false;
                        }
                        else {
                            globalObjective.attemptProgressStatus.value = false;
                            globalObjective.attemptCompletionStatus.value = false;
                        }
                    }

                    if (adlseqMapInfo[index].writeProgressMeasure) {

                        value = this.courseDataModelObjective.progress_measure.value;
                        globalObjective.progress_measure.initialize(value);
                        globalObjective.attemptCompletionAmount.value = value;
                    }

                    if (adlseqMapInfo[index].writeRawScore) {

                        value = this.courseDataModelObjective.score.raw.value;
                        globalObjective.score.raw.value = value;
                    }

                    if (adlseqMapInfo[index].writeMinScore) {

                        value = this.courseDataModelObjective.score.min.value;
                        globalObjective.score.min.value = value;
                    }

                    if (adlseqMapInfo[index].writeMaxScore) {

                        value = this.courseDataModelObjective.score.max.value;
                        globalObjective.score.max.value = value;
                    }
                }
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in UpdateGlobalObjectiveNew " + e, "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.GetItemObjectivesMapInfoFromManifest = function (itemIndex, objectiveId) {

    var mapInfos = null;

    var itemObjectives = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);

    if (itemObjectives != null && itemObjectives.length > 0) {
        for (var i = 0; i < itemObjectives.length; i++) {

            if (itemObjectives[i].objectiveId == objectiveId) {
                mapInfos = itemObjectives[i].mapInfo;
                break;
            }
        }
    }

    return mapInfos;
}

Scorm2004ADLNavChoice.prototype.GetGlobalObjectiveWithIdFromCDM = function (objectiveId) {

    var globalObjective = null;

    var globalObjectives = this.scormApi.globalObjectives;

    if (globalObjectives != null && globalObjectives.length > 0) {
        for (var i = 0; i < globalObjectives.length; i++) {

            if (globalObjectives[i].id.value == objectiveId) {
                globalObjective = globalObjectives[i];
                break;
            }
        }
    }

    return globalObjective;
}

Scorm2004ADLNavChoice.prototype.GetItemObjectivesFromCourseDataModelWithId = function (itemIndex, objectiveId) {

    var result = null;

    var itemObjectives = this.GetItemObjectivesFromCourseDataModel(itemIndex);

    if (itemObjectives != null && itemObjectives.length > 0) {
        for (var i = 0; i < itemObjectives.length; i++) {

            if (itemObjectives[i].id.value == objectiveId) {
                result = itemObjectives[i];
                break;
            }
        }
    }

    return result;
}

Scorm2004ADLNavChoice.prototype.UpdateSequencingState = function (itemIndex) {

    try {
        if (!this.IsActivityTracked(itemIndex)) {
            return;
        }

        var objectiveCounter = 0;
        var primaryObj = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.primary)
        var normalObjective = null;
        var normalObjectiveFromManifest = null;
        var cmi = this.GetItemCmi(itemIndex);

        var completion_status = 'cmi.completion_status';
        var progress_measure = 'cmi.progress_measure';
        var success_status = 'cmi.success_status';
        var score_scaled = 'cmi.score.scaled';
        var score_raw = 'cmi.score.raw';
        var score_min = 'cmi.score.min';
        var score_max = 'cmi.score.max';

        if (primaryObj.length > 0) {
            if (!this.isSetBySco(itemIndex, completion_status)) {
                completion_status = 'cmi.objectives.0.completion_status';
            }
            if (!this.isSetBySco(itemIndex, success_status)) {
                success_status = 'cmi.objectives.0.success_status';
            }
            if (!this.isSetBySco(itemIndex, progress_measure)) {
                progress_measure = 'cmi.objectives.0.progress_measure';
            }
            if (!this.isSetBySco(itemIndex, score_scaled)) {
                score_scaled = 'cmi.objectives.0.score.scaled';
            }
            if (!this.isSetBySco(itemIndex, score_raw)) {
                score_raw = 'cmi.objectives.0.score.raw';
            }
            if (!this.isSetBySco(itemIndex, score_min)) {
                score_min = 'cmi.objectives.0.score.min';
            }
            if (!this.isSetBySco(itemIndex, score_max)) {
                score_max = 'cmi.objectives.0.score.max';
            }
            objectiveCounter = 1;
        }

        var completionSetByContent = this.GetSequencingElementsAttributeValue(itemIndex, false, "deliveryControls", "completionSetByContent", false);
        var objectiveSetByContent = this.GetSequencingElementsAttributeValue(itemIndex, false, "deliveryControls", "objectiveSetByContent", false);

        var counter = true;
        while (counter) {
            if (completionSetByContent || this.isSetBySco(itemIndex, completion_status)) {

                var attemptCompletionStatus = this.FetchCmiElementValueFromItemOrObjective(itemIndex, completion_status) === 'completed' ? true : false;
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionStatus, attemptCompletionStatus, normalObjective);

                var attemptProgressStatus = this.FetchCmiElementValueFromItemOrObjective(itemIndex, completion_status) !== 'unknown' ? true : false;
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptProgressStatus, attemptProgressStatus, normalObjective);

            }
            if (objectiveSetByContent || this.isSetBySco(itemIndex, success_status)) {

                var objectiveSatisfiedStatus = this.FetchCmiElementValueFromItemOrObjective(itemIndex, success_status) === 'passed' ? true : false;
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveSatisfiedStatus, objectiveSatisfiedStatus, normalObjective);

                var objectiveProgressStatus = this.FetchCmiElementValueFromItemOrObjective(itemIndex, success_status) !== 'unknown' ? true : false;
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, objectiveProgressStatus, normalObjective);
            }
            if (this.isSetBySco(itemIndex, progress_measure)) {
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmountStatus, true, normalObjective);

                var progressMeasureValue = this.FetchCmiElementValueFromItemOrObjective(itemIndex, progress_measure);
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.attemptCompletionAmount, parseFloat(progressMeasureValue), normalObjective);
            }
            if (this.isSetBySco(itemIndex, score_scaled)) {
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveMeasureStatus, true, normalObjective);

                var scaledValue = this.FetchCmiElementValueFromItemOrObjective(itemIndex, score_scaled);
                this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveNormalizedMeasure, parseFloat(scaledValue), normalObjective);
            }

            var score_raw_value = this.FetchCmiElementValueFromItemOrObjective(itemIndex, score_raw);
            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.score_raw, score_raw_value, normalObjective);

            var score_min_value = this.FetchCmiElementValueFromItemOrObjective(itemIndex, score_min);
            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.score_min, score_min_value, normalObjective);

            var score_max_value = this.FetchCmiElementValueFromItemOrObjective(itemIndex, score_max);
            this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.score_max, score_max_value, normalObjective);

            if (normalObjectiveFromManifest != null && !normalObjectiveFromManifest.objectiveContributesToRollup) {
                if (normalObjectiveFromManifest.satisfiedByMeasure) {

                    var objectiveNormalizedMeasure = this.GetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveNormalizedMeasure, normalObjective);
                    if (objectiveNormalizedMeasure >= normalObjectiveFromManifest.minNormalizedMeasure) {
                        this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, true, normalObjective);
                        this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveSatisfiedStatus, true, normalObjective);
                    } else {
                        this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveProgressStatus, true, normalObjective);
                        this.SetPrimaryObjectiveSatusValues(itemIndex, this.cEnum.RollupObjectiveStatusElement.objectiveSatisfiedStatus, false, normalObjective);
                    }
                }
            }

            if (cmi.objectives.__array.length >= objectiveCounter + 1) {
                completion_status = 'cmi.objectives.' + objectiveCounter + '.completion_status';
                success_status = 'cmi.objectives.' + objectiveCounter + '.success_status';
                progress_measure = 'cmi.objectives.' + objectiveCounter + '.progress_measure';
                score_scaled = 'cmi.objectives.' + objectiveCounter + '.score.scaled';
                score_raw = 'cmi.objectives.' + objectiveCounter + '.score.raw';
                score_min = 'cmi.objectives.' + objectiveCounter + '.score.min';
                score_max = 'cmi.objectives.' + objectiveCounter + '.score.max';

                var itemsAllObjectives = this.GetItemObjectivesFromManifest(itemIndex, this.cEnum.ObjectiveTypes.all);
                normalObjectiveFromManifest = itemsAllObjectives[objectiveCounter];
                normalObjective = this.scormApi.courseDataModel[itemIndex].cmi.objectives.__array[objectiveCounter];
                objectiveCounter++;
            } else {
                counter = false;
            }
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in UpdateSequencingState " + e, "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.FetchCmiElementValueFromItemOrObjective = function (itemIndex, node) {
    try {
        var nodeArray = node.split('.');
        var element = this.scormApi.courseDataModel[itemIndex];
        var lastAttribute = nodeArray[nodeArray.length - 1];

        for (var i = 0; i < nodeArray.length; i++) {
            var attribute = nodeArray[i];
            if (isNaN(parseInt(attribute)) && element[attribute] != null) {
                element = element[attribute];
            }
            else if (!isNaN(parseInt(attribute))) {
                element = element.__array[parseInt(attribute)];
            }
            else {
                return null;
            }
        }

        return element.value;
    }
    catch (e) {
        this.scormApi.traceError("Error caught in FetchCmiElementValueFromItemOrObjective " + e, "", 1);
        throw e;
    }
}

Scorm2004ADLNavChoice.prototype.isSetBySco = function (itemIndex, node) {
    try {
        var nodeArray = node.split('.');
        var element = this.scormApi.courseDataModel[itemIndex];

        for (var i = 0; i < nodeArray.length; i++) {

            var attribute = nodeArray[i];

            if (isNaN(parseInt(attribute)) && element[attribute] != null) {
                element = element[attribute];
            }
            else if (!isNaN(parseInt(attribute))) {
                element = element.__array[parseInt(attribute)];
            }
            else {
                return null;
            }
        }

        if (element.setBySco) {
            return true;
        }
        else {
            return false;
        }
    }
    catch (e) {
        this.scormApi.traceError("Error caught in isSetBySco " + e, "", 1);
        throw e;
    }

}
