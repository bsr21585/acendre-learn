﻿using System;

namespace Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004
{
    /// <summary>
    /// An empty placeholder class for client script embedded resources.
    /// This needs to exist because <see cref="ClientScriptManager.RegisterClientScriptResource"/>
    /// has to bind to a type (class) to embed the script resource.
    /// </summary>
    public class Scorm2004
    { }
}
