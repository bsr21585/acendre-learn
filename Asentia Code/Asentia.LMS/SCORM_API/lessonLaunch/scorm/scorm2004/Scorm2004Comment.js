﻿/**
 * Scorm2004Comment
 *
 * Public Properties:
 *   comment
 *   location
 *   timestamp
 */
function Scorm2004Comment()
{
	//use shorthand references for convenience below
	var a = Scorm2004DataModel.ACCESS;
	var t = Scorm2004DataModel.DATA_TYPE;
	
	this.comment	= new Scorm2004DataItem(this, a.ReadWrite,	t.CharString,	4000); //REQ_57.3 & REQ_58.3
	this.location	= new Scorm2004DataItem(this, a.ReadWrite,	t.CharString,	250); //REQ_57.4 & REQ_58.4
	this.timestamp	= new Scorm2004DataItem(this, a.ReadWrite,	t.Time); //REQ_57.5 & REQ_58.5
};
