﻿/**
 * Scorm2004InteractionObjective
 *
 * Public Properties:
 *   id
 */
function Scorm2004InteractionObjective()
{
	//use shorthand references for convenience below
	var a = Scorm2004DataModel.ACCESS;
	var t = Scorm2004DataModel.DATA_TYPE;

	this.id = new Scorm2004DataItem(this, a.ReadWrite,	t.LongId,	4000); //REQ_64.5.2
};
