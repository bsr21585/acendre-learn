﻿/**
 * Scorm2004Interaction
 *
 * Public Properties:
 *   id
 *   type
 *   timestamp
 *   weighting
 *   learner_response
 *   result
 *   latency
 *   description
 *   objectives._count
 *   objectives.n.id
 *   correct_responses._count
 *   correct_responses.n.pattern
 */
function Scorm2004Interaction()
{
	//use shorthand references for convenience below
	var a = Scorm2004DataModel.ACCESS;
	var t = Scorm2004DataModel.DATA_TYPE;
	
	this.id					= new Scorm2004DataItem(this, a.ReadWrite,	t.LongId,			4000); //REQ_64.3
	this.type				= new Scorm2004DataItem(this, a.ReadWrite,	t.Vocabulary,		t.Vocabulary.InteractionType); //REQ_64.4
	this.timestamp			= new Scorm2004DataItem(this, a.ReadWrite,	t.Time,				4000); //REQ_64.6
	this.weighting			= new Scorm2004DataItem(this, a.ReadWrite,	t.Real); //REQ_64.8
	this.learner_response	= new Scorm2004DataItem(this, a.ReadWrite,	t.CharString); //REQ_64.9
	this.result				= new Scorm2004DataItem(this, a.ReadWrite,	t.Vocabulary,		t.Vocabulary.InteractionResult); //REQ_64.10
	this.latency			= new Scorm2004DataItem(this, a.ReadWrite,	t.TimeInterval); //REQ_64.11
	this.description		= new Scorm2004DataItem(this, a.ReadWrite,	t.CharStringLocal,	250); //REQ_64.12
	
	this.objectives = //REQ_64.5
	{
		__array		: new Array(),
		__itemType	: "Scorm2004InteractionObjective()",
		_count		: new Scorm2004DataItem(this, a.ReadOnly,	t.Integer, "0:..") //REQ_64.5.1
	};
	this.correct_responses = //REQ_64.7
	{
		__array		: new Array(),
		__itemType	: "Scorm2004InteractionCorrectResponse()",
		_count		: new Scorm2004DataItem(this, a.ReadOnly,	t.Integer, "0:..") //REQ_64.7.1
	};
	
	this.objectives._count.initialize("0", true);
	this.correct_responses._count.initialize("0", true);
};
