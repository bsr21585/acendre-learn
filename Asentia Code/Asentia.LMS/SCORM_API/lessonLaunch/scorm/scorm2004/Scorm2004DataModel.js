﻿/**
* Scorm2004DataModel
*
* Public Properties:
* cmi. :
*   _version
*   comments_from_learner. ...
*      _children
*      _count
*      n.comment		*
*      n.location		*
*      n.timestamp		*
*   comments_from_lms. ...
*      _children
*      _count
*      n.comment		*
*      n.location		*
*      n.date_time		*
*   completion_status
*   completion_threshold
*   credit
*   entry
*   exit
*   interactions. ...
*      _count
*      _children
*      n.id
*      n.type
*      n.timestamp
*      n.weighting
*      n.learner_response
*      n.result
*      n.latency
*      n.description
*      n.objectives._count
*      n.objectives.n.id
*      n.correct_responses._count
*      n.correct_responses.n.pattern
*   launch_data
*   learner_id
*   learner_name
*   learner_preference. ...
*      _children
*      language			*
*      delivery_speed		*
*      audio_captioning	*
*      audio_level			*
*   location
*   max_time_allowed
*   mode
*   objectives. ...
*      _children
*      _count
*      n.id
*      n.score._children
*      n.score.scaled
*      n.score.raw
*      n.score.min
*      n.score.max
*      n.success_status
*      n.completion_status
*      n.description
*   progress_measure
*   scaled_passing_score
*   score. ...
*      scaled
*      raw
*      max
*      min
*   session_time
*   success_status
*   suspend_data
*   time_limit_action
*   total_time
* adl. :
*   nav. ...
*      request
*         request_valid.continue
*         request_valid.previous
*         request_valid.continueCollection (special workaround for adl.nav.request_valid.continue{target=URI}
* 
* Private Properties:
*   comments_from_learner.__array
*   comments_from_learner.__itemType
*   comments_from_lms.__array
*   comments_from_lms.__itemType
*   interactions.__array
*   interactions.__itemType
*   objectives.__array
*   objectives.__itemType
* 
* Private Methods:
*   getChildPropertyNames()
*/

//STATIC CONSTANTS
Scorm2004DataModel.ACCESS =
{
    ReadOnly: 0,
    WriteOnly: 1,
    ReadWrite: 2
};

//types
Scorm2004DataModel.DATA_TYPE =
{
    LongId: 0,
    Real: 1,
    Integer: 2,
    CharString: 3,
    CharStringLocal: 4,
    Time: 5,
    TimeInterval: 6,
    Language: 7,
    Vocabulary:
	{
	    Bool: "-1|0|1",
	    CompletionStatus: "completed|incomplete|not attempted|unknown",
	    Credit: "credit|no-credit",
	    Entry: "ab-initio|resume|",
	    Exit: "time-out|suspend|logout|normal|",
	    InteractionType: "true-false|choice|fill-in|long-fill-in|likert|matching|performance|sequencing|numeric|other",
	    Mode: "browse|normal|review",
	    NavRequest: "continue|previous|{target=}choice|{target=}jump|exit|exitAll|abandon|abandonAll|suspendAll|_none_",
	    NavRequestValid: "true|false|unknown",
	    SuccessStatus: "passed|failed|unknown",
	    InteractionResult: "correct|incorrect|unanticipated|neutral|[Real]",
	    TimeLimitAction: "exit,message|exit,no message|continue,message|continue,no message"
	}
};


//CONSTRUCTOR

function Scorm2004DataModel() {
    //use shorthand references for convenience below
    var a = Scorm2004DataModel.ACCESS;
    var t = Scorm2004DataModel.DATA_TYPE;

    this.scoIdentifier = null;
    this.cmi = {};
    this.adl = {};
    //cmi data model
    this.cmi._version = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.CharString); //REQ_55        
    this.cmi.comments_from_learner = //REQ_57
	{
	__array: new Array(),
	__itemType: "Scorm2004Comment()",
	_children: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.CharString), //REQ_57.1
	_count: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Integer, "0:..") //REQ_57.2
};

    this.cmi.completionStatusChangedDuringRuntime = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.cmi.progressMeasureChangedDuringRuntime = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.cmi.successStatusChangedDuringRuntime = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.cmi.measureChangedDuringRuntime = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);

    this.cmi.availableChildren = new Array();

    this.cmi.comments_from_lms = //REQ_58
	{
	__array: new Array(),
	__itemType: "Scorm2004Comment()",
	_children: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.CharString), //REQ_58.1
	_count: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Integer, "0:..") //REQ_58.2
};
    this.cmi.completion_status = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary, t.Vocabulary.CompletionStatus); //REQ_59
    this.cmi.completion_threshold = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Real, "0.0:1.0"); //REQ_60
    this.cmi.credit = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Vocabulary, t.Vocabulary.Credit); //REQ_61
    this.cmi.entry = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Vocabulary, t.Vocabulary.Entry); //REQ_62
    this.cmi.exit = new Scorm2004DataItem(this.cmi, a.WriteOnly, t.Vocabulary, t.Vocabulary.Exit); //REQ_63
    //Added by chetu
    this.cmi.activityAttemptCount = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Integer, "0:..");
    this.cmi.activityEffectiveAttemptCount = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Integer, "0:..");

    this.cmi.interactions = //REQ_64
	{
	__array: new Array(),
	__itemType: "Scorm2004Interaction()",
	_children: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.CharString), //REQ_64.1
	_count: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Integer, "0:..") //REQ_64.2
};
    this.cmi.launch_data = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.CharString, 4000); //REQ_65
    this.cmi.learner_id = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.LongId, 4000); //REQ_66
    this.cmi.learner_name = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.CharStringLocal, 250); //REQ_67
    this.cmi.learner_preference = //REQ_68
	{
	_children: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.CharString), //REQ_68.1
	language: new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Language, 250), //REQ_68.2
	delivery_speed: new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real, "0:.."), //REQ_68.3
	audio_captioning: new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary, t.Vocabulary.Bool), //REQ_68.4
	audio_level: new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real, "0:..") //REQ_68.5
};
    this.cmi.location = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.CharString, 1000); //REQ_69
    this.cmi.max_time_allowed = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.TimeInterval); //REQ_70
    this.cmi.mode = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Vocabulary, t.Vocabulary.Mode); //REQ_71
    this.cmi.objectives = //REQ_72
	{
	__array: new Array(),
	__itemType: "Scorm2004Objective()",
	_children: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.CharString), //REQ_72.1
	_count: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Integer, "0:..") //REQ_72.2
	};

    this.cmi.globalObjectives = //REQ_72
	{
	    __array: new Array(),
	    __itemType: "Scorm2004Objective()",
	    _children: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.CharString), //REQ_72.1
	    _count: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Integer, "0:..") //REQ_72.2
	};
    this.cmi.progress_measure = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real, "0.0:1.0"); //REQ_73
    this.cmi.scaled_passing_score = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Real, "-1.0:1.0"); //REQ_74
    this.cmi.score = //REQ_75
	{
	_children: new Scorm2004DataItem(this.cmi, a.ReadOnly, t.CharString), //REQ_75.1
	scaled: new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real, "-1.0:1.0"), //REQ_75.2
	raw: new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real), //REQ_75.3
	max: new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real), //REQ_75.4
	min: new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real) //REQ_75.5
};
    this.cmi.session_time = new Scorm2004DataItem(this.cmi, a.WriteOnly, t.TimeInterval); //REQ_76
    this.cmi.success_status = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary, t.Vocabulary.SuccessStatus); //REQ_77
    this.cmi.suspend_data = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.CharString, 64000); //REQ_78
    this.cmi.time_limit_action = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.Vocabulary, t.Vocabulary.TimeLimitAction); //REQ_79
    this.cmi.total_time = new Scorm2004DataItem(this.cmi, a.ReadOnly, t.TimeInterval); //REQ_80

    //Added by chetu 
    this.cmi.isActive = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary.Bool);
    //Added by chetu 
    this.cmi.isSuspended = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary.Bool);
    //Added by chetu 
    this.cmi.attemptCompletionAmount = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real); //
    
    this.cmi.objectiveProgressStatus = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary.Bool);
    this.cmi.objectiveSatisfiedStatus = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary.Bool);
    this.cmi.objectiveMeasureStatus = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary.Bool);
    this.cmi.objectiveNormalizedMeasure = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real);
    this.cmi.attemptProgressStatus = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary.Bool); //
    this.cmi.attemptCompletionStatus = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary.Bool); //
    this.cmi.activityProgressStatus = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary.Bool);
    this.cmi.attemptCompletionAmountStatus = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Vocabulary.Bool);
    
    //initialize the array _children property values
    this.cmi.score._children.initialize(Scorm2004DataModel.getChildPropertyNames(this.cmi.score));
    this.cmi.comments_from_learner._children.initialize(Scorm2004DataModel.getChildPropertyNames(new Scorm2004Comment()));
    this.cmi.comments_from_lms._children.initialize(Scorm2004DataModel.getChildPropertyNames(new Scorm2004Comment()));
    this.cmi.interactions._children.initialize(Scorm2004DataModel.getChildPropertyNames(new Scorm2004Interaction()));
    this.cmi.learner_preference._children.initialize(Scorm2004DataModel.getChildPropertyNames(this.cmi.learner_preference));
    this.cmi.objectives._children.initialize(Scorm2004DataModel.getChildPropertyNames(new Scorm2004Objective()));

    //initialize array _count properties to "0"
    this.cmi.comments_from_learner._count.initialize("0", true);
    this.cmi.comments_from_lms._count.initialize("0", true);
    this.cmi.interactions._count.initialize("0", true);
    this.cmi.objectives._count.initialize("0", true);

    //added by chetu
    this.cmi.isActive.initialize(false);
    //added by chetu
    this.cmi.isSuspended.initialize(false);

    //Added by chetu 
    this.cmi.attemptCompletionAmount.initialize(0.0);
    this.cmi.objectiveProgressStatus.initialize(false);
    this.cmi.objectiveSatisfiedStatus.initialize(false);
    this.cmi.objectiveMeasureStatus.initialize(false);
    this.cmi.objectiveNormalizedMeasure.initialize(0.0);
    this.cmi.attemptProgressStatus.initialize(false);
    this.cmi.attemptCompletionStatus.initialize(false);
    this.cmi.activityProgressStatus.initialize(false);
    this.cmi.attemptCompletionAmountStatus.initialize(false);
   

    this.cmi.completionStatusChangedDuringRuntime.initialize(false);
    this.cmi.progressMeasureChangedDuringRuntime.initialize(false);
    this.cmi.successStatusChangedDuringRuntime.initialize(false);
    this.cmi.measureChangedDuringRuntime.initialize(false);

    //Added by chetu to do activity Attempt Count.
    this.cmi.activityAttemptCount.initialize(0);
    this.cmi.activityEffectiveAttemptCount.initialize(0);

    //initialize any data elements that have initial values
    this.cmi._version.initialize("1.0"); //REQ_55.3
    this.cmi.completion_status.initialize("unknown"); //REQ_59.3
    this.cmi.credit.initialize("credit"); //REQ_61.3
    this.cmi.entry.initialize("ab-initio"); //REQ_62.3.1F
    this.cmi.exit.initialize(""); //REQ_63.3

    if (CurrentPageLanguage != "")
    { this.cmi.learner_preference.language.initialize(CurrentPageLanguage); } //REQ_68.2.2
    else
    { this.cmi.learner_preference.language.initialize("en-US"); } //REQ_68.2.2

    this.cmi.learner_preference.delivery_speed.initialize("1"); //REQ_68.3.3
    this.cmi.learner_preference.audio_captioning.initialize("0"); //REQ_68.4.3
    this.cmi.learner_preference.audio_level.initialize("1"); //no documented requirement, but this is listed in the 2004 RTE description
    this.cmi.mode.initialize("normal"); //REQ_71.3
    this.cmi.success_status.initialize("unknown"); //REQ_77.3
    this.cmi.time_limit_action.initialize("continue,no message"); //listed in the RTE, not the specs
    this.cmi.total_time.initialize("PT0H0M0S"); //REQ_80.4
    
    //set session time but don't mark it as initialized
    this.cmi.session_time.value = "PT0H0M0S"; //REQ_80.4

    // Scorm CAM specification 3.4.1.15
    this.cmi.completion_threshold.initialize(1.0); 

    //adl data model
    this.adl.nav = {};
    this.adl.nav.request = new Scorm2004DataItem(this.adl.nav, a.ReadWrite, t.Vocabulary, t.Vocabulary.NavRequest); //REQ_47

    this.adl.nav.request_valid = {};
    this.adl.nav.request_valid._continue = new Scorm2004DataItem(this.adl.nav, a.ReadOnly, t.Vocabulary, t.Vocabulary.NavRequestValid); //REQ_47
    this.adl.nav.request_valid.previous = new Scorm2004DataItem(this.adl.nav, a.ReadOnly, t.Vocabulary, t.Vocabulary.NavRequestValid); //REQ_47

    /*	
    NOTE: the next element is not used yet.
    I started to set it up, but since it's read only, for now I just 
    put a special workaround in getDataElement() to always return 
    the temporary object below: this.adl.nav.request_valid.choice.
		
    The next element will become a special workaround for the actual element
    called by "adl.nav.request_valid.choice.{target=<SOME_URI>}"
    */
    //this.adl.nav.request_valid.choice = new Array();

    this.adl.nav.request_valid.choice = new Scorm2004DataItem(this.adl.nav.request_valid, a.ReadOnly, t.Vocabulary, t.Vocabulary.NavRequestValid);

    this.adl.nav.request.initialize("_none_");
    this.adl.nav.request_valid._continue.initialize("unknown");
    this.adl.nav.request_valid.previous.initialize("unknown");
    this.adl.nav.request_valid.choice.initialize("unknown");
};


//STATIC METHODS

/**
* Returns a comma-delimited list of all of the specified dataElement's 
* standard child property names, excluding the property called "_children". 
* A non-standard child property starts with double underscores and will be
* excluded as well.
*
* @param dataElement is not a Scorm12DataItem but is an Object in the data model
* 			such as "[datamodel].core" that has a set of child property names 
*			like "credit", "entry", "exit, etc.
*/
Scorm2004DataModel.getChildPropertyNames = function (dataElement) {
    var keys = "";
    var key;

    if (typeof dataElement == "object") {
        for (key in dataElement) {
            if (key != "_children" && key.substr(0, 2) != "__")
                keys += key + ",";
        }

        //strip the last comma
        if (keys.length > 0)
            keys = keys.substr(0, keys.length - 1);
    }

    return keys;
}
