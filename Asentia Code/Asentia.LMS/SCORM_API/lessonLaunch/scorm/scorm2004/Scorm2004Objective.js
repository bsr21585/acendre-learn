﻿/**
* Scorm2004Objective
*
* Public Properties:
*   value
*   id
*   success_status
*   completion_status
*   description
*   score._children
*   score.scaled
*   score.raw
*   score.min
*   score.max
*/
function Scorm2004Objective() {
    //use shorthand references for convenience below
    var a = Scorm2004DataModel.ACCESS;
    var t = Scorm2004DataModel.DATA_TYPE;

    this.id = new Scorm2004DataItem(this, a.ReadWrite, t.LongId, 4000); //REQ_72.3
    this.completion_status = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary, t.Vocabulary.CompletionStatus); //REQ_72.6
    this.success_status = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary, t.Vocabulary.SuccessStatus); //REQ_72.5
    this.description = new Scorm2004DataItem(this, a.ReadWrite, t.CharStringLocal, 250);  //REQ_72.7
    this.progress_measure = new Scorm2004DataItem(this, a.ReadWrite, t.Real, "0.0:1.0"); //missing requirement #
    //Added by chetu
    this.objectiveProgressStatus = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.objectiveSatisfiedStatus = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.objectiveMeasureStatus = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.objectiveNormalizedMeasure = new Scorm2004DataItem(this, a.ReadWrite, t.Real);
    this.attemptProgressStatus = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.attemptCompletionStatus = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.objectiveContributestoRollup = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.attemptCompletionAmount = new Scorm2004DataItem(this, a.ReadWrite, t.Real);
    this.attemptCompletionAmountStatus = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.attemptAbsoluteDuration = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real); //
    this.attemptExperiencedDuration = new Scorm2004DataItem(this.cmi, a.ReadWrite, t.Real); //

    this.completionStatusChangedDuringRuntime = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.progressMeasureChangedDuringRuntime = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.successStatusChangedDuringRuntime = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);
    this.measureChangedDuringRuntime = new Scorm2004DataItem(this, a.ReadWrite, t.Vocabulary.Bool);

    this.score =
	{
	    _children: new Scorm2004DataItem(this, a.ReadOnly, t.CharString), //REQ_72.4.1
	    scaled: new Scorm2004DataItem(this, a.ReadWrite, t.Real, "-1.0:1.0"), //REQ_72.4.2
	    raw: new Scorm2004DataItem(this, a.ReadWrite, t.Real), //REQ_72.4.3
	    max: new Scorm2004DataItem(this, a.ReadWrite, t.Real), //REQ_72.4.5
	    min: new Scorm2004DataItem(this, a.ReadWrite, t.Real) //REQ_72.4.4
	};

    this.score._children.initialize(Scorm2004DataModel.getChildPropertyNames(this.score), true); //REQ_72.4.1.3

    this.completion_status.initialize("unknown", true); //not a documented requirement but seems like they meant this
    this.success_status.initialize("unknown", true); //REQ_72.5.3
    //Added by chetu
    this.objectiveContributestoRollup.initialize(false);
    this.objectiveProgressStatus.initialize(false);
    this.objectiveSatisfiedStatus.initialize(false);
    this.objectiveMeasureStatus.initialize(false);
    this.objectiveNormalizedMeasure.initialize(0.0);
    this.attemptProgressStatus.initialize(false);
    this.attemptCompletionStatus.initialize(false);
    this.attemptCompletionAmount.initialize(0.0);
    this.attemptCompletionAmountStatus.initialize(false);

    this.attemptAbsoluteDuration.initialize(0.0);
    this.attemptExperiencedDuration.initialize(0.0);

    this.completionStatusChangedDuringRuntime.initialize(false);
    this.progressMeasureChangedDuringRuntime.initialize(false);
    this.successStatusChangedDuringRuntime.initialize(false);
    this.measureChangedDuringRuntime.initialize(false);
};
