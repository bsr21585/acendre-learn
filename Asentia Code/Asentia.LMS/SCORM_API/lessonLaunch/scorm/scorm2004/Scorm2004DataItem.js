﻿/**
 * Scorm2004DataItem
 *
 * Constructor:
 *   new Scorm2004DataItem(parent, accessLevel, dataType[, constraints]);
 *
 * Public Properties:
 *   isImplementedByLMS
 *   isInitialized
 *   value
 *   accessLevel
 *   dataType
 *   constraints
 *
 * Public Properties:
 *   initialize(value)
 */
function Scorm2004DataItem(parent, accessLevel, dataType, constraints) 
{
	this.parent				= parent;
	this.isImplementedByLMS	= true;
	this.isInitialized		= false;
	this.value				= null;
	this.pendingValue		= "";
	this.accessLevel		= accessLevel;
	this.dataType			= dataType;
	this.constraints        = constraints;
	this.setBySco           = false;
};

//PUBLIC METHODS

/**
 * Initlaizes the value of this data item. If "dontFlagAsPropertySet" is true, 
 * the parent Data Model will not be notified that at least one property has been set.
 */
Scorm2004DataItem.method('initialize', function (value, dontFlagAsPropertySet) {
    //don't initialize if value is null	
    if (value == null) {
        return;
    }

    this.value = value;
    this.isInitialized = true;
    this.pendingValue = "";

    //set that at least one property in the containing object was initialized
    //i.e. if this is an interaction,  interaction.atLeastOnePropertySet = true
    if (!dontFlagAsPropertySet && this.parent != undefined && !this.parent.atLeastOnePropertySet)
        this.parent.atLeastOnePropertySet = true;
});
