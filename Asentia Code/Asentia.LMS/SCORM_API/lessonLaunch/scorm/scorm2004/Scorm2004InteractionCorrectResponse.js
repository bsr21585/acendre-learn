﻿/**
 * Scorm2004InteractionCorrectResponse
 *
 * Public Properties:
 *   pattern
 */
function Scorm2004InteractionCorrectResponse()
{
	//use shorthand references for convenience below
	var a = Scorm2004DataModel.ACCESS;
	var t = Scorm2004DataModel.DATA_TYPE;

	this.pattern = new Scorm2004DataItem(this, a.ReadWrite,	t.CharString); //note: need to add validation here depending on the interaction type (REQ_64.7.2.2)
};
