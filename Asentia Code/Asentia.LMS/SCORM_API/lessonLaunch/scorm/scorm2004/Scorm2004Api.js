﻿/**
* Scorm2004Api
* 
* Public Properties:
*    version
* 
* Public SCORM 2004 Methods:
*    Initialize("")
*    Terminate("")
*    Commit("")
*    GetValue(strDataElementName)
*    SetValue(strDataElementName, strDataElementValue)
*    GetLastError()
*    GetErrorString(pstrErrorCode)
*    GetDiagnostic(pstrParam)
*/

//STATIC CONFIG CONSTANTS

Scorm2004Api.MAX_TIME_ALLOWED_INTERVAL_SECONDS = 5; //number of seconds between polling for the max-time-allowed (if set by the lesson)

Scorm2004Api.TEST_SUITE_MODE = false; //true : when testing...adjusts for real world issues and test suite bugs.



//STATIC CONSTANTS (error code numbers are spec REQ's)

Scorm2004Api.ERRORS =
{
    None: { code: "0", descr: "No error" },
    GeneralInitFailure: { code: "102", descr: "General Initialization Failure" },
    AlreadyInitialized: { code: "103", descr: "Already Initialized" },
    Terminated: { code: "104", descr: "Content Instance Terminated" },
    GeneralTermFailure: { code: "111", descr: "General Termination Failure" },
    TermBeforeInit: { code: "112", descr: "Termination Before Initialization" },
    TermAfterTerm: { code: "113", descr: "Termination After Termination" },
    GetDataBeforeInit: { code: "122", descr: "Retrieve Data Before Initialization" },
    GetDataAfterTerm: { code: "123", descr: "Retrieve Data After Termination" },
    SetDataBeforeInit: { code: "132", descr: "Store Data Before Initialization" },
    SetDataAfterTerm: { code: "133", descr: "Store Data After Termination" },
    CommitBeforeInit: { code: "142", descr: "Commit Before Initialization" },
    CommitAfterTerm: { code: "143", descr: "Store After Termination" },
    NonEmptyString: { code: "201", descr: "Non-emptystring value passed to a parameter that requires an empty string" },
    GeneralGetFailure: { code: "301", descr: "General Get Failure" },
    GeneralSetFailure: { code: "351", descr: "General Set Failure" },
    GeneralCommitFailure: { code: "391", descr: "General Commit Failure" },
    DataElementNotDef: { code: "401", descr: "Undefined Data Model Element" },
    DataElementNotImp: { code: "402", descr: "Unimplemented Data Model Element" },
    DataElementNotInit: { code: "403", descr: "Data Model Element Value Not Initialized" },
    DataElementReadOnly: { code: "404", descr: "Data Model Element Is Read Only" },
    DataElementWriteOnly: { code: "405", descr: "Data Model Element Is Write Only" },
    DataValueTypeMismatch: { code: "406", descr: "Data Model Element Type Mismatch" },
    DataValueOutOfRange: { code: "407", descr: "Data Model Element Value Out Of Range" },
    DataDependency: { code: "408", descr: "Data Model Dependency Not Established" },

    // Added by chetu from SCORM_2004_4ED_v1_1_SN_20090814.pdf    
    NB_2_1_1: { code: "NB.2.1-1", descr: "Current Activity is already defined / Sequencing session has already begun" },
    NB_2_1_2: { code: "NB.2.1-2", descr: "Current Activity is not defined / Sequencing session has not begun" },
    NB_2_1_3: { code: "NB.2.1-3", descr: "Suspended Activity is not defined" },
    NB_2_1_4: { code: "NB.2.1-4", descr: "Flow Sequencing Control Mode violation" },
    NB_2_1_5: { code: "NB.2.1-5", descr: "Flow or Forward Only Sequencing Control Mode violation" },
    NB_2_1_6: { code: "NB.2.1-6", descr: "No activity is “previous” to the root" },
    NB_2_1_7: { code: "NB.2.1-7", descr: "Unsupported navigation request" },
    NB_2_1_8: { code: "NB.2.1-8", descr: "Choice Exit Sequencing Control Mode violation" },
    NB_2_1_9: { code: "NB.2.1-9", descr: "No activities to consider" },
    NB_2_1_10: { code: "NB.2.1-10", descr: "Choice Sequencing Control Mode violation" },
    NB_2_1_11: { code: "NB.2.1-11", descr: "Target activity does not exist" },
    NB_2_1_12: { code: "NB.2.1-12", descr: "Current Activity already terminated" },
    NB_2_1_13: { code: "NB.2.1-13", descr: "Undefined navigation request" },
    TB_2_3_1: { code: "TB.2.3-1", descr: "Current Activity is not defined / Sequencing session has not begun" },
    TB_2_3_2: { code: "TB.2.3-2", descr: "Current Activity already terminated" },
    TB_2_3_3: { code: "TB.2.3-3", descr: "Cannot suspend an inactive root" },
    TB_2_3_4: { code: "TB.2.3-4", descr: "Activity tree root has no parent" },
    TB_2_3_5: { code: "TB.2.3-5", descr: "Nothing to suspend; No active activities" },
    TB_2_3_6: { code: "TB.2.3-6", descr: "Nothing to abandon; No active activities" },
    TB_2_3_7: { code: "TB.2.3-7", descr: "Undefined termination request" },
    SB_2_1_1: { code: "SB.2.1-1", descr: "Last activity in the tree" },
    SB_2_1_2: { code: "SB.2.1-2", descr: "Cluster has no available children" },
    SB_2_1_3: { code: "SB.2.1-3", descr: "No activity is “previous” to the root" },
    SB_2_1_4: { code: "SB.2.1-4", descr: "Forward Only Sequencing Control Mode violation" },
    SB_2_2_1: { code: "SB.2.2-1", descr: "Flow Sequencing Control Mode violation" },
    SB_2_2_2: { code: "SB.2.2-2", descr: "Activity unavailable" },
    SB_2_4_1: { code: "SB.2.4-1", descr: "Forward Traversal Blocked" },
    SB_2_4_2: { code: "SB.2.4-2", descr: "Forward Only Sequencing Control Mode violation" },
    SB_2_4_3: { code: "SB.2.4-3", descr: "No activity is “previous” to the root" },
    SB_2_5_1: { code: "SB.2.5-1", descr: "Current Activity is defined / Sequencing session already begun" },
    SB_2_6_1: { code: "SB.2.6-1", descr: "Current Activity is defined / Sequencing session already begun" },
    SB_2_6_2: { code: "SB.2.6-2", descr: "No Suspended Activity defined" },
    SB_2_7_1: { code: "SB.2.7-1", descr: "Current Activity is not defined / Sequencing session has not begun" },
    SB_2_7_2: { code: "SB.2.7-2", descr: "Flow Sequencing Control Mode violation" },
    SB_2_7_3: { code: "SB.2.7-3", descr: "Flow Sequencing Control Mode violation" },
    SB_2_8_1: { code: "SB.2.8-1", descr: "Current Activity is not defined / Sequencing session has not begun" },
    SB_2_8_2: { code: "SB.2.8-2", descr: "Flow Sequencing Control Mode violation" },
    SB_2_9_1: { code: "SB.2.9-1", descr: "No target for Choice" },
    SB_2_9_2: { code: "SB.2.9-2", descr: "Target activity does not exist or is unavailable" },
    SB_2_9_3: { code: "SB.2.9.3", descr: "Target activity hidden from choice" },
    SB_2_9_4: { code: "SB.2.9-4", descr: "Choice Sequencing Control Mode violation" },
    SB_2_9_5: { code: "SB.2.9-5", descr: "No activities to consider" },
    SB_2_9_6: { code: "SB.2.9-6", descr: "Unable to activate target; target is not a child of the Current Activity" },
    SB_2_9_7: { code: "SB.2.9-7", descr: "Choice Exit Sequencing Control Mode violation" },
    SB_2_9_8: { code: "SB.2.9-8", descr: "Unable to choose target activity – constrained choice" },
    SB_2_9_9: { code: "SB.2.9-9", descr: "Choice request prevented by Flow-only activity" },
    SB_2_10_1: { code: "SB.2.10-1", descr: "Current Activity is not defined / Sequencing session has not begun" },
    SB_2_10_2: { code: "SB.2.10-2", descr: "Current Activity is active or suspended" },
    SB_2_10_3: { code: "SB.2.10-3", descr: "Flow Sequencing Control Mode violation" },
    SB_2_11_1: { code: "SB.2.11-1", descr: "Current Activity is not defined / Sequencing session has not begun" },
    SB_2_11_2: { code: "SB.2.11-2", descr: "Current Activity has not been terminated" },
    SB_2_12_1: { code: "SB.2.12-1", descr: "Undefined sequencing request" },
    SB_2_13_1: { code: "SB.2.13-1", descr: "Current Activity is not defined / Sequencing session has not begun" },
    DB_1_1_1: { code: "DB.1.1-1", descr: "Cannot deliver a non-leaf activity" },
    DB_1_1_2: { code: "DB.1.1-2", descr: "Nothing to deliver" },
    DB_1_1_3: { code: "DB.1.1-3", descr: "Activity unavailable" },
    DB_2_1: { code: "DB.2-1", descr: "Identified activity is already active" }

};

Scorm2004Api.STATE =
{
    NotInitialized: 0,
    Running: 1,
    TerminateStarted: 2,
    Terminated: 3
};

Scorm2004Api.DATA_STATE =
{
    Unsaved: 0,
    Saved: 1
};

Scorm2004Api.COMMIT_STATE =
{
    NotCommitted: 0,
    Committing: 1,
    Committed: 2
};

Scorm2004Api.RETURN_VALUE =
{
    True: "true",
    False: "false",
    Empty: ""
};

var scorm2004ApiInstance = null;
var setvalueCompleted = false;
var waitingTime = 0;
var TerminateSCOwaitingTime = 0;
var singleScoReturnWaitingTime = 0;
var sessionStarted = false;
var terminateCompleted = false;
var interval;
var returnSingleSco;
var terminateInterval = null;
var pendingNavigationRequest = null;

//CONSTRUCTOR 

function Scorm2004Api(courseLaunchController) {

    scorm2004ApiInstance = this;

    // reference the course launch controller
    this.courseLaunchController = courseLaunchController;

    this.alertAndConfirmWindow = window; // override this by calling setAlertConfirmOwner(someOtherWindow)

    // data models
    this.dataModel = new Scorm2004DataModel(); // LOSE THIS SOON    
    this.activeDataModel = new Scorm2004DataModel();
    this.courseDataModel = new Array();
    this.imsManifest = null;
    this.activeSCOIdentifier = null;
    this.activeSCOIndex = null;
    this.previousSCOIdentifier = null;
    this.nextSCOIdentifier = null;
    this.completion_status = "unknown";
    this.success_status = "unknown";
    this.total_time = 0;
    this.entry = "ab-initio";
    this.activityAttemptCount = new Array();
    this.availableChildrens = new Array();
    this.actualAttemptCount = 0;
    this.effectiveAttemptCount = 0;
    //score
    this.score =
            {
                max: this.dataModel.cmi.score.max.value,
                min: this.dataModel.cmi.score.min.value,
                raw: this.dataModel.cmi.score.raw.value,
                scaled: this.dataModel.cmi.score.scaled.value
            };
    // intialize the state variables
    this.activeState = Scorm2004Api.STATE.NotInitialized;
    this.activeCommitState = Scorm2004Api.COMMIT_STATE.NotCommitted;
    this.activeDataState = Scorm2004Api.DATA_STATE.Saved;

    // initialize the error variable (REQ_3.1)
    this.activeError = Scorm2004Api.ERRORS.None;

    // start the session timer
    this.sessionTimeStart = new Date();
    this.lastLaunchTotalTimeInMS = 0; // this is initialized in deserialze();
    this.timeoutDateTime = null;

    this.queueCommitOnSaverResponse = false;
    this.queueFinishOnSaverResponse = false;
    this.onFinishCompletedCalled = false;

    // public property "version" per REQ_2.6, REQ_2.6.1
    this.version = "1.0. Scorm2004Api";

    this.scorm2004ADLNav = new Scorm2004ADLNavChoice(this);

    this.globalObjectives = new Array();
    this.primaryObjectives = new Array();
    this.saveRTECallFinished = false;
    this.globalSaved = false;

    this.callDatabaseSaveOnNextCommit = false;

    /**
    * Call this to update the over all package status attribute values i.e global data model values or global status values. 
    */
    Scorm2004Api.prototype.fillGlobalModelValues = function () {
        this.DebuggerTraceCall('fillGlobalModelValues');        

        //Added to Add actual attempt count and effective Attempt count
        this.actualAttemptCount = this.scorm2004ADLNav.SetGlobalActualAttemptCount(0);        
        this.effectiveAttemptCount = this.scorm2004ADLNav.SetGlobalEffectiveAttemptCount(0);

        // fill global total time
        this.total_time = 0;
        for (var activityIndex = 0; activityIndex < this.courseDataModel.length; activityIndex++) {

            // fetch identifier ref to check the activity is leaf or non leaf activity
            var identifierref = this.scorm2004ADLNav.GetItemIdentifierRef(activityIndex);

            // if identifierref value is not null, it means it is a leaf activity
            if (identifierref != null) {
                this.total_time += this.timeIntervalToMs(this.courseDataModel[activityIndex].cmi.total_time.value);                
            }
        }

        // convert total time from ms to seconds 
        this.total_time = this.total_time / 1000;


        // fill globle success vale and completion value
        this.success_status = this.scorm2004ADLNav.ComputeSuccessStatus(0);
        this.completion_status = this.scorm2004ADLNav.ComputeCompletionStatus(0);

        var scoreMaxValue = 0;
        var scoreMinValue = 0;
        var scoreRawValue = 0;
        var scoreScaleValue = 0;

        var count = 0;

        for (var index = 1; index < this.courseDataModel.length; index++) {

            if (this.courseDataModel[index].cmi.score != null && this.courseDataModel[index].cmi.score != undefined && this.courseDataModel[index].cmi.score.scaled.value != null && this.courseDataModel[index].cmi.score.scaled.value != '' && this.courseDataModel[index].cmi.score.max.value > 0) {
                count = count + 1;
                scoreMaxValue = scoreMaxValue + this.courseDataModel[index].cmi.score.max.value != null ? this.courseDataModel[index].cmi.score.max.value : 0;
                scoreMinValue = scoreMinValue + this.courseDataModel[index].cmi.score.min.value != null ? this.courseDataModel[index].cmi.score.min.value : 0;
                scoreRawValue = scoreRawValue + this.courseDataModel[index].cmi.score.raw.value != null ? this.courseDataModel[index].cmi.score.raw.value : 0;
                scoreScaleValue = scoreScaleValue + this.courseDataModel[index].cmi.score.scaled.value != null ? this.courseDataModel[index].cmi.score.scaled.value : 0;
            }
        }

        if (count == 0) {
            scoreMaxValue = null;
            scoreMinValue = null;
            scoreRawValue = null;
            scoreScaleValue = null;
        }
        else {
            scoreMaxValue = scoreMaxValue / count;
            scoreMinValue = scoreMinValue / count;
            scoreRawValue = scoreRawValue / count;
            scoreScaleValue = scoreScaleValue / count;
        }

        // fill global score
        this.score =
               {
                   max: scoreMaxValue,
                   min: scoreMinValue,
                   raw: scoreRawValue,
                   scaled: scoreScaleValue
               };

        // fill global activityAttemptCount, availableChildrens 
        this.activityAttemptCount = new Array();
        this.availableChildrens = new Array();        

        for (var index = 0; index < this.courseDataModel.length; index++) {
            this.activityAttemptCount.push(this.courseDataModel[index].cmi.activityAttemptCount.value);
            this.availableChildrens.push(this.courseDataModel[index].cmi.availableChildren);
        }

         // as soon as the global data is saved, the entry value will changed from 'ab-inito' to 'resume'
        this.entry = "resume";
    }

};

Scorm2004Api.inherits(EventDispatcher);

//PUBLIC METHODS

/**
* Call this to have alerts and confirms open from another window besides this one.
*/
Scorm2004Api.method('setAlertConfirmOwner', function (windowHandle) {

    this.DebuggerTraceCall('setAlertConfirmOwner');
    this.alertAndConfirmWindow = windowHandle;
});

/**
* Called by the CourseLaunchController to find out if this API is either in the process of finishing or has already finished.
*/
Scorm2004Api.method('isFinishingOrFinished', function () {
    this.DebuggerTraceCall('isFinishingOrFinished');
    return (this.activeState == Scorm2004Api.STATE.TerminateStarted || this.activeState == Scorm2004Api.STATE.Terminated);
});

/**
* Called by the CourseLaunchController to save the data if it has not already been saved.
* Simply calls Terminate.
*/
Scorm2004Api.method('finishApi', function () {
    this.DebuggerTraceCall('finishApi');
    this.Terminate("");
});

/**
* Called by the CourseLaunchController to prevent any subsequent public methods from being processed or even tracing events.
*/
Scorm2004Api.method('abort', function () {
    this.DebuggerTraceCall('abort');
    this.isAborted = true;
});

/**
* Initialize("") REQ_4
*
* @param pstrEmpty must be be empty-string.
*/
Scorm2004Api.prototype.Initialize = function (pstrEmpty) {

    if (this.isAborted)
        return;

    var methodName = "Initialize";

    this.traceCall(methodName, pstrEmpty);

    try {
        //API cannot be initialized after it was just terminated REQ_4.4
        //if (this.activeState == Scorm2004Api.STATE.Terminated)
        //{
        //	this.setActiveError(Scorm2004Api.ERRORS.Terminated, "", 0);
        //	return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
        //}	 
        //API can only be initialized once REQ_4.3
        if (this.activeState != Scorm2004Api.STATE.NotInitialized) {
            this.setActiveError(Scorm2004Api.ERRORS.AlreadyInitialized, "", 0);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
        }

        //pstrEmpty must be an empty string REQ_3.2, REQ_4.1.1
        if (typeof pstrEmpty != "string" || pstrEmpty.length != 0) {
            this.setActiveError(Scorm2004Api.ERRORS.NonEmptyString, "", 1);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
        }

        //set the state to initialized (running)
        this.activeState = Scorm2004Api.STATE.Running;

        //deserialize the data model based on the Lesson Data Loader's XML DOM
        this.deserialize();

        //begin the session timer
        this.sessionTimeStart = new Date();

        //if a maximum time was set, begin the timer
        if (this.activeDataModel.cmi.max_time_allowed.isInitialized) {
            //calculate when the timeout should occurr
            var startTimeInMs = Number(this.sessionTimeStart);            
            var timeoutInMs = this.timeIntervalToMs(this.activeDataModel.cmi.max_time_allowed.value) - this.lastLaunchTotalTimeInMS;
            
            //if negative interval for some reason, timeout immediately
            if (timeoutInMs < 0)
                timeoutInMs = 0;

            //store the timeout date/time (not used for any purpose)
            this.timeoutDateTime = new Date(startTimeInMs + timeoutInMs);

            this.traceNote("Timeout at " + this.timeoutDateTime + "(time limit: " + this.activeDataModel.cmi.max_time_allowed.value + "; total time: " + (this.lastLaunchTotalTimeInMS / 1000) + "s; remaining time limit: " + (timeoutInMs / 1000) + "s)");

            //start the time-out stop-watch - check every second to see if the time limit has been reached

            var thisInstance = this;
            this.checkTimeoutInterval = window.setInterval(
				function () {
				    thisInstance.checkTimeout();
				},
				Scorm2004Api.MAX_TIME_ALLOWED_INTERVAL_SECONDS * 1000
			);
        }

        //Initialize successful, reset error, Return true REQ_4.5
        this.setActiveError(Scorm2004Api.ERRORS.None);
        return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.True);
    }
    catch (e) {
        //REQ_4.2
        this.traceNote("Caught an error in Initialize: " + e);
        this.setActiveError(Scorm2004Api.ERRORS.GeneralInitFailure, "", 1);
        return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
    }
};

/**
* Terminate("") REQ_5
*
* @param pstrEmpty must be be empty-string.
*/

Scorm2004Api.prototype.Terminate = function (pstrEmpty) {
    this.DebuggerTraceCall('Terminate');

    terminateCompleted = false;
    if (this.isAborted)
        return;

    var methodName = "Terminate";

    this.traceCall(methodName, pstrEmpty);

    try {

        // if terminate request is for single SCO package activity then return to LMS
        // TODO: RETURN TO ORIGINAL CONDITION BEFORE CHECK IN AND AFTER CHETU INVESTIGATES
        //if (this.imsManifest.organizations[0].items.length == 2) {
            //var returnSingleSco = setInterval(ReturnToLMSForSingleSCO, 1000);            
        //}

        //make sure only 1 Terminate process happens simultaneously
        if (this.activeState != Scorm2004Api.STATE.TerminateStarted) {
            //API cannot be terminiated if it's already terminated REQ_5.5
            if (this.activeState == Scorm2004Api.STATE.Terminated) {
                this.setActiveError(Scorm2004Api.ERRORS.TermAfterTerm, "", 0);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            //API must have been initialized REQ_5.4
            if (this.activeState == Scorm2004Api.STATE.NotInitialized) {
                this.setActiveError(Scorm2004Api.ERRORS.TermBeforeInit, "", 0);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            //pstrEmpty must be an empty string REQ_3.2, REQ_5.1.1
            if (typeof pstrEmpty != "string" || pstrEmpty.length != 0) {
                this.setActiveError(Scorm2004Api.ERRORS.NonEmptyString, "", 1);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            //set the state to Terminate Started
            this.activeState = Scorm2004Api.STATE.TerminateStarted;

            //dispatch the FINISH_STARTED event so the controller can close the SCO window while it is processing
            this.dispatchEvent(new Event(Event.FINISH_STARTED, "Scorm2004Api"));

            //if the RTEDataSaver is still processing, queue Terminate for when it returns
            if (this.activeCommitState == Scorm2004Api.COMMIT_STATE.Committing) {
                this.queueFinishOnSaverResponse = true;

                this.traceNote("Terminate has been queued until the RTEDataSaver completes the pending Commit.");
                this.setActiveError(Scorm2004Api.ERRORS.None);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.True);
            }

            //If the call to here was a result of a queued Terminate, trace this note and clear the queue flag
            if (this.queueFinishOnSaverResponse) {
                this.traceNote("This call resulted from a queued Terminate call.");
                this.queueFinishOnSaverResponse = false;

                if (this.queueCommitOnSaverResponse) {
                    this.traceNote("Queued Commit call will be bypassed.");
                    this.queueCommitOnSaverResponse = false;
                }
            }

            //if Terminate was called as a result of a timeout...
            if (this.terminateFromTimeout) {
                this.traceNote("Terminate triggerd by timeout.");
            }

            //Commit REQ_5.2.1
            if (this.Commit("") == Scorm2004Api.RETURN_VALUE.False) {
                //Return commit failure error REQ_5.2.1.1
                this.setActiveError(Scorm2004Api.ERRORS.GeneralCommitFailure, "", 0);
                this.activeState = Scorm2004Api.STATE.Running;
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }
        }

        //Terminate successful, reset error, Return true REQ_5.2
        this.activeState = Scorm2004Api.STATE.NotInitialized;
        this.setActiveError(Scorm2004Api.ERRORS.None);
        this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.True);

        terminateInterval = setInterval(CallOverallSequencingOnTerminateCompleted, 500);
        terminateCompleted = true;

        return Scorm2004Api.RETURN_VALUE.True;
    }
    catch (e) {
        //REQ_5.3        
        this.traceNote("Caught an error in Terminate: " + e);
        this.setActiveError(Scorm2004Api.ERRORS.GeneralTermFailure, "", 1);
        return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
    }
};

/**
* Commit("") REQ_8
*
* @param pstrEmpty must be be empty-string.
*/

Scorm2004Api.prototype.Commit = function (pstrEmpty) {

    if (this.isAborted) {        
        return;
    }

    var methodName = "Commit";

    this.traceCall(methodName, pstrEmpty);

    try {
        //API must not be terminated REQ_8.4
        if (this.activeState == Scorm2004Api.STATE.Terminated) {
            this.setActiveError(Scorm2004Api.ERRORS.CommitAfterTerm, "", 0);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
        }

        //API must have been initialized REQ_8.5
        if (this.activeState == Scorm2004Api.STATE.NotInitialized) {
            this.setActiveError(Scorm2004Api.ERRORS.CommitBeforeInit, "", 0);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
        }

        //pstrEmpty must be an empty string REQ_8.1.1
        if (typeof pstrEmpty != "string" || pstrEmpty.length != 0) {
            this.setActiveError(Scorm2004Api.ERRORS.NonEmptyString);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
        }
        
        //if in browse or review mode, bypass commit
        if (this.courseDataModel[this.activeSCOIndex].cmi.mode.value == "browse" || this.courseDataModel[this.activeSCOIndex].cmi.mode.value == "review") {
            this.traceNote("Bypassing commit because mode is \"" + this.courseDataModel[this.activeSCOIndex].cmi.mode.value + "\".");
            this.setActiveError(Scorm2004Api.ERRORS.None);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.True);
        }

        //Don't commit if SetData hasn't been called since last Commit REQ_8.2.1
        if (this.activeState != Scorm2004Api.STATE.TerminateStarted && this.activeDataState == Scorm2004Api.DATA_STATE.Saved) {
            this.queueCommitOnSaverResponse = false;

            this.traceNote("Bypassing commit because no data has been set since the last call to Commit.");
            this.setActiveError(Scorm2004Api.ERRORS.None);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.True);
        }

        //If currently commiting, queue up a commit for when the RTEDataSaver completes the previous Commit.
        if (this.activeCommitState == Scorm2004Api.COMMIT_STATE.Committing) {
            this.queueCommitOnSaverResponse = true;

            this.traceNote("Commit has been queued until the RTEDataSaver completes the pending Commit.");
            this.setActiveError(Scorm2004Api.ERRORS.None);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.True);
        }

        //If the call to here was a result of a queued Commit, trace this note and clear the queue flag
        if (this.queueCommitOnSaverResponse) {
            this.traceNote("This call resulted from a queued Commit call.");
            this.queueCommitOnSaverResponse = false;
        }

        //evaluate overrides
        this.evaluateOverrides("cmi.completion_status");
        this.evaluateOverrides("cmi.success_status");

        //lock the commit by setting the commit state
        this.activeCommitState = Scorm2004Api.COMMIT_STATE.Committing;

        //serialize to the RTEDataSaver
        //save to the database only if this Commit call was called by Terminate; save to XML file otherwise.
        var saveToDB = (this.activeState == Scorm2004Api.STATE.TerminateStarted);

        // override saveToDB if callDatabaseSaveOnNextCommit is true
        if (this.callDatabaseSaveOnNextCommit) {
            saveToDB = true;
        }

        this.serialize(saveToDB);

        //this.scorm2004ADLNav.RunSetrteForItemAndItsObjectives(this.activeSCOIndex);
        this.scorm2004ADLNav.OverallRollupProcess(this.activeSCOIndex);
        this.scorm2004ADLNav.CheckMenu();
        this.scorm2004ADLNav.UpdateActivitiesStatus();

        //Commit successful, but do not change the commit state to Committed until AJAX returns - this is done in commitData_Done
        this.setActiveError(Scorm2004Api.ERRORS.None);

        // after we're done, reset callDatabaseSaveOnNextCommit to false
        this.callDatabaseSaveOnNextCommit = false;

        return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.True);
    }
    catch (e) {
        //REQ_8.3
        this.traceNote("Caught an error in Commit: " + e);
        this.setActiveError(Scorm2004Api.ERRORS.GeneralCommitFailure, "", 1);
        return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
    }
};



/**
* GetValue(pstrDataElementName) REQ_6
*
* @param pstrDataElementName string of the SCORM 1.2 data model element name
* @return string of the data element's value
*/
Scorm2004Api.prototype.GetValue = function (pstrDataElementName) {
    if (this.isAborted)
        return;

    var methodName = "GetValue";

    this.traceCall(methodName, pstrDataElementName);

    try {
        //API must not be terminated REQ_6.9
        if (this.activeState == Scorm2004Api.STATE.Terminated || this.activeState == Scorm2004Api.STATE.TerminateStarted) {
            this.setActiveError(Scorm2004Api.ERRORS.GetDataAfterTerm, "", 0);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.Empty);
        }

        //API must have been initialized REQ_6.8
        if (this.activeState == Scorm2004Api.STATE.NotInitialized) {
            this.setActiveError(Scorm2004Api.ERRORS.GetDataBeforeInit, "", 0);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.Empty);
        }

        pstrDataElementName = this.alterNameForInternalUse(pstrDataElementName);

        //pstrDataElementName must be a defined data model element name REQ_6.3
        if (this.isValidDataElementName(pstrDataElementName)) {
            //data element is defined so get the element for further validation
            var objDataElement = this.getDataElement(pstrDataElementName);
        }
        else if (this.isValidArrayDataElementName(pstrDataElementName)) {
            if (!this.isArrayDataElementIndexInRange(pstrDataElementName)) {
                this.setActiveError(Scorm2004Api.ERRORS.GeneralGetFailure, "", 1);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.Empty);
            }
            else {
                //array data element is defined and index is in range, so get the element for further validation
                var objDataElement = this.getArrayDataElement(pstrDataElementName, false);
            }
        }
        else {
            // this.setActiveError(Scorm2004Api.ERRORS.DataElementNotDef, "", 1);
            //code modified by chetu  
            this.setActiveError(Scorm2004Api.ERRORS.GeneralGetFailure, "", 1);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.Empty);
        }

        //pstrDataElementName must be a data model element implemented by the LMS REQ_6.4
        if (!objDataElement.isImplementedByLMS) {
            this.setActiveError(Scorm2004Api.ERRORS.DataElementNotImp, "", 1);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.Empty);
        }

        //pstrDataElementName must not be write-only REQ_6.6
        if (objDataElement.accessLevel == Scorm2004DataModel.ACCESS.WriteOnly) {
            this.setActiveError(Scorm2004Api.ERRORS.DataElementWriteOnly, "", 0);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.Empty);
        }

        //pstrDataElementName value must have been initialized if not read-only REQ_6.5
        if (!objDataElement.isInitialized) {
            this.setActiveError(Scorm2004Api.ERRORS.DataElementNotInit, "", 0);
            return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.Empty);
        }



        //GetValue successful, reset error, return the value REQ_6.2
        this.setActiveError(Scorm2004Api.ERRORS.None);
        return this.traceReturn(methodName, objDataElement.value);
    }
    catch (e) {
        //REQ_6.7
        this.traceNote("Caught an error in GetValue: " + e);
        this.setActiveError(Scorm2004Api.ERRORS.GeneralGetFailure, "", 1);
        return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.Empty);
    }
};

/**
* SetValue(pstrDataElementName) REQ_7
* @param pstrDataElementName string of the SCORM 2004 data model element name
* @param pstrDataElementValue value that element should be set to
* @return "true" if successful, "false" if not.
*/
Scorm2004Api.prototype.SetValue = function (pstrDataElementName, pstrDataElementValue) {

    setvalueCompleted = false;

    if (this.isAborted)
        return;

    var methodName = "SetValue";

    //replaced dobule quotes, carriage return and new line characters as these were causing issue when deserializing json data.

    if (typeof pstrDataElementValue == "string") {

        pstrDataElementValue = replaceAll('"', '\'', pstrDataElementValue);
        pstrDataElementValue = replaceAll('\r', '', pstrDataElementValue);
        pstrDataElementValue = replaceAll('\n', '', pstrDataElementValue);
    }


    this.traceCall(methodName, pstrDataElementName, pstrDataElementValue);

    try {

        // if the cmi.exit value is suspend then cmi.entry set to resume.
        if (pstrDataElementName == "cmi.exit") {
            if (pstrDataElementValue == "suspend") {
                this.courseDataModel[this.activeSCOIndex].cmi.entry.value = "resume";
            }
            else {
                this.courseDataModel[this.activeSCOIndex].cmi.entry.value = "";
            }
        }

        // if cmi mode is set to "review" from scorm.cs code behind file then 
        // dont alter its value through SetValue function
        if (pstrDataElementName == "cmi.mode" && CmiMode == "review") {
            pstrDataElementValue = "review";
        }

        //Logic to make API wait for completion of pending navigation process to work
        if (pstrDataElementName == "adl.nav.request") {
            if (pstrDataElementValue == "suspend" || pstrDataElementValue == "suspendAll") {
                this.courseDataModel[this.activeSCOIndex].cmi.entry.value = "resume";
                this.courseDataModel[this.activeSCOIndex].cmi.exit.value = "suspend";
            }

            this.scorm2004ADLNav.ReorderChildren();
            scorm2004ApiInstance = this;
            if (sessionStarted == false || this.activeState == Scorm2004Api.STATE.Terminated || this.activeState == Scorm2004Api.STATE.NotInitialized) {

                this.scorm2004ADLNav.OverallSequencingProcess(pstrDataElementValue, this.activeSCOIdentifier);
                sessionStarted = true;
            }
            else if (pendingNavigationRequest == null) {
                pendingNavigationRequest = pstrDataElementValue;

                if (pendingNavigationRequest == 'exit' || pendingNavigationRequest == 'exitAll' || pendingNavigationRequest == "suspend" || pendingNavigationRequest == "suspendAll") {
                    interval = setInterval(function () { StartInterval(SavingContentStr) }, 500);
                }
                else {
                    interval = setInterval(function () { StartInterval(LoadingContentStr) }, 500);
                }
            }
        }
        else {
            //API must not be terminated REQ_7.10
            if (this.activeState == Scorm2004Api.STATE.Terminated || this.activeState == Scorm2004Api.STATE.TerminateStarted) {
                this.setActiveError(Scorm2004Api.ERRORS.SetDataAfterTerm, "", 0);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            //API must have been initialized REQ_7.9
            if (this.activeState == Scorm2004Api.STATE.NotInitialized) {
                this.setActiveError(Scorm2004Api.ERRORS.SetDataBeforeInit, "", 1);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            var blnIsArrayElement = false;
            pstrDataElementName = this.alterNameForInternalUse(pstrDataElementName);

            //pstrDataElementName must be a defined data model element name REQ_7.3
            if (this.isValidDataElementName(pstrDataElementName)) {
                //data element is defined so get the element for further validation
                var objDataElement = this.getDataElement(pstrDataElementName);
            }
            else if (this.isValidArrayDataElementName(pstrDataElementName)) {
                //if not in range, error
                if (!this.isArrayDataElementIndexInRange(pstrDataElementName)) {
                    this.setActiveError(Scorm2004Api.ERRORS.GeneralSetFailure, "", 1);
                    return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
                }
                    //else, return reference to element
                else {
                    //array data element is defined and index is in range, so get the element for further validation
                    var objDataElement = this.getArrayDataElement(pstrDataElementName, true);

                    //set the blnIsArrayElement flag so in case validation errors occurr, we'll remove the element we added as a result of the above line
                    blnIsArrayElement = true;
                }
            }
            else {
                //this.setActiveError(Scorm2004Api.ERRORS.DataElementNotDef, "", 1);
                //code modified by chetu  
                this.setActiveError(Scorm2004Api.ERRORS.GeneralSetFailure, "", 1);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            //pstrDataElementName must be a data model element implemented by the LMS REQ_7.4
            if (!objDataElement.isImplementedByLMS) {
                this.setActiveError(Scorm2004Api.ERRORS.DataElementNotImp, "", 1);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            //pstrDataElementName must not be read-only REQ_7.5
            if (objDataElement.accessLevel == Scorm2004DataModel.ACCESS.ReadOnly) {
                this.setActiveError(Scorm2004Api.ERRORS.DataElementReadOnly, "", 0);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            if (Scorm2004Api.TEST_SUITE_MODE)
                objDataElement.pendingValue = pstrDataElementValue;
            else
                objDataElement.pendingValue = String(pstrDataElementValue);

            //pstrDataElementValue must be valid data type for element pstrDataElementName REQ_7.6
            if (!this.isDataValueTypeValid(objDataElement)) {
                objDataElement.pendingValue = null;

                if (blnIsArrayElement)
                    this.undoArrayElementAppend(objDataElement);

                this.setActiveError(Scorm2004Api.ERRORS.DataValueTypeMismatch, "", 1);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            //pstrDataElementValue must be in the valid value range for element pstrDataElementName REQ_7.7
            if (!this.isDataValueInRange(objDataElement)) {
                objDataElement.pendingValue = null;

                if (blnIsArrayElement)
                    this.undoArrayElementAppend(objDataElement);

                this.setActiveError(Scorm2004Api.ERRORS.DataValueOutOfRange, "", 1);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            //all required element dependencies must be in place REQ_7.8
            if (!this.areRequiredDependenciesInPlace(objDataElement, pstrDataElementName)) {
                objDataElement.pendingValue = null;

                if (blnIsArrayElement) {
                    try {
                        this.undoArrayElementAppend(objDataElement);
                    }
                    catch (e) {
                        this.doJSAlert("Error when calling undoArrayElementAppend(): " + e);
                    }
                }
                this.setActiveError(Scorm2004Api.ERRORS.DataDependency, "", 1);
                return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
            }

            //set the element's value & mark the element as 'initialized'
            var previousValue = objDataElement.value;

            //to validate the setvalue function before initializing any element.
            this.ValidateSetvalue(pstrDataElementName, objDataElement);

            objDataElement.initialize(objDataElement.pendingValue);
            objDataElement.setBySco = true

            //If the setvalue set the cmi.exit suspanded then set the is suspanded property to true (Aug 1st,16)
            if (pstrDataElementName == "cmi.exit" && (pstrDataElementValue == "suspend" || pstrDataElementValue == "suspendAll")) {
                this.courseDataModel[this.activeSCOIndex].cmi.isSuspended.value = true;
            }

            this.scorm2004ADLNav.TrackChangesDuringRuntime(this.activeSCOIndex, pstrDataElementName);
            objDataElement.pendingValue = previousValue;

            if (parseInt(this.activeDataModel.cmi.objectives._count.value) > parseInt(this.courseDataModel[this.activeSCOIndex].cmi.objectives._count.value)) {

                this.courseDataModel[this.activeSCOIndex].cmi.objectives.__array[this.activeDataModel.cmi.objectives._count.value - 1] = this.activeDataModel.cmi.objectives.__array[this.activeDataModel.cmi.objectives._count.value - 1];
                this.courseDataModel[this.activeSCOIndex].cmi.objectives._count.initialize(String(this.activeDataModel.cmi.objectives._count.value));
            }

            if (parseInt(this.activeDataModel.cmi.interactions._count.value) > parseInt(this.courseDataModel[this.activeSCOIndex].cmi.interactions._count.value)) {

                this.courseDataModel[this.activeSCOIndex].cmi.interactions.__array[this.activeDataModel.cmi.interactions._count.value - 1] = this.activeDataModel.cmi.interactions.__array[this.activeDataModel.cmi.interactions._count.value - 1];
                this.courseDataModel[this.activeSCOIndex].cmi.interactions._count.initialize(String(this.activeDataModel.cmi.interactions._count.value));
            }

            //evaluate any relations for this
            this.evaluateOverrides(pstrDataElementName);

            objDataElement.pendingValue = null;

            //TO DO:
            //this.reportToAPIInspector(pstrDataElementName, objDataElement.value);

            //set the data state to Unsaved
            this.activeDataState = Scorm2004Api.DATA_STATE.Unsaved;

            var objectiveIndex;
            if (pstrDataElementName.indexOf("cmi.objectives.") > -1) {
                objectiveIndex = pstrDataElementName.split('.')[2];
            }

            // if this is a single sco package, and completion is just changing to "completed", then set the flag to save to database on next commit            
            if (this.courseDataModel.length == 2 && pstrDataElementName == "cmi.completion_status" && (previousValue == "unknown" || previousValue == "incomplete") && objDataElement.value == "completed") {
                this.callDatabaseSaveOnNextCommit = true;
            }

            //update the status of the activity's check box in the activity tree
            ManageTreeButtonsAndSCOStatus();

            this.scorm2004ADLNav.UpdateSequencingState(this.activeSCOIndex);
        }

        //SetValue successful, reset error, return true REQ_7.2
        this.setActiveError(Scorm2004Api.ERRORS.None);

        this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.True);
        setvalueCompleted = true;

        return Scorm2004Api.RETURN_VALUE.True;
    }
    catch (e) {
        //REQ_7.7
        this.traceNote("Caught an error in SetValue: " + e);
        this.setActiveError(Scorm2004Api.ERRORS.GeneralSetFailure, "", 1);
        return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.False);
    }
};

/**
* ValidateSetvalue()
*/
Scorm2004Api.prototype.ValidateSetvalue = function (pstrDataElementName, objDataElement) {
    this.DebuggerTraceCall('ValidateSetvalue');
    var split = pstrDataElementName.split('.');
    if (split.length > 3 && split[3] == "id") {
        if (pstrDataElementName.indexOf("cmi.objectives.") > -1 && objDataElement.isInitialized == true && objDataElement.value != objDataElement.pendingValue) {

            if (objDataElement.pendingValue == "" || objDataElement.pendingValue == null) {
                this.setActiveError(Scorm2004Api.ERRORS.DataValueTypeMismatch, "", 1);
                throw this.traceReturn("SetValue", Scorm2004Api.RETURN_VALUE.False);
            }
            else {
                this.setActiveError(Scorm2004Api.ERRORS.GeneralSetFailure, "", 1);
                throw this.traceReturn("SetValue", Scorm2004Api.RETURN_VALUE.False);
            }
        }
    }

};

/**
* GetLastError() REQ_9
*/

Scorm2004Api.prototype.GetLastError = function () {
    if (this.isAborted)
        return;

    var methodName = "GetLastError";

    this.traceCall(methodName);

    return this.traceReturn(methodName, this.activeError.code);
};

/**
* GetErrorString(pstrErrorCode) REQ_10
*/

Scorm2004Api.prototype.GetErrorString = function (pstrErrorCode) {
    if (this.isAborted)
        return;

    pstrErrorCode = String(pstrErrorCode);

    var methodName = "GetErrorString";

    this.traceCall(methodName, pstrErrorCode);

    //return the description of the specified error code 1_2_REQ_11.2
    for (var errItem in Scorm2004Api.ERRORS) {
        if (Scorm2004Api.ERRORS[errItem].code == pstrErrorCode) {
            return this.traceReturn(methodName, Scorm2004Api.ERRORS[errItem].descr);
        }
    }

    //If unknown error code, return empty string REQ_10.4
    return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.Empty);
};

/**
* GetDiagnostic(pstrParam) REQ_11
* Implemeneted but not used for anything yet.
*/

Scorm2004Api.prototype.GetDiagnostic = function (pstrParam) {
    if (this.isAborted)
        return;

    var methodName = "GetDiagnostic";

    this.traceCall(methodName, pstrParam);

    return this.traceReturn(methodName, Scorm2004Api.RETURN_VALUE.Empty);
};



//PRIVATE

/**
* Called to format a Scorm API method call then dispatch it as a trace.
* Arguments are: (functionName[,arg1,...,argN])
* Arguments which are not strings are made red and bold.
*/

Scorm2004Api.prototype.traceCall = function (functionName) {
    var line = "<b>" + functionName + "</b> (";

    for (var i = 1; i < arguments.length; i++) {
        //all arguments should be of type String, so if not, show them as red/bold/not-in-quotes.
        if (typeof arguments[i] == "string")
            line += '"<span style="color: #009900">' + StringHelper.escapeTags(arguments[i]) + '</span>"';
        else
            line += '<span style="color: #FF0000"><b>' + arguments[i] + '</b></span>';

        if (i < arguments.length - 1)
            line += ", ";
    }

    line += ")";

    this.dispatchEvent(new Event(Event.TRACE, "Scorm2004Api", line));
};

/**
* Called to dispatch a TRACE event. 
* @param functionName is the name of the function
* @param returnVal is one of the items in the Scorm2004Api.RETURN_VALUE set
* @return returnVal - the same as the input.
*/

Scorm2004Api.prototype.traceReturn = function (functionName, returnVal) {
    var line = "return " + functionName + ": ";

    //all arguments should be of type String, so if not, show them as red/bold/not-in-quotes.
    if (typeof returnVal == "string")
        line += '"' + StringHelper.escapeTags(returnVal) + '"';
    else
        line += '<span color="#FF0000"><b>' + returnVal + '</b></span>';

    this.dispatchEvent(new Event(Event.TRACE, "Scorm2004Api", line));

    return returnVal;
};

/**
* Called to dispatch a generic TRACE event.
*/
Scorm2004Api.prototype.traceNote = function (line) {
    this.dispatchEvent(new Event(Event.TRACE, "Scorm2004Api", line));
};

/**
* Sets the current error in the API, and dispatches an error event for logging.
* @param err - any item within the set of Scorm2004Api.ERRORS
* @param details - a string of details to be included in the dispatched event
* @param warningOrError - 0 = warning; 1 = error. This determined the type of event dispatched.
*/
Scorm2004Api.prototype.setActiveError = function (err, details, warningOrError) {
    this.DebuggerTraceCall('setActiveError');
    this.activeError = err;

    //don't bother dispatching an event if the error set is "None" for no-error.
    if (err != Scorm2004Api.ERRORS.None) {
        this.traceError(err.code + " - " + err.descr + ". " + details, "Scorm2004Api", warningOrError);
    }
};

/**
* Dispatches an error/warning event for logging purposes.
* @param details - a string of details to be included in the dispatched event
* @param source - a string representing the location of the error
* @param warningOrError - 0 = warning; 1 = error. This determined the type of event dispatched.
*/
Scorm2004Api.prototype.traceError = function (details, source, warningOrError) {
    this.DebuggerTraceCall('traceError');
    this.dispatchEvent(new Event((warningOrError == 1 ? Event.ERROR : Event.WARNING), source, details));
}

/**
* Does an alert via the configured window, or if that fails, just a regular alert.
*/
Scorm2004Api.prototype.doJSAlert = function (str) {

    this.DebuggerTraceCall('doJSAlert');
    try {
        this.alertAndConfirmWindow.alert(str);
    }
    catch (e) {
        alert(str);
    }
};

/**
* Called by LMSInitialize to restore the previous state based on the JSON that was loaded by the RTEDataLoader.
* This is not called if the RTEDataLoader has any type of HTTP or XML Status error -- those are handled by the RTEDataSaver.
*/
Scorm2004Api.prototype.deserialize = function () {
    this.DebuggerTraceCall('deserialize');
    /*
    The format of a successful response is:
	
    <cmi>
    <comments_from_learner>
    <comment_from_learner>
    <comment></comment>
    <lesson_location></lesson_location>
    <timestamp></timestamp>
    </comment_from_learner>
    </comments_from_learner>
    <completion_status>status</completion_status>
    <completion_threshold>threshold</completion_threshold>
    <entry>entry</entry> (present only if initialized)
    <interactions>
    <interaction>
    <id></id>
    <type></type>
    <objectives>
    <objective>
    <id></id>
    </objective>
    </objectives>
    <timestamp></timestamp>
    <correct_responses>
    <correct_response>
    <pattern></pattern>
    </correct_response>
    </correct_responses>
    <weighting></weighting>
    <learner_response></learner_response>
    <result></result>
    <latency></latency>
    <description></description>
    </interaction>
    </interactions>
    <launch_data></launch_data>
    <learner_id></learner_id>
    <learner_name></learner_name>
    <lesson_location></lesson_location>
    <max_time_allowed></max_time_allowed>
    <mode></mode>
    <credit></credit>
    <objectives>
    <objective>
    <id></id>
    <score>
    <scaled></scaled>
    <raw></raw>
    <min></min>
    <max></max>
    </score>
    <success_status></success_status>
    <completion_status></completion_status>
    <description></description>
    <progress_measure></progress_measure>
    </objective>
    </objectives>
    <progress_measure></progress_measure>
    <scaled_passing_score></scaled_passing_score>
    <score>
    <scaled></scaled>
    <raw></raw>
    <min></min>
    <max></max>
    </score>
    <success_status></success_status>
    <suspend_data></suspend_data>
    <time_limit_action></time_limit_action>
    <total_time></total_time>
    </cmi>
    </body>
    </serv_response>
	
    This function would NOT be called if the JSON was a failed response, because the RTEDataLoader handles that now.
    */


    this.activeDataModel = null;
    this.activeDataModel = new Scorm2004DataModel();

    var c = this.activeDataModel.cmi;
    var pc = this.courseDataModel[this.activeSCOIndex].cmi;

    //initialized lastLaunchTotalTimeInMS
    this.lastLaunchTotalTimeInMS = this.timeIntervalToMs(pc.total_time.value);

    //start with all the top-level text nodes - if value is null, the initialize will not happen
    c.completion_status = pc.completion_status;
    c.completion_threshold = pc.completion_threshold;
    c.entry = pc.entry;
    c.exit = pc.exit;
    c.launch_data = pc.launch_data;
    c.learner_id = pc.learner_id;
    c.learner_name = pc.learner_name;
    c.location = pc.location;

    //Fill the cmi mode value
    c.mode = pc.mode;
    c.progress_measure = pc.progress_measure;
    c.scaled_passing_score = pc.scaled_passing_score;    
    c.success_status = pc.success_status;
    c.successStatusChangedDuringRuntime = pc.successStatusChangedDuringRuntime
    c.suspend_data = pc.suspend_data;
    c.time_limit_action = pc.time_limit_action;

    //handle max_time_allowed which is delivered in seconds
    c.max_time_allowed = pc.max_time_allowed;

    //handle the total_time which is delivered in seconds
    c.total_time = pc.total_time

    //this.traceNote("Previous time in this lesson: " + Number(pc.total_time) + " seconds");
    //Added By Chetu START
    c.isActive.value = pc.isActive.value,
    //
        c.isSuspended.value = pc.isSuspended.value;
    c.attemptCompletionAmount.value = pc.attemptCompletionAmount.value;
    c.objectiveProgressStatus.value = pc.objectiveProgressStatus.value;
    c.objectiveSatisfiedStatus.value = pc.objectiveSatisfiedStatus.value;
    //
    c.objectiveMeasureStatus.value = pc.objectiveMeasureStatus.value,
        c.objectiveNormalizedMeasure.value = pc.objectiveNormalizedMeasure.value,
        c.attemptProgressStatus.value = pc.attemptProgressStatus.value,
        c.attemptCompletionStatus.value = pc.attemptCompletionStatus.value,
    //
        c.activityProgressStatus.value = pc.activityProgressStatus.value,
        c.activityAttemptCount.value = pc.activityAttemptCount.value;
    c.attemptCompletionAmountStatus.value = pc.attemptCompletionAmountStatus.value;
    //
    c.activityEffectiveAttemptCount.value = pc.activityEffectiveAttemptCount.value;
    //END
    //score
    c.score.max = pc.score.max;
    c.score.min = pc.score.min;
    c.score.raw = pc.score.raw;
    c.score.scaled = pc.score.scaled;

    //get interactions   
    //wrong condition updated by chetu 
    //  if (pc.interactions.length == null) {
    if (pc.interactions.__array.length == null) {
        interactionsLength = 0;
    }
    else {
        interactionsLength = pc.interactions.__array.length;
    }

    if (interactionsLength > 0) {

        for (var x = 0; x < interactionsLength; x++) {
            var interactionObject = pc.interactions.__array[x];

            var objInteraction = new Scorm2004Interaction();

            objInteraction.id = interactionObject.id;
            objInteraction.type = interactionObject.type;
            objInteraction.timestamp = interactionObject.timestamp;
            objInteraction.weighting = interactionObject.weighting;
            objInteraction.learner_response = interactionObject.learner_response;
            objInteraction.result = interactionObject.result;
            objInteraction.description = interactionObject.description;

            //latency arrives as seconds, so convert to TimeInterval
            objInteraction.latency = interactionObject.latency;

            if (interactionObject.objectives.length == null) {
                interactionObjectivesLength = 0;
            }
            else {
                interactionObjectivesLength = interactionObject.objectives.length;
            }

            if (interactionObjectivesLength > 0) {

                for (var y = 0; y < interactionObject.objectives.length; y++) {
                    var interactionObjectivesObject = interactionObject.objectives[y];

                    var objInteractionObjective = new Scorm2004InteractionObjective();

                    objInteractionObjective.id = interactionObjectivesObject.id;

                    this.appendToCollection(objInteraction.objectives, objInteractionObjective);
                }
            }

            if (interactionObject.correct_responses.__array.length == null) {
                interactionsCorrectResponsesLength = 0;
            }
            else {
                interactionsCorrectResponsesLength = interactionObject.correct_responses.__array.length;
            }

            if (interactionsCorrectResponsesLength > 0) {

                for (var y = 0; y < interactionsCorrectResponsesLength; y++) {
                    var correctResponseObject = interactionObject.correct_responses.__array[y];

                    var objCorrectResponse = new Scorm2004InteractionCorrectResponse();

                    objCorrectResponse.pattern = correctResponseObject.pattern;

                    this.appendToCollection(objInteraction.correct_responses, objCorrectResponse);
                }
            }

            this.appendToCollection(c.interactions, objInteraction);
        }
    }

    //get objectives
    //wrong condition updated by chetu 
    //    if (pc.objectives.length == null) {


    /**
    // Added these lines of code to  initialize objective structure before the launching of SCO activity.
    //This will provide objective structure from the manifest if it is not present in the JSON file.
    **/
    //if (pc.objectives.__array.length == null || pc.objectives.__array.length == 0) {

    //here we are setting objectives in course datamodel from manifest object with availble objective id
    //    this.SetObjectivesInDataModel();
    //}
    for (var x = 0; x < pc.objectives.__array.length; x++) {

        var objectiveObject = pc.objectives.__array[x];
        var objObjective = new Scorm2004Objective();

        objObjective.id = objectiveObject.id;
        objObjective.success_status = objectiveObject.success_status;
        objObjective.completion_status = objectiveObject.completion_status;
        objObjective.description = objectiveObject.description;
        objObjective.score.max = objectiveObject.score.max;
        objObjective.score.min = objectiveObject.score.min;
        objObjective.score.raw = objectiveObject.score.raw;
        objObjective.score.scaled = objectiveObject.score.scaled;
        //Added By Chetu  START
        objObjective.progress_measure = objectiveObject.progress_measure;

        objObjective.objectiveProgressStatus.value = objectiveObject.objectiveProgressStatus.value;
        objObjective.objectiveSatisfiedStatus.value = objectiveObject.objectiveSatisfiedStatus.value;
        objObjective.objectiveMeasureStatus.value = objectiveObject.objectiveMeasureStatus.value;
        objObjective.objectiveNormalizedMeasure.value = objectiveObject.objectiveNormalizedMeasure.value;
        objObjective.attemptProgressStatus.value = objectiveObject.attemptProgressStatus.value;
        objObjective.attemptCompletionStatus.value = objectiveObject.attemptCompletionStatus.value;
        objObjective.objectiveContributestoRollup.value = objectiveObject.objectiveContributestoRollup.value;
        objObjective.attemptCompletionAmountStatus.value = objectiveObject.attemptCompletionAmountStatus.value;
        //END

        this.appendToCollection(c.objectives, objObjective);

    }

    //for (var x = 0; x < pc.globalObjectives.__array.length; x++) {

    //    var objectiveObject = pc.globalObjectives.__array[x];
    //    var objGlobalObjective = new Scorm2004Objective();

    //    objGlobalObjective.id = objectiveObject.id;
    //    objGlobalObjective.success_status = objectiveObject.success_status;
    //    objGlobalObjective.completion_status = objectiveObject.completion_status;
    //    objGlobalObjective.description = objectiveObject.description;
    //    objGlobalObjective.score.max = objectiveObject.score.max;
    //    objGlobalObjective.score.min = objectiveObject.score.min;
    //    objGlobalObjective.score.raw = objectiveObject.score.raw;
    //    objGlobalObjective.score.scaled = objectiveObject.score.scaled;
    //    //added by chetu
    //    objGlobalObjective.progress_measure = objectiveObject.progress_measure;
    //    //Added By Chetu  START
    //    objGlobalObjective.objectiveProgressStatus.value = objectiveObject.objectiveProgressStatus.value;
    //    objGlobalObjective.objectiveSatisfiedStatus.value = objectiveObject.objectiveSatisfiedStatus.value;
    //    objGlobalObjective.objectiveMeasureStatus.value = objectiveObject.objectiveMeasureStatus.value;
    //    objGlobalObjective.objectiveNormalizedMeasure.value = objectiveObject.objectiveNormalizedMeasure.value;
    //    objGlobalObjective.attemptProgressStatus.value = objectiveObject.attemptProgressStatus.value;
    //    objGlobalObjective.attemptCompletionStatus.value = objectiveObject.attemptCompletionStatus.value;
    //    objGlobalObjective.objectiveContributestoRollup.value = objectiveObject.objectiveContributestoRollup.value;
    //    objGlobalObjective.attemptCompletionAmountStatus.value = objectiveObject.attemptCompletionAmountStatus.value;
    //    //END

    //    this.appendToCollection(c.globalObjectives, objGlobalObjective);

    //}
};

/**
* Helper function to append an object to a collection.
*/
Scorm2004Api.prototype.appendToCollection = function (collection, obj) {
    this.DebuggerTraceCall('appendToCollection');
    var arr = collection.__array;

    arr[arr.length] = obj;

    collection._count.initialize(String(arr.length));
};

/**
* Called by Commit to save the current state to the RTEDataSaver.
* 
* @param saveToDB boolean if true, data will be saved to the database, if false, will be saved to an XML file.
*/

Scorm2004Api.prototype.serialize = function (saveToDB) {
    this.DebuggerTraceCall('serialize');

    var cmiJSONString = this.GetItemRTEJson(this.activeSCOIndex);

    //save
    var saver = this.courseLaunchController.getRteDataSaver();

    saver.addEventListener(Event.SCO_DATA_SAVED,
                           function (e) {
                               this.onSerializeLoaded(e);
                               this.saveGlobal(saveToDB);
                           },
                           this); //this listener gets added only once on the first addEventListener call    
    
    saver.save(this.activeSCOIdentifier, cmiJSONString, this.schemaversion, saveToDB);
}

Scorm2004Api.prototype.GetItemRTEJson = function (itemIndex, returnObject) {

    var c = this.courseDataModel[itemIndex].cmi;

    //calulate the total time the learner has been in this lesson... REQ_76.4
    var totalTimeInS = itemIndex == this.activeSCOIndex ? this.getTotalTimeInSeconds() : (this.timeIntervalToMs(c.total_time.value) / 1000);

    //if terminating from a timeout, set the total time to the max time allowed
    if (this.terminateFromTimeout) {
        c.total_time.initialize(c.max_time_allowed.value);
        totalTimeInS = this.timeIntervalToMs(c.max_time_allowed.value) / 1000;
    }

    this.traceNote("Total time in this lesson: " + totalTimeInS + " seconds");

    /*
    TO DO:
    this.reportToAPIInspector("cmi.total_time", 	c.total_time.value);
    this.reportToAPIInspector("cmi.session_time",	c.session_time.value);
    */

    //for simplicity, build an intermediary object, then translate the object to an XML string, 
    //which will exclude tags for any properties that have a null value
    var cmiXML =
	{
	    credit: c.credit.value,
	    mode: c.mode.value,
	    completion_status: c.completion_status.value,
	    entry: c.entry.value,
	    exit: c.exit.value,
	    location: c.location.value,
	    progress_measure: c.progress_measure.value,
	    activityAttemptCount: c.activityAttemptCount.value,
	    activityEffectiveAttemptCount: c.activityEffectiveAttemptCount.value,

	    //Added later
	    max_time_allowed: c.max_time_allowed.value,
	    time_limit_action: c.time_limit_action.value,
	    completion_threshold: c.completion_threshold.value,
	    isActive: c.isActive.value,
	    isSuspended: c.isSuspended.value,
	    attemptCompletionAmount: c.attemptCompletionAmount.value,
	    objectiveProgressStatus: c.objectiveProgressStatus.value,
	    objectiveSatisfiedStatus: c.objectiveSatisfiedStatus.value,
	    objectiveMeasureStatus: c.objectiveMeasureStatus.value,
	    objectiveNormalizedMeasure: c.objectiveNormalizedMeasure.value,
	    attemptProgressStatus: c.attemptProgressStatus.value,
	    attemptCompletionStatus: c.attemptCompletionStatus.value,
	    activityProgressStatus: c.activityProgressStatus.value,
	    attemptCompletionAmountStatus: c.attemptCompletionAmountStatus.value,
	    //END
	    score:
		{
		    max: c.score.max.value,
		    min: c.score.min.value,
		    raw: c.score.raw.value,
		    scaled: c.score.scaled.value
		},
	    success_status: c.success_status.value,
	    successStatusChangedDuringRuntime: c.successStatusChangedDuringRuntime.value,
	    suspend_data: c.suspend_data.value,
	    total_time: totalTimeInS
	};

    //now for the more compicated collection elements...
    var arr;
    //interactions
    arr = cmiXML.interactions = new Array();

    for (var x = 0; x < c.interactions.__array.length; x++) {
        var interaction = arr[x] = new Object();
        var i = c.interactions.__array[x];

        interaction.id = i.id.value;
        interaction.type = i.type.value;
        interaction.timestamp = i.timestamp.value;

        //expand timestamp to full date/time in case any parts are missing
        interaction.timestampConverted = this.expandDateTime(i.timestamp.value);

        interaction.weighting = i.weighting.value;
        interaction.learner_response = i.learner_response.value;
        interaction.result = i.result.value;
        interaction.description = i.description.value;

        //latency needs to be delivered as seconds, so convert TimeInterval to seconds
        var latencyAsTimeInterval = i.latency.value;
        if (latencyAsTimeInterval != null) {
            interaction.latency = (this.timeIntervalToMs(latencyAsTimeInterval) / 1000);
        }

        interaction.objectives = new Array();

        for (var y = 0; y < i.objectives.__array.length; y++) {
            var objective = interaction.objectives[y] = new Object();
            var o = i.objectives.__array[y];

            objective.id = o.id.value;
        }

        interaction.correct_responses = new Array();

        for (var y = 0; y < i.correct_responses.__array.length; y++) {
            var correct_response = interaction.correct_responses[y] = new Object();
            var cr = i.correct_responses.__array[y];

            correct_response.pattern = cr.pattern.value;
        }
    }

    // replaceing old ids with new  ids of cmiXML.interactions
    var duplicateInterationIndex = [];
    var uniqueInteractionId = [];
    for (var i = cmiXML.interactions.length - 1; i >= 0; i--) {
        if (uniqueInteractionId.indexOf(cmiXML.interactions[i].id) > -1) {
            duplicateInterationIndex.push(i);
        }
        else {
            uniqueInteractionId.push(cmiXML.interactions[i].id);
        }
    }

    for (var i = 0; i < duplicateInterationIndex.length; i++) {
        cmiXML.interactions.splice(duplicateInterationIndex[i], 1);
    }

    //objectives
    arr = cmiXML.objectives = new Array();

    for (var x = 0; x < c.objectives.__array.length; x++) {
        var objective = arr[x] = new Object();
        var o = c.objectives.__array[x];

        objective.id = o.id.value;
        objective.completion_status = o.completion_status.value;
        objective.success_status = o.success_status.value;
        objective.progress_measure = o.progress_measure.value;
        objective.description = o.description.value;
        //Added By Chetu  START
        objective.objectiveProgressStatus = o.objectiveProgressStatus.value;
        objective.objectiveSatisfiedStatus = o.objectiveSatisfiedStatus.value;
        objective.objectiveMeasureStatus = o.objectiveMeasureStatus.value;
        objective.objectiveNormalizedMeasure = o.objectiveNormalizedMeasure.value;
        objective.attemptProgressStatus = o.attemptProgressStatus.value;
        objective.attemptCompletionStatus = o.attemptCompletionStatus.value;
        objective.objectiveContributestoRollup = o.objectiveContributestoRollup.value;
        objective.attemptCompletionAmountStatus = o.attemptCompletionAmountStatus.value;
        //END
        objective.score =
		{
		    max: o.score.max.value,
		    min: o.score.min.value,
		    raw: o.score.raw.value,
		    scaled: o.score.scaled.value
		};
    }

    if (returnObject) {
        return cmiXML;
    }
    else {
        //convert the Object to a string of JSON
        var cmiJSONString = JSON.stringify(cmiXML).replace(new RegExp('\"', 'g'), '\\"');

        return cmiJSONString;
    }
}

Scorm2004Api.prototype.SaveNonLeafItemsJson = function (returnToLMS) {
    var itemsToSave = new Array();

    for (var activityIndex = 0; activityIndex < this.courseDataModel.length; activityIndex++) {
        
        var identifier = this.scorm2004ADLNav.GetItemIdentifier(activityIndex);
        var identifierref = this.scorm2004ADLNav.GetItemIdentifierRef(activityIndex);
        var cmiMode = this.courseDataModel[activityIndex].cmi.mode.value;
        var cmiJSONString = this.GetItemRTEJson(activityIndex, true);

        // only push to non-leaf items array if there is no identifierref and the cmi.mode is not "browse" or "review"
        if (identifierref == null && cmiMode != "browse" && cmiMode != "review") {
            var obj = { Key: identifier, Value: cmiJSONString };
            itemsToSave.push(obj);
        }
    }

    // only continue with save if there are items in the array
    if (itemsToSave.length > 0) {

        var itemsToSaveJs = JSON.stringify(itemsToSave).replace(new RegExp('\"', 'g'), '\\"');

        var saver = this.courseLaunchController.getRteDataSaver();

        saver.addEventListener(Event.NON_LEAF_DATA_ITEMS_SAVED,
                            function (e) {
                                if (returnToLMS) {

                                    // if we are exiting, dispatch exit event and finish completed event
                                    this.courseLaunchController.dispatchEvent(new Event(Event.DATA_SAVED_FOR_EXIT, "DATA SAVED FOR EXIT")); // WIDGET                                    
                                    this.dispatchEvent(new Event(Event.FINISH_COMPLETED, "Scorm2004Api"));

                                    if (GLOBAL.COURSE_INFO.ReturnUrl != null) {
                                        document.location.href = GLOBAL.COURSE_INFO.ReturnUrl;
                                    }
                                    else {
                                        HideLoadAndSaveContentModalPopup();
                                    }
                                }
                                else {
                                    HideLoadAndSaveContentModalPopup();
                                }
                            },
                            this); //this listener gets added only once on the first addEventListener call    

        saver.save("|__NonLeafDataItems__|", itemsToSaveJs, this.schemaversion, false);
    }
    // else, do normal return process that would have happened in the listener above
    else {
        if (returnToLMS) {

            // if we are exiting, dispatch exit event
            this.courseLaunchController.dispatchEvent(new Event(Event.DATA_SAVED_FOR_EXIT, "DATA SAVED FOR EXIT")); // WIDGET

            if (GLOBAL.COURSE_INFO.ReturnUrl != null) {
                document.location.href = GLOBAL.COURSE_INFO.ReturnUrl;
            }
            else {
                HideLoadAndSaveContentModalPopup();
            }            
        }
        else {
            HideLoadAndSaveContentModalPopup();
        }
    }
}

/**
* This is called once the RTEDataSaver has completed SUCCESSFULLY. It runs in the correct scope of this-instance.
* This is not called if the saver has any type of HTTP or XML Status error -- those are handled by the RTEDataSaver.
* 
* @param e the event object returned by the RTEDataSaver which contains the resulting XML DOM object in the "target" property.
*/
Scorm2004Api.prototype.onSerializeLoaded = function (e) {
    this.DebuggerTraceCall('onSerializeLoaded');
    try {
        /*
        The format of a successful response is:
        
        <serv_response>
        <head>
        <status>success</status>
        </head>
        <body>
        <token>sdf676sd87f68s7d</token>  this is extracted by the RTEDataSaver.
        </body>
        </serv_response>

        This function would NOT be called if the XML was a failed response, because the RTEDataSaver handles that now.
        */

        this.traceNote("Commit process is complete.");

        //unlock commmit by changing the commit state
        this.activeCommitState = Scorm2004Api.COMMIT_STATE.Committed;

        if (this.queueFinishOnSaverResponse) {
            //state would currently be TerminateStarted to lock any calls to GetValue/SetValue/Commit
            //but unlock the state to this call so Terminate will proceed.
            this.activeState = Scorm2004Api.STATE.Running;
            this.Terminate("");
            return;
        }
        else if (this.queueCommitOnSaverResponse) {
            this.Commit("");
            return;
        }

        //if this call to Commit was called by Terminate, dispatch the onFinishCompleted event
        if (this.activeState == Scorm2004Api.STATE.TerminateStarted) {
            this.activeState = Scorm2004Api.STATE.Terminated;

            this.dispatchEvent(new Event(Event.FINISH_COMPLETED, "Scorm2004Api"));
        }
    }
    catch (e) {
        this.traceError("Error caught in onSerializeLoaded: " + e, "", 1);
    }
};

/**
* This is called once the RTEDataSaver has completed SUCCESSFULLY. It runs in the correct scope of this-instance.
* This is not called if the saver has any type of HTTP or XML Status error -- those are handled by the RTEDataSaver.
* 
* @param e the event object returned by the RTEDataSaver which contains the resulting XML DOM object in the "target" property.
*/

Scorm2004Api.prototype.saveGlobal = function (saveToDB) {
    this.DebuggerTraceCall('saveGlobal');    
    
    //update the over all package attribute values
    this.fillGlobalModelValues();

    // save global
    var globalXML =
        {
            activeSCOIdentifier: this.activeSCOIdentifier,
            activeSCOIndex: this.activeSCOIndex,
            previousSCOIdentifier: this.previousSCOIdentifier,
            nextSCOIdentifier: this.nextSCOIdentifier,
            entry:this.entry,
            completion_status: this.completion_status,
            success_status: this.success_status,
            actualAttemptCount: this.actualAttemptCount,
            effectiveAttemptCount: this.effectiveAttemptCount,
            total_time: this.total_time,
            activityAttemptCount: this.activityAttemptCount,
            availableChildrens: this.availableChildrens,
            score: this.score
        };

    var arr;
    arr = globalXML.primaryObjectives = new Array();

    for (var index = 0; index < this.primaryObjectives.length; index++) {
        var objective = arr[index] = new Object();
        var o = this.primaryObjectives[index];

        objective.id = o.id.value;
        objective.completion_status = o.completion_status.value;
        objective.success_status = o.success_status.value;
        objective.progress_measure = o.progress_measure.value;
        objective.description = o.description.value;
        //Added By Chetu  START
        objective.objectiveProgressStatus = o.objectiveProgressStatus.value;
        objective.objectiveSatisfiedStatus = o.objectiveSatisfiedStatus.value;
        objective.objectiveMeasureStatus = o.objectiveMeasureStatus.value;
        objective.objectiveNormalizedMeasure = o.objectiveNormalizedMeasure.value;
        objective.attemptProgressStatus = o.attemptProgressStatus.value;
        objective.attemptCompletionStatus = o.attemptCompletionStatus.value;
        objective.objectiveContributestoRollup = o.objectiveContributestoRollup.value;
        objective.attemptCompletionAmountStatus = o.attemptCompletionAmountStatus.value;
        //END
        objective.score =
        {
            max: o.score.max == null ? null : o.score.max.value,
            min: o.score.min == null ? null : o.score.min.value,
            raw: o.score.raw == null ? null : o.score.raw.value,
            scaled: o.score.scaled == null ? null : o.score.scaled.value,
        };
    }

    var arrGlogalObjectives;
    arrGlogalObjectives = globalXML.globalObjectives = new Array();

    for (var index = 0; index < this.globalObjectives.length; index++) {
        var glogalObjective = arrGlogalObjectives[index] = new Object();
        var o = this.globalObjectives[index];

        glogalObjective.id = o.id.value;
        glogalObjective.completion_status = o.completion_status.value;
        glogalObjective.success_status = o.success_status.value;
        glogalObjective.progress_measure = o.progress_measure.value;
        glogalObjective.description = o.description.value;
        //Added By Chetu  START
        glogalObjective.objectiveProgressStatus = o.objectiveProgressStatus.value;
        glogalObjective.objectiveSatisfiedStatus = o.objectiveSatisfiedStatus.value;
        glogalObjective.objectiveMeasureStatus = o.objectiveMeasureStatus.value;
        glogalObjective.objectiveNormalizedMeasure = o.objectiveNormalizedMeasure.value;
        glogalObjective.attemptProgressStatus = o.attemptProgressStatus.value;
        glogalObjective.attemptCompletionStatus = o.attemptCompletionStatus.value;
        glogalObjective.objectiveContributestoRollup = o.objectiveContributestoRollup.value;
        glogalObjective.attemptCompletionAmountStatus = o.attemptCompletionAmountStatus.value;
        //END
        glogalObjective.score =
        {
            max: o.score.max == null ? null : o.score.max.value,
            min: o.score.min == null ? null : o.score.min.value,
            raw: o.score.raw == null ? null : o.score.raw.value,
            scaled: o.score.scaled == null ? null : o.score.scaled.value,
        };
    }

    //convert the Object to a string of JSON
    var globalJSONString = JSON.stringify(globalXML).replace(new RegExp('\"', 'g'), '\\"');

    //save
    var globalSaver = this.courseLaunchController.getGlobalDataSaver();
    globalSaver.addEventListener(Event.GLOBAL_DATA_SAVED,
                                 function (e) {
                                     this.traceNote("Global save process is complete.");
                                     this.saveRTECallFinished = true;  // set flag to check RTE save process status
                                     this.globalSaved = saveToDB;      // set flag to check global data save status
                                 },
                                 this); //this listener gets added only once on the first addEventListener call    

    globalSaver.save("global_data", globalJSONString, this.schemaversion, saveToDB);
};


/**
* This is called once the RTEDataSaver has completed SUCCESSFULLY. It runs in the correct scope of this-instance.
* This is not called if the saver has any type of HTTP or XML Status error -- those are handled by the RTEDataSaver.
* 
* @param e the event object returned by the RTEDataSaver which contains the resulting XML DOM object in the "target" property.
*/
Scorm2004Api.prototype.onGlobalSaveLoaded = function (e) {
    this.DebuggerTraceCall('onGlobalSaveLoaded');
    try {
        /*
        The format of a successful response is:
        
        <serv_response>
        <head>
        <status>success</status>
        </head>
        <body>
        <token>sdf676sd87f68s7d</token>  this is extracted by the RTEDataSaver.
        </body>
        </serv_response>

        This function would NOT be called if the XML was a failed response, because the RTEDataSaver handles that now.
        */

        this.traceNote("Global save process is complete.");
    }
    catch (e) {
        this.traceError("Error caught in onGlobalSaveLoaded: " + e, "", 1);
    }
};

/**
* Returns true if the name is a string, starts with "cmi" or "adl", and exists in the data model.
*/
Scorm2004Api.prototype.isValidDataElementName = function (pstrDataElementName) {
    this.DebuggerTraceCall('isValidDataElementName');
    //make sure the parameter is a string
    if (typeof pstrDataElementName != "string")
        return false;

    //make sure pstrDataElementName begins with "cmi"
    var prefix = pstrDataElementName.substr(0, 3);
    if (prefix != "cmi" && prefix != "adl")
        return false;

    //make sure pstrDataElementName is a valid data model element
    if (!this.getDataElement(pstrDataElementName))
        return false;

    return true;
};

/**
* Renames ".continue" to "._continue" specifically for the 
* adl.nav.request.continue element -- due to IE bug using 
* the "continue" keyword.
*/
Scorm2004Api.prototype.alterNameForInternalUse = function (pstrDataElementName) {
    this.DebuggerTraceCall('alterNameForInternalUse');
    //special replacements in name:
    if (pstrDataElementName.indexOf(".continue") > -1)
        pstrDataElementName = pstrDataElementName.split(".continue").join("._continue");

    return pstrDataElementName;
}

/**
* Returns the corresponding element in the data model, or false if it doesn't exist.
* Note - for "array" elements like cmi.objectives.1.id, you must use getArrayDataElement()
*/
Scorm2004Api.prototype.getDataElement = function (pstrDataElementName) {
    this.DebuggerTraceCall('getDataElement');
    try {
        return eval("this.activeDataModel." + pstrDataElementName);
    }
    catch (e) {
        //special temporary exception for: adl.nav.request_valid.choice.{target=XYZ} until we get sequencing done
        if (pstrDataElementName.indexOf("adl.nav.request_valid.choice.{target=") == 0
            && pstrDataElementName.charAt(pstrDataElementName.length - 1) == "}")
            return this.activeDataModel.adl.nav.request_valid.choice;

        //otherwise the data element name is invalid
        return false;
    }
};

/**
* Returns true if the data element name is a valid array data element, i.e. cmi.objectives.1.id
*/
Scorm2004Api.prototype.isValidArrayDataElementName = function (pstrDataElementName) {
    this.DebuggerTraceCall('isValidArrayDataElementName');
    try {
        //check if there is at least one array index notation within the name (cmi.abcd.5.asdf)
        var pattern = /.[0-9]+./;
        if (!pattern.test(pstrDataElementName))
            return false;

        //loop through each part and validate each
        /*
        cmi.interactions.5.objectives.3.id
        1. validate cmi.interactions in "this"
        2. get itemType of cmi.interactions 
        3. validate objectives in last itemType
        4. get itemType of objectives
        5. validate id in last itemType
        */

        var arr = pstrDataElementName.split(".");
        var container = this.activeDataModel;
        var propertyPart = "";

        for (var i = 0; i < arr.length; i++) {
            //if part is not not an array index, build the property part string
            if (isNaN(arr[i])) {
                propertyPart += arr[i];
                if (i < arr.length - 1 && isNaN(arr[i + 1])) {
                    propertyPart += ".";
                }
            }
            //if part is numeric or is the last part, evaluate it to make sure it exists
            if (!isNaN(arr[i])) {
                if (!eval("container." + propertyPart))
                    return false;

                //get the next object container
                var itemType = eval("container." + propertyPart + ".__itemType");
                container = eval("new " + itemType);

                //reset the propertyPart string
                propertyPart = "";

                //validate the last property
            }
            else if (i == arr.length - 1) {
                if (!eval("container." + propertyPart))
                    return false;
            }
        }

        return true;
    }
    catch (e) {
        this.traceNote("Caught an error in isValidArrayDataElementName(): " + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
* Returns true if index is > 0 and <= count. 
* Assumes pstrDataElementName is a valid array data element name.
*/
Scorm2004Api.prototype.isArrayDataElementIndexInRange = function (pstrDataElementName) {
    this.DebuggerTraceCall('isArrayDataElementIndexInRange');
    try {
        var arr = pstrDataElementName.split(".");
        var container = this.activeDataModel;
        var propertyPart = "";
        var index;

        for (var i = 0; i < arr.length; i++) {
            //if part is not not an array index, build the property part string
            if (isNaN(arr[i])) {
                propertyPart += arr[i];
                if (i < arr.length - 1 && isNaN(arr[i + 1])) {
                    propertyPart += ".";
                }
            }

            //if part is numeric, build the container
            if (!isNaN(arr[i])) {
                container = eval("container." + propertyPart);

                index = Number(arr[i]);

                if (index < 0 || index > Number(container._count.value))
                    return false;
                else if (index == Number(container._count.value))
                    container = eval("new " + container.__itemType);
                else
                    container = container.__array[index];

                propertyPart = "";
            }
        }

        return true;
    }
    catch (e) {
        this.traceNote("Caught an error in isArrayDataElementIndexInRange():" + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
* @param pstrDataElementName i.e. "objectives.5.score.raw"
* @param blnAppendNewElement:
*	if blnAppendNewElement is true AND if current _count is equal to the requested index, then the count is incremented by 1 and a new element is appended to the array
*	else if blnAppendNewElement is false AND if current _count is equal to the requested index, then the count stays the same and a temporary element is returned
*	else return the existing element
*/
Scorm2004Api.prototype.getArrayDataElement = function (pstrDataElementName, blnAppendNewElement) {
    this.DebuggerTraceCall('getArrayDataElement');

    try {
        var arr = pstrDataElementName.split(".");
        var container = this.activeDataModel;
        var newContainer;
        var propertyPart = "";
        var index;

        for (var i = 0; i < arr.length; i++) {
            //if part is not not an array index, build the property part string
            if (isNaN(arr[i])) {
                propertyPart += arr[i];
                if (i < arr.length - 1 && isNaN(arr[i + 1]))
                    propertyPart += ".";
            }

            //if part is numeric, build the container
            if (!isNaN(arr[i])) {
                container = eval("container." + propertyPart);

                index = Number(arr[i]);

                if (index < 0 || index > Number(container._count.value))
                    return false;

                else if (Number(container._count.value) == index) {
                    if (blnAppendNewElement) {
                        //create the new index & update the _count property
                        container.__array[index] = eval("new " + container.__itemType);
                        container._count.initialize(String(container.__array.length));

                        //store a reference to the the parent and a flag atLeastOnePropertySet in case a rollback needs to happen due to attempting to set invalid data
                        container.__array[index].parent = container;

                        container = container.__array[index];
                    }
                    else
                        container = eval("new " + container.__itemType);
                }
                else
                    container = container.__array[index]

                propertyPart = "";
            }

                //if part is the last part, return it
            else if (i == arr.length - 1)
                return eval("container." + propertyPart);
        }
    }
    catch (e) {
        this.traceNote("Caught an error in getArrayDataElement():" + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
* Removes the last array element (assumed to be pobjDataElement) from the parent
* collection's array ONLY IF no other properties in the parent have been previously set successfully
* 
* i.e. if the object is interactions.5.latency, but no other properties of the 5th interaction have 
*	      been set yet (like interactions.5.id), then the 5th interaction will be discarded and the highest
*		  index will become 4 (meaning 5 total interactions since it's zero-based)
*/
Scorm2004Api.prototype.undoArrayElementAppend = function (pobjDataElement) {
    this.DebuggerTraceCall('undoArrayElementAppend');
    try {
        var parentObject = pobjDataElement.parent; //i.e. instance of Interaction()

        if (!parentObject.atLeastOnePropertySet) {
            //this.traceNote("atLeastOnePropertySet != true, so undoing");
            var collectionObject = parentObject.parent; //i.e. interactions
            var arr = collectionObject.__array;
            arr.splice(arr.length - 1, 1);
            collectionObject._count.value = String(arr.length);
        }
    }
    catch (e) {
        this.traceNote("Caught an error in undoArrayElementAppend():" + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
}

/**
* Returns true if the data type of pobjDataElement is valid according to 
* the type of element pobjDataElement.pendingValue.
* @param pobjDataElement - the data element to test
* @param pobjDataType - the optional data type to use; if not defined, uses pobjDataElement.dataType
*/
Scorm2004Api.prototype.isDataValueTypeValid = function (pobjDataElement, pobjDataType) {
    this.DebuggerTraceCall('isDataValueTypeValid');
    try {
        var objDataType = pobjDataElement.dataType;

        //make sure the value is being passed as a string
        if (typeof pobjDataElement.pendingValue != "string")
            return false;

        if (typeof (pobjDataType) != "undefined")
            objDataType = pobjDataType;

        switch (objDataType) {
            //CharString REQ_81                                         
            case Scorm2004DataModel.DATA_TYPE.CharString:

                return this.validateLength(pobjDataElement, "CharString");

                //CharStringLocal REQ_82
            case Scorm2004DataModel.DATA_TYPE.CharStringLocal:
                if (!this.validateLength(pobjDataElement, "CharStringLocal"))
                    return false;
                else {
                    //if string begins with {lang=xx...}, lang value must be valid
                    var pattern = /^(\{lang=)([a-z]?)(\})/;
                    if (pattern.test(pobjDataElement.pendingValue)) {
                        //string contains lang type, make sure it's a valid lang type
                        //we should validate against ISO369-1, ISO369-2, ISO3166-1 but right now we're just doing string validation
                        var pattern = /^(\{lang=)(i|x|[a-z]{2,3})(\-[a-z]+)*(\})(.)/i;

                        return pattern.test(pobjDataElement.pendingValue);
                    }
                    else
                        return true;
                }

                //Vocabulary
            case Scorm2004DataModel.DATA_TYPE.Vocabulary:
                var arrList = pobjDataElement.constraints.split("|");
                var x;
                for (x = 0; x < arrList.length; x++) {
                    //if value includes another data type in brackets, validate against that type
                    var vocabValue = arrList[x];
                    if (vocabValue.indexOf("[") == 0) {
                        var dataTypeName = vocabValue.substring(1, vocabValue.length - 1);
                        var dataType = eval("Scorm2004DataModel.DATA_TYPE." + dataTypeName);

                        if (this.isDataValueTypeValid(pobjDataElement, dataType)) {
                            return true;
                        }
                    }
                        //workaround for choice and jump navigation requests
                    else if (pobjDataElement.pendingValue.indexOf("{target=") == 0
                             && pobjDataElement.pendingValue.indexOf("}") >= 8) {
                        if (pobjDataElement.pendingValue.indexOf("choice") > 8
                            || pobjDataElement.pendingValue.indexOf("jump") > 8) {
                            var pendingValueCleaned = "";
                            var navigationString = "";

                            var targetString = pobjDataElement.pendingValue.substring(pobjDataElement.pendingValue.indexOf("{target="),
                                                                                      pobjDataElement.pendingValue.indexOf("{target=") + 8);

                            var closeBracketString = pobjDataElement.pendingValue.substring(pobjDataElement.pendingValue.indexOf("}"),
                                                                                            pobjDataElement.pendingValue.indexOf("}") + 1);
                            if (pobjDataElement.pendingValue.indexOf("choice") > 8) {
                                navigationString = pobjDataElement.pendingValue.substring(pobjDataElement.pendingValue.indexOf("choice"));
                            }
                            else if (pobjDataElement.pendingValue.indexOf("jump") > 8) {
                                navigationString = pobjDataElement.pendingValue.substring(pobjDataElement.pendingValue.indexOf("jump"));
                            }
                            else { }

                            pendingValueCleaned = targetString + closeBracketString + navigationString;

                            if (pendingValueCleaned == vocabValue) {
                                return true;
                            }
                        }
                    }
                        //else validate against explicit vocab value
                    else if (pobjDataElement.pendingValue == vocabValue)
                        return true;
                }

                return false;

                //LongId
            case Scorm2004DataModel.DATA_TYPE.LongId:

                return this.validateLength(pobjDataElement, "LongId");

                //Real
            case Scorm2004DataModel.DATA_TYPE.Real:
                //only the following forms should work:   2  2.  2.5  .5   (or any of those preceeded with a minus sign)
                var pattern = /^\-?(([0-9]{1,10})|([0-9]{1,10}\.[0-9]{0,7})|(\.[0-9]{1,7}))$/;

                return pattern.test(pobjDataElement.pendingValue);

                //Integer
            case Scorm2004DataModel.DATA_TYPE.Integer:
                var pattern = /^(|((|-)([0-9]+)))$/;

                return pattern.test(pobjDataElement.pendingValue);

                //Time REQ_88
            case Scorm2004DataModel.DATA_TYPE.Time:
                //this is pretty complex so this code has been placed in a separate function
                return this.isDateTimeValid(pobjDataElement.pendingValue);

                //TimeInterval REQ_89
            case Scorm2004DataModel.DATA_TYPE.TimeInterval:
                var pattern = /^P([0-9]+Y)?([0-9]+M)?([0-9]+D)?(T([0-9]+H)?([0-9]+M)?(([0-9]+S)|(\.[0-9]{1,2}S)|([0-9]+\.[0-9]{1,2}S))?)?$/;

                if (!pattern.test(pobjDataElement.pendingValue))
                    return false;

                var lastChar = pobjDataElement.pendingValue.charAt(pobjDataElement.pendingValue.length - 1);
                if (lastChar == "P" || lastChar == "T")
                    return false;

                return true;

                //Language REQ_83
            case Scorm2004DataModel.DATA_TYPE.Language:

                if (!this.validateLength(pobjDataElement, "LongId"))
                    return false;
                else {
                    //for now, just validate using a pattern & do not compare to ISO code lists
                    var langPattern = /^(i|x|[a-z]{2,3})(\-[a-z]+)*$/i

                    return langPattern.test(pobjDataElement.pendingValue);
                }
        }

        return true;
    }
    catch (e) {
        this.traceNote("Caught an error in isDataValueTypeValid(): " + e);
        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
* Called by isDataValueTypeValid for types known to have length limits
*/
Scorm2004Api.prototype.validateLength = function (pobjDataElement, pstrDataType) {
    this.DebuggerTraceCall('validateLength');
    try {
        //truncate if value is longer than a numeric limit in the constraints
        if (!isNaN(pobjDataElement.constraints)) {
            var currCount = pobjDataElement.pendingValue.length;
            var maxCount = Number(pobjDataElement.constraints);

            if (currCount > maxCount) {
                this.traceError(pstrDataType + "length limit is " + maxCount + ". Value length is " + currCount + ".");
                return false;
            }
        }
    }
    catch (e) {
        this.traceNote("Caught an error in validateLength(): " + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }

    return true;
}

/**
* Returns true if the data of pobjDataElement is in the correct range of values according to its defined constraints.
* @param pobjDataElement the data element to test
* @param pstrDataElementName the name of the data element for some very specific tests
*/
Scorm2004Api.prototype.isDataValueInRange = function (pobjDataElement) {
    this.DebuggerTraceCall('isDataValueInRange');
    try {
        //returns true if the data of pstrDataElementValue is in the correct range of values according to the type of element pobjDataElement
        var blnInRange = false;
        var constraints = "";
        var arrConstraints;

        switch (pobjDataElement.dataType) {
            //Real or Integer - low:high  where ".." is for infinity                                
            case Scorm2004DataModel.DATA_TYPE.Real:
            case Scorm2004DataModel.DATA_TYPE.Integer:
                constraints = pobjDataElement.constraints;

                var numValue = Number(pobjDataElement.pendingValue);

                if (typeof constraints == "string" && constraints.length > 0) {
                    arrConstraints = constraints.split(":");
                    var low = arrConstraints[0];
                    var high = arrConstraints[1];

                    if (low != "..")
                        low = Number(low);

                    if (high != "..")
                        high = Number(high);

                    if (low == ".." && high == "..")
                        return true; //any value is in range

                    else if (low == ".." && numValue > high)
                        return false;  //values that exceed the max are not in range

                    else if (high == ".." && numValue < low)
                        return false;  //values that are below the min are not in range

                    else if (numValue < low || numValue > high)
                        return false;

                    //otherwise, it's in range
                    return true;
                }
                break;
        }

        return true;
    }
    catch (e) {
        this.traceNote("Caught an error in isDataValueInRange(): " + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
* Returns true if the required dependencies are in place, specifically for interactions 
* and objectives -- the "id" and "type" properties must be set before any other properties are set.
* @param pobjDataElement the data element to test
* @param pstrDataElementName the name of the data element for some very specific tests
*/
Scorm2004Api.prototype.areRequiredDependenciesInPlace = function (pobjDataElement, pstrDataElementName) {
    this.DebuggerTraceCall('areRequiredDependenciesInPlace');
    try {
        if (pstrDataElementName.indexOf("cmi.interactions.") == 0) {
            //if setting cmi.interactions.n.(anything but id or type) but cmi.interactions.n.id is not initialized, return false

            //interactions requirements:
            //REQ_64.5.2.5, REQ_64.6.5, REQ_64.5.2.5, REQ_64.8.5, REQ_64.11.5, REQ_64.12.5, 
            //and part of: REQ_64.7.2.5, REQ_64.9.5
            var pattern = /^(cmi\.interactions\.)([0-9]+)(\.)(id|type)/
            if (!pattern.test(pstrDataElementName)) {
                //0	   1           2  3
                //cmi.interactions.20.thispart
                var arrName = pstrDataElementName.split(".");

                var indexOfInteraction = Number(arrName[2]);

                if (!this.activeDataModel.cmi.interactions.__array[indexOfInteraction].id.isInitialized)
                    return false;

                //if setting cmi.interactions.n.correct_responses.n.pattern or cmi.interactions.n.learner_response  but cmi.interactions.n.type is not initialized, return false
                //(REQ_64.7.2.5, REQ_64.9.5)
                pattern = /^(cmi\.interactions\.)([0-9]+)(\.)(correct_responses|learner_response)/

                if (pattern.test(pstrDataElementName)) {
                    if (!this.activeDataModel.cmi.interactions.__array[indexOfInteraction].type.isInitialized)
                        return false;
                }
            }
        }

        else if (pstrDataElementName.indexOf("cmi.objectives.") == 0) {
            //if setting cmi.objectives.n.(anything but id) but cmi.objectives.n.id is not initialized, return false

            //objectives requirements:
            //REQ_72.4.2.6, REQ_72.4.3.5, REQ_72.4.4.5, REQ_72.4.5.5, REQ_72.5.7, REQ_72.6.5, REQ_72.7.5
            var pattern = /^(cmi\.objectives\.)([0-9]+)(\.)id/
            if (!pattern.test(pstrDataElementName)) {
                //0	   1         2  3
                //cmi.objectives.20.thispart
                var arrName = pstrDataElementName.split(".");

                var indexOfObjective = Number(arrName[2]);

                if (!this.activeDataModel.cmi.objectives.__array[indexOfObjective].id.isInitialized)
                    return false;
            }
        }

        //returns true if the required dependencies of pobjDataElement are in place
        return true;
    }
    catch (e) {
        this.traceNote("Caught an error in areRequiredDependenciesInPlace(): " + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

/**
* Updates completion_status if completion_threshold and progress_measure are set.
* Updates success_status if scaled_passing_score and score.scaled are set.
*/
Scorm2004Api.prototype.evaluateOverrides = function (pstrDataElementName) {
    var blnOverrideMade = false;

    //var c = this.activeDataModel.cmi;
    var c = this.courseDataModel[this.activeSCOIndex].cmi;    

    this.DebuggerTraceCall('evaluateOverrides');

    try {
        //REQ_59.5.1 and REQ_59.5.2
        if (pstrDataElementName == "cmi.completion_status" || pstrDataElementName == "cmi.progress_measure") {
            if (c.completion_threshold.isInitialized && c.progress_measure.setBySco) {
                if (Number(c.completion_threshold.value) > 0
                    && (
                        c.progress_measure.value == null
                        || c.progress_measure.value == ""
                        || Number(c.progress_measure.value) < Number(c.completion_threshold.value)
                        )
                    ) {
                    if (c.completion_status.value != "incomplete") {
                        c.completion_status.initialize("incomplete");
                        blnOverrideMade = true;
                    }
                }
                else {
                    if (c.completion_status.value != "completed") {
                        c.completion_status.initialize("completed");
                        blnOverrideMade = true;
                    }
                }

                if (blnOverrideMade) {
                    this.traceNote('"cmi.completion_status" was overridden as "' + c.completion_status.value + '". (progress_measure is "' + c.progress_measure.value + '" and completion_threshold is "' + c.completion_threshold.value + '")');

                    //TO DO:
                    //this.reportToAPIInspector("cmi.completion_status", c.completion_status.value);
                }
            }
        }
        //REQ_77.5.1 and REQ_77.5.2
        else if (pstrDataElementName == "cmi.success_status" || pstrDataElementName == "cmi.score.scaled") {
            if (c.scaled_passing_score.isInitialized) {
                if (c.score.scaled.value == null || c.score.scaled.value == "") {
                    c.success_status.initialize("unknown");
                    blnOverrideMade = true;
                }
                else {
                    if (Number(c.score.scaled.value) < Number(c.scaled_passing_score.value)) {
                        if (c.success_status.value != "failed") {
                            c.success_status.initialize("failed");
                            blnOverrideMade = true;
                        }
                    }
                    else {
                        if (c.success_status.value != "passed") {
                            c.success_status.initialize("passed");
                            blnOverrideMade = true;
                        }
                    }
                }
            }
            else {
                if (c.score.scaled.value != null && c.score.scaled.value != "" && c.success_status.value == "") {
                    c.success_status.initialize("unknown");
                    blnOverrideMade = true;
                }
            }

            if (blnOverrideMade) {
                this.traceNote('"cmi.success_status" was overridden as "' + c.success_status.value + '". (score.scaled is "' + c.score.scaled.value + '" and scaled_passing_score is "' + c.scaled_passing_score.value + '")');
                //TO DO
                //this.reportToAPIInspector("cmi.success_status", c.success_status.value);
            }
        }
    }
    catch (e) {
        this.traceNote("Caught an error in evaluateOverrides(): " + e);

        //propogate the error up to the caller to be handled
        throw (e);
    }
};

///**
//* Returns the specified number of milliseconds to a SCORM 2004 Time Interval format.
//* The Time Interval format is: P[yY][mM][dD][T[hH][mM][sS]]
//*/
//Scorm2004Api.prototype.msToTimeInterval = function (pintMilliseconds) {
//    var seconds, minutes, hours;

//    seconds = Math.floor(pintMilliseconds / 1000);
//    minutes = Math.floor(seconds / 60);
//    seconds = seconds % 60;
//    hours = Math.floor(minutes / 60);
//    minutes = minutes % 60;

//    return "PT" + hours + "H" + minutes + "M" + seconds + "S";
//};

///**
//* Returns the specified SCORM 2004 Time Interval format converted to milliseconds.
//* The Time Interval format is: P[yY][mM][dD][T[hH][mM][sS]]
//*
//* NOTE: This currently *ignores* Months and Years because those values cannot be exactly converted to milliseconds
//*/
//Scorm2004Api.prototype.timeIntervalToMs = function (pstrTimeInterval) {
//    if (pstrTimeInterval == "")
//        return 0;

//    var years = 0; //not currently used here
//    var months = 0; //not currently used here
//    var days = 0;
//    var hours = 0;
//    var minutes = 0;
//    var seconds = 0;

//    var milliseconds = 0;

//    var patternS = /([0-9]+)(S)/;
//    var patternM = /([0-9]+)(M)/;
//    var patternH = /([0-9]+)(H)/;
//    var patternD = /([0-9]+)(D)/;

//    try { seconds = Number(patternS.exec(pstrTimeInterval)[1]); } catch (e) { }
//    try { minutes = Number(patternM.exec(pstrTimeInterval)[1]); } catch (e) { }
//    try { hours = Number(patternH.exec(pstrTimeInterval)[1]); } catch (e) { }
//    try { days = Number(patternD.exec(pstrTimeInterval)[1]); } catch (e) { }

//    milliseconds += seconds * 1000;
//    milliseconds += minutes * 60 * 1000;
//    milliseconds += hours * 60 * 60 * 1000;
//    milliseconds += days * 24 * 60 * 60 * 1000;

//    return milliseconds;
//};


var secondPart = 1000;
var minutePart = secondPart * 60;
var hourPart = minutePart * 60;
var dayPart = hourPart * 24;
var monthPart = dayPart * (((365 * 4) + 1) / 48);
var yearPart = monthPart * 12;

function IsFormatMatching(str) {
    if (str.search(/[PYMDTHS]/) >= 0) {
        return true;
    } else {
        return false;
    }
}

Scorm2004Api.prototype.msToTimeInterval = function (msTime) {
    var result = "";
    var time1;
    var time2;
    var time3;
    var time4;
    var days;
    var time5;
    var time6;
    time1 = msTime;
    time6 = Math.floor(time1 / yearPart);
    time1 -= (time6 * yearPart);
    time5 = Math.floor(time1 / monthPart);
    time1 -= (time5 * monthPart);
    days = Math.floor(time1 / dayPart);
    time1 -= (days * dayPart);
    time4 = Math.floor(time1 / hourPart);
    time1 -= (time4 * hourPart);
    time3 = Math.floor(time1 / minutePart);
    time1 -= (time3 * minutePart);
    time2 = Math.floor(time1 / secondPart);
    time1 -= (time2 * secondPart);
    if (time6 > 0) {
        result += time6 + "Y";
    }
    if (result > 0) {
        ScormTime += time5 + "M";
    }
    if (days > 0) {
        result += days + "D";
    }
    if ((time1 + time2 + time3 + time4) > 0) {
        result += "T";
        if (time4 > 0) {
            result += time4 + "H";
        }
        if (time3 > 0) {
            result += time3 + "M";
        }
        if ((time1 + time2) > 0) {
            result += time2;
            if (time1 > 0) {
                result += "." + time1;
            }
            result += "S";
        }
    }
    if (result === "") {
        result = "T0H0M0S";
    }
    result = "P" + result;
    return result;
}

Scorm2004Api.prototype.timeIntervalToMs = function (timeInterval) {
    this.DebuggerTraceCall('timeIntervalToMs');
    if (timeInterval === "") {
        return 0;
    }
    var result = 0;
    var time1;
    var time2;
    var time3;
    var time4 = 0;
    var time5 = 0;
    var time6 = 0;
    var days = 0;
    var time7 = 0;
    var time8 = 0;
    timeInterval = new String(timeInterval);
    time1 = "";
    time2 = "";
    time3 = false;
    for (var i = 1; i < timeInterval.length; i++) {
        time2 = timeInterval.charAt(i);
        if (IsFormatMatching(time2)) {
            switch (time2.toUpperCase()) {
                case "Y":
                    time8 = parseInt(time1, 10);
                    break;
                case "M":
                    if (time3) {
                        time5 = parseInt(time1, 10);
                    } else {
                        time7 = parseInt(time1, 10);
                    }
                    break;
                case "D":
                    days = parseInt(time1, 10);
                    break;
                case "H":
                    time6 = parseInt(time1, 10);
                    break;
                case "S":
                    time4 = parseFloat(time1);
                    break;
                case "T":
                    time3 = true;
                    break;
            }
            time1 = "";
        } else {
            time1 += "" + time2;
        }
    }
    result = (time8 * yearPart) + (time7 * monthPart) + (days * dayPart) + (time6 * hourPart) + (time5 * minutePart) + (time4 * secondPart);
    result = Math.round(result);
    return result;
}

/**
* Returns the total time the learner has been in this lesson... REQ_76.4
*/
Scorm2004Api.prototype.getTotalTimeInSeconds = function () {
    var totalTimeInMS = 0;
    var totalSessionTimeInMS = 0;
    this.DebuggerTraceCall('getTotalTimeInSeconds');

    //if the sco has reported session time, use that
    //if (this.activeDataModel.cmi.session_time.isInitialized)
        //totalSessionTimeInMS = this.timeIntervalToMs(this.activeDataModel.cmi.session_time.value);

        //else calulate the session time
    //else {
        totalSessionTimeInMS = (new Date()) - this.sessionTimeStart;

        //update the session time value but don't mark the element as initialized
        this.activeDataModel.cmi.session_time.value = this.msToTimeInterval(totalSessionTimeInMS);
    //}

    totalTimeInMS = this.lastLaunchTotalTimeInMS + totalSessionTimeInMS;

    totalTimeInTimeInterval = this.msToTimeInterval(totalTimeInMS);

    //Initialize the model element for next time access the updated value
    this.courseDataModel[this.activeSCOIndex].cmi.total_time.initialize(totalTimeInTimeInterval);

    return totalTimeInMS / 1000;
}

/**
* If the current date/time has reached the time-out date/time, time out
*/
Scorm2004Api.prototype.checkTimeout = function () {
    this.DebuggerTraceCall('checkTimeout');
    var now = new Date();

    if (now >= this.timeoutDateTime) {
        window.clearTimeout(this.checkTimeoutInterval);
        this.timeout();
    }
}

/**
* Called by checkTimeout if the session time limit has been exceeded.
*/
Scorm2004Api.prototype.timeout = function () {
    this.DebuggerTraceCall('timeout');
    if (this.activeState != Scorm2004Api.STATE.Running)
        return;

    var c = this.activeDataModel.cmi;

    switch (c.time_limit_action.value) {
        case "exit,message":
            //message
            this.doJSAlert("You have exceeded the allowed time limit for this lesson. This lesson will now close.");

            //set exit to time-out
            this.traceNote("Timed out. Setting cmi.exit to 'time-out'.");
            c.exit.initialize("time-out");
            //this.reportToAPIInspector("cmi.exit", c.exit.value);

            //call Terminate
            this.traceNote("time_limit_action is 'exit,message', so calling Terminate.");
            this.terminateFromTimeout = true;
            this.Terminate("");
            this.courseLaunchController.processLMSNavRequest("exitAll");

            break;

        case "exit,no message":
            //set exit to time-out
            this.traceNote("Timed out. Setting cmi.exit to 'time-out'.");
            c.exit.initialize("time-out");
            //this.reportToAPIInspector("cmi.exit", c.exit.value);

            //call Terminate
            this.traceNote("time_limit_action is 'exit,no message', so calling Terminate.");
            this.terminateFromTimeout = true;
            this.Terminate("");

            this.courseLaunchController.processLMSNavRequest("exitAll");
            break;

        case "continue,message":
            this.doJSAlert("You have exceeded the allowed time limit for this lesson, however you may continue working.");
            this.traceNote("Timed out, but time_limit_action is 'continue,message', so learner is allowed to continue.");
            break;

        case "continue,no message":
            this.traceNote("Timed out, but time_limit_action is 'continue,no message', so learner is allowed to continue.");
            break;

        default:
            this.traceNote("Timed out, but time_limit_action is an unknown value, so doing nothing.");
            break;
    }
};

/**
* Returns true if param is a valid SCORM 2004 "Time" timestamp format
* 
* See REQ_88.1. Some valid examples are:
* 
* 	a[0] = "2007"; // year - 4 digits - 1970 <= YYYY <= 2038
* 	a[1] = "2007-05"; // month - 2 digits - 01 <= MM <= 12
* 	a[2] = "2007-05-31"; // day - 2 digits - 01 <= DD <= 31
* 	a[3] = "2007-05-31T23"; // T then hours - 2 digits - 00 <= hh <= 23
* 	a[4] = "2007-05-31T23:59"; // minutes - 2 digits - 00 <= mm <= 59
* 	a[5] = "2007-05-31T23:59:59"; // seconds - 2 digits - 00 <= ss <= 59
* 	a[6] = "2007-05-31T23:59:59.99"; // fractions of a second - 1 or 2 digits
* 	a[7] = "2007-05-31T23:59:59.99Z"; // time zone designator - Z for UTC
* 	a[8] = "2007-05-31T23:59:59.99+12"; // time zone designator +hh
* 	a[9] = "2007-05-31T23:59:59.99-08"; // time zone designator -hh
* 	a[10] = "2007-05-31T23:59:59.99+12:30"; // time zone designator +hh:mm
* 	a[11] = "2007-05-31T23:59:59.99-08:30"; // time zone designator -hh:mm
*/
Scorm2004Api.prototype.isDateTimeValid = function (strDateTime) {

    var dateTime;
    var datePart, timePart;
    this.DebuggerTraceCall('isDateTimeValid');
    if (strDateTime.length == 0)
        return true;

    dateTime = strDateTime.split("T");

    //there should only be one or two elements - date, or date and time
    if (dateTime.length > 2)
        return false;

    if (!this.isDateValid(dateTime[0]))
        return false;

    if (dateTime.length == 2 && !this.isTimeValid(dateTime[1]))
        return false;

    return true;
}

/**
* Used by isDateTimeValid() to validate the 
* date portion of a SCORM 2004 "Time" timestamp format.
* 			 
* Note: I'm not sure why Number() wasn't used for numeric string to
* number conversion.
* However parseInt("08") == 0 instead of 8, so for number conversion
* here we'll use parseFloat, which parseFloat("08") == 8
*/
Scorm2004Api.prototype.isDateValid = function (strDate) {
    this.DebuggerTraceCall('isDateValid');
    //validate date digits & characters
    // YYYY       -MM         -DD
    var datePattern = /^[0-9]{4}(|\-[0-9]{2}(|\-[0-9]{2}))$/

    if (!datePattern.test(strDate))
        return false;

    //the pattern is correct, now make sure each value is in range
    var arrDate = strDate.split("-");

    //1970 <= year <= 2038
    var year = parseFloat(arrDate[0]);

    if (year < 1970 || year > 2038)
        return false;

    if (arrDate.length > 1) {
        var month = parseFloat(arrDate[1]);

        if (month < 1 || month > 12)
            return false;

        if (arrDate.length == 3) {
            var date = parseFloat(arrDate[2]);

            if (date < 1 || date > 31)
                return false;

            //make sure the day is valid for this month & year (i.e. 2007-02-31 is not valid) - test by setting/reading a Date()
            var dateTest = new Date(year, month - 1, date);

            if (dateTest.getFullYear() != year || dateTest.getMonth() + 1 != month || dateTest.getDate() != date)
                return false;
        }
    }

    return true;
}

/**
* Used sed by isDateTimeValid() to validate the 
* time portion of a SCORM 2004 "time" timestamp type.
* 
* Note: I'm not sure why Number() wasn't used for numeric string to
* number conversion.
* However parseInt("08") == 0 instead of 8, so for number conversion
* here we'll use parseFloat, which parseFloat("08") == 8
*/
Scorm2004Api.prototype.isTimeValid = function (strTime) {
    this.DebuggerTraceCall('isTimeValid');
    //validate time date digits & characters
    // hh         :mm         :ss        .1 or .25    Z or +/-hh          :mm
    var timePattern = /^[0-9]{2}(|:[0-9]{2}(|\:[0-9]{2}(|\.[0-9]{1,2}(|Z|(\+|\-)[0-9]{2}(|\:[0-9]{2})))))$/

    if (!timePattern.test(strTime))
        return false;

    //is there a time zone offset? ( find a Z, + or - )
    var charTZD = null;
    if (strTime.indexOf("Z") > -1) {
        charTZD = "Z";
    }
    else if (strTime.indexOf("+") > -1) {
        charTZD = "+";
    }
    else if (strTime.indexOf("-") > -1) {
        charTZD = "-";
    }

    if (charTZD == null) {
        var timeWithoutOffset = strTime;
        var timeOffset = "";
    }
    else {
        var timeOffsetStartPos = strTime.indexOf(charTZD);
        var timeWithoutOffset = strTime.substring(0, timeOffsetStartPos - 1);
        var timeOffset = strTime.substring(timeOffsetStartPos + 1);
    }

    //validate the time without the offset
    var arrTimeWithoutOffset = timeWithoutOffset.split(":");

    var hours = parseFloat(arrTimeWithoutOffset[0]);

    if (hours > 23)
        return false;

    if (arrTimeWithoutOffset.length > 1) {
        var minutes = parseFloat(arrTimeWithoutOffset[1]);

        if (minutes > 59)
            return false;

        if (arrTimeWithoutOffset.length > 2) {
            var seconds = parseFloat(arrTimeWithoutOffset[2]);

            if (seconds > 59.99)
                return false;
        }
    }

    //if there is a timezone offset, validate it
    if (charTZD == "+" || charTZD == "-") //assume Z has already been validated by the regexp
    {
        var arrTimeOffset = timeOffset.split(":");

        hours = parseFloat(arrTimeOffset[0]);

        if (hours > 23)
            return false;

        if (arrTimeOffset.length > 1) {
            minutes = parseFloat(arrTimeOffset[1]);

            if (minutes > 59)
                return false;
        }
    }

    return true;
}

/**
* Converts a valid SCORM 2004 timestamp (Time datatype) to a full timestamp
* with any missing parts filled in...
*
* @param strDateTime YYYY[-MM[-DD[Thh[:mm[:ss[.s[TZD]]]]]]]
* 
* @return "MM-DD-YYYY HH:MM:SS"
*         "01-01-YYYY 00:00:00"
* 
* note: the time zone designator is currently *ignored* and partial
* seconds are truncated
*/
Scorm2004Api.prototype.expandDateTime = function (strDateTime) {
    this.DebuggerTraceCall('expandDateTime');
    if (strDateTime == null)
        return null;

    var dateTime;
    var datePart, timePart;

    if (strDateTime.length == 0)
        return true;

    dateTime = strDateTime.split("T");

    var expandedDateTime = this.expandDate(dateTime[0]) + " ";

    if (dateTime.length == 1)
        expandedDateTime += "00:00:00.0";
    else
        expandedDateTime += this.expandTime(dateTime[1]);

    return expandedDateTime;
}

/**
* Used by expandDateTime() to expand a valid
* date portion of a SCORM 2004 "time" timestamp type to MM-DD-YYYY.
*/
Scorm2004Api.prototype.expandDate = function (strDate) {
    this.DebuggerTraceCall('expandDate');
    //the pattern is correct, now make sure each value is in range
    var arrDate = strDate.split("-");

    //1970 <= year <= 2038
    var year = arrDate[0];

    if (arrDate.length == 1)
        return "01-01-" + year;
    else {
        var month = arrDate[1];

        if (arrDate.length == 2)
            return month + "-01-" + year;
        else {
            var date = parseFloat(arrDate[2]);

            return month + "-" + date + "-" + year;
        }
    }
}

/**
* Used by expandDateTime() to expand a valid
* time portion of a SCORM 2004 "time" timestamp type to HH:MM:SS.s.
* 			
* note: the time zone portion is ignored.
*/
Scorm2004Api.prototype.expandTime = function (strTime) {
    this.DebuggerTraceCall('expandTime');
    //is there a time zone offset? ( find a Z, + or - )
    var charTZD = null;
    if (strTime.indexOf("Z") > -1) {
        charTZD = "Z";
    }
    else if (strTime.indexOf("+") > -1) {
        charTZD = "+";
    }
    else if (strTime.indexOf("-") > -1) {
        charTZD = "-";
    }

    if (charTZD == null) {
        var timeWithoutOffset = strTime;
        var timeOffset = "";
    }
    else {
        var timeOffsetStartPos = strTime.indexOf(charTZD);
        var timeWithoutOffset = strTime.substring(0, timeOffsetStartPos - 1);
        var timeOffset = strTime.substring(timeOffsetStartPos + 1);
    }

    //work with the time without the offset
    var arrTimeWithoutOffset = timeWithoutOffset.split(":");

    var hours = arrTimeWithoutOffset[0];

    if (arrTimeWithoutOffset.length == 1)
        return hours + ":00:00.0"
    else {
        var minutes = arrTimeWithoutOffset[1];

        if (arrTimeWithoutOffset.length == 2)
            return hours + ":" + minutes + ":00.0"
        else {
            var seconds = arrTimeWithoutOffset[2];

            var secondsWithoutDecimal = seconds.split(".")[0];

            return hours + ":" + minutes + ":" + secondsWithoutDecimal;
        }
    }
}

/** Added by Chetu
* Used to initialize the objectives in course data model 
* time portion of a SCORM 2004 "time" timestamp type to HH:MM:SS.s.
*/
Scorm2004Api.prototype.SetObjectivesInDataModel = function () {
    this.DebuggerTraceCall('SetObjectivesInDataModel');

    for (var i = 0; i < this.imsManifest.organizations[0].items.length; i++) {
        var imsssSequencing = this.imsManifest.organizations[0].items[i].imsssSequencing;
        if (imsssSequencing != null) {
            var imsObjectives = imsssSequencing.imsssObjectives;

            if (imsObjectives != null && imsObjectives != undefined) {

                if (imsObjectives.primaryObjective != null
                    && imsObjectives.primaryObjective != undefined
                    && imsObjectives.primaryObjective.objectiveId != null
                    && imsObjectives.primaryObjective.objectiveId.length > 0) {

                    var objetiveInstance = new Scorm2004Objective();
                    var idItem = new Scorm2004DataItem();
                    idItem.value = imsObjectives.primaryObjective.objectiveId;
                    idItem.isInitialized = true;
                    objetiveInstance.id = idItem;
                    objetiveInstance.objectiveContributestoRollup.value = true;

                    var itemObjectives = this.scorm2004ADLNav.GetItemObjectivesFromCourseDataModel(i, this.scorm2004ADLNav.cEnum.ObjectiveTypes.all);
                    var objectiveWithId = this.scorm2004ADLNav.GetObjectiveWithId(idItem.value, itemObjectives, this.scorm2004ADLNav.cEnum.GetObjective.item);
                    if (objectiveWithId == null) {
                        this.appendToCollection(this.courseDataModel[i].cmi.objectives, objetiveInstance);
                    }
                }

                if (imsObjectives.objectives != null && imsObjectives.objectives != undefined) {
                    var j = 0;
                    while (j < imsObjectives.objectives.length) {
                        var objetiveInstance = new Scorm2004Objective();
                        var idItem = new Scorm2004DataItem();
                        idItem.value = imsObjectives.objectives[j].objectiveId;
                        idItem.isInitialized = true;
                        objetiveInstance.id = idItem;

                        var itemObjectives = this.scorm2004ADLNav.GetItemObjectivesFromCourseDataModel(i, this.scorm2004ADLNav.cEnum.ObjectiveTypes.all);
                        var objectiveWithId = this.scorm2004ADLNav.GetObjectiveWithId(idItem.value, itemObjectives, this.scorm2004ADLNav.cEnum.GetObjective.item);
                        if (objectiveWithId == null) {
                            this.appendToCollection(this.courseDataModel[i].cmi.objectives, objetiveInstance);
                        }
                        j++;
                    }
                }
            }
            if (imsssSequencing.idRef != null) {


                var sequencingCollection = this.imsManifest.sequencingCollection;
                if (sequencingCollection != null && sequencingCollection.length > 0) {
                    for (var index = 0; index < sequencingCollection.length; index++) {
                        if (sequencingCollection[index].id == imsssSequencing.idRef) {

                            var imsssObjectives = sequencingCollection[index].imsssObjectives;

                            if (imsssObjectives != null) {

                                var primaryObjective = imsssObjectives.primaryObjective;

                                if (primaryObjective != null
                                    && primaryObjective != undefined
                                    && primaryObjective.objectiveId != null
                                    && primaryObjective.objectiveId.length > 0
                                    ) {

                                    var objetiveInstance = new Scorm2004Objective();
                                    var idItem = new Scorm2004DataItem();
                                    idItem.value = primaryObjective.objectiveId;
                                    idItem.isInitialized = true;

                                    objetiveInstance.id = idItem;
                                    objetiveInstance.objectiveContributestoRollup.value = true;

                                    var itemObjectives = this.scorm2004ADLNav.GetItemObjectivesFromCourseDataModel(i, this.scorm2004ADLNav.cEnum.ObjectiveTypes.all);
                                    var objectiveWithId = this.scorm2004ADLNav.GetObjectiveWithId(idItem.value, itemObjectives, this.scorm2004ADLNav.cEnum.GetObjective.item);
                                    if (objectiveWithId == null) {
                                        this.appendToCollection(this.courseDataModel[i].cmi.objectives, objetiveInstance);
                                    }
                                }

                                var objectives = imsssObjectives.objectives;

                                if (objectives != null) {
                                    for (var counter = 0; counter < objectives.length; counter++) {


                                        var objetiveInstance = new Scorm2004Objective();
                                        var idItem = new Scorm2004DataItem();
                                        idItem.value = objectives[counter].objectiveId;
                                        idItem.isInitialized = true;
                                        objetiveInstance.id = idItem;

                                        var itemObjectives = this.scorm2004ADLNav.GetItemObjectivesFromCourseDataModel(i, this.scorm2004ADLNav.cEnum.ObjectiveTypes.all);
                                        var objectiveWithId = this.scorm2004ADLNav.GetObjectiveWithId(idItem.value, itemObjectives, this.scorm2004ADLNav.cEnum.GetObjective.item);
                                        if (objectiveWithId == null) {
                                            this.appendToCollection(this.courseDataModel[i].cmi.objectives, objetiveInstance);
                                        }

                                    }
                                }
                            }

                        }
                    }
                }


                var imsssObjectives = this.scorm2004ADLNav.FetchSequencingAttributeFromSubnode(imsssSequencing, "imsssObjectives");
                if (imsssObjectives != null && imsssObjectives != undefined) {
                    var primaryObjective = imsssObjectives.primaryObjective;
                    var ObjectivesArray = imsssObjectives.objectives;

                    if (primaryObjective != null || primaryObjective != undefined) {
                        var objetiveInstance = new Scorm2004Objective();
                        var idItem = new Scorm2004DataItem();
                        idItem.value = primaryObjective.objectiveId;
                        idItem.isInitialized = true;
                        objetiveInstance.id = idItem;

                        var itemObjectives = this.scorm2004ADLNav.GetItemObjectivesFromCourseDataModel(i, this.scorm2004ADLNav.cEnum.ObjectiveTypes.all);
                        var objectiveWithId = this.scorm2004ADLNav.GetObjectiveWithId(idItem.value, itemObjectives, this.scorm2004ADLNav.cEnum.GetObjective.item);

                        if (objectiveWithId == null) {
                            this.appendToCollection(this.courseDataModel[i].cmi.objectives, objetiveInstance);
                        }
                    }

                    if ((ObjectivesArray != null || ObjectivesArray != undefined) && ObjectivesArray.length > 0) {
                        var j = 0;
                        while (j < ObjectivesArray.objectives.length) {

                            var objetiveInstance = new Scorm2004Objective();
                            var idItem = new Scorm2004DataItem();
                            idItem.value = ObjectivesArray.objectives[j].objectiveId;
                            idItem.isInitialized = true;
                            objetiveInstance.id = idItem;

                            var itemObjectives = this.scorm2004ADLNav.GetItemObjectivesFromCourseDataModel(i, this.scorm2004ADLNav.cEnum.ObjectiveTypes.all);
                            var objectiveWithId = this.scorm2004ADLNav.GetObjectiveWithId(idItem.value, itemObjectives, this.scorm2004ADLNav.cEnum.GetObjective.item);
                            if (objectiveWithId == null) {
                                this.appendToCollection(this.courseDataModel[i].cmi.objectives, objetiveInstance);
                            }
                            j++;
                        }
                    }
                }
            }
        }
    }
}


/** Added by Chetu
* Used to initialize the objectives in course data model 
*/
Scorm2004Api.prototype.InitilzeObjectiveIfNotInitialized = function (itemIndex) {
    this.DebuggerTraceCall('InitilzeObjectiveIfNotInitialized');

    try {
        var objectives = this.scorm2004ADLNav.GetItemObjectivesFromManifest(itemIndex, this.scorm2004ADLNav.cEnum.ObjectiveTypes.all);

        if (objectives != null && objectives.length > 0) {
            for (var index = 0; index < objectives.length; index++) {
                var objetiveInstance = new Scorm2004Objective();
                var idItem = new Scorm2004DataItem();
                idItem.value = objectives[index].objectiveId;
                idItem.isInitialized = true;
                objetiveInstance.id = idItem;


                var itemObjectives = this.scorm2004ADLNav.GetItemObjectivesFromCourseDataModel(itemIndex, this.scorm2004ADLNav.cEnum.ObjectiveTypes.all);
                var objectiveWithId = this.scorm2004ADLNav.GetObjectiveWithId(idItem.value, itemObjectives, this.scorm2004ADLNav.cEnum.GetObjective.item);
                if (objectiveWithId == null) {
                    this.appendToCollection(this.courseDataModel[itemIndex].cmi.objectives, objetiveInstance);
                }
            }
        }
    }
    catch (e) {
        this.traceError("Error caught in InitilzeObjectiveIfNotInitialized", "", 1);
        throw e;
    }
}

//Method to add the caller method name, passed as 'funName' parameter, into the logger panel
Scorm2004Api.prototype.DebuggerTraceCall = function (funName) {
    if (DebugMode.toLowerCase() == "true") {
        this.traceCall(funName);
        if (this.courseDataModel != undefined && this.courseDataModel.length > 1) {
            this.traceCall("status: " + this.courseDataModel[1].cmi.success_status.value);
        }
    }
}

// Method to redirect control to LMS if the terminate request come for a single sco package
function ReturnToLMSForSingleSCO() {
    singleScoReturnWaitingTime = singleScoReturnWaitingTime + 1;
    // if pending navigation request is not set as 'exit' or 'exitAll' even after 5 sec. then call exit by itself and clear the interval
    if (pendingNavigationRequest == 'Exit' || pendingNavigationRequest == "ExitAll") {
        clearInterval(returnSingleSco);
    }
    if (pendingNavigationRequest == null && singleScoReturnWaitingTime > 10) {
        clearInterval(returnSingleSco);
        scorm2004ApiInstance.SetValue('adl.nav.request', 'exit');
    }
}

function CallOverallSequencingOnTerminateCompleted() {
    if (terminateCompleted && pendingNavigationRequest != null) {
        scorm2004ApiInstance.scorm2004ADLNav.OverallSequencingProcess(pendingNavigationRequest, this.activeSCOIdentifier);
        pendingNavigationRequest = null;
        clearInterval(terminateInterval);
    }
    else if (pendingNavigationRequest == null) {
        clearInterval(terminateInterval);
    }
}

function StartInterval(msg) {
    if (setvalueCompleted) {
        //Display the loader modal popup
        ShowLoaderModalPopup(msg);

        var href = "../../../../launch/ContentPlaceHolder.html";
        SetContentFrameUrl(href);

        clearInterval(interval);
        clearInterval(returnSingleSco);

        if (pendingNavigationRequest == "exitAll" || pendingNavigationRequest == "suspend" || pendingNavigationRequest == "suspendAll") {
            ReturnWhenGlobalSaveCompleted(true);
        }
        else if (pendingNavigationRequest == "exit") {
            //Return to LMS if exit is called on a single-SCO package
            if (this.imsManifest.organizations[0].items.length <= 2) {
                ReturnWhenGlobalSaveCompleted(true);
            }
            else {
                ReturnWhenGlobalSaveCompleted(false);
            }
        }
    }
}
function TerminateSCOafterExitCall() {
    waitingTime = waitingTime + 1000;
    if (waitingTime > 10000 && scorm2004ApiInstance.activeState != Scorm2004Api.STATE.Terminated) {
        scorm2004ApiInstance.Terminate("");
        HideLoadAndSaveContentModalPopup();
        clearInterval(TerminateSCOwaitingTime);
    }
}

function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}
