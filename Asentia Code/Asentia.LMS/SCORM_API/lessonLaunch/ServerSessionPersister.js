﻿//uses JSONLoader
//uses EventDispatcher

ServerSessionPersister.SESSION_PRESERVE_MINUTES = 5;
ServerSessionPersister.SESSION_PRESERVE_URL = "/_gateway/scorm/setapidata.asp?t=KeepAlive&sid=0&code=0";

function ServerSessionPersister() {
    this.JSONLoader = new JSONLoader("ServerSessionPersister");

    //on events, just propogate the event up
    //this.JSONLoader.addEventListener(Event.LOADED, this.dispatchEvent, this);
    //this.JSONLoader.addEventListener(Event.FATAL_ERROR, this.dispatchEvent, this);
    //this.JSONLoader.addEventListener(Event.ERROR, this.dispatchEvent, this);
    //this.JSONLoader.addEventListener(Event.TRACE, this.dispatchEvent, this);
}

ServerSessionPersister.inherits(EventDispatcher);

//PUBLIC

/**
 * Starts the interval that keeps the session alive every ServerSessionPersister.SESSION_PRESERVE_MINUTES minutes.
 */
ServerSessionPersister.method('start', function () {
    if (!this.started) {
        this.started = true;

        var thisInstance = this;

        this.intervalId = setInterval(
			function () {
			    thisInstance.JSONLoader.sendAndLoad(ServerSessionPersister.SESSION_PRESERVE_URL);
			},
			ServerSessionPersister.SESSION_PRESERVE_MINUTES * 60 * 1000);
    }
});

/**
 * Stops the interval that keeps the session alive.
 */
ServerSessionPersister.method('stop', function () {
    if (this.started) {
        this.started = false;

        clearInterval(this.intervalId);
    }
});

/**
 * Aborts the current JSONLoader and stops the interval.
 */
ServerSessionPersister.method('abort', function () {
    this.JSONLoader.abort();

    this.stop();
});
