﻿var treeMenu = new Object();

treeMenu.closeFolder = "/_images/tree/folder-closed.gif";    //set image path to "closed" folder image
treeMenu.openFolder = "/_images/tree/folder-open.gif";      //set image path to "open" folder image

//////////No need to edit beyond here///////////////////////////

treeMenu.createTree = function (treeId) {

    var ulTags = document.getElementById(treeId).getElementsByTagName("ul");

    for (var i = 0; i < ulTags.length; i++) {
        treeMenu.buildSubTree(treeId, ulTags[i], i);
    }

    treeMenu.flatten(treeId, "expand");
}

treeMenu.buildSubTree = function (treeId, ulElement, index) {

    ulElement.parentNode.className = "submenu";

    ulElement.setAttribute("rel", "closed");

    ulElement.parentNode.onclick = function (e) {
        var subMenu = this.getElementsByTagName("ul")[0];

        if (subMenu.getAttribute("rel") == "closed") {
            subMenu.style.display = "block";
            subMenu.setAttribute("rel", "open");
            ulElement.parentNode.style.backgroundImage = "url('" + treeMenu.openFolder + "')";
        }
        else if (subMenu.getAttribute("rel") == "open") {
            subMenu.style.display = "none";
            subMenu.setAttribute("rel", "closed");
            ulElement.parentNode.style.backgroundImage = "url('" + treeMenu.closeFolder + "')";
        }
        treeMenu.preventPropagate(e);
    }

    ulElement.onclick = function (e) {
        treeMenu.preventPropagate(e);
    }
}

treeMenu.expandSubTree = function (treeId, ulElement) { //expand a UL element and any of its parent ULs
    var rootNode = document.getElementById(treeId);
    var currentNode = ulElement;

    currentNode.style.display = "block";
    currentNode.parentNode.style.backgroundImage = "url('" + treeMenu.openFolder + "')";

    while (currentNode != rootNode) {
        if (currentNode.tagName == "UL") { //if parent node is a UL, expand it too
            currentNode.style.display = "block";
            currentNode.setAttribute("rel", "open"); //indicate it's open
            currentNode.parentNode.style.backgroundImage = "url('" + treeMenu.openFolder + "')";
        }
        currentNode = currentNode.parentNode;
    }
}

treeMenu.flatten = function (treeId, action) { //expand or contract all UL elements
    var ulTags = document.getElementById(treeId).getElementsByTagName("ul");

    for (var i = 0; i < ulTags.length; i++) {
        ulTags[i].style.display = (action == "expand") ? "block" : "none";
        var relValue = (action == "expand") ? "open" : "closed";
        ulTags[i].setAttribute("rel", relValue);
        ulTags[i].parentNode.style.backgroundImage = (action == "expand") ? "url('" + treeMenu.openFolder + "')" : "url('" + treeMenu.closeFolder + "')";
    }
}

////A few utility functions below//////////////////////
treeMenu.preventPropagate = function (e) { //prevent action from bubbling upwards
    if (typeof e != "undefined") {
        e.stopPropagation();
    }
    else {
        event.cancelBubble = true;
    }
}

treeMenu.doTask = function (target, functionRef, taskType) { //assign a function to execute to an event handler (ie: onunload)
    var taskType = (window.addEventListener) ? taskType : "on" + taskType

    if (target.addEventListener) {
        target.addEventListener(taskType, functionRef, false);
    }
    else if (target.attachEvent) {
        target.attachEvent(taskType, functionRef);
    }
}