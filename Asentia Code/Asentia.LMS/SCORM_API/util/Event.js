﻿Event.BUSY = "busy";
Event.CHANGED = "changed";
Event.ERROR = "error";
Event.FATAL_ERROR = "fatalError";
Event.FINISH_STARTED = "finishStarted";
Event.FINISH_COMPLETED = "finishCompleted";
Event.LOADED = "loaded";
Event.TRACE = "trace";
Event.WARNING = "warning";
Event.WINDOW_CLOSED = "windowClosed";
Event.WINDOW_OPENED = "windowOpened";
Event.COURSE_LAUNCH_CONTROLLER_READY = "courseLaunchControllerReady";
Event.SCO_DATA_SAVED = "scoDataSaved";
Event.GLOBAL_DATA_SAVED = "globalDataSaved";
Event.NON_LEAF_DATA_ITEMS_SAVED = "nonLeafDataItemsSaved";
Event.DATA_SAVED_FOR_EXIT = "dataSavedForExit";


function Event(type, target, description)
{
	this.type = type;
	this.target = target;
	this.description = description;
}
