﻿//uses ArrayList

function Queue()
{
	this.arrayList = new ArrayList();
}

Queue.method('push', function(listItem)
{
	this.arrayList.add(listItem);
});

Queue.method('shift', function()
{
	return this.arrayList.remove(0);
});

Queue.method('size', function()
{
	return this.arrayList.size();
});

Queue.method('isEmpty', function()
{
	return (this.arrayList.size() == 0);
});
