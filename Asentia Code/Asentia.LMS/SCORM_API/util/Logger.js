﻿/**
 * Constructor accepts 0 or more objects as arguments to immediately begin 
 * listening to their loggable dispatched events.
 *
 * The events that are currently displayed in the log are:
 *		Event.TRACE
 *		Event.ERROR
 *		Event.FATAL_ERROR
 *		Event.WINDOW_OPENED
 *		Event.WINDOW_CLOSED
 */

//uses Queue

Logger.OUTPUT_DELAY = 500; //number of milliseconds that must past between last call to log event dispatch and when they are outputted
Logger.OUTPUT_QUEUE_LIMIT = 2000; //maximum number of items in the queue before they are outtputted, regardless of the delay

Logger.LABEL_SCROLL_AUTOMATICALLY = "Scroll automatically";
Logger.LABEL_SELECT_ALL = "Select All Log";

Logger.WARNING_COLOR = "#CCCC00";
Logger.ERROR_COLOR = "#FF7F00";
Logger.FATAL_ERROR_COLOR = "#FF0000";

Logger.instanceIndex = 0;

/**
 * Pass the constructor a list of N arguments that are objects which extend EventDispatcher that should be 
 * monitored using this Logger.
 */
function Logger()
{
	this.outputContainer = null; //will point to the <ol> that should contain the log entries 
	
	this.logQueue = new Queue();
	
	for (var i = 0; i < arguments.length; i++)
	{
		if (!arguments[i])
			alert("Logger() - argument at index " + i + " is " + arguments[i] + " so it will not be logged.");
		else
			this.listenForLogEvents(arguments[i]);
	}
	
	this.instanceIndex = Logger.instanceIndex;
	Logger.instanceIndex++;
	
	this.autoScroll = true;
}

Logger.inherits(EventDispatcher);


//PUBLIC

/**
 * Links this logger to the <div> that should contain it; attaches the 
 * <ol> that will contain each log line, attaches the auto-scroll checkbox, starts 
 * to output to the div on the page.
 *
 * Gives the <ol> a className of "Logger"
 */
Logger.prototype.attachToContainer = function (container) {
    var thisInstance = this;

    //create a scrolling <div> to contain the <ol> since having the <ol> scroll has margin issues in IE
    this.scrollingDiv = document.createElement("div");
    this.scrollingDiv.className = "logger";
    container.appendChild(this.scrollingDiv);

    //create the <ol> that will contain each log item and attached it to the specified div
    this.outputContainer = document.createElement("ol");

    this.scrollingDiv.appendChild(this.outputContainer);

    var checkboxDiv = document.createElement("div");
    checkboxDiv.style.textAlign = "right";
    container.appendChild(checkboxDiv);

    //create the checkbox that will be linked to the bottom right corner of the logger
    this.autoScrollCheckbox = document.createElement("input");
    this.autoScrollCheckbox.type = "checkbox";
    this.autoScrollCheckbox.onclick = function () {
        thisInstance.setAutoScroll(this.checked);
    };
    this.autoScrollCheckbox.id = "loggerAutoScroll" + this.instanceIndex;
    checkboxDiv.appendChild(this.autoScrollCheckbox);

    //create the label that is linked to the checkbox
    var label = document.createElement("label");
    label.setAttribute("for", this.autoScrollCheckbox.id);
    label.appendChild(document.createTextNode(Logger.LABEL_SCROLL_AUTOMATICALLY));
    label.style.marginLeft = "5px";
    checkboxDiv.appendChild(label);

    //append a divider between the items
    checkboxDiv.appendChild(document.createTextNode(" | "));

    //create a "Select All" link
    var selectAll = document.createElement("a");
    selectAll.onclick = function () {
        StringHelper.selectTextInObject(thisInstance.outputContainer);
    }
    selectAll.style.cursor = "pointer";
    selectAll.appendChild(document.createTextNode(Logger.LABEL_SELECT_ALL));
    checkboxDiv.appendChild(selectAll);

    //set the default to auto scroll
    this.setAutoScroll(true);
}

//PUBLIC

Logger.prototype.setAutoScroll = function(bln)
{
	this.autoScroll = bln;
	
	this.autoScrollCheckbox.checked = bln;
	
	if (bln)
		this.scrollToBottom();
}


//PRIVATE

/**
 * Call this to have this logger listen to and log the events dispatched from
 * the specified object.
 * @param objectToLog - and object that dispatches one or more of 
 *						the events: Event.TRACE, Event.ERROR, Event.FATAL_ERROR
 */
Logger.prototype.listenForLogEvents = function(objectToLog)
{
	if (!objectToLog)
	{
		alert("Logger.listenForLogEvents - argument is " + objectToLog + " so it will not be logged.");
		return;
	}
		
	var thisInstance = this;
	
	var listener = function(e)
	{
		thisInstance.logEvent(e);
	};
	
	if (objectToLog.addEventListener)
	{
		objectToLog.addEventListener(Event.TRACE, listener);
		objectToLog.addEventListener(Event.ERROR, listener);
		objectToLog.addEventListener(Event.FATAL_ERROR, listener);
		objectToLog.addEventListener(Event.WINDOW_OPENED, listener);
		objectToLog.addEventListener(Event.WINDOW_CLOSED, listener);
	}
}


/**
 * Called by an object that is dispatching an Event.TRACE, Event.ERROR, or Event.FATAL_ERROR
 */
Logger.prototype.logEvent = function(e)
{
	if (this.outputContainer)
	{
		var targetString = "";
		
		if (typeof e.target == "string" && e.target.length > 0)
		{
			targetString = " (" + e.target + ") ";
		}
		
		switch (e.type)
		{
			case Event.WARNING:
				this.logLine("<span style='color:" + Logger.WARNING_COLOR + "'>" + targetString + " WARNING: " + e.description + "</span>");
				break;
				
			case Event.ERROR:
				this.logLine("<span style='color:" + Logger.ERROR_COLOR + "'>" + targetString + " ERROR: " + e.description + "</span>");
				break;
			
			case Event.FATAL_ERROR:
				this.logLine("<span style='color:" + Logger.FATAL_ERROR_COLOR + "'>" + targetString + " FATAL ERROR: " + e.description + "</span>");
				break;
				
			case Event.WINDOW_OPENED:
				this.logLine(targetString + " window opened: <a href='" + e.description + "' target='_blank'>" + e.description + "</span>");
				break;
				
			case Event.WINDOW_CLOSED:
				this.logLine(targetString + " window closed.");
				break;
			
			default:
				this.logLine(targetString + e.description);
		}
	}
}

/**
 * Called by an object that is dispatching an Event.TRACE, Event.ERROR, or Event.FATAL_ERROR
 */
Logger.prototype.logLine = function(line)
{
	var timestamp = new Date();
	
	//if this line is being displayed at a different time than the last line was displayed, draw a timestamp line.
	if (!this.lastTimestampValue || timestamp.valueOf() - 1000 > this.lastTimestampValue)
	{
		this.lastTimestampValue = timestamp.valueOf();
		
		var div = document.createElement("div");
		div.style.marginTop = "10px";
		div.style.marginBottom = "0";
		div.innerHTML = "<b>" + timestamp.toString() + "</b>"; //use the <b> tags so formatting sticks when copying and pasting the output to a new email
		this.logQueue.push(div);
	}
	
	//log this line as a <li> to be attached to the container <ol> in a separate process
	var li = document.createElement("li");
	li.innerHTML = line;
	this.logQueue.push(li);
	
	if (this.logQueue.size() >= Logger.OUTPUT_QUEUE_LINE_LIMIT)
		this.emptyLogQueue();
	else
		this.emptyLogQueueAfterDelay();
	
	/*
	if (this.div)
	{
		switch (e.type)
		{
			case Event.TRACE:
			
				break;
			
			case Event.ERROR:
			
				break;
			
			case Event.FATAL_ERROR:
			
				break;
		}
		
		//THIS IS ONLY TEMPORARY
		this.div.innerHTML += e.description;
	}
	*/
}

/**
 * Calls emptyLogQueue() after the configured delay.
 */
Logger.prototype.emptyLogQueueAfterDelay = function()
{
	var thisInstance = this;
	
	window.clearTimeout(this.emptyLogQueueTimeoutId);
	this.emptyLogQueueTimeoutId = window.setTimeout(
		function()
		{
			thisInstance.emptyLogQueue();
		},
		Logger.OUTPUT_DELAY);
}

/**
 * Dumps the contents of the queue into the emptyLogQueue() after the configured delay.
 */
Logger.prototype.emptyLogQueue = function()
{
	window.clearTimeout(this.emptyLogQueueTimeoutId);
	
	while (this.logQueue.size() > 0)
		this.outputContainer.appendChild(this.logQueue.shift());
	
	if (this.autoScroll)
		this.scrollToBottom();
}

/**
 * Scrolls to the bottom of the <ul>.
 */
Logger.prototype.scrollToBottom = function()
{
	this.scrollingDiv.scrollTop = this.scrollingDiv.scrollHeight;
}
