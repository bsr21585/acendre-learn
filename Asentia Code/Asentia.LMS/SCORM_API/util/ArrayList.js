﻿function ArrayList()
{
	this.arr = [];
}

ArrayList.method('add', function(listItem)
{
	this.arr[this.arr.length] = listItem;
});

ArrayList.method('remove', function(listItemIndex)
{
	if (listItemIndex > -1)
	{
		var removed = this.arr.splice(listItemIndex, 1);
		return removed[0];
	}
});

ArrayList.method('removeByItem', function(listItem)
{
	return this.remove(this.getItemIndex(listItem));
});

ArrayList.method('size', function()
{
	return this.arr.length;
});

ArrayList.method('getItem', function(itemIndex)
{
	return this.arr[itemIndex];
});

ArrayList.method('getItemIndex', function(listItem)
{
	for (var i = 0; i < this.arr.length; i++)
	{
		if (this.arr[i] == listItem)
			return i;
	}
	
	return -1;
});
