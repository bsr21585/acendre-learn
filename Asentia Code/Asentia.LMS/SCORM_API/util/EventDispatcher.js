﻿//uses ArrayList

function EventDispatcher()
{
	//don't instantiate anything here, or it will be shared by all subclasses 
}

/**
 * @param eventName string representing the name of the event
 * @param listenerFunction function to call when the event is dispatched
 * @param callScope optional; the value of "this" within the called listener function
 */
EventDispatcher.method('addEventListener', function(eventName, listenerFunction, callScope)
{
	if (!this.set)
		this.set = [];
	
	var eventListenerList = this.getEventListenerList(eventName);
	
	//add the listener function if it has not already been added.
	if (this.getEventListenerIndex(eventListenerList, listenerFunction) == -1)
	{
		var listenerItem = {
			fn: listenerFunction, 
			scope: callScope
		};
		
		eventListenerList.add(listenerItem);
	}
});

/**
 * @param eventName string representing the name of the event
 * @param listenerFunction function to remove from dispatch list for this event
 */
EventDispatcher.method('removeEventListener', function(eventName, listenerFunction)
{
	var eventListenerList = this.getEventListenerList(eventName);
	
	var index = this.getEventListenerIndex(eventListenerList, listenerFunction);
	
	eventListenerList.remove(index);
});

/**
 * @param eventObject is an object that has a "type" property
 */
EventDispatcher.method('dispatchEvent', function(eventObject)
{    
	var eventListenerList = this.getEventListenerList(eventObject.type);
	var listenerItem;
	
	for (var i = 0; i < eventListenerList.size(); i++)
	{
		listenerItem = eventListenerList.getItem(i);
		
		listenerItem.fn.call(listenerItem.scope, eventObject);
	}
});

//PRIVATE

/**
 * Returns the ArrayList for the event, or if not already created, creates a new ArrayList.
 * @param eventName string representing the name of the event
 */
EventDispatcher.method('getEventListenerList', function(eventName)
{
	var eventListenerList = this.set[eventName];
	
	if (!eventListenerList)
		eventListenerList = this.set[eventName] = new ArrayList();
	
	return eventListenerList;
});


/**
 * Returns the index of the event listener within the specified event listener ArrayList, or -1.
 *
 * @param eventListenerList the ArrayList containing event listener objects (which contain a function and a scope).
 * @param listenerFunction reference to the function to find in the list.
 * @return the index of the function in the list, or -1.
 */
EventDispatcher.method('getEventListenerIndex', function(eventListenerList, listenerFunction)
{
	for (var i = 0; i < eventListenerList.size(); i++)
	{
		if (eventListenerList.getItem(i).fn == listenerFunction)
			return i;
	}
	
	return -1;
});
