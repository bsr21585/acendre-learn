﻿function StringHelper()
{
	//nothing to do
}

/**
 * Replaces < and > with &lt; and &gt;.
 * If str is null, makes the string "[null]".
 */
StringHelper.escapeTags = function(str)
{
	if (str == null)
		return "[null]";
	
	if (str == undefined)
		return "[undefined]";
		
	return str.replace(/</g,'&lt;').replace(/>/g,'&gt;');
};

/**
 * Selects all text in the specified object.
 * @param obj - a reference to the object whose text should be selected.
 */
StringHelper.selectTextInObject = function(obj)
{
	StringHelper.deselectText();
	
	if (document.selection)
	{
		var range = document.body.createTextRange();
		range.moveToElementText(obj);
		range.select();
	}
	else if (window.getSelection)
	{
		var range = document.createRange();
		range.selectNode(obj);
		window.getSelection().addRange(range);
	}
}

/**
 * Deselects the selected text on the page.
 */
StringHelper.deselectText = function()
{
	if (document.selection)
		document.selection.empty();
	
	else if (window.getSelection)
		window.getSelection().removeAllRanges();
}
