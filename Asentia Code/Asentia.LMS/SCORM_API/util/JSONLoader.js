﻿/**
 * JSONLoader
 */
 
//uses EventDispatcher
//uses Function
//uses StringHelper

JSONLoader.MAX_AUTO_RETRIES_ON_HTTP_ERROR = 3;
	//maximum number of times sendXMLRequest is automatically retried if an HTTP error ocurrs.
	//if this limit is reached, the user is prompted to continue
	
JSONLoader.DELAY_BETWEEN_RETRIES = 1000;
	//milliseconds between retries
	
JSONLoader.SERVER_RESPONSE_TIME_LIMIT = 600000;
//JSONLoader.SERVER_RESPONSE_TIME_LIMIT = 45000;
	//maximum number of milliseconds to wait until receiving respone from server
	//if this time limit is reached, alert that the server is unresponsive 
	//and ask user if he/she wants to try again 
	
JSONLoader.MESSAGE_HTTP_ERROR_RETRY = "An error ocurred while sending data. Click OK to retry or click Cancel.";

JSONLoader.MESSAGE_SERVER_UNRESPONSIVE_RETRY = "Server response time limit reached. Click OK to retry or click Cancel.";

/**
 * This class is a cross-browser wrapper for invoking XML-HTTP Requests.
 * 
 * @param instanceName - the identifier used when dispatching trace calls.
 */
function JSONLoader(instanceName)
{
	//instanceName - used for traces to differentiate instances of this
	this.instanceName = instanceName;
	
	this.sendXmlRequestCallCount = 0;
		//number of times the current sendXMLRequestAux has been attempted
		//this is reset to 0 when a new sendXMLRequest is invoked
	
	this.alertAndConfirmWindow = window; //override this by calling setAlertConfirmOwner(someOtherWindow)
	
	//(leave the custom error messages blank until they are overridden)
	this.customHttpErrorRetryMessage = "";
	this.customServerUnresponsiveRetryMessage = "";
}

JSONLoader.inherits(EventDispatcher);

//PUBLIC

/**
 * Call this to have alerts and confirms open from another window besides this one.
 *
 * NOTE - THIS CAUSES PROBLEMS IF THE SCO ALSO OPENS ALERTS, so for now, keep alerts/confirms 
 * coming from THIS window.
 */
JSONLoader.method('setAlertConfirmOwner', function(windowHandle)
{
	return;
});

/**
 * Call this to override the default message defined in: JSONLoader.MESSAGE_HTTP_ERROR_RETRY
 */
JSONLoader.method('setHttpErrorRetryMessage', function(msg)
{
	this.customHttpErrorRetryMessage = msg;
});

/**
 * Call this to override the default message defined in: JSONLoader.MESSAGE_SERVER_UNRESPONSIVE_RETRY
 */
JSONLoader.method('setServerUnresponsiveRetryMessage', function(msg)
{
	this.customServerUnresponsiveRetryMessage = msg;
});

/**
 * Called to send and receive XML. Retains a copy of URL and the XML to Post, and
 * calls sendAndLoadPreviousRequest() (after a delay - to workaround a Firefox bug).
 *
 * @param url - the url of the xml service to use
 * @param xmlToPost - a string of XML posted as a form variable called "xmlRequest"
 */

JSONLoader.method('sendAndLoad', function (url, data, contentType) {

    
    var thisInstance = this;

    //reset call count
    this.sendXmlRequestCallCount = 0;

    //queue the URL and xmlToPost in case the request needs to be tried multiple times
    this.url = url;

    if (!data)
        data = "";

    this.data = data;

    if (!contentType)
        contentType = "";

    this.contentType = contentType;

    //send the request after a delay
    this.sendAndLoadPreviousRequestAfterDelay();
});

/**
 * Aborts the pending XML Request, if any.
 */
JSONLoader.method('abort', function()
{
	try
	{
		this.isAborted = true;
		
		this.xhr.abort();
	}
	catch (e) {}
});



//PRIVATE

JSONLoader.method('dispatchTrace', function(msg)
{
	this.dispatchEvent(new Event(Event.TRACE, "JSONLoader[" + this.instanceName + "]", msg));
});

JSONLoader.method('dispatchError', function(msg)
{
	this.dispatchEvent(new Event(Event.ERROR, "JSONLoader[" + this.instanceName + "]", msg));
});

JSONLoader.method('dispatchFatalError', function(msg)
{
	this.dispatchEvent(new Event(Event.FATAL_ERROR, "JSONLoader[" + this.instanceName + "]", msg));
});

/**
 * Called by sendAndLoadPreviousRequest this to reset the XmlHttpRequest (and aborts any in progress).
 */
JSONLoader.method('initXHR', function () {
    
    if (this.isAborted)
        return;

    //cancel the pending request, if any (don't abort this JSONLoader instance though, that will prevent it from working in the future.)
    try {
        this.xhr.abort();
    }
    catch (e) { }

    //create a new XHR object
    if (window.XMLHttpRequest) {
        this.xhr = new XMLHttpRequest();

        //if (this.xhr.overrideMimeType)
        //     this.xhr.overrideMimeType('application/json');
    }
    else if (window.ActiveXObject) // IE
    {
        try {
            this.xhr = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                this.xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) { }
        }
    }

    if (!this.xhr) {
        this.dispatchError("XHR failed to initialize.");
    }

    //clear the time limit clock
    clearTimeout(this.requestTimerId);
});

/**
 * Called to try the previous request after the configured delay.
 */
JSONLoader.method('sendAndLoadPreviousRequestAfterDelay', function()
{
	var thisInstance = this;
	
	window.setTimeout(
		function()
		{
			thisInstance.sendAndLoadPreviousRequest();
		},
		JSONLoader.DELAY_BETWEEN_RETRIES	
	);
});

/**
 * Called by sendAndLoadPreviousRequestAfterDelay to make the actual request.
 */
JSONLoader.method('sendAndLoadPreviousRequest', function () {

   // alert("sand and load function");
    
    if (this.isAborted)
        return;

    var thisInstance = this;
    //increment the request call count
    this.sendXmlRequestCallCount++;

    //intialize the XML HTTP Request
    this.initXHR();

    this.dispatchTrace("Opening request #" + this.sendXmlRequestCallCount + " to: <a href='" + this.url + "' target='_blank'>" + this.url + "</a>");

    // open the connection
    this.xhr.open("POST", this.url, true);

    // set the timeout to 10 minutes
    this.xhr.timeout = 600000;

    //specify content type
    if (this.contentType != "") {
        this.xhr.setRequestHeader("Content-Type", this.contentType);
    }
    else {
        this.xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    }

    //workaround for safari 2 bug - see http://lists.apple.com/archives/dashboard-dev/2005/Jun/msg00085.html
    this.xhr.setRequestHeader("If-Modified-Since", "Wed, 15 Nov 1995 00:00:00 GMT");

    //send it
    this.xhr.send(this.data);

    //listen for the response
    this.xhr.onreadystatechange = function () {
        thisInstance.onReadyStateChange();
    };

    //start a timer to detect if the server does not respond to this request
    clearTimeout(this.requestTimerId);
    this.requestTimerId = setTimeout(
		function () {
		    thisInstance.alertRequestTimeout();
		},
		JSONLoader.SERVER_RESPONSE_TIME_LIMIT
	);
});

/**
 * Called by the XHR when its ready-state changes.
 */
JSONLoader.method('onReadyStateChange', function () {
    //;
    //alert("on ready state change");
    
    if (this.isAborted)
        return;

    var thisInstance = this;

    var blnIsBusy = false;

    try {
        var readyState = this.xhr.readyState;
        var httpStatus = this.xhr.status;
        var httpStatusText = this.xhr.statusText;

        this.dispatchTrace("XHR readyState: " + readyState + ", httpStatus: " + httpStatus + " (" + httpStatusText + ")");

        if (readyState == 4) {
            //clear the time limit clock since we received a response from the server
            clearTimeout(this.requestTimerId);

            if (httpStatus == 200) {
                this.dispatchEvent(new Event(Event.LOADED, this.xhr.responseText));
            }
            else if (String(httpStatus) == "undefined") {
                //this is a Safari bug - sometimes it returns an xmlRequest.status of undefined, so just trace it
                this.dispatchTrace("XHR httpStatus is undefined.");
            }
            else {
                this.dispatchTrace("Request failed. httpStatus: " + httpStatus + "; " + StringHelper.escapeTags(this.xhr.responseText));

                /*
                Handle the httpStatus code - and retry 3 times.
				
                If it still fails:
                300...505 blocks (see http://www.helpwithpcs.com/courses/html/html_http_status_codes.htm)
                12001 thru 12156 (see http://support.microsoft.com/kb/193625)
                */

                if (
					(httpStatus >= 300 && httpStatus <= 307)
					|| (httpStatus >= 400 && httpStatus <= 417)
					|| (httpStatus >= 500 && httpStatus <= 505)
					|| (httpStatus >= 12001 && httpStatus <= 12156)
					) {
                    if (httpStatus == 500) {
                        this.dispatchError("An ASP error occurred:\n\n" + this.xhr.responseText);
                    }

                    //If within the retry limit, retry the request.
                    if (this.sendXmlRequestCallCount <= JSONLoader.MAX_AUTO_RETRIES_ON_HTTP_ERROR) {
                        this.dispatchTrace("Auto-retry of sendAndLoad.");

                        this.sendAndLoadPreviousRequestAfterDelay();
                    }
                    //Otherwise, ask the user if they would like to keep retrying or not.
                    else {
                        //decide whether to use the default message or the custom message
                        var message = JSONLoader.MESSAGE_HTTP_ERROR_RETRY;

                        if (this.customHttpErrorRetryMessage.length > 0)
                            message = this.customHttpErrorRetryMessage;

                        message += "\n\nJSONLoader httpStatus: " + httpStatus;

                        if (httpStatus == 500) {
                            message += "\n\n" + this.xhr.responseText;
                        }

                        if (this.alertAndConfirmWindow.confirm(message)) {
                            this.dispatchTrace("User chose to retry sendAndLoad.");

                            this.sendAndLoadPreviousRequestAfterDelay();
                        }
                        else {
                            this.dispatchFatalError("User cancelled retry attempts.");
                        }
                    }
                }
                else if (httpStatus == 0) {
                    //this should only result from a call to xhr.abort()
                    this.dispatchTrace("Data request cancelled (http status 0)");
                }
                else {
                    this.dispatchTrace("Unexpected http status.");
                }
            }
        }
        else
            blnIsBusy = true;
    }
    catch (e) {
        blnIsBusy = true;
    }

    if (blnIsBusy) {
        this.dispatchTrace("Busy.");
    }
});

/**
 * Called by timer set in sendAndLoadPreviousRequest() when the server does not respond
 * to the request within the configured time limit.
 *
 * The pending request is cancelled, and the user is asked if they would like to retry the request.
 */
JSONLoader.method('alertRequestTimeout', function()
{
	if (this.isAborted)
		return;
	
	var thisInstance = this;
	
	this.dispatchTrace("Server has not responded to request within configured time limit.");
	
	//cancel the pending request since it has taken too long
	try
	{
		this.xhr.abort();
	}
	catch (e) {}
	
	//get the message to alert
	var message = JSONLoader.MESSAGE_SERVER_UNRESPONSIVE_RETRY;
	if (this.customServerUnresponsiveRetryMessage.length > 0)
		message = this.customServerUnresponsiveRetryMessage;
	
	message += "\n\nJSONLoader:alertRequestTimeout";
	
	//let the user decide whether to try the request again or not
	if (this.alertAndConfirmWindow.confirm(message))
	{
		this.dispatchTrace("User chose to retry timed-out request.");
		
		this.sendAndLoadPreviousRequestAfterDelay();
	}
	else
	{
		this.dispatchTrace("User chose to not retry timed-out request. Dispatching fatal error.");
		
		this.dispatchFatalError("Server has not responded to request within configured time limit.");
	}
});
