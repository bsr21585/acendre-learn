﻿function UserRatingControl(i, o, u, ru, r, useBigImages) {
	this.htmlIdentifier = i;
	this.objectID = o;
	this.rateUrl = u;
	this.resetUrl = ru;
	this.yourRating = r;
	this.bigImageClass = "";

	if (useBigImages) {
	    this.bigImageClass = "Big";
	}
}

UserRatingControl.prototype.setYourRating = function() {
	// posted back from ajax. updates the display
    $("#" + this.htmlIdentifier).find(".YourRatingStars" + this.bigImageClass).removeClass("Rating" + this.bigImageClass + "0");
    $("#" + this.htmlIdentifier).find(".YourRatingStars" + this.bigImageClass).removeClass("Rating" + this.bigImageClass + "1");
    $("#" + this.htmlIdentifier).find(".YourRatingStars" + this.bigImageClass).removeClass("Rating" + this.bigImageClass + "2");
    $("#" + this.htmlIdentifier).find(".YourRatingStars" + this.bigImageClass).removeClass("Rating" + this.bigImageClass + "3");
    $("#" + this.htmlIdentifier).find(".YourRatingStars" + this.bigImageClass).removeClass("Rating" + this.bigImageClass + "4");
    $("#" + this.htmlIdentifier).find(".YourRatingStars" + this.bigImageClass).removeClass("Rating" + this.bigImageClass + "5");
	
    $("#" + this.htmlIdentifier).find(".YourRatingStars" + this.bigImageClass).addClass("Rating" + this.bigImageClass + this.yourRating);
	
	var d = $("#" + this.htmlIdentifier + "_YourRatingValue").get(0);
	d.removeChild(d.childNodes[0]);
	d.appendChild(document.createTextNode(XStars.replace("{0}", this.yourRating)));
}

UserRatingControl.prototype.postYourRating = function(n) {
    if (0 <= n <= 5) {
        this.yourRating = n;
    }
    else {
        return false;
    }

    var jsonString;

    // begin JSON string
    jsonString = "{";

    // "idObject" variable
    jsonString += "\"idObject\":" + this.objectID + ",";

    // "rating" variable
    jsonString += "\"rating\":" + this.yourRating;

    // end JSON string
    jsonString += "}";

    $.ajax({
        type: "POST",
        url: this.rateUrl,
        data: jsonString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: this.setYourRating(),
        error: function (response) {
            // do nothing
        }

    });
}

UserRatingControl.prototype.postReset = function(n) {
    var jsonString;

    // begin JSON string
    jsonString = "{";

    // "idObject" variable
    jsonString += "\"idObject\":" + this.objectID;

    // end JSON string
    jsonString += "}";

    $.ajax({
        type: "POST",
        url: this.resetUrl,
        data: jsonString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: this.resetCallback(),
        error: function (response) {
            // do nothing
        }

    });
}

UserRatingControl.prototype.resetCallback = function(){
	// posted back from ajax. updates the display
	$("#" + this.htmlIdentifier).find(".ResetRatingCaption").get(0).style.display = "none";
	$("#" + this.htmlIdentifier).find(".AvgRatingStars" + this.bigImageClass).get(0).style.width = "0px";
	
	var d = $("#" + this.htmlIdentifier + "_AvgRatingValue").get(0);
	d.removeChild(d.childNodes[0]);
	d.appendChild(document.createTextNode(NotYetRated));
	
	var d = $("#" + this.htmlIdentifier + "_AvgRatingCaption").get(0);
	d.removeChild(d.childNodes[0]);
	d.appendChild(document.createTextNode(AverageOfRatings.replace("{0}", "0") + ": "));
}

UserRatingControl.prototype.rate = function() {
    $("#" + this.htmlIdentifier).find(".SetRatingStars" + this.bigImageClass).get(0).style.display = "block";
    $("#" + this.htmlIdentifier).find(".AvgRatingStars" + this.bigImageClass).get(0).style.display = "none";
    $("#" + this.htmlIdentifier).find(".YourRatingStars" + this.bigImageClass).get(0).style.display = "none";
}

UserRatingControl.prototype.ret = function() {
	if (this.yourRating > 0) {
	    $("#" + this.htmlIdentifier).find(".SetRatingStars" + this.bigImageClass).get(0).style.display = "none";
	    $("#" + this.htmlIdentifier).find(".YourRatingStars" + this.bigImageClass).get(0).style.display = "block";
	}
	else {
	    $("#" + this.htmlIdentifier).find(".SetRatingStars" + this.bigImageClass).get(0).style.display = "none";
	    $("#" + this.htmlIdentifier).find(".AvgRatingStars" + this.bigImageClass).get(0).style.display = "block";
	}
}