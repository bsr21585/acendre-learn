﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Controls
{
    class EventEmailNotificationForm : WebControl
    {
        #region Constructors
        /// <summary>
        /// Constructor for building a event email notification form pre-populated with email notification data.
        /// </summary>
        /// <param name="ReportObject">email notification object</param>
        public EventEmailNotificationForm(string id, string languageString, DataTable eventTypesDataTable, EventEmailNotification eventemailnotificationObject, Page pageInstance = null)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.LanguageString = languageString;
            this.PageInstance = pageInstance;
            
            this.EventEmailNotificationObject = eventemailnotificationObject;

            if (this.EventEmailNotificationObject != null)
            {
                this.IdEventEmailNotification = EventEmailNotificationObject.IdEventEmailNotification;
                this.IdSite = EventEmailNotificationObject.IdSite;
            }

            // build the form
            this._BuildTabsList();

            this.TabPanelsContainer = new Panel();
            this.TabPanelsContainer.ID = this.ID + "_TabPanelsContainer";
            this.TabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.Controls.Add(this.TabPanelsContainer);

            this._BuildEventEmailNotificationForm(eventTypesDataTable);

            // build javascript
            this._InitializeJavascript();
        }
        #endregion
        
        #region Enums
        //Event Type of email notification
        public enum NotificationEventType
        {
            UserCreated = 101,
            UserDeleted = 102,
            UserExpires = 103,
            UserRegistrationApproved = 104,
            UserRegistrationRejected = 105,
            UserMessageReceived = 106,
            UserRegistrationRequestSubmitted = 107,
            CourseEnrolled = 201,
            CourseExpires = 202,
            CourseRevoked = 203,
            CourseDue = 204,
            CourseCompleted = 205,
            CourseStart = 206,
            CourseEnrollmentRequestApproved = 207,
            CourseEnrollmentRequestRejected = 208,
            CourseEnrollmentRequestSubmitted = 209,
            ModulePassed = 301,
            ModuleFailed = 302,
            ModuleCompleted = 303,
            OJTRequestSubmitted = 304,
            TaskSubmitted = 305,
            SessionMeets = 401,
            SessionInstructorAssignedChanged = 402,
            SessionInstructorRemoved = 403,
            SessionLearnerJoined = 404,
            SessionLearnerRemoved = 405,
            SessionTimeLocationChanged = 406,
            SessionLearnerJoinedWaitlist = 407,
            SessionLearnerRemovedWaitlist = 408,
            SessionLearnerPromoted = 409,
            SessionLearnerDemoted = 410,
            LearningPathEnrolled = 501,
            LearningPathExpires = 502,
            LearningPathRevoked = 503,
            LearningPathDue = 504,
            LearningPathCompleted = 505,
            LearningPathStart = 506,
            CertificationEnrolled = 601,
            CertificationExpires = 602,
            CertificationRevoked = 603,
            CertificationAwarded = 604,
            CertificationRenewed = 605,
            CertificationTaskSubmitted = 606,
            CertificateEarned = 701,
            CertificateAwarded = 702,
            CertificateRevoked = 703,
            CertificateExpires = 704,
            DiscussionCourseModeratedMessage = 901,
            DiscussionGroupModeratedMessage = 902
        }
        // The recipient
        public enum EventTypeRecipient
        {
            SystemAdministrator = 1,
            Learner = 2,
            User = 3,
            Supervisor = 4,
            Instructor = 5,
            Learners = 6,
            Users = 7,
            Supervisors = 8,
            Proctors = 9,
            Moderator_s = 10,
            Approver_s = 11,
            SpecificEmailAddress = 12
        }

        // Attachment type
        public enum EmailNotificationAttachmentType
        {
            CalendarFiles = 0
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(EventEmailNotificationForm), "Asentia.LMS.Controls.EventEmailNotificationForm.js");

            // build start up call for CKEditor and add to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));
            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine("});");
            multipleStartUpCallsScript.AppendLine("var certificationsEnabled = " + AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE).ToString().ToLower() + "; ");
            multipleStartUpCallsScript.AppendLine("var taskModulesEnabled = " + AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.TASKMODULES_ENABLE).ToString().ToLower() + "; ");
            multipleStartUpCallsScript.AppendLine("var ojtModulesEnabled = " + AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OJTMODULES_ENABLE).ToString().ToLower() + "; ");
            multipleStartUpCallsScript.AppendLine("var learningPathsEnabled = " + AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE).ToString().ToLower() + "; ");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);

            base.OnPreRender(e);
        }
        #endregion

        #region Properties
        public Panel TabPanelsContainer;

        /// <summary>
        /// EventEmailNotification object.
        /// </summary>
        public EventEmailNotification EventEmailNotificationObject;

        /// <summary>
        /// The instance of the calling page.
        /// </summary>
        public Page PageInstance = null;

        /// <summary>
        /// The language of this form.
        /// </summary>
        public string LanguageString;

        /// <summary>
        /// Site Id.
        /// </summary>
        public int IdSite { get; set; }

        /// <summary>
        /// EventEmailNotification Id.
        /// </summary>
        public int IdEventEmailNotification { get; set; }

        /// <summary>
        /// IsActive Value
        /// </summary>
        public bool? IsActive { get { return Convert.ToBoolean(_DropDownListIsActive.SelectedValue); } }

        /// <summary>
        /// EventEmailNotification Title.
        /// </summary>
        public string Title { get { return _TitleTextBox.Text; } }

        /// <summary>
        /// EventEmailNotification TitleTextBoxID.
        /// </summary>
        public string TitleID { get { return _TitleTextBox.ID; } }

        /// <summary>
        /// EventEmailNotification EventType.
        /// </summary>
        public string EventType { get { return _DropDownListEvent.SelectedValue; } }

        /// <summary>
        /// EventEmailNotification Recipient.
        /// </summary>
        public string Recipient { get { return _DropDownRecipient.SelectedValue; } }

        /// <summary>
        /// EventEmailNotification TimeFrame.
        /// </summary>
        public string TimeFrame { get { return _DropDownListTimeFrame.SelectedValue; } }

        /// <summary>
        /// EventEmailNotification Before or After Event Happened.
        /// </summary>
        public string BeforeAfter { get { return _DropDownListBeforeAfter.SelectedValue; } }

        /// <summary>
        /// EventEmailNotification Before or After Event Happened.
        /// </summary>
        public string Priority { get { return _DropDownListPriority.SelectedValue; } }

        /// <summary>
        /// IsHTMLBased Value
        /// </summary>
        public bool? IsHTMLBased { get { return Convert.ToBoolean(_Type.SelectedValue); } }

        /// <summary>
        /// EventEmailNotification From Person Name
        /// </summary>
        public string From { get { return _FromTextBox.Text; } }

        /// <summary>
        /// EventEmailNotification Copy To Emails
        /// </summary>
        public string CopyTo { get { return _CopyToTextBox.Text; } }

        /// <summary>
        /// EventEmailNotification Specific Email Address
        /// </summary>
        public string SpecificEmailAddress { get { return _SpecificEmailTextBox.Text; } }

        /// <summary>
        /// EventEmailNotification From Person Name
        /// </summary>
        public string Subject { get { return _SubjectTextBox.Text; } }

        /// <summary>
        /// EventEmailNotification Form Subject ID
        /// </summary>
        public string SubjectID { get { return _SubjectTextBox.ID; } }

        /// <summary>
        /// EventEmailNotification Body
        /// </summary>
        public string Body { get { return _BodyTextBox.Text; } }

        /// <summary>
        /// EventEmailNotification Body ID
        /// </summary>
        public string BodyID { get { return _BodyTextBox.ID; } }

        /// <summary>
        /// EventEmailNotification Body
        /// </summary>
        public string BodyHTML { get { return _BodyTextHTMLBox.Text; } }

        /// <summary>
        /// EventEmailNotification Body ID
        /// </summary>
        public string BodyHTMLID { get { return _BodyTextHTMLBox.ID; } }

        /// <summary>
        /// EventEmailNotification CurrentTime
        /// </summary>
        public bool CurrentTime { get { return _CurrentTimeCheckBox.Checked; } }

        /// <summary>
        /// EventEmailNotification CurrentTime
        /// </summary>
        public string Interval { get { return _IntervalTextBox.Text; } }

        /// <summary>
        /// EventEmailNotification AttachmentType
        /// </summary>
        public int? AttachmentType { 
            get {
                if (_CalendarAttachmentCheckBox.Checked)
                    return (int)EmailNotificationAttachmentType.CalendarFiles;
                else
                    return null;
            }
        }

        #endregion

        #region Private Properties

        private DropDownList _DropDownListIsActive = new DropDownList();
        private DropDownList _DropDownListEvent = new DropDownList();
        private DropDownList _DropDownRecipient = new DropDownList();
        private DropDownList _DropDownListTimeFrame = new DropDownList();
        private DropDownList _DropDownListBeforeAfter = new DropDownList();
        private DropDownList _DropDownListPriority = new DropDownList();
        private TextBox _TitleTextBox = new TextBox();
        private TextBox _SpecificEmailTextBox = new TextBox();
        private TextBox _FromTextBox = new TextBox();
        private TextBox _CopyToTextBox = new TextBox();
        private TextBox _SubjectTextBox = new TextBox();
        private TextBox _BodyTextBox = new TextBox();
        private TextBox _BodyTextHTMLBox = new TextBox();
        private CheckBox _CurrentTimeCheckBox = new CheckBox();
        private TextBox _IntervalTextBox = new TextBox();
        private RadioButtonList _Type = new RadioButtonList();
        private CheckBox _CalendarAttachmentCheckBox = new CheckBox();
        private LinkButton _PlaceHolderButton = new LinkButton();
        private ModalPopup _PlaceHolderModal;
        private Button _HiddenPlaceHolderModalLaunchButton;
        private Panel _TabOptionsContentPanel, _TabMessageContentPanel;
        #endregion

        #region Methods
        #region GetEventTypeText
        /// <summary>
        /// Return Event Type Text by an Event Type ID
        /// </summary>
        public static string GetEventTypeText(int typeID)
        {
            NotificationEventType eventType = (NotificationEventType)typeID;
            return _GlobalResources.ResourceManager.GetString(eventType.ToString(), new CultureInfo(AsentiaSessionState.UserCulture));
        }
        #endregion        
        #endregion

        #region Private Methods
        #region _InitializeJavascript
        /// <summary>
        /// Builds javascript initial objects
        /// </summary>
        private void _InitializeJavascript()
        {
            /// Create a multiple dimentional array in global js object to carry the Recipient List ddl information 
            Literal javascriptCode = new Literal();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type=\"text/javascript\">");
            sb.AppendLine(" var recipientObject = new Array(" + Enum.GetNames(typeof(EventTypeRecipient)).Length + ");");
            sb.AppendLine(" var beforeText = '" + _GlobalResources.Before + "';");
            sb.AppendLine(" var afterText = '" + _GlobalResources.After + "';");
            int i = 0;

            foreach (EventTypeRecipient enumValue in Enum.GetValues(typeof(EventTypeRecipient)))
            {
                EventTypeRecipient enumName = ((EventTypeRecipient)Convert.ToInt32(enumValue));
                sb.AppendLine("     recipientObject[" + i + "] = new Array(3); ");
                sb.AppendLine(" 	recipientObject[" + i + "]['optionText'] = '" + _GlobalResources.ResourceManager.GetString(enumName.ToString())+ "';");
                sb.AppendLine(" 	recipientObject[" + i + "]['optionValue'] = '" + Convert.ToInt32(enumValue).ToString()+ "';");
                if (this.EventEmailNotificationObject != null && Convert.ToInt32(enumValue).ToString() == this.EventEmailNotificationObject.IdEventTypeRecipient.ToString())
                {
                    sb.AppendLine(" 	recipientObject[" + i + "]['optionSelected'] = true;");
                }
                else
                {
                    sb.AppendLine(" 	recipientObject[" + i + "]['optionSelected'] = false;");
                }
                i++;
            }
            sb.AppendLine("</script>");
            javascriptCode.Text = sb.ToString();

            // attach controls
            this.Controls.Add(javascriptCode);
        }
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for the form.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Options", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("Message", _GlobalResources.Message));

            // build and attach the tabs
            this.Controls.Add(AsentiaPage.BuildTabListPanel(this.ID, tabs, null, this.PageInstance));
        }
        #endregion

        #region _BuildEventEmailNotificationForm
        /// <summary>
        /// Builds the tab content container for specific purpose of each tab
        /// </summary>
        private void _BuildEventEmailNotificationForm(DataTable eventTypesDataTable)
        {
            _TabOptionsContentPanel = new Panel();
            _TabOptionsContentPanel.ID = this.ID + "_Options_TabPanel";
            this._BuildOptionsTabContent(this._TabOptionsContentPanel, eventTypesDataTable);
            this.TabPanelsContainer.Controls.Add(_TabOptionsContentPanel);

            _TabMessageContentPanel = new Panel();
            _TabMessageContentPanel.ID = this.ID + "_Message_TabPanel";
            _TabMessageContentPanel.Style.Add("display", "none");
            this._BuildMessageTabContent(this._TabMessageContentPanel);
            this._BuildPlaceHolderModal();
            this.TabPanelsContainer.Controls.Add(_TabMessageContentPanel);

        }
        #endregion

        #region _BuildOptionsTabContent
        /// <summary>
        /// Builds the tab content container for Upload tab
        /// </summary>
        private void _BuildOptionsTabContent(Control optionsControls, DataTable eventTypesDataTable)
        {
            Panel tabOptionsPanel = new Panel();
            tabOptionsPanel.ID = "EmailNotification_Options_Panel";

            #region Name_Field
            //Name_Field
            this._TitleTextBox.ID = "TitleText_Field";
            this._TitleTextBox.CssClass = "InputXLong";
            this._TitleTextBox.Columns = 50;
            this._TitleTextBox.Text = (this.EventEmailNotificationObject != null) ? this.EventEmailNotificationObject.Name : String.Empty;

            tabOptionsPanel.Controls.Add(AsentiaPage.BuildFormField("TitleText",
                                                                    _GlobalResources.Name,
                                                                    this._TitleTextBox.ID,
                                                                    this._TitleTextBox,
                                                                    true,
                                                                    true,
                                                                    true));

            #endregion

            #region Active_Field
            //Active_Field
            _DropDownListIsActive.ID = this.IdEventEmailNotification + "_IsActive_Field";
            _DropDownListIsActive.Items.Add(new ListItem(_GlobalResources.Active, "true"));
            _DropDownListIsActive.Items.Add(new ListItem(_GlobalResources.Inactive, "false"));

            tabOptionsPanel.Controls.Add(AsentiaPage.BuildFormField(this.IdEventEmailNotification + "_IsActive",
                                                        _GlobalResources.Status,
                                                        this._DropDownListIsActive.ID,
                                                        this._DropDownListIsActive,
                                                        true,
                                                        true,
                                                        false));

            // set the value if there is an email notification objectan email notification object
            if (this.EventEmailNotificationObject != null)
            { _DropDownListIsActive.SelectedValue = (bool)this.EventEmailNotificationObject.IsActive ? "true" : "false"; }
            #endregion

            #region _Field_Recipient

            List<Control> eventRecipientInputControls = new List<Control>();

            //Event option dropdown control
            this._DropDownListEvent.ID = "DropDownList_EventType";
            this._DropDownListEvent.Attributes.Add("onChange", "EventTypeOnChange(false);");

            Regex rgx = new Regex(@"[\s\/-]");

            foreach (DataRow eventTypeRow in eventTypesDataTable.Rows)
            {
                string eventTypeName = _GlobalResources.ResourceManager.GetString(rgx.Replace(eventTypeRow["name"].ToString(), ""), new CultureInfo(this.LanguageString));
                ListItem eventTypeItem = new ListItem(GetEventTypeText(Convert.ToInt32(eventTypeRow["idEventType"])), eventTypeRow["idEventType"].ToString());
                eventTypeItem.Attributes.Add("allowPriorSend", eventTypeRow["allowPriorSend"].ToString());
                eventTypeItem.Attributes.Add("allowPostSend", eventTypeRow["allowPostSend"].ToString());
                eventTypeItem.Attributes.Add("idEventTypeRecipients", eventTypeRow["idEventTypeRecipients"].ToString());
                this._DropDownListEvent.Items.Add(eventTypeItem);
            }

            // set the value if there is an email notification object
            if (this.EventEmailNotificationObject != null)
            { _DropDownListEvent.SelectedValue = this.EventEmailNotificationObject.IdEventType.ToString(); }

            Literal eventFieldLabel = new Literal();
            eventFieldLabel.Text = _GlobalResources.SendANotificationInRelationToWhen + " ";

            //Add label and dropdown list to controls list
            eventRecipientInputControls.Add(eventFieldLabel);
            eventRecipientInputControls.Add(this._DropDownListEvent);

            Label toFieldLabel = new Label();
            toFieldLabel.Text = " " + _GlobalResources.to_lower + " ";

            this._DropDownRecipient.ID = "RecipientList";
            this._DropDownRecipient.Attributes.Add("onChange", "RecipientOnChange();");

            foreach (EventTypeRecipient enumValue in Enum.GetValues(typeof(EventTypeRecipient)))
            {
                EventTypeRecipient enumName = ((EventTypeRecipient)Convert.ToInt32(enumValue));
                this._DropDownRecipient.Items.Add(new ListItem(_GlobalResources.ResourceManager.GetString(enumName.ToString()), Convert.ToInt32(enumValue).ToString()));
            }

            // set the value if there is an email notification object
            if (this.EventEmailNotificationObject != null)
            { _DropDownRecipient.SelectedValue = this.EventEmailNotificationObject.IdEventTypeRecipient.ToString(); }

            ///text box area for specific email address(es)
            this._SpecificEmailTextBox.ID = "SpecificEmailTextBox";
            this._SpecificEmailTextBox.CssClass = "InputMedium";
            this._SpecificEmailTextBox.Columns = 50;
            this._SpecificEmailTextBox.Text = (this.EventEmailNotificationObject != null) ? this.EventEmailNotificationObject.SpecificEmailAddress : String.Empty;

            //Add label and dropdown list to controls list
            eventRecipientInputControls.Add(toFieldLabel);
            eventRecipientInputControls.Add(this._DropDownRecipient);
            eventRecipientInputControls.Add(this._SpecificEmailTextBox);

            tabOptionsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Recipient",
                                                                           _GlobalResources.EventRecipient,
                                                                           eventRecipientInputControls,
                                                                           true,
                                                                           true));
            #endregion

            #region From_Field
            ///text box area for email notification From\
            this._FromTextBox.ID = "FromText";
            this._FromTextBox.CssClass = "InputMedium";
            this._FromTextBox.Columns = 50;
            this._FromTextBox.Text = (this.EventEmailNotificationObject != null) ? this.EventEmailNotificationObject.From : String.Empty;

            tabOptionsPanel.Controls.Add(AsentiaPage.BuildFormField("FromText",
                                                                    _GlobalResources.FromReplyTo,
                                                                    this._FromTextBox.ID,
                                                                    this._FromTextBox,
                                                                    false,
                                                                    true,
                                                                    false));
            #endregion

            #region CopyTo_Field
            ///text box area for email notification Copy To\
            this._CopyToTextBox.ID = "CopyToText";
            this._CopyToTextBox.CssClass = "InputMedium";
            this._CopyToTextBox.Columns = 50;
            this._CopyToTextBox.Text = (this.EventEmailNotificationObject != null) ? this.EventEmailNotificationObject.CopyTo : String.Empty;

            tabOptionsPanel.Controls.Add(AsentiaPage.BuildFormField("CopyToText",
                                                                    _GlobalResources.CopyTo,
                                                                    this._CopyToTextBox.ID,
                                                                    this._CopyToTextBox,
                                                                    false,
                                                                    true,
                                                                    false));
            #endregion

            #region _Field_Send
            List<Control> sendFieldInputControls = new List<Control>();

            Panel sendFieldContainer = new Panel();
            sendFieldContainer.ID = "Send_Container";
            sendFieldContainer.CssClass = "FormFieldContainer";

            this._CurrentTimeCheckBox.ID = "CurrentTimeCheckBox";            
            this._CurrentTimeCheckBox.Text = _GlobalResources.WhenTheSelectedEventOccurs;
            this._CurrentTimeCheckBox.Attributes.Add("onClick", "CurrentTimeCheckBoxOnClick();");

            bool currentTimeChecked = true;

            if (this.EventEmailNotificationObject != null)
            {
                currentTimeChecked = (String.IsNullOrWhiteSpace(this.EventEmailNotificationObject.TimeFrame)) || (this.EventEmailNotificationObject.Interval == null);
                if (currentTimeChecked)
                {
                    this._CurrentTimeCheckBox.Checked = true;
                }
                else
                {
                    this._CurrentTimeCheckBox.Checked = false;
                }
            }

            //Add checkbox control to list
            sendFieldInputControls.Add(this._CurrentTimeCheckBox);

            // Panel send at different time for event
            Panel sendDifferentInputPanel = new Panel();
            sendDifferentInputPanel.ID = "Send_DifferentTime_Panel";

            Label sendDifferentLabel = new Label();
            sendDifferentLabel.Text = _GlobalResources.OR_UPPER;
            sendDifferentLabel.AssociatedControlID = this._IntervalTextBox.ID; ;
            sendDifferentInputPanel.Controls.Add(sendDifferentLabel);

            Panel timeFrameInputPanel = new Panel();
            timeFrameInputPanel.ID = "TimeFrame_Input_Panel";

            ///text box area for interval
            this._IntervalTextBox.ID = "IntervalNumberTextBox";
            this._IntervalTextBox.Columns = 5;

            if (this.EventEmailNotificationObject != null)
            {
                this._IntervalTextBox.Text = (!currentTimeChecked) ? Math.Abs((int)this.EventEmailNotificationObject.Interval).ToString() : String.Empty;
            }

            timeFrameInputPanel.Controls.Add(_IntervalTextBox);

            // Time Frame Drop Down List
            this._DropDownListTimeFrame.ID = "TimeFrame_Drop_Down_List";
            this._DropDownListTimeFrame.Items.Add(new ListItem(Asentia.Common._GlobalResources.Month_s, "m"));
            this._DropDownListTimeFrame.Items.Add(new ListItem(Asentia.Common._GlobalResources.Week_s, "ww"));
            this._DropDownListTimeFrame.Items.Add(new ListItem(Asentia.Common._GlobalResources.Day_s, "d"));

            //set the value if there is an email notification object
            if (this.EventEmailNotificationObject != null)
            {
                if (!String.IsNullOrWhiteSpace(this.EventEmailNotificationObject.TimeFrame))
                { this._DropDownListTimeFrame.SelectedValue = this.EventEmailNotificationObject.TimeFrame.ToString(); }
            }
            timeFrameInputPanel.Controls.Add(this._DropDownListTimeFrame);

            // Before or After Event Drop Down List
            this._DropDownListBeforeAfter.ID = "Drop_Down_List_BeforeAfter";
            this._DropDownListBeforeAfter.Items.Add(new ListItem(_GlobalResources.Before, "before"));
            this._DropDownListBeforeAfter.Items.Add(new ListItem(_GlobalResources.After, "after"));

            //set the value if there is an email notification object
            if (this.EventEmailNotificationObject != null && this.EventEmailNotificationObject.Interval != null)
            { this._DropDownListBeforeAfter.SelectedValue = ((int)this.EventEmailNotificationObject.Interval > 0) ? "after" : "before"; }

            timeFrameInputPanel.Controls.Add(this._DropDownListBeforeAfter);

            Label eventOccursLabel = new Label();
            eventOccursLabel.Text = " " + _GlobalResources.EventOccurs;

            timeFrameInputPanel.Controls.Add(eventOccursLabel);

            sendDifferentInputPanel.Controls.Add(timeFrameInputPanel);

            //Add panel to list
            sendFieldInputControls.Add(sendDifferentInputPanel);            

            tabOptionsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Send",
                                                                                        _GlobalResources.Send,
                                                                                        sendFieldInputControls,
                                                                                        true,
                                                                                        true));

            #endregion

            optionsControls.Controls.Add(tabOptionsPanel);
        }
        #endregion

        #region _BuildMessageTabContent
        /// <summary>
        /// Builds the tab content container for Upload tab
        /// </summary>
        private void _BuildMessageTabContent(Control MessageControls)
        {
            Panel tabMessagePanel = new Panel();
            tabMessagePanel.ID = "EmailNotification_Message_Panel";

            #region _Field_Subject
            this._SubjectTextBox.ID = "SubjectText_Field";
            this._SubjectTextBox.CssClass = "InputXLong";
            this._SubjectTextBox.Columns = 50;

            tabMessagePanel.Controls.Add(AsentiaPage.BuildFormField("SubjectText",
                                                                    _GlobalResources.Subject,
                                                                    this._SubjectTextBox.ID,
                                                                    this._SubjectTextBox,
                                                                    true,
                                                                    true,
                                                                    true));
            #endregion

            #region _Field_Priority
            this._DropDownListPriority.ID = this.IdEventEmailNotification + "_Priority_Field";
            this._DropDownListPriority.Items.Add(new ListItem(_GlobalResources.Normal, "0"));
            this._DropDownListPriority.Items.Add(new ListItem(_GlobalResources.Low, "1"));
            this._DropDownListPriority.Items.Add(new ListItem(_GlobalResources.High, "2"));

            tabMessagePanel.Controls.Add(AsentiaPage.BuildFormField(this.IdEventEmailNotification + "_Priority",
                                                                    _GlobalResources.Priority,
                                                                    this._DropDownListPriority.ID,
                                                                    this._DropDownListPriority,
                                                                    true,
                                                                    true,
                                                                    false));

            // set the value if there is an email notification object
            if (this.EventEmailNotificationObject != null)
                this._DropDownListPriority.SelectedValue = this.EventEmailNotificationObject.Priority.ToString();
            #endregion

            #region _Field_Type
            this._Type.ID = "RadioButtonList_Type_Field";
            this._Type.Items.Add(new ListItem(_GlobalResources.PlainText, "False"));
            this._Type.Items.Add(new ListItem(_GlobalResources.HtmlBased, "True"));
            this._Type.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this._Type.SelectedIndex = 0;

            tabMessagePanel.Controls.Add(AsentiaPage.BuildFormField(this.IdEventEmailNotification + "_Type",
                                                           _GlobalResources.Type,
                                                           this._Type.ID,
                                                           this._Type,
                                                           true,
                                                           true,
                                                           false));

            // set the value if there is an email notification object
            if (this.EventEmailNotificationObject != null && this.EventEmailNotificationObject.IsHTMLBased == true)
            { this._Type.SelectedValue = "True"; }
            else
            { this._Type.SelectedValue = "False"; }

            // set the value if there is an email notification object
            this._CalendarAttachmentCheckBox.Checked  = (this.EventEmailNotificationObject != null && this.EventEmailNotificationObject.AttachmentType == 0);
            #endregion

            #region _Field_Attachment
            this._CalendarAttachmentCheckBox.ID = "CheckBox_Calendar_Attachment_Field";
            this._CalendarAttachmentCheckBox.Text = _GlobalResources.AttachCalendarFilesForSessionMeetingTime_s;

            tabMessagePanel.Controls.Add(AsentiaPage.BuildFormField("CalendarAttachment",
                                                           _GlobalResources.Attachment,
                                                           this._CalendarAttachmentCheckBox.ID,
                                                           this._CalendarAttachmentCheckBox,
                                                           false,
                                                           false,
                                                           false));
            #endregion

            #region Body_Field_Plain_Text
            this._BodyTextBox.ID = "BodyText_Field";
            this._BodyTextBox.Style.Add("width", "98%");
            this._BodyTextBox.TextMode = TextBoxMode.MultiLine;
            this._BodyTextBox.Rows = 13;

            tabMessagePanel.Controls.Add(AsentiaPage.BuildFormField("BodyText",
                                                        _GlobalResources.Body,
                                                        this._BodyTextBox.ID,
                                                        this._BodyTextBox,
                                                        true,
                                                        true,
                                                        true));
            #endregion 

            #region Body_Field_HTML_Based
            this._BodyTextHTMLBox.ID = "HTMLBodyText_Field";
            this._BodyTextBox.Style.Add("width", "98%");
            this._BodyTextHTMLBox.CssClass = "ckeditor";
            this._BodyTextHTMLBox.TextMode = TextBoxMode.MultiLine;
            this._BodyTextHTMLBox.Rows = 12;

            tabMessagePanel.Controls.Add(AsentiaPage.BuildFormField("HTMLBodyText",
                                                        _GlobalResources.Body,
                                                        this._BodyTextHTMLBox.ID,
                                                        this._BodyTextHTMLBox,
                                                        true,
                                                        true,
                                                        true));
            #endregion

            #region PlaceHolder Link
            this._PlaceHolderButton.ID = "PlaceHolderButton";
            this._PlaceHolderButton.CssClass = "GridDeleteButton";
            this._PlaceHolderButton.OnClientClick = "document.getElementById(\"HiddenPlaceHolderModalLaunchButton\").click(); return false;";

            // add button image
            Image addImage = new Image();
            addImage.ID = "AddButtonImage";
            addImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            addImage.CssClass = "SmallIcon";
            addImage.AlternateText = _GlobalResources.Add;
            this._PlaceHolderButton.Controls.Add(addImage);

            // add place holder button text
            Literal addText = new Literal();
            addText.Text = _GlobalResources.AddPlaceholder;
            this._PlaceHolderButton.Controls.Add(addText);

            tabMessagePanel.Controls.Add(AsentiaPage.BuildFormField("Placeholder",
                                                        null,
                                                        this._PlaceHolderButton.ID,
                                                        this._PlaceHolderButton,
                                                        false,
                                                        false,
                                                        false));
            #endregion

            //Place Holder Modal Launch Button
            this._HiddenPlaceHolderModalLaunchButton = new Button();
            this._HiddenPlaceHolderModalLaunchButton.ID = "HiddenPlaceHolderModalLaunchButton";
            this._HiddenPlaceHolderModalLaunchButton.Style.Add("display", "none");
            tabMessagePanel.Controls.Add(this._HiddenPlaceHolderModalLaunchButton);

            MessageControls.Controls.Add(tabMessagePanel);
        }
        #endregion

        #region _BuildPlaceHolderModal
        /// <summary>
        /// Builds the modal for place holder.
        /// </summary>
        private void _BuildPlaceHolderModal()
        {
            // set modal properties
            this._PlaceHolderModal = new ModalPopup("PlaceHolderModal");
            this._PlaceHolderModal.Type = ModalPopupType.Information;
            this._PlaceHolderModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION,
                                                                                 ImageFiles.EXT_PNG);
            this._PlaceHolderModal.HeaderIconAlt = _GlobalResources.Placeholders;
            this._PlaceHolderModal.HeaderText = _GlobalResources.Placeholders;
            this._PlaceHolderModal.TargetControlID = "HiddenPlaceHolderModalLaunchButton";

            // build the modal body
            Panel placeHolderModalBody = new Panel();
            this._CreatePlaceHolderForEmailBody(placeHolderModalBody);

            // add controls to body
            this._PlaceHolderModal.AddControlToBody(placeHolderModalBody);

            // add modal to container
            this.TabPanelsContainer.Controls.Add(this._PlaceHolderModal);
        }
        #endregion

        #region _CreatePlaceHolderForEmailBody
        /// <summary>
        /// Create a PlaceHolder for the Email Message quick entry
        /// </summary>
        private void _CreatePlaceHolderForEmailBody(Control parentPanel)
        {
            Panel panelPlaceHolder = new Panel();
            panelPlaceHolder.CssClass = "EmailBodyPlaceHolder";
            //panelPlaceHolder.Height = Unit.Pixel(250);

            // PLACE-HOLDERS
            Panel sectionTitleOne = new Panel();
            sectionTitleOne.CssClass = "EmailPlaceHolderTitle";
            Label placeHoldersLabel = new Label();
            placeHoldersLabel.Text = _GlobalResources.Placeholders;
            sectionTitleOne.Controls.Add(placeHoldersLabel);
            panelPlaceHolder.Controls.Add(sectionTitleOne);

            Panel sectionContentOne = new Panel();
            sectionContentOne.CssClass = "EmailPlaceHolderContent";
            Label placeHoldersContentOne = new Label();
            placeHoldersContentOne.Text = _GlobalResources.TheFollowingGeneralPlaceholdersMayBePut;
            sectionContentOne.Controls.Add(placeHoldersContentOne);
            panelPlaceHolder.Controls.Add(sectionContentOne);

            Panel sectionContentTwo = new Panel();
            sectionContentTwo.CssClass = "EmailPlaceHolderContent";
            Label placeHoldersContentTwo = new Label();
            placeHoldersContentTwo.Text = _GlobalResources.ClickAPlaceholderToInsertItAtTheCurrent;
            sectionContentTwo.Controls.Add(placeHoldersContentTwo);
            panelPlaceHolder.Controls.Add(sectionContentTwo);

            //EVENT/OBJECT
            Panel sectionTitleTwo = new Panel();
            sectionTitleTwo.CssClass = "EmailPlaceHolderTitle";
            Label eventObjectLabel = new Label();
            eventObjectLabel.Text = _GlobalResources.EventObject + ":";
            sectionTitleTwo.Controls.Add(eventObjectLabel);
            panelPlaceHolder.Controls.Add(sectionTitleTwo);

            Panel sectionContentEvent = new Panel();
            sectionContentEvent.CssClass = "EmailPlaceHolderContent";
            Label eventObjectContent = new Label();
            eventObjectContent.Text = _GlobalResources.TheObjectTheEventRefersTo;
            sectionContentEvent.Controls.Add(eventObjectContent);

            Panel linkButtonTwo1 = new Panel();
            linkButtonTwo1.CssClass = "EmailLinkContent";
            HyperLink lkObject1 = new HyperLink();
            lkObject1.CssClass = "emailBodyLink";
            lkObject1.Text = PlaceholderConstants.PLACEHOLDER_OBJECT_NAME;
            lkObject1.ToolTip = _GlobalResources.TheSpecificNameOfTheObjectForTheGivenEvent;
            linkButtonTwo1.Controls.Add(lkObject1);

            Panel linkButtonTwo2 = new Panel();
            linkButtonTwo2.CssClass = "EmailLinkContent";
            HyperLink lkObject2 = new HyperLink();
            lkObject2.CssClass = "emailBodyLink";
            lkObject2.Text = PlaceholderConstants.PLACEHOLDER_EVENT_DATE;
            lkObject2.ToolTip = _GlobalResources.TheDateTheEventOccurredOrWillOccur;
            linkButtonTwo2.Controls.Add(lkObject2);

            Panel linkButtonTwo3 = new Panel();
            linkButtonTwo3.CssClass = "EmailLinkContent";
            HyperLink lkObject3 = new HyperLink();
            lkObject3.CssClass = "emailBodyLink";
            lkObject3.Text = PlaceholderConstants.PLACEHOLDER_EVENT_TIME;
            lkObject3.ToolTip = _GlobalResources.TheTimeTheEventOccurredOrWillOccur;
            linkButtonTwo3.Controls.Add(lkObject3);

            Panel linkButtonTwo4 = new Panel();
            linkButtonTwo4.CssClass = "EmailLinkContent";
            HyperLink lkObject4 = new HyperLink();
            lkObject4.CssClass = "emailBodyLink";
            lkObject4.Text = PlaceholderConstants.PLACEHOLDER_OPTOUT_LINK;
            lkObject4.ToolTip = _GlobalResources.LinkToOptOutOfEmailNotifications;
            linkButtonTwo4.Controls.Add(lkObject4);

            panelPlaceHolder.Controls.Add(sectionContentEvent);
            panelPlaceHolder.Controls.Add(linkButtonTwo1);
            panelPlaceHolder.Controls.Add(linkButtonTwo2);
            panelPlaceHolder.Controls.Add(linkButtonTwo3);
            panelPlaceHolder.Controls.Add(linkButtonTwo4);

            //USER
            Panel sectionTitleUser = new Panel();
            sectionTitleUser.CssClass = "EmailPlaceHolderTitle PlaceholderUser";
            Label userLabel = new Label();
            userLabel.Text = _GlobalResources.User + ":";
            sectionTitleUser.Controls.Add(userLabel);
            panelPlaceHolder.Controls.Add(sectionTitleUser);

            Panel sectionContentUser = new Panel();
            sectionContentUser.CssClass = "EmailPlaceHolderContent PlaceholderUser";
            Label userContent = new Label();
            userContent.Text = _GlobalResources.TheUserThatIsTheSubjectOfTheEvent;
            sectionContentUser.Controls.Add(userContent);

            Panel linkUser1Panel = new Panel();
            linkUser1Panel.CssClass = "EmailLinkContent PlaceholderUser";
            HyperLink linkUser1 = new HyperLink();
            linkUser1.CssClass = "emailBodyLink";
            linkUser1.Text = PlaceholderConstants.PLACEHOLDER_USER_FIRSTNAME;
            linkUser1.ToolTip = _GlobalResources.TheFirstNameOfTheUserThatIsTheSubject;
            linkUser1Panel.Controls.Add(linkUser1);

            Panel linkUser2Panel = new Panel();
            linkUser2Panel.CssClass = "EmailLinkContent PlaceholderUser";
            HyperLink linkUser2 = new HyperLink();
            linkUser2.CssClass = "emailBodyLink";
            linkUser2.Text = PlaceholderConstants.PLACEHOLDER_USER_FULLNAME;
            linkUser2.ToolTip = _GlobalResources.TheFullNameOfTheUserThatIsTheSubject;
            linkUser2Panel.Controls.Add(linkUser2);

            Panel linkUser3Panel = new Panel();
            linkUser3Panel.CssClass = "EmailLinkContent PlaceholderUser";
            HyperLink linkUser3 = new HyperLink();
            linkUser3.CssClass = "emailBodyLink";
            linkUser3.Text = PlaceholderConstants.PLACEHOLDER_USER_LOGIN;
            linkUser3.ToolTip = _GlobalResources.TheLoginUsernameOfTheUserThatIsTheSubject;
            linkUser3Panel.Controls.Add(linkUser3);

            Panel linkUser4Panel = new Panel();
            linkUser4Panel.CssClass = "EmailLinkContent PlaceholderUser";
            HyperLink linkUser4 = new HyperLink();
            linkUser4.CssClass = "emailBodyLink";
            linkUser4.Text = PlaceholderConstants.PLACEHOLDER_USER_EMAIL;
            linkUser4.ToolTip = _GlobalResources.TheEmailAddressOfTheUserThatIsTheSubject;
            linkUser4Panel.Controls.Add(linkUser4);

            panelPlaceHolder.Controls.Add(sectionContentUser);
            panelPlaceHolder.Controls.Add(linkUser1Panel);
            panelPlaceHolder.Controls.Add(linkUser2Panel);
            panelPlaceHolder.Controls.Add(linkUser3Panel);
            panelPlaceHolder.Controls.Add(linkUser4Panel);

            //RECIPIENT
            Panel sectionTitleRecipient = new Panel();
            sectionTitleRecipient.CssClass = "EmailPlaceHolderTitle";
            Label recipientLabel = new Label();
            recipientLabel.Text = _GlobalResources.Recipient + ":";
            sectionTitleRecipient.Controls.Add(recipientLabel);
            panelPlaceHolder.Controls.Add(sectionTitleRecipient);

            Panel sectionContentRecipient = new Panel();
            sectionContentRecipient.CssClass = "EmailPlaceHolderContent";
            Label recipientContent = new Label();
            recipientContent.Text = _GlobalResources.TheRecipientOfTheNotification;
            sectionContentRecipient.Controls.Add(recipientContent);

            Panel linkRecipientPanel1 = new Panel();
            linkRecipientPanel1.CssClass = "EmailLinkContent";
            HyperLink linkRecipient1 = new HyperLink();
            linkRecipient1.CssClass = "emailBodyLink";
            linkRecipient1.Text = PlaceholderConstants.PLACEHOLDER_RECIPIENT_FULLNAME;
            linkRecipient1.ToolTip = _GlobalResources.TheFullNameOfTheRecipient;
            linkRecipientPanel1.Controls.Add(linkRecipient1);

            Panel linkRecipientPanel2 = new Panel();
            linkRecipientPanel2.CssClass = "EmailLinkContent";
            HyperLink linkRecipient2 = new HyperLink();
            linkRecipient2.CssClass = "emailBodyLink";
            linkRecipient2.Text = PlaceholderConstants.PLACEHOLDER_RECIPIENT_FIRSTNAME;
            linkRecipient2.ToolTip = _GlobalResources.TheFirstNameOfTheRecipient;
            linkRecipientPanel2.Controls.Add(linkRecipient2);

            Panel linkRecipientPanel3 = new Panel();
            linkRecipientPanel3.CssClass = "EmailLinkContent";
            HyperLink linkRecipient3 = new HyperLink();
            linkRecipient3.CssClass = "emailBodyLink";
            linkRecipient3.Text = PlaceholderConstants.PLACEHOLDER_RECIPIENT_LOGIN;
            linkRecipient3.ToolTip = _GlobalResources.TheLoginUsernameOfTheRecipient;
            linkRecipientPanel3.Controls.Add(linkRecipient3);

            Panel linkRecipientPanel4 = new Panel();
            linkRecipientPanel4.CssClass = "EmailLinkContent";
            HyperLink linkRecipient4 = new HyperLink();
            linkRecipient4.CssClass = "emailBodyLink";
            linkRecipient4.Text = PlaceholderConstants.PLACEHOLDER_RECIPIENT_EMAIL;
            linkRecipient4.ToolTip = _GlobalResources.TheEmailAddressOfTheRecipient;
            linkRecipientPanel4.Controls.Add(linkRecipient4);

            panelPlaceHolder.Controls.Add(sectionContentRecipient);
            panelPlaceHolder.Controls.Add(linkRecipientPanel1);
            panelPlaceHolder.Controls.Add(linkRecipientPanel2);
            panelPlaceHolder.Controls.Add(linkRecipientPanel3);
            panelPlaceHolder.Controls.Add(linkRecipientPanel4);

            //COURSE ENROLLMENT
            Panel sectionTitleEnrollment = new Panel();
            sectionTitleEnrollment.CssClass = "EmailPlaceHolderTitle PlaceholderCourseEnrollment";
            Label enrollmentLabel = new Label();
            enrollmentLabel.Text = _GlobalResources.Course + " " + _GlobalResources.Enrollment + ":";
            sectionTitleEnrollment.Controls.Add(enrollmentLabel);
            panelPlaceHolder.Controls.Add(sectionTitleEnrollment);

            Panel sectionContentEnrollment = new Panel();
            sectionContentEnrollment.CssClass = "EmailPlaceHolderContent PlaceholderCourseEnrollment";
            Label enrollementContent = new Label();
            enrollementContent.Text = _GlobalResources.InformationAboutASpecificEnrollment;
            sectionContentEnrollment.Controls.Add(enrollementContent);

            Panel linkEnrollmentPanel1 = new Panel();
            linkEnrollmentPanel1.CssClass = "EmailLinkContent PlaceholderCourseEnrollment";
            HyperLink linkEnrollment1 = new HyperLink();
            linkEnrollment1.CssClass = "emailBodyLink";
            linkEnrollment1.Text = PlaceholderConstants.PLACEHOLDER_ENROLLMENT_COURSEESTCOMPLETIONTIME;
            linkEnrollment1.ToolTip = _GlobalResources.TheEstimatedTimeToCompleteTheCourse;
            linkEnrollmentPanel1.Controls.Add(linkEnrollment1);

            Panel linkEnrollmentPanel2 = new Panel();
            linkEnrollmentPanel2.CssClass = "EmailLinkContent PlaceholderCourseEnrollment";
            HyperLink linkEnrollment2 = new HyperLink();
            linkEnrollment2.CssClass = "emailBodyLink";
            linkEnrollment2.Text = PlaceholderConstants.PLACEHOLDER_ENROLLMENT_COURSEDESCRIPTION;
            linkEnrollment2.ToolTip = _GlobalResources.TheCoursesDescriptionTakenFromShortDescription;
            linkEnrollmentPanel2.Controls.Add(linkEnrollment2);

            Panel linkEnrollmentPanel3 = new Panel();
            linkEnrollmentPanel3.CssClass = "EmailLinkContent PlaceholderCourseEnrollment";
            HyperLink linkEnrollment3 = new HyperLink();
            linkEnrollment3.CssClass = "emailBodyLink";
            linkEnrollment3.Text = PlaceholderConstants.PLACEHOLDER_ENROLLMENT_DUEDATE;
            linkEnrollment3.ToolTip = _GlobalResources.TheDueDateForTheUsersEnrollment;
            linkEnrollmentPanel3.Controls.Add(linkEnrollment3);

            Panel linkEnrollmentPanel4 = new Panel();
            linkEnrollmentPanel4.CssClass = "EmailLinkContent PlaceholderCourseEnrollment";
            HyperLink linkEnrollment4 = new HyperLink();
            linkEnrollment4.CssClass = "emailBodyLink";
            linkEnrollment4.Text = PlaceholderConstants.PLACEHOLDER_ENROLLMENT_EXPIREDATE;
            linkEnrollment4.ToolTip = _GlobalResources.TheDueDateForTheUsersEnrollment;
            linkEnrollmentPanel4.Controls.Add(linkEnrollment4);

            panelPlaceHolder.Controls.Add(sectionContentEnrollment);
            panelPlaceHolder.Controls.Add(linkEnrollmentPanel1);
            panelPlaceHolder.Controls.Add(linkEnrollmentPanel2);
            panelPlaceHolder.Controls.Add(linkEnrollmentPanel3);
            panelPlaceHolder.Controls.Add(linkEnrollmentPanel4);

            //LEARNING PATH ENROLLMENT
            Panel sectionTitleLPEnrollment = new Panel();
            sectionTitleLPEnrollment.CssClass = "EmailPlaceHolderTitle PlaceholderLearningPathEnrollment";
            Label enrollmentLPLabel = new Label();
            enrollmentLPLabel.Text = _GlobalResources.LearningPath + " " + _GlobalResources.Enrollment + ":";
            sectionTitleLPEnrollment.Controls.Add(enrollmentLPLabel);
            panelPlaceHolder.Controls.Add(sectionTitleLPEnrollment);

            Panel sectionContentLPEnrollment = new Panel();
            sectionContentLPEnrollment.CssClass = "EmailPlaceHolderContent PlaceholderLearningPathEnrollment";
            Label enrollementLPContent = new Label();
            enrollementLPContent.Text = _GlobalResources.InformationAboutASpecificEnrollment;
            sectionContentLPEnrollment.Controls.Add(enrollementLPContent);

            Panel linkLPEnrollmentPanel1 = new Panel();
            linkLPEnrollmentPanel1.CssClass = "EmailLinkContent PlaceholderLearningPathEnrollment";
            HyperLink linkLPEnrollment1 = new HyperLink();
            linkLPEnrollment1.CssClass = "emailBodyLink";
            linkLPEnrollment1.Text = PlaceholderConstants.PLACEHOLDER_ENROLLMENT_LEARNINGPATHEDESCRIPTION;
            linkLPEnrollment1.ToolTip = _GlobalResources.TheDueDateForTheUsersEnrollment;
            linkLPEnrollmentPanel1.Controls.Add(linkLPEnrollment1);

            Panel linkLPEnrollmentPanel2 = new Panel();
            linkLPEnrollmentPanel2.CssClass = "EmailLinkContent PlaceholderLearningPathEnrollment";
            HyperLink linkLPEnrollment2 = new HyperLink();
            linkLPEnrollment2.CssClass = "emailBodyLink";
            linkLPEnrollment2.Text = PlaceholderConstants.PLACEHOLDER_ENROLLMENT_DUEDATE;
            linkLPEnrollment2.ToolTip = _GlobalResources.TheDueDateForTheUsersEnrollment;
            linkLPEnrollmentPanel2.Controls.Add(linkLPEnrollment2);

            Panel linkLPEnrollmentPanel3 = new Panel();
            linkLPEnrollmentPanel3.CssClass = "EmailLinkContent PlaceholderLearningPathEnrollment";
            HyperLink linkLPEnrollment3 = new HyperLink();
            linkLPEnrollment3.CssClass = "emailBodyLink";
            linkLPEnrollment3.Text = PlaceholderConstants.PLACEHOLDER_ENROLLMENT_EXPIREDATE;
            linkLPEnrollment3.ToolTip = _GlobalResources.TheDueDateForTheUsersEnrollment;
            linkLPEnrollmentPanel3.Controls.Add(linkLPEnrollment3);

            panelPlaceHolder.Controls.Add(sectionContentLPEnrollment);
            panelPlaceHolder.Controls.Add(linkLPEnrollmentPanel1);
            panelPlaceHolder.Controls.Add(linkLPEnrollmentPanel2);
            panelPlaceHolder.Controls.Add(linkLPEnrollmentPanel3);

            //SESSION
            Panel sectionTitleSession = new Panel();
            sectionTitleSession.CssClass = "EmailPlaceHolderTitle PlaceholderSession";
            Label sessionLabel = new Label();
            sessionLabel.Text = _GlobalResources.Session + ":";
            sectionTitleSession.Controls.Add(sessionLabel);
            panelPlaceHolder.Controls.Add(sectionTitleSession);

            Panel sectionContentSession = new Panel();
            sectionContentSession.CssClass = "EmailPlaceHolderContent PlaceholderSession";
            Label sessionContent = new Label();
            sessionContent.Text = _GlobalResources.InformationAboutASpecificSession;
            sectionContentSession.Controls.Add(sessionContent);

            Panel linkSessionPanel1 = new Panel();
            linkSessionPanel1.CssClass = "EmailLinkContent PlaceholderSession";
            HyperLink linkSession1 = new HyperLink();
            linkSession1.CssClass = "emailBodyLink";
            linkSession1.Text = PlaceholderConstants.PLACEHOLDER_SESSION_DATETIME;
            linkSessionPanel1.Controls.Add(linkSession1);

            Panel linkSessionPanel2 = new Panel();
            linkSessionPanel2.CssClass = "EmailLinkContent PlaceholderSession";
            HyperLink linkSession2 = new HyperLink();
            linkSession2.CssClass = "emailBodyLink";
            linkSession2.Text = PlaceholderConstants.PLACEHOLDER_SESSION_TIMEZONE;
            //linkSession2.ToolTip = _GlobalResources.TheEstimatedTimeToCompleteTheCourse;
            linkSessionPanel2.Controls.Add(linkSession2);

            Panel linkSessionPanel3 = new Panel();
            linkSessionPanel3.CssClass = "EmailLinkContent PlaceholderSession";
            HyperLink linkSession3 = new HyperLink();
            linkSession3.CssClass = "emailBodyLink";
            linkSession3.Text = PlaceholderConstants.PLACEHOLDER_SESSION_LOCATION;
            //linkSession3.ToolTip = _GlobalResources.TheEstimatedTimeToCompleteTheCourse;
            linkSessionPanel3.Controls.Add(linkSession3);

            Panel linkSessionPanel4 = new Panel();
            linkSessionPanel4.CssClass = "EmailLinkContent PlaceholderSession";
            HyperLink linkSession4 = new HyperLink();
            linkSession4.CssClass = "emailBodyLink";
            linkSession4.Text = PlaceholderConstants.PLACEHOLDER_SESSION_LOCATIONDESCRIPTION;
            //linkSession4.ToolTip = _GlobalResources.TheEstimatedTimeToCompleteTheCourse;
            linkSessionPanel4.Controls.Add(linkSession4);

            Panel linkSessionPanel5 = new Panel();
            linkSessionPanel5.CssClass = "EmailLinkContent PlaceholderSession";
            HyperLink linkSession5 = new HyperLink();
            linkSession5.CssClass = "emailBodyLink";
            linkSession5.Text = PlaceholderConstants.PLACEHOLDER_SESSION_INSTRUCTORFULLNAME;
            linkSessionPanel5.Controls.Add(linkSession5);

            Panel linkSessionPanel6 = new Panel();
            linkSessionPanel6.ID = "DropSessionPanel";
            linkSessionPanel6.CssClass = "EmailLinkContent PlaceholderSession";
            HyperLink linkSession6 = new HyperLink();
            linkSession6.CssClass = "emailBodyLink";
            linkSession6.Text = PlaceholderConstants.PLACEHOLDER_SESSION_DROPLINK;
            // show the drop session link only if the notification is going to learners
            if (this.Recipient == Convert.ToString(EventTypeRecipient.Learners))
            {
                linkSessionPanel6.Style.Add("display", "");
            }
            else
            {
                linkSessionPanel6.Style.Add("display", "none");
            }
            linkSessionPanel6.Controls.Add(linkSession6);

            panelPlaceHolder.Controls.Add(sectionContentSession);
            panelPlaceHolder.Controls.Add(linkSessionPanel1);
            panelPlaceHolder.Controls.Add(linkSessionPanel2);
            panelPlaceHolder.Controls.Add(linkSessionPanel3);
            panelPlaceHolder.Controls.Add(linkSessionPanel4);
            panelPlaceHolder.Controls.Add(linkSessionPanel5);
            panelPlaceHolder.Controls.Add(linkSessionPanel6);


            // USER REGISTRATION REJECTED
            Panel sectionTitleUserRegistrationRejection = new Panel();
            sectionTitleUserRegistrationRejection.CssClass = "EmailPlaceHolderTitle PlaceholderUserRegistrationRejection";
            Label userRegistrationRejectionLabel = new Label();
            userRegistrationRejectionLabel.Text = _GlobalResources.UserRegistrationRejected + ":";
            sectionTitleUserRegistrationRejection.Controls.Add(userRegistrationRejectionLabel);
            panelPlaceHolder.Controls.Add(sectionTitleUserRegistrationRejection);

            Panel sectionContentUserRegistrationRejection = new Panel();
            sectionContentUserRegistrationRejection.CssClass = "EmailPlaceHolderContent PlaceholderUserRegistrationRejection";
            Label userRegistrationRejectionContent = new Label();
            userRegistrationRejectionContent.Text = _GlobalResources.ReasonForUserRegistrationRejection;
            sectionContentUserRegistrationRejection.Controls.Add(userRegistrationRejectionContent);

            Panel linkUserRegistrationRejectionPanel1 = new Panel();
            linkUserRegistrationRejectionPanel1.CssClass = "EmailLinkContent PlaceholderUserRegistrationRejection";
            HyperLink linkUserRegistrationRejection1 = new HyperLink();
            linkUserRegistrationRejection1.CssClass = "emailBodyLink";
            linkUserRegistrationRejection1.Text = PlaceholderConstants.PLACEHOLDER_USERREGISTRATIONREJECTION_REJECTIONCOMMENTS;
            linkUserRegistrationRejection1.ToolTip = _GlobalResources.ReasonForUserRegistrationRejection;
            linkUserRegistrationRejectionPanel1.Controls.Add(linkUserRegistrationRejection1);

            panelPlaceHolder.Controls.Add(sectionContentUserRegistrationRejection);
            panelPlaceHolder.Controls.Add(linkUserRegistrationRejectionPanel1);

            // COURSE ENROLLMENT REQUERST REJECTED
            Panel sectionTitleEnrollmentRequestRejection = new Panel();
            sectionTitleEnrollmentRequestRejection.CssClass = "EmailPlaceHolderTitle PlaceholderEnrollmentRequestRejection";
            Label enrollmentRequestRejectionLabel = new Label();
            enrollmentRequestRejectionLabel.Text = _GlobalResources.CourseEnrollmentRequestRejected + ":";
            sectionTitleEnrollmentRequestRejection.Controls.Add(enrollmentRequestRejectionLabel);
            panelPlaceHolder.Controls.Add(sectionTitleEnrollmentRequestRejection);

            Panel sectionContentEnrollmentRequestRejection = new Panel();
            sectionContentEnrollmentRequestRejection.CssClass = "EmailPlaceHolderContent PlaceholderEnrollmentRequestRejection";
            Label enrollmentRequestRejectionContent = new Label();
            enrollmentRequestRejectionContent.Text = _GlobalResources.ReasonForCourseEnrollmentRejection;
            sectionContentEnrollmentRequestRejection.Controls.Add(enrollmentRequestRejectionContent);

            Panel linkEnrollmentRequestRejectionPanel1 = new Panel();
            linkEnrollmentRequestRejectionPanel1.CssClass = "EmailLinkContent PlaceholderEnrollmentRequestRejection";
            HyperLink linkEnrollmentRequestRejection1 = new HyperLink();
            linkEnrollmentRequestRejection1.CssClass = "emailBodyLink";
            linkEnrollmentRequestRejection1.Text = PlaceholderConstants.PLACEHOLDER_ENROLLMENTREQUEST_REJECTIONCOMMENTS;
            linkEnrollmentRequestRejection1.ToolTip = _GlobalResources.ReasonForCourseEnrollmentRejection;
            linkEnrollmentRequestRejectionPanel1.Controls.Add(linkEnrollmentRequestRejection1);

            panelPlaceHolder.Controls.Add(sectionContentEnrollmentRequestRejection);
            panelPlaceHolder.Controls.Add(linkEnrollmentRequestRejectionPanel1);

            //CERTIFICATION
            Panel sectionTitleCertification = new Panel();
            sectionTitleCertification.CssClass = "EmailPlaceHolderTitle PlaceholderCertification";
            Label certificationLabel = new Label();
            certificationLabel.Text = _GlobalResources.Certification + ":";
            sectionTitleCertification.Controls.Add(certificationLabel);
            panelPlaceHolder.Controls.Add(sectionTitleCertification);

            Panel sectionContentCertification = new Panel();
            sectionContentCertification.CssClass = "EmailPlaceHolderContent PlaceholderCertification";
            Label certificationContent = new Label();
            certificationContent.Text = _GlobalResources.InformationAboutASpecificCertification;
            sectionContentCertification.Controls.Add(certificationContent);

            Panel linkCertificationPanel1 = new Panel();
            linkCertificationPanel1.CssClass = "EmailLinkContent PlaceholderCertification";

            HyperLink linkCertification1 = new HyperLink();
            linkCertification1.CssClass = "emailBodyLink";
            linkCertification1.Text = PlaceholderConstants.PLACEHOLDER_CERTIFICATION_DESCRIPTION;
            linkCertification1.ToolTip = _GlobalResources.TheCertificationsDescription;
            linkCertificationPanel1.Controls.Add(linkCertification1);

            panelPlaceHolder.Controls.Add(sectionContentCertification);
            panelPlaceHolder.Controls.Add(linkCertificationPanel1);

            //DISCUSSION
            Panel sectionTitleDiscussion = new Panel();
            sectionTitleDiscussion.CssClass = "EmailPlaceHolderTitle PlaceholderDiscussion";
            Label discussionLabel = new Label();
            discussionLabel.Text = _GlobalResources.Discussion + ":";
            sectionTitleDiscussion.Controls.Add(discussionLabel);
            panelPlaceHolder.Controls.Add(sectionTitleDiscussion);

            Panel sectionContentDiscussion = new Panel();
            sectionContentDiscussion.CssClass = "EmailPlaceHolderContent PlaceholderDiscussion";
            Label discussionContent = new Label();
            discussionContent.Text = _GlobalResources.DetailsOfTheDiscussionMessage;
            sectionContentDiscussion.Controls.Add(discussionContent);

            Panel linkDiscussionPanel1 = new Panel();
            linkDiscussionPanel1.CssClass = "EmailLinkContent PlaceholderDiscussion";

            HyperLink linkDiscussion1 = new HyperLink();
            linkDiscussion1.CssClass = "emailBodyLink";
            linkDiscussion1.Text = PlaceholderConstants.PLACEHOLDER_DISCUSSION_MESSAGE;
            linkDiscussion1.ToolTip = _GlobalResources.DiscussionFeed;
            linkDiscussionPanel1.Controls.Add(linkDiscussion1);

            panelPlaceHolder.Controls.Add(sectionContentDiscussion);
            panelPlaceHolder.Controls.Add(linkDiscussionPanel1); 

            // attach the placeholders panel
            parentPanel.Controls.Add(panelPlaceHolder);
        }
        #endregion
        #endregion
    }
}
