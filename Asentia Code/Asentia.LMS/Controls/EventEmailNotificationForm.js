﻿/*
functions when page loading
*/

var targetBody = 'undefined';
var subjectText = false;

function UpdateInsertPlaceholderFocusSubject() {
    subjectText = true;
    targetBody = $("input[id^='SubjectText_Field']").filter(":visible");
}

function UpdateInsertPlaceholderFocusBody() {
    subjectText = false;
    targetBody = $("textarea[id^='BodyText_Field']").filter(":visible");
}

function UpdateInsertPlaceholderFocusBodyHTML() {
    subjectText = false;
    for (var instanceName in CKEDITOR.instances) {
        if ($("#HTMLBodyText_CKEDITORContainer" + instanceName.substr(instanceName.lastIndexOf('_')).replace("_Field", '')).is(':visible')) {
            targetBody = CKEDITOR.instances['HTMLBodyText_Field' + instanceName.substr(instanceName.lastIndexOf('_')).replace("_Field", '')];            
        }
    }
}

$(document).ready(function () {
    EventTypeOnChange(true);
    CurrentTimeCheckBoxOnClick();

    if (certificationsEnabled == false)
        $("#DropDownList_EventType option[value^='6']").remove();

    if (taskModulesEnabled == false)
        $("#DropDownList_EventType option[value='305']").remove();

    if (ojtModulesEnabled == false)
        $("#DropDownList_EventType option[value='304']").remove();

    if (learningPathsEnabled == false)
        $("#DropDownList_EventType option[value^='5']").remove();

    if ($("#DropDownList_EventType").is(':disabled'))
        $("#CurrentTimeCheckBox").prop('disabled', true);

    //sets focus id
    $("input[id^='SubjectText_Field']").each(function (index) {
        $("input[id^='SubjectText_Field']")[index].addEventListener("focus", UpdateInsertPlaceholderFocusSubject, true);
    });

    $("textarea[id^='BodyText_Field']").each(function (index) {
        $("textarea[id^='BodyText_Field']")[index].addEventListener("focus", UpdateInsertPlaceholderFocusBody, true);
    });
    
    for (var instanceName in CKEDITOR.instances) {
        CKEDITOR.instances['HTMLBodyText_Field' + instanceName.substr(instanceName.lastIndexOf('_')).replace("_Field", '')].on('blur', UpdateInsertPlaceholderFocusBodyHTML);
    }
       
    var htmlBased = false;
        
    if ($("#RadioButtonList_Type_Field input:checked").attr('id').slice(-1) == '1') {
        $("#BodyText_Container").hide();
        $("#HTMLBodyText_Container").show();
        htmlBased = true;

    } else  {
        $("#BodyText_Container").show();
        $("#HTMLBodyText_Container").hide();
        htmlBased = false;
    }

    if ($("#RecipientList option:selected").val() == '12') {
        $("#SpecificEmailTextBox").show();
    } else {
        $("#SpecificEmailTextBox").hide();
    }

    $("#RadioButtonList_Type_Field input").change(function () {
        int = 0;
        if ($("#RadioButtonList_Type_Field input:checked").attr('id').slice(-1) == '1') {
            $("#BodyText_Container").hide();
            $("#HTMLBodyText_Container").show();
            for (var instanceName in CKEDITOR.instances) {
                CKEDITOR.instances['HTMLBodyText_Field' + instanceName.substr(instanceName.lastIndexOf('_')).replace("_Field", '')].setData($("textarea[id^='BodyText_Field']").eq(int).val());                
                int++;
            }
            $("textarea[id^='BodyText_Field']").val('');
            htmlBased = true;
        }
        else {
            $("#BodyText_Container").show();
            $("#HTMLBodyText_Container").hide();
            for (var instanceName in CKEDITOR.instances) {
                CKEDITOR.instances['HTMLBodyText_Field' + instanceName.substr(instanceName.lastIndexOf('_')).replace("_Field", '')].setData('');
            }
            htmlBased = false;
        }
    });

    //Define place holder link position
    $("#BodyText_Container").removeClass("FormFieldContainer");
    $("#HTMLBodyText_Container").removeClass("FormFieldContainer");
    $("#Placeholder_InputContainer").removeClass("FormFieldInputContainer").addClass("AddPlaceHolder");

    //Insert place holder text
    $('.emailBodyLink').click(function (e) {
        $find('PlaceHolderModalModalPopupExtender').hide();
        if (targetBody == 'undefined') {
            subjectText = false;
            if (htmlBased)
                for (var instanceName in CKEDITOR.instances) {
                    if ($("#HTMLBodyText_CKEDITORContainer" + instanceName.substr(instanceName.lastIndexOf('_')).replace("_Field", '')).is(':visible'))
                        targetBody = CKEDITOR.instances['HTMLBodyText_Field' + instanceName.substr(instanceName.lastIndexOf('_')).replace("_Field", '')];
                }
            else
                targetBody = $("textarea[id^='BodyText_Field']").filter(":visible");
        }
        if (htmlBased && !subjectText) {
            var origText = targetBody.getData();
            targetBody.insertText($(e.target).text());
        }
        else {
            var cursorPosition = $(targetBody).prop("selectionStart");
            if (targetBody.focus) {
                targetBody.val(targetBody.val().substr(0, cursorPosition) + $(e.target).text() + targetBody.val().substr(cursorPosition, targetBody.val().length - cursorPosition)).focus();
                targetBody.setCursorPosition(cursorPosition + $(e.target).text().length);
            }
        }
    });   
});

$.fn.setCursorPosition = function (pos) {
    this.each(function (index, elem) {
        if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
        } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    });
    return this;
};

//Fuction to bind the recipient dynamically on event type dropdown value change
function EventTypeOnChange(selected) {
    var eligibleRecipients = $("option:selected", $("#DropDownList_EventType")).attr("idEventTypeRecipients").replace(/\s/g, "").split(",");
    var $recipient = $("#RecipientList");
    var recipientOptions = $("#RecipientList option");
    var originalSelectedValue = $("option:selected", $("#RecipientList")).val();
    var selectedValue;

    $recipient.empty(); // remove old options
    $.each(recipientObject, function (index, option) {

        if ($.inArray(option.optionValue, eligibleRecipients) !== -1) {
            $recipient.append($("<option></option>")
                .attr("value", option.optionValue).text(option.optionText));
            if (selected && option.optionSelected)
            {
                selectedValue = option.optionValue;
            }
        }
    });

    if (selected) {
        selectedValue = originalSelectedValue;
    }
    $("#RecipientList option[value='" + selectedValue + "']").prop("selected", true);
    
    var $ddlBeforeAfter = $("#Drop_Down_List_BeforeAfter");
    var originalBeforeAfter = $("option:selected", $("#Drop_Down_List_BeforeAfter")).val();

    $ddlBeforeAfter.empty();

    if (($("option:selected", $("#DropDownList_EventType")).attr("allowPriorSend").toLowerCase() === "true") &&
        ($("option:selected", $("#DropDownList_EventType")).attr("allowPostSend").toLowerCase() === "true")) {
        $("#CurrentTimeCheckBox").prop('disabled', false);
        $ddlBeforeAfter.append($("<option></option>").attr("value", 'before').text(beforeText));
        $ddlBeforeAfter.append($("<option></option>").attr("value", 'after').text(afterText));
        $("#Drop_Down_List_BeforeAfter option[value='" + originalBeforeAfter + "']").prop("selected", true);
        $("#Send_DifferentTime_Panel").show();
    } else if (($("option:selected", $("#DropDownList_EventType")).attr("allowPriorSend").toLowerCase() === "true") &&
        ($("option:selected", $("#DropDownList_EventType")).attr("allowPostSend").toLowerCase() === "false")) {
        $("#CurrentTimeCheckBox").prop('disabled', false);
        $ddlBeforeAfter.append($("<option></option>").attr("value", 'before').text(beforeText));
        $("#Send_DifferentTime_Panel").show();
    } else if (($("option:selected", $("#DropDownList_EventType")).attr("allowPriorSend").toLowerCase() === "false") &&
        ($("option:selected", $("#DropDownList_EventType")).attr("allowPostSend").toLowerCase() === "true")) {
        $("#CurrentTimeCheckBox").prop('disabled', false);
        $ddlBeforeAfter.append($("<option></option>").attr("value", 'after').text(afterText));
        $("#Send_DifferentTime_Panel").show();
    } else {
        $("#CurrentTimeCheckBox").prop('checked', true);
        $("#IntervalNumberTextBox").val('');
        $("#TimeFrame_Drop_Down_List option").removeAttr("selected");
        $("#Send_DifferentTime_Panel").hide();
    }

    var disabledCondition = $("#CurrentTimeCheckBox").is(':checked');
    $("#IntervalNumberTextBox").prop('disabled', disabledCondition ? true : false);
    $("#TimeFrame_Drop_Down_List").prop('disabled', disabledCondition ? true : false);
    $("#Drop_Down_List_BeforeAfter").prop('disabled', disabledCondition ? true : false);

    var eventTypeInit = Math.floor(parseInt($("option:selected", $("#DropDownList_EventType")).val()) / 100);
    var eventType = parseInt($("option:selected", $("#DropDownList_EventType")).val());

    // Check if attachment option is needed (only shown for 401(session meets)/404(learner join session)/409(promoted from waitlist))
    if (eventType == 401 || eventType == 404 || eventType == 406 || eventType == 409) {
        $("#CalendarAttachment_Container").show();
    }
    else {
        $("#CalendarAttachment_Container").hide();
        $("#CheckBox_Calendar_Attachment_Field").prop('checked', false);
    }

    // Check event type relative to available placeholders shown
    // Check if the event is a User Registration Rejection    
    if (eventType == 105) {
        $(".PlaceholderUser").show();
        $(".PlaceholderUserRegistrationRejection").show();
        $(".PlaceholderCourseEnrollment").hide();
        $(".PlaceholderLearningPathEnrollment").hide();
        $(".PlaceholderSession").hide();
        $(".PlaceholderEnrollmentRequestRejection").hide();
        $(".PlaceholderCertification").hide();
        $(".PlaceholderDiscussion").hide();
    }
    else if (eventTypeInit == 4) {
        // Check if eventType = 401(session meets)/406(session time/location change), remove user placeholder
        if (eventType == 401 || eventType == 406) {
            $(".PlaceholderUser").hide();
        }
        else {
            $(".PlaceholderUser").show();
        }
        $(".PlaceholderSession").show();
        $(".PlaceholderCourseEnrollment").hide();
        $(".PlaceholderLearningPathEnrollment").hide();
        $(".PlaceholderUserRegistrationRejection").hide();
        $(".PlaceholderEnrollmentRequestRejection").hide();
        $(".PlaceholderCertification").hide();
        $(".PlaceholderDiscussion").hide();
    }
    else if (eventTypeInit == 2 || eventTypeInit == 5) {
        // Check if enrollment is course or learning paht
        if (eventTypeInit == 2) {
            $(".PlaceholderCourseEnrollment").show();
            $(".PlaceholderLearningPathEnrollment").hide();
        }
        else
        {
            $(".PlaceholderLearningPathEnrollment").show();
            $(".PlaceholderCourseEnrollment").hide();
        }
        $(".PlaceholderUser").show();
        $(".PlaceholderSession").hide();
        $(".PlaceholderUserRegistrationRejection").hide();

        // Check if the event is an Enrollment Request Rejection
        if (eventType == 208) {
            $(".PlaceholderEnrollmentRequestRejection").show();
        }
        else {
            $(".PlaceholderEnrollmentRequestRejection").hide();
        }

        $(".PlaceholderCertification").hide();
        $(".PlaceholderDiscussion").hide();
    }
    else if (eventTypeInit == 6) {
        $(".PlaceholderUser").show();
        $(".PlaceholderCertification").show();
        $(".PlaceholderCourseEnrollment").hide();
        $(".PlaceholderLearningPathEnrollment").hide();
        $(".PlaceholderSession").hide();
        $(".PlaceholderUserRegistrationRejection").hide();
        $(".PlaceholderEnrollmentRequestRejection").hide();
        $(".PlaceholderDiscussion").hide();
    }
    else if (eventTypeInit == 9) {
        $(".PlaceholderUser").show();
        $(".PlaceholderDiscussion").show();
        $(".PlaceholderCourseEnrollment").hide();
        $(".PlaceholderLearningPathEnrollment").hide();
        $(".PlaceholderSession").hide();
        $(".PlaceholderUserRegistrationRejection").hide();
        $(".PlaceholderEnrollmentRequestRejection").hide();
        $(".PlaceholderCertification").hide();
    }
    else {
        $(".PlaceholderUser").show();
        $(".PlaceholderCourseEnrollment").hide();
        $(".PlaceholderLearningPathEnrollment").hide();
        $(".PlaceholderSession").hide();
        $(".PlaceholderUserRegistrationRejection").hide();
        $(".PlaceholderEnrollmentRequestRejection").hide();
        $(".PlaceholderCertification").hide();
        $(".PlaceholderDiscussion").hide();
    }
}

//Fuction to disable the time interval controls if current time check box is checked
function CurrentTimeCheckBoxOnClick() {
    var disabledCondition = $("#CurrentTimeCheckBox").is(':checked');
    if (disabledCondition) $("#IntervalNumberTextBox").val('');

    $("#IntervalNumberTextBox").prop('disabled', disabledCondition ? true : false);
    $("#TimeFrame_Drop_Down_List").prop('disabled', disabledCondition ? true : false);
    $("#Drop_Down_List_BeforeAfter").prop('disabled', disabledCondition ? true : false);
}

// recipient on change event
function RecipientOnChange() {
    var eventId = Math.floor(parseInt($("option:selected", $("#DropDownList_EventType")).val()) / 100);
    var recipientId = $("#RecipientList option:selected").val();

    // if this is a session event email sent to learners, show the drop link placeholder
    if (eventId == 4 && (recipientId == 6 || recipientId == 2)) {
        $('#DropSessionPanel').show();
    } else {
        $('#DropSessionPanel').hide();
    }

    if (recipientId == 12) {
        $('#SpecificEmailTextBox').show();
    } else {
        $('#SpecificEmailTextBox').val('');
        $('#SpecificEmailTextBox').hide();
    }
}