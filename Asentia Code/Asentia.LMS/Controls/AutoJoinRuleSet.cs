﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Asentia.Common;
using Asentia.LMS.Library;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Data;
using Asentia.UMS.Library;
using System.Reflection;

namespace Asentia.LMS.Controls
{
    /// <summary>
    /// Displays an AutoJoinRuleSet control that allows user to create rules.
    /// </summary>
    [ToolboxData("<{0}:AutoJoinRuleSet runat=server></{0}:AutoJoinRuleSet>")]
    public class AutoJoinRuleSet : WebControl
    {
        #region Fields
        /// <summary>
        /// Used to store rule set id
        /// </summary>
        private int _IdRuleSet;

        /// <summary>
        /// Used to display controls for defining rules.
        /// </summary>
        private DataTable _RulesData;

        /// <summary>
        /// Used to display controls for defining rules.
        /// </summary>
        private Table _RulesTable;

        /// <summary>
        /// Used to add a new rule.
        /// </summary>
        private LinkButton _AddRuleButton;

        /// <summary>
        /// Identifies whether information is complete.
        /// </summary>
        private bool _IsInformationComplete = true;

        /// <summary>
        /// Date Format.
        /// </summary>
        private const string _DateFormat = "yyyy-MM-dd"; // ISO-8601
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the AutoJoinRuleSet class.
        /// </summary>
        public AutoJoinRuleSet()
        {
            this._RulesTable = new Table();
            this._AddRuleButton = new LinkButton();
        }

        /// <summary>
        /// Initializes a new instance of the AutoJoinRuleSet class.
        /// </summary>
        public AutoJoinRuleSet(int idRuleSet)
        {
            this._IdRuleSet = idRuleSet;
            this._RulesTable = new Table();
            this._AddRuleButton = new LinkButton();
        }

        /// <summary>
        /// Initializes a new instance of the AutoJoinRuleSet class.
        /// </summary>
        public AutoJoinRuleSet(DataTable rules)
        {
            this._RulesData = rules;
            this._RulesTable = new Table();
            this._AddRuleButton = new LinkButton();
        }
        #endregion

        #region Overridden Methods
        #region OnInit
        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ((Page)HttpContext.Current.Handler).RegisterRequiresControlState(this);
            this.Controls.Clear();
            base.CreateChildControls();

            // tules table
            this._RulesTable.ID = "RulesTable_" + this.ClientID;
            this._RulesTable.ClientIDMode = ClientIDMode.AutoID;
            this.Controls.Add(this._RulesTable);
        }
        #endregion

        #region OnLoad
        /// <summary>
        /// OnLoad
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            // "add rule" button
            this._AddRuleButton.ID = "AddRuleButton_" + this.ClientID;
            this._AddRuleButton.ClientIDMode = ClientIDMode.AutoID;
            
            Image newRuleImageForLink = new Image();
            newRuleImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            newRuleImageForLink.CssClass = "SmallIcon";

            if (!this.Enabled)
            { newRuleImageForLink.CssClass += " DimIcon"; }

            this._AddRuleButton.Controls.Add(newRuleImageForLink);
            this.Controls.Add(this._AddRuleButton);

            if (this.Enabled)
            { this._AddRuleButton.OnClientClick = "addRule(this); return false;"; }

            // load existing rules based on whether or not this is an existing ruleset and if this is a postback            
            if (!((Page)HttpContext.Current.Handler).IsPostBack)
            {
                if (this._IdRuleSet > 0)
                {
                    DataTable rules = new RuleSet().GetAssociatedRules(this._IdRuleSet);

                    if (rules != null && rules.Rows.Count > 0)
                    { this._LoadRules(rules); }
                    else
                    { this._AddRule(1); } // if rules could not be loaded or there are none for the ruleset, build one rule row as we need one by default
                }
                else // if this is a new ruleset, build one rule row as we need one by default, and return
                {
                    this._AddRule(1); 
                    return;
                }
            }
            else
            {
                // load rules if there is already data in the RulesData object
                if (this._RulesData != null)
                { this._LoadRules(this._RulesData); }
                else if (this._IdRuleSet > 0) // if there is no existing RulesData, get the rules
                {
                    DataTable rules = new RuleSet().GetAssociatedRules(this._IdRuleSet);

                    if (rules != null && rules.Rows.Count > 0)
                    { this._LoadRules(rules); }
                    else // if rules could not be loaded or there are none for the ruleset, build one rule row as we need one by default
                    { 
                        this._AddRule(1);
                        return;
                    }
                }
                else // no existing RulesData object and no IdRuleSet, build one rule row as we need one by default
                { 
                    this._AddRule(1);
                    return;
                } 
            }
            
            // build JS script to loop through rule inputs so we can enable/disable operators, and/or apply datepickers where "less than (date)" or "greater than (date)" is selected
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("$(\".RuleRow .OperatorsList\").each(function () {");
            sb.AppendLine("    RuleOperatorChanged(this);");
            sb.AppendLine("});");
            sb.AppendLine("");
            sb.AppendLine("$(\".RuleRow .UserFieldsList\").each(function () {");
            sb.AppendLine("    RuleFieldChanged(this);");
            sb.AppendLine("});");

            ScriptManager.RegisterStartupScript(_RulesTable, this.GetType(), "EvaluateRuleOperatorsOnLoad", sb.ToString(), true);
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// OnPreRender
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            EnsureChildControls();

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = ((Page)HttpContext.Current.Handler).ClientScript;
            csm.RegisterClientScriptResource(this.GetType(), "Asentia.LMS.Controls.AutoJoinRuleSet.js");

            // ADD DATEPICKER INIT AND DESTROY JS FUNCTIONS

            // compile string of month names
            string monthNames = "['"
                                + _GlobalResources.January_abbr + "','"
                                + _GlobalResources.February_abbr + "','"
                                + _GlobalResources.March_abbr + "','"
                                + _GlobalResources.April_abbr + "','"
                                + _GlobalResources.May + "','"
                                + _GlobalResources.June_abbr + "','"
                                + _GlobalResources.July_abbr + "','"
                                + _GlobalResources.August_abbr + "','"
                                + _GlobalResources.September_abbr + "','"
                                + _GlobalResources.October_abbr + "','"
                                + _GlobalResources.November_abbr + "','"
                                + _GlobalResources.December_abbr + "']";

            // compile string of day names
            string dayNames = "['"
                              + _GlobalResources.Sunday_abbr + "','"
                              + _GlobalResources.Monday_abbr + "','"
                              + _GlobalResources.Tuesday_abbr + "','"
                              + _GlobalResources.Wednesday_abbr + "','"
                              + _GlobalResources.Thursday_abbr + "','"
                              + _GlobalResources.Friday_abbr + "','"
                              + _GlobalResources.Saturday_abbr + "']";

            // previous button text
            string prevText = "'" + _GlobalResources.Prev + "'";

            // next button text
            string nextText = "'" + _GlobalResources.Next + "'";

            // build the functions
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("function InitializeDatePicker(fieldId) {");
            sb.AppendLine(" $(\"#\" + fieldId).datepicker({");
            sb.AppendLine("     dateFormat: \"yy-mm-dd\",");
            sb.AppendLine("     changeMonth: true,");
            sb.AppendLine("     changeYear: true,");
            sb.AppendLine("     nextText: " + nextText + ",");
            sb.AppendLine("     prevText: " + prevText + ",");
            sb.AppendLine("     dayNamesMin: " + dayNames + ",");
            sb.AppendLine("     monthNamesShort: " + monthNames + ",");
            sb.AppendLine("     yearRange: \"1900:+10\"");
            sb.AppendLine(" });");
            sb.AppendLine("}");
            sb.AppendLine("");
            sb.AppendLine("function DestroyDatePicker(fieldId) {");
            sb.AppendLine(" $(\"#\" + fieldId).datepicker(\"destroy\");");            
            sb.AppendLine("}");

            // attach the functions to the page
            csm.RegisterClientScriptBlock(this.GetType(), "DatePickerInitAndDestroyFunctions", sb.ToString(), true);            
        }
        #endregion

        #region RenderContents
        /// <summary>
        /// RenderContents
        /// </summary>
        /// <param name="output"></param>
        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write("<div class=\"RuleSetControl\">");
            output.Write("<table>");
            output.Write("<tr>");
            output.Write("<td>");
            this._RulesTable.RenderControl(output);
            output.Write("</td>");
            output.Write("<td class=\"AddRuleColumn\">");
            this._AddRuleButton.RenderControl(output);
            output.Write("</td>");
            output.Write("</tr>");
            output.Write("</table>");
            output.Write("</div>");
        }
        #endregion
        #endregion

        #region Private Methods
        #region _AddErrorClassToControl
        private void _AddErrorClassToControl(WebControl reqControl, string toolTipErrorMessage)
        {
            reqControl.CssClass += " RuleError";
            reqControl.ToolTip = toolTipErrorMessage;

            _IsInformationComplete = false;
        }
        #endregion

        #region _RemoveErrorClassFromControl
        private void _RemoveErrorClassFromControl(WebControl reqControl)
        {
            reqControl.CssClass = reqControl.CssClass.ToString().Replace(" RuleError", "");
            reqControl.ToolTip = null;
        }
        #endregion

        #region _AddRule
        /// <summary>
        /// Adds controls for a rule.
        /// </summary>
        private void _AddRule(int id)
        {
            //Create row and cells
            TableRow rowRule = new TableRow();
            rowRule.ID = "Rule_" + id;
            rowRule.CssClass = "RuleRow";
            TableCell cellUserField = new TableCell();
            TableCell cellOperator = new TableCell();
            TableCell cellUserFieldValue = new TableCell();
            TableCell cellRemoveButton = new TableCell();

            //Create and add user invisible text for ruleId
            TextBox ruleIdValue = new TextBox();
            ruleIdValue.ID = "RuleIdValue_" + id;
            ruleIdValue.CssClass = "RuleIdValue";
            ruleIdValue.Style["display"] = "none";
            cellUserField.Controls.Add(ruleIdValue);

            //Create and add user fields list
            DropDownList userFieldsList = new DropDownList();
            userFieldsList.ID = "UserFieldsList_" + id;
            userFieldsList.CssClass = "UserFieldsList";
            userFieldsList.Attributes.Add("onchange", "RuleFieldChanged(this)");
            _LoadUserFields(userFieldsList);
            cellUserField.Controls.Add(userFieldsList);

            //Create and add operators list
            DropDownList operatorsList = new DropDownList();
            operatorsList.ID = "OperatorsList_" + id;
            operatorsList.CssClass = "OperatorsList";
            operatorsList.Attributes.Add("onchange", "RuleOperatorChanged(this)");
            operatorsList.Items.Add(new ListItem(_GlobalResources.startswith_lower, "sw"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.doesnotstartwith_lower, "nsw"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.endswith_lower, "ew"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.doesnotendwith_lower, "new"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.contains_lower, "c"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.doesnotcontain_lower, "nc"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.equals_lower, "eq"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.doesnotequal_lower, "neq"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.greaterthannumeric_lower, "gtn"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.lessthannumeric_lower, "ltn"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.greaterthandate_lower, "gtd"));
            operatorsList.Items.Add(new ListItem(_GlobalResources.lessthandate_lower, "ltd"));
            cellOperator.Controls.Add(operatorsList);

            //Create and add text box
            TextBox userFieldValue = new TextBox();
            userFieldValue.ID = "UserFieldValue_" + id;
            userFieldValue.CssClass = "UserFieldValue";
            userFieldValue.MaxLength = 255;
            cellUserFieldValue.Controls.Add(userFieldValue);

            //Create and add remove button
            LinkButton removeRuleButton = new LinkButton();
            removeRuleButton.ID = "RemoveRuleButton_" + id;
            removeRuleButton.Attributes.Add("ID", id.ToString());
            removeRuleButton.CssClass = "RemoveRuleButton";

            Image removeRuleImageForLink = new Image();
            removeRuleImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            removeRuleImageForLink.CssClass = "SmallIcon";

            //if (this.Enabled)
            //{ removeRuleImageForLink.CssClass += " DimIcon"; }

            removeRuleButton.Controls.Add(removeRuleImageForLink);

            if (this.Enabled)
            { removeRuleButton.OnClientClick = "removeRule(this);return false;"; }

            cellRemoveButton.Controls.Add(removeRuleButton);

            //Add cells to row
            rowRule.Cells.Add(cellUserField);
            rowRule.Cells.Add(cellOperator);
            rowRule.Cells.Add(cellUserFieldValue);
            rowRule.Cells.Add(cellRemoveButton);

            //Add row to the table
            _RulesTable.Rows.Add(rowRule);
        }
        #endregion

        #region _LoadUserFields
        /// <summary>
        /// Populates the user fields drop down list.
        /// </summary>
        /// <param name="userFieldsList">The user fields drop down list.</param>
        private void _LoadUserFields(DropDownList userFieldsList)
        {
            UserAccountData userAccountData = new UserAccountData(UserAccountDataFileType.Site, true, false);
            List<UserAccountData.UserFieldForLabel> lstUserFields = userAccountData.GetRuleSetEligibleFieldLabelsInLanguage(AsentiaSessionState.UserCulture);

            foreach (UserAccountData.UserFieldForLabel userField in lstUserFields)
            {
                ListItem userFieldItem = new ListItem(userField.Label, userField.Identifier);

                if (userField.InputType == UserAccountData.InputType.Date || userField.Identifier.StartsWith("dt"))
                { userFieldItem.Attributes.Add("class", "UserFieldTypeDate"); }

                if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RESTRICTEDRULECRITERIA_ENABLE))
                {
                    userFieldsList.Items.Add(userFieldItem);
                }
            }

            // add group to fields list
            userFieldsList.Items.Add(new ListItem(_GlobalResources.Group, "group"));
        }
        #endregion

        #region _LoadRules
        /// <summary>
        /// Load provided Rules of for a rule set
        /// </summary>
        private void _LoadRules(DataTable rules)
        {
            for (int i = 0; i < rules.Rows.Count; i++)
            {
                int id = i + 1;
                _AddRule(id);

                TextBox ruleIdValue = (TextBox)this.FindControl("RuleIdValue_" + id);
                DropDownList userFieldList = (DropDownList)this.FindControl("UserFieldsList_" + id);
                DropDownList operatorsList = (DropDownList)this.FindControl("OperatorsList_" + id);
                TextBox userFieldValue = (TextBox)this.FindControl("UserFieldValue_" + id);

                if (ruleIdValue != null && userFieldList != null && operatorsList != null && userFieldValue != null)
                {
                    ruleIdValue.Text = Convert.ToString(rules.Rows[i]["idRule"]);

                    ListItem selectedItem = null;

                    selectedItem = userFieldList.Items.FindByValue(Convert.ToString(rules.Rows[i]["userField"]));
                    if (selectedItem != null)
                    {
                        selectedItem.Selected = true;
                    }

                    selectedItem = null;
                    selectedItem = operatorsList.Items.FindByValue(Convert.ToString(rules.Rows[i]["operator"]));
                    if (selectedItem != null)
                    {
                        selectedItem.Selected = true;
                    }

                    if (rules.Rows[i]["textValue"] != DBNull.Value)
                    {
                        userFieldValue.Text = Convert.ToString(rules.Rows[i]["textValue"]);
                    }
                    else if (rules.Rows[i]["dateValue"] != DBNull.Value)
                    {
                        userFieldValue.Text = Convert.ToDateTime(rules.Rows[i]["dateValue"]).ToString(_DateFormat);
                    }
                    else if (rules.Rows[i]["numValue"] != DBNull.Value)
                    {
                        userFieldValue.Text = Convert.ToString(rules.Rows[i]["numValue"]);
                    }
                    else if (rules.Rows[i]["bitValue"] != DBNull.Value)
                    {
                        userFieldValue.Text = Convert.ToString(rules.Rows[i]["bitValue"]);
                    }
                }
            }
        }
        #endregion
        #endregion

        #region Public Methods
        #region GetRulesAfterValidatingData
        /// <summary>
        /// returns the rules datatable if data is valid
        /// </summary>
        /// <returns></returns>
        public DataTable GetRulesAfterValidatingData()
        {

            DataTable rulesDataTable = new DataTable();
            rulesDataTable.Columns.Add("idRule", typeof(int));
            rulesDataTable.Columns.Add("idSite", typeof(int));
            rulesDataTable.Columns.Add("userField", typeof(string));
            rulesDataTable.Columns.Add("operator", typeof(string));
            rulesDataTable.Columns.Add("textValue", typeof(string));
            rulesDataTable.Columns.Add("dateValue", typeof(DateTime));
            rulesDataTable.Columns.Add("numValue", typeof(int));
            rulesDataTable.Columns.Add("bitValue", typeof(bool));

            #region Validation And Creating Rules Data Table

            if (_RulesTable != null && _RulesTable.Rows.Count > 0)
            {
                for (int i = 0; i < _RulesTable.Rows.Count; i++)
                {
                    int id = i + 1;
                    TableRow row = (TableRow)_RulesTable.FindControl("Rule_" + id);
                    if (row != null)
                    {
                        TextBox ruleIdValue = (TextBox)row.FindControl("RuleIdValue_" + id);
                        TextBox userFieldValue = (TextBox)row.FindControl("UserFieldValue_" + id);

                        if (ruleIdValue != null && userFieldValue != null)
                        {
                            DropDownList userFieldsList = (DropDownList)row.FindControl("UserFieldsList_" + id);
                            DropDownList operatorsList = (DropDownList)row.FindControl("OperatorsList_" + id);

                            // validate user field if selected value is not "group"
                            if (userFieldsList.SelectedValue.ToLower() != "group")
                            {
                                FieldInfo[] fields = typeof(User).GetFields();
                                FieldInfo field = fields.Where(f => f.Name.ToLower() == userFieldsList.SelectedValue.ToLower()).FirstOrDefault();
                                
                                if (field == null)
                                {
                                    throw new Exception(_GlobalResources.TheSelectedUserFieldDoesNotExist);
                                }

                                bool isValidValue = false;

                                try
                                {
                                    if (string.IsNullOrWhiteSpace(userFieldValue.Text))
                                    {
                                        _AddErrorClassToControl(userFieldValue, _GlobalResources.YouMustEnterAValue);
                                    }
                                    else
                                    {
                                        // 1) Validate value against the user field type
                                        try
                                        {
                                            if (field.FieldType == typeof(Nullable<Int32>) || field.FieldType == typeof(Int32))
                                            { Convert.ToInt32(userFieldValue.Text); }
                                            else if (field.FieldType == typeof(Nullable<DateTime>) || field.FieldType == typeof(DateTime))
                                            { DateTime.ParseExact(userFieldValue.Text, _DateFormat, CultureInfo.InvariantCulture); }
                                            else if (field.FieldType == typeof(Nullable<Boolean>) || field.FieldType == typeof(Boolean))
                                            { Convert.ToBoolean(userFieldValue.Text); }
                                            else
                                            { }                                           
                                        }
                                        catch
                                        { 
                                            if (field.FieldType == typeof(Nullable<Int32>) || field.FieldType == typeof(Int32))
                                            { throw new AsentiaException(_GlobalResources.MustBeAValidNumber); }
                                            else if (field.FieldType == typeof(Nullable<DateTime>) || field.FieldType == typeof(DateTime))
                                            { throw new AsentiaException(_GlobalResources.MustBeAValidDateYYYYMMDD); }
                                            else if (field.FieldType == typeof(Nullable<Boolean>) || field.FieldType == typeof(Boolean))
                                            { throw new AsentiaException(_GlobalResources.MustBeAValidBoolean); }                                            
                                            else
                                            { throw new AsentiaException(_GlobalResources.IsNotValid); }
                                        }
                                        
                                        // 2) validate against "greater than" and "less than" date or number operators
                                        if (operatorsList.SelectedValue == "gtd" || operatorsList.SelectedValue == "ltd")
                                        {
                                            DateTime parsedDate;

                                            if (!DateTime.TryParse(userFieldValue.Text, out parsedDate))
                                            { throw new AsentiaException(_GlobalResources.MustBeAValidDateYYYYMMDD); }
                                        }
                                        else if (operatorsList.SelectedValue == "gtn" || operatorsList.SelectedValue == "ltn")
                                        {
                                            int parsedInt;

                                            if (!Int32.TryParse(userFieldValue.Text, out parsedInt))
                                            { throw new AsentiaException(_GlobalResources.MustBeAValidNumber); }
                                        }
                                        else
                                        { }
                                        
                                        // all validations passed, remove any previous errors and mark as valid
                                        _RemoveErrorClassFromControl(userFieldValue);
                                        isValidValue = true;
                                    }
                                }
                                catch (AsentiaException aEx) // catch the validation exception and set the error on the control
                                {
                                    _AddErrorClassToControl(userFieldValue, aEx.Message);
                                }

                                if (isValidValue)
                                {
                                    DataRow dataRow = rulesDataTable.NewRow();
                                    dataRow["idRule"] = Convert.ToInt32(ruleIdValue.Text);
                                    dataRow["idSite"] = AsentiaSessionState.IdSite;
                                    dataRow["userField"] = userFieldsList.SelectedValue;
                                    dataRow["operator"] = operatorsList.SelectedValue;

                                    if (field.FieldType == typeof(Nullable<Int32>) || field.FieldType == typeof(Int32))
                                    {
                                        dataRow["numValue"] = Convert.ToInt32(userFieldValue.Text);
                                    }
                                    else if (field.FieldType == typeof(Nullable<DateTime>) || field.FieldType == typeof(DateTime))
                                    {
                                        dataRow["dateValue"] = DateTime.ParseExact(userFieldValue.Text, _DateFormat, CultureInfo.InvariantCulture);
                                    }
                                    else if (field.FieldType == typeof(Nullable<Boolean>) || field.FieldType == typeof(Boolean))
                                    {
                                        dataRow["bitValue"] = Convert.ToBoolean(userFieldValue.Text);
                                    }                                    
                                    else
                                    {
                                        if (operatorsList.SelectedValue == "gtd" || operatorsList.SelectedValue == "ltd")
                                        { dataRow["dateValue"] = DateTime.ParseExact(userFieldValue.Text, _DateFormat, CultureInfo.InvariantCulture); }
                                        else if (operatorsList.SelectedValue == "gtn" || operatorsList.SelectedValue == "ltn")
                                        { dataRow["numValue"] = Convert.ToInt32(userFieldValue.Text); }
                                        else
                                        { dataRow["textValue"] = userFieldValue.Text; }
                                    }

                                    rulesDataTable.Rows.Add(dataRow);
                                }
                            }
                            else // valadate group selected value which is always a string
                            {
                                bool isValidValue = true;

                                if (string.IsNullOrWhiteSpace(userFieldValue.Text))
                                {
                                    isValidValue = false;
                                    _AddErrorClassToControl(userFieldValue, _GlobalResources.YouMustEnterAValue);
                                }

                                if (isValidValue)
                                {
                                    DataRow dataRow = rulesDataTable.NewRow();
                                    dataRow["idRule"] = Convert.ToInt32(ruleIdValue.Text);
                                    dataRow["idSite"] = AsentiaSessionState.IdSite;
                                    dataRow["userField"] = userFieldsList.SelectedValue;
                                    dataRow["operator"] = operatorsList.SelectedValue;
                                    dataRow["textValue"] = userFieldValue.Text;

                                    rulesDataTable.Rows.Add(dataRow);
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            if (_IsInformationComplete)
            {
                return rulesDataTable;
            }
            else
            {
                return null;
            }
        }
        #endregion
        #endregion
    }

    #region RuleObject Class
    /// <summary>
    /// Used to receive the json stringified data with rules information.
    /// </summary>    
    internal class RuleObject
    {
        #region Properties
        public string IdRule
        {
            get { return this._IdRule; }
            set { this._IdRule = value; }
        }

        public string UserField
        {
            get { return this._UserField; }
            set { this._UserField = value; }
        }

        public string Operator
        {
            get { return this._Operator; }
            set { this._Operator = value; }
        }

        public string Value
        {
            get { return this._Value; }
            set { this._Value = value; }
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// Rule Id
        /// </summary>
        private string _IdRule = String.Empty;
        
        /// <summary>
        /// User Field
        /// </summary>
        private string _UserField = String.Empty;

        /// <summary>
        /// Operator
        /// </summary>
        private string _Operator = String.Empty;
        
        /// <summary>
        /// Value
        /// </summary>
        private string _Value = String.Empty;
        #endregion
    }
    #endregion
}
