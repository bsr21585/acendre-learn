﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Controls
{
    public class StandupTrainingObjectMenu : WebControl
    {
        #region Constructor
        public StandupTrainingObjectMenu(StandupTraining standupTrainingObject)
            : base(HtmlTextWriterTag.Div)
        {
            this.StandupTrainingObject = standupTrainingObject;
            this.ID = "StandupTrainingObjectMenu";
        }
        #endregion

        #region Properties
        /// <summary>
        /// The standup training that this menu is built for.
        /// </summary>
        public StandupTraining StandupTrainingObject;

        /// <summary>
        /// The selected menu item.
        /// </summary>
        public MenuObjectItem SelectedItem = MenuObjectItem.None;
        #endregion

        #region MenuObjectItem ENUM
        public enum MenuObjectItem
        {
            None = 0,
            StandupTrainingDashboard = 1,
            StandupTrainingProperties = 2,
            Sessions = 3,
            EmailNotifications = 4,            
        }
        #endregion

        #region Private Methods
        #region _BuildStandupTrainingObjectMenu
        private void _BuildStandupTrainingObjectMenu()
        {
            this.CssClass = "ObjectMenu";

            // STANDUP TRAINING DASHBOARD

            // standup training dashboard link container
            Panel standupTrainingDashboardLinkContainer = new Panel();
            standupTrainingDashboardLinkContainer.ID = "StandupTrainingDashboardLinkContainer";
            standupTrainingDashboardLinkContainer.CssClass = "ObjectMenuLinkContainer";
            standupTrainingDashboardLinkContainer.ToolTip = _GlobalResources.InstructorLedTrainingDashboard;

            HyperLink standupTrainingDashboardImageLink = new HyperLink();
            standupTrainingDashboardImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD, ImageFiles.EXT_PNG);
            standupTrainingDashboardImageLink.NavigateUrl = "/administrator/standuptraining/Dashboard.aspx?id=" + this.StandupTrainingObject.Id.ToString();
            standupTrainingDashboardLinkContainer.Controls.Add(standupTrainingDashboardImageLink);

            this.Controls.Add(standupTrainingDashboardLinkContainer);

            // STANDUP TRAINING PROPERTIES

            // standup training properties link container
            Panel standupTrainingPropertiesLinkContainer = new Panel();
            standupTrainingPropertiesLinkContainer.ID = "StandupTrainingPropertiesLinkContainer";
            standupTrainingPropertiesLinkContainer.CssClass = "ObjectMenuLinkContainer";
            standupTrainingPropertiesLinkContainer.ToolTip = _GlobalResources.InstructorLedTrainingProperties;

            HyperLink standupTrainingPropertiesImageLink = new HyperLink();
            standupTrainingPropertiesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLASSROOM, ImageFiles.EXT_PNG);
            standupTrainingPropertiesImageLink.NavigateUrl = "/administrator/standuptraining/Modify.aspx?id=" + this.StandupTrainingObject.Id.ToString();
            standupTrainingPropertiesLinkContainer.Controls.Add(standupTrainingPropertiesImageLink);

            this.Controls.Add(standupTrainingPropertiesLinkContainer);

            // SESSIONS            

            // sessions link container
            Panel sessionsLinkContainer = new Panel();
            sessionsLinkContainer.ID = "SessionsLinkContainer";
            sessionsLinkContainer.CssClass = "ObjectMenuLinkContainer";
            sessionsLinkContainer.ToolTip = _GlobalResources.Sessions;

            HyperLink sessionsImageLink = new HyperLink();
            sessionsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION, ImageFiles.EXT_PNG);
            sessionsImageLink.NavigateUrl = "/administrator/standuptraining/sessions/Default.aspx?stid=" + this.StandupTrainingObject.Id.ToString();
            sessionsLinkContainer.Controls.Add(sessionsImageLink);

            this.Controls.Add(sessionsLinkContainer);

            // EMAIL NOTIFICATIONS

            // email notifications link container
            Panel emailNotificationsLinkContainer = new Panel();
            emailNotificationsLinkContainer.ID = "EmailNotificationsLinkContainer";
            emailNotificationsLinkContainer.CssClass = "ObjectMenuLinkContainer";
            emailNotificationsLinkContainer.ToolTip = _GlobalResources.EmailNotifications;

            HyperLink emailNotificationsImageLink = new HyperLink();
            emailNotificationsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
            emailNotificationsImageLink.NavigateUrl = "/administrator/standuptraining/emailnotifications/Default.aspx?stid=" + this.StandupTrainingObject.Id.ToString();
            emailNotificationsLinkContainer.Controls.Add(emailNotificationsImageLink);

            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE) ?? false)
            {
                this.Controls.Add(emailNotificationsLinkContainer);
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnLoad
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.CreateControls();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        #endregion

        #region CreateControls
        /// <summary>
        /// Creates the controls for rendering to page.
        /// </summary>
        private void CreateControls()
        {
            // build the standup training object menu
            this._BuildStandupTrainingObjectMenu();

            // apply selected class to item
            switch (this.SelectedItem)
            {
                case MenuObjectItem.None:
                    // nothing to do
                    break;
                case MenuObjectItem.StandupTrainingDashboard:
                    // select the standup training dashboard link
                    Panel standupTrainingDashboardLinkContainer = (Panel)this.FindControl("StandupTrainingDashboardLinkContainer");

                    if (standupTrainingDashboardLinkContainer != null)
                    { standupTrainingDashboardLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.StandupTrainingProperties:
                    // select the standup training properties link
                    Panel standupTrainingPropertiesLinkContainer = (Panel)this.FindControl("StandupTrainingPropertiesLinkContainer");

                    if (standupTrainingPropertiesLinkContainer != null)
                    { standupTrainingPropertiesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Sessions:
                    // select the sessions link
                    Panel sessionsLinkContainer = (Panel)this.FindControl("SessionsLinkContainer");

                    if (sessionsLinkContainer != null)
                    { sessionsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.EmailNotifications:
                    // select the email notifications link
                    Panel emailNotificationsLinkContainer = (Panel)this.FindControl("EmailNotificationsLinkContainer");

                    if (emailNotificationsLinkContainer != null)
                    { emailNotificationsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;                
            }
        }
        #endregion
        #endregion
    }
}
