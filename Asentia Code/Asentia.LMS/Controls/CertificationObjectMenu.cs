﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Controls
{
    public class CertificationObjectMenu : WebControl
    {
        #region Constructor
        public CertificationObjectMenu(Certification certificationObject)
            : base(HtmlTextWriterTag.Div)
        {
            this.CertificationObject = certificationObject;
            this.ID = "CertificationObjectMenu";
        }
        #endregion

        #region Properties
        /// <summary>
        /// The certification that this menu is built for.
        /// </summary>
        public Certification CertificationObject;

        /// <summary>
        /// The selected menu item.
        /// </summary>
        public MenuObjectItem SelectedItem = MenuObjectItem.None;
        #endregion

        #region MenuObjectItem ENUM
        public enum MenuObjectItem
        {
            None = 0,
            CertificationDashboard = 1,
            CertificationProperties = 2,            
            EmailNotifications = 3,
            AutoJoinRules = 4,
            Certificates = 5,
        }
        #endregion

        #region Private Methods
        #region _BuildCertificationObjectMenu
        private void _BuildCertificationObjectMenu()
        {
            this.CssClass = "ObjectMenu";

            // CERTIFICATION DASHBOARD

            // certification dashboard link container
            Panel certificationDashboardLinkContainer = new Panel();
            certificationDashboardLinkContainer.ID = "CertificationDashboardLinkContainer";
            certificationDashboardLinkContainer.CssClass = "ObjectMenuLinkContainer";
            certificationDashboardLinkContainer.ToolTip = _GlobalResources.CertificationDashboard;

            HyperLink certificationDashboardImageLink = new HyperLink();
            certificationDashboardImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD, ImageFiles.EXT_PNG);
            certificationDashboardImageLink.NavigateUrl = "/administrator/certifications/Dashboard.aspx?id=" + this.CertificationObject.Id.ToString();
            certificationDashboardLinkContainer.Controls.Add(certificationDashboardImageLink);

            this.Controls.Add(certificationDashboardLinkContainer);

            // CERTIFICATION PROPERTIES

            // certification properties link container
            Panel certificationPropertiesLinkContainer = new Panel();
            certificationPropertiesLinkContainer.ID = "CertificationPropertiesLinkContainer";
            certificationPropertiesLinkContainer.CssClass = "ObjectMenuLinkContainer";
            certificationPropertiesLinkContainer.ToolTip = _GlobalResources.CertificationProperties;

            HyperLink certificationPropertiesImageLink = new HyperLink();
            certificationPropertiesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
            certificationPropertiesImageLink.NavigateUrl = "/administrator/certifications/Modify.aspx?id=" + this.CertificationObject.Id.ToString();
            certificationPropertiesLinkContainer.Controls.Add(certificationPropertiesImageLink);

            this.Controls.Add(certificationPropertiesLinkContainer);

            // CERTIFICATES            

            // certificates link container
            Panel certificatesLinkContainer = new Panel();
            certificatesLinkContainer.ID = "CertificatesLinkContainer";
            certificatesLinkContainer.CssClass = "ObjectMenuLinkContainer";
            certificatesLinkContainer.ToolTip = _GlobalResources.Certificates;

            HyperLink certificatesImageLink = new HyperLink();
            certificatesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
            certificatesImageLink.NavigateUrl = "/administrator/certifications/certificates/Default.aspx?cid=" + this.CertificationObject.Id.ToString();
            certificatesLinkContainer.Controls.Add(certificatesImageLink);

            this.Controls.Add(certificatesLinkContainer);

            // EMAIL NOTIFICATIONS

            // email notifications link container
            Panel emailNotificationsLinkContainer = new Panel();
            emailNotificationsLinkContainer.ID = "EmailNotificationsLinkContainer";
            emailNotificationsLinkContainer.CssClass = "ObjectMenuLinkContainer";
            emailNotificationsLinkContainer.ToolTip = _GlobalResources.EmailNotifications;

            HyperLink emailNotificationsImageLink = new HyperLink();
            emailNotificationsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
            emailNotificationsImageLink.NavigateUrl = "/administrator/certifications/emailnotifications/Default.aspx?cid=" + this.CertificationObject.Id.ToString();
            emailNotificationsLinkContainer.Controls.Add(emailNotificationsImageLink);

            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE) ?? false)
            {
                this.Controls.Add(emailNotificationsLinkContainer);
            }

            // AUTO-JOIN RULES         

            // auto-join rules link container
            Panel autoJoinRulesLinkContainer = new Panel();
            autoJoinRulesLinkContainer.ID = "AutoJoinRulesLinkContainer";
            autoJoinRulesLinkContainer.CssClass = "ObjectMenuLinkContainer";
            autoJoinRulesLinkContainer.ToolTip = _GlobalResources.AutoJoinRules;

            HyperLink autoJoinRulesImageLink = new HyperLink();
            autoJoinRulesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYNCHRONIZE, ImageFiles.EXT_PNG);
            autoJoinRulesImageLink.NavigateUrl = "/administrator/certifications/AutoJoinRules.aspx?cid=" + this.CertificationObject.Id.ToString();
            autoJoinRulesLinkContainer.Controls.Add(autoJoinRulesImageLink);

            this.Controls.Add(autoJoinRulesLinkContainer);
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnLoad
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.CreateControls();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        #endregion

        #region CreateControls
        /// <summary>
        /// Creates the controls for rendering to page.
        /// </summary>
        private void CreateControls()
        {
            // build the certification object menu
            this._BuildCertificationObjectMenu();

            // apply selected class to item
            switch (this.SelectedItem)
            {
                case MenuObjectItem.None:
                    // nothing to do
                    break;
                case MenuObjectItem.CertificationDashboard:
                    // select the certification dashboard link
                    Panel certificationDashboardLinkContainer = (Panel)this.FindControl("CertificationDashboardLinkContainer");

                    if (certificationDashboardLinkContainer != null)
                    { certificationDashboardLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.CertificationProperties:
                    // select the certification properties link
                    Panel certificationPropertiesLinkContainer = (Panel)this.FindControl("CertificationPropertiesLinkContainer");

                    if (certificationPropertiesLinkContainer != null)
                    { certificationPropertiesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;                
                case MenuObjectItem.EmailNotifications:
                    // select the email notifications link
                    Panel emailNotificationsLinkContainer = (Panel)this.FindControl("EmailNotificationsLinkContainer");

                    if (emailNotificationsLinkContainer != null)
                    { emailNotificationsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.AutoJoinRules:
                    // select the auto-join rules link
                    Panel autoJoinRulesLinkContainer = (Panel)this.FindControl("AutoJoinRulesLinkContainer");

                    if (autoJoinRulesLinkContainer != null)
                    { autoJoinRulesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
            }
        }
        #endregion
        #endregion
    }
}
