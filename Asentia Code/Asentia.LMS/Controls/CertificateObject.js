﻿/*

CertificateObject

*/
CertificateObject = function(LayoutJSONObj, AdditionalJSONObj, CertificateModeType, DictionaryJSONObj, CertificateContainerElement, CertImageLocation, CertWidth, CertHeight, SaveElement) {

    var availableCertNodeCount = 0;
    var existingCertNodeCount = 0;
    var draggableObjCount = 0;
    var currentDraggableItemOffsetLeft = 0;
    var currentDraggableItemOffsetTop = 0;
    var maxDraggableFontSize = 104;
    var minDraggableFontSize = 4;
    var certificateDraggableFieldElement;
    var Parent = this;
    //input values
    this.Layout = LayoutJSONObj;
    this.Additional = AdditionalJSONObj;
    this.PageMode = CertificateModeType;
    this.TextDictionary = DictionaryJSONObj;
    this.DropElement = CertificateContainerElement;
    this.ImageLocation = CertImageLocation;
    this.currentDraggableElement;
    //manipulated Container values
    //set Default Certificate Height for Editor
    this.CertificateWidth = CertWidth;
    this.CertificateHeight = CertHeight;
    this.ImageSource = "";
    this.SaveElement = SaveElement;




    switch (this.PageMode) {

        case "1":

            //Create the Container Element with the Image from Layout.
            this.setupContainer(this.Layout);

            this.CertificateElement = document.createElement("div");
            this.CertificateElement.id = "CertificateObject"
            this.CertificateElement.style.width = this.CertificateWidth;
            this.CertificateElement.style.height = this.CertificateHeight;
            this.CertificateElement.style.backgroundImage = "url('" + this.ImageSource + "')";

            this.DropElement.appendChild(this.CertificateElement)

            currentContainer = this.CertificateElement.id

            //Create Field List
            this.FieldsListElement = document.createElement("div");
            this.FieldsListElement.id = "FieldsList";


            //Create Clickable Menu
            this.ClickableMenuElement = document.createElement("div");
            this.ClickableMenuElement.id = "MenuContainer"
            this.ClickableMenuElement.innerHTML = this.TextDictionary.Menu + " &#x2261";
            $(this.ClickableMenuElement).on("click", function() {
                $(this).css("top", "-70px");
                $(Parent.FieldsListElement).css('opacity', 0).slideDown('slow').animate({
                    opacity: 1
                }, {
                    queue: false,
                    duration: 'slow'
                });




            });

            this.DropElement.appendChild(this.ClickableMenuElement)


            /* Title of Field List */

            this.FieldTitleElement = document.createElement("div");
            this.FieldTitleElement.id = "FieldTitle";
            this.FieldTitleElement.innerHTML = this.TextDictionary.Add_Labels_and_Fields;

            this.ClickableFieldCloseElement = document.createElement("img");
            this.ClickableFieldCloseElement.id = "CloseFieldList";
            this.ClickableFieldCloseElement.name = "CloseFieldList";
            this.ClickableFieldCloseElement.src = this.ImageLocation + "remove.png";

            this.ClickableFieldCloseElement.onclick = function() {
                $(Parent.ClickableMenuElement).css("top", "-5px");
                $(Parent.FieldsListElement).css('opacity', 1).slideUp('slow').animate({
                    opacity: 0
                }, {
                    queue: false,
                    duration: 'slow'
                });




            };

            /* Create Field List Body */

            this.FieldBodyElement = document.createElement("div");
            this.FieldBodyElement.id = "FieldBody";

            /* Add Static Label */

            this.FieldStaticLabelContainerElement = document.createElement("div");
            this.FieldStaticLabelContainerElement.id = "StaticLabelContainer";

            this.FieldStaticLabelTextElement = document.createElement("div");
            this.FieldStaticLabelTextElement.id = "StaticLabelText";
            this.FieldStaticLabelTextElement.innerHTML = this.TextDictionary.Static_Label + ":";

            this.FieldStaticLabelInputElement = document.createElement("input");
            this.FieldStaticLabelInputElement.type = "text";
            this.FieldStaticLabelInputElement.id = "StaticLabelInput";

            this.FieldStaticLabelAddElement = document.createElement("input");
            this.FieldStaticLabelAddElement.type = "button";
            this.FieldStaticLabelAddElement.id = "StaticLabelAdd";
            this.FieldStaticLabelAddElement.value = this.TextDictionary.Add;
            this.FieldStaticLabelAddElement.onclick = function() {

                var label = $(Parent.FieldStaticLabelInputElement).val();
                label = label.replace(/\</g, "&lt;").replace(/\>/g, "&gt;").replace(/\t+/g, "");
                if (label == null || $.trim(label) == "") {
                    $(Parent.FieldStaticLabelInputElement).focus();



                } else {
                    certificateDraggableFieldElement = Parent.createDraggableElement('Label', label, label, Parent.ImageLocation, null, ++draggableObjCount);
                    Parent.CertificateElement.appendChild(certificateDraggableFieldElement);
                    Parent.addDraggableToElement(certificateDraggableFieldElement.id, Parent.CertificateElement, Parent.currentDraggableElement);
                    Parent.addResizableToElement(certificateDraggableFieldElement.id, Parent.CertificateElement, Parent.currentDraggableElement);
                    Parent.setLabelPosition(certificateDraggableFieldElement.id, Parent.CertificateElement);
                    Parent.setHandlers(certificateDraggableFieldElement.id, Parent.CertificateElement, minDraggableFontSize, maxDraggableFontSize, Parent.currentDraggableElement);
                    $(Parent.FieldStaticLabelInputElement).val('');
                    $(Parent.FieldStaticLabelInputElement).focus();
                    Parent.getCertificateString();
                }

            };

            this.FieldStaticLabelInputElement.onkeyup = function(event) {
                event = event || window.event;
                if (event.keyCode == 13) {
                    event.preventDefault();
                    Parent.FieldStaticLabelAddElement.click();
                    return true;
                }

            };

            this.FieldStaticLabelClearElement = document.createElement("input");
            this.FieldStaticLabelClearElement.type = "button";
            this.FieldStaticLabelClearElement.id = "StaticLabelClear";
            this.FieldStaticLabelClearElement.value = this.TextDictionary.Clear;
            this.FieldStaticLabelClearElement.onclick = function() {

                $(Parent.FieldStaticLabelInputElement).val('');
                $(Parent.FieldStaticLabelInputElement).focus();

            };


            this.FieldStaticLabelContainerElement.appendChild(this.FieldStaticLabelTextElement);

            this.FieldStaticLabelContainerElement.appendChild(this.FieldStaticLabelInputElement);

            this.FieldStaticLabelContainerElement.appendChild(this.FieldStaticLabelAddElement);

            this.FieldStaticLabelContainerElement.appendChild(this.FieldStaticLabelClearElement);

            /* Add Draggable Data Fields */

            this.FieldDataFieldsContainerElement = document.createElement("div");
            this.FieldDataFieldsContainerElement.id = "DataFieldsContainer";


            this.FieldDataFieldsTextElement = document.createElement("div");
            this.FieldDataFieldsTextElement.id = "DataFieldsText";
            this.FieldDataFieldsTextElement.innerHTML = this.TextDictionary.Data_Fields + ": ";

            this.FieldDataFieldsInstructionsElement = document.createElement("div");
            this.FieldDataFieldsInstructionsElement.id = "DataFieldsInstructions";
            this.FieldDataFieldsInstructionsElement.innerHTML = this.TextDictionary.drag_and_drop_fields_on_certificate;

            this.FieldDataFieldsTextElement.appendChild(this.FieldDataFieldsInstructionsElement);

            this.FieldDataFieldsDraggableContainerElement = document.createElement("div");
            this.FieldDataFieldsDraggableContainerElement.id = "DataFieldsDraggableContainer";

            if (typeof(this.Additional.AvailableFields) != 'undefined') {

                for (availableCertNodeCount = 0; availableCertNodeCount < Object.keys(this.Additional.AvailableFields).length; availableCertNodeCount++) {
                    /*Add to layout*/
                    if (typeof(this.Layout.Certificate) != 'undefined') {


                        if (typeof(this.Layout.Certificate.Elements) != 'undefined') {
                            for (existingCertNodeCount = 0; existingCertNodeCount < Object.keys(this.Layout.Certificate.Elements).length; existingCertNodeCount++) {
                                if (this.Layout.Certificate.Elements[existingCertNodeCount].Identifier == this.Additional.AvailableFields[availableCertNodeCount].Identifier) {
                                    name = this.Additional.AvailableFields[availableCertNodeCount].Identifier;
                                    fieldLabel = this.Additional.AvailableFields[availableCertNodeCount].Label;
                                    fieldPlaceHolder = this.Additional.AvailableFields[availableCertNodeCount].Value;
                                    certificateDraggableFieldElement = this.createDraggableElement(name, fieldLabel, fieldPlaceHolder, this.ImageLocation, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, ++draggableObjCount);
                                    this.CertificateElement.appendChild(certificateDraggableFieldElement);
                                    this.addDraggableToElement(certificateDraggableFieldElement.id, this.CertificateElement, this.currentDraggableElement);
                                    this.addResizableToElement(certificateDraggableFieldElement.id, this.CertificateElement, this.currentDraggableElement);
                                    this.setPreviousPosition(certificateDraggableFieldElement.id, this.CertificateElement, this.Layout.Certificate.Elements[existingCertNodeCount].Left, this.Layout.Certificate.Elements[existingCertNodeCount].Top, this.Layout.Certificate.Elements[existingCertNodeCount].Width, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, this.Layout.Certificate.Elements[existingCertNodeCount].Alignment);
                                    this.setHandlers(certificateDraggableFieldElement.id, this.CertificateElement, minDraggableFontSize, maxDraggableFontSize, this.currentDraggableElement);
                                    this.setFormatters(certificateDraggableFieldElement.id, this.CertificateElement);
                                }
                            }
                        }
                    }


                    /*Add to Field List*/
                    this['DraggableFieldDataElement_' + availableCertNodeCount] = document.createElement("div");
                    this['DraggableFieldDataElement_' + availableCertNodeCount].id = "DraggableFieldDataElement_" + availableCertNodeCount;
                    this['DraggableFieldDataElement_' + availableCertNodeCount].setAttribute('name', this.Additional.AvailableFields[availableCertNodeCount].Identifier);
                    this['DraggableFieldDataElement_' + availableCertNodeCount].className = "DraggableFieldDataElement";
                    this['DraggableFieldDataElement_' + availableCertNodeCount].setAttribute('placeholder', this.Additional.AvailableFields[availableCertNodeCount].Value);
                    this['DraggableFieldDataElement_' + availableCertNodeCount].innerHTML = this.Additional.AvailableFields[availableCertNodeCount].Label;

                    this['DraggableFieldDataElementContainer_' + availableCertNodeCount] = document.createElement("div");
                    this['DraggableFieldDataElementContainer_' + availableCertNodeCount].className = "DraggableFieldDataElementContainer";
                    this['DraggableFieldDataElementContainer_' + availableCertNodeCount].appendChild(this['DraggableFieldDataElement_' + availableCertNodeCount]);
                    this.FieldDataFieldsDraggableContainerElement.appendChild(this['DraggableFieldDataElementContainer_' + availableCertNodeCount]);

                }

                if (typeof(this.Layout.Certificate.Elements) != 'undefined') {
                    for (existingCertNodeCount = 0; existingCertNodeCount < Object.keys(this.Layout.Certificate.Elements).length; existingCertNodeCount++) {
                        if (this.Layout.Certificate.Elements[existingCertNodeCount].IsStaticLabel == "true") {
                            name = this.Layout.Certificate.Elements[existingCertNodeCount].Identifier;
                            fieldLabel = this.Layout.Certificate.Elements[existingCertNodeCount].Value;
                            fieldPlaceHolder = this.Layout.Certificate.Elements[existingCertNodeCount].Value;
                            certificateDraggableFieldElement = this.createDraggableElement(name, fieldLabel, fieldPlaceHolder, this.ImageLocation, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, ++draggableObjCount);
                            this.CertificateElement.appendChild(certificateDraggableFieldElement);
                            this.addDraggableToElement(certificateDraggableFieldElement.id, this.CertificateElement, this.currentDraggableElement);
                            this.addResizableToElement(certificateDraggableFieldElement.id, this.CertificateElement, this.currentDraggableElement);
                            this.setPreviousPosition(certificateDraggableFieldElement.id, this.CertificateElement, this.Layout.Certificate.Elements[existingCertNodeCount].Left, this.Layout.Certificate.Elements[existingCertNodeCount].Top, this.Layout.Certificate.Elements[existingCertNodeCount].Width, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, this.Layout.Certificate.Elements[existingCertNodeCount].Alignment);
                            this.setHandlers(certificateDraggableFieldElement.id, this.CertificateElement, minDraggableFontSize, maxDraggableFontSize, this.currentDraggableElement);
                            this.setFormatters(certificateDraggableFieldElement.id, this.CertificateElement);
                        }
                    }
                }

            }

            this.FieldDataFieldsContainerElement.appendChild(this.FieldDataFieldsTextElement);

            this.FieldDataFieldsContainerElement.appendChild(this.FieldDataFieldsDraggableContainerElement);

            this.FieldBodyElement.appendChild(this.FieldStaticLabelContainerElement);

            this.FieldBodyElement.appendChild(this.FieldDataFieldsContainerElement);

            this.FieldTitleElement.appendChild(this.ClickableFieldCloseElement);

            this.FieldsListElement.appendChild(this.FieldTitleElement);

            this.FieldsListElement.appendChild(this.FieldBodyElement);

            this.DropElement.appendChild(this.FieldsListElement);

            $(this.FieldsListElement).draggable({
                start: function(event, ui) {
                    var offset = ui.offset;
                    currentDraggableItemOffsetLeft = offset.left;
                    currentDraggableItemOffsetTop = offset.top;
                },
                drag: function(event, ui) {
                    var offset = ui.offset;
                    currentDraggableItemOffsetLeft = offset.left;
                    currentDraggableItemOffsetTop = offset.top;
                    $(Parent.FieldsListElement).css({
                        opacity: .3
                    });


                },
                stop: function(event, ui) {
                    $(Parent.FieldsListElement).css({
                        opacity: 1
                    });


                    Parent.getCertificateString();
                },
                cursor: 'move',
                handle: '#' + $(Parent.FieldTitleElement).attr('id'),
                snap: '#' + $(Parent.CertificateElement).attr('id'),
                snapMode: "both",
                containment: $(Parent.CertificateElement).parent()
            });

            this.DropElement.appendChild(this.FieldsListElement);

            $(".DraggableFieldDataElement").draggable({
                revert: 'invalid',
                helper: 'clone',
                start: function(event, ui) {
                    var offset = ui.offset;
                    currentDraggableItemOffsetLeft = offset.left;
                    currentDraggableItemOffsetTop = offset.top;
                    $(ui.helper).addClass("DraggingItem");
                    $('#' + $(Parent.FieldsListElement).attr('id')).css('opacity', 1).animate({
                        opacity: .75
                    }, {
                        queue: false,
                        duration: 'slow'
                    });




                },
                drag: function(event, ui) {
                    var offset = ui.offset;
                    currentDraggableItemOffsetLeft = offset.left;
                    currentDraggableItemOffsetTop = offset.top;
                },
                stop: function(event, ui) {
                    $(ui.helper).removeClass("DraggingItem");
                    $('#' + $(Parent.FieldsListElement).attr('id')).css('opacity', .75).animate({
                        opacity: 1
                    }, {
                        queue: false,
                        duration: 'slow'
                    });




                    Parent.getCertificateString();
                },
                cursor: 'move',
                containment: '#' + Parent.CertificateElement.id
            });


            $("#" + this.CertificateElement.id).droppable({
                accept: '.DraggableFieldDataElement',
                drop: function(event, ui) {
                    var id = $(ui.draggable).attr('id');
                    var name = $(ui.draggable).attr('name');
                    var fieldLabel = $(ui.draggable).text();
                    var fieldPlaceHolder = $(ui.draggable).attr('placeholder');
                    var certificateDraggableFieldElement = Parent.createDraggableElement(name, fieldLabel, fieldPlaceHolder, Parent.ImageLocation, null, ++draggableObjCount);
                    Parent.CertificateElement.appendChild(certificateDraggableFieldElement);
                    Parent.addDraggableToElement(certificateDraggableFieldElement.id, Parent.CertificateElement, Parent.currentDraggableElement);
                    Parent.addResizableToElement(certificateDraggableFieldElement.id, Parent.CertificateElement, Parent.currentDraggableElement);
                    Parent.setPosition(certificateDraggableFieldElement.id, Parent.CertificateElement, currentDraggableItemOffsetLeft, currentDraggableItemOffsetTop);
                    Parent.setHandlers(certificateDraggableFieldElement.id, Parent.CertificateElement, minDraggableFontSize, maxDraggableFontSize, Parent.currentDraggableElement);
                    Parent.setFormatters(certificateDraggableFieldElement.id, Parent.CertificateElement);
                    Parent.currentDraggableElement = certificateDraggableFieldElement.id;
                }
            });




            if (typeof(this.Layout.Certificate.Container) != 'undefined') {

                if ((typeof(this.Layout.Certificate.Container.Width) != 'undefined') && (typeof(this.Layout.Certificate.Container.Height) != 'undefined')) {
                    //this.ResizeElements(this.Layout.Certificate.Container.Width, this.Layout.Certificate.Container.Height, (this.CertificateWidth).replace("px", ""), (this.CertificateHeight).replace("px", ""), "DraggableCertificateElement");

                }

            }

            //this.addResizableToCertificate(this.CertificateElement.id);




            //this.displayDraggableElements(this.Layout,this.Additional);

            $(window).resize(function() {
                Parent.adjustPosition(Parent.FieldsListElement, Parent.CertificateElement);
            });


            $(document).bind('keydown', function(event) {
                Parent.handleActiveKeyboardKeys(event, Parent.currentDraggableElement, Parent.CertificateElement.id)
            });


            break;
        case "2":

            //Create the Container Element with the Image from Layout.
            this.setupContainer(this.Layout);

            this.CertificateElement = document.createElement("div");
            this.CertificateElement.id = "CertificateViewer"
            //this.CertificateElement.style.width = this.CertificateWidth;
            //this.CertificateElement.style.height = this.CertificateHeight;
            //this.CertificateElement.style.marginBottom = "75%";
            this.CertificateElement.style.position = "relative";

            this.DropElement.appendChild(this.CertificateElement)


            this.CertificateImage = document.createElement("img");
            this.CertificateImage.id = "CertificateViewerImage"
            this.CertificateImage.style.width = "100%";
            this.CertificateImage.style.height = "100%";
            this.CertificateImage.src = this.ImageSource;

            this.CertificateElement.appendChild(this.CertificateImage)

            /*Add to layout*/
            if (typeof(this.Layout.Certificate) != 'undefined') {

                if (typeof(this.Layout.Certificate.Elements) != 'undefined') {
                    for (existingCertNodeCount = 0; existingCertNodeCount < Object.keys(this.Layout.Certificate.Elements).length; existingCertNodeCount++) {
                        for (availableCertNodeCount = 0; availableCertNodeCount < Object.keys(this.Additional.AwardData).length; availableCertNodeCount++) {

                            if (this.Layout.Certificate.Elements[existingCertNodeCount].Identifier == this.Additional.AwardData[availableCertNodeCount].Identifier) {
                                name = this.Additional.AwardData[availableCertNodeCount].Identifier;
                                fieldValue = this.Additional.AwardData[availableCertNodeCount].Value;
                                certificateDraggableFieldElement = this.createStaticElement("view", name, fieldValue, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, ++draggableObjCount);
                                this.CertificateElement.appendChild(certificateDraggableFieldElement);
                                this.setPreviousPosition(certificateDraggableFieldElement.id, this.CertificateElement, this.Layout.Certificate.Elements[existingCertNodeCount].Left, this.Layout.Certificate.Elements[existingCertNodeCount].Top, this.Layout.Certificate.Elements[existingCertNodeCount].Width, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, this.Layout.Certificate.Elements[existingCertNodeCount].Alignment);
                            }

                        }

                        if (this.Layout.Certificate.Elements[existingCertNodeCount].IsStaticLabel == "true") {
                            name = this.Layout.Certificate.Elements[existingCertNodeCount].Identifier;
                            fieldValue = this.Layout.Certificate.Elements[existingCertNodeCount].Value;
                            certificateDraggableFieldElement = this.createStaticElement("view", name, fieldValue, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, ++draggableObjCount);
                            this.CertificateElement.appendChild(certificateDraggableFieldElement);
                            this.setPreviousPosition(certificateDraggableFieldElement.id, this.CertificateElement, this.Layout.Certificate.Elements[existingCertNodeCount].Left, this.Layout.Certificate.Elements[existingCertNodeCount].Top, this.Layout.Certificate.Elements[existingCertNodeCount].Width, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, this.Layout.Certificate.Elements[existingCertNodeCount].Alignment);
                        }


                    }

                }
            }



            if (typeof(this.Layout.Certificate.Container) != 'undefined') {




                if ((typeof(this.Layout.Certificate.Container.Width) != 'undefined') && (typeof(this.Layout.Certificate.Container.Height) != 'undefined')) {




                    var replaceCertWidth = String(this.CertificateWidth);
                    if (replaceCertWidth.indexOf("px") != -1) {
                        replaceCertWidth = (replaceCertWidth).replace("px", "");
                    }



                    var replaceCertHeight = String(this.CertificateHeight);
                    if (replaceCertHeight.indexOf("px") != -1) {
                        replaceCertHeight = (replaceCertHeight).replace("px", "");
                    }


                    this.ResizeElements(this.Layout.Certificate.Container.Width, this.Layout.Certificate.Container.Height, replaceCertWidth, replaceCertHeight, "StaticCertificateElement");

                }

            }




            Parent.ResizeElements(Parent.Layout.Certificate.Container.Width, Parent.Layout.Certificate.Container.Height, (String($("#" + Parent.CertificateElement.id).width())).replace("px", ""), (String($("#" + Parent.CertificateElement.id).height())).replace("px", ""), "StaticCertificateElement");




            $(window).resize(function() {




                var isInIframe = (window.location != window.parent.location) ? true : false;

                if (isInIframe && $("#" + Parent.DropElement.id).width() <= (Parent.CertificateWidth).replace("px", "")) {
                    $('body').width($(window).width());
                }
                Parent.ResizeElements(Parent.Layout.Certificate.Container.Width, Parent.Layout.Certificate.Container.Height, (String($("#" + Parent.CertificateElement.id).width())).replace("px", ""), (String($("#" + Parent.CertificateElement.id).height())).replace("px", ""), "StaticCertificateElement");

            });

            break;
        case "3":

            //Create the Container Element with the Image from Layout.
            this.setupContainer(this.Layout);

            this.CertificateElement = document.createElement("img");
            this.CertificateElement.id = "CertificatePrinter"
            this.CertificateElement.style.width = this.CertificateWidth;
            this.CertificateElement.style.height = this.CertificateHeight;

            this.CertificateElement.src = this.ImageSource;

            $("#" + this.DropElement.id).width(this.CertificateWidth);
            $("#" + this.DropElement.id).height(this.CertificateHeight);

            this.DropElement.appendChild(this.CertificateElement)

            /*Add to layout*/
            if (typeof(this.Layout.Certificate) != 'undefined') {

                if (typeof(this.Layout.Certificate.Elements) != 'undefined') {

                    for (existingCertNodeCount = 0; existingCertNodeCount < Object.keys(this.Layout.Certificate.Elements).length; existingCertNodeCount++) {
                        for (availableCertNodeCount = 0; availableCertNodeCount < Object.keys(this.Additional.AwardData).length; availableCertNodeCount++) {

                            if (this.Layout.Certificate.Elements[existingCertNodeCount].Identifier == this.Additional.AwardData[availableCertNodeCount].Identifier) {
                                name = this.Additional.AwardData[availableCertNodeCount].Identifier;
                                fieldValue = this.Additional.AwardData[availableCertNodeCount].Value;
                                certificateDraggableFieldElement = this.createStaticElement("print", name, fieldValue, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, ++draggableObjCount);
                                this.DropElement.appendChild(certificateDraggableFieldElement);
                                this.setPreviousPosition(certificateDraggableFieldElement.id, this.DropElement, this.Layout.Certificate.Elements[existingCertNodeCount].Left, this.Layout.Certificate.Elements[existingCertNodeCount].Top, this.Layout.Certificate.Elements[existingCertNodeCount].Width, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, this.Layout.Certificate.Elements[existingCertNodeCount].Alignment);
                            }

                        }

                        if (this.Layout.Certificate.Elements[existingCertNodeCount].IsStaticLabel == "true") {
                            name = this.Layout.Certificate.Elements[existingCertNodeCount].Identifier;
                            fieldValue = this.Layout.Certificate.Elements[existingCertNodeCount].Value;
                            certificateDraggableFieldElement = this.createStaticElement("print", name, fieldValue, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, ++draggableObjCount);
                            this.DropElement.appendChild(certificateDraggableFieldElement);
                            this.setPreviousPosition(certificateDraggableFieldElement.id, this.DropElement, this.Layout.Certificate.Elements[existingCertNodeCount].Left, this.Layout.Certificate.Elements[existingCertNodeCount].Top, this.Layout.Certificate.Elements[existingCertNodeCount].Width, this.Layout.Certificate.Elements[existingCertNodeCount].FontSize, this.Layout.Certificate.Elements[existingCertNodeCount].Alignment);
                        }




                    }



                }

            }

            if (typeof(this.Layout.Certificate.Container) != 'undefined') {

                if ((typeof(this.Layout.Certificate.Container.Width) != 'undefined') && (typeof(this.Layout.Certificate.Container.Height) != 'undefined')) {

                    var replaceCertWidth = String(this.CertificateWidth);
                    if (replaceCertWidth.indexOf("px") != -1) {
                        replaceCertWidth = (replaceCertWidth).replace("px", "");
                    }

                    var replaceCertHeight = String(this.CertificateHeight);
                    if (replaceCertHeight.indexOf("px") != -1) {
                        replaceCertHeight = (replaceCertHeight).replace("px", "");
                    }

                    this.ResizeElements(this.Layout.Certificate.Container.Width, this.Layout.Certificate.Container.Height, replaceCertWidth, replaceCertHeight, "StaticCertificateElement");

                }

            }




            jQuery(document).ready(function($) {
                $("img").load(function() {
                    setTimeout(function() {




                        if (document.execCommand('print', false, null) == false) {
                            window.print();




                        }

                        try {
                            if (!!navigator.platform.match(/iPhone|iPod|iPad/)) {

                            } else {
                                parent.jQuery.fancybox.close();
                            }
                        } catch (err) {

                            CertificateContainerElement.remove();

                        }
                    }, 400);
                });
            });

            break;
    }




}

CertificateObject.prototype.setupContainer = function(Layout) {

    // Setup Background Image / Height / Width
    if (typeof(Layout.Certificate) != 'undefined') {

        if (typeof(Layout.Certificate.Container) != 'undefined') {

            if (typeof(Layout.Certificate.Container.Width) != 'undefined') {
                this.OriginalCertificateWidth = Layout.Certificate.Container.Width;
            }

            if (typeof(Layout.Certificate.Container.Height) != 'undefined') {
                this.OriginalCertificateHeight = Layout.Certificate.Container.Height;
            }

            if (typeof(Layout.Certificate.Container.ImageSource) != 'undefined') {
                this.ImageSource = Layout.Certificate.Container.ImageSource;
            }

            if (typeof(Layout.Certificate.Container.FontSize) != 'undefined') {
                this.OriginalCertificateFontSize = Layout.Certificate.Container.FontSize;
            }

        }

    }

}

CertificateObject.prototype.ResizeElements = function(originalWidth, originalHeight, currentWidth, currentHeight, className) {



    var replaceCertWidth = String(currentWidth);
    if (replaceCertWidth.indexOf("px") != -1) {

        replaceCertWidth = (replaceCertWidth).replace("px", "");
    }


    var replaceOriginalCertWidth = String(originalWidth);
    if (replaceOriginalCertWidth.indexOf("px") != -1) {

        replaceOriginalCertWidth = (replaceOriginalCertWidth).replace("px", "");
    }


    $("." + className).each(function(index, item) {

        var originalFontSize = ($(this).find(".Content").attr("originalFontSize"));
        originalFontSize = originalFontSize.replace("em", "");

        var defaultFontSize = ($('body').css("font-size")).replace('px', '');

        //originalFontSize = (($(this).find(".Content").css("font-size")).replace('px', '')/defaultFontSize)*originalFontSize;
        var newFontSize = ((parseFloat(originalFontSize)) * (parseFloat(replaceCertWidth) / parseFloat(replaceOriginalCertWidth))) + "em";

        $(this).find(".Content").css("font-size", newFontSize);

    });

}

CertificateObject.prototype.adjustPosition = function(element, container) {

    if (parseFloat($("#" + element.id).css("left"), 10) + parseFloat($("#" + element.id).css("width"), 10) > parseFloat($("#" + container.id).parent().css("width"), 10)) {
        if (parseFloat($("#" + container.id).parent().css("width"), 10) == 0) {


            $("#" + element.id).animate({
                left: '10px'
            }, {
                queue: false,
                duration: 100
            });
        } else {

            $("#" + element.id).animate({
                left: (parseFloat($("#" + container.id).parent().css("width"), 10) - parseFloat($("#" + element.id).css("width"), 10) - 10) + "px"
            }, {
                queue: false,
                duration: 100
            });




        }
    }

}

CertificateObject.prototype.createStaticElement = function(mode, fieldName, fieldValue, originalFontSize, draggableObjCount) {
    var staticItem = document.createElement("div");
    staticItem.id = mode + "Static_" + (draggableObjCount);
    staticItem.className = "StaticCertificateElement";

    var Content = document.createElement("div");
    Content.id = "Content_" + (draggableObjCount);
    Content.className = "Content";
    Content.setAttribute('fieldName', fieldName);
    Content.setAttribute('originalFontSize', originalFontSize);
    Content.innerHTML = fieldValue;

    staticItem.appendChild(Content);

    return staticItem;
}

CertificateObject.prototype.createDraggableElement = function(fieldName, fieldLabel, fieldPlaceholder, imagePath, originalFontSize, draggableObjCount) {

    var Parent = this;

    var draggableItem = document.createElement("div");
    draggableItem.id = "Draggable_" + (draggableObjCount);
    draggableItem.className = "DraggableCertificateElement";

    var topFormatter = document.createElement("div");
    topFormatter.id = "FormatterWrapperTop_" + (draggableObjCount);
    topFormatter.className = "FormatterWrapperTop";

    var bottomFormatter = document.createElement("div");
    bottomFormatter.id = "FormatterWrapperBottom_" + (draggableObjCount);
    bottomFormatter.className = "FormatterWrapperBottom";

    var Formatter = document.createElement("div");
    Formatter.id = "Formatter";
    Formatter.className = "Formatter";

    var AlignLeft = document.createElement("img");
    AlignLeft.id = "AlignLeft";
    AlignLeft.src = imagePath + "alignLeft.png";

    var AlignCenter = document.createElement("img");
    AlignCenter.id = "AlignCenter";
    AlignCenter.src = imagePath + "alignCenter.png";

    var AlignRight = document.createElement("img");
    AlignRight.id = "AlignRight";
    AlignRight.src = imagePath + "alignRight.png";

    var IncreaseFontSize = document.createElement("img");
    IncreaseFontSize.id = "IncreaseFontSize";
    IncreaseFontSize.src = imagePath + "increaseSize.png";

    var DecreaseFontSize = document.createElement("img");
    DecreaseFontSize.id = "DecreaseFontSize";
    DecreaseFontSize.src = imagePath + "decreaseSize.png";

    var Remove = document.createElement("img");
    Remove.id = "Remove";
    Remove.src = imagePath + "remove.png";

    var Content = document.createElement("div");
    Content.id = "Content_" + (draggableObjCount);
    Content.className = "Content";
    Content.setAttribute('fieldName', fieldName);
    Content.setAttribute('placeHolder', fieldPlaceholder);
    Content.setAttribute('originalText', fieldLabel);
    var defaultFontSize = ($('body').css("font-size")).replace('px', '');
	var correctedOriginalCertificateFontSize = String(Parent.OriginalCertificateFontSize);
	
 	if (correctedOriginalCertificateFontSize.indexOf("px") != -1) {
		correctedOriginalCertificateFontSize = correctedOriginalCertificateFontSize.replace("px", "");
	}

	var correctedOriginalCertificateWidth = String(Parent.OriginalCertificateWidth);
	
 	if (correctedOriginalCertificateWidth.indexOf("px") != -1) {
		correctedOriginalCertificateWidth = correctedOriginalCertificateWidth.replace("px", "");
	}

	var correctedCertificateWidth = String(Parent.CertificateWidth);
	
 	if (correctedCertificateWidth.indexOf("px") != -1) {
		correctedCertificateWidth = correctedCertificateWidth.replace("px", "");
	}

    if (originalFontSize != null) {

        originalFontSize = originalFontSize.replace("em", "") * (correctedOriginalCertificateFontSize / defaultFontSize);


        originalFontSize = (originalFontSize) / (correctedOriginalCertificateWidth / correctedCertificateWidth) + "em";
        Content.setAttribute('originalFontSize', (originalFontSize));
    } else {

        Content.setAttribute('originalFontSize', (1) / ($('#' + currentContainer).width() / correctedCertificateWidth) + "em");
    }


    Content.innerHTML = fieldPlaceholder;

    Formatter.appendChild(AlignLeft);
    Formatter.appendChild(AlignCenter);
    Formatter.appendChild(AlignRight);
    Formatter.appendChild(IncreaseFontSize);
    Formatter.appendChild(DecreaseFontSize);
    Formatter.appendChild(Remove);

    topFormatter.appendChild(Formatter);
    bottomFormatter.appendChild(Formatter.cloneNode(true));

    draggableItem.appendChild(topFormatter);
    draggableItem.appendChild(Content);
    draggableItem.appendChild(bottomFormatter);

    return draggableItem;
}



CertificateObject.prototype.addDraggableToElement = function(draggableElementId, container, currentDraggableElement) {
    var Parent = this;
    $("#" + draggableElementId).draggable({
        stack: ".DraggableCertificateElement",
        handle: ".Content",
        containment: '#' + $(container).attr('id'),
        drag: function(event, ui) {
            var id = $(this).attr("id");
            var top = document.getElementById(id).style.top;

            if (top.indexOf("%") != -1) {
                top = top.replace("%", "") * (document.getElementById($(container).attr('id')).style.height).replace("px", "") / 100;

            } else if (top.indexOf("px") != -1) {
                top = top.replace("px", "")
            }

            if (top > 25) {
                $("#FormatterWrapperBottom_" + id.replace("Draggable_", "")).css("display", "none");
                $("#FormatterWrapperTop_" + id.replace("Draggable_", "")).css("display", "block");



            } else {
                $("#FormatterWrapperTop_" + id.replace("Draggable_", "")).css("display", "none");
                $("#FormatterWrapperBottom_" + id.replace("Draggable_", "")).css("display", "block");
            }
        },
        stop: function(event, ui) {
            Parent.getCertificateString();
            Parent.currentDraggableElement = draggableElementId;
        }
    });
}

CertificateObject.prototype.addResizableToCertificate = function(certificateElementId) {
    /*
    var Parent = this;
            $("#" + certificateElementId).resizable({
            handles: 'e',
            autoHide: true,
            resize: function (event, ui) { 
                Parent.LocationPercentElements( "DraggableCertificateElement", String($('#' + currentContainer).width()), String($('#' + currentContainer).height()) );
                Parent.ResizeElements(Parent.CertificateWidth, Parent.CertificateHeight, String($('#' + currentContainer).width()), String($('#' + currentContainer).height()), "DraggableCertificateElement");
                Parent.getCertificateString();


            },
            minWidth: 120,
            aspectRatio: 4 / 3,
            stop: function (event, ui) {


            }
        });
    */
}


CertificateObject.prototype.LocationPercentElements = function(className, currentWidth, currentHeight) {


    $("." + className).each(function(index, item) {

        var currentItemLeft = document.getElementById($(item).attr("id")).style.left;
        var currentItemTop = document.getElementById($(item).attr("id")).style.top;
        var currentItemWidth = document.getElementById($(item).attr("id")).style.width;


        if ((currentItemLeft).slice(-2) == 'px') {
            currentItemLeft = (currentItemLeft).replace('px', '');
            currentItemLeft = (currentItemLeft) / currentWidth * 100 + "%";
            document.getElementById($(item).attr("id")).style.left = currentItemLeft;
        }

        if ((currentItemTop).slice(-2) == 'px') {
            currentItemTop = (currentItemTop).replace('px', '');
            currentItemTop = (currentItemTop) / currentHeight * 100 + "%";
            document.getElementById($(item).attr("id")).style.top = currentItemTop;
        }
        if (currentItemWidth == "") {

            currentItemWidth = $(item).css("width");


        }
        if ((currentItemWidth).slice(-2) == 'px') {
            currentItemWidth = (currentItemWidth).replace('px', '');
            currentItemWidth = (currentItemWidth) / currentWidth * 100 + "%";
            document.getElementById($(item).attr("id")).style.width = currentItemWidth;
            document.getElementById($(item).find('.FormatterWrapperTop').attr("id")).style.width = '';
            document.getElementById($(item).find('.FormatterWrapperBottom').attr("id")).style.width = '';
            document.getElementById($(item).find('.Content').attr("id")).style.width = '';
        }



    });





}


CertificateObject.prototype.addResizableToElement = function(draggableElementId, container, currentDraggableElement) {
    var Parent = this;
    $("#" + draggableElementId).resizable({
        handles: 'e',
        autoHide: true,
        resize: function(event, ui) {
            var size = ui.size;
            var formatterWrapper = $(ui.element).find('.FormatterWrapperTop');
            $(formatterWrapper).css({
                "width": size.width
            });


            formatterWrapper = $(ui.element).find('.FormatterWrapperBottom');
            $(formatterWrapper).css({
                "width": size.width
            });


            formatterWrapper = $(ui.element).find('.Content');
            $(formatterWrapper).css({
                "width": size.width
            });


            var restackDraggableElements = $("#" + draggableElementId).data('ui-draggable');
            restackDraggableElements._mouseStart(event);
            restackDraggableElements._mouseDrag(event);
            restackDraggableElements._mouseStop(event);
        },
        containment: '#' + $(container).attr('id'),
        minWidth: 120,
        maxWidth: container.style.width,
        stop: function(event, ui) {
            Parent.getCertificateString();
            Parent.currentDraggableElement = draggableElementId;
        }
    });
}

CertificateObject.prototype.setPosition = function(draggableElementId, container, currentDraggableItemOffsetLeft, currentDraggableItemOffsetTop) {
    var droppableOffset = $('#' + $(container).attr('id')).offset();

    $("#" + draggableElementId).css({
        'left': currentDraggableItemOffsetLeft - droppableOffset.left,
        'top': currentDraggableItemOffsetTop - droppableOffset.top
    });


    this.getCertificateString();
}

CertificateObject.prototype.setLabelPosition = function(draggableElementId, container) {
    var Parent = this;
    var leftOffset = ($('#' + $(container).attr('id')).width() / 2);
    var topOffset = ($('#' + $(container).attr('id')).height() / 2);
    leftOffset = leftOffset - ($("#" + draggableElementId).width() / 2);
    topOffset = topOffset - ($("#" + draggableElementId).height() / 2);
    $("#" + draggableElementId).css({
        'left': leftOffset,
        'top': topOffset
    });



    Parent.currentDraggableElement = draggableElementId;
}

CertificateObject.prototype.setPreviousPosition = function(elementId, container, left, top, width, fontSize, alignment) {
    var defaultFontSize = ($('body').css("font-size")).replace('px', '');



    $("#" + elementId).css({
        'left': left,
        'top': top,
        'width': width
    });




    var replaceCertWidth = String(this.CertificateWidth);
    if (replaceCertWidth.indexOf("px") != -1) {

        replaceCertWidth = (replaceCertWidth).replace("px", "");
    }


    var replaceOriginalCertWidth = String(this.OriginalCertificateWidth);
    if (replaceOriginalCertWidth.indexOf("px") != -1) {

        replaceOriginalCertWidth = (replaceOriginalCertWidth).replace("px", "");
    }


    if (this.OriginalCertificateFontSize == (defaultFontSize + "px")) {

        fontSize = (fontSize.replace("em", "")) / (replaceOriginalCertWidth / replaceCertWidth) + "em";
    } else {


        if (this.OriginalCertificateFontSize != null) {

            fontSize = fontSize.replace("em", "") * ((this.OriginalCertificateFontSize).replace("px", "") / defaultFontSize);


        }
        fontSize = (fontSize) / (replaceOriginalCertWidth / replaceCertWidth) + "em";
    }
    $("#" + elementId).find(".Content").css({
        'font-size': fontSize,
        'text-align': alignment
    })



}


CertificateObject.prototype.setFormatters = function(draggableElementId, container) {
    var top = document.getElementById(draggableElementId).style.top;
    if (top.indexOf("%") != -1) {
        top = top.replace("%", "") * (document.getElementById($(container).attr('id')).style.height).replace("px", "") / 100;

    } else if (top.indexOf("px") != -1) {
        top = top.replace("px", "")
    }
    if (top > 25) {
        $("#FormatterWrapperBottom_" + draggableElementId.replace("Draggable_", "")).css("display", "none");
        $("#FormatterWrapperTop_" + draggableElementId.replace("Draggable_", "")).css("display", "block");

    } else {
        $("#FormatterWrapperTop_" + draggableElementId.replace("Draggable_", "")).css("display", "none");
        $("#FormatterWrapperBottom_" + draggableElementId.replace("Draggable_", "")).css("display", "block");
    }

}




CertificateObject.prototype.handleActiveKeyboardKeys = function(e, currentDraggableElement, currentContainerElement) {




    if (typeof this.currentDraggableElement != 'undefined') {
        var position;
        var draggable = $('#' + this.currentDraggableElement);
        var container = $('#' + currentContainerElement);
        var distance = 1;

        position = draggable.position();

        switch (e.keyCode) {

            case 37:
                position.left -= distance;
                break; // Left


            case 38:
                position.top -= distance;
                break; // Up


            case 39:
                position.left += distance;
                break; // Right


            case 40:
                position.top += distance;
                break; // Down


            default:
                return true; // Exit and bubble
        }


        if (position.left >= 0 && position.top >= 0 && position.left + draggable.width() <= container.width() && position.top + draggable.height() <= container.height()) {
            draggable.css(position);
            this.getCertificateString();
        }

        e.preventDefault();

    }

}

CertificateObject.prototype.setHandlers = function(draggableElementId, container, minDraggableFontSize, maxDraggableFontSize, currentDraggableElement) {

    var Parent = this;
    $("#" + draggableElementId + ' .Formatter > #AlignLeft').click(function() {
        $(this).parent().parent().parent().find(".Content").css("text-align", "left");
        Parent.getCertificateString();
    });

    $("#" + draggableElementId + ' .Formatter > #AlignCenter').click(function() {
        $(this).parent().parent().parent().find(".Content").css("text-align", "center");
        Parent.getCertificateString();
    });

    $("#" + draggableElementId + ' .Formatter > #AlignRight').click(function() {
        $(this).parent().parent().parent().find(".Content").css("text-align", "right");
        Parent.getCertificateString();
    });

    $("#" + draggableElementId + ' .Formatter > #IncreaseFontSize').click(function() {
        var fontSize = parseFloat($(this).parent().parent().parent().find(".Content").css("font-size").replace("px", ""));
        var defaultFontSize = ($('body').css("font-size")).replace('px', '');
        if (fontSize < maxDraggableFontSize) {
            $(this).parent().parent().parent().find(".Content").css("font-size", (++fontSize) + "px");


            $(this).parent().parent().parent().find(".Content").attr("originalFontSize", (fontSize / defaultFontSize) / ($('#' + currentContainer).width() / (Parent.CertificateWidth).replace("px", "")) + "em");


            $(this).parent().parent().parent().find(".FormatterWrapperBottom").css("top", ($(this).parent().parent().parent().find(".Content").outerHeight(true) + 3) + "px");
        }
        Parent.getCertificateString();
    });

    $("#" + draggableElementId + ' .Formatter > #DecreaseFontSize').click(function() {
        var fontSize = parseFloat($(this).parent().parent().parent().find(".Content").css("font-size").replace("px", ""));
        var defaultFontSize = ($('body').css("font-size")).replace('px', '');
        if (fontSize > minDraggableFontSize) {
            $(this).parent().parent().parent().find(".Content").css("font-size", (--fontSize) + "px");


            $(this).parent().parent().parent().find(".Content").attr("originalFontSize", (fontSize / defaultFontSize) / ($('#' + currentContainer).width() / (Parent.CertificateWidth).replace("px", "")) + "em");


            $(this).parent().parent().parent().find(".FormatterWrapperBottom").css("top", ($(this).parent().parent().parent().find(".Content").outerHeight(true) + 1) + "px");
        }
        Parent.getCertificateString();
    });

    $("#" + draggableElementId + ' .Formatter > #Remove').click(function() {
        $(this).parent().parent().parent().remove();
        if (draggableElementId == this.currentDraggableElement) {
            this.currentDraggableElement = "";
        }
        Parent.getCertificateString();
    });

    $("#" + draggableElementId).click(function(event) {
        Parent.currentDraggableElement = draggableElementId;
        var restackDraggableElements = $("#" + draggableElementId).data('ui-draggable');
        restackDraggableElements._mouseStart(event);
        restackDraggableElements._mouseDrag(event);
        restackDraggableElements._mouseStop(event);
    });

    $("#" + draggableElementId).find(".Content").hover(
        function() {
            //$("#" + draggableElementId).find(".FormatterWrapperBottom").css("top", ($("#" + draggableElementId).find(".Content").outerHeight(true) + 3) + "px");
            $("#" + draggableElementId).find(".Content").html($("#" + draggableElementId).find(".Content").attr("originaltext"));

        },
        function() {
            $("#" + draggableElementId).find(".Content").html($("#" + draggableElementId).find(".Content").attr("placeholder"));



        }
    );


}


CertificateObject.prototype.updateCertificateImageString = function(newImageLocation) {




    var existingJSONInstanceData = jQuery.parseJSON(document.getElementById(this.SaveElement.id).value);

    var existingSTRINGInstanceData = document.getElementById(this.SaveElement.id).value;

    existingSTRINGInstanceData = existingSTRINGInstanceData.replace(existingJSONInstanceData.Certificate.Container.ImageSource, newImageLocation);

    document.getElementById(this.SaveElement.id).value = existingSTRINGInstanceData;




}


CertificateObject.prototype.getCertificateString = function() {


    var defaultFontSize = ($('body').css("font-size")).replace('px', '');
    if (isNaN(defaultFontSize)) {

        defaultFontSize = 16;
    }
    var d = new Date();
    var n = d.getMilliseconds();
    //console.log(n)
    var certificateString = '{"Certificate" : { ';
    var certificateFieldString = '';
    var certificateLabelString = '';
    var droppableOffset = $('#' + currentContainer).offset();
    var certificateWidth = $('#' + currentContainer).width();
    var certificateHeight = $('#' + currentContainer).height();
    var fieldNumber = 0;
    var labelNumber = 0
    var certificateImage = "";

    certificateImage = $('#' + currentContainer).css("backgroundImage");

    if (certificateImage == "" || certificateImage == "none") {


    } else {
        certificateImage = certificateImage.replace(/^url\(["']?/, '').replace(/["']?\)$/, '').replace(/\"/gi, "").replace(window.location.protocol + "//", "").replace(window.location.host, "");
    }

    certificateString += '"Container" : { ';

    certificateString += '"Width" : "' + certificateWidth + 'px", ';

    certificateString += '"Height" : "' + certificateHeight + 'px", ';

    certificateString += '"FontSize" : "' + defaultFontSize + 'px", ';

    certificateString += '"ImageSource": "' + certificateImage + '" }';


    $(".DraggableCertificateElement").each(function(index, item) {

        var draggableOffset = $(item);
        var left;
        var top;

        if ((draggableOffset.css("left")).slice(-2) == 'px') {
            left = (draggableOffset.css("left")).replace('px', '');
        }

        if ((draggableOffset.css("top")).slice(-2) == 'px') {
            top = (draggableOffset.css("top")).replace('px', '');
        }

        var left = (left) / certificateWidth * 100 + "%";
        var top = (top) / certificateHeight * 100 + "%";

        var text = $(item).find(".Content").text().trim();


        var fontSize = $(item).find(".Content").css("font-size");
        var textAlign = $(item).find(".Content").css("text-align");
        var width = $(item).css("width");
        var fieldName = $(item).find(".Content").attr('fieldName').toString();

        if (fontSize.slice(-2) == 'px') {
            //original font size in px * conversion for % / 100 which is the conversion to em			
            fontSize = (parseFloat(fontSize) / defaultFontSize) + 'em';

        } else if (fontSize.slice(-1) == '%') {

            fontSize = (parseFloat(fontSize) / 100) + 'em';


        } else {
            fontSize = fontSize
        }


        if (width.slice(-2) == 'px') {

            width = (parseFloat(width) / certificateWidth) * 100 + "%";
        }

        if (text.trim().length > 0) {
            fieldNumber += 1;
            var certificateFieldInfoString = ""




            if (fieldName.indexOf("Label") >= 0) {
                labelNumber += 1;
                certificateFieldInfoString = '{"Identifier": "Label_' + labelNumber + '", ';
                certificateFieldInfoString += '"Value" : "' + text.replace(/\\/g, "\\\\").replace(/"/g, '\\"').replace(/\t+/g, "") + '", ';
                certificateFieldInfoString += '"IsStaticLabel" : "true", ';





            } else {
                certificateFieldInfoString = '{"Identifier": "' + fieldName + '", ';
                certificateFieldInfoString += '"Value" : "", ';
                certificateFieldInfoString += '"IsStaticLabel" : "false", ';
            }

            certificateFieldInfoString += '"Top": "' + top + '", '
            certificateFieldInfoString += '"Left": "' + left + '", '
            certificateFieldInfoString += '"Width": "' + width + '", '
            certificateFieldInfoString += '"FontSize": "' + fontSize + '", '
            certificateFieldInfoString += '"Alignment": "' + textAlign + '"} ';




            if (fieldNumber > 1) {
                certificateFieldString += ", " + certificateFieldInfoString;




            } else {
                certificateFieldString += certificateFieldInfoString;
            }
        }
    });

    if (fieldNumber >= 1) {

        certificateString += ', "Elements": [' + certificateFieldString + ']';

    } else {}


    certificateString += '}}';


    //escape JSON Special Characters
    document.getElementById(this.SaveElement.id).value = certificateString;


    d = new Date();
    n = d.getMilliseconds();
    //console.log(certificateString)
    //console.log(n)

    return certificateString;

}