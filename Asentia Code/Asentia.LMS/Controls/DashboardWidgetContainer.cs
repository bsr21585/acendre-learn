﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Asentia.Common;
using System.Web;
using System.Web.Services;
using drawing = System.Drawing;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;
using Asentia.LRS.Library;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace Asentia.LMS.Controls
{
    public class DashboardWidgetContainer : WebControl
    {
        #region _Widget Class
        private class _Widget
        {
            #region Constructor
            public _Widget(string id, string headerTitle, Action<string, bool> builderMethod, string headerIconPath, string widgetBarIconPath, string tooltiptext, bool refreshOnDrop)
            {
                this.Id = id;
                this.HeaderTitle = headerTitle;
                this.BuilderMethod = builderMethod;
                this.HeaderIconPath = headerIconPath;
                this.WidgetBarIconPath = widgetBarIconPath;
                this.TooltipText = tooltiptext;
                this.RefreshOnDrop = refreshOnDrop;
            }
            #endregion

            #region Properties
            /// <summary>
            /// The widget id.
            /// </summary>
            public string Id;

            /// <summary>
            /// The title of the widget that goes into the widget header.
            /// </summary>
            public string HeaderTitle;

            /// <summary>
            /// Delegate method that represents the method used to build
            /// the content of the widget.
            /// </summary>
            public Action<string, bool> BuilderMethod;

            /// <summary>
            /// The path to the icon that goes into the widget header.
            /// </summary>
            public string HeaderIconPath;

            /// <summary>
            /// The path to the icon that goes into the widget bar to
            /// represent the widget.
            /// </summary>
            public string WidgetBarIconPath;

            /// <summary>
            /// The tooltip text
            /// </summary>
            public string TooltipText;

            /// <summary>
            /// Should the widget content refresh after being dragged and
            /// dropped to a different position on the dashboard?
            /// 
            /// Note: This is necessary for widgets containing analytics
            /// as the drag and drop action causes the widget to lose its
            /// <canvas> content.
            /// </summary>
            public bool RefreshOnDrop;
            #endregion
        }
        #endregion

        #region Constructor
        public DashboardWidgetContainer()
        { ;}
        #endregion

        #region Private Properties
        /// <summary>
        /// An instance of AsentiaPage so that we can access AsentiaPage methods.
        /// </summary>
        private AsentiaPage _AsentiaPageInstance;

        /// <summary>
        /// A user object to get logged in user's information.
        /// </summary>
        private User _UserObject;

        /// <summary>
        /// Ecommerce settings object to get the portal's ecommerce settings.
        /// </summary>
        private EcommerceSettings _EcommerceSettings;

        /// <summary>
        /// An ArrayList that holds the widgets.
        /// </summary>
        private ArrayList _Widgets = new ArrayList();

        /// <summary>
        /// The name of the file that contains the dashboard configuration for an Administrator
        /// dashboard. Path to the file will either be to a user's specific file (if exists) or
        /// the default file.
        /// </summary>
        private const string _ADMINISTRATOR_DASHBOARD_CONFIG_FILE = "AdministratorDashboard.xml";

        /// <summary>
        /// The name of the file that contains the dashboard configuration for a Learner
        /// dashboard. Path to the file will either be to a user's specific file (if exists)
        /// or the default file.
        /// </summary>
        private const string _LEARNER_DASHBOARD_CONFIG_FILE = "LearnerDashboard.xml";

        /// <summary>
        /// The name of the file that contains the dashboard configuration for a Reporter
        /// dashboard. Path to the file will either be to a user's specific file (if exists)
        /// or the default file.
        /// </summary>
        private const string _REPORTER_DASHBOARD_CONFIG_FILE = "ReporterDashboard.xml";

        /// <summary>
        /// The name of the file that contains the dashboard configuration for a new combined
        /// dashboard. Path to the file will either be to a user's specific file (if exists)
        /// or the default file. This file will gradually replace the old 3 separated dashboard files
        /// </summary>
        private const string _DASHBOARD_CONFIG_FILE = "Dashboard.xml";

        /// <summary>
        /// The identifier of our static transcript widget for the learner dashboard.
        /// </summary>
        /// 
        private const string _TRANSCRIPT_WIDGET_ID = "TranscriptWidget";

        /// <summary>
        /// The identifier of our static enrollments widget for the learner dashboard.
        /// </summary>
        private const string _ENROLLMENT_WIDGET_ID = "EnrollmentsWidget";

        /// <summary>
        /// The identifier of our static Calendar widget for the learner dashboard.
        /// </summary>
        private const string _CALENDAR_WIDGET_ID = "CalendarWidget";

        /// <summary>
        /// The identifier of our static Wall Moderation widget for the admin dashboard.
        /// </summary>
        private const string _WALL_MODERATION_WIDGET_ID = "WallModerationWidget";

        /// <summary>
        /// The identifier of our static Task Proctoring widget for the admin dashboard.
        /// </summary>
        private const string _TASK_PROCTORING_WIDGET_ID = "TaskProctoringWidget";

        /// <summary>
        /// The identifier of our static OJT Proctoring widget for the admin dashboard.
        /// </summary>
        private const string _OJT_PROCTORING_WIDGET_ID = "OJTProctoringWidget";

        /// <summary>
        /// The identifier of our static ILT Roster Management widget for the admin dashboard.
        /// </summary>
        private const string _ILT_ROSTER_MANAGEMENT_WIDGET_ID = "ILTRosterManagementWidget";

        /// <summary>
        /// The identifier of our static User Registration Approval Widget for the admin dashboard.
        /// </summary>
        private const string _USER_REGISTRATION_APPROVAL_WIDGET_ID = "UserRegistrationApprovalWidget";

        /// <summary>
        /// The identifier of our static Self Enrollment Approval Widget for the admin dashboard.
        /// </summary>
        private const string _SELF_ENROLLMENT_APPROVAL_WIDGET_ID = "SelfEnrollmentApprovalWidget";

        /// <summary>
        /// The identifier of our static ITL Sessions Widget for the learner dashboard.
        /// </summary>
        private const string _ITL_SESSIONS_WIDGET = "ILTSessionsWidget";

        /// <summary>
        /// The identifier of our static Administrative Tasks Widget for the admin dashboard
        /// </summary>
        private const string _ADMINISTRATIVE_TASKS_WIDGET_ID = "AdministrativeTasksWidget";

        /// <summary>
        /// The help icon text just like message center menu
        /// </summary>
        private const string _ALERT_ICON_TEXT = "!";
        
        /* PROPERTIES FOR CERTIFICATES WIDGET */

        /// <summary>
        /// View certificate hidden trigger button.
        /// </summary>
        private Button _CertificateViewerModalHiddenLaunchButton;

        /// <summary>
        /// Hidden field to store the certificate id for certificate export to pdf.
        /// </summary>
        private HiddenField _ExportCertificateToPdfHiddenCertificateId;

        /// <summary>
        /// Hidden field to store the certificate record id for certificate export to pdf.
        /// </summary>
        private HiddenField _ExportCertificateToPdfHiddenCertificateRecordId;

        /// <summary>
        /// Export certificate to PDF hidden trigger button.
        /// </summary>
        private Button _ExportCertificateToPdfHiddenButton;

        /// <summary>
        /// String of certificate data for PDF document.
        /// </summary>
        private string _CertificatePdfString;

        /* PROPERTIES FOR TRANSCRIPT WIDGET */        

        /// <summary>
        /// Transcript viewer modal hidden trigger button.
        /// </summary>
        private Button _TranscriptViewerModalHiddenLaunchButton;
        
        /// <summary>
        /// Pop Up Modal for the report 
        /// </summary>
        private ModalPopup _ReportModal;

        /// <summary>
        /// Transcript Full View Link
        /// </summary>
        private LinkButton _OpenTranscriptLink;

        /// <summary>
        /// TransactionItemsGrid for the User Receipt
        /// </summary>
        private Grid _TransactionItemsGrid;

        /* PROPERTIES FOR PURCHASES WIDGET */


        /* PROPERTIES FOR LEADERBOARD ADMINISTRATOR WIDGET */

        /// <summary>
        /// Hidden field that stores the type of leaderboard to render for the Leaderboards Administrator Widget. 
        /// This just sits in the codebehind to be able to pass the selected leaderboard type to the rendering function
        /// for the widget. This does not get attached to instansiating page's controls. 
        /// </summary>
        private HiddenField _LeaderboardsAdministratorLeaderboardType = new HiddenField();

        /// <summary>
        /// Hidden field that stores the start type that the leaderboard should render data for, for the selected leaderboard
        /// type on the Leaderboards Administrator Widget.
        /// This just sits in the codebehind to be able to pass the selected leaderboard start type to the rendering function
        /// for the widget. This does not get attached to instansiating page's controls. 
        /// </summary>
        private HiddenField _LeaderboardsAdministratorLeaderboardStartType = new HiddenField();

        /* PROPERTIES FOR LEADERBOARD LEARNER WIDGET */

        /// <summary>
        /// Hidden field that stores the type of leaderboard to render for the Leaderboards Learner Widget. 
        /// This just sits in the codebehind to be able to pass the selected leaderboard type to the rendering function
        /// for the widget. This does not get attached to instansiating page's controls. 
        /// </summary>
        private HiddenField _LeaderboardsLearnerLeaderboardType = new HiddenField();

        /// <summary>
        /// Hidden field that stores the start type that the leaderboard should render data for, for the selected leaderboard
        /// type on the Leaderboards Learner Widget.
        /// This just sits in the codebehind to be able to pass the selected leaderboard start type to the rendering function
        /// for the widget. This does not get attached to instansiating page's controls. 
        /// </summary>
        private HiddenField _LeaderboardsLearnerLeaderboardStartType = new HiddenField();

        /// <summary>
        /// Hidden field that stores the "relative to" filter that the leaderboard should render data for, for the selected leaderboard
        /// type on the Leaderboards Learner Widget.
        /// This just sits in the codebehind to be able to pass the selected leaderboard "relative to" filter to the rendering function
        /// for the widget. This does not get attached to instansiating page's controls. 
        /// </summary>
        private HiddenField _LeaderboardsLearnerLeaderboardRelativeTo = new HiddenField();

        /* PROPERTIES FOR PENDING USER REGISTRATIONS WIDGET */

        /// <summary>
        /// Approve/Reject pending user registration modal hidden trigger button.
        /// </summary>
        private Button _ApproveRejectPendingUserRegistrationModalHiddenLaunchButton;

        /// <summary>
        /// Hidden field to store the user id and action (approve or reject) for pending user registrations widget.
        /// </summary>
        private HiddenField _ApproveRejectPendingUserRegistrationData;

        /// <summary>
        /// Modal popup for pending user registration approval or rejection.
        /// </summary>
        private ModalPopup _ApproveRejectPendingUserRegistrationModal;

        /* PROPERTIES FOR PENDING COURSE ENROLLMENTS WIDGET */

        /// <summary>
        /// Approve/Reject pending course enrollment modal hidden trigger button.
        /// </summary>
        private Button _ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton;

        /// <summary>
        /// Hidden field to store the user id and action (approve or reject) for pending course enrollments widget.
        /// </summary>
        private HiddenField _ApproveRejectPendingCourseEnrollmentData;

        /// <summary>
        /// Modal popup for pending course enrollment approval or rejection.
        /// </summary>
        private ModalPopup _ApproveRejectPendingCourseEnrollmentModal;

        /* PROPERTIES FOR ILT SESSIONS WIDGET */

        /// <summary>
        /// Hidden launch button for the ILT Sessions Modal Popup
        /// </summary>
        private Button _ILTSessionsModalHiddenLaunchButton;

        /// <summary>
        /// Hidden load button fo the ILT Sessions Modal
        /// </summary>
        private Button _ILTSessionsModalHiddenLoadButton;

        /// <summary>
        /// Hidden field to hold the ILT Session ID
        /// </summary>
        private HiddenField _ILTSessionsData;

        /// <summary>
        /// Hidden field to hold the session title for the modal
        /// </summary>
        private HiddenField _ILTSessionsTitle;

        /// <summary>
        /// ILT Sessions Modal Popup
        /// </summary>
        private ModalPopup _ILTSessionsModal;

        /// <summary>
        /// Details panel for the ILT Sessions Modal Popup
        /// </summary>
        private Panel _ILTSessionsModalDetailsPanel;

        /// <summary>
        /// Content for downloading the ILT Session Calendar File
        /// </summary>
        private HiddenField _ILTCalendarContent = new HiddenField();

        /// <summary>
        /// Hidden button to download the ILT Session Calendar File
        /// </summary>
        private Button _HiddenILTDownloadCalendarFileButton = new Button();
        #endregion

        #region Private Methods
        #region _InitializeWidgets
        /// <summary>
        /// Initializes all of the widgets that go into the dashboard 
        /// </summary>
        /// <remarks>
        /// All widgets are declared and added to the array list. 
        /// To build a widget, all you need to do is declare it, and create its
        /// builder method in this class.
        /// </remarks>
        private void _InitializeWidgets()
        {
            // get logged in user information.
            this._UserObject = new User(AsentiaSessionState.IdSiteUser);

            // get ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // To add a widget, add its widget information to the _Widgets ArrayList. 
            // Then, add its builder method to this class.

            // WIDGETS FOR LEARNER USERS ONLY (NOT ADMIN)

            if (AsentiaSessionState.IdSiteUser > 1) 
            {

                // Enrollments Widget
                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WIDGET_LEARNER_ENROLLMENTS_MODE) == "list")
                {
                    // builds the enrollment widget in list mode
                    this._Widgets.Add(new _Widget(_ENROLLMENT_WIDGET_ID,
                                                _GlobalResources.Enrollments,
                                                _BuildEnrollmentsWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG),
                                                _GlobalResources.Enrollments,
                                                false));
                }
                else
                {
                    // builds the enrollment widget in tile mode
                    this._Widgets.Add(new _Widget(_ENROLLMENT_WIDGET_ID,
                                                _GlobalResources.Enrollments,
                                                _BuildEnrollmentsTileWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG),
                                                _GlobalResources.Enrollments,
                                                false));
                }

                // Calendar Widget
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_CALENDAR_ENABLED))
                {
                    this._Widgets.Add(new _Widget(_CALENDAR_WIDGET_ID,
                                                _GlobalResources.Calendar,
                                                _BuildCalendarWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR, ImageFiles.EXT_PNG),
                                                _GlobalResources.Calendar,
                                                false));
                }

                // Feed Widget
                if (((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_FEED_ENABLED)) && 
                    ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE)) &&
                    ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE)))
                {
                    this._Widgets.Add(new _Widget("FeedWidget",
                                                _GlobalResources.DiscussionFeed,
                                                _BuildFeedWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_FEED, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_FEED, ImageFiles.EXT_PNG),
                                                _GlobalResources.DiscussionFeed,
                                                false));
                }

                // Purchases Widget
                if (this._EcommerceSettings.IsEcommerceSetAndVerified && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_PURCHASES_ENABLED))
                {
                    this._Widgets.Add(new _Widget("PurchasesWidget",
                                                _GlobalResources.Purchases,
                                                _BuildPurchasesWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_RECEIPT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_RECEIPT, ImageFiles.EXT_PNG),
                                                _GlobalResources.Purchases,
                                                false));
                }

                // Certificates Widget
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_CERTIFICATES_ENABLED))
                {
                    this._Widgets.Add(new _Widget("CertificatesWidget",
                                                _GlobalResources.Certificates,
                                                _BuildCertificatesWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG),
                                                _GlobalResources.Certificates,
                                                false));

                    // attach the hidden trigger button(s) and input(s) for the modal(s)
                    this._CertificateViewerModalHiddenLaunchButton = new Button();
                    this._CertificateViewerModalHiddenLaunchButton.ID = "CertificateViewerModalHiddenLaunchButton";
                    this._CertificateViewerModalHiddenLaunchButton.Style.Add("display", "none");

                    this._ExportCertificateToPdfHiddenCertificateId = new HiddenField();
                    this._ExportCertificateToPdfHiddenCertificateId.ID = "ExportCertificateToPdfHiddenCertificateId";

                    this._ExportCertificateToPdfHiddenCertificateRecordId = new HiddenField();
                    this._ExportCertificateToPdfHiddenCertificateRecordId.ID = "ExportCertificateToPdfHiddenCertificateRecordId";

                    this._ExportCertificateToPdfHiddenButton = new Button();
                    this._ExportCertificateToPdfHiddenButton.ID = "ExportCertificateToPdfHiddenButton";
                    this._ExportCertificateToPdfHiddenButton.Style.Add("display", "none");
                    this._ExportCertificateToPdfHiddenButton.Command += new CommandEventHandler(this._ExportCertificateToPDFButton_Command);

                    this.Controls.Add(this._CertificateViewerModalHiddenLaunchButton);
                    this.Controls.Add(this._ExportCertificateToPdfHiddenCertificateId);
                    this.Controls.Add(this._ExportCertificateToPdfHiddenCertificateRecordId);
                    this.Controls.Add(this._ExportCertificateToPdfHiddenButton);

                    // build modal(s)
                    this._BuildViewCertificateModal(this._CertificateViewerModalHiddenLaunchButton.ID, this);
                }

                // Transcript Widget
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_TRANSCRIPT_ENABLED))
                {
                    this._Widgets.Add(new _Widget("TranscriptWidget",
                                                _GlobalResources.Transcript,
                                                _BuildTranscriptWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_TRANSCRIPT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_TRANSCRIPT, ImageFiles.EXT_PNG),
                                                _GlobalResources.Transcript,
                                                false));

                    // attach the hidden trigger button(s) and input(s) for the modal(s)
                    this._TranscriptViewerModalHiddenLaunchButton = new Button();
                    this._TranscriptViewerModalHiddenLaunchButton.ID = "TranscriptViewerModalHiddenLaunchButton";
                    this._TranscriptViewerModalHiddenLaunchButton.Style.Add("display", "none");

                    this.Controls.Add(this._TranscriptViewerModalHiddenLaunchButton);

                    // build modal(s)
                    this._BuildViewReportModal(this);
                }

                // MyCommunities Widget
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_MYCOMMUNITIES_ENABLED) && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
                {
                    this._Widgets.Add(new _Widget("MyCommunitiesWidget",
                                                _GlobalResources.MyCommunities,
                                                _BuildMyCommunitiesWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG),
                                                _GlobalResources.MyCommunities,
                                                false));
                }

                // MyLearningPaths Widget

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE)
                    && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_MYLEARNINGPATHS_ENABLED))
                {
                    this._Widgets.Add(new _Widget("MyLearningPathsWidget",
                            _GlobalResources.MyLearningPaths,
                            _BuildMyLearningPathsWidget,
                            ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG),
                            ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG),
                            _GlobalResources.MyLearningPaths,
                            false));
                }                     

                // Leaderboards Widget (LEARNER)
                if ((!(bool)this._UserObject.ExcludeFromLeaderboards)
                    && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_LEADERBOARDS_ENABLED)
                    && ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSETOTAL_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSECREDITS_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATETOTAL_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATECREDITS_ENABLE)))
                {
                    this._Widgets.Add(new _Widget("LeaderboardsLearnerWidget",
                                                _GlobalResources.MyLeaderboardsTop10,
                                                _BuildLeaderboardsLearnerWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD, ImageFiles.EXT_PNG),
                                                _GlobalResources.MyLeaderboardsTop10,
                                                false));
                }

                // MyCertifications Widget
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE)
                    && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_MYCERTIFICATIONS_ENABLED))
                {
                    this._Widgets.Add(new _Widget("MyCertificationsWidget",
                                                _GlobalResources.MyCertifications,
                                                _BuildMyCertificationsWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG),
                                                _GlobalResources.MyCertifications,
                                                false));
                }

                // Documents Widget
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_DOCUMENTS_ENABLED))
                {
                    this._Widgets.Add(new _Widget("DocumentsWidget",
                                                _GlobalResources.Documents,
                                                _BuildDocumentsWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_DOCUMENT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_DOCUMENT, ImageFiles.EXT_PNG),
                                                _GlobalResources.Documents,
                                                false));
                }

                // ILT Sessions Widget
                if((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_ILTSESSIONS_ENABLED))
                {
                    this._Widgets.Add(new _Widget("ILTSessionsWidget",
                                                _GlobalResources.InstructorLedTrainingSessions,
                                                _BuildILTSessionsWidget,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_SESSION, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_SESSION, ImageFiles.EXT_PNG),
                                                _GlobalResources.InstructorLedTrainingSessions,
                                                false));

                    // attach the hidden trigger buttons and inputs for the modal
                    this._ILTSessionsModalHiddenLaunchButton = new Button();
                    this._ILTSessionsModalHiddenLaunchButton.ID = "ILTSessionsModalHiddenLaunchButton";
                    this._ILTSessionsModalHiddenLaunchButton.Style.Add("display", "none");

                    this.Controls.Add(this._ILTSessionsModalHiddenLaunchButton);

                    this._ILTSessionsData = new HiddenField();
                    this._ILTSessionsData.ID = "ILTSessionsData";

                    this.Controls.Add(this._ILTSessionsData);

                    this._ILTSessionsTitle = new HiddenField();
                    this._ILTSessionsTitle.ID = "ILTSessionsTitle";

                    this.Controls.Add(this._ILTSessionsTitle);

                    // hidden button to download calendar file
                    this._HiddenILTDownloadCalendarFileButton.ID = "HiddenILTDownloadCalendarFileButton";
                    this._HiddenILTDownloadCalendarFileButton.ClientIDMode = ClientIDMode.Static;
                    this._HiddenILTDownloadCalendarFileButton.Style.Add("display", "none");
                    this._HiddenILTDownloadCalendarFileButton.Command += this._DownloadILTSessionCalendarFile;

                    this.Controls.Add(this._HiddenILTDownloadCalendarFileButton);
                    this._ILTCalendarContent.ID = "ILTCalendarContent";
                    this.Controls.Add(this._ILTCalendarContent);

                    // build modal(s)
                    this._BuildILTSessionsModal(this._ILTSessionsModalHiddenLaunchButton.ID, this);
                }
             }

            // WIDGETS FOR ALL USERS, EVEN "ADMIN"

            // Enrollment Statistics Widget
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_ENROLLMENTSTATISTICS_ENABLED)
                && (
                    AsentiaSessionState.IdSiteUser == 1
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager) // enrollment statistics widget
                    )
                )
            {
                this._Widgets.Add(new _Widget("EnrollmentStatisticsWidget",
                                            _GlobalResources.EnrollmentStatistics,
                                            _BuildEnrollmentStatisticsWidget,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_WIDGET_ANALYTICS, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_WIDGET_ANALYTICS, ImageFiles.EXT_PNG),
                                            _GlobalResources.EnrollmentStatistics,
                                            true));
            }

            // Team Leaderboards Widget (ADMINISTRATOR)
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_LEADERBOARDS_ENABLED)
                && (
                    (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSETOTAL_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSECREDITS_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATETOTAL_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATECREDITS_ENABLE)
                    )
                && (
                    AsentiaSessionState.IdSiteUser == 1
                    || (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_Leaderboards) && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager))  // leaderboards widget
                    )
                )
            {
                this._Widgets.Add(new _Widget("LeaderboardsAdministratorWidget",
                                            _GlobalResources.TeamLeaderboardsTop10,
                                            _BuildLeaderboardsAdministratorWidget,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD, ImageFiles.EXT_PNG),
                                            _GlobalResources.TeamLeaderboardsTop10,
                                            false));
            }

            // Wall Moderation Widget
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_WALLMODERATION_ENABLED)
                && 
                (
                    (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE)
                    && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE))
                &&
                (
                    AsentiaSessionState.IdSiteUser == 1
                    || AsentiaSessionState.IsUserAWallModerator // wall moderation widget
                    )
                )
            {
                this._Widgets.Add(new _Widget(_WALL_MODERATION_WIDGET_ID,
                                            _GlobalResources.DiscussionModeration,
                                            _BuildWallModerationWidget,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG),
                                            _GlobalResources.DiscussionModeration,
                                            false));
            }

            /************** THIS WIDGET DATA IS NOW PART OF ADMINISTRATIVE TASKS WIDGET*******************************************
            // Task Proctoring Widget
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_TASKPROCTORING_ENABLED)
                && (
                    AsentiaSessionState.IdSiteUser == 1
                    || AsentiaSessionState.IsUserACourseExpert // ojt and task proctoring widgets
                    || AsentiaSessionState.IsUserASupervisor // ojt and task proctoring widgets
                    )
                )
            {
                this._Widgets.Add(new _Widget(_TASK_PROCTORING_WIDGET_ID,
                                            _GlobalResources.TaskProctoring,
                                            _BuildTaskProctoringWidget,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_TASK, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_TASK, ImageFiles.EXT_PNG),
                                            _GlobalResources.TaskProctoring,
                                            false));
            }
            ***********************************************************************************************************************/

            /************** THIS WIDGET DATA IS NOW PART OF ADMINISTRATIVE TASKS WIDGET*******************************************
            // OJT Proctoring Widget
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_OJTPROCTORING_ENABLED)
                && (
                    AsentiaSessionState.IdSiteUser == 1
                    || AsentiaSessionState.IsUserACourseExpert // ojt and task proctoring widgets
                    || AsentiaSessionState.IsUserASupervisor // ojt and task proctoring widgets
                    )
                )
            {
                this._Widgets.Add(new _Widget(_OJT_PROCTORING_WIDGET_ID,
                                            _GlobalResources.OJTProctoring,
                                            _BuildOJTProctoringWidget,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_OJT, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_OJT, ImageFiles.EXT_PNG),
                                            _GlobalResources.OJTProctoring,
                                            false));
            }
             ***********************************************************************************************************************/

            // ILT Roster Management Widget
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_ILTROSTERMANAGEMENT_ENABLED)
                && (
                    AsentiaSessionState.IdSiteUser == 1
                    || AsentiaSessionState.IsUserAnILTInstructor // ilt roster management widget
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_InstructorLedTrainingManager) // ilt roster management widget
                    )
                )
            {
                this._Widgets.Add(new _Widget(_ILT_ROSTER_MANAGEMENT_WIDGET_ID,
                                            _GlobalResources.ILTRosterManagement,
                                            _BuildILTRosterManagementWidget,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG),
                                            _GlobalResources.ILTRosterManagement,
                                            false));
            }
                   
            // Report Subscriptions Widget
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_REPORTER_REPORTSUBSCRIPTIONS_ENABLED)
                && AsentiaSessionState.IdSiteUser > 1 // only shown on regular user view
                && (
                    AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_CatalogAndCourseInformation)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Certificates)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserDemographics)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCourseTranscripts)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserLearningPathTranscripts)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserInstructorLedTrainingTranscripts)
                    //|| AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_XAPI)
                    || (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Purchases) && this._EcommerceSettings.IsEcommerceSetAndVerified)
                    || (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts) && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                    )
                ) 
            {
                this._Widgets.Add(new _Widget("ReportSubscriptionsWidget",
                                            _GlobalResources.MyReportSubscriptions,
                                            _BuildReportSubscriptionsWidget,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG),
                                            _GlobalResources.MyReportSubscriptions,
                                            false));
            }

            // Report Shortcuts Widget
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_REPORTER_REPORTSHORTCUTS_ENABLED)
                && (
                    AsentiaSessionState.IdSiteUser == 1
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_CatalogAndCourseInformation)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Certificates)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserDemographics)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCourseTranscripts)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserLearningPathTranscripts)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserInstructorLedTrainingTranscripts)
                    //|| AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_XAPI)
                    || (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Purchases) && this._EcommerceSettings.IsEcommerceSetAndVerified)
                    || (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts) && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                    )
                )
            {
                this._Widgets.Add(new _Widget("ReportShortcutsWidget",
                                            _GlobalResources.ReportShortcuts,
                                            _BuildReportShortcutsWidget,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG),
                                            _GlobalResources.ReportShortcuts,
                                            false));
            }

            /************** THIS WIDGET DATA IS NOW PART OF ADMINISTRATIVE TASKS WIDGET*******************************************
            // Pending User Registrations Widget
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_PENDINGUSERREGISTRATIONS_ENABLED)
                && ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_ACCOUNTREGISTRATIONAPPROVAL) || User.DoUserAccountsNeedApproval())
                && (AsentiaSessionState.IdSiteUser == 1 || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserRegistrationApproval))) // check if user is admin or if they have user registration approval permissions
            {
                this._Widgets.Add(new _Widget("UserRegistrationApprovalWidget",
                                            _GlobalResources.PendingUserRegistrations,
                                            _BuildUserRegistrationApprovalWidget,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG),
                                            _GlobalResources.PendingUserRegistrations,
                                            true));

                // attach the hidden trigger button(s) and input(s) for the modal(s)
                this._ApproveRejectPendingUserRegistrationModalHiddenLaunchButton = new Button();
                this._ApproveRejectPendingUserRegistrationModalHiddenLaunchButton.ID = "ApproveRejectPendingUserRegistrationModalHiddenLaunchButton";                
                this._ApproveRejectPendingUserRegistrationModalHiddenLaunchButton.Style.Add("display", "none");               

                this._ApproveRejectPendingUserRegistrationData = new HiddenField();
                this._ApproveRejectPendingUserRegistrationData.ID = "ApproveRejectPendingUserRegistrationData";

                this.Controls.Add(this._ApproveRejectPendingUserRegistrationModalHiddenLaunchButton);
                this.Controls.Add(this._ApproveRejectPendingUserRegistrationData);

                // build modal(s)
                this._BuildPendingUserRegistrationActionModal(this._ApproveRejectPendingUserRegistrationModalHiddenLaunchButton.ID, this);
            }
             ************************************************************************************************************************************/

            /************** THIS WIDGET DATA IS NOW PART OF ADMINISTRATIVE TASKS WIDGET*******************************************
            // Enrollment Approval Widget            
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_PENDINGCOURSEENROLLMENTS_ENABLED)
                &&
                (
                 AsentiaSessionState.IdSiteUser == 1
                 || AsentiaSessionState.IsUserACourseExpert
                 || AsentiaSessionState.IsUserASupervisor
                 || AsentiaSessionState.IsUserACourseApprover
                 || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_SelfEnrollmentApproval)
                )
               )
            {
                this._Widgets.Add(new _Widget("CourseEnrollmentApprovalWidget",
                                             _GlobalResources.PendingEnrollments,
                                             _BuildCourseEnrollmentApprovalWidget,
                                             ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_SERIES, ImageFiles.EXT_PNG),
                                             ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_SERIES, ImageFiles.EXT_PNG),
                                             _GlobalResources.PendingEnrollments,
                                             true));

                // attach the hidden trigger button(s) and input(s) for the modal(s)
                this._ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton = new Button();
                this._ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton.ID = "ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton";
                this._ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton.Style.Add("display", "none");

                this._ApproveRejectPendingCourseEnrollmentData = new HiddenField();
                this._ApproveRejectPendingCourseEnrollmentData.ID = "ApproveRejectPendingCourseEnrollmentData";

                this.Controls.Add(this._ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton);
                this.Controls.Add(this._ApproveRejectPendingCourseEnrollmentData);

                // build modal(s)
                this._BuildPendingCourseEnrollmentActionModal(this._ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton.ID, this);
            }
             ************************************************************************************************************************************/

            /************** THIS WIDGET DATA IS NOW PART OF ADMINISTRATIVE TASKS WIDGET*******************************************
            // Certification Task Proctoring Widget
            if (
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE)
                && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_CERTIFICATIONTASKPROCTORING_ENABLED)
                && (
                    AsentiaSessionState.IdSiteUser == 1                    
                    || AsentiaSessionState.IsUserASupervisor // ojt and task proctoring widgets
                    )
                )
            {
                this._Widgets.Add(new _Widget("CertificationTaskProctoringWidget",
                                            _GlobalResources.CertificationTaskProctoring,
                                            _BuildCertificationTaskProctoringWidget,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG),
                                            _GlobalResources.CertificationTaskProctoring,
                                            false));
            }
             * ********************************************************************************************************************************/

            // Administrative Tasks Widget
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_ADMINISTRATIVETASKS_ENABLED)
                && (
                    AsentiaSessionState.IdSiteUser == 1
                    || AsentiaSessionState.IsUserACourseExpert
                    || AsentiaSessionState.IsUserACourseApprover
                    || AsentiaSessionState.IsUserASupervisor
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserRegistrationApproval)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_SelfEnrollmentApproval)
                    )
                )
            {
                this._Widgets.Add(new _Widget("AdministrativeTasksWidget",
                                _GlobalResources.AdministrativeTasks,
                                _BuildAdministrativeTasksWidget,
                                ImageFiles.GetIconPath(ImageFiles.ICON_ADMINISTRATOR, ImageFiles.EXT_PNG),
                                ImageFiles.GetIconPath(ImageFiles.ICON_ADMINISTRATOR, ImageFiles.EXT_PNG),
                                _GlobalResources.AdministrativeTasks,
                                false));

                // pending user registration hidden fields
                this._ApproveRejectPendingUserRegistrationModalHiddenLaunchButton = new Button();
                this._ApproveRejectPendingUserRegistrationModalHiddenLaunchButton.ID = "ApproveRejectPendingUserRegistrationModalHiddenLaunchButton";
                this._ApproveRejectPendingUserRegistrationModalHiddenLaunchButton.Style.Add("display", "none");

                this._ApproveRejectPendingUserRegistrationData = new HiddenField();
                this._ApproveRejectPendingUserRegistrationData.ID = "ApproveRejectPendingUserRegistrationData";

                this.Controls.Add(this._ApproveRejectPendingUserRegistrationModalHiddenLaunchButton);
                this.Controls.Add(this._ApproveRejectPendingUserRegistrationData);

                // pending course enrollment hidden fields
                this._BuildPendingUserRegistrationActionModal(this._ApproveRejectPendingUserRegistrationModalHiddenLaunchButton.ID, this);
               
                this._ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton = new Button();
                this._ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton.ID = "ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton";
                this._ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton.Style.Add("display", "none");

                this._ApproveRejectPendingCourseEnrollmentData = new HiddenField();
                this._ApproveRejectPendingCourseEnrollmentData.ID = "ApproveRejectPendingCourseEnrollmentData";

                this.Controls.Add(this._ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton);
                this.Controls.Add(this._ApproveRejectPendingCourseEnrollmentData);

                // build modal
                this._BuildPendingCourseEnrollmentActionModal(this._ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton.ID, this);
            }
        }
        #endregion

        #region _BuildWidgetBarIcon
        /// <summary>
        /// Builds the icon on that goes on the widget bar for the widget.
        /// </summary>
        /// <param name="id">id of the widget</param>
        /// <param name="iconImagePath">path to the icon image</param>
        /// <returns>control that represents the widget icon on the widget bar</returns>
        private Control _BuildWidgetBarIcon(string id, string iconImagePath, string tooltipText)
        {
            // widget item
            HtmlGenericControl listItem = new HtmlGenericControl("li");
            listItem.ID = id + "IconLI";
            listItem.Attributes.Add("class", "WidgetIconLI");

            // widget item link
            LinkButton listItemA = new LinkButton();
            listItemA.CssClass = "WidgetIcon";
            listItemA.ID = id + "Icon";
            listItemA.ToolTip = tooltipText;
            listItemA.Command += new CommandEventHandler(this._ProcessWidgetIconClick);

            // put a client-side onclick action on the button to show/hide the widget
            // unless it is the "Enrollments" widget
            if (AsentiaSessionState.UserDevice == AsentiaSessionState.UserDeviceType.Desktop)
            {
                if (id != _ENROLLMENT_WIDGET_ID)
                { listItemA.Attributes.Add("onclick", "ShowWidgetOnIconClick(this);"); }
            }
            else
            { listItemA.Attributes.Add("onclick", "ShowMobileWidgetOnIconClick(this.id);"); }

            // widget item link image
            Image listItemAImage = new Image();
            listItemAImage.CssClass = "MediumIcon";
            listItemAImage.ImageUrl = iconImagePath;
            listItemA.Controls.Add(listItemAImage);

            // attach widget item link and tooltip to widget item
            listItem.Controls.Add(listItemA);

            // alert overlay for widget icons

            // 1) Enrollment Widget - show alert if there are any overdue enrollments
            // 2) Calendar Widget - show alert if there are any calender items on current date/month
            // 3) Wall Moderation Widget - show alert if there are any items in the wall moderation widget 
            // 4) Task Proctoring Widget - show alert if there are any items in the task proctoring widget 
            // 5) OJT Proctoring Widget - show alert if there are any items in the OJT proctoring widget

            if (id == _ENROLLMENT_WIDGET_ID || id == _CALENDAR_WIDGET_ID || id == _WALL_MODERATION_WIDGET_ID || id == _TASK_PROCTORING_WIDGET_ID || id == _OJT_PROCTORING_WIDGET_ID)
            {
                Panel alertIconContainer = new Panel();
                Label alertIconLabel = new Label();
                alertIconLabel.CssClass = "WidgetIconAlert";
                alertIconLabel.ID = id + "_AlertIcon";
                alertIconLabel.Text = _ALERT_ICON_TEXT;
                alertIconLabel.Style.Add("display", "none");
                alertIconContainer.Controls.Add(alertIconLabel);
                listItem.Controls.Add(alertIconContainer);
            }

            return listItem;
        }
        #endregion

        #region _BuildWidgetPlaceholderControl
        /// <summary>
        /// Builds widget place holder controls
        /// </summary>
        /// <param name="id"></param>
        /// <param name="widgetHeaderText"></param>
        /// <param name="widgetHeaderIconPath"></param>
        /// <param name="noMinimumHeight"></param>
        /// <returns></returns>
        private Control _BuildWidgetPlaceholderControl(string id, string widgetHeaderText, string widgetHeaderIconPath, bool noMinimumHeight, bool refreshOnDrop)
        {
            bool mobile = (AsentiaSessionState.UserDevice != AsentiaSessionState.UserDeviceType.Desktop);

            // DECLARE CONTROLS FOR WIDGET
            Panel widgetPlaceholder = new Panel();
            widgetPlaceholder.ID = id + "Control";

            if (id.Contains("CalendarWidget"))
            { widgetPlaceholder.CssClass = "CalendarControl DraggableControl"; }
            else
            { widgetPlaceholder.CssClass = "DraggableControl"; }

            widgetPlaceholder.Style.Add("display", "none");

            // set an attribute for whether or not the widget should be refreshed after drag and drop
            widgetPlaceholder.Attributes.Add("refreshOnDrop", refreshOnDrop.ToString().ToLower());

            // if mobile, add "data-role" attribute
            if (mobile)
            { widgetPlaceholder.Attributes.Add("data-role", "page"); }

            Panel widgetHeader = new Panel();
            widgetHeader.ID = id + "Header";
            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard")
            {
                widgetHeader.CssClass = "WidgetHeader"; // widgets are movable
            }
            else
            {
                widgetHeader.CssClass = "RestrictedWidgetHeader"; // widgets are not movable
            }

            UpdatePanel widgetContent = new UpdatePanel();
            widgetContent.ID = id + "Content";
            widgetContent.ChildrenAsTriggers = true;
            widgetContent.UpdateMode = UpdatePanelUpdateMode.Conditional;

            // apply widget content style
            if (noMinimumHeight)
            { widgetContent.Attributes.Add("class", "WidgetContentNoMinimumHeight"); }
            else
            { widgetContent.Attributes.Add("class", "WidgetContent"); }

            // make the submit button an AsyncPostBackTrigger for the Update Panel - only needed for standard dashboard or mobile
            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard" ||
                AsentiaSessionState.UserDevice != AsentiaSessionState.UserDeviceType.Desktop)
            {
                AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
                trigger.ControlID = id +"Icon";
                trigger.EventName = "Click";
                widgetContent.Triggers.Add(trigger);
            }

            // BUILD WIDGET HEADER
            HtmlGenericControl headerWrapper = new HtmlGenericControl("p");
            headerWrapper.ID = id + "HeaderWrapper";

            // header icon
            System.Web.UI.WebControls.Image headerIcon = new System.Web.UI.WebControls.Image();
            headerIcon.ID = id + "HeaderIcon";
            headerIcon.AlternateText = widgetHeaderText;
            headerIcon.ImageUrl = widgetHeaderIconPath;
            headerIcon.CssClass = "MediumIcon";

            headerWrapper.Controls.Add(headerIcon);

            // header text
            Localize headerText = new Localize();
            headerText.Text = widgetHeaderText;
            headerWrapper.Controls.Add(headerText);

            widgetHeader.Controls.Add(headerWrapper);

            // add close icon if this is not the enrollment widget, not on mobile, and standard dashboard mode
            if (id != _ENROLLMENT_WIDGET_ID && !mobile && AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard")
            {
                HyperLink closeIcon = new HyperLink();
                closeIcon.CssClass = "WidgetControlIcon";
                closeIcon.ID = id + "CloseIcon";
                closeIcon.NavigateUrl = "javascript:void(0);";
                closeIcon.Attributes.Add("onclick", "CloseWidget(this);");
                closeIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE,
                                                            ImageFiles.EXT_PNG);

                widgetHeader.Controls.Add(closeIcon);
            }

            LinkButton refreshIcon = new LinkButton();
            refreshIcon.CssClass = "WidgetControlIcon";
            refreshIcon.ID = id + "RefreshIcon";
            refreshIcon.Command += new CommandEventHandler(this._ProcessRefreshIconClick);

            System.Web.UI.WebControls.Image refreshIconImage = new System.Web.UI.WebControls.Image();
            refreshIconImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RELOAD,
                                                               ImageFiles.EXT_PNG);
            refreshIconImage.CssClass = "XSmallIcon";
            refreshIcon.Controls.Add(refreshIconImage);
            widgetHeader.Controls.Add(refreshIcon);

            // only add a collapse/exapand button if this is not on mobile and this is the standard dashboard
            if (!mobile && AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard")
            {
                // collapse/expand icon
                HyperLink collapseExpandIcon = new HyperLink();
                collapseExpandIcon.CssClass = "WidgetControlIcon";
                collapseExpandIcon.ID = id + "CollapseExpandIcon";
                collapseExpandIcon.NavigateUrl = "javascript:void(0);";
                collapseExpandIcon.Attributes.Add("onclick", "CollapseExpandWidget(this);");
                collapseExpandIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COLLAPSE,
                                                                     ImageFiles.EXT_PNG);

                widgetHeader.Controls.Add(collapseExpandIcon);
            }

            // make the refresh button an AsyncPostBackTrigger for the content UpdatePanel
            AsyncPostBackTrigger refreshTrigger = new AsyncPostBackTrigger();
            refreshTrigger.ControlID = refreshIcon.ID;
            refreshTrigger.EventName = "Click";
            widgetContent.Triggers.Add(refreshTrigger);

            // ATTACH CONTROLS TO WIDGET PLACEHOLDER
            widgetPlaceholder.Controls.Add(widgetHeader);
            widgetPlaceholder.Controls.Add(widgetContent);

            // RETURN
            return widgetPlaceholder;
        }
        #endregion

        #region _LoadUserDashboardConfig
        /// <summary>
        /// Loads user dashboard config
        /// </summary>
        /// <returns></returns>
        private XmlDocument _LoadUserDashboardConfig()
        {
            
            // Only load the saved dashboard config for the standard dashboard, otherwise load the default config
            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard")
            {
                // grab the config file name based on if the new combined config file exists or not.
                string configFilename = String.Empty;

                // first, determine if the user currently has a "user config folder"
                // if not, create it

                if (!Directory.Exists(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser)))
                {
                    // create directory
                    Directory.CreateDirectory(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser));
                }

                // In the "user config folder", check if they have a 
                // config file, if not, copy the default config

                DirectoryInfo di = new DirectoryInfo(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser));
                bool copyConfigFileBeforeLoad = false;

                if (di.GetFiles("*.xml").Length == 0)
                {
                    copyConfigFileBeforeLoad = true;
                    configFilename = _DASHBOARD_CONFIG_FILE;
                }
                else
                {
                    if (AsentiaSessionState.IdSiteUser > 1)
                    {
                        if (File.Exists(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _LEARNER_DASHBOARD_CONFIG_FILE)))
                        {
                            XmlDocument learnerXML = new XmlDocument();
                            learnerXML.Load(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _LEARNER_DASHBOARD_CONFIG_FILE));

                            if (File.Exists(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _ADMINISTRATOR_DASHBOARD_CONFIG_FILE)))
                            {
                                XmlDocument adminXML = new XmlDocument();
                                adminXML.Load(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _ADMINISTRATOR_DASHBOARD_CONFIG_FILE));
                                foreach (XmlNode node in adminXML.DocumentElement.ChildNodes)
                                {
                                    XmlNode imported = learnerXML.ImportNode(node, true);
                                    learnerXML.DocumentElement.AppendChild(imported);
                                }
                            }

                            if (File.Exists(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _REPORTER_DASHBOARD_CONFIG_FILE)))
                            {
                                XmlDocument reportXML = new XmlDocument();
                                reportXML.Load(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _REPORTER_DASHBOARD_CONFIG_FILE));
                                foreach (XmlNode node in reportXML.DocumentElement.ChildNodes)
                                {
                                    XmlNode imported = learnerXML.ImportNode(node, true);
                                    learnerXML.DocumentElement.AppendChild(imported);
                                }
                            }

                            return learnerXML;
                        }
                        else
                        {
                            if (!File.Exists(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _DASHBOARD_CONFIG_FILE)))
                            { copyConfigFileBeforeLoad = true; }

                            configFilename = _DASHBOARD_CONFIG_FILE;
                        }
                    }
                    else
                    {
                        if (File.Exists(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _ADMINISTRATOR_DASHBOARD_CONFIG_FILE)))
                        {
                            XmlDocument adminXML = new XmlDocument();
                            adminXML.Load(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _ADMINISTRATOR_DASHBOARD_CONFIG_FILE));

                            if (File.Exists(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _REPORTER_DASHBOARD_CONFIG_FILE)))
                            {
                                XmlDocument reportXML = new XmlDocument();
                                reportXML.Load(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _REPORTER_DASHBOARD_CONFIG_FILE));
                                foreach (XmlNode node in reportXML.DocumentElement.ChildNodes)
                                {
                                    XmlNode imported = adminXML.ImportNode(node, true);
                                    adminXML.DocumentElement.AppendChild(imported);
                                }
                            }

                            return adminXML;
                        }
                        else
                        {
                            if (!File.Exists(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _DASHBOARD_CONFIG_FILE)))
                            { copyConfigFileBeforeLoad = true; }

                            configFilename = _DASHBOARD_CONFIG_FILE;
                        }
                    }
                }

                // if we need to copy the default dashboard config file, do it
                if (copyConfigFileBeforeLoad)
                {
                    // copy default config file
                    if (File.Exists(MapPathSecure(SitePathConstants.SITE_USERS_DEFAULT + _DASHBOARD_CONFIG_FILE)))
                    {
                        // copy from "default user" folder in the site
                        File.Copy(MapPathSecure(SitePathConstants.SITE_USERS_DEFAULT + _DASHBOARD_CONFIG_FILE),
                                    MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _DASHBOARD_CONFIG_FILE));
                    }
                    else
                    {
                        // copy from "default user" folder in the default site
                        File.Copy(MapPathSecure(SitePathConstants.DEFAULT_SITE_USERS_DEFAULT + _DASHBOARD_CONFIG_FILE),
                                    MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + _DASHBOARD_CONFIG_FILE));
                    }
                }

                // now, load the config file and return the XML
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(MapPathSecure(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + configFilename));
                return xmlDocument;
            }
            else // RESTRICTED DASHBOARD LOADS DEFAULT LEARNER OR ADMINISTRATOR DASHBOARD XML - THESE FILES ONLY USED FOR RESTRICTED DASHBOARD
            {
                XmlDocument xmlDocument = new XmlDocument();

                if (AsentiaSessionState.IdSiteUser > 1)
                {
                    xmlDocument.Load(MapPathSecure(SitePathConstants.DEFAULT_SITE_USERS_DEFAULT + _LEARNER_DASHBOARD_CONFIG_FILE));
                }
                else
                {
                    xmlDocument.Load(MapPathSecure(SitePathConstants.DEFAULT_SITE_USERS_DEFAULT + _ADMINISTRATOR_DASHBOARD_CONFIG_FILE));
                }
                return xmlDocument;
            }

        }
        #endregion

        #region _ProcessWidgetIconClick
        /// <summary>
        /// Processes the click of a widget bar icon in order to load the widget's content.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ProcessWidgetIconClick(object sender, CommandEventArgs e)
        {
            // get the sender's id so we can strip it down to the widget's id
            LinkButton icon = (LinkButton)sender;
            string widgetId = icon.ID.Replace("Icon", "");

            // update the widget's content
            this._UpdateWidgetContent(widgetId);
        }
        #endregion

        #region _ProcessRefreshIconClick
        /// <summary>
        /// Processes the click of a widget's refresh icon in order to re-load the widget's content.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ProcessRefreshIconClick(object sender, CommandEventArgs e)
        {
            // get the sender's id so we can strip it down to the widget's id
            LinkButton icon = (LinkButton)sender;
            string widgetId = icon.ID.Replace("RefreshIcon", "");

            // update the widget's content
            this._UpdateWidgetContent(widgetId);
        }
        #endregion

        #region _UpdateWidgetContent
        /// <summary>
        /// Updates a widget's content by calling the "widget specific" method to load
        /// the widgets content.
        /// </summary>
        /// <param name="widgetId">id of the widget</param>
        private void _UpdateWidgetContent(string widgetId)
        {
            // get the widget's container control
            UpdatePanel updatePanel = (UpdatePanel)this.FindControl(widgetId + "Content");

            // loop through widgets that were loaded, find the
            // widget to be updated, and update it
            foreach (_Widget widget in _Widgets)
            {
                if (widgetId == widget.Id)
                {
                    widget.BuilderMethod.Invoke(widget.Id, false);
                    break;
                }
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.Common.ClientScript), "Asentia.Common.TableSorting.js");
            ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Carousel.min.js");
            ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.LMS.Controls.DashboardWidgetContainer), "Asentia.LMS.Controls.DashboardWidgetContainer.js");
            ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.LMS.Controls.DashboardWidgetContainer), "Asentia.LMS.Controls.CertificateObject.js");
            ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.LMS.Controls.DashboardWidgetContainer), "Asentia.LMS.Pages.Administrator.Users.Certificates.js");
            ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.LMS.Controls.DashboardWidgetContainer), "Asentia.LMS.Pages.Administrator.Users.Transcript.js");

            if (AsentiaSessionState.UserDevice != AsentiaSessionState.UserDeviceType.Desktop)
            {
                ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery.mobile-1.4.5.min.js");
                ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery.kinetic.min.js");
                ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery.smoothTouchScroll.min.js");             
            }
            
            // set the strings for global variables
            StringBuilder globalJs = new StringBuilder();
            
            globalJs.AppendLine("var NoMessages = \"" + _GlobalResources.ThereAreNoMessagesCurrentlyAwaitingModeration + "\";");                        
            globalJs.AppendLine("var DefaultCertificateImagePath =\"" + SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "\";");
            globalJs.AppendLine("var ApproveUserRegistrationText = \"" + _GlobalResources.ApproveUserRegistration + "\";");
            globalJs.AppendLine("var ApproveCourseEnrollmentText = \"" + _GlobalResources.ApproveCourseEnrollment + "\";");
            globalJs.AppendLine("var RejectUserRegistrationText = \"" + _GlobalResources.RejectUserRegistration + "\";");
            globalJs.AppendLine("var RejectCourseEnrollmentText = \"" + _GlobalResources.RejectCourseEnrollment + "\";");
            globalJs.AppendLine("var DashboardMode = \"" + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) + "\";");

            // add start up script
            ScriptManager.RegisterStartupScript(this.Page, typeof(DashboardWidgetContainer), "GlobalJs", globalJs.ToString(), true);
        }
        #endregion

        #region OnInit
        /// <summary>
        /// Overrides "OnInit' event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            bool mobile = (AsentiaSessionState.UserDevice != AsentiaSessionState.UserDeviceType.Desktop);            

            // GET ASENTIA PAGE
            this._AsentiaPageInstance = (AsentiaPage)this.Page;

            // INITIALIZE WIDGETS
            this._InitializeWidgets();

            // **
            // BUILD WIDGET BAR
            // **
            
            // only build widget bar and layout selector for the standard dashboard
            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard" || mobile)
            {
                Panel outerBarContainer = new Panel();
                outerBarContainer.ID = "OuterBarContainer";

                // update panel for widget bar
                UpdatePanel widgetBar = new UpdatePanel();
                widgetBar.ID = "WidgetBar";
                widgetBar.UpdateMode = UpdatePanelUpdateMode.Conditional;
                widgetBar.ChildrenAsTriggers = false;

                if (!mobile)
                {
                    // build the widget layout selector
                    HtmlGenericControl unorderedListLayout = new HtmlGenericControl("ul");
                    unorderedListLayout.Attributes.Add("class", "WidgetLayoutSelectorList");

                    // one column
                    HtmlGenericControl oneColumnListItem = new HtmlGenericControl("li");
                    oneColumnListItem.ID = "OneColumnIconLI";
                    oneColumnListItem.Attributes.Add("class", "WidgetLayoutIconLI");

                    HyperLink oneColumnLayout = new HyperLink();
                    oneColumnLayout.ID = "OneColumn";
                    oneColumnLayout.CssClass = "WidgetLayoutIcon";
                    oneColumnLayout.NavigateUrl = "javascript:void(0);";
                    oneColumnLayout.Attributes.Add("onclick", "SetWidgetContainerLayout(this.id, '50%', '50%', true);");
                    oneColumnLayout.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ONECOLUMN,
                                                                  ImageFiles.EXT_PNG);
                    // attach controls
                    oneColumnListItem.Controls.Add(oneColumnLayout);
                    unorderedListLayout.Controls.Add(oneColumnListItem);


                    // two column
                    HtmlGenericControl twoColumnListItem = new HtmlGenericControl("li");
                    twoColumnListItem.ID = "TwoColumnIconLI";
                    twoColumnListItem.Attributes.Add("class", "WidgetLayoutIconLI");

                    HyperLink twoColumnLayout = new HyperLink();
                    twoColumnLayout.ID = "TwoColumn";
                    twoColumnLayout.CssClass = "WidgetLayoutIcon";
                    twoColumnLayout.NavigateUrl = "javascript:void(0);";
                    twoColumnLayout.Attributes.Add("onclick", "SetWidgetContainerLayout(this.id, '50%', '50%', true);");
                    twoColumnLayout.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_TWOCOLUMN,
                                                                  ImageFiles.EXT_PNG);
                    // attach controls
                    twoColumnListItem.Controls.Add(twoColumnLayout);
                    unorderedListLayout.Controls.Add(twoColumnListItem);

                    // attach layout selector to widget bar
                    widgetBar.ContentTemplateContainer.Controls.Add(unorderedListLayout);


                    // build the widget bar title
                    HtmlGenericControl widgetBarTitle = new HtmlGenericControl("p");
                    widgetBarTitle.ID = "WidgetBarTitle";
                    widgetBarTitle.Attributes.Add("class", "MobileHidden");
                    widgetBarTitle.InnerText = _GlobalResources.Widgets;

                    widgetBar.ContentTemplateContainer.Controls.Add(widgetBarTitle);
                }

                // build the ul that will contain li's representing widget icons
                HtmlGenericControl unorderedList = new HtmlGenericControl("ul");
                unorderedList.Attributes.Add("class", "WidgetList");

                if (mobile)
                {
                    // move left icon
                    HtmlGenericControl moveLeftIcon = new HtmlGenericControl("li");
                    moveLeftIcon.ID = "MoveLeftIconLI";
                    moveLeftIcon.Attributes.Add("class", "WidgetIconLI  Navigator");

                    HyperLink moveLeftIconLink = new HyperLink();
                    moveLeftIconLink.ID = "MoveLeftIconLink";
                    moveLeftIconLink.CssClass = "WidgetIcon";
                    moveLeftIconLink.NavigateUrl = "javascript:void(0);";
                    moveLeftIconLink.Attributes.Add("onclick", "WidgetBarMoveLeftClick();");
                    moveLeftIconLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PREVIOUS,
                                                                  ImageFiles.EXT_PNG);
                    // attach controls
                    moveLeftIcon.Controls.Add(moveLeftIconLink);
                    unorderedList.Controls.Add(moveLeftIcon);
                }

                // build and attach the each of the widget icons
                foreach (_Widget widget in _Widgets)
                {
                    // to do, evaluate rules and permissions
                    unorderedList.Controls.Add(this._BuildWidgetBarIcon(widget.Id, widget.WidgetBarIconPath, widget.TooltipText));
                }

                if (mobile)
                {
                    // move right icon
                    HtmlGenericControl moveRightIcon = new HtmlGenericControl("li");
                    moveRightIcon.ID = "MoveRightIconLI";
                    moveRightIcon.Attributes.Add("class", "WidgetIconLI  Navigator");

                    HyperLink moveRightIconLink = new HyperLink();
                    moveRightIconLink.ID = "MoveRightIconLink";
                    moveRightIconLink.CssClass = "WidgetIcon";
                    moveRightIconLink.NavigateUrl = "javascript:void(0);";
                    moveRightIconLink.Attributes.Add("onclick", "WidgetBarMoveRightClick();");
                    moveRightIconLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_NEXT,
                                                                  ImageFiles.EXT_PNG);
                    // attach controls
                    moveRightIcon.Controls.Add(moveRightIconLink);
                    unorderedList.Controls.Add(moveRightIcon);
                }

                // attach controls
                widgetBar.ContentTemplateContainer.Controls.Add(unorderedList);
                outerBarContainer.Controls.Add(widgetBar);
                this.Controls.Add(outerBarContainer);
            }

            // **
            // WIDGET CONTENT CONTAINER
            // **

            Panel widgetContainer = new Panel();
            Panel enrollmentsWidgetColumn = new Panel();

            if (!mobile)
            {
                widgetContainer.ID = "WidgetContainer";

                // static column for enrollments widget - only added to the container
                enrollmentsWidgetColumn.ID = "EnrollmentsWidgetColumn";
                enrollmentsWidgetColumn.CssClass = "OneColumnLayout";
            }

            // column one - this starts with only building one column; the second
            // second column will be built if needed on the initial page load
            Panel widgetColumnWrapper = new Panel();
            widgetColumnWrapper.ID = "WidgetColumnWrapper1";
            widgetColumnWrapper.CssClass = "WidgetColumnWrapper OneColumnLayout";

            Panel widgetColumn = new Panel();
            widgetColumn.ID = "WidgetColumn1";
            widgetColumn.CssClass = "WidgetColumn";
            

            // build and attach the widget placeholder controls to the container
            foreach (_Widget widget in _Widgets)
            {
                if (widget.Id == _ENROLLMENT_WIDGET_ID && !mobile)
                { enrollmentsWidgetColumn.Controls.Add(this._BuildWidgetPlaceholderControl(widget.Id, widget.HeaderTitle, widget.HeaderIconPath, false, false)); }
                else
                { widgetColumn.Controls.Add(this._BuildWidgetPlaceholderControl(widget.Id, widget.HeaderTitle, widget.HeaderIconPath, false, widget.RefreshOnDrop)); }
            }
            
            // attach controls
            if (!mobile)
            { this.Controls.Add(enrollmentsWidgetColumn); }
            
            widgetColumnWrapper.Controls.Add(widgetColumn);
            widgetContainer.Controls.Add(widgetColumnWrapper);

            this.Controls.Add(widgetContainer);
            
            // **
            // WIDGET CONTENT PLACEHOLDERS & INITIAL CONTENT LOADING
            // **

            // build each widget's content placeholders and attach them to the widget's content placeholder 
            // control using each widget's specific content method
            foreach (_Widget widget in _Widgets)
            {                
                widget.BuilderMethod.Invoke(widget.Id, true);
            }

            // build the javascript to perform initial load and placement of the widgets based on the dashboard's 
            // saved config file (or the default if a saved file is not found for the user)

            // get config xml
            XmlDocument dashboardConfig = this._LoadUserDashboardConfig();

            // CREATE LITERAL AND STRING BUILDER FOR JAVASCRIPT USED TO
            // STORE VARIABLES NEEDED BY THE "SAVE" WEB SERVICE AND TO
            // PERFORM THE INITIAL LOAD AND PLACEMENT OF WIDGETS

            Literal javascriptCode = new Literal();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type=\"text/javascript\">");

            sb.AppendLine(" ExpandImagePath = \""
                          + ImageFiles.GetIconPath(ImageFiles.ICON_EXPAND,
                                                   ImageFiles.EXT_PNG)
                          + "\";");
            sb.AppendLine(" CollapseImagePath = \""
                          + ImageFiles.GetIconPath(ImageFiles.ICON_COLLAPSE,
                                                   ImageFiles.EXT_PNG)
                          + "\";");
            sb.AppendLine(" ");

            // widget loading and placement
            sb.AppendLine(" $(document).ready(function () {");

            if (mobile)
            {
                // for mobile dashboard widgets, always load the first widget
                if (this._Widgets.Count > 0)
                {
                    _Widget firstWidget = (_Widget)this._Widgets[0];
                    string firstWidgetId = firstWidget.Id;

                    sb.AppendLine("     function waitFor" + firstWidgetId + "() {");
                    sb.AppendLine("         if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()){");
                    sb.AppendLine("             setTimeout(waitFor" + firstWidgetId + ", 100);");
                    sb.AppendLine("         } else {");

                    if (firstWidgetId == _ENROLLMENT_WIDGET_ID)
                    { sb.AppendLine("             ShowMobileWidgetOnLoad('EnrollmentsWidgetColumn', '" + firstWidgetId + "', false);"); }
                    else
                    { sb.AppendLine("             ShowMobileWidgetOnLoad('WidgetContainer', '" + firstWidgetId + "', false);"); }

                    sb.AppendLine("         }");
                    sb.AppendLine("     }");
                    sb.AppendLine("     waitFor" + firstWidgetId + "();");
                }
            }
            else
            {
                // get the layout (OneColumn or TwoColumn), and column widths from the config and set the js call to load it
                string layout = dashboardConfig.GetElementsByTagName("dashboard").Item(0).Attributes["layout"].Value;
                string column1Width = "50%";
                string column2Width = "50%";

                if (
                    dashboardConfig.GetElementsByTagName("dashboard").Item(0).Attributes["column1Width"] != null
                    &&
                    dashboardConfig.GetElementsByTagName("dashboard").Item(0).Attributes["column2Width"] != null
                   )
                {
                    column1Width = dashboardConfig.GetElementsByTagName("dashboard").Item(0).Attributes["column1Width"].Value;
                    column2Width = dashboardConfig.GetElementsByTagName("dashboard").Item(0).Attributes["column2Width"].Value;
                }

                sb.AppendLine("     SetWidgetContainerLayout('" + layout + "','" + column1Width + "', '" + column2Width + "', false);");

                // set the "Enrollments" widget to load if user is not a administrator
                if (AsentiaSessionState.IdSiteUser > 1) 
                {
                    sb.AppendLine("     function waitFor" + _ENROLLMENT_WIDGET_ID + "() {");
                    sb.AppendLine("         if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()){");
                    sb.AppendLine("             setTimeout(waitFor" + _ENROLLMENT_WIDGET_ID + ", 100);");
                    sb.AppendLine("         } else {");
                    sb.AppendLine("             ShowWidgetOnLoad('EnrollmentsWidgetColumn', '" + _ENROLLMENT_WIDGET_ID + "', false);");
                    sb.AppendLine("         }");
                    sb.AppendLine("     }");
                    sb.AppendLine("     waitFor" + "EnrollmentsWidget" + "();");
                }

            }

            // get the "widget" nodes
            XmlNodeList widgetNodes = dashboardConfig.GetElementsByTagName("widget");

            // loop through each "widget" node, and add js calls to it if it exists in 
            // our "available" widget collection
            foreach (XmlNode widgetNode in widgetNodes)
            {
                string widgetNodeId = widgetNode.Attributes["id"].Value;
                int widgetNodeColumn = Convert.ToInt32(widgetNode.Attributes["column"].Value);
                string widgetNodeIsCollapsed = widgetNode.Attributes["isCollapsed"].Value;

                if (!mobile)
                {
                    foreach (_Widget widget in _Widgets)
                    {
                        if (widget.Id == widgetNodeId && widget.Id != _ENROLLMENT_WIDGET_ID)
                        {
                            sb.AppendLine("     function waitFor" + widget.Id + "() {");
                            sb.AppendLine("         if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()){");
                            sb.AppendLine("             setTimeout(waitFor" + widget.Id + ", 100);");
                            sb.AppendLine("         } else {");
                            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard")
                            {
                                sb.AppendLine("             ShowWidgetOnLoad('WidgetColumn" + widgetNodeColumn + "', '" + widget.Id + "', " + widgetNodeIsCollapsed + ", true);");
                            }
                            else
                            {
                                sb.AppendLine("             ShowWidgetOnLoad('WidgetColumn" + widgetNodeColumn + "', '" + widget.Id + "', false, false);");
                            }
                            sb.AppendLine("         }");
                            sb.AppendLine("     }");
                            sb.AppendLine("     waitFor" + widget.Id + "();");
                            break;
                        }
                    }
                }
            }

            // add the script closing tag and attach the script code to the literal
            sb.AppendLine(" });");
            sb.AppendLine("</script>");
            javascriptCode.Text = sb.ToString();

            // attach controls
            this.Controls.Add(javascriptCode);

            base.OnInit(e);
        }
        #endregion
        #endregion

        #region Widget Content Builder Methods
        #region _BuildWidgetContentLoadingPlaceholder
        /// <summary>
        /// Builds widget content place holder
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        /// <returns></returns>
        private Panel _BuildWidgetContentLoadingPlaceholder(string widgetId, bool isInitialLoad)
        {
            Panel loadingPlaceholder = new Panel();
            loadingPlaceholder.ID = widgetId + "WidgetLoadingPlaceholder";
            loadingPlaceholder.CssClass = "WidgetLoadingPlaceholder";

            // loading icon
            HtmlGenericControl contentLoadingImageWrapper = new HtmlGenericControl("p");
            contentLoadingImageWrapper.ID = widgetId + "ContentLoadingImageWrapper";

            System.Web.UI.WebControls.Image contentLoadingImage = new System.Web.UI.WebControls.Image();
            contentLoadingImage.ID = widgetId + "ContentLoadingImage";
            contentLoadingImage.AlternateText = _GlobalResources.Loading;
            contentLoadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING,
                                                                    ImageFiles.EXT_GIF);
            contentLoadingImage.CssClass = "MediumIcon";

            contentLoadingImageWrapper.Controls.Add(contentLoadingImage);
            loadingPlaceholder.Controls.Add(contentLoadingImageWrapper);

            // loading text
            HtmlGenericControl contentLoadingTextWrapper = new HtmlGenericControl("p");
            contentLoadingTextWrapper.ID = widgetId + "ContentLoadingTextWrapper";

            Localize contentLoadingText = new Localize();
            contentLoadingText.Text = _GlobalResources.Loading;

            contentLoadingTextWrapper.Controls.Add(contentLoadingText);
            loadingPlaceholder.Controls.Add(contentLoadingTextWrapper);

            return loadingPlaceholder;
        }
        #endregion

        #region _WidgetBuilderTemplate
        /// <summary>
        /// Template method to serve as the basis for wideget builder methods.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _WidgetBuilderTemplate(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // WIDGET SPECIFIC CODE GOES HERE - REPLACE THIS COMMENT

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);
        }
        #endregion

        #region _BuildEnrollmentsWidget
        /// <summary>
        /// Builds enrollment widget
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildEnrollmentsWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            HiddenField enrollmentWidgetHiddenForShowingAlertIcon = new HiddenField();
            enrollmentWidgetHiddenForShowingAlertIcon.ID = widgetId + "HiddenForShowingAlertIcon";
            enrollmentWidgetHiddenForShowingAlertIcon.Value = "0";
            widgetContent.ContentTemplateContainer.Controls.Add(enrollmentWidgetHiddenForShowingAlertIcon);

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // WIDGET SPECIFIC CODE GOES HERE - REPLACE THIS COMMENT

            // TABS
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Enrolled", _GlobalResources.Enrolled));
            tabs.Enqueue(new KeyValuePair<string, string>("Pending", _GlobalResources.Pending));
            tabs.Enqueue(new KeyValuePair<string, string>("Overdue", _GlobalResources.Overdue));
            tabs.Enqueue(new KeyValuePair<string, string>("Completed", _GlobalResources.Completed));
            tabs.Enqueue(new KeyValuePair<string, string>("Expired", _GlobalResources.Expired));

            // build and attach the tabs
            loadedWidgetContent.Controls.Add(AsentiaPage.BuildTabListPanel(widgetId, tabs));

            // BEGIN GRIDS
            // BEGIN ENROLLED GRID
            // "Enrolled" is the default tab, so this is visible on page load.
            UpdatePanel enrolledPanel = new UpdatePanel();
            enrolledPanel.ID = widgetId + "_Enrolled_TabPanel";
            enrolledPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            enrolledPanel.Attributes.Add("style", "display: block;");

            Grid enrolledGrid = new Grid("Account", true);

            enrolledGrid.ID = widgetId + "EnrolledGrid";
            enrolledGrid.StoredProcedure = Enrollment.GridProcedureForUserDashboard;
            enrolledGrid.DefaultRecordsPerPage = 10;
            enrolledGrid.ShowTimeInDateStrings = true;
            enrolledGrid.EmptyDataText = _GlobalResources.NoEnrollmentsFound;
            enrolledGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            enrolledGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            enrolledGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            enrolledGrid.AddFilter("@enrollmentStatus", SqlDbType.NVarChar, 20, "enrolled");
            enrolledGrid.IdentifierField = "idEnrollment";
            enrolledGrid.DefaultSortColumn = "title";

            // data key names
            enrolledGrid.DataKeyNames = new string[] { "idEnrollment", "idCourse", "title" };

            // columns
            GridColumn enrolledCourse = new GridColumn(_GlobalResources.Course, "title", "title");
            GridColumn enrolledProgress = new GridColumn(_GlobalResources.Progress, null, true, true); // calculated in row databound event
            GridColumn enrolledDueDate = new GridColumn(_GlobalResources.DueDate, "dtDue", "dtDue", false, true);
            GridColumn enrolledExpires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires", false, true);
            GridColumn enrolledView = new GridColumn(_GlobalResources.View, "launch", true);
            enrolledView.AddProperty(new GridColumnProperty("1", "<a href=\"/dashboard/Enrollment.aspx?idEnrollment=##idEnrollment##\">"
                                                            + "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View
                                                            + "\" title=\"" + _GlobalResources.ViewCourse + "\" />"
                                                            + "</a>"));
            enrolledView.AddProperty(new GridColumnProperty("2", "<img class=\"SmallIcon DimIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View
                                                            + "\" title=\"" + _GlobalResources.ViewCourseUnavailable + "\" />"));
            enrolledView.AddProperty(new GridColumnProperty("3", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.PrerequisitesIncomplete + "\" />"));
            enrolledView.AddProperty(new GridColumnProperty("4", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.LearningPathPrerequisitesIncomplete + "\" />"));
            enrolledView.AddProperty(new GridColumnProperty("5", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.Locked + "\" />"));

            // add columns to data grid
            enrolledGrid.AddColumn(enrolledCourse);
            enrolledGrid.AddColumn(enrolledProgress);
            enrolledGrid.AddColumn(enrolledDueDate);
            enrolledGrid.AddColumn(enrolledExpires);

            // build and add wall column if the course discussion feature is enabled
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE))
            {
                GridColumn enrolledWall = new GridColumn(_GlobalResources.Discussion, "wall", true);
                enrolledWall.AddProperty(new GridColumnProperty("True", "<a href=\"/courses/Wall.aspx?id=##idCourse##\">"
                                                                + "<img class=\"SmallIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION_BUTTON,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.Discussion + "\" />"
                                                                + "</a>"));
                enrolledWall.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION_BUTTON,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.Discussion + "\" />"));

                enrolledGrid.AddColumn(enrolledWall);
            }

            enrolledGrid.AddColumn(enrolledView);
            enrolledGrid.RowDataBound += this._EnrollmentsWidgetGrid_RowDataBound;

            enrolledPanel.ContentTemplateContainer.Controls.Add(enrolledGrid);
            loadedWidgetContent.Controls.Add(enrolledPanel);
            // END ENROLLED GRID

            // BEGIN PENDING GRID
            UpdatePanel pendingPanel = new UpdatePanel();
            pendingPanel.ID = widgetId + "_Pending_TabPanel";
            pendingPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            pendingPanel.Attributes.Add("style", "display: none;");

            Grid pendingGrid = new Grid("Account", true);

            pendingGrid.ID = widgetId + "PendingGrid";
            pendingGrid.StoredProcedure = EnrollmentRequest.GridProcedureForUserDashboard;
            pendingGrid.DefaultRecordsPerPage = 10;
            pendingGrid.ShowTimeInDateStrings = true;
            pendingGrid.EmptyDataText = _GlobalResources.NoPendingEnrollmentsFound;
            pendingGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            pendingGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            pendingGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            pendingGrid.IdentifierField = "idEnrollmentRequest";
            pendingGrid.DefaultSortColumn = "title";

            // data key names
            pendingGrid.DataKeyNames = new string[] { "idEnrollmentRequest", "idCourse", "title" };

            // columns
            GridColumn pendingCourse = new GridColumn(_GlobalResources.Course, "title", "title");
            GridColumn pendingRequestDate = new GridColumn(_GlobalResources.RequestDate, "dtRequested", "dtRequested", false, true);

            // add columns to data grid
            pendingGrid.AddColumn(pendingCourse);
            pendingGrid.AddColumn(pendingRequestDate);            

            pendingPanel.ContentTemplateContainer.Controls.Add(pendingGrid);
            loadedWidgetContent.Controls.Add(pendingPanel);
            // END PENDING GRID

            // BEGIN OVERDUE GRID
            UpdatePanel overduePanel = new UpdatePanel();
            overduePanel.ID = widgetId + "_Overdue_TabPanel";
            overduePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            overduePanel.Attributes.Add("style", "display: none;");

            Grid overdueGrid = new Grid("Account", true);

            overdueGrid.ID = widgetId + "OverdueGrid";
            overdueGrid.StoredProcedure = Enrollment.GridProcedureForUserDashboard;
            overdueGrid.DefaultRecordsPerPage = 10;
            overdueGrid.ShowTimeInDateStrings = true;
            overdueGrid.EmptyDataText = _GlobalResources.YouHaveNoOverdueEnrollments;
            overdueGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            overdueGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            overdueGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            overdueGrid.AddFilter("@enrollmentStatus", SqlDbType.NVarChar, 20, "overdue");
            overdueGrid.IdentifierField = "idEnrollment";
            overdueGrid.DefaultSortColumn = "title";

            // data key names
            overdueGrid.DataKeyNames = new string[] { "idEnrollment", "idCourse", "title" };

            // columns
            GridColumn overdueCourse = new GridColumn(_GlobalResources.Course, "title", "title");
            GridColumn overdueProgress = new GridColumn(_GlobalResources.Progress, null, true, true); // calculated in row databound event
            GridColumn overdueDueDate = new GridColumn(_GlobalResources.DueDate, "dtDue", "dtDue", false, true);
            GridColumn overdueExpires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires", false, true);

            GridColumn overdueView = new GridColumn(_GlobalResources.View, "launch", true);
            overdueView.AddProperty(new GridColumnProperty("1", "<a href=\"/dashboard/Enrollment.aspx?idEnrollment=##idEnrollment##\">"
                                                            + "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View
                                                            + "\" title=\"" + _GlobalResources.ViewCourse + "\" />"
                                                            + "</a>"));
            overdueView.AddProperty(new GridColumnProperty("2", "<img class=\"SmallIcon DimIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View
                                                            + "\" title=\"" + _GlobalResources.ViewCourseUnavailable + "\" />"));
            overdueView.AddProperty(new GridColumnProperty("3", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.PrerequisitesIncomplete + "\" />"));
            overdueView.AddProperty(new GridColumnProperty("4", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.LearningPathPrerequisitesIncomplete + "\" />"));
            overdueView.AddProperty(new GridColumnProperty("5", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.Locked + "\" />"));

            // add columns to data grid
            overdueGrid.AddColumn(overdueCourse);
            overdueGrid.AddColumn(overdueProgress);
            overdueGrid.AddColumn(overdueDueDate);
            overdueGrid.AddColumn(overdueExpires);

            // build and add wall column if the course discussion feature is enabled
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE)){
                GridColumn overdueWall = new GridColumn(_GlobalResources.Discussion, "wall", true);
                overdueWall.AddProperty(new GridColumnProperty("True", "<a href=\"/courses/Wall.aspx?id=##idCourse##\">"
                                                                    + "<img class=\"SmallIcon\" src=\""
                                                                    + ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION_BUTTON,
                                                                                             ImageFiles.EXT_PNG)
                                                                    + "\" alt=\"" + _GlobalResources.Discussion + "\" />"
                                                                    + "</a>"));
                overdueWall.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                    + ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION_BUTTON,
                                                                                             ImageFiles.EXT_PNG)
                                                                    + "\" alt=\"" + _GlobalResources.Discussion + "\" />"));
                overdueGrid.AddColumn(overdueWall);
            }

            overdueGrid.AddColumn(overdueView);
            overdueGrid.RowDataBound += this._EnrollmentsWidgetGrid_RowDataBound;

            overduePanel.ContentTemplateContainer.Controls.Add(overdueGrid);
            loadedWidgetContent.Controls.Add(overduePanel);
            // END OVERDUE GRID

            // BEGIN COMPLETED GRID
            UpdatePanel completedPanel = new UpdatePanel();
            completedPanel.ID = widgetId + "_Completed_TabPanel";
            completedPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            completedPanel.Attributes.Add("style", "display: none;");

            Grid completedGrid = new Grid("Account", true);

            completedGrid.ID = widgetId + "CompletedGrid";
            completedGrid.StoredProcedure = Enrollment.GridProcedureForUserDashboard;
            completedGrid.DefaultRecordsPerPage = 10;
            completedGrid.ShowTimeInDateStrings = true;
            completedGrid.EmptyDataText = _GlobalResources.NoEnrollmentsCompletedWIthinLast30Days;
            completedGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            completedGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            completedGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            completedGrid.AddFilter("@enrollmentStatus", SqlDbType.NVarChar, 20, "completed");
            completedGrid.IdentifierField = "idEnrollment";
            completedGrid.DefaultSortColumn = "title";

            // data key names
            completedGrid.DataKeyNames = new string[] { "idEnrollment", "idCourse", "title" };

            // columns
            GridColumn completedCourse = new GridColumn(_GlobalResources.Course, "title", "title");
            GridColumn completedProgress = new GridColumn(_GlobalResources.Progress, null, true, true); // calculated in row databound event
            GridColumn completedCompletedDate = new GridColumn(_GlobalResources.DateCompleted, "dtCompleted", "dtCompleted", false, true);
            GridColumn completedExpires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires", false, true);

            GridColumn completedView = new GridColumn(_GlobalResources.Review, "launch", true);
            completedView.AddProperty(new GridColumnProperty("1", "<a href=\"/dashboard/Enrollment.aspx?idEnrollment=##idEnrollment##\">"
                                                            + "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO_REVIEW,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Review
                                                            + "\" title=\"" + _GlobalResources.ReviewCourse + "\" />"
                                                            + "</a>"));
            completedView.AddProperty(new GridColumnProperty("2", "<img class=\"SmallIcon DimIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO_REVIEW,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View
                                                            + "\" title=\"" + _GlobalResources.ViewCourseUnavailable + "\" />"));
            completedView.AddProperty(new GridColumnProperty("3", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.PrerequisitesIncomplete + "\" />"));
            completedView.AddProperty(new GridColumnProperty("4", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.LearningPathPrerequisitesIncomplete + "\" />"));
            completedView.AddProperty(new GridColumnProperty("5", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.Locked + "\" />"));

            // add columns to data grid
            completedGrid.AddColumn(completedCourse);
            completedGrid.AddColumn(completedProgress);
            completedGrid.AddColumn(completedCompletedDate);
            completedGrid.AddColumn(completedExpires);

            // build and add wall column if the course discussion feature is enabled
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE))
            {
                GridColumn completedWall = new GridColumn(_GlobalResources.Discussion, "wall", true);
                completedWall.AddProperty(new GridColumnProperty("True", "<a href=\"/courses/Wall.aspx?id=##idCourse##\">"
                                                                    + "<img class=\"SmallIcon\" src=\""
                                                                    + ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION_BUTTON,
                                                                                             ImageFiles.EXT_PNG)
                                                                    + "\" alt=\"" + _GlobalResources.Discussion + "\" />"
                                                                    + "</a>"));
                completedWall.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                    + ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION_BUTTON,
                                                                                             ImageFiles.EXT_PNG)
                                                                    + "\" alt=\"" + _GlobalResources.Discussion + "\" />"));

                completedGrid.AddColumn(completedWall);
            }
            completedGrid.AddColumn(completedView);
            completedGrid.RowDataBound += this._EnrollmentsWidgetGrid_RowDataBound;

            completedPanel.ContentTemplateContainer.Controls.Add(completedGrid);
            loadedWidgetContent.Controls.Add(completedPanel);
            // END COMPLETED GRID

            // BEGIN EXPIRED GRID
            UpdatePanel expiredPanel = new UpdatePanel();
            expiredPanel.ID = widgetId + "_Expired_TabPanel";
            expiredPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            expiredPanel.Attributes.Add("style", "display: none;");

            Grid expiredGrid = new Grid("Account", true);

            expiredGrid.ID = widgetId + "ExpiredGrid";
            expiredGrid.StoredProcedure = Enrollment.GridProcedureForUserDashboard;
            expiredGrid.DefaultRecordsPerPage = 10;
            expiredGrid.ShowTimeInDateStrings = true;
            expiredGrid.EmptyDataText = _GlobalResources.NoEnrollmentsExpiredWIthinLast30Days;
            expiredGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            expiredGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            expiredGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            expiredGrid.AddFilter("@enrollmentStatus", SqlDbType.NVarChar, 20, "expired");
            expiredGrid.IdentifierField = "idEnrollment";
            expiredGrid.DefaultSortColumn = "title";

            // data key names
            expiredGrid.DataKeyNames = new string[] { "idEnrollment", "idCourse" };

            // columns
            GridColumn expiredCourse = new GridColumn(_GlobalResources.Course, "title", "title");
            GridColumn expiredProgress = new GridColumn(_GlobalResources.Progress, null, true, true); // calculated in row databound event
            GridColumn expiredDueDate = new GridColumn(_GlobalResources.DueDate, "dtDue", "dtDue", false, true);
            GridColumn expiredExpires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires", false, true);

            GridColumn expiredView = new GridColumn(_GlobalResources.View, "launch", true);
            expiredView.AddProperty(new GridColumnProperty("1", "<a href=\"/dashboard/Enrollment.aspx?idEnrollment=##idEnrollment##\">"
                                                            + "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View
                                                            + "\" title=\"" + _GlobalResources.ViewCourse + "\" />"
                                                            + "</a>"));
            expiredView.AddProperty(new GridColumnProperty("2", "<img class=\"SmallIcon DimIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View
                                                            + "\" title=\"" + _GlobalResources.ViewCourseUnavailable + "\" />"));
            expiredView.AddProperty(new GridColumnProperty("3", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.PrerequisitesIncomplete + "\" />"));
            expiredView.AddProperty(new GridColumnProperty("4", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.LearningPathPrerequisitesIncomplete + "\" />"));
            expiredView.AddProperty(new GridColumnProperty("5", "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.Locked
                                                            + "\" title=\"" + _GlobalResources.Locked + "\" />"));

            // add columns to data grid
            expiredGrid.AddColumn(expiredCourse);
            expiredGrid.AddColumn(expiredProgress);
            expiredGrid.AddColumn(expiredDueDate);
            expiredGrid.AddColumn(expiredExpires);

            // build and add wall column if the course discussion feature is enabled
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE))
            {
                GridColumn expiredWall = new GridColumn(_GlobalResources.Discussion, "wall", true);
                expiredWall.AddProperty(new GridColumnProperty("True", "<a href=\"/courses/Wall.aspx?id=##idCourse##\">"
                                                                    + "<img class=\"SmallIcon\" src=\""
                                                                    + ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION_BUTTON,
                                                                                             ImageFiles.EXT_PNG)
                                                                    + "\" alt=\"" + _GlobalResources.Discussion + "\" />"
                                                                    + "</a>"));
                expiredWall.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                    + ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION_BUTTON,
                                                                                             ImageFiles.EXT_PNG)
                                                                    + "\" alt=\"" + _GlobalResources.Discussion + "\" />"));
                expiredGrid.AddColumn(expiredWall);
            }

            expiredGrid.AddColumn(expiredView);
            expiredGrid.RowDataBound += this._EnrollmentsWidgetGrid_RowDataBound;

            expiredPanel.ContentTemplateContainer.Controls.Add(expiredGrid);
            loadedWidgetContent.Controls.Add(expiredPanel);
            // END EXPIRED GRID
            // END GRIDS

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            {
                enrolledGrid.BindData();
                pendingGrid.BindData();
                overdueGrid.BindData();
                completedGrid.BindData();
                expiredGrid.BindData();

                // re-do the tabs, this time with numbers in the titles
                Panel tabsPanel = (Panel)this.FindControl(widgetId + "_TabsPanel");

                if (tabsPanel != null)
                {
                    // clear the panel and tabs queue
                    tabsPanel.Controls.Clear();
                    tabs.Clear();

                    // queue up the tabs                    
                    tabs.Enqueue(new KeyValuePair<string, string>("Enrolled", _GlobalResources.Enrolled + " (" + enrolledGrid.RowCount.ToString() + ")"));
                    tabs.Enqueue(new KeyValuePair<string, string>("Pending", _GlobalResources.Pending + " (" + pendingGrid.RowCount.ToString() + ")"));
                    tabs.Enqueue(new KeyValuePair<string, string>("Overdue", _GlobalResources.Overdue + " (" + overdueGrid.RowCount.ToString() + ")"));
                    tabs.Enqueue(new KeyValuePair<string, string>("Completed", _GlobalResources.Completed + " (" + completedGrid.RowCount.ToString() + ")"));
                    tabs.Enqueue(new KeyValuePair<string, string>("Expired", _GlobalResources.Expired + " (" + expiredGrid.RowCount.ToString() + ")"));

                    // re-build the tabs                    
                    AsentiaPage.BuildTabListPanel(widgetId, tabs, tabsPanel);
                }                

                //show the ! icon on enrollment widgetbar icon if there aren any overdues
                if (overdueGrid.RowCount > 0)
                { enrollmentWidgetHiddenForShowingAlertIcon.Value = "1"; }
                else { enrollmentWidgetHiddenForShowingAlertIcon.Value = "0"; }

                // SCRIPTING
                
                // initialize a specialized version of tooltips for unscheduled ilt
                StringBuilder globalJs = new StringBuilder();                
                globalJs.AppendLine("$(\".EnrollmentsWidgetUnscheduledILTAlert img\").tooltip({");
                globalJs.AppendLine("    position: {");
                globalJs.AppendLine("        my: \"left bottom+45\",");
                globalJs.AppendLine("        at: \"left bottom\",");
                globalJs.AppendLine("        collision: \"flip\",");
                globalJs.AppendLine("        using: function (position, feedback) {");
                globalJs.AppendLine("            $(this).css(position);");
                globalJs.AppendLine("");
                globalJs.AppendLine("            if (feedback.horizontal == \"right\") {");
                globalJs.AppendLine("                //$(this).addClass(\"ui-tooltip-right\");");
                globalJs.AppendLine("            }");
                globalJs.AppendLine("            else {");
                globalJs.AppendLine("                //$(this).addClass(\"ui-tooltip-left\");");
                globalJs.AppendLine("            }");
                globalJs.AppendLine("        }");
                globalJs.AppendLine("    }");
                globalJs.AppendLine("});");

                // add start up script(s)
                ScriptManager.RegisterStartupScript(this.Page, typeof(Asentia.LMS.Controls.DashboardWidgetContainer), "EnrollmentsWidgetGlobalJs", globalJs.ToString(), true);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "AlertIconForOverDueEnrollmentsWidget", "ShowHideAlertIcon('" + enrollmentWidgetHiddenForShowingAlertIcon.ID + "','" + _ENROLLMENT_WIDGET_ID + "');", true);
            }
        }
        #endregion

        #region _BuildEnrollmentsTileWidget
        /// <summary>
        /// Builds the enrollment widget in tile mode
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildEnrollmentsTileWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            HiddenField enrollmentWidgetHiddenForShowingAlertIcon = new HiddenField();
            enrollmentWidgetHiddenForShowingAlertIcon.ID = widgetId + "HiddenForShowingAlertIcon";
            enrollmentWidgetHiddenForShowingAlertIcon.Value = "0";
            widgetContent.ContentTemplateContainer.Controls.Add(enrollmentWidgetHiddenForShowingAlertIcon);

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // container
            Panel enrollmentsCarouselContainer = new Panel();
            enrollmentsCarouselContainer.ID = widgetId + "EnrollmentsCarouselContainer";
            enrollmentsCarouselContainer.CssClass = "EnrollmentsCarouselControl";
            enrollmentsCarouselContainer.Style.Add("padding", "0px 35px");

            loadedWidgetContent.Controls.Add(enrollmentsCarouselContainer);

            Panel enrollmentTile = new Panel();
            double percentageCompleted = 0;
            int numLessons = 0;
            int numLessonsCompleted = 0;

            int tileCount = 0;


            // load overdue tiles
            DataTable dt = Enrollment.GetEnrollmentDataTableForUser("overdue");
            tileCount += dt.Rows.Count;

            foreach (DataRow row in dt.Rows)
            {

                // PROGRESS
                numLessons = Convert.ToInt32(row["numLessons"]); // will never be null
                numLessonsCompleted = Convert.ToInt32(row["numLessonsCompleted"]); // will never be null

                // only calculate percentage and populate text if the number of lessons is greater than 0
                if (numLessons > 0)
                { percentageCompleted = ((double)numLessonsCompleted / (double)numLessons) * 100; }

                // if the percentage completed is 0 and there has been at least a first launch, set the percentage completed to 5
                // so we will at least show some type of progress, this is for asthetics
                if (percentageCompleted == 0 && !String.IsNullOrWhiteSpace(row["dtFirstLaunch"].ToString()))
                { percentageCompleted = 5; }

                DateTime? dueDate = null;
                DateTime? expiresDate = null;

                if (row["dtDue"] != DBNull.Value)
                {
                    dueDate = Convert.ToDateTime(row["dtDue"]);
                }

                if (row["dtExpires"] != DBNull.Value)
                {
                    expiresDate = Convert.ToDateTime(row["dtExpires"]);
                }

                enrollmentTile = this._BuildTile(Convert.ToInt32(row["idEnrollment"].ToString()), Convert.ToInt32(row["idCourse"].ToString()), "overdue", HttpContext.Current.Server.HtmlEncode(row["title"].ToString()), dueDate, expiresDate, percentageCompleted, row["avatar"].ToString(), Convert.ToInt32(row["launch"].ToString()), Convert.ToBoolean(row["wall"]));

                enrollmentsCarouselContainer.Controls.Add(enrollmentTile);

            }

            // load enrolled tiles
            dt = Enrollment.GetEnrollmentDataTableForUser("enrolled");
            tileCount += dt.Rows.Count;

            foreach (DataRow row in dt.Rows)
            {

                // PROGRESS
                numLessons = Convert.ToInt32(row["numLessons"]); // will never be null
                numLessonsCompleted = Convert.ToInt32(row["numLessonsCompleted"]); // will never be null

                // only calculate percentage and populate text if the number of lessons is greater than 0
                if (numLessons > 0)
                { percentageCompleted = ((double)numLessonsCompleted / (double)numLessons) * 100; }

                // if the percentage completed is 0 and there has been at least a first launch, set the percentage completed to 5
                // so we will at least show some type of progress, this is for asthetics
                if (percentageCompleted == 0 && !String.IsNullOrWhiteSpace(row["dtFirstLaunch"].ToString()))
                { percentageCompleted = 5; }

                DateTime? dueDate = null;
                DateTime? expiresDate = null;

                if (row["dtDue"] != DBNull.Value)
                {
                    dueDate = Convert.ToDateTime(row["dtDue"]);
                }

                if (row["dtExpires"] != DBNull.Value)
                {
                    expiresDate = Convert.ToDateTime(row["dtExpires"]);
                }

                enrollmentTile = this._BuildTile(Convert.ToInt32(row["idEnrollment"].ToString()), Convert.ToInt32(row["idCourse"].ToString()), "enrolled", HttpContext.Current.Server.HtmlEncode(row["title"].ToString()), dueDate, expiresDate, percentageCompleted, row["avatar"].ToString(), Convert.ToInt32(row["launch"].ToString()), Convert.ToBoolean(row["wall"]));

                enrollmentsCarouselContainer.Controls.Add(enrollmentTile);

            }

            // load completed tiles
            dt = Enrollment.GetEnrollmentDataTableForUser("completed");
            tileCount += dt.Rows.Count;

            foreach (DataRow row in dt.Rows)
            {

                // PROGRESS
                numLessons = Convert.ToInt32(row["numLessons"]); // will never be null
                numLessonsCompleted = Convert.ToInt32(row["numLessonsCompleted"]); // will never be null

                // only calculate percentage and populate text if the number of lessons is greater than 0
                if (numLessons > 0)
                { percentageCompleted = ((double)numLessonsCompleted / (double)numLessons) * 100; }

                // if the percentage completed is 0 and there has been at least a first launch, set the percentage completed to 5
                // so we will at least show some type of progress, this is for asthetics
                if (percentageCompleted == 0 && !String.IsNullOrWhiteSpace(row["dtFirstLaunch"].ToString()))
                { percentageCompleted = 5; }

                DateTime? dueDate = null;
                DateTime? expiresDate = null;

                if (row["dtDue"] != DBNull.Value)
                {
                    dueDate = Convert.ToDateTime(row["dtDue"]);
                }

                if (row["dtExpires"] != DBNull.Value)
                {
                    expiresDate = Convert.ToDateTime(row["dtExpires"]);
                }

                enrollmentTile = this._BuildTile(Convert.ToInt32(row["idEnrollment"].ToString()), Convert.ToInt32(row["idCourse"].ToString()), "completed", HttpContext.Current.Server.HtmlEncode(row["title"].ToString()), dueDate, expiresDate, percentageCompleted, row["avatar"].ToString(), Convert.ToInt32(row["launch"].ToString()), Convert.ToBoolean(row["wall"]));

                enrollmentsCarouselContainer.Controls.Add(enrollmentTile);

            }

            // load expired tiles
            dt = Enrollment.GetEnrollmentDataTableForUser("expired");
            tileCount += dt.Rows.Count;

            foreach (DataRow row in dt.Rows)
            {

                // PROGRESS
                numLessons = Convert.ToInt32(row["numLessons"]); // will never be null
                numLessonsCompleted = Convert.ToInt32(row["numLessonsCompleted"]); // will never be null

                // only calculate percentage and populate text if the number of lessons is greater than 0
                if (numLessons > 0)
                { percentageCompleted = ((double)numLessonsCompleted / (double)numLessons) * 100; }

                // if the percentage completed is 0 and there has been at least a first launch, set the percentage completed to 5
                // so we will at least show some type of progress, this is for asthetics
                if (percentageCompleted == 0 && !String.IsNullOrWhiteSpace(row["dtFirstLaunch"].ToString()))
                { percentageCompleted = 5; }

                DateTime? dueDate = null;
                DateTime? expiresDate = null;

                if (row["dtDue"] != DBNull.Value)
                {
                    dueDate = Convert.ToDateTime(row["dtDue"]);
                }

                if (row["dtExpires"] != DBNull.Value)
                {
                    expiresDate = Convert.ToDateTime(row["dtExpires"]);
                }

                enrollmentTile = this._BuildTile(Convert.ToInt32(row["idEnrollment"].ToString()), Convert.ToInt32(row["idCourse"].ToString()), "expired", HttpContext.Current.Server.HtmlEncode(row["title"].ToString()), dueDate, expiresDate, percentageCompleted, row["avatar"].ToString(), Convert.ToInt32(row["launch"].ToString()), Convert.ToBoolean(row["wall"]));

                enrollmentsCarouselContainer.Controls.Add(enrollmentTile);

            }

            // Load Pending Tiles
            dt = EnrollmentRequest.GetEnrollmentRequestDataTableForUser();
            tileCount += dt.Rows.Count;

            foreach (DataRow row in dt.Rows)
            {

                enrollmentTile = this._BuildTile(Convert.ToInt32(row["idEnrollmentRequest"].ToString()), Convert.ToInt32(row["idCourse"].ToString()), "pending", HttpContext.Current.Server.HtmlEncode(row["title"].ToString()), null, null, 0, row["avatar"].ToString(), 1, false);

                enrollmentsCarouselContainer.Controls.Add(enrollmentTile);

            }

            // if there are no tiles, then we need to show a message indicating no enrollments
            if (tileCount == 0)
            {
                enrollmentsCarouselContainer.Style.Add("text-align", "center");
                Literal noEnrollmentsMessage = new Literal();
                noEnrollmentsMessage.Text = _GlobalResources.NoEnrollmentsFound;
                enrollmentsCarouselContainer.Controls.Add(noEnrollmentsMessage);
            }

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // initialize the carousel
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "EnrollmentsCarousel", "$(\".EnrollmentsCarouselControl\").not('.slick-initialized').slick({dots: true, infinite: false, slidesToShow: 1, slidesToScroll: Math.floor($(window).width() / 300), variableWidth:true});", true);
        }
        #endregion

        #region BuildTile
        /// <summary>
        /// Builds enrollment tile.
        /// </summary>
        /// <param name="idEnrollment">enrollment id</param>
        /// <param name="idCourse">course id</param>
        /// <param name="status">enrollment status (overdue, enrolled, completed, pending, expired)</param>
        /// <param name="enrollmentTitle">title of the enrollment</param>
        /// <param name="dtDue">enrollment due date</param>
        /// <param name="dtExpires">enrollment expiration date</param>
        /// <param name="percentageCompleted">enrollment percentage completed</param>
        /// <param name="avatar">avatar</param>
        /// <param name="locked">enrollment locked (2 = expired, 3 = locked by pre-requisites, 4 = locked by learning path ordering, 5 = locked</param>
        /// <param name="isWallActive">is the discussion feed active?</param>
        /// <returns></returns>
        private Panel _BuildTile(int idEnrollment, int idCourse, string status, string enrollmentTitle, DateTime? dtDue, DateTime? dtExpires, double percentageCompleted, string avatar, int locked, bool isWallActive)
        {

            string avatarPath = String.Empty;
            string avatarImageClass = String.Empty;
            if (!String.IsNullOrWhiteSpace(avatar))
            {
                avatarPath = SitePathConstants.SITE_COURSES_ROOT + idCourse.ToString() + "/" + avatar;
                avatarImageClass = "AvatarImage";
            }
            else
            { avatarPath = ImageFiles.GetAvatarPath(ImageFiles.AVATAR_CATALOG_COURSE, ImageFiles.EXT_PNG); }

            string controlIdEnd = idEnrollment.ToString();
            // need to add pending to the end of id's for pending enrollments, because idEnrollment is
            // really idEnrollmentRequest, and there is a chance that there could be an idEnrollment that
            // equals an idEnrollmentRequest.
            if (status == "pending")
            {
                controlIdEnd = "pending_" + idEnrollment.ToString();
            }
            

            // tile wrapper
            Panel tile = new Panel();
            tile.ID = "Tile_" + controlIdEnd;
            tile.CssClass = "Tile";
            
            if (locked >= 3)
            {
                tile.CssClass += " TileEnrollmentLocked";

            }
            else if (status == "overdue")
            {
                tile.CssClass += " TileEnrollmentOverdue";
            }
            else if(status == "expired")
            {
                tile.CssClass += " TileEnrollmentExpired";
            }
            else if (status == "completed")
            {
                tile.CssClass += " TileEnrollmentCompleted";
            }
            else if (status == "pending")
            {
                tile.CssClass += " TileEnrollmentPending";
            }

            
            // tile content outer wrapper
            Panel tileOuterWrapper = new Panel();
            tileOuterWrapper.ID = "TileContentOuterWrapper_" + controlIdEnd;
            tileOuterWrapper.CssClass = "TileContentOuterWrapper";
            tile.Controls.Add(tileOuterWrapper);

            // tile content inner wrapper
            Panel tileInnerWrapper = new Panel();
            tileInnerWrapper.ID = "TileContentInnerWrapper_" + controlIdEnd;
            tileInnerWrapper.CssClass = "TileContentInnerWrapper";
            tileOuterWrapper.Controls.Add(tileInnerWrapper);

            // tile image panel
            Panel tileImage = new Panel();
            tileImage.ID = "TileImage_" + controlIdEnd;
            tileImage.CssClass = "TileEnrollmentImg";
            tileInnerWrapper.Controls.Add(tileImage);

            // tile content panel
            Panel tileContent = new Panel();
            tileContent.ID = "TileContent_" + controlIdEnd;
            tileContent.CssClass = "TileEnrollmentContent";
            tileInnerWrapper.Controls.Add(tileContent);

            // tile content top
            Panel tileContentTop = new Panel();
            tileContentTop.ID = "TileContentTop_" + controlIdEnd;
            tileContentTop.CssClass = "TileContentTop";
            tileContent.Controls.Add(tileContentTop);


            // tile content bottom
            Panel tileContentBottom = new Panel();
            tileContentBottom.ID = "TileContentBottom_" + controlIdEnd;
            tileContentBottom.CssClass = "TileEnrollmentFooter";
                

                

            // status - if the status is enrolled and enrollment is not locked, we will not show status
            if (status != "enrolled" || locked >= 3)
            {
                Panel statusPanel = new Panel();
                statusPanel.CssClass = "TileEnrollmentStatus";
                Literal statusLiteral = new Literal();
                statusPanel.Controls.Add(statusLiteral);
                tileImage.Controls.Add(statusPanel);

                if (locked >= 3)
                {
                    statusLiteral.Text = _GlobalResources.Locked;
                }
                else if (status == "overdue")
                {
                    statusLiteral.Text = _GlobalResources.Overdue;
                }
                else if (status == "completed")
                {
                    statusLiteral.Text = _GlobalResources.Completed;

                    // Add an enrollment check for completed enrollments
                    Panel enrollmentCheckPanel = new Panel();
                    enrollmentCheckPanel.CssClass = "TileEnrollmentCheck";

                    Image enrollmentCheckImage = new Image();
                    enrollmentCheckImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                    enrollmentCheckPanel.Controls.Add(enrollmentCheckImage);

                    tileImage.Controls.Add(enrollmentCheckPanel);
                }
                else if (status == "expired")
                {
                    statusLiteral.Text = _GlobalResources.Expired;
                }
                else if (status == "pending")
                {
                    statusLiteral.Text = _GlobalResources.Pending;
                }
            }

            Panel statusIconContainer = new Panel();
            statusIconContainer.CssClass = "TileEnrollmentAction";


            HyperLink statusIconLink = new HyperLink();
                
            Image statusIcon = new Image();
                

            // if it's locked, show lock icon
            if (locked == 3) // locked by pre-rerquisites
            {
                    
                statusIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_RED, ImageFiles.EXT_PNG);
                statusIcon.AlternateText = _GlobalResources.LockedByPrerequisites;
                statusIcon.ToolTip = _GlobalResources.LockedByPrerequisites;
                statusIconLink.Enabled = false;
                    
            }
            else if (locked == 4) // locked by learning path ordering
            {
                statusIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_RED, ImageFiles.EXT_PNG);
                statusIcon.AlternateText = _GlobalResources.LockedByLearningPathOrdering;
                statusIcon.ToolTip = _GlobalResources.LockedByLearningPathOrdering;
                statusIconLink.Enabled = false;
            }
            else if (locked == 5) // course is locked
            {
                statusIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_RED, ImageFiles.EXT_PNG);
                statusIcon.AlternateText = _GlobalResources.Locked;
                statusIcon.ToolTip = _GlobalResources.Locked;
                statusIconLink.Enabled = false;
            }
            else if (status == "completed") // show review icon for completed enrollments
            {
                statusIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO_REVIEW, ImageFiles.EXT_PNG);
                statusIcon.AlternateText = _GlobalResources.ReviewCourse;
                statusIcon.ToolTip = _GlobalResources.ReviewCourse;
                statusIconLink.NavigateUrl = "/dashboard/Enrollment.aspx?idEnrollment=" + idEnrollment.ToString();
            }
            else if (status == "enrolled" || status == "overdue") // show launch icon for enrolled or overdue enrollments
            {
                statusIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                statusIcon.AlternateText = _GlobalResources.Launch;
                statusIcon.ToolTip = _GlobalResources.Launch;
                statusIconLink.NavigateUrl = "/dashboard/Enrollment.aspx?idEnrollment=" + idEnrollment.ToString();
                
            }
            else if (status == "expired") // dim expired enrollments status link
            {
                statusIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                statusIconLink.Enabled = false;
                statusIcon.CssClass += " DimIcon";
            }
            else // pending enrollments cannot launch course, but show that they will be launchable after approval
            {
                statusIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                statusIconLink.Enabled = false;
                        
            }
               
            statusIconLink.Controls.Add(statusIcon);
            statusIconContainer.Controls.Add(statusIconLink);
            tileImage.Controls.Add(statusIconContainer);

            // object avatar              
            Panel objectAvatarPanel = new Panel();

            HyperLink enrollmentHyperLink = new HyperLink();
            enrollmentHyperLink.CssClass = "TileEnrollmentImg";


            Image objectAvatarImage = new Image();
            objectAvatarImage.AlternateText = enrollmentTitle;
            objectAvatarImage.ImageUrl = avatarPath;

            if (!String.IsNullOrWhiteSpace(avatarImageClass))
            { objectAvatarImage.CssClass += " " + avatarImageClass; }

            tileImage.Controls.Add(objectAvatarImage);
                
            Panel objectCodeWrapper = new Panel();
            objectCodeWrapper.CssClass = "TileTitle";
            tileContent.Controls.Add(objectCodeWrapper);

            Literal objectCodeText = new Literal();
            objectCodeText.Text = enrollmentTitle;
            objectCodeWrapper.Controls.Add(objectCodeText);

            Panel enrollmentDetailsPanel = new Panel();
            enrollmentDetailsPanel.ID = "EnrollmentDetails_" + controlIdEnd;
            enrollmentDetailsPanel.CssClass = "TileEnrollmentDetails";

            Panel enrollmentDueDatePanel = new Panel();
            Literal enrollmentDueDateLiteral = new Literal();
            enrollmentDueDateLiteral.Text = "<strong>" + _GlobalResources.Due + ": </Strong>";
            if (dtDue != null)
            {
                enrollmentDueDateLiteral.Text = enrollmentDueDateLiteral.Text + dtDue.ToString();
            }
            else
            {
                enrollmentDueDateLiteral.Text = enrollmentDueDateLiteral.Text + _GlobalResources.NoDueDate;
            }
            enrollmentDueDatePanel.Controls.Add(enrollmentDueDateLiteral);
            enrollmentDetailsPanel.Controls.Add(enrollmentDueDatePanel);

            Panel enrollmentExpiresDatePanel = new Panel();
            Literal enrollmentExpiresDateLiteral = new Literal();
            enrollmentExpiresDateLiteral.Text = "<strong>" + _GlobalResources.Expires + ": </strong>";
            if (dtExpires != null)
            {
                enrollmentExpiresDateLiteral.Text = enrollmentExpiresDateLiteral.Text + dtExpires.ToString();
            }
            else
            {
                enrollmentExpiresDateLiteral.Text = enrollmentExpiresDateLiteral.Text + _GlobalResources.NeverExpires;
            }
            enrollmentExpiresDatePanel.Controls.Add(enrollmentExpiresDateLiteral);
            enrollmentDetailsPanel.Controls.Add(enrollmentExpiresDatePanel);

            tileContent.Controls.Add(enrollmentDetailsPanel);

            tileInnerWrapper.Controls.Add(tileContentBottom);

            // tile content bottom left
            Panel tileContentBottomLeft = new Panel();
            tileContentBottomLeft.ID = "TileContentBottomLeft_" + controlIdEnd;
            tileContentBottomLeft.CssClass = "TileEnrollmentProgressBarContainer";
            tileContentBottom.Controls.Add(tileContentBottomLeft);

            ProgressIndicator enrollmentProgress = new ProgressIndicator("Enrollment_" + controlIdEnd + "Progress", (int)Math.Round(percentageCompleted, 0), null);
            tileContentBottomLeft.Controls.Add(enrollmentProgress);

            // discussion link - only if the feature is enabled
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE))
            {
                HyperLink discussionLink = new HyperLink();
                discussionLink.ID = "DiscussionLink_" + controlIdEnd;
                discussionLink.CssClass = "TileDiscussion";
                discussionLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION_BUTTON, ImageFiles.EXT_PNG);

                if (isWallActive && status != "pending")
                {
                    discussionLink.NavigateUrl = "/courses/Wall.aspx?id=" + idCourse.ToString();
                    discussionLink.ToolTip = _GlobalResources.DiscussionFeed;
                }
                else
                {
                    discussionLink.Enabled = false;
                    discussionLink.CssClass += " DimIcon";
                }
                tileContentBottom.Controls.Add(discussionLink);
            }
                

            return tile;
        }
        #endregion

        #region _EnrollmentsWidgetGrid_RowDataBound
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _EnrollmentsWidgetGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // get data values for row
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idEnrollment = Convert.ToInt32(rowView["idEnrollment"]); // will never be null
                string dtFirstLaunch = rowView["dtFirstLaunch"].ToString();
                
                // PROGRESS

                int numLessons = Convert.ToInt32(rowView["numLessons"]); // will never be null
                int numLessonsCompleted = Convert.ToInt32(rowView["numLessonsCompleted"]); // will never be null    
                double percentageCompleted = 0;
                string progressText = null;

                // only calculate percentage and populate text if the number of lessons is greater than 0
                if (numLessons > 0)
                { percentageCompleted = ((double)numLessonsCompleted / (double)numLessons) * 100; }

                // if the percentage completed is 0 and there has been at least a first launch, set the percentage completed to 5
                // so we will at least show some type of progress, this is for asthetics
                if (percentageCompleted == 0 && !String.IsNullOrWhiteSpace(dtFirstLaunch))
                { percentageCompleted = 5; }

                // attach a progress indicator
                ProgressIndicator enrollmentProgress = new ProgressIndicator("Enrollment_" + idEnrollment.ToString() + "Progress_", (int)Math.Round(percentageCompleted, 0), progressText);
                e.Row.Cells[1].Controls.Add(enrollmentProgress);
          
                // UNSCHEDULED ILT - If there is unscheduled ILT, put an alert next to the title.

                bool hasUnscheduledILT = Convert.ToBoolean(rowView["hasUnscheduledILT"]);

                if (hasUnscheduledILT)
                {
                    string existingTitleText = e.Row.Cells[0].Text;

                    // create a span for the title text
                    Label titleText = new Label();
                    titleText.CssClass = "EnrollmentsWidgetCourseTitleText";
                    titleText.Text = existingTitleText;
                    e.Row.Cells[0].Controls.Add(titleText);

                    // create a span and image for the alert
                    Label unscheduledILTAlertWrapper = new Label();
                    unscheduledILTAlertWrapper.CssClass = "EnrollmentsWidgetUnscheduledILTAlert";
                    e.Row.Cells[0].Controls.Add(unscheduledILTAlertWrapper);

                    Image unscheduledILTAlert = new Image();
                    unscheduledILTAlert.CssClass = "SmallIcon";
                    unscheduledILTAlert.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG);
                    unscheduledILTAlert.AlternateText = _GlobalResources.Alert;
                    unscheduledILTAlert.ToolTip = _GlobalResources.ThisEnrollmentHasInstructorLedTrainingYouHaveNotYetRegisteredForPleaseClickTheEnrollmentViewButtonToRegisterForInstructorLedTrainingSession_sForThisEnrollment;
                    unscheduledILTAlertWrapper.Controls.Add(unscheduledILTAlert);
                }
            }
        }
        #endregion

        #region _BuildTranscriptWidget
        /// <summary>
        /// Builds Transcript Widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildTranscriptWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();
            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";
            loadedWidgetContent.CssClass = "TranscriptRecordUpdatePanel";

            // TABS
            Panel transcriptTabsPanel = new Panel();
            transcriptTabsPanel.ID = widgetId + "_TabsPanel";

            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Course", _GlobalResources.Course));

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                tabs.Enqueue(new KeyValuePair<string, string>("LearningPath", _GlobalResources.LearningPath));
            }
            
            tabs.Enqueue(new KeyValuePair<string, string>("ILT", _GlobalResources.ILT));

            // build the tabs
            AsentiaPage.BuildTabListPanel(widgetId, tabs, transcriptTabsPanel);

            if (!isInitialLoad)
            {
                // ADD OPEN TRANSCRIPT BUTTON TO TABS PANEL

                // open transcript link icon
                Image openTranscriptLinkIcon = new Image();
                openTranscriptLinkIcon.CssClass = "SmallIcon";
                openTranscriptLinkIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OPEN, ImageFiles.EXT_PNG);
                openTranscriptLinkIcon.AlternateText = _GlobalResources.Open;
                openTranscriptLinkIcon.ToolTip = _GlobalResources.Open;

                // open transcript link
                this._OpenTranscriptLink = new LinkButton();
                this._OpenTranscriptLink.ID = widgetId + "OpenTranscriptLink";
                this._OpenTranscriptLink.Controls.Add(openTranscriptLinkIcon);
                this._OpenTranscriptLink.OnClientClick = "javascript:OnTranscriptReportClick();return false;";
                transcriptTabsPanel.Controls.AddAt(0, this._OpenTranscriptLink);

                // SCRIPTING

                // prevent widget from scrolling the page with a startup JS
                StringBuilder globalJs = new StringBuilder();
                globalJs.AppendLine("PreventOuterScrolling(\"ReportModalPopupModalPopupBody\");");

                // add start up script
                ScriptManager.RegisterStartupScript(this.Page, typeof(Asentia.LMS.Controls.DashboardWidgetContainer), "TranscriptWidgetGlobalJs", globalJs.ToString(), true);
            }

            // attach tab panel control
            loadedWidgetContent.Controls.Add(transcriptTabsPanel);
            // END TABS

            // BEGIN COURSE TRANSCRIPT TAB CONTENT
            // "Course Transcript" is the default tab, so this is visible on page load.
            Panel courseTranscriptPanel = new Panel();
            courseTranscriptPanel.ID = widgetId + "_Course_TabPanel";
            courseTranscriptPanel.Attributes.Add("style", "display: block;");
            _BuildUserCourseTranscripts(courseTranscriptPanel, isInitialLoad);
            loadedWidgetContent.Controls.Add(courseTranscriptPanel);
            // END COURSE TRANSCRIPT TAB CONTENT

            // BEGIN LEARNING PATH TRANSCRIPT TAB CONTENT
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                Panel learningPathTranscriptPanel = new Panel();
                learningPathTranscriptPanel.ID = widgetId + "_LearningPath_TabPanel";
                learningPathTranscriptPanel.Attributes.Add("style", "display: none;");
                _BuildUserLearningPathTranscripts(learningPathTranscriptPanel, isInitialLoad);
                loadedWidgetContent.Controls.Add(learningPathTranscriptPanel);
            }
            // END COURSE TRANSCRIPT TAB CONTENT

            // BEGIN INSTRUCTOR LED TRANSCRIPT TAB CONTENT
            Panel instructorLedTranscriptPanel = new Panel();
            instructorLedTranscriptPanel.ID = widgetId + "_ILT_TabPanel";
            instructorLedTranscriptPanel.Attributes.Add("style", "display: none;");
            _BuildUserInstructorLedTranscripts(instructorLedTranscriptPanel, isInitialLoad);
            loadedWidgetContent.Controls.Add(instructorLedTranscriptPanel);
            // END COURSE TRANSCRIPT TAB CONTENT


            //// attach the control before any data bind occurs, if the control is attached
            //// after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

        }
        #endregion

        #region _BuildUserCourseTranscripts
        /// <summary>
        ///Builds User Course Transcript Tab Content
        /// </summary>        
        private void _BuildUserCourseTranscripts(Control loadedTabContent, bool isInitialLoad)
        {
            Panel quickViewPanel = new Panel();

            Report transcriptQuickView = new Report();

            // create quick view table for transcript widget
            Table tblTranscriptQuickView = new Table();
            tblTranscriptQuickView.ID = "TableCourseTranscriptQuickView";
            tblTranscriptQuickView.CssClass = "GridTable";

            // HEADER ROW AND COLUMNS

            // create the header row
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.TableSection = TableRowSection.TableHeader;
            headerRow.ID = "CourseTranscriptQuickViewTableHeaderRow";
            headerRow.CssClass = "GridHeaderRow";

            // course header cell
            TableHeaderCell courseHeader = new TableHeaderCell();
            courseHeader.ID = "CourseTranscriptQuickViewTableCourseColumn";
            HyperLink courseColumnName = new HyperLink();
            courseColumnName.Text = _GlobalResources.Course;
            courseColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableCourseTranscriptQuickView'; SortTable(0,'T');";
            courseHeader.Controls.Add(courseColumnName);
            headerRow.Cells.Add(courseHeader);

            // credits header cell
            TableHeaderCell creditsHeader = new TableHeaderCell();
            creditsHeader.ID = "CourseTranscriptQuickViewTableCreditsColumn";
            HyperLink creditsColumnName = new HyperLink();
            creditsColumnName.Text = _GlobalResources.Credits;
            creditsColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableCourseTranscriptQuickView'; SortTable(1,'N');";
            creditsHeader.Controls.Add(creditsColumnName);
            headerRow.Cells.Add(creditsHeader);

            // date completed header cell
            TableHeaderCell dateCompletedHeader = new TableHeaderCell();
            dateCompletedHeader.ID = "CourseTranscriptQuickViewTableDateCompletedColumn";
            HyperLink dateCompletedColumnName = new HyperLink();
            dateCompletedColumnName.Text = _GlobalResources.DateCompleted;
            dateCompletedColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableCourseTranscriptQuickView'; SortTable(2,'D');";
            dateCompletedHeader.Controls.Add(dateCompletedColumnName);
            headerRow.Cells.Add(dateCompletedHeader);

            // attach header row to table
            tblTranscriptQuickView.Rows.Add(headerRow);

            // loop through user account data tabs and build the table
            int i = 1;
            foreach (DataRow row in transcriptQuickView.GetPersonalTranscriptQuickView(EnumDataSet.UserCourseTranscripts).Rows)
            {
                i++;
                TableRow dataRow = new TableRow();
                // apply css for alternating rows
                if (i % 2 == 0)
                { dataRow.CssClass = "UserField GridDataRow"; }
                else
                { dataRow.CssClass = "UserField GridDataRow GridDataRowAlternate"; }

                TableCell courseCell = new TableCell();
                courseCell.Text = HttpContext.Current.Server.HtmlEncode(row["Course"].ToString());
                dataRow.Cells.Add(courseCell);

                TableCell creditsCell = new TableCell();
                creditsCell.Text = HttpContext.Current.Server.HtmlEncode(row["Credits"].ToString());
                dataRow.Cells.Add(creditsCell);

                TableCell dateCompletedCell = new TableCell();
                dateCompletedCell.Text = this._GetTranscriptFormattedDateTimeString(row["Date Completed"].ToString());
                dataRow.Cells.Add(dateCompletedCell);

                // attach data row to table
                tblTranscriptQuickView.Rows.Add(dataRow);
            }

            if (i == 1)
            {
                TableRow dataRow = new TableRow();
                dataRow.CssClass = "GridDataRowEmpty";
                TableCell rowEmptyMessageCell = new TableCell();
                rowEmptyMessageCell.ColumnSpan = 3;
                rowEmptyMessageCell.Text = _GlobalResources.NoRecordsFound;
                dataRow.Cells.Add(rowEmptyMessageCell);
                tblTranscriptQuickView.Rows.Add(dataRow);
            }

            quickViewPanel.Controls.Add(tblTranscriptQuickView);

            loadedTabContent.Controls.Add(quickViewPanel);
        }
        #endregion

        #region _BuildUserLearningPathTranscripts
        /// <summary>
        ///Builds User Learning Path Transcript Tab Content
        /// </summary>        
        private void _BuildUserLearningPathTranscripts(Control loadedTabContent, bool isInitialLoad)
        {
            Panel quickViewPanel = new Panel();

            Report transcriptQuickView = new Report();

            // create quick view table for transcript widget
            Table tblTranscriptQuickView = new Table();
            tblTranscriptQuickView.ID = "TableLearningPathTranscriptQuickView";
            tblTranscriptQuickView.CssClass = "GridTable";

            // HEADER ROW AND COLUMNS

            // create the header row
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.TableSection = TableRowSection.TableHeader;
            headerRow.ID = "LearningPathTranscriptQuickViewTableHeaderRow";
            headerRow.CssClass = "GridHeaderRow";

            // learning path header cell
            TableHeaderCell courseHeader = new TableHeaderCell();
            courseHeader.ID = "LearningPathTranscriptQuickViewTableCourseColumn";
            HyperLink courseColumnName = new HyperLink();
            courseColumnName.Text = _GlobalResources.LearningPath;
            courseColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableLearningPathTranscriptQuickView'; SortTable(0,'T');";
            courseHeader.Controls.Add(courseColumnName);
            headerRow.Cells.Add(courseHeader);

            // date completed header cell
            TableHeaderCell dateCompletedHeader = new TableHeaderCell();
            dateCompletedHeader.ID = "LearningPathTranscriptQuickViewTableDateCompletedColumn";
            HyperLink dateCompletedColumnName = new HyperLink();
            dateCompletedColumnName.Text = _GlobalResources.DateCompleted;
            dateCompletedColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableLearningPathTranscriptQuickView'; SortTable(1,'D');";
            dateCompletedHeader.Controls.Add(dateCompletedColumnName);
            headerRow.Cells.Add(dateCompletedHeader);

            // attach header row to table
            tblTranscriptQuickView.Rows.Add(headerRow);

            // loop through user account data tabs and build the table
            int i = 1;
            foreach (DataRow row in transcriptQuickView.GetPersonalTranscriptQuickView(EnumDataSet.UserLearningPathTranscripts).Rows)
            {
                i++;
                TableRow dataRow = new TableRow();
                // apply css for alternating rows
                if (i % 2 == 0)
                { dataRow.CssClass = "UserField GridDataRow"; }
                else
                { dataRow.CssClass = "UserField GridDataRow GridDataRowAlternate"; }

                TableCell courseCell = new TableCell();
                courseCell.Text = HttpContext.Current.Server.HtmlEncode(row["Learning Path"].ToString());
                dataRow.Cells.Add(courseCell);

                TableCell dateCompletedCell = new TableCell();
                dateCompletedCell.Text = this._GetTranscriptFormattedDateTimeString(row["Date Completed"].ToString());
                dataRow.Cells.Add(dateCompletedCell);

                // attach data row to table
                tblTranscriptQuickView.Rows.Add(dataRow);
            }

            if (i == 1)
            {
                TableRow dataRow = new TableRow();
                dataRow.CssClass = "GridDataRowEmpty";
                TableCell rowEmptyMessageCell = new TableCell();
                rowEmptyMessageCell.ColumnSpan = 3;
                rowEmptyMessageCell.Text = _GlobalResources.NoRecordsFound;
                dataRow.Cells.Add(rowEmptyMessageCell);
                tblTranscriptQuickView.Rows.Add(dataRow);
            }

            quickViewPanel.Controls.Add(tblTranscriptQuickView);

            loadedTabContent.Controls.Add(quickViewPanel);

        }
        #endregion

        #region _BuildUserInstructorLedTranscripts
        /// <summary>
        ///Builds user instructor led transcripts tab content
        /// </summary>        
        private void _BuildUserInstructorLedTranscripts(Control loadedTabContent, bool isInitialLoad)
        {
            Panel quickViewPanel = new Panel();

            Report transcriptQuickView = new Report();

            // create quick view table for transcript widget
            Table tblTranscriptQuickView = new Table();
            tblTranscriptQuickView.ID = "TableInstructorLedTranscriptQuickView";
            tblTranscriptQuickView.CssClass = "GridTable";

            // HEADER ROW AND COLUMNS

            // create the header row
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.TableSection = TableRowSection.TableHeader;
            headerRow.ID = "InstructorLedTranscriptQuickViewTableHeaderRow";
            headerRow.CssClass = "GridHeaderRow";

            // module header cell
            TableHeaderCell moduleHeader = new TableHeaderCell();
            moduleHeader.ID = "InstructorLedTranscriptQuickViewTableModuleColumn";
            HyperLink moduleColumnName = new HyperLink();
            moduleColumnName.Text = _GlobalResources.Module;
            moduleColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableInstructorLedTranscriptQuickView'; SortTable(0,'T');";
            moduleHeader.Controls.Add(moduleColumnName);
            headerRow.Cells.Add(moduleHeader);

            // session header cell
            TableHeaderCell sessionHeader = new TableHeaderCell();
            sessionHeader.ID = "InstructorLedTranscriptQuickViewTableSessionColumn";
            HyperLink sessionColumnName = new HyperLink();
            sessionColumnName.Text = _GlobalResources.Session;
            sessionColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableInstructorLedTranscriptQuickView'; SortTable(1,'T');";
            sessionHeader.Controls.Add(sessionColumnName);
            headerRow.Cells.Add(sessionHeader);

            // date completed header cell
            TableHeaderCell dateCompletedHeader = new TableHeaderCell();
            dateCompletedHeader.ID = "InstructorLedTranscriptQuickViewTableDateCompletedColumn";
            HyperLink dateCompletedColumnName = new HyperLink();
            dateCompletedColumnName.Text = _GlobalResources.DateCompleted;
            dateCompletedColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableInstructorLedTranscriptQuickView'; SortTable(2,'D');";
            dateCompletedHeader.Controls.Add(dateCompletedColumnName);
            headerRow.Cells.Add(dateCompletedHeader);

            // attach header row to table
            tblTranscriptQuickView.Rows.Add(headerRow);

            // loop through user account data tabs and build the table
            int i = 1;
            foreach (DataRow row in transcriptQuickView.GetPersonalTranscriptQuickView(EnumDataSet.UserInstructorLedTranscripts).Rows)
            {
                i++;
                TableRow dataRow = new TableRow();
                // apply css for alternating rows
                if (i % 2 == 0)
                { dataRow.CssClass = "UserField GridDataRow"; }
                else
                { dataRow.CssClass = "UserField GridDataRow GridDataRowAlternate"; }

                TableCell courseCell = new TableCell();
                courseCell.Text = HttpContext.Current.Server.HtmlEncode(row["Module"].ToString());
                dataRow.Cells.Add(courseCell);

                TableCell sessionCell = new TableCell();
                sessionCell.Text = HttpContext.Current.Server.HtmlEncode(row["Session"].ToString());
                dataRow.Cells.Add(sessionCell);

                TableCell dateCompletedCell = new TableCell();
                dateCompletedCell.Text = this._GetTranscriptFormattedDateTimeString(row["Date Completed"].ToString());
                dataRow.Cells.Add(dateCompletedCell);

                // attach data row to table
                tblTranscriptQuickView.Rows.Add(dataRow);
            }

            if (i == 1)
            {
                TableRow dataRow = new TableRow();
                dataRow.CssClass = "GridDataRowEmpty";
                TableCell rowEmptyMessageCell = new TableCell();
                rowEmptyMessageCell.ColumnSpan = 3;
                rowEmptyMessageCell.Text = _GlobalResources.NoRecordsFound;
                dataRow.Cells.Add(rowEmptyMessageCell);
                tblTranscriptQuickView.Rows.Add(dataRow);
            }

            quickViewPanel.Controls.Add(tblTranscriptQuickView);

            loadedTabContent.Controls.Add(quickViewPanel);

        }
        #endregion

        #region _GetTranscriptFormattedDateTimeString
        /// <summary>
        /// Formats a Date/Time string so that is is enclosed inside of <span> tags.
        /// </summary>
        /// <param name="valueToFormat"></param>
        /// <returns></returns>
        private string _GetTranscriptFormattedDateTimeString(string valueToFormat)
        {
            string formattedDateTimeString = valueToFormat;

            if (!String.IsNullOrWhiteSpace(valueToFormat))
            {
                // if the data is a date, it should be displayed as a formatted date
                DateTime dateValue;

                if (DateTime.TryParse(valueToFormat, out dateValue))
                {                    
                    // separate date and time into <spans> so that times could be hidden by CSS if we wanted
                    string formattedDate = "<span class=\"TranscriptFormattedDate\">" + dateValue.ToShortDateString() + "</span>";
                    string formattedTime = "<span class=\"TranscriptFormattedTime\">" + dateValue.ToShortTimeString() + "</span>";                            

                    // attach date and time <span>s to the row
                    formattedDateTimeString = formattedDate + " " + formattedTime;                    
                }
            }

            // return
            return formattedDateTimeString;
        }
        #endregion

        #region _BuildViewReportModal
        /// <summary>
        ///Builds view report modal pop up.
        /// </summary>        
        private void _BuildViewReportModal(Control reportPanel)
        {
            this._ReportModal = new ModalPopup("ReportModalPopup");
            this._ReportModal.Type = ModalPopupType.Information;
            this._ReportModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_TRANSCRIPT, ImageFiles.EXT_PNG);
            this._ReportModal.HeaderIconAlt = _GlobalResources.Transcript;
            this._ReportModal.HeaderText = _GlobalResources.Transcript;
            this._ReportModal.TargetControlID = this._TranscriptViewerModalHiddenLaunchButton.ID;
                    
            // build the modal body
            Panel modalPanel = new Panel();
            modalPanel.CssClass = "TranscriptWidgetModalBody";

            Pages.Administrator.Users.Transcript.BuildTranscriptFullView(AsentiaSessionState.IdSiteUser.ToString(), HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.UserLastName.ToString()) + ", " + HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.UserFirstName.ToString()), modalPanel, true);

            this._ReportModal.AddControlToBody(modalPanel);

            reportPanel.Controls.Add(this._ReportModal);
        }
        #endregion

        #region _BuildCertificatesWidget
        /// <summary>
        /// Method to build the Certificates Widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildCertificatesWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";
            loadedWidgetContent.CssClass = "CertificateRecordGridUpdatePanel";

            Grid certificateGrid = new Grid("Account", true);

            certificateGrid.ID = widgetId + "CertificateGrid";
            certificateGrid.StoredProcedure = Library.Certificate.GridProcedureForUser;
            certificateGrid.DefaultRecordsPerPage = 10;
            certificateGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            certificateGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            certificateGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            certificateGrid.AddFilter("@idUser", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            certificateGrid.IdentifierField = "idCertificateRecord";
            certificateGrid.DefaultSortColumn = "timestamp";
            certificateGrid.IsDefaultSortDescending = true;
            certificateGrid.ShowTimeInDateStrings = false;

            // data key names
            certificateGrid.DataKeyNames = new string[] { "idCertificateRecord", "idCertificate" };

            // columns
            GridColumn certificateName = new GridColumn(_GlobalResources.Certificate, "name", "name");
            GridColumn credits = new GridColumn(_GlobalResources.Credits, "credits", "credits");
            GridColumn awardDate = new GridColumn(_GlobalResources.AwardDate, "timestamp", "timestamp");
            GridColumn expires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires");

            GridColumn saveAsPdf = new GridColumn(_GlobalResources.SaveAsPDF, "isPDFExportOn", true);
            saveAsPdf.AddProperty(new GridColumnProperty("True", "<a href=\"javascript:void(0);\" target=\"_blank\" onclick=\"ExportCertificateToPDF(##idCertificate##, ##idCertificateRecord##);return false;\">"
                                                                + "<img class=\"MediumIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_PDF,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.SaveAsPDF + "\" />"
                                                                + "</a>"));

            GridColumn print = new GridColumn(_GlobalResources.Print, "isPrintOn", true);
            print.AddProperty(new GridColumnProperty("True", "<a href=\"javascript:void(0);\" target=\"_blank\"  onclick=\"PrintCertificate(##idCertificateRecord##);return false;\" >"
                                                                + "<img class=\"MediumIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_PRINT,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.Print + "\" />"
                                                                + "</a>"));

            GridColumn view = new GridColumn(_GlobalResources.View, "isViewOn", true);
            view.AddProperty(new GridColumnProperty("True", "<a href=\"javascript:void(0);\" onclick=\"ViewCertificate(##idCertificateRecord##);return false;\">"
                                                                + "<img class=\"MediumIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_VIEW,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.View + "\" />"
                                                                + "</a>"));

            // add columns to data grid
            certificateGrid.AddColumn(certificateName);
            certificateGrid.AddColumn(credits);
            certificateGrid.AddColumn(awardDate);
            certificateGrid.AddColumn(expires);
            certificateGrid.AddColumn(saveAsPdf);
            certificateGrid.AddColumn(print);
            certificateGrid.AddColumn(view);

            loadedWidgetContent.Controls.Add(certificateGrid);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { certificateGrid.BindData(); }
        }
        #endregion

        #region _BuildViewCertificateModal
        /// <summary>
        /// Builds the modal for viewing the certificate
        /// </summary>
        /// <param name="idCertificateRecord"></param>
        /// <param name="idCertificate"></param>
        /// <param name="targetControlId"></param>
        /// <param name="modalContainer"></param>
        private void _BuildViewCertificateModal(string targetControlId, Control modalContainer)
        {
            ModalPopup viewCertificateModal = new ModalPopup("ViewCertificateModal");
            viewCertificateModal.Type = ModalPopupType.Information;
            viewCertificateModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
            viewCertificateModal.HeaderIconAlt = _GlobalResources.Certificate;
            viewCertificateModal.HeaderText = _GlobalResources.Certificate;
            viewCertificateModal.TargetControlID = targetControlId;
            viewCertificateModal.ShowLoadingPlaceholder = true;

            // create and add certificate wrapper to modal body
            Panel certificateWrapper = new Panel();
            certificateWrapper.ID = "CertificateWrapper";

            viewCertificateModal.AddControlToBody(certificateWrapper);

            // attach modal to container
            modalContainer.Controls.Add(viewCertificateModal);                                         
        }
        #endregion

        #region _ExportCertificateToPDFButton_Command
        /// <summary>
        /// Handles the "Export to pdf" button click.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _ExportCertificateToPDFButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(this._ExportCertificateToPdfHiddenCertificateId.Value) && !String.IsNullOrWhiteSpace(this._ExportCertificateToPdfHiddenCertificateRecordId.Value))
                {
                    //Bulid certifiacateId
                    int certificateID = Convert.ToInt32(this._ExportCertificateToPdfHiddenCertificateId.Value);
                    int certificateRecordID = Convert.ToInt32(this._ExportCertificateToPdfHiddenCertificateRecordId.Value);
                    Certificate certificateObject = new Certificate(certificateID);
                    CertificateRecord crObject = new CertificateRecord(certificateRecordID);

                    string certificateName = String.Empty;
                    string pdfFileName = "[FirstName]-[LastName]_[CertificateTitle]_[AwardDateISO].pdf";
                    string firstNameValue = String.Empty;
                    string lastNameValue = String.Empty;

                    foreach (Certificate.LanguageSpecificProperty certificateLanguageSpecificProperty in certificateObject.LanguageSpecificProperties)
                    {
                        if (certificateLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { certificateName = certificateLanguageSpecificProperty.Name; }
                    }

                    pdfFileName = pdfFileName.Replace("[CertificateTitle]", Utility.RemoveSpecialCharactersForFileName(certificateObject.Name));
                    pdfFileName = pdfFileName.Replace("[AwardDateISO]", String.Format("{0:yyyy-MM-dd}", TimeZoneInfo.ConvertTimeFromUtc((DateTime)crObject.Timestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName))));

                    //Build file path
                    string filePath = SitePathConstants.SITE_CERTIFICATES_ROOT + certificateID + "/Certificate.json";

                    //Load html of the selected language
                    if (File.Exists(HttpContext.Current.Server.MapPath(filePath)))
                    {
                        string certLayoutString = crObject.BuildCertificateLayoutJSON();

                        string viewerPDF = String.Empty;

                        //Calculate proportional height and width.
                        double defaultWidth = 800;
                        double defaultHeight = 600;
                        JObject layoutString = JObject.Parse(certLayoutString);

                        double imageWidth = Convert.ToDouble(((string)layoutString["Certificate"]["Container"]["Width"]).Replace("px", ""));
                        double imageHeight = Convert.ToDouble(((string)layoutString["Certificate"]["Container"]["Height"]).Replace("px", ""));

                        double newWidth = imageWidth;
                        double newHeight = imageHeight;
                        double percentIncreased = 0;
                        bool lanscapeOrientation = true;

                        if (newHeight > newWidth)
                        {
                            lanscapeOrientation = false;
                            defaultWidth = 600;
                            defaultHeight = 800;
                        }

                        double widthToIncrease = defaultWidth - imageWidth;
                        double percentWidthToIncrease = (widthToIncrease / imageWidth) * 100;
                        double heightToIncrease = defaultHeight - imageHeight;
                        double percentheightToIncrease = (heightToIncrease / imageHeight) * 100;

                        percentIncreased = (percentWidthToIncrease < percentheightToIncrease) ? percentWidthToIncrease : percentheightToIncrease;

                        newWidth = imageWidth + (imageWidth * percentIncreased) / 100;
                        newHeight = imageHeight + (imageHeight * percentIncreased) / 100;

                        foreach (var element in layoutString["Certificate"]["Elements"])
                        {
                            string left = (string)element["Left"];
                            left = left.Replace("%", "");
                            string top = (string)element["Top"];
                            top = top.Replace("%", "");
                            string width = (string)element["Width"];
                            width = width.Replace("%", "");

                            string viewerElement = element["IsStaticLabel"] + " | ";
                            viewerElement += (((bool)element["IsStaticLabel"]) ? (string)element["Value"] : (string)element["Identifier"]) + " | ";
                            viewerElement += (string)element["FontSize"] + " | ";
                            viewerElement += Convert.ToString(Convert.ToDouble(element["Left"].Value<string>().Replace("%", "")) / 100 * imageWidth) + " | ";
                            viewerElement += Convert.ToString(Convert.ToDouble(element["Top"].Value<string>().Replace("%", "")) / 100 * imageHeight) + " | ";
                            viewerElement += (string)element["Alignment"] + " | ";
                            viewerElement += (string)element["Width"] + " | ";
                            viewerPDF += viewerElement + " ^ ";
                        }

                        this._CertificatePdfString = viewerPDF.Substring(0, viewerPDF.Length - 3);

                        string backgroundImageFilePath = null;
                        if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + certificateID + "/" + certificateObject.FileName)))
                        {
                            backgroundImageFilePath = HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + certificateID + "/" + certificateObject.FileName);
                        }

                        string url;
                        if (backgroundImageFilePath != null && File.Exists(backgroundImageFilePath))
                        {
                            url = backgroundImageFilePath;
                        }
                        else
                        {
                            url = HttpContext.Current.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "default.jpg");
                        }

                        using (var pdfGenerator = new PdfGenerator(url, newWidth, newHeight, PdfGenerator.PageSize.Letter, (lanscapeOrientation) ? PdfGenerator.Orientation.Landscape : PdfGenerator.Orientation.Portrait))
                        {
                            if (!(String.IsNullOrWhiteSpace(this._CertificatePdfString)))
                            {
                                FieldInfo[] fieldInfoArray = this._UserObject.GetType().GetFields();
                                string[] labelsArray = this._CertificatePdfString.Split(new string[] { " ^ " }, StringSplitOptions.None);

                                firstNameValue = fieldInfoArray[0] != null ? fieldInfoArray[1].GetValue(this._UserObject).ToString() : String.Empty;
                                lastNameValue = fieldInfoArray[2] != null ? fieldInfoArray[3].GetValue(this._UserObject).ToString() : String.Empty;

                                pdfFileName = pdfFileName.Replace("[FirstName]", firstNameValue);
                                pdfFileName = pdfFileName.Replace("[LastName]", lastNameValue);

                                foreach (var str in labelsArray)
                                {
                                    var arr = str.Split(new string[] { " | " }, StringSplitOptions.None);
                                    bool isStaticLabel = false;
                                    string label = arr[1];
                                    double fontSize = Convert.ToDouble(arr[2].Replace("em", "")) * 16;
                                    double x = Convert.ToDouble(arr[3]);
                                    double y = Convert.ToDouble(arr[4]);
                                    string textAlignment = arr[5].ToLower();
                                    double width = Convert.ToDouble(arr[6].Replace("%", "")) * newWidth / 100;

                                    if (isStaticLabel)
                                    {
                                        label = label.Replace("[^]", "^").Replace("[|]", "|"); //Replace the marked separators
                                    }
                                    else
                                    {
                                        foreach (FieldInfo fieldInfo in fieldInfoArray)
                                        {
                                            object fieldValue = fieldInfo.GetValue(this._UserObject);
                                            string value = fieldValue != null ? fieldValue.ToString() : String.Empty;

                                            if (fieldInfo.Name.ToLower() == label.ToLower())
                                            {
                                                if (fieldInfo.Name == "DtExpires")
                                                { label = value != null ? String.Format("{0:MMMM dd, yyyy}", TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(value), TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName))) : String.Empty; }
                                                else
                                                { label = value; }
                                                break;
                                            }
                                        }

                                        if (label == "fullName")
                                            label = firstNameValue + " " + lastNameValue;

                                        switch (label)
                                        {
                                            case "certificateName":
                                                label = certificateName; break;
                                            case "issuingOrganization":
                                                label = certificateObject.IssuingOrganization != null ? certificateObject.IssuingOrganization.ToString() : String.Empty; break;
                                            case "dtAward":
                                                label = crObject.Timestamp != null ? String.Format("{0:MMMM dd, yyyy}", TimeZoneInfo.ConvertTimeFromUtc((DateTime)crObject.Timestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName))) : String.Empty; break;
                                            case "dtCertificateExpires":
                                                label = crObject.Expires != null ? String.Format("{0:MMMM dd, yyyy}", TimeZoneInfo.ConvertTimeFromUtc((DateTime)crObject.Expires, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName))) : String.Empty; break;
                                            case "certificateCode":
                                                label = crObject.Code != null ? crObject.Code.ToString() : String.Empty; break;
                                            case "certificateCredits":
                                                label = crObject.Credits != null ? crObject.Credits.ToString() : String.Empty; break;
                                        }
                                    }

                                    PdfGenerator.Alignment alignment;
                                    switch (textAlignment)
                                    {
                                        case "center":
                                            alignment = PdfGenerator.Alignment.Center;
                                            break;
                                        case "right":
                                            alignment = PdfGenerator.Alignment.Right;
                                            break;
                                        default:
                                            alignment = PdfGenerator.Alignment.Left;
                                            break;
                                    }

                                    width = width + ((width * percentIncreased) / 100);

                                    fontSize = fontSize + ((fontSize * percentIncreased) / 100);

                                    //Write string to the document
                                    pdfGenerator.WriteString(label, x, y, fontSize, alignment, width, fontSize + 12);
                                }
                            }

                            //Send certificate as a pdf.
                            MemoryStream stream = pdfGenerator.GetStream();
                            Page.Response.Clear();
                            Page.Response.ContentType = "application/pdf";
                            Page.Response.AddHeader("Content-Disposition", "attachment; filename=" + pdfFileName + ";");
                            Page.Response.AddHeader("content-length", stream.Length.ToString());
                            Page.Response.BinaryWrite(stream.ToArray());
                            Page.Response.Flush();
                            stream.Close();
                            Page.Response.End();
                        }

                    }
                }
            }
            catch (AsentiaException)
            {

            }
        }
        #endregion

        #region _BuildCalendarWidget
        /// <summary>
        /// Builds calender widget
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildCalendarWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            //adding a hidden field,used for shoing/hiding alert icon
            HiddenField callenderWidgetHiddenForShowingAlertIcon = new HiddenField();
            callenderWidgetHiddenForShowingAlertIcon.ID = widgetId + "HiddenForShowingAlertIcon";
            callenderWidgetHiddenForShowingAlertIcon.Value = "0";
            widgetContent.ContentTemplateContainer.Controls.Add(callenderWidgetHiddenForShowingAlertIcon);

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";
            
            Asentia.LMS.Controls.Calendar userCalendar = new Asentia.LMS.Controls.Calendar(AsentiaSessionState.UserTimezoneCurrentLocalTime);
            userCalendar.CalendarModalControlDestination = this;
            loadedWidgetContent.Controls.Add(userCalendar);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            //show the ! icon on calender widgetbar icon if there aren any items for current date
            if (userCalendar.HasRecordsForShowingAlertIconOnWidget)
            { callenderWidgetHiddenForShowingAlertIcon.Value = "1"; }
            else { callenderWidgetHiddenForShowingAlertIcon.Value = "0"; }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "AlertIconForCalenderWidget", "ShowHideAlertIcon('" + callenderWidgetHiddenForShowingAlertIcon.ID + "','" + _CALENDAR_WIDGET_ID + "');", true);
        }
        #endregion        

        #region _BuildFeedWidget
        /// <summary>
        /// Method to build the Feed Widget.
        /// </summary>
        /// <param name="widgetId">widget id</param>
        private void _BuildFeedWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // FEED MESSAGES CONTAINER

            Panel feedMessagesContainer = new Panel();
            feedMessagesContainer.ID = widgetId + "_FeedMessagesContainer";

            // make an unclickable button that will be the default button for the feed messages panel
            // so that we can control postbacks when enter is pressed in the comment fields
            Button feedMessagesContainerDefaultButton = new Button();
            feedMessagesContainerDefaultButton.ID = widgetId + "_FeedMessagesContainerDefaultButton";
            feedMessagesContainerDefaultButton.OnClientClick = "return false;";
            feedMessagesContainerDefaultButton.Style.Add("display", "none");
            feedMessagesContainer.Controls.Add(feedMessagesContainerDefaultButton);
            feedMessagesContainer.DefaultButton = feedMessagesContainerDefaultButton.ID;

            // attach the feed messages panel
            loadedWidgetContent.Controls.Add(feedMessagesContainer);

            // LOADING PANEL - OLDER MESSAGES

            Panel olderMessagesLoadingPanel = new Panel();
            olderMessagesLoadingPanel.ID = widgetId + "_OlderMessagesLoadingPanel";
            olderMessagesLoadingPanel.CssClass = "WallLoadingPanel";
            olderMessagesLoadingPanel.Style.Add("display", "none");

            Image olderMessagesLoadingImage = new Image();
            olderMessagesLoadingImage.ID = widgetId + "OlderMessagesLoadingImage";
            olderMessagesLoadingImage.CssClass = "MediumIcon";
            olderMessagesLoadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING, ImageFiles.EXT_GIF);
            olderMessagesLoadingImage.AlternateText = _GlobalResources.Loading;

            Label olderMessagesLoadingText = new Label();
            olderMessagesLoadingText.Text = _GlobalResources.LoadingMessages;

            olderMessagesLoadingPanel.Controls.Add(olderMessagesLoadingImage);
            olderMessagesLoadingPanel.Controls.Add(olderMessagesLoadingText);

            loadedWidgetContent.Controls.Add(olderMessagesLoadingPanel);

            // MODALS FOR CONFIRMATION OF MESSAGE POSTING AND MESSAGE DELETION

            // message posted for moderation
            Button hiddenButtonForPostedForModerationModal = new Button();
            hiddenButtonForPostedForModerationModal.ID = widgetId + "_HiddenButtonForPostedForModerationModal";
            hiddenButtonForPostedForModerationModal.Style.Add("display", "none");
            loadedWidgetContent.Controls.Add(hiddenButtonForPostedForModerationModal);

            this._BuildMessagePostedForModerationModal(loadedWidgetContent, hiddenButtonForPostedForModerationModal.ID, widgetId);

            // message deleted
            Button hiddenButtonForMessageDeletedModal = new Button();
            hiddenButtonForMessageDeletedModal.ID = widgetId + "_HiddenButtonForMessageDeletedModal";
            hiddenButtonForMessageDeletedModal.Style.Add("display", "none");
            loadedWidgetContent.Controls.Add(hiddenButtonForMessageDeletedModal);

            this._BuildMessageDeletedModal(loadedWidgetContent, hiddenButtonForMessageDeletedModal.ID, widgetId);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, waiting to databind saves
            // an unnecessary database hit on initial load
            if (!isInitialLoad)
            {
                // set up a js ajax load for the feed messages

                // set the strings for global variables
                StringBuilder globalJs = new StringBuilder();
                globalJs.AppendLine("var FeedWidgetLastRecord = \"" + AsentiaSessionState.UtcNow + "\";");
                globalJs.AppendLine("var IsFeedWidgetInitialLoad = true;");
                globalJs.AppendLine("var FeedWidgetScrollPosition = 0;");
                globalJs.AppendLine("var IsFeedWidgetLoading = false;");
                globalJs.AppendLine("GetFeedWidgetRecords();");
                globalJs.AppendLine("OnFeedWidgetScroll();");

                // add start up script
                ScriptManager.RegisterStartupScript(this.Page, typeof(Asentia.LMS.Controls.DashboardWidgetContainer), "FeedWidgetGlobalJs", globalJs.ToString(), true);
            }
        }
        #endregion

        #region _BuildWallModerationWidget
        /// <summary>
        /// Method to build the Wall Moderation Widget.
        /// </summary>
        /// <param name="widgetId">widget id</param>
        private void _BuildWallModerationWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            //adding a hidden field,used for shoing/hiding alert icon
            HiddenField wallModerationWidgetHiddenForShowingAlertIcon = new HiddenField();
            wallModerationWidgetHiddenForShowingAlertIcon.ID = widgetId + "HiddenForShowingAlertIcon";
            wallModerationWidgetHiddenForShowingAlertIcon.Value = "0";
            widgetContent.ContentTemplateContainer.Controls.Add(wallModerationWidgetHiddenForShowingAlertIcon);

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // FEED MESSAGES CONTAINER

            Panel feedMessagesContainer = new Panel();
            feedMessagesContainer.ID = widgetId + "_FeedMessagesContainer";

            // make an unclickable button that will be the default button for the feed messages panel
            // so that we can control postbacks when enter is pressed in the comment fields
            Button feedMessagesContainerDefaultButton = new Button();
            feedMessagesContainerDefaultButton.ID = widgetId + "_FeedMessagesContainerDefaultButton";
            feedMessagesContainerDefaultButton.OnClientClick = "return false;";
            feedMessagesContainerDefaultButton.Style.Add("display", "none");
            feedMessagesContainer.Controls.Add(feedMessagesContainerDefaultButton);
            feedMessagesContainer.DefaultButton = feedMessagesContainerDefaultButton.ID;

            // attach the feed messages panel
            loadedWidgetContent.Controls.Add(feedMessagesContainer);

            // LOADING PANEL - OLDER MESSAGES

            Panel olderMessagesLoadingPanel = new Panel();
            olderMessagesLoadingPanel.ID = widgetId + "_OlderMessagesLoadingPanel";
            olderMessagesLoadingPanel.CssClass = "WallLoadingPanel";
            olderMessagesLoadingPanel.Style.Add("display", "none");

            Image olderMessagesLoadingImage = new Image();
            olderMessagesLoadingImage.ID = "OlderMessagesLoadingImage";
            olderMessagesLoadingImage.CssClass = "MediumIcon";
            olderMessagesLoadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING, ImageFiles.EXT_GIF);
            olderMessagesLoadingImage.AlternateText = _GlobalResources.Loading;

            Label olderMessagesLoadingText = new Label();
            olderMessagesLoadingText.Text = _GlobalResources.LoadingMessages;

            olderMessagesLoadingPanel.Controls.Add(olderMessagesLoadingImage);
            olderMessagesLoadingPanel.Controls.Add(olderMessagesLoadingText);

            loadedWidgetContent.Controls.Add(olderMessagesLoadingPanel);

            // MODALS FOR CONFIRMATION OF MESSAGE POSTING AND MESSAGE DELETION

            // message approved
            Button hiddenButtonForMessageApprovedModal = new Button();
            hiddenButtonForMessageApprovedModal.ID = widgetId + "_HiddenButtonForMessageApprovedModal";
            hiddenButtonForMessageApprovedModal.Style.Add("display", "none");
            loadedWidgetContent.Controls.Add(hiddenButtonForMessageApprovedModal);

            this._BuildMessageApprovedModal(loadedWidgetContent, hiddenButtonForMessageApprovedModal.ID, widgetId);

            // message deleted
            Button hiddenButtonForMessageDeletedModal = new Button();
            hiddenButtonForMessageDeletedModal.ID = widgetId + "_HiddenButtonForMessageDeletedModal";
            hiddenButtonForMessageDeletedModal.Style.Add("display", "none");
            loadedWidgetContent.Controls.Add(hiddenButtonForMessageDeletedModal);

            this._BuildMessageDeletedModal(loadedWidgetContent, hiddenButtonForMessageDeletedModal.ID, widgetId);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, waiting to databind saves
            // an unnecessary database hit on initial load
            if (!isInitialLoad)
            {
                // set up a js ajax load for the feed messages

                // set the strings for global variables
                StringBuilder globalJs = new StringBuilder();
                globalJs.AppendLine("var WallModerationWidgetLastRecord = \"" + AsentiaSessionState.UtcNow + "\";");
                globalJs.AppendLine("var IsWallModerationWidgetInitialLoad = true;");
                globalJs.AppendLine("var WallModerationWidgetScrollPosition = 0;");
                globalJs.AppendLine("var IsWallModerationWidgetLoading = false;");
                globalJs.AppendLine("GetWallModerationWidgetRecords();");
                globalJs.AppendLine("OnWallModerationWidgetScroll();");

                // add start up script
                ScriptManager.RegisterStartupScript(this.Page, typeof(Asentia.LMS.Controls.DashboardWidgetContainer), "WallModerationWidgetGlobalJs", globalJs.ToString(), true);
            }
        }
        #endregion

        #region _BuildMessagePostedForModerationModal
        private void _BuildMessagePostedForModerationModal(Panel loadedWidgetContent, string triggerId, string widgetId)
        {
            // set modal properties
            ModalPopup messagePostedForModerationModal = new ModalPopup(widgetId + "_MessagePostedForModerationModal");
            messagePostedForModerationModal.Type = ModalPopupType.Information;
            messagePostedForModerationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG);
            messagePostedForModerationModal.HeaderIconAlt = _GlobalResources.Discussion;
            messagePostedForModerationModal.HeaderText = _GlobalResources.MessagePosted;
            messagePostedForModerationModal.TargetControlID = triggerId;

            // build the modal body
            messagePostedForModerationModal.DisplayFeedback(_GlobalResources.YourMessageHasBeenPostedForModerationOnceApprovedByAModeratorYourMessageWillBeDisplayedInTheDiscussion, false);

            // add modal to container
            loadedWidgetContent.Controls.Add(messagePostedForModerationModal);
        }
        #endregion

        #region _BuildMessageApprovedModal
        private void _BuildMessageApprovedModal(Panel loadedWidgetContent, string triggerId, string widgetId)
        {
            // set modal properties
            ModalPopup messageApprovedModal = new ModalPopup(widgetId + "_MessageApprovedModal");
            messageApprovedModal.Type = ModalPopupType.Information;
            messageApprovedModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG);
            messageApprovedModal.HeaderIconAlt = _GlobalResources.Discussion;
            messageApprovedModal.HeaderText = _GlobalResources.MessageApproved;
            messageApprovedModal.TargetControlID = triggerId;

            // build the modal body
            messageApprovedModal.DisplayFeedback(_GlobalResources.TheMessageHasBeenApprovedItWillNowBeDisplayedInTheDiscussion, false);

            // add modal to container
            loadedWidgetContent.Controls.Add(messageApprovedModal);
        }
        #endregion

        #region _BuildMessageDeletedModal
        private void _BuildMessageDeletedModal(Panel loadedWidgetContent, string triggerId, string widgetId)
        {
            // set modal properties
            ModalPopup messageDeletedModal = new ModalPopup(widgetId + "_MessageDeletedModal");
            messageDeletedModal.Type = ModalPopupType.Information;
            messageDeletedModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG);
            messageDeletedModal.HeaderIconAlt = _GlobalResources.Discussion;
            messageDeletedModal.HeaderText = _GlobalResources.MessageDeleted;
            messageDeletedModal.TargetControlID = triggerId;

            // build the modal body
            messageDeletedModal.DisplayFeedback(_GlobalResources.TheMessageHasBeenDeletedSuccessfully, false);

            // add modal to container
            loadedWidgetContent.Controls.Add(messageDeletedModal);
        }
        #endregion

        #region _BuildPurchasesWidget
        /// <summary>
        /// Method to build the Purchases Widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildPurchasesWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            Grid purchasesGrid = new Grid("Account", true);

            purchasesGrid.ID = widgetId + "PurchasesGrid";
            purchasesGrid.StoredProcedure = Purchase.GridProcedure;
            purchasesGrid.DefaultRecordsPerPage = 10;
            purchasesGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            purchasesGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            purchasesGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            purchasesGrid.AddFilter("@idUser", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            purchasesGrid.IdentifierField = "idPurchase";
            purchasesGrid.DefaultSortColumn = "dtOrder";
            purchasesGrid.EmptyDataText = _GlobalResources.NoPurchasesFound;
            purchasesGrid.IsDefaultSortDescending = true;

            // data key names
            purchasesGrid.DataKeyNames = new string[] { "idPurchase" };

            // columns
            GridColumn orderNumber = new GridColumn(_GlobalResources.OrderNumber, "orderNumber");
            GridColumn dtOrder = new GridColumn(_GlobalResources.OrderDate, "dtOrder", "dtOrder");
            GridColumn amount = new GridColumn(_GlobalResources.Amount, "amount", "amount");

            GridColumn viewReceipt = new GridColumn(_GlobalResources.Receipt, "viewReceipt", true);
            viewReceipt.AddProperty(new GridColumnProperty("True", "<a href=\"/myprofile/Receipt.aspx?id=##idPurchase##\">"
                                                                + "<img class=\"SmallIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_RECEIPT,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.Receipt + "\" />"
                                                                + "</a>"));
            viewReceipt.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_RECEIPT,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.ReceiptDisabled + "\" />"));

            // add columns to data grid
            purchasesGrid.AddColumn(orderNumber);
            purchasesGrid.AddColumn(dtOrder);
            purchasesGrid.AddColumn(amount);
            purchasesGrid.AddColumn(viewReceipt);
            purchasesGrid.RowDataBound += new GridViewRowEventHandler(this._PurchasesGrid_RowDataBound);

            loadedWidgetContent.Controls.Add(purchasesGrid);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { purchasesGrid.BindData(); }
        }
        #endregion

        #region _PurchasesGrid_RowDataBound
        /// <summary>
        /// Handles purchases grid row data bound event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _PurchasesGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // set the currency symbol for NumberFormat so we can use it to format currency
            CultureInfo ci = new CultureInfo(AsentiaSessionState.UserCulture);
            ci.NumberFormat.CurrencySymbol = this._EcommerceSettings.CurrencySymbol;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;

                // FORMAT "AMOUNT" COLUMN
                double amount = 0;

                // parse out cost and paid
                double.TryParse(rowView["amount"].ToString(), out amount);

                // add paid to its correct column in the table                
                e.Row.Cells[2].Text = amount.ToString("C", ci);
            }
        }
        #endregion

        #region _BuildMyCommunitiesWidget
        /// <summary>
        /// Method to build the MyCommunities Widget.
        /// </summary>
        /// <param name="widgetId">widget id</param>
        private void _BuildMyCommunitiesWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            Grid myCommunitiesGrid = new Grid("Account", true);

            myCommunitiesGrid.ID = widgetId + "MyCommunitiesGrid";
            myCommunitiesGrid.StoredProcedure = "[User.GetMyGroupsGrid]";
            myCommunitiesGrid.DefaultRecordsPerPage = 10;

            myCommunitiesGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            myCommunitiesGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            myCommunitiesGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);

            myCommunitiesGrid.AddFilter("@idUser", SqlDbType.Int, 4, this._UserObject.Id);
            myCommunitiesGrid.IdentifierField = "idGroup";
            myCommunitiesGrid.DefaultSortColumn = "name";
            myCommunitiesGrid.EmptyDataText = _GlobalResources.NoCommunitiesFound;

            myCommunitiesGrid.RowDataBound += this._MyCommunitiesGrid_RowDataBound;

            // data key names
            myCommunitiesGrid.DataKeyNames = new string[] { "idGroup" };

            // columns
            GridColumn name = new GridColumn(_GlobalResources.CommunityName, "name", "name"); 

            // add columns to data grid
            myCommunitiesGrid.AddColumn(name);

            // build group wall column and add to grid only if group discussion feature is enabled
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
            {
                GridColumn showGroupWall = new GridColumn(_GlobalResources.DiscussionDocuments, null, true);
                myCommunitiesGrid.AddColumn(showGroupWall);
            }

            loadedWidgetContent.Controls.Add(myCommunitiesGrid);            

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { myCommunitiesGrid.BindData(); }
        }
        #endregion

        #region _MyCommunitiesGrid_RowDataBound
        /// <summary>
        /// Row Data Bound event for my communities grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MyCommunitiesGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
    
            if (e.Row.RowType == DataControlRowType.DataRow &&
                (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;

                int idGroup = Convert.ToInt32(rowView["idGroup"]);

                HyperLink groupWallHyperLink = new HyperLink();
                groupWallHyperLink.NavigateUrl = @"/groups/Wall.aspx?id=" + idGroup.ToString();

                Image groupWallImage = new Image();
                groupWallImage.CssClass = "SmallIcon";
                groupWallImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION_BUTTON,
                                                                    ImageFiles.EXT_PNG);

                groupWallHyperLink.Controls.Add(groupWallImage);

                e.Row.Cells[1].Controls.Add(groupWallHyperLink);

            }
        }
        #endregion

        #region _BuildMyLearningPathsWidget
        /// <summary>
        /// Template method to serve as the basis for wideget builder methods.
        /// </summary>
        /// <param name="widgetId">widget id</param>
        private void _BuildMyLearningPathsWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // WIDGET SPECIFIC CODE GOES HERE - REPLACE THIS COMMENT

            // TABS
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Enrolled", _GlobalResources.Enrolled));
            tabs.Enqueue(new KeyValuePair<string, string>("Overdue", _GlobalResources.Overdue));
            tabs.Enqueue(new KeyValuePair<string, string>("Completed", _GlobalResources.Completed));
            tabs.Enqueue(new KeyValuePair<string, string>("Expired", _GlobalResources.Expired));

            // build and attach the tabs
            loadedWidgetContent.Controls.Add(AsentiaPage.BuildTabListPanel(widgetId, tabs));

            // BEGIN GRIDS
            // BEGIN ENROLLED GRID
            // "Enrolled" is the default tab, so this is visible on page load.
            UpdatePanel enrolledPanel = new UpdatePanel();
            enrolledPanel.ID = widgetId + "_Enrolled_TabPanel";
            enrolledPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            enrolledPanel.Attributes.Add("style", "display: block;");

            Grid enrolledGrid = new Grid("Account", true);

            enrolledGrid.ID = widgetId + "EnrolledGrid";
            enrolledGrid.StoredProcedure = LearningPathEnrollment.GridProcedureForUserDashboard;
            enrolledGrid.DefaultRecordsPerPage = 10;
            enrolledGrid.ShowTimeInDateStrings = true;
            enrolledGrid.EmptyDataText = _GlobalResources.NoLearningPathEnrollmentsFound;
            enrolledGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            enrolledGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            enrolledGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            enrolledGrid.AddFilter("@enrollmentStatus", SqlDbType.NVarChar, 20, "enrolled");
            enrolledGrid.IdentifierField = "idLearningPathEnrollment";
            enrolledGrid.DefaultSortColumn = "title";

            // data key names
            enrolledGrid.DataKeyNames = new string[] { "idLearningPathEnrollment", "idLearningPath", "title" };

            // columns
            GridColumn enrolledLearningPath = new GridColumn(_GlobalResources.LearningPath, "title", "title");
            GridColumn enrolledProgress = new GridColumn(_GlobalResources.Progress, null, true);
            GridColumn enrolledDueDate = new GridColumn(_GlobalResources.DueDate, "dtDue", "dtDue");
            GridColumn enrolledExpires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires", false, true);

            GridColumn enrolledView = new GridColumn(_GlobalResources.View, "view", true);
            enrolledView.AddProperty(new GridColumnProperty("True", "<a href=\"/dashboard/LearningPathEnrollment.aspx?id=##idLearningPathEnrollment##\">"
                                                            + "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"
                                                            + "</a>"));
            enrolledView.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"));

            // add columns to data grid
            enrolledGrid.AddColumn(enrolledLearningPath);
            enrolledGrid.AddColumn(enrolledProgress);
            enrolledGrid.AddColumn(enrolledDueDate);
            enrolledGrid.AddColumn(enrolledExpires);
            enrolledGrid.AddColumn(enrolledView);
            enrolledGrid.RowDataBound += this._MyLearningPathsWidgetGrid_RowDataBound;

            enrolledPanel.ContentTemplateContainer.Controls.Add(enrolledGrid);
            loadedWidgetContent.Controls.Add(enrolledPanel);
            // END ENROLLED GRID

            // BEGIN OVERDUE GRID
            UpdatePanel overduePanel = new UpdatePanel();
            overduePanel.ID = widgetId + "_Overdue_TabPanel";
            overduePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            overduePanel.Attributes.Add("style", "display: none;");

            Grid overdueGrid = new Grid("Account", true);

            overdueGrid.ID = widgetId + "OverdueGrid";
            overdueGrid.StoredProcedure = LearningPathEnrollment.GridProcedureForUserDashboard;
            overdueGrid.DefaultRecordsPerPage = 10;
            overdueGrid.ShowTimeInDateStrings = true;
            overdueGrid.EmptyDataText = _GlobalResources.YouHaveNoOverdueLearningPathEnrollments;
            overdueGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            overdueGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            overdueGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            overdueGrid.AddFilter("@enrollmentStatus", SqlDbType.NVarChar, 20, "overdue");
            overdueGrid.IdentifierField = "idLearningPathEnrollment";
            overdueGrid.DefaultSortColumn = "title";

            // data key names
            overdueGrid.DataKeyNames = new string[] { "idLearningPathEnrollment", "idLearningPath", "title" };

            // columns
            GridColumn overdueLearningPath = new GridColumn(_GlobalResources.LearningPath, "title", "title");
            GridColumn overdueProgress = new GridColumn(_GlobalResources.Progress, null, true);
            GridColumn overdueDueDate = new GridColumn(_GlobalResources.DueDate, "dtDue", "dtDue");
            GridColumn overdueExpires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires", false, true);

            GridColumn overdueView = new GridColumn(_GlobalResources.View, "view", true);
            overdueView.AddProperty(new GridColumnProperty("True", "<a href=\"/dashboard/LearningPathEnrollment.aspx?id=##idLearningPathEnrollment##\">"
                                                            + "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"
                                                            + "</a>"));
            overdueView.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"));

            // add columns to data grid
            overdueGrid.AddColumn(overdueLearningPath);
            overdueGrid.AddColumn(overdueProgress);
            overdueGrid.AddColumn(overdueDueDate);
            overdueGrid.AddColumn(overdueExpires);
            overdueGrid.AddColumn(overdueView);
            overdueGrid.RowDataBound += this._MyLearningPathsWidgetGrid_RowDataBound;

            overduePanel.ContentTemplateContainer.Controls.Add(overdueGrid);
            loadedWidgetContent.Controls.Add(overduePanel);
            // END OVERDUE GRID

            // BEGIN COMPLETED GRID
            UpdatePanel completedPanel = new UpdatePanel();
            completedPanel.ID = widgetId + "_Completed_TabPanel";
            completedPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            completedPanel.Attributes.Add("style", "display: none;");

            Grid completedGrid = new Grid("Account", true);

            completedGrid.ID = widgetId + "CompletedGrid";
            completedGrid.StoredProcedure = LearningPathEnrollment.GridProcedureForUserDashboard;
            completedGrid.DefaultRecordsPerPage = 10;
            completedGrid.ShowTimeInDateStrings = true;
            completedGrid.EmptyDataText = _GlobalResources.NoLearningPathEnrollmentsHaveBeenCompleted;
            completedGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            completedGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            completedGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            completedGrid.AddFilter("@enrollmentStatus", SqlDbType.NVarChar, 20, "completed");
            completedGrid.IdentifierField = "idLearningPathEnrollment";
            completedGrid.DefaultSortColumn = "title";

            // data key names
            completedGrid.DataKeyNames = new string[] { "idLearningPathEnrollment", "idLearningPath", "title" };

            // columns
            GridColumn completedLearningPath = new GridColumn(_GlobalResources.LearningPath, "title", "title");
            GridColumn completedProgress = new GridColumn(_GlobalResources.Progress, null, true);
            GridColumn completedCompletedDate = new GridColumn(_GlobalResources.DateCompleted, "dtCompleted", "dtCompleted");
            GridColumn completedExpires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires", false, true);

            GridColumn completedView = new GridColumn(_GlobalResources.Review, "view", true);
            completedView.AddProperty(new GridColumnProperty("True", "<a href=\"/dashboard/LearningPathEnrollment.aspx?id=##idLearningPathEnrollment##\">"
                                                            + "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"
                                                            + "</a>"));
            completedView.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"));

            // add columns to data grid
            completedGrid.AddColumn(completedLearningPath);
            completedGrid.AddColumn(completedProgress);
            completedGrid.AddColumn(completedCompletedDate);
            completedGrid.AddColumn(completedExpires);
            completedGrid.AddColumn(completedView);
            completedGrid.RowDataBound += this._MyLearningPathsWidgetGrid_RowDataBound;

            completedPanel.ContentTemplateContainer.Controls.Add(completedGrid);
            loadedWidgetContent.Controls.Add(completedPanel);
            // END COMPLETED GRID

            // BEGIN EXPIRED GRID
            UpdatePanel expiredPanel = new UpdatePanel();
            expiredPanel.ID = widgetId + "_Expired_TabPanel";
            expiredPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            expiredPanel.Attributes.Add("style", "display: none;");

            Grid expiredGrid = new Grid("Account", true);

            expiredGrid.ID = widgetId + "ExpiredGrid";
            expiredGrid.StoredProcedure = LearningPathEnrollment.GridProcedureForUserDashboard;
            expiredGrid.DefaultRecordsPerPage = 10;
            expiredGrid.ShowTimeInDateStrings = true;
            expiredGrid.EmptyDataText = _GlobalResources.NoEnrollmentsExpiredWIthinLast30Days;
            expiredGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            expiredGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            expiredGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            expiredGrid.AddFilter("@enrollmentStatus", SqlDbType.NVarChar, 20, "expired");
            expiredGrid.IdentifierField = "idLearningPathEnrollment";
            expiredGrid.DefaultSortColumn = "title";

            // data key names
            expiredGrid.DataKeyNames = new string[] { "idLearningPathEnrollment", "idLearningPath" };

            // columns
            GridColumn expiredLearningPath = new GridColumn(_GlobalResources.LearningPath, "title", "title");
            GridColumn expiredProgress = new GridColumn(_GlobalResources.Progress, null, true, true); // calculated in row databound event
            GridColumn expiredDueDate = new GridColumn(_GlobalResources.DueDate, "dtDue", "dtDue", false, true);
            GridColumn expiredExpires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires", false, true);

            GridColumn expiredView = new GridColumn(_GlobalResources.View, "view", true);
            expiredView.AddProperty(new GridColumnProperty("True", "<a href=\"/dashboard/LearningPathEnrollment.aspx?id=##idLearningPathEnrollment##\">"
                                                + "<img class=\"SmallIcon\" src=\""
                                                + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                         ImageFiles.EXT_PNG)
                                                + "\" alt=\"" + _GlobalResources.View + "\" />"
                                                + "</a>"));
            expiredView.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"));


            // add columns to data grid
            expiredGrid.AddColumn(expiredLearningPath);
            expiredGrid.AddColumn(expiredProgress);
            expiredGrid.AddColumn(expiredDueDate);
            expiredGrid.AddColumn(expiredExpires);
            expiredGrid.AddColumn(expiredView);
            expiredGrid.RowDataBound += this._MyLearningPathsWidgetGrid_RowDataBound;

            expiredPanel.ContentTemplateContainer.Controls.Add(expiredGrid);
            loadedWidgetContent.Controls.Add(expiredPanel);
            // END EXPIRED GRID

            // END GRIDS

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            {
                enrolledGrid.BindData();
                overdueGrid.BindData();
                completedGrid.BindData();
                expiredGrid.BindData();

                // re-do the tabs, this time with numbers in the titles
                Panel tabsPanel = (Panel)this.FindControl(widgetId + "_TabsPanel");

                if (tabsPanel != null)
                {
                    // clear the panel and tabs queue
                    tabsPanel.Controls.Clear();
                    tabs.Clear();

                    // queue up the tabs                    
                    tabs.Enqueue(new KeyValuePair<string, string>("Enrolled", _GlobalResources.Enrolled + " (" + enrolledGrid.RowCount.ToString() + ")"));
                    tabs.Enqueue(new KeyValuePair<string, string>("Overdue", _GlobalResources.Overdue + " (" + overdueGrid.RowCount.ToString() + ")"));
                    tabs.Enqueue(new KeyValuePair<string, string>("Completed", _GlobalResources.Completed + " (" + completedGrid.RowCount.ToString() + ")"));
                    tabs.Enqueue(new KeyValuePair<string, string>("Expired", _GlobalResources.Expired + " (" + expiredGrid.RowCount.ToString() + ")"));

                    // re-build the tabs                    
                    AsentiaPage.BuildTabListPanel(widgetId, tabs, tabsPanel);
                }   
            }
        }
        #endregion

        #region _MyLearningPathsWidgetGrid_RowDataBound
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MyLearningPathsWidgetGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // get data values for row
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idLearningPathEnrollment = Convert.ToInt32(rowView["idLearningPathEnrollment"]); // will never be null

                // PROGRESS

                int numCourses = Convert.ToInt32(rowView["numCourses"]); // will never be null
                int numCoursesCompleted = Convert.ToInt32(rowView["numCoursesCompleted"]); // will never be null    
                double percentageCompleted = 0;
                string progressText = null;

                // only calculate percentage and populate text if the number of lessons is greater than 0
                if (numCourses > 0)
                {
                    percentageCompleted = ((double)numCoursesCompleted / (double)numCourses) * 100;
                    //progressText = numCoursesCompleted.ToString() + "/" + numCourses.ToString(); // leave text out for now
                }

                // attach a progress indicator
                ProgressIndicator enrollmentProgress = new ProgressIndicator("LearningPathEnrollment_" + idLearningPathEnrollment.ToString() + "Progress_", (int)Math.Round(percentageCompleted, 0), progressText);
                e.Row.Cells[1].Controls.Add(enrollmentProgress);
            }
        }
        #endregion

        #region _BuildLeaderboardsLearnerWidget
        /// <summary>
        /// Builds Leader boards learner widget
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildLeaderboardsLearnerWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // make sure the leaderboard type is a valid one, if it's not, default to "enrollment_count"
            string leaderboardType = this._LeaderboardsLearnerLeaderboardType.Value.ToString();

            if (String.IsNullOrWhiteSpace(leaderboardType)
                || (leaderboardType != "enrollment_count"
                && leaderboardType != "enrollment_credits"
                && leaderboardType != "certificate_count"
                && leaderboardType != "certificate_credits"))
            { leaderboardType = "enrollment_count"; }

            // make sure the leaderboard start type is a valid one, if it's not, default to "all_time"
            string leaderboardStartType = this._LeaderboardsLearnerLeaderboardStartType.Value.ToString();

            if (String.IsNullOrWhiteSpace(leaderboardStartType)
                || (leaderboardStartType != "all_time"
                && leaderboardStartType != "this_year"
                && leaderboardStartType != "this_month"
                && leaderboardStartType != "this_week"))
            { leaderboardStartType = "all_time"; }

            // make sure the leaderboard "relative to" filter is a valid one, if it's not, default to "_global_"
            string leaderboardRelativeTo = this._LeaderboardsLearnerLeaderboardRelativeTo.Value.ToString();

            if (String.IsNullOrWhiteSpace(leaderboardRelativeTo))
            { leaderboardRelativeTo = "_global_"; }

            // LEADERBOARD OPTIONS
            Panel leaderboardOptionsContainer = new Panel();
            leaderboardOptionsContainer.ID = widgetId + "LeaderboardOptionsContainer";

            // leaderboard type selector
            DropDownList leaderboardTypeSelector = new DropDownList();
            leaderboardTypeSelector.ID = widgetId + "LeaderboardTypeSelector";

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSETOTAL_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCourseTotal, "enrollment_count")); }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSECREDITS_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCourseCredits, "enrollment_credits")); }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATETOTAL_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCertificateTotal, "certificate_count")); }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATECREDITS_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCertificateCredits, "certificate_credits")); }

            leaderboardTypeSelector.AutoPostBack = true;
            leaderboardTypeSelector.SelectedIndexChanged += this._LeaderboardType_SelectedIndexChanged;
            leaderboardTypeSelector.SelectedValue = leaderboardType;

            leaderboardOptionsContainer.Controls.Add(leaderboardTypeSelector);

            // leaderboard start type selector
            DropDownList leaderboardStartTypeSelector = new DropDownList();
            leaderboardStartTypeSelector.ID = widgetId + "LeaderboardStartTypeSelector";

            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.alltime_lower, "all_time"));
            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.thisyear_lower, "this_year"));
            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.thismonth_lower, "this_month"));
            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.thisweek_lower, "this_week"));

            leaderboardStartTypeSelector.AutoPostBack = true;
            leaderboardStartTypeSelector.SelectedIndexChanged += this._LeaderboardStartType_SelectedIndexChanged;
            leaderboardStartTypeSelector.SelectedValue = leaderboardStartType;

            leaderboardOptionsContainer.Controls.Add(leaderboardStartTypeSelector);

            // leaderboard relative to label
            Label leaderboardRelativeToLabel = new Label();
            leaderboardRelativeToLabel.ID = "LeaderboardRelativeToLabel";
            leaderboardRelativeToLabel.Text = _GlobalResources.relativeto_lower;

            leaderboardOptionsContainer.Controls.Add(leaderboardRelativeToLabel);

            // leaderboard relative to selector
            DropDownList leaderboardRelativeToSelector = new DropDownList();
            leaderboardRelativeToSelector.ID = widgetId + "LeaderboardRelativeToSelector";

            leaderboardRelativeToSelector.Items.Add(new ListItem(_GlobalResources.Global, "_global_"));

            // get the user fields that are eligible for the "relative to" filter
            UserAccountData userAccountData = new UserAccountData(UserAccountDataFileType.Site, true, false);
            List<UserAccountData.UserFieldForLabel> userFieldsForRelativeToSelector = userAccountData.GetLeaderboardRelativeToEligibleFieldLabelsInLanguage(AsentiaSessionState.UserCulture);

            foreach (UserAccountData.UserFieldForLabel userField in userFieldsForRelativeToSelector)
            {
                if (this._UserObject != null)
                {
                    // dynamically grab the value of the field for the calling user using Reflection
                    object fieldValueObject = typeof(User).GetField(userField.UserObjectPropertyName).GetValue(this._UserObject);                    

                    if (fieldValueObject != null)
                    {
                        string fieldValueString = fieldValueObject.ToString();

                        if (!String.IsNullOrWhiteSpace(fieldValueString))
                        { leaderboardRelativeToSelector.Items.Add(new ListItem(userField.Label + ": " + fieldValueString, userField.Identifier)); }
                    }
                }                
            }

            leaderboardRelativeToSelector.AutoPostBack = true;
            leaderboardRelativeToSelector.SelectedIndexChanged += this._LeaderboardRelativeTo_SelectedIndexChanged;
            leaderboardRelativeToSelector.SelectedValue = leaderboardRelativeTo;

            leaderboardOptionsContainer.Controls.Add(leaderboardRelativeToSelector);

            // attach controls to container
            loadedWidgetContent.Controls.Add(leaderboardOptionsContainer);

            // DATA
            DataTable leaderboardDt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                
                if (!String.IsNullOrWhiteSpace(leaderboardRelativeTo) && leaderboardRelativeTo != "_global_")
                { databaseObject.AddParameter("@relativeTo", leaderboardRelativeTo, SqlDbType.NVarChar, 50, ParameterDirection.Input); }
                else
                { databaseObject.AddParameter("@relativeTo", null, SqlDbType.NVarChar, 50, ParameterDirection.Input); }
                
                databaseObject.AddParameter("@numRecords", 10, SqlDbType.Int, 4, ParameterDirection.Input);

                if (leaderboardStartType == "all_time")
                { databaseObject.AddParameter("@dtStart", null, SqlDbType.DateTime, 8, ParameterDirection.Input); }
                else
                {
                    DateTime startDate = new DateTime();

                    if (leaderboardStartType == "this_year")
                    { startDate = new DateTime(AsentiaSessionState.UtcNow.Year, 1, 1); }
                    else if (leaderboardStartType == "this_month")
                    { startDate = new DateTime(AsentiaSessionState.UtcNow.Year, AsentiaSessionState.UtcNow.Month, 1); }
                    else if (leaderboardStartType == "this_week")
                    {
                        int dowStartDiff = AsentiaSessionState.UtcNow.DayOfWeek - DayOfWeek.Sunday;

                        if (dowStartDiff < 0)
                        { dowStartDiff += 7; }

                        startDate = AsentiaSessionState.UtcNow.AddDays(-1 * dowStartDiff).Date;
                    }
                    else
                    { }

                    databaseObject.AddParameter("@dtStart", startDate, SqlDbType.DateTime, 8, ParameterDirection.Input);
                }

                databaseObject.AddParameter("@leaderboardType", leaderboardType, SqlDbType.NVarChar, 20, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Site.GetLeaderboard]", true);

                // load leaderboard rs
                leaderboardDt.Load(sdr);

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { databaseObject.Dispose(); }

            // LEADERBOARD TABLE
            Panel leaderboardTableContainer = new Panel();
            leaderboardTableContainer.ID = widgetId + "LeaderboardTableContainer";

            Table leaderboardTable = new Table();
            leaderboardTable.ID = widgetId + "LeaderboardTable";
            leaderboardTable.CssClass = "GridTable";

            TableHeaderRow leaderboardTableHeaderRow = new TableHeaderRow();
            leaderboardTableHeaderRow.CssClass = "GridHeaderRow";

            TableHeaderCell leaderboardTableNumberHeaderCell = new TableHeaderCell();
            leaderboardTableNumberHeaderCell.CssClass = "Centered";
            Label numberLabel = new Label();
            numberLabel.Text = _GlobalResources.NumberSymbol;
            leaderboardTableNumberHeaderCell.Controls.Add(numberLabel);
            leaderboardTableHeaderRow.Cells.Add(leaderboardTableNumberHeaderCell);

            TableHeaderCell leaderboardTableNameHeaderCell = new TableHeaderCell();
            Label nameLabel = new Label();
            nameLabel.Text = _GlobalResources.Name;
            leaderboardTableNameHeaderCell.Controls.Add(nameLabel);
            leaderboardTableHeaderRow.Cells.Add(leaderboardTableNameHeaderCell);

            TableHeaderCell leaderboardTableScoreHeaderCell = new TableHeaderCell();
            Label scoreLabel = new Label();
            scoreLabel.Text = _GlobalResources.Score;
            leaderboardTableScoreHeaderCell.Controls.Add(scoreLabel);
            leaderboardTableHeaderRow.Cells.Add(leaderboardTableScoreHeaderCell);

            leaderboardTable.Rows.Add(leaderboardTableHeaderRow);

            int i = 0;

            foreach (DataRow dr in leaderboardDt.Rows)
            {
                TableRow tr = new TableRow();

                // add css for alternating row
                if (i % 2 == 1)
                { tr.CssClass = "GridDataRow GridDataRowAlternate"; }
                else
                { tr.CssClass = "GridDataRow"; }

                if (Convert.ToInt32(dr["idUser"]) == AsentiaSessionState.IdSiteUser)
                { tr.CssClass += " LeaderboardCurrentUserRow"; }

                TableCell positionCell = new TableCell();
                positionCell.CssClass = "Centered";
                Label positionLabel = new Label();
                positionLabel.Text = dr["position"].ToString();
                positionCell.Controls.Add(positionLabel);
                tr.Cells.Add(positionCell);

                TableCell learnerNameCell = new TableCell();

                int position = Convert.ToInt32(dr["position"]);

                if (position <= 3)
                {
                    Image trophyImage = new Image();
                    trophyImage.CssClass = "XSmallIcon LeaderboardTrophyIcon";

                    switch (position)
                    {
                        case 1:
                            trophyImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD_GOLD, ImageFiles.EXT_PNG);
                            learnerNameCell.Controls.Add(trophyImage);
                            break;
                        case 2:
                            trophyImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD_SILVER, ImageFiles.EXT_PNG);
                            learnerNameCell.Controls.Add(trophyImage);
                            break;
                        case 3:
                            trophyImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD_BRONZE, ImageFiles.EXT_PNG);
                            learnerNameCell.Controls.Add(trophyImage);
                            break;
                        default:
                            break;
                    }
                }

                Label learnerNameLabel = new Label();
                learnerNameLabel.Text = HttpContext.Current.Server.HtmlEncode(dr["name"].ToString().Replace("[others]", _GlobalResources.Others));
                learnerNameCell.Controls.Add(learnerNameLabel);
                tr.Cells.Add(learnerNameCell);

                TableCell totalCell = new TableCell();
                Label totalLabel = new Label();
                totalLabel.Text = dr["total"].ToString();
                totalCell.Controls.Add(totalLabel);
                tr.Cells.Add(totalCell);

                leaderboardTable.Rows.Add(tr);
                i++;
            }

            TableRow topCoursesByCompletionsLastRow = new TableRow();
            leaderboardTable.Rows.Add(topCoursesByCompletionsLastRow);

            leaderboardTableContainer.Controls.Add(leaderboardTable);

            // attach table to widget
            loadedWidgetContent.Controls.Add(leaderboardTableContainer);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);
        }
        #endregion

        #region _BuildEnrollmentStatisticsWidget
        /// <summary>
        /// Builds widget for enrollment statistics.
        /// </summary>
        /// <param name="widgetId">widget id</param>
        private void _BuildEnrollmentStatisticsWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            if (!isInitialLoad)
            {
                // DATA
                DataTable courseEnrollmentsStatusDt = new DataTable();
                DataTable top5CoursesByEnrollmentsDt = new DataTable();
                DataTable top5CoursesByCompletionsDt = new DataTable();
                DataTable learningPathEnrollmentsStatusDt = new DataTable();
                DataTable top5LearningPathsByEnrollmentsDt = new DataTable();
                DataTable top5LearningPathsByCompletionsDt = new DataTable();
                

                AsentiaDatabase databaseObject = new AsentiaDatabase();

                try
                {
                    databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                    databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                    databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                    databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                    databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                    SqlDataReader sdr = databaseObject.ExecuteDataReader("[Widget.EnrollmentStatistics]", true);

                    // load course enrollments status rs
                    courseEnrollmentsStatusDt.Load(sdr);

                    // load top 5 courses by enrollment rs
                    top5CoursesByEnrollmentsDt.Load(sdr);

                    // load top 5 courses by completions rs
                    top5CoursesByCompletionsDt.Load(sdr);

                    // load learning path enrollments status rs
                    learningPathEnrollmentsStatusDt.Load(sdr);

                    // load top 5 learning paths by enrollment rs
                    top5LearningPathsByEnrollmentsDt.Load(sdr);

                    // load top 5 learning paths by completions rs
                    top5LearningPathsByCompletionsDt.Load(sdr);

                    sdr.Close();

                    DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                }
                catch
                { throw; }
                finally
                { databaseObject.Dispose(); }

                // CHARTS

                // container
                Panel enrollmentsStatusStatisticsContainer = new Panel();
                enrollmentsStatusStatisticsContainer.ID = widgetId + "EnrollmentStatusStatisticsContainer";
                enrollmentsStatusStatisticsContainer.CssClass = "CarouselControl";
                loadedWidgetContent.Controls.Add(enrollmentsStatusStatisticsContainer);

                // course enrollments pie
                List<string> courseEnrollmentsPieChartColors = new List<string>();
                courseEnrollmentsPieChartColors.Add("#2E7DBD");
                courseEnrollmentsPieChartColors.Add("#55BD86");
                courseEnrollmentsPieChartColors.Add("#FED155");
                courseEnrollmentsPieChartColors.Add("#EC6E61");

                Chart courseEnrollmentsPieChart = new Chart("CourseEnrollmentsPieChart", ChartType.Doughnut, courseEnrollmentsStatusDt, courseEnrollmentsPieChartColors, _GlobalResources.CourseEnrollments, null);
                courseEnrollmentsPieChart.ShowTotalInTitle = true;
                courseEnrollmentsPieChart.ShowTotalAndPercentageInLegend = true;
                courseEnrollmentsPieChart.IsResponsive = true;
                courseEnrollmentsPieChart.CanvasWidth = 200;
                courseEnrollmentsPieChart.CanvasHeight = 200;

                enrollmentsStatusStatisticsContainer.Controls.Add(courseEnrollmentsPieChart);

                // top 5 courses by enrollments list
                Chart top5CoursesByEnrollmentsList = new Chart("TopCoursesByEnrollmentsList", ChartType.List, top5CoursesByEnrollmentsDt, new List<string>(), _GlobalResources.TopCoursesByNumberOfEnrollments, null);
                enrollmentsStatusStatisticsContainer.Controls.Add(top5CoursesByEnrollmentsList);

                // top 5 courses by completions list
                Chart top5CoursesByCompletionsList = new Chart("TopCoursesByCompletionsList", ChartType.List, top5CoursesByCompletionsDt, new List<string>(), _GlobalResources.TopCoursesByNumberOfCompletions, null);
                enrollmentsStatusStatisticsContainer.Controls.Add(top5CoursesByCompletionsList);

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    // learning path enrollments pie
                    List<string> learningPathEnrollmentsPieChartColors = new List<string>();
                    learningPathEnrollmentsPieChartColors.Add("#A6729B");
                    learningPathEnrollmentsPieChartColors.Add("#45848C");
                    learningPathEnrollmentsPieChartColors.Add("#A69B72");
                    learningPathEnrollmentsPieChartColors.Add("#F17B85");

                    Chart learningPathEnrollmentsPieChart = new Chart("LearningPathEnrollmentsPieChart", ChartType.Doughnut, learningPathEnrollmentsStatusDt, learningPathEnrollmentsPieChartColors, _GlobalResources.LearningPathEnrollments, null);
                    learningPathEnrollmentsPieChart.ShowTotalInTitle = true;
                    learningPathEnrollmentsPieChart.ShowTotalAndPercentageInLegend = true;
                    learningPathEnrollmentsPieChart.IsResponsive = true;
                    learningPathEnrollmentsPieChart.CanvasWidth = 200;
                    learningPathEnrollmentsPieChart.CanvasHeight = 200;

                    enrollmentsStatusStatisticsContainer.Controls.Add(learningPathEnrollmentsPieChart);

                    // top 5 learning paths by enrollments list
                    Chart top5LearningPathsByEnrollmentsList = new Chart("TopLearningPathsByEnrollmentsList", ChartType.List, top5LearningPathsByEnrollmentsDt, new List<string>(), _GlobalResources.TopLearningPathsByNumberOfEnrollments, null);
                    enrollmentsStatusStatisticsContainer.Controls.Add(top5LearningPathsByEnrollmentsList);

                    // top 5 learning paths by completions list
                    Chart top5LearningPathsByCompletionsList = new Chart("TopLearningPathsByCompletionsList", ChartType.List, top5LearningPathsByCompletionsDt, new List<string>(), _GlobalResources.TopLearningPathsByNumberOfCompletions, null);
                    enrollmentsStatusStatisticsContainer.Controls.Add(top5LearningPathsByCompletionsList);
                }

                // attach the control before any data bind occurs, if the control is attached
                // after a data bind, the control does not retain its viewstate
                widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

                // initialize the carousel
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "EnrollmentStatisticsCarousel", "$(\".CarouselControl\").slick({dots: true, autoplay: true, autoplaySpeed: 10000,});", true);
            }
        }
        #endregion

        #region _BuildLeaderboardsAdministratorWidget
        /// <summary>
        /// BUILDS LEADER BOARD ADMINISTRATOR WIDGET
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildLeaderboardsAdministratorWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // make sure the leaderboard type is a valid one, if it's not, default to "enrollment_count"
            string leaderboardType = this._LeaderboardsAdministratorLeaderboardType.Value.ToString();

            if (String.IsNullOrWhiteSpace(leaderboardType)
                || (leaderboardType != "enrollment_count"
                && leaderboardType != "enrollment_credits"
                && leaderboardType != "certificate_count"
                && leaderboardType != "certificate_credits"))
            { leaderboardType = "enrollment_count"; }

            // make sure the leaderboard start type is a valid one, if it's not, default to "all_time"
            string leaderboardStartType = this._LeaderboardsAdministratorLeaderboardStartType.Value.ToString();

            if (String.IsNullOrWhiteSpace(leaderboardStartType)
                || (leaderboardStartType != "all_time"
                && leaderboardStartType != "this_year"
                && leaderboardStartType != "this_month"
                && leaderboardStartType != "this_week"))
            { leaderboardStartType = "all_time"; }

            // LEADERBOARD OPTIONS
            Panel leaderboardOptionsContainer = new Panel();
            leaderboardOptionsContainer.ID = widgetId + "LeaderboardOptionsContainer";

            // leaderboard type selector
            DropDownList leaderboardTypeSelector = new DropDownList();
            leaderboardTypeSelector.ID = widgetId + "LeaderboardTypeSelector";

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSETOTAL_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCourseTotal, "enrollment_count")); }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSECREDITS_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCourseCredits, "enrollment_credits")); }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATETOTAL_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCertificateTotal, "certificate_count")); }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATECREDITS_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCertificateCredits, "certificate_credits")); }

            leaderboardTypeSelector.AutoPostBack = true;
            leaderboardTypeSelector.SelectedIndexChanged += this._LeaderboardType_SelectedIndexChanged;
            leaderboardTypeSelector.SelectedValue = leaderboardType;

            leaderboardOptionsContainer.Controls.Add(leaderboardTypeSelector);

            // leaderboard start type selector
            DropDownList leaderboardStartTypeSelector = new DropDownList();
            leaderboardStartTypeSelector.ID = widgetId + "LeaderboardStartTypeSelector";

            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.alltime_lower, "all_time"));
            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.thisyear_lower, "this_year"));
            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.thismonth_lower, "this_month"));
            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.thisweek_lower, "this_week"));

            leaderboardStartTypeSelector.AutoPostBack = true;
            leaderboardStartTypeSelector.SelectedIndexChanged += this._LeaderboardStartType_SelectedIndexChanged;
            leaderboardStartTypeSelector.SelectedValue = leaderboardStartType;

            leaderboardOptionsContainer.Controls.Add(leaderboardStartTypeSelector);

            // attach controls to container
            loadedWidgetContent.Controls.Add(leaderboardOptionsContainer);

            // DATA
            DataTable leaderboardDt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@relativeTo", null, SqlDbType.NVarChar, 50, ParameterDirection.Input);
                databaseObject.AddParameter("@numRecords", 10, SqlDbType.Int, 4, ParameterDirection.Input);

                if (leaderboardStartType == "all_time")
                { databaseObject.AddParameter("@dtStart", null, SqlDbType.DateTime, 8, ParameterDirection.Input); }
                else
                {
                    DateTime startDate = new DateTime();

                    if (leaderboardStartType == "this_year")
                    { startDate = new DateTime(AsentiaSessionState.UtcNow.Year, 1, 1); }
                    else if (leaderboardStartType == "this_month")
                    { startDate = new DateTime(AsentiaSessionState.UtcNow.Year, AsentiaSessionState.UtcNow.Month, 1); }
                    else if (leaderboardStartType == "this_week")
                    {
                        int dowStartDiff = AsentiaSessionState.UtcNow.DayOfWeek - DayOfWeek.Sunday;

                        if (dowStartDiff < 0)
                        { dowStartDiff += 7; }

                        startDate = AsentiaSessionState.UtcNow.AddDays(-1 * dowStartDiff).Date;
                    }
                    else
                    { }

                    databaseObject.AddParameter("@dtStart", startDate, SqlDbType.DateTime, 8, ParameterDirection.Input);
                }

                databaseObject.AddParameter("@leaderboardType", leaderboardType, SqlDbType.NVarChar, 20, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Site.GetLeaderboard]", true);

                // load leaderboard rs
                leaderboardDt.Load(sdr);

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch (AsentiaException ex)
            { throw; }
            finally
            { databaseObject.Dispose(); }

            // LEADERBOARD TABLE
            Panel leaderboardTableContainer = new Panel();
            leaderboardTableContainer.ID = widgetId + "LeaderboardTableContainer";

            Table leaderboardTable = new Table();
            leaderboardTable.ID = widgetId + "LeaderboardTable";
            leaderboardTable.CssClass = "GridTable";

            TableHeaderRow leaderboardTableHeaderRow = new TableHeaderRow();
            leaderboardTableHeaderRow.CssClass = "GridHeaderRow";

            TableHeaderCell leaderboardTableNumberHeaderCell = new TableHeaderCell();
            leaderboardTableNumberHeaderCell.CssClass = "Centered";
            Label numberLabel = new Label();
            numberLabel.Text = _GlobalResources.NumberSymbol;
            leaderboardTableNumberHeaderCell.Controls.Add(numberLabel);
            leaderboardTableHeaderRow.Cells.Add(leaderboardTableNumberHeaderCell);

            TableHeaderCell leaderboardTableNameHeaderCell = new TableHeaderCell();
            Label nameLabel = new Label();
            nameLabel.Text = _GlobalResources.Name;
            leaderboardTableNameHeaderCell.Controls.Add(nameLabel);
            leaderboardTableHeaderRow.Cells.Add(leaderboardTableNameHeaderCell);

            TableHeaderCell leaderboardTableScoreHeaderCell = new TableHeaderCell();
            Label scoreLabel = new Label();
            scoreLabel.Text = _GlobalResources.Score;
            leaderboardTableScoreHeaderCell.Controls.Add(scoreLabel);
            leaderboardTableHeaderRow.Cells.Add(leaderboardTableScoreHeaderCell);

            leaderboardTable.Rows.Add(leaderboardTableHeaderRow);

            int i = 0;            

            foreach (DataRow dr in leaderboardDt.Rows)
            {
                TableRow tr = new TableRow();

                // add css for alternating row
                if (i % 2 == 1)
                { tr.CssClass = "GridDataRow GridDataRowAlternate"; }
                else
                { tr.CssClass = "GridDataRow"; }

                TableCell positionCell = new TableCell();
                positionCell.CssClass = "Centered";
                Label positionLabel = new Label();
                positionLabel.Text = dr["position"].ToString();
                positionCell.Controls.Add(positionLabel);
                tr.Cells.Add(positionCell);

                TableCell learnerNameCell = new TableCell();

                int position = Convert.ToInt32(dr["position"]);

                if (position <= 3)
                {
                    Image trophyImage = new Image();
                    trophyImage.CssClass = "XSmallIcon LeaderboardTrophyIcon";

                    switch (position)
                    {
                        case 1:
                            trophyImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD_GOLD, ImageFiles.EXT_PNG);
                            learnerNameCell.Controls.Add(trophyImage);
                            break;
                        case 2:
                            trophyImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD_SILVER, ImageFiles.EXT_PNG);
                            learnerNameCell.Controls.Add(trophyImage);
                            break;
                        case 3:
                            trophyImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD_BRONZE, ImageFiles.EXT_PNG);
                            learnerNameCell.Controls.Add(trophyImage);
                            break;
                        default:
                            break;
                    }
                }

                Label learnerNameLabel = new Label();
                learnerNameLabel.Text = HttpContext.Current.Server.HtmlEncode(dr["name"].ToString().Replace("[others]", _GlobalResources.Others));
                learnerNameCell.Controls.Add(learnerNameLabel);
                tr.Cells.Add(learnerNameCell);

                TableCell totalCell = new TableCell();
                Label totalLabel = new Label();
                totalLabel.Text = dr["total"].ToString();
                totalCell.Controls.Add(totalLabel);
                tr.Cells.Add(totalCell);

                leaderboardTable.Rows.Add(tr);
                i++;
            }

            TableRow topCoursesByCompletionsLastRow = new TableRow();
            leaderboardTable.Rows.Add(topCoursesByCompletionsLastRow);

            leaderboardTableContainer.Controls.Add(leaderboardTable);

            // attach table to widget
            loadedWidgetContent.Controls.Add(leaderboardTableContainer);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);
        }
        #endregion

        #region _LeaderboardType_SelectedIndexChanged
        /// <summary>
        /// Handles Leader board type list selected index change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LeaderboardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get the leaderboard type selector
            DropDownList leaderboardTypeSelector = (DropDownList)sender;

            // declare variable for leaderboard start type selector
            DropDownList leaderboardStartTypeSelector;

            // declare variable for leaderboard "relative to" selector
            DropDownList leaderboardRelativeToSelector;

            if (leaderboardTypeSelector != null)
            {
                switch (leaderboardTypeSelector.ID)
                {
                    case "LeaderboardsAdministratorWidgetLeaderboardTypeSelector":

                        // set leaderboard type hidden field value
                        this._LeaderboardsAdministratorLeaderboardType.Value = leaderboardTypeSelector.SelectedValue.ToString();

                        // also get and set value for the start type selector
                        leaderboardStartTypeSelector = (DropDownList)this.FindControl("LeaderboardsAdministratorWidgetLeaderboardStartTypeSelector");
                        if (leaderboardStartTypeSelector != null)
                        { this._LeaderboardsAdministratorLeaderboardStartType.Value = leaderboardStartTypeSelector.SelectedValue.ToString(); }

                        // call leaderboard builder method
                        this._BuildLeaderboardsAdministratorWidget("LeaderboardsAdministratorWidget", false);
                        break;

                    case "LeaderboardsLearnerWidgetLeaderboardTypeSelector":

                        // set leaderboard type hidden field value
                        this._LeaderboardsLearnerLeaderboardType.Value = leaderboardTypeSelector.SelectedValue.ToString();

                        // also get and set value for the start type selector
                        leaderboardStartTypeSelector = (DropDownList)this.FindControl("LeaderboardsLearnerWidgetLeaderboardStartTypeSelector");
                        if (leaderboardStartTypeSelector != null)
                        { this._LeaderboardsLearnerLeaderboardStartType.Value = leaderboardStartTypeSelector.SelectedValue.ToString(); }

                        // also get and set value for the "relative to" selector
                        leaderboardRelativeToSelector = (DropDownList)this.FindControl("LeaderboardsLearnerWidgetLeaderboardRelativeToSelector");
                        if (leaderboardRelativeToSelector != null)
                        { this._LeaderboardsLearnerLeaderboardRelativeTo.Value = leaderboardRelativeToSelector.SelectedValue.ToString(); }

                        // call leaderboard builder method
                        this._BuildLeaderboardsLearnerWidget("LeaderboardsLearnerWidget", false);
                        break;

                    default:
                        break;
                }
            }
        }
        #endregion

        #region _LeaderboardStartType_SelectedIndexChanged
        /// <summary>
        /// Handles leader board start type list selected index change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LeaderboardStartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get the leaderboard start type selector
            DropDownList leaderboardStartTypeSelector = (DropDownList)sender;

            // declare variable for leaderboard type selector
            DropDownList leaderboardTypeSelector;

            // declare variable for leaderboard "relative to" selector
            DropDownList leaderboardRelativeToSelector;

            if (leaderboardStartTypeSelector != null)
            {
                switch (leaderboardStartTypeSelector.ID)
                {
                    case "LeaderboardsAdministratorWidgetLeaderboardStartTypeSelector":

                        // set leaderboard start type hidden field value
                        this._LeaderboardsAdministratorLeaderboardStartType.Value = leaderboardStartTypeSelector.SelectedValue.ToString();

                        // also get and set value for the type selector
                        leaderboardTypeSelector = (DropDownList)this.FindControl("LeaderboardsAdministratorWidgetLeaderboardTypeSelector");
                        if (leaderboardTypeSelector != null)
                        { this._LeaderboardsAdministratorLeaderboardType.Value = leaderboardTypeSelector.SelectedValue.ToString(); }

                        // call leaderboard builder method
                        this._BuildLeaderboardsAdministratorWidget("LeaderboardsAdministratorWidget", false);
                        break;

                    case "LeaderboardsLearnerWidgetLeaderboardStartTypeSelector":

                        // set leaderboard start type hidden field value
                        this._LeaderboardsLearnerLeaderboardStartType.Value = leaderboardStartTypeSelector.SelectedValue.ToString();

                        // also get and set value for the type selector
                        leaderboardTypeSelector = (DropDownList)this.FindControl("LeaderboardsLearnerWidgetLeaderboardTypeSelector");
                        if (leaderboardTypeSelector != null)
                        { this._LeaderboardsLearnerLeaderboardType.Value = leaderboardTypeSelector.SelectedValue.ToString(); }

                        // also get and set value for the "relative to" selector
                        leaderboardRelativeToSelector = (DropDownList)this.FindControl("LeaderboardsLearnerWidgetLeaderboardRelativeToSelector");
                        if (leaderboardRelativeToSelector != null)
                        { this._LeaderboardsLearnerLeaderboardRelativeTo.Value = leaderboardRelativeToSelector.SelectedValue.ToString(); }

                        // call leaderboard builder method
                        this._BuildLeaderboardsLearnerWidget("LeaderboardsLearnerWidget", false);
                        break;

                    default:
                        break;
                }
            }
        }
        #endregion

        #region _LeaderboardRelativeTo_SelectedIndexChanged
        /// <summary>
        /// Handles leader board relative to filter list selected index change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LeaderboardRelativeTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get the leaderboard start type selector
            DropDownList leaderboardRelativeToSelector = (DropDownList)sender;

            // declare variable for leaderboard type selector
            DropDownList leaderboardTypeSelector;

            // declare variable for leaderboard start type selector
            DropDownList leaderboardStartTypeSelector;

            if (leaderboardRelativeToSelector != null)
            {
                switch (leaderboardRelativeToSelector.ID)
                {
                    // "relative to" is for learner leaderboard only
                    case "LeaderboardsLearnerWidgetLeaderboardRelativeToSelector":

                        // set leaderboard "relative to" hidden field value
                        this._LeaderboardsLearnerLeaderboardRelativeTo.Value = leaderboardRelativeToSelector.SelectedValue.ToString();

                        // also get and set value for the type selector
                        leaderboardTypeSelector = (DropDownList)this.FindControl("LeaderboardsLearnerWidgetLeaderboardTypeSelector");
                        if (leaderboardTypeSelector != null)
                        { this._LeaderboardsLearnerLeaderboardType.Value = leaderboardTypeSelector.SelectedValue.ToString(); }

                        // also get and set value for the start type selector
                        leaderboardStartTypeSelector = (DropDownList)this.FindControl("LeaderboardsLearnerWidgetLeaderboardStartTypeSelector");
                        if (leaderboardStartTypeSelector != null)
                        { this._LeaderboardsLearnerLeaderboardStartType.Value = leaderboardStartTypeSelector.SelectedValue.ToString(); }

                        // call leaderboard builder method
                        this._BuildLeaderboardsLearnerWidget("LeaderboardsLearnerWidget", false);
                        break;

                    default:
                        break;
                }
            }
        }
        #endregion

        #region _BuildTaskProctoringWidget
        /// <summary>
        /// BUILDS TASK PROCTOR WIDGET
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildTaskProctoringWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            // hidden field for showing/hiding alert icon
            HiddenField taskWidgetHiddenForShowingAlertIcon = new HiddenField();
            taskWidgetHiddenForShowingAlertIcon.ID = widgetId + "HiddenForShowingAlertIcon";
            taskWidgetHiddenForShowingAlertIcon.Value = "0";
            widgetContent.ContentTemplateContainer.Controls.Add(taskWidgetHiddenForShowingAlertIcon);

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // we need to create an update panel specifically for the grid so that when we need to update the grid
            // after a task is proctored, we are only updating the grid and not the entire widget
            UpdatePanel taskProctoringGridUpdatePanel = new UpdatePanel();
            taskProctoringGridUpdatePanel.ID = widgetId + "_TaskProctoringGridUpdatePanel";
            taskProctoringGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            // build the grid
            Grid taskProctoringGrid = new Grid("Account", true);

            taskProctoringGrid.ID = widgetId + "_TaskProctoringGrid";
            taskProctoringGrid.AddCheckboxColumn = false;
            taskProctoringGrid.ShowSearchBox = false;
            taskProctoringGrid.ShowRecordsPerPageSelectbox = false;
            taskProctoringGrid.ShowTimeInDateStrings = false;
            taskProctoringGrid.DefaultRecordsPerPage = 5;

            taskProctoringGrid.StoredProcedure = "[Widget.TaskProctor]";
            taskProctoringGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            taskProctoringGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            taskProctoringGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            taskProctoringGrid.IdentifierField = "idData-Lesson";
            taskProctoringGrid.DefaultSortColumn = "dtUploaded";

            // data key names
            taskProctoringGrid.DataKeyNames = new string[] { "idData-Lesson", "idData-Task" };

            // columns
            GridColumn learner = new GridColumn(_GlobalResources.Learner, "userDisplayName");
            GridColumn courseLesson = new GridColumn(_GlobalResources.CourseModule, null); // contents of this column will be populated by the method that handles RowDataBound
            GridColumn taskView = new GridColumn(_GlobalResources.View, null, true);

            // add columns to data grid 
            taskProctoringGrid.AddColumn(learner);
            taskProctoringGrid.AddColumn(courseLesson);
            taskProctoringGrid.AddColumn(taskView);
            taskProctoringGrid.RowDataBound += new GridViewRowEventHandler(this._TaskProctoringGrid_RowDataBound);

            // attach grid to update panel and update panel to loaded widget content panel
            taskProctoringGridUpdatePanel.ContentTemplateContainer.Controls.Add(taskProctoringGrid);
            loadedWidgetContent.Controls.Add(taskProctoringGridUpdatePanel);

            // build modal popup for grading the tasks
            Button hiddenButtonForTaskModal = new Button();
            hiddenButtonForTaskModal.ID = widgetId + "_HiddenButtonForTaskModal";
            hiddenButtonForTaskModal.Style.Add("display", "none");
            loadedWidgetContent.Controls.Add(hiddenButtonForTaskModal);

            this._BuildTaskModal(loadedWidgetContent, hiddenButtonForTaskModal.ID, widgetId);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            {
                taskProctoringGrid.BindData();
            }

            //show the ! icon on task proctoring widgetbar icon if there aren any items
            if (taskProctoringGrid.RowCount > 0)
            { taskWidgetHiddenForShowingAlertIcon.Value = "1"; }
            else { taskWidgetHiddenForShowingAlertIcon.Value = "0"; }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "AlertIconTaskWidget", "ShowHideAlertIcon('" + taskWidgetHiddenForShowingAlertIcon.ID + "','" + _TASK_PROCTORING_WIDGET_ID + "');", true);
        }
        #endregion

        #region _TaskProctoringGrid_RowDataBound
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _TaskProctoringGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // get data values for row
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idUser = Convert.ToInt32(rowView["idUser"]); // will never be null
                int idEnrollment = Convert.ToInt32(rowView["idEnrollment"]); // will never be null
                int idDataLesson = Convert.ToInt32(rowView["idData-Lesson"]); // will never be null
                int idLesson = Convert.ToInt32(rowView["idLesson"]); // will never be null
                int idDataTask = Convert.ToInt32(rowView["idData-Task"]);
                string uploadedTaskFilename = rowView["uploadedTaskFilename"].ToString();
                string taskUploadedDate = rowView["dtUploaded"].ToString();

                // COURSE/LESSON

                Panel courseNameContainer = new Panel();
                Literal courseName = new Literal();
                courseName.Text = _GlobalResources.Course + ": " + HttpContext.Current.Server.HtmlEncode(rowView["courseName"].ToString());
                courseNameContainer.Controls.Add(courseName);
                e.Row.Cells[1].Controls.Add(courseNameContainer);

                Panel lessonNameContainer = new Panel();
                Literal lessonName = new Literal();
                lessonName.Text = _GlobalResources.Lesson + ": " + HttpContext.Current.Server.HtmlEncode(rowView["lessonName"].ToString());
                lessonNameContainer.Controls.Add(lessonName);
                e.Row.Cells[1].Controls.Add(lessonNameContainer);

                // TASK VIEW BUTTON

                // button
                LinkButton launchTaskLink = new LinkButton();
                launchTaskLink.ID = "LaunchTaskLink_" + idDataLesson.ToString();
                launchTaskLink.OnClientClick = "LoadTaskModalContent(" + idUser + ", " + idEnrollment + ", " + idDataLesson + ", " + idDataTask + ", " + idLesson + ", \"" + uploadedTaskFilename + "\", \"" + taskUploadedDate + "\"); return false;";

                Image launchTaskLinkImage = new Image();
                launchTaskLinkImage.ID = "LaunchTaskLinkImage_" + idDataLesson.ToString();
                launchTaskLinkImage.CssClass = "SmallIcon";
                launchTaskLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                launchTaskLinkImage.AlternateText = _GlobalResources.Task;

                launchTaskLink.Controls.Add(launchTaskLinkImage);

                e.Row.Cells[2].Controls.Add(launchTaskLink);
            }
        }
        #endregion

        #region _BuildTaskModal
        /// <summary>
        /// 
        /// </summary>
        private void _BuildTaskModal(Panel loadedWidgetContent, string triggerId, string widgetId)
        {
            // set modal properties
            ModalPopup taskModal = new ModalPopup(widgetId + "_TaskModal");
            taskModal.Type = ModalPopupType.Form;
            taskModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_TASK, ImageFiles.EXT_PNG);
            taskModal.HeaderIconAlt = _GlobalResources.Task;
            taskModal.HeaderText = _GlobalResources.Task;
            taskModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            taskModal.TargetControlID = triggerId;
            taskModal.SubmitButton.Command += new CommandEventHandler(this._TaskSubmit_Command);

            // build the modal form panel
            Panel taskModalFormPanel = new Panel();
            taskModalFormPanel.ID = widgetId + "_TaskModalFormPanel";
            taskModalFormPanel.Visible = false; // don't render this, we will render in the _LoadTaskModalContent method

            // note, we need to add the controls for setting completion, status, and score so that they register properly in the control stack

            // completion status

            DropDownList completionStatusDropDown = new DropDownList();
            completionStatusDropDown.ID = "AdministrativeTasksWidget_TaskModal_CompletionStatus_Field";
            completionStatusDropDown.Items.Add(new ListItem(_GlobalResources.IncompleteInProgress, Convert.ToInt32(Lesson.LessonCompletionStatus.Incomplete).ToString()));
            completionStatusDropDown.Items.Add(new ListItem(_GlobalResources.Completed, Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString()));

            taskModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("AdministrativeTasksWidget_TaskModal_CompletionStatus",
                                                                       _GlobalResources.CompletionStatus,
                                                                       completionStatusDropDown.ID,
                                                                       completionStatusDropDown,
                                                                       false,
                                                                       false,
                                                                       false));

            // success status

            DropDownList successStatusDropDown = new DropDownList();
            successStatusDropDown.ID = "AdministrativeTasksWidget_TaskModal_SuccessStatus_Field";
            successStatusDropDown.Items.Add(new ListItem(_GlobalResources.Unknown, Convert.ToInt32(Lesson.LessonSuccessStatus.Unknown).ToString()));
            successStatusDropDown.Items.Add(new ListItem(_GlobalResources.Passed, Convert.ToInt32(Lesson.LessonSuccessStatus.Passed).ToString()));
            successStatusDropDown.Items.Add(new ListItem(_GlobalResources.Failed, Convert.ToInt32(Lesson.LessonSuccessStatus.Failed).ToString()));

            taskModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("AdministrativeTasksWidget_TaskModal_SuccessStatus",
                                                                       _GlobalResources.SuccessStatus,
                                                                       successStatusDropDown.ID,
                                                                       successStatusDropDown,
                                                                       false,
                                                                       false,
                                                                       false));

            // score

            TextBox score = new TextBox();
            score.ID = "AdministrativeTasksWidget_TaskModal_Score_Field";
            score.CssClass = "InputXShort";

            taskModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("AdministrativeTasksWidget_TaskModal_Score",
                                                                       _GlobalResources.Score,
                                                                       score.ID,
                                                                       score,
                                                                       false,
                                                                       true,
                                                                       false));

            // build the modal load button
            Button taskModalLoadButton = new Button();
            taskModalLoadButton.ID = widgetId + "_TaskModalLoadButton";
            taskModalLoadButton.Style.Add("display", "none");
            taskModalLoadButton.Click += this._LoadTaskModalContent;

            // build the task data hidden field
            HiddenField taskData = new HiddenField();
            taskData.ID = widgetId + "_TaskData";

            // build the modal body
            taskModal.AddControlToBody(taskModalFormPanel);
            taskModal.AddControlToBody(taskModalLoadButton);
            taskModal.AddControlToBody(taskData);

            // add modal to container
            loadedWidgetContent.Controls.Add(taskModal);
        }
        #endregion

        #region _LoadTaskModalContent
        /// <summary>
        /// Loads content for task modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadTaskModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            ModalPopup taskModal = (ModalPopup)this.FindControl("AdministrativeTasksWidget_TaskModal");
            HiddenField taskData = (HiddenField)taskModal.FindControl("AdministrativeTasksWidget_TaskData");
            Panel taskModalFormPanel = (Panel)taskModal.FindControl("AdministrativeTasksWidget_TaskModalFormPanel");

            try
            {
                // clear the modal feedback
                taskModal.ClearFeedback();

                // make the submit and close buttons visible
                taskModal.SubmitButton.Visible = true;
                taskModal.CloseButton.Visible = true;

                // if the sender is TaskProctoringWidget_TaskModalLoadButton, reset the completion, success, and score form fields
                Button senderButton = (Button)sender;

                if (senderButton.ID == "AdministrativeTasksWidget_TaskModalLoadButton")
                {
                    DropDownList completionStatusData = (DropDownList)taskModalFormPanel.FindControl("AdministrativeTasksWidget_TaskModal_CompletionStatus_Field");
                    DropDownList successStatusData = (DropDownList)taskModalFormPanel.FindControl("AdministrativeTasksWidget_TaskModal_SuccessStatus_Field");
                    TextBox scoreData = (TextBox)taskModalFormPanel.FindControl("AdministrativeTasksWidget_TaskModal_Score_Field");

                    completionStatusData.SelectedIndex = 0;
                    successStatusData.SelectedIndex = 0;
                    scoreData.Text = String.Empty;
                }

                // get the user id, enrollment id, lesson data id, task id, lesson id, uploaded task filename, and task uploaded date
                string[] taskDataItems = taskData.Value.Split('|');
                int idUser = Convert.ToInt32(taskDataItems[0]);
                int idEnrollment = Convert.ToInt32(taskDataItems[1]);
                int idDataLesson = Convert.ToInt32(taskDataItems[2]);
                int idDataTask = Convert.ToInt32(taskDataItems[3]);
                int idLesson = Convert.ToInt32(taskDataItems[4]);


                string uploadedTaskFilename = taskDataItems[5];

                DateTime? taskUploadedDate = null;
                if (!String.IsNullOrWhiteSpace(taskDataItems[6]))
                {
                    taskUploadedDate = Convert.ToDateTime(taskDataItems[6]);

                    // convert to user's local time
                    taskUploadedDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)taskUploadedDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                }

                // build the form
                if (!String.IsNullOrWhiteSpace(uploadedTaskFilename))
                {
                    Panel taskInstructionsPanel = new Panel();
                    taskInstructionsPanel.ID = "AdministrativeTasksWidget_TaskModal_TaskInstructionsPanel";
                    this._AsentiaPageInstance.FormatPageInformationPanel(taskInstructionsPanel, _GlobalResources.ViewTheSubmittedTaskAndGradeItUsingTheFormBelow, true);
                    taskModalFormPanel.Controls.AddAt(0, taskInstructionsPanel); // we use AddAt because there are other controls already in the panel, and we want this to render before them

                    // UPLOADED TASK

                    List<Control> uploadedTaskControls = new List<Control>();

                    // uploaded task link
                    HyperLink uploadedTaskLink = new HyperLink();
                    uploadedTaskLink.NavigateUrl = SitePathConstants.SITE_USERS_ROOT + idUser.ToString() + "/Tasks/" + idDataLesson.ToString() + "/" + uploadedTaskFilename;
                    uploadedTaskLink.Target = "_blank";
                    uploadedTaskLink.Text = uploadedTaskFilename;
                    uploadedTaskControls.Add(uploadedTaskLink);

                    // uploaded on
                    Panel uploadedTaskUploadDateContainer = new Panel();
                    uploadedTaskUploadDateContainer.ID = "AdministrativeTasksWidget_TaskModal_UploadedTaskUploadDateContainer";

                    Literal uploadedTaskUploadDate = new Literal();
                    uploadedTaskUploadDate.Text = _GlobalResources.TheTaskWasSubmittedOn + " " + taskUploadedDate.ToString();
                    uploadedTaskUploadDateContainer.Controls.Add(uploadedTaskUploadDate);

                    uploadedTaskControls.Add(uploadedTaskUploadDateContainer);

                    // we use AddAt because there are other controls already in the panel, and we want this to render before them
                    taskModalFormPanel.Controls.AddAt(1, AsentiaPage.BuildMultipleInputControlFormField("AdministrativeTasksWidget_TaskModal_UploadedTask",
                                                                                                        _GlobalResources.UploadedTask,
                                                                                                        uploadedTaskControls,
                                                                                                        false,
                                                                                                        false));

                    // set the form panel so it will render
                    taskModalFormPanel.Visible = true;
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(dnfEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(fnuEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(cpeEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(dEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                taskModal.DisplayFeedback(ex.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _TaskSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the task modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _TaskSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            ModalPopup taskModal = (ModalPopup)this.FindControl("AdministrativeTasksWidget_TaskModal");
            HiddenField taskData = (HiddenField)taskModal.FindControl("AdministrativeTasksWidget_TaskData");
            Panel taskModalFormPanel = (Panel)taskModal.FindControl("AdministrativeTasksWidget_TaskModalFormPanel");
            Grid taskProctoringGrid = (Grid)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGrid");
            UpdatePanel taskProctoringGridUpdatePanel = (UpdatePanel)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGridUpdatePanel");

            try
            {
                // clear the modal feedback
                taskModal.ClearFeedback();

                // get the enrollment id, lesson data id, and task id
                string[] taskDataItems = taskData.Value.Split('|');
                int idEnrollment = Convert.ToInt32(taskDataItems[1]);
                int idDataLesson = Convert.ToInt32(taskDataItems[2]);
                int idDataTask = Convert.ToInt32(taskDataItems[3]);

                // get the values from the completion, success, and score controls

                DropDownList completionStatusData = (DropDownList)taskModalFormPanel.FindControl("AdministrativeTasksWidget_TaskModal_CompletionStatus_Field");
                DropDownList successStatusData = (DropDownList)taskModalFormPanel.FindControl("AdministrativeTasksWidget_TaskModal_SuccessStatus_Field");
                TextBox scoreData = (TextBox)taskModalFormPanel.FindControl("AdministrativeTasksWidget_TaskModal_Score_Field");

                int completion = Convert.ToInt32(completionStatusData.SelectedValue);
                int success = Convert.ToInt32(successStatusData.SelectedValue);
                int? score = null;

                // validate the data and save

                // if success is passed, completion must be completed
                if ((Lesson.LessonSuccessStatus)success == Lesson.LessonSuccessStatus.Passed && (Lesson.LessonCompletionStatus)completion != Lesson.LessonCompletionStatus.Completed)
                {
                    this._LoadTaskModalContent(sender, e);
                    taskModal.DisplayFeedback(_GlobalResources.WhenSuccessStatusIsMarkedAsPassedCompletionStatusMustBeMarkedAsCompleted, true);
                }
                else
                {
                    bool isValid = true;

                    // validate score
                    int parsedScore = 0;

                    if ((!String.IsNullOrWhiteSpace(scoreData.Text) && !int.TryParse(scoreData.Text, out parsedScore)))
                    {
                        isValid = false;
                        this._LoadTaskModalContent(sender, e);
                        this._AsentiaPageInstance.ApplyErrorMessageToFieldErrorPanel(taskModalFormPanel, "AdministrativeTasksWidget_TaskModal_Score", _GlobalResources.Score + " " + _GlobalResources.IsInvalid);
                        taskModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                    }
                    else
                    { score = parsedScore; }

                    if (isValid)
                    {
                        DataLesson.ProctorTask(idEnrollment, idDataLesson, idDataTask, completion, success, score);

                        taskModalFormPanel.Controls.Clear();
                        taskModal.SubmitButton.Visible = false;
                        taskModal.CloseButton.Visible = false;
                        taskModal.DisplayFeedback(_GlobalResources.TheTaskDataHasBeenSavedSuccessfully, false);

                        taskProctoringGrid.BindData();
                        taskProctoringGridUpdatePanel.Update();
                    }
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(dnfEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(fnuEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(cpeEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(dEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                taskModal.DisplayFeedback(ex.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildOJTProctoringWidget
        /// <summary>
        /// BUILDS OJT PROCTOR WIDGET
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildOJTProctoringWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            //adding a hidden field,used for shoing/hiding alert icon
            HiddenField ojtWidgetHiddenForShowingAlertIcon = new HiddenField();
            ojtWidgetHiddenForShowingAlertIcon.ID = widgetId + "HiddenForShowingAlertIcon";
            ojtWidgetHiddenForShowingAlertIcon.Value = "0";
            widgetContent.ContentTemplateContainer.Controls.Add(ojtWidgetHiddenForShowingAlertIcon);

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            Grid ojtProctoringGrid = new Grid("Account", true);

            ojtProctoringGrid.ID = widgetId + "OJTProctoringGrid";
            ojtProctoringGrid.AddCheckboxColumn = false;
            ojtProctoringGrid.ShowSearchBox = false;
            ojtProctoringGrid.ShowRecordsPerPageSelectbox = false;
            ojtProctoringGrid.ShowTimeInDateStrings = false;
            ojtProctoringGrid.DefaultRecordsPerPage = 5;

            ojtProctoringGrid.StoredProcedure = "[Widget.OJTProctor]";
            ojtProctoringGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            ojtProctoringGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            ojtProctoringGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            ojtProctoringGrid.IdentifierField = "idData-Lesson";
            ojtProctoringGrid.DefaultSortColumn = "userDisplayName";

            // data key names
            ojtProctoringGrid.DataKeyNames = new string[] { "idData-Lesson", "idContentPackage" };

            // columns
            GridColumn learner = new GridColumn(_GlobalResources.Learner, "userDisplayName");
            GridColumn courseLesson = new GridColumn(_GlobalResources.CourseModule, null); // contents of this column will be populated by the method that handles RowDataBound

            GridColumn ojtView = new GridColumn(_GlobalResources.View, "idContentPackageType", true);

            ojtView.AddProperty(new GridColumnProperty(Convert.ToInt32(ContentPackage.ContentPackageType.SCORM).ToString(), "<a href=\"/launch/Scorm.aspx?id=##idData-Lesson##&cpid=##idContentPackage##&launchType=ojtWidget\">"
                                                            + "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"
                                                            + "</a>"));

            ojtView.AddProperty(new GridColumnProperty(Convert.ToInt32(ContentPackage.ContentPackageType.xAPI).ToString(), "<a href=\"/launch/Tincan.aspx?id=##idData-Lesson##&cpid=##idContentPackage##&launchType=ojtWidget\">"
                                                            + "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"
                                                            + "</a>"));

            // add columns to data grid 
            ojtProctoringGrid.AddColumn(learner);
            ojtProctoringGrid.AddColumn(courseLesson);
            ojtProctoringGrid.AddColumn(ojtView);
            ojtProctoringGrid.RowDataBound += new GridViewRowEventHandler(this._OJTProctoringGrid_RowDataBound);

            loadedWidgetContent.Controls.Add(ojtProctoringGrid);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            {
                ojtProctoringGrid.BindData();
            }

            ////show the ! icon on OJT protectoring widgetbar icon if there aren any items
            if (ojtProctoringGrid.RowCount > 0)
            { ojtWidgetHiddenForShowingAlertIcon.Value = "1"; }
            else { ojtWidgetHiddenForShowingAlertIcon.Value = "0"; }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "AlertIconOJTProctoringWidget", "ShowHideAlertIcon('" + ojtWidgetHiddenForShowingAlertIcon.ID + "','" + _OJT_PROCTORING_WIDGET_ID + "');", true);
        }
        #endregion

        #region _OJTProctoringGrid_RowDataBound
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _OJTProctoringGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;

                // COURSE/LESSON

                Panel courseNameContainer = new Panel();
                Literal courseName = new Literal();
                courseName.Text = _GlobalResources.Course + ": " + HttpContext.Current.Server.HtmlEncode(rowView["courseName"].ToString());
                courseNameContainer.Controls.Add(courseName);
                e.Row.Cells[1].Controls.Add(courseNameContainer);

                Panel lessonNameContainer = new Panel();
                Literal lessonName = new Literal();
                lessonName.Text = _GlobalResources.Lesson + ": " + HttpContext.Current.Server.HtmlEncode(rowView["lessonName"].ToString());
                lessonNameContainer.Controls.Add(lessonName);
                e.Row.Cells[1].Controls.Add(lessonNameContainer);
            }
        }
        #endregion

        #region _BuildILTRosterManagementWidget
        /// <summary>
        /// BUILDS ILT ROSTER MANAGEMENT WIDGET
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildILTRosterManagementWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            Grid iltRosterManagementGrid = new Grid("Account", true);

            iltRosterManagementGrid.ID = widgetId + "ILTRosterManagementGrid";
            iltRosterManagementGrid.AddCheckboxColumn = false;
            iltRosterManagementGrid.ShowSearchBox = false;

            iltRosterManagementGrid.ShowRecordsPerPageSelectbox = false;
            iltRosterManagementGrid.ShowTimeInDateStrings = true;
            iltRosterManagementGrid.DefaultRecordsPerPage = 5;

            iltRosterManagementGrid.StoredProcedure = "[Widget.ILTRosterManagement]";
            iltRosterManagementGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            iltRosterManagementGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            iltRosterManagementGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            iltRosterManagementGrid.IdentifierField = "idStandupTrainingInstance";
            iltRosterManagementGrid.DefaultSortColumn = "dtStart";

            // data key names
            iltRosterManagementGrid.DataKeyNames = new string[] { "idStandupTrainingInstance", "idStandupTraining" };

            // columns
            GridColumn moduleSession = new GridColumn(_GlobalResources.ModuleSession, null); // contents of this column will be populated by the method that handles RowDataBound

            GridColumn dtStart = new GridColumn(_GlobalResources.StartDate, "dtStart");

            GridColumn launch = new GridColumn(_GlobalResources.Launch, null, true); // contents of this column will be populated by the method that handles RowDataBound

            GridColumn manage = new GridColumn(_GlobalResources.ManageRoster, "manage", true);

            manage.AddProperty(new GridColumnProperty("True", "<a href=\"/administrator/standuptraining/sessions/ManageRoster.aspx?stid=##idStandupTraining##&id=##idStandupTrainingInstance##\">"
                                                                + "<img class=\"SmallIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION_BUTTON, ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.ManageRoster + "\" />"
                                                                + "</a>"));
            manage.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION_BUTTON, ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.ManageRosterDisabled + "\" />"));

            // add columns to data grid 
            iltRosterManagementGrid.AddColumn(moduleSession);
            iltRosterManagementGrid.AddColumn(dtStart);
            iltRosterManagementGrid.AddColumn(launch);
            iltRosterManagementGrid.AddColumn(manage);
            iltRosterManagementGrid.RowDataBound += new GridViewRowEventHandler(this._ILTRosterManagementGrid_RowDataBound);

            loadedWidgetContent.Controls.Add(iltRosterManagementGrid);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            {
                iltRosterManagementGrid.BindData();
            }
        }
        #endregion

        #region _ILTRosterManagementGrid_RowDataBound
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ILTRosterManagementGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;

                // MODULE/SESSION
                Panel moduleTitleContainer = new Panel();
                Literal moduleTitle = new Literal();
                moduleTitle.Text = _GlobalResources.Module + ": " + HttpContext.Current.Server.HtmlEncode(rowView["moduleTitle"].ToString());
                moduleTitleContainer.Controls.Add(moduleTitle);
                e.Row.Cells[0].Controls.Add(moduleTitleContainer);

                Panel sessionTitleContainer = new Panel();
                Literal sessionTitle = new Literal();
                sessionTitle.Text = _GlobalResources.Session + ": " + HttpContext.Current.Server.HtmlEncode(rowView["sessionTitle"].ToString());
                sessionTitleContainer.Controls.Add(sessionTitle);
                e.Row.Cells[0].Controls.Add(sessionTitleContainer);

                //2 LAUNCH
                if (Convert.ToBoolean(rowView["launch"]) && !String.IsNullOrWhiteSpace(rowView["hostUrl"].ToString()))
                {
                    HyperLink hostLaunchLink = new HyperLink();
                    hostLaunchLink.NavigateUrl = rowView["hostUrl"].ToString();
                    hostLaunchLink.Target = "_blank";

                    Image hostLaunchImage = new Image();
                    hostLaunchImage.CssClass = "SmallIcon";

                    if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(rowView["type"]) == StandupTrainingInstance.MeetingType.GoToMeeting)
                    {
                        hostLaunchImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOMEETING, ImageFiles.EXT_PNG);
                        hostLaunchImage.AlternateText = _GlobalResources.GoToMeeting;
                    }
                    else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(rowView["type"]) == StandupTrainingInstance.MeetingType.GoToTraining)
                    {
                        hostLaunchImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOTRAINING, ImageFiles.EXT_PNG);
                        hostLaunchImage.AlternateText = _GlobalResources.GoToTraining;
                    }
                    else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(rowView["type"]) == StandupTrainingInstance.MeetingType.GoToWebinar)
                    {
                        hostLaunchImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOWEBINAR, ImageFiles.EXT_PNG);
                        hostLaunchImage.AlternateText = _GlobalResources.GoToWebinar;
                    }
                    else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(rowView["type"]) == StandupTrainingInstance.MeetingType.WebEx)
                    {
                        hostLaunchImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_WEBEX, ImageFiles.EXT_PNG);
                        hostLaunchImage.AlternateText = _GlobalResources.WebEx;
                    }
                    else
                    { }
                    
                    hostLaunchLink.Controls.Add(hostLaunchImage);
                    e.Row.Cells[2].Controls.Add(hostLaunchLink);
                }
            }
        }
        #endregion

        #region _BuildReportSubscriptionsWidget
        /// <summary>
        /// Method to build the Report Subscriptions Widget.
        /// </summary>
        /// <param name="widgetId">widget id</param>
        private void _BuildReportSubscriptionsWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            Grid reportGrid = new Grid("Account", true);

            reportGrid.ID = widgetId + "ReportSubscriptionGrid";
            reportGrid.StoredProcedure = "[Report.GetSubscriptionForUser]";
            reportGrid.DefaultRecordsPerPage = 10;
            reportGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            reportGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            reportGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            reportGrid.AddFilter("@idUser", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            reportGrid.IdentifierField = "idReportSubscription";
            reportGrid.DefaultSortColumn = "title";
            reportGrid.ShowTimeInDateStrings = false;

            // data key names
            reportGrid.DataKeyNames = new string[] { "idReportSubscription" };

            // columns
            GridColumn reportName = new GridColumn(_GlobalResources.Report, "title", "title");
            GridColumn datasetName = new GridColumn(_GlobalResources.Dataset, "dataSet", "dataSet");
            GridColumn ownerName = new GridColumn(_GlobalResources.Owner, "ownerName", "ownerName");

            // add columns to data grid
            reportGrid.AddColumn(reportName);
            reportGrid.AddColumn(datasetName);
            reportGrid.AddColumn(ownerName);

            loadedWidgetContent.Controls.Add(reportGrid);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { reportGrid.BindData(); }
        }
        #endregion

        #region _BuildReportShortcutsWidget
        /// <summary>
        /// Method to build the Report Shortcuts Widget.
        /// </summary>
        /// <param name="widgetId">widget id</param>
        private void _BuildReportShortcutsWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            Grid reportGrid = new Grid("Account", true);

            reportGrid.ID = widgetId + "ReportShortcutsGrid";
            reportGrid.StoredProcedure = "[Report.GetListShortcuts]";
            reportGrid.DefaultRecordsPerPage = 10;
            reportGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            reportGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            reportGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            reportGrid.AddFilter("@idUser", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            reportGrid.IdentifierField = "idReportToReportShortcutsWidgetLink";
            reportGrid.DefaultSortColumn = "reportName";
            reportGrid.ShowTimeInDateStrings = false;

            // data key names
            reportGrid.DataKeyNames = new string[] { "idReportToReportShortcutsWidgetLink", "idReport", "reportName" };

            // columns
            GridColumn reportName = new GridColumn(_GlobalResources.Report, "reportName", "reportName");
            reportName.AddProperty(new GridColumnProperty("True", "<a href=\"/reporting/reports/Modify.aspx?idReport=##idReport##\">"
                                                    + "##reportName##"
                                                    + "</a>"));
            GridColumn ownerName = new GridColumn(_GlobalResources.Owner, "ownerName", "ownerName");

            // add columns to data grid
            reportGrid.AddColumn(reportName);
            reportGrid.AddColumn(ownerName);

            loadedWidgetContent.Controls.Add(reportGrid);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { reportGrid.BindData(); }
        }
        #endregion        

        #region _BuildUserRegistrationApprovalWidget
        /// <summary>
        /// BUILDS USER REGISTRATION APPROVAL WIDGET
        /// </summary>
        /// <param name="widgetId">Widget ID</param>
        /// <param name="isInitialLoad">Is this an initial load?</param>
        private void _BuildUserRegistrationApprovalWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // we need to create an update panel specifically for the grid so that when we need to update the grid
            // after a registration is approved or rejected, we are only updating the grid and not the entire widget
            UpdatePanel userRegistrationApprovalGridUpdatePanel = new UpdatePanel();
            userRegistrationApprovalGridUpdatePanel.ID = widgetId + "UserRegistrationApprovalGridUpdatePanel";
            userRegistrationApprovalGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            Grid userRegistrationApprovalGrid = new Grid("Account", true);

            userRegistrationApprovalGrid.ID = widgetId + "UserRegistrationApprovalGrid";
            userRegistrationApprovalGrid.AddCheckboxColumn = false;
            userRegistrationApprovalGrid.ShowSearchBox = false;
            
            userRegistrationApprovalGrid.StoredProcedure = "[Widget.UserRegistrationApproval]";
            userRegistrationApprovalGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            userRegistrationApprovalGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            userRegistrationApprovalGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            
            userRegistrationApprovalGrid.DefaultRecordsPerPage = 10;
            userRegistrationApprovalGrid.IdentifierField = "idUser";
            userRegistrationApprovalGrid.DefaultSortColumn = "displayName";

            // data key names
            userRegistrationApprovalGrid.DataKeyNames = new string[] { "idUser" };            

            // columns
            GridColumn name = new GridColumn(_GlobalResources.Name, "displayName", "displayName");
            GridColumn userName = new GridColumn(_GlobalResources.Username, "username", "username");
            GridColumn emailAddress = new GridColumn(_GlobalResources.EmailAddress, "email", "email");
            
            GridColumn approve = new GridColumn(_GlobalResources.Approve, null, true);
            GridColumn reject = new GridColumn(_GlobalResources.Reject, null, true);
            
            // add columns to data grid 
            userRegistrationApprovalGrid.AddColumn(name);
            userRegistrationApprovalGrid.AddColumn(userName);
            userRegistrationApprovalGrid.AddColumn(emailAddress);
            userRegistrationApprovalGrid.AddColumn(approve);
            userRegistrationApprovalGrid.AddColumn(reject);

            userRegistrationApprovalGrid.RowDataBound += new GridViewRowEventHandler(this._UserRegistrationApprovalGrid_RowDataBound);

            // attach grid to update panel and update panel to loaded widget content panel
            userRegistrationApprovalGridUpdatePanel.ContentTemplateContainer.Controls.Add(userRegistrationApprovalGrid);
            loadedWidgetContent.Controls.Add(userRegistrationApprovalGridUpdatePanel);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);
                          
            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { userRegistrationApprovalGrid.BindData(); }            
        }
        #endregion

        #region _UserRegistrationApprovalGrid_RowDataBound
        /// <summary>
        /// Data Bound event handler for the User Registration Approval Widget to display
        /// approve and reject image buttons for each row.
        /// </summary>
        /// <param name="sender">Sender Row</param>
        /// <param name="e">Event Arguments</param>
        private void _UserRegistrationApprovalGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {                
                // retrieve user's ID from the row
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idUser = Convert.ToInt32(rowView["idUser"]);
                                                
                // approve button
                HyperLink approveLink = new HyperLink();
                approveLink.ID = "UserRegistrationApproval_" + idUser.ToString();
                approveLink.ToolTip = _GlobalResources.ApproveRegistration;
                approveLink.Attributes.Add("onclick", "UserRegistrationApproveRejectClick('" + idUser.ToString() + "', 'approve');");                

                Image approveImage = new Image();
                approveImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                approveImage.CssClass = "SmallIcon";
                approveLink.Controls.Add(approveImage);                                                                          

                // attach the control to the cell
                e.Row.Cells[3].Controls.Add(approveLink);                                

                // reject button
                HyperLink rejectLink = new HyperLink();
                rejectLink.ID = "UserRegistrationRejection_" + idUser.ToString();
                rejectLink.ToolTip = _GlobalResources.RejectRegistration;
                rejectLink.Attributes.Add("onclick", "UserRegistrationApproveRejectClick('" + idUser.ToString() + "', 'reject');");

                Image rejectImage = new Image();
                rejectImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                rejectImage.CssClass = "SmallIcon";
                rejectLink.Controls.Add(rejectImage);                

                // attach the control to the cell
                e.Row.Cells[4].Controls.Add(rejectLink); 
            }
        }
        #endregion

        #region _BuildPendingUserRegistrationActionModal
        /// <summary>
        /// Builds the approval/rejection modal for pending user registrations.
        /// </summary>        
        private void _BuildPendingUserRegistrationActionModal(string targetControlId, Control modalContainer)
        {
            this._ApproveRejectPendingUserRegistrationModal = new ModalPopup("ApproveRejectPendingUserRegistrationModal");
            this._ApproveRejectPendingUserRegistrationModal.Type = ModalPopupType.Form;
            this._ApproveRejectPendingUserRegistrationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);
            this._ApproveRejectPendingUserRegistrationModal.HeaderIconAlt = _GlobalResources.PendingUserRegistrations;
            this._ApproveRejectPendingUserRegistrationModal.HeaderText = _GlobalResources.PendingUserRegistrations;
            this._ApproveRejectPendingUserRegistrationModal.TargetControlID = targetControlId;
            this._ApproveRejectPendingUserRegistrationModal.ReloadPageOnClose = false;

            // the submit button of this modal should only handle rejection
            this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Command += this._UserRegistrationReject_Command;

            // build the reject comments container
            Panel rejectionCommentsContainer = new Panel();
            rejectionCommentsContainer.ID = "ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer";

            // build the reject comments textbox
            TextBox rejectCommentsTextbox = new TextBox();
            rejectCommentsTextbox.ID = "ApproveRejectPendingUserRegistrationModal_RejectCommentsTextbox_Field";
            rejectCommentsTextbox.TextMode = TextBoxMode.MultiLine;
            rejectCommentsTextbox.Rows = 5;

            rejectionCommentsContainer.Controls.Add(AsentiaPage.BuildFormField("ApproveRejectPendingUserRegistrationModal_RejectCommentsTextbox",
                                                                        _GlobalResources.PleaseEnterTheReasonForRejectingThisRegistration,
                                                                        rejectCommentsTextbox.ID,
                                                                        rejectCommentsTextbox,
                                                                        false,
                                                                        false,
                                                                        false));

            this._ApproveRejectPendingUserRegistrationModal.AddControlToBody(rejectionCommentsContainer);

            // add the load button to the modal
            Button approveRejectPendingUserRegistrationModalHiddenLoadButton = new Button();
            approveRejectPendingUserRegistrationModalHiddenLoadButton.ID = "ApproveRejectPendingUserRegistrationModalHiddenLoadButton";
            approveRejectPendingUserRegistrationModalHiddenLoadButton.Command += this._UserRegistrationApproveReject_Click;
            approveRejectPendingUserRegistrationModalHiddenLoadButton.Style.Add("display", "none");

            this._ApproveRejectPendingUserRegistrationModal.AddControlToBody(approveRejectPendingUserRegistrationModalHiddenLoadButton);

            // attach modal to container
            modalContainer.Controls.Add(this._ApproveRejectPendingUserRegistrationModal);
        }
        #endregion

        #region _UserRegistrationApproveReject_Click
        /// <summary>
        /// Click event handler for approving or rejecting a user registration request.
        /// </summary>
        /// <param name="sender">Sender Button</param>
        /// <param name="e">Event Arguments</param>

        private void _UserRegistrationApproveReject_Click(Object sender, CommandEventArgs e)
        {
            string[] userRegistrationData = this._ApproveRejectPendingUserRegistrationData.Value.Split('|');
            int idUser = Convert.ToInt32(userRegistrationData[0]);
            string action = userRegistrationData[1];

            if (action == "approve")
            {
                // register startup script to change the header
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ChangePendingUserRegistrationWidgetHeader", "$(\"#ApproveRejectPendingUserRegistrationModalModalPopupHeaderText\").html(ApproveUserRegistrationText);", true);

                // hide the submit and close buttons, and the rejection container
                this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;

                // do the approval
                AsentiaDatabase databaseObject = new AsentiaDatabase();

                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@isRegistrationApproved", 1, SqlDbType.Bit, 1, ParameterDirection.Input); // THIS IS AN APPROVAL - set approval flag to 1
                databaseObject.AddParameter("@idApprover", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                try
                {
                    databaseObject.ExecuteNonQuery("[User.ApproveRejectRegistration]", true);

                    DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                    // show the feedback
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(_GlobalResources.TheUserRegistrationHasBeenApprovedSuccessfully, false);

                    // refresh the grid
                    Grid userRegistrationApprovalGrid = (Grid)this.FindControl("UserRegistrationApprovalWidgetUserRegistrationApprovalGrid");
                    UpdatePanel userRegistrationApprovalGridUpdatePanel = (UpdatePanel)this.FindControl("UserRegistrationApprovalWidgetUserRegistrationApprovalGridUpdatePanel");
                    if (userRegistrationApprovalGrid != null && userRegistrationApprovalGridUpdatePanel != null)
                    {
                        userRegistrationApprovalGrid.BindData();
                        userRegistrationApprovalGridUpdatePanel.Update();
                    }

                    // administrative tasks grid
                    Grid administrativeTasksGrid = (Grid)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGrid");
                    UpdatePanel administrativeTasksGridUpdatePanel = (UpdatePanel)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGridUpdatePanel");
                    if (administrativeTasksGrid != null && administrativeTasksGridUpdatePanel != null)
                    {
                        administrativeTasksGrid.BindData();
                        administrativeTasksGridUpdatePanel.Update();
                    }
                }                
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(dnfEx.Message, true);
                    this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                }
                catch (DatabaseFieldNotUniqueException fnuEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(fnuEx.Message, true);
                    this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                }
                catch (DatabaseCallerPermissionException cpeEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(cpeEx.Message, true);
                    this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                }
                catch (DatabaseException dEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(dEx.Message, true);
                    this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                }
                catch (AsentiaException ex)
                {
                    // display the failure message
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(ex.Message, true);
                    this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                }
                finally
                {
                    databaseObject.Dispose();
                }
            }
            else // reject
            {
                // register startup script to change the header
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ChangePendingUserRegistrationWidgetHeader", "$(\"#ApproveRejectPendingUserRegistrationModalModalPopupHeaderText\").html(RejectUserRegistrationText);", true);

                // show the submit and close buttons, and the rejection container
                this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = true;
                this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = true;
                this._ApproveRejectPendingUserRegistrationModal.SubmitButton.CssClass = this._ApproveRejectPendingUserRegistrationModal.SubmitButton.CssClass.Replace("DisabledButton", "");
                this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = true;
                this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = true;
            }                                  
        }
        #endregion

        #region _UserRegistrationReject_Command
        /// <summary>
        /// Click event handler for rejecting the user registration and submitting rejection comments.
        /// </summary>
        /// <param name="sender">Sender Button</param>
        /// <param name="e">Command Arguments</param>

        private void _UserRegistrationReject_Command(Object sender, CommandEventArgs e)
        {
            string[] userRegistrationData = this._ApproveRejectPendingUserRegistrationData.Value.Split('|');
            TextBox rejectionComments = (TextBox)this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectCommentsTextbox_Field");
            int idUser = Convert.ToInt32(userRegistrationData[0]);
            string action = userRegistrationData[1];

            if (action == "reject")
            {                
                AsentiaDatabase databaseObject = new AsentiaDatabase();

                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@isRegistrationApproved", 0, SqlDbType.Bit, 1, ParameterDirection.Input); // THIS IS A REJECTION - set approval flag to 0
                databaseObject.AddParameter("@idApprover", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@rejectionComments", rejectionComments.Text, SqlDbType.NVarChar, -1, ParameterDirection.Input);

                try
                {                    
                    databaseObject.ExecuteNonQuery("[User.ApproveRejectRegistration]", true);

                    DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                    // show the feedback
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(_GlobalResources.TheUserRegistrationHasBeenRejectedSuccessfully, false);
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.CssClass += " DisabledButton";

                    // refresh the grid
                    Grid userRegistrationApprovalGrid = (Grid)this.FindControl("UserRegistrationApprovalWidgetUserRegistrationApprovalGrid");
                    UpdatePanel userRegistrationApprovalGridUpdatePanel = (UpdatePanel)this.FindControl("UserRegistrationApprovalWidgetUserRegistrationApprovalGridUpdatePanel");
                    if (userRegistrationApprovalGrid != null && userRegistrationApprovalGridUpdatePanel != null)
                    {
                        userRegistrationApprovalGrid.BindData();
                        userRegistrationApprovalGridUpdatePanel.Update();
                    }

                    // administrative tasks grid
                    Grid administrativeTasksGrid = (Grid)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGrid");
                    UpdatePanel administrativeTasksGridUpdatePanel = (UpdatePanel)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGridUpdatePanel");
                    administrativeTasksGrid.BindData();
                    administrativeTasksGridUpdatePanel.Update();
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(dnfEx.Message, true);
                    this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                }
                catch (DatabaseFieldNotUniqueException fnuEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(fnuEx.Message, true);
                    this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                }
                catch (DatabaseCallerPermissionException cpeEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(cpeEx.Message, true);
                    this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                }
                catch (DatabaseException dEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(dEx.Message, true);
                    this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                }
                catch (AsentiaException ex)
                {
                    // display the failure message
                    this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(ex.Message, true);
                    this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
                }
                finally
                {
                    databaseObject.Dispose();
                }
            }
            else // we should never see this, if we do, it's a problem
            {
                // display the failure message
                this._ApproveRejectPendingUserRegistrationModal.DisplayFeedback(_GlobalResources.TheUserRegistrationRecordWasNotMarkedForRejection, true);
                this._ApproveRejectPendingUserRegistrationModal.FindControl("ApproveRejectPendingUserRegistrationModal_RejectionCommentsContainer").Visible = false;
                this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Visible = false;
                this._ApproveRejectPendingUserRegistrationModal.SubmitButton.Enabled = false;
                this._ApproveRejectPendingUserRegistrationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildCourseEnrollmentApprovalWidget
        /// <summary>
        /// BUILDS COURSE ENROLLMENT APPROVAL WIDGET
        /// </summary>
        /// <param name="widgetId">Widget ID</param>
        /// <param name="isInitialLoad">Is this an initial load?</param>
        private void _BuildCourseEnrollmentApprovalWidget(string widgetId, bool isInitialLoad)
        {            
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // we need to create an update panel specifically for the grid so that when we need to update the grid
            // after a registration is approved or rejected, we are only updating the grid and not the entire widget
            UpdatePanel courseEnrollmentApprovalGridUpdatePanel = new UpdatePanel();
            courseEnrollmentApprovalGridUpdatePanel.ID = widgetId + "CourseEnrollmentApprovalGridUpdatePanel";
            courseEnrollmentApprovalGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            Grid courseEnrollmentApprovalGrid = new Grid("Account", true);

            courseEnrollmentApprovalGrid.ID = widgetId + "CourseEnrollmentApprovalGrid";
            courseEnrollmentApprovalGrid.AddCheckboxColumn = false;
            courseEnrollmentApprovalGrid.ShowSearchBox = false;

            courseEnrollmentApprovalGrid.StoredProcedure = "[Widget.SelfEnrollmentApproval]";
            courseEnrollmentApprovalGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            courseEnrollmentApprovalGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            courseEnrollmentApprovalGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);

            courseEnrollmentApprovalGrid.DefaultRecordsPerPage = 10;
            courseEnrollmentApprovalGrid.IdentifierField = "idEnrollmentRequest";
            courseEnrollmentApprovalGrid.DefaultSortColumn = "displayName";

            // data key names
            courseEnrollmentApprovalGrid.DataKeyNames = new string[] { "idEnrollmentRequest" };

            // columns
            GridColumn name = new GridColumn(_GlobalResources.Name, "displayName", "displayName");
            GridColumn course = new GridColumn(_GlobalResources.Course, "course", "course");            
            GridColumn approve = new GridColumn(_GlobalResources.Approve, null, true);
            GridColumn reject = new GridColumn(_GlobalResources.Reject, null, true);

            // add columns to data grid 
            courseEnrollmentApprovalGrid.AddColumn(name);
            courseEnrollmentApprovalGrid.AddColumn(course);
            courseEnrollmentApprovalGrid.AddColumn(approve);
            courseEnrollmentApprovalGrid.AddColumn(reject);

            courseEnrollmentApprovalGrid.RowDataBound += new GridViewRowEventHandler(this._CourseEnrollmentApprovalGrid_RowDataBound);

            // attach grid to update panel and update panel to loaded widget content panel
            courseEnrollmentApprovalGridUpdatePanel.ContentTemplateContainer.Controls.Add(courseEnrollmentApprovalGrid);
            loadedWidgetContent.Controls.Add(courseEnrollmentApprovalGridUpdatePanel);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { courseEnrollmentApprovalGrid.BindData(); }
        }
        #endregion

        #region _CourseEnrollmentApprovalGrid_RowDataBound
        /// <summary>
        /// Data Bound event handler for the Course Enrollment Approval Widget to display
        /// approve and reject image buttons for each row.
        /// </summary>
        /// <param name="sender">Sender Row</param>
        /// <param name="e">Event Arguments</param>
        private void _CourseEnrollmentApprovalGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // retrieve user's ID from the row
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idEnrollmentRequest = Convert.ToInt32(rowView["idEnrollmentRequest"]);

                // approve button
                HyperLink approveLink = new HyperLink();
                approveLink.ID = "CourseEnrollmentApproval_" + idEnrollmentRequest.ToString();
                approveLink.ToolTip = _GlobalResources.ApproveEnrollment;
                approveLink.Attributes.Add("onclick", "CourseEnrollmentApproveRejectClick('" + idEnrollmentRequest.ToString() + "', 'approve');");

                Image approveImage = new Image();
                approveImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                approveImage.CssClass = "SmallIcon";
                approveLink.Controls.Add(approveImage);                                             

                // attach the control to the cell
                e.Row.Cells[2].Controls.Add(approveLink);

                // reject button
                HyperLink rejectLink = new HyperLink();
                rejectLink.ID = "CourseEnrollmentRejection_" + idEnrollmentRequest.ToString();
                rejectLink.ToolTip = _GlobalResources.RejectEnrollment;
                rejectLink.Attributes.Add("onclick", "CourseEnrollmentApproveRejectClick('" + idEnrollmentRequest.ToString() + "', 'reject');");

                Image rejectImage = new Image();
                rejectImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                rejectImage.CssClass = "SmallIcon";
                rejectLink.Controls.Add(rejectImage);

                // attach the control to the cell
                e.Row.Cells[3].Controls.Add(rejectLink);
            }
        }
        #endregion

        #region _BuildPendingCourseEnrollmentActionModal
        /// <summary>
        /// Builds the approval/rejection modal for pending course enrollments.
        /// </summary>        
        private void _BuildPendingCourseEnrollmentActionModal(string targetControlId, Control modalContainer)
        {
            this._ApproveRejectPendingCourseEnrollmentModal = new ModalPopup("ApproveRejectPendingCourseEnrollmentModal");
            this._ApproveRejectPendingCourseEnrollmentModal.Type = ModalPopupType.Form;
            this._ApproveRejectPendingCourseEnrollmentModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_SERIES, ImageFiles.EXT_PNG);
            this._ApproveRejectPendingCourseEnrollmentModal.HeaderIconAlt = _GlobalResources.PendingCourseEnrollments;
            this._ApproveRejectPendingCourseEnrollmentModal.HeaderText = _GlobalResources.PendingCourseEnrollments;
            this._ApproveRejectPendingCourseEnrollmentModal.TargetControlID = targetControlId;
            this._ApproveRejectPendingCourseEnrollmentModal.ReloadPageOnClose = false;

            // the submit button of this modal should only handle rejection
            this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Command += this._CourseEnrollmentReject_Command;

            // build the reject comments container
            Panel rejectionCommentsContainer = new Panel();
            rejectionCommentsContainer.ID = "ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer";

            // build the reject comments textbox
            TextBox rejectCommentsTextbox = new TextBox();
            rejectCommentsTextbox.ID = "ApproveRejectPendingCourseEnrollmentModal_RejectCommentsTextbox_Field";
            rejectCommentsTextbox.TextMode = TextBoxMode.MultiLine;
            rejectCommentsTextbox.Rows = 5;

            rejectionCommentsContainer.Controls.Add(AsentiaPage.BuildFormField("ApproveRejectPendingCourseEnrollmentModal_RejectCommentsTextbox",
                                                                        _GlobalResources.PleaseEnterTheReasonForRejectingThisEnrollment,
                                                                        rejectCommentsTextbox.ID,
                                                                        rejectCommentsTextbox,
                                                                        false,
                                                                        false,
                                                                        false));

            this._ApproveRejectPendingCourseEnrollmentModal.AddControlToBody(rejectionCommentsContainer);

            // add the load button to the modal
            Button approveRejectPendingCourseEnrollmentModalHiddenLoadButton = new Button();
            approveRejectPendingCourseEnrollmentModalHiddenLoadButton.ID = "ApproveRejectPendingCourseEnrollmentModalHiddenLoadButton";
            approveRejectPendingCourseEnrollmentModalHiddenLoadButton.Command += this._CourseEnrollmentApproveReject_Click;
            approveRejectPendingCourseEnrollmentModalHiddenLoadButton.Style.Add("display", "none");

            this._ApproveRejectPendingCourseEnrollmentModal.AddControlToBody(approveRejectPendingCourseEnrollmentModalHiddenLoadButton);

            // attach modal to container
            modalContainer.Controls.Add(this._ApproveRejectPendingCourseEnrollmentModal);
        }
        #endregion

        #region _CourseEnrollmentApproveReject_Click
        /// <summary>
        /// Click event handler for approving or rejecting a course enrollment request.
        /// </summary>
        /// <param name="sender">Sender Button</param>
        /// <param name="e">Event Arguments</param>

        private void _CourseEnrollmentApproveReject_Click(Object sender, CommandEventArgs e)
        {
            string[] courseEnrollmentData = this._ApproveRejectPendingCourseEnrollmentData.Value.Split('|');
            int idEnrollmentRequest = Convert.ToInt32(courseEnrollmentData[0]);
            string action = courseEnrollmentData[1];

            if (action == "approve")
            {
                // register startup script to change the header
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ChangePendingCourseEnrollmentWidgetHeader", "$(\"#ApproveRejectPendingCourseEnrollmentModalModalPopupHeaderText\").html(ApproveCourseEnrollmentText);", true);

                // hide the submit and close buttons, and the rejection container
                this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;

                // do the approval
                AsentiaDatabase databaseObject = new AsentiaDatabase();

                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idEnrollmentRequest", idEnrollmentRequest, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@isEnrollmentApproved", 1, SqlDbType.Bit, 1, ParameterDirection.Input); // THIS IS AN APPROVAL - set approval flag to 1
                databaseObject.AddParameter("@idApprover", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                try
                {
                    databaseObject.ExecuteNonQuery("[EnrollmentRequest.ApproveReject]", true);

                    DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                    // once the request is approved, enroll the user in the course
                    EnrollmentRequest enrollmentRequestObject = new EnrollmentRequest(idEnrollmentRequest);
                    Enrollment enrollmentObject = new Enrollment();
                    enrollmentObject.EnrollCourse(enrollmentRequestObject.IdCourse, enrollmentRequestObject.IdUser, 0);

                    // show the feedback
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(_GlobalResources.TheCourseEnrollmentHasBeenApprovedSuccessfully, false);

                    // refresh the grid
                    Grid courseEnrollmentApprovalGrid = (Grid)this.FindControl("CourseEnrollmentApprovalWidgetCourseEnrollmentApprovalGrid");
                    UpdatePanel courseEnrollmentGridUpdatePanel = (UpdatePanel)this.FindControl("CourseEnrollmentApprovalWidgetCourseEnrollmentApprovalGridUpdatePanel");
                    if (courseEnrollmentApprovalGrid != null && courseEnrollmentGridUpdatePanel != null)
                    {
                        courseEnrollmentApprovalGrid.BindData();
                        courseEnrollmentGridUpdatePanel.Update();
                    }

                    // administrative tasks grid
                    Grid administrativeTasksGrid = (Grid)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGrid");
                    UpdatePanel administrativeTasksGridUpdatePanel = (UpdatePanel)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGridUpdatePanel");
                    if (administrativeTasksGrid != null && administrativeTasksGridUpdatePanel != null)
                    {
                        administrativeTasksGrid.BindData();
                        administrativeTasksGridUpdatePanel.Update();
                    }
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(dnfEx.Message, true);
                    this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                }
                catch (DatabaseFieldNotUniqueException fnuEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(fnuEx.Message, true);
                    this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                }
                catch (DatabaseCallerPermissionException cpeEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(cpeEx.Message, true);
                    this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                }
                catch (DatabaseException dEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(dEx.Message, true);
                    this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                }
                catch (AsentiaException ex)
                {
                    // display the failure message
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(ex.Message, true);
                    this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                }
                finally
                {
                    databaseObject.Dispose();
                }
            }
            else // reject
            {
                // register startup script to change the header
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ChangePendingCourseEnrollmentWidgetHeader", "$(\"#ApproveRejectPendingCourseEnrollmentModalModalPopupHeaderText\").html(RejectCourseEnrollmentText);", true);

                // show the submit and close buttons, and the rejection container
                this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = true;
                this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = true;
                this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.CssClass = this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.CssClass.Replace("DisabledButton", "");
                this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = true;
                this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = true;
            }
        }
        #endregion

        #region _CourseEnrollmentReject_Command
        /// <summary>
        /// Click event handler for rejecting the course enrollment and submitting rejection comments.
        /// </summary>
        /// <param name="sender">Sender Button</param>
        /// <param name="e">Command Arguments</param>

        private void _CourseEnrollmentReject_Command(Object sender, CommandEventArgs e)
        {
            string[] courseEnrollmentData = this._ApproveRejectPendingCourseEnrollmentData.Value.Split('|');
            TextBox rejectionComments = (TextBox)this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectCommentsTextbox_Field");
            int idEnrollmentRequest = Convert.ToInt32(courseEnrollmentData[0]);
            string action = courseEnrollmentData[1];

            if (action == "reject")
            {
                AsentiaDatabase databaseObject = new AsentiaDatabase();

                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idEnrollmentRequest", idEnrollmentRequest, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@isEnrollmentApproved", 0, SqlDbType.Bit, 1, ParameterDirection.Input); // THIS IS A REJECTION - set approval flag to 0
                databaseObject.AddParameter("@idApprover", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@rejectionComments", rejectionComments.Text, SqlDbType.NVarChar, -1, ParameterDirection.Input);

                try
                {
                    databaseObject.ExecuteNonQuery("[EnrollmentRequest.ApproveReject]", true);

                    DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                    // show the feedback
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(_GlobalResources.TheCourseEnrollmentHasBeenRejectedSuccessfully, false);
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.CssClass += " DisabledButton";

                    // refresh the grid
                    Grid courseEnrollmentApprovalGrid = (Grid)this.FindControl("CourseEnrollmentApprovalWidgetCourseEnrollmentApprovalGrid");
                    UpdatePanel courseEnrollmentGridUpdatePanel = (UpdatePanel)this.FindControl("CourseEnrollmentApprovalWidgetCourseEnrollmentApprovalGridUpdatePanel");
                    if (courseEnrollmentApprovalGrid != null && courseEnrollmentGridUpdatePanel != null)
                    {
                        courseEnrollmentApprovalGrid.BindData();
                        courseEnrollmentGridUpdatePanel.Update();
                    }

                    // administrative tasks grid
                    Grid administrativeTasksGrid = (Grid)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGrid");
                    UpdatePanel administrativeTasksGridUpdatePanel = (UpdatePanel)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGridUpdatePanel");
                    administrativeTasksGrid.BindData();
                    administrativeTasksGridUpdatePanel.Update();
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(dnfEx.Message, true);
                    this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                }
                catch (DatabaseFieldNotUniqueException fnuEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(fnuEx.Message, true);
                    this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                }
                catch (DatabaseCallerPermissionException cpeEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(cpeEx.Message, true);
                    this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                }
                catch (DatabaseException dEx)
                {
                    // display the failure message
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(dEx.Message, true);
                    this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                }
                catch (AsentiaException ex)
                {
                    // display the failure message
                    this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(ex.Message, true);
                    this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                    this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
                }
                finally
                {
                    databaseObject.Dispose();
                }
            }
            else // we should never see this, if we do, it's a problem
            {
                // display the failure message
                this._ApproveRejectPendingCourseEnrollmentModal.DisplayFeedback(_GlobalResources.TheCourseEnrollmentRecordWasNotMarkedForRejection, true);
                this._ApproveRejectPendingCourseEnrollmentModal.FindControl("ApproveRejectPendingCourseEnrollmentModal_RejectionCommentsContainer").Visible = false;
                this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Visible = false;
                this._ApproveRejectPendingCourseEnrollmentModal.SubmitButton.Enabled = false;
                this._ApproveRejectPendingCourseEnrollmentModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildMyCertificationsWidget
        /// <summary>
        /// Template method to serve as the basis for wideget builder methods.
        /// </summary>
        /// <param name="widgetId">widget id</param>
        private void _BuildMyCertificationsWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // WIDGET SPECIFIC CODE GOES HERE - REPLACE THIS COMMENT

            Grid myCertificationsGrid = new Grid("Account", true);

            myCertificationsGrid.ID = widgetId + "MyCertificationsGrid";
            myCertificationsGrid.StoredProcedure = Certification.GridProcedureForUserDashboard;
            myCertificationsGrid.DefaultRecordsPerPage = 10;
            myCertificationsGrid.ShowTimeInDateStrings = false;
            myCertificationsGrid.EmptyDataText = _GlobalResources.NoCertificationsFound;

            myCertificationsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            myCertificationsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            myCertificationsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            myCertificationsGrid.IdentifierField = "idCertificationToUserLink";
            myCertificationsGrid.DefaultSortColumn = "title";            

            // data key names
            myCertificationsGrid.DataKeyNames = new string[] { "idCertificationToUserLink", "idCertification", "title" };

            // columns
            GridColumn title = new GridColumn(_GlobalResources.Certification, "title", "title");
            
            GridColumn status = new GridColumn(_GlobalResources.Status, "status", "status");
            status.AddProperty(new GridColumnProperty("0", _GlobalResources.IncompleteInProgress));
            status.AddProperty(new GridColumnProperty("1", _GlobalResources.Current));
            status.AddProperty(new GridColumnProperty("2", _GlobalResources.Expired));

            GridColumn expires = new GridColumn(_GlobalResources.Expires, "currentExpirationDate", "currentExpirationDate");

            GridColumn view = new GridColumn(_GlobalResources.View, "view", true);
            view.AddProperty(new GridColumnProperty("True", "<a href=\"/dashboard/Certification.aspx?cid=##idCertification##&culid=##idCertificationToUserLink##\">"
                                                            + "<img class=\"SmallIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"
                                                            + "</a>"));
            view.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                            + ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                                     ImageFiles.EXT_PNG)
                                                            + "\" alt=\"" + _GlobalResources.View + "\" />"));

            // add columns to data grid
            myCertificationsGrid.AddColumn(title);
            myCertificationsGrid.AddColumn(status);
            myCertificationsGrid.AddColumn(expires);
            myCertificationsGrid.AddColumn(view);            

            loadedWidgetContent.Controls.Add(myCertificationsGrid);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { myCertificationsGrid.BindData(); }
        }
        #endregion

        #region _BuildCertificationTaskProctoringWidget
        /// <summary>
        /// BUILDS CERTIFICATION TASK PROCTOR WIDGET
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildCertificationTaskProctoringWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            // we need to create an update panel specifically for the grid so that when we need to update the grid
            // after a task is proctored, we are only updating the grid and not the entire widget
            UpdatePanel taskProctoringGridUpdatePanel = new UpdatePanel();
            taskProctoringGridUpdatePanel.ID = widgetId + "_CertificationTaskProctoringGridUpdatePanel";
            taskProctoringGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            // build the grid
            Grid taskProctoringGrid = new Grid("Account", true);

            taskProctoringGrid.ID = widgetId + "_CertificationTaskProctoringGrid";
            taskProctoringGrid.AddCheckboxColumn = false;
            taskProctoringGrid.ShowSearchBox = false;
            taskProctoringGrid.ShowRecordsPerPageSelectbox = false;
            taskProctoringGrid.ShowTimeInDateStrings = false;
            taskProctoringGrid.DefaultRecordsPerPage = 5;

            taskProctoringGrid.StoredProcedure = "[Widget.CertificationTaskProctor]";
            taskProctoringGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            taskProctoringGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            taskProctoringGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            taskProctoringGrid.IdentifierField = "idData-CertificationModuleRequirement";
            taskProctoringGrid.DefaultSortColumn = "dtSubmitted";

            // data key names
            taskProctoringGrid.DataKeyNames = new string[] { "idData-CertificationModuleRequirement" };

            // columns
            GridColumn learner = new GridColumn(_GlobalResources.Learner, "userDisplayName");
            GridColumn certificationRequirement = new GridColumn(_GlobalResources.CertificationRequirement, null); // contents of this column will be populated by the method that handles RowDataBound
            GridColumn taskView = new GridColumn(_GlobalResources.View, null, true);

            // add columns to data grid 
            taskProctoringGrid.AddColumn(learner);
            taskProctoringGrid.AddColumn(certificationRequirement);
            taskProctoringGrid.AddColumn(taskView);
            taskProctoringGrid.RowDataBound += new GridViewRowEventHandler(this._CertificationTaskProctoringGrid_RowDataBound);

            // attach grid to update panel and update panel to loaded widget content panel
            taskProctoringGridUpdatePanel.ContentTemplateContainer.Controls.Add(taskProctoringGrid);
            loadedWidgetContent.Controls.Add(taskProctoringGridUpdatePanel);

            // build modal popup for grading the tasks
            Button hiddenButtonForTaskModal = new Button();
            hiddenButtonForTaskModal.ID = widgetId + "_HiddenButtonForCertificationTaskModal";
            hiddenButtonForTaskModal.Style.Add("display", "none");
            loadedWidgetContent.Controls.Add(hiddenButtonForTaskModal);

            this._BuildCertificationTaskModal(loadedWidgetContent, hiddenButtonForTaskModal.ID, widgetId);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { taskProctoringGrid.BindData(); }
        }
        #endregion

        #region _CertificationTaskProctoringGrid_RowDataBound
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _CertificationTaskProctoringGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // get data values for row
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idUser = Convert.ToInt32(rowView["idUser"]); // will never be null
                int idCertificationToUserLink = Convert.ToInt32(rowView["idCertificationToUserLink"]); // will never be null
                int idDataCertificationModuleRequirement = Convert.ToInt32(rowView["idData-CertificationModuleRequirement"]); // will never be null                                
                string uploadedTaskFilename = rowView["completionDocumentationFilePath"].ToString();
                string taskUploadedDate = rowView["dtSubmitted"].ToString();

                // CERTIFICATION/REQUIREMENT

                Panel certificationNameContainer = new Panel();
                Literal certificationName = new Literal();
                certificationName.Text = _GlobalResources.Certification + ": " + HttpContext.Current.Server.HtmlEncode(rowView["certificationName"].ToString());
                certificationNameContainer.Controls.Add(certificationName);
                e.Row.Cells[1].Controls.Add(certificationNameContainer);

                Panel requirementNameContainer = new Panel();
                Literal requirementName = new Literal();
                requirementName.Text = _GlobalResources.Requirement + ": " + HttpContext.Current.Server.HtmlEncode(rowView["requirementName"].ToString());
                requirementNameContainer.Controls.Add(requirementName);
                e.Row.Cells[1].Controls.Add(requirementNameContainer);

                // TASK VIEW BUTTON

                // button
                LinkButton launchTaskLink = new LinkButton();
                launchTaskLink.ID = "LaunchCertificationTaskLink_" + idDataCertificationModuleRequirement.ToString();
                launchTaskLink.OnClientClick = "LoadCertificationTaskModalContent(" + idUser + ", " + idCertificationToUserLink + ", " + idDataCertificationModuleRequirement + ", \"" + uploadedTaskFilename + "\", \"" + taskUploadedDate + "\"); return false;";

                Image launchTaskLinkImage = new Image();
                launchTaskLinkImage.ID = "LaunchCertificationTaskLinkImage_" + idDataCertificationModuleRequirement.ToString();
                launchTaskLinkImage.CssClass = "SmallIcon";
                launchTaskLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                launchTaskLinkImage.AlternateText = _GlobalResources.CertificationTask;

                launchTaskLink.Controls.Add(launchTaskLinkImage);

                e.Row.Cells[2].Controls.Add(launchTaskLink);
            }
        }
        #endregion

        #region _BuildCertificationTaskModal
        /// <summary>
        /// 
        /// </summary>
        private void _BuildCertificationTaskModal(Panel loadedWidgetContent, string triggerId, string widgetId)
        {
            // set modal properties
            ModalPopup taskModal = new ModalPopup(widgetId + "_CertificationTaskModal");
            taskModal.Type = ModalPopupType.Form;
            taskModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
            taskModal.HeaderIconAlt = _GlobalResources.CertificationTask;
            taskModal.HeaderText = _GlobalResources.CertificationTask;
            taskModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            taskModal.TargetControlID = triggerId;
            taskModal.SubmitButton.Command += new CommandEventHandler(this._CertificationTaskSubmit_Command);

            // build the modal form panel
            Panel taskModalFormPanel = new Panel();
            taskModalFormPanel.ID = widgetId + "_CertificationTaskModalFormPanel";
            taskModalFormPanel.Visible = false; // don't render this, we will render in the _LoadCertificationTaskModalContent method

            // note, we need to add the control for setting approval, it registers properly in the control stack

            // approve

            CheckBox approveCheckbox = new CheckBox();
            approveCheckbox.ID = "AdministrativeTasksWidget_CertificationTaskModal_Approve_Field";
            approveCheckbox.Text = _GlobalResources.SignoffOnThisTaskAsCompleted;

            taskModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("AdministrativeTasksWidget_CertificationTaskModal_Approve",
                                                                       _GlobalResources.Signoff,
                                                                       approveCheckbox.ID,
                                                                       approveCheckbox,
                                                                       false,
                                                                       false,
                                                                       false));

            // build the modal load button
            Button taskModalLoadButton = new Button();
            taskModalLoadButton.ID = widgetId + "_CertificationTaskModalLoadButton";
            taskModalLoadButton.Style.Add("display", "none");
            taskModalLoadButton.Click += this._LoadCertificationTaskModalContent;

            // build the task data hidden field
            HiddenField taskData = new HiddenField();
            taskData.ID = widgetId + "_CertificationTaskData";

            // build the modal body
            taskModal.AddControlToBody(taskModalFormPanel);
            taskModal.AddControlToBody(taskModalLoadButton);
            taskModal.AddControlToBody(taskData);

            // add modal to container
            loadedWidgetContent.Controls.Add(taskModal);
        }
        #endregion

        #region _LoadCertificationTaskModalContent
        /// <summary>
        /// Loads content for certification task modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadCertificationTaskModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            ModalPopup taskModal = (ModalPopup)this.FindControl("AdministrativeTasksWidget_CertificationTaskModal");
            HiddenField taskData = (HiddenField)taskModal.FindControl("AdministrativeTasksWidget_CertificationTaskData");
            Panel taskModalFormPanel = (Panel)taskModal.FindControl("AdministrativeTasksWidget_CertificationTaskModalFormPanel");

            try
            {
                // clear the modal feedback
                taskModal.ClearFeedback();

                // make the submit and close buttons visible
                taskModal.SubmitButton.Visible = true;
                taskModal.CloseButton.Visible = true;

                // if the sender is CertificationTaskProctoringWidget_CertificationTaskModalLoadButton, reset the approval form field
                Button senderButton = (Button)sender;

                if (senderButton.ID == "AdministrativeTasksWidget_CertificationTaskModalLoadButton")
                {
                    CheckBox approveCheckboxData = (CheckBox)taskModalFormPanel.FindControl("AdministrativeTasksWidget_CertificationTaskModal_Approve_Field");

                    approveCheckboxData.Checked = false;                    
                }

                // get the user id, certification user link id, certification requirement data id, uploaded task filename, and task uploaded date
                string[] taskDataItems = taskData.Value.Split('|');
                int idUser = Convert.ToInt32(taskDataItems[0]);
                int idCertificationToUserLink = Convert.ToInt32(taskDataItems[1]);
                int idDataCertificationModuleRequirement = Convert.ToInt32(taskDataItems[2]);                


                string uploadedTaskFilename = taskDataItems[3];

                DateTime? taskUploadedDate = null;
                if (!String.IsNullOrWhiteSpace(taskDataItems[4]))
                {
                    taskUploadedDate = Convert.ToDateTime(taskDataItems[4]);

                    // convert to user's local time
                    taskUploadedDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)taskUploadedDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                }

                // build the form
                if (!String.IsNullOrWhiteSpace(uploadedTaskFilename))
                {
                    Panel taskInstructionsPanel = new Panel();
                    taskInstructionsPanel.ID = "AdministrativeTasksWidget_CertificationTaskModal_CertificationTaskInstructionsPanel";
                    this._AsentiaPageInstance.FormatPageInformationPanel(taskInstructionsPanel, _GlobalResources.ViewTheSubmittedTaskAndSignoffOnItUsingTheFormBelow, true);
                    taskModalFormPanel.Controls.AddAt(0, taskInstructionsPanel); // we use AddAt because there are other controls already in the panel, and we want this to render before them

                    // UPLOADED TASK
                    if (!String.IsNullOrWhiteSpace(uploadedTaskFilename))
                    {
                        List<Control> uploadedTaskControls = new List<Control>();

                        // uploaded task link
                        HyperLink uploadedTaskLink = new HyperLink();
                        uploadedTaskLink.NavigateUrl = SitePathConstants.SITE_USERS_ROOT + idUser.ToString() + "/CertificationTasks/" + idDataCertificationModuleRequirement.ToString() + "/" + uploadedTaskFilename;
                        uploadedTaskLink.Target = "_blank";
                        uploadedTaskLink.Text = uploadedTaskFilename;
                        uploadedTaskControls.Add(uploadedTaskLink);

                        // uploaded on
                        Panel uploadedTaskUploadDateContainer = new Panel();
                        uploadedTaskUploadDateContainer.ID = "AdministrativeTasksWidget_CertificationTaskModal_UploadedCertificationTaskUploadDateContainer";

                        Literal uploadedTaskUploadDate = new Literal();
                        uploadedTaskUploadDate.Text = _GlobalResources.TheTaskWasSubmittedOn + " " + taskUploadedDate.ToString();
                        uploadedTaskUploadDateContainer.Controls.Add(uploadedTaskUploadDate);

                        uploadedTaskControls.Add(uploadedTaskUploadDateContainer);

                        // we use AddAt because there are other controls already in the panel, and we want this to render before them
                        taskModalFormPanel.Controls.AddAt(1, AsentiaPage.BuildMultipleInputControlFormField("AdministrativeTasksWidget_CertificationTaskModal_UploadedCertificationTask",
                                                                                                            _GlobalResources.UploadedTask,
                                                                                                            uploadedTaskControls,
                                                                                                            false,
                                                                                                            false));
                    }

                    // set the form panel so it will render
                    taskModalFormPanel.Visible = true;
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(dnfEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(fnuEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(cpeEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(dEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                taskModal.DisplayFeedback(ex.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _CertificationTaskSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the certification task modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CertificationTaskSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            ModalPopup taskModal = (ModalPopup)this.FindControl("AdministrativeTasksWidget_CertificationTaskModal");
            HiddenField taskData = (HiddenField)taskModal.FindControl("AdministrativeTasksWidget_CertificationTaskData");
            Panel taskModalFormPanel = (Panel)taskModal.FindControl("AdministrativeTasksWidget_CertificationTaskModalFormPanel");
            Grid taskProctoringGrid = (Grid)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGrid");
            UpdatePanel taskProctoringGridUpdatePanel = (UpdatePanel)this.FindControl("AdministrativeTasksWidgetAdministrativeTasksGridUpdatePanel");

            try
            {
                // clear the modal feedback
                taskModal.ClearFeedback();

                // get the certification to user link id, and the module requirement data id
                string[] taskDataItems = taskData.Value.Split('|');
                int idCertificationToUserLink = Convert.ToInt32(taskDataItems[1]);
                int idDataCertificationModuleRequirement = Convert.ToInt32(taskDataItems[2]);                

                // get the values from the approval control
                CheckBox approveCheckboxData = (CheckBox)taskModalFormPanel.FindControl("AdministrativeTasksWidget_CertificationTaskModal_Approve_Field");

                bool approve = Convert.ToBoolean(approveCheckboxData.Checked);

                // save the data
                CertificationModuleRequirement.ProctorTask(idCertificationToUserLink, idDataCertificationModuleRequirement, approve);

                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
                taskModal.DisplayFeedback(_GlobalResources.TheTaskDataHasBeenSavedSuccessfully, false);

                taskProctoringGrid.BindData();
                taskProctoringGridUpdatePanel.Update();                
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(dnfEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(fnuEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(cpeEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                taskModal.DisplayFeedback(dEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                taskModal.DisplayFeedback(ex.Message, true);
                taskModalFormPanel.Controls.Clear();
                taskModal.SubmitButton.Visible = false;
                taskModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildDocumentsWidget
        /// <summary>
        /// BUILDS DOCUMENTS WIDGET
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildDocumentsWidget(string widgetId, bool isInitialLoad)
        {
            
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            Grid documentsGrid = new Grid("Account", true);

            documentsGrid.ID = widgetId + "DocumentsGrid";
            documentsGrid.StoredProcedure = DocumentRepositoryItem.GridProcedureForUserDashboard;
            documentsGrid.DefaultRecordsPerPage = 10;
            documentsGrid.EmptyDataText = _GlobalResources.NoDocumentsFound;
            documentsGrid.ShowSearchBox = true;
            documentsGrid.AddCheckboxColumn = false;

            documentsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            documentsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            documentsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            documentsGrid.IdentifierField = "idDocumentRepositoryItem";
            documentsGrid.DefaultSortColumn = "filename";

            documentsGrid.RowDataBound += this._DocumentsGrid_RowDataBound;

            // data key names
            documentsGrid.DataKeyNames = new string[] { "idDocumentRepositoryItem", "filename", "objectType", "idObject", "objectName", "label" };

            // columns
            GridColumn filename = new GridColumn(_GlobalResources.Document, "label", "label");          

            // add columns to data grid
            documentsGrid.AddColumn(filename);

            loadedWidgetContent.Controls.Add(documentsGrid);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { documentsGrid.BindData();}

        }
        #endregion

        #region _DocumentsGrid_RowDataBound
        /// <summary>
        /// row data bound event for documents widget grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DocumentsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;

                // get data values for row
                string label = HttpContext.Current.Server.HtmlEncode(rowView["label"].ToString());
                
                string filename = rowView["filename"].ToString();
                string objectType = rowView["objectType"].ToString();
                string idObject = rowView["idObject"].ToString();
                string objectName = HttpContext.Current.Server.HtmlEncode(rowView["objectName"].ToString());

                HyperLink documentHyperLink = new HyperLink();
                documentHyperLink.NavigateUrl = @"/warehouse/" + AsentiaSessionState.SiteHostname + @"/documents/" + objectType + @"/" + idObject + @"/" + filename;
                documentHyperLink.Target = "_blank";
                // if the label is not empty, then the hyperlink text is the label, otherwise use the filename
                if (!String.IsNullOrWhiteSpace(label))
                {
                    documentHyperLink.Text = label;
                }
                else
                {
                    documentHyperLink.Text = filename;
                }

                e.Row.Cells[0].Controls.Add(documentHyperLink);

                HtmlGenericControl documentDetailsSpan = new HtmlGenericControl("div");
                documentDetailsSpan.Attributes["class"] = "MessageCommentData";
                documentDetailsSpan.InnerHtml = "(" + objectType + " - " + objectName + ")";
  

                e.Row.Cells[0].Controls.Add(documentDetailsSpan);
              
            }
        }
        #endregion

        #region _BuildILTSessionsWidget
        /// <summary>
        /// BUILDS THE LEARNER ILT SESSIONS WIDGET
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildILTSessionsWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            UpdatePanel iltSessionGridUpdatePanel = new UpdatePanel();
            iltSessionGridUpdatePanel.ID = widgetId + "ILTSessionGridUpdatePanel";
            iltSessionGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            Grid iltSessionGrid = new Grid("Account", true);

            iltSessionGrid.ID = widgetId + "ILTSessionGrid";
            iltSessionGrid.StoredProcedure = StandUpTrainingInstanceToUserLink.GridProcedure;
            iltSessionGrid.EmptyDataText = _GlobalResources.NoInstructorLedTrainingResultsWereFound;
            iltSessionGrid.ShowSearchBox = false;
            iltSessionGrid.AddCheckboxColumn = false;

            iltSessionGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            iltSessionGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            iltSessionGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            iltSessionGrid.IdentifierField = "idStandupTrainingInstanceToUserLink";
            iltSessionGrid.DefaultSortColumn = "title";

            iltSessionGrid.RowDataBound += this._ILTSessionGrid_RowDataBound;

            // data key names
            iltSessionGrid.DataKeyNames = new string[] { "idStandupTrainingInstanceToUserLink", "title" };

            // columns
            GridColumn title = new GridColumn(_GlobalResources.InstructorLedTrainingSession, "title", "title");
            GridColumn join = new GridColumn(null, null, null);
            

            // add columns to data grid
            iltSessionGrid.AddColumn(title);
            iltSessionGrid.AddColumn(join);

            iltSessionGridUpdatePanel.ContentTemplateContainer.Controls.Add(iltSessionGrid);

            loadedWidgetContent.Controls.Add(iltSessionGridUpdatePanel);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { iltSessionGrid.BindData(); }

            
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "UpdateHeaderText", "$('#ILTSessionsModalModalPopupHeaderText').text($('#ILTSessionsTitle').val());", true);
        }
        #endregion

        #region _ILTSessionGrid_RowDataBound
        /// <summary>
        /// Row data bound event handler for the ILT Session Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ILTSessionGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // get data values for row
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idStandupTrainingInstanceToUserLink = Convert.ToInt32(rowView["idStandupTrainingInstanceToUserLink"]);
                int idStandupTrainingInstance = Convert.ToInt32(rowView["idStandupTrainingInstance"]);
                string title = HttpContext.Current.Server.HtmlEncode(rowView["title"].ToString());
                StandupTrainingInstance.MeetingType type = (StandupTrainingInstance.MeetingType)rowView["type"];
                string urlRegistration = rowView["urlRegistration"].ToString();
                string urlAttend = rowView["urlAttend"].ToString();
                string genericJoinUrl = rowView["genericJoinUrl"].ToString();
                string specificJoinUrl = rowView["specificJoinUrl"].ToString();
                string meetingPassword = rowView["meetingPassword"].ToString();
                bool isCallerEnrolled = Convert.ToBoolean(rowView["isCallerEnrolled"]);
                
                LinkButton titleLinkButton = new LinkButton();
                titleLinkButton.ID = "ILTSessionsWidget_TitleLinkButton_" + idStandupTrainingInstanceToUserLink.ToString();
                titleLinkButton.Text = title;
                titleLinkButton.OnClientClick = "LoadILTSessionDetailsModal(" + idStandupTrainingInstance + ", " + idStandupTrainingInstanceToUserLink + "); return false;";
                e.Row.Cells[0].Controls.Add(titleLinkButton);

                // determine if standup training instance has started for an online course
                if (type != StandupTrainingInstance.MeetingType.Classroom)
                {
                    bool standupTrainingInstanceHasStarted = false;
                    ArrayList meetingTimes = new StandupTrainingInstance(idStandupTrainingInstance).MeetingTimes;

                    foreach (StandupTrainingInstance.MeetingTimeProperty meetingTime in meetingTimes)
                    {

                        // determine if the session has started
                        if (meetingTime.DtStart.AddMinutes(-15) <= AsentiaSessionState.UtcNow)
                        { standupTrainingInstanceHasStarted = true; }
                    }
                        
                    // create join link if it's an online course
                    if (type == StandupTrainingInstance.MeetingType.Online && !String.IsNullOrWhiteSpace(urlAttend) && !standupTrainingInstanceHasStarted) // online
                    {        

                        HyperLink urlAttendLink = new HyperLink();
                        urlAttendLink.NavigateUrl = urlAttend;
                        urlAttendLink.Target = "_blank";

                        Image urlAttendImage = new Image();
                        urlAttendImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SESSION, ImageFiles.EXT_PNG);
                        urlAttendImage.ToolTip = _GlobalResources.JoinMeeting;
                        urlAttendImage.CssClass = "SmallIcon";
                        urlAttendLink.Controls.Add(urlAttendImage);

                        e.Row.Cells[1].Controls.Add(urlAttendLink);
                        
                    }
                    else if (type == StandupTrainingInstance.MeetingType.GoToMeeting && !String.IsNullOrWhiteSpace(genericJoinUrl) && !standupTrainingInstanceHasStarted) // GTM
                    {
                        HyperLink gtmLink = new HyperLink();
                        gtmLink.NavigateUrl = genericJoinUrl;
                        gtmLink.Target = "_blank";

                        Image gtmImage = new Image();
                        gtmImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOMEETING, ImageFiles.EXT_PNG);
                        gtmImage.ToolTip = _GlobalResources.JoinMeeting;
                        gtmImage.CssClass = "SmallIcon";
                        gtmLink.Controls.Add(gtmImage);

                        e.Row.Cells[1].Controls.Add(gtmLink);

                    }
                    else if (type == StandupTrainingInstance.MeetingType.GoToWebinar && !String.IsNullOrWhiteSpace(specificJoinUrl) && !standupTrainingInstanceHasStarted) // GTW
                    {

                        HyperLink gtwLink = new HyperLink();
                        gtwLink.NavigateUrl = specificJoinUrl;
                        gtwLink.Target = "_blank";
                        gtwLink.Text = _GlobalResources.JoinWebinar;

                        Image gtwImage = new Image();
                        gtwImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOWEBINAR, ImageFiles.EXT_PNG);
                        gtwImage.CssClass = "SmallIcon";
                        gtwImage.ToolTip = _GlobalResources.JoinWebinar;
                        gtwLink.Controls.Add(gtwImage);

                        e.Row.Cells[1].Controls.Add(gtwLink);


                    }
                    else if (type == StandupTrainingInstance.MeetingType.GoToTraining && !String.IsNullOrWhiteSpace(specificJoinUrl) && !standupTrainingInstanceHasStarted) // GTT
                    {
                        HyperLink gttLink = new HyperLink();
                        gttLink.NavigateUrl = specificJoinUrl;
                        gttLink.Target = "_blank";

                        Image gttImage = new Image();
                        gttImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOTRAINING, ImageFiles.EXT_PNG);
        
                        gttImage.CssClass = "SmallIcon";
                        gttImage.ToolTip = _GlobalResources.JoinTraining;
                        gttLink.Controls.Add(gttImage);

                        e.Row.Cells[1].Controls.Add(gttLink);

                    }
                    else if (type == StandupTrainingInstance.MeetingType.WebEx && isCallerEnrolled && (!String.IsNullOrWhiteSpace(specificJoinUrl) || !String.IsNullOrWhiteSpace(genericJoinUrl)) && !standupTrainingInstanceHasStarted) // WebEx
                    {
                        HyperLink webexLink = new HyperLink();

                        if (!String.IsNullOrWhiteSpace(specificJoinUrl))
                        { webexLink.NavigateUrl = specificJoinUrl; }
                        else
                        { webexLink.NavigateUrl = genericJoinUrl; }

                        webexLink.Target = "_blank";

                        Image wbxImage = new Image();
                        wbxImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_WEBEX, ImageFiles.EXT_PNG);
                        //wbxImage.Style.Add("margin-right", "6px");
                        wbxImage.CssClass = "SmallIcon";
                        

                        if (!String.IsNullOrWhiteSpace(meetingPassword))
                        {
                            wbxImage.ToolTip = _GlobalResources.JoinMeeting + " (" + _GlobalResources.Password + ": " + meetingPassword + ")";

                        }
                        else
                        {
                            wbxImage.ToolTip = _GlobalResources.JoinMeeting;
                        }

                        webexLink.Controls.Add(wbxImage);

                        e.Row.Cells[1].Controls.Add(webexLink);
                        

                    }


                }
            }
        }
        #endregion

        #region _BuildILTSessionsModal
        /// <summary>
        /// Builds the ILT Sessions Modal
        /// </summary>
        /// <param name="targetControlId">ID of the target control</param>
        /// <param name="modalContainer">container to hold the modal</param>
        private void _BuildILTSessionsModal(string targetControlId, Control modalContainer)
        {
            this._ILTSessionsModal = new ModalPopup("ILTSessionsModal");
            this._ILTSessionsModal.Type = ModalPopupType.Form;
            this._ILTSessionsModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_SESSION, ImageFiles.EXT_PNG);
            this._ILTSessionsModal.HeaderIconAlt = _GlobalResources.InstructorLedTrainingSession;
            this._ILTSessionsModal.TargetControlID = targetControlId;
            this._ILTSessionsModal.ReloadPageOnClose = false;
            this._ILTSessionsModal.ShowLoadingPlaceholder = true;
            this._ILTSessionsModal.SubmitButton.Command += this._DropILTSession_Command;
            this._ILTSessionsModal.SubmitButton.Visible = false;
            this._ILTSessionsModal.CloseButton.Visible = false;

            this._ILTSessionsModalHiddenLoadButton = new Button();
            this._ILTSessionsModalHiddenLoadButton.ID = "ILTSessionsModalHiddenLoadButton";
            this._ILTSessionsModalHiddenLoadButton.Click += this._ILTSessionsModalHiddenLoadButton_Click;
            this._ILTSessionsModalHiddenLoadButton.Style.Add("display", "none");
            this._ILTSessionsModalHiddenLoadButton.ClientIDMode = ClientIDMode.Static;

            this._ILTSessionsModalDetailsPanel = new Panel();
            this._ILTSessionsModalDetailsPanel.ID = "ILTSessionsUpdatePanel";

            this._ILTSessionsModal.AddControlToBody(this._ILTSessionsModalHiddenLoadButton);
            this._ILTSessionsModal.AddControlToBody(this._ILTSessionsModalDetailsPanel);

            modalContainer.Controls.Add(this._ILTSessionsModal);
            
        }

        #endregion

        #region _ILTSessionsModalHiddenLoadButton_Click
        /// <summary>
        /// Method to load the ILT Session details in the modal popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ILTSessionsModalHiddenLoadButton_Click(object sender, EventArgs e)
        {
            
            // clear controls from detail modal popup
            this._ILTSessionsModalDetailsPanel.Controls.Clear();
            this._ILTSessionsModal.ClearFeedback();
            
            StandupTrainingInstance iltSession = new StandupTrainingInstance(Convert.ToInt32(this._ILTSessionsData.Value));
            
            string title = iltSession.Title;
            StandupTrainingInstance.MeetingType type = iltSession.Type;
            string urlRegistration = iltSession.UrlRegistration;
            string urlAttend = iltSession.UrlAttend;
            string city = iltSession.City;
            string province = iltSession.Province;
            string description = iltSession.Description;
            string locationDescription = iltSession.LocationDescription;
            string genericJoinUrl = iltSession.GenericJoinUrl;
            string meetingPassword = iltSession.MeetingPassword;
            bool isRestrictedDrop = new StandupTraining(iltSession.IdStandupTraining).IsRestrictedDrop;

            StandUpTrainingInstanceToUserLink sessionToUserLink = new StandUpTrainingInstanceToUserLink(iltSession.Id, AsentiaSessionState.IdSiteUser);
            string specificJoinUrl = sessionToUserLink.JoinUrl;
            bool isCallerEnrolled = !sessionToUserLink.IsWaitingList;
            bool isCallerWaitlisted = sessionToUserLink.IsWaitingList;

            DataTable dtInstructors = iltSession.GetInstructors(null);
            ArrayList meetingTimes = iltSession.MeetingTimes;

            bool standupTrainingInstanceHasStarted = false;

            #region Meeting Time(s)
            List<Control> meetingTimesContainer = new List<Control>();

            foreach (StandupTrainingInstance.MeetingTimeProperty meetingTime in meetingTimes)
            {
                Panel meetingTimePanel = new Panel();

                DateTime dtStart = meetingTime.DtStart;
                DateTime dtEnd = meetingTime.DtEnd;

                // determine if the session has started
                if (dtStart.AddMinutes(-15) <= AsentiaSessionState.UtcNow)
                { standupTrainingInstanceHasStarted = true; }

                // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                if (type == StandupTrainingInstance.MeetingType.Classroom)
                {
                    int idTimezone = meetingTime.IdTimezone;
                    string tzDotNetName = new Timezone(idTimezone).dotNetName;

                    dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                    dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));

                }
                else
                {
                    dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                    dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                }

                // meeting time values
                Literal meetingTimesText = new Literal();
                meetingTimesText.Text = Convert.ToString(dtStart) + " - " + Convert.ToString(dtEnd);
                meetingTimePanel.Controls.Add(meetingTimesText);

                // Combine location description with the city/state
                string fullLocationWithCity;
                // if there is no location description, only use city/province, and vice versa
                if (locationDescription == null || locationDescription == "")
                {
                    fullLocationWithCity = city + ", " + province;
                }
                else if (city == null || city == "" || province == null || province == "")
                {
                    fullLocationWithCity = locationDescription;
                }
                else
                {
                    fullLocationWithCity = locationDescription + " (" + city + ", " + province + ")";
                }

                
                LinkButton standupTrainingInstanceMeetingTimeICalendarLink = new LinkButton();
                standupTrainingInstanceMeetingTimeICalendarLink.CssClass = "ICalendarLink";
                standupTrainingInstanceMeetingTimeICalendarLink.OnClientClick = "CreateILTCalendarObject('" + iltSession.Id + "', '" + dtStart.ToString("o") + "', '" + dtEnd.ToString("o") + "'); return false;";

                Image icalendarImage = new Image();
                icalendarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR, ImageFiles.EXT_PNG);
                icalendarImage.CssClass = "XSmallIcon";

                standupTrainingInstanceMeetingTimeICalendarLink.Controls.Add(icalendarImage);
                meetingTimePanel.Controls.Add(standupTrainingInstanceMeetingTimeICalendarLink);

                meetingTimesContainer.Add(meetingTimePanel);
            }

            #endregion

            #region Description
            if (!String.IsNullOrWhiteSpace(description))
            {
                Literal descriptionText = new Literal();
                descriptionText.Text = description;

                // attach the controls
                this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildFormField("Description",
                                                                                 _GlobalResources.Description,
                                                                                 descriptionText.ID,
                                                                                 descriptionText,
                                                                                 false,
                                                                                 false,
                                                                                 false));
            }
            #endregion
            
            #region Location
            if (type == StandupTrainingInstance.MeetingType.Online) // online
            {
                List<Control> onlineSessionControls = new List<Control>();

                // location label
                Panel locationContainer = new Panel();
                locationContainer.Style.Add("margin-bottom", "5px");

                Literal locationText = new Literal();
                locationText.Text = _GlobalResources.Online;
                locationContainer.Controls.Add(locationText);

                onlineSessionControls.Add(locationContainer);

                // show registration/attendance URLs
                        
                // url register -- show until 15 minutes prior to session starting
                if (!String.IsNullOrWhiteSpace(urlRegistration) && !standupTrainingInstanceHasStarted)
                {
                    Panel urlRegisterContainer = new Panel();

                    Label urlRegisterLabel = new Label();
                    urlRegisterLabel.Text = _GlobalResources.Register + ": ";
                    urlRegisterContainer.Controls.Add(urlRegisterLabel);

                    HyperLink urlRegisterLink = new HyperLink();
                    urlRegisterLink.NavigateUrl = urlRegistration;
                    urlRegisterLink.Target = "_blank";
                    urlRegisterLink.Text = urlRegistration;
                    urlRegisterContainer.Controls.Add(urlRegisterLink);

                    onlineSessionControls.Add(urlRegisterContainer);
                }

                // url attend - only show 15 minutes prior to session starting                        
                if (!String.IsNullOrWhiteSpace(urlAttend) && !standupTrainingInstanceHasStarted)
                {
                    Panel urlAttendContainer = new Panel();

                    Label urlAttendLabel = new Label();
                    urlAttendLabel.Text = _GlobalResources.Attend + ": ";
                    urlAttendContainer.Controls.Add(urlAttendLabel);

                    HyperLink urlAttendLink = new HyperLink();
                    urlAttendLink.NavigateUrl = urlAttend;
                    urlAttendLink.Target = "_blank";
                    urlAttendLink.Text = urlAttend;
                    urlAttendContainer.Controls.Add(urlAttendLink);

                    onlineSessionControls.Add(urlAttendContainer);
                }

                // attach the controls
                this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                    _GlobalResources.Location,
                                                                                                    onlineSessionControls,
                                                                                                    false,
                                                                                                    false));
            }
            else if (type == StandupTrainingInstance.MeetingType.GoToMeeting) // GTM
            {
                List<Control> gtmSessionControls = new List<Control>();

                // location label
                Panel locationContainer = new Panel();
                locationContainer.Style.Add("margin-bottom", "5px");

                Image gtmImage = new Image();
                gtmImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOMEETING, ImageFiles.EXT_PNG);
                gtmImage.Style.Add("margin-right", "6px");
                gtmImage.CssClass = "SmallIcon";
                locationContainer.Controls.Add(gtmImage);

                Literal locationText = new Literal();
                locationText.Text = _GlobalResources.GoToMeeting;
                locationContainer.Controls.Add(locationText);

                gtmSessionControls.Add(locationContainer);

                // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                        
                // join link - only show 15 minutes prior to session starting                        
                if (!String.IsNullOrWhiteSpace(genericJoinUrl) && !standupTrainingInstanceHasStarted)
                {
                    Panel joinLinkContainer = new Panel();

                    HyperLink joinLink = new HyperLink();
                    joinLink.NavigateUrl = genericJoinUrl;
                    joinLink.Target = "_blank";
                    joinLink.Text = _GlobalResources.JoinMeeting;
                    joinLinkContainer.Controls.Add(joinLink);

                    gtmSessionControls.Add(joinLinkContainer);
                }
                        

                // attach the controls
                this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                        _GlobalResources.Location,
                                                                                                        gtmSessionControls,
                                                                                                        false,
                                                                                                        false));
            }
            else if (type == StandupTrainingInstance.MeetingType.GoToWebinar) // GTW
            {
                List<Control> gtwSessionControls = new List<Control>();

                // location label
                Panel locationContainer = new Panel();
                locationContainer.Style.Add("margin-bottom", "5px");

                Image gtwImage = new Image();
                gtwImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOWEBINAR, ImageFiles.EXT_PNG);
                gtwImage.Style.Add("margin-right", "6px");
                gtwImage.CssClass = "SmallIcon";
                locationContainer.Controls.Add(gtwImage);

                Literal locationText = new Literal();
                locationText.Text = _GlobalResources.GoToWebinar;
                locationContainer.Controls.Add(locationText);

                gtwSessionControls.Add(locationContainer);

                // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                        
                // join link - only show 15 minutes prior to session starting                        
                if (!String.IsNullOrWhiteSpace(specificJoinUrl) && !standupTrainingInstanceHasStarted)
                {
                    Panel joinLinkContainer = new Panel();

                    HyperLink joinLink = new HyperLink();
                    joinLink.NavigateUrl = specificJoinUrl;
                    joinLink.Target = "_blank";
                    joinLink.Text = _GlobalResources.JoinWebinar;
                    joinLinkContainer.Controls.Add(joinLink);

                    gtwSessionControls.Add(joinLinkContainer);
                }
                        

                // attach the controls
                this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                     _GlobalResources.Location,
                                                                                                     gtwSessionControls,
                                                                                                     false,
                                                                                                     false));
            }
            else if (type == StandupTrainingInstance.MeetingType.GoToTraining) // GTT
            {
                List<Control> gttSessionControls = new List<Control>();

                // location label
                Panel locationContainer = new Panel();
                locationContainer.Style.Add("margin-bottom", "5px");

                Image gttImage = new Image();
                gttImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOTRAINING, ImageFiles.EXT_PNG);
                gttImage.Style.Add("margin-right", "6px");
                gttImage.CssClass = "SmallIcon";
                locationContainer.Controls.Add(gttImage);

                Literal locationText = new Literal();
                locationText.Text = _GlobalResources.GoToTraining;
                locationContainer.Controls.Add(locationText);

                gttSessionControls.Add(locationContainer);

                // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                        
                // join link - only show 15 minutes prior to session starting                        
                if (!String.IsNullOrWhiteSpace(specificJoinUrl) && !standupTrainingInstanceHasStarted)
                {
                    Panel joinLinkContainer = new Panel();

                    HyperLink joinLink = new HyperLink();
                    joinLink.NavigateUrl = specificJoinUrl;
                    joinLink.Target = "_blank";
                    joinLink.Text = _GlobalResources.JoinTraining;
                    joinLinkContainer.Controls.Add(joinLink);

                    gttSessionControls.Add(joinLinkContainer);
                }
                        

                // attach the controls
                this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                     _GlobalResources.Location,
                                                                                                     gttSessionControls,
                                                                                                     false,
                                                                                                     false));
            }
            else if (type == StandupTrainingInstance.MeetingType.WebEx) // WebEx
            {
                List<Control> wbxSessionControls = new List<Control>();

                // location label
                Panel locationContainer = new Panel();
                locationContainer.Style.Add("margin-bottom", "5px");

                Image wbxImage = new Image();
                wbxImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_WEBEX, ImageFiles.EXT_PNG);
                wbxImage.Style.Add("margin-right", "6px");
                wbxImage.CssClass = "SmallIcon";
                locationContainer.Controls.Add(wbxImage);

                Literal locationText = new Literal();
                locationText.Text = _GlobalResources.WebEx;
                locationContainer.Controls.Add(locationText);

                wbxSessionControls.Add(locationContainer);

                // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                if (isCallerEnrolled)
                {
                    // join link - only show 15 minutes prior to session starting                        
                    if ((!String.IsNullOrWhiteSpace(specificJoinUrl) || !String.IsNullOrWhiteSpace(genericJoinUrl)) && !standupTrainingInstanceHasStarted)
                    {
                        Panel joinLinkContainer = new Panel();

                        HyperLink joinLink = new HyperLink();

                        if (!String.IsNullOrWhiteSpace(specificJoinUrl))
                        { joinLink.NavigateUrl = specificJoinUrl; }
                        else
                        { joinLink.NavigateUrl = genericJoinUrl; }

                        joinLink.Target = "_blank";
                        joinLink.Text = _GlobalResources.JoinMeeting;
                        joinLinkContainer.Controls.Add(joinLink);

                        if (!String.IsNullOrWhiteSpace(meetingPassword))
                        {
                            Literal meetingPasswordLit = new Literal();
                            meetingPasswordLit.Text = " (" + _GlobalResources.Password + ": " + meetingPassword + ")";
                            joinLinkContainer.Controls.Add(meetingPasswordLit);
                        }

                        wbxSessionControls.Add(joinLinkContainer);
                    }
                }

                // attach the controls
                this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                     _GlobalResources.Location,
                                                                                                     wbxSessionControls,
                                                                                                     false,
                                                                                                     false));
            }
            else // classroom
            {
                Literal locationText = new Literal();
                locationText.Text = city + ", " + province;

                // attach the controls
                this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildFormField("Location",
                                                                                _GlobalResources.Location,
                                                                                locationText.ID,
                                                                                locationText,
                                                                                false,
                                                                                false,
                                                                                false));
            }
            #endregion
            
            #region Location Description
            if (!String.IsNullOrWhiteSpace(locationDescription) && isCallerEnrolled) // show if caller is enrolled
            {
                Literal locationDescriptionText = new Literal();
                locationDescriptionText.Text = locationDescription;

                // attach the controls
                this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildFormField("LocationDescription",
                                                                                _GlobalResources.LocationDescription,
                                                                                locationDescriptionText.ID,
                                                                                locationDescriptionText,
                                                                                false,
                                                                                false,
                                                                                false));
            }
            #endregion
                    
            #region Instructors
            List<Control> instructorNames = new List<Control>();
            foreach(DataRow row in dtInstructors.Rows){

                Literal instructorText = new Literal();
                instructorText.Text = row["displayNameWithUsername"].ToString() + ";";
                instructorNames.Add(instructorText);
                        
            }
                    
            //Add controls to instructor panel
            if(instructorNames.Count > 0){
            this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Instructors",
                                                                            _GlobalResources.Instructors,
                                                                            instructorNames,
                                                                            false,
                                                                            false,
                                                                            false));
            }
            #endregion
                    
            

            // attach the MEETING TIME controls
            this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("MeetingTimes",
                                                                                         _GlobalResources.MeetingTime_s,
                                                                                         meetingTimesContainer,
                                                                                         false,
                                                                                         false));
                    
                    
            #region Session Additional Information
            // if the caller is waitlisted, let them know
            if (isCallerWaitlisted)
            {
                List<Control> sessionAdditionalInfoControls = new List<Control>();

                // on waitlist text
                Panel waitlistTextContainer = new Panel();
                waitlistTextContainer.Style.Add("margin-bottom", "5px");

                Literal waitlistText = new Literal();
                waitlistText.Text = _GlobalResources.YouAreCurrentlyOnTheWaitlistForThisSession;
                waitlistTextContainer.Controls.Add(waitlistText);

                sessionAdditionalInfoControls.Add(waitlistTextContainer);

                // attach the controls
                this._ILTSessionsModalDetailsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("SessionAdditionalInformation",
                                                                                                            String.Empty,
                                                                                                            sessionAdditionalInfoControls,
                                                                                                            false,
                                                                                                            false));
            }
                    
            // if the caller can drop the session, show the "drop session" button
            if (!isRestrictedDrop && (isCallerEnrolled || isCallerWaitlisted) && !standupTrainingInstanceHasStarted)
            {
                this._ILTSessionsModal.SubmitButton.Visible = true;
                this._ILTSessionsModal.SubmitButton.CommandArgument = standupTrainingInstanceHasStarted.ToString() + "|" + AsentiaSessionState.IdSiteUser.ToString();
                this._ILTSessionsModal.SubmitButton.Text = _GlobalResources.DropSession;
            }

            #endregion

        }
        #endregion

        #region _DropILTSession_Command
        /// <summary>
        /// Handles the event initiated by the button click for removing learner from standup training instance.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _DropILTSession_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get the values needed to drop the session
                Button dropButton = (Button)sender;
                string[] commandArguments = dropButton.CommandArgument.Split('|');

                bool hasSessionAlreadyOccurred = Convert.ToBoolean(commandArguments[0]);

                int idUser = 0;
                if (!String.IsNullOrWhiteSpace(commandArguments[1]))
                { idUser = Convert.ToInt32(commandArguments[1]); }

                int idStandupTrainingInstance = Convert.ToInt32(this._ILTSessionsData.Value);

                // remove learner from standup training instance
                if (!hasSessionAlreadyOccurred)
                {
                    StandupTrainingInstance standupTrainingInstance = new StandupTrainingInstance(idStandupTrainingInstance);
                    standupTrainingInstance.RemoveUser(idUser);

                    // display feedback
                    this._ILTSessionsModal.DisplayFeedback(_GlobalResources.YouHaveBeenRemovedFromTheInstructorLedTrainingSessionSuccessfully, false);

                    // refresh the grid
                    Grid iltSessionsGrid = (Grid)this.FindControl("ILTSessionsWidgetILTSessionGrid");
                    UpdatePanel iltSessionsGridUpdatePanel = (UpdatePanel)this.FindControl("ILTSessionsWidgetILTSessionGridUpdatePanel");
                    iltSessionsGrid.BindData();
                    iltSessionsGridUpdatePanel.Update();
                }

                this._ILTSessionsModal.SubmitButton.Visible = false;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ILTSessionsModal.DisplayFeedback(dnfEx.Message, true);
                this._ILTSessionsModal.SubmitButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ILTSessionsModal.DisplayFeedback(fnuEx.Message, true);
                this._ILTSessionsModal.SubmitButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ILTSessionsModal.DisplayFeedback(cpeEx.Message, true);
                this._ILTSessionsModal.SubmitButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ILTSessionsModal.DisplayFeedback(dEx.Message, true);
                this._ILTSessionsModal.SubmitButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ILTSessionsModal.DisplayFeedback(ex.Message, true);
                this._ILTSessionsModal.SubmitButton.Visible = false;
            }
        }
        #endregion

        #region _DownloadILTSessionCalendarFile
        /// <summary>
        /// Download the calendar file for an ILT Session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DownloadILTSessionCalendarFile(object sender, CommandEventArgs e)
        {
            // split the ICalendarContent to remove the new line characters
            string[] splitString = this._ILTCalendarContent.Value.Split('|');

            StandupTrainingInstance iltSession = new StandupTrainingInstance(Convert.ToInt32(splitString[0]));

            string dtStart = splitString[1];
            string dtEnd = splitString[2];
            string title = iltSession.Title;

            // Combine location description with the city/state
            string fullLocationWithCity;
            // if there is no location description, only use city/province, and vice versa
            if (iltSession.LocationDescription == null || iltSession.LocationDescription == "")
            {
                fullLocationWithCity = iltSession.City + ", " + iltSession.Province;
            }
            else if (iltSession.City == null || iltSession.City == "" || iltSession.Province == null || iltSession.Province == "")
            {
                fullLocationWithCity = iltSession.LocationDescription;
            }
            else
            {
                fullLocationWithCity = iltSession.LocationDescription + " (" + iltSession.City + ", " + iltSession.Province + ")";
            }

            string description = "";
            if (iltSession.Description != null)
            {
                description = iltSession.Description;
            }

            string[] calendarSplitString = Utility.GetICalendarFormatedString(dtStart, dtEnd, title, fullLocationWithCity, description).Split(new string[] { @"\n" }, StringSplitOptions.RemoveEmptyEntries);

            // append each line separately to the calendar file
            StringBuilder sb = new StringBuilder();
            foreach (string s in calendarSplitString)
            {
                sb.AppendLine(s);
            }

            // download the calendar file
            HttpContext.Current.Response.ContentType = "text/calendar;charset-utf8";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=AsentiaMeeting.ics");
            HttpContext.Current.Response.Write(sb.ToString());
            HttpContext.Current.Response.End();
        }
        #endregion

        #region _BuildAdministrativeTasksWidget
        /// <summary>
        /// BUILDS THE ADMINISTRATIVE TOOLS WIDGET
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private void _BuildAdministrativeTasksWidget(string widgetId, bool isInitialLoad)
        {
            // get the widget's container control
            UpdatePanel widgetContent = (UpdatePanel)this.FindControl(widgetId + "Content");

            // clear the contents of the widget
            widgetContent.ContentTemplateContainer.Controls.Clear();

            widgetContent.ContentTemplateContainer.Controls.Add(this._BuildWidgetContentLoadingPlaceholder(widgetId, isInitialLoad));

            Panel loadedWidgetContent = new Panel();
            loadedWidgetContent.ID = widgetId + "LoadedContent";

            
            UpdatePanel administrativeTasksGridUpdatePanel = new UpdatePanel();
            administrativeTasksGridUpdatePanel.ID = widgetId + "AdministrativeTasksGridUpdatePanel";
            administrativeTasksGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            Grid administrativeTasksGrid = new Grid("Account", true);

            administrativeTasksGrid.ID = widgetId + "AdministrativeTasksGrid";
            administrativeTasksGrid.StoredProcedure = "[Widget.AdministrativeTasks]";
            administrativeTasksGrid.EmptyDataText = _GlobalResources.NoAdministrativeTasksWereFound;
            administrativeTasksGrid.DefaultRecordsPerPage = 10;
            administrativeTasksGrid.ShowSearchBox = false;
            administrativeTasksGrid.AddCheckboxColumn = false;

            administrativeTasksGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            administrativeTasksGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            administrativeTasksGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            administrativeTasksGrid.IdentifierField = "idAdministrativeTask";
            administrativeTasksGrid.DefaultSortColumn = "type";

            administrativeTasksGrid.RowDataBound += this._AdministrativeTasksGrid_RowDataBound;

            // data key names
            administrativeTasksGrid.DataKeyNames = new string[] { "idAdministrativeTask", "task", "type" };

            // columns
            GridColumn task = new GridColumn(_GlobalResources.Task, "task", "task");
            GridColumn type = new GridColumn(_GlobalResources.Type, "type", "type");
            GridColumn actions = new GridColumn(_GlobalResources.Actions, null, null);


            // add columns to data grid
            administrativeTasksGrid.AddColumn(task);
            administrativeTasksGrid.AddColumn(type);
            administrativeTasksGrid.AddColumn(actions);

            administrativeTasksGridUpdatePanel.ContentTemplateContainer.Controls.Add(administrativeTasksGrid);

            loadedWidgetContent.Controls.Add(administrativeTasksGridUpdatePanel);

            // build modal popup for grading the tasks
            Button hiddenButtonForTaskModal = new Button();
            hiddenButtonForTaskModal.ID = widgetId + "_HiddenButtonForTaskModal";
            hiddenButtonForTaskModal.Style.Add("display", "none");
            loadedWidgetContent.Controls.Add(hiddenButtonForTaskModal);

            this._BuildTaskModal(loadedWidgetContent, hiddenButtonForTaskModal.ID, widgetId);

            // build modal popup for grading the certification tasks
            Button hiddenButtonForCertificationTaskModal = new Button();
            hiddenButtonForCertificationTaskModal.ID = widgetId + "_HiddenButtonForCertificationTaskModal";
            hiddenButtonForCertificationTaskModal.Style.Add("display", "none");
            loadedWidgetContent.Controls.Add(hiddenButtonForCertificationTaskModal);

            this._BuildCertificationTaskModal(loadedWidgetContent, hiddenButtonForCertificationTaskModal.ID, widgetId);

            // attach the control before any data bind occurs, if the control is attached
            // after a data bind, the control does not retain its viewstate
            widgetContent.ContentTemplateContainer.Controls.Add(loadedWidgetContent);

            // data bind only if this is not an initial load, not data binding
            // causes the Grid to not appear, and saves an unnecessary database
            // thus allowing us to lazy load the content
            if (!isInitialLoad)
            { administrativeTasksGrid.BindData(); }

        }
        #endregion

        #region _AdministrativeTasksGrid_RowDataBound
        /// <summary>
        /// Row data bound event handler for the Administrative Tasks Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AdministrativeTasksGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                string type = rowView["type"].ToString();

                // check task type
                if(type == "Task Proctor"){ // Task Proctor Task
                    // get data values for row
                    rowView = (DataRowView)e.Row.DataItem;
                    int idUser = Convert.ToInt32(rowView["idUser"]); // will never be null
                    int idEnrollment = Convert.ToInt32(rowView["idEnrollment"]); // will never be null
                    int idDataLesson = Convert.ToInt32(rowView["idData-Lesson"]); // will never be null
                    int idLesson = Convert.ToInt32(rowView["idLesson"]); // will never be null
                    int idDataTask = Convert.ToInt32(rowView["idData-Task"]);
                    string uploadedTaskFilename = rowView["uploadedTaskFilename"].ToString();
                    string taskUploadedDate = rowView["dtUploaded"].ToString();

                    // COURSE/LESSON
                    Panel displayNameContainer = new Panel();
                    Literal displayName = new Literal();
                    displayName.Text = _GlobalResources.TaskProctoring + ": " + HttpContext.Current.Server.HtmlEncode(rowView["userDisplayName"].ToString());
                    displayNameContainer.Controls.Add(displayName);
                    e.Row.Cells[0].Controls.Add(displayNameContainer);
                    
                    Panel courseNameContainer = new Panel();
                    Literal courseName = new Literal();
                    courseName.Text = _GlobalResources.Course + ": " + HttpContext.Current.Server.HtmlEncode(rowView["courseName"].ToString());
                    courseNameContainer.Controls.Add(courseName);
                    e.Row.Cells[0].Controls.Add(courseNameContainer);

                    Panel lessonNameContainer = new Panel();
                    Literal lessonName = new Literal();
                    lessonName.Text = _GlobalResources.Lesson + ": " + HttpContext.Current.Server.HtmlEncode(rowView["lessonName"].ToString());
                    lessonNameContainer.Controls.Add(lessonName);
                    e.Row.Cells[0].Controls.Add(lessonNameContainer);

                    e.Row.Cells[1].Text = _GlobalResources.TaskProctoring;
                    
                    // TASK VIEW BUTTON

                    // button
                    LinkButton launchTaskLink = new LinkButton();
                    launchTaskLink.ID = "LaunchTaskLink_" + idDataLesson.ToString();
                    launchTaskLink.OnClientClick = "LoadTaskModalContent(" + idUser + ", " + idEnrollment + ", " + idDataLesson + ", " + idDataTask + ", " + idLesson + ", \"" + uploadedTaskFilename + "\", \"" + taskUploadedDate + "\"); return false;";

                    Image launchTaskLinkImage = new Image();
                    launchTaskLinkImage.ID = "LaunchTaskLinkImage_" + idDataLesson.ToString();
                    launchTaskLinkImage.CssClass = "SmallIcon";
                    launchTaskLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                    launchTaskLinkImage.AlternateText = _GlobalResources.Task;

                    launchTaskLink.Controls.Add(launchTaskLinkImage);

                    e.Row.Cells[2].Controls.Add(launchTaskLink);

                }
                else if(type == "OJT Proctor"){ // OJT Proctor Task

                    int idDataLesson = Convert.ToInt32(rowView["idData-Lesson"]);
                    int idContentPackage = Convert.ToInt32(rowView["idContentPackage"]);
                    int idContentPackageType = Convert.ToInt32(rowView["idContentPackageType"]);

                    // COURSE/LESSON

                    Panel displayNameContainer = new Panel();
                    Literal displayName = new Literal();
                    displayName.Text = _GlobalResources.OJTProctoring + ": " + HttpContext.Current.Server.HtmlEncode(rowView["userDisplayName"].ToString());
                    displayNameContainer.Controls.Add(displayName);
                    e.Row.Cells[0].Controls.Add(displayNameContainer);

                    Panel courseNameContainer = new Panel();
                    Literal courseName = new Literal();
                    courseName.Text = _GlobalResources.Course + ": " + HttpContext.Current.Server.HtmlEncode(rowView["courseName"].ToString());
                    courseNameContainer.Controls.Add(courseName);
                    e.Row.Cells[0].Controls.Add(courseNameContainer);

                    Panel lessonNameContainer = new Panel();
                    Literal lessonName = new Literal();
                    lessonName.Text = _GlobalResources.Lesson + ": " + HttpContext.Current.Server.HtmlEncode(rowView["lessonName"].ToString());
                    lessonNameContainer.Controls.Add(lessonName);
                    e.Row.Cells[0].Controls.Add(lessonNameContainer);

                    e.Row.Cells[1].Text = _GlobalResources.OJTProctoring;

                    Panel viewContainer = new Panel();
                    HyperLink viewHyperLink = new HyperLink();
                    if (idContentPackageType == Convert.ToInt32(ContentPackage.ContentPackageType.SCORM)) // SCORM content package
                    {
                        viewHyperLink.NavigateUrl = @"/launch/Scorm.aspx?id=" + idDataLesson + "&cpid=" + idContentPackage + "&launchType=ojtWidget";
                    }
                    else // xAPI content package
                    {
                        viewHyperLink.NavigateUrl = @"/launch/Tincan.aspx?id=" + idDataLesson + "&cpid=" + idContentPackage + "&launchType=ojtWidget";
                    }
                    Image viewImage = new Image();
                    viewImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO,
                                                                ImageFiles.EXT_PNG);
                    viewImage.CssClass = "SmallIcon";
                    viewImage.AlternateText = _GlobalResources.View;
                    viewHyperLink.Controls.Add(viewImage);
                    e.Row.Cells[2].Controls.Add(viewHyperLink);
                }
                else if (type == "Certification Task Proctor"){ // Certification Task Proctor Task

                    // get data values for row
                    rowView = (DataRowView)e.Row.DataItem;
                    int idUser = Convert.ToInt32(rowView["idUser"]); // will never be null
                    int idCertificationToUserLink = Convert.ToInt32(rowView["idCertificationToUserLink"]); // will never be null
                    int idDataCertificationModuleRequirement = Convert.ToInt32(rowView["idData-CertificationModuleRequirement"]); // will never be null                                
                    string uploadedTaskFilename = rowView["completionDocumentationFilePath"].ToString();
                    string taskUploadedDate = rowView["dtSubmitted"].ToString();

                    // CERTIFICATION/REQUIREMENT
                    Panel displayNameContainer = new Panel();
                    Literal displayName = new Literal();
                    displayName.Text = _GlobalResources.CertificationTaskProctoring + ": " + HttpContext.Current.Server.HtmlEncode(rowView["userDisplayName"].ToString());
                    displayNameContainer.Controls.Add(displayName);
                    e.Row.Cells[0].Controls.Add(displayNameContainer);

                    Panel certificationNameContainer = new Panel();
                    Literal certificationName = new Literal();
                    certificationName.Text = _GlobalResources.Certification + ": " + HttpContext.Current.Server.HtmlEncode(rowView["certificationName"].ToString());
                    certificationNameContainer.Controls.Add(certificationName);
                    e.Row.Cells[0].Controls.Add(certificationNameContainer);

                    Panel requirementNameContainer = new Panel();
                    Literal requirementName = new Literal();
                    requirementName.Text = _GlobalResources.Requirement + ": " + HttpContext.Current.Server.HtmlEncode(rowView["requirementName"].ToString());
                    requirementNameContainer.Controls.Add(requirementName);
                    e.Row.Cells[0].Controls.Add(requirementNameContainer);

                    e.Row.Cells[1].Text = _GlobalResources.CertificationTaskProctoring;

                    // TASK VIEW BUTTON

                    // button
                    LinkButton launchTaskLink = new LinkButton();
                    launchTaskLink.ID = "LaunchCertificationTaskLink_" + idDataCertificationModuleRequirement.ToString();
                    launchTaskLink.OnClientClick = "LoadCertificationTaskModalContent(" + idUser + ", " + idCertificationToUserLink + ", " + idDataCertificationModuleRequirement + ", \"" + uploadedTaskFilename + "\", \"" + taskUploadedDate + "\"); return false;";

                    Image launchTaskLinkImage = new Image();
                    launchTaskLinkImage.ID = "LaunchCertificationTaskLinkImage_" + idDataCertificationModuleRequirement.ToString();
                    launchTaskLinkImage.CssClass = "SmallIcon";
                    launchTaskLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                    launchTaskLinkImage.AlternateText = _GlobalResources.CertificationTask;

                    launchTaskLink.Controls.Add(launchTaskLinkImage);

                    e.Row.Cells[2].Controls.Add(launchTaskLink);

                }else if(type == "User Registration Approval"){ // User Registration Approval Task

                    e.Row.Cells[1].Text = _GlobalResources.UserRegistrationApproval;

                    // retrieve user's ID from the row
                    rowView = (DataRowView)e.Row.DataItem;
                    int idUser = Convert.ToInt32(rowView["idUser"]);
                                                
                    // approve button
                    HyperLink approveLink = new HyperLink();
                    approveLink.ID = "UserRegistrationApproval_" + idUser.ToString();
                    approveLink.CssClass = "ImageLink";
                    approveLink.ToolTip = _GlobalResources.ApproveRegistration;
                    approveLink.Attributes.Add("onclick", "UserRegistrationApproveRejectClick('" + idUser.ToString() + "', 'approve');");                

                    Image approveImage = new Image();
                    approveImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);                    
                    approveLink.Controls.Add(approveImage);                                                                          

                    // attach the control to the cell
                    e.Row.Cells[2].Controls.Add(approveLink);                                

                    // reject button
                    HyperLink rejectLink = new HyperLink();
                    rejectLink.ID = "UserRegistrationRejection_" + idUser.ToString();
                    rejectLink.CssClass = "ImageLink";
                    rejectLink.ToolTip = _GlobalResources.RejectRegistration;
                    rejectLink.Attributes.Add("onclick", "UserRegistrationApproveRejectClick('" + idUser.ToString() + "', 'reject');");

                    Image rejectImage = new Image();
                    rejectImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    rejectLink.Controls.Add(rejectImage);                

                    // attach the control to the cell
                    e.Row.Cells[2].Controls.Add(rejectLink); 
                    
                }else if(type == "Self Enrollment Approval"){ // self enrollment approval task

                    e.Row.Cells[1].Text = _GlobalResources.SelfEnrollmentApproval;

                    // retrieve user's ID from the row
                    rowView = (DataRowView)e.Row.DataItem;
                    int idEnrollmentRequest = Convert.ToInt32(rowView["idEnrollmentRequest"]);

                    // approve button
                    HyperLink approveLink = new HyperLink();
                    approveLink.ID = "CourseEnrollmentApproval_" + idEnrollmentRequest.ToString();
                    approveLink.CssClass = "ImageLink";
                    approveLink.ToolTip = _GlobalResources.ApproveEnrollment;
                    approveLink.Attributes.Add("onclick", "CourseEnrollmentApproveRejectClick('" + idEnrollmentRequest.ToString() + "', 'approve');");

                    Image approveImage = new Image();
                    approveImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                    approveLink.Controls.Add(approveImage);

                    // attach the control to the cell
                    e.Row.Cells[2].Controls.Add(approveLink);

                    // reject button
                    HyperLink rejectLink = new HyperLink();
                    rejectLink.ID = "CourseEnrollmentRejection_" + idEnrollmentRequest.ToString();
                    rejectLink.CssClass = "ImageLink";
                    rejectLink.ToolTip = _GlobalResources.RejectEnrollment;
                    rejectLink.Attributes.Add("onclick", "CourseEnrollmentApproveRejectClick('" + idEnrollmentRequest.ToString() + "', 'reject');");

                    Image rejectImage = new Image();
                    rejectImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    rejectLink.Controls.Add(rejectImage);

                    // attach the control to the cell
                    e.Row.Cells[2].Controls.Add(rejectLink);
                }
             
            }
        }
        #endregion

        #endregion

        #region Static Methods
        #endregion
    }
}