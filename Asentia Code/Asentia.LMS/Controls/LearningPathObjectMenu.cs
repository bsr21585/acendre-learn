﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Controls
{
    public class LearningPathObjectMenu : WebControl
    {
        #region Constructor
        public LearningPathObjectMenu(LearningPath learningPathObject)
            : base(HtmlTextWriterTag.Div)
        {
            this.LearningPathObject = learningPathObject;
            this.ID = "LearningPathObjectMenu";
        }
        #endregion

        #region Properties
        /// <summary>
        /// The learning path that this menu is built for.
        /// </summary>
        public LearningPath LearningPathObject;

        /// <summary>
        /// The selected menu item.
        /// </summary>
        public MenuObjectItem SelectedItem = MenuObjectItem.None;
        #endregion

        #region MenuObjectItem ENUM
        public enum MenuObjectItem
        {
            None = 0,
            LearningPathDashboard = 1,
            LearningPathProperties = 2,
            Certificates = 3,
            EmailNotifications = 4,
            RulesetEnrollments = 5,            
        }
        #endregion

        #region Private Methods
        #region _BuildLearningPathObjectMenu
        private void _BuildLearningPathObjectMenu()
        {
            this.CssClass = "ObjectMenu";

            // LEARNING PATH DASHBOARD

            // learning path dashboard link container
            Panel learningPathDashboardLinkContainer = new Panel();
            learningPathDashboardLinkContainer.ID = "LearningPathDashboardLinkContainer";
            learningPathDashboardLinkContainer.CssClass = "ObjectMenuLinkContainer";
            learningPathDashboardLinkContainer.ToolTip = _GlobalResources.LearningPathDashboard;

            HyperLink learningPathDashboardImageLink = new HyperLink();
            learningPathDashboardImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD, ImageFiles.EXT_PNG);
            learningPathDashboardImageLink.NavigateUrl = "/administrator/learningpaths/Dashboard.aspx?id=" + this.LearningPathObject.Id.ToString();
            learningPathDashboardLinkContainer.Controls.Add(learningPathDashboardImageLink);

            this.Controls.Add(learningPathDashboardLinkContainer);

            // build links for the learning path properties, certificates, and email notifications if the user has learning path content manager permissions
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathContentManager))
            {
                // LEARNING PATH PROPERTIES

                // learning path properties link container
                Panel learningPathPropertiesLinkContainer = new Panel();
                learningPathPropertiesLinkContainer.ID = "LearningPathPropertiesLinkContainer";
                learningPathPropertiesLinkContainer.CssClass = "ObjectMenuLinkContainer";
                learningPathPropertiesLinkContainer.ToolTip = _GlobalResources.LearningPathProperties;

                HyperLink learningPathPropertiesImageLink = new HyperLink();
                learningPathPropertiesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
                learningPathPropertiesImageLink.NavigateUrl = "/administrator/learningpaths/Modify.aspx?id=" + this.LearningPathObject.Id.ToString();
                learningPathPropertiesLinkContainer.Controls.Add(learningPathPropertiesImageLink);

                this.Controls.Add(learningPathPropertiesLinkContainer);

                // CERTIFICATES            

                // certificates link container
                Panel certificatesLinkContainer = new Panel();
                certificatesLinkContainer.ID = "CertificatesLinkContainer";
                certificatesLinkContainer.CssClass = "ObjectMenuLinkContainer";
                certificatesLinkContainer.ToolTip = _GlobalResources.Certificates;

                HyperLink certificatesImageLink = new HyperLink();
                certificatesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
                certificatesImageLink.NavigateUrl = "/administrator/learningpaths/certificates/Default.aspx?lpid=" + this.LearningPathObject.Id.ToString();
                certificatesLinkContainer.Controls.Add(certificatesImageLink);

                this.Controls.Add(certificatesLinkContainer);

                // EMAIL NOTIFICATIONS

                // email notifications link container
                Panel emailNotificationsLinkContainer = new Panel();
                emailNotificationsLinkContainer.ID = "EmailNotificationsLinkContainer";
                emailNotificationsLinkContainer.CssClass = "ObjectMenuLinkContainer";
                emailNotificationsLinkContainer.ToolTip = _GlobalResources.EmailNotifications;

                HyperLink emailNotificationsImageLink = new HyperLink();
                emailNotificationsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
                emailNotificationsImageLink.NavigateUrl = "/administrator/learningpaths/emailnotifications/Default.aspx?lpid=" + this.LearningPathObject.Id.ToString();
                emailNotificationsLinkContainer.Controls.Add(emailNotificationsImageLink);

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE) ?? false)
                {
                    this.Controls.Add(emailNotificationsLinkContainer);
                }
            }

            // build enrollments link if the user has learning path enrollment manager permissions
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathEnrollmentManager))
            {
                // RULE SET ENROLLMENTS            

                // rule set enrollments link container
                Panel ruleSetEnrollmentsLinkContainer = new Panel();
                ruleSetEnrollmentsLinkContainer.ID = "RuleSetEnrollmentsLinkContainer";
                ruleSetEnrollmentsLinkContainer.CssClass = "ObjectMenuLinkContainer";
                ruleSetEnrollmentsLinkContainer.ToolTip = _GlobalResources.Enrollments;

                HyperLink ruleSetEnrollmentsImageLink = new HyperLink();
                ruleSetEnrollmentsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_SERIES, ImageFiles.EXT_PNG);
                ruleSetEnrollmentsImageLink.NavigateUrl = "/administrator/learningpaths/rulesetenrollments/Default.aspx?lpid=" + this.LearningPathObject.Id.ToString();
                ruleSetEnrollmentsLinkContainer.Controls.Add(ruleSetEnrollmentsImageLink);

                this.Controls.Add(ruleSetEnrollmentsLinkContainer);
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnLoad
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.CreateControls();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        #endregion

        #region CreateControls
        /// <summary>
        /// Creates the controls for rendering to page.
        /// </summary>
        private void CreateControls()
        {
            // build the learning path object menu
            this._BuildLearningPathObjectMenu();

            // apply selected class to item
            switch (this.SelectedItem)
            {
                case MenuObjectItem.None:
                    // nothing to do
                    break;
                case MenuObjectItem.LearningPathDashboard:
                    // select the learning path dashboard link
                    Panel learningPathDashboardLinkContainer = (Panel)this.FindControl("LearningPathDashboardLinkContainer");

                    if (learningPathDashboardLinkContainer != null)
                    { learningPathDashboardLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.LearningPathProperties:
                    // select the learning path properties link
                    Panel learningPathPropertiesLinkContainer = (Panel)this.FindControl("LearningPathPropertiesLinkContainer");

                    if (learningPathPropertiesLinkContainer != null)
                    { learningPathPropertiesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Certificates:
                    // select the certificates link
                    Panel certificatesLinkContainer = (Panel)this.FindControl("CertificatesLinkContainer");

                    if (certificatesLinkContainer != null)
                    { certificatesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.EmailNotifications:
                    // select the email notifications link
                    Panel emailNotificationsLinkContainer = (Panel)this.FindControl("EmailNotificationsLinkContainer");

                    if (emailNotificationsLinkContainer != null)
                    { emailNotificationsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.RulesetEnrollments:
                    // select the ruleset enrollments link
                    Panel rulesetEnrollmentsLinkContainer = (Panel)this.FindControl("RuleSetEnrollmentsLinkContainer");

                    if (rulesetEnrollmentsLinkContainer != null)
                    { rulesetEnrollmentsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;                
            }
        }
        #endregion
        #endregion
    }
}
