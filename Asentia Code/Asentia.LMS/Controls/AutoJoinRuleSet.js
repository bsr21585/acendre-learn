﻿// document ready event
$(document).ready(function () {

    var ruleRows = $("tr.RuleRow");

    if (ruleRows != null && ruleRows.length <= 1) {
        $(".RemoveRuleButton").hide();
    }

    // initialize a datepicker on the document and wrap it in a div
    // this prevents the issue of datepicker initialization causing a vertical scrollbar on the page
    $(document).datepicker();
    $("#ui-datepicker-div").wrap("<div style=\"position: absolute; top: 0px;\"></div>");
});

// Removes a rule from the ruleset
function removeRule(elem) {

    var ruleRows = $("tr.RuleRow");

    if (ruleRows != null && ruleRows.length > 1) {
        $(elem).parent().parent().remove();
    }

    var ruleRows = $("tr.RuleRow");

    if (ruleRows != null && ruleRows.length <= 1) {
        $(".RemoveRuleButton").hide();
    }
}

// Adds a rule to the ruleset
function addRule(elem) {

    var ruleRows = $("tr.RuleRow");

    if (ruleRows != null) {

        var newRuleRow = $(ruleRows[ruleRows.length - 1]).clone();

        if (newRuleRow != null) {

            var ruleIdValue = $(newRuleRow).find(".RuleIdValue")[0];
            var userFieldList = $(newRuleRow).find(".UserFieldsList")[0];
            var operatorList = $(newRuleRow).find(".OperatorsList")[0];
            var textBox = $(newRuleRow).find(".UserFieldValue")[0];
            var removeRuleButton = $(newRuleRow).find(".RemoveRuleButton")[0];

            if (ruleIdValue != null && userFieldList != null && operatorList != null && textBox != null && removeRuleButton != null) {

                $(ruleIdValue).val("");
                $(userFieldList).val("");
                $(operatorList).val("");
                $(textBox).val("");
                $(textBox).removeClass("hasDatepicker");

                var lastId = userFieldList["id"].split("_")[1];
                var newId = parseInt(lastId) + 1;

                $(newRuleRow).attr("id", $(newRuleRow).attr("id").replace("_" + lastId, "_" + newId));

                $(ruleIdValue).attr("id", $(ruleIdValue).attr("id").replace("_" + lastId, "_" + newId));
                $(ruleIdValue).attr("name", $(ruleIdValue).attr("name").replace("_" + lastId, "_" + newId));

                $(userFieldList).attr("id", $(userFieldList).attr("id").replace("_" + lastId, "_" + newId));
                $(userFieldList).attr("name", $(userFieldList).attr("name").replace("_" + lastId, "_" + newId));

                $(operatorList).attr("id", $(operatorList).attr("id").replace("_" + lastId, "_" + newId));
                $(operatorList).attr("name", $(operatorList).attr("name").replace("_" + lastId, "_" + newId));

                $(textBox).attr("id", $(textBox).attr("id").replace("_" + lastId, "_" + newId));
                $(textBox).attr("name", $(textBox).attr("name").replace("_" + lastId, "_" + newId));

                $(removeRuleButton).attr("id", $(removeRuleButton).attr("id").replace("_" + lastId, "_" + newId));

                $(ruleRows[ruleRows.length - 1]).after(newRuleRow);

                $(".RemoveRuleButton").show();
            }
        }
    }
}

// Gets the rules from inputs and puts them in a struct
function getRules(elem, hfId) {

    var rules = new Array();

    $("tr.RuleRow").each(function (index, obj) {

        var ruleIdValue = $(obj).find(".RuleIdValue")[0];
        var userFieldList = $(obj).find(".UserFieldsList")[0];
        var operatorList = $(obj).find(".OperatorsList")[0];
        var textBox = $(obj).find(".UserFieldValue")[0];

        if (userFieldList != null && operatorList != null && textBox != null) {

            var rule = new RuleObject();
            rule.IdRule = $(ruleIdValue).val();
            rule.UserField = $(userFieldList).val();
            rule.Operator = $(operatorList).val();
            rule.Value = $(textBox).val();
            rules.push(rule);
        }
    });

    var stringifiedRules = JSON.stringify(rules);
    $("#" + hfId).val(stringifiedRules);
}

// Rule structure used in getRules function
function RuleObject() {

    //represents the user field
    this.IdRule = null;
    //represents the user field
    this.UserField = null;
    //represents the operator
    this.Operator = null;
    //represents the value
    this.Value = null;
}

// Function to handle rule field change
// Removes operator functionality for certain fields, right now just "Group"
function RuleFieldChanged(fieldDropDown) {
    var fieldDropDownId = fieldDropDown.id;
    var operatorDropDownId = fieldDropDownId.replace("UserFieldsList", "OperatorsList");

    var selectedField = $("#" + fieldDropDownId).val();
    var selectedOperator = $("#" + operatorDropDownId).val();

    // if the selected field is group, disable "greater than" and "less than" operators
    // otherwise, enable them
    if (selectedField == "group") {
        $("#" + operatorDropDownId + " option[value*='gtn']").prop("disabled", true);
        $("#" + operatorDropDownId + " option[value*='ltn']").prop("disabled", true);
        $("#" + operatorDropDownId + " option[value*='gtd']").prop("disabled", true);
        $("#" + operatorDropDownId + " option[value*='ltd']").prop("disabled", true);

        // if any of the "greater than" or "less than" operators were selected, unselect them and return to default "starts with"
        if (selectedOperator == "gtn" || selectedOperator == "ltn" || selectedOperator == "gtd" || selectedOperator == "ltd") {
            $("#" + operatorDropDownId).val("sw");
        }
    }
    else {
        $("#" + operatorDropDownId + " option[value*='gtn']").prop("disabled", false);
        $("#" + operatorDropDownId + " option[value*='ltn']").prop("disabled", false);
        $("#" + operatorDropDownId + " option[value*='gtd']").prop("disabled", false);
        $("#" + operatorDropDownId + " option[value*='ltd']").prop("disabled", false);
    }

    // evaluate the operator based on field change
    RuleOperatorChanged(document.getElementById(operatorDropDownId));
}

// Function to handle rule operator change
// Initializes datepicker on rule value field when "less than (date)" or "greater than (date)" is selected,
// or when "equals" or "not equals" is selected and the field is defined as a date field, this also destroys 
// the datepicker when another operator is selected that is not valid for datepicker.
function RuleOperatorChanged(operatorDropDown) {
    var operatorDropDownId = operatorDropDown.id;
    var fieldDropDownId = operatorDropDownId.replace("OperatorsList", "UserFieldsList");
    var userFieldValueId = operatorDropDownId.replace("OperatorsList", "UserFieldValue");

    var selectedField = $("#" + fieldDropDownId).val();
    var selectedOperator = $("#" + operatorDropDownId).val();

    // if the selected operator is "less than (date)" or "greater than (date)" OR "equals" or "not equals" AND the field is defined as a date field,
    // AND there is not already a datepicker on the associated rule value field, initialize the datepicker; otherwise, destroy the datepicker
    if (selectedOperator == "ltd" || selectedOperator == "gtd" || ((selectedOperator == "eq" || selectedOperator == "neq") && $("#" + fieldDropDownId + " option[value*='" + selectedField + "']").hasClass("UserFieldTypeDate"))) {
        if (!$("#" + userFieldValueId).hasClass("hasDatepicker")) {
            InitializeDatePicker(userFieldValueId);
        }
    }
    else {
        if ($("#" + userFieldValueId).hasClass("hasDatepicker")) {
            DestroyDatePicker(userFieldValueId);
        }
    }
}