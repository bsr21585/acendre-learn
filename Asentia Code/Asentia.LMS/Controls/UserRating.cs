﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.IO;

namespace Asentia.LMS.Controls
{
    public class UserRating : WebControl
    {
        #region Constructor
        public UserRating(string identifier)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = identifier;
        }
        #endregion

        #region Properties
        public bool UseBigImages = false;
        public bool ShowYourRating = false;
        public bool ShowUserRating = false;

        public int IdObject = 0;
        public double UsersRating = 0;
        public int YourRating = 0;
        public int Votes = 0;

        public string RateUrl = null;
        public string ResetUrl = null;
        #endregion

        #region Private Properties
        private const int _STARSWIDTH_SMALL = 92;
        private const int _STARSHEIGHT_SMALL = 16;

        private const int _STARSWIDTH_BIG = 172;
        private const int _STARSHEIGHT_BIG = 30;

        private const string _STARSIMAGEBASE_SMALL = "/_images/ratings/graystars_small.png";
        private const string _STARSIMAGERED_SMALL = "/_images/ratings/redstars_small.png";
        private const string _STARSIMAGEYELLOW_SMALL = "/_images/ratings/yellowstars_small.png";

        private const string _STARSIMAGEBASE_BIG = "/_images/ratings/graystars_big.png";
        private const string _STARSIMAGERED_BIG = "/_images/ratings/redstars_big.png";
        private const string _STARSIMAGEYELLOW_BIG = "/_images/ratings/yellowstars_big.png";
        #endregion

        #region Methods
        #region BuildUserRatingControl
        /// <summary>
        /// Public accessor method to kick off the building of the user rating control.
        /// This is used when building the control using a web method. See catalog.
        /// </summary>
        public void BuildUserRatingControl()
        {
            this._BuildUserRatingControl();
        }
        #endregion
        #endregion

        #region Private Methods
        #region _BuildUserRatingControl
        private void _BuildUserRatingControl()
        {
            string avgRatingStarsDisplay;
            string yourRatingStarsDisplay;

            if (!String.IsNullOrWhiteSpace(this.RateUrl) && this.YourRating != 0)
            {
                avgRatingStarsDisplay = "none";
                yourRatingStarsDisplay = "block";
            }
            else
            {
                avgRatingStarsDisplay = "block";
                yourRatingStarsDisplay = "none";
            }

            // set the class of this control
            this.CssClass = "UserRatingControl";

            // stars container
            Panel starsContainer = new Panel();
            starsContainer.ID = this.ID + "_StarsContainer";

            if (this.UseBigImages)
            { starsContainer.CssClass = "StarsContainerBig";  }
            else
            { starsContainer.CssClass = "StarsContainer"; }

            if (!String.IsNullOrWhiteSpace(this.RateUrl))
            {
                starsContainer.Attributes.Add("onMouseOver", this.ID + ".rate();");
                starsContainer.Attributes.Add("onMouseOut", this.ID + ".ret();");
            }

            // base stars
            Panel baseStars = new Panel();
            baseStars.ID = this.ID + "_BaseStars";

            if (this.UseBigImages)
            { baseStars.CssClass = "BaseStarsBig";  }
            else
            { baseStars.CssClass = "BaseStars"; }

            Image baseStarsImage = new Image();

            if (this.UseBigImages)
            {
                baseStarsImage.ImageUrl = _STARSIMAGEBASE_BIG;
                baseStarsImage.Width = _STARSWIDTH_BIG;
                baseStarsImage.Height = _STARSHEIGHT_BIG;
            }
            else
            {
                baseStarsImage.ImageUrl = _STARSIMAGEBASE_SMALL;
                baseStarsImage.Width = _STARSWIDTH_SMALL;
                baseStarsImage.Height = _STARSHEIGHT_SMALL;
            }

            // attach controls to containers
            baseStars.Controls.Add(baseStarsImage);
            starsContainer.Controls.Add(baseStars);

            // avg rating stars (red)
            Panel avgRatingStars = new Panel();
            avgRatingStars.ID = this.ID + "_AvgStars";

            if (this.UseBigImages)
            { avgRatingStars.CssClass = "AvgRatingStarsBig"; }
            else
            { avgRatingStars.CssClass = "AvgRatingStars"; }

            avgRatingStars.Style.Add("display", avgRatingStarsDisplay);

            int avgRating;

            if (this.UseBigImages)
            { avgRating = Convert.ToInt32(_STARSWIDTH_BIG * (this.UsersRating * 20 * .01)); }
            else
            { avgRating = Convert.ToInt32(_STARSWIDTH_SMALL * (this.UsersRating * 20 * .01)); }

            avgRatingStars.Style.Add("width", avgRating.ToString() + "px");

            Image avgRatingStarsImage = new Image();

            if (this.UseBigImages)
            {
                avgRatingStarsImage.ImageUrl = _STARSIMAGERED_BIG;
                avgRatingStarsImage.Width = _STARSWIDTH_BIG;
                avgRatingStarsImage.Height = _STARSHEIGHT_BIG;
            }
            else
            {
                avgRatingStarsImage.ImageUrl = _STARSIMAGERED_SMALL;
                avgRatingStarsImage.Width = _STARSWIDTH_SMALL;
                avgRatingStarsImage.Height = _STARSHEIGHT_SMALL;
            }

            // attach controls to containers
            avgRatingStars.Controls.Add(avgRatingStarsImage);
            starsContainer.Controls.Add(avgRatingStars);

            // your rating stars (yellow)
            Panel yourRatingStars = new Panel();
            yourRatingStars.ID = this.ID + "_YourStars";

            if (this.UseBigImages)
            { yourRatingStars.CssClass = "YourRatingStarsBig RatingBig" + this.YourRating.ToString(); }
            else
            { yourRatingStars.CssClass = "YourRatingStars Rating" + this.YourRating.ToString(); }

            yourRatingStars.Style.Add("display", yourRatingStarsDisplay);

            Image yourRatingStarsImage = new Image();

            if (this.UseBigImages)
            {
                yourRatingStarsImage.ImageUrl = _STARSIMAGEYELLOW_BIG;
                yourRatingStarsImage.Width = _STARSWIDTH_BIG;
                yourRatingStarsImage.Height = _STARSHEIGHT_BIG;
            }
            else
            {
                yourRatingStarsImage.ImageUrl = _STARSIMAGEYELLOW_SMALL;
                yourRatingStarsImage.Width = _STARSWIDTH_SMALL;
                yourRatingStarsImage.Height = _STARSHEIGHT_SMALL;
            }

            // attach controls to containers
            yourRatingStars.Controls.Add(yourRatingStarsImage);
            starsContainer.Controls.Add(yourRatingStars);

            // interactive stars only if RateUrl is set
            if (!String.IsNullOrWhiteSpace(this.RateUrl))
            {
                Panel interactiveRatingStars = new Panel();
                interactiveRatingStars.ID = this.ID + "_InteractiveStars";

                if (this.UseBigImages)
                { interactiveRatingStars.CssClass = "SetRatingStarsBig"; }
                else
                { interactiveRatingStars.CssClass = "SetRatingStars"; }

                interactiveRatingStars.Style.Add("display", "none");

                string interactiveRatingLinksCssClass;

                if (this.UseBigImages)
                { interactiveRatingLinksCssClass = "StarBig"; }
                else
                { interactiveRatingLinksCssClass = "Star"; }

                // 5 star
                HyperLink interactiveRatingLink5 = new HyperLink();
                interactiveRatingLink5.CssClass = interactiveRatingLinksCssClass;
                interactiveRatingLink5.NavigateUrl = "javascript:void(0);";
                interactiveRatingLink5.Attributes.Add("onClick", this.ID + ".postYourRating(5);");
                interactiveRatingLink5.Attributes.Add("alt", String.Format(_GlobalResources.ClickToRateStars, 5));
                interactiveRatingLink5.Attributes.Add("title", String.Format(_GlobalResources.ClickToRateStars, 5));
                interactiveRatingLink5.Text = "5";

                if (this.UseBigImages)
                { interactiveRatingLink5.Style.Add("width", "172px"); }
                else
                { interactiveRatingLink5.Style.Add("width", "92px"); }

                // attach control to container
                interactiveRatingStars.Controls.Add(interactiveRatingLink5);

                // 4 star
                HyperLink interactiveRatingLink4 = new HyperLink();
                interactiveRatingLink4.CssClass = interactiveRatingLinksCssClass;
                interactiveRatingLink4.NavigateUrl = "javascript:void(0);";
                interactiveRatingLink4.Attributes.Add("onClick", this.ID + ".postYourRating(4);");
                interactiveRatingLink4.Attributes.Add("alt", String.Format(_GlobalResources.ClickToRateStars, 4));
                interactiveRatingLink4.Attributes.Add("title", String.Format(_GlobalResources.ClickToRateStars, 4));
                interactiveRatingLink4.Text = "4";

                if (this.UseBigImages)
                { interactiveRatingLink4.Style.Add("width", "138px"); }
                else
                { interactiveRatingLink4.Style.Add("width", "74px"); }

                // attach control to container
                interactiveRatingStars.Controls.Add(interactiveRatingLink4);

                // 3 star
                HyperLink interactiveRatingLink3 = new HyperLink();
                interactiveRatingLink3.CssClass = interactiveRatingLinksCssClass;
                interactiveRatingLink3.NavigateUrl = "javascript:void(0);";
                interactiveRatingLink3.Attributes.Add("onClick", this.ID + ".postYourRating(3);");
                interactiveRatingLink3.Attributes.Add("alt", String.Format(_GlobalResources.ClickToRateStars, 3));
                interactiveRatingLink3.Attributes.Add("title", String.Format(_GlobalResources.ClickToRateStars, 3));
                interactiveRatingLink3.Text = "3";

                if (this.UseBigImages)
                { interactiveRatingLink3.Style.Add("width", "103px"); }
                else
                { interactiveRatingLink3.Style.Add("width", "55px"); }

                // attach control to container
                interactiveRatingStars.Controls.Add(interactiveRatingLink3);

                // 2 star
                HyperLink interactiveRatingLink2 = new HyperLink();
                interactiveRatingLink2.CssClass = interactiveRatingLinksCssClass;
                interactiveRatingLink2.NavigateUrl = "javascript:void(0);";
                interactiveRatingLink2.Attributes.Add("onClick", this.ID + ".postYourRating(2);");
                interactiveRatingLink2.Attributes.Add("alt", String.Format(_GlobalResources.ClickToRateStars, 2));
                interactiveRatingLink2.Attributes.Add("title", String.Format(_GlobalResources.ClickToRateStars, 2));
                interactiveRatingLink2.Text = "2";

                if (this.UseBigImages)
                { interactiveRatingLink2.Style.Add("width", "69px"); }
                else
                { interactiveRatingLink2.Style.Add("width", "37px"); }

                // attach control to container
                interactiveRatingStars.Controls.Add(interactiveRatingLink2);

                // 1 star
                HyperLink interactiveRatingLink1 = new HyperLink();
                interactiveRatingLink1.CssClass = interactiveRatingLinksCssClass;
                interactiveRatingLink1.NavigateUrl = "javascript:void(0);";
                interactiveRatingLink1.Attributes.Add("onClick", this.ID + ".postYourRating(1);");
                interactiveRatingLink1.Attributes.Add("alt", String.Format(_GlobalResources.ClickToRateStars, 1));
                interactiveRatingLink1.Attributes.Add("title", String.Format(_GlobalResources.ClickToRateStars, 1));
                interactiveRatingLink1.Text = "1";

                if (this.UseBigImages)
                { interactiveRatingLink1.Style.Add("width", "34px"); }
                else
                { interactiveRatingLink1.Style.Add("width", "18px"); }

                // attach control to container
                interactiveRatingStars.Controls.Add(interactiveRatingLink1);

                // attach interactive stars container to main stars container
                starsContainer.Controls.Add(interactiveRatingStars);
            }

            // attach stars container to this control
            this.Controls.Add(starsContainer);

            // show your rating
            if (this.ShowYourRating)
            {
                Panel yourRatingContainer = new Panel();
                yourRatingContainer.ID = this.ID + "_YourRatingContainer";

                yourRatingContainer.CssClass = "YourRatingContainer";

                Label yourRatingLabel = new Label();
                yourRatingLabel.ID = this.ID + "_YourRatingCaption";
                yourRatingLabel.CssClass = "YourRatingCaption";
                yourRatingLabel.Text = _GlobalResources.YourRating + ": ";
                yourRatingContainer.Controls.Add(yourRatingLabel);

                Label yourRatingValueLabel = new Label();
                yourRatingValueLabel.ID = this.ID + "_YourRatingValue";
                yourRatingValueLabel.CssClass = "YourRatingValue";

                if (this.YourRating == 0)
                { yourRatingValueLabel.Text = _GlobalResources.NotYetRated; }
                else
                { yourRatingValueLabel.Text = String.Format(_GlobalResources.XStars, this.YourRating.ToString()); }

                yourRatingContainer.Controls.Add(yourRatingValueLabel);

                this.Controls.Add(yourRatingContainer);
            }

            // show user rating
            if (this.ShowUserRating)
            {
                Panel avgRatingContainer = new Panel();
                avgRatingContainer.ID = this.ID + "_AvgRatingContainer";

                avgRatingContainer.CssClass = "AvgRatingContainer";

                Label avgRatingLabel = new Label();
                avgRatingLabel.ID = this.ID + "_AvgRatingCaption";
                avgRatingLabel.CssClass = "AvgRatingCaption";
                avgRatingLabel.Text = String.Format(_GlobalResources.AverageOfRatings + ": ", this.Votes.ToString());
                avgRatingContainer.Controls.Add(avgRatingLabel);

                Label avgRatingValueLabel = new Label();
                avgRatingValueLabel.ID = this.ID + "_AvgRatingValue";
                avgRatingValueLabel.CssClass = "AvgRatingValue";

                if (this.UsersRating == 0)
                { avgRatingValueLabel.Text = _GlobalResources.NotYetRated; }
                else
                { avgRatingValueLabel.Text = String.Format(_GlobalResources.XStars, this.UsersRating.ToString()); }

                avgRatingContainer.Controls.Add(avgRatingValueLabel);

                if (!String.IsNullOrWhiteSpace(this.ResetUrl) && this.Votes > 0)
                {
                    Label resetLabel = new Label();
                    resetLabel.ID = this.ID + "_ResetRatingCaption";
                    resetLabel.CssClass = "ResetRatingCaption";

                    Literal openParen = new Literal();
                    openParen.Text = "(";

                    HyperLink resetLink = new HyperLink();
                    resetLink.NavigateUrl = "javascript:void(0);";
                    resetLink.Attributes.Add("onClick", this.ID + ".postReset();");
                    resetLink.Text = _GlobalResources.ResetRating;

                    Literal closeParen = new Literal();
                    closeParen.Text = ")";

                    resetLabel.Controls.Add(openParen);
                    resetLabel.Controls.Add(resetLink);
                    resetLabel.Controls.Add(closeParen);
                    avgRatingContainer.Controls.Add(resetLabel);
                }

                this.Controls.Add(avgRatingContainer);
            }

            // javascript for interactive elements, rendered only if RateUrl or ResetUrl is not null
            if (!String.IsNullOrWhiteSpace(this.RateUrl) || !String.IsNullOrWhiteSpace(this.ResetUrl))
            {
                HtmlGenericControl scriptTag = new HtmlGenericControl("script");
                scriptTag.Attributes.Add("type", "text/javascript");

                Literal scriptLine = new Literal();
                scriptLine.Text = "var " + this.ID + " = new UserRatingControl(\"" + this.ID + "\", " + this.IdObject + ", \"" + this.RateUrl + "\", \"" + this.ResetUrl + "\", " + this.YourRating.ToString() + ", " + this.UseBigImages.ToString().ToLower() + ");";

                scriptTag.Controls.Add(scriptLine);
                this.Controls.Add(scriptTag);
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(this.GetType(), "Asentia.LMS.Controls.UserRating.js");

            // build global JS variables
            StringBuilder globalJS = new StringBuilder();
            globalJS.AppendLine("XStars = \"" + _GlobalResources.XStars + "\";");
            globalJS.AppendLine("NotYetRated = \"" + _GlobalResources.NotYetRated + "\";");
            globalJS.AppendLine("AverageOfRatings = \"" + _GlobalResources.AverageOfRatings + "\";");

            csm.RegisterClientScriptBlock(typeof(UserRating), "UserRatingGlobalJS", globalJS.ToString(), true);
        }
        #endregion

        #region CreateChildControls
        /// <summary>
        /// Creates the controls for rendering to page.
        /// </summary>
        protected override void CreateChildControls()
        {
            // build the user rating control
            this._BuildUserRatingControl();

            // continue executing base method
            base.CreateChildControls();
        }
        #endregion
        #endregion
    }
}