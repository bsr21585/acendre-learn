﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Controls
{
    public class CourseObjectMenu : WebControl
    {
        #region Constructor
        public CourseObjectMenu(Course courseObject)
            : base(HtmlTextWriterTag.Div)
        {
            this.CourseObject = courseObject;
            this.ID = "CourseObjectMenu";
        }
        #endregion

        #region Properties
        /// <summary>
        /// The course that this menu is built for.
        /// </summary>
        public Course CourseObject;

        /// <summary>
        /// The selected menu item.
        /// </summary>
        public MenuObjectItem SelectedItem = MenuObjectItem.None;
        #endregion

        #region MenuObjectItem ENUM
        public enum MenuObjectItem
        {
            None = 0,
            CourseDashboard = 1,
            CourseProperties = 2,
            Certificates = 3,
            EmailNotifications = 4,
            RulesetEnrollments = 5,
            Discussion = 6,
        }
        #endregion

        #region Private Methods
        #region _BuildCourseObjectMenu
        private void _BuildCourseObjectMenu()
        {
            this.CssClass = "ObjectMenu";

            // COURSE DASHBOARD

            // course dashboard link container
            Panel courseDashboardLinkContainer = new Panel();
            courseDashboardLinkContainer.ID = "CourseDashboardLinkContainer";
            courseDashboardLinkContainer.CssClass = "ObjectMenuLinkContainer";
            courseDashboardLinkContainer.ToolTip = _GlobalResources.CourseDashboard;

            HyperLink courseDashboardImageLink = new HyperLink();
            courseDashboardImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD, ImageFiles.EXT_PNG);
            courseDashboardImageLink.NavigateUrl = "/administrator/courses/Dashboard.aspx?id=" + this.CourseObject.Id.ToString();
            courseDashboardLinkContainer.Controls.Add(courseDashboardImageLink);

            this.Controls.Add(courseDashboardLinkContainer);

            // build links for course properties, certificates, and email notification if the user has course content manager permissions
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseContentManager))
            {
                // COURSE PROPERTIES

                // course properties link container
                Panel coursePropertiesLinkContainer = new Panel();
                coursePropertiesLinkContainer.ID = "CoursePropertiesLinkContainer";
                coursePropertiesLinkContainer.CssClass = "ObjectMenuLinkContainer";
                coursePropertiesLinkContainer.ToolTip = _GlobalResources.CourseProperties;

                HyperLink coursePropertiesImageLink = new HyperLink();
                coursePropertiesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
                coursePropertiesImageLink.NavigateUrl = "/administrator/courses/Modify.aspx?id=" + this.CourseObject.Id.ToString();
                coursePropertiesLinkContainer.Controls.Add(coursePropertiesImageLink);

                this.Controls.Add(coursePropertiesLinkContainer);

                // CERTIFICATES            

                // certificates link container
                Panel certificatesLinkContainer = new Panel();
                certificatesLinkContainer.ID = "CertificatesLinkContainer";
                certificatesLinkContainer.CssClass = "ObjectMenuLinkContainer";
                certificatesLinkContainer.ToolTip = _GlobalResources.Certificates;

                HyperLink certificatesImageLink = new HyperLink();
                certificatesImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
                certificatesImageLink.NavigateUrl = "/administrator/courses/certificates/Default.aspx?cid=" + this.CourseObject.Id.ToString();
                certificatesLinkContainer.Controls.Add(certificatesImageLink);

                this.Controls.Add(certificatesLinkContainer);

                // EMAIL NOTIFICATIONS

                // email notifications link container
                Panel emailNotificationsLinkContainer = new Panel();
                emailNotificationsLinkContainer.ID = "EmailNotificationsLinkContainer";
                emailNotificationsLinkContainer.CssClass = "ObjectMenuLinkContainer";
                emailNotificationsLinkContainer.ToolTip = _GlobalResources.EmailNotifications;

                HyperLink emailNotificationsImageLink = new HyperLink();
                emailNotificationsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
                emailNotificationsImageLink.NavigateUrl = "/administrator/courses/emailnotifications/Default.aspx?cid=" + this.CourseObject.Id.ToString();
                emailNotificationsLinkContainer.Controls.Add(emailNotificationsImageLink);

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE) ?? false)
                {
                    this.Controls.Add(emailNotificationsLinkContainer);
                }
            }

            // build rule set enrollments link if the user has course enrollment manager permissions
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseEnrollmentManager))
            {
                // RULE SET ENROLLMENTS            

                // rule set enrollments link container
                Panel ruleSetEnrollmentsLinkContainer = new Panel();
                ruleSetEnrollmentsLinkContainer.ID = "RuleSetEnrollmentsLinkContainer";
                ruleSetEnrollmentsLinkContainer.CssClass = "ObjectMenuLinkContainer";
                ruleSetEnrollmentsLinkContainer.ToolTip = _GlobalResources.Enrollments;

                HyperLink ruleSetEnrollmentsImageLink = new HyperLink();
                ruleSetEnrollmentsImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_SERIES, ImageFiles.EXT_PNG);
                ruleSetEnrollmentsImageLink.NavigateUrl = "/administrator/courses/rulesetenrollments/Default.aspx?cid=" + this.CourseObject.Id.ToString();
                ruleSetEnrollmentsLinkContainer.Controls.Add(ruleSetEnrollmentsImageLink);

                this.Controls.Add(ruleSetEnrollmentsLinkContainer);
            }
            
            // DISCUSSION

            if (this.CourseObject.IsFeedActive == true && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE))
            {
                // discussion link container
                Panel discussionLinkContainer = new Panel();
                discussionLinkContainer.ID = "CourseDiscussionLinkContainer";
                discussionLinkContainer.CssClass = "ObjectMenuLinkContainer";
                discussionLinkContainer.ToolTip = _GlobalResources.Discussion;

                HyperLink discussionImageLink = new HyperLink();
                discussionImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG);
                discussionImageLink.NavigateUrl = "/administrator/courses/ManageWall.aspx?id=" + this.CourseObject.Id.ToString();
                discussionLinkContainer.Controls.Add(discussionImageLink);

                this.Controls.Add(discussionLinkContainer);
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnLoad
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.CreateControls();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        #endregion

        #region CreateControls
        /// <summary>
        /// Creates the controls for rendering to page.
        /// </summary>
        private void CreateControls()
        {
            // build the course object menu
            this._BuildCourseObjectMenu();            

            // apply selected class to item
            switch (this.SelectedItem)
            {
                case MenuObjectItem.None:
                    // nothing to do
                    break;
                case MenuObjectItem.CourseDashboard:
                    // select the course dashboard link
                    Panel courseDashboardLinkContainer = (Panel)this.FindControl("CourseDashboardLinkContainer");

                    if (courseDashboardLinkContainer != null)
                    { courseDashboardLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.CourseProperties:
                    // select the course properties link
                    Panel coursePropertiesLinkContainer = (Panel)this.FindControl("CoursePropertiesLinkContainer");

                    if (coursePropertiesLinkContainer != null)
                    { coursePropertiesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Certificates:
                    // select the certificates link
                    Panel certificatesLinkContainer = (Panel)this.FindControl("CertificatesLinkContainer");

                    if (certificatesLinkContainer != null)
                    { certificatesLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.EmailNotifications:
                    // select the email notifications link
                    Panel emailNotificationsLinkContainer = (Panel)this.FindControl("EmailNotificationsLinkContainer");

                    if (emailNotificationsLinkContainer != null)
                    { emailNotificationsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.RulesetEnrollments:
                    // select the ruleset enrollments link
                    Panel rulesetEnrollmentsLinkContainer = (Panel)this.FindControl("RuleSetEnrollmentsLinkContainer");

                    if (rulesetEnrollmentsLinkContainer != null)
                    { rulesetEnrollmentsLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
                case MenuObjectItem.Discussion:
                    // select the discussion link
                    Panel courseDiscussionLinkContainer = (Panel)this.FindControl("CourseDiscussionLinkContainer");

                    if (courseDiscussionLinkContainer != null)
                    { courseDiscussionLinkContainer.CssClass += " ObjectMenuSelectedItem"; }

                    break;
            }
        }
        #endregion
        #endregion
    }
}
