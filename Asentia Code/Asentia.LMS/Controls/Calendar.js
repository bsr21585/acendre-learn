﻿//Function to show/hide the loading panels on the begining of calendar page request
function BeginCalendarPageRequest() {
    $("#CalendarPostbackLoadingPlaceholder").show();
    $("#CalendarBodyContent").hide();
}

//Function to show/hide the loading panels on the ending of calendar page request
function EndCalendarPageRequest() {
    $("#CalendarPostbackLoadingPlaceholder").hide();
    $("#CalendarBodyContent").show();
}

//Function to feed the hidden field values, opening and loading the detail modal popup.
function OpenDetailModalPopup(eventTitle, idEvent, eventType) {

    //Set value of event id in hidden field
    $("#" + HiddenEventId).val(idEvent);
    $("#" + HiddenEventTitle).val(eventTitle);
    $("#" + HiddenEventType).val(eventType);


    //Change the title of modal as the event title
    $("#EventEnrollmentModalPopupModalPopupHeaderText").text(eventTitle);

    //Open the event detail modal
    $('#' + HiddenPopupTargetButton).click();

    //Load detail on the modal popup
    $('#' + HiddenEventDetailLoadButton).click();
}

function pageLoad() {

    //Change the title of modal as the event title
    $("#EventEnrollmentModalPopupModalPopupHeaderText").text($("#HiddenEventTitle").val());
}