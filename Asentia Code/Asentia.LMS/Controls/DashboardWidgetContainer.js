﻿// IMPORTANT NOTE - If you have a widget-specific method that dynamically loads modals or other data and
// there are issues with it performing the actions it is supposed to while the dashboard is still loading
// widgets, i.e. the page is in async postback, wrap the following if statement around the actions your
// method performs:
//
// if (!Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
//      YOUR METHOD'S ACTIONS
// }

/* GLOBAL VARIABLES */
var ConstDotNetMasterPageIdPrefix = "Master$PageContentPlaceholder$";
var _WidgetId = null;
var _TriggerAppendIds = new Array();
var TileWidgetResizeTimer = null;

/* ON LOAD */
/* 
Sets the methods to be called on the beginning of an async postback
and the end of an async postback, and sets the draggable and sortable
settings on the widget icons and widgets.
*/
$(document).ready(function () {
    
    // Get the instance of PageRequestManager
    var RequestManager = Sys.WebForms.PageRequestManager.getInstance();

    // Add initializeRequest and endRequest
    RequestManager.add_initializeRequest(WidgetInitializeRequest);
    RequestManager.add_endRequest(WidgetEndRequest);

    // Add the IDs we append to widget postback triggers
    // this allows us to loop through and extract the widget
    // id so that we can manipulate child controls of the widget.
    // Anytime a new postback trigger is added to a widget, it should
    // be added below.

    _TriggerAppendIds[0] = "RefreshIcon";
    _TriggerAppendIds[1] = "Icon";

    // Call methods to enable jQuery draggable and sortable functionality
    // on widget bar items and widgets themselves - only for standard dashboard
    if (DashboardMode == "Standard") {
        SetDraggableOnWidgetIcons();
        ResetWidgetContainerLayout();
    }

    // check if the enrollments widget is in TILE VIEW
    if ($('#EnrollmentsWidgetEnrollmentsCarouselContainer').length) {
        // change the number of tiles to scroll based on window resize
        $(window).resize(function () {
            if (TileWidgetResizeTimer != null) window.clearTimeout(TileWidgetResizeTimer);
            TileWidgetResizeTimer = window.setTimeout(function () {
                // need to jump back to the first slide because the scrolling has an issue if you resize while in the middle of the carousel
                $('.EnrollmentsCarouselControl.slick-initialized').slick("slickGoTo", 0);
                $('.EnrollmentsCarouselControl.slick-initialized').slick('slickSetOption', 'slidesToScroll', Math.floor($(window).width() / 300));
            }, 200);
        })
    }
});


/*
METHOD: the mobile touch screen functions
Method to handle the mobile swipe left or right functions.
*/
$(document).on("pagecreate", "#MobileDashboardWidgets", function () {
    $("#MobileDashboardWidgets").on("swipeleft swiperight", "#WidgetColumn1", function (event) {
        var swipeLeftIndex, swipeRightIndex;
        for (i = 0; i < $('.WidgetIcon').length; i++) {
            if ($("li[id$='WidgetIconLI']").eq(i).hasClass("WidgetIconLIOn")) {
                $("li[id$='WidgetIconLI']").eq(i).removeClass("WidgetIconLIOn");
                $("li[id$='WidgetIconLI']").eq(i).removeClass("WidgetIconMobileRightHide");
                $("li[id$='WidgetIconLI']").eq(i).removeClass("WidgetIconMobileLeftHide");
                swipeLeftIndex = (i >= ($('.WidgetIcon').length - 3)) ? $('.WidgetIcon').length - 2 : i + 2;
                swipeRightIndex = (i <= 1) ? 1 : i;
            }
        }
        $("div[id$='WidgetControl']").css("display", "none");

        if (event.type == "swipeleft") {
            $("#MoveRightIconLI").removeClass("MoveDim");
            if (DashboardMode == "Standard") {
                ShowMobileWidget("", $('.WidgetIcon').eq(swipeLeftIndex), true, false, true);
            } else {
                ShowMobileWidget("", $('.WidgetIcon').eq(swipeLeftIndex), true, false, false);
            }
            if ($("li[id$='WidgetIconLI']").eq(swipeLeftIndex - 1).hasClass("WidgetIconMobileRightHide")) {
                WidgetBarMoveLeftClick();
            } else {
                MobileWidgetBarAdjustment();
            }
        }
        else if (event.type == "swiperight") {
            $("#MoveLeftIconLI").removeClass("MoveDim");
            if (DashboardMode == "Standard") {
                ShowMobileWidget("", $('.WidgetIcon').eq(swipeRightIndex), true, false, true);
            } else {
                ShowMobileWidget("", $('.WidgetIcon').eq(swipeRightIndex), true, false, false);
            }
            if ($("li[id$='WidgetIconLI']").eq(swipeRightIndex - 1).hasClass("WidgetIconMobileLeftHide")) {
                WidgetBarMoveRightClick();
            } else {
                MobileWidgetBarAdjustment();
            }
        }
    });

    maxNumberAllowed = Math.ceil(($(document).width() - 200) / 40);

    if ($('.WidgetIcon').not('.Navigator').length > maxNumberAllowed) {
        shownNum = 0;
        for (i = 1; i < $('.WidgetIcon').length  ; i++) {
            if ($("li[id$='WidgetIconLI']").eq(i).is(':visible')) {
                shownNum++;
            }
            if (shownNum > maxNumberAllowed) {
                $("li[id$='WidgetIconLI']").eq(i).addClass("WidgetIconMobileRightHide");
            }
        }
    }

    if ($(".WidgetIconMobileRightHide").length <= 2) {
        $(".WidgetIconMobileRightHide").removeClass("WidgetIconMobileRightHide");
        $("#MoveRightIconLI").addClass("MobileNavigatorHide");
        $("#MoveLeftIconLI").addClass("MobileNavigatorHide");
    }
    else if ($(".WidgetIconMobileLeftHide").length == 0) {
            $("#MoveRightIconLI").addClass("MoveDim");        
    }

    $(window).resize(function () {
        MobileWidgetBarAdjustment();
    });
});


/*
METHOD: WidgetInitializeRequest
Method to handle the beginning of an async postback.
This method gets the target widget of the async postback
so that we can show a loader in the widget while its loading.
*/
function WidgetInitializeRequest(sender, args) {
    
    try {
        var UpdatePanelIds = args.get_updatePanelsToUpdate();

        // first, see if the panel to update has been identified
        // if so, use it to obtain the widget id
        // the update panel will be identified if the postback trigger
        // is inside of the update panel, i.e. pagination, etc.
        if (UpdatePanelIds.length == 1) {
            var UpdatePanelRawId = UpdatePanelIds[0];
            _WidgetId = Helper.GetElementStaticId(UpdatePanelRawId);
            _WidgetId = _WidgetId.toString().replace("Content", "");

            // UPDATE PANEL ID PARTS EMBEDDED IN INDIVIDUAL WIDGETS
            _WidgetId = _WidgetId.toString().replace("EnrolledTabPanel", "");
            _WidgetId = _WidgetId.toString().replace("OverdueTabPanel", "");
            _WidgetId = _WidgetId.toString().replace("CompletedTabPanel", "");
            _WidgetId = _WidgetId.toString().replace("ExpiredTabPanel", "");
        }
            // else, get the id of the element that caused the postback
            // and obtain the widget id from there
            // this is the case where a postback trigger is outside of
            // the update panel, i.e. widget icon, widget refresh icon
        else {
            var _PostbackElementId = null;
            _PostbackElementId = args.get_postBackElement().id;

            _WidgetId = _PostbackElementId;

            // loop through the trigger ids we append, and remove them
            // once that is done, we have the id of the widget
            for (var i = 0; i < _TriggerAppendIds.length; i++) {
                _WidgetId = _WidgetId.toString().replace(_TriggerAppendIds[i], '');
            }
        }

        // loop through all child elements of the widget, showing the
        // widget loading placeholder and hiding the rest
        $("#" + _WidgetId + "Content").children().each(function () {
            if (this.id.indexOf("WidgetLoadingPlaceholder") > -1) {
                $("#" + this.id).show();
            }
            else {
                $("#" + this.id).hide();
            }
        });

    }
    catch (e) {
        // do nothing on error, die quietly
    }
}

/*
METHOD: WidgetEndRequest
Method to handle the end of an async postback.
This method gets the target widget of the async postback
so that we can show show the content and hide the loader.
*/
function WidgetEndRequest(sender, args) {
    
    try {
        // loop through all child elements of the widget, hiding the
        // widget loading placeholder and showing the rest
        $("#" + _WidgetId + "Content").children().each(function () {
            if (this.id.indexOf("WidgetLoadingPlaceholder") > -1) {
                console.log("#" + this.id + " HIDE");
                $("#" + this.id).hide();
            }
            else {
                $("#" + this.id).show();
            }
        });
    }
    catch (e) {
        // do nothing on error, die quietly  
        console.log("ERROR Hiding Widget Loading Placeholder");
        console.dir(e);
    }
}

/* DISPLAY METHODS */

/*
METHOD: ResetWidgetContainerLayout
Method that places the jQuery sortable functionality
on the widget columns.
*/
function ResetWidgetContainerLayout() {

    $(".WidgetColumn").sortable({
        placeholder: "WidgetSortPlaceholder",
        connectWith: ".WidgetColumn",
        tolerance: "pointer",
        cancel: ".WidgetContent",
        forcePlaceholderSize: true,
        start: function (event, ui) {
        },
        stop: function (event, ui) {
            // if the widget is to be refreshed on drop, refresh it  by navigating to the href of the widget's refresh icon
            // it has to be a "navigate to href" so the postback fires
            if (ui.item.attr("refreshOnDrop") == "true") {
                var widgetBaseName = ui.item.attr("id").replace("Control", "");
                window.location = $("#" + widgetBaseName + "RefreshIcon").prop("href");
            }

            // save the dashboard widget configuration
            if (DashboardMode == "Standard") { SaveWidgetConfiguration(); }
        },
        helper: function (event, ui) {
            return ui.clone().appendTo("body");
        }
    });
}

/*
METHOD: SetWidgetContainerLayout
Method that sets the layout of the widget dashboard (One or Two Column), 
*/
function SetWidgetContainerLayout(layoutColumn, column1Width, column2Width, saveConfiguration) {

    var layoutContainerId = layoutColumn;

    if (layoutContainerId == "OneColumn") {
        // append any items in widget column 2 to widget column 1
        $(".DraggableControl", "#WidgetColumn2").each(function () {
            $("#WidgetColumn1").append($(this).clone());
            $(this).remove();
        });

        // remove widget column wrapper 2
        $("#WidgetColumnWrapper2").remove();

        // modify classes on widget column 1
        $("#WidgetColumn1").addClass("SelectedLayout");
        $("#WidgetColumnWrapper1").addClass("OneColumnLayout");
        $("#WidgetColumnWrapper1").removeClass("TwoColumnLayout");
        $("#WidgetColumnWrapper1").removeClass("LeftColumn");

        // modify classes on widget column selectors
        $("#OneColumnIconLI").addClass("WidgetLayoutIconLIOn");
        $("#TwoColumnIconLI").removeClass("WidgetLayoutIconLIOn");

        // clear any width previously set on WidgetColumnWrapper1
        $("#WidgetColumnWrapper1").css("width", "");
    }
    else {
        // modify classes on widget column 1
        $("#WidgetColumnWrapper1").addClass("TwoColumnLayout");
        $("#WidgetColumnWrapper1").addClass("LeftColumn");
        $("#WidgetColumn1").removeClass("SelectedLayout");
        $("#WidgetColumnWrapper1").removeClass("OneColumnLayout");

        // if widget column 2 doesn't exist, create it
        if ($("#WidgetColumnWrapper2").length == 0) {
            $("#WidgetContainer").append('<div id="WidgetColumnWrapper2" class="WidgetColumnWrapper TwoColumnLayout RightColumn"><div id="WidgetColumn2" class="WidgetColumn"></div></div>');

            // call method to set jQuery droppable on the columns
            SetDroppableOnWidgetContainer();
        }

        // modify classes on widget column 2
        $("#WidgetColumn2").addClass("SelectedLayout");

        // modify classes on widget column selectors
        $("#TwoColumnIconLI").addClass("WidgetLayoutIconLIOn");
        $("#OneColumnIconLI").removeClass("WidgetLayoutIconLIOn");

        // set the widths of the widget columns
        $("#WidgetColumnWrapper1").css("width", column1Width);
        $("#WidgetColumnWrapper2").css("width", column2Width);

        // set resizable on the widget columns
        var widgetContainer = $("#WidgetContainer");
        var totalWidth;

        $(".WidgetColumnWrapper").resizable({
            handles: 'e',
            start: function (event, ui) {
                totalWidth = ui.originalSize.width + ui.originalElement.next().outerWidth();
            },
            stop: function (event, ui) {
                var divPercentWidth = 100 * ui.originalElement.outerWidth() / widgetContainer.innerWidth();
                ui.originalElement.css("width", divPercentWidth + "%");

                var nextDiv = ui.originalElement.next();
                var nextDivPercentWidth = 100 * nextDiv.outerWidth() / widgetContainer.innerWidth();
                nextDiv.css("width", nextDivPercentWidth + "%");

                if (DashboardMode == "Standard") { SaveWidgetConfiguration(); }
                
            },
            resize: function (event, ui) {
                ui.originalElement.next().width(totalWidth - ui.size.width);
            }
        });
    }

    // if we need to save the configuration of the widgets, save it
    if (saveConfiguration && DashboardMode == "Standard") {
        SaveWidgetConfiguration();
    }

    // call method to make sure jQuery sortable is set on column(s)
    if (DashboardMode == "Standard") {
        ResetWidgetContainerLayout();
    }
}

/*
METHOD: SetDraggableOnWidgetIcons
Method that makes the widget icons draggable.
*/
function SetDraggableOnWidgetIcons() {

    $(".WidgetList").find(".WidgetIcon").draggable({ containment: "#WidgetContainer", helper: "clone", revert: false, snap: ".WidgetList,.WidgetIcon", snapTolerance: "10", distance: "50", stop: function (event, ui) { } });

    // call method to set jQuery droppable on the columns
    SetDroppableOnWidgetContainer();
}

/*
METHOD: SetDroppableOnWidgetContainer
Method that makes the widget columns a droppable target for
the draggable widget icons, and sets the loader functions
for the widget when an icon is dropped.
*/
function SetDroppableOnWidgetContainer() {

    $("#WidgetContainer").find(".WidgetColumn").droppable({
        accept: ".WidgetIcon", activeClass: "HighlightDroppableColumn", drop:
                    function (event, ui) {
                        if (this.id == "WidgetColumn1") {
                            if (typeof (ui.draggable.attr('id')) === "undefined") { }
                            else {
                                if (ui.draggable.attr('id').toString().indexOf("Icon") >= 0) {
                                    ShowWidget("WidgetColumn1", document.getElementById(ui.draggable.attr('id')), true, false, true);
                                }
                            }
                        }
                        else if (this.id == "WidgetColumn2") {
                            if (typeof (ui.draggable.attr('id')) === "undefined") { }
                            else {
                                if (ui.draggable.attr('id').toString().indexOf("Icon") >= 0) {
                                    ShowWidget("WidgetColumn2", document.getElementById(ui.draggable.attr('id')), true, false, true);
                                }
                            }
                        }
                    }
    });
}

/*
METHOD: ShowWidgetOnLoad
Method that is called by dynamic javascript written to the page
to handle the loading of widgets that should be loaded on initial
page load.
*/
function ShowWidgetOnLoad(containerColumn, elementId, isCollapsed) {
    if (DashboardMode == "Standard") {
        widgetIconElement = document.getElementById(elementId + "Icon");
        ShowWidget(containerColumn, widgetIconElement, true, isCollapsed, false);
    } else {
        ShowWidgetForRestrictedDashboard(containerColumn, elementId, true);
    }
}

/*
METHOD: ShowMobileWidgetOnLoad
Method that is called by dynamic javascript written to the page
to handle the loading of widgets that should be loaded on initial
page load.
*/
function ShowMobileWidgetOnLoad(containerColumn, elementId, isCollapsed) {

    widgetIconElement = document.getElementById(elementId + "Icon");
    ShowMobileWidget(containerColumn, widgetIconElement, true, isCollapsed, false);
    
}

/*
METHOD: ShowWidgetOnIconClick
Method that is called by when a widget icon is clicked to handle
the DISPLAY of a widget. Note that no postback is called from this 
because the icons are set as triggers for their widget's UpdatePanel
and ASP.NET AJAX handles that for us.
*/
function ShowWidgetOnIconClick(element) {
    ShowWidget("", element, false, false, true);
}


/*
METHOD: ShowMobileWidgetOnIconClick
*/
function ShowMobileWidgetOnIconClick(elementID) {
    element = document.getElementById(elementID);
    ShowMobileWidget("", element, false, false, true);
    MobileWidgetBarAdjustment();
}


/*
METHOD: ShowWidget
Method that is called to load and display a widget.
*/
function ShowWidget(containerColumn, element, doPostBack, isCollapsed, saveConfiguration) {
    
    var widgetBarIconId = $(element).attr("id");
    var widgetControlId = widgetBarIconId.toString().replace("Icon", "Control");
    var widgetId = widgetBarIconId.toString().replace("Icon", "");

    // if a postback is required, do it
    if (doPostBack) {
        __doPostBack(ConstDotNetMasterPageIdPrefix + widgetBarIconId, "");
    }

    // show/hide the widget
    if ($("#" + widgetControlId).is(":visible")) {
        // hide widget and un-highlight its icon
        $("#" + widgetControlId).css("display", "none");
        $("#" + widgetBarIconId + "LI").removeClass("WidgetIconLIOn");

        //hide alert icon
        $("#" + widgetId + "_AlertIcon").css("display", "none");
    }
    else {
        // determine where to place widget and place it
        if (containerColumn == "WidgetColumn1") {
            $("#WidgetColumn1").append(document.getElementById(widgetControlId));
            $("#WidgetColumn1").find(".DraggableControl").eq($("#WidgetColumn1").children() - 1).before(document.getElementById(widgetControlId));
        }
        else if (containerColumn == "WidgetColumn2") {
            $("#WidgetColumn2").append(document.getElementById(widgetControlId));
            $("#WidgetColumn2").find(".DraggableControl").eq($("#WidgetColumn2").children() - 1).before(document.getElementById(widgetControlId));
        }
        else if (containerColumn == "EnrollmentsWidgetColumn") {
            $("#EnrollmentsWidgetColumn").append(document.getElementById(widgetControlId));
            $("#EnrollmentsWidgetColumn").find(".DraggableControl").eq($("#EnrollmentsWidgetColumn").children() - 1).before(document.getElementById(widgetControlId));
        }
        else {
            if ($("#WidgetColumn1").hasClass("SelectedLayout")) {
                $("#WidgetColumn1").append(document.getElementById(widgetControlId));
                $("#WidgetColumn1").find(".DraggableControl").eq($("#WidgetColumn1").children() - 1).before(document.getElementById(widgetControlId));
            }
            else if ($("#WidgetColumn2").hasClass("SelectedLayout")) {

                // get number of "visible" widgets in column 1
                var column1Count = 0;
                $("#WidgetColumn1").children().each(function () {
                    if ($("#" + this.id).is(":visible")) {
                        column1Count++;
                    }
                });

                // get number of "visible" widgets in column 2
                var column2Count = 0;
                $("#WidgetColumn2").children().each(function () {
                    if ($("#" + this.id).is(":visible")) {
                        column2Count++;
                    }
                });

                // append widget to column based on number of "visible" widgets in each column
                if (column1Count <= column2Count) {
                    $("#WidgetColumn1").append(document.getElementById(widgetControlId));
                    $("#WidgetColumn1").find(".DraggableControl").eq($("#WidgetColumn1").children() - 1).before(document.getElementById(widgetControlId));
                }
                else {
                    $("#WidgetColumn2").append(document.getElementById(widgetControlId));
                    $("#WidgetColumn2").find(".DraggableControl").eq($("#WidgetColumn2").children() - 1).before(document.getElementById(widgetControlId));
                }
            }
        }

        // show widget and highlight its icon
        $("#" + widgetControlId).css("display", "block");
        $("#" + widgetBarIconId + "LI").addClass("WidgetIconLIOn");


        // collapse or expand the widget
        var widgetContentId = widgetBarIconId.toString().replace("Icon", "Content");

        if (isCollapsed) {
            $("#" + widgetContentId).hide();
        }
        else {
            $("#" + widgetContentId).show();
        }

        // set the expand/collapse icon on the widget
        SetExpandColapseWidgetIcon(widgetControlId);
    }

    // if we need to save the configuration of the widgets, save it
    if (saveConfiguration && DashboardMode == "Standard") {
        SaveWidgetConfiguration();
    }

    // call method to make sure jQuery sortable is set on column(s)
    if (DashboardMode == "Standard") {
        ResetWidgetContainerLayout();
    }
}

/*
METHOD: ShowWidgetForRestrictedDashboard
Method that is called to load and display a widget for the restricted dashboard.
*/
function ShowWidgetForRestrictedDashboard(containerColumn, elementId, doPostBack) {
    
    var widgetControlId = elementId + "Control";
    var widgetId = elementId;

    // if a postback is required, do it
    if (doPostBack) {
        __doPostBack(ConstDotNetMasterPageIdPrefix + widgetId + "RefreshIcon", "");
    }

    
    // determine where to place widget and place it
    if (containerColumn == "WidgetColumn1") {
        $("#WidgetColumn1").append(document.getElementById(widgetControlId));
        $("#WidgetColumn1").find(".DraggableControl").eq($("#WidgetColumn1").children() - 1).before(document.getElementById(widgetControlId));
    }
    else if (containerColumn == "WidgetColumn2") {
        $("#WidgetColumn2").append(document.getElementById(widgetControlId));
        $("#WidgetColumn2").find(".DraggableControl").eq($("#WidgetColumn2").children() - 1).before(document.getElementById(widgetControlId));
    }
    else if (containerColumn == "EnrollmentsWidgetColumn") {
        $("#EnrollmentsWidgetColumn").append(document.getElementById(widgetControlId));
        $("#EnrollmentsWidgetColumn").find(".DraggableControl").eq($("#EnrollmentsWidgetColumn").children() - 1).before(document.getElementById(widgetControlId));
    }
    else {
        if ($("#WidgetColumn1").hasClass("SelectedLayout")) {
            $("#WidgetColumn1").append(document.getElementById(widgetControlId));
            $("#WidgetColumn1").find(".DraggableControl").eq($("#WidgetColumn1").children() - 1).before(document.getElementById(widgetControlId));
        }
        else if ($("#WidgetColumn2").hasClass("SelectedLayout")) {

            // get number of "visible" widgets in column 1
            var column1Count = 0;
            $("#WidgetColumn1").children().each(function () {
                if ($("#" + this.id).is(":visible")) {
                    column1Count++;
                }
            });

            // get number of "visible" widgets in column 2
            var column2Count = 0;
            $("#WidgetColumn2").children().each(function () {
                if ($("#" + this.id).is(":visible")) {
                    column2Count++;
                }
            });

            // append widget to column based on number of "visible" widgets in each column
            if (column1Count <= column2Count) {
                $("#WidgetColumn1").append(document.getElementById(widgetControlId));
                $("#WidgetColumn1").find(".DraggableControl").eq($("#WidgetColumn1").children() - 1).before(document.getElementById(widgetControlId));
            }
            else {
                $("#WidgetColumn2").append(document.getElementById(widgetControlId));
                $("#WidgetColumn2").find(".DraggableControl").eq($("#WidgetColumn2").children() - 1).before(document.getElementById(widgetControlId));
            }
        }
    }

    // show widget
    $("#" + widgetControlId).css("display", "block");

    // make widget visible
    var widgetContentId = elementId + "Content";
        
    $("#" + widgetContentId).show();
    
}

/*
METHOD: ShowMobileWidget
Method that is called to load and display a widget on a mobile device.
*/
function ShowMobileWidget(containerColumn, element, doPostBack, isCollapsed, saveConfiguration) {

    var widgetBarIconId = $(element).attr("id");
    var widgetControlId = widgetBarIconId.toString().replace("Icon", "Control");
    var widgetBarIconIDPrev = $("a", $("#" + widgetBarIconId + "LI").prev()).attr("id");
    var widgetBarIconIDNext = $("a", $("#" + widgetBarIconId + "LI").next()).attr("id");
    var widgetId = widgetBarIconId.toString().replace("Icon", "");

    $("#WidgetColumn1").removeClass("TwoColumnLayout");
    $("#WidgetColumn1").addClass("OneColumnLayout");

    // if a postback is required, do it
    if (doPostBack) {
        __doPostBack(ConstDotNetMasterPageIdPrefix + widgetBarIconId, "");
    }

    // show widget and highlight its icon
    $("#" + widgetControlId).css("display", "block")
                            .siblings()
                            .css("display", "none");
    $("#" + widgetBarIconId + "LI").addClass("WidgetIconLIOn")
                                   .siblings()
                                   .removeClass("WidgetIconLIOn");
    $("#" + widgetId + "_AlertIcon").css("display", "inline-block");

    MobileWidgetBarAdjustment();
    $("#" + widgetId + "_AlertIcon").css("display", "none");
    $(".MobileWidgetLeftEdge").attr("href", "javascript:ShowMobileWidgetOnIconClick('" + widgetBarIconIDPrev + "');");
    $(".MobileWidgetRightEdge").attr("href", "javascript:ShowMobileWidgetOnIconClick('" + widgetBarIconIDNext + "');");
}

/*
METHOD: SetExpandColapseWidgetIcon
Method that is called to set the appropriate expand/collapse
icon based on a widget's expanded/collapsed state.
*/
function SetExpandColapseWidgetIcon(widgetControlId) {
    var widgetId = widgetControlId.toString().replace("Control", "");

    if ($("#" + widgetId + "Content").is(":visible")) {
        $("#" + widgetId + "CollapseExpandIcon img").attr("src", CollapseImagePath);
    }
    else {
        $("#" + widgetId + "CollapseExpandIcon img").attr("src", ExpandImagePath);
    }
}

/*
METHOD: CollapseExpandWidget
Method that is called hide the widget and save the configuration.
*/
function CollapseExpandWidget(element) {

    var widgetCollapseExpandButtonId = $(element).attr("id");
    var widgetContentId = widgetCollapseExpandButtonId.toString().replace("CollapseExpandIcon", "Content");

    // collapse/expand the widget
    if ($("#" + widgetContentId).is(":visible")) {
        $("#" + widgetContentId).css("display", "none");
        $("#" + widgetCollapseExpandButtonId + " img").attr("src", ExpandImagePath);
    }
    else {
        $("#" + widgetContentId).css("display", "block");
        $("#" + widgetCollapseExpandButtonId + " img").attr("src", CollapseImagePath);
    }

    // save widget configuration
    SaveWidgetConfiguration();
}

/*
METHOD: CloseWidget
Method that is called hide the widget and save the configuration.
*/
function CloseWidget(element) {

    var widgetCloseButtonId = $(element).attr("id");
    var widgetControlId = widgetCloseButtonId.toString().replace("CloseIcon", "Control");
    var widgetBarIconId = widgetCloseButtonId.toString().replace("CloseIcon", "Icon");

    var widgetBarId = widgetCloseButtonId.toString().replace("CloseIcon", "");

    // hide the widget
    if ($("#" + widgetControlId).is(":visible")) {
        $("#" + widgetControlId).css("display", "none");
        $("#" + widgetBarIconId + "LI").removeClass("WidgetIconLIOn");
        //hide alert icon
        $("#" + widgetBarId + "_AlertIcon").css("display", "none");
    }

    // save widget configuration
    SaveWidgetConfiguration();
}

/* DATA METHODS */

/*
METHOD: SaveWidgetConfiguration
Method that is called to get the current configuration of widgets and
save them to the user's widget dashboard configuration file.
*/
function SaveWidgetConfiguration() {

    var layout;
    var column1Width = null;
    var column2Width = null;

    // get the selected layout
    if ($("#WidgetColumn1").hasClass("SelectedLayout")) {
        layout = "OneColumn";
    }
    else {
        layout = "TwoColumn";

        // need to get width using the method below because we need percentage, and jQuery only returns pixels when you use .css("width")
        $('#WidgetColumnWrapper1').each(function (index) {
            column1Width = this.style.width;
        });

        $('#WidgetColumnWrapper2').each(function (index) {
            column2Width = this.style.width;
        });
    }

    var columnOneWidgets = new Array();
    var columnTwoWidgets = new Array();
    var i = 0;

    // get "visible" widgets in column 1
    $("#WidgetColumn1").children().each(function () {
        if ($("#" + this.id).is(":visible")) {
            var widgetId = this.id.toString().replace("Control", "");
            columnOneWidgets[i] = widgetId;

            if ($("#" + widgetId + "Content").is(":visible")) {
                columnOneWidgets[i] += "|false";
            }
            else {
                columnOneWidgets[i] += "|true";
            }

            i++;
        }
    });

    // if we have a two column layout, get 
    // all "visible" widgets in column 2
    if (layout == "TwoColumn") {
        i = 0;

        $("#WidgetColumn2").children().each(function () {
            if ($("#" + this.id).is(":visible")) {
                var widgetId = this.id.toString().replace("Control", "");
                columnTwoWidgets[i] = widgetId;

                if ($("#" + widgetId + "Content").is(":visible")) {
                    columnTwoWidgets[i] += "|false";
                }
                else {
                    columnTwoWidgets[i] += "|true";
                }

                i++;
            }
        });
    }

    //**
    //JSON
    //**

    // build a JSON string that contains the dashboard configuration
    var jsonString;

    // begin JSON string
    jsonString = "{";

    // "layout" variable
    jsonString += "\"layout\":\"" + layout + "\",";

    if (column1Width != null && column2Width != null) {
        // "column1Width" variable
        jsonString += "\"column1Width\":\"" + column1Width + "\",";

        // "column2Width" variable
        jsonString += "\"column2Width\":\"" + column2Width + "\",";
    }
    else {
        // "column1Width" variable
        jsonString += "\"column1Width\":\"\",";

        // "column2Width" variable
        jsonString += "\"column2Width\":\"\",";
    }

    // "columnOneWidgets" array
    jsonString += "\"columnOneWidgets\":[";

    for (i = 0; i < columnOneWidgets.length; i++) {
        jsonString += "\"" + columnOneWidgets[i] + "\"";

        if (i < columnOneWidgets.length - 1) {
            jsonString += ",";
        }
    }

    jsonString += "],";

    // "columnTwoWidgets" array
    jsonString += "\"columnTwoWidgets\":[";

    for (i = 0; i < columnTwoWidgets.length; i++) {
        jsonString += "\"" + columnTwoWidgets[i] + "\"";

        if (i < columnTwoWidgets.length - 1) {
            jsonString += ",";
        }
    }

    jsonString += "]";

    // end JSON string
    jsonString += "}";

    //**
    //AJAX
    //**

    // send JSON string to web service to save dashboard config
    $.ajax({
        type: "POST",
        url: "../_util/WidgetService.asmx/SaveDashboardConfiguration",
        data: jsonString,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            // do nothing
        },
        error: function (response) {
            // do nothing
        }
    });
}

/* INDIVIDUAL WIDGET METHODS */
/* Add javascript methods for individual widgets below. */


/*
METHOD: PreventOuterScrolling
Method that prevents outer containers from scrolling when scrolling inside widget.
It can be called called for any widget to prevent page scrolling when the user is scrolling inside the widget.
*/
function PreventOuterScrolling(widgetControlID) {
    if ($("#" + widgetControlID) != null) {

        // prevent the window from scrolling when scrolling inside the widget content div
        $("#" + widgetControlID).bind('mousewheel DOMMouseScroll', function (e) {
            var e0 = e.originalEvent,
                delta = e0.wheelDelta || -e0.detail;

            this.scrollTop += (delta < 0 ? 1 : -1) * 30;
            e.preventDefault();
        });
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////        ENROLLMENTS WIDGET         ///////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

/*
METHOD: ToggleEnrollmentTab
Method that is used to toggle between the different tabs of the
enrollments widget.
*/
function ToggleEnrollmentTab(elementId, widgetId) {

    // create an array of the tab names
    var enrollmentTabTypes = new Array();
    enrollmentTabTypes[0] = "Enrolled";
    enrollmentTabTypes[1] = "Pending";
    enrollmentTabTypes[2] = "Overdue";
    enrollmentTabTypes[3] = "Completed";
    enrollmentTabTypes[4] = "Expired";

    // extract the enrollment tab name from the id of the tab link
    var enrollmentTabToShow = elementId.toString().replace(widgetId, "");
    enrollmentTabToShow = enrollmentTabToShow.toString().replace("TabLink", "");

    // loop through the tab name array, show the widget to be shown,
    // and hide the rest.
    for (var i = 0; i < enrollmentTabTypes.length; i++) {
        if (enrollmentTabToShow == enrollmentTabTypes[i]) {
            $("#" + widgetId + enrollmentTabTypes[i] + "TabPanel").show();
            $("#" + widgetId + enrollmentTabTypes[i] + "TabLI").addClass("TabbedListLIOn");
        }
        else {
            $("#" + widgetId + enrollmentTabTypes[i] + "TabPanel").hide();
            $("#" + widgetId + enrollmentTabTypes[i] + "TabLI").removeClass("TabbedListLIOn");
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////        TRANSCRIPT WIDGET         ///////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

/*
METHOD: ToggleTranscriptTab
Method that is used to toggle between the different tabs of the
transcript widget.
*/
function ToggleTranscriptTab(elementId, widgetId) {

    // create an array of the tab names
    var transcriptTabTypes = new Array();
    transcriptTabTypes[0] = "CourseTranscript";
    transcriptTabTypes[1] = "LearningPathTranscript";
    transcriptTabTypes[2] = "InstructorLedTranscript";

    // create an array of the full view tab names
    var transcriptFullViewTabTypes = new Array();
    transcriptFullViewTabTypes[0]= "CourseTranscriptFullView";
    transcriptFullViewTabTypes[1]= "LearningPathTranscriptFullView";
    transcriptFullViewTabTypes[2]= "InstructorLedTranscriptFullView";

    // extract the transcript tab name from the id of the tab link
    var transcriptTabToShow = elementId.toString().replace(widgetId, "");
    transcriptTabToShow = transcriptTabToShow.toString().replace("TabLink", "");

    // loop through the tab name array, show the widget to be shown,
    // and hide the rest.
    for (var i = 0; i < transcriptTabTypes.length; i++) {
        if (transcriptTabToShow == transcriptTabTypes[i]) {
            $("#" +widgetId +transcriptTabTypes[i] + "TabPanel").show();
            $("#" +widgetId +transcriptTabTypes[i]+ "TabLI").addClass("TabbedListLIOn");
            $("#" +transcriptFullViewTabTypes[i]+ "TabPanel").show();
            $("#" +transcriptFullViewTabTypes[i]+ "TabLI").addClass("TabbedListLIOn");
            }
            else {
            $("#" +widgetId +transcriptTabTypes[i]+ "TabPanel").hide();
            $("#" +widgetId +transcriptTabTypes[i]+ "TabLI").removeClass("TabbedListLIOn");
            $("#" +transcriptFullViewTabTypes[i]+ "TabPanel").hide();
            $("#" +transcriptFullViewTabTypes[i]+ "TabLI").removeClass("TabbedListLIOn");
            }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////        PURCHASES WIDGET         ////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////        FEED WIDGET         //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

function GetFeedWidgetRecords() {
    IsFeedWidgetLoading = true;
    $("#FeedWidget_OlderMessagesLoadingPanel").show();

    $.ajax({
        type: "POST",
        url: "../_util/WidgetService.asmx/BuildFeedWidgetMessages",
        data: "{dtQuery: \"" + FeedWidgetLastRecord + "\", getMessagesNewerThanDtQuery: " + false + ", isInitialLoad: " + IsFeedWidgetInitialLoad + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnGetFeedWidgetRecordsSuccess,
        failure: function (response) {
            $("#FeedWidget_OlderMessagesLoadingPanel").hide();
            IsFeedWidgetLoading = false;
            IsFeedWidgetInitialLoad = false;
            //alert(response.d);
        },
        error: function (response) {
            $("#FeedWidget_OlderMessagesLoadingPanel").hide();
            IsFeedWidgetLoading = false;
            IsFeedWidgetInitialLoad = false;
            //alert(response.d);
        }
    });
}

function OnGetFeedWidgetRecordsSuccess(response) {
    var responseObject = response.d;

    $(responseObject.html).hide().appendTo("#FeedWidget_FeedMessagesContainer").fadeIn(1000);
    FeedWidgetLastRecord = responseObject.lastRecord;

    $("#FeedWidget_OlderMessagesLoadingPanel").hide();
    IsFeedWidgetLoading = false;
    IsFeedWidgetInitialLoad = false;
}

function OnFeedWidgetScroll() {
    if ($("#FeedWidgetContent") != null) {

        // prevent the window from scrolling when scrolling inside the FeedWidgetContent div
        PreventOuterScrolling("FeedWidgetContent");

        // grab more records when FeedWidgetContent div is scrolled to bottom
        $("#FeedWidgetContent").scroll(function () {
            if (FeedWidgetScrollPosition < $("#FeedWidgetContent").scrollTop()) {
                if ($("#FeedWidgetContent").scrollTop() + $("#FeedWidgetContent").innerHeight() >= $("#FeedWidgetContent")[0].scrollHeight) {
                    if (!IsFeedWidgetLoading) {
                        GetFeedWidgetRecords();
                    }
                }
            }
            FeedWidgetScrollPosition = $("#FeedWidgetContent").scrollTop();
        });
    }
}

function PostFeedWidgetComment(feedType, feedObjectId, sender, e) {
    var enterKey = 13;

    if (e.which == enterKey) {
        var textBoxValue = $("#" + sender.id).val().replace(/\"/g, '\\\"');

        if (textBoxValue != "") {
            var idParentMessage = sender.id.replace("FeedWidget_MessageCommentField_" + feedType + "_", "");

            IsFeedWidgetLoading = true;

            $.ajax({
                type: "POST",
                url: "../_util/WidgetService.asmx/SaveFeedWidgetMessage",
                data: "{feedType: \"" + feedType + "\", feedObjectId: " + feedObjectId + ", message: \"" + textBoxValue + "\", idParentMessage: " + idParentMessage + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPostFeedWidgetCommentSuccess,
                failure: function (response) {
                    IsFeedWidgetLoading = false;
                    //alert(response.d);
                },
                error: function (response) {
                    IsFeedWidgetLoading = false;
                    //alert(response.d);
                }
            });

            $("#" + sender.id).val("");
        }
    }
}

function OnPostFeedWidgetCommentSuccess(response) {
    var responseObject = response.d;

    if (responseObject.html != "") {
        $(responseObject.html).hide().appendTo("#FeedWidget_MessageCommentsContainer_" + responseObject.feedType + "_" + responseObject.idParentMessage).fadeIn(1000);
    }
    else {
        $("#FeedWidget_HiddenButtonForPostedForModerationModal").click();
    }

    IsFeedWidgetLoading = false;
}

function DeleteFeedWidgetMessage(feedType, idMessage) {
    IsFeedWidgetLoading = true;

    var idParentMessage = null;

    $.ajax({
        type: "POST",
        url: "../_util/WidgetService.asmx/DeleteFeedWidgetMessage",
        data: "{feedType: \"" + feedType + "\", idMessage: " + idMessage + ", idParentMessage: " + idParentMessage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnDeleteFeedWidgetMessageSuccess,
        failure: function (response) {
            IsFeedWidgetLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            IsFeedWidgetLoading = false;
            //alert(response.d);
        }
    });
}

function OnDeleteFeedWidgetMessageSuccess(response) {
    var responseObject = response.d;

    $("#FeedWidget_MessageContainer_" + responseObject.feedType + "_" + responseObject.idMessage).remove();
    $("#FeedWidget_HiddenButtonForMessageDeletedModal").click();
    IsFeedWidgetLoading = false;
}

function DeleteFeedWidgetComment(feedType, idMessage, idParentMessage) {
    IsFeedWidgetLoading = true;

    $.ajax({
        type: "POST",
        url: "../_util/WidgetService.asmx/DeleteFeedWidgetMessage",
        data: "{feedType: \"" + feedType + "\", idMessage: " + idMessage + ", idParentMessage: " + idParentMessage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnDeleteFeedWidgetCommentSuccess,
        failure: function (response) {
            IsFeedWidgetLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            IsFeedWidgetLoading = false;
            //alert(response.d);
        }
    });
}

function OnDeleteFeedWidgetCommentSuccess(response) {
    var responseObject = response.d;

    $("#FeedWidget_MessageCommentContainer_" + responseObject.feedType + "_" + responseObject.idParentMessage + "_" + responseObject.idMessage).remove();
    $("#FeedWidget_HiddenButtonForMessageDeletedModal").click();
    IsFeedWidgetLoading = false;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////        WALL MODERATION WIDGET         /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

function GetWallModerationWidgetRecords() {
    IsWallModerationWidgetLoading = true;
    $("#WallModerationWidget_OlderMessagesLoadingPanel").show();

    $.ajax({
        type: "POST",
        url: "../_util/WidgetService.asmx/BuildWallModerationWidgetMessages",
        data: "{dtQuery: \"" + WallModerationWidgetLastRecord + "\", getMessagesNewerThanDtQuery: " + false + ", isInitialLoad: " + IsWallModerationWidgetInitialLoad + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnGetWallModerationWidgetRecordsSuccess,
        failure: function (response) {
            $("#WallModerationWidget_OlderMessagesLoadingPanel").hide();
            IsWallModerationWidgetLoading = false;
            IsWallModerationWidgetInitialLoad = false;
            //alert(response.d);
        },
        error: function (response) {
            $("#WallModerationWidget_OlderMessagesLoadingPanel").hide();
            IsWallModerationWidgetLoading = false;
            IsWallModerationWidgetInitialLoad = false;
            //alert(response.d);
        }
    });
}

function OnGetWallModerationWidgetRecordsSuccess(response) {
    var responseObject = response.d;

    $(responseObject.html).hide().appendTo("#WallModerationWidget_FeedMessagesContainer").fadeIn(1000);
    WallModerationWidgetLastRecord = responseObject.lastRecord;

    $("#WallModerationWidget_OlderMessagesLoadingPanel").hide();
    IsWallModerationWidgetLoading = false;
    IsWallModerationWidgetInitialLoad = false;

    //set the value in hidden field and then call the ShowHideAlertIcon method to show/hide alert icon
    if (responseObject.hasRecords) {
        $("#WallModerationWidgetHiddenForShowingAlertIcon").val("1");
    }
    else {
        $("#WallModerationWidgetHiddenForShowingAlertIcon").val("0");
    }
    ShowHideAlertIcon("WallModerationWidgetHiddenForShowingAlertIcon", "WallModerationWidget")
}

function OnWallModerationWidgetScroll() {
    if ($("#WallModerationWidgetContent") != null) {
        
        // prevent the window from scrolling when scrolling inside the WallModerationWidgetContent div
        PreventOuterScrolling("WallModerationWidgetContent");

        // grab more records when WallModerationWidgetContent div is scrolled to bottom
        $("#WallModerationWidgetContent").scroll(function () {
            if (WallModerationWidgetScrollPosition < $("#WallModerationWidgetContent").scrollTop()) {
                if ($("#WallModerationWidgetContent").scrollTop() + $("#WallModerationWidgetContent").innerHeight() >= $("#WallModerationWidgetContent")[0].scrollHeight) {
                    if (!IsWallModerationWidgetLoading) {
                        GetWallModerationWidgetRecords();
                    }
                }
            }
            WallModerationWidgetScrollPosition = $("#WallModerationWidgetContent").scrollTop();
        });
    }
}

function ApproveWallModerationWidgetMessage(feedType, idMessage) {
    IsWallModerationWidgetLoading = true;

    var idParentMessage = null;

    $.ajax({
        type: "POST",
        url: "../_util/WidgetService.asmx/ApproveWallModerationWidgetMessage",
        data: "{feedType: \"" + feedType + "\", idMessage: " + idMessage + ", idParentMessage: " + idParentMessage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnApproveWallModerationWidgetMessageSuccess,
        failure: function (response) {
            IsWallModerationWidgetLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            IsWallModerationWidgetLoading = false;
            //alert(response.d);
        }
    });
}

function OnApproveWallModerationWidgetMessageSuccess(response) {
    var responseObject = response.d;

    $("#WallModerationWidget_MessageContainer_" + responseObject.feedType + "_" + responseObject.idMessage).remove();
    $("#WallModerationWidget_HiddenButtonForMessageApprovedModal").click();
    IsWallModerationWidgetLoading = false;

    //checking if there are no records on wall moderation widget(means you have just approved the last message) then hide the alert icon and show "no record" message
    if ($("#WallModerationWidgetLoadedContent .MessageContainer").length == 0) {
        $("#WallModerationWidget_AlertIcon").css("display", "none");

        var noRecordsToDisplayContainer = "<div id=\"WallModerationWidget_NoMessagesMessageContainer\" class=\"centered\" style=\"\">" + NoMessages + "</div>"
        $("#WallModerationWidgetLoadedContent").append(noRecordsToDisplayContainer);
    }
}

function DeleteWallModerationWidgetMessage(feedType, idMessage) {
    IsWallModerationWidgetLoading = true;

    var idParentMessage = null;

    $.ajax({
        type: "POST",
        url: "../_util/WidgetService.asmx/DeleteWallModerationWidgetMessage",
        data: "{feedType: \"" + feedType + "\", idMessage: " + idMessage + ", idParentMessage: " + idParentMessage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnDeleteWallModerationWidgetMessageSuccess,
        failure: function (response) {
            IsWallModerationWidgetLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            IsWallModerationWidgetLoading = false;
            //alert(response.d);
        }
    });
}

function OnDeleteWallModerationWidgetMessageSuccess(response) {
    var responseObject = response.d;

    $("#WallModerationWidget_MessageContainer_" + responseObject.feedType + "_" + responseObject.idMessage).remove();
    $("#WallModerationWidget_HiddenButtonForMessageDeletedModal").click();
    IsWallModerationWidgetLoading = false;

    //checking if there are no records on wall moderation widget(means you have just deleted the last message) then hide the alert icon and show "no record" message
    if ($("#WallModerationWidgetLoadedContent .MessageContainer").length == 0) {
        $("#WallModerationWidget_AlertIcon").css("display", "none");
      
        var noRecordsToDisplayContainer = "<div id=\"WallModerationWidget_NoMessagesMessageContainer\" class=\"centered\" style=\"\">" + NoMessages + "</div>"
        $("#WallModerationWidgetLoadedContent").append(noRecordsToDisplayContainer);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////        TASK PROCTORING WIDGET         /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

function LoadTaskModalContent(idUser, idEnrollment, idDataLesson, idDataTask, idLesson, uploadedTaskFilename, taskUploadedDate) {
    // make sure dashboard items are not still loading before doing this, if this is allowed to execute while the page is in async postback, the modal doesn't load properly
    if (!Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
        $("#AdministrativeTasksWidget_TaskData").val(idUser + "|" + idEnrollment + "|" + idDataLesson + "|" + idDataTask + "|" + idLesson + "|" + uploadedTaskFilename + "|" + taskUploadedDate);

        $("#AdministrativeTasksWidget_HiddenButtonForTaskModal").click();
        $("#AdministrativeTasksWidget_TaskModalLoadButton").click();
    }
}

function ShowHideAlertIcon(hiddenFieldId, widgetControlId) {
    if ($("#" + hiddenFieldId).val() == "1" && $("#" + widgetControlId + "IconLI").hasClass("WidgetIconLIOn")) {
        $("#" + widgetControlId + "_AlertIcon").css("display", "inline-block");
    }
    else if ($("#" + hiddenFieldId).val() == "0") {
        $("#" + widgetControlId + "_AlertIcon").css("display", "none");
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////        TRANSCRIPT WIDGET         ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function OnTranscriptReportClick() {
    $("#TranscriptViewerModalHiddenLaunchButton").click();
}

///////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////        MOBILE WIDGET BAR TRANSFORM     //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function WidgetBarMoveLeftClick() {
    $("#WidgetBar ul li").not(".WidgetIconMobileLeftHide").not(".Navigator").first().addClass("WidgetIconMobileLeftHide");
    MobileWidgetBarAdjustment();
}

function WidgetBarMoveRightClick() {
    $("#WidgetBar ul li.WidgetIconMobileLeftHide").last().removeClass("WidgetIconMobileLeftHide");
    MobileWidgetBarAdjustment();
}

///////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////        MOBILE WIDGET BAR MAIN ADJUSTMENT METHOD   ///////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function MobileWidgetBarAdjustment() {
    $(".WidgetIconMobileRightHide").removeClass("WidgetIconMobileRightHide");
    $("#MoveRightIconLI").removeClass("MobileNavigatorHide");
    $("#MoveLeftIconLI").removeClass("MobileNavigatorHide");

    maxNumberAllowed = Math.ceil(($(document).width() - 200) / 40);

    if ($('.WidgetIcon').not('.Navigator').length > maxNumberAllowed)
    {
        shownNum = 0;
        for (i = 0; i < $('.WidgetIcon').length ; i++) {
            if ($("li[id$='WidgetIconLI']").not(".Navigator").eq(i).is(':visible')) {
                shownNum++;
            }
            if (shownNum > maxNumberAllowed) {
                $("li[id$='WidgetIconLI']").eq(i).addClass("WidgetIconMobileRightHide");
            }
        }
        if ($("li[id$='WidgetIconLI']:visible").not(".Navigator").length < maxNumberAllowed) {
            if ($(".WidgetIconMobileLefttHide").length > 0)
                $(".WidgetIconMobileLefttHide").last().removeClass("WidgetIconMobileLeftHide");
            else
                $(".WidgetIconMobileRightHide").first().removeClass("WidgetIconMobileRightHide");
        }
    }

    if ($(".WidgetIconMobileRightHide").length + $(".WidgetIconMobileLeftHide").length <= 2) {
        $(".WidgetIconMobileRightHide").removeClass("WidgetIconMobileRightHide");
        $(".WidgetIconMobileLefttHide").removeClass("WidgetIconMobileLeftHide");
        $("#MoveRightIconLI").addClass("MobileNavigatorHide");
        $("#MoveLeftIconLI").addClass("MobileNavigatorHide");
    }
    else {
        $("#MoveRightIconLI").removeClass("MoveDim");
        $("#MoveLeftIconLI").removeClass("MoveDim");

        if ($(".WidgetIconMobileRightHide").length == 0) {
            $("#MoveLeftIconLI").addClass("MoveDim");
        }

        if ($(".WidgetIconMobileLeftHide").length == 0) {
            $("#MoveRightIconLI").addClass("MoveDim");
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////    APPROVAL WIDGETS   ///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function UserRegistrationApproveRejectClick(idUser, action) {
    // set the hidden input values
    $("#ApproveRejectPendingUserRegistrationData").val(idUser + "|" + action);
    
    // clear the text box
    $("#ApproveRejectPendingUserRegistrationModal_RejectCommentsTextbox_Field").val("");    

    // launch the modal
    $("#ApproveRejectPendingUserRegistrationModalHiddenLaunchButton").click();

    // load the modal
    $("#ApproveRejectPendingUserRegistrationModalHiddenLoadButton").click();
}

function CourseEnrollmentApproveRejectClick(idEnrollmentRequest, action) {
    // set the hidden input values
    $("#ApproveRejectPendingCourseEnrollmentData").val(idEnrollmentRequest + "|" + action);

    // clear the text box
    $("#ApproveRejectPendingCourseEnrollmentModal_RejectCommentsTextbox_Field").val("");        

    // launch the modal
    $("#ApproveRejectPendingCourseEnrollmentModalHiddenLaunchButton").click();

    // load the modal
    $("#ApproveRejectPendingCourseEnrollmentModalHiddenLoadButton").click();
}

//////////////////////////////////////////////////////////////////////////////////////////////
///////////////////        CERTIFICATION TASK PROCTORING WIDGET         //////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

function LoadCertificationTaskModalContent(idUser, idCertificationToUserLink, idDataCertificationModuleRequirement, uploadedTaskFilename, taskUploadedDate) {
    // make sure dashboard items are not still loading before doing this, if this is allowed to execute while the page is in async postback, the modal doesn't load properly
    if (!Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
        $("#AdministrativeTasksWidget_CertificationTaskData").val(idUser + "|" + idCertificationToUserLink + "|" + idDataCertificationModuleRequirement + "|" + uploadedTaskFilename + "|" + taskUploadedDate);

        $("#AdministrativeTasksWidget_HiddenButtonForCertificationTaskModal").click();
        $("#AdministrativeTasksWidget_CertificationTaskModalLoadButton").click();
    }
}

/////////////////////////////////////////////////////////////////////////////
///////////////////        ILT SESSIONS WIDGET         //////////////////////
/////////////////////////////////////////////////////////////////////////////
function LoadILTSessionDetailsModal(idStandupTrainingInstance, idStandupTrainingInstanceToUserLink) {
    if (!Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
        $("#ILTSessionsData").val(idStandupTrainingInstance);

        var title = $("#ILTSessionsWidget_TitleLinkButton_" + idStandupTrainingInstanceToUserLink).text();

        $("#ILTSessionsTitle").val(title);

        $("#ILTSessionsModalModalPopupHeaderText").text(title);

        $("#ILTSessionsModalHiddenLaunchButton").click();

        $("#ILTSessionsModalHiddenLoadButton").click();
    }
    
}

// Function: CreateILTCalendarObject
function CreateILTCalendarObject(idSession, dtStart, dtEnd) {
    $("#ILTCalendarContent").val(idSession + "|" + dtStart + "|" + dtEnd);
    $("#HiddenILTDownloadCalendarFileButton").click();
}


