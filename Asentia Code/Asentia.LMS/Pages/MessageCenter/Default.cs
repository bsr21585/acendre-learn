﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Web.Services;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.MessageCenter
{
    public partial class Default : AsentiaAuthenticatedPage
    {
        #region Public Properties

        public Panel ObjectOptionsPanel;
        public Panel DataGridsContainer;
        public Panel DataGridsActionContainer;
        #endregion

        #region Private Properties
        private InboxMessage _InboxMessageObject;

        private ModalPopup _InboxGridConfirmActionModal;
        private ModalPopup _DraftsGridConfirmActionModal;
        private ModalPopup _SentItemsGridConfirmActionModal;
        private ModalPopup _ComposeMessageModalPopup;

        private TextBox _MessageTo = new TextBox();
        private TextBox _Subject = new TextBox();
        private TextBox _MessageBody = new TextBox();
        private TextBox _ReplyMessageBody = new TextBox();

        private HiddenField _IdInboxMessageHidden = new HiddenField();
        private HiddenField _IdToUserHiddenValue = new HiddenField();
        private HiddenField _ObjectTypeToUserHiddenValue = new HiddenField();
        private HiddenField _ToUserHiddenValue = new HiddenField();

        private Button _SendButton;
        private Button _SaveDraftButton;
        private Button _UpdateMessagePopupFieldsButton;
        private Button _TargetButtonForComposeModal;

        private Panel _MessageFormContainer = new Panel();
        private Panel _MessageCenterTabsPanelsContainer;
        private Panel _InboxActionPanel;
        private Panel _SentItemsActionPanel;
        private Panel _DraftsActionPanel;
        private Panel _InboxFeedbackMessagePanel;
        private Panel _SentItemFeedbackMessagePanel;
        private Panel _DraftFeedbackMessagePanel;

        private Grid _InboxGrid;
        private Grid _SentItemsGrid;
        private Grid _DraftsGrid;

        private UpdatePanel _DraftsGridUpdatePanel;
        private UpdatePanel _SentItemsGridUpdatePanel;
        private UpdatePanel _InboxGridUpdatePanel;

        private LinkButton _DeleteInboxMessageButton;
        private LinkButton _DeleteDraftsMessageButton;
        private LinkButton _DeleteSentItemsMessageButton;
        #endregion

        #region Page_Load
        /// <summary>
        /// Handles the Page Load Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check to ensure message center is enabled on the portal
            if (!AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.MESSAGECENTER_ENABLE) ?? false)
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/messagecenter/Default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MessageCenter));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.General, _GlobalResources.MessageCenter, ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL_CHECK,
                                                                                                                     ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            //build message center page properties
            this._BuildMessageCenterProperties();

            // build the grid, actions panel, and modal
            this._BuildGridsActionPanel();

            //add the input fields in compose message modal popup
            this._BuildInputControls();

            // Builds the modal for composing the message.
            this._BuildComposeMessageActionModal();

            //build compose new message button
            this._BuildObjectOptionsPanel();

            //Builds show items details controls
            this._BuildSentMeassgeDetailsActionModal();

            //Builds required controls for reply process
            this._BuildReplyProcessControls();

            // clear feedback message 
            this._ClearFeedbackMessagePanel();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this._InboxGrid.BindData();
                this._SentItemsGrid.BindData();
                this._DraftsGrid.BindData();
            }
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.MessageCenter.Default.js");

            csm.RegisterStartupScript(typeof(Enrollment), "JsVariables", "var ToUserHiddenValue='" + this._ToUserHiddenValue.ClientID
                + "';var UpdateMessagePopupFieldsButton ='" + this._UpdateMessagePopupFieldsButton.ClientID
                + "';var IdInboxMessageHidden ='" + this._IdInboxMessageHidden.ClientID
                + "';var TargetButtonForComposeModal ='" + this._TargetButtonForComposeModal.ClientID
                + "';var PopulateSentItemDetailsHidden ='" + this._PopulateSentItemDetailsHidden.ClientID
                + "';var TargetControlIdForviewSentItemDetailsModal ='" + this._TargetControlIdForViewSentItemDetailsModal.ClientID
                + "';var HiddenButtonForPopulatingSentItemDetailsModal ='" + this._HiddenButtonForPopulatingSentItemDetailsModal.ClientID
                + "';var PopulateInboxItemDetailsHidden ='" + this._PopulateInboxItemDetailsHidden.ClientID
                + "';var HiddenButtonForPopulatingInboxItemDetailsModal ='" + this._HiddenButtonForPopulatingInboxItemDetailsModal.ClientID
                + "';var TargetControlIdForViewInboxItemDetailsModal ='" + this._TargetControlIdForViewInboxItemDetailsModal.ClientID
                + "';var TargetControlIdforReplyModal ='" + this._TargetControlIdforReplyModal.ClientID
                + "';var ObjectTypeToUserHiddenValue ='" + this._ObjectTypeToUserHiddenValue.ClientID
                + "';var IdToUserHiddenValue ='" + this._IdToUserHiddenValue.ClientID + "';", true);
        }
        #endregion

        #region _BuildComposeMessageActionModal
        /// <summary>
        /// Build's compose message action modal.
        /// </summary>
        private void _BuildComposeMessageActionModal()
        {
            //hidden button used to open compose madal popup
            this._TargetButtonForComposeModal = new Button();
            this._TargetButtonForComposeModal.ID = "TargetButtonForComposeModal";
            this._TargetButtonForComposeModal.Attributes.Add("style", "display:none;");

            this.DataGridsActionContainer.Controls.Add(this._TargetButtonForComposeModal);
            this._BuildComposeMessageModal(this._TargetButtonForComposeModal.ID);
        }
        #endregion

        #region _BuildComposeMessageModal
        /// <summary>
        /// Builds the modal for composing the message.
        /// </summary>
        private void _BuildComposeMessageModal(string targetControlid)
        {
            this._ComposeMessageModalPopup = new ModalPopup("ComposeMessageModalPopup");

            // set modal properties
            this._ComposeMessageModalPopup.Type = ModalPopupType.Form;
            this._ComposeMessageModalPopup.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL_CHECK,
                                                                                   ImageFiles.EXT_PNG);
            this._ComposeMessageModalPopup.HeaderIconAlt = _GlobalResources.Compose;
            this._ComposeMessageModalPopup.HeaderText = _GlobalResources.Compose;

            this._ComposeMessageModalPopup.TargetControlID = targetControlid;
            this._ComposeMessageModalPopup.ReloadPageOnClose = false;
            this._ComposeMessageModalPopup.SubmitButton.Visible = false;
            this._ComposeMessageModalPopup.CloseButton.Visible = false;
            this._ComposeMessageModalPopup.DisableSubmitButton();

            this._UpdateMessagePopupFieldsButton = new Button();
            this._UpdateMessagePopupFieldsButton.ID = "InboxMessageButton";
            this._UpdateMessagePopupFieldsButton.Attributes.Add("style", "display:none;");
            this._UpdateMessagePopupFieldsButton.Click += this._UpdateMessagePopupFieldsButton_Click;

            Panel composeMailActionsPanel = new Panel();
            composeMailActionsPanel.ID = "ComposeMailActionsPanel";

            this._SaveDraftButton = new Button();
            this._SaveDraftButton.Text = _GlobalResources.SaveDraft;
            this._SaveDraftButton.Command += new CommandEventHandler(this._SaveDraftButton_Command);
            this._SaveDraftButton.CssClass = "Button ActionButton";
            composeMailActionsPanel.Controls.Add(this._SaveDraftButton);

            this._SendButton = new Button();
            this._SendButton.Text = _GlobalResources.Send;
            this._SendButton.Command += new CommandEventHandler(this._SendButton_Command);
            this._SendButton.CssClass = "Button ActionButton";
            composeMailActionsPanel.Controls.Add(this._SendButton);

            this._IdInboxMessageHidden.ID = "IdInboxMessageHidden";
            this._IdToUserHiddenValue.ID = "IdToUserHiddenValue";
            this._ObjectTypeToUserHiddenValue.ID = "ObjectTypeToUserHiddenValue";
            this._ToUserHiddenValue.ID = "ToUserHiddenValue";

            Panel innerBodyWrapper = new Panel();
            innerBodyWrapper.ID = "InnerBodyWrapper";

            innerBodyWrapper.Controls.Add(this._UpdateMessagePopupFieldsButton);
            innerBodyWrapper.Controls.Add(this._MessageFormContainer);
            innerBodyWrapper.Controls.Add(composeMailActionsPanel);
            innerBodyWrapper.Controls.Add(this._IdInboxMessageHidden);
            innerBodyWrapper.Controls.Add(this._IdToUserHiddenValue);
            innerBodyWrapper.Controls.Add(this._ObjectTypeToUserHiddenValue);
            innerBodyWrapper.Controls.Add(this._ToUserHiddenValue);

            // add controls to body
            this._ComposeMessageModalPopup.AddControlToBody(innerBodyWrapper);

            this.DataGridsContainer.Controls.Add(this._ComposeMessageModalPopup);
        }
        #endregion

        #region _UpdateMessagePopupFieldsButton_Click
        /// <summary>
        /// Update/Reset input field of message popup
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void _UpdateMessagePopupFieldsButton_Click(object sender, EventArgs e)
        {
            this._MessageFormContainer.Controls.Clear();
            this._ComposeMessageModalPopup.ClearFeedback();
            this._EnableOrDisableTextBox(false, false, true);

            this._BuildInputControls(Convert.ToInt32(this._IdInboxMessageHidden.Value));
        }
        #endregion

        #region _SaveDraftButton_Command
        /// <summary>
        /// Saves the Drafts into the Database (but does not 'Send' the message)
        /// </summary>
        private void _SaveDraftButton_Command(object sender, EventArgs e)
        {
            try
            {
                AsentiaPage.ClearErrorClassFromFieldsRecursive(this);

                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                string recipientText = this._MessageTo.Text.ToString();

                // created dtGroupUserList datatable to fill all user in group 
                DataTable dtGroupUserList = new DataTable();

                // check the message receptent is group or individual
                if (this._ObjectTypeToUserHiddenValue.Value == "group")
                {
                    Group groupObject = new Group(Convert.ToInt32(this._IdToUserHiddenValue.Value));

                    // get all group members from the group id
                    dtGroupUserList = groupObject.GetUsers(false, null);
                }

                int idInboxMessage = 0;

                if (dtGroupUserList.Rows.Count > 0)
                {
                    // save draft mail to every group memeber through the userid
                    foreach (DataRow drGroupUser in dtGroupUserList.Rows)
                    {
                        this._InboxMessageObject = new InboxMessage();
                        this._InboxMessageObject.IdParentInboxMessage = null;
                        this._InboxMessageObject.IdRecipient = Convert.ToInt32(drGroupUser.ItemArray[0]);
                        this._InboxMessageObject.Subject = this._Subject.Text;
                        this._InboxMessageObject.Message = this._MessageBody.Text;
                        this._InboxMessageObject.IsDraft = true;
                        this._InboxMessageObject.IsSent = false;
                        this._InboxMessageObject.IsRead = false;
                        this._InboxMessageObject.IsRecipientDeleted = false;
                        this._InboxMessageObject.IsSenderDeleted = false;

                        // logged in user is the sender of the message
                        this._InboxMessageObject.IdSender = AsentiaSessionState.IdSiteUser;

                        // save draft messages
                        idInboxMessage = this._InboxMessageObject.Save();
                    }
                }
                else
                {
                    // if sending the saved draft message then populate through hidden field value
                    if (String.IsNullOrWhiteSpace(this._IdInboxMessageHidden.Value) || Convert.ToInt32(this._IdInboxMessageHidden.Value) == 0)
                    {
                        this._InboxMessageObject = new InboxMessage();
                    }
                    else
                    {
                        this._InboxMessageObject = InboxMessage._Details(Convert.ToInt32(this._IdInboxMessageHidden.Value), InboxMessage.MessageTypes.Draft);
                    }

                    this._InboxMessageObject.IdParentInboxMessage = null;
                    this._InboxMessageObject.IdRecipient = Convert.ToInt32(this._IdToUserHiddenValue.Value); ;
                    this._InboxMessageObject.Subject = this._Subject.Text;
                    this._InboxMessageObject.Message = this._MessageBody.Text;
                    this._InboxMessageObject.IsDraft = true;
                    this._InboxMessageObject.IsSent = false;
                    this._InboxMessageObject.IsRead = false;
                    this._InboxMessageObject.IsRecipientDeleted = false;
                    this._InboxMessageObject.IsSenderDeleted = false;

                    // logged in user is the sender of the message
                    this._InboxMessageObject.IdSender = AsentiaSessionState.IdSiteUser;

                    // save draft message
                    idInboxMessage = this._InboxMessageObject.Save();
                }

                if (idInboxMessage > 0)
                {
                    this._EnableOrDisableTextBox(true, false, false);

                    this._IdInboxMessageHidden.Value = Convert.ToString(idInboxMessage);
                    this._ComposeMessageModalPopup.DisplayFeedback(_GlobalResources.MessageHasBeenSavedSuccessfully, false);
                }
                else
                {
                    this._ComposeMessageModalPopup.DisplayFeedback(_GlobalResources.UnableToSendMessagePleaseTryAgain, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {
                // bind draft grid
                this._DraftsGrid.BindData();
                this._DraftsGridUpdatePanel.Update();
            }
        }

        #endregion

        #region _SendButton_Command
        /// <summary>
        /// Actually Sends the message to the recipient
        /// </summary>
        private void _SendButton_Command(object sender, EventArgs e)
        {
            try
            {
                AsentiaPage.ClearErrorClassFromFieldsRecursive(this);

                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                string recipientText = this._MessageTo.Text.ToString();

                // created dtGroupUserList datatable to fill all user in group 
                DataTable dtGroupUserList = new DataTable();

                if (this._ObjectTypeToUserHiddenValue.Value == "group")
                {
                    Group groupObject = new Group(Convert.ToInt32(this._IdToUserHiddenValue.Value));

                    // get all group members from the group id
                    dtGroupUserList = groupObject.GetUsers(false, null);
                }

                int idInboxMessage = 0;

                // check the message receptent is group or individual
                if (dtGroupUserList.Rows.Count > 0)
                {
                    // send mail to every group memeber through the userid
                    foreach (DataRow drGroupUser in dtGroupUserList.Rows)
                    {
                        idInboxMessage = 0;
                        this._InboxMessageObject = new InboxMessage();
                        this._InboxMessageObject.IdParentInboxMessage = null;
                        this._InboxMessageObject.IdRecipient = Convert.ToInt32(drGroupUser.ItemArray[0]);
                        this._InboxMessageObject.Subject = this._Subject.Text;
                        this._InboxMessageObject.Message = this._MessageBody.Text;
                        this._InboxMessageObject.IsDraft = false;
                        this._InboxMessageObject.IsSent = false;
                        this._InboxMessageObject.IsRead = false;
                        this._InboxMessageObject.IsRecipientDeleted = false;
                        this._InboxMessageObject.IsSenderDeleted = false;

                        // logged in user is the sender of the message
                        this._InboxMessageObject.IdSender = AsentiaSessionState.IdSiteUser;

                        // save message
                        idInboxMessage = this._InboxMessageObject.Save();

                        // send saved message
                        this._InboxMessageObject.Send(idInboxMessage);
                    }
                }
                else
                {
                    // if sending the saved draft message then populate through hidden field vale
                    if (String.IsNullOrWhiteSpace(this._IdInboxMessageHidden.Value) || Convert.ToInt32(this._IdInboxMessageHidden.Value) == 0)
                    {
                        this._InboxMessageObject = new InboxMessage();
                    }
                    else
                    {
                        this._InboxMessageObject = InboxMessage._Details(Convert.ToInt32(this._IdInboxMessageHidden.Value), InboxMessage.MessageTypes.Draft);
                    }

                    this._InboxMessageObject.IdParentInboxMessage = null;
                    this._InboxMessageObject.IdRecipient = Convert.ToInt32(this._IdToUserHiddenValue.Value);
                    this._InboxMessageObject.Subject = this._Subject.Text;
                    this._InboxMessageObject.Message = this._MessageBody.Text;
                    this._InboxMessageObject.IsDraft = false;
                    this._InboxMessageObject.IsSent = false;
                    this._InboxMessageObject.IsRead = false;
                    this._InboxMessageObject.IsRecipientDeleted = false;
                    this._InboxMessageObject.IsSenderDeleted = false;

                    // logged in user is the sender of the message
                    this._InboxMessageObject.IdSender = AsentiaSessionState.IdSiteUser;

                    // save message
                    idInboxMessage = this._InboxMessageObject.Save();

                    // send saved message
                    this._InboxMessageObject.Send(idInboxMessage);
                }

                if (idInboxMessage > 0)
                {
                    this._EnableOrDisableTextBox(true, true, true);
                    this._ComposeMessageModalPopup.DisplayFeedback(_GlobalResources.MessageHasBeenSentSuccessfully, false);
                }
                else
                {
                    this._ComposeMessageModalPopup.DisplayFeedback(_GlobalResources.UnableToSendMessagePleaseTryAgain, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {
                if (this._InboxMessageObject != null && this._InboxMessageObject.IdRecipient == AsentiaSessionState.IdSiteUser)
                {
                    // bind inbox grid after sending message to self
                    this._InboxGrid.BindData();
                    this._InboxGridUpdatePanel.Update();
                }

                // bind draft grid after draft sending
                this._DraftsGrid.BindData();
                this._DraftsGridUpdatePanel.Update();
                this._SentItemsGrid.BindData();
                this._SentItemsGridUpdatePanel.Update();
            }
        }
        #endregion

        #region _EnableOrDisableTextBox
        /// <summary>
        /// Method for enabling/disabling the input controls and action buttons.
        /// </summary>
        /// <param name="toDisable">toDisable</param>
        /// <param name="isSendButtonToHide">isSendButtonToHide</param>
        /// <param name="cleraHiddenFields">cleraHiddenFields</param>
        private void _EnableOrDisableTextBox(bool toDisable, bool isSendButtonToHide, bool cleraHiddenFields)
        {
            this._MessageTo.Enabled = !toDisable;
            this._Subject.Enabled = !toDisable;
            this._MessageBody.Enabled = !toDisable;

            if (toDisable)
            {
                if (isSendButtonToHide)
                {
                    this._SaveDraftButton.Enabled = false;
                    this._SaveDraftButton.CssClass = "Button ActionButton DisabledButton";
                    this._SendButton.Enabled = false;
                    this._SendButton.CssClass = "Button ActionButton DisabledButton";
                }
                else
                {
                    this._SaveDraftButton.Enabled = false;
                    this._SaveDraftButton.CssClass = "Button ActionButton DisabledButton";
                    this._SendButton.Enabled = true;
                    this._SendButton.CssClass = "Button ActionButton";
                }
            }
            else
            {
                this._MessageTo.Text = string.Empty;
                this._Subject.Text = string.Empty;
                this._MessageBody.Text = string.Empty;

                this._SaveDraftButton.Enabled = true;
                this._SaveDraftButton.CssClass = "Button ActionButton";
                this._SendButton.Enabled = true;
                this._SendButton.CssClass = "Button ActionButton";
            }

            if (cleraHiddenFields)
            {
                this._IdToUserHiddenValue.Value = "0";
                this._ObjectTypeToUserHiddenValue.Value = string.Empty;
                this._ToUserHiddenValue.Value = string.Empty;
            }
        }

        #endregion

        #region _EnableOrDisable_ReplyTextBox
        /// <summary>
        /// Method for enabling/disabling the Reply input controls and action buttons.
        /// </summary>
        /// <param name="toDisable">toDisable</param>
        /// <param name="isSendButtonToHide">isSendButtonToHide</param>
        /// <param name="cleraHiddenFields">cleraHiddenFields</param>
        private void _EnableOrDisableReplyTextBox(bool toDisable, bool isSendButtonToHide, bool cleraHiddenFields)
        {
            if (toDisable)
            {
                if (isSendButtonToHide)
                {
                    this._ReplySaveDraftButton.Enabled = false;
                    this._ReplySaveDraftButton.CssClass = "Button ActionButton DisabledButton";
                    this._ReplySendButton.Enabled = false;
                    this._ReplySendButton.CssClass = "Button ActionButton DisabledButton";
                    this._InboxReplyButton.Enabled = false;
                    this._InboxReplyButton.CssClass = "Button ActionButton DisabledButton";
                }
                else
                {
                    this._ReplySendButton.Enabled = true;
                    this._ReplySendButton.CssClass = "Button ActionButton";
                    this._ReplySaveDraftButton.Enabled = true;
                    this._ReplySaveDraftButton.CssClass = "Button ActionButton";
                    this._ReplyMessageBody.Text = string.Empty;
                }
            }
            else
            {
                this._ReplySaveDraftButton.Enabled = true;
                this._ReplySaveDraftButton.CssClass = "Button ActionButton";
                this._ReplySendButton.Enabled = false;
                this._ReplySendButton.CssClass = "Button ActionButton DisabledButton";
                this._InboxReplyButton.Enabled = false;
                this._InboxReplyButton.CssClass = "Button ActionButton DisabledButton";
            }

            if (cleraHiddenFields)
            {
                this._IdToUserHiddenValue.Value = "0";
                this._ObjectTypeToUserHiddenValue.Value = string.Empty;
                this._ToUserHiddenValue.Value = string.Empty;
            }
        }

        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            
            bool isValid = true;

            // To Field
            if ((String.IsNullOrWhiteSpace(this._MessageTo.Text))
                || (String.IsNullOrWhiteSpace(this._IdToUserHiddenValue.Value) || this._IdToUserHiddenValue.Value == "0")
                || (String.IsNullOrWhiteSpace(this._ToUserHiddenValue.Value) || this._MessageTo.Text != this._ToUserHiddenValue.Value))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._MessageFormContainer, "ComposeMessageTo", _GlobalResources.To + " " + _GlobalResources.IsRequired);
            }

            // Subject Field
            if (String.IsNullOrWhiteSpace(this._Subject.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._MessageFormContainer, "ComposeMessageSubject", _GlobalResources.Subject + " " + _GlobalResources.IsRequired);
            }

            // Body Field
            if (String.IsNullOrWhiteSpace(this._MessageBody.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._MessageFormContainer, "ComposeMessageBody", _GlobalResources.Message + " " + _GlobalResources.IsRequired);
            }
            return isValid;
        }
        #endregion

        #region _BuildInputControls
        /// <summary>
        /// Method to add the input fields in compose message modal popup
        /// </summary>
        /// <param name="idInboxMessage"></param>
        private void _BuildInputControls(int idInboxMessage = 0)
        {
            if (idInboxMessage != 0)
            {
                this._InboxMessageObject = InboxMessage._Details(idInboxMessage, InboxMessage.MessageTypes.Draft);
                this._IdToUserHiddenValue.Value = Convert.ToString(this._InboxMessageObject.IdRecipient);
                this._ToUserHiddenValue.Value = this._InboxMessageObject.RecipientName;
                this._ObjectTypeToUserHiddenValue.Value = "user";
            }

            #region Message To
            this._MessageFormContainer.ID = "messageFormContainer";

            // Main Field Container
            Panel messageToFieldContainer = new Panel();
            messageToFieldContainer.ID = "ComposeMessageTo";
            messageToFieldContainer.CssClass = "FormFieldContainer";

            //Input Field
            this._MessageTo = new TextBox();
            this._MessageTo.ID = "ComposeMessageTo" + "_Field";
            this._MessageTo.Text = this._InboxMessageObject != null ? this._InboxMessageObject.RecipientName : "";
            this._MessageTo.CssClass = "InputLong";
            this._MessageTo.Attributes.Add("onkeyup", "RecipientAutoComplete(this.value);");

            this._MessageFormContainer.Controls.Add(AsentiaPage.BuildFormField("ComposeMessageTo",
                                                                                _GlobalResources.To,
                                                                                this._MessageTo.ID,
                                                                                this._MessageTo,
                                                                                true,
                                                                                true,
                                                                                false));

            // if draft already save form inbox reply then modal popup message-to field will desable other wise it is enable
            if (idInboxMessage != 0 && this._InboxMessageObject.IdParentInboxMessage != null)
            {
                this._MessageTo.Enabled = false;
            }
            else
            {
                this._MessageTo.Enabled = true;
            }
            #endregion Message To

            #region Recipient AutoComplete

            Panel RecipientAutoCompleteContainer = new Panel();
            RecipientAutoCompleteContainer.ID = "RecipientAutoCompleteContainer";
            RecipientAutoCompleteContainer.CssClass = "RecipientAutoCompleteContainer";
            RecipientAutoCompleteContainer.Attributes.Add("hidden", "true");

            this._MessageFormContainer.Controls.Add(RecipientAutoCompleteContainer);

            #endregion Recipient AutoComplete

            #region Message Subject

            // Main Field Container
            Panel messageSubjectFieldContainer = new Panel();
            messageSubjectFieldContainer.ID = "ComposeMessageSubject";
            messageSubjectFieldContainer.CssClass = "FormFieldContainer";

            //Input Field
            this._Subject = new TextBox();
            this._Subject.ID = "ComposeMessageSubject" + "_Field";
            this._Subject.Text = this._InboxMessageObject != null ? this._InboxMessageObject.Subject : "";
            this._Subject.CssClass = "InputLong";
            this._MessageFormContainer.Controls.Add(AsentiaPage.BuildFormField("ComposeMessageSubject",
                                                                               _GlobalResources.Subject,
                                                                               this._Subject.ID,
                                                                               this._Subject,
                                                                               true,
                                                                               true,
                                                                               false));


            // if draft already save form inbox reply then modal popup message-to field will desable other wise it is enable
            if (idInboxMessage != 0 && this._InboxMessageObject.IdParentInboxMessage != null)
            {
                this._Subject.Enabled = false;
            }
            else
            {
                this._Subject.Enabled = true;
            }
            #endregion Message Subject

            #region Message Body

            // Main Field Container
            Panel messageBodyFieldContainer = new Panel();
            messageBodyFieldContainer.ID = "ComposeMessageBody";
            messageBodyFieldContainer.CssClass = "FormFieldContainer";

            //Input Field
            this._MessageBody = new TextBox();
            this._MessageBody.ID = "ComposeMessageBody" + "_Field";
            this._MessageBody.Text = this._InboxMessageObject != null ? this._InboxMessageObject.Message : "";
            this._MessageBody.Columns = 80;
            this._MessageBody.Rows = 5;
            this._MessageBody.TextMode = TextBoxMode.MultiLine;
            this._MessageFormContainer.Controls.Add(AsentiaPage.BuildFormField("ComposeMessageBody",
                                                                               _GlobalResources.Message,
                                                                                this._MessageBody.ID,
                                                                                this._MessageBody,
                                                                                true,
                                                                                true,
                                                                                false));


            #endregion Message Body
        }
        #endregion

        #region _DeleteButton_Inbox_Command
        /// <summary>
        /// Performs the delete action on Inbox Grid.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Inbox_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._InboxGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._InboxGrid.Rows[i].FindControl(this._InboxGrid.ID + "_GridSelectRecord_" + i);
                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.InboxMessage.DeleteInbox(recordsToDelete);

                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this._InboxFeedbackMessagePanel, _GlobalResources.TheSelectedMessage_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedbackInSpecifiedContainer(this._InboxFeedbackMessagePanel, _GlobalResources.NoMessage_sSelectedForDeletion, true);

                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._InboxFeedbackMessagePanel, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._InboxFeedbackMessagePanel, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._InboxFeedbackMessagePanel, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._InboxFeedbackMessagePanel, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._InboxFeedbackMessagePanel, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this._InboxGrid.BindData();
                this._InboxGridUpdatePanel.Update();
            }
        }
        #endregion

        #region _DeleteButton_Drafts_Command
        /// <summary>
        /// Performs the delete action on Drafts Grid .
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Drafts_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._DraftsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._DraftsGrid.Rows[i].FindControl(this._DraftsGrid.ID + "_GridSelectRecord_" + i);
                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.InboxMessage.DeleteDraft(recordsToDelete);

                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this._DraftFeedbackMessagePanel, _GlobalResources.TheSelectedMessage_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedbackInSpecifiedContainer(this._DraftFeedbackMessagePanel, _GlobalResources.NoMessage_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._DraftFeedbackMessagePanel, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._DraftFeedbackMessagePanel, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._DraftFeedbackMessagePanel, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._DraftFeedbackMessagePanel, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._DraftFeedbackMessagePanel, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this._DraftsGrid.BindData();
            }
        }
        #endregion

        #region _DeleteButton_SentItems_Command
        /// <summary>
        /// Performs the delete action on SentItems Grid .
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_SentItems_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._SentItemsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._SentItemsGrid.Rows[i].FindControl(this._SentItemsGrid.ID + "_GridSelectRecord_" + i);
                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.InboxMessage.DeleteOutgoing(recordsToDelete);

                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this._SentItemFeedbackMessagePanel, _GlobalResources.TheSelectedMessage_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    this.DisplayFeedbackInSpecifiedContainer(this._SentItemFeedbackMessagePanel, _GlobalResources.NoMessage_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._SentItemFeedbackMessagePanel, dnfEx.Message, false);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._SentItemFeedbackMessagePanel, fnuEx.Message, false);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._SentItemFeedbackMessagePanel, cpeEx.Message, false);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._SentItemFeedbackMessagePanel, dEx.Message, false);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this._SentItemFeedbackMessagePanel, ex.Message, false);
            }
            finally
            {
                // rebind the grid
                this._SentItemsGrid.BindData();
            }
        }
        #endregion

        #region _BuildObjectOptionsPanel
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // COMPOSE MESSAGE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("ComposeMessageLink",
                                                null,
                                               "javascript: void(0);",
                                               "OpenComposeModal(0);return false;",
                                                _GlobalResources.Compose,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL_CHECK, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }

        #endregion

        #region _BuildMessageCenterProperties
        /// <summary>
        /// Build's message center properties.
        /// </summary>
        private void _BuildMessageCenterProperties()
        {
            // build the message center properties form tabs
            this._BuildMessageCenterPropertiesTabs();

            this._MessageCenterTabsPanelsContainer = new Panel();
            this._MessageCenterTabsPanelsContainer.ID = "MessageCenter_TabPanelsContainer";
            this._MessageCenterTabsPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.DataGridsContainer.Controls.Add(this._MessageCenterTabsPanelsContainer);

            //build Inbox,SentItems and Drafts data grida
            this._BuildInboxGrid();
            this._BuildSentItemsGrid();
            this._BuildDraftsGrid();
        }
        #endregion

        #region _BuildMessageCenterPropertiesTabs
        /// <summary>
        /// build the message tabs on the update panel
        /// </summary>
        private void _BuildMessageCenterPropertiesTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Inbox", _GlobalResources.Inbox));
            tabs.Enqueue(new KeyValuePair<string, string>("SentItems", _GlobalResources.SentItems));
            tabs.Enqueue(new KeyValuePair<string, string>("Drafts", _GlobalResources.Drafts));

            // build and attach the tabs
            this.DataGridsContainer.Controls.Add(AsentiaPage.BuildTabListPanel("MessageCenter", tabs));
        }
        #endregion

        #region _BuildInboxGrid
        /// <summary>
        /// build's inbox grid.
        /// </summary>
        private void _BuildInboxGrid()
        {
            //Inbox data grid container
            Panel inboxGridContainer = new Panel();
            inboxGridContainer.ID = "MessageCenter_" + "Inbox" + "_TabPanel";
            inboxGridContainer.CssClass = "TabPanelContainer";
            inboxGridContainer.Attributes.Add("style", "display: block;");

            //UPDATE PANEL
            this._InboxGridUpdatePanel = new UpdatePanel();
            this._InboxGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._InboxGridUpdatePanel.ID = "InboxGridUpdatepanel";

            #region Inbox feed back message panel

            this._InboxFeedbackMessagePanel = new Panel();
            this._InboxFeedbackMessagePanel.ID = "InboxFeedbackMessagePanel";

            #endregion

            // display the inbox Delete feedback
            this._InboxGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._InboxFeedbackMessagePanel);

            #region GRID
            this._InboxGrid = new Grid();

            this._InboxGrid.ShowSearchBox = true;
            this._InboxGrid.StoredProcedure = Library.InboxMessage.GridProcedure;
            this._InboxGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._InboxGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._InboxGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._InboxGrid.IdentifierField = "idInboxMessage";
            this._InboxGrid.DefaultSortColumn = "dtCreated";
            this._InboxGrid.IsDefaultSortDescending = true;
            this._InboxGrid.ShowTimeInDateStrings = true;

            // data key names
            this._InboxGrid.DataKeyNames = new string[] { "idInboxMessage" };

            // columns
            GridColumn displayName = new GridColumn(_GlobalResources.From, "from", "from");
            GridColumn subject = new GridColumn(_GlobalResources.Subject, "subject", "subject");
            GridColumn dtCreated = new GridColumn(_GlobalResources.Date, "dtCreated", "dtCreated");

            // add columns to data grid
            this._InboxGrid.AddColumn(displayName);
            this._InboxGrid.AddColumn(subject);
            this._InboxGrid.AddColumn(dtCreated);
            this._InboxGrid.RowDataBound += InboxGrid_RowDataBound;
            #endregion

            //add data Grid to Update panel           
            this._InboxGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._InboxGrid);

            #region Action button
            //add inbox grid action panel
            this._InboxActionPanel = new Panel();
            this._InboxActionPanel.ID = "InboxActionPanel";
            #endregion

            //add button to update panel
            this._InboxGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._InboxActionPanel);

            //Add inbox update panel
            inboxGridContainer.Controls.Add(this._InboxGridUpdatePanel);

            //add data Grid Update panel to container
            this._MessageCenterTabsPanelsContainer.Controls.Add(inboxGridContainer);
        }
        #endregion

        #region _BuildSentItemsGrid
        /// <summary>
        ///  build's sent items grid.
        /// </summary>
        private void _BuildSentItemsGrid()
        {
            //SentItems data grid container
            Panel sentItemsGridContainer = new Panel();
            sentItemsGridContainer.ID = "MessageCenter_" + "SentItems" + "_TabPanel";
            sentItemsGridContainer.CssClass = "TabPanelContainer";
            sentItemsGridContainer.Attributes.Add("style", "display: none;");

            //UPDATE PANEL
            this._SentItemsGridUpdatePanel = new UpdatePanel();
            this._SentItemsGridUpdatePanel.ID = "SentItemsGridUpdatePanel";
            this._SentItemsGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            sentItemsGridContainer.Controls.Add(this._SentItemsGridUpdatePanel);

            #region SentItem Feedback Message Panel

            this._SentItemFeedbackMessagePanel = new Panel();
            this._SentItemFeedbackMessagePanel.ID = "SentItemFeedbackMessagePanel";

            #endregion

            //add Sent Item  feedback message panel 
            this._SentItemsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._SentItemFeedbackMessagePanel);


            // GRID
            this._SentItemsGrid = new Grid();

            this._SentItemsGrid.ShowSearchBox = true;
            this._SentItemsGrid.StoredProcedure = Library.InboxMessage.SentItemsProcedure;
            this._SentItemsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._SentItemsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._SentItemsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._SentItemsGrid.IdentifierField = "idInboxMessage";
            this._SentItemsGrid.DefaultSortColumn = "dtCreated";
            this._SentItemsGrid.IsDefaultSortDescending = true;
            this._SentItemsGrid.ShowTimeInDateStrings = true;

            // data key names
            this._SentItemsGrid.DataKeyNames = new string[] { "idInboxMessage" };

            // columns
            GridColumn displayName = new GridColumn(_GlobalResources.To, "to", "to");
            GridColumn subject = new GridColumn(_GlobalResources.Subject, "subject", "subject");
            GridColumn dtCreated = new GridColumn(_GlobalResources.Date, "dtCreated", "dtCreated");

            // add columns to data grid
            this._SentItemsGrid.AddColumn(displayName);
            this._SentItemsGrid.AddColumn(subject);
            this._SentItemsGrid.AddColumn(dtCreated);
            this._SentItemsGrid.RowDataBound += SentGrid_RowDataBound;

            //add data Grid to Update panel
            this._SentItemsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._SentItemsGrid);

            //add data Grid Update panel to container
            this._MessageCenterTabsPanelsContainer.Controls.Add(sentItemsGridContainer);

            //add sent items grid action panel
            this._SentItemsActionPanel = new Panel();
            this._SentItemsActionPanel.ID = "SentItemsActionPanel";
            this._SentItemsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._SentItemsActionPanel);

        }

        #endregion

        #region _BuildDraftsGrid
        /// <summary>
        /// build's drafts grid.
        /// </summary>
        private void _BuildDraftsGrid()
        {
            //Drafts grid container
            Panel draftsGridContainer = new Panel();
            draftsGridContainer.ID = "MessageCenter_" + "Drafts" + "_TabPanel";
            draftsGridContainer.CssClass = "TabPanelContainer";
            draftsGridContainer.Attributes.Add("style", "display: none;");

            //UPDATE PANEL
            this._DraftsGridUpdatePanel = new UpdatePanel();
            this._DraftsGridUpdatePanel.ID = "DraftsGridUpdatePanel";
            this._DraftsGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            draftsGridContainer.Controls.Add(this._DraftsGridUpdatePanel);

            #region Draft Feedback Message Panel

            this._DraftFeedbackMessagePanel = new Panel();
            this._DraftFeedbackMessagePanel.ID = "DraftFeedbackMessagePanel";

            #endregion
            // add draft message panel
            this._DraftsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._DraftFeedbackMessagePanel);

            // GRID
            this._DraftsGrid = new Grid();

            this._DraftsGrid.ShowSearchBox = true;
            this._DraftsGrid.StoredProcedure = Library.InboxMessage.DraftsGridProcedure;
            this._DraftsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._DraftsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._DraftsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._DraftsGrid.IdentifierField = "idInboxMessage";
            this._DraftsGrid.DefaultSortColumn = "dtCreated";
            this._DraftsGrid.IsDefaultSortDescending = true;
            this._DraftsGrid.ShowTimeInDateStrings = true;

            // data key names
            this._DraftsGrid.DataKeyNames = new string[] { "idInboxMessage" };

            // columns
            GridColumn displayName = new GridColumn(_GlobalResources.To, "to", "to");
            GridColumn subject = new GridColumn(_GlobalResources.Subject, "subject", "subject");
            GridColumn dtCreated = new GridColumn(_GlobalResources.Date, "dtCreated", "dtCreated");

            // add columns to data grid
            this._DraftsGrid.AddColumn(displayName);
            this._DraftsGrid.AddColumn(subject);
            this._DraftsGrid.AddColumn(dtCreated);
            this._DraftsGrid.RowDataBound += DraftGrid_RowDataBound;

            //add data Grid to Update panel
            this._DraftsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._DraftsGrid);

            //add data Grid Update panel to container
            this._MessageCenterTabsPanelsContainer.Controls.Add(draftsGridContainer);

            //add Draft grid action panel
            this._DraftsActionPanel = new Panel();
            this._DraftsActionPanel.ID = "DraftsActionPanel";
            this._DraftsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._DraftsActionPanel);

        }
        #endregion

        #region Clear feedback message panel
        /// <summary>
        /// clear feedback message of message grid
        /// </summary>
        private void _ClearFeedbackMessagePanel()
        {
            this._DraftFeedbackMessagePanel.CssClass = string.Empty;
            this._SentItemFeedbackMessagePanel.CssClass = string.Empty;
            this._InboxFeedbackMessagePanel.CssClass = string.Empty;
        }

        #endregion

        #region Grids_RowDataBound
        /// <summary>
        /// Row data bound events of message, draft and sent message grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InboxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                bool isRead = false;

                if (DataBinder.GetPropertyValue(e.Row.DataItem, "isRead") != null)
                { isRead = Convert.ToBoolean(DataBinder.GetPropertyValue(e.Row.DataItem, "isRead")); }

                if (!isRead)
                {
                    e.Row.Style.Add("font-weight", "700");
                }

                for (int i = 1; i < e.Row.Cells.Count; i++)
                {
                    int itemId = Convert.ToInt32(this._InboxGrid.DataKeys[e.Row.RowIndex].Values["idInboxMessage"]);
                    e.Row.Cells[i].Attributes["onclick"] = "ShowInboxItemDetails(" + Convert.ToString(itemId) + "); return false;";
                }
            }
        }

        protected void DraftGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int i = 1; i < e.Row.Cells.Count; i++)
                {
                    int itemId = Convert.ToInt32(this._DraftsGrid.DataKeys[e.Row.RowIndex].Values["idInboxMessage"]);
                    e.Row.Cells[i].Attributes["onclick"] = "OpenComposeModal(" + Convert.ToString(itemId) + ");return false;";
                }
            }
        }

        protected void SentGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int i = 1; i < e.Row.Cells.Count; i++)
                {
                    int itemId = Convert.ToInt32(this._SentItemsGrid.DataKeys[e.Row.RowIndex].Values["idInboxMessage"]);
                    e.Row.Cells[i].Attributes["onclick"] = "ShowSentItemDetails(" + Convert.ToString(itemId) + ");return false;";
                }
            }
        }
        #endregion

        #region _BuildGridsActionPanel
        /// <summary>
        /// Build's grids action panel.
        /// </summary>
        private void _BuildGridsActionPanel()
        {
            //Build's grids action panel.
            this._BuildInboxActionsPanel();
            this._BuildSentItemsActionsPanel();
            this._BuildDraftsActionsPanel();

            //Builds grid action confirmation modals
            this._BuildInboxGridConfirmActionModal();
            this._BuildSentItemsGridActionsModal();
            this._BuildDraftsGridConfirmActionModal();
        }
        #endregion

        #region _BuildInboxActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Inbox Grid data.
        /// </summary>
        private void _BuildInboxActionsPanel()
        {
            this._InboxActionPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteInboxMessageButton = new LinkButton();
            this._DeleteInboxMessageButton.ID = "DeleteInboxMessageButton";
            this._DeleteInboxMessageButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "InboxGridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteInboxMessageButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedMessage_s;
            this._DeleteInboxMessageButton.Controls.Add(deleteText);

            // add delete button to panel
            this._InboxActionPanel.Controls.Add(this._DeleteInboxMessageButton);
        }
        #endregion

        #region _BuildSentItemsActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Sentitems Grid data.
        /// </summary>
        private void _BuildSentItemsActionsPanel()
        {
            this._SentItemsActionPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteSentItemsMessageButton = new LinkButton();
            this._DeleteSentItemsMessageButton.ID = "DeleteSentItemsMessageButton";
            this._DeleteSentItemsMessageButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "SentItemsGridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteSentItemsMessageButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedMessage_s;
            this._DeleteSentItemsMessageButton.Controls.Add(deleteText);

            // add delete button to panel
            this._SentItemsActionPanel.Controls.Add(this._DeleteSentItemsMessageButton);
        }
        #endregion

        #region _BuildDraftsActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Drafts Grid data.
        /// </summary>
        private void _BuildDraftsActionsPanel()
        {
            this._DraftsActionPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteDraftsMessageButton = new LinkButton();
            this._DeleteDraftsMessageButton.ID = "DeleteDraftsMessageButton";
            this._DeleteDraftsMessageButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "DraftsGridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteDraftsMessageButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedMessage_s;
            this._DeleteDraftsMessageButton.Controls.Add(deleteText);

            // add delete button to panel
            this._DraftsActionPanel.Controls.Add(this._DeleteDraftsMessageButton);
        }
        #endregion

        #region _BuildInboxGridConfirmActionModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Inbox data grid.
        /// </summary>
        private void _BuildInboxGridConfirmActionModal()
        {
            this._InboxGridConfirmActionModal = new ModalPopup("InboxGridConfirmActionModal");

            // set modal properties
            this._InboxGridConfirmActionModal.Type = ModalPopupType.Confirm;
            this._InboxGridConfirmActionModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                            ImageFiles.EXT_PNG);
            this._InboxGridConfirmActionModal.HeaderIconAlt = _GlobalResources.Delete;
            this._InboxGridConfirmActionModal.HeaderText = _GlobalResources.DeleteSelectedMessage_s;
            this._InboxGridConfirmActionModal.TargetControlID = this._DeleteInboxMessageButton.ClientID;
            this._InboxGridConfirmActionModal.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Inbox_Command);

            // build the modal body
            HtmlGenericControl confirmationWrapper = new HtmlGenericControl("p");
            confirmationWrapper.ID = "InboxGridConfirmActionModalBody";

            Literal confirmationAlert = new Literal();
            confirmationAlert.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseMessage_s;

            confirmationWrapper.Controls.Add(confirmationAlert);

            // add controls to body
            this._InboxGridConfirmActionModal.AddControlToBody(confirmationWrapper);
            this._InboxGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._InboxGridConfirmActionModal);
        }
        #endregion

        #region _BuildDraftsGridConfirmActionModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on drafts data grid.
        /// </summary>
        private void _BuildDraftsGridConfirmActionModal()
        {
            this._DraftsGridConfirmActionModal = new ModalPopup("DraftsGridConfirmActionModal");

            // set modal properties
            this._DraftsGridConfirmActionModal.Type = ModalPopupType.Confirm;
            this._DraftsGridConfirmActionModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                            ImageFiles.EXT_PNG);
            this._DraftsGridConfirmActionModal.HeaderIconAlt = _GlobalResources.Delete;
            this._DraftsGridConfirmActionModal.HeaderText = _GlobalResources.DeleteSelectedMessage_s;
            this._DraftsGridConfirmActionModal.TargetControlID = this._DeleteDraftsMessageButton.ClientID;
            this._DraftsGridConfirmActionModal.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Drafts_Command);

            // build the modal body
            HtmlGenericControl confirmationWrapper = new HtmlGenericControl("p");
            confirmationWrapper.ID = "DraftsGridConfirmActionModalBody";

            Literal confirmationAlert = new Literal();
            confirmationAlert.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseMessage_s;

            confirmationWrapper.Controls.Add(confirmationAlert);

            // add controls to body
            this._DraftsGridConfirmActionModal.AddControlToBody(confirmationWrapper);
            this._DraftsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._DraftsGridConfirmActionModal);

        }
        #endregion

        #region _BuildSentItemsGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on sent items data grid.
        /// </summary>
        private void _BuildSentItemsGridActionsModal()
        {
            this._SentItemsGridConfirmActionModal = new ModalPopup("SentItemsGridConfirmActionModal");

            // set modal properties
            this._SentItemsGridConfirmActionModal.Type = ModalPopupType.Confirm;
            this._SentItemsGridConfirmActionModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                            ImageFiles.EXT_PNG);
            this._SentItemsGridConfirmActionModal.HeaderIconAlt = _GlobalResources.Delete;
            this._SentItemsGridConfirmActionModal.HeaderText = _GlobalResources.DeleteSelectedMessage_s;
            this._SentItemsGridConfirmActionModal.TargetControlID = this._DeleteSentItemsMessageButton.ClientID;
            this._SentItemsGridConfirmActionModal.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_SentItems_Command);

            // build the modal body
            HtmlGenericControl confirmationWrapper = new HtmlGenericControl("p");
            confirmationWrapper.ID = "SentItemsGridConfirmActionModalBody";

            Literal confirmationAlert = new Literal();
            confirmationAlert.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseMessage_s;

            confirmationWrapper.Controls.Add(confirmationAlert);

            // add controls to body
            this._SentItemsGridConfirmActionModal.AddControlToBody(confirmationWrapper);
            this._SentItemsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._SentItemsGridConfirmActionModal);
        }
        #endregion
    }

    /// <summary>
    /// Partial call resposible for showing details of sent message item, when you click on a row of sent items grid
    /// </summary>
    public partial class Default : AsentiaAuthenticatedPage
    {
        #region Private Properties

        private ModalPopup _SentMessageDetailsModal;

        private LinkButton _TargetControlIdForViewSentItemDetailsModal;

        private HiddenField _PopulateSentItemDetailsHidden;

        private Button _TargetControlIdforReplyModal;
        private Button _HiddenButtonForPopulatingSentItemDetailsModal;

        private InboxMessage _SentMessageObject;

        private Label _MessageToLabel;
        private Label _MessageSubjectLabel;
        private Label _MessageBodyLabel;

        #endregion

        #region _GetSentMessageObject
        /// <summary>
        /// Method to get the message object 
        /// </summary>
        private void _GetSentMessageObject(int messgeId)
        {
            this._SentMessageObject = InboxMessage._Details(messgeId, InboxMessage.MessageTypes.Sent);
        }
        #endregion

        #region _BuildSentMeassgeDetailsActionModal
        /// <summary>
        /// Build's sent meassge details action modal.
        /// </summary>
        private void _BuildSentMeassgeDetailsActionModal()
        {
            //button used to open SentMessageDetailsModal using javscript
            this._TargetControlIdForViewSentItemDetailsModal = new LinkButton();
            this._TargetControlIdForViewSentItemDetailsModal.ID = "TargetControlIdForviewSentItemDetailsModal";
            this._TargetControlIdForViewSentItemDetailsModal.Attributes.Add("style", "display:none;");

            this._PopulateSentItemDetailsHidden = new HiddenField();
            this._PopulateSentItemDetailsHidden.ID = "PopulateSentItemDetailsHidden";

            this.DataGridsActionContainer.Controls.Add(this._PopulateSentItemDetailsHidden);
            this.DataGridsActionContainer.Controls.Add(this._TargetControlIdForViewSentItemDetailsModal);

            this._BuildSentMessageDetailsModal(this._TargetControlIdForViewSentItemDetailsModal.ID);
        }
        #endregion

        #region _BuildSentMessageDetailsModal
        /// <summary>
        /// Build's sent message details modal.
        /// </summary>
        /// <param name="targetControlId">The target control identifier.</param>
        private void _BuildSentMessageDetailsModal(string targetControlId)
        {
            this._SentMessageDetailsModal = new ModalPopup("SentMessageDetailsModal");

            // set modal properties
            this._SentMessageDetailsModal.Type = ModalPopupType.Form;
            this._SentMessageDetailsModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL_CHECK,
                                                                                  ImageFiles.EXT_PNG);
            this._SentMessageDetailsModal.HeaderIconAlt = _GlobalResources.SentItems;
            this._SentMessageDetailsModal.HeaderText = _GlobalResources.SentItems;

            this._SentMessageDetailsModal.TargetControlID = targetControlId;
            this._SentMessageDetailsModal.ReloadPageOnClose = false;
            this._SentMessageDetailsModal.SubmitButton.Visible = false;
            this._SentMessageDetailsModal.CloseButtonTextType = ModalPopupButtonText.Ok;

            //adding hidden button for populating input controls        
            this._HiddenButtonForPopulatingSentItemDetailsModal = new Button();
            this._HiddenButtonForPopulatingSentItemDetailsModal.ID = "HiddenButtonForPopulatingSentItemDetailsModal";
            this._HiddenButtonForPopulatingSentItemDetailsModal.Command += new CommandEventHandler(this._PopulateSentItemDetailsModalButton_Command);
            this._HiddenButtonForPopulatingSentItemDetailsModal.Style.Add("display", "none");

            // build the modal body
            // course title field
            Panel modalPropertiesContainer = new Panel();
            modalPropertiesContainer.ID = "ModalProperties_Container";

            #region Message To

            // Input Field
            this._MessageToLabel = new Label();
            this._MessageToLabel.ID = "MessageTo" + "_Field";

            //Field Label Container
            modalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("MessageTo",
                                                             _GlobalResources.To,
                                                              this._MessageToLabel.ID,
                                                              this._MessageToLabel,
                                                             false,
                                                             false,
                                                             false));
            #endregion

            #region Message Subject

            // Input Field
            this._MessageSubjectLabel = new Label();
            this._MessageSubjectLabel.ID = "MessageSubject" + "_Field";

            //Field Label Container
            modalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("MessageSubject",
                                                             _GlobalResources.Subject,
                                                               this._MessageSubjectLabel.ID,
                                                               this._MessageSubjectLabel,
                                                             false,
                                                             false,
                                                             false));
            #endregion Message Subject

            #region Message Body

            // Input Field
            this._MessageBodyLabel = new Label();
            this._MessageBodyLabel.ID = "MessageBody" + "_Field";
            this._MessageBodyLabel.Text = this._InboxMessageObject != null ? this._InboxMessageObject.Message : "";

            //Field Label Container
            modalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("MessageBody",
                                                             _GlobalResources.Message,
                                                             this._MessageBodyLabel.ID,
                                                              this._MessageBodyLabel,
                                                             false,
                                                             false,
                                                             false));
            #endregion Message Body

            modalPropertiesContainer.Controls.Add(this._HiddenButtonForPopulatingSentItemDetailsModal);
            this._SentMessageDetailsModal.AddControlToBody(modalPropertiesContainer);
            this.DataGridsContainer.Controls.Add(this._SentMessageDetailsModal);
        }
        #endregion

        #region _PopulateSentItemDetailsModalButton_Command
        /// <summary>
        /// Handles the event initiated by the hidden Button.
        /// Loading input controls
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _PopulateSentItemDetailsModalButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(this._PopulateSentItemDetailsHidden.Value))
                {
                    this._GetSentMessageObject(Convert.ToInt32(this._PopulateSentItemDetailsHidden.Value));
                    this._MessageSubjectLabel.Text = this._SentMessageObject != null ? this._SentMessageObject.Subject : string.Empty;
                    this._MessageToLabel.Text = this._SentMessageObject != null ? this._SentMessageObject.RecipientDelivered : string.Empty;
                    this._MessageBodyLabel.Text = this._SentMessageObject != null ? this._SentMessageObject.Message : string.Empty;
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._SentMessageDetailsModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dnfEx)
            {
                // display the failure message
                this._SentMessageDetailsModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._SentMessageDetailsModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._SentMessageDetailsModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException aEx)
            {
                // display the failure message
                this._SentMessageDetailsModal.DisplayFeedback(aEx.Message, true);
            }
        }
        #endregion
    }

    /// <summary>
    /// Partial call resposible for showing details of inbox message item,when you click on a row of inbox items grid
    /// and the process of send/reply or save message
    /// </summary>
    public partial class Default : AsentiaAuthenticatedPage
    {
        #region Private Property

        private HiddenField _PopulateInboxItemDetailsHidden;
        private HiddenField _IdInboxMessageForReplyHidden;

        private ModalPopup _InboxMessageDetailsModal;
        private ModalPopup _ReplyMessageModal;

        private Panel _MessageActionsPanel = new Panel();
        private Panel _ReplyModalContainer;

        private UpdatePanel _ThreadedMessageContainerUpdatePanel;

        private LinkButton _TargetControlIdForViewInboxItemDetailsModal;

        private InboxMessage _ReplyMessageObject;

        private Button _ReplySendButton;
        private Button _ReplySaveDraftButton;
        private Button _HiddenButtonForPopulatingInboxItemDetailsModal;
        private Button _InboxReplyButton;

        #endregion

        #region Public Property

        public Panel _ThreadedMessageContainer = new Panel();

        #endregion

        #region _GetInboxMessageObject
        /// <summary>
        /// Method to get the message object 
        /// </summary>
        private void _GetInboxMessageObject(int messgeId)
        {
            this._InboxMessageObject = InboxMessage._Details(messgeId, InboxMessage.MessageTypes.Inbox);
        }
        #endregion

        #region _BuildInboxMeassgeDetailsActionModal
        /// <summary>
        /// Build's Inbox meassge details action modal.
        /// </summary>
        private void _BuildInboxMeassgeDetailsActionModal()
        {
            this._TargetControlIdForViewInboxItemDetailsModal = new LinkButton();
            this._TargetControlIdForViewInboxItemDetailsModal.ID = "TargetControlIdForViewInboxItemDetailsModal";
            this._TargetControlIdForViewInboxItemDetailsModal.Attributes.Add("style", "display:none;");

            this._PopulateInboxItemDetailsHidden = new HiddenField();
            this._PopulateInboxItemDetailsHidden.ID = "PopulateInboxItemDetailsHidden";

            this.DataGridsActionContainer.Controls.Add(this._PopulateInboxItemDetailsHidden);
            this.DataGridsActionContainer.Controls.Add(this._TargetControlIdForViewInboxItemDetailsModal);

            this._BuildInboxMessageDetailsModal(this._TargetControlIdForViewInboxItemDetailsModal.ID);
        }
        #endregion

        #region _BuildInboxMessageDetailsModal
        /// <summary>
        /// Build's inbox message details modal.
        /// </summary>
        /// <param name="targetControlId">The target control identifier.</param>
        private void _BuildInboxMessageDetailsModal(string targetControlId)
        {
            this._InboxMessageDetailsModal = new ModalPopup("InboxMessageDetailsModal");

            // set modal properties
            this._InboxMessageDetailsModal.Type = ModalPopupType.Form;
            this._InboxMessageDetailsModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL_CHECK,
                                                                                   ImageFiles.EXT_PNG);
            this._InboxMessageDetailsModal.HeaderIconAlt = _GlobalResources.Inbox;
            this._InboxMessageDetailsModal.HeaderText = _GlobalResources.Inbox;

            this._InboxMessageDetailsModal.TargetControlID = targetControlId;
            this._InboxMessageDetailsModal.ReloadPageOnClose = false;
            this._InboxMessageDetailsModal.SubmitButton.Visible = false;

            this._InboxMessageDetailsModal.CloseButtonTextType = ModalPopupButtonText.Close;

            //adding hidden button for populating input controls        
            this._HiddenButtonForPopulatingInboxItemDetailsModal = new Button();
            this._HiddenButtonForPopulatingInboxItemDetailsModal.ID = "HiddenButtonForPopulatingInboxItemDetailsModal";
            this._HiddenButtonForPopulatingInboxItemDetailsModal.Command += new CommandEventHandler(this._PopulateInboxItemDetailsModalButton_Command);
            this._HiddenButtonForPopulatingInboxItemDetailsModal.Style.Add("display", "none");

            //adding custom  own submit button to cahnge text for REPLY process 
            this._InboxReplyButton = new Button();
            this._InboxReplyButton.ID = "ReplyButton";
            this._InboxReplyButton.Text = _GlobalResources.Reply;
            this._InboxReplyButton.OnClientClick = "OpenReplyModal();return false;";
            this._InboxReplyButton.CssClass = "Button ActionButton";

            // build the modal body
            //add thread messages container
            this._ThreadedMessageContainer = new Panel();
            this._ThreadedMessageContainer.ID = "ThreadedMessageContainer";
            this._ThreadedMessageContainer.CssClass = "ViewMessage";
            this._ThreadedMessageContainerUpdatePanel = new UpdatePanel();
            this._ThreadedMessageContainerUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._ThreadedMessageContainerUpdatePanel.ContentTemplateContainer.Controls.Add(this._ThreadedMessageContainer);

            this._InboxMessageDetailsModal.AddControlToBody(this._HiddenButtonForPopulatingInboxItemDetailsModal);
            this._InboxMessageDetailsModal.AddControlToBody(this._ThreadedMessageContainerUpdatePanel);
            this._InboxMessageDetailsModal.AddControlToButtonsContainer(_InboxReplyButton);

            this.DataGridsContainer.Controls.Add(this._InboxMessageDetailsModal);
        }
        #endregion

        #region _PopulateInboxItemDetailsModalButton_Command
        /// <summary>
        /// Handles the event initiated by the hidden Button.
        /// Loading input controls
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _PopulateInboxItemDetailsModalButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(this._PopulateInboxItemDetailsHidden.Value))
                {
                    this._GetInboxMessageObject(Convert.ToInt32(this._PopulateInboxItemDetailsHidden.Value));

                    this._GetThreadMessages();

                    _EnableOrDisableReplyTextBox(true, false, true);

                    string subject = this._InboxMessageObject != null ? this._InboxMessageObject.Subject : _GlobalResources.Inbox;

                    ScriptManager.RegisterStartupScript(this, GetType(), "MakeCollapsiblePanel", "MakeCollapsiblePanel();", true);

                    // to disply Subject on the header of reply modal popup
                    ScriptManager.RegisterStartupScript(this, GetType(), "DisplayInboxMessageDetails", "DisplayInboxMessageDetails('" + subject + "');", true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._InboxMessageDetailsModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dnfEx)
            {
                // display the failure message
                this._InboxMessageDetailsModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._InboxMessageDetailsModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._InboxMessageDetailsModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException aEx)
            {
                // display the failure message
                this._InboxMessageDetailsModal.DisplayFeedback(aEx.Message, true);
            }

        }
        #endregion

        #region _BuildReplyMessageModal
        /// <summary>
        /// Builds the modal for reply of inbox message.
        /// </summary>
        private void _BuildReplyMessageModal(string targetControlId)
        {
            this._ReplyMessageModal = new ModalPopup("ReplyMessage");

            // set modal properties
            this._ReplyMessageModal.Type = ModalPopupType.Form;
            this._ReplyMessageModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL_CHECK,
                                                                            ImageFiles.EXT_PNG);
            this._ReplyMessageModal.HeaderIconAlt = _GlobalResources.Reply;
            this._ReplyMessageModal.HeaderText = _GlobalResources.Reply;

            this._ReplyMessageModal.TargetControlID = targetControlId;
            this._ReplyMessageModal.SubmitButton.Visible = false;
            this._ReplyMessageModal.CloseButton.Visible = false;

            this._ReplyModalContainer = new Panel();
            this._ReplyModalContainer.ID = "ReplyModalContainer";

            #region message body containers

            //Message Body TextBox
            this._ReplyMessageBody = new TextBox();
            this._ReplyMessageBody.ID = "ReplyMessageBody" + "_Field";
            this._ReplyMessageBody.Columns = 80;
            this._ReplyMessageBody.Rows = 5;
            this._ReplyMessageBody.TextMode = TextBoxMode.MultiLine;

            this._ReplyModalContainer.Controls.Add(AsentiaPage.BuildFormField("ReplyMessageBody",
                                                                                _GlobalResources.Message,
                                                                                this._ReplyMessageBody.ID,
                                                                                this._ReplyMessageBody,
                                                                                true,
                                                                                true,
                                                                                false));

            #endregion

            //add hidden field to store idInboxMessage
            this._IdInboxMessageForReplyHidden = new HiddenField();
            this._IdInboxMessageForReplyHidden.ID = "IdInboxMessageForReply";
            this._ReplyModalContainer.Controls.Add(this._IdInboxMessageForReplyHidden);

            #region Action Panel
            //add action panel to Modal
            this._MessageActionsPanel = new Panel();

            this._ReplySendButton = new Button();
            this._ReplySendButton.ID = "ReplySaveDraftButton";
            this._ReplySendButton.Text = _GlobalResources.SaveDraft;
            this._ReplySendButton.Command += new CommandEventHandler(this._Reply_SaveDraftButton_Command);
            this._ReplySendButton.CssClass = "Button ActionButton";
            this._MessageActionsPanel.Controls.Add(this._ReplySendButton);

            this._ReplySaveDraftButton = new Button();
            this._ReplySaveDraftButton.ID = "ReplySendButton";
            this._ReplySaveDraftButton.Text = _GlobalResources.Send;
            this._ReplySaveDraftButton.Command += new CommandEventHandler(this._Reply_SendButton_Command);
            this._ReplySaveDraftButton.CssClass = "Button ActionButton";
            this._MessageActionsPanel.Controls.Add(this._ReplySaveDraftButton);

            this._ReplyModalContainer.Controls.Add(this._MessageActionsPanel);
            #endregion

            // add controls to body
            this._ReplyMessageModal.AddControlToBody(this._ReplyModalContainer);


            this.DataGridsActionContainer.Controls.Add(this._ReplyMessageModal);
        }
        #endregion

        #region _ReplyHiddenButton_Click
        /// <summary>
        /// Event fired from hidden button te reset the textbox value and visibility 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ReplyHiddenButton_Click(object sender, EventArgs e)
        {
            this._ReplyMessageModal.ClearFeedback();
            this._EnableOrDisableTextBox(false, false, true);

        }
        #endregion

        #region _Reply_SaveDraftButton_Command
        /// <summary>
        /// Save draft event for saving the message as draft(REPLY)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _Reply_SaveDraftButton_Command(object sender, EventArgs e)
        {
            try
            {
                AsentiaPage.ClearErrorClassFromFieldsRecursive(this);

                this._GetInboxMessageObject(Convert.ToInt32(this._PopulateInboxItemDetailsHidden.Value));

                if (!this._ValidateReplyModalForm())
                {
                    //rebuild the thread message panel to display the updated thred of messages
                    this._GetThreadMessages();
                    this._ThreadedMessageContainerUpdatePanel.Update();
                    ScriptManager.RegisterStartupScript(this, GetType(), "MakeCollapsiblePanel", "MakeCollapsiblePanel();", true);

                    throw new AsentiaException();
                }

                if (this._InboxMessageObject != null)
                {
                    this._ReplyMessageObject = new InboxMessage();
                    this._ReplyMessageObject.IdSender = AsentiaSessionState.IdSiteUser;
                    this._ReplyMessageObject.IdRecipient = this._InboxMessageObject.IdSender;
                    if (_InboxMessageObject.IdParentInboxMessage.HasValue)
                    {
                        this._ReplyMessageObject.IdParentInboxMessage = this._InboxMessageObject.IdParentInboxMessage;
                    }
                    else
                    {
                        this._ReplyMessageObject.IdParentInboxMessage = this._InboxMessageObject.IdInboxMessage;
                    }
                    this._ReplyMessageObject.Subject = this._InboxMessageObject.Subject;
                    this._ReplyMessageObject.Message = this._ReplyMessageBody.Text;

                    int idInboxMessage = this._ReplyMessageObject.Save();

                    if (idInboxMessage > 0)
                    {
                        //rebuild the thread message panel to display the updated thred of messages
                        this._GetThreadMessages();
                        this._ThreadedMessageContainerUpdatePanel.Update();
                        ScriptManager.RegisterStartupScript(this, GetType(), "MakeCollapsiblePanel", "MakeCollapsiblePanel();", true);

                        //reset the hidden field value to zero.
                        this._IdInboxMessageForReplyHidden.Value = "0";
                        this._EnableOrDisableReplyTextBox(false, false, true);

                        this._IdInboxMessageForReplyHidden.Value = Convert.ToString(idInboxMessage);

                        this._ReplyMessageModal.DisplayFeedback(_GlobalResources.MessageHasBeenSavedSuccessfully, false);
                    }
                    else
                    {
                        this._ReplyMessageModal.DisplayFeedback(_GlobalResources.UnableToSendMessagePleaseTryAgain, true);
                    }
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ReplyMessageModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ReplyMessageModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ReplyMessageModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ReplyMessageModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ReplyMessageModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {
                this._DraftsGrid.BindData();
                this._DraftsGridUpdatePanel.Update();
            }
        }
        #endregion

        #region _Reply_SendButton_Command
        /// <summary>
        /// Send button event to send the message to the recipient
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _Reply_SendButton_Command(object sender, EventArgs e)
        {
            try
            {
                AsentiaPage.ClearErrorClassFromFieldsRecursive(this);

                this._GetInboxMessageObject(Convert.ToInt32(this._PopulateInboxItemDetailsHidden.Value));

                if (!this._ValidateReplyModalForm())
                {
                    //rebuild the thread message panel to display the updated thred of messages
                    this._GetThreadMessages();
                    this._ThreadedMessageContainerUpdatePanel.Update();
                    ScriptManager.RegisterStartupScript(this, GetType(), "MakeCollapsiblePanel", "MakeCollapsiblePanel();", true);

                    throw new AsentiaException();
                }

                bool isDraftToSend = false;

                if (this._IdInboxMessageForReplyHidden != null && !String.IsNullOrWhiteSpace(this._IdInboxMessageForReplyHidden.Value) && Convert.ToInt32(this._IdInboxMessageForReplyHidden.Value) > 0)
                {
                    isDraftToSend = true;
                }
                if (isDraftToSend)
                {
                    this._InboxMessageObject = InboxMessage._Details(Convert.ToInt32(this._IdInboxMessageForReplyHidden.Value), InboxMessage.MessageTypes.Draft);
                }
                else
                {
                    this._GetInboxMessageObject(Convert.ToInt32(this._PopulateInboxItemDetailsHidden.Value));
                }

                if (this._InboxMessageObject != null)
                {
                    this._ReplyMessageObject = new InboxMessage();

                    if (isDraftToSend)
                    {
                        this._ReplyMessageObject.IdInboxMessage = this._InboxMessageObject.IdInboxMessage;
                    }

                    this._ReplyMessageObject.IdSender = AsentiaSessionState.IdSiteUser;
                    this._ReplyMessageObject.IdRecipient = this._InboxMessageObject.IdSender;
                    if (_InboxMessageObject.IdParentInboxMessage.HasValue)
                    {
                        this._ReplyMessageObject.IdParentInboxMessage = this._InboxMessageObject.IdParentInboxMessage;
                    }
                    else
                    {
                        this._ReplyMessageObject.IdParentInboxMessage = this._InboxMessageObject.IdInboxMessage;
                    }
                    this._ReplyMessageObject.Subject = this._InboxMessageObject.Subject;
                    this._ReplyMessageObject.Message = this._ReplyMessageBody.Text;
                    int idInboxMessage = this._ReplyMessageObject.Save();
                    this._ReplyMessageObject.Send(idInboxMessage);
                    if (idInboxMessage > 0)
                    {
                        //rebuild the thread message panel to display the updated thred of messages
                        this._GetThreadMessages();
                        this._ThreadedMessageContainerUpdatePanel.Update();
                        ScriptManager.RegisterStartupScript(this, GetType(), "MakeCollapsiblePanel", "MakeCollapsiblePanel();", true);

                        //reset the hidden field value to zero.
                        this._IdInboxMessageForReplyHidden.Value = "0";

                        this._EnableOrDisableReplyTextBox(true, true, true);
                        this._ReplyMessageModal.DisplayFeedback(_GlobalResources.MessageHasBeenSentSuccessfully, false);
                    }
                    else
                    {
                        this._ReplyMessageModal.DisplayFeedback(_GlobalResources.UnableToSendMessagePleaseTryAgain, true);
                    }
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ReplyMessageModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ReplyMessageModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ReplyMessageModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ReplyMessageModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ReplyMessageModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {
                try
                {
                    if (this._ReplyMessageObject != null && this._ReplyMessageObject.IdRecipient == AsentiaSessionState.IdSiteUser)
                    {
                        // bind inbox grid when message sent to self
                        this._InboxGrid.BindData();
                        this._InboxGridUpdatePanel.Update();
                    }

                }
                finally
                {
                    this._SentItemsGrid.BindData();
                    this._SentItemsGridUpdatePanel.Update();
                }
            }
        }
        #endregion

        #region Get Threaded Messages
        /// <summary>
        /// Method to generate the thread of all message in conversation
        /// </summary>
        private void _GetThreadMessages()
        {
            this._ThreadedMessageContainer.Controls.Clear();
            int idInboxMessage = this._InboxMessageObject.IdInboxMessage;
            List<InboxMessage> _messages = InboxMessage.ThreadMessages(idInboxMessage);
            int counter = 0;
            foreach (InboxMessage i in _messages)
            {
                Panel pnl = new Panel();
                pnl.Attributes.Add("title", i.senderName);
                pnl.Attributes.Add("subject", i.Subject);
                pnl.Attributes.Add("time", Convert.ToString(i.DateCreated));

                if (i.IdInboxMessage == idInboxMessage)
                {
                    pnl.CssClass = "CollapsibleContainer Lastone";
                }
                else
                {
                    pnl.CssClass = "CollapsibleContainer";
                }
                if (counter % 2 == 0)
                    pnl.CssClass += " Grey";
                else
                    pnl.CssClass += " White";
                HtmlGenericControl messageDetails = new HtmlGenericControl("p");
                Literal message = new Literal();

                // if message text contains new line then replacing it with the <br /> tag.   
                message.Text = i.Message.Replace("\n", "<br />").Trim();

                messageDetails.Controls.Add(message);
                pnl.Controls.Add(messageDetails);
                this._ThreadedMessageContainer.Controls.Add(pnl);

                counter++;
            }

        }


        #endregion

        #region _ValidateReplyModalForm
        /// <summary>
        /// validate reply modal form.
        /// </summary>
        /// <returns></returns>
        private bool _ValidateReplyModalForm()
        {
            bool isValid = true;
            // Body Field
            if (String.IsNullOrWhiteSpace(this._ReplyMessageBody.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._ReplyModalContainer, "ReplyMessageBody", _GlobalResources.Message + " " + _GlobalResources.IsRequired);
            }
            return isValid;
        }
        #endregion

        #region _BuildReplyProcessControls
        /// <summary>
        /// build reply process controls.
        /// </summary>
        private void _BuildReplyProcessControls()
        {
            //builds reply modal
            this._BuildReplyActionPanel();

            this._BuildInboxMeassgeDetailsActionModal();
        }
        #endregion

        #region _BuildReplyActionPanel
        /// <summary>
        /// Method to create the reply button action panel on page
        /// </summary>
        private void _BuildReplyActionPanel()
        {

            this._TargetControlIdforReplyModal = new Button();
            this._TargetControlIdforReplyModal.ID = "ReplyTargetcontrolButton";
            this._TargetControlIdforReplyModal.Attributes.Add("style", "display:none;");
            this.DataGridsActionContainer.Controls.Add(this._TargetControlIdforReplyModal);

            //builds reply modal
            this._BuildReplyMessageModal(this._TargetControlIdforReplyModal.ID);
        }
        #endregion

    }
}
