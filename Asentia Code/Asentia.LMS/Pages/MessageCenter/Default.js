﻿

function RecipientAutoComplete(keyinValue)
{
    if (keyinValue.length > 2 && /^[a-zA-Z0-9\-\s]+$/.test(keyinValue)) {
         $.ajax({
            type: "POST",
            url: "/_util/UtilityServices.asmx/SearchRecipient",
            data: "{searchParam:\"" + keyinValue + "\"}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: RecipientAutoCompleteSuccess,
            failure: function (response) {
                alert('failure');
            },
            error: function (xhr, status, error) {
                alert(error);
            }
         });
    }
    else {
        $("#RecipientAutoCompleteContainer").hide();
    }
}

function RecipientAutoCompleteSuccess(response) {
    var responseObject = response.d;
    if (responseObject.actionSuccessful) {

        if (responseObject.recipients != null) {
            var htmlCode = "";
            // go through the recipients list
            for (var i = 0; i < responseObject.recipients.length; i++) {
                if (responseObject.recipients[i].RecipientName != "undefined")
                    htmlCode += "<div id=\"RecipientAutoCompleteItem\" class=\"RecipientAutoCompleteItem\" onclick=\"AutoCompleteSelected('" + responseObject.recipients[i].RecipientID + "', '" + responseObject.recipients[i].RecipientName + "','" + responseObject.recipients[i].RecipientType + "');\"><span><img src=\"" + responseObject.recipients[i].RecipientImagePath + "\" class=\"SmallIcon\" /></span><p>" + responseObject.recipients[i].RecipientName + "</p></div>";
            } 
            $("#RecipientAutoCompleteContainer").html(htmlCode);
            $("#RecipientAutoCompleteContainer").show();
        }
        else
        {
            $("#RecipientAutoCompleteContainer").html("");
            $("#RecipientAutoCompleteContainer").hide();
        }
    }
}

//function is called to fill the name, id and objectType values in the hidden fields when a recipient from the auto complete list is selected 
function AutoCompleteSelected(id, name, type) {
    $("#ComposeMessageTo_Field").val(name);
    $("#RecipientAutoCompleteContainer").hide();

    $('#' + ToUserHiddenValue).val(name);
    $('#' + IdToUserHiddenValue).val(id);
    $('#' + ObjectTypeToUserHiddenValue).val(type);
}

//function is called when any draft row is clicked
function LaunchUpdatedPopup(idinboxMsg) {
    $('#' + IdInboxMessageHidden).val(idinboxMsg);
    document.getElementById(UpdateMessagePopupFieldsButton).click();
    document.getElementById(TargetButton).click();
}

//open compose message modal and reset input fields accordingly
function OpenComposeModal(idInboxMessge) {

    $("#" + IdInboxMessageHidden).val(idInboxMessge);
    document.getElementById(UpdateMessagePopupFieldsButton).click();
    document.getElementById(TargetButtonForComposeModal).click();
}

//Shows selected sent item details
function ShowSentItemDetails(itemId) {

    //assign selected message id value to hidden field
    $("#" + PopulateSentItemDetailsHidden).val(itemId);

    //populating input controls
    document.getElementById(HiddenButtonForPopulatingSentItemDetailsModal).click();

    //open popup
    document.getElementById(TargetControlIdForviewSentItemDetailsModal).click();
}

//Shows selected inbox item details
function ShowInboxItemDetails(itemId) {

    $('#InboxMessageDetailsModalModalPopupHeaderText').text(''); // clear reply modal popup header text
    $("#" + PopulateInboxItemDetailsHidden).val(itemId);
    document.getElementById(HiddenButtonForPopulatingInboxItemDetailsModal).click();
    document.getElementById(TargetControlIdForViewInboxItemDetailsModal).click();
}

//open reply modal
function OpenReplyModal(itemId) {
    document.getElementById(TargetControlIdforReplyModal).click();
    $("#ReplyMessageModalPopupFeedbackContainer").hide()
}

//Method to attach collapse feature for the message thread panel
(function ($) {
    $.fn.extend({
        collapsiblePanel: function () {
          
            // Call the ConfigureCollapsiblePanel function for the selected element
            return $(this).each(ConfigureCollapsiblePanel);
        }
    });
})(jQuery);

//Method to create a div with message title and date time value when message thresad is collapsed 
function ConfigureCollapsiblePanel() {
    $(this).addClass("ui-widget");

    // Wrap the contents of the container within a new div.
    $(this).children().wrapAll("<div class='CollapsibleContainerContent ui-widget-content' style='display:none;' ></div>");

    // Create a new div,  Put the title, Sbuject and Time in here.
    $("<div class='CollapsibleContainerTitle ui-widget-header'><div class='ThreadMessageName'>" + $(this).attr("title") +
    "</div><div class='ThreadMessageTime'>" + $(this).attr("time") + "</div></div>").prependTo($(this));

    // Assign a call to CollapsibleContainerTitleOnClick for the click event of the new title div.
    $(".CollapsibleContainerTitle", this).click(CollapsibleContainerTitleOnClick);
}

//Method to attach the toggle feature on collapsible panels
function CollapsibleContainerTitleOnClick() {
    // The item clicked is the title div... get this parent (the overall container) and toggle the content within it.
    $(".CollapsibleContainerContent", $(this).parent()).slideToggle();
    $(this).parent().toggleClass("BoldIt");
}

//Method to make collapsible panel
function MakeCollapsiblePanel() {
    $(".CollapsibleContainer").collapsiblePanel();
    $(".Lastone").find(".ui-widget-content").attr("style", "display:block");
    $(".Lastone").toggleClass("BoldIt");
}

function DisplayInboxMessageDetails(subject) {
    $('#InboxMessageDetailsModalModalPopupHeaderText').text(subject);
}
