﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.LMS.Pages.Catalog
{
    public class PaypalSuccess : AsentiaPage
    {
        #region Properties
        public Panel PagePropertiesWrapperContainer;
        #endregion

        #region Page_Load
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/catalog/PayPalReturn.css");

            // display the message
            this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.YourTransactionHasBeenCompletedAsSoonAsYourTransactionHasBeenClearedByPayPalYouWillBeGrantedAccessToYourPurchase + "</br></br>" + _GlobalResources.NoteCreditCardOrdersShouldbeClearedMomentarilyElectronicChecksMayTakeUpToSeveralDaysToProcess, false);
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Catalog.PaypalReturnPage.js");
        }
        #endregion
    }
}
