﻿var TransitionSpeed = 250;

function ShowCatalogRoot(transition) {
    // reset the breadcrumb stack to the first 2 items "Home" and "Catalog" (root) by trimming the length to 2
    BreadcrumbStack.length = 2;

    // bring us back to the top
    $(document).scrollTop(0);

    // make the transition back to root - only "backward" and "jump", never "forward"
    if (transition == "backward") {
        $("#" + CurrentViewIdentifier).toggle("slide", { direction: "right" }, TransitionSpeed, function () { $("#Catalog_RootViewContainer").toggle("slide", { direction: "left" }, TransitionSpeed, function () { $("#Catalog_CatalogDetailsViewContainer").empty(); $("#Catalog_ObjectDetailsViewContainer").empty(); }); });

    }
    else { // this is a jump
        $("#" + CurrentViewIdentifier).hide();
        $("#Catalog_RootViewContainer").show();
    }

    // process breadcrumb stack
    ProcessBreadcrumbStack();

    // resize the containers so we dont have unnecessary vertical scroll
    ResizeCatalogPageContainers();

    // set the current view identifier
    CurrentViewIdentifier = "Catalog_RootViewContainer";
}

function ShowCatalogDetails(idCatalog, transition, pushToBreadcrumbStack) {
    // show the loading panel
    //$("#BlackoutLoadingPanel").show();

    // make the ajax call
    $.ajax({
        type: "POST",
        url: "Default.aspx/BuildCatalogDetailsPanel",
        data: "{idCatalog: " + idCatalog + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            // get the response object
            var responseObject = response.d;

            // hide the blackout panel
            //$("#BlackoutLoadingPanel").hide();

            // put the html returned from ajax into the container
            $("#Catalog_CatalogDetailsViewContainer").empty();
            $(responseObject.html).appendTo("#Catalog_CatalogDetailsViewContainer");

            // push or pull from the breadcrumb stack based on the transition
            if (transition == "forward" || transition == "jumpfw") {
                // forward transition means add to the stack
                if (pushToBreadcrumbStack) {
                    BreadcrumbStack.push({ idObject: idCatalog, objectTitle: responseObject.objectTitle, isLinkActionHref: false, href: null, onClickAction: "ShowCatalogDetails(##idObjectReplacer##, '##transitionReplacer##', true);", showLink: false, jsTransition: "backward" });
                }
            }
            else {
                // backward or jump transition means the object is already on the stack, find it and remove everything after it
                for (var i = 0; i < BreadcrumbStack.length; i++) {
                    if (BreadcrumbStack[i].idObject == idCatalog) {
                        // remove everything after the index by resetting the length to index + 1
                        BreadcrumbStack.length = i + 1;
                    }
                }
            }

            // bring us back to the top
            $(document).scrollTop(0);

            // transition from current view to destination view
            if (transition == "forward") {
                $("#" + CurrentViewIdentifier).toggle("slide", { direction: "left" }, TransitionSpeed, function () { $("#Catalog_CatalogDetailsViewContainer").toggle("slide", { direction: "right" }, TransitionSpeed, function () { $("#Catalog_ObjectDetailsViewContainer").empty(); }); });
            }
            else if (transition == "backward") {
                $("#" + CurrentViewIdentifier).toggle("slide", { direction: "right" }, TransitionSpeed, function () { $("#Catalog_CatalogDetailsViewContainer").toggle("slide", { direction: "left" }, TransitionSpeed, function () { $("#Catalog_ObjectDetailsViewContainer").empty(); }); });
            }
            else { // this is a jump
                $("#" + CurrentViewIdentifier).hide();
                $("#Catalog_CatalogDetailsViewContainer").show();
                $("#CatalogPayPalButton").prop('disabled', true);
                $("#CatalogAddToCartButton").prop('disabled', true);
                $("#CatalogEnrollButton").prop('disabled', true);
            }

            // process breadcrumb stack
            ProcessBreadcrumbStack();

            // resize the containers so we dont have unnecessary vertical scroll
            ResizeCatalogPageContainers();

            // set the current view identifier
            CurrentViewIdentifier = "Catalog_CatalogDetailsViewContainer";

            // set the last visited item fields
            $("#LastVisitedItemId").val(idCatalog);
            $("#LastVisitedItemType").val("catalog");
        },
        failure: function (response) {
            //$("#BlackoutLoadingPanel").hide();
            //alert(response.d);
        },
        error: function (response) {
            //$("#BlackoutLoadingPanel").hide();
            //alert(response.d);
        }
    });
}

function ShowCourseDetails(idCourse, transition, pushToBreadcrumbStack) {
    // show the loading panel
    //$("#BlackoutLoadingPanel").show();
    
    // make the ajax call
    $.ajax({
        type: "POST",
        url: "Default.aspx/BuildCourseDetailsPanel",
        data: "{idCourse: " + idCourse + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            
            // get the response object
            var responseObject = response.d;

            // hide the blackout panel
            //$("#BlackoutLoadingPanel").hide();

            // put the html returned from ajax into the container
            $("#Catalog_ObjectDetailsViewContainer").empty();
            $(responseObject.html).appendTo("#Catalog_ObjectDetailsViewContainer");

            // push to the breadcrumb stack
            if (pushToBreadcrumbStack) {
                BreadcrumbStack.push({ idObject: idCourse, objectTitle: responseObject.objectTitle, isLinkActionHref: false, href: null, onClickAction: "ShowCourseDetails(##idObjectReplacer##, '##transitionReplacer##', true);", showLink: false, jsTransition: "backward" });
            }

            // bring us back to the top
            $(document).scrollTop(0);

            // transition into course details
            if (transition == "forward") {
                $("#" + CurrentViewIdentifier).toggle("slide", { direction: "left" }, TransitionSpeed, function () { $("#Catalog_ObjectDetailsViewContainer").toggle("slide", { direction: "right" }, TransitionSpeed, function () { $("#Catalog_CatalogDetailsViewContainer").empty(); }); });
            }
            else { // this is a jump
                $("#" + CurrentViewIdentifier).hide();
                $("#Catalog_ObjectDetailsViewContainer").show();
                $("#CoursePayPalButton").prop('disabled', true);
                $("#CourseAddToCartButton").prop('disabled', true);
                $("#CourseEnrollButton").prop('disabled', true);
            }

            // process breadcrumb stack
            ProcessBreadcrumbStack();

            // resize the containers so we dont have unnecessary vertical scroll
            ResizeCatalogPageContainers();

            // set the current view identifier
            CurrentViewIdentifier = "Catalog_ObjectDetailsViewContainer";
            
            // set the last visited item fields
            $("#LastVisitedItemId").val(idCourse);
            $("#LastVisitedItemType").val("course");
        },
        failure: function (response) {
            //$("#BlackoutLoadingPanel").hide();
            //alert(response.d);
        },
        error: function (response) {
            //$("#BlackoutLoadingPanel").hide();
            //alert(response.d);
        }
    });
}

function ShowLearningPathDetails(idLearningPath, transition, pushToBreadcrumbStack) {
    // show the loading panel
    //$("#BlackoutLoadingPanel").show();

    // make the ajax call
    $.ajax({
        type: "POST",
        url: "Default.aspx/BuildLearningPathDetailsPanel",
        data: "{idLearningPath: " + idLearningPath + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            // get the response object
            var responseObject = response.d;

            // hide the blackout panel
            //$("#BlackoutLoadingPanel").hide();

            // put the html returned from ajax into the container
            $("#Catalog_ObjectDetailsViewContainer").empty();
            $(responseObject.html).appendTo("#Catalog_ObjectDetailsViewContainer");

            // push to the breadcrumb stack
            if (pushToBreadcrumbStack) {
                BreadcrumbStack.push({ idObject: idLearningPath, objectTitle: responseObject.objectTitle, isLinkActionHref: false, href: null, onClickAction: "ShowLearningPathDetails(##idObjectReplacer##, '##transitionReplacer##', true);", showLink: false, jsTransition: "backward" });
            }

            // bring us back to the top
            $(document).scrollTop(0);

            // transition into learning path details
            if (transition == "forward") {
                $("#" + CurrentViewIdentifier).toggle("slide", { direction: "left" }, TransitionSpeed, function () { $("#Catalog_ObjectDetailsViewContainer").toggle("slide", { direction: "right" }, TransitionSpeed, function () { $("#Catalog_CatalogDetailsViewContainer").empty(); }); });
            }
            else { // this is a jump
                $("#" + CurrentViewIdentifier).hide();
                $("#Catalog_ObjectDetailsViewContainer").show();
                $("#LearningPathPayPalButton").prop('disabled', true);
                $("#LearningPathAddToCartButton").prop('disabled', true);
                $("#LearningPathEnrollButton").prop('disabled', true);
            }

            // process breadcrumb stack
            ProcessBreadcrumbStack();

            // resize the containers so we dont have unnecessary vertical scroll
            ResizeCatalogPageContainers();

            // set the current view identifier
            CurrentViewIdentifier = "Catalog_ObjectDetailsViewContainer";
            
            // set the last visited item fields
            $("#LastVisitedItemId").val(idLearningPath);
            $("#LastVisitedItemType").val("learningpath");
        },
        failure: function (response) {
            //$("#BlackoutLoadingPanel").hide();
            //alert(response.d);
        },
        error: function (response) {
            //$("#BlackoutLoadingPanel").hide();
            //alert(response.d);
        }
    });
}

function ShowInstructorLedTrainingDetails(idInstructorLedTraining, transition, pushToBreadcrumbStack) {
    // show the loading panel
    //$("#BlackoutLoadingPanel").show();

    // make the ajax call
    $.ajax({
        type: "POST",
        url: "Default.aspx/BuildInstructorLedTrainingDetailsPanel",
        data: "{idInstructorLedTraining: " + idInstructorLedTraining + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            // get the response object
            var responseObject = response.d;

            // hide the blackout panel
            //$("#BlackoutLoadingPanel").hide();

            // put the html returned from ajax into the container
            $("#Catalog_ObjectDetailsViewContainer").empty();
            $(responseObject.html).appendTo("#Catalog_ObjectDetailsViewContainer");

            // push to the breadcrumb stack
            if (pushToBreadcrumbStack) {
                BreadcrumbStack.push({ idObject: idInstructorLedTraining, objectTitle: responseObject.objectTitle, isLinkActionHref: false, href: null, onClickAction: "ShowInstructorLedTrainingDetails(##idObjectReplacer##, '##transitionReplacer##', true);", showLink: false, jsTransition: "backward" });
            }

            // bring us back to the top
            $(document).scrollTop(0);

            // transition into ilt details
            if (transition == "forward") {
                $("#" + CurrentViewIdentifier).toggle("slide", { direction: "left" }, TransitionSpeed, function () { $("#Catalog_ObjectDetailsViewContainer").toggle("slide", { direction: "right" }, TransitionSpeed, function () { $("#Catalog_CatalogDetailsViewContainer").empty(); }); });
            }
            else { // this is a jump
                $("#" + CurrentViewIdentifier).hide();
                $("#Catalog_ObjectDetailsViewContainer").show();
                $("#ILTPayPalButton").prop('disabled', true);
                $("#ILTAddToCartButton").prop('disabled', true);
                $("#ILTEnrollButton").prop('disabled', true);
            }

            // process breadcrumb stack
            ProcessBreadcrumbStack();

            // resize the containers so we dont have unnecessary vertical scroll
            ResizeCatalogPageContainers();

            // set the current view identifier
            CurrentViewIdentifier = "Catalog_ObjectDetailsViewContainer";
            
            // set the last visited item fields
            $("#LastVisitedItemId").val(idInstructorLedTraining);
            $("#LastVisitedItemType").val("ilt");
        },
        failure: function (response) {
            //$("#BlackoutLoadingPanel").hide();
            //alert(response.d);
        },
        error: function (response) {
            //$("#BlackoutLoadingPanel").hide();
            //alert(response.d);
        }
    });
}

function ShowCommunityDetails(idGroup, transition, pushToBreadcrumbStack) {
    // show the loading panel
    //$("#BlackoutLoadingPanel").show();

    // make the ajax call
    $.ajax({
        type: "POST",
        url: "Default.aspx/BuildCommunityDetailsPanel",
        data: "{idGroup: " + idGroup + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            // get the response object
            var responseObject = response.d;

            // hide the blackout panel
            //$("#BlackoutLoadingPanel").hide();

            // put the html returned from ajax into the container
            $("#Catalog_ObjectDetailsViewContainer").empty();
            $(responseObject.html).appendTo("#Catalog_ObjectDetailsViewContainer");

            // push to the breadcrumb stack
            if (pushToBreadcrumbStack) {
                BreadcrumbStack.push({ idObject: idGroup, objectTitle: responseObject.objectTitle, isLinkActionHref: false, href: null, onClickAction: "ShowCommunityDetails(##idObjectReplacer##, '##transitionReplacer##', true);", showLink: false, jsTransition: "backward" });
            }

            // bring us back to the top
            $(document).scrollTop(0);

            // transition into community details
            if (transition == "forward") {
                $("#" + CurrentViewIdentifier).toggle("slide", { direction: "left" }, TransitionSpeed, function () { $("#Catalog_ObjectDetailsViewContainer").toggle("slide", { direction: "right" }, TransitionSpeed, function () { $("#Catalog_CatalogDetailsViewContainer").empty(); }); });
            }
            else { // this is a jump
                $("#" + CurrentViewIdentifier).hide();
                $("#Catalog_ObjectDetailsViewContainer").show();
            }

            // process breadcrumb stack
            ProcessBreadcrumbStack();

            // resize the containers so we dont have unnecessary vertical scroll
            ResizeCatalogPageContainers();

            // set the current view identifier
            CurrentViewIdentifier = "Catalog_ObjectDetailsViewContainer";
            
            // set the last visited item fields
            $("#LastVisitedItemId").val(idGroup);
            $("#LastVisitedItemType").val("community");
        },
        failure: function (response) {
            //$("#BlackoutLoadingPanel").hide();
            //alert(response.d);
        },
        error: function (response) {
            //$("#BlackoutLoadingPanel").hide();
            //alert(response.d);
        }
    });
}

function ResizeCatalogPageContainers() {
    var contentContainerHeight = $("#ContentContainer").outerHeight(true);
    var windowHeightWithoutMasthead = $(window).outerHeight(true) - $("#MastheadContainer").outerHeight(true);

    if (windowHeightWithoutMasthead < contentContainerHeight) {
        $("#MasterContentLeft").height(windowHeightWithoutMasthead);
        $("#MasterContentRight").height(windowHeightWithoutMasthead);
    }
    else {
        $("#MasterContentLeft").height(contentContainerHeight);
        $("#MasterContentRight").height(contentContainerHeight);
    }
}

function ProcessBreadcrumbStack() {
    // loop through the breadcrumb stack to properly set breadcrumb actions
    //  - index 0 is always "Home" and never changes
    //  - index 1 is always "Catalog" (root) and changes to a link when there are items in front of it
    //      - one item in front means the transition is "backward", more than one means it is "jump"
    //  - indexes before the second to last get a link and a "jump" transition
    //  - the second to last index gets a link and a "backward" transition, and gets its link put in the "Back" link
    //  - the last index does not get a link
    for (var i = 0; i < BreadcrumbStack.length; i++) {
        if (i == BreadcrumbStack.length - 1) { // it's the last item so reset it and make sure it does not get linked
            BreadcrumbStack[i].showLink = false;
            BreadcrumbStack[i].jsTransition = null;
        }
        else if (i == BreadcrumbStack.length - 2) { // link and transition "backward"
            BreadcrumbStack[i].showLink = true;
            BreadcrumbStack[i].jsTransition = "backward";
        }
        else if (i < BreadcrumbStack.length - 2) { // link and transition "jump"
            BreadcrumbStack[i].showLink = true;
            BreadcrumbStack[i].jsTransition = "jump";
        }
    }

    // set up the html templates for breadcrumb items
    var itemTemplateNoLink = "<div class=\"BreadcrumbItem\">##objectTitleReplacer##</div>";
    var itemTemplateWithHrefLink = "<div class=\"BreadcrumbItem\"><a href=\"##hrefReplacer##\">##objectTitleReplacer##</a></div>";
    var itemTemplateWithOnClickLink = "<div class=\"BreadcrumbItem\"><a href=\"javascript: void(0);\" onclick=\"##onClickActionReplacer##\">##objectTitleReplacer##</a></div>";
    var delimiterTemplate = "<div class=\"BreadcrumbDelimiter\">//</div>";

    // empty the page breadcrumb container
    $("#PageBreadcrumbContainer").empty();

    // loop through the breadcrumb stack and add the breadcrumbs
    for (var i = 0; i < BreadcrumbStack.length; i++) {
        var breadcrumb = "";

        if (BreadcrumbStack[i].showLink) {
            if (BreadcrumbStack[i].isLinkActionHref) {
                breadcrumb = itemTemplateWithHrefLink;
                breadcrumb = breadcrumb.replace("##hrefReplacer##", BreadcrumbStack[i].href);
                breadcrumb = breadcrumb.replace("##objectTitleReplacer##", BreadcrumbStack[i].objectTitle.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
            }
            else {
                var onClickAction = "";
                onClickAction = BreadcrumbStack[i].onClickAction;
                onClickAction = onClickAction.replace("##idObjectReplacer##", BreadcrumbStack[i].idObject);
                onClickAction = onClickAction.replace("##transitionReplacer##", BreadcrumbStack[i].jsTransition);

                breadcrumb = itemTemplateWithOnClickLink;
                breadcrumb = breadcrumb.replace("##onClickActionReplacer##", onClickAction);
                breadcrumb = breadcrumb.replace("##objectTitleReplacer##", BreadcrumbStack[i].objectTitle.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
            }
        }
        else {
            breadcrumb = itemTemplateNoLink;
            breadcrumb = breadcrumb.replace("##objectTitleReplacer##", BreadcrumbStack[i].objectTitle.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        }

        // append the breadcrumb
        $(breadcrumb).appendTo("#PageBreadcrumbContainer");

        // append the delimiter if this is not the last item
        if (i < BreadcrumbStack.length - 1) {
            $(delimiterTemplate).appendTo("#PageBreadcrumbContainer");
        }
    }

    // build the "Back" button action
    // note, these actions are applied on a class as opposed to an identifier because
    // we use the same "Back" button id inside of each view, and there is a chance that
    // one exists while the other is going away (due to transition timing), so applying
    // to class will apply to all of them

    // reset and unbind the "Back" button
    $(".DetailsViewBackButtonLink").prop("href", "javascript: void(0);");
    $(".DetailsViewBackButtonLink").unbind();

    // get the second to last breadcrumb index, this is our "Back" destination
    var secondToLastBreadcrumbIndex = BreadcrumbStack.length - 2;

    // if the index is greater than 0, apply the action
    if (secondToLastBreadcrumbIndex > 0) {
        if (BreadcrumbStack[secondToLastBreadcrumbIndex].isLinkActionHref) {
            $(".DetailsViewBackButtonLink").prop("href", BreadcrumbStack[secondToLastBreadcrumbIndex].href);
        }
        else {
            var backOnClickAction = "";
            backOnClickAction = BreadcrumbStack[secondToLastBreadcrumbIndex].onClickAction;
            backOnClickAction = backOnClickAction.replace("##idObjectReplacer##", BreadcrumbStack[secondToLastBreadcrumbIndex].idObject);
            backOnClickAction = backOnClickAction.replace("##transitionReplacer##", BreadcrumbStack[secondToLastBreadcrumbIndex].jsTransition);

            $(".DetailsViewBackButtonLink").click(function () { eval(backOnClickAction); });
        }
    }
}

function LoadViewILTSessionsFromCourseModalContent(idInstructorLedTraining) {
    // empty the modal popup body
    $("#ViewILTSessionsFromCourseModalModalPopupBody").empty();

    // make the ajax call
    $.ajax({
        type: "POST",
        url: "Default.aspx/PopulateViewILTDetailsFromCourseModal",
        data: "{idInstructorLedTraining: " + idInstructorLedTraining + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            // get the response object
            var responseObject = response.d;

            // put the html returned from ajax into the container
            $(responseObject.html).appendTo("#ViewILTSessionsFromCourseModalModalPopupBody");

            // show the modal
            $("#" + ViewILTSessionsFromCourseModalHiddenLaunchButtonId).click();
        },
        failure: function (response) {
            //alert(response.d);
        },
        error: function (response) {
            //alert(response.d);
        }
    });
}

function EnrollCatalog(idCatalog, process) {
    $("#" + EnrollCatalogDataId).val(idCatalog + "|" + process);
    $("#" + EnrollCatalogModalHiddenLaunchButtonId).click();
    $("#" + EnrollCatalogModalHiddenLoadButtonId).click();
}

function EnrollCourse(idCourse, process) {
    $("#" + EnrollCourseDataId).val(idCourse + "|" + process);
    $("#" + EnrollCourseModalHiddenLaunchButtonId).click();
    $("#" + EnrollCourseModalHiddenLoadButtonId).click();
}

function EnrollLearningPath(idLearningPath, process) {
    $("#" + EnrollLearningPathDataId).val(idLearningPath + "|" + process);
    $("#" + EnrollLearningPathModalHiddenLaunchButtonId).click();
    $("#" + EnrollLearningPathModalHiddenLoadButtonId).click();
}

function LoadEnrollInInstructorLedTrainingModalContent(idInstructorLedTraining, process) {
    $("#" + EnrollInInstructorLedTrainingDataId).val(idInstructorLedTraining + "|" + process);
    $("#" + EnrollInInstructorLedTrainingModalHiddenLaunchButtonId).click();
    $("#" + EnrollInInstructorLedTrainingModalHiddenLoadButtonId).click();
}

function LoadEnrollInCourseWithOneInstructorLedTrainingModalContent(idCourse, idInstructorLedTraining, process) {
    $("#" + EnrollInCourseWithOneInstructorLedTrainingDataId).val(idCourse + "|" + idInstructorLedTraining + "|" + process);
    $("#" + EnrollInCourseWithOneInstructorLedTrainingModalHiddenLaunchButtonId).click();
    $("#" + EnrollInCourseWithOneInstructorLedTrainingModalHiddenLoadButtonId).click();
}

function GetSelectedInstructorLedTrainingInstanceListItem() {
    // initialize a value
    $("#" + EnrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceDataId).val("0|true");

    $("input[name=\"Master$PageContentPlaceholder$EnrollInInstructorLedTrainingModal$EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceSelect\"]").each(function () {
        if (this.checked) {
            var idInstructorLedTrainingInstance = this.id.replace("EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceSelect_", "");
            var isWaitingList = $(this).attr("waitinglist");
            $("#" + EnrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceDataId).val(idInstructorLedTrainingInstance + "|" + isWaitingList);
        }
    });
}

function GetSelectedSingleInstructorLedTrainingInstanceListItem() {
    // initialize a value
    $("#" + EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceDataId).val("0|true");

    $("input[name=\"Master$PageContentPlaceholder$EnrollInCourseWithOneInstructorLedTrainingModal$EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceSelect\"]").each(function () {
        if (this.checked) {
            var idInstructorLedTrainingInstance = this.id.replace("EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceSelect_", "");
            var isWaitingList = $(this).attr("waitinglist");
            $("#" + EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceDataId).val(idInstructorLedTrainingInstance + "|" + isWaitingList);
        }
    });
}

function JoinUnjoinCommunity(idGroup, isJoin) {
    $("#" + JoinUnjoinCommunityDataId).val(idGroup + "|" + isJoin);
    $("#" + JoinUnjoinCommunityModalHiddenLaunchButtonId).click();
    $("#" + JoinUnjoinCommunityModalHiddenLoadButtonId).click();
}

function GetCouponCodeInput() {
    // get the coupon code that was entered
    var couponCodeInputValue = $("#CouponCodeToApply_Field").val();
    $("#" + ApplyCouponPayPalCouponCodeDataId).val(couponCodeInputValue);
}

function ShowCourseSampleScreens(sampleScreenPaths, index) {    
    // launch the modal
    $("#" +CourseSampleScreensModalHiddenLaunchButtonId).click();

    // build the sample screens
    var screensHTML = "";

    for (var i = 0; i < sampleScreenPaths.length; i++) {
        screensHTML = screensHTML + "<div>";
        screensHTML = screensHTML + "<img src=\"" + sampleScreenPaths[i] + "\" />";
        screensHTML = screensHTML + "</div>";
    }    

    // apply the carousel
    // note that the code below looks a bit wonky, but it should be done this way as each call piggybacks 
    // off the other as a callback, this ensures one process finishes before the other starts
    if ($("#CourseSampleScreensModalContentPanel").hasClass("slick-initialized")) {
        $("#CourseSampleScreensModalContentPanel").html("").html(screensHTML).slick('removeSlide', null, null, true).slick("unslick").slick({ dots: true, autoplay: false, }).slick("slickGoTo", index, true);
    }
    else {
        $("#CourseSampleScreensModalContentPanel").html("").html(screensHTML).slick({dots: true, autoplay: false,}).slick("slickGoTo", index, true);
    }
}