﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Catalog
{
    public class Search : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CatalogSearchContentWrapperContainer;
        public Panel CatalogSearchWrapperContainer;
        public Panel SearchBoxPanel;
        public Panel TabContentContainer;
        public Panel CatalogSearchResultMainContainer;

        // login form
        public LoginForm LoginFormControl = new LoginForm();
        public ModalPopup LoginFormModal = new ModalPopup("LoginFormModal");
        #endregion

        #region Private Properties
        string _QsSearchParamUrlEncoded;
        string _QsSearchParamUrlDecoded;

        // search text box
        TextBox _SearchBox;

        // search result data tables
        DataTable _CatalogResults;
        DataTable _CourseResults;
        DataTable _LessonResults;
        DataTable _LearningPathResults;
        DataTable _CommunityResults;
        DataTable _ILTResults;
        DataTable _DocumentResults;
        #endregion

        #region OnPreInit
        protected override void OnPreInit(EventArgs e)
        {
            if (AsentiaSessionState.IdSiteUser > 0)
            { LoginFormControl.Visible = false; }

            base.OnPreInit(e);
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // instansiate a client script manager
            ClientScriptManager csm = this.Page.ClientScript;

            // set the target control for the login modal
            // note: this is already done when setting up the modal, but needs to be done again here for
            // reasons that we need to figure out, in the meantime this works
            this.LoginFormModal.TargetControlID = AsentiaMasterPage.LogInButtonId;

            // write javascript to jump to the first tab with results
            if (this._CatalogResults.Rows.Count > 0)
            {
                StringBuilder tabJumpJS = new StringBuilder();
                tabJumpJS.AppendLine("$(\"#SearchResults_Catalogs_TabLI\").click();");
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpTabJS", tabJumpJS.ToString(), true);
            }
            else if (this._CourseResults.Rows.Count > 0)
            {
                StringBuilder tabJumpJS = new StringBuilder();
                tabJumpJS.AppendLine("$(\"#SearchResults_Courses_TabLI\").click();");
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpTabJS", tabJumpJS.ToString(), true);
            }
            else if (this._LessonResults.Rows.Count > 0)
            {
                StringBuilder tabJumpJS = new StringBuilder();
                tabJumpJS.AppendLine("$(\"#SearchResults_Lessons_TabLI\").click();");
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpTabJS", tabJumpJS.ToString(), true);
            }
            else if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                if (this._LearningPathResults.Rows.Count > 0)
                {
                    StringBuilder tabJumpJS = new StringBuilder();
                    tabJumpJS.AppendLine("$(\"#SearchResults_LearningPaths_TabLI\").click();");
                    csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpTabJS", tabJumpJS.ToString(), true);
                }
            }
            else if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE))
            {
                if (this._ILTResults.Rows.Count > 0)
                {
                    StringBuilder tabJumpJS = new StringBuilder();
                    tabJumpJS.AppendLine("$(\"#SearchResults_ILT_TabLI\").click();");
                    csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpTabJS", tabJumpJS.ToString(), true);
                }
            }
            else if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
            {
                if (this._CommunityResults.Rows.Count > 0)
                {
                    StringBuilder tabJumpJS = new StringBuilder();
                    tabJumpJS.AppendLine("$(\"#SearchResults_Communities_TabLI\").click();");
                    csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpTabJS", tabJumpJS.ToString(), true);
                }
            }
            else
            { }

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "CloseIconClientScript", "$(\"#LoginFormModalModalCloseIcon\").click(function () {" +
                " $(\"#" + this.LoginFormControl.UsernameBox.ID + "\").val(''); " +
                " $(\"#" + this.LoginFormControl.PasswordBox.ID + "\").val(''); " +
                " $(\"#LoginFormForgotPasswordCancelButton\").click(); " +
                " $(\"#LoginFormFeedbackPanel\").hide(); " +
                "});", true);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/catalog/Search.css");

            // bounce if the catalog is not enabled, or if login is required and user is not logged in, or search is not enabled
            if (
                (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_ENABLE) != true)
                ||
                (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_REQUIRE_LOGIN) == true && AsentiaSessionState.IdSiteUser == 0)
                ||
                (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_SEARCH_ENABLE) == false)
               )
            { Response.Redirect("/"); }

            // get the querystring search parameter
            this._QsSearchParamUrlEncoded = this.QueryStringString("searchParam", String.Empty);
            this._QsSearchParamUrlDecoded = Server.UrlDecode(this._QsSearchParamUrlEncoded);

            // if there is no search param, just pop back to the catalog
            if (String.IsNullOrWhiteSpace(this._QsSearchParamUrlEncoded))
            { Response.Redirect("/catalog"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Catalog, "/catalog"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.CatalogSearch));
            this.BuildBreadcrumb(breadCrumbLinks);

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CatalogSearchContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CatalogSearchWrapperContainer.CssClass = "FormContentContainer";

            // build the login modal
            this._BuildLoginModal();

            // build the search panel
            this._BuildSearchPanel();

            // execute the searches
            this._CatalogResults = LMS.Library.Catalog.Search(this._QsSearchParamUrlDecoded, "catalog");
            this._CourseResults = LMS.Library.Catalog.Search(this._QsSearchParamUrlDecoded, "course");
            this._LessonResults = LMS.Library.Catalog.Search(this._QsSearchParamUrlDecoded, "lesson");

            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE) == true && 
                AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_LEARNINGPATHS_ENABLE) == true)
            { this._LearningPathResults = LMS.Library.Catalog.Search(this._QsSearchParamUrlDecoded, "learningpath"); }
            else
            { this._LearningPathResults = new DataTable(); }

            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE) == true && 
                AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_STANDUPTRAINING_ENABLE) == true)
            { this._ILTResults = LMS.Library.Catalog.Search(this._QsSearchParamUrlDecoded, "ilt"); }
            else
            { this._ILTResults = new DataTable(); }

            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE) == true &&
                AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_COMMUNITY_ENABLE) == true)
            { this._CommunityResults = LMS.Library.Catalog.Search(this._QsSearchParamUrlDecoded, "community"); }
            else
            { this._CommunityResults = new DataTable(); }

            this._DocumentResults = LMS.Library.Catalog.Search(this._QsSearchParamUrlDecoded, "document");

            // build the search reuslt tabs
            this._BuildSearchResultTabs();

            // build the search result panels
            this._BuildSearchResultPanels();
        }
        #endregion

        #region _BuildLoginModal
        /// <summary>
        /// _BuildLoginModal
        /// </summary>
        private void _BuildLoginModal()
        {
            // if the user isn't logged in, show the login form
            if (AsentiaSessionState.IdSiteUser == 0)
            {
                // set form render type and forgot password submit button command
                this.LoginFormControl.FormRenderType = LoginFormRenderType.InModal;
                this.LoginFormControl.LoginButton.Command += new CommandEventHandler(this._LoginButton_Command);
                this.LoginFormControl.ForgotPasswordSubmitButton.Command += new CommandEventHandler(this._ForgotPassword_Command);

                // build the modal
                this.LoginFormModal.Type = ModalPopupType.EmbeddedForm;
                this.LoginFormModal.TargetControlID = AsentiaMasterPage.LogInButtonId;
                this.LoginFormModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                                                                            ImageFiles.EXT_PNG);
                this.LoginFormModal.HeaderIconAlt = _GlobalResources.Log_In;
                this.LoginFormModal.HeaderText = _GlobalResources.Log_In;

                this.LoginFormModal.FocusControlIdOnLaunch = this.LoginFormControl.UsernameBox.ID;
                this.LoginFormModal.ReloadPageOnClose = false;

                // attach login control to modal
                this.LoginFormModal.AddControlToBody(this.LoginFormControl);

                // attach modal to page
                this.PageContentContainer.Controls.Add(this.LoginFormModal);

                // ensure controls exist
                this.EnsureChildControls();
            }
        }
        #endregion

        #region _LoginButton_Command
        /// <summary>
        /// Method to perform the user login.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">arguments</param>
        private void _LoginButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string lastActiveSessionId = "";
                bool simultaniousLogin = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_SIMULTANEOUSLOGIN_ENABLED);
                string loginPriority = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_LOGINPRIORITY);

                // make sure username and password are specified
                if (String.IsNullOrWhiteSpace(this.LoginFormControl.UsernameBox.Text)
                    || String.IsNullOrWhiteSpace(this.LoginFormControl.PasswordBox.Text))
                {
                    throw new AsentiaException(_GlobalResources.UsernameAndPasswordMustBeSpecified);
                }

                // authenticate login
                Asentia.UMS.Library.Login.AuthenticateUser(this.LoginFormControl.UsernameBox.Text, this.LoginFormControl.PasswordBox.Text, out lastActiveSessionId);

                if (simultaniousLogin == false && loginPriority == "latest" && !String.IsNullOrWhiteSpace(lastActiveSessionId) && lastActiveSessionId != AsentiaSessionState.SessionId)
                { Asentia.UMS.Library.Login.DeleteBySessionId(lastActiveSessionId); }

                // if we get here without throwing an exception, it means a successful login, so redirect back here to the catalog search
                Response.Redirect("/catalog/Search.aspx");
            }
            catch (Exception ex)
            {
                // show the error
                this.LoginFormControl.DisplayFeedback(ex.Message, true, true);

                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                { this.LoginFormControl.ShowFormContentPanel(); }
            }
        }
        #endregion

        #region _ForgotPassword_Command
        /// <summary>
        /// Method to process the forgot password form submission from the modal.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">arguments</param>
        private void _ForgotPassword_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // make sure username is specified
                if (String.IsNullOrWhiteSpace(this.LoginFormControl.ForgotPasswordModalUsernameBox.Text))
                {
                    throw new AsentiaException(_GlobalResources.UsernameMustBeSpecified);
                }

                // reset the password
                string emailSentTo = Asentia.UMS.Library.Login.ResetPassword(this.LoginFormControl.ForgotPasswordModalUsernameBox.Text);

                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(true);
                    this.LoginFormControl.DisplayFeedback(String.Format(_GlobalResources.YourPasswordHasBeenSuccessfullyResetAndSentToEmail, emailSentTo), false, false);
                }
                else
                {
                    // disable the submit button and show the success feedback in the forgot password modal
                    this.LoginFormControl.ForgotPasswordModal.DisableSubmitButton();
                    this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(String.Format(_GlobalResources.YourPasswordHasBeenSuccessfullyResetAndSentToEmail, emailSentTo), false);
                }

            }
            catch (Exception ex)
            {
                // show the error
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(false);
                    this.LoginFormControl.DisplayFeedback(ex.Message, true, false);
                }
                else
                { this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(ex.Message, true); }
            }
        }
        #endregion

        #region _BuildSearchPanel
        /// <summary>
        /// Builds the search panel for searching the catalog.
        /// </summary>
        private void _BuildSearchPanel()
        {
            this.SearchBoxPanel.CssClass = "SearchBoxPanel";

            this._SearchBox = new TextBox();
            this._SearchBox.ID = "SearchBox";
            this._SearchBox.CssClass = "InputLong";
            this._SearchBox.Text = this._QsSearchParamUrlDecoded;
            this.SearchBoxPanel.Controls.Add(this._SearchBox);

            // search button
            Button searchButton = new Button();
            searchButton.ID = "SearchButton";
            searchButton.CssClass = "Button ActionButton";
            searchButton.Text = _GlobalResources.Search;
            searchButton.Command += new CommandEventHandler(this._SearchButton_Command);
            this.SearchBoxPanel.Controls.Add(searchButton);

            // set the default button
            this.SearchBoxPanel.DefaultButton = searchButton.ID;
        }
        #endregion

        #region _BuildSearchResultTabs
        /// <summary>
        /// Builds the tabs for the search results.
        /// </summary>
        private void _BuildSearchResultTabs()
        {


            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Catalogs", _GlobalResources.Catalogs + " (" + this._CatalogResults.Rows.Count.ToString() + ")"));
            tabs.Enqueue(new KeyValuePair<string, string>("Courses", _GlobalResources.Courses + " (" + this._CourseResults.Rows.Count.ToString() + ")"));
            tabs.Enqueue(new KeyValuePair<string, string>("Lessons", _GlobalResources.Modules + " (" + this._LessonResults.Rows.Count.ToString() + ")"));

            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE) == true)
            {
                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_LEARNINGPATHS_ENABLE) == true)
                { tabs.Enqueue(new KeyValuePair<string, string>("LearningPaths", _GlobalResources.LearningPaths + " (" + this._LearningPathResults.Rows.Count.ToString() + ")")); }
            }

            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE) == true)
            {
                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_STANDUPTRAINING_ENABLE) == true)
                { tabs.Enqueue(new KeyValuePair<string, string>("ILT", _GlobalResources.ILT + " (" + this._ILTResults.Rows.Count.ToString() + ")")); }
            }

            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE) == true)
            {
                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_COMMUNITY_ENABLE) == true)
                { tabs.Enqueue(new KeyValuePair<string, string>("Communities", _GlobalResources.Communities + " (" + this._CommunityResults.Rows.Count.ToString() + ")")); }
            }
            tabs.Enqueue(new KeyValuePair<string, string>("Documents", _GlobalResources.Documents + " (" + this._DocumentResults.Rows.Count.ToString() + ")"));

            // build and attach the tabs
            this.TabContentContainer.Controls.Add(AsentiaPage.BuildTabListPanel("SearchResults", tabs));
        }
        #endregion

        #region _BuildSearchResultPanels
        /// <summary>
        /// Builds the panels for the search results.
        /// </summary>
        private void _BuildSearchResultPanels()
        {
            // "Catalogs" is the default tab, so this is visible on page load
            Panel catalogsPanel = new Panel();
            catalogsPanel.ID = "SearchResults_" + "Catalogs" + "_TabPanel";
            catalogsPanel.CssClass = "TabPanelContainer";
            catalogsPanel.Attributes.Add("style", "display: block;");
            this.CatalogSearchResultMainContainer.Controls.Add(catalogsPanel);

            if (this._CatalogResults.Rows.Count > 0)
            {
                foreach (DataRow row in this._CatalogResults.Rows)
                { catalogsPanel.Controls.Add(this._BuildSearchResultRow(row)); }
            }
            else
            {
                Panel noCatalogResultsFoundContainer = new Panel();
                noCatalogResultsFoundContainer.ID = "NoCatalogResultsFoundContainer";
                catalogsPanel.Controls.Add(noCatalogResultsFoundContainer);

                Literal noCatalogResultsFound = new Literal();
                noCatalogResultsFound.Text = _GlobalResources.NoCatalogResultsWereFound;
                noCatalogResultsFoundContainer.Controls.Add(noCatalogResultsFound);
            }

            // "Courses" panel
            Panel coursesPanel = new Panel();
            coursesPanel.ID = "SearchResults_" + "Courses" + "_TabPanel";
            coursesPanel.CssClass = "TabPanelContainer";
            coursesPanel.Attributes.Add("style", "display: none;");
            this.CatalogSearchResultMainContainer.Controls.Add(coursesPanel);

            if (this._CourseResults.Rows.Count > 0)
            {
                foreach (DataRow row in this._CourseResults.Rows)
                { coursesPanel.Controls.Add(this._BuildSearchResultRow(row)); }
            }
            else
            {
                Panel noCourseResultsFoundContainer = new Panel();
                noCourseResultsFoundContainer.ID = "NoCourseResultsFoundContainer";
                coursesPanel.Controls.Add(noCourseResultsFoundContainer);

                Literal noCourseResultsFound = new Literal();
                noCourseResultsFound.Text = _GlobalResources.NoCourseResultsWereFound;
                noCourseResultsFoundContainer.Controls.Add(noCourseResultsFound);
            }

            // "Lessons" panel
            Panel lessonsPanel = new Panel();
            lessonsPanel.ID = "SearchResults_" + "Lessons" + "_TabPanel";
            lessonsPanel.CssClass = "TabPanelContainer";
            lessonsPanel.Attributes.Add("style", "display: none;");
            this.CatalogSearchResultMainContainer.Controls.Add(lessonsPanel);

            if (this._LessonResults.Rows.Count > 0)
            {
                foreach (DataRow row in this._LessonResults.Rows)
                { lessonsPanel.Controls.Add(this._BuildSearchResultRow(row)); }
            }
            else
            {
                Panel noLessonResultsFoundContainer = new Panel();
                noLessonResultsFoundContainer.ID = "NoLessonResultsFoundContainer";
                lessonsPanel.Controls.Add(noLessonResultsFoundContainer);

                Literal noLessonResultsFound = new Literal();
                noLessonResultsFound.Text = _GlobalResources.NoModuleResultsWereFound;
                noLessonResultsFoundContainer.Controls.Add(noLessonResultsFound);
            }

            // "Learning Paths" panel
            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE) == true)
            {
                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_LEARNINGPATHS_ENABLE) == true)
                {
                    Panel learningPathsPanel = new Panel();
                    learningPathsPanel.ID = "SearchResults_" + "LearningPaths" + "_TabPanel";
                    learningPathsPanel.CssClass = "TabPanelContainer";
                    learningPathsPanel.Attributes.Add("style", "display: none;");
                    this.CatalogSearchResultMainContainer.Controls.Add(learningPathsPanel);

                    if (this._LearningPathResults.Rows.Count > 0)
                    {
                        foreach (DataRow row in this._LearningPathResults.Rows)
                        { learningPathsPanel.Controls.Add(this._BuildSearchResultRow(row)); }
                    }
                    else
                    {
                        Panel noLearningPathResultsFoundContainer = new Panel();
                        noLearningPathResultsFoundContainer.ID = "NoLearningPathResultsFoundContainer";
                        learningPathsPanel.Controls.Add(noLearningPathResultsFoundContainer);

                        Literal noLearningPathResultsFound = new Literal();
                        noLearningPathResultsFound.Text = _GlobalResources.NoLearningPathResultsWereFound;
                        noLearningPathResultsFoundContainer.Controls.Add(noLearningPathResultsFound);
                    }
                }
            }

            // "ILT" panel
            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE) == true)
            {
                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_STANDUPTRAINING_ENABLE) == true)
                {
                    Panel iltPanel = new Panel();
                    iltPanel.ID = "SearchResults_" + "ILT" + "_TabPanel";
                    iltPanel.CssClass = "TabPanelContainer";
                    iltPanel.Attributes.Add("style", "display: none;");
                    this.CatalogSearchResultMainContainer.Controls.Add(iltPanel);

                    if (this._ILTResults.Rows.Count > 0)
                    {
                        foreach (DataRow row in this._ILTResults.Rows)
                        { iltPanel.Controls.Add(this._BuildSearchResultRow(row)); }
                    }
                    else
                    {
                        Panel noILTResultsFoundContainer = new Panel();
                        noILTResultsFoundContainer.ID = "NoILTResultsFoundContainer";
                        iltPanel.Controls.Add(noILTResultsFoundContainer);

                        Literal noILTResultsFound = new Literal();
                        noILTResultsFound.Text = _GlobalResources.NoInstructorLedTrainingResultsWereFound;
                        noILTResultsFoundContainer.Controls.Add(noILTResultsFound);
                    }
                }
            }
            // "Communities" panel
            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE) == true)
            {
                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_COMMUNITY_ENABLE) == true)
                {
                    Panel communitiesPanel = new Panel();
                    communitiesPanel.ID = "SearchResults_" + "Communities" + "_TabPanel";
                    communitiesPanel.CssClass = "TabPanelContainer";
                    communitiesPanel.Attributes.Add("style", "display: none;");
                    this.CatalogSearchResultMainContainer.Controls.Add(communitiesPanel);

                    if (this._CommunityResults.Rows.Count > 0)
                    {
                        foreach (DataRow row in this._CommunityResults.Rows)
                        { communitiesPanel.Controls.Add(this._BuildSearchResultRow(row)); }
                    }
                    else
                    {
                        Panel noCommunityResultsFoundContainer = new Panel();
                        noCommunityResultsFoundContainer.ID = "NoCommunityResultsFoundContainer";
                        communitiesPanel.Controls.Add(noCommunityResultsFoundContainer);

                        Literal noCommunityResultsFound = new Literal();
                        noCommunityResultsFound.Text = _GlobalResources.NoCommunityResultsWereFound;
                        noCommunityResultsFoundContainer.Controls.Add(noCommunityResultsFound);
                    }
                }
            }
            // "Documents" panel
            Panel documentsPanel = new Panel();
            documentsPanel.ID = "SearchResults_" + "Documents" + "_TabPanel";
            documentsPanel.CssClass = "TabPanelContainer";
            documentsPanel.Attributes.Add("style", "display: none;");
            this.CatalogSearchResultMainContainer.Controls.Add(documentsPanel);

            if (this._DocumentResults.Rows.Count > 0)
            {
                foreach (DataRow row in this._DocumentResults.Rows)
                { documentsPanel.Controls.Add(this._BuildSearchResultRow(row)); }
            }
            else
            {
                Panel noDocumentResultsFoundContainer = new Panel();
                noDocumentResultsFoundContainer.ID = "NoDocumentResultsFoundContainer";
                documentsPanel.Controls.Add(noDocumentResultsFoundContainer);

                Literal noDocumentResultsFound = new Literal();
                noDocumentResultsFound.Text = _GlobalResources.NoDocumentResultsWereFound;
                noDocumentResultsFoundContainer.Controls.Add(noDocumentResultsFound);
            }
        }
        #endregion

        #region _BuildSearchResultRow
        /// <summary>
        /// Builds a container for a search result row.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private Panel _BuildSearchResultRow(DataRow row)
        {
            // get the row data
            int idObject = Convert.ToInt32(row["idObject"]);
            string objectType = row["objectType"].ToString();
            string objectCode = row["objectCode"].ToString();
            string objectTitle = row["objectTitle"].ToString();
            string objectDescription = row["objectDescription"].ToString();
            
            int? containedWithinObjectId = null;

            if (!String.IsNullOrWhiteSpace(row["containedWithinObjectId"].ToString()))
            { containedWithinObjectId = Convert.ToInt32(row["containedWithinObjectId"]); }

            string containedWithinObjectType = row["containedWithinObjectType"].ToString();
            string containedWithinObjectTitle = row["containedWithinObjectTitle"].ToString();

            // build the search result container
            Panel searchResultContainer = new Panel();
            searchResultContainer.ID = "SearchResultContainer_" + objectType + "_" + idObject.ToString();
            searchResultContainer.CssClass = "SearchResultContainer";

            // build the search result object title
            Panel searchResultTitleContainer = new Panel();
            searchResultTitleContainer.ID = "SearchResultTitleContainer_" + objectType + "_" + idObject.ToString();
            searchResultTitleContainer.CssClass = "SearchResultTitleContainer";
            searchResultContainer.Controls.Add(searchResultTitleContainer);

            if (objectType == "lesson")
            {
                Literal searchResultTitle = new Literal();
                searchResultTitle.Text = objectTitle;
                searchResultTitleContainer.Controls.Add(searchResultTitle);
            }
            else
            {
                HyperLink searchResultTitleLink = new HyperLink();

                if (objectType == "course" && !String.IsNullOrWhiteSpace(objectCode) && AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_HIDE_COURSE_CODES) != true)
                { searchResultTitleLink.Text = objectCode + " - " + objectTitle; }
                else if (objectType == "document" && String.IsNullOrWhiteSpace(objectTitle))
                { searchResultTitleLink.Text = objectDescription; } // objectDescription is the filename for documents
                else
                { searchResultTitleLink.Text = objectTitle; }

                // if the object is a document, link to the document
                if (objectType == "document")
                {
                    if (containedWithinObjectType == "group")
                    {
                        
                        // if the user is in the group or admin account, document is downloadable, otherwise just show the document name
                        if (Asentia.UMS.Library.User.IsMemberOfGroup((int)containedWithinObjectId) || AsentiaSessionState.IdSiteUser == 1)
                        {
                            searchResultTitleLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_GROUP + containedWithinObjectId.ToString() + "/" + objectDescription;
                            searchResultTitleLink.Target = "_blank";
                            searchResultTitleContainer.Controls.Add(searchResultTitleLink);
                        }
                        else
                        {
                            Literal groupDocumentLiteral = new Literal();
                            // if the object title is empty, show the filename
                            if (String.IsNullOrWhiteSpace(objectTitle))
                            {
                                groupDocumentLiteral.Text = objectDescription;
                            }
                            else
                            {
                                groupDocumentLiteral.Text = objectTitle;
                            }
                            searchResultTitleContainer.Controls.Add(groupDocumentLiteral);
                        }
                    }
                    else if (containedWithinObjectType == "course")
                    {
                        
                        // if the user is enrolled in the course or admin account, document is downloadable, otherwise just show the document name
                        if (Asentia.UMS.Library.User.IsCourseEnrolled((int)containedWithinObjectId) || AsentiaSessionState.IdSiteUser == 1)
                        {
                            searchResultTitleLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_COURSE + containedWithinObjectId.ToString() + "/" + objectDescription;
                            searchResultTitleLink.Target = "_blank";
                            searchResultTitleContainer.Controls.Add(searchResultTitleLink);
                        }
                        else
                        {
                            Literal courseDocumentLiteral = new Literal();
                            // if the object title is empty, show the filename
                            if (String.IsNullOrWhiteSpace(objectTitle))
                            {
                                courseDocumentLiteral.Text = objectDescription;
                            }
                            else
                            {
                                courseDocumentLiteral.Text = objectTitle;
                            }
                            searchResultTitleContainer.Controls.Add(courseDocumentLiteral);
                        }
                    }
                    else
                    {
                        if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE) == true)
                        {
                            // if the user is enrolled in the learning path or admin account, document is downloadable, otherwise just show the document name
                            if (Asentia.UMS.Library.User.IsLearningPathEnrolled((int)containedWithinObjectId) || AsentiaSessionState.IdSiteUser == 1)
                            {
                                searchResultTitleLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LEARNINGPATH + containedWithinObjectId.ToString() + "/" + objectDescription;
                                searchResultTitleLink.Target = "_blank";
                                searchResultTitleContainer.Controls.Add(searchResultTitleLink);
                            }
                            else
                            {
                                Literal learningPathDocumentLiteral = new Literal();
                                // if the object title is empty, show the filename
                                if (String.IsNullOrWhiteSpace(objectTitle))
                                {
                                    learningPathDocumentLiteral.Text = objectDescription;
                                }
                                else
                                {
                                    learningPathDocumentLiteral.Text = objectTitle;
                                }
                                searchResultTitleContainer.Controls.Add(learningPathDocumentLiteral);
                            }
                        }
                    }
                    
                }
                else
                {
                    searchResultTitleLink.NavigateUrl = "/catalog/Default.aspx?id=" + idObject.ToString() + "&type=" + objectType + "&searchParam=" + this._QsSearchParamUrlEncoded;
                    searchResultTitleContainer.Controls.Add(searchResultTitleLink);
                }
            }

            // build the search result object contained within, if needed
            if (objectType == "lesson" || objectType == "document")
            {
                Panel searchResultContainedInContainer = new Panel();
                searchResultContainedInContainer.ID = "SearchResultContainedInContainer_" + objectType + "_" + idObject.ToString();
                searchResultContainedInContainer.CssClass = "SearchResultContainedInContainer";
                searchResultContainer.Controls.Add(searchResultContainedInContainer);

                if (objectType == "lesson") // lesson object
                {
                    HyperLink searchResultContainedInLink = new HyperLink();
                    searchResultContainedInLink.Text = _GlobalResources.Course + ": " + containedWithinObjectTitle;
                    searchResultContainedInLink.NavigateUrl = "/catalog/Default.aspx?id=" + containedWithinObjectId.ToString() + "&type=" + containedWithinObjectType + "&searchParam=" + this._QsSearchParamUrlEncoded;
                    searchResultContainedInContainer.Controls.Add(searchResultContainedInLink);
                }
                else // document object
                {
                    Literal searchResultContainedInLiteral = new Literal();
                    if (containedWithinObjectType == "group")
                    { // group document
                        searchResultContainedInLiteral.Text = _GlobalResources.Group + ": " + containedWithinObjectTitle;
                    }
                    else if (containedWithinObjectType == "course")
                    { // course document
                        searchResultContainedInLiteral.Text = _GlobalResources.Course + ": " + containedWithinObjectTitle;
                    }
                    else
                    { // learning path document
                        if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE) == true)
                        {
                            searchResultContainedInLiteral.Text = _GlobalResources.LearningPath + ": " + containedWithinObjectTitle;
                        }
                    }
                    searchResultContainedInContainer.Controls.Add(searchResultContainedInLiteral);
                }
            }

            // build the search result object description for all types except document
            if (objectType != "document")
            {
                Panel searchResultDescriptionContainer = new Panel();
                searchResultDescriptionContainer.ID = "SearchResultDescriptionContainer_" + objectType + "_" + idObject.ToString();
                searchResultDescriptionContainer.CssClass = "SearchResultDescriptionContainer";
                searchResultContainer.Controls.Add(searchResultDescriptionContainer);

                Literal searchResultObjectDescription = new Literal();
                searchResultObjectDescription.Text = objectDescription;
                searchResultDescriptionContainer.Controls.Add(searchResultObjectDescription);
            }

            // return the panel
            return searchResultContainer;
        }
        #endregion

        #region _SearchButton_Command
        /// <summary>
        /// Handles the search button click.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _SearchButton_Command(object sender, CommandEventArgs e)
        {
            // if the search box isn't blank, get the value and redirect to the search page, otherwise redirect back to catalog
            if (!String.IsNullOrWhiteSpace(this._SearchBox.Text))
            { Response.Redirect("/catalog/Search.aspx?searchParam=" + Server.UrlEncode(this._SearchBox.Text)); }
            else
            { Response.Redirect("/catalog"); }
        }
        #endregion
    }
}
