﻿$(document).ready(function () {
    // hide the masthead
    $("#MastheadContainer").hide();

    // hide the footer
    $("#FooterContainer").hide();

    // assign window.close to the info status panel
    $("#FeedbackInformationStatusPanelCloseButton").click(function () {
        window.close();
    });
});