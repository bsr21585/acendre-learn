﻿using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;
using AjaxControlToolkit;

namespace Asentia.LMS.Pages.Catalog
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CatalogFormContentWrapperContainer;
        public Panel CatalogWrapperContainer;
        public Panel SearchBoxPanel;
        public Panel TabContentContainer;
        public Panel CatalogMainContainer;

        // login form
        public LoginForm LoginFormControl = new LoginForm();
        public ModalPopup LoginFormModal = new ModalPopup("LoginFormModal");
        #endregion

        #region Private Properties
        private const int MAX_CHARACTERS_IN_TITLE_BEFORE_ELLIPSE = 45;

        int _QsId;
        string _QsType;
        string _QsSearchParam;
        string _QsShortcode;

        // search text box
        TextBox _SearchBox;

        // view ilt sessions from course modal
        private ModalPopup _ViewILTSessionsFromCourseModal;
        private Button _ViewILTSessionsFromCourseModalHiddenLaunchButton;

        // enroll in instructor led training modal
        private ModalPopup _EnrollInInstructorLedTrainingModal;
        private Button _EnrollInInstructorLedTrainingModalHiddenLaunchButton;
        private Button _EnrollInInstructorLedTrainingModalHiddenLoadButton;
        private Panel _EnrollInInstructorLedTrainingDetailsPanel;
        private HiddenField _EnrollInInstructorLedTrainingData;
        private HiddenField _EnrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceData;

        // enroll in course with one ILT
        private ModalPopup _EnrollInCourseWithOneInstructorLedTrainingModal;
        private Button _EnrollInCourseWithOneInstructorLedTrainingModalHiddenLaunchButton;
        private Button _EnrollInCourseWithOneInstructorLedTrainingModalHiddenLoadButton;
        private Panel _EnrollInCourseWithOneInstructorLedTrainingDetailsPanel;
        private HiddenField _EnrollInCourseWithOneInstructorLedTrainingData;
        private HiddenField _EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceData;

        // join/unjoin community modal
        private ModalPopup _JoinUnjoinCommunityModal;
        private Button _JoinUnjoinCommunityModalHiddenLaunchButton;
        private Button _JoinUnjoinCommunityModalHiddenLoadButton;
        private Panel _JoinUnjoinCommunityModalDetailsPanel;
        private HiddenField _JoinUnjoinCommunityData;

        // enroll catalog modal
        private ModalPopup _EnrollCatalogModal;
        private Button _EnrollCatalogModalHiddenLaunchButton;
        private Button _EnrollCatalogModalHiddenLoadButton;
        private Panel _EnrollCatalogModalDetailsPanel;
        private HiddenField _EnrollCatalogData;

        // enroll course modal
        private ModalPopup _EnrollCourseModal;
        private Button _EnrollCourseModalHiddenLaunchButton;
        private Button _EnrollCourseModalHiddenLoadButton;
        private Panel _EnrollCourseModalDetailsPanel;
        private HiddenField _EnrollCourseData;

        // enroll learning path modal
        private ModalPopup _EnrollLearningPathModal;
        private Button _EnrollLearningPathModalHiddenLaunchButton;
        private Button _EnrollLearningPathModalHiddenLoadButton;
        private Panel _EnrollLearningPathModalDetailsPanel;
        private HiddenField _EnrollLearningPathData;

        // apply coupon/buy now for paypal modal        
        private ModalPopup _ApplyCouponPayPalBuyNowModal;
        private Button _ApplyCouponPayPalBuyNowModalHiddenLaunchButton;
        private Panel _ApplyCouponPayPalBuyNowModalDetailsPanel;
        private HiddenField _ApplyCouponPayPalBuyNowData;
        private HiddenField _ApplyCouponPayPalCouponCodeData;

        // course sample screens modal
        private ModalPopup _CourseSampleScreensModal;
        private Button _CourseSampleScreensModalHiddenLaunchButton;

        // hidden fields to store last item visited, this is so non-logged in 
        // users can be redirected back to the last item after they've logged in
        private HiddenField _LastVisitedItemId;
        private HiddenField _LastVisitedItemType;
        #endregion

        #region Enums
        #region CatalogTileType
        public enum CatalogTileType
        {
            CatalogsHeader = 0,
            Catalog = 1,
            CoursesHeader = 2,
            Course = 3,
            LearningPathsHeader = 4,
            LearningPath = 5,
            InstructorLedTrainingHeader = 6,
            InstructorLedTraining = 7,
            CommunitiesHeader = 8,
            Community = 9,
        }
        #endregion
        #endregion

        #region OnPreInit
        protected override void OnPreInit(EventArgs e)
        {
            if (AsentiaSessionState.IdSiteUser > 0)
            { LoginFormControl.Visible = false; }

            base.OnPreInit(e);
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Catalog.Default.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Carousel.min.js");

            string googleAnalyticsTrackingCode = String.Empty;
            googleAnalyticsTrackingCode = this.GetTrackingCode();

            if (!String.IsNullOrWhiteSpace(googleAnalyticsTrackingCode))
            {
                bool applyScriptTags = true;

                if (googleAnalyticsTrackingCode.Contains("<script") && googleAnalyticsTrackingCode.Contains("</script"))
                { applyScriptTags = false; }

                // build start up call for google analytics tracking code
                StringBuilder googleAnalyticsTrackingScript = new StringBuilder();
                googleAnalyticsTrackingScript.AppendLine(googleAnalyticsTrackingCode);

                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "GoogleAnalyticsTrackingCode", googleAnalyticsTrackingScript.ToString(), applyScriptTags);
            }

            // set the target control for the login modal
            // note: this is already done when setting up the modal, but needs to be done again here for
            // reasons that we need to figure out, in the meantime this works
            this.LoginFormModal.TargetControlID = AsentiaMasterPage.LogInButtonId;

            // set up javascript globals 
            StringBuilder globalJS = new StringBuilder();
            globalJS.AppendLine("var BreadcrumbStack = [");
            globalJS.AppendLine("        { idObject: null, objectTitle: \"" + _GlobalResources.Home + "\", isLinkActionHref: true, href: \"/\", onClickAction: null, showLink: true, jsTransition: null },");
            globalJS.AppendLine("        { idObject: null, objectTitle: \"" + _GlobalResources.Catalog + "\", isLinkActionHref: false, href: null, onClickAction: \"ShowCatalogRoot('##transitionReplacer##');\", showLink: false, jsTransition: null },");

            // append a search results breadcrumb if there was a search param
            if (!String.IsNullOrWhiteSpace(this._QsSearchParam))
            { globalJS.AppendLine("        { idObject: null, objectTitle: \"" + _GlobalResources.SearchResults + "\", isLinkActionHref: true, href: \"/catalog/Search.aspx?searchParam=" + this._QsSearchParam + "\", onClickAction: null, showLink: true, jsTransition: null },"); }

            globalJS.AppendLine("];");
            globalJS.AppendLine("");
            globalJS.AppendLine("var CurrentViewIdentifier = \"Catalog_RootViewContainer\";");
            globalJS.AppendLine("var ViewILTSessionsFromCourseModalHiddenLaunchButtonId = \"" + this._ViewILTSessionsFromCourseModalHiddenLaunchButton.ID + "\";");
            globalJS.AppendLine("var EnrollInInstructorLedTrainingModalHiddenLaunchButtonId = \"" + this._EnrollInInstructorLedTrainingModalHiddenLaunchButton.ID + "\";");
            globalJS.AppendLine("var EnrollInInstructorLedTrainingModalHiddenLoadButtonId = \"" + this._EnrollInInstructorLedTrainingModalHiddenLoadButton.ID + "\";");
            globalJS.AppendLine("var EnrollInInstructorLedTrainingDataId = \"" + this._EnrollInInstructorLedTrainingData.ID + "\";");
            globalJS.AppendLine("var EnrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceDataId = \"" + this._EnrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceData.ID + "\";");
            globalJS.AppendLine("var EnrollInCourseWithOneInstructorLedTrainingModalHiddenLaunchButtonId = \"" + this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLaunchButton.ID + "\";");
            globalJS.AppendLine("var EnrollInCourseWithOneInstructorLedTrainingModalHiddenLoadButtonId = \"" + this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLoadButton.ID + "\";");
            globalJS.AppendLine("var EnrollInCourseWithOneInstructorLedTrainingDataId = \"" + this._EnrollInCourseWithOneInstructorLedTrainingData.ID + "\";");
            globalJS.AppendLine("var EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceDataId = \"" + this._EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceData.ID + "\";");
            globalJS.AppendLine("var JoinUnjoinCommunityModalHiddenLaunchButtonId = \"" + this._JoinUnjoinCommunityModalHiddenLaunchButton.ID + "\";");
            globalJS.AppendLine("var JoinUnjoinCommunityModalHiddenLoadButtonId = \"" + this._JoinUnjoinCommunityModalHiddenLoadButton.ID + "\";");
            globalJS.AppendLine("var JoinUnjoinCommunityDataId = \"" + this._JoinUnjoinCommunityData.ID + "\";");
            globalJS.AppendLine("var EnrollCatalogModalHiddenLaunchButtonId = \"" + this._EnrollCatalogModalHiddenLaunchButton.ID + "\";");
            globalJS.AppendLine("var EnrollCatalogModalHiddenLoadButtonId = \"" + this._EnrollCatalogModalHiddenLoadButton.ID + "\";");
            globalJS.AppendLine("var EnrollCatalogDataId = \"" + this._EnrollCatalogData.ID + "\";");
            globalJS.AppendLine("var EnrollCourseModalHiddenLaunchButtonId = \"" + this._EnrollCourseModalHiddenLaunchButton.ID + "\";");
            globalJS.AppendLine("var EnrollCourseModalHiddenLoadButtonId = \"" + this._EnrollCourseModalHiddenLoadButton.ID + "\";");
            globalJS.AppendLine("var EnrollCourseDataId = \"" + this._EnrollCourseData.ID + "\";");
            globalJS.AppendLine("var EnrollLearningPathModalHiddenLaunchButtonId = \"" + this._EnrollLearningPathModalHiddenLaunchButton.ID + "\";");
            globalJS.AppendLine("var EnrollLearningPathModalHiddenLoadButtonId = \"" + this._EnrollLearningPathModalHiddenLoadButton.ID + "\";");
            globalJS.AppendLine("var EnrollLearningPathDataId = \"" + this._EnrollLearningPathData.ID + "\";");
            globalJS.AppendLine("var ApplyCouponPayPalCouponCodeDataId = \"" + this._ApplyCouponPayPalCouponCodeData.ID + "\";");
            globalJS.AppendLine("var CourseSampleScreensModalHiddenLaunchButtonId = \"" + this._CourseSampleScreensModalHiddenLaunchButton.ID + "\";");
            

            csm.RegisterClientScriptBlock(typeof(Default), "CatalogGlobalJS", globalJS.ToString(), true);

            // set a startup script to jump to an object if querystring parameters are in place, or jump to root if none

            if (this._QsId > 0 && this._QsType == "catalog")
            {
                StringBuilder itemJumpJS = new StringBuilder();
                itemJumpJS.AppendLine("ShowCatalogDetails(" + this._QsId + ", \"jumpfw\", true);");
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpToCatalogItemJS", itemJumpJS.ToString(), true);
            }
            else if (this._QsId > 0 && this._QsType == "course")
            {
                StringBuilder itemJumpJS = new StringBuilder();
                itemJumpJS.AppendLine("ShowCourseDetails(" + this._QsId + ", \"jump\", true);");
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpToCatalogItemJS", itemJumpJS.ToString(), true);
            }
            else if (this._QsId > 0 && this._QsType == "learningpath")
            {
                StringBuilder itemJumpJS = new StringBuilder();
                itemJumpJS.AppendLine("ShowLearningPathDetails(" + this._QsId + ", \"jump\", true);");
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpToCatalogItemJS", itemJumpJS.ToString(), true);
            }
            else if (this._QsId > 0 && this._QsType == "ilt")
            {
                StringBuilder itemJumpJS = new StringBuilder();
                itemJumpJS.AppendLine("ShowInstructorLedTrainingDetails(" + this._QsId + ", \"jump\", true);");
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpToCatalogItemJS", itemJumpJS.ToString(), true);
            }
            else if (this._QsId > 0 && this._QsType == "community")
            {
                StringBuilder itemJumpJS = new StringBuilder();
                itemJumpJS.AppendLine("ShowCommunityDetails(" + this._QsId + ", \"jump\", true);");
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpToCatalogItemJS", itemJumpJS.ToString(), true);
            }
            else // jump directly to catalog root
            {
                StringBuilder itemJumpJS = new StringBuilder();
                itemJumpJS.AppendLine("ShowCatalogRoot(\"jump\");");
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JumpToCatalogItemJS", itemJumpJS.ToString(), true);
            }

            // clean up login modal after closing it
            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "CloseIconClientScript", "$(\"#LoginFormModalModalCloseIcon\").click(function () {" +
                " $(\"#" + this.LoginFormControl.UsernameBox.ID + "\").val(''); " +
                " $(\"#" + this.LoginFormControl.PasswordBox.ID + "\").val(''); " +
                " $(\"#LoginFormForgotPasswordCancelButton\").click(); " +
                " $(\"#LoginFormFeedbackPanel\").hide(); " +
                "});", true);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // include page-specific css files
            this.IncludePageSpecificCssFile("UserRating.css");
            this.IncludePageSpecificCssFile("page-specific/catalog/Default.css");

            // bounce if the catalog is not enabled, or if login is required and user is not logged in
            if (
                (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_ENABLE) != true)
                ||
                (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_REQUIRE_LOGIN) == true && AsentiaSessionState.IdSiteUser == 0)
               )
            { Response.Redirect("/"); }            

            // get the querystring parameters
            this._QsId = this.QueryStringInt("id", 0);
            this._QsType = this.QueryStringString("type", null);
            this._QsSearchParam = this.QueryStringString("searchParam", null);
            this._QsShortcode = this.QueryStringString("sc", null);

            // if there is a shortcode in the querystring, get it's object id and populate _QsId
            if (!String.IsNullOrWhiteSpace(this._QsShortcode) && !String.IsNullOrWhiteSpace(this._QsType))
            {
                if (this._QsType == "course")
                { this._QsId = Course.GetCourseIdByShortcode(this._QsShortcode); }
                else if (this._QsType == "learningpath")
                { this._QsId = LearningPath.GetLearningPathIdByShortcode(this._QsShortcode); }
                else if (this._QsType == "ilt")
                { this._QsId = StandupTraining.GetStandupTrainingIdByShortcode(this._QsShortcode); }
                else if (this._QsType == "catalog")
                { this._QsId = LMS.Library.Catalog.GetCatalogIdByShortcode(this._QsShortcode); }
                else
                { }
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Catalog));
            this.BuildBreadcrumb(breadCrumbLinks);

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CatalogFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CatalogWrapperContainer.CssClass = "FormContentContainer";            

            // build the login modal
            this._BuildLoginModal();

            // build the search panel
            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_SEARCH_ENABLE) == true)
            { this._BuildSearchPanel(); }

            // build blackout loading panel
            this._BuildBlackoutLoadingPanel();

            // build the catalog views containers
            this._BuildCatalogViewsContainers();

            // build the view ilt sessions from course modal
            this._BuildViewILTSessionsFromCourseModal();

            // build the enroll in catalog modal
            this._BuildEnrollCatalogModal();

            // build the enroll in course modal
            this._BuildEnrollCourseModal();

            // build the enroll in learning path modal
            this._BuildEnrollLearningPathModal();

            // build the enroll in instructor led training instance modal
            this._BuildEnrollInInstructorLedTrainingInstanceModal();

            // build the modal used for enrolling a user in an ILT session for a course that only has one ILT
            this._BuildEnrollInCourseWithOneInstructorLedTrainingModal();

            // build the join/unjoin community modal
            this._BuildJoinUnjoinCommunityModal();

            // build the apply coupon/buy now for paypal modal
            this._BuildApplyCouponPayPalBuyNowModal();

            // build the course sample screens modal
            this._BuildCourseSampleScreensModal();
        }
        #endregion

        #region _BuildLoginModal
        /// <summary>
        /// _BuildLoginModal
        /// </summary>
        private void _BuildLoginModal()
        {
            // if the user isn't logged in, show the login form
            if (AsentiaSessionState.IdSiteUser == 0)
            {
                // set form render type and forgot password submit button command
                this.LoginFormControl.FormRenderType = LoginFormRenderType.InModal;
                this.LoginFormControl.LoginButton.Command += new CommandEventHandler(this._LoginButton_Command);
                this.LoginFormControl.ForgotPasswordSubmitButton.Command += new CommandEventHandler(this._ForgotPassword_Command);

                // build the modal
                this.LoginFormModal.Type = ModalPopupType.EmbeddedForm;
                this.LoginFormModal.TargetControlID = AsentiaMasterPage.LogInButtonId;
                this.LoginFormModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                                                                            ImageFiles.EXT_PNG);
                this.LoginFormModal.HeaderIconAlt = _GlobalResources.Log_In;
                this.LoginFormModal.HeaderText = _GlobalResources.Log_In;

                this.LoginFormModal.FocusControlIdOnLaunch = this.LoginFormControl.UsernameBox.ID;
                this.LoginFormModal.ReloadPageOnClose = false;

                // attach login control to modal
                this.LoginFormModal.AddControlToBody(this.LoginFormControl);

                // attach modal to page
                this.PageContentContainer.Controls.Add(this.LoginFormModal);

                // add hidden fields to store last visited item
                this._LastVisitedItemId = new HiddenField();
                this._LastVisitedItemId.ID = "LastVisitedItemId";
                this.PageContentContainer.Controls.Add(this._LastVisitedItemId);

                this._LastVisitedItemType = new HiddenField();
                this._LastVisitedItemType.ID = "LastVisitedItemType";
                this.PageContentContainer.Controls.Add(this._LastVisitedItemType);

                // ensure controls exist
                this.EnsureChildControls();
            }
        }
        #endregion

        #region _LoginButton_Command
        /// <summary>
        /// Method to perform the user login.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">arguments</param>
        private void _LoginButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                string lastActiveSessionId = "";
                bool simultaniousLogin = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_SIMULTANEOUSLOGIN_ENABLED);
                string loginPriority = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_LOGINPRIORITY);

                // make sure username and password are specified
                if (String.IsNullOrWhiteSpace(this.LoginFormControl.UsernameBox.Text)
                    || String.IsNullOrWhiteSpace(this.LoginFormControl.PasswordBox.Text))
                {
                    throw new AsentiaException(_GlobalResources.UsernameAndPasswordMustBeSpecified);
                }

                // authenticate login
                Asentia.UMS.Library.Login.AuthenticateUser(this.LoginFormControl.UsernameBox.Text, this.LoginFormControl.PasswordBox.Text, out lastActiveSessionId);

                if (simultaniousLogin == false && loginPriority == "latest" && !String.IsNullOrWhiteSpace(lastActiveSessionId) && lastActiveSessionId != AsentiaSessionState.SessionId)
                { Asentia.UMS.Library.Login.DeleteBySessionId(lastActiveSessionId); }

                //if we get here without throwing an exception, it means a successful login, so redirect back here to the catalog
                if (!String.IsNullOrWhiteSpace(this._LastVisitedItemId.Value) && !string.IsNullOrWhiteSpace(this._LastVisitedItemType.Value))
                { Response.Redirect("/catalog?id=" + this._LastVisitedItemId.Value + "&type=" + this._LastVisitedItemType.Value); }
                else
                { Response.Redirect("/catalog"); }
            }
            catch (Exception ex)
            {
                // show the error
                this.LoginFormControl.DisplayFeedback(ex.Message, true, true);

                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                { this.LoginFormControl.ShowFormContentPanel(); }
            }
        }
        #endregion

        #region _ForgotPassword_Command
        /// <summary>
        /// Method to process the forgot password form submission from the modal.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">arguments</param>
        private void _ForgotPassword_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // make sure username is specified
                if (String.IsNullOrWhiteSpace(this.LoginFormControl.ForgotPasswordModalUsernameBox.Text))
                {
                    throw new AsentiaException(_GlobalResources.UsernameMustBeSpecified);
                }

                // reset the password
                string emailSentTo = Asentia.UMS.Library.Login.ResetPassword(this.LoginFormControl.ForgotPasswordModalUsernameBox.Text);

                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(true);
                    this.LoginFormControl.DisplayFeedback(String.Format(_GlobalResources.YourPasswordHasBeenSuccessfullyResetAndSentToEmail, emailSentTo), false, false);
                }
                else
                {
                    // disable the submit button and show the success feedback in the forgot password modal
                    this.LoginFormControl.ForgotPasswordModal.DisableSubmitButton();
                    this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(String.Format(_GlobalResources.YourPasswordHasBeenSuccessfullyResetAndSentToEmail, emailSentTo), false);
                }

            }
            catch (Exception ex)
            {
                // show the error
                if (this.LoginFormControl.FormRenderType == LoginFormRenderType.InModal)
                {
                    // show forgot password panel with submit disables and display feedback
                    this.LoginFormControl.ShowForgotPasswordPanel(false);
                    this.LoginFormControl.DisplayFeedback(ex.Message, true, false);
                }
                else
                { this.LoginFormControl.ForgotPasswordModal.DisplayFeedback(ex.Message, true); }
            }
        }
        #endregion

        #region _BuildSearchPanel
        /// <summary>
        /// Builds the search panel for searching the catalog.
        /// </summary>
        private void _BuildSearchPanel()
        {
            this.SearchBoxPanel.CssClass = "SearchBoxPanel";

            this._SearchBox = new TextBox();
            this._SearchBox.ID = "SearchBox";
            this._SearchBox.CssClass = "InputLong";
            this._SearchBox.Attributes.Add("placeholder", _GlobalResources.SearchCatalog);

            this.SearchBoxPanel.Controls.Add(this._SearchBox);

            // search button
            Button searchButton = new Button();
            searchButton.ID = "SearchButton";
            searchButton.CssClass = "Button ActionButton SearchButton";
            searchButton.Text = _GlobalResources.Search;
            searchButton.Command += new CommandEventHandler(this._SearchButton_Command);
            this.SearchBoxPanel.Controls.Add(searchButton);

            // set the default button
            this.SearchBoxPanel.DefaultButton = searchButton.ID;
        }
        #endregion

        #region _BuildBlackoutLoadingPanel
        /// <summary>
        /// Builds the blackout panel with loading image for when catalog AJAX requests are made.
        /// </summary>
        private void _BuildBlackoutLoadingPanel()
        {
            Panel blackoutLoadingPanel = new Panel();
            blackoutLoadingPanel.ID = "BlackoutLoadingPanel";
            blackoutLoadingPanel.Style.Add("display", "none");

            HtmlGenericControl blackoutLoadingPanelLoadingImageWrapper = new HtmlGenericControl("p");
            blackoutLoadingPanelLoadingImageWrapper.ID = "BlackoutLoadingPanelLoadingImageWrapper";

            Image blackoutLoadingPanelLoadingImage = new Image();
            blackoutLoadingPanelLoadingImage.ID = "BlackoutLoadingPanelLoadingImage";
            blackoutLoadingPanelLoadingImage.CssClass = "LargeIcon";
            blackoutLoadingPanelLoadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING_DARKBG, ImageFiles.EXT_GIF);
            blackoutLoadingPanelLoadingImage.AlternateText = _GlobalResources.Loading;

            blackoutLoadingPanelLoadingImageWrapper.Controls.Add(blackoutLoadingPanelLoadingImage);
            blackoutLoadingPanel.Controls.Add(blackoutLoadingPanelLoadingImageWrapper);
            this.CatalogWrapperContainer.Controls.Add(blackoutLoadingPanel);
        }
        #endregion

        #region _BuildCatalogViewsContainers
        /// <summary>
        /// Builds the catalog views containers, root view, catalog details view,
        /// and object details view.
        /// </summary>
        private void _BuildCatalogViewsContainers()
        {
            // root view container
            Panel catalogRootViewContainer = new Panel();
            catalogRootViewContainer.ID = "Catalog_RootViewContainer";
            catalogRootViewContainer.Style.Add("display", "none"); // default this to display: none because we will use JS to show it
            this.CatalogMainContainer.Controls.Add(catalogRootViewContainer);

            // catalog details view container
            Panel catalogCatalogDetailsViewContainer = new Panel();
            catalogCatalogDetailsViewContainer.ID = "Catalog_CatalogDetailsViewContainer";
            catalogCatalogDetailsViewContainer.Style.Add("display", "none");
            this.CatalogMainContainer.Controls.Add(catalogCatalogDetailsViewContainer);

            // object details view container
            Panel catalogObjectDetailsViewContainer = new Panel();
            catalogObjectDetailsViewContainer.ID = "Catalog_ObjectDetailsViewContainer";
            catalogObjectDetailsViewContainer.Style.Add("display", "none");
            this.CatalogMainContainer.Controls.Add(catalogObjectDetailsViewContainer);

            // build the tiles for the root view
            // if we are building the event calendar, we need to build extra containers, otherwise we just attach the tiles to the root view
            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_EVENTCALENDAR_ENABLE) == true)
            {
                // tiles
                Panel catalogRootViewLeftSideContainer = new Panel();
                catalogRootViewLeftSideContainer.ID = "Catalog_RootViewLeftSideContainer";
                catalogRootViewContainer.Controls.Add(catalogRootViewLeftSideContainer);

                BuildCatalogTiles(catalogRootViewLeftSideContainer, null);
                BuildCourseTiles(catalogRootViewLeftSideContainer, null);

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE) == true)
                {
                    if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_LEARNINGPATHS_ENABLE) == true)
                    { BuildLearningPathTiles(catalogRootViewLeftSideContainer, null); }
                }

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_STANDUPTRAINING_ENABLE) == true &&
                    (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE))
                { BuildInstructorLedTrainingTiles(catalogRootViewLeftSideContainer); }

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_COMMUNITY_ENABLE) == true && 
                    (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
                { BuildCommunityTiles(catalogRootViewLeftSideContainer); }

                // calendar
                Panel catalogRootViewRightSideContainer = new Panel();
                catalogRootViewRightSideContainer.ID = "Catalog_RootViewRightSideContainer";
                catalogRootViewContainer.Controls.Add(catalogRootViewRightSideContainer);

                Panel catalogEventCalendarContainer = new Panel();
                catalogEventCalendarContainer.ID = "CatalogEventCalendarContainer";
                catalogRootViewRightSideContainer.Controls.Add(catalogEventCalendarContainer);

                Panel catalogEventCalendarModalContainer = new Panel();
                catalogEventCalendarModalContainer.ID = "CatalogEventCalendarModalContainer";
                catalogEventCalendarContainer.Controls.Add(catalogEventCalendarModalContainer);

                Asentia.LMS.Controls.Calendar calendar = new Asentia.LMS.Controls.Calendar(AsentiaSessionState.UserTimezoneCurrentLocalTime, calendarEventsType: Asentia.LMS.Controls.Calendar.CalendarEventsType.CatalogILT);
                calendar.CalendarModalControlDestination = catalogEventCalendarModalContainer;
                catalogEventCalendarContainer.Controls.Add(calendar);
            }
            else
            {
                BuildCatalogTiles(catalogRootViewContainer, null);
                BuildCourseTiles(catalogRootViewContainer, null);

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE) == true)
                {
                    if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_LEARNINGPATHS_ENABLE) == true)
                    { BuildLearningPathTiles(catalogRootViewContainer, null); }
                }

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_STANDUPTRAINING_ENABLE) == true &&
                    (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE))
                { BuildInstructorLedTrainingTiles(catalogRootViewContainer); }

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_COMMUNITY_ENABLE) == true && 
                    (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
                { BuildCommunityTiles(catalogRootViewContainer); }
            }
        }
        #endregion

        #region _BuildViewILTSessionsFromCourseModal
        /// <summary>
        /// Builds the modal that shows upcoming ILT sessions from the course details view.
        /// This modal is informational only, therefore it's body gets loaded via AJAX, unlike
        /// other modals, which use partial postbacks.
        /// </summary>
        private void _BuildViewILTSessionsFromCourseModal()
        {
            // instansiate the trigger button
            this._ViewILTSessionsFromCourseModalHiddenLaunchButton = new Button();
            this._ViewILTSessionsFromCourseModalHiddenLaunchButton.ID = "ViewILTSessionsFromCourseModalHiddenLaunchButton";
            this._ViewILTSessionsFromCourseModalHiddenLaunchButton.Style.Add("display", "none");
            this.CatalogMainContainer.Controls.Add(this._ViewILTSessionsFromCourseModalHiddenLaunchButton);

            // set modal properties
            this._ViewILTSessionsFromCourseModal = new ModalPopup("ViewILTSessionsFromCourseModal");
            this._ViewILTSessionsFromCourseModal.Type = ModalPopupType.Information;
            this._ViewILTSessionsFromCourseModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            this._ViewILTSessionsFromCourseModal.HeaderIconAlt = _GlobalResources.UpcomingSessions;
            this._ViewILTSessionsFromCourseModal.HeaderText = _GlobalResources.UpcomingSessions;
            this._ViewILTSessionsFromCourseModal.TargetControlID = this._ViewILTSessionsFromCourseModalHiddenLaunchButton.ID;

            // add modal to container
            this.CatalogMainContainer.Controls.Add(this._ViewILTSessionsFromCourseModal);
        }
        #endregion

        #region _BuildEnrollCatalogModal
        /// <summary>
        /// Builds the modal popup for enrolling in a catalog.
        /// </summary>
        private void _BuildEnrollCatalogModal()
        {
            // THE SUPPORTING ELEMENTS - TRIGGERS, HIDDEN FIELDS, ETC.

            // instansiate enroll in catalog controls
            this._EnrollCatalogModalHiddenLaunchButton = new Button();
            this._EnrollCatalogModalHiddenLaunchButton.ID = "EnrollCatalogModalHiddenLaunchButton";
            this._EnrollCatalogModalHiddenLaunchButton.Style.Add("display", "none");

            this._EnrollCatalogModalHiddenLoadButton = new Button();
            this._EnrollCatalogModalHiddenLoadButton.ID = "EnrollCatalogModalHiddenLoadButton";
            this._EnrollCatalogModalHiddenLoadButton.Style.Add("display", "none");
            this._EnrollCatalogModalHiddenLoadButton.Command += this._EnrollCatalog_Command;

            this._EnrollCatalogModalDetailsPanel = new Panel();
            this._EnrollCatalogModalDetailsPanel.ID = "EnrollCatalogModalDetailsPanel";

            this._EnrollCatalogData = new HiddenField();
            this._EnrollCatalogData.ID = "EnrollCatalogData";

            // add trigger button to container
            this.CatalogMainContainer.Controls.Add(this._EnrollCatalogModalHiddenLaunchButton);

            // THE MODAL

            // set modal properties
            this._EnrollCatalogModal = new ModalPopup("EnrollCatalogModal");
            this._EnrollCatalogModal.Type = ModalPopupType.Form;
            this._EnrollCatalogModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
            this._EnrollCatalogModal.HeaderIconAlt = _GlobalResources.EnrollCatalog;
            this._EnrollCatalogModal.HeaderText = _GlobalResources.EnrollCatalog;
            this._EnrollCatalogModal.SubmitButton.Visible = false;
            this._EnrollCatalogModal.CloseButton.Visible = false;

            this._EnrollCatalogModal.TargetControlID = this._EnrollCatalogModalHiddenLaunchButton.ID;

            // build the modal body
            this._EnrollCatalogModal.AddControlToBody(this._EnrollCatalogModalDetailsPanel);
            this._EnrollCatalogModal.AddControlToBody(this._EnrollCatalogModalHiddenLoadButton);
            this._EnrollCatalogModal.AddControlToBody(this._EnrollCatalogData);

            // add modal to container
            this.CatalogMainContainer.Controls.Add(this._EnrollCatalogModal);
        }
        #endregion

        #region _BuildEnrollCourseModal
        /// <summary>
        /// Builds the modal popup for enrolling in a course.
        /// </summary>
        private void _BuildEnrollCourseModal()
        {
            // THE SUPPORTING ELEMENTS - TRIGGERS, HIDDEN FIELDS, ETC.

            // instansiate enroll in course controls
            this._EnrollCourseModalHiddenLaunchButton = new Button();
            this._EnrollCourseModalHiddenLaunchButton.ID = "EnrollCourseModalHiddenLaunchButton";
            this._EnrollCourseModalHiddenLaunchButton.Style.Add("display", "none");

            this._EnrollCourseModalHiddenLoadButton = new Button();
            this._EnrollCourseModalHiddenLoadButton.ID = "EnrollCourseModalHiddenLoadButton";
            this._EnrollCourseModalHiddenLoadButton.Style.Add("display", "none");
            this._EnrollCourseModalHiddenLoadButton.Command += this._EnrollCourse_Command;

            this._EnrollCourseModalDetailsPanel = new Panel();
            this._EnrollCourseModalDetailsPanel.ID = "EnrollCourseModalDetailsPanel";

            this._EnrollCourseData = new HiddenField();
            this._EnrollCourseData.ID = "EnrollCourseData";

            // add trigger button to container
            this.CatalogMainContainer.Controls.Add(this._EnrollCourseModalHiddenLaunchButton);

            // THE MODAL

            // set modal properties
            this._EnrollCourseModal = new ModalPopup("EnrollCourseModal");
            this._EnrollCourseModal.Type = ModalPopupType.Form;
            this._EnrollCourseModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
            this._EnrollCourseModal.HeaderIconAlt = _GlobalResources.EnrollCourse;
            this._EnrollCourseModal.HeaderText = _GlobalResources.EnrollCourse;
            this._EnrollCourseModal.SubmitButton.Visible = false;
            this._EnrollCourseModal.CloseButton.Visible = false;

            this._EnrollCourseModal.TargetControlID = this._EnrollCourseModalHiddenLaunchButton.ID;

            // build the modal body
            this._EnrollCourseModal.AddControlToBody(this._EnrollCourseModalDetailsPanel);
            this._EnrollCourseModal.AddControlToBody(this._EnrollCourseModalHiddenLoadButton);
            this._EnrollCourseModal.AddControlToBody(this._EnrollCourseData);

            // add modal to container
            this.CatalogMainContainer.Controls.Add(this._EnrollCourseModal);
        }
        #endregion

        #region _BuildEnrollLearningPathModal
        /// <summary>
        /// Builds the modal popup for enrolling in a learning path.
        /// </summary>
        private void _BuildEnrollLearningPathModal()
        {
            // THE SUPPORTING ELEMENTS - TRIGGERS, HIDDEN FIELDS, ETC.

            // instansiate enroll in learning path controls
            this._EnrollLearningPathModalHiddenLaunchButton = new Button();
            this._EnrollLearningPathModalHiddenLaunchButton.ID = "EnrollLearningPathModalHiddenLaunchButton";
            this._EnrollLearningPathModalHiddenLaunchButton.Style.Add("display", "none");

            this._EnrollLearningPathModalHiddenLoadButton = new Button();
            this._EnrollLearningPathModalHiddenLoadButton.ID = "EnrollLearningPathModalHiddenLoadButton";
            this._EnrollLearningPathModalHiddenLoadButton.Style.Add("display", "none");
            this._EnrollLearningPathModalHiddenLoadButton.Command += this._EnrollLearningPath_Command;

            this._EnrollLearningPathModalDetailsPanel = new Panel();
            this._EnrollLearningPathModalDetailsPanel.ID = "EnrollLearningPathModalDetailsPanel";

            this._EnrollLearningPathData = new HiddenField();
            this._EnrollLearningPathData.ID = "EnrollLearningPathData";

            // add trigger button to container
            this.CatalogMainContainer.Controls.Add(this._EnrollLearningPathModalHiddenLaunchButton);

            // THE MODAL

            // set modal properties
            this._EnrollLearningPathModal = new ModalPopup("EnrollLearningPathModal");
            this._EnrollLearningPathModal.Type = ModalPopupType.Form;
            this._EnrollLearningPathModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
            this._EnrollLearningPathModal.HeaderIconAlt = _GlobalResources.EnrollLearningPath;
            this._EnrollLearningPathModal.HeaderText = _GlobalResources.EnrollLearningPath;
            this._EnrollLearningPathModal.SubmitButton.Visible = false;
            this._EnrollLearningPathModal.CloseButton.Visible = false;

            this._EnrollLearningPathModal.TargetControlID = this._EnrollLearningPathModalHiddenLaunchButton.ID;

            // build the modal body
            this._EnrollLearningPathModal.AddControlToBody(this._EnrollLearningPathModalDetailsPanel);
            this._EnrollLearningPathModal.AddControlToBody(this._EnrollLearningPathModalHiddenLoadButton);
            this._EnrollLearningPathModal.AddControlToBody(this._EnrollLearningPathData);

            // add modal to container
            this.CatalogMainContainer.Controls.Add(this._EnrollLearningPathModal);
        }
        #endregion

        #region _BuildEnrollInInstructorLedTrainingInstanceModal
        /// <summary>
        /// Builds the modal popup for enrolling into a instructor led training session.
        /// </summary>
        private void _BuildEnrollInInstructorLedTrainingInstanceModal()
        {
            // THE SUPPORTING ELEMENTS - TRIGGERS, HIDDEN FIELDS, ETC.

            // instansiate enroll in instructor led training controls
            this._EnrollInInstructorLedTrainingModalHiddenLaunchButton = new Button();
            this._EnrollInInstructorLedTrainingModalHiddenLaunchButton.ID = "EnrollInInstructorLedTrainingModalHiddenLaunchButton";
            this._EnrollInInstructorLedTrainingModalHiddenLaunchButton.Style.Add("display", "none");

            this._EnrollInInstructorLedTrainingModalHiddenLoadButton = new Button();
            this._EnrollInInstructorLedTrainingModalHiddenLoadButton.ID = "EnrollInInstructorLedTrainingModalHiddenLoadButton";
            this._EnrollInInstructorLedTrainingModalHiddenLoadButton.Style.Add("display", "none");
            this._EnrollInInstructorLedTrainingModalHiddenLoadButton.Click += this._LoadEnrollInInstructorLedTrainingModalContent;

            this._EnrollInInstructorLedTrainingDetailsPanel = new Panel();
            this._EnrollInInstructorLedTrainingDetailsPanel.ID = "EnrollInInstructorLedTrainingDetailsPanel";

            this._EnrollInInstructorLedTrainingData = new HiddenField();
            this._EnrollInInstructorLedTrainingData.ID = "EnrollInInstructorLedTrainingData";

            this._EnrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceData = new HiddenField();
            this._EnrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceData.ID = "EnrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceData";

            // add trigger button to container
            this.CatalogMainContainer.Controls.Add(this._EnrollInInstructorLedTrainingModalHiddenLaunchButton);

            // THE MODAL

            // set modal properties
            this._EnrollInInstructorLedTrainingModal = new ModalPopup("EnrollInInstructorLedTrainingModal");
            this._EnrollInInstructorLedTrainingModal.Type = ModalPopupType.Form;
            this._EnrollInInstructorLedTrainingModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            this._EnrollInInstructorLedTrainingModal.HeaderIconAlt = _GlobalResources.EnrollInInstructorLedTrainingSession;
            this._EnrollInInstructorLedTrainingModal.HeaderText = _GlobalResources.EnrollInInstructorLedTrainingSession;
            this._EnrollInInstructorLedTrainingModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._EnrollInInstructorLedTrainingModal.SubmitButtonCustomText = _GlobalResources.Enroll;
            this._EnrollInInstructorLedTrainingModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._EnrollInInstructorLedTrainingModal.TargetControlID = this._EnrollInInstructorLedTrainingModalHiddenLaunchButton.ID;
            this._EnrollInInstructorLedTrainingModal.SubmitButton.Command += new CommandEventHandler(this._EnrollInInstructorLedTraining_Command);
            this._EnrollInInstructorLedTrainingModal.SubmitButton.OnClientClick = "return GetSelectedInstructorLedTrainingInstanceListItem();";

            // build the modal body
            this._EnrollInInstructorLedTrainingModal.AddControlToBody(this._EnrollInInstructorLedTrainingDetailsPanel);
            this._EnrollInInstructorLedTrainingModal.AddControlToBody(this._EnrollInInstructorLedTrainingModalHiddenLoadButton);
            this._EnrollInInstructorLedTrainingModal.AddControlToBody(this._EnrollInInstructorLedTrainingData);
            this._EnrollInInstructorLedTrainingModal.AddControlToBody(this._EnrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceData);

            // add modal to container
            this.CatalogMainContainer.Controls.Add(this._EnrollInInstructorLedTrainingModal);
        }
        #endregion

        #region _BuildEnrollInCourseWithOneInstructorLedTrainingModal
        /// <summary>
        /// Builds the modal popup for enrolling into a single instructor led training session for a course
        /// </summary>
        private void _BuildEnrollInCourseWithOneInstructorLedTrainingModal()
        {
            // THE SUPPORTING ELEMENTS - TRIGGERS, HIDDEN FIELDS, ETC.

            // instansiate enroll in instructor led training controls
            this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLaunchButton = new Button();
            this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLaunchButton.ID = "EnrollInCourseWithOneInstructorLedTrainingModalHiddenLaunchButton";
            this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLaunchButton.Style.Add("display", "none");

            this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLoadButton = new Button();
            this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLoadButton.ID = "EnrollInCourseWithOneInstructorLedTrainingModalHiddenLoadButton";
            this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLoadButton.Style.Add("display", "none");
            this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLoadButton.Click += this._LoadEnrollInCourseWithOneInstructorLedTrainingModalContent;

            this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel = new Panel();
            this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.ID = "EnrollInCourseWithOneInstructorLedTrainingDetailsPanel";

            this._EnrollInCourseWithOneInstructorLedTrainingData = new HiddenField();
            this._EnrollInCourseWithOneInstructorLedTrainingData.ID = "EnrollInCourseWithOneInstructorLedTrainingData";

            this._EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceData = new HiddenField();
            this._EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceData.ID = "EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceData";

            // add trigger button to container
            this.CatalogMainContainer.Controls.Add(this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLaunchButton);

            // THE MODAL

            // set modal properties
            this._EnrollInCourseWithOneInstructorLedTrainingModal = new ModalPopup("EnrollInCourseWithOneInstructorLedTrainingModal");
            this._EnrollInCourseWithOneInstructorLedTrainingModal.Type = ModalPopupType.Form;
            this._EnrollInCourseWithOneInstructorLedTrainingModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            this._EnrollInCourseWithOneInstructorLedTrainingModal.HeaderIconAlt = _GlobalResources.EnrollInInstructorLedTrainingSession;
            this._EnrollInCourseWithOneInstructorLedTrainingModal.HeaderText = _GlobalResources.EnrollInCourseAndInstructorLedTrainingSession;
            this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButtonCustomText = _GlobalResources.Enroll;
            this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._EnrollInCourseWithOneInstructorLedTrainingModal.TargetControlID = this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLaunchButton.ID;
            this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Command += new CommandEventHandler(this._EnrollInCourseWithOneInstructorLedTraining_Command);
            this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.OnClientClick = "return GetSelectedSingleInstructorLedTrainingInstanceListItem();";

            // build the modal body
            this._EnrollInCourseWithOneInstructorLedTrainingModal.AddControlToBody(this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel);
            this._EnrollInCourseWithOneInstructorLedTrainingModal.AddControlToBody(this._EnrollInCourseWithOneInstructorLedTrainingModalHiddenLoadButton);
            this._EnrollInCourseWithOneInstructorLedTrainingModal.AddControlToBody(this._EnrollInCourseWithOneInstructorLedTrainingData);
            this._EnrollInCourseWithOneInstructorLedTrainingModal.AddControlToBody(this._EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceData);

            // add modal to container
            this.CatalogMainContainer.Controls.Add(this._EnrollInCourseWithOneInstructorLedTrainingModal);
        }
        #endregion

        #region _BuildJoinUnjoinCommunityModal
        /// <summary>
        /// Builds the modal popup for joining or unjoining user to/from a community.
        /// </summary>
        private void _BuildJoinUnjoinCommunityModal()
        {
            // THE SUPPORTING ELEMENTS - TRIGGERS, HIDDEN FIELDS, ETC.

            // instansiate enroll in instructor led training controls
            this._JoinUnjoinCommunityModalHiddenLaunchButton = new Button();
            this._JoinUnjoinCommunityModalHiddenLaunchButton.ID = "JoinUnjoinCommunityModalHiddenLaunchButton";
            this._JoinUnjoinCommunityModalHiddenLaunchButton.Style.Add("display", "none");

            this._JoinUnjoinCommunityModalHiddenLoadButton = new Button();
            this._JoinUnjoinCommunityModalHiddenLoadButton.ID = "JoinUnjoinCommunityModalHiddenLoadButton";
            this._JoinUnjoinCommunityModalHiddenLoadButton.Style.Add("display", "none");
            this._JoinUnjoinCommunityModalHiddenLoadButton.Command += this._JoinUnjoinCommunity_Command;

            this._JoinUnjoinCommunityModalDetailsPanel = new Panel();
            this._JoinUnjoinCommunityModalDetailsPanel.ID = "JoinUnjoinCommunityModalDetailsPanel";

            this._JoinUnjoinCommunityData = new HiddenField();
            this._JoinUnjoinCommunityData.ID = "JoinUnjoinCommunityData";

            // add trigger button to container
            this.CatalogMainContainer.Controls.Add(this._JoinUnjoinCommunityModalHiddenLaunchButton);

            // THE MODAL

            // set modal properties
            this._JoinUnjoinCommunityModal = new ModalPopup("JoinUnjoinCommunityModal");
            this._JoinUnjoinCommunityModal.Type = ModalPopupType.Form;
            this._JoinUnjoinCommunityModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            this._JoinUnjoinCommunityModal.HeaderIconAlt = _GlobalResources.JoinUnjoinCommunity;
            this._JoinUnjoinCommunityModal.HeaderText = _GlobalResources.JoinUnjoinCommunity;
            this._JoinUnjoinCommunityModal.SubmitButton.Visible = false;
            this._JoinUnjoinCommunityModal.CloseButton.Visible = false;

            this._JoinUnjoinCommunityModal.TargetControlID = this._JoinUnjoinCommunityModalHiddenLaunchButton.ID;

            // build the modal body
            this._JoinUnjoinCommunityModal.AddControlToBody(this._JoinUnjoinCommunityModalDetailsPanel);
            this._JoinUnjoinCommunityModal.AddControlToBody(this._JoinUnjoinCommunityModalHiddenLoadButton);
            this._JoinUnjoinCommunityModal.AddControlToBody(this._JoinUnjoinCommunityData);

            // add modal to container
            this.CatalogMainContainer.Controls.Add(this._JoinUnjoinCommunityModal);
        }
        #endregion

        #region _BuildApplyCouponPayPalBuyNowModal
        /// <summary>
        /// Builds the modal popup for applying coupon/buy now for PayPal purchase.
        /// </summary>
        private void _BuildApplyCouponPayPalBuyNowModal()
        {
            // THE SUPPORTING ELEMENTS - TRIGGERS, HIDDEN FIELDS, ETC.

            // instansiate enroll in course controls
            this._ApplyCouponPayPalBuyNowModalHiddenLaunchButton = new Button();
            this._ApplyCouponPayPalBuyNowModalHiddenLaunchButton.ID = "ApplyCouponPayPalBuyNowModalHiddenLaunchButton";
            this._ApplyCouponPayPalBuyNowModalHiddenLaunchButton.Style.Add("display", "none");

            this._ApplyCouponPayPalBuyNowModalDetailsPanel = new Panel();
            this._ApplyCouponPayPalBuyNowModalDetailsPanel.ID = "ApplyCouponPayPalBuyNowModalDetailsPanel";

            this._ApplyCouponPayPalBuyNowData = new HiddenField();
            this._ApplyCouponPayPalBuyNowData.ID = "ApplyCouponPayPalBuyNowData";

            this._ApplyCouponPayPalCouponCodeData = new HiddenField();
            this._ApplyCouponPayPalCouponCodeData.ID = "ApplyCouponPayPalCouponCodeData";

            // add trigger button to container
            this.CatalogMainContainer.Controls.Add(this._ApplyCouponPayPalBuyNowModalHiddenLaunchButton);

            // THE MODAL

            // set modal properties
            this._ApplyCouponPayPalBuyNowModal = new ModalPopup("ApplyCouponPayPalBuyNowModal");
            this._ApplyCouponPayPalBuyNowModal.Type = ModalPopupType.Form;
            this._ApplyCouponPayPalBuyNowModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PAYPAL, ImageFiles.EXT_PNG);
            this._ApplyCouponPayPalBuyNowModal.HeaderIconAlt = _GlobalResources.BuyNow;
            this._ApplyCouponPayPalBuyNowModal.HeaderText = _GlobalResources.BuyNow;
            this._ApplyCouponPayPalBuyNowModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._ApplyCouponPayPalBuyNowModal.SubmitButtonCustomText = _GlobalResources.BuyNow;
            this._ApplyCouponPayPalBuyNowModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._ApplyCouponPayPalBuyNowModal.TargetControlID = this._ApplyCouponPayPalBuyNowModalHiddenLaunchButton.ID;
            this._ApplyCouponPayPalBuyNowModal.SubmitButton.Command += new CommandEventHandler(this._ApplyCouponCodePayPalBuyNow_Command);
            this._ApplyCouponPayPalBuyNowModal.SubmitButton.OnClientClick = "return GetCouponCodeInput();";

            // build the modal body
            this._ApplyCouponPayPalBuyNowModal.AddControlToBody(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
            this._ApplyCouponPayPalBuyNowModal.AddControlToBody(this._ApplyCouponPayPalBuyNowData);
            this._ApplyCouponPayPalBuyNowModal.AddControlToBody(this._ApplyCouponPayPalCouponCodeData);

            // add modal to container
            this.CatalogMainContainer.Controls.Add(this._ApplyCouponPayPalBuyNowModal);
        }
        #endregion

        #region _BuildCourseSampleScreensModal
        /// <summary>
        /// Builds the modal popup for displaying course sample screens.
        /// </summary>
        private void _BuildCourseSampleScreensModal()
        {
            // THE SUPPORTING ELEMENTS - TRIGGERS, HIDDEN FIELDS, ETC.

            // instansiate enroll in course controls
            this._CourseSampleScreensModalHiddenLaunchButton = new Button();
            this._CourseSampleScreensModalHiddenLaunchButton.ID = "CourseSampleScreensModalHiddenLaunchButton";
            this._CourseSampleScreensModalHiddenLaunchButton.Style.Add("display", "none");                        

            // add trigger button to container
            this.CatalogMainContainer.Controls.Add(this._CourseSampleScreensModalHiddenLaunchButton);

            // THE MODAL

            // set modal properties
            this._CourseSampleScreensModal = new ModalPopup("CourseSampleScreensModal");
            this._CourseSampleScreensModal.Type = ModalPopupType.Information;
            this._CourseSampleScreensModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_IMAGE, ImageFiles.EXT_PNG);
            this._CourseSampleScreensModal.HeaderIconAlt = _GlobalResources.SampleScreens;
            this._CourseSampleScreensModal.HeaderText = _GlobalResources.SampleScreens;
            this._CourseSampleScreensModal.SubmitButton.Visible = false;
            this._CourseSampleScreensModal.CloseButton.Visible = false;

            this._CourseSampleScreensModal.TargetControlID = this._CourseSampleScreensModalHiddenLaunchButton.ID;

            // build the modal body
            Panel courseSampleScreensModalContentPanel = new Panel();
            courseSampleScreensModalContentPanel.ID = "CourseSampleScreensModalContentPanel";
            this._CourseSampleScreensModal.AddControlToBody(courseSampleScreensModalContentPanel);            

            // add modal to container
            this.CatalogMainContainer.Controls.Add(this._CourseSampleScreensModal);
        }
        #endregion

        #region BuildCatalogTiles
        public static void BuildCatalogTiles(Panel destinationPanel, int? idParentCatalog)
        {
            DataTable catalogs;

            if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_SHOW_PRIVATECOURSECATALOGS))
            {
                catalogs = LMS.Library.Catalog.GetCatalogs(idParentCatalog, false);
            }
            else
            { 
                catalogs = LMS.Library.Catalog.GetCatalogs(idParentCatalog); 
            }

            if (catalogs.Rows.Count > 0)
            {
                // first, do the header tile
                destinationPanel.Controls.Add(BuildTile(CatalogTileType.CatalogsHeader, null, false, 0, null, _GlobalResources.Catalogs, null, null, null, false, false, null, null));

                // now, do the tiles for each object  
                string lastTileId = String.Empty;

                foreach (DataRow row in catalogs.Rows)
                {                    
                    int idCatalog = Convert.ToInt32(row["idCatalog"]);
                    string title = HttpContext.Current.Server.HtmlEncode(row["title"].ToString());
                    string avatar = row["avatar"].ToString();
                    string numCatalogs = row["numCatalogs"].ToString();
                    string numCourses = row["numCourses"].ToString();
                    bool isPrivate = Convert.ToBoolean(row["isPrivate"]);
                    bool isAccessible = Convert.ToBoolean(row["isAccessible"]);
                    bool isClosed = Convert.ToBoolean(row["isClosed"]);
                    string calculatedCost = row["calculatedCost"].ToString();                    

                    if (
                        (!isPrivate)
                        ||
                        (isAccessible)
                        ||
                        (isPrivate && !isAccessible && AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_SHOW_PRIVATECOURSECATALOGS) == true)
                       )
                    {
                        lastTileId = idCatalog.ToString();
                        destinationPanel.Controls.Add(BuildTile(CatalogTileType.Catalog, idParentCatalog, !isAccessible, idCatalog, null, title, avatar, null, calculatedCost, isPrivate, isClosed, numCatalogs, numCourses));
                    }
                }

                // set a class on the last tile in the category
                Panel lastTile = (Panel)FindControlRecursive(destinationPanel, "CatalogTile_" + lastTileId);

                if (lastTile != null)
                { lastTile.CssClass += " LastTileInCategory"; }                
            }
        }
        #endregion

        #region BuildCourseTiles
        public static void BuildCourseTiles(Panel destinationPanel, int? idParentCatalog)
        {
            DataTable courses = LMS.Library.Catalog.GetCourses(idParentCatalog);

            if (courses.Rows.Count > 0)
            {
                // first, do the header tile
                destinationPanel.Controls.Add(BuildTile(CatalogTileType.CoursesHeader, null, false, 0, null, _GlobalResources.Courses, null, null, null, false, false, null, null));

                // now, do the tiles for each object
                string lastTileId = String.Empty;

                foreach (DataRow row in courses.Rows)
                {
                    int idCourse = Convert.ToInt32(row["idCourse"]);
                    string title = HttpContext.Current.Server.HtmlEncode(row["title"].ToString());
                    string avatar = row["avatar"].ToString();
                    string code = HttpContext.Current.Server.HtmlEncode(row["coursecode"].ToString());
                    string credits = HttpContext.Current.Server.HtmlEncode(row["credits"].ToString());
                    bool isClosed = Convert.ToBoolean(row["isClosed"]);
                    string cost = row["cost"].ToString();
                    bool isCallerEnrolled = Convert.ToBoolean(row["isCallerEnrolled"]);

                    lastTileId = idCourse.ToString();
                    destinationPanel.Controls.Add(BuildTile(CatalogTileType.Course, idParentCatalog, false, idCourse, code, title, avatar, credits, cost, false, isClosed, null, null));
                }

                // set a class on the last tile in the category                
                Panel lastTile = (Panel)FindControlRecursive(destinationPanel, "CourseTile_" + lastTileId);

                if (lastTile != null)
                { lastTile.CssClass += " LastTileInCategory"; }
            }
        }
        #endregion

        #region BuildLearningPathTiles
        public static void BuildLearningPathTiles(Panel destinationPanel, int? idParentCatalog)
        {
            DataTable learningPaths = LMS.Library.Catalog.GetLearningPaths(idParentCatalog);

            if (learningPaths.Rows.Count > 0)
            {
                // first, do the header tile
                destinationPanel.Controls.Add(BuildTile(CatalogTileType.LearningPathsHeader, null, false, 0, null, _GlobalResources.LearningPaths, null, null, null, false, false, null, null));

                // now, do the tiles for each object
                string lastTileId = String.Empty;

                foreach (DataRow row in learningPaths.Rows)
                {
                    int idLearningPath = Convert.ToInt32(row["idLearningPath"]);
                    string name = HttpContext.Current.Server.HtmlEncode(row["name"].ToString());
                    string avatar = row["avatar"].ToString();
                    bool isClosed = Convert.ToBoolean(row["isClosed"]);
                    string cost = row["cost"].ToString();
                    string numCourses = row["numCourses"].ToString();
                    bool isCallerEnrolled = Convert.ToBoolean(row["isCallerEnrolled"]);

                    lastTileId = idLearningPath.ToString();
                    destinationPanel.Controls.Add(BuildTile(CatalogTileType.LearningPath, idParentCatalog, false, idLearningPath, null, name, avatar, null, cost, false, isClosed, null, numCourses));
                }

                // set a class on the last tile in the category                
                Panel lastTile = (Panel)FindControlRecursive(destinationPanel, "LearningPathTile_" + lastTileId);

                if (lastTile != null)
                { lastTile.CssClass += " LastTileInCategory"; }
            }
        }
        #endregion

        #region BuildInstructorLedTrainingTiles
        public static void BuildInstructorLedTrainingTiles(Panel destinationPanel)
        {
            DataTable instructorLedTrainings = LMS.Library.Catalog.GetInstructorLedTrainingModules();

            if (instructorLedTrainings.Rows.Count > 0)
            {
                // first, do the header tile
                destinationPanel.Controls.Add(BuildTile(CatalogTileType.InstructorLedTrainingHeader, null, false, 0, null, _GlobalResources.InstructorLedTraining, null, null, null, false, false, null, null));

                // now, do the tiles for each object
                string lastTileId = String.Empty;

                foreach (DataRow row in instructorLedTrainings.Rows)
                {
                    int idStandupTraining = Convert.ToInt32(row["idStandupTraining"]);
                    string title = HttpContext.Current.Server.HtmlEncode(row["title"].ToString());
                    string avatar = row["avatar"].ToString();
                    string cost = row["cost"].ToString();
                    bool isOrWasCallerEnrolledInASession = Convert.ToBoolean(row["isOrWasCallerEnrolledInASession"]);

                    lastTileId = idStandupTraining.ToString();
                    destinationPanel.Controls.Add(BuildTile(CatalogTileType.InstructorLedTraining, null, false, idStandupTraining, null, title, avatar, null, cost, false, false, null, null));
                }

                // set a class on the last tile in the category                
                Panel lastTile = (Panel)FindControlRecursive(destinationPanel, "InstructorLedTrainingTile_" + lastTileId);

                if (lastTile != null)
                { lastTile.CssClass += " LastTileInCategory"; }
            }
        }
        #endregion

        #region BuildCommunityTiles
        public static void BuildCommunityTiles(Panel destinationPanel)
        {
            DataTable communities = LMS.Library.Catalog.GetCommunities();

            if (communities.Rows.Count > 0)
            {
                // first, do the header tile
                destinationPanel.Controls.Add(BuildTile(CatalogTileType.CommunitiesHeader, null, false, 0, null, _GlobalResources.Communities, null, null, null, false, false, null, null));

                // now, do the tiles for each object
                string lastTileId = String.Empty;

                foreach (DataRow row in communities.Rows)
                {
                    int idGroup = Convert.ToInt32(row["idGroup"]);
                    string name = HttpContext.Current.Server.HtmlEncode(row["name"].ToString());
                    string avatar = row["avatar"].ToString();
                    bool isFeedActive = Convert.ToBoolean(row["isFeedActive"]);
                    bool isCallerAlreadyAMember = Convert.ToBoolean(row["isCallerAlreadyAMember"]);

                    lastTileId = idGroup.ToString();
                    destinationPanel.Controls.Add(BuildTile(CatalogTileType.Community, null, false, idGroup, null, name, avatar, null, null, false, false, null, null));
                }

                // set a class on the last tile in the category
                Panel lastTile = (Panel)FindControlRecursive(destinationPanel, "CommunityTile_" + lastTileId);

                if (lastTile != null)
                { lastTile.CssClass += " LastTileInCategory"; }
            }
        }
        #endregion

        #region BuildTile
        /// <summary>
        /// Builds catalog tile for object (catalog, course, learning path, ilt, or community).
        /// </summary>
        /// <param name="tileType">type of tile</param>
        /// <param name="isDisabled">should tile be disabled (i.e. user cannot access)</param>
        /// <param name="objectId">id of the object</param>
        /// <param name="objectCode">object code (applicable to course)</param>
        /// <param name="objectTitle">object title</param>
        /// <param name="objectAvatar">object avatar</param>
        /// <param name="objectCredits">object credits (applicable to course)</param>
        /// <param name="objectCost">object cost (applicable to catalog and course)</param>
        /// <param name="isPrivate">is the object private (applicable to catalog)</param>
        /// <param name="isClosed">is the object closed (applicable to course)</param>
        /// <param name="numCatalogs">number of child catalogs (applicable to catalog)</param>
        /// <param name="numCourses">number of child courses (applicable to catalog)</param>
        /// <returns></returns>
        public static Panel BuildTile(CatalogTileType tileType, int? parentCatalogId, bool isDisabled, int objectId, string objectCode, string objectTitle, string objectAvatar, string objectCredits, string objectCost, bool isPrivate, bool isClosed, string numCatalogs, string numCourses)
        {
            // get the ecommerce settings
            EcommerceSettings ecommerceSettings = new EcommerceSettings();

            string idPrefix = String.Empty;
            string tileSpecificClass = String.Empty;
            string onClickAction = String.Empty;
            string avatarPath = String.Empty;
            string avatarImageClass = String.Empty;
            bool isHeaderTile = false;

            // format a string for the parent catalog id value to make it work for the JS string we are building
            string parentCatalogIdText = "null";

            if (parentCatalogId != null)
            { parentCatalogIdText = parentCatalogId.ToString(); }

            // set properties for the specified tile type
            switch (tileType)
            {
                case CatalogTileType.CatalogsHeader:
                    isHeaderTile = true;
                    idPrefix = "CatalogsHeader";
                    tileSpecificClass = "HeaderTile CatalogsHeaderTile";
                    break;
                case CatalogTileType.Catalog:
                    idPrefix = "Catalog";
                    tileSpecificClass = "CatalogTile";
                    onClickAction = "ShowCatalogDetails(" + objectId + ", \"forward\", true);";

                    if (!String.IsNullOrWhiteSpace(objectAvatar))
                    {
                        avatarPath = SitePathConstants.SITE_CATALOGS_ROOT + objectId.ToString() + "/" + objectAvatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        avatarImageClass = "AvatarImage";
                    }
                    else
                    { avatarPath = ImageFiles.GetAvatarPath(ImageFiles.AVATAR_CATALOG_CATALOG, ImageFiles.EXT_PNG); }

                    break;
                case CatalogTileType.CoursesHeader:
                    isHeaderTile = true;
                    idPrefix = "CoursesHeader";
                    tileSpecificClass = "HeaderTile CoursesHeaderTile";
                    break;
                case CatalogTileType.Course:
                    idPrefix = "Course";
                    tileSpecificClass = "CourseTile";
                    onClickAction = "ShowCourseDetails(" + objectId + ", \"forward\", true);";

                    if (!String.IsNullOrWhiteSpace(objectAvatar))
                    {
                        avatarPath = SitePathConstants.SITE_COURSES_ROOT + objectId.ToString() + "/" + objectAvatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");                        
                        avatarImageClass = "AvatarImage";
                    }
                    else                    
                    { avatarPath = ImageFiles.GetAvatarPath(ImageFiles.AVATAR_CATALOG_COURSE, ImageFiles.EXT_PNG); }

                    break;
                case CatalogTileType.LearningPathsHeader:
                    isHeaderTile = true;
                    idPrefix = "LearningPathsHeader";
                    tileSpecificClass = "HeaderTile LearningPathsHeaderTile";
                    break;
                case CatalogTileType.LearningPath:
                    idPrefix = "LearningPath";
                    tileSpecificClass = "LearningPathTile";
                    onClickAction = "ShowLearningPathDetails(" + objectId + ", \"forward\", true);";

                    if (!String.IsNullOrWhiteSpace(objectAvatar))
                    {
                        avatarPath = SitePathConstants.SITE_LEARNINGPATHS_ROOT + objectId.ToString() + "/" + objectAvatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        avatarImageClass = "AvatarImage";
                    }
                    else                    
                    { avatarPath = ImageFiles.GetAvatarPath(ImageFiles.AVATAR_CATALOG_LEARNINGPATH, ImageFiles.EXT_PNG); }

                    break;
                case CatalogTileType.InstructorLedTrainingHeader:
                    isHeaderTile = true;
                    idPrefix = "InstructorLedTrainingHeader";
                    tileSpecificClass = "HeaderTile InstructorLedTrainingHeaderTile";
                    break;
                case CatalogTileType.InstructorLedTraining:
                    idPrefix = "InstructorLedTraining";
                    tileSpecificClass = "InstructorLedTrainingTile";
                    onClickAction = "ShowInstructorLedTrainingDetails(" + objectId + ", \"forward\", true);";

                    if (!String.IsNullOrWhiteSpace(objectAvatar))
                    {
                        avatarPath = SitePathConstants.SITE_STANDUPTRAINING_ROOT + objectId.ToString() + "/" + objectAvatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        avatarImageClass = "AvatarImage";
                    }
                    else                    
                    { avatarPath = ImageFiles.GetAvatarPath(ImageFiles.AVATAR_CATALOG_ILT, ImageFiles.EXT_PNG); }

                    break;
                case CatalogTileType.CommunitiesHeader:
                    isHeaderTile = true;
                    idPrefix = "CommunitiesHeader";
                    tileSpecificClass = "HeaderTile CommunitiesHeaderTile";
                    break;
                case CatalogTileType.Community:
                    idPrefix = "Community";
                    tileSpecificClass = "CommunityTile";
                    onClickAction = "ShowCommunityDetails(" + objectId + ", \"forward\", true);";

                    if (!String.IsNullOrWhiteSpace(objectAvatar))
                    {
                        avatarPath = SitePathConstants.SITE_GROUPS_ROOT + objectId.ToString() + "/" + objectAvatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        avatarImageClass = "AvatarImage";
                    }
                    else
                    { avatarPath = ImageFiles.GetAvatarPath(ImageFiles.AVATAR_CATALOG_COMMUNITY, ImageFiles.EXT_PNG); }

                    break;
                default:
                    break;
            }

            // tile wrapper
            Panel tile = new Panel();
            tile.ID = idPrefix + "Tile_" + objectId.ToString();            

            if (!isHeaderTile)
            {
                tile.CssClass = "Tile " + tileSpecificClass;
                tile.Attributes.Add("onclick", onClickAction);

                if (isDisabled)
                {
                    tile.CssClass += " DisabledTile";
                    tile.Attributes.Remove("onclick");
                }
            }
            else
            { tile.CssClass = tileSpecificClass; }

            // if it's not a header tile, build all of the inner tile elements
            if (!isHeaderTile)
            {
                // tile content outer wrapper
                Panel tileOuterWrapper = new Panel();
                tileOuterWrapper.ID = idPrefix + "TileContentOuterWrapper_" + objectId.ToString();
                tileOuterWrapper.CssClass = "TileContentOuterWrapper";
                tile.Controls.Add(tileOuterWrapper);

                // tile content inner wrapper
                Panel tileInnerWrapper = new Panel();
                tileInnerWrapper.ID = idPrefix + "TileContentInnerWrapper_" + objectId.ToString();
                tileInnerWrapper.CssClass = "TileContentInnerWrapper";
                tileOuterWrapper.Controls.Add(tileInnerWrapper);

                // tile image panel
                Panel tileImage = new Panel();
                tileImage.ID = idPrefix + "TileImage_" + objectId.ToString();
                tileImage.CssClass = "TileImage";
                tileInnerWrapper.Controls.Add(tileImage);

                // tile content panel
                Panel tileContent = new Panel();
                tileContent.ID = idPrefix + "TileContent_" + objectId.ToString();
                tileContent.CssClass = "TileContent";
                tileInnerWrapper.Controls.Add(tileContent);

                // tile content top
                Panel tileContentTop = new Panel();
                tileContentTop.ID = idPrefix + "TileContentTop" + objectId.ToString();
                tileContentTop.CssClass = "TileContentTop";
                tileContent.Controls.Add(tileContentTop);

                // tile content middle
                Panel tileContentMiddle = new Panel();
                tileContentMiddle.ID = idPrefix + "TileContentMiddle" + objectId.ToString();
                tileContentMiddle.CssClass = "TileContentMiddle";
                tileContent.Controls.Add(tileContentMiddle);

                // tile content bottom
                Panel tileContentBottom = new Panel();
                tileContentBottom.ID = idPrefix + "TileContentBottom" + objectId.ToString();
                tileContentBottom.CssClass = "TileContentBottom";
                tileContent.Controls.Add(tileContentBottom);

                // tile content bottom left
                Panel tileContentBottomLeft = new Panel();
                tileContentBottomLeft.ID = idPrefix + "TileContentBottomLeft" + objectId.ToString();
                tileContentBottomLeft.CssClass = "TileContentBottomLeft";
                tileContentBottom.Controls.Add(tileContentBottomLeft);

                // tile content bottom right
                Panel tileContentBottomRight = new Panel();
                tileContentBottomRight.ID = idPrefix + "TileContentBottomRight" + objectId.ToString();
                tileContentBottomRight.CssClass = "TileContentBottomRight";
                tileContentBottom.Controls.Add(tileContentBottomRight);

                // object avatar, if not a header tile                
                Panel objectAvatarPanel = new Panel();

                Image objectAvatarImage = new Image();
                objectAvatarImage.AlternateText = objectTitle;
                objectAvatarImage.ImageUrl = avatarPath;                

                if (!String.IsNullOrWhiteSpace(avatarImageClass))
                { objectAvatarImage.CssClass += " " + avatarImageClass; }

                objectAvatarPanel.Controls.Add(objectAvatarImage);
                tileImage.Controls.Add(objectAvatarPanel);

                // if course and closed, show lock icon (TileContentTop)
                if (tileType == CatalogTileType.Course && isClosed)
                {
                    Image lockIcon = new Image();
                    lockIcon.CssClass = "ObjectLockedImage";
                    lockIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_WHITE, ImageFiles.EXT_PNG);
                    lockIcon.AlternateText = (tileType == CatalogTileType.Catalog) ? _GlobalResources.Private : _GlobalResources.Closed;
                    lockIcon.ToolTip = _GlobalResources.Closed;
                    tileContentTop.Controls.Add(lockIcon);
                }

                // object code (course specific)
                if (
                    tileType == CatalogTileType.Course
                    && AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_HIDE_COURSE_CODES) != true
                    && !String.IsNullOrWhiteSpace(objectCode)
                    )
                {
                    Panel objectCodeWrapper = new Panel();
                    objectCodeWrapper.CssClass = "TileSubTitle";
                    tileContentMiddle.Controls.Add(objectCodeWrapper);

                    Literal objectCodeText = new Literal();
                    objectCodeText.Text = objectCode;
                    objectCodeWrapper.Controls.Add(objectCodeText);
                }

                // object title
                Panel objectTitlePanel = new Panel();
                objectTitlePanel.CssClass = "TileTitle";

                Literal objectTitleText = new Literal();

                if (objectTitle.Length <= MAX_CHARACTERS_IN_TITLE_BEFORE_ELLIPSE)
                { objectTitleText.Text = objectTitle; }
                else
                { objectTitleText.Text = objectTitle.Substring(0, MAX_CHARACTERS_IN_TITLE_BEFORE_ELLIPSE) + "&hellip;"; }

                objectTitlePanel.Controls.Add(objectTitleText);
                tileContentMiddle.Controls.Add(objectTitlePanel);
                
                // if catalog, show number of child courses and catalogs (bottom left)
                if (tileType == CatalogTileType.Catalog)
                {
                    // num catalogs
                    HtmlGenericControl catalogsSpan = new HtmlGenericControl("span");

                    Image catalogsImage = new Image();
                    catalogsImage.CssClass = "TileImageSmall";
                    catalogsImage.AlternateText = _GlobalResources.Catalog;
                    catalogsImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG_WHITE, ImageFiles.EXT_PNG);

                    Literal numberCatalogsText = new Literal();
                    numberCatalogsText.Text = " " + numCatalogs;

                    catalogsSpan.Controls.Add(catalogsImage);
                    catalogsSpan.Controls.Add(numberCatalogsText);
                    tileContentBottomLeft.Controls.Add(catalogsSpan);

                    // num courses
                    HtmlGenericControl coursesSpan = new HtmlGenericControl("span");

                    Image coursesImage = new Image();
                    coursesImage.CssClass = "TileImageSmall";
                    coursesImage.AlternateText = _GlobalResources.Course;
                    coursesImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_WHITE, ImageFiles.EXT_PNG);

                    Literal numberCoursesText = new Literal();
                    numberCoursesText.Text = " " + numCourses;

                    coursesSpan.Controls.Add(coursesImage);
                    coursesSpan.Controls.Add(numberCoursesText);
                    tileContentBottomLeft.Controls.Add(coursesSpan);
                }

                // if learning path, show number of courses (bottom left)
                if (tileType == CatalogTileType.LearningPath)
                {
                    // num courses
                    HtmlGenericControl coursesSpan = new HtmlGenericControl("span");

                    Image coursesImage = new Image();
                    coursesImage.CssClass = "TileImageSmall";
                    coursesImage.AlternateText = _GlobalResources.Course;
                    coursesImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_WHITE, ImageFiles.EXT_PNG);

                    Literal numberCoursesText = new Literal();
                    numberCoursesText.Text = " " + numCourses;

                    coursesSpan.Controls.Add(coursesImage);
                    coursesSpan.Controls.Add(numberCoursesText);
                    tileContentBottomLeft.Controls.Add(coursesSpan);
                }

                // if course and has credits, show credits (bottom left)
                if (tileType == CatalogTileType.Course && !String.IsNullOrWhiteSpace(objectCredits))
                {
                    Literal creditsText = new Literal();
                    creditsText.Text = "Cr: " + objectCredits;
                    tileContentBottomLeft.Controls.Add(creditsText);
                }

                // if site uses e-commerce and object is course, catalog, learning path, or ilt and not closed, show cost (bottom right)
                if (
                    (ecommerceSettings.IsEcommerceSetAndVerified)
                    && (tileType == CatalogTileType.Catalog || tileType == CatalogTileType.Course || tileType == CatalogTileType.LearningPath || tileType == CatalogTileType.InstructorLedTraining)
                    && (!isClosed)
                    )
                {
                    Literal objectCostText = new Literal();

                    if (!String.IsNullOrWhiteSpace(objectCost) && objectCost != "0.00")
                    {
                        objectCostText.Text = String.Format("{0}{1}", ecommerceSettings.CurrencySymbol, objectCost, ecommerceSettings.CurrencyCode);
                    }
                    else
                    { objectCostText.Text = _GlobalResources.Free; }

                    tileContentBottomRight.Controls.Add(objectCostText);
                }
            }
            else // it's a header tile, so just add the title to it
            {
                // header tile title                
                Literal objectTitleText = new Literal();

                if (objectTitle.Length <= MAX_CHARACTERS_IN_TITLE_BEFORE_ELLIPSE)
                { objectTitleText.Text = objectTitle; }
                else
                { objectTitleText.Text = objectTitle.Substring(0, MAX_CHARACTERS_IN_TITLE_BEFORE_ELLIPSE) + "&hellip;"; }

                tile.Controls.Add(objectTitleText);
            }

            return tile;
        }
        #endregion

        #region CatalogDetailsJsonDataStruct
        public struct CatalogDetailsJsonData
        {
            public bool actionSuccessful;
            public string html;
            public string exception;
            public int? idObject;
            public string objectTitle;
        }
        #endregion

        #region BuildBackButtonContainer
        /// <summary>
        /// Builds the back button container and link for a details view.
        /// 
        /// The onclick action of the link will be populated dynamically via the ProcessBreadcrumb JS method.
        /// </summary>
        /// <returns></returns>
        public static Panel BuildBackButtonContainer()
        {
            // container
            Panel detailsViewBackButtonContainer = new Panel();
            detailsViewBackButtonContainer.ID = "DetailsViewBackButtonContainer";
            detailsViewBackButtonContainer.CssClass = "";

            // link
            HyperLink detailsViewBackButtonLink = new HyperLink();
            detailsViewBackButtonLink.ID = "DetailsViewBackButtonLink";
            detailsViewBackButtonLink.CssClass = "ImageLink DetailsViewBackButtonLink";
            detailsViewBackButtonLink.NavigateUrl = "javascript:void(0);";
            detailsViewBackButtonContainer.Controls.Add(detailsViewBackButtonLink);

            // link image
            Image detailsViewBackButtonImage = new Image();
            detailsViewBackButtonImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_BACK, ImageFiles.EXT_PNG);
            detailsViewBackButtonImage.AlternateText = _GlobalResources.Back;
            detailsViewBackButtonLink.Controls.Add(detailsViewBackButtonImage);

            // link text
            Literal detailsViewBackButtonText = new Literal();
            detailsViewBackButtonText.Text = _GlobalResources.Back;
            detailsViewBackButtonLink.Controls.Add(detailsViewBackButtonText);

            // return
            return detailsViewBackButtonContainer;
        }
        #endregion

        #region BuildCatalogDetailsPanel
        [WebMethod(EnableSession = true)]
        public static CatalogDetailsJsonData BuildCatalogDetailsPanel(int idCatalog)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            CatalogDetailsJsonData jsonData = new CatalogDetailsJsonData();

            // get the ecommerce settings
            EcommerceSettings ecommerceSettings = new EcommerceSettings();

            try
            {
                // get the catalog object
                Asentia.LMS.Library.Catalog catalog = new Library.Catalog(idCatalog);

                // get the catalog's language-specific title and description
                string title = catalog.Title;
                string shortDescription = catalog.ShortDescription;
                string description = HttpUtility.HtmlDecode(catalog.LongDescription);
                if (!String.IsNullOrWhiteSpace(description)) { description = new System.Text.RegularExpressions.Regex("<span style=\"[^\"]*\">").Replace(description, ""); }

                foreach (Library.Catalog.LanguageSpecificProperty catalogLanguageSpecific in catalog.LanguageSpecificProperties)
                {
                    if (catalogLanguageSpecific.LangString == AsentiaSessionState.UserCulture)
                    {
                        title = catalogLanguageSpecific.Title;
                        shortDescription = catalogLanguageSpecific.ShortDescription;
                        description = HttpUtility.HtmlDecode(catalogLanguageSpecific.LongDescription);
                        break;
                    }
                }

                // BACK BUTTON - render control detailsViewBackButtonContainer

                Panel detailsViewBackButtonContainer = BuildBackButtonContainer();

                // CATALOG DETAILS VIEW LEFT SIDE CONTAINER - render control catalogDetailsViewLeftSideContainer

                Panel catalogDetailsViewLeftSideContainer = new Panel();
                catalogDetailsViewLeftSideContainer.ID = "Catalog_CatalogDetailsViewLeftSideContainer";
                catalogDetailsViewLeftSideContainer.CssClass = "";

                // CATALOG TITLE

                Panel catalogTitleContainer = new Panel();
                catalogTitleContainer.ID = "CatalogTitleContainer";
                catalogTitleContainer.CssClass = "DetailsViewTitleContainer";
                catalogDetailsViewLeftSideContainer.Controls.Add(catalogTitleContainer);

                Panel catalogTitleIconContainer = new Panel();
                catalogTitleIconContainer.ID = "CatalogTitleIconContainer";
                catalogTitleContainer.Controls.Add(catalogTitleIconContainer);

                Image catalogTitleIcon = new Image();
                catalogTitleIcon.CssClass = "LargeIcon";

                if (catalog.Avatar != null)
                {
                    catalogTitleIcon.CssClass += " AvatarImage";
                    catalogTitleIcon.ImageUrl = SitePathConstants.SITE_CATALOGS_ROOT + catalog.IdCatalog + "/" + catalog.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                }
                else
                { catalogTitleIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG, ImageFiles.EXT_PNG); }

                catalogTitleIconContainer.Controls.Add(catalogTitleIcon);

                HtmlGenericControl catalogTitle = new HtmlGenericControl("p");
                catalogTitle.InnerText = title;
                catalogTitleContainer.Controls.Add(catalogTitle);

                // CATALOG BASIC INFORMATION

                Panel catalogBasicInformationContainer = new Panel();
                catalogBasicInformationContainer.ID = "CatalogBasicInformationContainer";
                catalogBasicInformationContainer.CssClass = "DetailsViewBasicInformationContainer";
                catalogDetailsViewLeftSideContainer.Controls.Add(catalogBasicInformationContainer);

                // STATUS
                Panel catalogStatusWrapperContainer = new Panel();
                catalogStatusWrapperContainer.ID = "CatalogStatusWrapperContainer";
                catalogStatusWrapperContainer.CssClass = "DetailsViewStatusWrapperContainer";

                if (catalog.IsClosed == false)
                {
                    if (AsentiaSessionState.IdSiteUser == 0) // not logged in user
                    {
                        Button catalogLoginButton = new Button();
                        catalogLoginButton.ID = "CatalogLoginButton";
                        catalogLoginButton.Text = _GlobalResources.LogInToEnroll;
                        catalogLoginButton.OnClientClick = "$(\"#SettingsControlForLogin\").click(); return false;";
                        catalogLoginButton.CssClass = "Button ActionButton EnrollButton";
                        catalogStatusWrapperContainer.Controls.Add(catalogLoginButton);
                    }
                    else if (AsentiaSessionState.IdSiteUser == 1) // logged in user is administrator
                    {
                        Button catalogOpenButton = new Button();
                        catalogOpenButton.ID = "CatalogOpenButton";
                        catalogOpenButton.Text = _GlobalResources.Open;
                        catalogOpenButton.Enabled = false;
                        catalogOpenButton.CssClass = "Button ActionButton OpenButton";
                        catalogStatusWrapperContainer.Controls.Add(catalogOpenButton);
                    }
                    else // logged in user is a normal user
                    {
                        // do not check if user is enrolled, catalogs can be enrolled many times over

                        // make the proper "enroll" or "purchase" button based on ecommerce settings and cost
                        if (ecommerceSettings.IsEcommerceSetAndVerified && catalog.CalculatedCost > 0) // ecommerce is set and verified and catalog has a cost, make a purchase button
                        {
                            if (ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL) // paypal, make a buy now button
                            {
                                // Note: Buy Now button should always be available here. If any item is already in the active PayPal cart because of a previous
                                // purchase that was not followed through with PayPal, it will be cleared and replaced by the method handling the button click.

                                Button catalogPayPalButton = new Button();
                                catalogPayPalButton.ID = "CatalogPayPalButton";
                                catalogPayPalButton.Text = _GlobalResources.BuyNow;
                                catalogPayPalButton.OnClientClick = "EnrollCatalog(" + idCatalog + ", \"paypal\"); return false;";
                                catalogPayPalButton.CssClass = "Button ActionButton PayPalButton";
                                catalogStatusWrapperContainer.Controls.Add(catalogPayPalButton);

                                // if there are no courses included, disable the buttons
                                if (catalog.NumberEnrollableCourses == 0)
                                {
                                    catalogPayPalButton.OnClientClick = "";
                                    catalogPayPalButton.Enabled = false;
                                    catalogPayPalButton.CssClass = "Button ActionButton PayPalButton";
                                }
                            }
                            else // all other payment processors use the cart, make an add to cart button, or link to my cart if item is already there
                            {
                                if (!_IsItemInCart(idCatalog, PurchaseItemType.Catalog))
                                {
                                    Button catalogAddToCartButton = new Button();
                                    catalogAddToCartButton.ID = "CatalogAddToCartButton";
                                    catalogAddToCartButton.Text = _GlobalResources.AddToCart;
                                    catalogAddToCartButton.OnClientClick = "EnrollCatalog(" + idCatalog + ", \"addtocart\"); return false;";
                                    catalogAddToCartButton.CssClass = "Button ActionButton CartButton";
                                    catalogStatusWrapperContainer.Controls.Add(catalogAddToCartButton);                                    

                                    // if there are no courses included, disable the buttons
                                    if (catalog.NumberEnrollableCourses == 0)
                                    {
                                        catalogAddToCartButton.OnClientClick = "";
                                        catalogAddToCartButton.Enabled = false;
                                        catalogAddToCartButton.CssClass = "Button ActionButton CartButton";
                                    }
                                }
                                else // make my cart link
                                {
                                    Button catalogMyCartButton = new Button();
                                    catalogMyCartButton.ID = "CatalogMyCartButton";
                                    catalogMyCartButton.Text = _GlobalResources.GoToCart;                                    
                                    catalogMyCartButton.CssClass = "Button ActionButton CartButton";
                                    catalogMyCartButton.OnClientClick = "window.location = '/myprofile/MyCart.aspx'; return false;";
                                    catalogStatusWrapperContainer.Controls.Add(catalogMyCartButton);
                                }
                            }
                        }
                        else // just make an enroll button
                        {
                            Button catalogEnrollButton = new Button();
                            catalogEnrollButton.ID = "CatalogEnrollButton";
                            catalogEnrollButton.Text = _GlobalResources.Enroll;
                            catalogEnrollButton.OnClientClick = "EnrollCatalog(" + idCatalog + ", \"enroll\"); return false;";
                            catalogEnrollButton.CssClass = "Button ActionButton EnrollButton";
                            catalogStatusWrapperContainer.Controls.Add(catalogEnrollButton);

                            // if there are no courses included, disable the buttons
                            if (catalog.NumberEnrollableCourses == 0)
                            {
                                catalogEnrollButton.Enabled = false;
                                catalogEnrollButton.OnClientClick = "";
                                catalogEnrollButton.CssClass = "Button ActionButton EnrollButton";                                
                            }
                        }
                    }
                }
                else // catalog is closed, do nothing
                { }

                // attach status wrapper to basic information container
                catalogBasicInformationContainer.Controls.Add(catalogStatusWrapperContainer);

                // COST AND COURSES INCLUDED
                Panel catalogCostNumCoursesWrapperContainer = new Panel();
                catalogCostNumCoursesWrapperContainer.ID = "CatalogCostNumCoursesWrapperContainer";
                catalogCostNumCoursesWrapperContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                // if ecommerce is set and verified, show the cost
                if (ecommerceSettings.IsEcommerceSetAndVerified)
                {
                    Panel catalogCostContainer = new Panel();
                    catalogCostContainer.ID = "CatalogCostContainer";

                    Label catalogCostLabel = new Label();
                    catalogCostLabel.CssClass = "bold";
                    catalogCostLabel.Text = _GlobalResources.Cost + ": ";
                    catalogCostContainer.Controls.Add(catalogCostLabel);

                    Label catalogCostValueLabel = new Label();

                    if (catalog.CalculatedCost > 0)
                    { catalogCostValueLabel.Text = String.Format("{0}{1:N2} ({2})", ecommerceSettings.CurrencySymbol, catalog.CalculatedCost, ecommerceSettings.CurrencyCode); }
                    else
                    { catalogCostValueLabel.Text = _GlobalResources.Free; }

                    catalogCostContainer.Controls.Add(catalogCostValueLabel);

                    catalogCostNumCoursesWrapperContainer.Controls.Add(catalogCostContainer);
                }

                // number of courses included in the catalog
                Panel catalogCoursesIncludedContainer = new Panel();
                catalogCoursesIncludedContainer.ID = "CatalogCoursesIncludedContainer";

                Label catalogCoursesIncludedLabel = new Label();
                catalogCoursesIncludedLabel.CssClass = "bold";
                catalogCoursesIncludedLabel.Text = _GlobalResources.CoursesIncluded + ": ";
                catalogCoursesIncludedContainer.Controls.Add(catalogCoursesIncludedLabel);

                Label catalogCoursesIncludedValueLabel = new Label();
                catalogCoursesIncludedValueLabel.Text = catalog.NumberEnrollableCourses + " (" + _GlobalResources.ClosedCoursesWillNotBeIncluded + ")";
                catalogCoursesIncludedContainer.Controls.Add(catalogCoursesIncludedValueLabel);

                catalogCostNumCoursesWrapperContainer.Controls.Add(catalogCoursesIncludedContainer);

                // attach cost and courses included wrapper to basic information container
                catalogBasicInformationContainer.Controls.Add(catalogCostNumCoursesWrapperContainer);

                // CATALOG DESCRIPTION

                Panel catalogDescriptionContainer = new Panel();
                catalogDescriptionContainer.ID = "CatalogDescriptionContainer";
                catalogDescriptionContainer.CssClass = "DetailsViewSectionContainer";
                catalogDetailsViewLeftSideContainer.Controls.Add(catalogDescriptionContainer);

                // label
                Panel catalogDescriptionLabelContainer = new Panel();
                catalogDescriptionLabelContainer.ID = "CatalogDescriptionLabelContainer";
                catalogDescriptionLabelContainer.CssClass = "DetailsViewSectionLabel";
                catalogDescriptionContainer.Controls.Add(catalogDescriptionLabelContainer);

                Literal catalogDescriptionLabel = new Literal();
                catalogDescriptionLabel.Text = _GlobalResources.Description;
                catalogDescriptionLabelContainer.Controls.Add(catalogDescriptionLabel);

                // description
                Panel catalogDescriptionContentContainer = new Panel();
                catalogDescriptionContentContainer.ID = "CatalogDescriptionContentContainer";
                catalogDescriptionContentContainer.CssClass = "DetailsViewSectionContentContainer";
                catalogDescriptionContainer.Controls.Add(catalogDescriptionContentContainer);

                Label catalogDescription = new Label();
                catalogDescription.ID = "CatalogDescription";

                if (!String.IsNullOrWhiteSpace(description))
                { catalogDescription.Text = description; }
                else
                { catalogDescription.Text = shortDescription; }

                catalogDescriptionContentContainer.Controls.Add(catalogDescription);

                // CATALOG DETAILS VIEW RIGHT SIDE CONTAINER - render control catalogDetailsViewRightSideContainer

                Panel catalogDetailsViewRightSideContainer = new Panel();
                catalogDetailsViewRightSideContainer.ID = "Catalog_CatalogDetailsViewRightSideContainer";
                catalogDetailsViewRightSideContainer.CssClass = "";

                // CATALOG CHILD CATALOGS AND COURSES

                BuildCatalogTiles(catalogDetailsViewRightSideContainer, catalog.IdCatalog);
                BuildCourseTiles(catalogDetailsViewRightSideContainer, catalog.IdCatalog);

                // write the data to a text writer for json output
                TextWriter textWriter = new StringWriter();
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                detailsViewBackButtonContainer.RenderControl(htmlTextWriter);
                catalogDetailsViewLeftSideContainer.RenderControl(htmlTextWriter);
                catalogDetailsViewRightSideContainer.RenderControl(htmlTextWriter);

                jsonData.actionSuccessful = true;
                jsonData.html += textWriter.ToString();
                jsonData.exception = String.Empty;
                jsonData.idObject = (int)idCatalog;
                jsonData.objectTitle = title;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.idObject = null;
                jsonData.objectTitle = null;
            }

            // return jsonData
            return jsonData;
        }
        #endregion

        #region BuildCourseDetailsPanel
        [WebMethod(EnableSession = true)]
        public static CatalogDetailsJsonData BuildCourseDetailsPanel(int idCourse)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            CatalogDetailsJsonData jsonData = new CatalogDetailsJsonData();

            // get the ecommerce settings
            EcommerceSettings ecommerceSettings = new EcommerceSettings();
            
            try
            {
                // get the course object
                Asentia.LMS.Library.Course course = new Library.Course(idCourse);

                // get the course's language-specific title and description
                string title = course.Title;
                string shortDescription = course.ShortDescription;
                string description = HttpUtility.HtmlDecode(course.LongDescription);
                if (!String.IsNullOrWhiteSpace(description)) { description = new System.Text.RegularExpressions.Regex("<span style=\"[^\"]*\">").Replace(description, ""); }
                string objectives = HttpUtility.HtmlDecode(course.Objectives);

                foreach (Library.Course.LanguageSpecificProperty courseLanguageSpecific in course.LanguageSpecificProperties)
                {
                    if (courseLanguageSpecific.LangString == AsentiaSessionState.UserCulture)
                    {
                        title = courseLanguageSpecific.Title;
                        shortDescription = courseLanguageSpecific.ShortDescription;
                        description = HttpUtility.HtmlDecode(courseLanguageSpecific.LongDescription);
                        objectives = HttpUtility.HtmlDecode(courseLanguageSpecific.Objectives);
                        break;
                    }
                }

                // BACK BUTTON - render control detailsViewBackButtonContainer

                Panel detailsViewBackButtonContainer = BuildBackButtonContainer();

                // COURSE TITLE - render control courseTitleContainer

                Panel courseTitleContainer = new Panel();
                courseTitleContainer.ID = "CourseTitleContainer";
                courseTitleContainer.CssClass = "DetailsViewTitleContainer";

                Panel courseTitleIconContainer = new Panel();
                courseTitleIconContainer.ID = "CourseTitleIconContainer";
                courseTitleContainer.Controls.Add(courseTitleIconContainer);

                Image courseTitleIcon = new Image();
                courseTitleIcon.CssClass = "LargeIcon";

                if (course.Avatar != null)
                {
                    courseTitleIcon.CssClass += " AvatarImage";
                    courseTitleIcon.ImageUrl = SitePathConstants.SITE_COURSES_ROOT + course.Id + "/" + course.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                }
                else
                { courseTitleIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG); }

                courseTitleIconContainer.Controls.Add(courseTitleIcon);

                HtmlGenericControl courseTitle = new HtmlGenericControl("p");

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_HIDE_COURSE_CODES) == false && !String.IsNullOrWhiteSpace(course.CourseCode))
                { courseTitle.InnerText = course.CourseCode + " - " + title; }
                else
                { courseTitle.InnerText = title; }

                courseTitleContainer.Controls.Add(courseTitle);

                // COURSE BASIC INFORMATION - render control courseBasicInformationContainer

                Panel courseBasicInformationContainer = new Panel();
                courseBasicInformationContainer.ID = "CourseBasicInformationContainer";
                courseBasicInformationContainer.CssClass = "DetailsViewBasicInformationContainer";

                // STATUS

                Panel courseStatusWrapperContainer = new Panel();
                courseStatusWrapperContainer.ID = "CourseStatusWrapperContainer";
                courseStatusWrapperContainer.CssClass = "DetailsViewStatusWrapperContainer";

                DataTable courseLessons = course.GetLessons(null);

                // check if there is exactly one ILT in this course
                int numILT = 0;
                int idILT = 0;
                bool hasOneILT = false;

                foreach (DataRow row in courseLessons.Rows)
                {
                    if (Convert.ToBoolean(row["hasILT"]) && numILT == 0)
                    {
                        numILT += 1;
                        idILT = Convert.ToInt32(row["idStandupTraining"]);
                        hasOneILT = true;
                    }
                    else if (Convert.ToBoolean(row["hasILT"]) && numILT > 0)
                    {
                        numILT += 1;
                        idILT = 0;
                        hasOneILT = false;
                        break;
                    }
                    else
                    { }
                }

                if (course.IsClosed == false)
                {
                    if (AsentiaSessionState.IdSiteUser == 0) // not logged in user
                    {
                        Button courseLoginButton = new Button();
                        courseLoginButton.ID = "CourseLoginButton";
                        courseLoginButton.Text = _GlobalResources.LogInToEnroll;
                        courseLoginButton.OnClientClick = "$(\"#SettingsControlForLogin\").click(); return false;";
                        courseLoginButton.CssClass = "Button ActionButton EnrollButton";
                        courseStatusWrapperContainer.Controls.Add(courseLoginButton);
                    }
                    else if (AsentiaSessionState.IdSiteUser == 1) // logged in user is administrator
                    {
                        Button courseOpenButton = new Button();
                        courseOpenButton.ID = "CourseOpenButton";
                        courseOpenButton.Text = _GlobalResources.Open;
                        courseOpenButton.Enabled = false;
                        courseOpenButton.CssClass = "Button ActionButton OpenButton";
                        courseStatusWrapperContainer.Controls.Add(courseOpenButton);
                    }
                    else // logged in user is a normal user
                    {
                        if (!course.IsCallingUserEnrolled) // user does not already have a current enrollment 
                        {
                            // if course has one-time only self-enrollment property enabled and user has a previous enrollment, show closed button
                            if (!course.IsMultipleSelfEnrollmentEligible)
                            {
                                Button courseClosedButton = new Button();
                                courseClosedButton.ID = "CourseClosedButton";
                                courseClosedButton.Text = _GlobalResources.Closed;
                                courseClosedButton.CssClass = "Button ActionButton ClosedButton";
                                courseClosedButton.Enabled = false;
                                courseStatusWrapperContainer.Controls.Add(courseClosedButton);
                            }

                            // if the course is locked for prerequisites for this user, show a locked button
                            else if (course.IsSelfEnrollmentLockedByPrerequisites)
                            {
                                Button coursePrerequisiteLockedButton = new Button();
                                coursePrerequisiteLockedButton.ID = "CoursePrerequisiteLockedButton";
                                coursePrerequisiteLockedButton.Text = _GlobalResources.PrerequisitesIncomplete;
                                coursePrerequisiteLockedButton.Enabled = false;
                                coursePrerequisiteLockedButton.CssClass = "Button ActionButton PrerequisiteLockedButton";
                                courseStatusWrapperContainer.Controls.Add(coursePrerequisiteLockedButton);
                            }

                            else
                            {
                                // make the proper "enroll" or "purchase" button based on ecommerce settings and cost
                                if (ecommerceSettings.IsEcommerceSetAndVerified && course.Cost > 0) // ecommerce is set and verified and course has a cost, make a purchase button
                                {
                                    if (ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL) // paypal, make a buy now button
                                    {
                                        // Note: Buy Now button should always be available here. If any item is already in the active PayPal cart because of a previous
                                        // purchase that was not followed through with PaypPal, it will be cleared and replaced by the method handling the button click.

                                        Button coursePayPalButton = new Button();
                                        coursePayPalButton.ID = "CoursePayPalButton";
                                        coursePayPalButton.Text = _GlobalResources.BuyNow;
                                        coursePayPalButton.CssClass = "Button ActionButton PayPalButton";
                                        courseStatusWrapperContainer.Controls.Add(coursePayPalButton);

                                        // if the course only has one ILT, allow the user to enroll in a session for that ILT upon enrollment
                                        if (hasOneILT)
                                        {
                                            coursePayPalButton.OnClientClick = "LoadEnrollInCourseWithOneInstructorLedTrainingModalContent(" + idCourse + ", " + idILT + ", \"paypal\"); return false;";
                                        }
                                        else
                                        {
                                            coursePayPalButton.OnClientClick = "EnrollCourse(" + idCourse + ", \"paypal\"); return false;";
                                        }
                                    }
                                    else // all other payment processors use the cart, make an add to cart button, or link to my cart if item is already there
                                    {
                                        if (!_IsItemInCart(idCourse, PurchaseItemType.Course))
                                        {
                                            Button courseAddToCartButton = new Button();
                                            courseAddToCartButton.ID = "CourseAddToCartButton";
                                            courseAddToCartButton.Text = _GlobalResources.AddToCart;
                                            courseAddToCartButton.CssClass = "Button ActionButton CartButton";
                                            courseStatusWrapperContainer.Controls.Add(courseAddToCartButton);

                                            // if the course only has one ILT, allow the user to enroll in a session for that ILT upon enrollment
                                            if (hasOneILT)
                                            {
                                                courseAddToCartButton.OnClientClick = "LoadEnrollInCourseWithOneInstructorLedTrainingModalContent(" + idCourse + ", " + idILT + ", \"addtocart\"); return false;";
                                            }
                                            else
                                            {
                                                courseAddToCartButton.OnClientClick = "EnrollCourse(" + idCourse + ", \"addtocart\"); return false;";
                                            }
                                        }
                                        else // make my cart link
                                        {
                                            Button courseMyCartButton = new Button();
                                            courseMyCartButton.ID = "CourseMyCartButton";
                                            courseMyCartButton.Text = _GlobalResources.GoToCart;
                                            courseMyCartButton.CssClass = "Button ActionButton CartButton";
                                            courseMyCartButton.OnClientClick = "window.location = '/myprofile/MyCart.aspx'; return false;";
                                            courseStatusWrapperContainer.Controls.Add(courseMyCartButton);
                                        }
                                    }
                                }
                                else 
                                {
                                    // check if there is a pending enrollment request
                                    if (EnrollmentRequest.IsEnrollmentRequestPending(course.Id, AsentiaSessionState.IdSiteUser))
                                    {
                                        Button courseEnrollButton = new Button();
                                        courseEnrollButton.ID = "CourseEnrollButton";
                                        courseEnrollButton.Text = _GlobalResources.EnrollmentPending;
                                        courseEnrollButton.CssClass = "Button ActionButton EnrollButton";
                                        courseEnrollButton.Enabled = false;
                                        courseStatusWrapperContainer.Controls.Add(courseEnrollButton);
                                    }
                                    else // just create an enroll link
                                    {
                                        Button courseEnrollButton = new Button();
                                        courseEnrollButton.ID = "CourseEnrollButton";
                                        courseEnrollButton.Text = _GlobalResources.Enroll;
                                        courseEnrollButton.CssClass = "Button ActionButton EnrollButton";
                                        courseStatusWrapperContainer.Controls.Add(courseEnrollButton);

                                        // if the course only has one ILT, allow the user to enroll in a session for that ILT upon enrollment
                                        if (hasOneILT)
                                        {
                                            courseEnrollButton.OnClientClick = "LoadEnrollInCourseWithOneInstructorLedTrainingModalContent(" + idCourse + ", " + idILT + ", \"enroll\"); return false;";
                                        }
                                        else // else just enroll in the course
                                        {
                                            courseEnrollButton.OnClientClick = "EnrollCourse(" + idCourse + ", \"enroll\"); return false;";
                                        }
                                    }
                                }
                            }
                        }
                        else // user already has a current enrollment
                        {
                            Button courseEnrolledButton = new Button();
                            courseEnrolledButton.ID = "CourseEnrolledButton";
                            courseEnrolledButton.Text = _GlobalResources.Enrolled;
                            courseEnrolledButton.CssClass = "Button ActionButton EnrollButton";
                            courseEnrolledButton.Enabled = false;
                            courseStatusWrapperContainer.Controls.Add(courseEnrolledButton);
                        }
                    }
                }
                else // course is closed
                {
                    Button courseClosedButton = new Button();
                    courseClosedButton.ID = "CourseClosedButton";
                    courseClosedButton.Text = _GlobalResources.Closed;
                    courseClosedButton.CssClass = "Button ActionButton ClosedButton";
                    courseClosedButton.Enabled = false;
                    courseStatusWrapperContainer.Controls.Add(courseClosedButton);
                }

                // attach status wrapper to basic information container
                courseBasicInformationContainer.Controls.Add(courseStatusWrapperContainer);

                // COST, CREDITS, AND ESTIMATED TIME

                if (
                    (ecommerceSettings.IsEcommerceSetAndVerified)
                    ||
                    (course.Credits != null)
                    ||
                    (course.EstimatedLengthInMinutes != null && course.EstimatedLengthInMinutes > 0)
                    ||
                    (course.SelfEnrollmentDueInterval != null && course.SelfEnrollmentDueInterval > 0)
                    ||
                    (course.SelfEnrollmentExpiresFromStartInterval != null && course.SelfEnrollmentExpiresFromStartInterval > 0)
                    ||
                    (course.SelfEnrollmentExpiresFromFirstLaunchInterval != null && course.SelfEnrollmentExpiresFromFirstLaunchInterval > 0)
                   )
                {
                    Panel courseCostCreditsEstTimeWrapperContainer = new Panel();
                    courseCostCreditsEstTimeWrapperContainer.ID = "CourseCostCreditsEstTimeWrapperContainer";
                    courseCostCreditsEstTimeWrapperContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                    // if ecommerce is set and verified, show the cost
                    if (ecommerceSettings.IsEcommerceSetAndVerified)
                    {
                        Panel courseCostContainer = new Panel();
                        courseCostContainer.ID = "CourseCostContainer";

                        Label courseCostLabel = new Label();
                        courseCostLabel.CssClass = "bold";
                        courseCostLabel.Text = _GlobalResources.Cost + ": ";
                        courseCostContainer.Controls.Add(courseCostLabel);

                        Label courseCostValueLabel = new Label();

                        if (course.Cost > 0)
                        { courseCostValueLabel.Text = String.Format("{0}{1:N2} ({2})", ecommerceSettings.CurrencySymbol, course.Cost, ecommerceSettings.CurrencyCode); }
                        else
                        { courseCostValueLabel.Text = _GlobalResources.Free; }

                        courseCostContainer.Controls.Add(courseCostValueLabel);

                        courseCostCreditsEstTimeWrapperContainer.Controls.Add(courseCostContainer);
                    }

                    // course credits
                    if (course.Credits != null)
                    {
                        Panel courseCreditsContainer = new Panel();
                        courseCreditsContainer.ID = "CourseCreditsContainer";

                        Label courseCreditsLabel = new Label();
                        courseCreditsLabel.CssClass = "bold";
                        courseCreditsLabel.Text = _GlobalResources.Credits + ": ";
                        courseCreditsContainer.Controls.Add(courseCreditsLabel);

                        Label courseCreditsValueLabel = new Label();
                        courseCreditsValueLabel.Text = course.Credits.ToString();
                        courseCreditsContainer.Controls.Add(courseCreditsValueLabel);

                        courseCostCreditsEstTimeWrapperContainer.Controls.Add(courseCreditsContainer);
                    }

                    // course estimated time
                    if (course.EstimatedLengthInMinutes != null && course.EstimatedLengthInMinutes > 0)
                    {
                        string lengthData = null;

                        if (course.EstimatedLengthInMinutes < 60)
                        { lengthData = String.Format(_GlobalResources.XMinute_s, course.EstimatedLengthInMinutes.ToString()); }
                        else
                        {
                            int estimatedLengthHours = (int)course.EstimatedLengthInMinutes / 60;
                            int estimatedLengthMinutes = (int)course.EstimatedLengthInMinutes - (estimatedLengthHours * 60);

                            if (estimatedLengthMinutes > 0)
                            { lengthData = String.Format(_GlobalResources.XHour_sAndXMinute_s, estimatedLengthHours, estimatedLengthMinutes); }
                            else
                            { lengthData = String.Format(_GlobalResources.XHour_s, estimatedLengthHours); }
                        }

                        Panel courseEstimatedLengthContainer = new Panel();
                        courseEstimatedLengthContainer.ID = "CourseEstimatedLengthContainer";

                        Label courseEstimatedLengthLabel = new Label();
                        courseEstimatedLengthLabel.CssClass = "bold";
                        courseEstimatedLengthLabel.Text = _GlobalResources.EstimatedLength + ": ";
                        courseEstimatedLengthContainer.Controls.Add(courseEstimatedLengthLabel);

                        Label courseEstimatedLengthValueLabel = new Label();
                        courseEstimatedLengthValueLabel.Text = lengthData;
                        courseEstimatedLengthContainer.Controls.Add(courseEstimatedLengthValueLabel);

                        courseCostCreditsEstTimeWrapperContainer.Controls.Add(courseEstimatedLengthContainer);
                    }

                    // due interval
                    if (course.SelfEnrollmentDueInterval != null && course.SelfEnrollmentDueInterval > 0)
                    {
                        string dueInterval = String.Empty;

                        if (course.SelfEnrollmentDueTimeframe == "d")
                        { dueInterval = course.SelfEnrollmentDueInterval.ToString() + " " + _GlobalResources.day_s_lower; }
                        else if (course.SelfEnrollmentDueTimeframe == "ww")
                        { dueInterval = course.SelfEnrollmentDueInterval.ToString() + " " + _GlobalResources.week_s_lower; }
                        else if (course.SelfEnrollmentDueTimeframe == "m")
                        { dueInterval = course.SelfEnrollmentDueInterval.ToString() + " " + _GlobalResources.month_s_lower; }
                        else if (course.SelfEnrollmentDueTimeframe == "yyyy")
                        { dueInterval = course.SelfEnrollmentDueInterval.ToString() + " " + _GlobalResources.year_s_lower; }
                        else
                        { }

                        Panel courseSelfEnrollmentDueContainer = new Panel();
                        courseSelfEnrollmentDueContainer.ID = "CourseSelfEnrollmentDueContainer";

                        Label courseSelfEnrollmentDueLabel = new Label();
                        courseSelfEnrollmentDueLabel.CssClass = "bold";
                        courseSelfEnrollmentDueLabel.Text = _GlobalResources.Due + ": ";
                        courseSelfEnrollmentDueContainer.Controls.Add(courseSelfEnrollmentDueLabel);

                        Label courseSelfEnrollmentDueValueLabel = new Label();
                        courseSelfEnrollmentDueValueLabel.Text = String.Format(_GlobalResources._Interval_FromEnrollmentDate, dueInterval);
                        courseSelfEnrollmentDueContainer.Controls.Add(courseSelfEnrollmentDueValueLabel);

                        courseCostCreditsEstTimeWrapperContainer.Controls.Add(courseSelfEnrollmentDueContainer);
                    }

                    // access interval
                    if (
                        (course.SelfEnrollmentExpiresFromStartInterval != null && course.SelfEnrollmentExpiresFromStartInterval > 0)
                        ||
                        (course.SelfEnrollmentExpiresFromFirstLaunchInterval != null && course.SelfEnrollmentExpiresFromFirstLaunchInterval > 0)
                       )
                    {
                        string fromStartInterval = String.Empty;

                        if (course.SelfEnrollmentExpiresFromStartTimeframe == "d")
                        { fromStartInterval = course.SelfEnrollmentExpiresFromStartInterval.ToString() + " " + _GlobalResources.day_s_lower; }
                        else if (course.SelfEnrollmentExpiresFromStartTimeframe == "ww")
                        { fromStartInterval = course.SelfEnrollmentExpiresFromStartInterval.ToString() + " " + _GlobalResources.week_s_lower; }
                        else if (course.SelfEnrollmentExpiresFromStartTimeframe == "m")
                        { fromStartInterval = course.SelfEnrollmentExpiresFromStartInterval.ToString() + " " + _GlobalResources.month_s_lower; }
                        else if (course.SelfEnrollmentExpiresFromStartTimeframe == "yyyy")
                        { fromStartInterval = course.SelfEnrollmentExpiresFromStartInterval.ToString() + " " + _GlobalResources.year_s_lower; }
                        else
                        { }

                        string fromFirstLaunchInterval = String.Empty;

                        if (course.SelfEnrollmentExpiresFromFirstLaunchTimeframe == "d")
                        { fromFirstLaunchInterval = course.SelfEnrollmentExpiresFromFirstLaunchInterval.ToString() + " " + _GlobalResources.day_s_lower; }
                        else if (course.SelfEnrollmentExpiresFromFirstLaunchTimeframe == "ww")
                        { fromFirstLaunchInterval = course.SelfEnrollmentExpiresFromFirstLaunchInterval.ToString() + " " + _GlobalResources.week_s_lower; }
                        else if (course.SelfEnrollmentExpiresFromFirstLaunchTimeframe == "m")
                        { fromFirstLaunchInterval = course.SelfEnrollmentExpiresFromFirstLaunchInterval.ToString() + " " + _GlobalResources.month_s_lower; }
                        else if (course.SelfEnrollmentExpiresFromFirstLaunchTimeframe == "yyyy")
                        { fromFirstLaunchInterval = course.SelfEnrollmentExpiresFromFirstLaunchInterval.ToString() + " " + _GlobalResources.year_s_lower; }
                        else
                        { }

                        Panel courseSelfEnrollmentAccessContainer = new Panel();
                        courseSelfEnrollmentAccessContainer.ID = "CourseSelfEnrollmentAccessContainer";

                        Label courseSelfEnrollmentAccessLabel = new Label();
                        courseSelfEnrollmentAccessLabel.CssClass = "bold";
                        courseSelfEnrollmentAccessLabel.Text = _GlobalResources.Access + ": ";
                        courseSelfEnrollmentAccessContainer.Controls.Add(courseSelfEnrollmentAccessLabel);

                        Label courseSelfEnrollmentAccessValueLabel = new Label();

                        if (!String.IsNullOrWhiteSpace(fromStartInterval) && !String.IsNullOrWhiteSpace(fromFirstLaunchInterval))
                        { courseSelfEnrollmentAccessValueLabel.Text = String.Format(_GlobalResources._Interval_FromEnrollmentDateOr_Interval_FromFirstLaunchOfAModule, fromStartInterval, fromFirstLaunchInterval); }
                        else if (!String.IsNullOrWhiteSpace(fromStartInterval) && String.IsNullOrWhiteSpace(fromFirstLaunchInterval))
                        { courseSelfEnrollmentAccessValueLabel.Text = String.Format(_GlobalResources._Interval_FromEnrollmentDate, fromStartInterval); }
                        else if (String.IsNullOrWhiteSpace(fromStartInterval) && !String.IsNullOrWhiteSpace(fromFirstLaunchInterval))
                        { courseSelfEnrollmentAccessValueLabel.Text = String.Format(_GlobalResources._Interval_FromFirstLaunchOfAModule, fromFirstLaunchInterval); }
                        else
                        { }

                        courseSelfEnrollmentAccessContainer.Controls.Add(courseSelfEnrollmentAccessValueLabel);

                        courseCostCreditsEstTimeWrapperContainer.Controls.Add(courseSelfEnrollmentAccessContainer);
                    }

                    // attach cost and estimated time wrapper to basic information container
                    courseBasicInformationContainer.Controls.Add(courseCostCreditsEstTimeWrapperContainer);
                }

                // RATINGS

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RATINGS_COURSE_ENABLE) == true)
                {
                    if (!course.DisallowRating)
                    {
                        Panel courseRatingsContainer = new Panel();
                        courseRatingsContainer.ID = "CourseRatingsContainer";
                        courseRatingsContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                        UserRating courseRatingControl = new UserRating("CourseRatingControl");
                        courseRatingControl.IdObject = course.Id;

                        if (course.Rating != null)
                        { courseRatingControl.UsersRating = (double)course.Rating; }

                        if (course.Votes != null)
                        { courseRatingControl.Votes = (int)course.Votes; }

                        courseRatingControl.YourRating = course.GetUsersRating(AsentiaSessionState.IdSiteUser);
                        courseRatingControl.ShowYourRating = true;
                        courseRatingControl.ShowUserRating = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RATINGS_COURSE_PUBLICIZE);

                        // build trigger the building of the user rating control
                        // we need to kick this off manually because the method building this is a web method
                        courseRatingControl.BuildUserRatingControl();

                        // attach the course rating control
                        courseRatingsContainer.Controls.Add(courseRatingControl);

                        // attach ratings wrapper to basic information container
                        courseBasicInformationContainer.Controls.Add(courseRatingsContainer);
                    }
                }

                // WALL

                if (course.IsFeedActive == true && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE))
                {
                    Panel courseWallLinkContainer = new Panel();
                    courseWallLinkContainer.ID = "CourseWallLinkContainer";
                    courseWallLinkContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                    HyperLink courseWallLink = new HyperLink();
                    courseWallLink.ID = "CourseWallLink";
                    courseWallLink.CssClass = "ImageLink";
                    courseWallLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION,
                                                                     ImageFiles.EXT_PNG);
                    courseWallLink.NavigateUrl = "/courses/Wall.aspx?id=" + course.Id.ToString();
                    courseWallLink.ToolTip = _GlobalResources.JumpToCourseDiscussion;
                    courseWallLinkContainer.Controls.Add(courseWallLink);

                    // if the user is not admin, course wall is not open access, and user is not enrolled, disable the link
                    if (
                        (AsentiaSessionState.IdSiteUser > 1 && course.IsFeedOpenSubscription == false && !course.IsCallingUserEnrolled)
                        ||
                        AsentiaSessionState.IdSiteUser == 0
                       )
                    {
                        courseWallLink.NavigateUrl = null;

                        if (AsentiaSessionState.IdSiteUser == 0)
                        { courseWallLink.ToolTip = _GlobalResources.ThisCourseHasADiscussion; }
                        else
                        { courseWallLink.ToolTip = _GlobalResources.ThisCourseHasADiscussionThatCanOnlyBeViewedByEnrolledUsers; }

                        courseWallLink.Enabled = false;
                    }

                    // attach wall link wrapper to basic information container
                    courseBasicInformationContainer.Controls.Add(courseWallLinkContainer);
                }

                // SOCIAL MEDIA

                if (course.SocialMediaProperties.Count > 0)
                {
                    Panel courseSocialMediaLinksContainer = new Panel();
                    courseSocialMediaLinksContainer.ID = "CourseSocialMediaLinksContainer";
                    courseSocialMediaLinksContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                    int socialMediaItemsCount = 0;

                    foreach (Course.SocialMediaProperty socialMediaProperty in course.SocialMediaProperties)
                    {
                        socialMediaItemsCount++;

                        HyperLink socialMediaLink = new HyperLink();
                        socialMediaLink.ID = "SocialMediaLink_" + socialMediaItemsCount.ToString();
                        socialMediaLink.CssClass = "ImageLink";
                        socialMediaLink.NavigateUrl = socialMediaProperty.Protocol + socialMediaProperty.Href;
                        socialMediaLink.Target = "_blank";
                        socialMediaLink.ToolTip = socialMediaProperty.Protocol + socialMediaProperty.Href;

                        switch (socialMediaProperty.IconType)
                        {
                            case "facebook":
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_FACEBOOK,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                            case "linkedin":
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LINKEDIN,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                            case "twitter":
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_TWITTER,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                            case "google":
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOOGLE,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                            case "youtube":
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_YOUTUBE,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                            default:
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CUSTOMFORUM,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                        }

                        courseSocialMediaLinksContainer.Controls.Add(socialMediaLink);
                    }

                    // attach social media links wrapper to basic information container
                    courseBasicInformationContainer.Controls.Add(courseSocialMediaLinksContainer);
                }

                // COURSE DESCRIPTION - render control courseDescriptionContainer

                Panel courseDescriptionContainer = new Panel();
                courseDescriptionContainer.ID = "CourseDescriptionContainer";
                courseDescriptionContainer.CssClass = "DetailsViewSectionContainer";

                // label
                Panel courseDescriptionLabelContainer = new Panel();
                courseDescriptionLabelContainer.ID = "CourseDescriptionLabelContainer";
                courseDescriptionLabelContainer.CssClass = "DetailsViewSectionLabel";
                courseDescriptionContainer.Controls.Add(courseDescriptionLabelContainer);

                Literal courseDescriptionLabel = new Literal();
                courseDescriptionLabel.Text = _GlobalResources.Description;
                courseDescriptionLabelContainer.Controls.Add(courseDescriptionLabel);

                // description
                Panel courseDescriptionContentContainer = new Panel();
                courseDescriptionContentContainer.ID = "CourseDescriptionContentContainer";
                courseDescriptionContentContainer.CssClass = "DetailsViewSectionContentContainer";
                courseDescriptionContainer.Controls.Add(courseDescriptionContentContainer);

                Label courseDescription = new Label();
                courseDescription.ID = "CourseDescription";

                if (!String.IsNullOrWhiteSpace(description))
                { courseDescription.Text = description; }
                else
                { courseDescription.Text = shortDescription; }

                courseDescriptionContentContainer.Controls.Add(courseDescription);

                // COURSE OBJECTIVES - render control courseObjectivesContainer

                Panel courseObjectivesContainer = new Panel();

                if (!String.IsNullOrWhiteSpace(objectives))
                {
                    courseObjectivesContainer.ID = "CourseObjectivesContainer";
                    courseObjectivesContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel courseObjectivesLabelContainer = new Panel();
                    courseObjectivesLabelContainer.ID = "CourseObjectivesLabelContainer";
                    courseObjectivesLabelContainer.CssClass = "DetailsViewSectionLabel";
                    courseObjectivesContainer.Controls.Add(courseObjectivesLabelContainer);

                    Literal courseObjectivesLabel = new Literal();
                    courseObjectivesLabel.Text = _GlobalResources.Objectives;
                    courseObjectivesLabelContainer.Controls.Add(courseObjectivesLabel);

                    // objectives
                    Panel courseObjectivesContentContainer = new Panel();
                    courseObjectivesContentContainer.ID = "CourseObjectivesContentContainer";
                    courseObjectivesContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    courseObjectivesContainer.Controls.Add(courseObjectivesContentContainer);

                    Label courseObjectives = new Label();
                    courseObjectives.ID = "CourseObjectives";
                    courseObjectives.Text = objectives;

                    courseObjectivesContentContainer.Controls.Add(courseObjectives);
                }

                // COURSE LESSONS - render control courseLessonsContainer

                Panel courseLessonsContainer = new Panel();

                if (courseLessons.Rows.Count > 0)
                {
                    courseLessonsContainer.ID = "CourseLessonsContainer";
                    courseLessonsContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel courseLessonsLabelContainer = new Panel();
                    courseLessonsLabelContainer.ID = "CourseLessonsLabelContainer";
                    courseLessonsLabelContainer.CssClass = "DetailsViewSectionLabel";
                    courseLessonsContainer.Controls.Add(courseLessonsLabelContainer);

                    Literal courseLessonsLabel = new Literal();
                    courseLessonsLabel.Text = _GlobalResources.Modules;
                    courseLessonsLabelContainer.Controls.Add(courseLessonsLabel);

                    // lessons
                    Panel courseLessonsContentContainer = new Panel();
                    courseLessonsContentContainer.ID = "CourseLessonsContentContainer";
                    courseLessonsContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    courseLessonsContainer.Controls.Add(courseLessonsContentContainer);

                    Table courseLessonsTable = new Table();
                    courseLessonsTable.ID = "CourseLessonsTable";
                    courseLessonsContentContainer.Controls.Add(courseLessonsTable);

                    foreach (DataRow row in courseLessons.Rows)
                    {
                        int idLesson = Convert.ToInt32(row["idLesson"]);
                        bool hasOnlineContent = Convert.ToBoolean(row["hasOnlineContent"]);
                        bool hasILT = Convert.ToBoolean(row["hasILT"]);
                        bool hasTask = Convert.ToBoolean(row["hasTask"]);
                        bool hasOJT = Convert.ToBoolean(row["hasOJT"]);

                        int? idStandupTraining = null;

                        if (!String.IsNullOrWhiteSpace(row["idStandupTraining"].ToString()))
                        { idStandupTraining = Convert.ToInt32(row["idStandupTraining"]); }

                        // row
                        TableRow lessonRow = new TableRow();
                        courseLessonsTable.Rows.Add(lessonRow);

                        // title
                        TableCell lessonTitle = new TableCell();
                        lessonTitle.Text = row["title"].ToString();
                        lessonRow.Cells.Add(lessonTitle);

                        // content type(s)
                        TableCell lessonContentTypes = new TableCell();
                        lessonRow.Cells.Add(lessonContentTypes);

                        // online
                        if (hasOnlineContent)
                        {
                            HyperLink onlineContentLink = new HyperLink();
                            onlineContentLink.ID = "OnlineLessonContentLink_" + idLesson.ToString();
                            onlineContentLink.CssClass = "ImageLink";
                            onlineContentLink.NavigateUrl = null;
                            onlineContentLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG);
                            onlineContentLink.ToolTip = _GlobalResources.Online;
                            onlineContentLink.Enabled = false;
                            lessonContentTypes.Controls.Add(onlineContentLink);
                        }

                        // ilt
                        if (hasILT)
                        {
                            Image iltContentImage = new Image();
                            iltContentImage.ID = "ILTContentImage_" + idLesson.ToString();
                            iltContentImage.CssClass = "SmallIcon";
                            iltContentImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLASSROOM, ImageFiles.EXT_PNG);
                            lessonContentTypes.Controls.Add(iltContentImage);

                            HyperLink iltContentLink = new HyperLink();
                            iltContentLink.ID = "ILTLessonContentLink_" + idLesson.ToString();
                            iltContentLink.CssClass = "ImageLink";
                            iltContentLink.NavigateUrl = null;
                            iltContentLink.Text = "(" + _GlobalResources.ViewUpcomingMeetingDatesAndTimes + ")";

                            if (idStandupTraining != null)
                            { iltContentLink.Attributes.Add("onclick", "LoadViewILTSessionsFromCourseModalContent(" + idStandupTraining.ToString() + ");"); }
                            else
                            { iltContentLink.Enabled = false; }

                            lessonContentTypes.Controls.Add(iltContentLink);
                        }

                        // task
                        if (hasTask)
                        {
                            HyperLink taskContentLink = new HyperLink();
                            taskContentLink.ID = "TaskLessonContentLink_" + idLesson.ToString();
                            taskContentLink.CssClass = "ImageLink";
                            taskContentLink.NavigateUrl = null;
                            taskContentLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_TASK, ImageFiles.EXT_PNG);
                            taskContentLink.ToolTip = _GlobalResources.Task;
                            taskContentLink.Enabled = false;
                            lessonContentTypes.Controls.Add(taskContentLink);
                        }

                        // ojt
                        if (hasOJT)
                        {
                            HyperLink ojtContentLink = new HyperLink();
                            ojtContentLink.ID = "OJTLessonContentLink_" + idLesson.ToString();
                            ojtContentLink.CssClass = "ImageLink";
                            ojtContentLink.NavigateUrl = null;
                            ojtContentLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OJT, ImageFiles.EXT_PNG);
                            ojtContentLink.ToolTip = _GlobalResources.OJT;
                            ojtContentLink.Enabled = false;
                            lessonContentTypes.Controls.Add(ojtContentLink);
                        }
                    }
                }

                // COURSE MATERIALS - render control courseMaterialsContainer

                DataTable courseMaterials = course.GetCourseMaterials(null);
                Panel courseMaterialsContainer = new Panel();

                if (courseMaterials.Rows.Count > 0)
                {
                    courseMaterialsContainer.ID = "CourseMaterialsContainer";
                    courseMaterialsContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel courseMaterialsLabelContainer = new Panel();
                    courseMaterialsLabelContainer.ID = "CourseMaterialsLabelContainer";
                    courseMaterialsLabelContainer.CssClass = "DetailsViewSectionLabel";
                    courseMaterialsContainer.Controls.Add(courseMaterialsLabelContainer);

                    Literal courseMaterialsLabel = new Literal();
                    courseMaterialsLabel.Text = _GlobalResources.CourseMaterials;
                    courseMaterialsLabelContainer.Controls.Add(courseMaterialsLabel);

                    // course materials
                    Panel courseMaterialsContentContainer = new Panel();
                    courseMaterialsContentContainer.ID = "CourseMaterialsContentContainer";
                    courseMaterialsContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    courseMaterialsContainer.Controls.Add(courseMaterialsContentContainer);

                    foreach (DataRow row in courseMaterials.Rows)
                    {
                        Panel courseMaterialLinkContainer = new Panel();
                        courseMaterialLinkContainer.ID = "CourseMaterialLinkContainer_" + row["idDocumentRepositoryItem"].ToString();
                        courseMaterialLinkContainer.CssClass = "DetailsViewListItemContainer";
                        courseMaterialsContentContainer.Controls.Add(courseMaterialLinkContainer);

                        if (
                            !Convert.ToBoolean(row["isPrivate"])
                            ||
                            AsentiaSessionState.IdSiteUser == 1
                            ||
                            (Convert.ToBoolean(row["isPrivate"]) && course.IsCallingUserEnrolled)
                           )
                        {
                            HyperLink courseMaterialLink = new HyperLink();
                            courseMaterialLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_COURSE + course.Id + "/" + Convert.ToString(row["fileName"]);
                            courseMaterialLink.Target = "_blank";
                            courseMaterialLink.Text = row["label"].ToString();
                            courseMaterialLinkContainer.Controls.Add(courseMaterialLink);

                            Literal courseMaterialSize = new Literal();
                            courseMaterialSize.Text = " (" + Asentia.Common.Utility.GetSizeStringFromKB(Convert.ToInt32(row["kb"])) + ")";
                            courseMaterialLinkContainer.Controls.Add(courseMaterialSize);
                        }
                        else
                        {
                            Literal courseMaterialText = new Literal();
                            courseMaterialText.Text = row["label"].ToString() + " (" + Asentia.Common.Utility.GetSizeStringFromKB(Convert.ToInt32(row["kb"])) + ")";
                            courseMaterialLinkContainer.Controls.Add(courseMaterialText);
                        }
                    }
                }

                // COURSE CERTIFICATES - render control courseCertificatesContainer

                DataTable courseCertificates = course.GetCertificates(null);
                Panel courseCertificatesContainer = new Panel();

                if (courseCertificates.Rows.Count > 0)
                {
                    courseCertificatesContainer.ID = "CourseCertificatesContainer";
                    courseCertificatesContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel courseCertificatesLabelContainer = new Panel();
                    courseCertificatesLabelContainer.ID = "CourseCertificatesLabelContainer";
                    courseCertificatesLabelContainer.CssClass = "DetailsViewSectionLabel";
                    courseCertificatesContainer.Controls.Add(courseCertificatesLabelContainer);

                    Literal courseCertificatesLabel = new Literal();
                    courseCertificatesLabel.Text = _GlobalResources.Certificates;
                    courseCertificatesLabelContainer.Controls.Add(courseCertificatesLabel);

                    // course materials
                    Panel courseCertificatesContentContainer = new Panel();
                    courseCertificatesContentContainer.ID = "CourseCertificatesContentContainer";
                    courseCertificatesContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    courseCertificatesContainer.Controls.Add(courseCertificatesContentContainer);

                    foreach (DataRow row in courseCertificates.Rows)
                    {
                        Panel courseCertificateLinkContainer = new Panel();
                        courseCertificateLinkContainer.ID = "CourseCertificateLinkContainer_" + row["idCertificate"].ToString();
                        courseCertificateLinkContainer.CssClass = "DetailsViewListItemContainer";
                        courseCertificatesContentContainer.Controls.Add(courseCertificateLinkContainer);

                        Literal courseCertificateText = new Literal();
                        courseCertificateText.Text = row["name"].ToString();
                        courseCertificatesContentContainer.Controls.Add(courseCertificateText);
                    }
                }

                // COURSE SAMPLE SCREENS - render control courseSampleScreensContainer

                DataTable courseSampleScreens = course.GetSampleScreens();
                Panel courseSampleScreensContainer = new Panel();

                if (courseSampleScreens.Rows.Count > 0)
                {
                    courseSampleScreensContainer.ID = "CourseSampleScreensContainer";
                    courseSampleScreensContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel courseSampleScreensLabelContainer = new Panel();
                    courseSampleScreensLabelContainer.ID = "CourseSampleScreensLabelContainer";
                    courseSampleScreensLabelContainer.CssClass = "DetailsViewSectionLabel";
                    courseSampleScreensContainer.Controls.Add(courseSampleScreensLabelContainer);

                    Literal courseSampleScreensLabel = new Literal();
                    courseSampleScreensLabel.Text = _GlobalResources.SampleScreens;
                    courseSampleScreensLabelContainer.Controls.Add(courseSampleScreensLabel);

                    // sample screens
                    Panel courseSampleScreensContentContainer = new Panel();
                    courseSampleScreensContentContainer.ID = "CourseSampleScreensContentContainer";
                    courseSampleScreensContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    courseSampleScreensContainer.Controls.Add(courseSampleScreensContentContainer);

                    // put all sample screen paths into a string that represents a JS array
                    string sampleScreenPathsJSArray = String.Empty;
                    string[] sampleScreenPaths = new string[courseSampleScreens.Rows.Count];

                    int index = 0;
                    sampleScreenPathsJSArray = "[";

                    foreach (DataRow row in courseSampleScreens.Rows)
                    {
                        sampleScreenPaths[index] = SitePathConstants.SITE_COURSES_ROOT + course.Id + "/samplescreens/" + row["filename"].ToString();
                        sampleScreenPathsJSArray += "\"" + sampleScreenPaths[index] + "\",";

                        index++;
                    }

                    sampleScreenPathsJSArray = sampleScreenPathsJSArray.Substring(0, sampleScreenPathsJSArray.Length - 1);
                    sampleScreenPathsJSArray += "]";

                    // loop through sample screen paths and build links
                    for (index = 0; index < sampleScreenPaths.Length; index++)
                    {
                        Panel courseSampleScreenThumbnailContainer = new Panel();
                        courseSampleScreenThumbnailContainer.ID = "CourseSampleScreenThumbnailContainer_" + index.ToString();
                        courseSampleScreenThumbnailContainer.CssClass = "CourseSampleScreenThumbnailContainer";
                        courseSampleScreensContentContainer.Controls.Add(courseSampleScreenThumbnailContainer);

                        HyperLink courseSampleScreenLink = new HyperLink();
                        courseSampleScreenLink.ID = "courseSampleScreenLink_" + index.ToString();
                        courseSampleScreenLink.CssClass = "CourseSampleScreenLink";
                        courseSampleScreenLink.ImageUrl = sampleScreenPaths[index];
                        courseSampleScreenLink.NavigateUrl = "javascript:void(0);";
                        courseSampleScreenLink.Attributes.Add("onclick", "ShowCourseSampleScreens(" + sampleScreenPathsJSArray + "," + index.ToString() + ");");
                        courseSampleScreenThumbnailContainer.Controls.Add(courseSampleScreenLink);                        
                    }
                }

                // COURSE EXPERTS - render control courseExpertsContainer

                DataTable courseExperts = course.GetExperts(null);
                Panel courseExpertsContainer = new Panel();

                if (courseExperts.Rows.Count > 0)
                {
                    courseExpertsContainer.ID = "CourseExpertsContainer";
                    courseExpertsContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel courseExpertsLabelContainer = new Panel();
                    courseExpertsLabelContainer.ID = "CourseExpertsLabelContainer";
                    courseExpertsLabelContainer.CssClass = "DetailsViewSectionLabel";
                    courseExpertsContainer.Controls.Add(courseExpertsLabelContainer);

                    Literal courseExpertsLabel = new Literal();
                    courseExpertsLabel.Text = _GlobalResources.Experts;
                    courseExpertsLabelContainer.Controls.Add(courseExpertsLabel);

                    // course experts
                    Panel courseExpertsContentContainer = new Panel();
                    courseExpertsContentContainer.ID = "CourseExpertsContentContainer";
                    courseExpertsContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    courseExpertsContainer.Controls.Add(courseExpertsContentContainer);

                    foreach (DataRow row in courseExperts.Rows)
                    {
                        Panel courseExpertContainer = new Panel();
                        courseExpertContainer.ID = "CourseExpertContainer_" + row["idUser"].ToString();
                        courseExpertContainer.CssClass = "DetailsViewListItemContainer";
                        courseExpertsContentContainer.Controls.Add(courseExpertContainer);

                        Literal courseExpert = new Literal();
                        courseExpert.Text = row["displayName"].ToString();
                        courseExpertContainer.Controls.Add(courseExpert);
                    }
                }

                // COURSE PREREQUISITES - render control coursePrerequisitesContainer

                DataTable coursePrerequisites = course.GetPrerequisites(null);
                Panel coursePrerequisitesContainer = new Panel();

                if (coursePrerequisites.Rows.Count > 0)
                {
                    coursePrerequisitesContainer.ID = "CoursePrerequisitesContainer";
                    coursePrerequisitesContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel coursePrerequisitesLabelContainer = new Panel();
                    coursePrerequisitesLabelContainer.ID = "CoursePrerequisitesLabelContainer";
                    coursePrerequisitesLabelContainer.CssClass = "DetailsViewSectionLabel";
                    coursePrerequisitesContainer.Controls.Add(coursePrerequisitesLabelContainer);

                    Literal coursePrerequisitesLabel = new Literal();
                    coursePrerequisitesLabel.Text = _GlobalResources.Prerequisites;
                    coursePrerequisitesLabelContainer.Controls.Add(coursePrerequisitesLabel);

                    // course prerequisites
                    Panel coursePrerequisitesContentContainer = new Panel();
                    coursePrerequisitesContentContainer.ID = "CoursePrerequisitesContentContainer";
                    coursePrerequisitesContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    coursePrerequisitesContainer.Controls.Add(coursePrerequisitesContentContainer);

                    foreach (DataRow row in coursePrerequisites.Rows)
                    {
                        Panel coursePrerequisiteContainer = new Panel();
                        coursePrerequisiteContainer.ID = "CoursePrerequisiteContainer_" + row["idCourse"].ToString();
                        coursePrerequisiteContainer.CssClass = "DetailsViewListItemContainer";
                        coursePrerequisitesContentContainer.Controls.Add(coursePrerequisiteContainer);

                        Literal coursePrerequisite = new Literal();
                        coursePrerequisite.Text = row["title"].ToString();
                        coursePrerequisiteContainer.Controls.Add(coursePrerequisite);
                    }
                }

                // write the data to a text writer for json output
                TextWriter textWriter = new StringWriter();
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);

                detailsViewBackButtonContainer.RenderControl(htmlTextWriter);
                courseTitleContainer.RenderControl(htmlTextWriter);
                courseBasicInformationContainer.RenderControl(htmlTextWriter);
                courseDescriptionContainer.RenderControl(htmlTextWriter);

                if (!String.IsNullOrWhiteSpace(objectives))
                { courseObjectivesContainer.RenderControl(htmlTextWriter); }

                if (courseSampleScreens.Rows.Count > 0)
                { courseSampleScreensContainer.RenderControl(htmlTextWriter); }

                if (courseLessons.Rows.Count > 0)
                { courseLessonsContainer.RenderControl(htmlTextWriter); }

                if (courseMaterials.Rows.Count > 0)
                { courseMaterialsContainer.RenderControl(htmlTextWriter); }

                if (courseCertificates.Rows.Count > 0)
                { courseCertificatesContainer.RenderControl(htmlTextWriter); }

                if (courseExperts.Rows.Count > 0)
                { courseExpertsContainer.RenderControl(htmlTextWriter); }

                if (coursePrerequisites.Rows.Count > 0)
                { coursePrerequisitesContainer.RenderControl(htmlTextWriter); }

                jsonData.actionSuccessful = true;
                jsonData.html += textWriter.ToString();
                jsonData.exception = String.Empty;
                jsonData.idObject = (int)idCourse;
                jsonData.objectTitle = title;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message + ex.StackTrace;
                jsonData.idObject = null;
                jsonData.objectTitle = null;
            }

            // return jsonData
            return jsonData;
        }
        #endregion

        #region PopulateViewILTDetailsFromCourseModal
        [WebMethod(EnableSession = true)]
        public static CatalogDetailsJsonData PopulateViewILTDetailsFromCourseModal(int idInstructorLedTraining)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            CatalogDetailsJsonData jsonData = new CatalogDetailsJsonData();

            try
            {
                // get the ilt object
                Asentia.LMS.Library.StandupTraining ilt = new Library.StandupTraining(idInstructorLedTraining);

                // get the ilt's enrollable sessions, important to do it here because we need the count to determine if this will be enrollable
                DataTable iltEnrollableSessions = ilt.GetEnrollableInstances();

                // ILT UPCOMING SESSIONS - render control iltUpcomingSessionsContainer

                Panel iltUpcomingSessionsContainer = new Panel();
                iltUpcomingSessionsContainer.ID = "ILTUpcomingSessionsContainer";
                iltUpcomingSessionsContainer.CssClass = "DetailsViewSectionContainer";

                // upcoming sessions
                Panel iltUpcomingSessionsContentContainer = new Panel();
                iltUpcomingSessionsContentContainer.ID = "ILTUpcomingSessionsContentContainer";
                iltUpcomingSessionsContentContainer.CssClass = "DetailsViewSectionContentContainer";
                iltUpcomingSessionsContainer.Controls.Add(iltUpcomingSessionsContentContainer);

                if (iltEnrollableSessions.Rows.Count > 0)
                {
                    // loop through available sessions creating a list item for each and listing all meeting times for each
                    int idInstructorLedTrainingInstance = 0;
                    int meetingTimesCount = 0;

                    foreach (DataRow row in iltEnrollableSessions.Rows)
                    {
                        Panel iltUpcomingSessionContainer = new Panel();

                        if (idInstructorLedTrainingInstance != Convert.ToInt32(row["idStandupTrainingInstance"]))
                        {
                            meetingTimesCount = 0;

                            // instance id
                            idInstructorLedTrainingInstance = Convert.ToInt32(row["idStandupTrainingInstance"]);

                            iltUpcomingSessionContainer.ID = "ILTUpcomingSessionContainer_" + idInstructorLedTrainingInstance.ToString();
                            iltUpcomingSessionContainer.CssClass = "DetailsViewSessionItemContainer";
                            iltUpcomingSessionsContentContainer.Controls.Add(iltUpcomingSessionContainer);

                            // title
                            Panel iltUpcomingSessionTitleContainer = new Panel();
                            iltUpcomingSessionTitleContainer.ID = "ILTUpcomingSessionTitleContainer_" + idInstructorLedTrainingInstance.ToString();
                            iltUpcomingSessionTitleContainer.CssClass = "DetailsViewILTSessionTitleContainer";
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionTitleContainer);

                            Literal iltUpcomingSessionTitle = new Literal();
                            iltUpcomingSessionTitle.Text = row["title"].ToString();
                            iltUpcomingSessionTitleContainer.Controls.Add(iltUpcomingSessionTitle);

                            // available seats
                            Panel iltUpcomingSessionAvailableSeatsContainer = new Panel();
                            iltUpcomingSessionAvailableSeatsContainer.ID = "ILTUpcomingSessionAvailableSeatsContainer_" + idInstructorLedTrainingInstance.ToString();
                            iltUpcomingSessionAvailableSeatsContainer.CssClass = "DetailsViewILTSessionPropertyContainer";
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionAvailableSeatsContainer);

                            Label iltUpcomingSessionAvailableSeatsLabel = new Label();
                            iltUpcomingSessionAvailableSeatsLabel.Text = _GlobalResources.Status + ": ";
                            iltUpcomingSessionAvailableSeatsContainer.Controls.Add(iltUpcomingSessionAvailableSeatsLabel);

                            Label iltUpcomingSessionAvailableSeatsValue = new Label();
                            iltUpcomingSessionAvailableSeatsValue.CssClass = "DetailsViewILTSessionAvailableSeatsLabel";

                            iltUpcomingSessionAvailableSeatsValue.Text = row["availableSeats"].ToString() + " " + _GlobalResources.of_lower + " " + row["seats"].ToString();

                            int availableSeats = Convert.ToInt32(row["availableSeats"]);
                            int seats = Convert.ToInt32(row["seats"]);
                            int availableWaitingSeats = Convert.ToInt32(row["availableWaitingSeats"]);
                            int waitingSeats = Convert.ToInt32(row["waitingSeats"]);

                            if (availableSeats > 0)
                            { iltUpcomingSessionAvailableSeatsValue.Text = _GlobalResources.Seat_sAvailable + " (" + availableSeats + "/" + seats + ")"; }
                            else
                            {
                                iltUpcomingSessionAvailableSeatsValue.CssClass = "DetailsViewILTSessionAvailableSeatsLabelWaitingList";
                                iltUpcomingSessionAvailableSeatsValue.Text = _GlobalResources.WaitingListAvailable + " (" + availableWaitingSeats + "/" + waitingSeats + ")"; 
                            }

                            iltUpcomingSessionAvailableSeatsContainer.Controls.Add(iltUpcomingSessionAvailableSeatsValue);

                            // location
                            Panel iltUpcomingSessionLocationContainer = new Panel();
                            iltUpcomingSessionLocationContainer.ID = "ILTUpcomingSessionLocationContainer_" + idInstructorLedTrainingInstance.ToString();
                            iltUpcomingSessionLocationContainer.CssClass = "DetailsViewILTSessionPropertyContainer";
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionLocationContainer);

                            Label iltUpcomingSessionLocationLabel = new Label();
                            iltUpcomingSessionLocationLabel.Text = _GlobalResources.Location + ": ";
                            iltUpcomingSessionLocationContainer.Controls.Add(iltUpcomingSessionLocationLabel);

                            Label iltUpcomingSessionLocationValue = new Label();

                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                if (!String.IsNullOrWhiteSpace(row["city"].ToString()) || !String.IsNullOrWhiteSpace(row["province"].ToString()))
                                {
                                    if (!String.IsNullOrWhiteSpace(row["city"].ToString()))
                                    { iltUpcomingSessionLocationValue.Text += row["city"].ToString(); }

                                    if (!String.IsNullOrWhiteSpace(row["province"].ToString()))
                                    { iltUpcomingSessionLocationValue.Text += ", " + row["province"].ToString(); }
                                }
                            }
                            else if (Convert.ToInt32(row["type"]) == 2)
                            { iltUpcomingSessionLocationValue.Text += _GlobalResources.GoToMeeting; }
                            else if (Convert.ToInt32(row["type"]) == 3)
                            { iltUpcomingSessionLocationValue.Text += _GlobalResources.GoToWebinar; }
                            else if (Convert.ToInt32(row["type"]) == 4)
                            { iltUpcomingSessionLocationValue.Text += _GlobalResources.GoToTraining; }
                            else if (Convert.ToInt32(row["type"]) == 5)
                            { iltUpcomingSessionLocationValue.Text += _GlobalResources.WebEx; }
                            else
                            { iltUpcomingSessionLocationValue.Text += _GlobalResources.Online; }

                            iltUpcomingSessionLocationContainer.Controls.Add(iltUpcomingSessionLocationValue);                            

                            // instructors
                            if (!String.IsNullOrWhiteSpace(row["instructorDisplayNames"].ToString()))
                            {
                                // instructors label
                                Panel iltUpcomingSessionInstructorsLabelContainer = new Panel();
                                iltUpcomingSessionInstructorsLabelContainer.ID = "ILTUpcomingSessionInstructorsLabelContainer_" + idInstructorLedTrainingInstance.ToString();
                                iltUpcomingSessionInstructorsLabelContainer.CssClass = "DetailsViewILTSessionPropertyContainer";
                                iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionInstructorsLabelContainer);

                                Label iltUpcomingSessionInstructorLabel = new Label();
                                iltUpcomingSessionInstructorLabel.Text = _GlobalResources.Instructor_s + ": ";
                                iltUpcomingSessionInstructorsLabelContainer.Controls.Add(iltUpcomingSessionInstructorLabel);

                                // instructors
                                string[] instructors = row["instructorDisplayNames"].ToString().Split('|');

                                for (int i = 0; i < instructors.Length; i++)
                                {
                                    Panel iltUpcomingSessionInstructorContainer = new Panel();
                                    iltUpcomingSessionInstructorContainer.ID = "ILTUpcomingSessionInstructorContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + i.ToString();
                                    iltUpcomingSessionInstructorContainer.CssClass = "DetailsViewILTSessionInstructorContainer";

                                    Literal iltUpcomingSessionInstructor = new Literal();
                                    iltUpcomingSessionInstructor.Text = instructors[i];

                                    iltUpcomingSessionInstructorContainer.Controls.Add(iltUpcomingSessionInstructor);
                                    iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionInstructorContainer);
                                }
                            }

                            // meeting times label
                            Panel iltUpcomingSessionMeetingTimesLabelContainer = new Panel();
                            iltUpcomingSessionMeetingTimesLabelContainer.ID = "ILTUpcomingSessionMeetingTimesLabelContainer_" + idInstructorLedTrainingInstance;
                            iltUpcomingSessionMeetingTimesLabelContainer.CssClass = "DetailsViewILTSessionPropertyContainer";
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionMeetingTimesLabelContainer);

                            Label iltUpcomingSessionMeetingTimesLabel = new Label();
                            iltUpcomingSessionMeetingTimesLabel.Text = _GlobalResources.MeetingTime_s + ": ";
                            iltUpcomingSessionMeetingTimesLabelContainer.Controls.Add(iltUpcomingSessionMeetingTimesLabel);

                            // meeting time
                            Panel iltUpcomingSessionMeetingTimeContainer = new Panel();
                            iltUpcomingSessionMeetingTimeContainer.ID = "ILTUpcomingSessionMeetingTimeContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                            iltUpcomingSessionMeetingTimeContainer.CssClass = "DetailsViewILTSessionMeetingTimeContainer";

                            DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                            DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                            // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                int idTimezone = Convert.ToInt32(row["idTimezone"]);
                                string tzDotNetName = new Timezone(idTimezone).dotNetName;

                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            }
                            else
                            {
                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            }

                            Literal iltUpcomingSessionMeetingTime = new Literal();
                            iltUpcomingSessionMeetingTime.Text = dtStart.ToString() + " - " + dtEnd.ToString();

                            iltUpcomingSessionMeetingTimeContainer.Controls.Add(iltUpcomingSessionMeetingTime);
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionMeetingTimeContainer);
                        }
                        else // additional meeting times
                        {
                            // meeting time
                            Panel iltUpcomingSessionMeetingTimeContainer = new Panel();
                            iltUpcomingSessionMeetingTimeContainer.ID = "ILTUpcomingSessionMeetingTimeContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                            iltUpcomingSessionMeetingTimeContainer.CssClass = "DetailsViewILTSessionMeetingTimeContainer";

                            DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                            DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                            // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                int idTimezone = Convert.ToInt32(row["idTimezone"]);
                                string tzDotNetName = new Timezone(idTimezone).dotNetName;

                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            }
                            else
                            {
                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            }

                            Literal iltUpcomingSessionMeetingTime = new Literal();
                            iltUpcomingSessionMeetingTime.Text = dtStart.ToString() + " - " + dtEnd.ToString();

                            iltUpcomingSessionMeetingTimeContainer.Controls.Add(iltUpcomingSessionMeetingTime);
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionMeetingTimeContainer);
                        }
                    }
                }
                else
                {
                    Panel iltUpcomingSessionContainer = new Panel();
                    iltUpcomingSessionContainer.ID = "ILTUpcomingSessionContainer_NoUpcomingSessions";
                    iltUpcomingSessionContainer.CssClass = "DetailsViewListItemContainer";
                    iltUpcomingSessionsContentContainer.Controls.Add(iltUpcomingSessionContainer);

                    Literal iltUpcomingSession = new Literal();
                    iltUpcomingSession.Text = _GlobalResources.ThereAreNoUpcomingSessions;
                    iltUpcomingSessionContainer.Controls.Add(iltUpcomingSession);
                }

                // write the data to a text writer for json output
                TextWriter textWriter = new StringWriter();
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);

                iltUpcomingSessionsContainer.RenderControl(htmlTextWriter);

                jsonData.actionSuccessful = true;
                jsonData.html += textWriter.ToString();
                jsonData.exception = String.Empty;
                jsonData.idObject = (int)idInstructorLedTraining;
                jsonData.objectTitle = null;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.idObject = null;
                jsonData.objectTitle = null;
            }

            // return jsonData
            return jsonData;
        }
        #endregion

        #region BuildLearningPathDetailsPanel
        [WebMethod(EnableSession = true)]
        public static CatalogDetailsJsonData BuildLearningPathDetailsPanel(int idLearningPath)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            CatalogDetailsJsonData jsonData = new CatalogDetailsJsonData();

            // get the ecommerce settings
            EcommerceSettings ecommerceSettings = new EcommerceSettings();

            try
            {
                // get the learning path object
                Asentia.LMS.Library.LearningPath learningPath = new Library.LearningPath(idLearningPath);

                // get the course's language-specific title and description
                string title = learningPath.Name;
                string shortDescription = learningPath.ShortDescription;
                string description = HttpUtility.HtmlDecode(learningPath.LongDescription);
                if(!String.IsNullOrWhiteSpace(description)){description = new System.Text.RegularExpressions.Regex("<span style=\"[^\"]*\">").Replace(description, "");}

                foreach (Library.LearningPath.LanguageSpecificProperty learningPathLanguageSpecific in learningPath.LanguageSpecificProperties)
                {
                    if (learningPathLanguageSpecific.LangString == AsentiaSessionState.UserCulture)
                    {
                        title = learningPathLanguageSpecific.Name;
                        shortDescription = learningPathLanguageSpecific.ShortDescription;
                        description = HttpUtility.HtmlDecode(learningPathLanguageSpecific.LongDescription);
                        break;
                    }
                }

                // BACK BUTTON - render control detailsViewBackButtonContainer

                Panel detailsViewBackButtonContainer = BuildBackButtonContainer();

                // LEARNING PATH TITLE - render control learningPathTitleContainer

                Panel learningPathTitleContainer = new Panel();
                learningPathTitleContainer.ID = "LearningPathTitleContainer";
                learningPathTitleContainer.CssClass = "DetailsViewTitleContainer";

                Panel learningPathTitleIconContainer = new Panel();
                learningPathTitleIconContainer.ID = "LearningPathTitleIconContainer";
                learningPathTitleContainer.Controls.Add(learningPathTitleIconContainer);

                Image learningPathTitleIcon = new Image();
                learningPathTitleIcon.CssClass = "LargeIcon";

                if (learningPath.Avatar != null)
                {
                    learningPathTitleIcon.CssClass += " AvatarImage";
                    learningPathTitleIcon.ImageUrl = SitePathConstants.SITE_LEARNINGPATHS_ROOT + learningPath.Id + "/" + learningPath.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                }
                else
                { learningPathTitleIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG); }

                learningPathTitleIconContainer.Controls.Add(learningPathTitleIcon);

                HtmlGenericControl learningPathTitle = new HtmlGenericControl("p");
                learningPathTitle.InnerText = title;

                learningPathTitleContainer.Controls.Add(learningPathTitle);

                // LEARNING PATH BASIC INFORMATION - render control learningPathBasicInformationContainer

                Panel learningPathBasicInformationContainer = new Panel();
                learningPathBasicInformationContainer.ID = "LearningPathBasicInformationContainer";
                learningPathBasicInformationContainer.CssClass = "DetailsViewBasicInformationContainer";

                // STATUS

                Panel learningPathStatusWrapperContainer = new Panel();
                learningPathStatusWrapperContainer.ID = "LearningPathStatusWrapperContainer";
                learningPathStatusWrapperContainer.CssClass = "DetailsViewStatusWrapperContainer";

                if (learningPath.IsClosed == false)
                {
                    if (AsentiaSessionState.IdSiteUser == 0) // not logged in user
                    {
                        Button learningPathLoginButton = new Button();
                        learningPathLoginButton.ID = "LearningPathLoginButton";
                        learningPathLoginButton.Text = _GlobalResources.LogInToEnroll;
                        learningPathLoginButton.OnClientClick = "$(\"#SettingsControlForLogin\").click(); return false;";
                        learningPathLoginButton.CssClass = "Button ActionButton EnrollButton";
                        learningPathStatusWrapperContainer.Controls.Add(learningPathLoginButton);
                    }
                    else if (AsentiaSessionState.IdSiteUser == 1) // logged in user is administrator
                    {
                        Button learningPathOpenButton = new Button();
                        learningPathOpenButton.ID = "LearningPathOpenButton";
                        learningPathOpenButton.Text = _GlobalResources.Open;
                        learningPathOpenButton.CssClass = "Button ActionButton EnrollButton";
                        learningPathOpenButton.Enabled = false;
                        learningPathStatusWrapperContainer.Controls.Add(learningPathOpenButton);
                    }
                    else // logged in user is a normal user
                    {
                        if (!learningPath.IsCallingUserEnrolled) // user does not already have a current enrollment
                        {
                            // if learning path has one-time only self-enrollment property enabled and user has a previous enrollment, show closed button
                            if (!learningPath.IsMultipleSelfEnrollmentEligible)
                            {
                                Button learningPathClosedButton = new Button();
                                learningPathClosedButton.ID = "LearningPathClosedButton";
                                learningPathClosedButton.Text = _GlobalResources.Closed;
                                learningPathClosedButton.CssClass = "Button ActionButton ClosedButton";
                                learningPathClosedButton.Enabled = false;
                                learningPathStatusWrapperContainer.Controls.Add(learningPathClosedButton);
                            }

                            // make the proper "enroll" or "purchase" button based on ecommerce settings and cost
                            else if (ecommerceSettings.IsEcommerceSetAndVerified && learningPath.Cost > 0) // ecommerce is ser and verified and learning path has a cost, make a purchase button
                            {
                                if (ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL) // paypal, make a buy now button
                                {
                                    // Note: Buy Now button should always be available here. If any item is already in the active PayPal cart because of a previous
                                    // purchase that was not followed through with PayPal, it will be cleared and replaced by the method handling the button click.

                                    Button learningPathPayPalButton = new Button();
                                    learningPathPayPalButton.ID = "LearningPathPayPalButton";
                                    learningPathPayPalButton.Text = _GlobalResources.BuyNow;
                                    learningPathPayPalButton.CssClass = "Button ActionButton PayPalButton";
                                    learningPathPayPalButton.OnClientClick = "EnrollLearningPath(" + idLearningPath + ", \"paypal\"); return false;";
                                    learningPathStatusWrapperContainer.Controls.Add(learningPathPayPalButton);
                                }
                                else // all other payment processors use the cart, make an add to cart button, or link to my cart if item is already there
                                {
                                    if (!_IsItemInCart(idLearningPath, PurchaseItemType.LearningPath))
                                    {
                                        Button learningPathAddToCartButton = new Button();
                                        learningPathAddToCartButton.ID = "LearningPathAddToCartButton";
                                        learningPathAddToCartButton.Text = _GlobalResources.BuyNow;
                                        learningPathAddToCartButton.CssClass = "Button ActionButton CartButton";
                                        learningPathAddToCartButton.OnClientClick = "EnrollLearningPath(" + idLearningPath + ", \"addtocart\"); return false;";
                                        learningPathStatusWrapperContainer.Controls.Add(learningPathAddToCartButton);
                                    }
                                    else // make my cart link
                                    {
                                        Button learningPathMyCartButton = new Button();
                                        learningPathMyCartButton.ID = "LearningPathMyCartButton";
                                        learningPathMyCartButton.Text = _GlobalResources.GoToCart;
                                        learningPathMyCartButton.CssClass = "Button ActionButton CartButton";
                                        learningPathMyCartButton.OnClientClick = "window.location = '/myprofile/MyCart.aspx'; return false;";
                                        learningPathStatusWrapperContainer.Controls.Add(learningPathMyCartButton);
                                    }
                                }
                            }
                            else if (learningPath.IsMultipleSelfEnrollmentEligible) // just make an enroll button
                            {
                                Button learningPathEnrollButton = new Button();
                                learningPathEnrollButton.ID = "LearningPathEnrollButton";
                                learningPathEnrollButton.Text = _GlobalResources.Enroll;
                                learningPathEnrollButton.CssClass = "Button ActionButton EnrollButton";
                                learningPathEnrollButton.OnClientClick = "EnrollLearningPath(" + idLearningPath + ", \"enroll\"); return false;";
                                learningPathStatusWrapperContainer.Controls.Add(learningPathEnrollButton);
                            }
                        }
                        else // user already has a current enrollment
                        {
                            Button learningPathEnrolledButton = new Button();
                            learningPathEnrolledButton.ID = "LearningPathEnrolledButton";
                            learningPathEnrolledButton.Text = _GlobalResources.Enrolled;
                            learningPathEnrolledButton.CssClass = "Button ActionButton EnrollButton";
                            learningPathEnrolledButton.Enabled = false;
                            learningPathStatusWrapperContainer.Controls.Add(learningPathEnrolledButton);
                        }
                    }
                }
                else // learning path is closed
                {
                    Button learningPathClosedButton = new Button();
                    learningPathClosedButton.ID = "LearningPathClosedButton";
                    learningPathClosedButton.Text = _GlobalResources.Closed;
                    learningPathClosedButton.CssClass = "Button ActionButton ClosedButton";
                    learningPathClosedButton.Enabled = false;
                    learningPathStatusWrapperContainer.Controls.Add(learningPathClosedButton);
                }

                // attach status wrapper to basic information container
                learningPathBasicInformationContainer.Controls.Add(learningPathStatusWrapperContainer);

                // COST

                // if ecommerce is set and verified, show the cost
                if (ecommerceSettings.IsEcommerceSetAndVerified)
                {
                    Panel learningPathCostWrapperContainer = new Panel();
                    learningPathCostWrapperContainer.ID = "LearningPathCostWrapperContainer";
                    learningPathCostWrapperContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                    Panel learningPathCostContainer = new Panel();
                    learningPathCostContainer.ID = "LearningPathCostContainer";

                    Label learningPathCostLabel = new Label();
                    learningPathCostLabel.CssClass = "bold";
                    learningPathCostLabel.Text = _GlobalResources.Cost + ": ";
                    learningPathCostContainer.Controls.Add(learningPathCostLabel);

                    Label learningPathCostValueLabel = new Label();

                    if (learningPath.Cost > 0)
                    { learningPathCostValueLabel.Text = String.Format("{0}{1:N2} ({2})", ecommerceSettings.CurrencySymbol, learningPath.Cost, ecommerceSettings.CurrencyCode); }
                    else
                    { learningPathCostValueLabel.Text = _GlobalResources.Free; }

                    learningPathCostContainer.Controls.Add(learningPathCostValueLabel);

                    learningPathCostWrapperContainer.Controls.Add(learningPathCostContainer);

                    // attach cost wrapper to basic information container
                    learningPathBasicInformationContainer.Controls.Add(learningPathCostWrapperContainer);
                }

                // LEARNING PATH DESCRIPTION - render control learningPathDescriptionContainer

                Panel learningPathDescriptionContainer = new Panel();
                learningPathDescriptionContainer.ID = "LearningPathDescriptionContainer";
                learningPathDescriptionContainer.CssClass = "DetailsViewSectionContainer";

                // label
                Panel learningPathDescriptionLabelContainer = new Panel();
                learningPathDescriptionLabelContainer.ID = "LearningPathDescriptionLabelContainer";
                learningPathDescriptionLabelContainer.CssClass = "DetailsViewSectionLabel";
                learningPathDescriptionContainer.Controls.Add(learningPathDescriptionLabelContainer);

                Literal learningPathDescriptionLabel = new Literal();
                learningPathDescriptionLabel.Text = _GlobalResources.Description;
                learningPathDescriptionLabelContainer.Controls.Add(learningPathDescriptionLabel);

                // description
                Panel learningPathDescriptionContentContainer = new Panel();
                learningPathDescriptionContentContainer.ID = "LearningPathDescriptionContentContainer";
                learningPathDescriptionContentContainer.CssClass = "DetailsViewSectionContentContainer";
                learningPathDescriptionContainer.Controls.Add(learningPathDescriptionContentContainer);

                Label learningPathDescription = new Label();
                learningPathDescription.ID = "LearningPathDescription";

                if (!String.IsNullOrWhiteSpace(description))
                { learningPathDescription.Text = description; }
                else
                { learningPathDescription.Text = shortDescription; }

                learningPathDescriptionContentContainer.Controls.Add(learningPathDescription);

                // LEARNING PATH COURSES - render control learningPathCoursesContainer

                DataTable learningPathCourses = learningPath.GetCourses(null);
                Panel learningPathCoursesContainer = new Panel();

                if (learningPathCourses.Rows.Count > 0)
                {
                    learningPathCoursesContainer.ID = "LearningPathCoursesContainer";
                    learningPathCoursesContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel learningPathCoursesLabelContainer = new Panel();
                    learningPathCoursesLabelContainer.ID = "LearningPathCoursesLabelContainer";
                    learningPathCoursesLabelContainer.CssClass = "DetailsViewSectionLabel";
                    learningPathCoursesContainer.Controls.Add(learningPathCoursesLabelContainer);

                    Literal learningPathCoursesLabel = new Literal();
                    learningPathCoursesLabel.Text = _GlobalResources.Courses;
                    learningPathCoursesLabelContainer.Controls.Add(learningPathCoursesLabel);

                    // courses
                    Panel learningPathCoursesContentContainer = new Panel();
                    learningPathCoursesContentContainer.ID = "LearningPathCoursesContentContainer";
                    learningPathCoursesContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    learningPathCoursesContainer.Controls.Add(learningPathCoursesContentContainer);

                    foreach (DataRow row in learningPathCourses.Rows)
                    {
                        Panel learningPathCourseContainer = new Panel();
                        learningPathCourseContainer.ID = "LearningPathCourseContainer_" + row["idCourse"].ToString();
                        learningPathCourseContainer.CssClass = "DetailsViewListItemContainer";
                        learningPathCoursesContentContainer.Controls.Add(learningPathCourseContainer);

                        Literal learningPathCourse = new Literal();
                        learningPathCourse.Text = row["title"].ToString();
                        learningPathCourseContainer.Controls.Add(learningPathCourse);
                    }
                }

                // LEARNING PATH COURSE SAMPLE SCREENS - render control courseSampleScreensContainer

                DataTable courseSampleScreens = learningPath.GetCourseSampleScreens();
                Panel courseSampleScreensContainer = new Panel();

                if (courseSampleScreens.Rows.Count > 0)
                {
                    courseSampleScreensContainer.ID = "CourseSampleScreensContainer";
                    courseSampleScreensContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel courseSampleScreensLabelContainer = new Panel();
                    courseSampleScreensLabelContainer.ID = "CourseSampleScreensLabelContainer";
                    courseSampleScreensLabelContainer.CssClass = "DetailsViewSectionLabel";
                    courseSampleScreensContainer.Controls.Add(courseSampleScreensLabelContainer);

                    Literal courseSampleScreensLabel = new Literal();
                    courseSampleScreensLabel.Text = _GlobalResources.SampleScreens;
                    courseSampleScreensLabelContainer.Controls.Add(courseSampleScreensLabel);

                    // sample screens
                    Panel courseSampleScreensContentContainer = new Panel();
                    courseSampleScreensContentContainer.ID = "CourseSampleScreensContentContainer";
                    courseSampleScreensContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    courseSampleScreensContainer.Controls.Add(courseSampleScreensContentContainer);

                    // put all sample screen paths into a string that represents a JS array
                    string sampleScreenPathsJSArray = String.Empty;
                    string[] sampleScreenPaths = new string[courseSampleScreens.Rows.Count];

                    int index = 0;
                    sampleScreenPathsJSArray = "[";

                    foreach (DataRow row in courseSampleScreens.Rows)
                    {
                        sampleScreenPaths[index] = SitePathConstants.SITE_COURSES_ROOT + row["idCourse"].ToString() + "/samplescreens/" + row["filename"].ToString();
                        sampleScreenPathsJSArray += "\"" + sampleScreenPaths[index] + "\",";

                        index++;
                    }

                    sampleScreenPathsJSArray = sampleScreenPathsJSArray.Substring(0, sampleScreenPathsJSArray.Length - 1);
                    sampleScreenPathsJSArray += "]";

                    // loop through sample screen paths and build links
                    for (index = 0; index < sampleScreenPaths.Length; index++)
                    {
                        Panel courseSampleScreenThumbnailContainer = new Panel();
                        courseSampleScreenThumbnailContainer.ID = "CourseSampleScreenThumbnailContainer_" + index.ToString();
                        courseSampleScreenThumbnailContainer.CssClass = "CourseSampleScreenThumbnailContainer";
                        courseSampleScreensContentContainer.Controls.Add(courseSampleScreenThumbnailContainer);

                        HyperLink courseSampleScreenLink = new HyperLink();
                        courseSampleScreenLink.ID = "courseSampleScreenLink_" + index.ToString();
                        courseSampleScreenLink.CssClass = "CourseSampleScreenLink";
                        courseSampleScreenLink.ImageUrl = sampleScreenPaths[index];
                        courseSampleScreenLink.NavigateUrl = "javascript:void(0);";
                        courseSampleScreenLink.Attributes.Add("onclick", "ShowCourseSampleScreens(" + sampleScreenPathsJSArray + "," + index.ToString() + ");");
                        courseSampleScreenThumbnailContainer.Controls.Add(courseSampleScreenLink);
                    }
                }

                // LEARNING PATH MATERIALS - render control learningPathMaterialsContainer

                DataTable learningPathMaterials = learningPath.GetLearningPathMaterials(null);
                Panel learningPathMaterialsContainer = new Panel();

                if (learningPathMaterials.Rows.Count > 0)
                {
                    learningPathMaterialsContainer.ID = "LearningPathMaterialsContainer";
                    learningPathMaterialsContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel learningPathMaterialsLabelContainer = new Panel();
                    learningPathMaterialsLabelContainer.ID = "LearningPathMaterialsLabelContainer";
                    learningPathMaterialsLabelContainer.CssClass = "DetailsViewSectionLabel";
                    learningPathMaterialsContainer.Controls.Add(learningPathMaterialsLabelContainer);

                    Literal learningPathMaterialsLabel = new Literal();
                    learningPathMaterialsLabel.Text = _GlobalResources.LearningPathMaterials;
                    learningPathMaterialsLabelContainer.Controls.Add(learningPathMaterialsLabel);

                    // learning path materials
                    Panel learningPathMaterialsContentContainer = new Panel();
                    learningPathMaterialsContentContainer.ID = "LearningPathMaterialsContentContainer";
                    learningPathMaterialsContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    learningPathMaterialsContainer.Controls.Add(learningPathMaterialsContentContainer);

                    foreach (DataRow row in learningPathMaterials.Rows)
                    {
                        Panel learningPathMaterialLinkContainer = new Panel();
                        learningPathMaterialLinkContainer.ID = "LearningPathMaterialLinkContainer_" + row["idDocumentRepositoryItem"].ToString();
                        learningPathMaterialLinkContainer.CssClass = "DetailsViewListItemContainer";
                        learningPathMaterialsContentContainer.Controls.Add(learningPathMaterialLinkContainer);

                        if (
                            !Convert.ToBoolean(row["isPrivate"])
                            ||
                            AsentiaSessionState.IdSiteUser == 1
                            ||
                            (Convert.ToBoolean(row["isPrivate"]) && learningPath.IsCallingUserEnrolled)
                           )
                        {
                            HyperLink learningPathMaterialLink = new HyperLink();
                            learningPathMaterialLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LEARNINGPATH + learningPath.Id + "/" + Convert.ToString(row["fileName"]);
                            learningPathMaterialLink.Target = "_blank";
                            learningPathMaterialLink.Text = row["label"].ToString();
                            learningPathMaterialLinkContainer.Controls.Add(learningPathMaterialLink);

                            Literal learningPathMaterialSize = new Literal();
                            learningPathMaterialSize.Text = " (" + Asentia.Common.Utility.GetSizeStringFromKB(Convert.ToInt32(row["kb"])) + ")";
                            learningPathMaterialLinkContainer.Controls.Add(learningPathMaterialSize);
                        }
                        else
                        {
                            Literal learningPathMaterialText = new Literal();
                            learningPathMaterialText.Text = row["label"].ToString() + " (" + Asentia.Common.Utility.GetSizeStringFromKB(Convert.ToInt32(row["kb"])) + ")";
                            learningPathMaterialLinkContainer.Controls.Add(learningPathMaterialText);
                        }
                    }
                }

                // write the data to a text writer for json output
                TextWriter textWriter = new StringWriter();
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);

                detailsViewBackButtonContainer.RenderControl(htmlTextWriter);
                learningPathTitleContainer.RenderControl(htmlTextWriter);
                learningPathBasicInformationContainer.RenderControl(htmlTextWriter);
                learningPathDescriptionContainer.RenderControl(htmlTextWriter);
                learningPathCoursesContainer.RenderControl(htmlTextWriter);

                if (courseSampleScreens.Rows.Count > 0)
                { courseSampleScreensContainer.RenderControl(htmlTextWriter); }

                if (learningPathMaterials.Rows.Count > 0)
                { learningPathMaterialsContainer.RenderControl(htmlTextWriter); }

                jsonData.actionSuccessful = true;
                jsonData.html += textWriter.ToString();
                jsonData.exception = String.Empty;
                jsonData.idObject = (int)idLearningPath;
                jsonData.objectTitle = title;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.idObject = null;
                jsonData.objectTitle = null;
            }

            // return jsonData
            return jsonData;
        }
        #endregion

        #region BuildInstructorLedTrainingDetailsPanel
        [WebMethod(EnableSession = true)]
        public static CatalogDetailsJsonData BuildInstructorLedTrainingDetailsPanel(int idInstructorLedTraining)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            CatalogDetailsJsonData jsonData = new CatalogDetailsJsonData();

            // get the ecommerce settings
            EcommerceSettings ecommerceSettings = new EcommerceSettings();

            try
            {
                // get the ilt object
                Asentia.LMS.Library.StandupTraining ilt = new Library.StandupTraining(idInstructorLedTraining);

                // get the ilt's enrollable sessions, important to do it here because we need the count to determine if this will be enrollable
                DataTable iltEnrollableSessions = ilt.GetEnrollableInstances();

                // get the ilt's language-specific title and description
                string title = ilt.Title;
                string description = HttpUtility.HtmlDecode(ilt.Description);
                if (!String.IsNullOrWhiteSpace(description)) { description = new System.Text.RegularExpressions.Regex("<span style=\"[^\"]*\">").Replace(description, ""); }
                string objectives = HttpUtility.HtmlDecode(ilt.Objectives);

                foreach (Library.StandupTraining.LanguageSpecificProperty iltLanguageSpecific in ilt.LanguageSpecificProperties)
                {
                    if (iltLanguageSpecific.LangString == AsentiaSessionState.UserCulture)
                    {
                        title = iltLanguageSpecific.Title;
                        description = HttpUtility.HtmlDecode(iltLanguageSpecific.Description);
                        objectives = HttpUtility.HtmlDecode(iltLanguageSpecific.Objectives);
                        break;
                    }
                }

                // BACK BUTTON - render control detailsViewBackButtonContainer

                Panel detailsViewBackButtonContainer = BuildBackButtonContainer();

                // ILT TITLE - render control iltTitleContainer

                Panel iltTitleContainer = new Panel();
                iltTitleContainer.ID = "ILTTitleContainer";
                iltTitleContainer.CssClass = "DetailsViewTitleContainer";

                Panel iltTitleIconContainer = new Panel();
                iltTitleIconContainer.ID = "ILTTitleIconContainer";
                iltTitleContainer.Controls.Add(iltTitleIconContainer);

                Image iltTitleIcon = new Image();
                iltTitleIcon.CssClass = "LargeIcon";

                if (ilt.Avatar != null)
                {
                    iltTitleIcon.CssClass += " AvatarImage";
                    iltTitleIcon.ImageUrl = SitePathConstants.SITE_STANDUPTRAINING_ROOT + ilt.Id + "/" + ilt.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                }
                else
                { iltTitleIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG); }

                iltTitleIconContainer.Controls.Add(iltTitleIcon);                

                HtmlGenericControl iltTitle = new HtmlGenericControl("p");
                iltTitle.InnerText = title;

                iltTitleContainer.Controls.Add(iltTitle);

                // ILT BASIC INFORMATION - render control iltBasicInformationContainer

                Panel iltBasicInformationContainer = new Panel();
                iltBasicInformationContainer.ID = "ILTBasicInformationContainer";
                iltBasicInformationContainer.CssClass = "DetailsViewBasicInformationContainer";

                // STATUS

                Panel iltStatusWrapperContainer = new Panel();
                iltStatusWrapperContainer.ID = "ILTStatusWrapperContainer";
                iltStatusWrapperContainer.CssClass = "DetailsViewStatusWrapperContainer";

                if (AsentiaSessionState.IdSiteUser == 0) // not logged in user
                {
                    Button iltLoginButton = new Button();
                    iltLoginButton.ID = "ILTLoginButton";
                    iltLoginButton.Text = _GlobalResources.LogInToEnroll;
                    iltLoginButton.OnClientClick = "$(\"#SettingsControlForLogin\").click(); return false;";
                    iltLoginButton.CssClass = "Button ActionButton EnrollButton";
                    iltStatusWrapperContainer.Controls.Add(iltLoginButton);
                }
                else if (AsentiaSessionState.IdSiteUser == 1) // logged in user is administrator
                {
                    if (iltEnrollableSessions.Rows.Count > 0)
                    {
                        Button iltOpenButton = new Button();
                        iltOpenButton.ID = "ILTOpenButton";
                        iltOpenButton.Text = _GlobalResources.Open;
                        iltOpenButton.CssClass = "Button ActionButton OpenButton";
                        iltOpenButton.Enabled = false;
                        iltStatusWrapperContainer.Controls.Add(iltOpenButton);
                    }
                    else
                    {
                        Button iltClosedButton = new Button();
                        iltClosedButton.ID = "ILTClosedButton";
                        iltClosedButton.Text = _GlobalResources.Closed;
                        iltClosedButton.CssClass = "Button ActionButton ClosedButton";
                        iltClosedButton.Enabled = false;
                        iltStatusWrapperContainer.Controls.Add(iltClosedButton);
                    }
                }
                else // logged in user is a normal user
                {
                    if (!ilt.IsCallingUserEnrolledInUpcomingSession) // user is not already enrolled in an upcoming session
                    {
                        if (iltEnrollableSessions.Rows.Count > 0) // ilt has enrollable sessions, make the proper "enroll" or "purchase" button based on ecommerce settings and cost
                        {
                            if (ecommerceSettings.IsEcommerceSetAndVerified && ilt.Cost > 0) // ecommerce is set and verified and ilt has a cost, make a purchase button
                            {
                                if (ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL) // paypal, make a buy now button
                                {
                                    // Note: Buy Now button should always be available here. If any item is already in the active PayPal cart because of a previous
                                    // purchase that was not followed through with PayPal, it will be cleared and replaced by the method handling the button click.

                                    Button iltPayPalButton = new Button();
                                    iltPayPalButton.ID = "ILTPayPalButton";
                                    iltPayPalButton.Text = _GlobalResources.BuyNow;
                                    iltPayPalButton.CssClass = "Button ActionButton PayPalButton";
                                    iltPayPalButton.OnClientClick = "LoadEnrollInInstructorLedTrainingModalContent(" + idInstructorLedTraining + ", \"paypal\"); return false;";
                                    iltStatusWrapperContainer.Controls.Add(iltPayPalButton);
                                }
                                else // all other payment processors use the cart, make an add to cart button, or link to my cart if item is already there
                                {
                                    if (!_IsItemInCart(idInstructorLedTraining, PurchaseItemType.InstructorLedTraining))
                                    {
                                        Button iltAddToCartButton = new Button();
                                        iltAddToCartButton.ID = "ILTAddToCartButton";
                                        iltAddToCartButton.Text = _GlobalResources.AddToCart;
                                        iltAddToCartButton.CssClass = "Button ActionButton CartButton";
                                        iltAddToCartButton.OnClientClick = "LoadEnrollInInstructorLedTrainingModalContent(" + idInstructorLedTraining + ", \"addtocart\"); return false;";
                                        iltStatusWrapperContainer.Controls.Add(iltAddToCartButton);
                                    }
                                    else // make my cart link
                                    {
                                        Button iltMyCartButton = new Button();
                                        iltMyCartButton.ID = "ILTMyCartButton";
                                        iltMyCartButton.Text = _GlobalResources.AddToCart;
                                        iltMyCartButton.CssClass = "Button ActionButton CartButton";
                                        iltMyCartButton.PostBackUrl = "/myprofile/MyCart.aspx";
                                        iltStatusWrapperContainer.Controls.Add(iltMyCartButton);
                                    }
                                }
                            }
                            else // just make an enroll button
                            {
                                Button iltEnrollButton = new Button();
                                iltEnrollButton.ID = "ILTEnrollButton";
                                iltEnrollButton.Text = _GlobalResources.Enroll;
                                iltEnrollButton.CssClass = "Button ActionButton EnrollButton";
                                iltEnrollButton.Attributes.Add("onclick", "LoadEnrollInInstructorLedTrainingModalContent(" + idInstructorLedTraining + ", \"enroll\"); return false;");
                                iltStatusWrapperContainer.Controls.Add(iltEnrollButton);
                            }
                        }
                        else // no enrollable sessions, show closed
                        {
                            Button iltClosedButton = new Button();
                            iltClosedButton.ID = "ILTClosedButton";
                            iltClosedButton.Text = _GlobalResources.Closed;
                            iltClosedButton.CssClass = "Button ActionButton ClosedButton";
                            iltClosedButton.Enabled = false;
                            iltStatusWrapperContainer.Controls.Add(iltClosedButton);
                        }
                    }
                    else // user is already enrolled in an upcoming session, show that
                    {
                        Button iltEnrolledButton = new Button();
                        iltEnrolledButton.ID = "ILTEnrolledButton";
                        iltEnrolledButton.Text = _GlobalResources.Enrolled;
                        iltEnrolledButton.CssClass = "Button ActionButton EnrollButton";
                        iltEnrolledButton.Enabled = false;
                        iltStatusWrapperContainer.Controls.Add(iltEnrolledButton);
                    }
                }

                // attach status wrapper to basic information container
                iltBasicInformationContainer.Controls.Add(iltStatusWrapperContainer);

                // COST

                // if ecommerce is set and verified, show the cost
                if (ecommerceSettings.IsEcommerceSetAndVerified)
                {
                    Panel iltCostWrapperContainer = new Panel();
                    iltCostWrapperContainer.ID = "ILTCostWrapperContainer";
                    iltCostWrapperContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                    Panel iltCostContainer = new Panel();
                    iltCostContainer.ID = "ILTCostContainer";

                    Label iltCostLabel = new Label();
                    iltCostLabel.CssClass = "bold";
                    iltCostLabel.Text = _GlobalResources.Cost + ": ";
                    iltCostContainer.Controls.Add(iltCostLabel);

                    Label iltCostValueLabel = new Label();

                    if (ilt.Cost > 0)
                    { iltCostValueLabel.Text = String.Format("{0}{1:N2} ({2})", ecommerceSettings.CurrencySymbol, ilt.Cost, ecommerceSettings.CurrencyCode); }
                    else
                    { iltCostValueLabel.Text = _GlobalResources.Free; }

                    iltCostContainer.Controls.Add(iltCostValueLabel);

                    iltCostWrapperContainer.Controls.Add(iltCostContainer);

                    // attach cost wrapper to basic information container
                    iltBasicInformationContainer.Controls.Add(iltCostWrapperContainer);
                }

                // ILT DESCRIPTION - render control iltDescriptionContainer

                Panel iltDescriptionContainer = new Panel();
                iltDescriptionContainer.ID = "ILTDescriptionContainer";
                iltDescriptionContainer.CssClass = "DetailsViewSectionContainer";

                // label
                Panel iltDescriptionLabelContainer = new Panel();
                iltDescriptionLabelContainer.ID = "ILTDescriptionLabelContainer";
                iltDescriptionLabelContainer.CssClass = "DetailsViewSectionLabel";
                iltDescriptionContainer.Controls.Add(iltDescriptionLabelContainer);

                Literal iltDescriptionLabel = new Literal();
                iltDescriptionLabel.Text = _GlobalResources.Description;
                iltDescriptionLabelContainer.Controls.Add(iltDescriptionLabel);

                // description
                Panel iltDescriptionContentContainer = new Panel();
                iltDescriptionContentContainer.ID = "ILTDescriptionContentContainer";
                iltDescriptionContentContainer.CssClass = "DetailsViewSectionContentContainer";
                iltDescriptionContainer.Controls.Add(iltDescriptionContentContainer);

                Label iltDescription = new Label();
                iltDescription.ID = "ILTDescription";
                iltDescription.Text = description;

                iltDescriptionContentContainer.Controls.Add(iltDescription);

                // ILT OBJECTIVES - render control iltObjectivesContainer
                Panel iltObjectivesContainer = new Panel();

                if (!String.IsNullOrWhiteSpace(objectives))
                {                    
                    iltObjectivesContainer.ID = "ILTObjectivesContainer";
                    iltObjectivesContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel iltObjectivesLabelContainer = new Panel();
                    iltObjectivesLabelContainer.ID = "ILTObjectivesLabelContainer";
                    iltObjectivesLabelContainer.CssClass = "DetailsViewSectionLabel";
                    iltObjectivesContainer.Controls.Add(iltObjectivesLabelContainer);

                    Literal iltObjectivesLabel = new Literal();
                    iltObjectivesLabel.Text = _GlobalResources.Objectives;
                    iltObjectivesLabelContainer.Controls.Add(iltObjectivesLabel);

                    // objectives
                    Panel iltObjectivesContentContainer = new Panel();
                    iltObjectivesContentContainer.ID = "ILTObjectivesContentContainer";
                    iltObjectivesContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    iltObjectivesContainer.Controls.Add(iltObjectivesContentContainer);

                    Label iltObjectives = new Label();
                    iltObjectives.ID = "ILTObjectives";
                    iltObjectives.Text = objectives;

                    iltObjectivesContentContainer.Controls.Add(iltObjectives);
                }

                // ILT UPCOMING SESSIONS - render control iltUpcomingSessionsContainer

                Panel iltUpcomingSessionsContainer = new Panel();
                iltUpcomingSessionsContainer.ID = "ILTUpcomingSessionsContainer";
                iltUpcomingSessionsContainer.CssClass = "DetailsViewSectionContainer";

                // label
                Panel iltUpcomingSessionsLabelContainer = new Panel();
                iltUpcomingSessionsLabelContainer.ID = "ILTUpcomingSessionsLabelContainer";
                iltUpcomingSessionsLabelContainer.CssClass = "DetailsViewSectionLabel";
                iltUpcomingSessionsContainer.Controls.Add(iltUpcomingSessionsLabelContainer);

                Literal iltUpcomingSessionsLabel = new Literal();
                iltUpcomingSessionsLabel.Text = _GlobalResources.UpcomingSessions;
                iltUpcomingSessionsLabelContainer.Controls.Add(iltUpcomingSessionsLabel);

                // upcoming sessions
                Panel iltUpcomingSessionsContentContainer = new Panel();
                iltUpcomingSessionsContentContainer.ID = "ILTUpcomingSessionsContentContainer";
                iltUpcomingSessionsContentContainer.CssClass = "DetailsViewSectionContentContainer";
                iltUpcomingSessionsContainer.Controls.Add(iltUpcomingSessionsContentContainer);

                if (iltEnrollableSessions.Rows.Count > 0)
                {
                    // loop through available sessions creating a list item for each and listing all meeting times for each
                    int idInstructorLedTrainingInstance = 0;
                    int meetingTimesCount = 0;

                    foreach (DataRow row in iltEnrollableSessions.Rows)
                    {
                        Panel iltUpcomingSessionContainer = new Panel();

                        if (idInstructorLedTrainingInstance != Convert.ToInt32(row["idStandupTrainingInstance"]))
                        {
                            meetingTimesCount = 0;

                            // instance id
                            idInstructorLedTrainingInstance = Convert.ToInt32(row["idStandupTrainingInstance"]);

                            iltUpcomingSessionContainer.ID = "ILTUpcomingSessionContainer_" + idInstructorLedTrainingInstance.ToString();
                            iltUpcomingSessionContainer.CssClass = "DetailsViewSessionItemContainer";
                            iltUpcomingSessionsContentContainer.Controls.Add(iltUpcomingSessionContainer);

                            // title
                            Panel iltUpcomingSessionTitleContainer = new Panel();
                            iltUpcomingSessionTitleContainer.ID = "ILTUpcomingSessionTitleContainer_" + idInstructorLedTrainingInstance.ToString();
                            iltUpcomingSessionTitleContainer.CssClass = "DetailsViewILTSessionTitleContainer";
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionTitleContainer);

                            Literal iltUpcomingSessionTitle = new Literal();
                            iltUpcomingSessionTitle.Text = row["title"].ToString();
                            iltUpcomingSessionTitleContainer.Controls.Add(iltUpcomingSessionTitle);

                            // available seats
                            Panel iltUpcomingSessionAvailableSeatsContainer = new Panel();
                            iltUpcomingSessionAvailableSeatsContainer.ID = "ILTUpcomingSessionAvailableSeatsContainer_" + idInstructorLedTrainingInstance.ToString();
                            iltUpcomingSessionAvailableSeatsContainer.CssClass = "DetailsViewILTSessionPropertyContainer";
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionAvailableSeatsContainer);

                            Label iltUpcomingSessionAvailableSeatsLabel = new Label();
                            iltUpcomingSessionAvailableSeatsLabel.Text = _GlobalResources.Status + ": ";
                            iltUpcomingSessionAvailableSeatsContainer.Controls.Add(iltUpcomingSessionAvailableSeatsLabel);

                            Label iltUpcomingSessionAvailableSeatsValue = new Label();
                            iltUpcomingSessionAvailableSeatsValue.CssClass = "DetailsViewILTSessionAvailableSeatsLabel";

                            iltUpcomingSessionAvailableSeatsValue.Text = row["availableSeats"].ToString() + " " + _GlobalResources.of_lower + " " + row["seats"].ToString();

                            int availableSeats = Convert.ToInt32(row["availableSeats"]);
                            int seats = Convert.ToInt32(row["seats"]);
                            int availableWaitingSeats = Convert.ToInt32(row["availableWaitingSeats"]);
                            int waitingSeats = Convert.ToInt32(row["waitingSeats"]);

                            if (availableSeats > 0)
                            { iltUpcomingSessionAvailableSeatsValue.Text = _GlobalResources.Seat_sAvailable + " (" + availableSeats + "/" + seats + ")"; }
                            else
                            {
                                iltUpcomingSessionAvailableSeatsValue.CssClass = "DetailsViewILTSessionAvailableSeatsLabelWaitingList";
                                iltUpcomingSessionAvailableSeatsValue.Text = _GlobalResources.WaitingListAvailable + " (" + availableWaitingSeats + "/" + waitingSeats + ")"; 
                            }

                            iltUpcomingSessionAvailableSeatsContainer.Controls.Add(iltUpcomingSessionAvailableSeatsValue);

                            // location
                            Panel iltUpcomingSessionLocationContainer = new Panel();
                            iltUpcomingSessionLocationContainer.ID = "ILTUpcomingSessionLocationContainer_" + idInstructorLedTrainingInstance.ToString();
                            iltUpcomingSessionLocationContainer.CssClass = "DetailsViewILTSessionPropertyContainer";
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionLocationContainer);

                            Label iltUpcomingSessionLocationLabel = new Label();
                            iltUpcomingSessionLocationLabel.Text = _GlobalResources.Location + ": ";
                            iltUpcomingSessionLocationContainer.Controls.Add(iltUpcomingSessionLocationLabel);

                            Label iltUpcomingSessionLocationValue = new Label();

                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                if (!String.IsNullOrWhiteSpace(row["city"].ToString()) || !String.IsNullOrWhiteSpace(row["province"].ToString()))
                                {
                                    if (!String.IsNullOrWhiteSpace(row["city"].ToString()))
                                    { iltUpcomingSessionLocationValue.Text += row["city"].ToString(); }

                                    if (!String.IsNullOrWhiteSpace(row["province"].ToString()))
                                    { iltUpcomingSessionLocationValue.Text += ", " + row["province"].ToString(); }
                                }
                            }
                            else if (Convert.ToInt32(row["type"]) == 2)
                            { iltUpcomingSessionLocationValue.Text += _GlobalResources.GoToMeeting; }
                            else if (Convert.ToInt32(row["type"]) == 3)
                            { iltUpcomingSessionLocationValue.Text += _GlobalResources.GoToWebinar; }
                            else if (Convert.ToInt32(row["type"]) == 4)
                            { iltUpcomingSessionLocationValue.Text += _GlobalResources.GoToTraining; }
                            else if (Convert.ToInt32(row["type"]) == 5)
                            { iltUpcomingSessionLocationValue.Text += _GlobalResources.WebEx; }
                            else
                            { iltUpcomingSessionLocationValue.Text += _GlobalResources.Online; }

                            iltUpcomingSessionLocationContainer.Controls.Add(iltUpcomingSessionLocationValue);
                            
                            // instructors
                            if (!String.IsNullOrWhiteSpace(row["instructorDisplayNames"].ToString()))
                            {
                                // instructors label
                                Panel iltUpcomingSessionInstructorsLabelContainer = new Panel();
                                iltUpcomingSessionInstructorsLabelContainer.ID = "ILTUpcomingSessionInstructorsLabelContainer_" + idInstructorLedTrainingInstance.ToString();
                                iltUpcomingSessionInstructorsLabelContainer.CssClass = "DetailsViewILTSessionPropertyContainer";
                                iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionInstructorsLabelContainer);

                                Label iltUpcomingSessionInstructorLabel = new Label();
                                iltUpcomingSessionInstructorLabel.Text = _GlobalResources.Instructor_s + ": ";
                                iltUpcomingSessionInstructorsLabelContainer.Controls.Add(iltUpcomingSessionInstructorLabel);

                                // instructors
                                string[] instructors = row["instructorDisplayNames"].ToString().Split('|');

                                for (int i = 0; i < instructors.Length; i++)
                                {
                                    Panel iltUpcomingSessionInstructorContainer = new Panel();
                                    iltUpcomingSessionInstructorContainer.ID = "ILTUpcomingSessionInstructorContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + i.ToString();
                                    iltUpcomingSessionInstructorContainer.CssClass = "DetailsViewILTSessionInstructorContainer";

                                    Literal iltUpcomingSessionInstructor = new Literal();
                                    iltUpcomingSessionInstructor.Text = instructors[i];

                                    iltUpcomingSessionInstructorContainer.Controls.Add(iltUpcomingSessionInstructor);
                                    iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionInstructorContainer);
                                }
                            }

                            // meeting times label
                            Panel iltUpcomingSessionMeetingTimesLabelContainer = new Panel();
                            iltUpcomingSessionMeetingTimesLabelContainer.ID = "ILTUpcomingSessionMeetingTimesLabelContainer_" + idInstructorLedTrainingInstance;
                            iltUpcomingSessionMeetingTimesLabelContainer.CssClass = "DetailsViewILTSessionPropertyContainer";
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionMeetingTimesLabelContainer);

                            Label iltUpcomingSessionMeetingTimesLabel = new Label();
                            iltUpcomingSessionMeetingTimesLabel.Text = _GlobalResources.MeetingTime_s + ": ";
                            iltUpcomingSessionMeetingTimesLabelContainer.Controls.Add(iltUpcomingSessionMeetingTimesLabel);

                            // meeting time
                            Panel iltUpcomingSessionMeetingTimeContainer = new Panel();
                            iltUpcomingSessionMeetingTimeContainer.ID = "ILTUpcomingSessionMeetingTimeContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                            iltUpcomingSessionMeetingTimeContainer.CssClass = "DetailsViewILTSessionMeetingTimeContainer";

                            DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                            DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                            // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                int idTimezone = Convert.ToInt32(row["idTimezone"]);
                                string tzDotNetName = new Timezone(idTimezone).dotNetName;

                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            }
                            else
                            {
                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            }

                            Literal iltUpcomingSessionMeetingTime = new Literal();
                            iltUpcomingSessionMeetingTime.Text = dtStart.ToString() + " - " + dtEnd.ToString();

                            iltUpcomingSessionMeetingTimeContainer.Controls.Add(iltUpcomingSessionMeetingTime);
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionMeetingTimeContainer);
                        }
                        else // additional meeting times
                        {
                            // meeting time
                            Panel iltUpcomingSessionMeetingTimeContainer = new Panel();
                            iltUpcomingSessionMeetingTimeContainer.ID = "ILTUpcomingSessionMeetingTimeContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                            iltUpcomingSessionMeetingTimeContainer.CssClass = "DetailsViewILTSessionMeetingTimeContainer";

                            DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                            DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                            // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                int idTimezone = Convert.ToInt32(row["idTimezone"]);
                                string tzDotNetName = new Timezone(idTimezone).dotNetName;

                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            }
                            else
                            {
                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            }

                            Literal iltUpcomingSessionMeetingTime = new Literal();
                            iltUpcomingSessionMeetingTime.Text = dtStart.ToString() + " - " + dtEnd.ToString();

                            iltUpcomingSessionMeetingTimeContainer.Controls.Add(iltUpcomingSessionMeetingTime);
                            iltUpcomingSessionContainer.Controls.Add(iltUpcomingSessionMeetingTimeContainer);
                        }
                    }
                }
                else
                {
                    Panel iltUpcomingSessionContainer = new Panel();
                    iltUpcomingSessionContainer.ID = "ILTUpcomingSessionContainer_NoUpcomingSessions";
                    iltUpcomingSessionContainer.CssClass = "DetailsViewListItemContainer";
                    iltUpcomingSessionsContentContainer.Controls.Add(iltUpcomingSessionContainer);

                    Literal iltUpcomingSession = new Literal();
                    iltUpcomingSession.Text = _GlobalResources.ThereAreNoUpcomingSessions;
                    iltUpcomingSessionContainer.Controls.Add(iltUpcomingSession);
                }

                // write the data to a text writer for json output
                TextWriter textWriter = new StringWriter();
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);

                detailsViewBackButtonContainer.RenderControl(htmlTextWriter);
                iltTitleContainer.RenderControl(htmlTextWriter);
                iltBasicInformationContainer.RenderControl(htmlTextWriter);
                iltDescriptionContainer.RenderControl(htmlTextWriter);
                if (!String.IsNullOrWhiteSpace(objectives))
                {
                    iltObjectivesContainer.RenderControl(htmlTextWriter);
                }
                iltUpcomingSessionsContainer.RenderControl(htmlTextWriter);

                jsonData.actionSuccessful = true;
                jsonData.html += textWriter.ToString();
                jsonData.exception = String.Empty;
                jsonData.idObject = (int)idInstructorLedTraining;
                jsonData.objectTitle = title;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.idObject = null;
                jsonData.objectTitle = null;
            }

            // return jsonData
            return jsonData;
        }
        #endregion

        #region BuildCommunityDetailsPanel
        [WebMethod(EnableSession = true)]
        public static CatalogDetailsJsonData BuildCommunityDetailsPanel(int idGroup)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            CatalogDetailsJsonData jsonData = new CatalogDetailsJsonData();

            try
            {
                // get the course object
                Asentia.UMS.Library.Group group = new UMS.Library.Group(idGroup);

                // get the group's language-specific title and description
                string title = group.Name;
                string shortDescription = group.ShortDescription;
                string description = HttpUtility.HtmlDecode(group.LongDescription);
                if(!String.IsNullOrWhiteSpace(description)){description = new System.Text.RegularExpressions.Regex("<span style=\"[^\"]*\">").Replace(description, "");}

                foreach (UMS.Library.Group.LanguageSpecificProperty groupLanguageSpecific in group.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecific.LangString == AsentiaSessionState.UserCulture)
                    {
                        title = groupLanguageSpecific.Name;
                        shortDescription = groupLanguageSpecific.ShortDescription;
                        description = HttpUtility.HtmlDecode(groupLanguageSpecific.LongDescription);
                        break;
                    }
                }

                // BACK BUTTON - render control detailsViewBackButtonContainer

                Panel detailsViewBackButtonContainer = BuildBackButtonContainer();

                // COMMUNITY TITLE - render control communityTitleContainer

                Panel communityTitleContainer = new Panel();
                communityTitleContainer.ID = "CommunityTitleContainer";
                communityTitleContainer.CssClass = "DetailsViewTitleContainer";

                Panel communityTitleIconContainer = new Panel();
                communityTitleIconContainer.ID = "CommunityTitleIconContainer";
                communityTitleContainer.Controls.Add(communityTitleIconContainer);

                Image communityTitleIcon = new Image();
                communityTitleIcon.CssClass = "LargeIcon";

                if (group.Avatar != null)
                {
                    communityTitleIcon.CssClass += " AvatarImage";
                    communityTitleIcon.ImageUrl = SitePathConstants.SITE_GROUPS_ROOT + group.Id + "/" + group.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                }
                else
                { communityTitleIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG); }

                communityTitleIconContainer.Controls.Add(communityTitleIcon);

                HtmlGenericControl communityTitle = new HtmlGenericControl("p");
                communityTitle.InnerText = title;

                communityTitleContainer.Controls.Add(communityTitle);

                // COMMUNITY BASIC INFORMATION - render control communityBasicInformationContainer

                Panel communityBasicInformationContainer = new Panel();
                communityBasicInformationContainer.ID = "CommunityBasicInformationContainer";
                communityBasicInformationContainer.CssClass = "DetailsViewBasicInformationContainer";

                // STATUS

                Panel communityStatusWrapperContainer = new Panel();
                communityStatusWrapperContainer.ID = "CommunityStatusWrapperContainer";
                communityStatusWrapperContainer.CssClass = "DetailsViewStatusWrapperContainer";

                if (AsentiaSessionState.IdSiteUser == 0) // not logged in user
                {
                    Button communityLoginButton = new Button();
                    communityLoginButton.ID = "CommunityLoginButton";
                    communityLoginButton.Text = _GlobalResources.LogInToJoin;
                    communityLoginButton.OnClientClick = "$(\"#SettingsControlForLogin\").click(); return false;";
                    communityLoginButton.CssClass = "Button ActionButton JoinButton";
                    communityStatusWrapperContainer.Controls.Add(communityLoginButton);
                }
                else if (AsentiaSessionState.IdSiteUser == 1) // logged in user is administrator
                {
                    Button communityOpenButton = new Button();
                    communityOpenButton.ID = "CommunityOpenButton";
                    communityOpenButton.Text = _GlobalResources.Open;
                    communityOpenButton.CssClass = "Button ActionButton OpenButton";
                    communityOpenButton.Enabled = false;
                    communityStatusWrapperContainer.Controls.Add(communityOpenButton);                    
                }
                else // logged in user is a normal user
                {
                    if (!group.IsCallingUserAMember) // user is not already a member of the group
                    {
                        Button communityJoinButton = new Button();
                        communityJoinButton.ID = "CommunityJoinButton";
                        communityJoinButton.Text = _GlobalResources.Join;
                        communityJoinButton.CssClass = "Button ActionButton OpenButton";
                        communityJoinButton.Attributes.Add("onclick", "JoinUnjoinCommunity(" + idGroup + ", true); return false;");
                        communityStatusWrapperContainer.Controls.Add(communityJoinButton);
                    }
                    else // user is already a member, add an unjoin button
                    {
                        Button communityUnjoinButton = new Button();
                        communityUnjoinButton.ID = "CommunityUnjoinButton";
                        communityUnjoinButton.Text = _GlobalResources.Unjoin;
                        communityUnjoinButton.CssClass = "Button ActionButton ClosedButton";
                        communityUnjoinButton.Attributes.Add("onclick", "JoinUnjoinCommunity(" + idGroup + ", false); return false;");
                        communityStatusWrapperContainer.Controls.Add(communityUnjoinButton);
                    }
                }

                // attach status wrapper to basic information container
                communityBasicInformationContainer.Controls.Add(communityStatusWrapperContainer);

                // WALL

                if (group.IsFeedActive == true && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
                {
                    Panel communityWallLinkContainer = new Panel();
                    communityWallLinkContainer.ID = "CommunityWallLinkContainer";
                    communityWallLinkContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                    HyperLink communityWallLink = new HyperLink();
                    communityWallLink.ID = "CommunityWallLink";
                    communityWallLink.CssClass = "ImageLink";
                    communityWallLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION,
                                                                        ImageFiles.EXT_PNG);
                    communityWallLink.NavigateUrl = "/groups/Wall.aspx?id=" + group.Id.ToString();
                    communityWallLink.ToolTip = _GlobalResources.JumpToCommunityDiscussion;
                    communityWallLinkContainer.Controls.Add(communityWallLink);

                    // if the user is not a member of the group and not admin, disable the link
                    if (
                        (AsentiaSessionState.IdSiteUser > 1 && !group.IsCallingUserAMember)
                        ||
                        AsentiaSessionState.IdSiteUser == 0
                       )
                    {
                        communityWallLink.NavigateUrl = null;
                        communityWallLink.ToolTip = _GlobalResources.ThisCommunityHasADiscussion;
                        communityWallLink.Enabled = false;
                    }

                    // attach wall link wrapper to basic information container
                    communityBasicInformationContainer.Controls.Add(communityWallLinkContainer);
                }

                // COMMUNITY DESCRIPTION - render control communityDescriptionContainer

                Panel communityDescriptionContainer = new Panel();
                communityDescriptionContainer.ID = "CommunityDescriptionContainer";
                communityDescriptionContainer.CssClass = "DetailsViewSectionContainer";

                // label
                Panel communityDescriptionLabelContainer = new Panel();
                communityDescriptionLabelContainer.ID = "CommunityDescriptionLabelContainer";
                communityDescriptionLabelContainer.CssClass = "DetailsViewSectionLabel";
                communityDescriptionContainer.Controls.Add(communityDescriptionLabelContainer);

                Literal communityDescriptionLabel = new Literal();
                communityDescriptionLabel.Text = _GlobalResources.Description;
                communityDescriptionLabelContainer.Controls.Add(communityDescriptionLabel);

                // description
                Panel communityDescriptionContentContainer = new Panel();
                communityDescriptionContentContainer.ID = "CommunityDescriptionContentContainer";
                communityDescriptionContentContainer.CssClass = "DetailsViewSectionContentContainer";
                communityDescriptionContainer.Controls.Add(communityDescriptionContentContainer);

                Label communityDescription = new Label();
                communityDescription.ID = "CommunityDescription";

                if (!String.IsNullOrWhiteSpace(description))
                { communityDescription.Text = description; }
                else
                { communityDescription.Text = shortDescription; }

                communityDescriptionContentContainer.Controls.Add(communityDescription);

                // COMMUNITY DOCUMENTS - render control communityDocumentsContainer

                DataTable communityDocuments = group.GetGroupDocuments(null);
                Panel communityDocumentsContainer = new Panel();

                if (communityDocuments.Rows.Count > 0 && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE))
                {
                    communityDocumentsContainer.ID = "CommunityDocumentsContainer";
                    communityDocumentsContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel communityDocumentsLabelContainer = new Panel();
                    communityDocumentsLabelContainer.ID = "CommunityDocumentsLabelContainer";
                    communityDocumentsLabelContainer.CssClass = "DetailsViewSectionLabel";
                    communityDocumentsContainer.Controls.Add(communityDocumentsLabelContainer);

                    Literal communityDocumentsLabel = new Literal();
                    communityDocumentsLabel.Text = _GlobalResources.Documents;
                    communityDocumentsLabelContainer.Controls.Add(communityDocumentsLabel);

                    // community documents
                    Panel communityDocumentsContentContainer = new Panel();
                    communityDocumentsContentContainer.ID = "CommunityDocumentsContentContainer";
                    communityDocumentsContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    communityDocumentsContainer.Controls.Add(communityDocumentsContentContainer);

                    foreach (DataRow row in communityDocuments.Rows)
                    {
                        Panel communityDocumentLinkContainer = new Panel();
                        communityDocumentLinkContainer.ID = "CommunityDocumentLinkContainer_" + row["idDocumentRepositoryItem"].ToString();
                        communityDocumentLinkContainer.CssClass = "DetailsViewListItemContainer";
                        communityDocumentsContentContainer.Controls.Add(communityDocumentLinkContainer);

                        if (
                            AsentiaSessionState.IdSiteUser == 1
                            ||
                            group.IsCallingUserAMember
                        )
                        {
                            HyperLink communityDocumentLink = new HyperLink();
                            communityDocumentLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_GROUP + group.Id + "/" + Convert.ToString(row["fileName"]);
                            communityDocumentLink.Target = "_blank";
                            communityDocumentLink.Text = row["label"].ToString();
                            communityDocumentLinkContainer.Controls.Add(communityDocumentLink);

                            Literal communityDocumentSize = new Literal();
                            communityDocumentSize.Text = " (" + Asentia.Common.Utility.GetSizeStringFromKB(Convert.ToInt32(row["kb"])) + ")";
                            communityDocumentLinkContainer.Controls.Add(communityDocumentSize);
                        }
                        else
                        {
                            Literal communityDocumentText = new Literal();
                            communityDocumentText.Text = row["label"].ToString() + " (" + Asentia.Common.Utility.GetSizeStringFromKB(Convert.ToInt32(row["kb"])) + ")";
                            communityDocumentLinkContainer.Controls.Add(communityDocumentText);
                        }
                    }
                }

                // write the data to a text writer for json output
                TextWriter textWriter = new StringWriter();
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);

                detailsViewBackButtonContainer.RenderControl(htmlTextWriter);
                communityTitleContainer.RenderControl(htmlTextWriter);
                communityBasicInformationContainer.RenderControl(htmlTextWriter);
                communityDescriptionContainer.RenderControl(htmlTextWriter);

                if (communityDocuments.Rows.Count > 0 && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE))
                { communityDocumentsContainer.RenderControl(htmlTextWriter); }


                jsonData.actionSuccessful = true;
                jsonData.html += textWriter.ToString();
                jsonData.exception = String.Empty;
                jsonData.idObject = (int)idGroup;
                jsonData.objectTitle = title;
                
                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.idObject = null;
                jsonData.objectTitle = null;
            }

            // return jsonData
            return jsonData;
        }
        #endregion

        #region _SearchButton_Command
        /// <summary>
        /// Handles the search button click.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _SearchButton_Command(object sender, CommandEventArgs e)
        {
            // if the search box isn't blank, get the value and redirect to the search page, otherwise redirect back
            if (!String.IsNullOrWhiteSpace(this._SearchBox.Text))
            { Response.Redirect("/catalog/Search.aspx?searchParam=" + Server.UrlEncode(this._SearchBox.Text)); }
            else
            { Response.Redirect("/catalog"); }
        }
        #endregion

        #region _EnrollCatalog_Command
        /// <summary>
        /// Handles the event initiated by clicking the enroll button for a catalog.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _EnrollCatalog_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // declare an ecommerce settings object
                EcommerceSettings ecommerceSettings = new EcommerceSettings();

                // hide the buttons
                this._EnrollCatalogModal.SubmitButton.Visible = false;
                this._EnrollCatalogModal.CloseButton.Visible = false;

                // get the catalog and the process method ("enroll", "paypal", or "addtocart")
                string[] enrollCatalogData = this._EnrollCatalogData.Value.Split('|');
                int idCatalog = Convert.ToInt32(enrollCatalogData[0]);
                string processorMethod = enrollCatalogData[1].ToString();

                LMS.Library.Catalog catalogObject = new LMS.Library.Catalog(idCatalog);

                // Note: The processorMethod is received from a hidden input where it was calculated when creating
                // the button for "enroll action" in the object's details pane. Due to the fact that one could manipulate
                // that input client-side, we need to do a sanity check to ensure that the requested action is appropriate.

                // "Enroll" should only be allowed from here if:
                //      - Ecommerce is not set and verified.
                //      - Ecommerce is set and verified AND the object is free (cost = 0).
                // "PayPal" should only be allowed from here if:
                //      - Ecommerce is set and verified, the processor is PayPal, and the object has a cost.
                // "AddToCart" should only be allowed from here if:
                //      - Ecommerce is set and verified, the processor is something other than "None" or "PayPal", and the object has a cost.

                if (processorMethod == "enroll" && ((!ecommerceSettings.IsEcommerceSetAndVerified) || (ecommerceSettings.IsEcommerceSetAndVerified && (catalogObject.CalculatedCost == 0 || catalogObject.CalculatedCost == null))))
                {
                    // enroll the user in the catalog
                    LMS.Library.Enrollment enrollmentObject = new LMS.Library.Enrollment();
                    enrollmentObject.EnrollCatalog(idCatalog, AsentiaSessionState.IdSiteUser, 0);

                    // display feedback
                    this._EnrollCatalogModalDetailsPanel.Controls.Clear();
                    this._EnrollCatalogModal.DisplayFeedback(String.Format(_GlobalResources.YouHaveBeenEnrolledInAllCoursesWithinThisCatalogSuccessfullyClickHereToGoToYourDashboardToViewYourCourseEnrollments, "<a href=\"/dashboard\">", "</a>"), false);
                }
                else if (processorMethod == "paypal" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL && catalogObject.CalculatedCost > 0)
                {                    
                    // add the item to the cart
                    int idTransactionItem = this._AddItemToCart(catalogObject.IdCatalog, PurchaseItemType.Catalog, catalogObject.Title, catalogObject.ShortDescription, (double)catalogObject.CalculatedCost, true);

                    // set-up next steps for paypal processing
                    this._ApplyCouponPayPalBuyNowData.Value = idTransactionItem.ToString();
                    this._BuildCouponCodeFormForPayPalBuyNow(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
                    this._EnrollCatalogModal.HideModal();
                    this._ApplyCouponPayPalBuyNowModal.SubmitButton.Visible = true;
                    this._ApplyCouponPayPalBuyNowModal.CloseButton.Visible = true;
                    this._ApplyCouponPayPalBuyNowModal.ShowModal();
                }
                else if (processorMethod == "addtocart" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_NONE && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_PAYPAL && catalogObject.CalculatedCost > 0)
                {
                    // add the item to the cart
                    this._AddItemToCart(catalogObject.IdCatalog, PurchaseItemType.Catalog, catalogObject.Title, catalogObject.ShortDescription, (double)catalogObject.CalculatedCost, false);

                    // display feedback
                    this._EnrollCatalogModalDetailsPanel.Controls.Clear();
                    this._EnrollCatalogModal.DisplayFeedback(_GlobalResources.CatalogAddedToCartSuccessfully, false);
                }
                else // invalid processor method, throw exception
                { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingThisEnrollmentPleaseContactAnAdministrator); }

                // register a script to reload the view
                StringBuilder reloadView = new StringBuilder();
                reloadView.AppendLine("ShowCatalogDetails(" + idCatalog + ", \"jump\", false);");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadCatalogViewAfterEnrollment", reloadView.ToString(), true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._EnrollCatalogModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._EnrollCatalogModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._EnrollCatalogModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._EnrollCatalogModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._EnrollCatalogModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _EnrollCourse_Command
        /// <summary>
        /// Handles the event initiated by clicking the enroll button for a course.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _EnrollCourse_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // declare an ecommerce settings object
                EcommerceSettings ecommerceSettings = new EcommerceSettings();

                // hide the buttons
                this._EnrollCourseModal.SubmitButton.Visible = false;
                this._EnrollCourseModal.CloseButton.Visible = false;

                // get the course and the process method ("enroll", "paypal", or "addtocart")
                string[] enrollCourseData = this._EnrollCourseData.Value.Split('|');
                int idCourse = Convert.ToInt32(enrollCourseData[0]);
                string processorMethod = enrollCourseData[1].ToString();

                LMS.Library.Course courseObject = new LMS.Library.Course(idCourse);

                // Note: The processorMethod is received from a hidden input where it was calculated when creating
                // the button for "enroll action" in the object's details pane. Due to the fact that one could manipulate
                // that input client-side, we need to do a sanity check to ensure that the requested action is appropriate.

                // "Enroll" should only be allowed from here if:
                //      - Ecommerce is not set and verified.
                //      - Ecommerce is set and verified AND the object is free (cost = 0).
                // "PayPal" should only be allowed from here if:
                //      - Ecommerce is set and verified, the processor is PayPal, and the object has a cost.
                // "AddToCart" should only be allowed from here if:
                //      - Ecommerce is set and verified, the processor is something other than "None" or "PayPal", and the object has a cost.

                if (processorMethod == "enroll" && ((!ecommerceSettings.IsEcommerceSetAndVerified) || (ecommerceSettings.IsEcommerceSetAndVerified && (courseObject.Cost == 0 || courseObject.Cost == null))))
                {
                    // Check if self-enrollment approval is required for the course
                    if ((bool)courseObject.IsSelfEnrollmentApprovalRequired)
                    {
                        // submit an enrollment request
                        EnrollmentRequest enrollmentRequestObject = new EnrollmentRequest();
                        int enrollmentId = enrollmentRequestObject.Submit(courseObject.Id, AsentiaSessionState.IdSiteUser);

                        // display feedback
                        this._EnrollCourseModalDetailsPanel.Controls.Clear();
                        this._EnrollCourseModal.DisplayFeedback(String.Format(_GlobalResources.YourEnrollmentRequestHasBeenSubmittedSuccessfullyClickHereToGoToYourDashboardToViewYourCourseEnrollmentsOnceYourEnrollmentHasBeeApprovedYouWillBeAbleToLaunchItFromThere, "<a href=\"/dashboard\">", "</a>"), false);
                    }
                    else
                    {
                        // enroll the user in the course
                        LMS.Library.Enrollment enrollmentObject = new LMS.Library.Enrollment();
                        enrollmentObject.EnrollCourse(idCourse, AsentiaSessionState.IdSiteUser, 0);

                        // display feedback
                        this._EnrollCourseModalDetailsPanel.Controls.Clear();
                        this._EnrollCourseModal.DisplayFeedback(String.Format(_GlobalResources.YouHaveBeenEnrolledInThisCourseSuccessfullyClickHereToGoToYourCourseEnrollment, "<a href=\"/dashboard/Enrollment.aspx?idEnrollment=" + enrollmentObject.Id.ToString() + "\">", "</a>"), false);
                    }                    
                }
                else if (processorMethod == "paypal" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL && courseObject.Cost > 0)
                {
                    // add the item to the cart
                    int idTransactionItem = this._AddItemToCart(courseObject.Id, PurchaseItemType.Course, courseObject.Title, courseObject.ShortDescription, (double)courseObject.Cost, true);

                    // set-up next steps for paypal processing
                    this._ApplyCouponPayPalBuyNowData.Value = idTransactionItem.ToString();
                    this._BuildCouponCodeFormForPayPalBuyNow(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
                    this._EnrollCourseModal.HideModal();
                    this._ApplyCouponPayPalBuyNowModal.SubmitButton.Visible = true;
                    this._ApplyCouponPayPalBuyNowModal.CloseButton.Visible = true;
                    this._ApplyCouponPayPalBuyNowModal.ShowModal();
                }
                else if (processorMethod == "addtocart" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_NONE && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_PAYPAL && courseObject.Cost > 0)
                {
                    // add the item to the cart
                    this._AddItemToCart(courseObject.Id, PurchaseItemType.Course, courseObject.Title, courseObject.ShortDescription, (double)courseObject.Cost, false);                                        

                    // display feedback
                    this._EnrollCourseModalDetailsPanel.Controls.Clear();
                    this._EnrollCourseModal.DisplayFeedback(_GlobalResources.CourseAddedToCartSuccessfully, false);
                }
                else // invalid processor method, throw exception
                { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingThisEnrollmentPleaseContactAnAdministrator); }

                // register a script to reload the view
                StringBuilder reloadView = new StringBuilder();
                reloadView.AppendLine("ShowCourseDetails(" + idCourse + ", \"jump\", false);");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadCourseViewAfterEnrollment", reloadView.ToString(), true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._EnrollCourseModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._EnrollCourseModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._EnrollCourseModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._EnrollCourseModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._EnrollCourseModal.DisplayFeedback(ex.Message, true);
            }
            catch (Exception exc)
            {
                // display the failure message
                this._EnrollCourseModal.DisplayFeedback(exc.Message + "<br /><br />" + exc.StackTrace, true);
            }
        }
        #endregion

        #region _EnrollLearningPath_Command
        /// <summary>
        /// Handles the event initiated by clicking the enroll button for a learning path.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _EnrollLearningPath_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // declare an ecommerce settings object
                EcommerceSettings ecommerceSettings = new EcommerceSettings();

                // hide the buttons
                this._EnrollLearningPathModal.SubmitButton.Visible = false;
                this._EnrollLearningPathModal.CloseButton.Visible = false;

                // get the learning path and the process method ("enroll", "paypal", or "addtocart")
                string[] enrollLearningPathData = this._EnrollLearningPathData.Value.Split('|');
                int idLearningPath = Convert.ToInt32(enrollLearningPathData[0]);
                string processorMethod = enrollLearningPathData[1].ToString();

                LMS.Library.LearningPath learningPathObject = new LMS.Library.LearningPath(idLearningPath);

                // Note: The processorMethod is received from a hidden input where it was calculated when creating
                // the button for "enroll action" in the object's details pane. Due to the fact that one could manipulate
                // that input client-side, we need to do a sanity check to ensure that the requested action is appropriate.

                // "Enroll" should only be allowed from here if:
                //      - Ecommerce is not set and verified.
                //      - Ecommerce is set and verified AND the object is free (cost = 0).
                // "PayPal" should only be allowed from here if:
                //      - Ecommerce is set and verified, the processor is PayPal, and the object has a cost.
                // "AddToCart" should only be allowed from here if:
                //      - Ecommerce is set and verified, the processor is something other than "None" or "PayPal", and the object has a cost.

                if (processorMethod == "enroll" && ((!ecommerceSettings.IsEcommerceSetAndVerified) || (ecommerceSettings.IsEcommerceSetAndVerified && (learningPathObject.Cost == 0 || learningPathObject.Cost == null))))
                {
                    // enroll the user in the learning path
                    LMS.Library.LearningPathEnrollment enrollmentObject = new LMS.Library.LearningPathEnrollment();
                    enrollmentObject.EnrollLearningPath(idLearningPath, AsentiaSessionState.IdSiteUser, 0);

                    // display feedback
                    this._EnrollLearningPathModalDetailsPanel.Controls.Clear();
                    this._EnrollLearningPathModal.DisplayFeedback(String.Format(_GlobalResources.YouHaveBeenEnrolledInThisLearningPathSuccessfullyClickHereToGoToYourLearningPathEnrollment, "<a href=\"/dashboard/LearningPathEnrollment.aspx?id=" + enrollmentObject.Id + "\">", "</a>"), false);
                }
                else if (processorMethod == "paypal" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL && learningPathObject.Cost > 0)
                {
                    // add the item to the cart
                    int idTransactionItem = this._AddItemToCart(learningPathObject.Id, PurchaseItemType.LearningPath, learningPathObject.Name, learningPathObject.ShortDescription, (double)learningPathObject.Cost, true);

                    // set-up next steps for paypal processing
                    this._ApplyCouponPayPalBuyNowData.Value = idTransactionItem.ToString();
                    this._BuildCouponCodeFormForPayPalBuyNow(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
                    this._EnrollLearningPathModal.HideModal();
                    this._ApplyCouponPayPalBuyNowModal.SubmitButton.Visible = true;
                    this._ApplyCouponPayPalBuyNowModal.CloseButton.Visible = true;
                    this._ApplyCouponPayPalBuyNowModal.ShowModal();
                }
                else if (processorMethod == "addtocart" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_NONE && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_PAYPAL && learningPathObject.Cost > 0)
                {
                    // add the item to the cart
                    this._AddItemToCart(learningPathObject.Id, PurchaseItemType.LearningPath, learningPathObject.Name, learningPathObject.ShortDescription, (double)learningPathObject.Cost, false);

                    // display feedback
                    this._EnrollLearningPathModalDetailsPanel.Controls.Clear();
                    this._EnrollLearningPathModal.DisplayFeedback(_GlobalResources.LearningPathAddedToCartSuccessfully, false);
                }
                else // invalid processor method, throw exception
                { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingThisEnrollmentPleaseContactAnAdministrator); }

                // register a script to reload the view
                StringBuilder reloadView = new StringBuilder();
                reloadView.AppendLine("ShowLearningPathDetails(" + idLearningPath + ", \"jump\", false);");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadLearningPathViewAfterEnrollment", reloadView.ToString(), true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._EnrollLearningPathModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._EnrollLearningPathModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._EnrollLearningPathModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._EnrollLearningPathModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._EnrollLearningPathModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _LoadEnrollInInstructorLedTrainingModalContent
        /// <summary>
        /// Loads content for enroll in instructor led training modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadEnrollInInstructorLedTrainingModalContent(object sender, EventArgs e)
        {
            try
            {                
                // clear the modal
                this._EnrollInInstructorLedTrainingModal.ClearFeedback();

                // make the submit and cancel buttons hidden by default
                this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;

                // get the instructor led training and the process method ("enroll", "paypal", or "addtocart")
                string[] enrollILTData = this._EnrollInInstructorLedTrainingData.Value.Split('|');
                int idInstructorLedTraining = Convert.ToInt32(enrollILTData[0]);
                string processorMethod = enrollILTData[1].ToString();

                // get the available instances
                StandupTraining instructorLedTrainingObject = new StandupTraining(idInstructorLedTraining);

                // get the enrollable instances and put them in a radio button list
                DataTable enrollableInstances = instructorLedTrainingObject.GetEnrollableInstances();

                if (enrollableInstances.Rows.Count > 0)
                {
                    // add instructions to modal
                    Panel selectSessionInstructionsContainer = new Panel();
                    selectSessionInstructionsContainer.ID = "SelectILTSessionInstructionsContainer";                    
                    this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Add(selectSessionInstructionsContainer);

                    // create a panel to house the available sessions
                    Panel availableInstructorLedTrainingSessionsListingPanel = new Panel();
                    availableInstructorLedTrainingSessionsListingPanel.ID = "AvailableInstructorLedTrainingSessionsListingPanel";

                    // loop through available sessions creating a list item for each and listing all meeting times for each
                    int idInstructorLedTrainingInstance = 0;
                    int meetingTimesCount = 0;
                    foreach (DataRow row in enrollableInstances.Rows)
                    {
                        if (idInstructorLedTrainingInstance != Convert.ToInt32(row["idStandupTrainingInstance"]))
                        {
                            meetingTimesCount = 0;

                            // instance id
                            idInstructorLedTrainingInstance = Convert.ToInt32(row["idStandupTrainingInstance"]);

                            // radio button selector
                            Panel instructorLedTrainingInstanceSelectContainer = new Panel();
                            instructorLedTrainingInstanceSelectContainer.ID = "EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceSelectContainer_" + idInstructorLedTrainingInstance.ToString();
                            instructorLedTrainingInstanceSelectContainer.CssClass = "SelectILTSessionTitleContainer";

                            RadioButton instructorLedTrainingInstanceSelect = new RadioButton();
                            instructorLedTrainingInstanceSelect.ID = "EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceSelect_" + idInstructorLedTrainingInstance.ToString();
                            instructorLedTrainingInstanceSelect.GroupName = "EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceSelect";
                            instructorLedTrainingInstanceSelect.Text = row["title"].ToString();

                            instructorLedTrainingInstanceSelectContainer.Controls.Add(instructorLedTrainingInstanceSelect);
                            availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceSelectContainer);

                            // status label (seat(s), waiting seat(s))
                            Panel instructorLedTrainingInstanceStatusLabelContainer = new Panel();
                            instructorLedTrainingInstanceStatusLabelContainer.ID = "EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceStatusLabelContainer_" + idInstructorLedTrainingInstance.ToString();
                            instructorLedTrainingInstanceStatusLabelContainer.CssClass = "SelectILTSessionPropertyContainer";

                            Literal instructorLedTrainingInstanceStatusLabel = new Literal();
                            instructorLedTrainingInstanceStatusLabel.Text = _GlobalResources.Status + ": ";

                            Label instructorLedTrainingInstanceSeats = new Label();
                            instructorLedTrainingInstanceSeats.CssClass = "SelectILTSessionAvailableSeatsLabel";

                            int availableSeats = Convert.ToInt32(row["availableSeats"]);
                            int seats = Convert.ToInt32(row["seats"]);
                            int availableWaitingSeats = Convert.ToInt32(row["availableWaitingSeats"]);
                            int waitingSeats = Convert.ToInt32(row["waitingSeats"]);

                            if (availableSeats > 0)
                            {
                                instructorLedTrainingInstanceSeats.Text = _GlobalResources.Seat_sAvailable + " (" + availableSeats + "/" + seats + ")";
                                instructorLedTrainingInstanceSelect.InputAttributes.Add("waitinglist", "false");
                            }
                            else
                            {
                                instructorLedTrainingInstanceSeats.CssClass = "SelectILTSessionAvailableSeatsLabelWaitingList";
                                instructorLedTrainingInstanceSeats.Text = _GlobalResources.WaitingListAvailable + " (" + availableWaitingSeats + "/" + waitingSeats + ")";
                                instructorLedTrainingInstanceSelect.InputAttributes.Add("waitinglist", "true");
                            }

                            instructorLedTrainingInstanceStatusLabelContainer.Controls.Add(instructorLedTrainingInstanceStatusLabel);
                            instructorLedTrainingInstanceStatusLabelContainer.Controls.Add(instructorLedTrainingInstanceSeats);
                            availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceStatusLabelContainer);

                            // location label
                            Panel instructorLedTrainingInstanceLocationLabelContainer = new Panel();
                            instructorLedTrainingInstanceLocationLabelContainer.ID = "EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceLocationLabelContainer_" + idInstructorLedTrainingInstance.ToString();
                            instructorLedTrainingInstanceLocationLabelContainer.CssClass = "SelectILTSessionPropertyContainer";

                            Literal instructorLedTrainingInstanceLocationLabel = new Literal();
                            instructorLedTrainingInstanceLocationLabel.Text = _GlobalResources.Location + ": ";

                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                if (!String.IsNullOrWhiteSpace(row["city"].ToString()) || !String.IsNullOrWhiteSpace(row["province"].ToString()))
                                {
                                    if (!String.IsNullOrWhiteSpace(row["city"].ToString()))
                                    { instructorLedTrainingInstanceLocationLabel.Text += row["city"].ToString(); }

                                    if (!String.IsNullOrWhiteSpace(row["province"].ToString()))
                                    { instructorLedTrainingInstanceLocationLabel.Text += ", " + row["province"].ToString(); }
                                }
                            }
                            else if (Convert.ToInt32(row["type"]) == 2)
                            { instructorLedTrainingInstanceLocationLabel.Text += _GlobalResources.GoToMeeting; }
                            else if (Convert.ToInt32(row["type"]) == 3)
                            { instructorLedTrainingInstanceLocationLabel.Text += _GlobalResources.GoToWebinar; }
                            else if (Convert.ToInt32(row["type"]) == 4)
                            { instructorLedTrainingInstanceLocationLabel.Text += _GlobalResources.GoToTraining; }
                            else if (Convert.ToInt32(row["type"]) == 5)
                            { instructorLedTrainingInstanceLocationLabel.Text += _GlobalResources.WebEx; }
                            else
                            { instructorLedTrainingInstanceLocationLabel.Text += _GlobalResources.Online; }

                            instructorLedTrainingInstanceLocationLabelContainer.Controls.Add(instructorLedTrainingInstanceLocationLabel);
                            availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceLocationLabelContainer);

                            // instructors
                            if (!String.IsNullOrWhiteSpace(row["instructorDisplayNames"].ToString()))
                            {
                                // instructors label
                                Panel instructorLedTrainingInstanceInstructorsLabelContainer = new Panel();
                                instructorLedTrainingInstanceInstructorsLabelContainer.ID = "EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceInstructorsLabelContainer_" + idInstructorLedTrainingInstance.ToString();
                                instructorLedTrainingInstanceInstructorsLabelContainer.CssClass = "SelectILTSessionPropertyContainer";
                                availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceInstructorsLabelContainer);

                                Label instructorLedTrainingInstanceInstructorsLabel = new Label();
                                instructorLedTrainingInstanceInstructorsLabel.Text = _GlobalResources.Instructor_s + ": ";
                                instructorLedTrainingInstanceInstructorsLabelContainer.Controls.Add(instructorLedTrainingInstanceInstructorsLabel);

                                // instructors
                                string[] instructors = row["instructorDisplayNames"].ToString().Split('|');

                                for (int i = 0; i < instructors.Length; i++)
                                {
                                    Panel instructorLedTrainingInstanceInstructorContainer = new Panel();
                                    instructorLedTrainingInstanceInstructorContainer.ID = "EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceInstructorContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + i.ToString();
                                    instructorLedTrainingInstanceInstructorContainer.CssClass = "SelectILTSessionInstructorContainer";

                                    Literal instructorLedTrainingInstanceInstructor = new Literal();
                                    instructorLedTrainingInstanceInstructor.Text = instructors[i];

                                    instructorLedTrainingInstanceInstructorContainer.Controls.Add(instructorLedTrainingInstanceInstructor);
                                    availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceInstructorContainer);
                                }
                            }

                            // meeting times label
                            Panel instructorLedTrainingInstanceMeetingTimesLabelContainer = new Panel();
                            instructorLedTrainingInstanceMeetingTimesLabelContainer.ID = "EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceMeetingTimesLabelContainer_" + idInstructorLedTrainingInstance.ToString();
                            instructorLedTrainingInstanceMeetingTimesLabelContainer.CssClass = "SelectILTSessionPropertyContainer";

                            Literal instructorLedTrainingInstanceMeetingTimesLabel = new Literal();
                            instructorLedTrainingInstanceMeetingTimesLabel.Text = _GlobalResources.MeetingTime_s + ":";

                            instructorLedTrainingInstanceMeetingTimesLabelContainer.Controls.Add(instructorLedTrainingInstanceMeetingTimesLabel);
                            availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceMeetingTimesLabelContainer);

                            // meeting time
                            Panel instructorLedTrainingInstanceMeetingTimeContainer = new Panel();
                            instructorLedTrainingInstanceMeetingTimeContainer.ID = "EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceMeetingTimeContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                            instructorLedTrainingInstanceMeetingTimeContainer.CssClass = "SelectILTSessionMeetingTimeContainer";

                            DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                            DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                            // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                int idTimezone = Convert.ToInt32(row["idTimezone"]);
                                string tzDotNetName = new Timezone(idTimezone).dotNetName;

                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            }
                            else
                            {
                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            }

                            Literal instructorLedTrainingInstanceMeetingTime = new Literal();
                            instructorLedTrainingInstanceMeetingTime.Text = dtStart.ToString() + " - " + dtEnd.ToString();

                            instructorLedTrainingInstanceMeetingTimeContainer.Controls.Add(instructorLedTrainingInstanceMeetingTime);
                            availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceMeetingTimeContainer);

                        }
                        else // additional meeting times
                        {
                            // meeting time
                            Panel instructorLedTrainingInstanceMeetingTimeContainer = new Panel();
                            instructorLedTrainingInstanceMeetingTimeContainer.ID = "EnrollInInstructorLedTrainingModal_InstructorLedTrainingInstanceMeetingTimeContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                            instructorLedTrainingInstanceMeetingTimeContainer.CssClass = "SelectILTSessionMeetingTimeContainer";

                            DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                            DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                            // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                int idTimezone = Convert.ToInt32(row["idTimezone"]);
                                string tzDotNetName = new Timezone(idTimezone).dotNetName;

                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            }
                            else
                            {
                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            }

                            Literal instructorLedTrainingInstanceMeetingTime = new Literal();
                            instructorLedTrainingInstanceMeetingTime.Text = dtStart.ToString() + " - " + dtEnd.ToString();

                            instructorLedTrainingInstanceMeetingTimeContainer.Controls.Add(instructorLedTrainingInstanceMeetingTime);
                            availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceMeetingTimeContainer);
                        }

                        meetingTimesCount++;
                    }

                    this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Add(availableInstructorLedTrainingSessionsListingPanel);

                    // set-up the instructions, and enable the buttons based on processor type
                    // no need to do a sanity check on the processorMethod here, that will be handled in _EnrollInInstructorLedTraining_Command 
                    if (processorMethod == "enroll")
                    {
                        // instructions
                        this.FormatFormInformationPanel(selectSessionInstructionsContainer, _GlobalResources.PleaseSelectASessionToAttendAndClickEnroll, false);

                        // buttons
                        this._EnrollInInstructorLedTrainingModal.SubmitButton.Text = _GlobalResources.Enroll;
                        this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = true;

                        this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = true;
                    }
                    else if (processorMethod == "paypal")
                    {
                        // instructions
                        this.FormatFormInformationPanel(selectSessionInstructionsContainer, _GlobalResources.PleaseSelectASessionToAttendAndClickContinue, false);

                        // buttons
                        this._EnrollInInstructorLedTrainingModal.SubmitButton.Text = _GlobalResources.Continue;
                        this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = true;

                        this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = true;
                    }
                    else if (processorMethod == "addtocart")
                    {
                        // instructions
                        this.FormatFormInformationPanel(selectSessionInstructionsContainer, _GlobalResources.PleaseSelectASessionToAttendAndClickAddToCart, false);

                        // buttons
                        this._EnrollInInstructorLedTrainingModal.SubmitButton.Text = _GlobalResources.AddToCart;
                        this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = true;

                        this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = true;
                    }

                }
                
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._EnrollInInstructorLedTrainingModal.DisplayFeedback(dnfEx.Message, true);
                this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._EnrollInInstructorLedTrainingModal.DisplayFeedback(fnuEx.Message, true);
                this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._EnrollInInstructorLedTrainingModal.DisplayFeedback(cpeEx.Message, true);
                this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._EnrollInInstructorLedTrainingModal.DisplayFeedback(dEx.Message, true);
                this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._EnrollInInstructorLedTrainingModal.DisplayFeedback(ex.Message, true);
                this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _LoadEnrollInCourseWithOneInstructorLedTrainingModalContent
        /// <summary>
        /// Loads content for enroll in single instructor led training modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadEnrollInCourseWithOneInstructorLedTrainingModalContent(object sender, EventArgs e)
        {
            try
            {

                // clear the modal
                this._EnrollInCourseWithOneInstructorLedTrainingModal.ClearFeedback();

                // make the submit and cancel buttons hidden by default
                this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;

                // get the course id, instructor led training and the process method ("enroll", "paypal", or "addtocart")
                string[] enrollILTData = this._EnrollInCourseWithOneInstructorLedTrainingData.Value.Split('|');
                int idCourse = Convert.ToInt32(enrollILTData[0]);
                int idInstructorLedTraining = Convert.ToInt32(enrollILTData[1]);
                string processorMethod = enrollILTData[2].ToString();

                // add instructions to modal
                Panel selectSessionInstructionsContainer = new Panel();
                selectSessionInstructionsContainer.ID = "SelectILTSessionInstructionsContainer";
                this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Add(selectSessionInstructionsContainer);

                bool showILTSessions = false;
                Course courseObject = new Course(idCourse);

                // set-up the instructions, and enable the buttons based on processor type
                // no need to do a sanity check on the processorMethod here, that will be handled in _EnrollInInstructorLedTraining_Command 
                if (processorMethod == "enroll")
                {
                    // Check if self-enrollment approval is required for the course
                    if ((bool)courseObject.IsSelfEnrollmentApprovalRequired)
                    {
                        // submit an enrollment request
                        EnrollmentRequest enrollmentRequestObject = new EnrollmentRequest();
                        int enrollmentId = enrollmentRequestObject.Submit(courseObject.Id, AsentiaSessionState.IdSiteUser);

                        // display feedback
                        this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(String.Format(_GlobalResources.YourEnrollmentRequestHasBeenSubmittedSuccessfullyClickHereToGoToYourDashboardToViewYourCourseEnrollmentsOnceYourEnrollmentHasBeeApprovedYouWillBeAbleToLaunchItFromThere, "<a href=\"/dashboard\">", "</a>"), false);
                    }
                    else
                    {
                        showILTSessions = true;

                        // enroll the user in the course
                        LMS.Library.Enrollment enrollmentObject = new LMS.Library.Enrollment();
                        enrollmentObject.EnrollCourse(idCourse, AsentiaSessionState.IdSiteUser, 0);
                            

                        // display feedback
                        this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.YouHaveBeenEnrolledInThisCourseSuccessfully, false);

                        // instructions
                        this.FormatFormInformationPanel(selectSessionInstructionsContainer, _GlobalResources.PleaseSelectASessionToAttendAndClickEnroll, false);

                        // buttons
                        this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Text = _GlobalResources.Enroll;
                        this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = true;

                        this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = true;
                    }              

                }
                else if (processorMethod == "paypal")
                {
                    showILTSessions = true;

                    // instructions
                    this.FormatFormInformationPanel(selectSessionInstructionsContainer, _GlobalResources.PleaseSelectASessionToAttendAndClickContinue, false);

                    // buttons
                    this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Text = _GlobalResources.Continue;
                    this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = true;

                    this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = true;
                }
                else if (processorMethod == "addtocart")
                {
                    showILTSessions = true;

                    // instructions
                    this.FormatFormInformationPanel(selectSessionInstructionsContainer, _GlobalResources.PleaseSelectASessionToAttendAndClickAddToCart, false);

                    // buttons
                    this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Text = _GlobalResources.AddToCart;
                    this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = true;

                    this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = true;
                }

                // show the ilt session select list
                if (showILTSessions)
                {
                    this._LoadEnrollInInstructorLedTrainingRadioButtons(idInstructorLedTraining);

                }

                // register a script to reload the view
                StringBuilder reloadView = new StringBuilder();
                reloadView.AppendLine("ShowCourseDetails(" + idCourse + ", \"jump\", false);");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadCourseViewAfterEnrollment", reloadView.ToString(), true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(dnfEx.Message, true);
                this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(fnuEx.Message, true);
                this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(cpeEx.Message, true);
                this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(dEx.Message, true);
                this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(ex.Message, true);
                this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _LoadEnrollInInstructorLedTrainingRadioButtons
        /// <summary>
        /// Loads the instructor led training session radio buttons for enrolling in a Course with on ILT.
        /// Needs separate function so that if the user does not choose a session, it can just load the radio buttons
        /// rather than the entire modal, so it doesn't create a duplicate course enrollment
        /// </summary>
        /// <param name="idInstructorLedTraining">instructor led training id</param>
        private void _LoadEnrollInInstructorLedTrainingRadioButtons(int idInstructorLedTraining)
        {
            // get the available instances
            StandupTraining instructorLedTrainingObject = new StandupTraining(idInstructorLedTraining);

            // get the enrollable instances and put them in a radio button list
            DataTable enrollableInstances = instructorLedTrainingObject.GetEnrollableInstances();

            if (enrollableInstances.Rows.Count > 0)
            {

                // create a panel to house the available sessions
                Panel availableInstructorLedTrainingSessionsListingPanel = new Panel();
                availableInstructorLedTrainingSessionsListingPanel.ID = "AvailableInstructorLedTrainingSessionsListingPanel";

                // loop through available sessions creating a list item for each and listing all meeting times for each
                int idInstructorLedTrainingInstance = 0;
                int meetingTimesCount = 0;
                foreach (DataRow row in enrollableInstances.Rows)
                {
                    if (idInstructorLedTrainingInstance != Convert.ToInt32(row["idStandupTrainingInstance"]))
                    {
                        meetingTimesCount = 0;

                        // instance id
                        idInstructorLedTrainingInstance = Convert.ToInt32(row["idStandupTrainingInstance"]);

                        // radio button selector
                        Panel instructorLedTrainingInstanceSelectContainer = new Panel();
                        instructorLedTrainingInstanceSelectContainer.ID = "EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceSelectContainer_" + idInstructorLedTrainingInstance.ToString();
                        instructorLedTrainingInstanceSelectContainer.CssClass = "SelectILTSessionTitleContainer";

                        RadioButton instructorLedTrainingInstanceSelect = new RadioButton();
                        instructorLedTrainingInstanceSelect.ID = "EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceSelect_" + idInstructorLedTrainingInstance.ToString();
                        instructorLedTrainingInstanceSelect.GroupName = "EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceSelect";
                        instructorLedTrainingInstanceSelect.Text = row["title"].ToString();

                        instructorLedTrainingInstanceSelectContainer.Controls.Add(instructorLedTrainingInstanceSelect);
                        availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceSelectContainer);

                        // status label (seat(s), waiting seat(s))
                        Panel instructorLedTrainingInstanceStatusLabelContainer = new Panel();
                        instructorLedTrainingInstanceStatusLabelContainer.ID = "EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceStatusLabelContainer_" + idInstructorLedTrainingInstance.ToString();
                        instructorLedTrainingInstanceStatusLabelContainer.CssClass = "SelectILTSessionPropertyContainer";

                        Literal instructorLedTrainingInstanceStatusLabel = new Literal();
                        instructorLedTrainingInstanceStatusLabel.Text = _GlobalResources.Status + ": ";

                        Label instructorLedTrainingInstanceSeats = new Label();
                        instructorLedTrainingInstanceSeats.CssClass = "SelectILTSessionAvailableSeatsLabel";

                        int availableSeats = Convert.ToInt32(row["availableSeats"]);
                        int seats = Convert.ToInt32(row["seats"]);
                        int availableWaitingSeats = Convert.ToInt32(row["availableWaitingSeats"]);
                        int waitingSeats = Convert.ToInt32(row["waitingSeats"]);

                        if (availableSeats > 0)
                        {
                            instructorLedTrainingInstanceSeats.Text = _GlobalResources.Seat_sAvailable + " (" + availableSeats + "/" + seats + ")";
                            instructorLedTrainingInstanceSelect.InputAttributes.Add("waitinglist", "false");
                        }
                        else
                        {
                            instructorLedTrainingInstanceSeats.CssClass = "SelectILTSessionAvailableSeatsLabelWaitingList";
                            instructorLedTrainingInstanceSeats.Text = _GlobalResources.WaitingListAvailable + " (" + availableWaitingSeats + "/" + waitingSeats + ")";
                            instructorLedTrainingInstanceSelect.InputAttributes.Add("waitinglist", "true");
                        }

                        instructorLedTrainingInstanceStatusLabelContainer.Controls.Add(instructorLedTrainingInstanceStatusLabel);
                        instructorLedTrainingInstanceStatusLabelContainer.Controls.Add(instructorLedTrainingInstanceSeats);
                        availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceStatusLabelContainer);

                        // location label
                        Panel instructorLedTrainingInstanceLocationLabelContainer = new Panel();
                        instructorLedTrainingInstanceLocationLabelContainer.ID = "EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceLocationLabelContainer_" + idInstructorLedTrainingInstance.ToString();
                        instructorLedTrainingInstanceLocationLabelContainer.CssClass = "SelectILTSessionPropertyContainer";

                        Literal instructorLedTrainingInstanceLocationLabel = new Literal();
                        instructorLedTrainingInstanceLocationLabel.Text = _GlobalResources.Location + ": ";

                        if (Convert.ToInt32(row["type"]) == 1)
                        {
                            if (!String.IsNullOrWhiteSpace(row["city"].ToString()) || !String.IsNullOrWhiteSpace(row["province"].ToString()))
                            {
                                if (!String.IsNullOrWhiteSpace(row["city"].ToString()))
                                { instructorLedTrainingInstanceLocationLabel.Text += row["city"].ToString(); }

                                if (!String.IsNullOrWhiteSpace(row["province"].ToString()))
                                { instructorLedTrainingInstanceLocationLabel.Text += ", " + row["province"].ToString(); }
                            }
                        }
                        else if (Convert.ToInt32(row["type"]) == 2)
                        { instructorLedTrainingInstanceLocationLabel.Text += _GlobalResources.GoToMeeting; }
                        else if (Convert.ToInt32(row["type"]) == 3)
                        { instructorLedTrainingInstanceLocationLabel.Text += _GlobalResources.GoToWebinar; }
                        else if (Convert.ToInt32(row["type"]) == 4)
                        { instructorLedTrainingInstanceLocationLabel.Text += _GlobalResources.GoToTraining; }
                        else if (Convert.ToInt32(row["type"]) == 5)
                        { instructorLedTrainingInstanceLocationLabel.Text += _GlobalResources.WebEx; }
                        else
                        { instructorLedTrainingInstanceLocationLabel.Text += _GlobalResources.Online; }

                        instructorLedTrainingInstanceLocationLabelContainer.Controls.Add(instructorLedTrainingInstanceLocationLabel);
                        availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceLocationLabelContainer);

                        // instructors
                        if (!String.IsNullOrWhiteSpace(row["instructorDisplayNames"].ToString()))
                        {
                            // instructors label
                            Panel instructorLedTrainingInstanceInstructorsLabelContainer = new Panel();
                            instructorLedTrainingInstanceInstructorsLabelContainer.ID = "EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceInstructorsLabelContainer_" + idInstructorLedTrainingInstance.ToString();
                            instructorLedTrainingInstanceInstructorsLabelContainer.CssClass = "SelectILTSessionPropertyContainer";
                            availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceInstructorsLabelContainer);

                            Label instructorLedTrainingInstanceInstructorsLabel = new Label();
                            instructorLedTrainingInstanceInstructorsLabel.Text = _GlobalResources.Instructor_s + ": ";
                            instructorLedTrainingInstanceInstructorsLabelContainer.Controls.Add(instructorLedTrainingInstanceInstructorsLabel);

                            // instructors
                            string[] instructors = row["instructorDisplayNames"].ToString().Split('|');

                            for (int i = 0; i < instructors.Length; i++)
                            {
                                Panel instructorLedTrainingInstanceInstructorContainer = new Panel();
                                instructorLedTrainingInstanceInstructorContainer.ID = "EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceInstructorContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + i.ToString();
                                instructorLedTrainingInstanceInstructorContainer.CssClass = "SelectILTSessionInstructorContainer";

                                Literal instructorLedTrainingInstanceInstructor = new Literal();
                                instructorLedTrainingInstanceInstructor.Text = instructors[i];

                                instructorLedTrainingInstanceInstructorContainer.Controls.Add(instructorLedTrainingInstanceInstructor);
                                availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceInstructorContainer);
                            }
                        }

                        // meeting times label
                        Panel instructorLedTrainingInstanceMeetingTimesLabelContainer = new Panel();
                        instructorLedTrainingInstanceMeetingTimesLabelContainer.ID = "EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceMeetingTimesLabelContainer_" + idInstructorLedTrainingInstance.ToString();
                        instructorLedTrainingInstanceMeetingTimesLabelContainer.CssClass = "SelectILTSessionPropertyContainer";

                        Literal instructorLedTrainingInstanceMeetingTimesLabel = new Literal();
                        instructorLedTrainingInstanceMeetingTimesLabel.Text = _GlobalResources.MeetingTime_s + ":";

                        instructorLedTrainingInstanceMeetingTimesLabelContainer.Controls.Add(instructorLedTrainingInstanceMeetingTimesLabel);
                        availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceMeetingTimesLabelContainer);

                        // meeting time
                        Panel instructorLedTrainingInstanceMeetingTimeContainer = new Panel();
                        instructorLedTrainingInstanceMeetingTimeContainer.ID = "EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceMeetingTimeContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                        instructorLedTrainingInstanceMeetingTimeContainer.CssClass = "SelectILTSessionMeetingTimeContainer";

                        DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                        DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                        // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                        if (Convert.ToInt32(row["type"]) == 1)
                        {
                            int idTimezone = Convert.ToInt32(row["idTimezone"]);
                            string tzDotNetName = new Timezone(idTimezone).dotNetName;

                            dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                        }
                        else
                        {
                            dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                        }

                        Literal instructorLedTrainingInstanceMeetingTime = new Literal();
                        instructorLedTrainingInstanceMeetingTime.Text = dtStart.ToString() + " - " + dtEnd.ToString();

                        instructorLedTrainingInstanceMeetingTimeContainer.Controls.Add(instructorLedTrainingInstanceMeetingTime);
                        availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceMeetingTimeContainer);

                    }
                    else // additional meeting times
                    {
                        // meeting time
                        Panel instructorLedTrainingInstanceMeetingTimeContainer = new Panel();
                        instructorLedTrainingInstanceMeetingTimeContainer.ID = "EnrollInCourseWithOneInstructorLedTrainingModal_InstructorLedTrainingInstanceMeetingTimeContainer_" + idInstructorLedTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                        instructorLedTrainingInstanceMeetingTimeContainer.CssClass = "SelectILTSessionMeetingTimeContainer";

                        DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                        DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                        // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                        if (Convert.ToInt32(row["type"]) == 1)
                        {
                            int idTimezone = Convert.ToInt32(row["idTimezone"]);
                            string tzDotNetName = new Timezone(idTimezone).dotNetName;

                            dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                        }
                        else
                        {
                            dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                        }

                        Literal instructorLedTrainingInstanceMeetingTime = new Literal();
                        instructorLedTrainingInstanceMeetingTime.Text = dtStart.ToString() + " - " + dtEnd.ToString();

                        instructorLedTrainingInstanceMeetingTimeContainer.Controls.Add(instructorLedTrainingInstanceMeetingTime);
                        availableInstructorLedTrainingSessionsListingPanel.Controls.Add(instructorLedTrainingInstanceMeetingTimeContainer);
                    }

                    meetingTimesCount++;
                }

                this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Add(availableInstructorLedTrainingSessionsListingPanel);
            }
            else
            {
                Panel selectSessionInstructionsContainer = (Panel)this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.FindControl("SelectILTSessionInstructionsContainer");
                
                selectSessionInstructionsContainer.Visible = false;
                this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;
            }
        }

        #endregion

        #region _EnrollInInstructorLedTraining_Command
        /// <summary>
        /// Handles the event initiated by the submit button in the enroll in standup training modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _EnrollInInstructorLedTraining_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // declare an ecommerce settings object
                EcommerceSettings ecommerceSettings = new EcommerceSettings();

                // get the instructor led training and the process method ("enroll", "paypal", or "addtocart")
                string[] enrollILTData = this._EnrollInInstructorLedTrainingData.Value.Split('|');
                int idInstructorLedTraining = Convert.ToInt32(enrollILTData[0]);
                string processorMethod = enrollILTData[1].ToString();

                // get the selected instructor training instance id and waitinglist bool
                string[] enrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceData = this._EnrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceData.Value.Split('|');
                int idInstructorLedTrainingInstance = Convert.ToInt32(enrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceData[0]);
                bool isWaitingList = Convert.ToBoolean(enrollInInstructorLedTrainingSelectedInstructorLedTrainingInstanceData[1]);

                // if there is no session selected, throw an error letting the user know to select a session
                if (idInstructorLedTrainingInstance == 0)
                {   
                    // reload the form
                    this._LoadEnrollInInstructorLedTrainingModalContent(sender, e);

                    // show the error and return
                    this._EnrollInInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.YouMustSelectASessionToContinue, true);
                    return;
                }

                StandupTraining instructorLedTrainingObject = new StandupTraining(idInstructorLedTraining);
                StandupTrainingInstance instructorLedTrainingInstance = new StandupTrainingInstance(idInstructorLedTrainingInstance);

                // Note: The processorMethod is received from a hidden input where it was calculated when creating
                // the button for "enroll action" in the object's details pane. Due to the fact that one could manipulate
                // that input client-side, we need to do a sanity check to ensure that the requested action is appropriate.

                // "Enroll" should only be allowed from here if:
                //      - Ecommerce is not set and verified.
                //      - Ecommerce is set and verified AND the object is free (cost = 0).
                // "PayPal" should only be allowed from here if:
                //      - Ecommerce is set and verified, the processor is PayPal, and the object has a cost.
                // "AddToCart" should only be allowed from here if:
                //      - Ecommerce is set and verified, the processor is something other than "None" or "PayPal", and the object has a cost.
                
                if (!isWaitingList)
                {
                    if (instructorLedTrainingInstance.SeatsRemaining > 0)
                    {
                        if (processorMethod == "enroll" && ((!ecommerceSettings.IsEcommerceSetAndVerified) || (ecommerceSettings.IsEcommerceSetAndVerified && (instructorLedTrainingObject.Cost == 0 || instructorLedTrainingObject.Cost == null))))
                        {
                            // enroll in instructor led training instance
                            instructorLedTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, false);

                            // display feedback
                            this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                            this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                            this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;
                            this._EnrollInInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.YouHaveBeenJoinedToTheInstructorLedTrainingSessionSuccessfully, false);
                        }
                        else if (processorMethod == "paypal" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL && instructorLedTrainingObject.Cost > 0)
                        {
                            // add the item to the cart
                            int idTransactionItem = this._AddItemToCart(instructorLedTrainingInstance.Id, PurchaseItemType.InstructorLedTraining, instructorLedTrainingObject.Title, instructorLedTrainingObject.Description, (double)instructorLedTrainingObject.Cost, true);

                            // set-up next steps for paypal processing
                            this._ApplyCouponPayPalBuyNowData.Value = idTransactionItem.ToString();
                            this._BuildCouponCodeFormForPayPalBuyNow(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
                            this._EnrollInInstructorLedTrainingModal.HideModal();
                            this._ApplyCouponPayPalBuyNowModal.SubmitButton.Visible = true;
                            this._ApplyCouponPayPalBuyNowModal.CloseButton.Visible = true;
                            this._ApplyCouponPayPalBuyNowModal.ShowModal();
                        }
                        else if (processorMethod == "addtocart" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_NONE && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_PAYPAL && instructorLedTrainingObject.Cost > 0)
                        {
                            // add the item to the cart
                            this._AddItemToCart(instructorLedTrainingInstance.Id, PurchaseItemType.InstructorLedTraining, instructorLedTrainingObject.Title, instructorLedTrainingObject.Description, (double)instructorLedTrainingObject.Cost, false);

                            // display feedback
                            this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                            this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                            this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;
                            this._EnrollInInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.InstructorLedTrainingAddedToCartSuccessfully, false);
                        }
                        else // invalid processor method, throw exception
                        { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingThisEnrollmentPleaseContactAnAdministrator); }
                    }
                    else
                    {
                        // clear the enroll in instructor led training panel and rebuild it
                        this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                        this._LoadEnrollInInstructorLedTrainingModalContent(sender, e);

                        // display error
                        this._EnrollInInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.ThereAreNoSeatsAvailableInTheSelectedInstructorLedTrainingSessionPleaseSelectAnotherSession, true);
                    }
                }
                else
                {
                    if (instructorLedTrainingInstance.WaitingSeatsRemaining > 0)
                    {
                        if (processorMethod == "enroll" && ((!ecommerceSettings.IsEcommerceSetAndVerified) || (ecommerceSettings.IsEcommerceSetAndVerified && (instructorLedTrainingObject.Cost == 0 || instructorLedTrainingObject.Cost == null))))
                        {
                            // enroll in instructor led training instance waiting list
                            instructorLedTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, true);

                            // display feedback
                            this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                            this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                            this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;
                            this._EnrollInInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.YouHaveBeenJoinedToTheInstructorLedTrainingSession_sWaitingListSuccessfully, false);
                        }
                        else if (processorMethod == "paypal" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL && instructorLedTrainingObject.Cost > 0)
                        {
                            // add the item to the cart
                            this._AddItemToCart(instructorLedTrainingInstance.Id, PurchaseItemType.InstructorLedTraining, instructorLedTrainingObject.Title, instructorLedTrainingObject.Description, (double)instructorLedTrainingObject.Cost, false);

                            // display feedback
                            this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                            this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                            this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;
                            this._EnrollInInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.InstructorLedTrainingAddedToCartSuccessfully, false);
                        }
                        else if (processorMethod == "addtocart" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_NONE && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_PAYPAL && instructorLedTrainingObject.Cost > 0)
                        {
                            // add the item to the cart
                            this._AddItemToCart(instructorLedTrainingInstance.Id, PurchaseItemType.InstructorLedTraining, instructorLedTrainingObject.Title, instructorLedTrainingObject.Description, (double)instructorLedTrainingObject.Cost, false);

                            // display feedback
                            this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                            this._EnrollInInstructorLedTrainingModal.SubmitButton.Visible = false;
                            this._EnrollInInstructorLedTrainingModal.CloseButton.Visible = false;
                            this._EnrollInInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.InstructorLedTrainingAddedToCartSuccessfully, false);
                        }
                        else // invalid processor method, throw exception
                        { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingThisEnrollmentPleaseContactAnAdministrator); }
                    }
                    else
                    {
                        // clear the enroll in instructor led training panel and rebuild it
                        this._EnrollInInstructorLedTrainingDetailsPanel.Controls.Clear();
                        this._LoadEnrollInInstructorLedTrainingModalContent(sender, e);

                        // display error
                        this._EnrollInInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.ThereAreNoWaitingListSeatsAvailableInTheSelectedInstructorLedTrainingSessionPleaseSelectAnotherSession, true);
                    }
                }

                // register a script to reload the view
                StringBuilder reloadView = new StringBuilder();
                reloadView.AppendLine("ShowInstructorLedTrainingDetails(" + idInstructorLedTraining + ", \"jump\", false);");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadILTViewAfterEnrollment", reloadView.ToString(), true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._EnrollInInstructorLedTrainingModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._EnrollInInstructorLedTrainingModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._EnrollInInstructorLedTrainingModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._EnrollInInstructorLedTrainingModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._EnrollInInstructorLedTrainingModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _EnrollInCourseWithOneInstructorLedTraining_Command
        /// <summary>
        /// Handles the event initiated by the submit button in the enroll in single standup training for course modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _EnrollInCourseWithOneInstructorLedTraining_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // declare an ecommerce settings object
                EcommerceSettings ecommerceSettings = new EcommerceSettings();

                // get the instructor led training and the process method ("enroll", "paypal", or "addtocart")
                string[] enrollILTData = this._EnrollInCourseWithOneInstructorLedTrainingData.Value.Split('|');
                int idCourse = Convert.ToInt32(enrollILTData[0]);
                int idInstructorLedTraining = Convert.ToInt32(enrollILTData[1]);
                string processorMethod = enrollILTData[2].ToString();

                // get the selected instructor training instance id and waitinglist bool
                string[] EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceData = this._EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceData.Value.Split('|');
                int idInstructorLedTrainingInstance = Convert.ToInt32(EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceData[0]);
                bool isWaitingList = Convert.ToBoolean(EnrollInCourseWithOneInstructorLedTrainingSelectedInstructorLedTrainingInstanceData[1]);

                // if there is no session selected, throw an error letting the user know to select a session
                if (idInstructorLedTrainingInstance == 0)
                {
                    // clear the enroll in instructor led training panel and rebuild it
                    this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                    this._LoadEnrollInInstructorLedTrainingRadioButtons(idInstructorLedTraining);

                    // show the error and return
                    this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.YouMustSelectASessionToContinue, true);
                    return;
                }

                // get the course
                Course courseObject = new Course(idCourse);

                // get the ILT and session
                StandupTraining instructorLedTrainingObject = new StandupTraining(idInstructorLedTraining);
                StandupTrainingInstance instructorLedTrainingInstance = new StandupTrainingInstance(idInstructorLedTrainingInstance);

                // Note: The processorMethod is received from a hidden input where it was calculated when creating
                // the button for "enroll action" in the object's details pane. Due to the fact that one could manipulate
                // that input client-side, we need to do a sanity check to ensure that the requested action is appropriate.

                // "Enroll" should only be allowed from here if:
                //      - Ecommerce is not set and verified.
                //      - Ecommerce is set and verified AND the object is free (cost = 0).
                // "PayPal" should only be allowed from here if:
                //      - Ecommerce is set and verified, the processor is PayPal, and the object has a cost.
                // "AddToCart" should only be allowed from here if:
                //      - Ecommerce is set and verified, the processor is something other than "None" or "PayPal", and the object has a cost

                if (!isWaitingList)
                {
                    if (instructorLedTrainingInstance.SeatsRemaining > 0)
                    {
                        if (processorMethod == "enroll" && ((!ecommerceSettings.IsEcommerceSetAndVerified) || (ecommerceSettings.IsEcommerceSetAndVerified && (courseObject.Cost == 0 || courseObject.Cost == null))))
                        {
                            // enroll in instructor led training instance
                            instructorLedTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, false);

                            // display feedback
                            this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.YouHaveBeenJoinedToTheInstructorLedTrainingSessionSuccessfully, false);
                        }
                        else if (processorMethod == "paypal" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL && courseObject.Cost > 0)
                        {
                            // add the item to the cart
                            int idTransactionItem = this._AddItemToCart(idCourse, PurchaseItemType.Course, courseObject.Title, courseObject.ShortDescription, (double)courseObject.Cost, true, instructorLedTrainingInstance.Id);

                            // set-up next steps for paypal processing
                            this._ApplyCouponPayPalBuyNowData.Value = idTransactionItem.ToString();
                            this._BuildCouponCodeFormForPayPalBuyNow(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.HideModal();
                            this._ApplyCouponPayPalBuyNowModal.SubmitButton.Visible = true;
                            this._ApplyCouponPayPalBuyNowModal.CloseButton.Visible = true;
                            this._ApplyCouponPayPalBuyNowModal.ShowModal();
                        }
                        else if (processorMethod == "addtocart" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_NONE && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_PAYPAL && courseObject.Cost > 0)
                        {
                            // add the item to the cart
                            this._AddItemToCart(idCourse, PurchaseItemType.Course, courseObject.Title, courseObject.ShortDescription, (double)courseObject.Cost, false, instructorLedTrainingInstance.Id);

                            // display feedback
                            this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.CourseAddedToCartSuccessfully, false);
                        }
                        else // invalid processor method, throw exception
                        { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingThisEnrollmentPleaseContactAnAdministrator); }
                    }
                    else
                    {
                        // clear the enroll in instructor led training panel and rebuild it
                        this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                        this._LoadEnrollInCourseWithOneInstructorLedTrainingModalContent(sender, e);

                        // display error
                        this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.ThereAreNoSeatsAvailableInTheSelectedInstructorLedTrainingSessionPleaseSelectAnotherSession, true);
                    }
                }
                else
                {
                    if (instructorLedTrainingInstance.WaitingSeatsRemaining > 0)
                    {
                        if (processorMethod == "enroll" && ((!ecommerceSettings.IsEcommerceSetAndVerified) || (ecommerceSettings.IsEcommerceSetAndVerified && (courseObject.Cost == 0 || courseObject.Cost == null))))
                        {
                            // enroll in instructor led training instance waiting list
                            instructorLedTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, true);

                            // display feedback
                            this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.YouHaveBeenJoinedToTheInstructorLedTrainingSession_sWaitingListSuccessfully, false);
                        }
                        else if (processorMethod == "paypal" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL && courseObject.Cost > 0)
                        {
                            // add the item to the cart
                            int idTransactionItem = this._AddItemToCart(idCourse, PurchaseItemType.Course, courseObject.Title, courseObject.ShortDescription, (double)courseObject.Cost, true, instructorLedTrainingInstance.Id);

                            // set-up next steps for paypal processing
                            this._ApplyCouponPayPalBuyNowData.Value = idTransactionItem.ToString();
                            this._BuildCouponCodeFormForPayPalBuyNow(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.HideModal();
                            this._ApplyCouponPayPalBuyNowModal.SubmitButton.Visible = true;
                            this._ApplyCouponPayPalBuyNowModal.CloseButton.Visible = true;
                            this._ApplyCouponPayPalBuyNowModal.ShowModal();
                        }
                        else if (processorMethod == "addtocart" && ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_NONE && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_PAYPAL && courseObject.Cost > 0)
                        {                            
                            // add the item to the cart
                            this._AddItemToCart(idCourse, PurchaseItemType.Course, courseObject.Title, courseObject.ShortDescription, (double)courseObject.Cost, false, instructorLedTrainingInstance.Id);

                            // display feedback
                            this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.SubmitButton.Visible = false;
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.CloseButton.Visible = false;
                            this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.CourseAddedToCartSuccessfully, false);
                        }
                        else // invalid processor method, throw exception
                        { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingThisEnrollmentPleaseContactAnAdministrator); }
                    }
                    else
                    {
                        // clear the enroll in instructor led training panel and rebuild it
                        this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                        this._LoadEnrollInCourseWithOneInstructorLedTrainingModalContent(sender, e);

                        // display error
                        this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(_GlobalResources.ThereAreNoWaitingListSeatsAvailableInTheSelectedInstructorLedTrainingSessionPleaseSelectAnotherSession, true);
                    }
                }

                StringBuilder reloadView = new StringBuilder();

                // if this is a free course, reload view to show instructor led training
                if (processorMethod == "enroll")
                {
                    // register a script to reload the view
                    
                    reloadView.AppendLine("ShowInstructorLedTrainingDetails(" + idInstructorLedTraining + ", \"jump\", false);");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadILTViewAfterEnrollment", reloadView.ToString(), true);
                }
                else // if this is a paid course, reload the view to just show course details
                {
                    // register a script to reload the view
                    reloadView.AppendLine("ShowCourseDetails(" + idCourse + ", \"jump\", false);");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadILTViewAfterEnrollment", reloadView.ToString(), true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._EnrollInCourseWithOneInstructorLedTrainingDetailsPanel.Controls.Clear();
                this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._EnrollInCourseWithOneInstructorLedTrainingModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _JoinUnjoinCommunity_Command
        /// <summary>
        /// Handles the event initiated by clicking the join/unjoin button for a community.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _JoinUnjoinCommunity_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // hide the buttons
                this._JoinUnjoinCommunityModal.SubmitButton.Visible = false;
                this._JoinUnjoinCommunityModal.CloseButton.Visible = false;

                // get the group and whether or not to join or unjoin
                string[] joinUnjoinCommunityData = this._JoinUnjoinCommunityData.Value.Split('|');
                int idGroup = Convert.ToInt32(joinUnjoinCommunityData[0]);
                bool isJoin = Convert.ToBoolean(joinUnjoinCommunityData[1]);

                Group groupObject = new Group(idGroup);

                // add current session user to data table
                DataTable usersToJoinUnjoin = new DataTable();
                usersToJoinUnjoin.Columns.Add("id", typeof(int));
                usersToJoinUnjoin.Rows.Add(AsentiaSessionState.IdSiteUser);

                if (isJoin)
                {
                    // join the user to the group
                    groupObject.JoinUsers(usersToJoinUnjoin);

                    // display feedback
                    this._JoinUnjoinCommunityModalDetailsPanel.Controls.Clear();
                    this._JoinUnjoinCommunityModal.DisplayFeedback(_GlobalResources.YouHaveJoinedTheCommunitySuccessfully, false);
                }
                else // unjoin
                {
                    // unjoin the user from the group
                    groupObject.RemoveUsers(usersToJoinUnjoin);

                    // display feedback
                    this._JoinUnjoinCommunityModalDetailsPanel.Controls.Clear();
                    this._JoinUnjoinCommunityModal.DisplayFeedback(_GlobalResources.YouHaveUnjoinedTheCommunitySuccessfully, false);
                }

                // register a script to reload the view
                StringBuilder reloadView = new StringBuilder();
                reloadView.AppendLine("ShowCommunityDetails(" + idGroup + ", \"jump\", false);");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadCommunityViewAfterJoinUnjoin", reloadView.ToString(), true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._JoinUnjoinCommunityModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._JoinUnjoinCommunityModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._JoinUnjoinCommunityModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._JoinUnjoinCommunityModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._JoinUnjoinCommunityModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _IsItemInCart
        /// <summary>
        /// Determines if an item is in the user's active cart.
        /// </summary>
        /// <param name="itemId">the id of the item</param>
        /// <param name="itemType">the type of the item</param>
        /// <returns>true/false</returns>
        private static bool _IsItemInCart(int itemId, PurchaseItemType itemType)
        {            
            // get the active cart for the user
            Purchase purchaseObject = new Purchase();
            purchaseObject.GetActiveCartForUser(AsentiaSessionState.IdSiteUser, false);

            if (purchaseObject.Id == 0) // no active cart, return false
            { return false; }

            // get the cart items
            DataTable cartItems = TransactionItem.GetItemsForPurchase(purchaseObject.Id);

            if (cartItems.Rows.Count == 0) // no items in cart, return false
            { return false; }

            // if the item type is ilt, we need to grab the sessions and match to them, because that is what is actually in the cart
            if (itemType == PurchaseItemType.InstructorLedTraining)
            {
                // get instructor led training information
                StandupTraining instructorLedTrainingObject = new StandupTraining(itemId);
                DataTable enrollableInstances = instructorLedTrainingObject.GetEnrollableInstances(); // enrollable instances are what we check against, as other logic should prevent any unenrollable instances from being in cart

                // loop through each enrollable instance looking for a match
                foreach (DataRow row in enrollableInstances.Rows)
                {
                    int idStandupTrainingInstance = Convert.ToInt32(row["idStandupTrainingInstance"]);
                    
                    // select matching rows from cartItems
                    DataRow[] foundRows = cartItems.Select("itemId= " + idStandupTrainingInstance.ToString() + " AND itemType= " + Convert.ToInt32(itemType).ToString());

                    // if found, return true
                    if (foundRows.Length > 0)
                    { return true; }
                }
                
            }
            else // all other item types are 1 to 1 match for id
            {
                // select matching rows from cartItems
                DataRow[] foundRows = cartItems.Select("itemId= " + itemId.ToString() + " AND itemType= " + Convert.ToInt32(itemType).ToString());

                // if found, return true
                if (foundRows.Length > 0)
                { return true; }
            }

            // if we got this far, it means the item is not in the cart, return false
            return false;
        }
        #endregion

        #region _AddItemToCart
        /// <summary>
        /// Adds item to user's active cart.
        /// </summary>
        /// <param name="itemId">the id of the item</param>
        /// <param name="itemType">the type of the item</param>
        /// <param name="itemName">the name of the item</param>
        /// <param name="itemDescription">the description of the item</param>
        /// <param name="itemCost">the cost of the item</param>
        /// <param name="idIltSession">ILT Session ID for a course with a single ILT</param>
        private int _AddItemToCart(int itemId, PurchaseItemType itemType, string itemName, string itemDescription, double itemCost, bool clearCartItems, int? idIltSession = null)
        {
            try
            {
                // get the active cart for the user, if there is no active cart, one will be created automatically
                Purchase purchaseObject = new Purchase();
                purchaseObject.GetActiveCartForUser(AsentiaSessionState.IdSiteUser, true);

                // clear the cart items, if necessary
                if (clearCartItems)
                { TransactionItem.ClearAllUnconfirmedForPurchase(purchaseObject.Id); }

                // get a user object for the user
                User userObject = new User(AsentiaSessionState.IdSiteUser);

                // add the item to the cart
                TransactionItem transactionItemObject = new TransactionItem();
                transactionItemObject.Id = 0;
                transactionItemObject.IdParentTransactionItem = null;
                transactionItemObject.IdPurchase = purchaseObject.Id;
                transactionItemObject.IdUser = AsentiaSessionState.IdSiteUser;
                transactionItemObject.UserName = userObject.DisplayName;
                transactionItemObject.IdAssigner = AsentiaSessionState.IdSiteUser;
                transactionItemObject.AssignerName = userObject.DisplayName;
                transactionItemObject.ItemId = itemId;
                transactionItemObject.ItemName = itemName;
                transactionItemObject.ItemType = itemType;
                transactionItemObject.Description = itemDescription;
                transactionItemObject.Cost = itemCost;
                transactionItemObject.Paid = itemCost;
                transactionItemObject.Confirmed = false;
                transactionItemObject.IdIltSession = idIltSession;

                int idTransactionItem = transactionItemObject.Save();
                
                // return the newly created transaction item
                return idTransactionItem;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region _BuildCouponCodeFormForPayPalBuyNow
        /// <summary>
        /// Builds the coupon code input for entering a coupon code prior to passing off to PayPal.
        /// </summary>
        /// <param name="targetControl">the control to render the input into</param>
        private void _BuildCouponCodeFormForPayPalBuyNow(Control targetControl)
        {
            // clear the controls from the target control (Panel)
            targetControl.Controls.Clear();

            // add instructions
            Panel instructionsContainer = new Panel();
            instructionsContainer.ID = "CouponCodeInstructionsContainer";                    
            targetControl.Controls.Add(instructionsContainer);
            this.FormatFormInformationPanel(instructionsContainer, _GlobalResources.IfYouHaveACouponCodeEnterItBelowAndClickBuyNowToContinueOtherwiseJustClickBuyNow, false);            

            // add coupon code field
            TextBox couponCodeToApply = new TextBox();
            couponCodeToApply.ID = "CouponCodeToApply_Field";
            couponCodeToApply.CssClass = "InputMedium";
            couponCodeToApply.MaxLength = 10;

            targetControl.Controls.Add(AsentiaPage.BuildFormField("CouponCodeToApply",
                                                           _GlobalResources.CouponCode,
                                                           couponCodeToApply.ID,
                                                           couponCodeToApply,
                                                           false,
                                                           false,
                                                           false));
        }
        #endregion

        #region _ApplyCouponCodePayPalBuyNow_Command
        /// <summary>
        /// Applies coupon code (if applicable) to item, enrolls in item if coupon makes it free, and forwards transaction
        /// to PayPal if item has a cost. This is for PayPal transactions only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ApplyCouponCodePayPalBuyNow_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get the ecommerce settings
                EcommerceSettings ecommerceSettings = new EcommerceSettings();

                // validate that the processor is set and verified and is PayPal
                if (!ecommerceSettings.IsEcommerceSetAndVerified || ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_PAYPAL)
                { throw new AsentiaException(_GlobalResources.CouldNotConnectToPaymentProcessorPleaseContactAnAdministrator); }

                // get the transaction item id, die if not valid
                int idTransactionItem;

                if (String.IsNullOrEmpty(this._ApplyCouponPayPalBuyNowData.Value) || !Int32.TryParse(this._ApplyCouponPayPalBuyNowData.Value, out idTransactionItem))
                { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingTheTransactionPleaseContactAnAdministrator); }
                
                // if there is a coupon code, apply it; otherwise, we just move on
                // note, if the coupon code cannot be applied because its invalid, or does not exist, a db exception will be thrown and this will be our stopping point

                HiddenField couponCodeInput = (HiddenField)FindControlRecursive(this._ApplyCouponPayPalBuyNowModal, "ApplyCouponPayPalCouponCodeData");

                if (!String.IsNullOrWhiteSpace(couponCodeInput.Value))
                { TransactionItem.ApplyCoupon(idTransactionItem, couponCodeInput.Value); }

                // by now, either a coupon code has been applied or there was no coupon code, so we build the paypal form, mark the purchase as pending
                // and ship off to paypal

                // get the transaction item object
                TransactionItem transactionItemObject = new TransactionItem(idTransactionItem);

                // if the effective price is 0, just enroll the user, confirm and detach the item from purchase, and report success
                if (transactionItemObject.Paid == 0)
                {
                    // enroll based on item type
                    switch (transactionItemObject.ItemType)
                    {
                        case PurchaseItemType.Catalog:
                            // enroll the user in the catalog
                            LMS.Library.Enrollment catalogEnrollmentObject = new LMS.Library.Enrollment();
                            catalogEnrollmentObject.EnrollCatalog((int)transactionItemObject.ItemId, AsentiaSessionState.IdSiteUser, transactionItemObject.Id);

                            // display feedback
                            this._ApplyCouponPayPalBuyNowModalDetailsPanel.Controls.Clear();                            
                            this._ApplyCouponPayPalBuyNowModal.DisplayFeedback(String.Format(_GlobalResources.YouHaveBeenEnrolledInAllCoursesWithinThisCatalogSuccessfullyClickHereToGoToYourDashboardToViewYourCourseEnrollments, "<a href=\"/dashboard\">", "</a>"), false);

                            break;
                        case PurchaseItemType.Course:
                            // enroll the user in the course
                            LMS.Library.Enrollment courseEnrollmentObject = new LMS.Library.Enrollment();
                            courseEnrollmentObject.EnrollCourse((int)transactionItemObject.ItemId, AsentiaSessionState.IdSiteUser, transactionItemObject.Id);

                            // display feedback
                            this._ApplyCouponPayPalBuyNowModalDetailsPanel.Controls.Clear();                            
                            this._ApplyCouponPayPalBuyNowModal.DisplayFeedback(String.Format(_GlobalResources.YouHaveBeenEnrolledInThisCourseSuccessfullyClickHereToGoToYourCourseEnrollment, "<a href=\"/dashboard/Enrollment.aspx?idEnrollment=" + courseEnrollmentObject.Id.ToString() + "\">", "</a>"), false);

                            break;
                        case PurchaseItemType.LearningPath:
                            // enroll the user in the learning path
                            LMS.Library.LearningPathEnrollment learningPathEnrollmentObject = new LMS.Library.LearningPathEnrollment();
                            learningPathEnrollmentObject.EnrollLearningPath((int)transactionItemObject.ItemId, AsentiaSessionState.IdSiteUser, transactionItemObject.Id);

                            // display feedback
                            this._ApplyCouponPayPalBuyNowModalDetailsPanel.Controls.Clear();                            
                            this._ApplyCouponPayPalBuyNowModal.DisplayFeedback(String.Format(_GlobalResources.YouHaveBeenEnrolledInThisLearningPathSuccessfullyClickHereToGoToYourLearningPathEnrollment, "<a href=\"/dashboard/LearningPathEnrollment.aspx?id=" + learningPathEnrollmentObject.Id + "\">", "</a>"), false);

                            break;
                        case PurchaseItemType.InstructorLedTraining:                            
                            // enroll in instructor led training instance
                            StandupTrainingInstance instructorLedTrainingInstance = new StandupTrainingInstance((int)transactionItemObject.ItemId);

                            if (instructorLedTrainingInstance.SeatsRemaining > 0)
                            { instructorLedTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, false); }
                            else if (instructorLedTrainingInstance.WaitingSeatsRemaining > 0)
                            { instructorLedTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, true); }
                            else
                            { throw new AsentiaException(_GlobalResources.ThereAreNoSeatsAvailableInTheSelectedInstructorLedTrainingSessionPleaseSelectAnotherSession); }

                            // display feedback
                            this._ApplyCouponPayPalBuyNowModalDetailsPanel.Controls.Clear();
                            this._ApplyCouponPayPalBuyNowModal.DisplayFeedback(_GlobalResources.YouHaveBeenJoinedToTheInstructorLedTrainingSessionSuccessfully, false);

                            break;
                        default:
                            throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingTheTransactionPleaseContactAnAdministrator);
                    }

                    // confirm and detach the item from purchase
                    DataTable transactionIdsToConfirm = new DataTable();
                    transactionIdsToConfirm.Columns.Add("id", typeof(int));

                    transactionIdsToConfirm.Rows.Add(transactionItemObject.Id);

                    TransactionItem.ConfirmTransactionItems(transactionIdsToConfirm, true);

                    // hide the form buttons
                    this._ApplyCouponPayPalBuyNowModal.SubmitButton.Visible = false;
                    this._ApplyCouponPayPalBuyNowModal.CloseButton.Visible = false;
                }
                else // continue on with paypal
                {
                    // get the paypal ecommerce properties for the portal
                    string payPalLogin = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PAYPAL_LOGIN);
                    string currencyCode = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_CURRENCY);

                    // set paypal url based on mode
                    string payPalUrl = String.Empty;

                    if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PAYPAL_MODE) == EcommerceSettings.PROCESSING_MODE_LIVE)
                    { payPalUrl = Config.EcommerceSettings.PayPalIPNReturnLiveURL; }
                    else
                    { payPalUrl = Config.EcommerceSettings.PayPalIPNReturnTestURL; }                

                    // set the return, cancel, and ipn notify urls for the portal
                    string payPalReturnUrl = HttpContext.Current.Request.IsSecureConnection ? "https://" + HttpContext.Current.Request.Url.Host + "/catalog/PaypalSuccess.aspx" : "http://" + HttpContext.Current.Request.Url.Host + "/catalog/PaypalSuccess.aspx";
                    string payPalCancelUrl = HttpContext.Current.Request.IsSecureConnection ? "https://" + HttpContext.Current.Request.Url.Host + "/catalog/PaypalCancel.aspx" : "http://" + HttpContext.Current.Request.Url.Host + "/catalog/PaypalCancel.aspx";
                    string payPalNotifyUrl = "https://" + HttpContext.Current.Request.Url.Host + "/_util/PayPalIPNListener.asmx/ReceiveIPN"; // notify url must always be https                

                    // build the paypal form
                    StringBuilder payPalFormSB = new StringBuilder();

                    payPalFormSB.Append("<form id=\"PayPalPaymentForm\" name=\"PayPalPaymentForm\" action=\"" + payPalUrl + "\" method=\"post\" style=\"display:none\">");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"cmd\" value=\"_xclick\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"business\" value=\"" + payPalLogin + "\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"payer_id\" value=\"" + AsentiaSessionState.IdSiteUser.ToString() + "\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"item_name\" value=\"" + transactionItemObject.ItemName + "\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"item_number\" value=\"" + transactionItemObject.Id.ToString() + "\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"amount\" value=\"" + transactionItemObject.Paid.ToString() + "\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"currency_code\" value=\"" + currencyCode + "\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"no_note\" value=\"1\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"no_shipping\" value=\"1\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"lc\" value=\"US\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"rm\" value=\"1\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"return\" value=\"" + payPalReturnUrl + "\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"cancel_return\" value=\"" + payPalCancelUrl + "\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"notify_url\" value=\"" + payPalNotifyUrl + "\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"add\" value=\"1\"><br />");
                    payPalFormSB.Append("<input type=\"hidden\" name=\"custom\" value=\"" + HttpContext.Current.Request.UserHostAddress + "\"><br />");
                    payPalFormSB.Append("</form>");
                    payPalFormSB.Append("<script type=\"text/javascript\">");
                    payPalFormSB.Append("document.getElementById(\"PayPalPaymentForm\").submit();");
                    payPalFormSB.Append("</script>");

                    // mark the purchase as pending
                    Purchase purchaseObject = new Purchase((int)transactionItemObject.IdPurchase);
                    purchaseObject.DtPending = AsentiaSessionState.UtcNow;
                    purchaseObject.Save();

                    // build startup js that opens the paypal form in a new window
                    StringBuilder reloadView = new StringBuilder();
                    reloadView.AppendLine("payPalWindow = window.open(\"\", \"payPalWindow\", \"width=1000,height=800,scrollbars=yes,resizable=yes,status=no,location=no\");");
                    reloadView.AppendLine("payPalWindow.document.write(\"" + payPalFormSB.ToString().Replace("\"", "\\\"") + "\")");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "GoToPayPal", reloadView.ToString(), true);

                    // display message letting the user know they must complete the transaction within 1 hour, and hide form buttons
                    this._ApplyCouponPayPalBuyNowModal.DisplayFeedback(_GlobalResources.YouMustCompleteYourTransactionWithPayPalWithinOneHour, false);
                    this._ApplyCouponPayPalBuyNowModal.SubmitButton.Visible = false;
                    this._ApplyCouponPayPalBuyNowModal.CloseButton.Visible = false;
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ApplyCouponPayPalBuyNowModal.DisplayFeedback(dnfEx.Message, true);
                this._BuildCouponCodeFormForPayPalBuyNow(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ApplyCouponPayPalBuyNowModal.DisplayFeedback(fnuEx.Message, true);
                this._BuildCouponCodeFormForPayPalBuyNow(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ApplyCouponPayPalBuyNowModal.DisplayFeedback(cpeEx.Message, true);
                this._BuildCouponCodeFormForPayPalBuyNow(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ApplyCouponPayPalBuyNowModal.DisplayFeedback(dEx.Message, true);
                this._BuildCouponCodeFormForPayPalBuyNow(this._ApplyCouponPayPalBuyNowModalDetailsPanel);
            }
            catch (AsentiaException ex) // Asentia exceptions here should be fatal, do not rebuild inputs and lose the buttons
            {
                // display the failure message
                this._ApplyCouponPayPalBuyNowModal.DisplayFeedback(ex.Message, true);

                // lose the buttons
                this._ApplyCouponPayPalBuyNowModal.SubmitButton.Visible = false;
                this._ApplyCouponPayPalBuyNowModal.CloseButton.Visible = false;
            }
        }
        #endregion
    }
}