﻿/* COURSE PROPERTIES */

//Page-specific tab change method
function CoursePropertiesTabChange(selectedTabId, tabContainerId) {
    switch (selectedTabId) {
        case "Properties":
        case "Settings":
        case "Personnel": 
        case "SampleScreens":
            $("#AddLessonLinkContainer").hide();
            $("#AddCourseMaterialLinkContainer").hide();

            $("#CourseModulesInstructionsPanel").hide();
            $("#CourseFormInstructionsPanel").show();            
            
            $("#LessonGridDeleteButton").hide();
            $("#CourseMaterialGridDeleteButton").hide();
            $("#CourseSaveButton").show();
            $("#CancelButton").show();            
            break;
        case "Modules":
            $("#AddCourseMaterialLinkContainer").hide();
            $("#AddLessonLinkContainer").show();

            $("#CourseFormInstructionsPanel").hide();
            $("#CourseModulesInstructionsPanel").show();            
            
            $("#CourseSaveButton").hide();
            $("#CancelButton").hide();
            $("#CourseMaterialGridDeleteButton").hide();
            $("#LessonGridDeleteButton").show();
            break;
        case "CourseMaterials":
            $("#AddLessonLinkContainer").hide();
            $("#AddCourseMaterialLinkContainer").show();

            $("#CourseFormInstructionsPanel").hide();
            $("#CourseModulesInstructionsPanel").hide();            

            $("#CourseSaveButton").hide();
            $("#CancelButton").hide();
            $("#LessonGridDeleteButton").hide();
            $("#CourseMaterialGridDeleteButton").show();
            break;
        default:
            break;
    }
}

//Method to set course wall on
function CourseWallOn() {
    $("#IsCourseWallModerated_Field").prop("disabled", false);
    $("#IsCourseWallOpenView_Field").prop("disabled", false);
}

//Method to set course wall off
function CourseWallOff() {
    $("#IsCourseWallModerated_Field").prop("checked", false);
    $("#IsCourseWallOpenView_Field").prop("checked", false);
    $("#IsCourseWallModerated_Field").prop("disabled", true);
    $("#IsCourseWallOpenView_Field").prop("disabled", true);
}

//Method to toggle "all modules in order setting"
function ToggleAllModulesInOrderSetting() {
    // if "require all modules to be completed in order" setting is checked, then "require first" and "lock last" are also checked
    if ($("#CourseForceLessonCompletionInOrder_Field").prop("checked")) {
        $("#CourseRequireFirstLessonToBeCompletedBeforeOthers_Field").prop("checked", true);
        $("#CourseRequireFirstLessonToBeCompletedBeforeOthers_Field").prop("disabled", true);
        $("#CourseLockLastLessonUntilOthersCompleted_Field").prop("checked", true);
        $("#CourseLockLastLessonUntilOthersCompleted_Field").prop("disabled", true);
    }
    else {
        $("#CourseRequireFirstLessonToBeCompletedBeforeOthers_Field").prop("disabled", false);
        $("#CourseLockLastLessonUntilOthersCompleted_Field").prop("disabled", false);
    }
}

//Method to add new social media link 
function AddSocialMediaNode() {
    var socialMediaItemsContainer = $("#CourseSocialMedia_ItemsContainer");
    var socialMediaItemCount = $("#CourseSocialMediaItemCount_Field");

    // increment the social media item count value
    socialMediaItemCount.val(parseInt(socialMediaItemCount.val()) + 1);

    // create a container div for the item
    var itemContainerDiv = $("<div id=\"CourseSocialMediaItem_" + socialMediaItemCount.val() + "_Container\" class=\"CourseSocialMediaItemContainer\" />");

    // append the container div to the items container div
    itemContainerDiv.appendTo(socialMediaItemsContainer);

    // create a container div for the item inputs
    var itemInputsContainerDiv = $("<div id=\"CourseSocialMediaItem_" + socialMediaItemCount.val() + "_InputsContainer\" class=\"CourseSocialMediaItemInputsContainer\" />");

    // append the item inputs container div to the item container div
    itemInputsContainerDiv.appendTo(itemContainerDiv);

    // create the icon for the social media link
    var itemIcon = $("<img id=\"CourseSocialMediaItem_" + socialMediaItemCount.val() + "_Icon\" src=\"" + CustomForumImagePath + "\" class=\"CourseSocialMediaItemIcon  SmallIcon\" />");

    // append the icon to the item inputs container div
    itemIcon.appendTo(itemInputsContainerDiv);

    // create the drop-down list for url protocol selection
    var protocolSelect = $("<select id=\"CourseSocialMediaItem_" + socialMediaItemCount.val() + "_ProtocolSelect\" class=\"CourseSocialMediaItemProtocolSelect\" />");

    // create and attach options to the protocol drop-down
    $("<option />", { value: "http://", text: "http://" }).appendTo(protocolSelect);
    $("<option />", { value: "https://", text: "https://" }).appendTo(protocolSelect);

    // append the drop-down list to the item inputs container div
    protocolSelect.appendTo(itemInputsContainerDiv);

    // create the textbox for url
    var urlTextBox = $("<input type=\"text\" id=\"CourseSocialMediaItem_" + socialMediaItemCount.val() + "_URLField\"  class=\"InputLong CourseSocialMediaItemURLField\" onChange=\"UpdateSocialMediaItemImage(" + parseInt(socialMediaItemCount.val()) + ");\" onKeyUp=\"UpdateSocialMediaItemImage(" + parseInt(socialMediaItemCount.val()) + ");\" />");

    // append the textbox to the item inputs container div
    urlTextBox.appendTo(itemInputsContainerDiv);

    // create the delete link
    var deleteItemLink = $("<a id=\"CourseSocialMediaItem_" + socialMediaItemCount.val() + "_DeleteItemLink\" href=\"javascript:RemoveSocialMediaNode(" + parseInt(socialMediaItemCount.val()) + ");\" />");

    // append the delete item link to the item inputs container div
    deleteItemLink.appendTo(itemInputsContainerDiv);

    // create the delete icon
    var deleteItemIcon = $("<img id=\"CourseSocialMediaItem_" + socialMediaItemCount.val() + "_DeleteItemIcon\" src=\"" + DeleteImagePath + "\" class=\"SmallIcon\" />");

    // append the delete item icon to the delete item link
    deleteItemIcon.appendTo(deleteItemLink);

    // create the "enrolled-only" checkbox wrapper
    var itemEnrolledOnlyCheckBoxWrapper = $("<div class=\"ToggleInput\">");

    // append the "enrolled-only" checkbox wrapper to the item container
    itemEnrolledOnlyCheckBoxWrapper.appendTo(itemContainerDiv);

    // create the "enrolled-only" checkbox
    var itemEnrolledOnlyCheckBox = $("<input type=\"checkbox\" id=\"CourseSocialMediaItem_" + socialMediaItemCount.val() + "_IsEnrolledOnly\" value=\"true\" class=\"CourseSocialMediaItemIsEnrolledOnlyCheckBox\" />");

    // append the "enrolled-only" checkbox to the wrapper tag
    itemEnrolledOnlyCheckBox.appendTo(itemEnrolledOnlyCheckBoxWrapper);

    // create the "enrolled-only" label
    var itemEnrolledOnlyLabel = $("<label for=\"CourseSocialMediaItem_" + socialMediaItemCount.val() + "_IsEnrolledOnly\">" + IsEnrolledLabelText + "</label>");

    // append the "enrolled-only" label to the wrapper tag
    itemEnrolledOnlyLabel.appendTo(itemEnrolledOnlyCheckBoxWrapper);
}

//Method to remove social media link
function RemoveSocialMediaNode(nodeIndex) {
    // remove the social media item
    $("#CourseSocialMediaItem_" + nodeIndex + "_Container").remove();
}

//Method to update social media link icon image
function UpdateSocialMediaItemImage(nodeIndex) {
    var socialMediaItemURLField = $("#CourseSocialMediaItem_" + nodeIndex + "_URLField");
    var socialMediaItemIcon = $("#CourseSocialMediaItem_" + nodeIndex + "_Icon");

    // build an array of regex validation strings for social media sites
    var validatorsString = ",^([^\/#\?]*[\.]|)(facebook\.com)(|\/|#|[\?]){1},^([^\/#\?]*[\.]|)(linkedin\.com)(|\/|#|[\?]){1},^([^\/#\?]*[\.]|)(twitter\.com)(|\/|#|[\?]){1},^([^\/#\?]*[\.]|)(google\.com)(|\/|#|[\?]){1},^([^\/#\?]*[\.]|)(youtube\.com)(|\/|#|[\?]){1},";
    var validatorsArray = validatorsString.split(",");

    // set the icon as the default
    socialMediaItemIcon.prop("src", CustomForumImagePath);

    // loop through validators and change icon for a match on any social media site
    for (var i = 0; i < validatorsArray.length; i++) {

        if (validatorsArray[i] != "") {

            var regexPattern = new RegExp(validatorsArray[i]);

            if (regexPattern.test(socialMediaItemURLField.val())) {

                if (i == 1) {
                    socialMediaItemIcon.prop("src", FacebookImagePath);
                }

                if (i == 2) {
                    socialMediaItemIcon.prop("src", LinkedInImagePath);
                }

                if (i == 3) {
                    socialMediaItemIcon.prop("src", TwitterImagePath);
                }

                if (i == 4) {
                    socialMediaItemIcon.prop("src", GoogleImagePath);
                }

                if (i == 5) {
                    socialMediaItemIcon.prop("src", YouTubeImagePath);
                }

            }

        }
    }
}

//Gets the added media items in json format
function GetSocialMediaItemsJSON() {
    var socialMediaItems = new Array();
    var socialMediaItemCount = parseInt($("#CourseSocialMediaItemCount_Field").val());
    var socialMediaItemsJSON = $("#CourseSocialMediaItemsJSON_Field");

    for (var i = 1; i <= socialMediaItemCount; i++) {
        var iconType = $("#CourseSocialMediaItem_" + i + "_Icon").prop("src");
        var href = $("#CourseSocialMediaItem_" + i + "_URLField").val();
        var protocol = $("#CourseSocialMediaItem_" + i + "_ProtocolSelect").val();
        var enrollmentRequired = $("#CourseSocialMediaItem_" + i + "_IsEnrolledOnly").is(':checked');

        if (iconType != null && href != null && protocol != null && enrollmentRequired != null) {
            var socialMediaProperty = new SocialMediaProperty();
            socialMediaProperty.IconType = iconType;
            socialMediaProperty.Href = href;
            socialMediaProperty.Protocol = protocol;
            socialMediaProperty.EnrollmentRequired = enrollmentRequired;
            socialMediaItems.push(socialMediaProperty);
        }
    }

    var stringifiedSocialMediaItems = JSON.stringify(socialMediaItems);
    socialMediaItemsJSON.val(stringifiedSocialMediaItems);
}

//Method to create social media object and properties
function SocialMediaProperty() {
    this.IconType = null;
    this.Href = null;
    this.Protocol = null;
    this.EnrollmentRequired = null;
}

//Method to add experts from modal popup to expert listing container
function AddExpertsToCourse() {
    var courseExpertListContainer = $("#CourseExpertsList_Container");
    var selectedUsers = $('select#SelectEligibleExpertUsersListBox').val();

    if (selectedUsers != null) {
        for (var i = 0; i < selectedUsers.length; i++) {
            if (!isNaN(Number(selectedUsers[i])) && !$("#Expert_" + selectedUsers[i]).length) {
                // add the selected user to the course expert list container
                var itemContainerDiv = $("<div id=\"Expert_" + selectedUsers[i] + "\">" + "<img class='SmallIcon' onclick=\"javascript:RemoveExpertFromCourse('" + selectedUsers[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" />" + $("#SelectEligibleExpertUsersListBox option[value='" + selectedUsers[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(courseExpertListContainer);

                // remove the user from the select list
                $("#SelectEligibleExpertUsersListBox option[value='" + selectedUsers[i] + "']").remove();
            }
        }
    }
}

//Method to remove experts from modal popup to expert listing container
function RemoveExpertFromCourse(expertId) {
    $("#Expert_" + expertId).remove();
}

//Gets the selected list of experts from the modal popup
function GetSelectedExpertsForHiddenField() {
    var courseExpertsListContainer = $("#CourseExpertsList_Container");
    var selectedCourseExpertsField = $("#SelectedCourseExperts_Field");
    var selectedCourseExperts = "";

    courseExpertsListContainer.children().each(function () {
        selectedCourseExperts = selectedCourseExperts + $(this).prop("id").replace("Expert_", "") + ",";
    });

    if (selectedCourseExperts.length > 0)
    { selectedCourseExperts = selectedCourseExperts.substring(0, selectedCourseExperts.length - 1); }

    selectedCourseExpertsField.val(selectedCourseExperts);
}

//Adds the selected prerequisites to the course
function AddPrerequisitesToCourse() {
    var coursePrerequisiteListContainer = $("#CoursePrerequisitesList_Container");
    var selectedCourses = $('select#SelectEligibleCoursePrerequisitesListBox').val();

    if (selectedCourses != null) {
        for (var i = 0; i < selectedCourses.length; i++) {
            if (!isNaN(Number(selectedCourses[i])) && !$("#Prerequisite_" + selectedCourses[i]).length) {
                // add the selected course to the course prerequisite list container
                var itemContainerDiv = $("<div id=\"Prerequisite_" + selectedCourses[i] + "\">" + "<img class=\"SmallIcon\" onclick=\"javascript:RemovePrerequisiteFromCourse('" + selectedCourses[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" />" + $("#SelectEligibleCoursePrerequisitesListBox option[value='" + selectedCourses[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(coursePrerequisiteListContainer);

                // remove the course from the select list
                $("#SelectEligibleCoursePrerequisitesListBox option[value='" + selectedCourses[i] + "']").remove();
            }
        }
    }
}

//Method to remove the selected prerequisites from the course
function RemovePrerequisiteFromCourse(courseId) {
    //removes the selected prerequisite from the course 
    $("#Prerequisite_" + courseId).remove();
}

//Method to display the uploaded course sample screen after successful upload
function CourseSampleScreenUploaded(uploadedFilePath) {
    var courseSampleScreensListContainer = $("#CourseSampleScreensList_Container");
    var index = $(".CourseSampleScreenPreviewContainer").length;
    var uploadedFileName = uploadedFilePath.replace("/_upload/avatar/", "");

    // add the uploaded sample screen to the course sample screens list container
    var itemContainerDiv = $("<div id=\"CourseSampleScreenPreviewContainer_" + index + "\" class=\"CourseSampleScreenPreviewContainer\" filename=\"" + uploadedFileName + "\" style=\"background-image: url('" + uploadedFilePath + "');\">" + "<div id=\"CourseSampleScreenDeleteButtonContainer_" + index + "\" class=\"CourseSampleScreenDeleteButtonContainer\">" + "<img id=\"CourseSampleScreenDeleteButton_" + index + "\" class=\"SmallIcon\" onclick=\"javascript:DeleteCourseSampleScreen('" + index + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" />" + "</div></div>");
    itemContainerDiv.appendTo(courseSampleScreensListContainer);

    // clear the upload success status container
    $("#CourseSampleScreenUploader_Field_CompletedPanel").html("");
}

//Method to delete a course sample screen
function DeleteCourseSampleScreen(index) {
    $("#CourseSampleScreenPreviewContainer_" + index).remove();
}

//Method to remove a course avatar
function DeleteCourseAvatar() {
    $("#CourseAvatar_Field_ImageContainer").remove();
    $("#ClearAvatar_Field").val("true");
}

//Gets the selected prerequisites
function GetSelectedPrerequisitesForHiddenField() {
    var coursePrerequisitesListContainer = $("#CoursePrerequisitesList_Container");
    var selectedCoursePrerequisitesField = $("#SelectedCoursePrerequisites_Field");
    var selectedCoursePrerequisites = "";

    coursePrerequisitesListContainer.children().each(function () {
        selectedCoursePrerequisites = selectedCoursePrerequisites + $(this).prop("id").replace("Prerequisite_", "") + ",";
    });

    if (selectedCoursePrerequisites.length > 0)
    { selectedCoursePrerequisites = selectedCoursePrerequisites.substring(0, selectedCoursePrerequisites.length - 1); }

    selectedCoursePrerequisitesField.val(selectedCoursePrerequisites);
}

//Gets the uploaded sample screens
function GetUploadedSampleScreensForHiddenField() {
    var uploadedCourseSampleScreensField = $("#UploadedCourseSampleScreens_Field");
    var uploadedCourseSampleScreenFileNames = "";

    $(".CourseSampleScreenPreviewContainer").each(function () {
        var fileName = $(this).attr("filename");
        
        if (fileName != null && fileName != "") {
            uploadedCourseSampleScreenFileNames = uploadedCourseSampleScreenFileNames + fileName + ",";
        }        
    });

    if (uploadedCourseSampleScreenFileNames.length > 0)
    { uploadedCourseSampleScreenFileNames = uploadedCourseSampleScreenFileNames.substring(0, uploadedCourseSampleScreenFileNames.length - 1); }

    uploadedCourseSampleScreensField.val(uploadedCourseSampleScreenFileNames);
}

//Method to populate hidden fields for dynamically denerated/selected items
function PopulateHiddenFieldsForDynamicCourseElements() {
    //Gets the added media items in json format
    GetSocialMediaItemsJSON();

    //Gets the selected list of experts from the modal popup
    GetSelectedExpertsForHiddenField();

    //Gets the selected list of approvers from the modal popup
    GetSelectedApproversForHiddenField();

    //Gets the selected prerequisites
    GetSelectedPrerequisitesForHiddenField();

    //Gets the uploaded sample screens
    GetUploadedSampleScreensForHiddenField();
}

//Method to add enrollment approvers from modal popup to approver listing container
function AddApproversToCourse() {
    var enrollmentApproverListContainer = $("#EnrollmentApproversList_Container");
    var selectedUsers = $('select#SelectEligibleApproverUsersListBox').val();

    if (selectedUsers != null) {
        for (var i = 0; i < selectedUsers.length; i++) {
            if (!isNaN(Number(selectedUsers[i])) && !$("#Approver_" + selectedUsers[i]).length) {
                // add the selected user to the enrollment approvers list container
                var itemContainerDiv = $("<div id=\"Approver_" + selectedUsers[i] + "\">" + "<img class='SmallIcon' onclick=\"javascript:RemoveApproverFromCourse('" + selectedUsers[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" />" + $("#SelectEligibleApproverUsersListBox option[value='" + selectedUsers[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(enrollmentApproverListContainer);

                // remove the user from the select list
                $("#SelectEligibleApproverUsersListBox option[value='" + selectedUsers[i] + "']").remove();
            }
        }
    }
}

//Method to remove enrollment approvers from modal popup to approver listing container
function RemoveApproverFromCourse(approverId) {
    $("#Approver_" + approverId).remove();
}

//Gets the selected list of enrollment approvers from the modal popup
function GetSelectedApproversForHiddenField() {
    var enrollmentApproversListContainer = $("#EnrollmentApproversList_Container");
    var selectedEnrollmentApproversField = $("#SelectedEnrollmentApprovers_Field");
    var selectedEnrollmentApprovers = "";

    enrollmentApproversListContainer.children().each(function () {
        selectedEnrollmentApprovers = selectedEnrollmentApprovers + $(this).prop("id").replace("Approver_", "") + ",";
    });

    if (selectedEnrollmentApprovers.length > 0)
    { selectedEnrollmentApprovers = selectedEnrollmentApprovers.substring(0, selectedEnrollmentApprovers.length - 1); }

    selectedEnrollmentApproversField.val(selectedEnrollmentApprovers);
}

//Updates shortcode link as you type
function UpdateShortcodeLink(shortcodeFieldId) {
    var shortcodeLinkText = $("#ShortcodeLink").text();
    var shortcodeLinkTextSplit = shortcodeLinkText.split("?");
    var shortcodeQSPrefix = "?type=course&sc=";
    
    shortcodeLinkText = shortcodeLinkTextSplit[0] + shortcodeQSPrefix + $("#CourseShortcode_Field").val();
    $("#ShortcodeLink").text(shortcodeLinkText);    
}

/* LESSON GRID */

//Method to make the rows of the lesson grid sortable so that lesson order can be changed
function InitializeSortableOnLessonGrid() {
    // attach "sortable" to the grid    
    $("#CourseLessonGrid").sortable({
        items: '.GridDataRow,.GridDataRowAlternate',
        containment: '#CourseLessonGrid',
        cursor: 'move',
        update: function (e, ui) {
            UpdateSortedLessonGrid();
        },        
        helper: function (event, ui) {
            return LessonGridSortableHelper(ui);            
        }
    });
}

// Function : UpdateSortedLessonGrid
// Updates the ordering of lessons and the UI elements of the grid so that numbering and alternating row styles are correct.
function UpdateSortedLessonGrid() {
    // re-order the numbering of the lessons
    $("#CourseLessonGrid tr td a span.LessonOrderLabel").each(function (index) {
        $(this).text(index + 1);
    });

    // set the alternate row css
    $("#CourseLessonGrid .GridDataRow").each(function (index) {
        if ($(this).hasClass("GridDataRowAlternate") && !(index % 2)) {
            $(this).removeClass("GridDataRowAlternate");
        }
        else if (!$(this).hasClass("GridDataRowAlternate") && (index % 2)) {
            $(this).addClass("GridDataRowAlternate");
        }
    });

    // set the ordering in the hidden field and save
    var lessonOrdering = "";    

    $("#CourseLessonGrid tr td:first-child input").each(function (index) {
        lessonOrdering = lessonOrdering + $(this).val() + "|";
    });

    lessonOrdering = lessonOrdering.slice(0, -1);
    $("#LessonOrderHiddenField").val(lessonOrdering);

    $.ajax({
        type: "POST",
        url: "Modify.aspx/SaveLessonOrdering",
        data: "{lessonOrdering: \"" + lessonOrdering + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSaveLessonOrderingSuccess,
        failure: function (response) {
        },
        error: function (response) {
        }
    });
}

// Function : LessonGridSortableHelper
// Helper function to maintain the look of the table row being dragged.
function LessonGridSortableHelper(ui) {
    var $originals = ui.children();
    var $helper = ui.clone();

    $helper.children().each(function (index) {
        // Set helper cell sizes to match the original sizes
        $(this).width($originals.eq(index).width());
    });

    return $helper.clone().appendTo("body");
}

// Function: OnSaveLessonOrderingSuccess
// Success handler for AJAX save of lesson ordering.
function OnSaveLessonOrderingSuccess(response) {
    var responseObject = response.d;
}

/* COURSE MATERIAL GRID */

function LoadMakeCourseMaterialPrivateModalContent(idDocumentRepositoryItem) {
    $("#CourseMaterialMakePrivateData").val(idDocumentRepositoryItem);

    $("#CourseMaterialMakePrivateConfirmationModalLaunchButton").click();
    $("#CourseMaterialMakePrivateConfirmationModalLoadButton").click();
}

function LoadMakeCourseMaterialPublicModalContent(idDocumentRepositoryItem) {
    $("#CourseMaterialMakePublicData").val(idDocumentRepositoryItem);

    $("#CourseMaterialMakePublicConfirmationModalLaunchButton").click();
    $("#CourseMaterialMakePublicConfirmationModalLoadButton").click();
}

/* LESSON MODIFY */

function MarkModuleOptional(idLesson) {
    $("#LessonOptionalData").val(idLesson);

    $("#LessonOptionalModalLaunchButton").click();
    $("#LessonOptionalModalLoadButton").click();
}

function MarkModuleRequired(idLesson) {
    $("#LessonRequiredData").val(idLesson);

    $("#LessonRequiredModalLaunchButton").click();
    $("#LessonRequiredModalLoadButton").click();
}

function LoadLessonModifyModalContent(idLesson, doPostbackLoad) {    
    $("#LessonModifyData").val(idLesson);    
    $("#LessonModifyModalLaunchButton").click();

    if (doPostbackLoad) {
        $("#LessonModifyModalLoadButton").click();
    }
    else {
        $("#LessonModifyModal_LessonPropertiesContentChangeWarning").hide();
        $("#LessonModifyModal_OverrideContentChangeLessonDataReset_Container").hide();
        UseContentPackageCheckBoxClick();
        UseStandupTrainingModuleCheckBoxClick();
        UseTaskCheckBoxClick();
        UseOJTCheckBoxClick();
        CheckForEmptyContentPackageSelection();
        CheckForEmptyStandupTrainingModuleSelection();
        CheckForEmptyOJTContentPackageSelection();
    }
}

function LaunchSelectContentPackageModal() {    
    $("#SelectContentPackageModalLaunchButton").click();
    $("#LessonModifyModal_SelectContentPackageClearSearchButton").click();
}

function LaunchSelectStandupTrainingModuleModal() {
    $("#SelectStandupTrainingModuleModalLaunchButton").click();
    $("#LessonModifyModal_SelectStandupTrainingModuleClearSearchButton").click();
}

function LaunchSelectOJTContentPackageModal() {
    $("#SelectOJTContentPackageModalLaunchButton").click();
    $("#LessonModifyModal_SelectOJTContentPackageClearSearchButton").click();
}

function SelectContentPackageForLesson() {
    var selectedContentPackageContainer = $("#LessonModifyModal_LessonSelectedContentPackage_Container");
    var selectedContentPackageNameContainer = $("#LessonModifyModal_SelectedContentPackageNameContainer");
    var selectedContentPackage = $('select#LessonModifyModal_SelectContentPackageListBox').val();

    if (selectedContentPackage != null) {
        // remove the selected content package name container
        selectedContentPackageNameContainer.remove();

        // build a new selected content package name container for the currently selected package
        var itemContainerDiv = $("<div id=\"LessonModifyModal_SelectedContentPackageNameContainer\">" + $("#LessonModifyModal_SelectContentPackageListBox option[value='" + selectedContentPackage + "']").text() + "</div>");
        itemContainerDiv.appendTo(selectedContentPackageContainer);

        // set the hidden field to the value of the selected content package
        $("#LessonModifyModal_SelectedContentPackage_Field").val(selectedContentPackage);

        // hide the modal
        $find('LessonModifyModal_SelectContentPackageForLessonModalModalPopupExtender').hide();
    }
}

function SelectOJTContentPackageForLesson() {
    var selectedContentPackageContainer = $("#LessonModifyModal_LessonSelectedOJTContentPackage_Container");
    var selectedContentPackageNameContainer = $("#LessonModifyModal_SelectedOJTContentPackageNameContainer");
    var selectedContentPackage = $('select#LessonModifyModal_SelectOJTContentPackageListBox').val();

    if (selectedContentPackage != null) {
        // remove the selected content package name container
        selectedContentPackageNameContainer.remove();

        // build a new selected content package name container for the currently selected package
        var itemContainerDiv = $("<div id=\"LessonModifyModal_SelectedOJTContentPackageNameContainer\">" + $("#LessonModifyModal_SelectOJTContentPackageListBox option[value='" + selectedContentPackage + "']").text() + "</div>");
        itemContainerDiv.appendTo(selectedContentPackageContainer);

        // set the hidden field to the value of the selected content package
        $("#LessonModifyModal_SelectedOJTContentPackage_Field").val(selectedContentPackage);

        // hide the modal
        $find('LessonModifyModal_SelectOJTContentPackageForLessonModalModalPopupExtender').hide();
    }
}

function SelectStandupTrainingModuleForLesson() {
    var selectedStandupTrainingModuleContainer = $("#LessonModifyModal_LessonSelectedStandupTrainingModule_Container");
    var selectedStandupTrainingModuleNameContainer = $("#LessonModifyModal_SelectedStandupTrainingModuleNameContainer");
    var selectedStandupTrainingModule = $('select#LessonModifyModal_SelectStandupTrainingModuleListBox').val();

    if (selectedStandupTrainingModule != null) {
        // remove the selected standup training module name container
        selectedStandupTrainingModuleNameContainer.remove();

        // build a new selected content package name container for the currently selected package
        var itemContainerDiv = $("<div id=\"LessonModifyModal_SelectedStandupTrainingModuleNameContainer\">" + $("#LessonModifyModal_SelectStandupTrainingModuleListBox option[value='" + selectedStandupTrainingModule + "']").text() + "</div>");
        itemContainerDiv.appendTo(selectedStandupTrainingModuleContainer);

        // set the hidden field to the value of the selected standup training module
        $("#LessonModifyModal_SelectedStandupTrainingModule_Field").val(selectedStandupTrainingModule);

        // hide the modal
        $find('LessonModifyModal_SelectStandupTrainingModuleForLessonModalModalPopupExtender').hide();
    }
}

function PopulateHiddenFieldsForDynamicLessonElements() {
}

//Method to make the form fields enable/disable on Content Package checkbox click
function UseContentPackageCheckBoxClick() {
    if ($("#LessonModifyModal_UseContentPackage_Field").prop("checked")) {
        $("#LessonModifyModal_ContentPackage_DetailsContainer").css("opacity", "1.0");
        $("#LessonModifyModal_LaunchSelectContentPackageModal").removeClass("aspNetDisabled");
        $("#LessonModifyModal_LaunchSelectContentPackageModal").css("cursor", "pointer");
        $("#LessonModifyModal_LaunchSelectContentPackageModal").css("pointer-events", "inherit");
    }
    else {
        $("#LessonModifyModal_ContentPackage_DetailsContainer").css("opacity", "0.3");
        $("#LessonModifyModal_SelectedContentPackageNameContainer").html(NoneSelectedText);
        $("#LessonModifyModal_SelectedContentPackage_Field").val("");
        $("#LessonModifyModal_LaunchSelectContentPackageModal").addClass("aspNetDisabled");
        $("#LessonModifyModal_LaunchSelectContentPackageModal").css("cursor", "default");
        $("#LessonModifyModal_LaunchSelectContentPackageModal").css("pointer-events", "none");
    }
}

function CheckForEmptyContentPackageSelection() {
    if ($("#LessonModifyModal_SelectedContentPackage_Field").val() == "") {
        $("#LessonModifyModal_SelectedContentPackageNameContainer").html(NoneSelectedText);
    }
}

//Method to make the form fields enable/disable on standup training module checkbox click
function UseStandupTrainingModuleCheckBoxClick() {
    if ($("#LessonModifyModal_UseStandupTrainingModule_Field").prop("checked")) {
        $("#LessonModifyModal_StandupTrainingModule_DetailsContainer").css("opacity", "1.0");
        $("#LessonModifyModal_LaunchSelectStandupTrainingModuleModal").removeClass("aspNetDisabled");
        $("#LessonModifyModal_LaunchSelectStandupTrainingModuleModal").css("cursor", "pointer");
        $("#LessonModifyModal_LaunchSelectStandupTrainingModuleModal").css("pointer-events", "inherit");
    }
    else {
        $("#LessonModifyModal_StandupTrainingModule_DetailsContainer").css("opacity", "0.3");
        $("#LessonModifyModal_SelectedStandupTrainingModuleNameContainer").html(NoneSelectedText);
        $("#LessonModifyModal_SelectedStandupTrainingModule_Field").val("");
        $("#LessonModifyModal_LaunchSelectStandupTrainingModuleModal").addClass("aspNetDisabled");
        $("#LessonModifyModal_LaunchSelectStandupTrainingModuleModal").css("cursor", "default");
        $("#LessonModifyModal_LaunchSelectStandupTrainingModuleModal").css("pointer-events", "none");
    }
}

//Method to check the empty standup training module selection
function CheckForEmptyStandupTrainingModuleSelection() {
    if ($("#LessonModifyModal_SelectedStandupTrainingModule_Field").val() == "") {
        $("#LessonModifyModal_SelectedStandupTrainingModuleNameContainer").html(NoneSelectedText);
    }
}

//Method to make the form fields enable/disable on task checkbox click
function UseTaskCheckBoxClick() {
    if ($("#LessonModifyModal_UseTask_Field").prop("checked")) {
        $("#LessonModifyModal_Task_DetailsContainer").css("opacity", "1.0");
        $("#LessonModifyModal_TaskAllowedDocumentType_Field input").prop("disabled", false);
        $("#LessonModifyModal_TaskUploadedResource_Field_UploadControl input").prop("disabled", false);
        $("#LessonModifyModal_TaskAllowSupervisorAsProctor_Field").prop("disabled", false);
        $("#LessonModifyModal_TaskAllowCourseExpertAsProctor_Field").prop("disabled", false);
    }
    else {
        $("#LessonModifyModal_Task_DetailsContainer").css("opacity", "0.3");
        $("#LessonModifyModal_TaskAllowedDocumentType_Field input").prop("disabled", true);
        $("#LessonModifyModal_UploadedResourceFileNameContainer").html("");
        $("#LessonModifyModal_UploadedResourceFileNameContainer").html(NoneSelectedText);
        $("#LessonModifyModal_TaskUploadedResource_Field_CompletedPanel").html("");
        $("#LessonModifyModal_TaskUploadedResource_ErrorContainer").html("");
        $("#LessonModifyModal_TaskUploadedResource_Field_UploadControl input").attr("style", "");
        $("#LessonModifyModal_TaskUploadedResource_Field_UploadControl input").prop("disabled", true);
        $("#LessonModifyModal_TaskUploadedResource_Field_UploadHiddenFieldOriginalFileName").val("");
        $("#LessonModifyModal_TaskUploadedResource_Field_UploadHiddenField").val("");
        $("#LessonModifyModal_TaskAllowSupervisorAsProctor_Field").prop("disabled", true);
        $("#LessonModifyModal_TaskAllowCourseExpertAsProctor_Field").prop("disabled", true);
        $("#LessonModifyModal_TaskAllowedDocumentType_Field input").removeAttr("checked");
        $("#LessonModifyModal_TaskAllowSupervisorAsProctor_Field").prop("checked", false);
        $("#LessonModifyModal_TaskAllowCourseExpertAsProctor_Field").prop("checked", false);
    }
}

//Method to add html with delete link and uploaded file name
function TaskResourceUploaded() {
    $("#LessonModifyModal_UploadedResourceFileNameContainer").html("");
    $("#LessonModifyModal_UploadedResourceFileNameContainer").html("<img class=\"SmallIcon\" onclick=\"javascript:RemoveTaskUploadedResource();\" src=\"" + RemoveImagePath + "\" style=\"cursor:pointer;\">" + $("#LessonModifyModal_TaskUploadedResource_Field_UploadHiddenFieldOriginalFileName").val());
}

//Method to clear value of the uploader saved path from task Resource Uploadeder control
function RemoveTaskUploadedResource() {
    $("#LessonModifyModal_UploadedResourceFileNameContainer").html("");
    $("#LessonModifyModal_UploadedResourceFileNameContainer").html(NoneSelectedText);
    $("#LessonModifyModal_TaskUploadedResource_Field_CompletedPanel").html("");
    $("#LessonModifyModal_TaskUploadedResource_ErrorContainer").html("");
    $("#LessonModifyModal_TaskUploadedResource_Field_UploadControl input").removeAttr("style");
    $("#LessonModifyModal_TaskUploadedResource_Field_UploadHiddenFieldOriginalFileName").val("");
    $("#LessonModifyModal_TaskUploadedResource_Field_UploadHiddenField").val("");
}

//Method to make the form fields enable/disable on OJT checkbox click
function UseOJTCheckBoxClick() {
    if ($("#LessonModifyModal_UseOJT_Field").prop("checked")) {
        $("#LessonModifyModal_OJT_DetailsContainer").css("opacity", "1.0");
        $("#LessonModifyModal_LaunchSelectOJTContentPackageModal").removeClass("aspNetDisabled");
        $("#LessonModifyModal_LaunchSelectOJTContentPackageModal").css("cursor", "pointer");
        $("#LessonModifyModal_LaunchSelectOJTContentPackageModal").css("pointer-events", "inherit");
        $("#LessonModifyModal_OJTAllowSupervisorAsProctor_Field").prop("disabled", false);
        $("#LessonModifyModal_OJTAllowCourseExpertAsProctor_Field").prop("disabled", false);
    }
    else {
        $("#LessonModifyModal_OJT_DetailsContainer").css("opacity", "0.3");
        $("#LessonModifyModal_SelectedOJTContentPackageNameContainer").html(NoneSelectedText);
        $("#LessonModifyModal_SelectedOJTContentPackage_Field").val("");
        $("#LessonModifyModal_LaunchSelectOJTContentPackageModal").addClass("aspNetDisabled");
        $("#LessonModifyModal_LaunchSelectOJTContentPackageModal").css("cursor", "default");
        $("#LessonModifyModal_LaunchSelectOJTContentPackageModal").css("pointer-events", "none");
        $("#LessonModifyModal_OJTAllowSupervisorAsProctor_Field").prop("checked", false);
        $("#LessonModifyModal_OJTAllowCourseExpertAsProctor_Field").prop("checked", false);
        $("#LessonModifyModal_OJTAllowSupervisorAsProctor_Field").prop("disabled", true);
        $("#LessonModifyModal_OJTAllowCourseExpertAsProctor_Field").prop("disabled", true);
    }
}

function CheckForEmptyOJTContentPackageSelection() {
    if ($("#LessonModifyModal_SelectedOJTContentPackage_Field").val() == "") {
        $("#LessonModifyModal_SelectedOJTContentPackageNameContainer").html(NoneSelectedText);
    }
}

/* COURSE MATERIAL MODIFY */

function LoadCourseMaterialModifyModalContent(idCourseMaterial, doPostbackLoad) {
    $("#CourseMaterialModifyData").val(idCourseMaterial);
    $("#CourseMaterialModifyModalLaunchButton").click();    

    if (doPostbackLoad) {
        $("#CourseMaterialModifyModalLoadButton").click();
    }
    else {
        CourseMaterialLanguageSelectionClick(document.getElementById("CourseMaterialModifyModal_CourseMaterialIsAllLanguages_Field"));
        ResetCourseMaterialUploaderField();
    }
}

function PopulateHiddenFieldsForDynamicCourseMaterialElements() {
}

//Method to reset the course material uploader
function ResetCourseMaterialUploaderField() {    
    $("#CourseMaterialModifyModal_CourseMaterialFile_Field_CompletedPanel").html("");
    $("#CourseMaterialModifyModal_CourseMaterialFile_ErrorContainer").html("");
    $("#CourseMaterialModifyModal_CourseMaterialFile_Field_UploadControl input").attr("style", "");
    $("#CourseMaterialModifyModal_CourseMaterialFile_Field_UploadHiddenFieldOriginalFileName").val("");
    $("#CourseMaterialModifyModal_CourseMaterialFile_Field_UploadHiddenField").val("");
}

//Method to enable/disable the course material language selector based on the "all languages" checkbox
function CourseMaterialLanguageSelectionClick(ele) {
    if (ele.checked == true) {
        $("#CourseMaterialModifyModal_CourseMaterialLanguage_Field").attr("disabled", "disabled")
    }
    else {
        $("#CourseMaterialModifyModal_CourseMaterialLanguage_Field").removeAttr("disabled");
    }
}