﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.Courses
{
    public class Dashboard : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CourseDashboardFormContentWrapperContainer;
        public Panel CourseObjectMenuContainer;
        public Panel CourseDashboardWrapperContainer;
        public Panel CourseDashboardInstructionsPanel;
        public Panel CourseDashboardFeedbackContainer;        
        #endregion

        #region Private Properties
        private Course _CourseObject;
        private EcommerceSettings _EcommerceSettings;
        private ObjectDashboard _CourseObjectDashboard;

        private ModalPopup _NewEnrollmentOptionsModal;
        private Button _NewEnrollmentOptionsModalLaunchButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;            
            csm.RegisterClientScriptResource(typeof(Dashboard), "Asentia.LMS.Pages.Administrator.Courses.Dashboard.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseContentManager) &&
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseEnrollmentManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("UserRating.css");
            this.IncludePageSpecificCssFile("ObjectDashboard.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/courses/Dashboard.css");

            // get the course object
            this._GetCourseObject();

            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetCourseObject
        /// <summary>
        /// Gets a course object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCourseObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._CourseObject = new Course(id); }
                }
                catch
                { Response.Redirect("~/administrator/courses"); }
            }
            else { Response.Redirect("~/administrator/courses"); }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // set container classes
            this.CourseDashboardFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CourseDashboardWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the course object menu
            if (this._CourseObject != null)
            {
                CourseObjectMenu courseObjectMenu = new CourseObjectMenu(this._CourseObject);
                courseObjectMenu.SelectedItem = CourseObjectMenu.MenuObjectItem.CourseDashboard;

                this.CourseObjectMenuContainer.Controls.Add(courseObjectMenu);
            }

            // instansiate new enrollment options modal launch button
            this._NewEnrollmentOptionsModalLaunchButton = new Button();
            this._NewEnrollmentOptionsModalLaunchButton.ID = "NewEnrollmentOptionsModalLaunchButton";
            this._NewEnrollmentOptionsModalLaunchButton.Style.Add("display", "none");
            this.CourseDashboardWrapperContainer.Controls.Add(this._NewEnrollmentOptionsModalLaunchButton);

            // build the course object dashboard
            this._CourseObjectDashboard = new ObjectDashboard("CourseObjectDashboard");

            this._BuildInformationWidget();
            this._BuildActionsWidget();
            this._BuildEnrollmentsWidget();
            this._BuildModuleStatsWidget();

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RATINGS_COURSE_ENABLE))
            { this._BuildRatingWidget(); }

            if (this._EcommerceSettings.IsEcommerceSetAndVerified)
            { this._BuildEcommerceWidget(); }

            // attach object widget to wrapper container
            this.CourseDashboardWrapperContainer.Controls.Add(this._CourseObjectDashboard);            
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get course title information
            string courseTitleInInterfaceLanguage = this._CourseObject.Title;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Course.LanguageSpecificProperty courseLanguageSpecificProperty in this._CourseObject.LanguageSpecificProperties)
                {
                    if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { courseTitleInInterfaceLanguage = courseLanguageSpecificProperty.Title; }
                }
            }

            // get course avatar information
            string courseImagePath;
            string imageCssClass = null;

            if (this._CourseObject.Avatar != null)
            {
                courseImagePath = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + this._CourseObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                imageCssClass = "AvatarImage";
            }
            else
            {
                courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD, ImageFiles.EXT_PNG);
            }                      

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Courses, "/administrator/courses"));
            breadCrumbLinks.Add(new BreadcrumbLink(courseTitleInInterfaceLanguage));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, courseTitleInInterfaceLanguage, courseImagePath, imageCssClass);
        }
        #endregion

        #region _BuildInformationWidget
        /// <summary>
        /// Builds the information widget.
        /// </summary>
        private void _BuildInformationWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> informationWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // CREATED DATE
            string formattedCreatedDate = TimeZoneInfo.ConvertTimeFromUtc(this._CourseObject.DtCreated, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", ""));
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("CreatedDate", _GlobalResources.Created + ":", formattedCreatedDate, ObjectDashboard.WidgetItemType.Text));

            // MODIFIED DATE
            string formattedModifiedDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._CourseObject.DtModified, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", ""));
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("LastModifiedDate", _GlobalResources.LastModified + ":", formattedModifiedDate, ObjectDashboard.WidgetItemType.Text));

            // CREDITS
            if (this._CourseObject.Credits != null)
            { informationWidgetItems.Add(new ObjectDashboard.WidgetItem("Credits", _GlobalResources.Credits + ":", this._CourseObject.Credits.ToString(), ObjectDashboard.WidgetItemType.Text)); }
            else
            { informationWidgetItems.Add(new ObjectDashboard.WidgetItem("Credits", _GlobalResources.Credits + ":", "-", ObjectDashboard.WidgetItemType.Text)); }

            // add the widget
            this._CourseObjectDashboard.AddWidget("InformationWidget", _GlobalResources.Information, informationWidgetItems);
        }
        #endregion

        #region _BuildActionsWidget
        /// <summary>
        /// Builds the actions widget.
        /// </summary>
        private void _BuildActionsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> actionsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // build links for module, course material, certificate, and email notification if the user has course content manager permissions
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseContentManager))
            {
                // NEW MODULE LINK

                Panel createModuleLinkWrapper = new Panel();

                HyperLink createModuleLink = new HyperLink();
                createModuleLink.NavigateUrl = "Modify.aspx?id=" + this._CourseObject.Id.ToString() + "&action=NewModule";
                createModuleLinkWrapper.Controls.Add(createModuleLink);

                Image createModuleLinkImage = new Image();
                createModuleLinkImage.CssClass = "SmallIcon";
                createModuleLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LESSON, ImageFiles.EXT_PNG);
                createModuleLink.Controls.Add(createModuleLinkImage);

                Image createModuleOverlayLinkImage = new Image();
                createModuleOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
                createModuleOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
                createModuleLink.Controls.Add(createModuleOverlayLinkImage);

                Literal createModuleLinkText = new Literal();
                createModuleLinkText.Text = _GlobalResources.CreateANewModule;
                createModuleLink.Controls.Add(createModuleLinkText);

                actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateModule", null, createModuleLinkWrapper, ObjectDashboard.WidgetItemType.Link));

                // NEW COURSE MATERIAL LINK

                Panel createCourseMaterialLinkWrapper = new Panel();

                HyperLink createCourseMaterialLink = new HyperLink();
                createCourseMaterialLink.NavigateUrl = "Modify.aspx?id=" + this._CourseObject.Id.ToString() + "&action=NewCourseMaterial";
                createCourseMaterialLinkWrapper.Controls.Add(createCourseMaterialLink);

                Image createCourseMaterialLinkImage = new Image();
                createCourseMaterialLinkImage.CssClass = "SmallIcon";
                createCourseMaterialLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG);
                createCourseMaterialLink.Controls.Add(createCourseMaterialLinkImage);

                Image createCourseMaterialOverlayLinkImage = new Image();
                createCourseMaterialOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
                createCourseMaterialOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
                createCourseMaterialLink.Controls.Add(createCourseMaterialOverlayLinkImage);

                Literal createCourseMaterialLinkText = new Literal();
                createCourseMaterialLinkText.Text = _GlobalResources.CreateANewCourseMaterial;
                createCourseMaterialLink.Controls.Add(createCourseMaterialLinkText);

                actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateCourseMaterial", null, createCourseMaterialLinkWrapper, ObjectDashboard.WidgetItemType.Link));

                // NEW CERTIFICATE LINK

                Panel createCertificateLinkWrapper = new Panel();

                HyperLink createCertificateLink = new HyperLink();
                createCertificateLink.NavigateUrl = "certificates/Modify.aspx?cid=" + this._CourseObject.Id.ToString();
                createCertificateLinkWrapper.Controls.Add(createCertificateLink);

                Image createCertificateLinkImage = new Image();
                createCertificateLinkImage.CssClass = "SmallIcon";
                createCertificateLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
                createCertificateLink.Controls.Add(createCertificateLinkImage);

                Image createCertificateOverlayLinkImage = new Image();
                createCertificateOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
                createCertificateOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
                createCertificateLink.Controls.Add(createCertificateOverlayLinkImage);

                Literal createCertificateLinkText = new Literal();
                createCertificateLinkText.Text = _GlobalResources.CreateANewCertificate;
                createCertificateLink.Controls.Add(createCertificateLinkText);

                actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateCertificate", null, createCertificateLinkWrapper, ObjectDashboard.WidgetItemType.Link));

                // NEW EMAIL NOTIFICATION LINK

                Panel createEmailNotificationLinkWrapper = new Panel();

                HyperLink createEmailNotificationLink = new HyperLink();
                createEmailNotificationLink.NavigateUrl = "emailnotifications/Modify.aspx?cid=" + this._CourseObject.Id.ToString();
                createEmailNotificationLinkWrapper.Controls.Add(createEmailNotificationLink);

                Image createEmailNotificationLinkImage = new Image();
                createEmailNotificationLinkImage.CssClass = "SmallIcon";
                createEmailNotificationLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
                createEmailNotificationLink.Controls.Add(createEmailNotificationLinkImage);

                Image createEmailNotificationOverlayLinkImage = new Image();
                createEmailNotificationOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
                createEmailNotificationOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
                createEmailNotificationLink.Controls.Add(createEmailNotificationOverlayLinkImage);

                Literal createEmailNotificationLinkText = new Literal();
                createEmailNotificationLinkText.Text = _GlobalResources.CreateANewEmailNotification;
                createEmailNotificationLink.Controls.Add(createEmailNotificationLinkText);

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE) ?? false)
                {
                    actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateEmailNotification", null, createEmailNotificationLinkWrapper, ObjectDashboard.WidgetItemType.Link));
                }
            }

            // build enrollments link if the user has course enrollment manager permissions
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseEnrollmentManager))
            {
                // NEW ENROLLMENT LINK

                Panel createEnrollmentLinkWrapper = new Panel();

                HyperLink createEnrollmentLink = new HyperLink();
                createEnrollmentLink.NavigateUrl = "javascript: void(0);";
                createEnrollmentLink.Attributes.Add("onclick", "LaunchNewEnrollmentOptionsModal();");
                createEnrollmentLinkWrapper.Controls.Add(createEnrollmentLink);

                Image createEnrollmentLinkImage = new Image();
                createEnrollmentLinkImage.CssClass = "SmallIcon";
                createEnrollmentLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
                createEnrollmentLink.Controls.Add(createEnrollmentLinkImage);

                Image createEnrollmentOverlayLinkImage = new Image();
                createEnrollmentOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
                createEnrollmentOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
                createEnrollmentLink.Controls.Add(createEnrollmentOverlayLinkImage);

                Literal createEnrollmentLinkText = new Literal();
                createEnrollmentLinkText.Text = _GlobalResources.CreateANewEnrollment;
                createEnrollmentLink.Controls.Add(createEnrollmentLinkText);

                actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateEnrollment", null, createEnrollmentLinkWrapper, ObjectDashboard.WidgetItemType.Link));

                // build the new enrollment options modal
                this._BuildNewEnrollmentOptionsModal();
            }

            // add the widget
            this._CourseObjectDashboard.AddWidget("ActionsWidget", _GlobalResources.Actions, actionsWidgetItems);
        }
        #endregion

        #region _BuildNewEnrollmentOptionsModal
        /// <summary>
        /// Builds the new enrollment options modal.
        /// </summary>
        private void _BuildNewEnrollmentOptionsModal()
        {
            this._NewEnrollmentOptionsModal = new ModalPopup("NewEnrollmentOptionsModal");

            // set modal properties
            this._NewEnrollmentOptionsModal.Type = ModalPopupType.Information;
            this._NewEnrollmentOptionsModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
            this._NewEnrollmentOptionsModal.HeaderIconAlt = _GlobalResources.NewEnrollment;
            this._NewEnrollmentOptionsModal.HeaderText = _GlobalResources.NewEnrollment;
            this._NewEnrollmentOptionsModal.TargetControlID = this._NewEnrollmentOptionsModalLaunchButton.ID;

            // links container
            Panel enrollmentActionLinksContainer = new Panel();

            // NEW FIXED DATE ONE TIME ENROLLMENT LINK

            Panel fixedDateOneTimeEnrollmentWrapper = new Panel();

            HyperLink addFixedDateOneTimeImageLink = new HyperLink();
            addFixedDateOneTimeImageLink.CssClass = "ImageLink";
            addFixedDateOneTimeImageLink.NavigateUrl = "rulesetenrollments/Modify.aspx?cid=" + this._CourseObject.Id.ToString() + "&type=1";
            fixedDateOneTimeEnrollmentWrapper.Controls.Add(addFixedDateOneTimeImageLink);

            Image fixedDateOneTimeImage = new Image();
            fixedDateOneTimeImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            fixedDateOneTimeImage.CssClass = "SmallIcon";
            addFixedDateOneTimeImageLink.Controls.Add(fixedDateOneTimeImage);

            Localize addFixedDateOneTimeLink = new Localize();
            addFixedDateOneTimeLink.Text = _GlobalResources.NewOneTimeWithFixedDates;
            addFixedDateOneTimeImageLink.Controls.Add(addFixedDateOneTimeLink);

            // ADD RELATIVE DATE ONE TIME ENROLLMENT

            Panel relativeDateOneTimeEnrollmentWrapper = new Panel();

            HyperLink addRelativeDateOneTimeImageLink = new HyperLink();
            addRelativeDateOneTimeImageLink.CssClass = "ImageLink";
            addRelativeDateOneTimeImageLink.NavigateUrl = "rulesetenrollments/Modify.aspx?cid=" + this._CourseObject.Id.ToString() + "&type=2";
            relativeDateOneTimeEnrollmentWrapper.Controls.Add(addRelativeDateOneTimeImageLink);

            Image relativeDateOneTimeImage = new Image();
            relativeDateOneTimeImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            relativeDateOneTimeImage.CssClass = "SmallIcon";
            addRelativeDateOneTimeImageLink.Controls.Add(relativeDateOneTimeImage);

            Localize addRelativeDateOneTimeLink = new Localize();
            addRelativeDateOneTimeLink.Text = _GlobalResources.NewOneTimeWithRelativeDates;
            addRelativeDateOneTimeImageLink.Controls.Add(addRelativeDateOneTimeLink);

            // ADD FIXED DATE RECURRING ENROLLMENT

            Panel fixedDateRecurringEnrollmentWrapper = new Panel();

            HyperLink addFixedDateRecurringImageLink = new HyperLink();
            addFixedDateRecurringImageLink.CssClass = "ImageLink";
            addFixedDateRecurringImageLink.NavigateUrl = "rulesetenrollments/Modify.aspx?cid=" + this._CourseObject.Id.ToString() + "&type=3";
            fixedDateRecurringEnrollmentWrapper.Controls.Add(addFixedDateRecurringImageLink);

            Image fixedDateRecurringImage = new Image();
            fixedDateRecurringImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            fixedDateRecurringImage.CssClass = "SmallIcon";
            addFixedDateRecurringImageLink.Controls.Add(fixedDateRecurringImage);

            Localize addFixedDateRecurringLink = new Localize();
            addFixedDateRecurringLink.Text = _GlobalResources.NewRecurringWithFixedDates;
            addFixedDateRecurringImageLink.Controls.Add(addFixedDateRecurringLink);

            // ADD RELATIVE DATE RECURRING ENROLLMENT

            Panel relativeDateRecurringEnrollmentWrapper = new Panel();

            HyperLink addRelativeDateRecurringImageLink = new HyperLink();
            addRelativeDateRecurringImageLink.CssClass = "ImageLink";
            addRelativeDateRecurringImageLink.NavigateUrl = "rulesetenrollments/Modify.aspx?cid=" + this._CourseObject.Id.ToString() + "&type=4";
            relativeDateRecurringEnrollmentWrapper.Controls.Add(addRelativeDateRecurringImageLink);

            Image relativeDateRecurringImage = new Image();
            relativeDateRecurringImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            relativeDateRecurringImage.CssClass = "SmallIcon";
            addRelativeDateRecurringImageLink.Controls.Add(relativeDateRecurringImage);

            Localize addRelativeDateRecurringLink = new Localize();
            addRelativeDateRecurringLink.Text = _GlobalResources.NewRecurringWithRelativeDates;
            addRelativeDateRecurringImageLink.Controls.Add(addRelativeDateRecurringLink);

            //adding links to container
            enrollmentActionLinksContainer.Controls.Add(fixedDateOneTimeEnrollmentWrapper);
            enrollmentActionLinksContainer.Controls.Add(relativeDateOneTimeEnrollmentWrapper);
            enrollmentActionLinksContainer.Controls.Add(fixedDateRecurringEnrollmentWrapper);
            enrollmentActionLinksContainer.Controls.Add(relativeDateRecurringEnrollmentWrapper);

            // add controls to body
            this._NewEnrollmentOptionsModal.AddControlToBody(enrollmentActionLinksContainer);

            // add modal to content container
            this.PageContentContainer.Controls.Add(this._NewEnrollmentOptionsModal);
        }
        #endregion

        #region _BuildEnrollmentsWidget
        /// <summary>
        /// Builds the enrollments widget.
        /// </summary>
        private void _BuildEnrollmentsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> enrollmentsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the data and build a chart
            DataTable courseEnrollmentsStatusDt = new DataTable();            
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            Chart courseEnrollmentsPieChart;

            // chart colors
            List<string> courseEnrollmentsPieChartColors = new List<string>();
            courseEnrollmentsPieChartColors.Add("#2E7DBD");
            courseEnrollmentsPieChartColors.Add("#55BD86");
            courseEnrollmentsPieChartColors.Add("#FED155");
            courseEnrollmentsPieChartColors.Add("#EC6E61");

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idCourse", this._CourseObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.EnrollmentStatistics]", true);

                // load recordset
                courseEnrollmentsStatusDt.Load(sdr);

                sdr.Close();

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // draw the chart
                courseEnrollmentsPieChart = new Chart("CourseEnrollmentsPieChart", ChartType.Doughnut, courseEnrollmentsStatusDt, courseEnrollmentsPieChartColors, _GlobalResources.Enrollments, null);                
                courseEnrollmentsPieChart.ShowTotalInTitle = true;
                courseEnrollmentsPieChart.ShowTotalAndPercentageInLegend = true;
                courseEnrollmentsPieChart.IsResponsive = false;
                courseEnrollmentsPieChart.CanvasWidth = 125;
                courseEnrollmentsPieChart.CanvasHeight = 125;
            }
            catch // on errors, just draw an empty chart
            {
                // draw an empty chart
                DataTable emptyDataTable = new DataTable();                
                emptyDataTable.Columns.Add("_Total_", typeof(int));                
                emptyDataTable.Rows.Add(0);

                courseEnrollmentsPieChart = new Chart("CourseEnrollmentsPieChart", ChartType.Doughnut, emptyDataTable, courseEnrollmentsPieChartColors, _GlobalResources.Enrollments, null);
                courseEnrollmentsPieChart.ShowTotalInTitle = true;
                courseEnrollmentsPieChart.ShowTotalAndPercentageInLegend = false;
                courseEnrollmentsPieChart.IsResponsive = false;
                courseEnrollmentsPieChart.CanvasWidth = 125;
                courseEnrollmentsPieChart.CanvasHeight = 125;
            }
            finally
            { databaseObject.Dispose(); }            

            // add the chart to the widget item list
            enrollmentsWidgetItems.Add(new ObjectDashboard.WidgetItem("EnrollmentStats", null, courseEnrollmentsPieChart, ObjectDashboard.WidgetItemType.Object));

            // add the widget
            this._CourseObjectDashboard.AddWidget("EnrollmentsWidget", _GlobalResources.Enrollments, enrollmentsWidgetItems);
        }
        #endregion

        #region _BuildModuleStatsWidget
        /// <summary>
        /// Builds the module stats widget.
        /// </summary>
        private void _BuildModuleStatsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> moduleStatsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the data and add it to the widget items            
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            string totalModules = "-";
            string easiestLessonTitle = "-";
            string hardestLessonTitle = "-";
            string averageTimePerLesson = "-";

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idCourse", this._CourseObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@totalModules", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@easiestLesson", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
                databaseObject.AddParameter("@hardestLesson", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
                databaseObject.AddParameter("@averageTimePerLesson", null, SqlDbType.NVarChar, 8, ParameterDirection.Output);

                databaseObject.ExecuteNonQuery("[Course.LessonStatistics]", true);

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // format the data strings
                totalModules = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@totalModules"].Value).ToString();
                
                if (!String.IsNullOrWhiteSpace(AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@easiestLesson"].Value)))
                { easiestLessonTitle = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@easiestLesson"].Value); }

                if (!String.IsNullOrWhiteSpace(AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@hardestLesson"].Value)))
                { hardestLessonTitle = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@hardestLesson"].Value); }

                if (!String.IsNullOrWhiteSpace(AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@averageTimePerLesson"].Value)))
                { averageTimePerLesson = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@averageTimePerLesson"].Value); }
            }
            catch // on errors, just bury it
            { }
            finally
            { databaseObject.Dispose(); }

            // add the items to the widget item list
            moduleStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("TotalModules", _GlobalResources.TotalModules + ":", totalModules, ObjectDashboard.WidgetItemType.Text));
            moduleStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("EasiestModule", _GlobalResources.EasiestModule + ":", easiestLessonTitle, ObjectDashboard.WidgetItemType.Text));
            moduleStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("HardestModule", _GlobalResources.HardestModule + ":", hardestLessonTitle, ObjectDashboard.WidgetItemType.Text));            
            moduleStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("AverageTimePerModule", _GlobalResources.AverageTimeModule + ":", averageTimePerLesson, ObjectDashboard.WidgetItemType.Text));

            // add the widget
            this._CourseObjectDashboard.AddWidget("ModuleStatsWidget", _GlobalResources.ModuleStats, moduleStatsWidgetItems);
        }
        #endregion

        #region _BuildRatingWidget
        /// <summary>
        /// Builds the rating widget.
        /// </summary>
        private void _BuildRatingWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> ratingWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // AVERAGE RATING                   
            if (this._CourseObject.DisallowRating != true)
            {
                UserRating courseRatingControl = new UserRating("CourseRatingControl");
                courseRatingControl.IdObject = this._CourseObject.Id;

                if (this._CourseObject.Rating != null)
                { courseRatingControl.UsersRating = (double)this._CourseObject.Rating; }

                if (this._CourseObject.Votes != null)
                { courseRatingControl.Votes = (int)this._CourseObject.Votes; }

                courseRatingControl.ShowUserRating = true;
                courseRatingControl.ResetUrl = "/_util/RatingService.asmx/ResetCourseRating";

                ratingWidgetItems.Add(new ObjectDashboard.WidgetItem("AverageRating", null, courseRatingControl, ObjectDashboard.WidgetItemType.Object));
            }
            else
            { ratingWidgetItems.Add(new ObjectDashboard.WidgetItem("AverageRating", null, _GlobalResources.RatingsAreNotEnabledForThisCourse, ObjectDashboard.WidgetItemType.Text)); }

            // add the widget
            this._CourseObjectDashboard.AddWidget("RatingWidget", _GlobalResources.Rating, ratingWidgetItems);
        }
        #endregion

        #region _BuildEcommerceWidget
        /// <summary>
        /// Builds the ecommerce widget.
        /// </summary>
        private void _BuildEcommerceWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> ecommerceWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // COST
            if (this._CourseObject.Cost > 0)
            {
                string formattedCourseCost = String.Format("{0}{1:N2} ({2})", this._EcommerceSettings.CurrencySymbol, this._CourseObject.Cost, this._EcommerceSettings.CurrencyCode);
                ecommerceWidgetItems.Add(new ObjectDashboard.WidgetItem("Cost", _GlobalResources.Cost + ":", formattedCourseCost, ObjectDashboard.WidgetItemType.Text));
            }
            else
            { ecommerceWidgetItems.Add(new ObjectDashboard.WidgetItem("Cost", _GlobalResources.Cost + ":", _GlobalResources.Free, ObjectDashboard.WidgetItemType.Text)); }

            // TOTAL EARNINGS

            // get the data and add it to the widget items            
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            double? totalEarnings = 0;

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idObject", this._CourseObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@itemType", PurchaseItemType.Course, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@totalEarnings", null, SqlDbType.Float, 8, ParameterDirection.Output);

                databaseObject.ExecuteNonQuery("[TransactionItem.GetTotalEarningsForObject]", true);

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // format the data strings
                totalEarnings = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@totalEarnings"].Value);

                if (totalEarnings == null)
                { totalEarnings = 0; }
            }
            catch // on errors, just bury it
            { }
            finally
            { databaseObject.Dispose(); }

            // add the earnings to the widget items
            string formattedCourseEarnings = String.Format("{0}{1:N2} ({2})", this._EcommerceSettings.CurrencySymbol, totalEarnings, this._EcommerceSettings.CurrencyCode);
            ecommerceWidgetItems.Add(new ObjectDashboard.WidgetItem("TotalEarnings", _GlobalResources.TotalEarnings + ":", formattedCourseEarnings, ObjectDashboard.WidgetItemType.Text)); 

            // add the widget
            this._CourseObjectDashboard.AddWidget("EcommerceWidget", _GlobalResources.ECommerce, ecommerceWidgetItems);
        }
        #endregion
    }
}
