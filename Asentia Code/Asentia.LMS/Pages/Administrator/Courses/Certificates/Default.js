﻿function RedirectToCertificateTemplate(idCourse) {
    var idCertificate = $('#CertificateTemplateListingListBox :selected').val();

    window.location.replace("/administrator/courses/certificates/Modify.aspx?cid=" + idCourse + "&id=" + idCertificate);
}