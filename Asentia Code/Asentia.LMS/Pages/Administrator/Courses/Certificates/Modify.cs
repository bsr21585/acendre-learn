﻿using AjaxControlToolkit;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using drawing = System.Drawing;


namespace Asentia.LMS.Pages.Administrator.Courses.Certificates
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Public Properties
        public Panel CertificatePropertiesFormContentWrapperContainer;
        public Panel CourseObjectMenuContainer;
        public Panel CertificatePropertiesWrapperContainer;
        public Panel CertificateFormContentContainer;
        public Panel ActionsPanel;
        public Panel CertificatePropertiesTabPanelsContainer;
        #endregion

        #region Private Properties
        private Course _CourseObject;
        private Certificate _CertificateObject;
        private CertificateRecord _CertificateRecordObject;

        private LinkButton _UploadBackgroundImageLink = new LinkButton();
        private ModalPopup _FileUploadModal = new ModalPopup("UploadFile");
        private UploaderAsync _BackgroundImageUploader;

        private TextBox _CertificateName;
        private TextBox _CertificateIssuingOrganization;
        private TextBox _CertificateCredits;
        private TextBox _CertificateCode;
        private CheckBox _CertificateIsActive;
        private CheckBox _ReissueBasedOnMostRecentCompletion;
        private DateIntervalSelector _CertificateExpiration;
        private TextBox _CertificateDescription;

        private HiddenField _HiddenCertificateHtml;
        private HiddenField _HiddenCertificateViewerHtml;
        private HiddenField _HiddenCertificateString;
        private HiddenField _HiddenCertificateJsonString;
        private HiddenField _HiddenImageDimensions;

        private HtmlGenericControl _CertificateWrapper;

        private Button _SaveButton;
        private Button _CancelButton;
        private Boolean _PortraitCertificate = false;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            string defaultLayoutString = "{ \"Certificate\": { \"Container\": { \"Width\" : \"800px\", \"Height\": \"600px\", \"ImageSource\": \"" + SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "default.jpg\"} } }";
            string stringCertificateAvailableFieldsJSON = this._CertificateRecordObject.BuildCertificateAvailableFieldsJSON(false);
            string stringCertificateLayoutJSON = (this._CertificateRecordObject.IdCertificate > 0) ? this._CertificateRecordObject.BuildCertificateLayoutJSON() : defaultLayoutString;
            if (stringCertificateLayoutJSON == "") { stringCertificateLayoutJSON = defaultLayoutString; }

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Controls.CertificateObject.js");
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.Courses.Certificates.Modify.js");

            // for multiple "start up" scripts, we need to build start up calls
            // and add them to the Page_Load
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);

            // "start up" scripts for certificate builder
            StringBuilder startupScript = new StringBuilder();

            startupScript.AppendLine(" var AvailableJSONObj =  jQuery.parseJSON( '" + stringCertificateAvailableFieldsJSON + " '); ");
            startupScript.AppendLine(" var PageMode = '1'; ");
            startupScript.AppendLine(" var BuilderDictionaryJSON = '{ '+");
            startupScript.AppendLine(" ' \"Menu\" : \"" + _GlobalResources.Menu + "\" , '+");
            startupScript.AppendLine(" ' \"Add_Labels_and_Fields\" : \"" + _GlobalResources.AddLabelsAndFields + "\" , '+");
            startupScript.AppendLine(" ' \"Static_Label\" : \"" + _GlobalResources.StaticLabel + "\" , '+");
            startupScript.AppendLine(" ' \"Add\" : \"" + _GlobalResources.Add + "\" , '+");
            startupScript.AppendLine(" ' \"Clear\" : \"" + _GlobalResources.Clear + "\" , '+");
            startupScript.AppendLine(" ' \"Data_Fields\" : \"" + _GlobalResources.DataFields + "\" , '+");
            startupScript.AppendLine(" ' \"drag_and_drop_fields_on_certificate\" : \"" + _GlobalResources.DragAndDropFieldsOnCertificate + "\"  '+");
            startupScript.AppendLine(" '}';           ");
            startupScript.AppendLine(" var CertImageLocation = '" + SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "'; ");
            startupScript.AppendLine(" var DropElement = document.getElementById(\"CertificateBuilderWrapper\");");
            if (this._PortraitCertificate)
            {
                startupScript.AppendLine(" var certificateImageDefaultWidth = '600px';  ");
                startupScript.AppendLine(" var certificateImageDefaultHeight = '800px';  ");
            }
            else
            {
                startupScript.AppendLine(" var certificateImageDefaultWidth = '800px';  ");
                startupScript.AppendLine(" var certificateImageDefaultHeight = '600px';  ");
            }



            startupScript.AppendLine(" var BuilderDictionaryJSONObj = jQuery.parseJSON( BuilderDictionaryJSON );");
            startupScript.AppendLine(" var SaveElement = document.getElementById('instanceData');");
            startupScript.AppendLine(" if(SaveElement.value == ''){");
            startupScript.AppendLine(" SaveElement.value = '" + stringCertificateLayoutJSON + "'; ");
            startupScript.AppendLine(" var LayoutJSONObj =  jQuery.parseJSON( '" + stringCertificateLayoutJSON + " '); ");
            startupScript.AppendLine("}else{");
            startupScript.AppendLine(" var LayoutJSONObj = jQuery.parseJSON(SaveElement.value);");
            startupScript.AppendLine("}");
            startupScript.AppendLine(" var CertificateBuilder = new CertificateObject(            ");
            startupScript.AppendLine("                          LayoutJSONObj, ");
            startupScript.AppendLine("                          AvailableJSONObj, ");
            startupScript.AppendLine("                          PageMode, ");
            startupScript.AppendLine("                          BuilderDictionaryJSONObj, ");
            startupScript.AppendLine("                          DropElement, ");
            startupScript.AppendLine("                          CertImageLocation, ");
            startupScript.AppendLine("                          certificateImageDefaultWidth, ");
            startupScript.AppendLine("                          certificateImageDefaultHeight, ");
            startupScript.AppendLine("                          SaveElement");
            startupScript.AppendLine("                          );");


            startupScript.AppendLine(" var certificateObjectElement = document.getElementById('CertificateObject');");

            // get the available languages
            ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();
            
            // put a container for the background image for each language in the certificate object
            foreach (string availableLanguage in availableLanguages)
            {
                
                startupScript.AppendLine(" var certificateBackgroundContainerElement = document.createElement('div');");
                startupScript.AppendLine(" certificateBackgroundContainerElement.id = 'CertificateBackgroundContainer_" + availableLanguage + "';");
                startupScript.AppendLine(" certificateObjectElement.appendChild(certificateBackgroundContainerElement);");
                startupScript.AppendLine(" certificateBackgroundContainerElement.className = 'CertificateBackgroundContainer';");
                if (availableLanguage != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    startupScript.AppendLine("certificateBackgroundContainerElement.style.display = 'none'");
                }
                 
                startupScript.AppendLine(" var backgroundFileName = document.getElementById('CertificateBackgroundUploadFilepath_" + availableLanguage + "').value;");
                if (this._CertificateObject == null)
                {
                    startupScript.AppendLine(" var backgroundFilePath = '" + SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "default.jpg'");
                }
                else
                {
                    startupScript.AppendLine("if(backgroundFileName == ''){");
                    startupScript.AppendLine(" var backgroundFilePath = '" + SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "default.jpg'");
                    startupScript.AppendLine("}else{");
                    // get the certificate filename
                    HiddenField languageSpecificCertificateUploadFilepathHiddenField = (HiddenField)this.CertificateFormContentContainer.FindControl("CertificateBackgroundUploadFilepath_" + availableLanguage);
                    if (File.Exists(Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + this._CertificateObject.Id.ToString() + "/" + languageSpecificCertificateUploadFilepathHiddenField.Value)))
                    {
                        startupScript.AppendLine(" var backgroundFilePath = '" + SitePathConstants.SITE_CERTIFICATES_ROOT + this._CertificateObject.Id.ToString() + "/' + backgroundFileName;");
                    }
                    else if (File.Exists(Server.MapPath(SitePathConstants.UPLOAD_IMAGE + languageSpecificCertificateUploadFilepathHiddenField.Value)))
                    {
                        startupScript.AppendLine(" var backgroundFilePath = '" + SitePathConstants.UPLOAD_IMAGE + "' + backgroundFileName;");
                    }
                    else
                    {
                        startupScript.AppendLine(" var backgroundFilePath = '" + SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "default.jpg'");
                    }
                    startupScript.AppendLine("}");
                }
                
                startupScript.AppendLine(" $('#' + certificateBackgroundContainerElement.id).css(\"background-image\", \"url('\" + backgroundFilePath + \"')\");");
                if (availableLanguage == AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    startupScript.AppendLine(" $('#' + certificateObjectElement.id).css(\"background-image\", \"url('\" + backgroundFilePath + \"')\");");
                }

                startupScript.AppendLine(" $('#' + certificateBackgroundContainerElement.id).css(\"filter\", \"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='\" + backgroundFilePath + \"',sizingMethod='scale')\");");
                startupScript.AppendLine("var imageSrc = backgroundFilePath;");
                startupScript.AppendLine("var img = new Image();");
                startupScript.AppendLine("var isPortrait;");
                startupScript.AppendLine("img.src = imageSrc;");
                startupScript.AppendLine("isPortrait = (img.naturalWidth < img.naturalHeight) ? true : false;");
                startupScript.AppendLine("newWidth = (isPortrait) ? 600 : 800;");
                startupScript.AppendLine("newHeight = (isPortrait) ? 800 : 600;");
                startupScript.AppendLine(" $('#' + certificateBackgroundContainerElement.id).css(\"width\", newWidth + \"px\");");
                //startupScript.AppendLine(" $('#' + certificateBackgroundContainerElement.id).css(\"height\", newHeight + \"px\");");
                startupScript.AppendLine(" certificateImageDefaultWidth = newWidth;");
                //startupScript.AppendLine(" certificateImageDefaultHeight = newHeight;");

            }

            csm.RegisterStartupScript(typeof(Modify), "Page_Load", startupScript.ToString(), true);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Handles the Page Load Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseContentManager))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("Certificate.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/courses/certificates/Modify.css");

            // get the certificate and course objects
            this._GetCertificateAndCourseObjects();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get course title information
            string courseTitleInInterfaceLanguage = this._CourseObject.Title;
            string courseImagePath;
            string courseImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Course.LanguageSpecificProperty courseLanguageSpecificProperty in this._CourseObject.LanguageSpecificProperties)
                {
                    if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { courseTitleInInterfaceLanguage = courseLanguageSpecificProperty.Title; }
                }
            }

            if (this._CourseObject.Avatar != null)
            {
                courseImagePath = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + this._CourseObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                courseImageCssClass = "AvatarImage";
            }
            else
            {
                courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            }

            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;            
            string pageTitle;

            if (this._CertificateObject != null)
            {
                string certificateTitleInInterfaceLanguage = this._CertificateObject.Name;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (Certificate.LanguageSpecificProperty certificateLanguageSpecificProperty in this._CertificateObject.LanguageSpecificProperties)
                    {
                        if (certificateLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { certificateTitleInInterfaceLanguage = certificateLanguageSpecificProperty.Name; }
                    }
                }

                breadCrumbPageTitle = certificateTitleInInterfaceLanguage;
                pageTitle = certificateTitleInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewCertificate;
                pageTitle = _GlobalResources.NewCertificate;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Courses, "/administrator/courses"));
            breadCrumbLinks.Add(new BreadcrumbLink(courseTitleInInterfaceLanguage, "/administrator/courses/Dashboard.aspx?id=" + this._CourseObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Certificates, "/administrator/courses/certificates/Default.aspx?cid=" + this._CourseObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, courseTitleInInterfaceLanguage, courseImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG), courseImageCssClass);
        }
        #endregion

        #region _GetCertificateAndCourseObjects
        /// <summary>
        /// Gets a certificate object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCertificateAndCourseObjects()
        {
            // get the id querystring parameter
            int qsCId = this.QueryStringInt("cid", 0);
            int vsCId = this.ViewStateInt(this.ViewState, "cid", 0);
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            // get course object - course object MUST be specified and exist for this page to load
            if (qsCId > 0 || vsCId > 0)
            {
                int cid = 0;

                if (qsCId > 0)
                { cid = qsCId; }

                if (vsCId > 0)
                { cid = vsCId; }

                try
                {
                    if (cid > 0)
                    { this._CourseObject = new Course(cid); }
                }
                catch
                { Response.Redirect("~/administrator/courses"); }
            }
            else
            { Response.Redirect("~/administrator/courses"); }

            this._CertificateRecordObject = new CertificateRecord();
            
            // get certificate object (if exists)
            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {                    
                    if (id > 0)
                    { 
                        this._CertificateObject = new Certificate(id);
                        this._CertificateRecordObject.IdCertificate = id;
                    }
                }
                catch
                { Response.Redirect("~/administrator/courses/certificates/Default.aspx?cid=" + this._CourseObject.Id.ToString()); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to create the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // builds the breadcrumb and page title.
            this._BuildBreadcrumbAndPageTitle();

            this.CertificatePropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CertificatePropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the course object menu
            if (this._CourseObject != null)
            {
                CourseObjectMenu courseObjectMenu = new CourseObjectMenu(this._CourseObject);
                courseObjectMenu.SelectedItem = CourseObjectMenu.MenuObjectItem.Certificates;

                this.CourseObjectMenuContainer.Controls.Add(courseObjectMenu);
            }

            // build tabs
            this._BuildTabsList();

            this.CertificatePropertiesTabPanelsContainer = new Panel();
            this.CertificatePropertiesTabPanelsContainer.ID = "CertificateProperties_TabPanelsContainer";
            this.CertificatePropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.CertificateFormContentContainer.Controls.Add(this.CertificatePropertiesTabPanelsContainer);

            // build the form input control panels
            this._BuildCertificateFormPropertiesPanel();
            this._BuildCertificateFormCertificateBuilderPanel();
            this._BuildCertificateFormCertificateBuilderHiddenFields();

            // build file upload modal
            this._BuildFileUploadModal();

            // build the actions panel
            this._BuildActionsPanel();

            // load certificate data
            this._LoadCertificateData();
        }
        #endregion
                
        #region _UploadComplete
        /// <summary>
        /// fires when file uploader control completes uploading the file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="filePath"></param>
        private void _UploadComplete(object sender, AsyncFileUploadEventArgs e, string filePath)
        {
            this._DisplayBackgroundImage(filePath);
        }
        #endregion

        #region _BuildFileUploadModal
        /// <summary>
        /// Builds the certificate background image upload modal.
        /// </summary>
        private void _BuildFileUploadModal()
        {
            this._FileUploadModal.ID = "FileUploadModal";

            this._BackgroundImageUploader = new UploaderAsync("BackgroundImageUploader", UploadType.Image, "PageFeedbackContainer");
            this._BackgroundImageUploader.ShowPreviewOnImageUpload = false;
            this._BackgroundImageUploader.ClientSideCompleteJSMethod = "changeBackgroundImage";
            this._BackgroundImageUploader.ServerSideCompleteMethod = this._UploadComplete;

            // set modal properties
            this._FileUploadModal.Type = ModalPopupType.Information;
            this._FileUploadModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG);
            this._FileUploadModal.HeaderIconAlt = _GlobalResources.UploadBackgroundImage;
            this._FileUploadModal.HeaderText = _GlobalResources.UploadBackgroundImage;
            this._FileUploadModal.TargetControlID = this._UploadBackgroundImageLink.ClientID;

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            body1Wrapper.Controls.Add(_BackgroundImageUploader);

            // add controls to body
            this._FileUploadModal.AddControlToBody(body1Wrapper);
            this.PageContentContainer.Controls.Add(this._FileUploadModal);
        }
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for the form.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("CertificateBuilder", _GlobalResources.CertificateBuilder));            

            // build and attach the tabs
            this.CertificateFormContentContainer.Controls.Add(AsentiaPage.BuildTabListPanel("Certificate", tabs, null, this.Page));
        }
        #endregion

        #region _BuildCertificateFormCertificateBuilderHiddenFields
        /// <summary>
        /// Method to initialize all hidden fields to build the certificate.
        /// </summary>
        private void _BuildCertificateFormCertificateBuilderHiddenFields()
        {
            // certificate html hidden field
            this._HiddenCertificateHtml = new HiddenField();
            this._HiddenCertificateHtml.ClientIDMode = ClientIDMode.Static;
            this._HiddenCertificateHtml.ID = "HiddenCertificateHtml";
            this.CertificatePropertiesTabPanelsContainer.Controls.Add(this._HiddenCertificateHtml);

            // certificate viewer html hidden field
            this._HiddenCertificateViewerHtml = new HiddenField();
            this._HiddenCertificateViewerHtml.ClientIDMode = ClientIDMode.Static;
            this._HiddenCertificateViewerHtml.ID = "HiddenCertificateViewerHtml";
            this.CertificatePropertiesTabPanelsContainer.Controls.Add(this._HiddenCertificateViewerHtml);

            // certificate string hidden field
            this._HiddenCertificateString = new HiddenField();
            this._HiddenCertificateString.ClientIDMode = ClientIDMode.Static;
            this._HiddenCertificateString.ID = "HiddenCertificateString";
            this.CertificatePropertiesTabPanelsContainer.Controls.Add(this._HiddenCertificateString);

            // certificate string hidden json string
            this._HiddenCertificateJsonString = new HiddenField();
            this._HiddenCertificateJsonString.ClientIDMode = ClientIDMode.Static;
            this._HiddenCertificateJsonString.ID = "instanceData";
            this.CertificatePropertiesTabPanelsContainer.Controls.Add(this._HiddenCertificateJsonString);

            // image dimensions hidden field
            this._HiddenImageDimensions = new HiddenField();
            this._HiddenImageDimensions.ClientIDMode = ClientIDMode.Static;
            this._HiddenImageDimensions.ID = "HiddenImageDimensions";
            this.CertificatePropertiesTabPanelsContainer.Controls.Add(this._HiddenImageDimensions);
        }
        #endregion

        #region _BuildCertificateFormPropertiesPanel
        /// <summary>
        ///Method to build the certificate form fields
        /// </summary>
        private void _BuildCertificateFormPropertiesPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "Certificate_Properties_TabPanel";
            propertiesPanel.CssClass = "FormContentContainer TabSubPanel";
            propertiesPanel.Attributes.Add("style", "display: block;");

            // certificate name field
            this._CertificateName = new TextBox();
            this._CertificateName.ID = "CertificateName_Field";
            this._CertificateName.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CertificateName",
                                                             _GlobalResources.Name,
                                                             this._CertificateName.ID,
                                                             this._CertificateName,
                                                             true,
                                                             true,
                                                             true));

            // issuing organization field
            this._CertificateIssuingOrganization = new TextBox();
            this._CertificateIssuingOrganization.ID = "IssuingOrganization_Field";
            this._CertificateIssuingOrganization.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("IssuingOrganization",
                                                             _GlobalResources.IssuingOrganization,
                                                             this._CertificateIssuingOrganization.ID,
                                                             this._CertificateIssuingOrganization,
                                                             false,
                                                             true,
                                                             false));

            // certificate credits field
            this._CertificateCredits = new TextBox();
            this._CertificateCredits.ID = "CertificateCredits_Field";
            this._CertificateCredits.CssClass = "InputXShort";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CertificateCredits",
                                                             _GlobalResources.Credits,
                                                             this._CertificateCredits.ID,
                                                             this._CertificateCredits,
                                                             true,
                                                             true,
                                                             false));


            // certificate code field
            this._CertificateCode = new TextBox();
            this._CertificateCode.ID = "CertificateCode_Field";
            this._CertificateCode.CssClass = "InputXShort";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CertificateCode",
                                                             _GlobalResources.Code,
                                                             this._CertificateCode.ID,
                                                             this._CertificateCode,
                                                             false,
                                                             true,
                                                             false));

            // certificate is active field
            this._CertificateIsActive = new CheckBox();
            this._CertificateIsActive.ID = "CertificateIsActive_Field";
            this._CertificateIsActive.Text = _GlobalResources.Active;
            this._CertificateIsActive.Checked = true;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CertificateIsActive",
                                                             _GlobalResources.Status,
                                                             this._CertificateIsActive.ID,
                                                             this._CertificateIsActive,
                                                             false,
                                                             true,
                                                             false));

            // award field
            List<Control> awardFieldControls = new List<Control>();

            this._ReissueBasedOnMostRecentCompletion = new CheckBox();
            this._ReissueBasedOnMostRecentCompletion.ID = "CertificateReissueBasedOnMostRecentCompletion_Field";
            this._ReissueBasedOnMostRecentCompletion.Text = _GlobalResources.ReissueUnexpiredCertificateAwardsBasedOnMostRecentCourseCompletionDate;
            awardFieldControls.Add(this._ReissueBasedOnMostRecentCompletion);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CertificateAward",
                                                                                _GlobalResources.Award,
                                                                                awardFieldControls,
                                                                                false,
                                                                                true));

            // expiration field
            this._CertificateExpiration = new DateIntervalSelector("CertificateExpiration_Field", true);
            this._CertificateExpiration.NoneCheckBoxChecked = true;
            this._CertificateExpiration.TextBeforeSelector = _GlobalResources.CertificateExpires;
            this._CertificateExpiration.TextAfterSelector = _GlobalResources.AfterAwardDate;
            this._CertificateExpiration.NoneCheckboxText = _GlobalResources.CertificateDoesNotExpire;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CertificateExpiration",
                                                             _GlobalResources.Expiration,
                                                             this._CertificateExpiration.ID,
                                                             this._CertificateExpiration,
                                                             false,
                                                             true,
                                                             false));

            // description field
            this._CertificateDescription = new TextBox();
            this._CertificateDescription.ID = "CertificateDescription_Field";
            this._CertificateDescription.CssClass = "ckeditor";
            this._CertificateDescription.Style.Add("width", "98%");
            this._CertificateDescription.TextMode = TextBoxMode.MultiLine;
            this._CertificateDescription.Rows = 10;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CertificateDescription",
                                                             _GlobalResources.Description,
                                                             this._CertificateDescription.ID,
                                                             this._CertificateDescription,
                                                             false,
                                                             true,
                                                             true));

            // attach panel to container
            this.CertificatePropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildCertificateFormCertificateBuilderPanel
        private void _BuildCertificateFormCertificateBuilderPanel()
        {
            Panel certificateBuilderPanel = new Panel();
            certificateBuilderPanel.ID = "Certificate_CertificateBuilder_TabPanel";
            certificateBuilderPanel.CssClass = "CertificateModify";
            certificateBuilderPanel.Attributes.Add("style", "display: none;");

            Button hiddenBuilderDefaultButton = new Button();
            hiddenBuilderDefaultButton.ID = "HiddenBuilderDefaultButton";
            hiddenBuilderDefaultButton.Style.Add("display", "none");
            hiddenBuilderDefaultButton.Attributes.Add("onclick", "return false;");

            certificateBuilderPanel.Controls.Add(hiddenBuilderDefaultButton);
            certificateBuilderPanel.DefaultButton = hiddenBuilderDefaultButton.ID;

            // build actions container
            Panel uploaderActionsContainer = new Panel();
            uploaderActionsContainer.ID = "UploaderActionsContainer";
            uploaderActionsContainer.Style.Add("padding-top", "5px");

            // build background image uploader
            Panel backgroundImageUploaderContainer = new Panel();
            backgroundImageUploaderContainer.CssClass = "ObjectOptionsPanel";
            backgroundImageUploaderContainer.Style.Add("display", "inline-block");

            this._UploadBackgroundImageLink.ID = "BackgroundImageLink";
            backgroundImageUploaderContainer.Controls.Add(
            this.BuildOptionsPanelImageLink("UploadBackgroundImageLink",
                                            this._UploadBackgroundImageLink,
                                            null,
                                            "resetUploader();",
                                            _GlobalResources.UploadBackgroundImage,
                                            null,
                                            ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG),
                                            ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG))
                );

            uploaderActionsContainer.Controls.Add(backgroundImageUploaderContainer);

            // build language selector
            LanguageSelectorForFormField certificateBackgroundLanguageSelector = new LanguageSelectorForFormField(null, false);
            certificateBackgroundLanguageSelector.ID = "CertificateBackground_LanguageSelector";

            // need to find the list of drop down controls and add a click event to each of them, which will show the proper background container
            foreach (HtmlGenericControl control in certificateBackgroundLanguageSelector.Controls[0].Controls[1].Controls[0].Controls)
            {
                control.Attributes.Add("onclick", "changeBackgroundImageLanguage('" + control.Attributes["language"] + "');");
            }
            uploaderActionsContainer.Controls.Add(certificateBackgroundLanguageSelector);

            certificateBuilderPanel.Controls.Add(uploaderActionsContainer);

            //inner certificate container holder
            this._CertificateWrapper = new HtmlGenericControl("div");
            this._CertificateWrapper.ID = "CertificateBuilderWrapper";

            // get the available languages
            ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

            // loop through languages and create a container for each language
            foreach (string availableLanguage in availableLanguages)
            {
                // get the culture of the info for the language
                CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                HiddenField certificateBackgroundUploadFilepath = new HiddenField();
                certificateBackgroundUploadFilepath.ID = "CertificateBackgroundUploadFilepath_" + availableLanguage;

                if (this._CertificateObject != null)
                {
                    // if there is a language specific background for this language, set the filename in the hidden field
                    foreach (Certificate.LanguageSpecificProperty languageSpecificProperty in this._CertificateObject.LanguageSpecificProperties)
                    {
                        if (languageSpecificProperty.LangString == availableLanguage)
                        {
                            certificateBackgroundUploadFilepath.Value = languageSpecificProperty.Filename;
                            break;
                        }
                    }
                }

                // set the english background filename to the default filename, in case
                // there is no english language set (i.e. before implementation of
                // multi-lingual certificates.
                if (availableLanguage == "en-US" && String.IsNullOrWhiteSpace(certificateBackgroundUploadFilepath.Value) && this._CertificateObject != null)
                {
                    certificateBackgroundUploadFilepath.Value = this._CertificateObject.FileName;
                }

                this._CertificateWrapper.Controls.Add(certificateBackgroundUploadFilepath);


                HiddenField instanceData = new HiddenField();
                instanceData.ClientIDMode = ClientIDMode.Static;
                instanceData.ID = "instanceData_" + availableLanguage;
                this._CertificateWrapper.Controls.Add(instanceData);

            }

            certificateBuilderPanel.Controls.Add(this._CertificateWrapper);

            // attach panel to container
            this.CertificatePropertiesTabPanelsContainer.Controls.Add(certificateBuilderPanel);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _LoadCertificateData
        /// <summary>
        /// Method to load the details of certificate
        /// </summary>
        private void _LoadCertificateData()
        {
            if (this._CertificateObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // name, description
                bool isDefaultPopulated = false;

                foreach (Certificate.LanguageSpecificProperty certificateLanguageSpecificProperty in this._CertificateObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (certificateLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._CertificateName.Text = certificateLanguageSpecificProperty.Name;
                        this._CertificateDescription.Text = certificateLanguageSpecificProperty.Description;

                        isDefaultPopulated = true;

                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificCertificateNameTextBox = (TextBox)this.CertificateFormContentContainer.FindControl(this._CertificateName.ID + "_" + certificateLanguageSpecificProperty.LangString);
                        TextBox languageSpecificCertificateDescriptionTextBox = (TextBox)this.CertificateFormContentContainer.FindControl(this._CertificateDescription.ID + "_" + certificateLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificCertificateNameTextBox != null)
                        { languageSpecificCertificateNameTextBox.Text = certificateLanguageSpecificProperty.Name; }

                        if (languageSpecificCertificateDescriptionTextBox != null)
                        { languageSpecificCertificateDescriptionTextBox.Text = certificateLanguageSpecificProperty.Description; }

                    }
                }

                if (!isDefaultPopulated)
                {
                    this._CertificateName.Text = this._CertificateObject.Name;
                    this._CertificateDescription.Text = this._CertificateObject.Description;
                }

                // NON-LANGUAGE SPECIFIC PROPERTIES

                this._CertificateIssuingOrganization.Text = this._CertificateObject.IssuingOrganization;
                this._CertificateCredits.Text = this._CertificateObject.Credits.ToString();
                this._CertificateCode.Text = this._CertificateObject.Code;
                this._CertificateIsActive.Checked = Convert.ToBoolean(this._CertificateObject.IsActive);
                this._ReissueBasedOnMostRecentCompletion.Checked = Convert.ToBoolean(this._CertificateObject.ReissueBasedOnMostRecentCompletion);

                if (this._CertificateObject.ExpiresInterval != null)
                {
                    this._CertificateExpiration.NoneCheckBoxChecked = false;
                    this._CertificateExpiration.IntervalValue = this._CertificateObject.ExpiresInterval.ToString();
                    this._CertificateExpiration.TimeframeValue = this._CertificateObject.ExpiresTimeframe;
                }
                else
                {
                    this._CertificateExpiration.NoneCheckBoxChecked = true;
                    this._CertificateExpiration.IntervalValue = null;
                    this._CertificateExpiration.TimeframeValue = null;
                }

            }
            else
            {
                // this will display the default background image
                this._DisplayBackgroundImage();
            }
        }
        #endregion

        #region _DisplayBackgroundImage
        /// <summary>
        /// Displays background image for certificate.
        /// </summary>
        /// <param name="imagePath"></param>
        private void _DisplayBackgroundImage(string imagePath = null)
        {
            string backgroundImage = null;
            int newHeight = 0;
            int newWidth = 0;
            drawing::Image objImage = null;

            if (!String.IsNullOrWhiteSpace(imagePath) && File.Exists(Server.MapPath(imagePath)))
            { backgroundImage = imagePath; }
            else
            { backgroundImage = SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "default.jpg"; }

            if (File.Exists(Server.MapPath(backgroundImage)))
            {
                objImage = drawing::Image.FromFile(Server.MapPath(backgroundImage));

                Certificate.GetImageHeightWidth(objImage.Width, objImage.Height, Certificate.CertificateImageDefaultWidth, Certificate.CertificateImageDefaultHeight, out newWidth, out newHeight);

                this._PortraitCertificate = (objImage.Height > objImage.Width) ? true : false;
            }
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;

            // NAME - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._CertificateName.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.CertificateFormContentContainer, "CertificateName", _GlobalResources.Name + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // CREDITS - REQUIRED & MUST BE DOUBLE
            if (String.IsNullOrWhiteSpace(this._CertificateCredits.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.CertificateFormContentContainer, "CertificateCredits", _GlobalResources.Credits + " " + _GlobalResources.IsRequired);
            }
            else
            {
                double credits;

                if (!double.TryParse(this._CertificateCredits.Text, out credits))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.CertificateFormContentContainer, "CertificateCredits", _GlobalResources.Credits + " " + _GlobalResources.IsInvalid);
                }
            }

            // EXPIRATION - IF SPECIFIED, INTERVAL MUST BE INTEGER
            if (!this._CertificateExpiration.NoneCheckBoxChecked)
            {
                int expirationInterval;
                if (String.IsNullOrWhiteSpace(this._CertificateExpiration.IntervalValue) || !int.TryParse(this._CertificateExpiration.IntervalValue, out expirationInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.CertificateFormContentContainer, "CertificateExpiration", _GlobalResources.Expiration + " " + _GlobalResources.IsInvalid);
                }
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.CertificateFormContentContainer, "Certificate_Properties_TabLI"); }

            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                
                // if there is no certificate object, or this is a template, create a new one
                string templateBackgroundImageFilePath = string.Empty;
                string templateFileName = string.Empty;
                int templateId = 0;
                int id;

                if (this._CertificateObject == null)
                {
                    this._CertificateObject = new Certificate();
                    
                }
                else if (this._CertificateObject.Id > 0 && (this._CertificateObject.ObjectType == 0 || this._CertificateObject.ObjectType == null) )
                {
                    templateId = this._CertificateObject.Id;
                    if (!String.IsNullOrWhiteSpace(this._CertificateObject.FileName))
                        { 
                            templateBackgroundImageFilePath = SitePathConstants.SITE_CERTIFICATES_ROOT + this._CertificateObject.Id.ToString() + "/" + this._CertificateObject.FileName; 
                            templateFileName = this._CertificateObject.FileName;
                        }
                    
                    this._CertificateObject = new Certificate();
                    
                }

                // validate the form

                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                // get the name of the previous file
                string previousFileName = this._CertificateObject.FileName;

                // populate the object
                this._CertificateObject.Name = this._CertificateName.Text;
                this._CertificateObject.IssuingOrganization = this._CertificateIssuingOrganization.Text;

                if (!String.IsNullOrWhiteSpace(this._CertificateCredits.Text))
                { this._CertificateObject.Credits = Convert.ToDouble(this._CertificateCredits.Text); }

                this._CertificateObject.Code = this._CertificateCode.Text;

                this._CertificateObject.IsActive = this._CertificateIsActive.Checked;

                this._CertificateObject.ReissueBasedOnMostRecentCompletion = this._ReissueBasedOnMostRecentCompletion.Checked;

                if (!this._CertificateExpiration.NoneCheckBoxChecked)
                {
                    this._CertificateObject.ExpiresInterval = Convert.ToInt32(this._CertificateExpiration.IntervalValue);
                    this._CertificateObject.ExpiresTimeframe = this._CertificateExpiration.TimeframeValue;
                }
                else
                {
                    this._CertificateObject.ExpiresInterval = null;
                    this._CertificateObject.ExpiresTimeframe = null;
                }

                if (!String.IsNullOrWhiteSpace(this._CertificateDescription.Text))
                { this._CertificateObject.Description = HttpUtility.HtmlDecode(this._CertificateDescription.Text); }
                else
                { this._CertificateObject.Description = null; }

                this._CertificateObject.IdObject = this._CourseObject.Id;
                this._CertificateObject.ObjectType = CertificateObjectType.Course;

                HiddenField defaultLanguageSpecificCertificateUploadFilepathHiddenField = (HiddenField)this.CertificateFormContentContainer.FindControl("CertificateBackgroundUploadFilepath_en-US");
                string defaultCertificateFilename = null;
                if (defaultLanguageSpecificCertificateUploadFilepathHiddenField != null)
                {
                    if (!String.IsNullOrWhiteSpace(defaultLanguageSpecificCertificateUploadFilepathHiddenField.Value))
                    {
                        defaultCertificateFilename = defaultLanguageSpecificCertificateUploadFilepathHiddenField.Value;
                    }
                }

                if (!String.IsNullOrWhiteSpace(defaultCertificateFilename))
                { this._CertificateObject.FileName = defaultCertificateFilename; }
                else
                { this._CertificateObject.FileName = null; }
                
                // save the certificate, save its returned id to viewstate, and set the current certificate object's id
                id = this._CertificateObject.Save();
                this.ViewState["id"] = id;
                this._CertificateObject.Id = id;
                this._CertificateRecordObject.IdCertificate = id;

                // check certificate folder existence and create if necessary
                if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + id)))
                { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + id)); }

                // json default filepath
                string defaultFilePath = SitePathConstants.SITE_CERTIFICATES_ROOT + "/" + this._CertificateObject.Id.ToString() + "/Certificate.json";
                
                // do certificate language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // get the certificate filename
                    HiddenField languageSpecificCertificateUploadFilepathHiddenField = (HiddenField)this.CertificateFormContentContainer.FindControl("CertificateBackgroundUploadFilepath_" + cultureInfo.Name);
                    string certificateFilename = null;
                    if (languageSpecificCertificateUploadFilepathHiddenField != null)
                    {
                        if (!String.IsNullOrWhiteSpace(languageSpecificCertificateUploadFilepathHiddenField.Value))
                        {
                            certificateFilename = languageSpecificCertificateUploadFilepathHiddenField.Value;
                        }
                    }

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string certificateName = null;
                        string certificateDescription = null;

                        // get text boxes
                        TextBox languageSpecificCertificateNameTextBox = (TextBox)this.CertificateFormContentContainer.FindControl(this._CertificateName.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificCertificateDescriptionTextBox = (TextBox)this.CertificateFormContentContainer.FindControl(this._CertificateDescription.ID + "_" + cultureInfo.Name);
                        

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificCertificateNameTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCertificateNameTextBox.Text))
                            { certificateName = languageSpecificCertificateNameTextBox.Text; }
                        }

                        if (languageSpecificCertificateDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCertificateDescriptionTextBox.Text))
                            { certificateDescription = languageSpecificCertificateDescriptionTextBox.Text; }
                        }


                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(certificateName) ||
                            !String.IsNullOrWhiteSpace(certificateDescription) ||
                            !String.IsNullOrWhiteSpace(certificateFilename))
                        {
                            // set the language's certificate filename to the default if it does not exist, because it is required
                            if (String.IsNullOrWhiteSpace(certificateName)) { certificateName = this._CertificateObject.Name; }

                            this._CertificateObject.SaveLang(cultureInfo.Name,
                                                             certificateName,
                                                             HttpUtility.HtmlDecode(certificateDescription),
                                                             certificateFilename);
                        }

                        
                    }
                    
                    // build file path and default filepath
                    string filePath = SitePathConstants.SITE_CERTIFICATES_ROOT + "/" + this._CertificateObject.Id.ToString() + "/Certificate_" + cultureInfo.Name + ".json";

                    // create the directory if it doesn't exist
                    if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + "/" + this._CertificateObject.Id.ToString())))
                    { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + "/" + this._CertificateObject.Id.ToString())); }

                    // save the background image
                    if (!String.IsNullOrWhiteSpace(certificateFilename))
                    {

                        
                        

                        // move file to certificates folder
                        string uploadedFilePath = Server.MapPath(SitePathConstants.UPLOAD_IMAGE + certificateFilename);
                        string templateFilePath = Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + templateId + "/" + certificateFilename);
                        string destinationFilePath = Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + id + "/" + certificateFilename);

                        if (File.Exists(uploadedFilePath))
                        {

                            previousFileName = null;
                            foreach (Certificate.LanguageSpecificProperty languageSpecificProperty in this._CertificateObject.LanguageSpecificProperties)
                            {
                                if (languageSpecificProperty.LangString == cultureInfo.Name)
                                {
                                    previousFileName = languageSpecificProperty.Filename;
                                    break;
                                }
                            }

                            // delete previous file if exists (because if the file extension is different then it will not be replaced).
                            if (!String.IsNullOrWhiteSpace(previousFileName))
                            {
                                string previousFilePath = Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + id + "/" + previousFileName);
                                if (File.Exists(previousFilePath))
                                {
                                    // call GC to remove the file access issue
                                    GC.Collect();
                                    GC.WaitForPendingFinalizers();
                                    File.Delete(previousFilePath);
                                }
                            }

                            // save new background file, first delete any exiting file with the same name


                            if (File.Exists(destinationFilePath))
                            {
                                // call GC to remove the file access issue
                                GC.Collect();
                                GC.WaitForPendingFinalizers();
                                File.Delete(destinationFilePath);
                            }
                            File.Move(uploadedFilePath, destinationFilePath);
                        }
                        if (templateId > 0 && File.Exists(templateFilePath))
                        {
                            File.Copy(templateFilePath, destinationFilePath);
                        }
                        


                        
                        drawing::Image bgImage = null;
                        if (File.Exists(destinationFilePath)) { bgImage = drawing::Image.FromFile(destinationFilePath); }


                        HiddenField languageSpecificCertificateJsonString = (HiddenField)this.CertificateFormContentContainer.FindControl("instanceData_" + cultureInfo.Name);

                        JObject jsonCertification = JObject.Parse(this._HiddenCertificateJsonString.Value);
                        jsonCertification["Certificate"]["Container"]["ImageSource"] = SitePathConstants.SITE_CERTIFICATES_ROOT + id + "/" + certificateFilename;
                        if (bgImage == null) { this._PortraitCertificate = true; } else { this._PortraitCertificate = (bgImage.Height > bgImage.Width) ? true : false; }
                        jsonCertification["Certificate"]["Container"]["Width"] = (this._PortraitCertificate) ? "600px" : "800px";
                        jsonCertification["Certificate"]["Container"]["Height"] = (this._PortraitCertificate) ? "800px" : "600px";

                        languageSpecificCertificateJsonString.Value = jsonCertification.ToString(Formatting.None);

                        File.WriteAllText(Server.MapPath(filePath), languageSpecificCertificateJsonString.Value);

                        // save the default certificate json if this language is default language
                        if (cultureInfo.Name == AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            File.WriteAllText(Server.MapPath(defaultFilePath), languageSpecificCertificateJsonString.Value);
                        }
                        
                    }
                    else
                    { }

                }

                // if the default json file was never created, create it using the default background
                if (!File.Exists(Server.MapPath(defaultFilePath)))
                {
                    File.WriteAllText(Server.MapPath(defaultFilePath), this._HiddenCertificateJsonString.Value);
                }

                this._BuildBreadcrumbAndPageTitle();                

                this.DisplayFeedback(_GlobalResources.CertificateHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
                this.ViewState["id"] = 0; // reset cert id in viewstate, in case exception happens on languages
                this._GetCertificateAndCourseObjects();
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
                this.ViewState["id"] = 0; // reset cert id in viewstate, in case exception happens on languages
                this._GetCertificateAndCourseObjects();
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
                this.ViewState["id"] = 0; // reset cert id in viewstate, in case exception happens on languages
                this._GetCertificateAndCourseObjects();
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
                this.ViewState["id"] = 0; // reset cert id in viewstate, in case exception happens on languages
                this._GetCertificateAndCourseObjects();
            }
            catch (AsentiaException)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                this.ViewState["id"] = 0; // reset cert id in viewstate, in case exception happens on languages
                this._GetCertificateAndCourseObjects();
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            // delete temporary uploaded file, if exists
            if (!String.IsNullOrWhiteSpace(this._BackgroundImageUploader.SavedFilePath) && File.Exists(Server.MapPath(this._BackgroundImageUploader.SavedFilePath)))
            { File.Delete(Server.MapPath(this._BackgroundImageUploader.SavedFilePath)); }

            Response.Redirect("Default.aspx?cid=" + this._CourseObject.Id.ToString());
        }
        #endregion
    }
}