﻿function LoadUnpublishCourseModalContent(idCourse) {    
    $("#CourseUnpublishData").val(idCourse);

    $("#UnpublishCourseConfirmationModalLaunchButton").click();
    $("#UnpublishCourseConfirmationModalLoadButton").click();
}

function LoadPublishCourseModalContent(idCourse) {
    $("#CoursePublishData").val(idCourse);

    $("#PublishCourseConfirmationModalLaunchButton").click();
    $("#PublishCourseConfirmationModalLoadButton").click();
}

function LoadOpenCourseModalContent(idCourse) {
    $("#CourseOpenData").val(idCourse);

    $("#OpenCourseConfirmationModalLaunchButton").click();
    $("#OpenCourseConfirmationModalLoadButton").click();
}

function LoadCloseCourseModalContent(idCourse) {
    $("#CourseCloseData").val(idCourse);

    $("#CloseCourseConfirmationModalLaunchButton").click();
    $("#CloseCourseConfirmationModalLoadButton").click();
}