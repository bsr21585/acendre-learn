﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.Courses.EmailNotifications
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel EmailNotificationsFormContentWrapperContainer;
        public Panel CourseObjectMenuContainer;
        public Panel EmailNotificationsWrapperContainer;
        public Panel ObjectOptionsPanel;
        public Panel ActionsPanel;
        public UpdatePanel EmailNotificationGridUpdatePanel;
        public Grid EmailNotificationGrid;
        #endregion

        #region Private Properties
        private Course _CourseObject;

        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmAction;        
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseContentManager))
            { Response.Redirect("/"); }

            // check to ensure Object Specific Email Notifications is enabled on the portal
            if (!AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE) ?? false)
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/courses/emailnotifications/Default.css");

            // get the course object
            this._GetCourseObject();

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.EmailNotificationsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.EmailNotificationsWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the course object menu
            if (this._CourseObject != null)
            {
                CourseObjectMenu courseObjectMenu = new CourseObjectMenu(this._CourseObject);
                courseObjectMenu.SelectedItem = CourseObjectMenu.MenuObjectItem.EmailNotifications;

                this.CourseObjectMenuContainer.Controls.Add(courseObjectMenu);
            }

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildGridActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.EmailNotificationGrid.BindData();
            }
        }
        #endregion

        #region _GetCourseObject
        /// <summary>
        /// Gets a course object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCourseObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("cid", 0);

            if (qsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                try
                {
                    if (id > 0)
                    { this._CourseObject = new Course(id); }
                }
                catch
                { Response.Redirect("~/administrator/courses"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get course title information
            string courseTitleInInterfaceLanguage = this._CourseObject.Title;
            string courseImagePath;
            string courseImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Course.LanguageSpecificProperty courseLanguageSpecificProperty in this._CourseObject.LanguageSpecificProperties)
                {
                    if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { courseTitleInInterfaceLanguage = courseLanguageSpecificProperty.Title; }
                }
            }

            if (this._CourseObject.Avatar != null)
            {
                courseImagePath = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + this._CourseObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                courseImageCssClass = "AvatarImage";
            }
            else
            {
                courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Courses, "/administrator/courses"));
            breadCrumbLinks.Add(new BreadcrumbLink(courseTitleInInterfaceLanguage, "/administrator/courses/Dashboard.aspx?id=" + this._CourseObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.EmailNotifications));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, courseTitleInInterfaceLanguage, courseImagePath, _GlobalResources.EmailNotifications, ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG), courseImageCssClass);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD EMAIL NOTIFICATION
            optionsPanelLinksContainer.Controls.Add(this.BuildOptionsPanelImageLink("AddEmailNotificationLink",
                                                                                    null,
                                                                                    "Modify.aspx?cid=" + this._CourseObject.Id.ToString(),
                                                                                    null,
                                                                                    _GlobalResources.NewEmailNotification,
                                                                                    null,
                                                                                    ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG),
                                                                                    ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG)));

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.EmailNotificationGrid.StoredProcedure = Library.EventEmailNotification.GridProcedure;
            this.EmailNotificationGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.EmailNotificationGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.EmailNotificationGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.EmailNotificationGrid.AddFilter("@eventType", SqlDbType.Int, 4, 2);
            this.EmailNotificationGrid.AddFilter("@idObject", SqlDbType.Int, 4, this._CourseObject.Id.ToString());
            this.EmailNotificationGrid.IdentifierField = "idEventEmailNotification";
            this.EmailNotificationGrid.DefaultSortColumn = "name";
            this.EmailNotificationGrid.SearchBoxPlaceholderText = _GlobalResources.SearchEmailNotifications;

            // data key names
            this.EmailNotificationGrid.DataKeyNames = new string[] { "idEventEmailNotification", "idObject" };

            // columns
            GridColumn nameEventType = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.EventType + "", null, "name"); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.EmailNotificationGrid.AddColumn(nameEventType);

            // row data bound event
            this.EmailNotificationGrid.RowDataBound += new GridViewRowEventHandler(_EmailNotificationGrid_RowDataBound);
        }
        #endregion

        #region _EmailNotificationGrid_RowDataBound
        /// <summary>
        /// Row data bound event for email notifications grid. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _EmailNotificationGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idEventEmailNotification = Convert.ToInt32(rowView["idEventEmailNotification"]); // will never be null

                // AVATAR, NAME, EVENT TYPE

                string name = Server.HtmlEncode(rowView["name"].ToString());
                string eventType = EventEmailNotificationForm.GetEventTypeText(Convert.ToInt32(rowView["eventType"].ToString()));

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = name;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink nameLink = new HyperLink();
                nameLink.NavigateUrl = "Modify.aspx?cid=" + this._CourseObject.Id.ToString() + "&id=" + idEventEmailNotification.ToString();
                nameLink.Text = name;
                nameLabel.Controls.Add(nameLink);

                // event type
                Label eventTypeLabel = new Label();
                eventTypeLabel.CssClass = "GridSecondaryLine";
                eventTypeLabel.Text = eventType;
                e.Row.Cells[1].Controls.Add(eventTypeLabel);
            }
        }
        #endregion

        #region _BuildGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedEmailNotifications_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedEmailNotifications_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            body1Wrapper.ID = "GridConfirmActionModalBody1";

            Literal body1 = new Literal();
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseEmailNotification_s;                        
            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            // add modal to container
            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.EmailNotificationGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.EmailNotificationGrid.Rows[i].FindControl(this.EmailNotificationGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                if (recordsToDelete.Rows.Count > 0)
                {
                    // delete the records
                    Library.EventEmailNotification.Delete(recordsToDelete);

                    // delete related xml file
                    foreach (DataRow row in recordsToDelete.Rows)
                    {
                        string fullFilePath = Server.MapPath(SitePathConstants.SITE_EMAILNOTIFICATIONS_ROOT + row["id"].ToString() + "/EmailNotification.xml");

                        if (File.Exists(fullFilePath))
                        { File.Delete(fullFilePath); }
                    }

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedEmailNotification_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display an error message that at least one record must be selected
                    this.DisplayFeedback(_GlobalResources.NoEmailNotification_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.EmailNotificationGrid.BindData();
            }
        }
        #endregion        
    }
}