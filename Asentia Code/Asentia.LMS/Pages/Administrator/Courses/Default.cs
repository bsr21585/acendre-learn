﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.Courses
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel CoursesFormContentWrapperContainer;
        public Panel GridAnalyticPanel;
        public UpdatePanel CourseGridUpdatePanel;
        public Grid CourseGrid;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmAction;

        // unpublish course confirmation modal
        private ModalPopup _UnpublishCourseConfirmationModal;
        private Button _UnpublishCourseConfirmationModalLaunchButton;
        private Button _UnpublishCourseConfirmationModalLoadButton;

        // publish course confirmation modal
        private ModalPopup _PublishCourseConfirmationModal;
        private Button _PublishCourseConfirmationModalLaunchButton;
        private Button _PublishCourseConfirmationModalLoadButton;

        // open course confirmation modal
        private ModalPopup _OpenCourseConfirmationModal;
        private Button _OpenCourseConfirmationModalLaunchButton;
        private Button _OpenCourseConfirmationModalLoadButton;

        // close course confirmation modal
        private ModalPopup _CloseCourseConfirmationModal;
        private Button _CloseCourseConfirmationModalLaunchButton;
        private Button _CloseCourseConfirmationModalLoadButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Courses.Default.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseContentManager) &&
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseEnrollmentManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("GridAnalytic.css");
            this.IncludePageSpecificCssFile("UserRating.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/courses/Default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Courses));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, _GlobalResources.Courses, ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CoursesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // instansiate unpublish course controls
            this._UnpublishCourseConfirmationModalLaunchButton = new Button();
            this._UnpublishCourseConfirmationModalLaunchButton.ID = "UnpublishCourseConfirmationModalLaunchButton";
            this._UnpublishCourseConfirmationModalLaunchButton.Style.Add("display", "none");

            this._UnpublishCourseConfirmationModalLoadButton = new Button();
            this._UnpublishCourseConfirmationModalLoadButton.ID = "UnpublishCourseConfirmationModalLoadButton";
            this._UnpublishCourseConfirmationModalLoadButton.Style.Add("display", "none");
            this._UnpublishCourseConfirmationModalLoadButton.Click += this._LoadUnpublishCourseConfirmationModalContent;

            // instansiate publish course controls
            this._PublishCourseConfirmationModalLaunchButton = new Button();
            this._PublishCourseConfirmationModalLaunchButton.ID = "PublishCourseConfirmationModalLaunchButton";
            this._PublishCourseConfirmationModalLaunchButton.Style.Add("display", "none");

            this._PublishCourseConfirmationModalLoadButton = new Button();
            this._PublishCourseConfirmationModalLoadButton.ID = "PublishCourseConfirmationModalLoadButton";
            this._PublishCourseConfirmationModalLoadButton.Style.Add("display", "none");
            this._PublishCourseConfirmationModalLoadButton.Click += this._LoadPublishCourseConfirmationModalContent;

            // instansiate open course controls
            this._OpenCourseConfirmationModalLaunchButton = new Button();
            this._OpenCourseConfirmationModalLaunchButton.ID = "OpenCourseConfirmationModalLaunchButton";
            this._OpenCourseConfirmationModalLaunchButton.Style.Add("display", "none");

            this._OpenCourseConfirmationModalLoadButton = new Button();
            this._OpenCourseConfirmationModalLoadButton.ID = "OpenCourseConfirmationModalLoadButton";
            this._OpenCourseConfirmationModalLoadButton.Style.Add("display", "none");
            this._OpenCourseConfirmationModalLoadButton.Click += this._LoadOpenCourseConfirmationModalContent;

            // instansiate close course controls
            this._CloseCourseConfirmationModalLaunchButton = new Button();
            this._CloseCourseConfirmationModalLaunchButton.ID = "CloseCourseConfirmationModalLaunchButton";
            this._CloseCourseConfirmationModalLaunchButton.Style.Add("display", "none");

            this._CloseCourseConfirmationModalLoadButton = new Button();
            this._CloseCourseConfirmationModalLoadButton.ID = "CloseCourseConfirmationModalLoadButton";
            this._CloseCourseConfirmationModalLoadButton.Style.Add("display", "none");
            this._CloseCourseConfirmationModalLoadButton.Click += this._LoadCloseCourseConfirmationModalContent;

            // attach the modal launch controls            
            this.CoursesFormContentWrapperContainer.Controls.Add(this._UnpublishCourseConfirmationModalLaunchButton);
            this.CoursesFormContentWrapperContainer.Controls.Add(this._PublishCourseConfirmationModalLaunchButton);
            this.CoursesFormContentWrapperContainer.Controls.Add(this._OpenCourseConfirmationModalLaunchButton);
            this.CoursesFormContentWrapperContainer.Controls.Add(this._CloseCourseConfirmationModalLaunchButton);

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();            
            this._BuildGridAnalytics();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // build modals for publish, unpublish, open, close
            this._BuildPublishCourseConfirmationModal();
            this._BuildUnpublishCourseConfirmationModal();
            this._BuildOpenCourseConfirmationModal();
            this._BuildCloseCourseConfirmationModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.CourseGrid.BindData();
            }
        }
        #endregion        

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            // build new course link if the user has course content manager permissions
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseContentManager))
            {
                Panel optionsPanelLinksContainer = new Panel();
                optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
                optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

                // ADD COURSE
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("AddCourseLink",
                                                    null,
                                                    "Modify.aspx",
                                                    null,
                                                    _GlobalResources.NewCourse,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                    );

                this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
            }
        }
        #endregion

        #region _BuildGridAnalytics
        /// <summary>
        /// Builds the grid analytics for the page.
        /// </summary>
        private void _BuildGridAnalytics()
        {
            // get the analytic data
            DataTable analyticData = GridAnalyticData.Courses();

            int total = Convert.ToInt32(analyticData.Rows[0]["total"]);
            int published = Convert.ToInt32(analyticData.Rows[0]["published"]);
            int nonPublished = Convert.ToInt32(analyticData.Rows[0]["nonPublished"]);
            int closed = Convert.ToInt32(analyticData.Rows[0]["closed"]);
            int createdThisWeek = Convert.ToInt32(analyticData.Rows[0]["createdThisWeek"]);
            int createdThisMonth = Convert.ToInt32(analyticData.Rows[0]["createdThisMonth"]);
            int createdThisYear = Convert.ToInt32(analyticData.Rows[0]["createdThisYear"]);

            // build title for column 1
            GridAnalytic.DataBlock column1Title = new GridAnalytic.DataBlock(total.ToString("N0"), _GlobalResources.Courses);

            // build a DataBlock list for the column 1 data
            List<GridAnalytic.DataBlock> dataBlocksColumn1 = new List<GridAnalytic.DataBlock>();
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(published.ToString("N0"), _GlobalResources.Published.ToLower()));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(nonPublished.ToString("N0"), _GlobalResources.NonPublished.ToLower()));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(closed.ToString("N0"), _GlobalResources.Closed.ToLower()));

            // build title for column 2
            GridAnalytic.DataBlock column2Title = new GridAnalytic.DataBlock(null, _GlobalResources.NewCourses);

            // build a DataBlock list for the column 2 data
            List<GridAnalytic.DataBlock> dataBlocksColumn2 = new List<GridAnalytic.DataBlock>();
            dataBlocksColumn2.Add(new GridAnalytic.DataBlock(createdThisWeek.ToString("N0"), _GlobalResources.thisweek_lower.ToLower()));
            dataBlocksColumn2.Add(new GridAnalytic.DataBlock(createdThisMonth.ToString("N0"), _GlobalResources.thismonth_lower.ToLower()));
            dataBlocksColumn2.Add(new GridAnalytic.DataBlock(createdThisYear.ToString("N0"), _GlobalResources.thisyear_lower.ToLower()));

            // build the GridAnalytic object
            GridAnalytic gridAnalyticObject = new GridAnalytic("CourseStatistics", column1Title, dataBlocksColumn1, column2Title, dataBlocksColumn2);

            // attach the object to the container
            this.GridAnalyticPanel.Controls.Add(gridAnalyticObject);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the course grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // apply css class to container
            this.CourseGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.CourseGrid.StoredProcedure = Library.Course.GridProcedure;
            this.CourseGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.CourseGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.CourseGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.CourseGrid.IdentifierField = "idCourse";
            this.CourseGrid.DefaultSortColumn = "title";
            this.CourseGrid.SearchBoxPlaceholderText = _GlobalResources.SearchCourses;

            // data key names
            this.CourseGrid.DataKeyNames = new string[] { "idCourse" };

            // columns
            GridColumn titleCodeRatingCredits = new GridColumn(_GlobalResources.Name + " (" + _GlobalResources.Code + "), " + _GlobalResources.Rating + ", " + _GlobalResources.Credits, null, "title"); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.CourseGrid.AddColumn(titleCodeRatingCredits);
            this.CourseGrid.AddColumn(options);

            // add row data bound event
            this.CourseGrid.RowDataBound += new GridViewRowEventHandler(this._CourseGrid_RowDataBound);
        }
        #endregion

        #region _CourseGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _CourseGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idCourse = Convert.ToInt32(rowView["idCourse"]);
                bool isCourseAttachedToACertification = Convert.ToBoolean(rowView["isAttachedToACertification"]);                

                // if the course is attached to a certification, it cannot be deleted, disable the checkbox
                if (isCourseAttachedToACertification)
                {
                    string rowOrdinal = e.Row.DataItemIndex.ToString();
                    CheckBox rowCheckBox = (CheckBox)e.Row.Cells[0].FindControl(this.CourseGrid.ID + "_GridSelectRecord_" + rowOrdinal);

                    if (rowCheckBox != null)
                    { 
                        rowCheckBox.Enabled = false;
                        rowCheckBox.InputAttributes.Add("title", _GlobalResources.ThisCourseIsAttachedToACertificationItCannotBeDeleted);
                    }
                }

                // AVATAR, TITLE, (CODE), RATING, CREDITS COLUMN

                string avatar = rowView["avatar"].ToString();
                string title = Server.HtmlEncode(rowView["title"].ToString());
                string coursecode = Server.HtmlEncode(rowView["coursecode"].ToString());
                string credits = Server.HtmlEncode(rowView["credits"].ToString());
                bool disallowRating = Convert.ToBoolean(rowView["disallowRating"]);
                double rating = Convert.ToDouble(rowView["rating"]);
                bool isPublished = Convert.ToBoolean(rowView["isPublished"]);
                bool isClosed = Convert.ToBoolean(rowView["isClosed"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                if (!String.IsNullOrWhiteSpace(avatar))
                {
                    avatarImagePath = SitePathConstants.SITE_COURSES_ROOT + idCourse.ToString() + "/" + avatar;
                    avatarImageClass += " AvatarImage";
                }

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = title;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // title
                Label titleLabel = new Label();
                titleLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(titleLabel);
                
                HyperLink titleLink = new HyperLink();
                titleLink.NavigateUrl = "Dashboard.aspx?id=" + idCourse.ToString();
                titleLink.Text = title;
                titleLabel.Controls.Add(titleLink);

                // course code
                if (!String.IsNullOrWhiteSpace(coursecode))
                {
                    Label coursecodeLabel = new Label();
                    coursecodeLabel.CssClass = "GridSecondaryTitle";
                    coursecodeLabel.Text = "(" + coursecode + ")";
                    e.Row.Cells[1].Controls.Add(coursecodeLabel);
                }

                // rating
                if (!disallowRating)
                {
                    Label ratingLabel = new Label();
                    ratingLabel.CssClass = "GridSecondaryLine";
                    e.Row.Cells[1].Controls.Add(ratingLabel);

                    Controls.UserRating ratingControl = new Controls.UserRating("CourseRating_" + idCourse.ToString());
                    ratingControl.ShowUserRating = false;
                    ratingControl.ShowYourRating = false;
                    ratingControl.UsersRating = rating;
                    ratingLabel.Controls.Add(ratingControl);
                }

                // credits
                if (!String.IsNullOrWhiteSpace(credits))
                {
                    Label creditsLabel = new Label();
                    creditsLabel.CssClass = "GridSecondaryLine";
                    creditsLabel.Text = credits + " " + _GlobalResources.Credits;
                    creditsLabel.Text = credits.ToString() + " " + ((credits != "1") ? _GlobalResources.Credits : _GlobalResources.Credit);
                    e.Row.Cells[1].Controls.Add(creditsLabel);
                }

                // OPTIONS COLUMN 

                // publish/unpublish
                if (isPublished)
                {
                    Label coursePublishUnPublishSpan = new Label();
                    coursePublishUnPublishSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(coursePublishUnPublishSpan);

                    LinkButton courseUnpublishLink = new LinkButton();
                    courseUnpublishLink.ID = "CourseUnpublishLink_" + idCourse.ToString();
                    courseUnpublishLink.OnClientClick = "LoadUnpublishCourseModalContent(" + idCourse + "); return false;";
                    courseUnpublishLink.ToolTip = _GlobalResources.UnpublishCourse;

                    Image courseUnpublishLinkImage = new Image();
                    courseUnpublishLinkImage.ID = "CourseUnpublishLinkImage_" + idCourse.ToString();
                    courseUnpublishLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                    courseUnpublishLinkImage.AlternateText = _GlobalResources.Published;

                    courseUnpublishLink.Controls.Add(courseUnpublishLinkImage);

                    coursePublishUnPublishSpan.Controls.Add(courseUnpublishLink);
                }
                else
                {
                    Label coursePublishUnPublishSpan = new Label();
                    coursePublishUnPublishSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(coursePublishUnPublishSpan);

                    LinkButton coursePublishLink = new LinkButton();
                    coursePublishLink.ID = "CoursePublishLink_" + idCourse.ToString();
                    coursePublishLink.OnClientClick = "LoadPublishCourseModalContent(" + idCourse + "); return false;";
                    coursePublishLink.ToolTip = _GlobalResources.PublishCourse;

                    Image coursePublishLinkImage = new Image();
                    coursePublishLinkImage.ID = "CoursePublishLinkImage_" + idCourse.ToString();
                    coursePublishLinkImage.CssClass = "DimIcon";
                    coursePublishLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                    coursePublishLinkImage.AlternateText = _GlobalResources.NonPublished;

                    coursePublishLink.Controls.Add(coursePublishLinkImage);

                    coursePublishUnPublishSpan.Controls.Add(coursePublishLink);
                }

                // open/close
                if (isClosed)
                {
                    Label courseOpenCloseSpan = new Label();
                    courseOpenCloseSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(courseOpenCloseSpan);

                    LinkButton courseOpenLink = new LinkButton();
                    courseOpenLink.ID = "CourseOpenLink_" + idCourse.ToString();
                    courseOpenLink.OnClientClick = "LoadOpenCourseModalContent(" + idCourse + "); return false;";
                    courseOpenLink.ToolTip = _GlobalResources.OpenCourse;

                    Image courseOpenLinkImage = new Image();
                    courseOpenLinkImage.ID = "CourseOpenLinkImage_" + idCourse.ToString();
                    courseOpenLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED, ImageFiles.EXT_PNG);
                    courseOpenLinkImage.AlternateText = _GlobalResources.Closed;

                    courseOpenLink.Controls.Add(courseOpenLinkImage);

                    courseOpenCloseSpan.Controls.Add(courseOpenLink);
                }
                else
                {
                    Label courseOpenCloseSpan = new Label();
                    courseOpenCloseSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(courseOpenCloseSpan);

                    LinkButton courseCloseLink = new LinkButton();
                    courseCloseLink.ID = "CourseCloseLink_" + idCourse.ToString();
                    courseCloseLink.OnClientClick = "LoadCloseCourseModalContent(" + idCourse + "); return false;";
                    courseCloseLink.ToolTip = _GlobalResources.CloseCourse;

                    Image courseCloseLinkImage = new Image();
                    courseCloseLinkImage.ID = "CourseCloseLinkImage_" + idCourse.ToString();
                    courseCloseLinkImage.CssClass = "DimIcon";
                    courseCloseLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED, ImageFiles.EXT_PNG);
                    courseCloseLinkImage.AlternateText = _GlobalResources.Open;

                    courseCloseLink.Controls.Add(courseCloseLinkImage);

                    courseOpenCloseSpan.Controls.Add(courseCloseLink);
                }

                // show modify link if the user has course content manager permissions
                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseContentManager))
                {
                    // modify
                    Label modifySpan = new Label();
                    modifySpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(modifySpan);

                    HyperLink modifyLink = new HyperLink();
                    modifyLink.NavigateUrl = "Modify.aspx?id=" + idCourse.ToString();
                    modifySpan.Controls.Add(modifyLink);

                    Image modifyIcon = new Image();
                    modifyIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
                    modifyIcon.AlternateText = _GlobalResources.Modify;
                    modifyIcon.ToolTip = _GlobalResources.CourseProperties;
                    modifyLink.Controls.Add(modifyIcon);
                }

                // show enrollments link if the user has course enrollment manager permissions
                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseEnrollmentManager)){
                    // enrollments
                    Label enrollmentsSpan = new Label();
                    enrollmentsSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(enrollmentsSpan);

                    HyperLink enrollmentsLink = new HyperLink();
                    enrollmentsLink.NavigateUrl = "rulesetenrollments/Default.aspx?cid=" + idCourse.ToString();
                    enrollmentsSpan.Controls.Add(enrollmentsLink);

                    Image enrollmentsIcon = new Image();
                    enrollmentsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_BUTTON, ImageFiles.EXT_PNG);
                    enrollmentsIcon.AlternateText = _GlobalResources.Enrollments;
                    enrollmentsIcon.ToolTip = _GlobalResources.Enrollments;
                    enrollmentsLink.Controls.Add(enrollmentsIcon);
                }
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedCourse_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedCourse_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseCourse_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.CourseGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.CourseGrid.Rows[i].FindControl(this.CourseGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.Course.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedCourse_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoCourse_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }            
            finally
            {
                // rebind the grid
                this.CourseGrid.BindData();
            }
        }
        #endregion

        #region _BuildUnpublishCourseConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the unpublish action performed on a course.
        /// </summary>
        private void _BuildUnpublishCourseConfirmationModal()
        {
            // set modal properties
            this._UnpublishCourseConfirmationModal = new ModalPopup("UnpublishCourseConfirmationModal");
            this._UnpublishCourseConfirmationModal.Type = ModalPopupType.Form;
            this._UnpublishCourseConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
            this._UnpublishCourseConfirmationModal.HeaderIconAlt = _GlobalResources.UnpublishCourse;
            this._UnpublishCourseConfirmationModal.HeaderText = _GlobalResources.UnpublishCourse;
            this._UnpublishCourseConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._UnpublishCourseConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._UnpublishCourseConfirmationModal.TargetControlID = this._UnpublishCourseConfirmationModalLaunchButton.ID;
            this._UnpublishCourseConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._UnpublishCourseSubmit_Command);

            // build the modal form panel
            Panel unpublishCourseConfirmationModalFormPanel = new Panel();
            unpublishCourseConfirmationModalFormPanel.ID = "UnpublishCourseConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmUnpublishActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToUnpublishThisCourse;

            body1Wrapper.Controls.Add(body1);
            unpublishCourseConfirmationModalFormPanel.Controls.Add(body1Wrapper);            

            // build the unpublish course data hidden field
            HiddenField courseUnpublishData = new HiddenField();
            courseUnpublishData.ID = "CourseUnpublishData";

            // build the modal body
            this._UnpublishCourseConfirmationModal.AddControlToBody(unpublishCourseConfirmationModalFormPanel);
            this._UnpublishCourseConfirmationModal.AddControlToBody(this._UnpublishCourseConfirmationModalLoadButton);
            this._UnpublishCourseConfirmationModal.AddControlToBody(courseUnpublishData);

            // add modal to container
            this.CoursesFormContentWrapperContainer.Controls.Add(this._UnpublishCourseConfirmationModal);
        }
        #endregion

        #region _LoadUnpublishCourseConfirmationModalContent
        /// <summary>
        /// Loads content for course unpublish confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadUnpublishCourseConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel unpublishCourseConfirmationModalFormPanel = (Panel)this._UnpublishCourseConfirmationModal.FindControl("UnpublishCourseConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._UnpublishCourseConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = true;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._UnpublishCourseConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._UnpublishCourseConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._UnpublishCourseConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._UnpublishCourseConfirmationModal.DisplayFeedback(dEx.Message, true);
                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._UnpublishCourseConfirmationModal.DisplayFeedback(ex.Message, true);
                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _UnpublishCourseSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the unpublish course confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _UnpublishCourseSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField courseUnpublishData = (HiddenField)this._UnpublishCourseConfirmationModal.FindControl("CourseUnpublishData");
            Panel unpublishCourseConfirmationModalFormPanel = (Panel)this._UnpublishCourseConfirmationModal.FindControl("UnpublishCourseConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._UnpublishCourseConfirmationModal.ClearFeedback();

                // get the course id
                int idCourse = Convert.ToInt32(courseUnpublishData.Value);                

                // unpublish the course
                Course courseObject = new Course(idCourse);
                courseObject.IsPublished = false;
                courseObject.Save();

                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
                this._UnpublishCourseConfirmationModal.DisplayFeedback(_GlobalResources.TheCourseHasBeenUnpublishedSuccessfully, false);

                // rebind the course grid and update
                this.CourseGrid.BindData();
                this.CourseGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._UnpublishCourseConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._UnpublishCourseConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._UnpublishCourseConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._UnpublishCourseConfirmationModal.DisplayFeedback(dEx.Message, true);
                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._UnpublishCourseConfirmationModal.DisplayFeedback(ex.Message, true);
                unpublishCourseConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishCourseConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildPublishCourseConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the publish action performed on a course.
        /// </summary>
        private void _BuildPublishCourseConfirmationModal()
        {
            // set modal properties
            this._PublishCourseConfirmationModal = new ModalPopup("PublishCourseConfirmationModal");
            this._PublishCourseConfirmationModal.Type = ModalPopupType.Form;
            this._PublishCourseConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
            this._PublishCourseConfirmationModal.HeaderIconAlt = _GlobalResources.PublishCourse;
            this._PublishCourseConfirmationModal.HeaderText = _GlobalResources.PublishCourse;
            this._PublishCourseConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._PublishCourseConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._PublishCourseConfirmationModal.TargetControlID = this._PublishCourseConfirmationModalLaunchButton.ID;
            this._PublishCourseConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._PublishCourseSubmit_Command);

            // build the modal form panel
            Panel publishCourseConfirmationModalFormPanel = new Panel();
            publishCourseConfirmationModalFormPanel.ID = "PublishCourseConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmPublishActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToPublishThisCourse;

            body1Wrapper.Controls.Add(body1);
            publishCourseConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the publish course data hidden field
            HiddenField coursePublishData = new HiddenField();
            coursePublishData.ID = "CoursePublishData";

            // build the modal body
            this._PublishCourseConfirmationModal.AddControlToBody(publishCourseConfirmationModalFormPanel);
            this._PublishCourseConfirmationModal.AddControlToBody(this._PublishCourseConfirmationModalLoadButton);
            this._PublishCourseConfirmationModal.AddControlToBody(coursePublishData);

            // add modal to container
            this.CoursesFormContentWrapperContainer.Controls.Add(this._PublishCourseConfirmationModal);
        }
        #endregion

        #region _LoadPublishCourseConfirmationModalContent
        /// <summary>
        /// Loads content for course publish confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadPublishCourseConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel publishCourseConfirmationModalFormPanel = (Panel)this._PublishCourseConfirmationModal.FindControl("PublishCourseConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._PublishCourseConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._PublishCourseConfirmationModal.SubmitButton.Visible = true;
                this._PublishCourseConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._PublishCourseConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._PublishCourseConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._PublishCourseConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._PublishCourseConfirmationModal.DisplayFeedback(dEx.Message, true);
                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._PublishCourseConfirmationModal.DisplayFeedback(ex.Message, true);
                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _PublishCourseSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the publish course confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _PublishCourseSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField coursePublishData = (HiddenField)this._PublishCourseConfirmationModal.FindControl("CoursePublishData");
            Panel publishCourseConfirmationModalFormPanel = (Panel)this._PublishCourseConfirmationModal.FindControl("PublishCourseConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._PublishCourseConfirmationModal.ClearFeedback();

                // get the course id
                int idCourse = Convert.ToInt32(coursePublishData.Value);

                // publish the course
                Course courseObject = new Course(idCourse);
                courseObject.IsPublished = true;
                courseObject.Save();

                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
                this._PublishCourseConfirmationModal.DisplayFeedback(_GlobalResources.TheCourseHasBeenPublishedSuccessfully, false);

                // rebind the course grid and update
                this.CourseGrid.BindData();
                this.CourseGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._PublishCourseConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._PublishCourseConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._PublishCourseConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._PublishCourseConfirmationModal.DisplayFeedback(dEx.Message, true);
                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._PublishCourseConfirmationModal.DisplayFeedback(ex.Message, true);
                publishCourseConfirmationModalFormPanel.Controls.Clear();
                this._PublishCourseConfirmationModal.SubmitButton.Visible = false;
                this._PublishCourseConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildOpenCourseConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the open action performed on a course.
        /// </summary>
        private void _BuildOpenCourseConfirmationModal()
        {
            // set modal properties
            this._OpenCourseConfirmationModal = new ModalPopup("OpenCourseConfirmationModal");
            this._OpenCourseConfirmationModal.Type = ModalPopupType.Form;
            this._OpenCourseConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_OPEN, ImageFiles.EXT_PNG);
            this._OpenCourseConfirmationModal.HeaderIconAlt = _GlobalResources.OpenCourse;
            this._OpenCourseConfirmationModal.HeaderText = _GlobalResources.OpenCourse;
            this._OpenCourseConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._OpenCourseConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._OpenCourseConfirmationModal.TargetControlID = this._OpenCourseConfirmationModalLaunchButton.ID;
            this._OpenCourseConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._OpenCourseSubmit_Command);

            // build the modal form panel
            Panel openCourseConfirmationModalFormPanel = new Panel();
            openCourseConfirmationModalFormPanel.ID = "OpenCourseConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmOpenActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToOpenThisCourse;

            body1Wrapper.Controls.Add(body1);
            openCourseConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the open course data hidden field
            HiddenField courseOpenData = new HiddenField();
            courseOpenData.ID = "CourseOpenData";

            // build the modal body
            this._OpenCourseConfirmationModal.AddControlToBody(openCourseConfirmationModalFormPanel);
            this._OpenCourseConfirmationModal.AddControlToBody(this._OpenCourseConfirmationModalLoadButton);
            this._OpenCourseConfirmationModal.AddControlToBody(courseOpenData);

            // add modal to container
            this.CoursesFormContentWrapperContainer.Controls.Add(this._OpenCourseConfirmationModal);
        }
        #endregion

        #region _LoadOpenCourseConfirmationModalContent
        /// <summary>
        /// Loads content for course open confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadOpenCourseConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel openCourseConfirmationModalFormPanel = (Panel)this._OpenCourseConfirmationModal.FindControl("OpenCourseConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._OpenCourseConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._OpenCourseConfirmationModal.SubmitButton.Visible = true;
                this._OpenCourseConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._OpenCourseConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._OpenCourseConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._OpenCourseConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._OpenCourseConfirmationModal.DisplayFeedback(dEx.Message, true);
                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._OpenCourseConfirmationModal.DisplayFeedback(ex.Message, true);
                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _OpenCourseSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the open course confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _OpenCourseSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField courseOpenData = (HiddenField)this._OpenCourseConfirmationModal.FindControl("CourseOpenData");
            Panel openCourseConfirmationModalFormPanel = (Panel)this._OpenCourseConfirmationModal.FindControl("OpenCourseConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._OpenCourseConfirmationModal.ClearFeedback();

                // get the course id
                int idCourse = Convert.ToInt32(courseOpenData.Value);

                // open the course
                Course courseObject = new Course(idCourse);
                courseObject.IsClosed = false;
                courseObject.Save();

                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
                this._OpenCourseConfirmationModal.DisplayFeedback(_GlobalResources.TheCourseHasBeenOpenedSuccessfully, false);

                // rebind the course grid and update
                this.CourseGrid.BindData();
                this.CourseGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._OpenCourseConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._OpenCourseConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._OpenCourseConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._OpenCourseConfirmationModal.DisplayFeedback(dEx.Message, true);
                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._OpenCourseConfirmationModal.DisplayFeedback(ex.Message, true);
                openCourseConfirmationModalFormPanel.Controls.Clear();
                this._OpenCourseConfirmationModal.SubmitButton.Visible = false;
                this._OpenCourseConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildCloseCourseConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the close action performed on a course.
        /// </summary>
        private void _BuildCloseCourseConfirmationModal()
        {
            // set modal properties
            this._CloseCourseConfirmationModal = new ModalPopup("CloseCourseConfirmationModal");
            this._CloseCourseConfirmationModal.Type = ModalPopupType.Form;
            this._CloseCourseConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED, ImageFiles.EXT_PNG);
            this._CloseCourseConfirmationModal.HeaderIconAlt = _GlobalResources.CloseCourse;
            this._CloseCourseConfirmationModal.HeaderText = _GlobalResources.CloseCourse;
            this._CloseCourseConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._CloseCourseConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._CloseCourseConfirmationModal.TargetControlID = this._CloseCourseConfirmationModalLaunchButton.ID;
            this._CloseCourseConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._CloseCourseSubmit_Command);

            // build the modal form panel
            Panel closeCourseConfirmationModalFormPanel = new Panel();
            closeCourseConfirmationModalFormPanel.ID = "CloseCourseConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmCloseActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToCloseThisCourse;

            body1Wrapper.Controls.Add(body1);
            closeCourseConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the close course data hidden field
            HiddenField courseCloseData = new HiddenField();
            courseCloseData.ID = "CourseCloseData";

            // build the modal body
            this._CloseCourseConfirmationModal.AddControlToBody(closeCourseConfirmationModalFormPanel);
            this._CloseCourseConfirmationModal.AddControlToBody(this._CloseCourseConfirmationModalLoadButton);
            this._CloseCourseConfirmationModal.AddControlToBody(courseCloseData);

            // add modal to container
            this.CoursesFormContentWrapperContainer.Controls.Add(this._CloseCourseConfirmationModal);
        }
        #endregion

        #region _LoadCloseCourseConfirmationModalContent
        /// <summary>
        /// Loads content for course close confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadCloseCourseConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel closeCourseConfirmationModalFormPanel = (Panel)this._CloseCourseConfirmationModal.FindControl("CloseCourseConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._CloseCourseConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._CloseCourseConfirmationModal.SubmitButton.Visible = true;
                this._CloseCourseConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CloseCourseConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CloseCourseConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CloseCourseConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CloseCourseConfirmationModal.DisplayFeedback(dEx.Message, true);
                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CloseCourseConfirmationModal.DisplayFeedback(ex.Message, true);
                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _CloseCourseSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the close course confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CloseCourseSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField courseCloseData = (HiddenField)this._CloseCourseConfirmationModal.FindControl("CourseCloseData");
            Panel closeCourseConfirmationModalFormPanel = (Panel)this._CloseCourseConfirmationModal.FindControl("CloseCourseConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._CloseCourseConfirmationModal.ClearFeedback();

                // get the course id
                int idCourse = Convert.ToInt32(courseCloseData.Value);

                // close the course
                Course courseObject = new Course(idCourse);
                courseObject.IsClosed = true;
                courseObject.Save();

                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
                this._CloseCourseConfirmationModal.DisplayFeedback(_GlobalResources.TheCourseHasBeenClosedSuccessfully, false);

                // rebind the course grid and update
                this.CourseGrid.BindData();
                this.CourseGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CloseCourseConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CloseCourseConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CloseCourseConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CloseCourseConfirmationModal.DisplayFeedback(dEx.Message, true);
                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CloseCourseConfirmationModal.DisplayFeedback(ex.Message, true);
                closeCourseConfirmationModalFormPanel.Controls.Clear();
                this._CloseCourseConfirmationModal.SubmitButton.Visible = false;
                this._CloseCourseConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion
    }
}
