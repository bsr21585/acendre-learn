﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Courses
{    
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CoursePropertiesFormContentWrapperContainer;
        public Panel CourseObjectMenuContainer;
        public Panel CoursePropertiesWrapperContainer;
        public Panel CoursePropertiesInstructionsPanel;
        public Panel CoursePropertiesFeedbackContainer;
        public Panel CoursePropertiesContainer;
        public Panel CoursePropertiesActionsPanel;
        public Panel CoursePropertiesTabPanelsContainer;
        #endregion

        #region Private Properties
        private HiddenField _PageAction;

        // COURSE MODIFY

        private Course _CourseObject;
        private EcommerceSettings _EcommerceSettings;

        private UploaderAsync _CourseAvatar;
        private UploaderAsync _CourseSampleScreenUploader;

        private TextBox _CourseCode;
        private TextBox _CourseTitle;
        private TextBox _CourseEstimatedTimeHours;
        private TextBox _CourseEstimatedTimeMinutes;
        private TextBox _CourseShortDescription;
        private TextBox _CourseLongDescription;
        private TextBox _CourseObjectives;
        private TextBox _CourseRevCode;
        private TextBox _CourseSearchTags;

        private CheckBox _EnableRatings;
        private UserRating _CourseRatingControl;

        private CheckBox _IsWallModerated;
        private CheckBox _IsWallOpenView;
        private CheckBox _CourseForceLessonCompletionInOrder;
        private CheckBox _CourseRequireFirstLessonToBeCompletedBeforeOthers;
        private CheckBox _CourseLockLastLessonUntilOthersCompleted;
        private CheckBox _SelfEnrollmentIsOneTimeOnly;
        private CheckBox _IsSelfEnrollmentApprovalRequired;
        private CheckBox _IsApprovalAllowedByCourseExperts;
        private CheckBox _IsApprovalAllowedByUserSupervisor;
        
        private RadioButtonList _CoursePublished;
        private RadioButtonList _CourseClosed;
        private RadioButtonList _CourseLocked;        
        private RadioButtonList _CourseWall;
        private RadioButtonList _CoursePrerequisitesIsAny;

        private HiddenField _CourseSocialMediaItemCount;
        private HiddenField _CourseSocialMediaItemsJSON;
        private HiddenField _SelectedCourseExperts;
        private HiddenField _SelectedEnrollmentApprovers;
        private HiddenField _SelectedCoursePrerequisites;
        private HiddenField _UploadedCourseSampleScreens;
        private HiddenField _ClearAvatar;

        private TextBox _CourseCost;
        private TextBox _CourseCredits;

        private DateIntervalSelector _SelfEnrollmentDue;
        private DateIntervalSelector _SelfEnrollmentExpiresFromStart;
        private DateIntervalSelector _SelfEnrollmentExpiresFromFirstLaunch;

        private DataTable _CourseExperts;
        private DataTable _EligibleCourseExpertsForSelectList;
        private DataTable _EnrollmentApprovers;
        private DataTable _EligibleEnrollmentApproversForSelectList;
        private DataTable _CoursePrerequisites;        
        private DataTable _EligibleCoursePrerequisitesForSelectList;
        private DataTable _CourseSampleScreens;

        private DynamicListBox _SelectEligibleExpertUsers;
        private DynamicListBox _SelectEligibleApproverUsers;
        private DynamicListBox _SelectEligibleCoursePrerequisites;

        private Button _CoursePropertiesSaveButton;
        private Button _CoursePropertiesCancelButton;

        private Panel _CourseSocialMediaItemsContainer;
        private Panel _CourseExpertsListContainer;
        private Panel _EnrollmentApproversListContainer;
        private Panel _PrerequisitesListContainer;
        private Panel _SampleScreensListContainer;
        
        private ModalPopup _SelectPrerequisitesForCourse;
        private ModalPopup _SelectExpertsForCourse;
        private ModalPopup _SelectApproversForCourse;

        private Image _AvatarImage;

        private TextBox _Shortcode;

        // LESSON GRID

        private Grid _LessonGrid;
        private ModalPopup _LessonGridConfirmAction;
        private HiddenField _LessonOrderHiddenField;        
        private LinkButton _LessonGridDeleteButton;

        // COURSE MATERIALS GRID

        private Grid _CourseMaterialGrid;
        private ModalPopup _CourseMaterialGridConfirmAction;
        private LinkButton _CourseMaterialGridDeleteButton;

        private ModalPopup _CourseMaterialMakePrivateConfirmationModal;
        private Button _CourseMaterialMakePrivateConfirmationModalLaunchButton;
        private Button _CourseMaterialMakePrivateConfirmationModalLoadButton;

        private ModalPopup _CourseMaterialMakePublicConfirmationModal;
        private Button _CourseMaterialMakePublicConfirmationModalLaunchButton;
        private Button _CourseMaterialMakePublicConfirmationModalLoadButton;

        // LESSON MODIFY

        private Lesson _LessonObject;

        private ModalPopup _LessonModifyModal;
        private Button _LessonModifyModalLaunchButton;
        private Button _LessonModifyModalLoadButton;
        private Button _SelectContentPackageModalLaunchButton;
        private Button _SelectStandupTrainingModuleModalLaunchButton;
        private Button _SelectOJTContentPackageModalLaunchButton;
        private HiddenField _LessonModifyData;

        private TextBox _LessonTitle;
        private TextBox _LessonRevCode;
        private TextBox _LessonShortDescription;
        private TextBox _LessonLongDescription;
        private TextBox _LessonSearchTags;
        private CheckBox _LessonOptional;

        private Panel _LessonTaskUploadedResourceContainer;
        private Panel _LessonSelectedStandupTrainingModuleContainer;
        private Panel _LessonSelectedContentPackageContainer;
        private Panel _LessonSelectedOJTContentPackageContainer;

        private CheckBox _OverrideContentChangeLessonDataReset;
        private CheckBox _UseContentPackageCheckBox;
        private CheckBox _UseStandupTrainingModuleCheckBox;
        private CheckBox _UseTaskCheckBox;
        private CheckBox _UseOJTCheckBox;

        private ModalPopup _SelectContentPackageForLesson;
        private ModalPopup _SelectOJTContentPackageForLesson;
        private ModalPopup _SelectStandupTrainingModuleForLesson;

        private DataTable _OJTContentPackagesForSelectList;
        private DataTable _ContentPackagesForSelectList;
        private DataTable _StandupTrainingModulesForSelectList;

        private HiddenField _SelectedContentPackage;
        private HiddenField _SelectedOJTContentPackage;
        private HiddenField _SelectedStandupTrainingModule;

        private DynamicListBox _SelectContentPackage;
        private DynamicListBox _SelectOJTContentPackage;
        private DynamicListBox _SelectStandupTrainingModule;

        private CheckBox _OJTAllowSupervisorAsProctor;
        private CheckBox _OJTAllowCourseExpertAsProctor;
        private CheckBox _TaskAllowSupervisorAsProctor;
        private CheckBox _TaskAllowCourseExpertAsProctor;

        private RadioButtonList _TaskAllowedDocumentType;
        private UploaderAsync _TaskResourceUploader;

        private HiddenField _LessonOptionalData;
        private Button _LessonOptionalModalLaunchButton;
        private Button _LessonOptionalModalLoadButton;
        private ModalPopup _LessonOptionalModal;

        private HiddenField _LessonRequiredData;
        private Button _LessonRequiredModalLaunchButton;
        private Button _LessonRequiredModalLoadButton;
        private ModalPopup _LessonRequiredModal;

        // COURSE MATERIAL MODIFY

        private DocumentRepositoryItem _CourseMaterialObject;

        private ModalPopup _CourseMaterialModifyModal;
        private Button _CourseMaterialModifyModalLaunchButton;
        private Button _CourseMaterialModifyModalLoadButton;
        private HiddenField _CourseMaterialModifyData;
        
        private UploaderAsync _CourseMaterialUploader;
        private LanguageSelector _CourseMaterialLanguage;

        private TextBox _CourseMaterialLabel;
        private TextBox _CourseMaterialSearchTags;

        private CheckBox _CourseMaterialIsPrivate;
        private CheckBox _CourseMaterialIsAllLanguages;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.Courses.Modify.js");

            // build start up call for MCE and add to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine("");                        
            multipleStartUpCallsScript.AppendLine(" // set extended course wall property fields");
            multipleStartUpCallsScript.AppendLine(" if ($(\"#CourseWall_Field_1\").prop(\"checked\")) {");
            multipleStartUpCallsScript.AppendLine("     $(\"#IsCourseWallModerated_Field\").prop(\"checked\", false);");
            multipleStartUpCallsScript.AppendLine("     $(\"#IsCourseWallOpenView_Field\").prop(\"checked\", false);");
            multipleStartUpCallsScript.AppendLine("     $(\"#IsCourseWallModerated_Field\").prop(\"disabled\", true);");
            multipleStartUpCallsScript.AppendLine("     $(\"#IsCourseWallOpenView_Field\").prop(\"disabled\", true);");
            multipleStartUpCallsScript.AppendLine(" }");
            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine(" // set lesson ordering fields");
            multipleStartUpCallsScript.AppendLine(" if ($(\"#CourseForceLessonCompletionInOrder_Field\").prop(\"checked\")) {");
            multipleStartUpCallsScript.AppendLine("     $(\"#CourseRequireFirstLessonToBeCompletedBeforeOthers_Field\").prop(\"checked\", true);");
            multipleStartUpCallsScript.AppendLine("     $(\"#CourseRequireFirstLessonToBeCompletedBeforeOthers_Field\").prop(\"disabled\", true);");
            multipleStartUpCallsScript.AppendLine("     $(\"#CourseLockLastLessonUntilOthersCompleted_Field\").prop(\"checked\", true);");
            multipleStartUpCallsScript.AppendLine("     $(\"#CourseLockLastLessonUntilOthersCompleted_Field\").prop(\"disabled\", true);");
            multipleStartUpCallsScript.AppendLine(" }");
            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine(" // initialize sortable on lesson grid");
            multipleStartUpCallsScript.AppendLine(" InitializeSortableOnLessonGrid();");

            // javascript for page action querystring, i.e. New Module
            if (this._PageAction.Value == "NewModule" && !Page.IsPostBack)
            {
                multipleStartUpCallsScript.AppendLine("");
                multipleStartUpCallsScript.AppendLine(" if ($(\"#PageAction\").val() == \"NewModule\") {");
                multipleStartUpCallsScript.AppendLine("     $(\"#PageAction\").val(\"\");");
                multipleStartUpCallsScript.AppendLine("     Helper.ToggleTab('Modules', 'CourseProperties');");
                multipleStartUpCallsScript.AppendLine("     CoursePropertiesTabChange('Modules', 'CourseProperties');");
                multipleStartUpCallsScript.AppendLine("     LoadLessonModifyModalContent(0, false);");
                multipleStartUpCallsScript.AppendLine(" }");
            }
            else if (this._PageAction.Value == "NewCourseMaterial" && !Page.IsPostBack)
            {
                multipleStartUpCallsScript.AppendLine("");
                multipleStartUpCallsScript.AppendLine(" if ($(\"#PageAction\").val() == \"NewCourseMaterial\") {");
                multipleStartUpCallsScript.AppendLine("     $(\"#PageAction\").val(\"\");");
                multipleStartUpCallsScript.AppendLine("     Helper.ToggleTab('CourseMaterials', 'CourseProperties');");
                multipleStartUpCallsScript.AppendLine("     CoursePropertiesTabChange('CourseMaterials', 'CourseProperties');");
                multipleStartUpCallsScript.AppendLine("     LoadCourseMaterialModifyModalContent(0, false);");
                multipleStartUpCallsScript.AppendLine(" }");
            }
            else
            { }

            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine("});");
            multipleStartUpCallsScript.AppendLine("");            
            multipleStartUpCallsScript.AppendLine("var NoneSelectedText = \"" + _GlobalResources.None + "\";");
            multipleStartUpCallsScript.AppendLine("var RemoveImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);

            // build global JS variables for Social Media elements
            StringBuilder smGlobalJS = new StringBuilder();
            smGlobalJS.AppendLine("IsEnrolledLabelText = \"" + _GlobalResources.DisplayOnlyWhenUserEnrolledInCourse + "\";");
            smGlobalJS.AppendLine("DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            smGlobalJS.AppendLine("CustomForumImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_CUSTOMFORUM, ImageFiles.EXT_PNG) + "\";");
            smGlobalJS.AppendLine("FacebookImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_FACEBOOK, ImageFiles.EXT_PNG) + "\";");
            smGlobalJS.AppendLine("LinkedInImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_LINKEDIN, ImageFiles.EXT_PNG) + "\";");
            smGlobalJS.AppendLine("TwitterImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_TWITTER, ImageFiles.EXT_PNG) + "\";");
            smGlobalJS.AppendLine("GoogleImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_GOOGLE, ImageFiles.EXT_PNG) + "\";");
            smGlobalJS.AppendLine("YouTubeImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_YOUTUBE, ImageFiles.EXT_PNG) + "\";");

            csm.RegisterClientScriptBlock(typeof(Modify), "SocialMediaGlobalJS", smGlobalJS.ToString(), true);            

            base.OnPreRender(e);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseContentManager) &&
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseEnrollmentManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("UserRating.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/courses/Modify.css");

            // get the course object
            this._GetCourseObject();

            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();            
        }
        #endregion

        #region _GetCourseObject
        /// <summary>
        /// Gets a course object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCourseObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._CourseObject = new Course(id); }
                }
                catch
                { Response.Redirect("~/administrator/courses"); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // set container classes
            this.CoursePropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CoursePropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the course object menu if we're editing a course
            if (this._CourseObject != null)
            {
                CourseObjectMenu courseObjectMenu = new CourseObjectMenu(this._CourseObject);
                courseObjectMenu.SelectedItem = CourseObjectMenu.MenuObjectItem.CourseProperties;

                this.CourseObjectMenuContainer.Controls.Add(courseObjectMenu);
            }

            // build the course properties form
            this._BuildCoursePropertiesForm();

            // build the course properties form actions panel
            this._BuildCoursePropertiesActionsPanel();

            // build lesson and course material grid actions modals if there is a course object
            if (this._CourseObject != null)
            {
                // build the lesson grid actions modal
                this._BuildLessonGridActionsModal();

                // build the course material grid actions modal
                this._BuildCourseMaterialGridActionsModal();

                // instansiate course material make private controls
                this._CourseMaterialMakePrivateConfirmationModalLaunchButton = new Button();
                this._CourseMaterialMakePrivateConfirmationModalLaunchButton.ID = "CourseMaterialMakePrivateConfirmationModalLaunchButton";
                this._CourseMaterialMakePrivateConfirmationModalLaunchButton.Style.Add("display", "none");
                this.CoursePropertiesActionsPanel.Controls.Add(this._CourseMaterialMakePrivateConfirmationModalLaunchButton);

                this._CourseMaterialMakePrivateConfirmationModalLoadButton = new Button();
                this._CourseMaterialMakePrivateConfirmationModalLoadButton.ID = "CourseMaterialMakePrivateConfirmationModalLoadButton";
                this._CourseMaterialMakePrivateConfirmationModalLoadButton.Style.Add("display", "none");
                this._CourseMaterialMakePrivateConfirmationModalLoadButton.Click += this._LoadCourseMaterialMakePrivateConfirmationModalContent;

                // instansiate course material make public controls
                this._CourseMaterialMakePublicConfirmationModalLaunchButton = new Button();
                this._CourseMaterialMakePublicConfirmationModalLaunchButton.ID = "CourseMaterialMakePublicConfirmationModalLaunchButton";
                this._CourseMaterialMakePublicConfirmationModalLaunchButton.Style.Add("display", "none");
                this.CoursePropertiesActionsPanel.Controls.Add(this._CourseMaterialMakePublicConfirmationModalLaunchButton);

                this._CourseMaterialMakePublicConfirmationModalLoadButton = new Button();
                this._CourseMaterialMakePublicConfirmationModalLoadButton.ID = "CourseMaterialMakePublicConfirmationModalLoadButton";
                this._CourseMaterialMakePublicConfirmationModalLoadButton.Style.Add("display", "none");
                this._CourseMaterialMakePrivateConfirmationModalLoadButton.Click += this._LoadCourseMaterialMakePublicConfirmationModalContent;
               
                // build the confirmation modals for making course materials private/public
                this._BuildCourseMaterialMakePrivateConfirmationModal();
                this._BuildCourseMaterialMakePublicConfirmationModal();

                // instansiate lesson modify modal controls
                this._LessonModifyModalLaunchButton = new Button();
                this._LessonModifyModalLaunchButton.ID = "LessonModifyModalLaunchButton";
                this._LessonModifyModalLaunchButton.Style.Add("display", "none");
                this.CoursePropertiesActionsPanel.Controls.Add(this._LessonModifyModalLaunchButton);

                this._LessonModifyModalLoadButton = new Button();
                this._LessonModifyModalLoadButton.ID = "LessonModifyModalLoadButton";
                this._LessonModifyModalLoadButton.Style.Add("display", "none");
                this._LessonModifyModalLoadButton.Click += this._LoadLessonModifyModalContent;

                this._SelectContentPackageModalLaunchButton = new Button();
                this._SelectContentPackageModalLaunchButton.ID = "SelectContentPackageModalLaunchButton";
                this._SelectContentPackageModalLaunchButton.Style.Add("display", "none");
                this.CoursePropertiesActionsPanel.Controls.Add(this._SelectContentPackageModalLaunchButton);

                this._SelectStandupTrainingModuleModalLaunchButton = new Button();
                this._SelectStandupTrainingModuleModalLaunchButton.ID = "SelectStandupTrainingModuleModalLaunchButton";
                this._SelectStandupTrainingModuleModalLaunchButton.Style.Add("display", "none");
                this.CoursePropertiesActionsPanel.Controls.Add(this._SelectStandupTrainingModuleModalLaunchButton);

                this._SelectOJTContentPackageModalLaunchButton = new Button();
                this._SelectOJTContentPackageModalLaunchButton.ID = "SelectOJTContentPackageModalLaunchButton";
                this._SelectOJTContentPackageModalLaunchButton.Style.Add("display", "none");
                this.CoursePropertiesActionsPanel.Controls.Add(this._SelectOJTContentPackageModalLaunchButton);

                // build the lesson modify modal
                this._BuildLessonModifyModal();

                // instansiate lesson make optional controls
                this._LessonOptionalModalLaunchButton = new Button();
                this._LessonOptionalModalLaunchButton.ID = "LessonOptionalModalLaunchButton";
                this._LessonOptionalModalLaunchButton.Style.Add("display", "none");
                this.CoursePropertiesActionsPanel.Controls.Add(this._LessonOptionalModalLaunchButton);

                this._LessonOptionalModalLoadButton = new Button();
                this._LessonOptionalModalLoadButton.ID = "LessonOptionalModalLoadButton";
                this._LessonOptionalModalLoadButton.Style.Add("display", "none");
                this._LessonOptionalModalLoadButton.Click += this._SetLessonOptional;

                this._LessonRequiredModalLaunchButton = new Button();
                this._LessonRequiredModalLaunchButton.ID = "LessonRequiredModalLaunchButton";
                this._LessonRequiredModalLaunchButton.Style.Add("display", "none");
                this.CoursePropertiesActionsPanel.Controls.Add(this._LessonRequiredModalLaunchButton);

                this._LessonRequiredModalLoadButton = new Button();
                this._LessonRequiredModalLoadButton.ID = "LessonRequiredModalLoadButton";
                this._LessonRequiredModalLoadButton.Style.Add("display", "none");
                this._LessonRequiredModalLoadButton.Click += this._SetLessonRequired;

                // build the confirmation modals for making lesson optional / required
                this._BuildLessonOptionalModal();
                this._BuildLessonRequiredModal();

                // instansiate course material modify modal controls
                this._CourseMaterialModifyModalLaunchButton = new Button();
                this._CourseMaterialModifyModalLaunchButton.ID = "CourseMaterialModifyModalLaunchButton";
                this._CourseMaterialModifyModalLaunchButton.Style.Add("display", "none");
                this.CoursePropertiesActionsPanel.Controls.Add(this._CourseMaterialModifyModalLaunchButton);

                this._CourseMaterialModifyModalLoadButton = new Button();
                this._CourseMaterialModifyModalLoadButton.ID = "CourseMaterialModifyModalLoadButton";
                this._CourseMaterialModifyModalLoadButton.Style.Add("display", "none");
                this._CourseMaterialModifyModalLoadButton.Click += this._LoadCourseMaterialModifyModalContent;

                // build the course material modify modal
                this._BuildCourseMaterialModifyModal();
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string courseImagePath;
            string imageCssClass = null;
            string pageTitle;

            if (this._CourseObject != null)
            {
                string courseTitleInInterfaceLanguage = this._CourseObject.Title;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (Course.LanguageSpecificProperty courseLanguageSpecificProperty in this._CourseObject.LanguageSpecificProperties)
                    {
                        if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { courseTitleInInterfaceLanguage = courseLanguageSpecificProperty.Title; }
                    }
                }                

                if (this._CourseObject.Avatar != null)
                {
                    courseImagePath = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + this._CourseObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    imageCssClass = "AvatarImage";
                }
                else
                {
                    courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
                }

                breadCrumbPageTitle = courseTitleInInterfaceLanguage;
                pageTitle = courseTitleInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewCourse;
                courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
                pageTitle = _GlobalResources.NewCourse;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Courses, "/administrator/courses"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, pageTitle, courseImagePath, imageCssClass);
        }
        #endregion

        #region _BuildCoursePropertiesForm
        /// <summary>
        /// Builds the Course Properties form.
        /// </summary>
        private void _BuildCoursePropertiesForm()
        {            
            // clear controls from container
            this.CoursePropertiesContainer.Controls.Clear();

            // build the course properties form tabs
            this._BuildCoursePropertiesFormTabs();

            // add lesson link container
            Panel addLessonLinkContainer = new Panel();
            addLessonLinkContainer.ID = "AddLessonLinkContainer";
            addLessonLinkContainer.CssClass = "ObjectOptionsPanel";
            addLessonLinkContainer.Style.Add("display", "none");

            // NEW LESSON
            addLessonLinkContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddLessonLink",
                                                null,
                                                "javascript: void(0);",
                                                "LoadLessonModifyModalContent(0, true);",
                                                _GlobalResources.NewModule,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_LESSON, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.CoursePropertiesContainer.Controls.Add(addLessonLinkContainer);

            // add course material link container
            Panel addCourseMaterialLinkContainer = new Panel();
            addCourseMaterialLinkContainer.ID = "AddCourseMaterialLinkContainer";
            addCourseMaterialLinkContainer.CssClass = "ObjectOptionsPanel";
            addCourseMaterialLinkContainer.Style.Add("display", "none");

            // NEW COURSE MATERIAL
            addCourseMaterialLinkContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddCourseMaterialLink",
                                                null,
                                                "javascript: void(0);",
                                                "LoadCourseMaterialModifyModalContent(0, true);",
                                                _GlobalResources.NewCourseMaterial,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.CoursePropertiesContainer.Controls.Add(addCourseMaterialLinkContainer);

            // form instructions panel - "Properties" is the first tab so do not hide this
            Panel formInstructionsPanel = new Panel();
            formInstructionsPanel.ID = "CourseFormInstructionsPanel";
            this.FormatPageInformationPanel(formInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisCourse, true);
            this.CoursePropertiesContainer.Controls.Add(formInstructionsPanel);

            // modules instructions panel
            Panel modulesInstructionsPanel = new Panel();
            modulesInstructionsPanel.ID = "CourseModulesInstructionsPanel";
            modulesInstructionsPanel.Style.Add("display", "none");
            this.FormatPageInformationPanel(modulesInstructionsPanel, _GlobalResources.YouCanSetModuleOrderingByDraggingModuleRowsInTheTableBelow, true);
            this.CoursePropertiesContainer.Controls.Add(modulesInstructionsPanel);

            // tab panels container
            this.CoursePropertiesTabPanelsContainer = new Panel();
            this.CoursePropertiesTabPanelsContainer.ID = "CourseProperties_TabPanelsContainer";
            this.CoursePropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.CoursePropertiesContainer.Controls.Add(this.CoursePropertiesTabPanelsContainer);

            // build the "properties" panel of the course properties form
            this._BuildCoursePropertiesFormPropertiesPanel();

            // build the "settings" panel of the course properties form
            this._BuildCoursePropertiesFormSettingsPanel();

            // build the "personnel" panel of the course properties form
            this._BuildCoursePropertiesFormPersonnelPanel();

            if (this._CourseObject != null)
            {
                // build the "modules" panel of the course properties form
                this._BuildCoursePropertiesFormModulesPanel();                

                // build the "course materials" panel of the course properties form
                this._BuildCoursePropertiesFormCourseMaterialsPanel();
            }
            
            // build the "sample screens" panel of the course properties form
            this._BuildCoursePropertiesFormSampleScreensPanel();

            // populate the form input elements
            this._PopulateCoursePropertiesInputElements();
        }
        #endregion

        #region _BuildCoursePropertiesFormTabs
        /// <summary>
        /// Builds the tabs of course properties form.
        /// </summary>
        private void _BuildCoursePropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("Settings", _GlobalResources.Settings));
            tabs.Enqueue(new KeyValuePair<string, string>("Personnel", _GlobalResources.Personnel));

            if (this._CourseObject != null)
            {
                tabs.Enqueue(new KeyValuePair<string, string>("Modules", _GlobalResources.Modules));
                tabs.Enqueue(new KeyValuePair<string, string>("CourseMaterials", _GlobalResources.CourseMaterials));
            }

            tabs.Enqueue(new KeyValuePair<string, string>("SampleScreens", _GlobalResources.SampleScreens));

            // build and attach the tabs
            this.CoursePropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("CourseProperties", tabs, null, this.Page, "CoursePropertiesTabChange"));            
        }
        #endregion

        #region _BuildCoursePropertiesFormPropertiesPanel
        /// <summary>
        /// Builds the course properties form fields under property tab.
        /// </summary>
        private void _BuildCoursePropertiesFormPropertiesPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "CourseProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.CssClass = "TabPanelContainer";
            propertiesPanel.Attributes.Add("style", "display: block;");            

            // course revcode field
            this._CourseRevCode = new TextBox();
            this._CourseRevCode.ID = "CourseRevCode_Field";
            this._CourseRevCode.CssClass = "InputLong";
            this._CourseRevCode.Enabled = false; // read-only

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseRevCode",
                                                             _GlobalResources.RevisionCode,
                                                             this._CourseRevCode.ID,
                                                             this._CourseRevCode,
                                                             false,
                                                             true,
                                                             false));            

            // course code field
            this._CourseCode = new TextBox();
            this._CourseCode.ID = "CourseCode_Field";
            this._CourseCode.CssClass = "InputShort";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseCode",
                                                             _GlobalResources.Code,
                                                             this._CourseCode.ID,
                                                             this._CourseCode,
                                                             false,
                                                             true,
                                                             false));

            // course title field
            this._CourseTitle = new TextBox();
            this._CourseTitle.ID = "CourseTitle_Field";
            this._CourseTitle.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseTitle",
                                                             _GlobalResources.Title,
                                                             this._CourseTitle.ID,
                                                             this._CourseTitle,
                                                             true,
                                                             true,
                                                             true));

            // catalog link shortcode
            List<Control> shortcodeInputControls = new List<Control>();

            this._Shortcode = new TextBox();
            this._Shortcode.ID = "CourseShortcode_Field";
            this._Shortcode.CssClass = "InputShort";
            this._Shortcode.MaxLength = 10;
            shortcodeInputControls.Add(this._Shortcode);

            if (this._CourseObject != null)
            {
                if (!String.IsNullOrWhiteSpace(this._CourseObject.Shortcode) && (bool)this._CourseObject.IsPublished)
                {
                    // build the shortcode link
                    Panel shortcodeUrlLinkContainer = new Panel();
                    shortcodeUrlLinkContainer.ID = "ShortcodeUrlLinkContainer";

                    string shortcodeUrl = String.Empty;

                    shortcodeUrl = "https://" + AsentiaSessionState.GlobalSiteObject.Hostname + "." + Config.AccountSettings.BaseDomain + "/catalog/?type=course&sc=" + this._CourseObject.Shortcode;

                    HyperLink shortcodeLink = new HyperLink();
                    shortcodeLink.ID = "ShortcodeLink";
                    shortcodeLink.NavigateUrl = shortcodeUrl;
                    shortcodeLink.Text = shortcodeUrl;
                    shortcodeLink.Target = "_blank";

                    shortcodeUrlLinkContainer.Controls.Add(shortcodeLink);
                    shortcodeInputControls.Add(shortcodeUrlLinkContainer);

                    // attach an onchange event to the shortcode text box so the link gets updated as it is being changed
                    this._Shortcode.Attributes.Add("onkeyup", "UpdateShortcodeLink(this.id);");
                }
            }

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseShortcode",
                                                                                 _GlobalResources.CatalogShortcode,
                                                                                 shortcodeInputControls,
                                                                                 false,
                                                                                 true));

            // course credits field
            this._CourseCredits = new TextBox();
            this._CourseCredits.ID = "CourseCredits_Field";
            this._CourseCredits.CssClass = "InputXShort";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseCredits",
                                                            _GlobalResources.Credits,
                                                            this._CourseCredits.ID,
                                                            this._CourseCredits,
                                                            false,
                                                            true,
                                                            false));

            // course cost field
            if (this._EcommerceSettings.IsEcommerceSet)
            {
                List<Control> courseCostInputControls = new List<Control>();

                Label courseCostSymbolLabel = new Label();
                courseCostSymbolLabel.Text = this._EcommerceSettings.CurrencySymbol + " ";
                courseCostInputControls.Add(courseCostSymbolLabel);

                this._CourseCost = new TextBox();
                this._CourseCost.ID = "CourseCost_Field";
                this._CourseCost.CssClass = "InputXShort";
                courseCostInputControls.Add(this._CourseCost);

                Label courseCostCodeLabel = new Label();
                courseCostCodeLabel.Text = " (" + this._EcommerceSettings.CurrencyCode + ") ";
                courseCostInputControls.Add(courseCostCodeLabel);

                propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseCost",
                                                                                     _GlobalResources.Cost,
                                                                                     courseCostInputControls,
                                                                                     false,
                                                                                     true));
            }            

            // course avatar field
            List<Control> avatarInputControls = new List<Control>();

            if (this._CourseObject != null)
            {
                if (this._CourseObject.Avatar != null)
                {
                    //course avatar image panel
                    Panel avatarImageContainer = new Panel();
                    avatarImageContainer.ID = "CourseAvatar_Field_ImageContainer";
                    avatarImageContainer.CssClass = "AvatarImageContainer";

                    // avatar image
                    this._AvatarImage = new Image();
                    avatarImageContainer.Controls.Add(this._AvatarImage);
                    avatarInputControls.Add(avatarImageContainer);

                    Panel avatarImageDeleteButtonContainer = new Panel();
                    avatarImageDeleteButtonContainer.ID = "AvatarImageDeleteButtonContainer";
                    avatarImageDeleteButtonContainer.CssClass = "AvatarDeleteButtonContainer";
                    avatarImageContainer.Controls.Add(avatarImageDeleteButtonContainer);

                    // delete course avatar image
                    Image deleteAvatarImage = new Image();
                    deleteAvatarImage.ID = "AvatarImageDeleteButton";
                    deleteAvatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                              ImageFiles.EXT_PNG);
                    deleteAvatarImage.CssClass = "SmallIcon";
                    deleteAvatarImage.Attributes.Add("onClick", "javascript:DeleteCourseAvatar();");
                    deleteAvatarImage.Style.Add("cursor", "pointer");

                    avatarImageDeleteButtonContainer.Controls.Add(deleteAvatarImage);

                    // clear avatar hidden field
                    this._ClearAvatar = new HiddenField();
                    this._ClearAvatar.ID = "ClearAvatar_Field";
                    this._ClearAvatar.Value = "false";
                    avatarInputControls.Add(this._ClearAvatar);
                }
            }
           
            // course avatar image upload
            this._CourseAvatar = new UploaderAsync("CourseAvatar_Field", UploadType.Avatar, "CourseAvatar_ErrorContainer");

            // set params to resize the course avatar upon upload
            this._CourseAvatar.ResizeOnUpload = true;
            this._CourseAvatar.ResizeMaxWidth = 256;
            this._CourseAvatar.ResizeMaxHeight = 256;

            avatarInputControls.Add(this._CourseAvatar);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseAvatar",
                                                                                 _GlobalResources.Avatar,
                                                                                 avatarInputControls,
                                                                                 false,
                                                                                 true));            

            // course estimated time field
            List<Control> courseEstimatedTimeInputControls = new List<Control>();

            this._CourseEstimatedTimeHours = new TextBox();
            this._CourseEstimatedTimeHours.ID = "CourseEstimatedTimeHours_Field";
            this._CourseEstimatedTimeHours.CssClass = "InputXShort";
            courseEstimatedTimeInputControls.Add(this._CourseEstimatedTimeHours);

            Label courseEstimatedTimeHoursFieldLabel = new Label();
            courseEstimatedTimeHoursFieldLabel.Text = " " + _GlobalResources.Hour_sAnd + " ";
            courseEstimatedTimeInputControls.Add(courseEstimatedTimeHoursFieldLabel);

            this._CourseEstimatedTimeMinutes = new TextBox();
            this._CourseEstimatedTimeMinutes.ID = "CourseEstimatedTimeMinutes_Field";
            this._CourseEstimatedTimeMinutes.CssClass = "InputXShort";
            courseEstimatedTimeInputControls.Add(this._CourseEstimatedTimeMinutes);

            Label courseEstimatedTimeMinutesFieldLabel = new Label();
            courseEstimatedTimeMinutesFieldLabel.Text = " " + _GlobalResources.Minute_s;
            courseEstimatedTimeInputControls.Add(courseEstimatedTimeMinutesFieldLabel);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseEstimatedTime",
                                                                                 _GlobalResources.EstimatedLength,
                                                                                 courseEstimatedTimeInputControls,
                                                                                 false,
                                                                                 true));            

            // course short description field
            this._CourseShortDescription = new TextBox();
            this._CourseShortDescription.ID = "CourseShortDescription_Field";
            this._CourseShortDescription.Style.Add("width", "98%");
            this._CourseShortDescription.TextMode = TextBoxMode.MultiLine;
            this._CourseShortDescription.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseShortDescription",
                                                             _GlobalResources.ShortDescription,
                                                             this._CourseShortDescription.ID,
                                                             this._CourseShortDescription,
                                                             true,
                                                             true,
                                                             true));

            // course long description field
            this._CourseLongDescription = new TextBox();
            this._CourseLongDescription.ID = "CourseLongDescription_Field";
            this._CourseLongDescription.CssClass = "ckeditor";
            this._CourseLongDescription.Style.Add("width", "98%");
            this._CourseLongDescription.TextMode = TextBoxMode.MultiLine;
            this._CourseLongDescription.Rows = 10;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseLongDescription",
                                                             _GlobalResources.Description,
                                                             this._CourseLongDescription.ID,
                                                             this._CourseLongDescription,
                                                             false,
                                                             true,
                                                             true));

            // course objectives field
            this._CourseObjectives = new TextBox();
            this._CourseObjectives.ID = "CourseObjectives_Field";
            this._CourseObjectives.CssClass = "ckeditor";
            this._CourseObjectives.TextMode = TextBoxMode.MultiLine;
            this._CourseObjectives.Rows = 10;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseObjectives",
                                                             _GlobalResources.Objectives,
                                                             this._CourseObjectives.ID,
                                                             this._CourseObjectives,
                                                             false,
                                                             true,
                                                             true));

            // course search tags field
            this._CourseSearchTags = new TextBox();
            this._CourseSearchTags.ID = "CourseSearchTags_Field";
            this._CourseSearchTags.Style.Add("width", "98%");
            this._CourseSearchTags.TextMode = TextBoxMode.MultiLine;
            this._CourseSearchTags.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseSearchTags",
                                                             _GlobalResources.SearchTags,
                                                             this._CourseSearchTags.ID,
                                                             this._CourseSearchTags,
                                                             false,
                                                             false,
                                                             true));

            // attach panel to container
            this.CoursePropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildCoursePropertiesFormSettingsPanel
        /// <summary>
        /// Builds the course properties form fields under setting tab.
        /// </summary>
        private void _BuildCoursePropertiesFormSettingsPanel()
        {
            Panel settingsPanel = new Panel();
            settingsPanel.ID = "CourseProperties_" + "Settings" + "_TabPanel";
            settingsPanel.CssClass = "TabPanelContainer";
            settingsPanel.Attributes.Add("style", "display: none;");

            // course ratings field
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RATINGS_COURSE_ENABLE))
            {
                List<Control> courseRatingsInputControls = new List<Control>();

                this._EnableRatings = new CheckBox();
                this._EnableRatings.ID = "CourseEnableRatings_Field";
                this._EnableRatings.Text = _GlobalResources.EnableRatingsForThisCourse;
                this._EnableRatings.Checked = true;
                courseRatingsInputControls.Add(this._EnableRatings);

                if (this._CourseObject != null)
                {
                    if (this._CourseObject.DisallowRating != true)
                    {
                        this._CourseRatingControl = new UserRating("CourseRatingControl");
                        this._CourseRatingControl.IdObject = this._CourseObject.Id;

                        if (this._CourseObject.Rating != null)
                        { this._CourseRatingControl.UsersRating = (double)this._CourseObject.Rating; }

                        if (this._CourseObject.Votes != null)
                        { this._CourseRatingControl.Votes = (int)this._CourseObject.Votes; }

                        this._CourseRatingControl.ShowUserRating = true;
                        this._CourseRatingControl.ResetUrl = "/_util/RatingService.asmx/ResetCourseRating";
                        courseRatingsInputControls.Add(this._CourseRatingControl);
                    }
                }

                settingsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseEnableRatings",
                                                                                   _GlobalResources.Rating,
                                                                                   courseRatingsInputControls,
                                                                                   false,
                                                                                   true));
            }

            // course wall field - make sure the feature is enabled
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE)) {
                List<Control> courseWallInputControls = new List<Control>();

                Panel courseWallFieldInputContainer = new Panel();
                courseWallFieldInputContainer.ID = "CourseWall_Field_Container";

                this._CourseWall = new RadioButtonList();
                this._CourseWall.ID = "CourseWall_Field";

                ListItem courseWallOn = new ListItem();
                courseWallOn.Text = _GlobalResources.On;
                courseWallOn.Value = "True";
                courseWallOn.Attributes.Add("onclick", "CourseWallOn();");

                ListItem courseWallOff = new ListItem();
                courseWallOff.Text = _GlobalResources.Off;
                courseWallOff.Value = "False";
                courseWallOff.Attributes.Add("onclick", "CourseWallOff();");

                this._CourseWall.Items.Add(courseWallOn);
                this._CourseWall.Items.Add(courseWallOff);
                this._CourseWall.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
                this._CourseWall.SelectedIndex = 0;
                courseWallFieldInputContainer.Controls.Add(this._CourseWall);
                courseWallInputControls.Add(courseWallFieldInputContainer);

                // is wall moderated field
                Panel courseWallIsModeratedFieldInputContainer = new Panel();
                courseWallIsModeratedFieldInputContainer.ID = "IsCourseWallModerated_Field_Container";

                this._IsWallModerated = new CheckBox();
                this._IsWallModerated.ID = "IsCourseWallModerated_Field";
                this._IsWallModerated.Text = _GlobalResources.IfThisIsCheckedDiscussionPostsForThisCourseWillBeModerated;
                courseWallIsModeratedFieldInputContainer.Controls.Add(this._IsWallModerated);
                courseWallInputControls.Add(courseWallIsModeratedFieldInputContainer);

                // is wall open view field
                Panel courseWallIsOpenViewFieldInputContainer = new Panel();
                courseWallIsOpenViewFieldInputContainer.ID = "IsCourseWallOpenView_Field_Container";

                this._IsWallOpenView = new CheckBox();
                this._IsWallOpenView.ID = "IsCourseWallOpenView_Field";
                this._IsWallOpenView.Text = _GlobalResources.AllowUsersWhoAreNotEnrolledInThisCourseToViewTheCourseDiscussion;
                courseWallIsOpenViewFieldInputContainer.Controls.Add(this._IsWallOpenView);
                courseWallInputControls.Add(courseWallIsOpenViewFieldInputContainer);

                settingsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseWall",
                                                                                _GlobalResources.Discussion,
                                                                                courseWallInputControls,
                                                                                false,
                                                                                true));
            }

            // course force lessons to be completed in order
            // module ordering fields
            List<Control> courseLessonOrderingInputControls = new List<Control>();

            this._CourseForceLessonCompletionInOrder = new CheckBox();
            this._CourseForceLessonCompletionInOrder.ID = "CourseForceLessonCompletionInOrder_Field";
            this._CourseForceLessonCompletionInOrder.Text = _GlobalResources.RequireAllModulesToBeCompletedInOrder;
            this._CourseForceLessonCompletionInOrder.Attributes.Add("onclick", "ToggleAllModulesInOrderSetting();");
            courseLessonOrderingInputControls.Add(this._CourseForceLessonCompletionInOrder);

            this._CourseRequireFirstLessonToBeCompletedBeforeOthers = new CheckBox();
            this._CourseRequireFirstLessonToBeCompletedBeforeOthers.ID = "CourseRequireFirstLessonToBeCompletedBeforeOthers_Field";
            this._CourseRequireFirstLessonToBeCompletedBeforeOthers.Text = _GlobalResources.RequireTheFirstModuleToBeCompletedBeforeAnyOther;
            courseLessonOrderingInputControls.Add(this._CourseRequireFirstLessonToBeCompletedBeforeOthers);

            this._CourseLockLastLessonUntilOthersCompleted = new CheckBox();
            this._CourseLockLastLessonUntilOthersCompleted.ID = "CourseLockLastLessonUntilOthersCompleted_Field";
            this._CourseLockLastLessonUntilOthersCompleted.Text = _GlobalResources.LockTheLastModuleUntilAllPreviousModulesAreCompleted;
            courseLessonOrderingInputControls.Add(this._CourseLockLastLessonUntilOthersCompleted);
            
            settingsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseLessonOrdering",
                                                                                     _GlobalResources.ModuleOrdering,
                                                                                     courseLessonOrderingInputControls,
                                                                                     false,
                                                                                     true));            

            // course is published field
            this._CoursePublished = new RadioButtonList();
            this._CoursePublished.ID = "CoursePublished_Field";
            this._CoursePublished.Items.Add(new ListItem(_GlobalResources.Yes, "True"));
            this._CoursePublished.Items.Add(new ListItem(_GlobalResources.No, "False"));
            this._CoursePublished.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this._CoursePublished.SelectedIndex = 0;

            settingsPanel.Controls.Add(AsentiaPage.BuildFormField("CoursePublished",
                                                           _GlobalResources.Published,
                                                           this._CoursePublished.ID,
                                                           this._CoursePublished,
                                                           false,
                                                           true,
                                                           false));

            // course is closed field
            this._CourseClosed = new RadioButtonList();
            this._CourseClosed.ID = "CourseClosed_Field";
            this._CourseClosed.Items.Add(new ListItem(_GlobalResources.Yes, "True"));
            this._CourseClosed.Items.Add(new ListItem(_GlobalResources.No, "False"));
            this._CourseClosed.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this._CourseClosed.SelectedIndex = 1;

            settingsPanel.Controls.Add(AsentiaPage.BuildFormField("CourseClosed",
                                                           _GlobalResources.Closed,
                                                           this._CourseClosed.ID,
                                                           this._CourseClosed,
                                                           false,
                                                           true,
                                                           false));

            // course is locked field
            this._CourseLocked = new RadioButtonList();
            this._CourseLocked.ID = "CourseLocked_Field";
            this._CourseLocked.Items.Add(new ListItem(_GlobalResources.Yes, "True"));
            this._CourseLocked.Items.Add(new ListItem(_GlobalResources.No, "False"));
            this._CourseLocked.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this._CourseLocked.SelectedIndex = 1;

            settingsPanel.Controls.Add(AsentiaPage.BuildFormField("CourseLocked",
                                                           _GlobalResources.Locked,
                                                           this._CourseLocked.ID,
                                                           this._CourseLocked,
                                                           false,
                                                           true,
                                                           false));       
     
            // self-enrollment one-time only field
            this._SelfEnrollmentIsOneTimeOnly = new CheckBox();
            this._SelfEnrollmentIsOneTimeOnly.ID = "IsSelfEnrollmentOneTimeOnly_Field";
            this._SelfEnrollmentIsOneTimeOnly.Text = _GlobalResources.LearnersMaySelfEnrollInThisCourseOneTimeOnlyRegardlessOfAnyPreviousEnrollmentCompletionsOfThisCourse;

            settingsPanel.Controls.Add(AsentiaPage.BuildFormField("IsSelfEnrollmentOneTimeOnly",
                                               _GlobalResources.SelfEnrollmentOneTimeOnly,
                                               this._SelfEnrollmentIsOneTimeOnly.ID,
                                               this._SelfEnrollmentIsOneTimeOnly,
                                               false,
                                               true,
                                               false));


            // self-enrollment due container
            this._SelfEnrollmentDue = new DateIntervalSelector("SelfEnrollmentDue_Field", true);
            this._SelfEnrollmentDue.NoneCheckboxText = _GlobalResources.NoDueDate;
            this._SelfEnrollmentDue.NoneCheckBoxChecked = true;

            this._SelfEnrollmentDue.TextBeforeSelector = _GlobalResources.UserMustCompleteCourseWithin;
            this._SelfEnrollmentDue.TextAfterSelector = _GlobalResources.AfterSelfEnrollment;

            settingsPanel.Controls.Add(AsentiaPage.BuildFormField("SelfEnrollmentDue",
                                                           _GlobalResources.SelfEnrollmentDue,
                                                           this._SelfEnrollmentDue.ID,
                                                           this._SelfEnrollmentDue,
                                                           false,
                                                           true,
                                                           false));

            // self-enrollment expires from start container
            this._SelfEnrollmentExpiresFromStart = new DateIntervalSelector("SelfEnrollmentEnrollmentExpiresFromStart_Field", true);
            this._SelfEnrollmentExpiresFromStart.NoneCheckboxText = _GlobalResources.Indefinite;
            this._SelfEnrollmentExpiresFromStart.NoneCheckBoxChecked = true;

            this._SelfEnrollmentExpiresFromStart.TextBeforeSelector = _GlobalResources.LearnersHaveAccessToTheCourseFor;
            this._SelfEnrollmentExpiresFromStart.TextAfterSelector = _GlobalResources.AfterSelfEnrollment;

            settingsPanel.Controls.Add(AsentiaPage.BuildFormField("SelfEnrollmentEnrollmentExpiresFromStart",
                                                           _GlobalResources.SelfEnrollmentAccessFromStart,
                                                           this._SelfEnrollmentExpiresFromStart.ID,
                                                           this._SelfEnrollmentExpiresFromStart,
                                                           false,
                                                           true,
                                                           false));
            
            // self-enrollment expires from first launch container
            this._SelfEnrollmentExpiresFromFirstLaunch = new DateIntervalSelector("SelfEnrollmentEnrollmentExpiresFromFirstLaunch_Field", true);
            this._SelfEnrollmentExpiresFromFirstLaunch.NoneCheckboxText = _GlobalResources.Indefinite;
            this._SelfEnrollmentExpiresFromFirstLaunch.NoneCheckBoxChecked = true;

            this._SelfEnrollmentExpiresFromFirstLaunch.TextBeforeSelector = _GlobalResources.LearnersHaveAccessToTheCourseFor;
            this._SelfEnrollmentExpiresFromFirstLaunch.TextAfterSelector = _GlobalResources.AfterFirstLaunchOfCourseWhenSelfEnrolled;

            settingsPanel.Controls.Add(AsentiaPage.BuildFormField("SelfEnrollmentEnrollmentExpiresFromFirstLaunch",
                                                           _GlobalResources.SelfEnrollmentAccessFromFirstLaunch,
                                                           this._SelfEnrollmentExpiresFromFirstLaunch.ID,
                                                           this._SelfEnrollmentExpiresFromFirstLaunch,
                                                           false,
                                                           true,
                                                           false));

            // self-enrollment approval required field
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
            {
                this._IsSelfEnrollmentApprovalRequired = new CheckBox();
                this._IsSelfEnrollmentApprovalRequired.ID = "IsSelfEnrollmentApprovalRequired_Field";
                this._IsSelfEnrollmentApprovalRequired.Text = _GlobalResources.RequireSelfEnrolledEnrollmentsOfThisCourseToBeApprovedByAnAdministrator;

                settingsPanel.Controls.Add(AsentiaPage.BuildFormField("IsSelfEnrollmentApprovalRequired",
                                                               _GlobalResources.SelfEnrollmentApproval,
                                                               this._IsSelfEnrollmentApprovalRequired.ID,
                                                               this._IsSelfEnrollmentApprovalRequired,
                                                               false,
                                                               true,
                                                               false));
            }


            // course social media field
            List<Control> courseSocialMediaInputControls = new List<Control>();

            this._CourseSocialMediaItemsContainer = new Panel();
            this._CourseSocialMediaItemsContainer.ID = "CourseSocialMedia_ItemsContainer";
            courseSocialMediaInputControls.Add(this._CourseSocialMediaItemsContainer);

            // add social media link hyperlink
            HyperLink addSocialMediaImageLink = new HyperLink();
            addSocialMediaImageLink.ID = "CourseSocialMedia_AddLinkImage";
            addSocialMediaImageLink.CssClass = "ImageLink";
            addSocialMediaImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD,
                                                                      ImageFiles.EXT_PNG);
            addSocialMediaImageLink.NavigateUrl = "javascript:AddSocialMediaNode();";
            courseSocialMediaInputControls.Add(addSocialMediaImageLink);

            HyperLink addSocialMediaLink = new HyperLink();
            addSocialMediaLink.ID = "CourseSocialMedia_AddLinkText";
            addSocialMediaLink.Text = _GlobalResources.AddSocialMediaLink;
            addSocialMediaLink.NavigateUrl = "javascript:AddSocialMediaNode();";
            courseSocialMediaInputControls.Add(addSocialMediaLink);

            // course social media items count hidden field
            this._CourseSocialMediaItemCount = new HiddenField();
            this._CourseSocialMediaItemCount.ID = "CourseSocialMediaItemCount_Field";
            this._CourseSocialMediaItemCount.Value = "0";
            courseSocialMediaInputControls.Add(this._CourseSocialMediaItemCount);

            // course social media items JSON hidden field
            this._CourseSocialMediaItemsJSON = new HiddenField();
            this._CourseSocialMediaItemsJSON.ID = "CourseSocialMediaItemsJSON_Field";
            courseSocialMediaInputControls.Add(this._CourseSocialMediaItemsJSON);

            settingsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseSocialMedia",
                                                                               _GlobalResources.SocialMedia,
                                                                               courseSocialMediaInputControls,
                                                                               false,
                                                                               true));

            // course prerequisites field
            List<Control> coursePrerequisitesInputControls = new List<Control>();

            if (this._CourseObject != null)
            {
                this._CoursePrerequisites = this._CourseObject.GetPrerequisites(null);
                this._EligibleCoursePrerequisitesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCoursePrerequisiteSelectList(this._CourseObject.Id, null);
            }
            else
            {
                this._EligibleCoursePrerequisitesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCoursePrerequisiteSelectList(0, null);
            }            

            // course prerequisites is any inputs            
            this._CoursePrerequisitesIsAny = new RadioButtonList();
            this._CoursePrerequisitesIsAny.ID = "CoursePrerequisitesIsAny_Field";
            this._CoursePrerequisitesIsAny.Items.Add(new ListItem(_GlobalResources.AnySingleCourseListedBelow, "True"));
            this._CoursePrerequisitesIsAny.Items.Add(new ListItem(_GlobalResources.AllCoursesListedBelow, "False"));
            this._CoursePrerequisitesIsAny.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Vertical;
            this._CoursePrerequisitesIsAny.SelectedIndex = 0;

            // course prerequisites is any description
            Panel coursePrerequisitesIsAnyDescriptionContainer = new Panel();
            coursePrerequisitesIsAnyDescriptionContainer.ID = "CoursePrerequisitesIsAny_DescriptionContainer";

            Label coursePrerequisitesIsAnyFieldLabel = new Label();
            coursePrerequisitesIsAnyFieldLabel.Text = _GlobalResources.LearnersWillBeUnableToEnrollInThisCourseUntilTheyHaveCompleted + ": ";
            coursePrerequisitesIsAnyFieldLabel.AssociatedControlID = this._CoursePrerequisitesIsAny.ID;

            coursePrerequisitesInputControls.Add(coursePrerequisitesIsAnyFieldLabel);
            coursePrerequisitesInputControls.Add(this._CoursePrerequisitesIsAny);            

            // course prerequisites inputs            

            // course selected prerequisites hidden field
            this._SelectedCoursePrerequisites = new HiddenField();
            this._SelectedCoursePrerequisites.ID = "SelectedCoursePrerequisites_Field";
            coursePrerequisitesInputControls.Add(this._SelectedCoursePrerequisites);

            // build a container for the prerequisites listing
            this._PrerequisitesListContainer = new Panel();
            this._PrerequisitesListContainer.ID = "CoursePrerequisitesList_Container";
            this._PrerequisitesListContainer.CssClass = "ItemListingContainer";

            coursePrerequisitesInputControls.Add(this._PrerequisitesListContainer);

            Panel coursePrerequisitesButtonsPanel = new Panel();
            coursePrerequisitesButtonsPanel.ID = "CoursePrerequisites_ButtonsPanel";

            // select prerequisites button

            // link
            Image selectPrerequisitesImageForLink = new Image();
            selectPrerequisitesImageForLink.ID = "LaunchSelectPrerequisitesModalImage";
            selectPrerequisitesImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            selectPrerequisitesImageForLink.CssClass = "MediumIcon";

            Localize selectPrerequisitesTextForLink = new Localize();
            selectPrerequisitesTextForLink.Text = _GlobalResources.SelectPrerequisite_s;

            LinkButton selectPrerequisitesLink = new LinkButton();
            selectPrerequisitesLink.ID = "LaunchSelectPrerequisitesModal";
            selectPrerequisitesLink.CssClass = "ImageLink";
            selectPrerequisitesLink.Controls.Add(selectPrerequisitesImageForLink);
            selectPrerequisitesLink.Controls.Add(selectPrerequisitesTextForLink);
            coursePrerequisitesButtonsPanel.Controls.Add(selectPrerequisitesLink);

            // attach the buttons panel to the container
            coursePrerequisitesInputControls.Add(coursePrerequisitesButtonsPanel);

            // build modals for adding and removing prerequisites to/from the course
            this._BuildSelectPrerequisitesModal(selectPrerequisitesLink.ID);

            settingsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CoursePrerequisites",
                                                                               _GlobalResources.Prerequisites,
                                                                               coursePrerequisitesInputControls,
                                                                               false,
                                                                               true));

            // attach panel to container
            this.CoursePropertiesTabPanelsContainer.Controls.Add(settingsPanel);
        }
        #endregion

        #region _BuildSelectPrerequisitesModal
        /// <summary>
        /// Builds the modal for selecting prerequisites to add to the course.
        /// </summary>
        private void _BuildSelectPrerequisitesModal(string targetControlId)
        {
            // set modal properties
            this._SelectPrerequisitesForCourse = new ModalPopup("SelectPrerequisitesForCourseModal");
            this._SelectPrerequisitesForCourse.Type = ModalPopupType.Form;
            this._SelectPrerequisitesForCourse.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            this._SelectPrerequisitesForCourse.HeaderIconAlt = _GlobalResources.SelectPrerequisite_s;
            this._SelectPrerequisitesForCourse.HeaderText = _GlobalResources.SelectPrerequisite_s;
            this._SelectPrerequisitesForCourse.TargetControlID = targetControlId;
            this._SelectPrerequisitesForCourse.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectPrerequisitesForCourse.SubmitButtonCustomText = _GlobalResources.AddPrerequisite_s;
            this._SelectPrerequisitesForCourse.SubmitButton.OnClientClick = "javascript:AddPrerequisitesToCourse(); return false;";
            this._SelectPrerequisitesForCourse.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectPrerequisitesForCourse.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectPrerequisitesForCourse.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleCoursePrerequisites = new DynamicListBox("SelectEligibleCoursePrerequisites");
            this._SelectEligibleCoursePrerequisites.NoRecordsFoundMessage = _GlobalResources.NoCoursesFound;
            this._SelectEligibleCoursePrerequisites.SearchButton.Command += new CommandEventHandler(this._SearchSelectPrerequisitesButton_Command);
            this._SelectEligibleCoursePrerequisites.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectPrerequisitesButton_Command);
            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataSource = this._EligibleCoursePrerequisitesForSelectList;
            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataBind();

            // add controls to body
            this._SelectPrerequisitesForCourse.AddControlToBody(this._SelectEligibleCoursePrerequisites);

            // add modal to container
            this.CoursePropertiesTabPanelsContainer.Controls.Add(this._SelectPrerequisitesForCourse);
        }
        #endregion

        #region _SearchSelectPrerequisitesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Prerequisite(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectPrerequisitesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectPrerequisitesForCourse.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleCoursePrerequisites.ListBoxControl.Items.Clear();

            // do the search
            if (this._CourseObject != null)
            { this._EligibleCoursePrerequisitesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCoursePrerequisiteSelectList(this._CourseObject.Id, this._SelectEligibleCoursePrerequisites.SearchTextBox.Text); }
            else
            { this._EligibleCoursePrerequisitesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCoursePrerequisiteSelectList(0, this._SelectEligibleCoursePrerequisites.SearchTextBox.Text); }

            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataSource = this._EligibleCoursePrerequisitesForSelectList;
            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataBind();

            // if no records available then disable the submit button
            if (this._SelectEligibleCoursePrerequisites.ListBoxControl.Items.Count == 0)
            {
                this._SelectPrerequisitesForCourse.SubmitButton.Enabled = false;
                this._SelectPrerequisitesForCourse.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectPrerequisitesForCourse.SubmitButton.Enabled = true;
                this._SelectPrerequisitesForCourse.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _ClearSearchSelectPrerequisitesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Prerequisite(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectPrerequisitesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectPrerequisitesForCourse.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleCoursePrerequisites.ListBoxControl.Items.Clear();
            this._SelectEligibleCoursePrerequisites.SearchTextBox.Text = "";

            // clear the search
            if (this._CourseObject != null)
            { this._EligibleCoursePrerequisitesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCoursePrerequisiteSelectList(this._CourseObject.Id, null); }
            else
            { this._EligibleCoursePrerequisitesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCoursePrerequisiteSelectList(0, null); }

            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataSource = this._EligibleCoursePrerequisitesForSelectList;
            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleCoursePrerequisites.ListBoxControl.DataBind();

            // if records available then enable the submit button
            if (this._SelectEligibleCoursePrerequisites.ListBoxControl.Items.Count > 0)
            {
                this._SelectPrerequisitesForCourse.SubmitButton.Enabled = true;
                this._SelectPrerequisitesForCourse.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectPrerequisitesForCourse.SubmitButton.Enabled = false;
                this._SelectPrerequisitesForCourse.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _BuildCoursePropertiesFormPersonnelPanel
        /// <summary>
        /// Builds the course properties form fields under personnel tab.
        /// </summary>
        private void _BuildCoursePropertiesFormPersonnelPanel()
        {
            Panel personnelPanel = new Panel();
            personnelPanel.ID = "CourseProperties_" + "Personnel" + "_TabPanel";
            personnelPanel.CssClass = "TabPanelContainer";
            personnelPanel.Attributes.Add("style", "display: none;");

            // course experts field
            List<Control> courseExpertsInputControls = new List<Control>();

            // populate datatables with lists of users who are course experts and who are not
            if (this._CourseObject != null)
            {
                this._CourseExperts = this._CourseObject.GetExperts(null);
                this._EligibleCourseExpertsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForCourseExpertSelectList(this._CourseObject.Id, null);
            }
            else
            {
                this._EligibleCourseExpertsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForCourseExpertSelectList(0, null);
            }                                    

            // course selected experts hidden field
            this._SelectedCourseExperts = new HiddenField();
            this._SelectedCourseExperts.ID = "SelectedCourseExperts_Field";
            courseExpertsInputControls.Add(this._SelectedCourseExperts);

            // build a container for the user membership listing
            this._CourseExpertsListContainer = new Panel();
            this._CourseExpertsListContainer.ID = "CourseExpertsList_Container";
            this._CourseExpertsListContainer.CssClass = "ItemListingContainer";

            courseExpertsInputControls.Add(this._CourseExpertsListContainer);

            Panel courseExpertsButtonsPanel = new Panel();
            courseExpertsButtonsPanel.ID = "CourseExperts_ButtonsPanel";

            // select experts button

            // link
            Image selectExpertsImageForLink = new Image();
            selectExpertsImageForLink.ID = "LaunchSelectExpertsModalImage";
            selectExpertsImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR, ImageFiles.EXT_PNG);
            selectExpertsImageForLink.CssClass = "MediumIcon";

            Localize selectExpertsTextForLink = new Localize();
            selectExpertsTextForLink.Text = _GlobalResources.SelectExpert_s;

            LinkButton selectExpertsLink = new LinkButton();
            selectExpertsLink.ID = "LaunchSelectExpertsModal";
            selectExpertsLink.CssClass = "ImageLink";
            selectExpertsLink.Controls.Add(selectExpertsImageForLink);
            selectExpertsLink.Controls.Add(selectExpertsTextForLink);
            courseExpertsButtonsPanel.Controls.Add(selectExpertsLink);

            // attach the buttons panel to the container
            courseExpertsInputControls.Add(courseExpertsButtonsPanel);

            // build modals for adding and removing experts to/from the course
            this._BuildSelectExpertsModal(selectExpertsLink.ID);

            personnelPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseExperts",
                                                                                _GlobalResources.Experts,
                                                                                courseExpertsInputControls,
                                                                                false,
                                                                                true));

            // enrollment approvers field
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
            {
                List<Control> enrollmentApproversInputControls = new List<Control>();

                // populate datatables with lists of users who are course enrollment approvers
                if (this._CourseObject != null)
                {
                    this._EnrollmentApprovers = this._CourseObject.GetApprovers(null);
                    this._EligibleEnrollmentApproversForSelectList = Asentia.UMS.Library.User.IdsAndNamesForEnrollmentApproversSelectList(this._CourseObject.Id, null);
                }
                else
                {
                    this._EligibleEnrollmentApproversForSelectList = Asentia.UMS.Library.User.IdsAndNamesForEnrollmentApproversSelectList(0, null);
                }

                this._IsApprovalAllowedByCourseExperts = new CheckBox();
                this._IsApprovalAllowedByCourseExperts.ID = "ApprovalAllowedByCourseExperts_Field";
                this._IsApprovalAllowedByCourseExperts.Text = _GlobalResources.AllowCourseExpertsToApproveEnrollmentsOfThisCourse;
                enrollmentApproversInputControls.Add(this._IsApprovalAllowedByCourseExperts);

                this._IsApprovalAllowedByUserSupervisor = new CheckBox();
                this._IsApprovalAllowedByUserSupervisor.ID = "ApprovalAllowedByUserSupervisor_Field";
                this._IsApprovalAllowedByUserSupervisor.Text = _GlobalResources.AllowUserSupervisorsToApproveEnrollmentsOfThisCourse;
                enrollmentApproversInputControls.Add(this._IsApprovalAllowedByUserSupervisor);

                // course selected enrollment approvers hidden field
                this._SelectedEnrollmentApprovers = new HiddenField();
                this._SelectedEnrollmentApprovers.ID = "SelectedEnrollmentApprovers_Field";
                enrollmentApproversInputControls.Add(this._SelectedEnrollmentApprovers);

                // build a container for the user membership listing
                this._EnrollmentApproversListContainer = new Panel();
                this._EnrollmentApproversListContainer.ID = "EnrollmentApproversList_Container";
                this._EnrollmentApproversListContainer.CssClass = "ItemListingContainer";

                enrollmentApproversInputControls.Add(this._EnrollmentApproversListContainer);

                Panel enrollmentApproversButtonsPanel = new Panel();
                enrollmentApproversButtonsPanel.ID = "EnrollmentApprovers_ButtonsPanel";

                // select approvers button

                // link
                Image selectApproversImageForLink = new Image();
                selectApproversImageForLink.ID = "LaunchSelectApproversModalImage";
                selectApproversImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);
                selectApproversImageForLink.CssClass = "MediumIcon";

                Localize selectApproversTextForLink = new Localize();
                selectApproversTextForLink.Text = _GlobalResources.SelectEnrollmentApprover_s;

                LinkButton selectApproversLink = new LinkButton();
                selectApproversLink.ID = "LaunchSelectApproversModal";
                selectApproversLink.CssClass = "ImageLink";
                selectApproversLink.Controls.Add(selectApproversImageForLink);
                selectApproversLink.Controls.Add(selectApproversTextForLink);
                enrollmentApproversButtonsPanel.Controls.Add(selectApproversLink);

                // attach the buttons panel to the container
                enrollmentApproversInputControls.Add(enrollmentApproversButtonsPanel);

                // build modals for adding and removing approvers to/from the course
                this._BuildSelectApproversModal(selectApproversLink.ID);

                personnelPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("EnrollmentApprovers",
                                                                                    _GlobalResources.EnrollmentApprovers,
                                                                                    enrollmentApproversInputControls,
                                                                                    false,
                                                                                    true));
            }

            // attach panel to container
            this.CoursePropertiesTabPanelsContainer.Controls.Add(personnelPanel);
        }
        #endregion

        #region _BuildSelectExpertsModal
        /// <summary>
        /// Builds the modal for selecting experts to add to the course.
        /// </summary>
        private void _BuildSelectExpertsModal(string targetControlId)
        {
            // set modal properties
            this._SelectExpertsForCourse = new ModalPopup("SelectExpertsForCourseModal");
            this._SelectExpertsForCourse.Type = ModalPopupType.Form;
            this._SelectExpertsForCourse.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR, ImageFiles.EXT_PNG);
            this._SelectExpertsForCourse.HeaderIconAlt = _GlobalResources.SelectExpert_s;
            this._SelectExpertsForCourse.HeaderText = _GlobalResources.SelectExpert_s;
            this._SelectExpertsForCourse.TargetControlID = targetControlId;
            this._SelectExpertsForCourse.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectExpertsForCourse.SubmitButtonCustomText = _GlobalResources.AddExpert_s;
            this._SelectExpertsForCourse.SubmitButton.OnClientClick = "javascript:AddExpertsToCourse(); return false;";
            this._SelectExpertsForCourse.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectExpertsForCourse.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectExpertsForCourse.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleExpertUsers = new DynamicListBox("SelectEligibleExpertUsers");
            this._SelectEligibleExpertUsers.NoRecordsFoundMessage = _GlobalResources.NoUsersFound;
            this._SelectEligibleExpertUsers.SearchButton.Command += new CommandEventHandler(this._SearchSelectExpertsButton_Command);
            this._SelectEligibleExpertUsers.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectExpertsButton_Command);
            this._SelectEligibleExpertUsers.ListBoxControl.DataSource = this._EligibleCourseExpertsForSelectList;
            this._SelectEligibleExpertUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleExpertUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleExpertUsers.ListBoxControl.DataBind();

            // add controls to body
            this._SelectExpertsForCourse.AddControlToBody(this._SelectEligibleExpertUsers);

            // add modal to container
            this.CoursePropertiesTabPanelsContainer.Controls.Add(this._SelectExpertsForCourse);
        }
        #endregion

        #region _SearchSelectExpertsButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Expert(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectExpertsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectExpertsForCourse.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleExpertUsers.ListBoxControl.Items.Clear();

            // do the search
            if (this._CourseObject != null)
            { this._EligibleCourseExpertsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForCourseExpertSelectList(this._CourseObject.Id, this._SelectEligibleExpertUsers.SearchTextBox.Text); }
            else
            { this._EligibleCourseExpertsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForCourseExpertSelectList(0, this._SelectEligibleExpertUsers.SearchTextBox.Text); }

            this._SelectEligibleExpertUsers.ListBoxControl.DataSource = this._EligibleCourseExpertsForSelectList;
            this._SelectEligibleExpertUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleExpertUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleExpertUsers.ListBoxControl.DataBind();

            // if no records available then disable the submit button
            if (this._SelectEligibleExpertUsers.ListBoxControl.Items.Count == 0)
            {
                this._SelectExpertsForCourse.SubmitButton.Enabled = false;
                this._SelectExpertsForCourse.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectExpertsForCourse.SubmitButton.Enabled = true;
                this._SelectExpertsForCourse.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _ClearSearchSelectExpertsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Experts" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectExpertsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectExpertsForCourse.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleExpertUsers.ListBoxControl.Items.Clear();
            this._SelectEligibleExpertUsers.SearchTextBox.Text = "";

            // clear the search
            if (this._CourseObject != null)
            { this._EligibleCourseExpertsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForCourseExpertSelectList(this._CourseObject.Id, null); }
            else
            { this._EligibleCourseExpertsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForCourseExpertSelectList(0, null); }

            this._SelectEligibleExpertUsers.ListBoxControl.DataSource = this._EligibleCourseExpertsForSelectList;
            this._SelectEligibleExpertUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleExpertUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleExpertUsers.ListBoxControl.DataBind();

            // if records available then enable the submit button
            if (this._SelectEligibleExpertUsers.ListBoxControl.Items.Count > 0)
            {
                this._SelectExpertsForCourse.SubmitButton.Enabled = true;
                this._SelectExpertsForCourse.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectExpertsForCourse.SubmitButton.Enabled = false;
                this._SelectExpertsForCourse.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion        

        #region _BuildSelectApproversModal
        /// <summary>
        /// Builds the modal for selecting approvers to add to the course.
        /// </summary>
        private void _BuildSelectApproversModal(string targetControlId)
        {
            // set modal properties
            this._SelectApproversForCourse = new ModalPopup("SelectApproversForCourseModal");
            this._SelectApproversForCourse.Type = ModalPopupType.Form;
            this._SelectApproversForCourse.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);
            this._SelectApproversForCourse.HeaderIconAlt = _GlobalResources.SelectEnrollmentApprover_s;
            this._SelectApproversForCourse.HeaderText = _GlobalResources.SelectEnrollmentApprover_s;
            this._SelectApproversForCourse.TargetControlID = targetControlId;
            this._SelectApproversForCourse.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectApproversForCourse.SubmitButtonCustomText = _GlobalResources.AddEnrollmentApprover_s;
            this._SelectApproversForCourse.SubmitButton.OnClientClick = "javascript:AddApproversToCourse(); return false;";
            this._SelectApproversForCourse.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectApproversForCourse.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectApproversForCourse.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleApproverUsers = new DynamicListBox("SelectEligibleApproverUsers");
            this._SelectEligibleApproverUsers.NoRecordsFoundMessage = _GlobalResources.NoUsersFound;
            this._SelectEligibleApproverUsers.SearchButton.Command += new CommandEventHandler(this._SearchSelectApproversButton_Command);
            this._SelectEligibleApproverUsers.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectApproversButton_Command);
            this._SelectEligibleApproverUsers.ListBoxControl.DataSource = this._EligibleEnrollmentApproversForSelectList;
            this._SelectEligibleApproverUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleApproverUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleApproverUsers.ListBoxControl.DataBind();

            // add controls to body
            this._SelectApproversForCourse.AddControlToBody(this._SelectEligibleApproverUsers);

            // add modal to container
            this.CoursePropertiesTabPanelsContainer.Controls.Add(this._SelectApproversForCourse);
        }
        #endregion

        #region _SearchSelectApproversButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Approver(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectApproversButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectApproversForCourse.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleApproverUsers.ListBoxControl.Items.Clear();

            // do the search
            if (this._CourseObject != null)
            { this._EligibleEnrollmentApproversForSelectList = Asentia.UMS.Library.User.IdsAndNamesForEnrollmentApproversSelectList(this._CourseObject.Id, this._SelectEligibleApproverUsers.SearchTextBox.Text); }
            else
            { this._EligibleEnrollmentApproversForSelectList = Asentia.UMS.Library.User.IdsAndNamesForEnrollmentApproversSelectList(0, this._SelectEligibleApproverUsers.SearchTextBox.Text); }

            this._SelectEligibleApproverUsers.ListBoxControl.DataSource = this._EligibleEnrollmentApproversForSelectList;
            this._SelectEligibleApproverUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleApproverUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleApproverUsers.ListBoxControl.DataBind();

            // if no records available then disable the submit button
            if (this._SelectEligibleApproverUsers.ListBoxControl.Items.Count == 0)
            {
                this._SelectApproversForCourse.SubmitButton.Enabled = false;
                this._SelectApproversForCourse.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectApproversForCourse.SubmitButton.Enabled = true;
                this._SelectApproversForCourse.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _ClearSearchSelectApproversButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Approvers" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectApproversButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectApproversForCourse.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleApproverUsers.ListBoxControl.Items.Clear();
            this._SelectEligibleApproverUsers.SearchTextBox.Text = "";

            // clear the search
            if (this._CourseObject != null)
            { this._EligibleEnrollmentApproversForSelectList = Asentia.UMS.Library.User.IdsAndNamesForEnrollmentApproversSelectList(this._CourseObject.Id, null); }
            else
            { this._EligibleEnrollmentApproversForSelectList = Asentia.UMS.Library.User.IdsAndNamesForEnrollmentApproversSelectList(0, null); }

            this._SelectEligibleApproverUsers.ListBoxControl.DataSource = this._EligibleEnrollmentApproversForSelectList;
            this._SelectEligibleApproverUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleApproverUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleApproverUsers.ListBoxControl.DataBind();

            // if records available then enable the submit button
            if (this._SelectEligibleApproverUsers.ListBoxControl.Items.Count > 0)
            {
                this._SelectApproversForCourse.SubmitButton.Enabled = true;
                this._SelectApproversForCourse.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectApproversForCourse.SubmitButton.Enabled = false;
                this._SelectApproversForCourse.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _BuildCoursePropertiesFormModulesPanel
        /// <summary>
        /// Builds the course properties form fields under modules tab.
        /// </summary>
        private void _BuildCoursePropertiesFormModulesPanel()
        {
            UpdatePanel modulesPanel = new UpdatePanel();
            modulesPanel.ID = "CourseProperties_" + "Modules" + "_TabPanel";
            modulesPanel.Attributes.Add("class", "TabPanelContainer");
            modulesPanel.Attributes.Add("style", "display: none;");

            // build hidden field for module ordering
            this._LessonOrderHiddenField = new HiddenField();
            this._LessonOrderHiddenField.ID = "LessonOrderHiddenField";
            this._LessonOrderHiddenField.Value = "";
            this.CoursePropertiesTabPanelsContainer.Controls.Add(this._LessonOrderHiddenField);

            // build the grid
            this._LessonGrid = new Grid();
            this._LessonGrid.ID = "CourseLessonGrid";
            this._LessonGrid.AllowPaging = false;
            this._LessonGrid.ShowSearchBox = false;
            this._LessonGrid.ShowRecordsPerPageSelectbox = false;
            this._LessonGrid.PageSize = 1000;

            this._LessonGrid.StoredProcedure = Lesson.GridProcedure;
            this._LessonGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._LessonGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._LessonGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._LessonGrid.AddFilter("@idCourse", SqlDbType.Int, 4, this._CourseObject.Id);
            this._LessonGrid.IdentifierField = "idLesson";
            this._LessonGrid.DefaultSortColumn = "title";

            // data key names
            this._LessonGrid.DataKeyNames = new string[] { "idLesson" };

            // columns
            GridColumn titleContentTypes = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.ContentType_s, null); // this is calculated dynamically in the RowDataBound method

            GridColumn optional = new GridColumn(_GlobalResources.Optional, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this._LessonGrid.AddColumn(titleContentTypes);
            this._LessonGrid.AddColumn(optional);

            // add row data bound event
            this._LessonGrid.RowDataBound += new GridViewRowEventHandler(this._LessonGrid_RowDataBound);

            // bind the grid
            this._LessonGrid.BindData();

            // attach the grid to the update panel
            modulesPanel.ContentTemplateContainer.Controls.Add(this._LessonGrid);        

            // attach panel to container
            this.CoursePropertiesTabPanelsContainer.Controls.Add(modulesPanel);

            // after we got the lesson ordering into the input field from the databound event, strip the last character which is a |
            if (this._LessonOrderHiddenField.Value != String.Empty)
            {
                this._LessonOrderHiddenField.Value = this._LessonOrderHiddenField.Value.Substring(0, this._LessonOrderHiddenField.Value.Length - 1);
            }
        }
        #endregion

        #region _LessonGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the lesson grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LessonGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idLesson = Convert.ToInt32(rowView["idLesson"]);

                bool isOptional = false;
                if (rowView["isOptional"] != DBNull.Value)
                { isOptional = Convert.ToBoolean(rowView["isOptional"]); }


                // add the lesson id to the ordering hidden field
                this._LessonOrderHiddenField.Value += idLesson.ToString() + "|";

                // AVATAR, TITLE, CONTENT TYPE(S)

                int lessonOrder = Convert.ToInt32(rowView["order"]);
                string title = rowView["title"].ToString();
                bool hasContentPackage = Convert.ToBoolean(rowView["hasContentPackage"]);
                bool hasStandupTraining = Convert.ToBoolean(rowView["hasStandupTraining"]);
                bool hasTask = Convert.ToBoolean(rowView["hasTask"]);
                bool hasOJT = Convert.ToBoolean(rowView["hasOJT"]);                

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_LESSON, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = title;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // title
                Label titleLabelWrapper = new Label();
                titleLabelWrapper.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(titleLabelWrapper);

                HyperLink titleLink = new HyperLink();
                titleLink.NavigateUrl = "javascript: void(0);";
                titleLink.Attributes.Add("onclick", "LoadLessonModifyModalContent(" + idLesson.ToString() + ", true);");
                titleLabelWrapper.Controls.Add(titleLink);

                Label orderLabel = new Label();
                orderLabel.CssClass = "LessonOrderLabel";
                orderLabel.Text = lessonOrder.ToString();
                titleLink.Controls.Add(orderLabel);

                Literal titleLabel = new Literal();
                titleLabel.Text = ". " + title;
                titleLink.Controls.Add(titleLabel);                

                // content types                               
                Label contentTypesLabel = new Label();
                contentTypesLabel.CssClass = "GridSecondaryLine ContentTypesLabel";                
                e.Row.Cells[1].Controls.Add(contentTypesLabel);

                Literal contentTypesText = new Literal();
                contentTypesText.Text = _GlobalResources.ContentType_s + ": ";
                contentTypesLabel.Controls.Add(contentTypesText);
            
                if (hasContentPackage)
                {
                    Image contentPackageImage = new Image();
                    contentPackageImage.CssClass = "SmallIcon";
                    contentPackageImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG);
                    contentPackageImage.AlternateText = _GlobalResources.ContentPackage;
                    contentPackageImage.ToolTip = _GlobalResources.ContentPackage;
                    contentTypesLabel.Controls.Add(contentPackageImage);
                }

                if (hasStandupTraining)
                {
                    Image standupTrainingImage = new Image();
                    standupTrainingImage.CssClass = "SmallIcon";
                    standupTrainingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLASSROOM, ImageFiles.EXT_PNG);
                    standupTrainingImage.AlternateText = _GlobalResources.InstructorLedTraining;
                    standupTrainingImage.ToolTip = _GlobalResources.InstructorLedTraining;
                    contentTypesLabel.Controls.Add(standupTrainingImage);
                }

                if (hasTask)
                {
                    Image taskImage = new Image();
                    taskImage.CssClass = "SmallIcon";
                    taskImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_TASK, ImageFiles.EXT_PNG);
                    taskImage.AlternateText = _GlobalResources.Task;
                    taskImage.ToolTip = _GlobalResources.Task;
                    contentTypesLabel.Controls.Add(taskImage);
                }

                if (hasOJT)
                {
                    Image ojtImage = new Image();
                    ojtImage.CssClass = "SmallIcon";
                    ojtImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OJT, ImageFiles.EXT_PNG);
                    ojtImage.AlternateText = _GlobalResources.OJT;
                    ojtImage.ToolTip = _GlobalResources.OJT;
                    contentTypesLabel.Controls.Add(ojtImage);
                }

                // OPTIONAL COLUMN 

                // optional/required
                if (isOptional)
                {
                    Label moduleOptionalSpan = new Label();
                    moduleOptionalSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(moduleOptionalSpan);

                    LinkButton moduleOptionalLink = new LinkButton();
                    moduleOptionalLink.ID = "ModuleOptionalLink_" + idLesson.ToString();
                    moduleOptionalLink.OnClientClick = "MarkModuleRequired(" + idLesson + "); return false;";
                    moduleOptionalLink.ToolTip = _GlobalResources.ModuleIsOptionalForCourseCompletionClickToMakeItRequired;

                    Image moduleOptionalLinkImage = new Image();
                    moduleOptionalLinkImage.ID = "ModuleOptionalLinkImage_" + idLesson.ToString();
                    moduleOptionalLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                    moduleOptionalLinkImage.AlternateText = _GlobalResources.Optional;

                    moduleOptionalLink.Controls.Add(moduleOptionalLinkImage);

                    moduleOptionalSpan.Controls.Add(moduleOptionalLink);
                }
                else
                {
                    Label moduleOptionalSpan = new Label();
                    moduleOptionalSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(moduleOptionalSpan);

                    LinkButton moduleOptionalLink = new LinkButton();
                    moduleOptionalLink.ID = "ModuleOptionalLink_" + idLesson.ToString();
                    moduleOptionalLink.OnClientClick = "MarkModuleOptional(" + idLesson + "); return false;";
                    moduleOptionalLink.ToolTip = _GlobalResources.ModuleIsRequiredForCourseCompletionClickToMakeItOptional;

                    Image moduleOptionalLinkImage = new Image();
                    moduleOptionalLinkImage.ID = "ModuleOptionalLinkImage_" + idLesson.ToString();
                    moduleOptionalLinkImage.CssClass = "DimIcon";
                    moduleOptionalLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                    moduleOptionalLinkImage.AlternateText = _GlobalResources.Required;

                    moduleOptionalLink.Controls.Add(moduleOptionalLinkImage);

                    moduleOptionalSpan.Controls.Add(moduleOptionalLink);
                }
            }
        }
        #endregion

        #region _BuildCoursePropertiesFormCourseMaterialsPanel
        /// <summary>
        /// Builds the course properties form fields under course materials tab.
        /// </summary>
        private void _BuildCoursePropertiesFormCourseMaterialsPanel()
        {
            UpdatePanel courseMaterialsPanel = new UpdatePanel();
            courseMaterialsPanel.ID = "CourseProperties_" + "CourseMaterials" + "_TabPanel";
            courseMaterialsPanel.Attributes.Add("class", "TabPanelContainer");
            courseMaterialsPanel.Attributes.Add("style", "display: none;");

            // build the grid
            this._CourseMaterialGrid = new Grid();
            this._CourseMaterialGrid.ID = "CourseMaterialGrid";
            this._CourseMaterialGrid.AllowPaging = false;
            this._CourseMaterialGrid.ShowSearchBox = false;
            this._CourseMaterialGrid.ShowRecordsPerPageSelectbox = false;
            this._CourseMaterialGrid.PageSize = 1000;

            this._CourseMaterialGrid.StoredProcedure = DocumentRepositoryItem.GridProcedure;
            this._CourseMaterialGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._CourseMaterialGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._CourseMaterialGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._CourseMaterialGrid.AddFilter("@idDocumentRepositoryObjectType", SqlDbType.Int, 4, DocumentRepositoryObjectType.Course);
            this._CourseMaterialGrid.AddFilter("@idObject", SqlDbType.Int, 4, this._CourseObject.Id);
            this._CourseMaterialGrid.IdentifierField = "idDocumentRepositoryItem";
            this._CourseMaterialGrid.DefaultSortColumn = "name";

            // data key names
            this._CourseMaterialGrid.DataKeyNames = new string[] { "idDocumentRepositoryItem" };

            // columns
            GridColumn nameFileSize = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.File + " (" + _GlobalResources.Size + ")", null); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Private, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this._CourseMaterialGrid.AddColumn(nameFileSize);
            this._CourseMaterialGrid.AddColumn(options);

            // add row data bound event
            this._CourseMaterialGrid.RowDataBound += new GridViewRowEventHandler(this._CourseMaterialGrid_RowDataBound);

            // bind the grid
            this._CourseMaterialGrid.BindData();

            // attach the grid to the update panel
            courseMaterialsPanel.ContentTemplateContainer.Controls.Add(this._CourseMaterialGrid);   

            // attach panel to container
            this.CoursePropertiesTabPanelsContainer.Controls.Add(courseMaterialsPanel);
        }
        #endregion

        #region _CourseMaterialGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the course material grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _CourseMaterialGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idDocumentRepositoryItem = Convert.ToInt32(rowView["idDocumentRepositoryItem"]);                

                // AVATAR, TITLE, FILE (SIZE) COLUMN
                
                string title = rowView["label"].ToString();
                string fileName = rowView["fileName"].ToString();
                string kb = rowView["kb"].ToString();                
                bool isPrivate = Convert.ToBoolean(rowView["isPrivate"]);                

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";                

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = title;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // title
                Label titleLabel = new Label();
                titleLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(titleLabel);

                HyperLink titleLink = new HyperLink();                
                titleLink.NavigateUrl = "javascript: void(0);";
                titleLink.Attributes.Add("onclick", "LoadCourseMaterialModifyModalContent(" + idDocumentRepositoryItem.ToString() + ", true);");
                titleLink.Text = title;
                titleLabel.Controls.Add(titleLink);                

                // file name
                Label fileNameLabel = new Label();
                fileNameLabel.CssClass = "GridSecondaryLine";
                e.Row.Cells[1].Controls.Add(fileNameLabel);

                HyperLink courseMaterialLink = new HyperLink();
                courseMaterialLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_COURSE + this._CourseObject.Id.ToString() + "/" + fileName;
                courseMaterialLink.Target = "_blank";
                courseMaterialLink.Text = fileName;
                fileNameLabel.Controls.Add(courseMaterialLink);

                Literal courseMaterialSize = new Literal();
                //courseMaterialSize.Text = " (" + Asentia.Common.Utility.GetSizeStringFromKB(kb) + ")";
                courseMaterialSize.Text = " (" + kb + ")";
                fileNameLabel.Controls.Add(courseMaterialSize);

                // PRIVATE COLUMN 

                // publish/unpublish
                if (isPrivate)
                {
                    Label courseMaterialPrivatePublicSpan = new Label();
                    courseMaterialPrivatePublicSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(courseMaterialPrivatePublicSpan);

                    LinkButton courseMaterialMakePublicLink = new LinkButton();
                    courseMaterialMakePublicLink.ID = "CourseMaterialMakePublicLink_" + idDocumentRepositoryItem.ToString();
                    courseMaterialMakePublicLink.OnClientClick = "LoadMakeCourseMaterialPublicModalContent(" + idDocumentRepositoryItem + "); return false;";
                    courseMaterialMakePublicLink.ToolTip = _GlobalResources.CourseMaterialIsPrivateClickToMakeItPublic;

                    Image courseMaterialMakePublicLinkImage = new Image();
                    courseMaterialMakePublicLinkImage.ID = "CourseMaterialMakePublicLinkImage_" + idDocumentRepositoryItem.ToString();
                    courseMaterialMakePublicLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED, ImageFiles.EXT_PNG);
                    courseMaterialMakePublicLinkImage.AlternateText = _GlobalResources.Private;

                    courseMaterialMakePublicLink.Controls.Add(courseMaterialMakePublicLinkImage);

                    courseMaterialPrivatePublicSpan.Controls.Add(courseMaterialMakePublicLink);
                }
                else
                {                    
                    Label courseMaterialPrivatePublicSpan = new Label();
                    courseMaterialPrivatePublicSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(courseMaterialPrivatePublicSpan);

                    LinkButton courseMaterialMakePrivateLink = new LinkButton();
                    courseMaterialMakePrivateLink.ID = "CourseMaterialMakePrivateLink_" + idDocumentRepositoryItem.ToString();
                    courseMaterialMakePrivateLink.OnClientClick = "LoadMakeCourseMaterialPrivateModalContent(" + idDocumentRepositoryItem + "); return false;";
                    courseMaterialMakePrivateLink.ToolTip = _GlobalResources.CourseMaterialIsPublicClickToMakeItPrivate;

                    Image courseMaterialMakePrivateLinkImage = new Image();
                    courseMaterialMakePrivateLinkImage.ID = "CourseMaterialMakePrivateLinkImage_" + idDocumentRepositoryItem.ToString();
                    courseMaterialMakePrivateLinkImage.CssClass = "DimIcon";
                    courseMaterialMakePrivateLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED, ImageFiles.EXT_PNG);
                    courseMaterialMakePrivateLinkImage.AlternateText = _GlobalResources.Public;

                    courseMaterialMakePrivateLink.Controls.Add(courseMaterialMakePrivateLinkImage);

                    courseMaterialPrivatePublicSpan.Controls.Add(courseMaterialMakePrivateLink);
                }
            }
        }
        #endregion

        #region _BuildCoursePropertiesFormSampleScreensPanel
        /// <summary>
        /// Builds the course properties form fields under sample screen tab.
        /// </summary>
        private void _BuildCoursePropertiesFormSampleScreensPanel()
        {            
            Panel sampleScreensPanel = new Panel();
            sampleScreensPanel.ID = "CourseProperties_" + "SampleScreens" + "_TabPanel";
            sampleScreensPanel.CssClass = "TabPanelContainer";
            sampleScreensPanel.Attributes.Add("style", "display: none;");            
            
            // course sample screens field
            List<Control> sampleScreensInputControls = new List<Control>();

            // course sample screens uploader
            this._CourseSampleScreenUploader = new UploaderAsync("CourseSampleScreenUploader_Field", UploadType.Avatar, "CourseSampleScreens_ErrorContainer");
            this._CourseSampleScreenUploader.ShowPreviewOnImageUpload = false;
            this._CourseSampleScreenUploader.ClientSideCompleteJSMethod = "CourseSampleScreenUploaded";

            // set params to resize the course sample screen upon upload
            this._CourseSampleScreenUploader.ResizeOnUpload = true;
            this._CourseSampleScreenUploader.ResizeMaxWidth = 600;
            this._CourseSampleScreenUploader.ResizeMaxHeight = 450;

            sampleScreensInputControls.Add(this._CourseSampleScreenUploader);

            // course uploaded sample screens hidden field
            this._UploadedCourseSampleScreens = new HiddenField();
            this._UploadedCourseSampleScreens.ID = "UploadedCourseSampleScreens_Field";
            sampleScreensInputControls.Add(this._UploadedCourseSampleScreens);           

            this._SampleScreensListContainer = new Panel();
            this._SampleScreensListContainer.ID = "CourseSampleScreensList_Container";

            sampleScreensInputControls.Add(this._SampleScreensListContainer);            

            sampleScreensPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseSampleScreens",
                                                                                 String.Empty,
                                                                                 sampleScreensInputControls,
                                                                                 false,
                                                                                 true));
            // attach panel to container
            this.CoursePropertiesTabPanelsContainer.Controls.Add(sampleScreensPanel);
        }
        #endregion        

        #region _BuildCoursePropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for Course Properties actions.
        /// </summary>
        private void _BuildCoursePropertiesActionsPanel()
        {
            // clear controls from container
            this.CoursePropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.CoursePropertiesActionsPanel.CssClass = "ActionsPanel";

            // COURSE SAVE BUTTON

            this._CoursePropertiesSaveButton = new Button();
            this._CoursePropertiesSaveButton.ID = "CourseSaveButton";
            this._CoursePropertiesSaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._CourseObject == null)
            { this._CoursePropertiesSaveButton.Text = _GlobalResources.CreateCourse; }
            else
            { this._CoursePropertiesSaveButton.Text = _GlobalResources.SaveChanges; }

            this._CoursePropertiesSaveButton.Command += new CommandEventHandler(this._CoursePropertiesSaveButton_Command);
            this._CoursePropertiesSaveButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicCourseElements();");

            this.CoursePropertiesActionsPanel.Controls.Add(this._CoursePropertiesSaveButton);

            // set the default button for the course properties container to the id of the properties save button
            this.CoursePropertiesContainer.DefaultButton = this._CoursePropertiesSaveButton.ID;            

            // CANCEL BUTTON

            this._CoursePropertiesCancelButton = new Button();
            this._CoursePropertiesCancelButton.ID = "CancelButton";
            this._CoursePropertiesCancelButton.CssClass = "Button NonActionButton";
            this._CoursePropertiesCancelButton.Text = _GlobalResources.Cancel;
            this._CoursePropertiesCancelButton.Command += new CommandEventHandler(this._CoursePropertiesCancelButton_Command);

            this.CoursePropertiesActionsPanel.Controls.Add(this._CoursePropertiesCancelButton);

            if (this._CourseObject != null)
            {
                // LESSON GRID DELETE BUTTON

                this._LessonGridDeleteButton = new LinkButton();
                this._LessonGridDeleteButton.ID = "LessonGridDeleteButton";
                this._LessonGridDeleteButton.CssClass = "GridDeleteButton";
                this._LessonGridDeleteButton.Style.Add("display", "none");

                // delete button image
                Image lessonGridDeleteImage = new Image();
                lessonGridDeleteImage.ID = "LessonGridDeleteButtonImage";
                lessonGridDeleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                lessonGridDeleteImage.CssClass = "MediumIcon";
                lessonGridDeleteImage.AlternateText = _GlobalResources.Delete;
                this._LessonGridDeleteButton.Controls.Add(lessonGridDeleteImage);

                // delete button text
                Literal lessonGridDeleteText = new Literal();
                lessonGridDeleteText.Text = _GlobalResources.DeleteSelectedModule_s;
                this._LessonGridDeleteButton.Controls.Add(lessonGridDeleteText);

                this.CoursePropertiesActionsPanel.Controls.Add(this._LessonGridDeleteButton);

                // COURSE MATERIALS GRID DELETE BUTTON

                this._CourseMaterialGridDeleteButton = new LinkButton();
                this._CourseMaterialGridDeleteButton.ID = "CourseMaterialGridDeleteButton";
                this._CourseMaterialGridDeleteButton.CssClass = "GridDeleteButton";
                this._CourseMaterialGridDeleteButton.Style.Add("display", "none");

                // delete button image
                Image courseMaterialGridDeleteImage = new Image();
                courseMaterialGridDeleteImage.ID = "CourseMaterialGridDeleteButtonImage";
                courseMaterialGridDeleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                courseMaterialGridDeleteImage.CssClass = "MediumIcon";
                courseMaterialGridDeleteImage.AlternateText = _GlobalResources.Delete;
                this._CourseMaterialGridDeleteButton.Controls.Add(courseMaterialGridDeleteImage);

                // delete button text
                Literal courseMaterialGridDeleteText = new Literal();
                courseMaterialGridDeleteText.Text = _GlobalResources.DeleteSelectedCourseMaterial_s;
                this._CourseMaterialGridDeleteButton.Controls.Add(courseMaterialGridDeleteText);

                this.CoursePropertiesActionsPanel.Controls.Add(this._CourseMaterialGridDeleteButton);                
            }

            // PAGE ACTION HIDDEN FIELD

            this._PageAction = new HiddenField();
            this._PageAction.ID = "PageAction";
            this._PageAction.Value = this.QueryStringString("action", String.Empty);
            this.CoursePropertiesActionsPanel.Controls.Add(this._PageAction);
        }
        #endregion

        #region _PopulateCoursePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateCoursePropertiesInputElements()
        {
            if (this._CourseObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // title, short description, long description, objectives
                bool isDefaultPopulated = false;

                foreach (Course.LanguageSpecificProperty courseLanguageSpecificProperty in this._CourseObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._CourseTitle.Text = courseLanguageSpecificProperty.Title;
                        this._CourseShortDescription.Text = courseLanguageSpecificProperty.ShortDescription;
                        this._CourseLongDescription.Text = courseLanguageSpecificProperty.LongDescription;
                        this._CourseObjectives.Text = courseLanguageSpecificProperty.Objectives;
                        this._CourseSearchTags.Text = courseLanguageSpecificProperty.SearchTags;

                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificCourseTitleTextBox = (TextBox)this.CoursePropertiesContainer.FindControl(this._CourseTitle.ID + "_" + courseLanguageSpecificProperty.LangString);
                        TextBox languageSpecificCourseShortDescriptionTextBox = (TextBox)this.CoursePropertiesContainer.FindControl(this._CourseShortDescription.ID + "_" + courseLanguageSpecificProperty.LangString);
                        TextBox languageSpecificCourseLongDescriptionTextBox = (TextBox)this.CoursePropertiesContainer.FindControl(this._CourseLongDescription.ID + "_" + courseLanguageSpecificProperty.LangString);
                        TextBox languageSpecificCourseObjectivesTextBox = (TextBox)this.CoursePropertiesContainer.FindControl(this._CourseObjectives.ID + "_" + courseLanguageSpecificProperty.LangString);
                        TextBox languageSpecificCourseSearchTagsTextBox = (TextBox)this.CoursePropertiesContainer.FindControl(this._CourseSearchTags.ID + "_" + courseLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificCourseTitleTextBox != null)
                        { languageSpecificCourseTitleTextBox.Text = courseLanguageSpecificProperty.Title; }

                        if (languageSpecificCourseShortDescriptionTextBox != null)
                        { languageSpecificCourseShortDescriptionTextBox.Text = courseLanguageSpecificProperty.ShortDescription; }

                        if (languageSpecificCourseLongDescriptionTextBox != null)
                        { languageSpecificCourseLongDescriptionTextBox.Text = courseLanguageSpecificProperty.LongDescription; }

                        if (languageSpecificCourseObjectivesTextBox != null)
                        { languageSpecificCourseObjectivesTextBox.Text = courseLanguageSpecificProperty.Objectives; }

                        if (languageSpecificCourseSearchTagsTextBox != null)
                        { languageSpecificCourseSearchTagsTextBox.Text = courseLanguageSpecificProperty.SearchTags; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._CourseTitle.Text = this._CourseObject.Title;
                    this._CourseShortDescription.Text = this._CourseObject.ShortDescription;
                    this._CourseLongDescription.Text = this._CourseObject.LongDescription;
                    this._CourseObjectives.Text = this._CourseObject.Objectives;
                    this._CourseSearchTags.Text = this._CourseObject.SearchTags;
                }

                // NON-LANGUAGE SPECIFIC PROPERTIES

                // course code
                this._CourseCode.Text = this._CourseObject.CourseCode;

                // catalog link shortcode
                this._Shortcode.Text = this._CourseObject.Shortcode;

                // ratings
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RATINGS_COURSE_ENABLE))
                {
                    if (this._CourseObject.DisallowRating)
                    { this._EnableRatings.Checked = false; }
                }

                // avatar image
                if (this._CourseObject.Avatar != null)
                {
                    this._AvatarImage.ImageUrl = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + this._CourseObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                }
                
                // estimated time
                if (this._CourseObject.EstimatedLengthInMinutes != null)
                {
                    int courseEstimatedHours = (int)this._CourseObject.EstimatedLengthInMinutes / 60;
                    int courseEstimatedMinutes = (int)this._CourseObject.EstimatedLengthInMinutes - (courseEstimatedHours * 60);

                    if (courseEstimatedHours > 0)
                    { this._CourseEstimatedTimeHours.Text = courseEstimatedHours.ToString(); }

                    this._CourseEstimatedTimeMinutes.Text = courseEstimatedMinutes.ToString();
                }                

                // rev code
                this._CourseRevCode.Text = this._CourseObject.RevCode;

                // is published
                if (this._CourseObject.IsPublished == true)
                { this._CoursePublished.SelectedValue = "True"; }
                else
                { this._CoursePublished.SelectedValue = "False"; }

                // is closed
                if (this._CourseObject.IsClosed == true)
                { this._CourseClosed.SelectedValue = "True"; }
                else
                { this._CourseClosed.SelectedValue = "False"; }

                // is locked
                if (this._CourseObject.IsLocked == true)
                { this._CourseLocked.SelectedValue = "True"; }
                else
                { this._CourseLocked.SelectedValue = "False"; }

                // course cost
                if (this._EcommerceSettings.IsEcommerceSet)
                { this._CourseCost.Text = this._CourseObject.Cost.ToString(); }

                // self-enrollment settings

                // self-enrollment is one-time only
                if (this._CourseObject.SelfEnrollmentIsOneTimeOnly == true)
                { this._SelfEnrollmentIsOneTimeOnly.Checked = true; }
                else
                { this._SelfEnrollmentIsOneTimeOnly.Checked = false; }

                // due
                if (this._CourseObject.SelfEnrollmentDueInterval != null)
                {
                    this._SelfEnrollmentDue.NoneCheckBoxChecked = false;
                    this._SelfEnrollmentDue.IntervalValue = this._CourseObject.SelfEnrollmentDueInterval.ToString();
                    this._SelfEnrollmentDue.TimeframeValue = this._CourseObject.SelfEnrollmentDueTimeframe;
                }
                else
                {
                    this._SelfEnrollmentDue.NoneCheckBoxChecked = true;
                    this._SelfEnrollmentDue.IntervalValue = null;
                    this._SelfEnrollmentDue.TimeframeValue = null;
                }

                // expires from start
                if (this._CourseObject.SelfEnrollmentExpiresFromStartInterval != null)
                {
                    this._SelfEnrollmentExpiresFromStart.NoneCheckBoxChecked = false;
                    this._SelfEnrollmentExpiresFromStart.IntervalValue = this._CourseObject.SelfEnrollmentExpiresFromStartInterval.ToString();
                    this._SelfEnrollmentExpiresFromStart.TimeframeValue = this._CourseObject.SelfEnrollmentExpiresFromStartTimeframe;
                }
                else
                {
                    this._SelfEnrollmentExpiresFromStart.NoneCheckBoxChecked = true;
                    this._SelfEnrollmentExpiresFromStart.IntervalValue = null;
                    this._SelfEnrollmentExpiresFromStart.TimeframeValue = null;
                }

                // expires from first launch
                if (this._CourseObject.SelfEnrollmentExpiresFromFirstLaunchInterval != null)
                {
                    this._SelfEnrollmentExpiresFromFirstLaunch.NoneCheckBoxChecked = false;
                    this._SelfEnrollmentExpiresFromFirstLaunch.IntervalValue = this._CourseObject.SelfEnrollmentExpiresFromFirstLaunchInterval.ToString();
                    this._SelfEnrollmentExpiresFromFirstLaunch.TimeframeValue = this._CourseObject.SelfEnrollmentExpiresFromFirstLaunchTimeframe;
                }
                else
                {
                    this._SelfEnrollmentExpiresFromFirstLaunch.NoneCheckBoxChecked = true;
                    this._SelfEnrollmentExpiresFromFirstLaunch.IntervalValue = null;
                    this._SelfEnrollmentExpiresFromFirstLaunch.TimeframeValue = null;
                }

                // self enrollment approval required
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
                {
                    if (this._CourseObject.IsSelfEnrollmentApprovalRequired == true)
                    { this._IsSelfEnrollmentApprovalRequired.Checked = true; }
                    else
                    { this._IsSelfEnrollmentApprovalRequired.Checked = false; }
                }

                // course feed object (wall) - only if the feature is enabled
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE))
                {
                    if (Convert.ToBoolean(this._CourseObject.IsFeedActive))
                    {
                        this._CourseWall.SelectedIndex = 0;
                        this._IsWallModerated.Checked = Convert.ToBoolean(this._CourseObject.IsFeedModerated);
                        this._IsWallOpenView.Checked = Convert.ToBoolean(this._CourseObject.IsFeedOpenSubscription);
                    }
                    else
                    {
                        this._CourseWall.SelectedIndex = 1;
                        this._IsWallModerated.Checked = false;
                        this._IsWallOpenView.Checked = false;
                    }
                }

                // course credits
                this._CourseCredits.Text = this._CourseObject.Credits.ToString();

                // lesson ordering settings              
                this._CourseForceLessonCompletionInOrder.Checked = this._CourseObject.ForceLessonCompletionInOrder;
                this._CourseRequireFirstLessonToBeCompletedBeforeOthers.Checked = this._CourseObject.RequireFirstLessonToBeCompletedBeforeOthers;
                this._CourseLockLastLessonUntilOthersCompleted.Checked = this._CourseObject.LockLastLessonUntilOthersCompleted;

                /* SOCIAL MEDIA ITEMS */

                int socialMediaItemsCount = 0;

                foreach (Course.SocialMediaProperty socialMediaProperty in this._CourseObject.SocialMediaProperties)
                {
                    socialMediaItemsCount++;

                    Panel socialMediaItemContainer = new Panel();
                    socialMediaItemContainer.ID = "CourseSocialMediaItem_" + socialMediaItemsCount + "_Container";
                    socialMediaItemContainer.CssClass = "CourseSocialMediaItemContainer";

                    Panel socialMediaItemInputsContainer = new Panel();
                    socialMediaItemInputsContainer.ID = "CourseSocialMediaItem_" + socialMediaItemsCount + "_InputsContainer";
                    socialMediaItemInputsContainer.CssClass = "CourseSocialMediaItemInputsContainer";
                    socialMediaItemContainer.Controls.Add(socialMediaItemInputsContainer);

                    Image itemIcon = new Image();
                    itemIcon.ID = "CourseSocialMediaItem_" + socialMediaItemsCount + "_Icon";
                    itemIcon.CssClass = "CourseSocialMediaItemIcon SmallIcon";

                    switch (socialMediaProperty.IconType)
                    {
                        case "facebook":
                            itemIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_FACEBOOK, ImageFiles.EXT_PNG);
                            break;
                        case "linkedin":
                            itemIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LINKEDIN, ImageFiles.EXT_PNG);
                            break;
                        case "twitter":
                            itemIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_TWITTER, ImageFiles.EXT_PNG);
                            break;
                        case "google":
                            itemIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOOGLE, ImageFiles.EXT_PNG);
                            break;
                        case "youtube":
                            itemIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_YOUTUBE, ImageFiles.EXT_PNG);
                            break;
                        default:
                            itemIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CUSTOMFORUM, ImageFiles.EXT_PNG);
                            break;
                    }

                    socialMediaItemInputsContainer.Controls.Add(itemIcon);

                    // protocol drop-down
                    DropDownList protocolSelect = new DropDownList();
                    protocolSelect.ID = "CourseSocialMediaItem_" + socialMediaItemsCount + "_ProtocolSelect";
                    protocolSelect.CssClass = "CourseSocialMediaItemProtocolSelect";
                    protocolSelect.Items.Add(new ListItem("http://"));
                    protocolSelect.Items.Add(new ListItem("https://"));
                    protocolSelect.SelectedValue = socialMediaProperty.Protocol;

                    socialMediaItemInputsContainer.Controls.Add(protocolSelect);

                    // url text box
                    TextBox urlTextBox = new TextBox();
                    urlTextBox.ID = "CourseSocialMediaItem_" + socialMediaItemsCount + "_URLField";
                    urlTextBox.CssClass = "InputLong CourseSocialMediaItemURLField";
                    urlTextBox.Attributes.Add("onChange", "UpdateSocialMediaItemImage(" + socialMediaItemsCount + ");");
                    urlTextBox.Attributes.Add("onKeyUp", "UpdateSocialMediaItemImage(" + socialMediaItemsCount + ");");
                    urlTextBox.Text = socialMediaProperty.Href;

                    socialMediaItemInputsContainer.Controls.Add(urlTextBox);

                    // delete item link
                    HtmlAnchor deleteItemLink = new HtmlAnchor();
                    deleteItemLink.ID = "CourseSocialMediaItem_" + socialMediaItemsCount + "_DeleteItemLink";
                    deleteItemLink.HRef = "javascript:RemoveSocialMediaNode(" + socialMediaItemsCount + ");";

                    Image deleteItemIcon = new Image();
                    deleteItemIcon.ID = "CourseSocialMediaItem_" + socialMediaItemsCount + "_DeleteItemIcon";
                    deleteItemIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                     ImageFiles.EXT_PNG);
                    deleteItemIcon.CssClass = "SmallIcon";

                    deleteItemLink.Controls.Add(deleteItemIcon);
                    socialMediaItemInputsContainer.Controls.Add(deleteItemLink);

                    // enrolled-only checkbox
                    CheckBox itemEnrolledOnlyCheckBox = new CheckBox();
                    itemEnrolledOnlyCheckBox.ID = "CourseSocialMediaItem_" + socialMediaItemsCount + "_IsEnrolledOnly";
                    itemEnrolledOnlyCheckBox.CssClass = "CourseSocialMediaItemIsEnrolledOnlyCheckBox";
                    itemEnrolledOnlyCheckBox.Text = _GlobalResources.DisplayOnlyWhenUserEnrolledInCourse;
                    itemEnrolledOnlyCheckBox.Attributes.Add("value", "true");
                    itemEnrolledOnlyCheckBox.Checked = socialMediaProperty.EnrollmentRequired;

                    socialMediaItemContainer.Controls.Add(itemEnrolledOnlyCheckBox);

                    // attach the item container to the items container
                    this._CourseSocialMediaItemsContainer.Controls.Add(socialMediaItemContainer);
                }

                // set the number of social media items
                this._CourseSocialMediaItemCount.Value = socialMediaItemsCount.ToString();

                /* COURSE EXPERTS */

                // loop through the datatable and add each member to the listing container
                foreach (DataRow row in this._CourseExperts.Rows)
                {
                    // container
                    Panel userNameContainer = new Panel();
                    userNameContainer.ID = "Expert_" + row["idUser"].ToString();

                    // remove expert button
                    Image removeExpertImage = new Image();
                    removeExpertImage.ID = "Expert_" + row["idUser"].ToString() + "_RemoveImage";
                    removeExpertImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    removeExpertImage.CssClass = "SmallIcon";
                    removeExpertImage.Attributes.Add("onClick", "javascript:RemoveExpertFromCourse('" + row["idUser"].ToString() + "');");
                    removeExpertImage.Style.Add("cursor", "pointer");

                    // expert name
                    Literal userName = new Literal();
                    userName.Text = row["displayName"].ToString();

                    // add controls to container
                    userNameContainer.Controls.Add(removeExpertImage);
                    userNameContainer.Controls.Add(userName);
                    this._CourseExpertsListContainer.Controls.Add(userNameContainer);
                }

                /* ENROLLMENT APPROVERS */

                // loop through the datatable and add each member to the listing container
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
                {
                    foreach (DataRow row in this._EnrollmentApprovers.Rows)
                    {
                        // container
                        Panel userNameContainer = new Panel();
                        userNameContainer.ID = "Approver_" + row["idUser"].ToString();

                        // remove approver button
                        Image removeApproverImage = new Image();
                        removeApproverImage.ID = "Approver_" + row["idUser"].ToString() + "_RemoveImage";
                        removeApproverImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        removeApproverImage.CssClass = "SmallIcon";
                        removeApproverImage.Attributes.Add("onClick", "javascript:RemoveApproverFromCourse('" + row["idUser"].ToString() + "');");
                        removeApproverImage.Style.Add("cursor", "pointer");

                        // expert name
                        Literal userName = new Literal();
                        userName.Text = row["displayName"].ToString();

                        // add controls to container
                        userNameContainer.Controls.Add(removeApproverImage);
                        userNameContainer.Controls.Add(userName);
                        this._EnrollmentApproversListContainer.Controls.Add(userNameContainer);
                    }

                    // is approval allowed by course experts
                    if (this._CourseObject.IsApprovalAllowedByCourseExperts == true)
                    { this._IsApprovalAllowedByCourseExperts.Checked = true; }
                    else
                    { this._IsApprovalAllowedByCourseExperts.Checked = false; }

                    // is approval allowed by supervisors
                    if (this._CourseObject.IsApprovalAllowedByUserSupervisor == true)
                    { this._IsApprovalAllowedByUserSupervisor.Checked = true; }
                    else
                    { this._IsApprovalAllowedByUserSupervisor.Checked = false; }
                }

                /* COURSE PREREQUISITES */

                // any/all radio button
                if (this._CourseObject.IsPrerequisiteAny != null)
                {
                    if (!(bool)this._CourseObject.IsPrerequisiteAny)
                    { this._CoursePrerequisitesIsAny.SelectedValue = "False"; }
                    else
                    { this._CoursePrerequisitesIsAny.SelectedValue = "True"; }
                }

                // loop through the datatable and add each prerequisite to the listing container
                foreach (DataRow row in this._CoursePrerequisites.Rows)
                {
                    // container
                    Panel courseNameContainer = new Panel();
                    courseNameContainer.ID = "Prerequisite_" + row["idCourse"].ToString();

                    // remove prerequisite button
                    Image removePrerequisiteImage = new Image();
                    removePrerequisiteImage.ID = "Prerequisite_" + row["idCourse"].ToString() + "_RemoveImage";
                    removePrerequisiteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    removePrerequisiteImage.CssClass = "SmallIcon";
                    removePrerequisiteImage.Attributes.Add("onClick", "javascript:RemovePrerequisiteFromCourse('" + row["idCourse"].ToString() + "');");
                    removePrerequisiteImage.Style.Add("cursor", "pointer");

                    // course name
                    Literal courseName = new Literal();
                    courseName.Text = row["title"].ToString();

                    // add controls to container
                    courseNameContainer.Controls.Add(removePrerequisiteImage);
                    courseNameContainer.Controls.Add(courseName);
                    this._PrerequisitesListContainer.Controls.Add(courseNameContainer);
                }

                /* COURSE SAMPLE SCREENS */

                // populate datatable with course sample screens
                this._CourseSampleScreens = this._CourseObject.GetSampleScreens();

                // loop through the datatable and add each sample screen to the listing container
                int index = 0;
                
                foreach (DataRow row in this._CourseSampleScreens.Rows)
                {                    
                    Panel courseSampleScreenPreviewContainer = new Panel();
                    courseSampleScreenPreviewContainer.ID = "CourseSampleScreenPreviewContainer_" + index.ToString();                    
                    courseSampleScreenPreviewContainer.CssClass = "CourseSampleScreenPreviewContainer";
                    courseSampleScreenPreviewContainer.Attributes.Add("filename", row["filename"].ToString());
                    courseSampleScreenPreviewContainer.Style.Add("background-image", "url('" + SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/samplescreens/" + row["filename"].ToString() + "')");
                    this._SampleScreensListContainer.Controls.Add(courseSampleScreenPreviewContainer);

                    Panel courseSampleScreenDeleteButtonContainer = new Panel();
                    courseSampleScreenDeleteButtonContainer.ID = "CourseSampleScreenDeleteButtonContainer_" + index.ToString();
                    courseSampleScreenDeleteButtonContainer.CssClass = "CourseSampleScreenDeleteButtonContainer";
                    courseSampleScreenPreviewContainer.Controls.Add(courseSampleScreenDeleteButtonContainer);

                    // delete sample screen button
                    Image deleteSampleScreenImage = new Image();
                    deleteSampleScreenImage.ID = "CourseSampleScreenDeleteButton_" + index.ToString();
                    deleteSampleScreenImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    deleteSampleScreenImage.CssClass = "SmallIcon";
                    deleteSampleScreenImage.Attributes.Add("onClick", "javascript:DeleteCourseSampleScreen('" + index.ToString() + "');");
                    deleteSampleScreenImage.Style.Add("cursor", "pointer");
                    courseSampleScreenDeleteButtonContainer.Controls.Add(deleteSampleScreenImage);

                    // increment the index
                    index++;
                }
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;
            bool settingsTabHasErrors = false;
            bool personnelTabHasErrors = false;            

            // TITLE - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._CourseTitle.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.CoursePropertiesContainer, "CourseTitle", _GlobalResources.Title + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // SHORT DESCRIPTION - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._CourseShortDescription.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.CoursePropertiesContainer, "CourseShortDescription", _GlobalResources.ShortDescription + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // ESTIMATED LENGTH - HOURS AND MINUTES MUST BE INTEGERS
            int hours;
            int minutes;

            if ((!String.IsNullOrWhiteSpace(this._CourseEstimatedTimeHours.Text) && !int.TryParse(this._CourseEstimatedTimeHours.Text, out hours)) ||
                (!String.IsNullOrWhiteSpace(this._CourseEstimatedTimeMinutes.Text) && !int.TryParse(this._CourseEstimatedTimeMinutes.Text, out minutes))
                )
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.CoursePropertiesContainer, "CourseEstimatedTime", _GlobalResources.EstimatedLength + " " + _GlobalResources.IsInvalid);
            }

            // COST - MUST BE DOUBLE
            if (this._EcommerceSettings.IsEcommerceSet)
            {
                double cost;

                if ((!String.IsNullOrWhiteSpace(this._CourseCost.Text) && !double.TryParse(this._CourseCost.Text, out cost)))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.CoursePropertiesContainer, "CourseCost", _GlobalResources.Cost + " " + _GlobalResources.IsInvalid);
                }
            }

            // CREDITS - MUST BE DOUBLE
            double credits;

            if ((!String.IsNullOrWhiteSpace(this._CourseCredits.Text) && !double.TryParse(this._CourseCredits.Text, out credits)))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.CoursePropertiesContainer, "CourseCredits", _GlobalResources.Credits + " " + _GlobalResources.IsInvalid);
            }

            // SOCIAL MEDIA - ALL MUST BE VALID URLs
            List<Library.Course.SocialMediaProperty> socialMediaItems = new JavaScriptSerializer().Deserialize<List<Library.Course.SocialMediaProperty>>(this._CourseSocialMediaItemsJSON.Value);

            foreach (Library.Course.SocialMediaProperty socialMediaProperty in socialMediaItems)
            {
                string fullUrl = socialMediaProperty.Protocol + socialMediaProperty.Href;

                if (!String.IsNullOrWhiteSpace(fullUrl) && !RegExValidation.ValidateUrl(fullUrl))
                {
                    isValid = false;
                    settingsTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.CoursePropertiesContainer, "CourseSocialMedia", _GlobalResources.SocialMedia + " " + _GlobalResources.ContainsOneOrMoreInvalidURLs);
                    break;
                }
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.CoursePropertiesContainer, "CourseProperties_Properties_TabLI"); }

            if (settingsTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.CoursePropertiesContainer, "CourseProperties_Settings_TabLI"); }

            if (personnelTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.CoursePropertiesContainer, "CourseProperties_Personnel_TabLI"); }

            // if this is invalid, we need to pre-populate multi-select and course screens fields with the objects that were selected
            if (!isValid)
            {
                /* COURSE EXPERTS */

                if (!String.IsNullOrWhiteSpace(this._SelectedCourseExperts.Value))
                {
                    // split the "value" of the hidden field to get an array of course expert ids
                    string[] selectedCourseExpertsIds = this._SelectedCourseExperts.Value.Split(',');

                    // loop through the array and add each expert to the listing container
                    foreach (string courseExpertId in selectedCourseExpertsIds)
                    {
                        if (this._CourseExpertsListContainer.FindControl("Expert_" + courseExpertId) == null)
                        {
                            // user object
                            Asentia.UMS.Library.User userObject = new UMS.Library.User(Convert.ToInt32(courseExpertId));

                            // container
                            Panel userNameContainer = new Panel();
                            userNameContainer.ID = "Expert_" + userObject.Id.ToString();

                            // remove expert button
                            Image removeExpertImage = new Image();
                            removeExpertImage.ID = "Expert_" + userObject.Id.ToString() + "_RemoveImage";
                            removeExpertImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                            removeExpertImage.CssClass = "SmallIcon";
                            removeExpertImage.Attributes.Add("onClick", "javascript:RemoveExpertFromCourse('" + userObject.Id.ToString() + "');");
                            removeExpertImage.Style.Add("cursor", "pointer");

                            // expert name
                            Literal userName = new Literal();
                            userName.Text = userObject.DisplayName + "(" + userObject.Username + ")";

                            // add controls to container
                            userNameContainer.Controls.Add(removeExpertImage);
                            userNameContainer.Controls.Add(userName);
                            this._CourseExpertsListContainer.Controls.Add(userNameContainer);
                        }
                    }
                }

                /* ENROLLMENT APPROVERS */

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
                {
                    if (!String.IsNullOrWhiteSpace(this._SelectedEnrollmentApprovers.Value))
                    {
                        // split the "value" of the hidden field to get an array of course expert ids
                        string[] selectedEnrollmentApproversIds = this._SelectedEnrollmentApprovers.Value.Split(',');

                        // loop through the array and add each expert to the listing container
                        foreach (string enrollmentApproverId in selectedEnrollmentApproversIds)
                        {
                            if (this._EnrollmentApproversListContainer.FindControl("Approver_" + enrollmentApproverId) == null)
                            {
                                // user object
                                Asentia.UMS.Library.User userObject = new UMS.Library.User(Convert.ToInt32(enrollmentApproverId));

                                // container
                                Panel userNameContainer = new Panel();
                                userNameContainer.ID = "Approver_" + userObject.Id.ToString();

                                // remove approver button
                                Image removeApproverImage = new Image();
                                removeApproverImage.ID = "Approver_" + userObject.Id.ToString() + "_RemoveImage";
                                removeApproverImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                                removeApproverImage.CssClass = "SmallIcon";
                                removeApproverImage.Attributes.Add("onClick", "javascript:RemoveApproverFromCourse('" + userObject.Id.ToString() + "');");
                                removeApproverImage.Style.Add("cursor", "pointer");

                                // approver name
                                Literal userName = new Literal();
                                userName.Text = userObject.DisplayName + "(" + userObject.Username + ")";

                                // add controls to container
                                userNameContainer.Controls.Add(removeApproverImage);
                                userNameContainer.Controls.Add(userName);
                                this._EnrollmentApproversListContainer.Controls.Add(userNameContainer);
                            }
                        }
                    }
                }

                /* COURSE PREREQUISITES */

                if (!String.IsNullOrWhiteSpace(this._SelectedCoursePrerequisites.Value))
                {
                    // split the "value" of the hidden field to get an array of course prerequisite ids
                    string[] selectedCoursePrerequisitesIds = this._SelectedCoursePrerequisites.Value.Split(',');

                    // loop through the array and add each prerequisite to the listing container
                    foreach (string coursePrerequisiteId in selectedCoursePrerequisitesIds)
                    {
                        if (this._PrerequisitesListContainer.FindControl("Prerequisite_" + coursePrerequisiteId) == null)
                        {
                            // course object
                            Course courseObject = new Course(Convert.ToInt32(coursePrerequisiteId));

                            // container
                            Panel courseNameContainer = new Panel();
                            courseNameContainer.ID = "Prerequisite_" + courseObject.Id.ToString();

                            // remove prerequisite button
                            Image removePrerequisiteImage = new Image();
                            removePrerequisiteImage.ID = "Prerequisite_" + courseObject.Id.ToString() + "_RemoveImage";
                            removePrerequisiteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                            removePrerequisiteImage.CssClass = "SmallIcon";
                            removePrerequisiteImage.Attributes.Add("onClick", "javascript:RemovePrerequisiteFromCourse('" + courseObject.Id.ToString() + "');");
                            removePrerequisiteImage.Style.Add("cursor", "pointer");

                            // course name
                            Literal courseName = new Literal();
                            courseName.Text = courseObject.Title;

                            foreach (Course.LanguageSpecificProperty courseLanguageSpecificProperty in courseObject.LanguageSpecificProperties)
                            {
                                if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                                {
                                    courseName.Text = courseLanguageSpecificProperty.Title;
                                    break;
                                }
                            }

                            // add controls to container
                            courseNameContainer.Controls.Add(removePrerequisiteImage);
                            courseNameContainer.Controls.Add(courseName);
                            this._PrerequisitesListContainer.Controls.Add(courseNameContainer);
                        }
                    }
                }

                /* COURSE SAMPLE SCREENS */

                if (!String.IsNullOrWhiteSpace(this._UploadedCourseSampleScreens.Value))
                {
                    // split the "value" of the hidden field to get an array of course prerequisite ids
                    string[] uploadedSampleScreensFileNames = this._UploadedCourseSampleScreens.Value.Split(',');

                    // loop through the array and add each sample screen to the listing container
                    int index = 0;
                    
                    foreach (string uploadedSampleScreensFileName in uploadedSampleScreensFileNames)
                    {
                        if (this._PrerequisitesListContainer.FindControl("CourseSampleScreenPreviewContainer_" + index.ToString()) == null)
                        {
                            Panel courseSampleScreenPreviewContainer = new Panel();
                            courseSampleScreenPreviewContainer.ID = "CourseSampleScreenPreviewContainer_" + index.ToString();
                            courseSampleScreenPreviewContainer.CssClass = "CourseSampleScreenPreviewContainer";
                            courseSampleScreenPreviewContainer.Attributes.Add("fileName", uploadedSampleScreensFileName);

                            // link the background image to the file's proper location
                            if (this._CourseObject != null)
                            {
                                if (File.Exists(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/samplescreens/" + uploadedSampleScreensFileName)))
                                { courseSampleScreenPreviewContainer.Style.Add("background-image", "url('" + SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/samplescreens/" + uploadedSampleScreensFileName + "')"); }
                                else
                                { courseSampleScreenPreviewContainer.Style.Add("background-image", "url('" + SitePathConstants.UPLOAD_AVATAR + uploadedSampleScreensFileName + "')"); }
                            }
                            else
                            { courseSampleScreenPreviewContainer.Style.Add("background-image", "url('" + SitePathConstants.UPLOAD_AVATAR + uploadedSampleScreensFileName + "')"); }
                            
                            this._SampleScreensListContainer.Controls.Add(courseSampleScreenPreviewContainer);

                            Panel courseSampleScreenDeleteButtonContainer = new Panel();
                            courseSampleScreenDeleteButtonContainer.ID = "CourseSampleScreenDeleteButtonContainer_" + index.ToString();
                            courseSampleScreenDeleteButtonContainer.CssClass = "CourseSampleScreenDeleteButtonContainer";
                            courseSampleScreenPreviewContainer.Controls.Add(courseSampleScreenDeleteButtonContainer);

                            // delete sample screen button
                            Image deleteSampleScreenImage = new Image();
                            deleteSampleScreenImage.ID = "CourseSampleScreenDeleteButton_" + index.ToString();
                            deleteSampleScreenImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                            deleteSampleScreenImage.CssClass = "SmallIcon";
                            deleteSampleScreenImage.Attributes.Add("onClick", "javascript:DeleteCourseSampleScreen('" + index.ToString() + "');");
                            deleteSampleScreenImage.Style.Add("cursor", "pointer");
                            courseSampleScreenDeleteButtonContainer.Controls.Add(deleteSampleScreenImage);
                        }

                        // increment the index
                        index++;
                    }
                }
            }

            return isValid;
        }
        #endregion

        #region _CoursePropertiesSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for Course Properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CoursePropertiesSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                // if there is no course object, create one
                if (this._CourseObject == null)
                { this._CourseObject = new Course(); }

                int id;

                // populate the object
                this._CourseObject.Title = this._CourseTitle.Text;

                if (!String.IsNullOrWhiteSpace(this._CourseCode.Text))
                { this._CourseObject.CourseCode = this._CourseCode.Text; }
                else
                { this._CourseObject.CourseCode = null; }
                
                if (!String.IsNullOrWhiteSpace(this._Shortcode.Text))
                { this._CourseObject.Shortcode = this._Shortcode.Text; }
                else
                { this._CourseObject.Shortcode = null; }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RATINGS_COURSE_ENABLE))
                {
                    if (this._EnableRatings.Checked)
                    { this._CourseObject.DisallowRating = false; }
                    else
                    { this._CourseObject.DisallowRating = true; }
                }

                if (this._EcommerceSettings.IsEcommerceSet)
                {
                    if (!String.IsNullOrWhiteSpace(this._CourseCost.Text))
                    { this._CourseObject.Cost = Convert.ToDouble(this._CourseCost.Text); }
                    else
                    { this._CourseObject.Cost = null; }
                }
                else
                { this._CourseObject.Cost = null; }

                if (!String.IsNullOrWhiteSpace(this._CourseCredits.Text))
                { this._CourseObject.Credits = Convert.ToDouble(this._CourseCredits.Text); }
                else
                { this._CourseObject.Credits = null; }

                this._CourseObject.ShortDescription = this._CourseShortDescription.Text;

                if (!String.IsNullOrWhiteSpace(this._CourseLongDescription.Text))
                { this._CourseObject.LongDescription = HttpUtility.HtmlDecode(this._CourseLongDescription.Text); }
                else
                { this._CourseObject.LongDescription = null; }

                if (!String.IsNullOrWhiteSpace(this._CourseObjectives.Text))
                { this._CourseObject.Objectives = HttpUtility.HtmlDecode(this._CourseObjectives.Text); }
                else
                { this._CourseObject.Objectives = null; }
                
                if (!String.IsNullOrWhiteSpace(this._CourseSearchTags.Text))
                { this._CourseObject.SearchTags = this._CourseSearchTags.Text; }
                else
                { this._CourseObject.SearchTags = null; }

                this._CourseObject.IsPublished = Convert.ToBoolean(this._CoursePublished.SelectedValue);
                this._CourseObject.IsClosed = Convert.ToBoolean(this._CourseClosed.SelectedValue);
                this._CourseObject.IsLocked = Convert.ToBoolean(this._CourseLocked.SelectedValue);

                // lesson ordering settings
                this._CourseObject.ForceLessonCompletionInOrder = this._CourseForceLessonCompletionInOrder.Checked;

                // if force completion in order is checked, the others are checked too
                if (this._CourseForceLessonCompletionInOrder.Checked)
                {
                    this._CourseObject.RequireFirstLessonToBeCompletedBeforeOthers = true;
                    this._CourseObject.LockLastLessonUntilOthersCompleted = true;
                }
                else
                {
                    this._CourseObject.RequireFirstLessonToBeCompletedBeforeOthers = this._CourseRequireFirstLessonToBeCompletedBeforeOthers.Checked;
                    this._CourseObject.LockLastLessonUntilOthersCompleted = this._CourseLockLastLessonUntilOthersCompleted.Checked;
                }

                if (!String.IsNullOrWhiteSpace(this._CourseEstimatedTimeHours.Text) || !String.IsNullOrWhiteSpace(this._CourseEstimatedTimeMinutes.Text))
                {
                    int totalMinutes = 0;

                    if (!String.IsNullOrWhiteSpace(this._CourseEstimatedTimeHours.Text))
                    { totalMinutes = (Convert.ToInt32(this._CourseEstimatedTimeHours.Text) * 60); }

                    if (!String.IsNullOrWhiteSpace(this._CourseEstimatedTimeMinutes.Text))
                    { totalMinutes += Convert.ToInt32(this._CourseEstimatedTimeMinutes.Text); }
                    
                    this._CourseObject.EstimatedLengthInMinutes = totalMinutes;
                }
                else
                { this._CourseObject.EstimatedLengthInMinutes = null; }

                this._CourseObject.IsPrerequisiteAny = Convert.ToBoolean(this._CoursePrerequisitesIsAny.SelectedValue);

                // self-enrollment settings

                // self-enrollment is one-time only
                this._CourseObject.SelfEnrollmentIsOneTimeOnly = this._SelfEnrollmentIsOneTimeOnly.Checked;

                // due
                if (this._SelfEnrollmentDue.NoneCheckBoxChecked)
                {
                    this._CourseObject.SelfEnrollmentDueInterval = null;
                    this._CourseObject.SelfEnrollmentDueTimeframe = null;
                }
                else
                {
                    this._CourseObject.SelfEnrollmentDueInterval = Convert.ToInt32(this._SelfEnrollmentDue.IntervalValue);
                    this._CourseObject.SelfEnrollmentDueTimeframe = this._SelfEnrollmentDue.TimeframeValue;
                }

                // expires from start
                if (this._SelfEnrollmentExpiresFromStart.NoneCheckBoxChecked)
                {
                    this._CourseObject.SelfEnrollmentExpiresFromStartInterval = null;
                    this._CourseObject.SelfEnrollmentExpiresFromStartTimeframe = null;
                }
                else
                {
                    this._CourseObject.SelfEnrollmentExpiresFromStartInterval = Convert.ToInt32(this._SelfEnrollmentExpiresFromStart.IntervalValue);
                    this._CourseObject.SelfEnrollmentExpiresFromStartTimeframe = this._SelfEnrollmentExpiresFromStart.TimeframeValue;
                }

                // expires from first launch
                if (this._SelfEnrollmentExpiresFromFirstLaunch.NoneCheckBoxChecked)
                {
                    this._CourseObject.SelfEnrollmentExpiresFromFirstLaunchInterval = null;
                    this._CourseObject.SelfEnrollmentExpiresFromFirstLaunchTimeframe = null;
                }
                else
                {
                    this._CourseObject.SelfEnrollmentExpiresFromFirstLaunchInterval = Convert.ToInt32(this._SelfEnrollmentExpiresFromFirstLaunch.IntervalValue);
                    this._CourseObject.SelfEnrollmentExpiresFromFirstLaunchTimeframe = this._SelfEnrollmentExpiresFromFirstLaunch.TimeframeValue;
                }

                // self-enrollment requires approval
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
                {
                    this._CourseObject.IsSelfEnrollmentApprovalRequired = this._IsSelfEnrollmentApprovalRequired.Checked;

                    // course experts approve self-enrollment
                    this._CourseObject.IsApprovalAllowedByCourseExperts = this._IsApprovalAllowedByCourseExperts.Checked;

                    // user supervisor approve self-enrollment
                    this._CourseObject.IsApprovalAllowedByUserSupervisor = this._IsApprovalAllowedByUserSupervisor.Checked;
                }

                // do course wall (feed) settings - only if feature is enabled
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE))
                {
                    if (Convert.ToBoolean(this._CourseWall.SelectedValue))
                    {
                        this._CourseObject.IsFeedActive = true;
                        this._CourseObject.IsFeedModerated = this._IsWallModerated.Checked;
                        this._CourseObject.IsFeedOpenSubscription = this._IsWallOpenView.Checked;
                    }
                    else
                    {
                        this._CourseObject.IsFeedActive = false;
                        this._CourseObject.IsFeedModerated = false;
                        this._CourseObject.IsFeedOpenSubscription = false;
                    }
                }

                // do social media properties
                this._CourseObject.SocialMediaProperties.Clear();

                List<Library.Course.SocialMediaProperty> socialMediaItems = new JavaScriptSerializer().Deserialize<List<Library.Course.SocialMediaProperty>>(this._CourseSocialMediaItemsJSON.Value);

                foreach (Library.Course.SocialMediaProperty socialMediaProperty in socialMediaItems)
                {
                    if (socialMediaProperty.IconType.Contains("facebook"))
                    { socialMediaProperty.IconType = "facebook"; }
                    else if (socialMediaProperty.IconType.Contains("linkedin"))
                    { socialMediaProperty.IconType = "linkedin"; }
                    else if (socialMediaProperty.IconType.Contains("twitter"))
                    { socialMediaProperty.IconType = "twitter"; }
                    else if (socialMediaProperty.IconType.Contains("google"))
                    { socialMediaProperty.IconType = "google"; }
                    else if (socialMediaProperty.IconType.Contains("youtube"))
                    { socialMediaProperty.IconType = "youtube"; }
                    else
                    { socialMediaProperty.IconType = "default"; }

                    this._CourseObject.SocialMediaProperties.Add(socialMediaProperty);
                }

                this._CourseObject.SerializeSocialMediaProperties();

                // do avatar if existing course, if its a new course, we'll do it after initial save
                bool isNewCourse = true;

                string avatarFilename = "";
                string avatarFilenameSmall = "";

                if (this._CourseObject.Id > 0)
                {
                    isNewCourse = false;
                    
                    if (this._CourseAvatar.SavedFilePath != null)
                    {
                        // check user folder existence and create if necessary
                        if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id)))
                        { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id)); }

                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._CourseAvatar.SavedFilePath)))
                        {
                            avatarFilename = "avatar" + Path.GetExtension(this._CourseAvatar.SavedFilePath);
                            avatarFilenameSmall = "avatar.small" + Path.GetExtension(this._CourseAvatar.SavedFilePath);

                            fullSavedFilePath = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + avatarFilename;
                            fullSavedFilePathSmall = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + avatarFilenameSmall;

                            // delete existing avatar images if any
                            if (this._CourseObject.Avatar != null)
                            {
                                // find avatar images with "avatar" in the filename
                                string avatarImageFilename = @"avatar*" + Path.GetExtension(this._CourseObject.Avatar);
                                string[] avatarImageList = Directory.GetFiles(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id), avatarImageFilename).Select(file => Path.GetFileName(file)).ToArray();
                                
                                foreach (string file in avatarImageList)
                                {
                                    File.Delete(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + file));
                                }
                                
                            }

                            // move the uploaded file into the course's folder
                            File.Copy(Server.MapPath(this._CourseAvatar.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._CourseAvatar.SavedFilePath));
                            this._CourseObject.Avatar = avatarFilename;

                            // create a smaller version of the avatar fo use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            this._CourseObject.SaveAvatar();
                        }
                        else
                        {
                            this._CourseObject.Avatar = null;
                        }
                    }
                    else if (this._ClearAvatar != null)
                    {
                        if (this._ClearAvatar.Value == "true")
                        {
                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + "avatar" + Path.GetExtension(this._CourseObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + "avatar" + Path.GetExtension(this._CourseObject.Avatar))); }

                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + "avatar.small" + Path.GetExtension(this._CourseObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + "avatar.small" + Path.GetExtension(this._CourseObject.Avatar))); }

                            this._CourseObject.Avatar = null;

                            this._CourseObject.SaveAvatar();
                        }
                    }
                    else
                    { }
                }

                // save the course, save its returned id to viewstate, and set the current course object's id
                id = this._CourseObject.Save();
                this.ViewState["id"] = id;
                this._CourseObject.Id = id;

                // if this was a new course we just saved, we now have the id so that
                // we can create the course's folder and update the avatar if necessary.
                if (isNewCourse)
                {
                    // check course folder existence and create if necessary
                    if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id)))
                    { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id)); }

                    // if there is an avatar, move it and update the database record
                    if (this._CourseAvatar.SavedFilePath != null)
                    {
                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._CourseAvatar.SavedFilePath)))
                        {
                            avatarFilename = "avatar" + Path.GetExtension(this._CourseAvatar.SavedFilePath);
                            avatarFilenameSmall = "avatar.small" + Path.GetExtension(this._CourseAvatar.SavedFilePath);

                            fullSavedFilePath = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + "avatar" + Path.GetExtension(this._CourseAvatar.SavedFilePath);
                            fullSavedFilePathSmall = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + "avatar.small" + Path.GetExtension(this._CourseAvatar.SavedFilePath);

                            // move the uploaded file into the user's folder
                            File.Copy(Server.MapPath(this._CourseAvatar.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._CourseAvatar.SavedFilePath));
                            this._CourseObject.Avatar = "avatar" + Path.GetExtension(this._CourseAvatar.SavedFilePath);

                            // create a smaller version of the avatar for use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            this._CourseObject.SaveAvatar();
                        }
                    }
                }

                // do course language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string courseTitle = null;
                        string courseShortDescription = null;
                        string courseLongDescription = null;
                        string courseObjectives = null;
                        string courseSearchTags = null;

                        // get text boxes
                        TextBox languageSpecificCourseTitleTextBox = (TextBox)this.CoursePropertiesContainer.FindControl(this._CourseTitle.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificCourseShortDescriptionTextBox = (TextBox)this.CoursePropertiesContainer.FindControl(this._CourseShortDescription.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificCourseLongDescriptionTextBox = (TextBox)this.CoursePropertiesContainer.FindControl(this._CourseLongDescription.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificCourseObjectivesTextBox = (TextBox)this.CoursePropertiesContainer.FindControl(this._CourseObjectives.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificCourseSearchTagsTextBox = (TextBox)this.CoursePropertiesContainer.FindControl(this._CourseSearchTags.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificCourseTitleTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCourseTitleTextBox.Text))
                            { courseTitle = languageSpecificCourseTitleTextBox.Text; }
                        }

                        if (languageSpecificCourseShortDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCourseShortDescriptionTextBox.Text))
                            { courseShortDescription = languageSpecificCourseShortDescriptionTextBox.Text; }
                        }

                        if (languageSpecificCourseLongDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCourseLongDescriptionTextBox.Text))
                            { courseLongDescription = languageSpecificCourseLongDescriptionTextBox.Text; }
                        }

                        if (languageSpecificCourseObjectivesTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCourseObjectivesTextBox.Text))
                            { courseObjectives = languageSpecificCourseObjectivesTextBox.Text; }
                        }

                        if (languageSpecificCourseSearchTagsTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCourseSearchTagsTextBox.Text))
                            { courseSearchTags = languageSpecificCourseSearchTagsTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(courseTitle) ||
                            !String.IsNullOrWhiteSpace(courseShortDescription) ||
                            !String.IsNullOrWhiteSpace(courseLongDescription) ||
                            !String.IsNullOrWhiteSpace(courseObjectives) ||
                            !String.IsNullOrWhiteSpace(courseSearchTags))
                        {
                            this._CourseObject.SaveLang(cultureInfo.Name,
                                                        courseTitle,
                                                        courseShortDescription,
                                                        HttpUtility.HtmlDecode(courseLongDescription),
                                                        HttpUtility.HtmlDecode(courseObjectives),
                                                        courseSearchTags);
                        }
                    }
                }

                // do course experts

                // delcare data table
                DataTable courseExpertsToSave = new DataTable();
                courseExpertsToSave.Columns.Add("id", typeof(int));

                if (!String.IsNullOrWhiteSpace(this._SelectedCourseExperts.Value))
                {
                    // split the "value" of the hidden field to get an array of course expert ids
                    string[] selectedCourseExpertsIds = this._SelectedCourseExperts.Value.Split(',');

                    // put ids into datatable 
                    foreach (string courseExpertId in selectedCourseExpertsIds)
                    { courseExpertsToSave.Rows.Add(Convert.ToInt32(courseExpertId)); }
                }

                // save the course experts
                this._CourseObject.SaveExperts(courseExpertsToSave);

                // do enrollment approvers

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
                {
                    // delcare data table
                    DataTable enrollmentApproversToSave = new DataTable();
                    enrollmentApproversToSave.Columns.Add("id", typeof(int));

                    if (!String.IsNullOrWhiteSpace(this._SelectedEnrollmentApprovers.Value))
                    {
                        // split the "value" of the hidden field to get an array of course expert ids
                        string[] selectedEnrollmentApproverIds = this._SelectedEnrollmentApprovers.Value.Split(',');

                        // put ids into datatable 
                        foreach (string enrollmentApproverId in selectedEnrollmentApproverIds)
                        { enrollmentApproversToSave.Rows.Add(Convert.ToInt32(enrollmentApproverId)); }
                    }

                    // save the enrollment approvers
                    this._CourseObject.SaveApprovers(enrollmentApproversToSave);
                }

                // do course prerequisites

                // delcare data table
                DataTable coursePrerequisitesToSave = new DataTable();
                coursePrerequisitesToSave.Columns.Add("id", typeof(int));

                if (!String.IsNullOrWhiteSpace(this._SelectedCoursePrerequisites.Value))
                {
                    // split the "value" of the hidden field to get an array of course prerequisite ids
                    string[] selectedCoursePrerequisiteIds = this._SelectedCoursePrerequisites.Value.Split(',');

                    // put ids into datatable 
                    foreach (string selectedCoursePrerequisiteId in selectedCoursePrerequisiteIds)
                    { coursePrerequisitesToSave.Rows.Add(Convert.ToInt32(selectedCoursePrerequisiteId)); }
                }

                // save the course prerequisites
                this._CourseObject.SavePrerequisites(coursePrerequisitesToSave);

                // do course sample screens

                // delcare data table
                DataTable courseUploadedSampleScreens = new DataTable();
                courseUploadedSampleScreens.Columns.Add("filename", typeof(string));

                if (!String.IsNullOrWhiteSpace(this._UploadedCourseSampleScreens.Value))
                {
                    string courseSampleScreensDirectory = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/samplescreens";

                    // check course sample screens folder existence and create if necessary
                    if (!Directory.Exists(Server.MapPath(courseSampleScreensDirectory)))
                    { Directory.CreateDirectory(Server.MapPath(courseSampleScreensDirectory)); }

                    // split the "value" of the hidden field to get an array of course sample screens
                    string[] uploadedCourseSampleScreenFileNames = this._UploadedCourseSampleScreens.Value.Split(',');
                    
                    // delete files that need to be deleted if there are existing sample screens
                    if (this._CourseSampleScreens != null)
                    {
                        foreach (DataRow row in this._CourseSampleScreens.Rows)
                        {
                            if (!uploadedCourseSampleScreenFileNames.Any(s => s.Equals(row["filename"])) && File.Exists(Server.MapPath(courseSampleScreensDirectory + "/" + row["filename"])))
                            { File.Delete(Server.MapPath(courseSampleScreensDirectory + "/" + row["filename"])); }
                        }
                    }

                    // put uploaded files into datatable and move to the course's samplescreens directory
                    foreach (string fileName in uploadedCourseSampleScreenFileNames)
                    {
                        courseUploadedSampleScreens.Rows.Add(fileName);

                        string uploadedFilePath = SitePathConstants.UPLOAD_AVATAR + fileName;
                        string destinationFilePath = courseSampleScreensDirectory + "/" + fileName;

                        // move the uploaded file into the course's samplescreens folder
                        if (File.Exists(Server.MapPath(uploadedFilePath)))
                        {
                            // move the uploaded file into the user's folder
                            File.Copy(Server.MapPath(uploadedFilePath), Server.MapPath(destinationFilePath), true);
                            File.Delete(Server.MapPath(uploadedFilePath));                            
                        }
                    }
                }

                // save the course sample screens
                this._CourseObject.SaveSampleScreens(courseUploadedSampleScreens);

                // load the saved course object
                this._CourseObject = new Course(id);

                // clear controls for containers that have dynamically added elements
                this.CourseObjectMenuContainer.Controls.Clear();
                this._CourseSocialMediaItemsContainer.Controls.Clear();

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, _GlobalResources.CourseHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CoursePropertiesCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for Course Properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CoursePropertiesCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/courses");
        }
        #endregion

        #region _SaveLessonOrderingJsonDataStruct
        /// <summary>
        /// Data structure for lesson ordering web method return.
        /// </summary>
        public struct _SaveLessonOrderingJsonData
        {
            public bool actionSuccessful;
            public string html;
            public string exception;                        
        }
        #endregion

        #region SaveLessonOrdering
        [WebMethod(EnableSession = true)]
        /// <summary>
        /// Web method to save lesson ordering after drag and drop.
        /// </summary>
        public static _SaveLessonOrderingJsonData SaveLessonOrdering(string lessonOrdering)
        {
            _SaveLessonOrderingJsonData jsonData = new _SaveLessonOrderingJsonData();

            try
            {
                if (String.IsNullOrWhiteSpace(lessonOrdering))
                { throw new Exception(""); }

                DataTable lessonIdsWithOrdering = new DataTable();
                lessonIdsWithOrdering.Columns.Add("id", typeof(int));
                lessonIdsWithOrdering.Columns.Add("order", typeof(int));

                string[] lessonOrderingInputItems = lessonOrdering.Split('|');

                int ordinal = 1;

                for (int i = 0; i < lessonOrderingInputItems.Length; i++)
                {
                    lessonIdsWithOrdering.Rows.Add(Convert.ToInt32(lessonOrderingInputItems[i]), ordinal);
                    ordinal++;
                }

                Lesson.UpdateOrder(lessonIdsWithOrdering);

                jsonData.actionSuccessful = true;
                jsonData.html = String.Empty;
                jsonData.exception = String.Empty;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;                

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region _BuildLessonGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on the Lesson Grid data.
        /// </summary>
        private void _BuildLessonGridActionsModal()
        {
            this._LessonGridConfirmAction = new ModalPopup("LessonGridConfirmAction");

            // set modal properties
            this._LessonGridConfirmAction.Type = ModalPopupType.Confirm;
            this._LessonGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._LessonGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._LessonGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedModule_s;
            this._LessonGridConfirmAction.TargetControlID = this._LessonGridDeleteButton.ClientID;
            this._LessonGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._LessonGridDeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "LessonGridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseModule_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._LessonGridConfirmAction.AddControlToBody(body1Wrapper);

            this.CoursePropertiesActionsPanel.Controls.Add(this._LessonGridConfirmAction);
        }
        #endregion

        #region _LessonGridDeleteButton_Command
        /// <summary>
        /// Performs the delete action on Lesson Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _LessonGridDeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._LessonGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._LessonGrid.Rows[i].FindControl(this._LessonGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Lesson.Delete(recordsToDelete);

                    // display the success message                    
                    this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, _GlobalResources.TheSelectedModule_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message                    
                    this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, _GlobalResources.NoModule_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {                
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, ex.Message, true);
            }
            finally
            {                                
                // clear controls for containers that have dynamically added elements
                this.CourseObjectMenuContainer.Controls.Clear();
                this._CourseSocialMediaItemsContainer.Controls.Clear();

                // build the page controls
                this._BuildControls();
            }
        }
        #endregion

        #region _BuildCourseMaterialGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on the Course Material Grid data.
        /// </summary>
        private void _BuildCourseMaterialGridActionsModal()
        {
            this._CourseMaterialGridConfirmAction = new ModalPopup("CourseMaterialGridConfirmAction");

            // set modal properties
            this._CourseMaterialGridConfirmAction.Type = ModalPopupType.Confirm;
            this._CourseMaterialGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._CourseMaterialGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._CourseMaterialGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedCourseMaterial_s;
            this._CourseMaterialGridConfirmAction.TargetControlID = this._CourseMaterialGridDeleteButton.ClientID;
            this._CourseMaterialGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._CourseMaterialGridDeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "CourseMaterialGridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseCourseMaterial_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._CourseMaterialGridConfirmAction.AddControlToBody(body1Wrapper);

            this.CoursePropertiesActionsPanel.Controls.Add(this._CourseMaterialGridConfirmAction);
        }
        #endregion

        #region _CourseMaterialGridDeleteButton_Command
        /// <summary>
        /// Performs the delete action on Course Material Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _CourseMaterialGridDeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._CourseMaterialGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._CourseMaterialGrid.Rows[i].FindControl(this._CourseMaterialGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    DocumentRepositoryItem.Delete(recordsToDelete);

                    // display the success message                    
                    this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, _GlobalResources.TheSelectedCourseMaterial_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message                    
                    this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, _GlobalResources.NoCourseMaterial_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CoursePropertiesFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // clear controls for containers that have dynamically added elements
                this.CourseObjectMenuContainer.Controls.Clear();
                this._CourseSocialMediaItemsContainer.Controls.Clear();

                // build the page controls
                this._BuildControls();
            }
        }
        #endregion

        #region _BuildCourseMaterialMakePrivateConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the make private action performed on a course material.
        /// </summary>
        private void _BuildCourseMaterialMakePrivateConfirmationModal()
        {
            // set modal properties
            this._CourseMaterialMakePrivateConfirmationModal = new ModalPopup("CourseMaterialMakePrivateConfirmationModal");
            this._CourseMaterialMakePrivateConfirmationModal.Type = ModalPopupType.Form;
            this._CourseMaterialMakePrivateConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG);
            this._CourseMaterialMakePrivateConfirmationModal.HeaderIconAlt = _GlobalResources.MakeCourseMaterialPrivate;
            this._CourseMaterialMakePrivateConfirmationModal.HeaderText = _GlobalResources.MakeCourseMaterialPrivate;
            this._CourseMaterialMakePrivateConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._CourseMaterialMakePrivateConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._CourseMaterialMakePrivateConfirmationModal.TargetControlID = this._CourseMaterialMakePrivateConfirmationModalLaunchButton.ID;
            this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._CourseMaterialMakePrivateSubmit_Command);

            // build the modal form panel
            Panel courseMaterialMakePrivateConfirmationModalFormPanel = new Panel();
            courseMaterialMakePrivateConfirmationModalFormPanel.ID = "CourseMaterialMakePrivateConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmCourseMaterialMakePrivateActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToMakeThisCourseMaterialPrivate;

            body1Wrapper.Controls.Add(body1);
            courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the data hidden field
            HiddenField courseMaterialMakePrivateData = new HiddenField();
            courseMaterialMakePrivateData.ID = "CourseMaterialMakePrivateData";

            // build the modal body
            this._CourseMaterialMakePrivateConfirmationModal.AddControlToBody(courseMaterialMakePrivateConfirmationModalFormPanel);
            this._CourseMaterialMakePrivateConfirmationModal.AddControlToBody(this._CourseMaterialMakePrivateConfirmationModalLoadButton);
            this._CourseMaterialMakePrivateConfirmationModal.AddControlToBody(courseMaterialMakePrivateData);

            // add modal to container
            this.CoursePropertiesActionsPanel.Controls.Add(this._CourseMaterialMakePrivateConfirmationModal);
        }
        #endregion

        #region _LoadCourseMaterialMakePrivateConfirmationModalContent
        /// <summary>
        /// Loads content for course material make private confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadCourseMaterialMakePrivateConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel courseMaterialMakePrivateConfirmationModalFormPanel = (Panel)this._CourseMaterialMakePrivateConfirmationModal.FindControl("CourseMaterialMakePrivateConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._CourseMaterialMakePrivateConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = true;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(dEx.Message, true);
                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(ex.Message, true);
                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _CourseMaterialMakePrivateSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the course material make private confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CourseMaterialMakePrivateSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            UpdatePanel courseMaterialGridUpdatePanel = (UpdatePanel)this.CoursePropertiesTabPanelsContainer.FindControl("CourseProperties_CourseMaterials_TabPanel");
            HiddenField courseMaterialMakePrivateData = (HiddenField)this._CourseMaterialMakePrivateConfirmationModal.FindControl("CourseMaterialMakePrivateData");
            Panel courseMaterialMakePrivateConfirmationModalFormPanel = (Panel)this._CourseMaterialMakePrivateConfirmationModal.FindControl("CourseMaterialMakePrivateConfirmationModalFormPanel");            

            try
            {
                // clear the modal feedback
                this._CourseMaterialMakePrivateConfirmationModal.ClearFeedback();

                // get the document repository item id
                int idDocumentRepositoryItem = Convert.ToInt32(courseMaterialMakePrivateData.Value);

                // make the course material private
                DocumentRepositoryItem documentRepositoryItemObject = new DocumentRepositoryItem(idDocumentRepositoryItem);
                documentRepositoryItemObject.IsPrivate = true;
                documentRepositoryItemObject.Save();

                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(_GlobalResources.TheCourseMaterialHasBeenMarkedAsPrivate, false);

                // rebind the course material grid and update
                this._CourseMaterialGrid.BindData();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(dEx.Message, true);
                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CourseMaterialMakePrivateConfirmationModal.DisplayFeedback(ex.Message, true);
                courseMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildCourseMaterialMakePublicConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the make public action performed on a course material.
        /// </summary>
        private void _BuildCourseMaterialMakePublicConfirmationModal()
        {
            // set modal properties
            this._CourseMaterialMakePublicConfirmationModal = new ModalPopup("CourseMaterialMakePublicConfirmationModal");
            this._CourseMaterialMakePublicConfirmationModal.Type = ModalPopupType.Form;
            this._CourseMaterialMakePublicConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG);
            this._CourseMaterialMakePublicConfirmationModal.HeaderIconAlt = _GlobalResources.MakeCourseMaterialPublic;
            this._CourseMaterialMakePublicConfirmationModal.HeaderText = _GlobalResources.MakeCourseMaterialPublic;
            this._CourseMaterialMakePublicConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._CourseMaterialMakePublicConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._CourseMaterialMakePublicConfirmationModal.TargetControlID = this._CourseMaterialMakePublicConfirmationModalLaunchButton.ID;
            this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._CourseMaterialMakePublicSubmit_Command);

            // build the modal form panel
            Panel courseMaterialMakePublicConfirmationModalFormPanel = new Panel();
            courseMaterialMakePublicConfirmationModalFormPanel.ID = "CourseMaterialMakePublicConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmCourseMaterialMakePublicActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToMakeThisCourseMaterialPublic;

            body1Wrapper.Controls.Add(body1);
            courseMaterialMakePublicConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the data hidden field
            HiddenField courseMaterialMakePublicData = new HiddenField();
            courseMaterialMakePublicData.ID = "CourseMaterialMakePublicData";

            // build the modal body
            this._CourseMaterialMakePublicConfirmationModal.AddControlToBody(courseMaterialMakePublicConfirmationModalFormPanel);
            this._CourseMaterialMakePublicConfirmationModal.AddControlToBody(this._CourseMaterialMakePublicConfirmationModalLoadButton);
            this._CourseMaterialMakePublicConfirmationModal.AddControlToBody(courseMaterialMakePublicData);

            // add modal to container
            this.CoursePropertiesActionsPanel.Controls.Add(this._CourseMaterialMakePublicConfirmationModal);
        }
        #endregion

        #region _LoadCourseMaterialMakePublicConfirmationModalContent
        /// <summary>
        /// Loads content for course material make public confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadCourseMaterialMakePublicConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel courseMaterialMakePublicConfirmationModalFormPanel = (Panel)this._CourseMaterialMakePublicConfirmationModal.FindControl("CourseMaterialMakePublicConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._CourseMaterialMakePublicConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = true;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(dEx.Message, true);
                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(ex.Message, true);
                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _CourseMaterialMakePublicSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the course material make public confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CourseMaterialMakePublicSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            UpdatePanel courseMaterialGridUpdatePanel = (UpdatePanel)this.CoursePropertiesTabPanelsContainer.FindControl("CourseProperties_CourseMaterials_TabPanel");
            HiddenField courseMaterialMakePublicData = (HiddenField)this._CourseMaterialMakePublicConfirmationModal.FindControl("CourseMaterialMakePublicData");
            Panel courseMaterialMakePublicConfirmationModalFormPanel = (Panel)this._CourseMaterialMakePublicConfirmationModal.FindControl("CourseMaterialMakePublicConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._CourseMaterialMakePublicConfirmationModal.ClearFeedback();

                // get the document repository item id
                int idDocumentRepositoryItem = Convert.ToInt32(courseMaterialMakePublicData.Value);

                // make the course material private
                DocumentRepositoryItem documentRepositoryItemObject = new DocumentRepositoryItem(idDocumentRepositoryItem);
                documentRepositoryItemObject.IsPrivate = false;
                documentRepositoryItemObject.Save();

                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(_GlobalResources.TheCourseMaterialHasBeenMarkedAsPublic, false);

                // rebind the course material grid and update
                this._CourseMaterialGrid.BindData();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(dEx.Message, true);
                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CourseMaterialMakePublicConfirmationModal.DisplayFeedback(ex.Message, true);
                courseMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._CourseMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._CourseMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region Lesson Modify
        #region _BuildLessonModifyModal
        /// <summary>
        /// Builds the lesson modify modal
        /// </summary>
        private void _BuildLessonModifyModal()
        {
            // set modal properties
            this._LessonModifyModal = new ModalPopup("LessonModifyModal");
            this._LessonModifyModal.Type = ModalPopupType.Form;
            this._LessonModifyModal.UseConditionalUpdateModeForUpdatePanel = true;
            this._LessonModifyModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LESSON, ImageFiles.EXT_PNG);
            this._LessonModifyModal.HeaderIconAlt = _GlobalResources.Module;
            this._LessonModifyModal.HeaderText = _GlobalResources.NewModule;
            this._LessonModifyModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._LessonModifyModal.SubmitButtonCustomText = _GlobalResources.CreateModule;
            this._LessonModifyModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._LessonModifyModal.TargetControlID = this._LessonModifyModalLaunchButton.ID;
            this._LessonModifyModal.SubmitButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicLessonElements();");
            this._LessonModifyModal.SubmitButton.Command += new CommandEventHandler(this._LessonModifySubmit_Command);            

            // build and format a page information panel with instructions
            Panel lessonPropertiesInstructionsPanel = new Panel();
            lessonPropertiesInstructionsPanel.ID = "LessonModifyModal_LessonPropertiesInstructionsPanel";
            this.FormatPageInformationPanel(lessonPropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisModule, true);
            this._LessonModifyModal.AddControlToBody(lessonPropertiesInstructionsPanel);

            // build the lesson properties form tabs
            this._BuildLessonPropertiesFormTabs();

            // build the tab panels container
            Panel lessonPropertiesTabPanelsContainer = new Panel();
            lessonPropertiesTabPanelsContainer.ID = "LessonModifyModal_LessonProperties_TabPanelsContainer";
            lessonPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this._LessonModifyModal.AddControlToBody(lessonPropertiesTabPanelsContainer);

            // build the lesson properties tab content
            lessonPropertiesTabPanelsContainer.Controls.Add(this._BuildLessonPropertiesFormPropertiesPanel());

            // build the lesson content tab content
            lessonPropertiesTabPanelsContainer.Controls.Add(this._BuildLessonPropertiesFormContentPanel());
            
            // build the lesson data hidden field - this will hold the lesson id of the lesson being edited
            this._LessonModifyData = new HiddenField();
            this._LessonModifyData.ID = "LessonModifyData";

            // add the load button and hidden input to the modal                        
            this._LessonModifyModal.AddControlToBody(this._LessonModifyModalLoadButton);
            this._LessonModifyModal.AddControlToBody(this._LessonModifyData);

            // add modal to container
            this.CoursePropertiesActionsPanel.Controls.Add(this._LessonModifyModal);            
        }
        #endregion

        #region _BuildLessonPropertiesFormTabs
        /// <summary>
        /// Method to build lesson properties form tabs
        /// </summary>
        private void _BuildLessonPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("Content", _GlobalResources.Content));

            // build and attach the tabs
            this._LessonModifyModal.AddControlToBody(AsentiaPage.BuildTabListPanel("LessonModifyModal_LessonProperties", tabs, null, this.Page));
        }
        #endregion

        #region _BuildLessonPropertiesFormPropertiesPanel
        /// <summary>
        /// Method to build lesson properties form panel controls
        /// </summary>
        private Panel _BuildLessonPropertiesFormPropertiesPanel()
        {
            // build the modal form panel
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "LessonModifyModal_LessonProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.CssClass = "TabPanelContainer";
            propertiesPanel.Attributes.Add("style", "display: block;");                        

            // lesson revcode field
            this._LessonRevCode = new TextBox();
            this._LessonRevCode.ID = "LessonModifyModal_LessonRevCode_Field";
            this._LessonRevCode.CssClass = "InputLong";
            this._LessonRevCode.Enabled = false; // read-only

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LessonModifyModal_LessonRevCode",
                                                             _GlobalResources.RevisionCode,
                                                             this._LessonRevCode.ID,
                                                             this._LessonRevCode,
                                                             false,
                                                             true,
                                                             false));

            // lesson title field
            this._LessonTitle = new TextBox();
            this._LessonTitle.ID = "LessonModifyModal_LessonTitle_Field";
            this._LessonTitle.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LessonModifyModal_LessonTitle",
                                                             _GlobalResources.Title,
                                                             this._LessonTitle.ID,
                                                             this._LessonTitle,
                                                             true,
                                                             true,
                                                             true));

            // is lesson optional
            this._LessonOptional = new CheckBox();
            this._LessonOptional.ID = "LessonModifyModal_LessonOptional_Field";
            this._LessonOptional.Text = _GlobalResources.ThisModuleIsOptionalForCourseCompletion;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LessonModifyModal_LessonOptional",
                                                            _GlobalResources.Options,
                                                            this._LessonOptional.ID,
                                                            this._LessonOptional,
                                                            false,
                                                            false,
                                                            false));

            // lesson short description field
            this._LessonShortDescription = new TextBox();
            this._LessonShortDescription.ID = "LessonModifyModal_LessonShortDescription_Field";
            this._LessonShortDescription.Style.Add("width", "98%");
            this._LessonShortDescription.TextMode = TextBoxMode.MultiLine;
            this._LessonShortDescription.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LessonModifyModal_LessonShortDescription",
                                                             _GlobalResources.ShortDescription,
                                                             this._LessonShortDescription.ID,
                                                             this._LessonShortDescription,
                                                             true,
                                                             true,
                                                             true));

            // lesson long description field
            this._LessonLongDescription = new TextBox();
            this._LessonLongDescription.ID = "LessonModifyModal_LessonLongDescription_Field";
            this._LessonLongDescription.CssClass = "ckeditor";
            this._LessonLongDescription.Style.Add("width", "98%");
            this._LessonLongDescription.TextMode = TextBoxMode.MultiLine;
            this._LessonLongDescription.Rows = 10;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LessonModifyModal_LessonLongDescription",
                                                             _GlobalResources.Description,
                                                             this._LessonLongDescription.ID,
                                                             this._LessonLongDescription,
                                                             false,
                                                             true,
                                                             true));

            // lesson search tags field
            this._LessonSearchTags = new TextBox();
            this._LessonSearchTags.ID = "LessonModifyModal_LessonSearchTags_Field";
            this._LessonSearchTags.Style.Add("width", "98%");
            this._LessonSearchTags.TextMode = TextBoxMode.MultiLine;
            this._LessonSearchTags.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LessonModifyModal_LessonSearchTags",
                                                             _GlobalResources.SearchTags,
                                                             this._LessonSearchTags.ID,
                                                             this._LessonSearchTags,
                                                             false,
                                                             false,
                                                             true));

            // return the properties panel
            return propertiesPanel;
        }
        #endregion

        #region _BuildLessonPropertiesFormContentPanel
        /// <summary>
        /// Builds the content selection panel for the lesson.
        /// </summary>
        private Panel _BuildLessonPropertiesFormContentPanel()
        {
            Panel contentPanel = new Panel();
            contentPanel.ID = "LessonModifyModal_LessonProperties_" + "Content" + "_TabPanel";
            contentPanel.CssClass = "TabPanelContainer";
            contentPanel.Attributes.Add("style", "display: none;");

            // build "content change warning" panel, we'll show it when modifying an existing lesson
            Panel contentChangeWarningPanel = new Panel();
            contentChangeWarningPanel.ID = "LessonModifyModal_LessonPropertiesContentChangeWarning";
            this.FormatSectionInformationPanel(contentChangeWarningPanel, _GlobalResources.ChangingOrRemovingContentWillResetModuleDataForLearnersWhoAreStillIncompleteInProgressForThisModule, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));
            contentChangeWarningPanel.Style.Add("display", "none");
            contentPanel.Controls.Add(contentChangeWarningPanel);

            // build an instructions panel
            Panel contentInstructionsPanel = new Panel();
            contentInstructionsPanel.ID = "LessonModifyModal_LessonPropertiesContentInstructions";
            this.FormatSectionInformationPanel(contentInstructionsPanel, _GlobalResources.SelectOneOrMoreOfTheFollowingContentTypesForTheModuleLearnersWillBeRequiredToCompleteOne);
            contentPanel.Controls.Add(contentInstructionsPanel);

            // build a container and error panel so that we can show a message if no content methods are selected
            Panel lessonContentTypesContainerPanel = new Panel();
            lessonContentTypesContainerPanel.ID = "LessonModifyModal_LessonContentTypes_Container";
            lessonContentTypesContainerPanel.CssClass = "FormFieldContainer";
            contentPanel.Controls.Add(lessonContentTypesContainerPanel);

            Panel lessonContentTypesErrorPanel = new Panel();
            lessonContentTypesErrorPanel.ID = "LessonModifyModal_LessonContentTypes_ErrorContainer";
            lessonContentTypesErrorPanel.CssClass = "FormFieldErrorContainer";
            lessonContentTypesContainerPanel.Controls.Add(lessonContentTypesErrorPanel);

            // OVERRIDE LESSON DATA RESET FIELD

            List<Control> overrideLessonDataResetInputControls = new List<Control>();

            Panel overrideContentChangeLessonDataInfoPanel = new Panel();
            overrideContentChangeLessonDataInfoPanel.ID = "LessonModifyModal_OverrideContentChangeLessonDataInfoPanel";

            List<string> contentChangeMessages = new List<string>();

            contentChangeMessages.Add(_GlobalResources.ThisShouldOnlyBeCheckedIfYouAreChangingAContentItemForThisModuleAndTheChangeIsMinor);
            contentChangeMessages.Add(_GlobalResources.ExampleYouAreChangingAContentPackageAndTheNewContentPackage);

            this.FormatFormInformationPanel(overrideContentChangeLessonDataInfoPanel, contentChangeMessages);
            overrideLessonDataResetInputControls.Add(overrideContentChangeLessonDataInfoPanel);            

            this._OverrideContentChangeLessonDataReset = new CheckBox();
            this._OverrideContentChangeLessonDataReset.ID = "LessonModifyModal_OverrideContentChangeLessonDataReset_Field";
            this._OverrideContentChangeLessonDataReset.Text = _GlobalResources.DoNotResetModuleDataForLearnersOnContentChange;
            overrideLessonDataResetInputControls.Add(this._OverrideContentChangeLessonDataReset);

            contentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LessonModifyModal_OverrideContentChangeLessonDataReset",
                                                                              _GlobalResources.OverrideModuleDataReset,
                                                                              overrideLessonDataResetInputControls,
                                                                              false,
                                                                              true));

            // CONTENT PACKAGE

            // populate datatable for content packages to go into select list
            this._ContentPackagesForSelectList = Asentia.LMS.Library.ContentPackage.IdsAndNamesForSelectList(null);

            // lesson content package field
            List<Control> lessonContentPackageInputControls = new List<Control>();

            // content package checkbox - to serve as label for the field - do not add to list of controls
            this._UseContentPackageCheckBox = new CheckBox();
            this._UseContentPackageCheckBox.ID = "LessonModifyModal_UseContentPackage_Field";
            this._UseContentPackageCheckBox.CssClass = "LessonContentTypeHeading";
            this._UseContentPackageCheckBox.Text = _GlobalResources.ContentPackage + ": ";
            this._UseContentPackageCheckBox.Attributes.Add("onclick", "UseContentPackageCheckBoxClick();");

            // content package details container
            Panel contentPackageDetailsContainer = new Panel();
            contentPackageDetailsContainer.ID = "LessonModifyModal_ContentPackage_DetailsContainer";
            contentPackageDetailsContainer.CssClass = "LessonContentTypeDetailsContainer";

            // content package error panel
            // building this is an exception to standard form error panel building because of the structure of these form fields
            Panel contentPackageErrorPanel = new Panel();
            contentPackageErrorPanel.ID = "LessonModifyModal_LessonContentPackage_ErrorContainer";
            contentPackageErrorPanel.CssClass = "FormFieldErrorContainer";
            contentPackageDetailsContainer.Controls.Add(contentPackageErrorPanel);

            // lesson content package hidden field
            this._SelectedContentPackage = new HiddenField();
            this._SelectedContentPackage.ID = "LessonModifyModal_SelectedContentPackage_Field";
            contentPackageDetailsContainer.Controls.Add(this._SelectedContentPackage);

            // build a container for the lesson content package listing
            this._LessonSelectedContentPackageContainer = new Panel();
            this._LessonSelectedContentPackageContainer.ID = "LessonModifyModal_LessonSelectedContentPackage_Container";

            // selected content package name - "None" is default
            Panel selectedContentPackageNameContainer = new Panel();
            selectedContentPackageNameContainer.ID = "LessonModifyModal_SelectedContentPackageNameContainer";
            Literal selectedContentPackageName = new Literal();
            selectedContentPackageName.Text = _GlobalResources.None;
            selectedContentPackageNameContainer.Controls.Add(selectedContentPackageName);
            this._LessonSelectedContentPackageContainer.Controls.Add(selectedContentPackageNameContainer);

            contentPackageDetailsContainer.Controls.Add(this._LessonSelectedContentPackageContainer);

            Panel lessonContentPackageButtonsPanel = new Panel();
            lessonContentPackageButtonsPanel.ID = "LessonModifyModal_ContentPackage_ButtonsPanel";

            // select content package button

            // link
            Image selectContentPackageImageForLink = new Image();
            selectContentPackageImageForLink.ID = "LessonModifyModal_LaunchSelectContentPackageModalImage";
            selectContentPackageImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG);
            selectContentPackageImageForLink.CssClass = "MediumIcon";

            Localize selectContentPackageTextForLink = new Localize();
            selectContentPackageTextForLink.Text = _GlobalResources.SelectContentPackage;

            HyperLink selectContentPackageLink = new HyperLink();
            selectContentPackageLink.ID = "LessonModifyModal_LaunchSelectContentPackageModal";
            selectContentPackageLink.CssClass = "ImageLink";
            selectContentPackageLink.NavigateUrl = "javascript: void(0);";
            selectContentPackageLink.Attributes.Add("onclick", "LaunchSelectContentPackageModal();");
            selectContentPackageLink.Controls.Add(selectContentPackageImageForLink);
            selectContentPackageLink.Controls.Add(selectContentPackageTextForLink);
            lessonContentPackageButtonsPanel.Controls.Add(selectContentPackageLink);

            // attach the buttons panel to the container
            contentPackageDetailsContainer.Controls.Add(lessonContentPackageButtonsPanel);

            lessonContentPackageInputControls.Add(contentPackageDetailsContainer);

            // build modal for selecting content package for the lesson
            this._BuildSelectContentPackageModal();

            contentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LessonModifyModal_LessonContentPackage",
                                                                                    this._UseContentPackageCheckBox,
                                                                                    lessonContentPackageInputControls,
                                                                                    false,
                                                                                    false)); // dont build an error panel because we build one above

            // INSTRUCTOR LED TRAINING

            // populate datatable for standup training modules to go into select list
            this._StandupTrainingModulesForSelectList = Asentia.LMS.Library.StandupTraining.IdsAndNamesForSelectList(null);

            // standup training module field
            List<Control> lessonStandupTrainingModuleInputControls = new List<Control>();

            // standup training module checkbox - to serve as label for the field - do not add to list of controls
            this._UseStandupTrainingModuleCheckBox = new CheckBox();
            this._UseStandupTrainingModuleCheckBox.ID = "LessonModifyModal_UseStandupTrainingModule_Field";
            this._UseStandupTrainingModuleCheckBox.CssClass = "LessonContentTypeHeading";
            this._UseStandupTrainingModuleCheckBox.Text = _GlobalResources.InstructorLedTrainingModule + ": ";
            this._UseStandupTrainingModuleCheckBox.Attributes.Add("onclick", "UseStandupTrainingModuleCheckBoxClick();");

            // standup training module details container
            Panel standupTrainingModuleDetailsContainer = new Panel();
            standupTrainingModuleDetailsContainer.ID = "LessonModifyModal_StandupTrainingModule_DetailsContainer";
            standupTrainingModuleDetailsContainer.CssClass = "LessonContentTypeDetailsContainer";

            // standup training module error panel
            // building this is an exception to standard form error panel building because of the structure of these form fields
            Panel standupTrainingModuleErrorPanel = new Panel();
            standupTrainingModuleErrorPanel.ID = "LessonModifyModal_LessonStandupTrainingModule_ErrorContainer";
            standupTrainingModuleErrorPanel.CssClass = "FormFieldErrorContainer";
            standupTrainingModuleDetailsContainer.Controls.Add(standupTrainingModuleErrorPanel);

            // lesson standup training module hidden field
            this._SelectedStandupTrainingModule = new HiddenField();
            this._SelectedStandupTrainingModule.ID = "LessonModifyModal_SelectedStandupTrainingModule_Field";
            standupTrainingModuleDetailsContainer.Controls.Add(this._SelectedStandupTrainingModule);

            // build a container for the lesson standup training module listing
            this._LessonSelectedStandupTrainingModuleContainer = new Panel();
            this._LessonSelectedStandupTrainingModuleContainer.ID = "LessonModifyModal_LessonSelectedStandupTrainingModule_Container";

            // selected standup training module name - "None" is default
            Panel selectedStandupTrainingModuleNameContainer = new Panel();
            selectedStandupTrainingModuleNameContainer.ID = "LessonModifyModal_SelectedStandupTrainingModuleNameContainer";
            Literal selectedStandupTrainingModuleName = new Literal();
            selectedStandupTrainingModuleName.Text = _GlobalResources.None;
            selectedStandupTrainingModuleNameContainer.Controls.Add(selectedStandupTrainingModuleName);
            this._LessonSelectedStandupTrainingModuleContainer.Controls.Add(selectedStandupTrainingModuleNameContainer);

            standupTrainingModuleDetailsContainer.Controls.Add(this._LessonSelectedStandupTrainingModuleContainer);

            Panel lessonStandupTrainingModuleButtonsPanel = new Panel();
            lessonStandupTrainingModuleButtonsPanel.ID = "LessonModifyModal_StandupTrainingModule_ButtonsPanel";

            // select standup training module button

            // link
            Image selectStandupTrainingModuleImageForLink = new Image();
            selectStandupTrainingModuleImageForLink.ID = "LessonModifyModal_LaunchSelectStandupTrainingModuleModalImage";
            selectStandupTrainingModuleImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION, ImageFiles.EXT_PNG);
            selectStandupTrainingModuleImageForLink.CssClass = "MediumIcon";

            Localize selectStandupTrainingModuleTextForLink = new Localize();
            selectStandupTrainingModuleTextForLink.Text = _GlobalResources.SelectInstructorLedTrainingModule;

            HyperLink selectStandupTrainingModuleLink = new HyperLink();
            selectStandupTrainingModuleLink.ID = "LessonModifyModal_LaunchSelectStandupTrainingModuleModal";
            selectStandupTrainingModuleLink.CssClass = "ImageLink";
            selectStandupTrainingModuleLink.NavigateUrl = "javascript: void(0);";
            selectStandupTrainingModuleLink.Attributes.Add("onclick", "LaunchSelectStandupTrainingModuleModal();");
            selectStandupTrainingModuleLink.Controls.Add(selectStandupTrainingModuleImageForLink);
            selectStandupTrainingModuleLink.Controls.Add(selectStandupTrainingModuleTextForLink);
            lessonStandupTrainingModuleButtonsPanel.Controls.Add(selectStandupTrainingModuleLink);

            // attach the buttons panel to the container
            standupTrainingModuleDetailsContainer.Controls.Add(lessonStandupTrainingModuleButtonsPanel);

            lessonStandupTrainingModuleInputControls.Add(standupTrainingModuleDetailsContainer);

            // build modal for selecting standup training module for the lesson
            this._BuildSelectStandupTrainingModuleModal();

            contentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LessonModifyModal_LessonStandupTrainingModule",
                                                                                    this._UseStandupTrainingModuleCheckBox,
                                                                                    lessonStandupTrainingModuleInputControls,
                                                                                    false,
                                                                                    false)); // dont build an error panel because we build one above

            // TASK

            // check to ensure task modules are enabled on the portal
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.TASKMODULES_ENABLE))
            {
                // task field
                List<Control> lessonTaskInputControls = new List<Control>();

                // task checkbox - to serve as label for the field - do not add to list of controls
                this._UseTaskCheckBox = new CheckBox();
                this._UseTaskCheckBox.ID = "LessonModifyModal_UseTask_Field";
                this._UseTaskCheckBox.CssClass = "LessonContentTypeHeading";
                this._UseTaskCheckBox.Text = _GlobalResources.Task + ": ";
                this._UseTaskCheckBox.Attributes.Add("onclick", "UseTaskCheckBoxClick();");

                // task details container
                Panel taskDetailsContainer = new Panel();
                taskDetailsContainer.ID = "LessonModifyModal_Task_DetailsContainer";
                taskDetailsContainer.CssClass = "LessonContentTypeDetailsContainer";

                // task error panel
                // building this is an exception to standard form error panel building because of the structure of these form fields
                Panel taskErrorPanel = new Panel();
                taskErrorPanel.ID = "LessonModifyModal_LessonTask_ErrorContainer";
                taskErrorPanel.CssClass = "FormFieldErrorContainer";
                taskDetailsContainer.Controls.Add(taskErrorPanel);

                // allowed document type label
                Panel taskAllowedDocumentTypeLabelContainer = new Panel();
                taskAllowedDocumentTypeLabelContainer.ID = "LessonModifyModal_TaskAllowedDocumentTypeLabelContainer";
                taskAllowedDocumentTypeLabelContainer.CssClass = "FormFieldLabelContainer";

                Literal taskAllowedDocumentTypeLabel = new Literal();
                taskAllowedDocumentTypeLabel.Text = _GlobalResources.DocumentType + ": ";

                taskAllowedDocumentTypeLabelContainer.Controls.Add(taskAllowedDocumentTypeLabel);
                taskDetailsContainer.Controls.Add(taskAllowedDocumentTypeLabelContainer);

                // allowed document type field
                this._TaskAllowedDocumentType = new RadioButtonList();
                this._TaskAllowedDocumentType.ID = "LessonModifyModal_TaskAllowedDocumentType_Field";
                this._TaskAllowedDocumentType.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Vertical;
                this._TaskAllowedDocumentType.Items.Add(new ListItem(_GlobalResources.AdobeAcrobatPDF, Convert.ToInt32(Lesson.TaskDocumentType.PDF).ToString()));
                this._TaskAllowedDocumentType.Items.Add(new ListItem(_GlobalResources.MicrosoftOfficeWordDOCDOCX, Convert.ToInt32(Lesson.TaskDocumentType.Word).ToString()));
                this._TaskAllowedDocumentType.Items.Add(new ListItem(_GlobalResources.MicrosoftOfficeExcelXLSXLSX, Convert.ToInt32(Lesson.TaskDocumentType.Excel).ToString()));
                this._TaskAllowedDocumentType.Items.Add(new ListItem(_GlobalResources.MicrosoftOfficePowerPointPPTPPTX, Convert.ToInt32(Lesson.TaskDocumentType.PowerPoint).ToString()));
                this._TaskAllowedDocumentType.Items.Add(new ListItem(_GlobalResources.TextTXT, Convert.ToInt32(Lesson.TaskDocumentType.Text).ToString()));
                this._TaskAllowedDocumentType.Items.Add(new ListItem(_GlobalResources.ImageJPEGJPGPNGGIF, Convert.ToInt32(Lesson.TaskDocumentType.Image).ToString()));
                this._TaskAllowedDocumentType.Items.Add(new ListItem(_GlobalResources.ZipZIP, Convert.ToInt32(Lesson.TaskDocumentType.Zip).ToString()));
                this._TaskAllowedDocumentType.Items.Add(new ListItem(_GlobalResources.VideoMP4, Convert.ToInt32(Lesson.TaskDocumentType.Mp4).ToString()));
                taskDetailsContainer.Controls.Add(this._TaskAllowedDocumentType);

                // uploaded resource label
                Panel taskUploadedResourceLabelContainer = new Panel();
                taskUploadedResourceLabelContainer.ID = "LessonModifyModal_TaskUploadedResourceLabelContainer";
                taskUploadedResourceLabelContainer.CssClass = "FormFieldLabelContainer";

                Literal taskUploadedResourceLabel = new Literal();
                taskUploadedResourceLabel.Text = _GlobalResources.TaskResource + ": ";

                taskUploadedResourceLabelContainer.Controls.Add(taskUploadedResourceLabel);
                taskDetailsContainer.Controls.Add(taskUploadedResourceLabelContainer);

                // build a container for the uploaded resource name
                this._LessonTaskUploadedResourceContainer = new Panel();
                this._LessonTaskUploadedResourceContainer.ID = "LessonModifyModal_LessonTaskUploadedResource_Container";

                // uploaded resource file name - "None" is default
                Panel uploadedResourceFileNameContainer = new Panel();
                uploadedResourceFileNameContainer.ID = "LessonModifyModal_UploadedResourceFileNameContainer";
                Literal uploadedResourceFileName = new Literal();
                uploadedResourceFileName.Text = _GlobalResources.None;
                uploadedResourceFileNameContainer.Controls.Add(uploadedResourceFileName);
                this._LessonTaskUploadedResourceContainer.Controls.Add(uploadedResourceFileNameContainer);

                taskDetailsContainer.Controls.Add(this._LessonTaskUploadedResourceContainer);

                // uploader error container
                Panel taskResourceUploaderErrorContainer = new Panel();
                taskResourceUploaderErrorContainer.ID = "LessonModifyModal_TaskUploadedResource_ErrorContainer";
                taskResourceUploaderErrorContainer.CssClass = "FormFieldErrorContainer";
                taskDetailsContainer.Controls.Add(taskResourceUploaderErrorContainer);

                // uploaded resource field
                this._TaskResourceUploader = new UploaderAsync("LessonModifyModal_TaskUploadedResource_Field", UploadType.LessonMaterial, "LessonModifyModal_TaskUploadedResource_ErrorContainer");
                this._TaskResourceUploader.ClientSideCompleteJSMethod = "TaskResourceUploaded";
                taskDetailsContainer.Controls.Add(this._TaskResourceUploader);

                // proctors label
                Panel taskProctorsLabelContainer = new Panel();
                taskProctorsLabelContainer.ID = "LessonModifyModal_TaskProctorsLabelContainer";
                taskProctorsLabelContainer.CssClass = "FormFieldLabelContainer";

                Literal taskProctorsLabel = new Literal();
                taskProctorsLabel.Text = _GlobalResources.Proctors + ": ";

                taskProctorsLabelContainer.Controls.Add(taskProctorsLabel);
                taskDetailsContainer.Controls.Add(taskProctorsLabelContainer);

                // allow supervisor as proctor field
                Panel taskAllowSupervisorAsProctorFieldContainer = new Panel();
                taskAllowSupervisorAsProctorFieldContainer.ID = "LessonModifyModal_TaskAllowSupervisorAsProctor_FieldContainer";

                this._TaskAllowSupervisorAsProctor = new CheckBox();
                this._TaskAllowSupervisorAsProctor.ID = "LessonModifyModal_TaskAllowSupervisorAsProctor_Field";
                this._TaskAllowSupervisorAsProctor.Text = _GlobalResources.AllowLearnersSupervisor_sToGradeAndOrMarkThisTaskAsCompleted;
                taskAllowSupervisorAsProctorFieldContainer.Controls.Add(this._TaskAllowSupervisorAsProctor);
                taskDetailsContainer.Controls.Add(taskAllowSupervisorAsProctorFieldContainer);

                // allow course expert as proctor field
                Panel taskAllowCourseExpertAsProctorFieldContainer = new Panel();
                taskAllowCourseExpertAsProctorFieldContainer.ID = "LessonModifyModal_TaskAllowCourseExpertAsProctor_FieldContainer";

                this._TaskAllowCourseExpertAsProctor = new CheckBox();
                this._TaskAllowCourseExpertAsProctor.ID = "LessonModifyModal_TaskAllowCourseExpertAsProctor_Field";
                this._TaskAllowCourseExpertAsProctor.Text = _GlobalResources.AllowExpert_sForThisCourseToGradeAndOrMarkThisTaskAsCompleted;
                taskAllowCourseExpertAsProctorFieldContainer.Controls.Add(this._TaskAllowCourseExpertAsProctor);
                taskDetailsContainer.Controls.Add(taskAllowCourseExpertAsProctorFieldContainer);

                lessonTaskInputControls.Add(taskDetailsContainer);

                contentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LessonModifyModal_LessonTask",
                                                                                         this._UseTaskCheckBox,
                                                                                         lessonTaskInputControls,
                                                                                         false,
                                                                                         false)); // dont build an error panel because we build one above
            }
       
            // OJT

            // check to ensure ojt modules are enabled on the portal
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OJTMODULES_ENABLE))
            {
                // populate datatable for content packages to go into select list
                this._OJTContentPackagesForSelectList = Asentia.LMS.Library.ContentPackage.IdsAndNamesForSelectList(null);

                // lesson content package field
                List<Control> lessonOJTContentPackageInputControls = new List<Control>();

                // ojt checkbox - to serve as label for the field - do not add to list of controls
                this._UseOJTCheckBox = new CheckBox();
                this._UseOJTCheckBox.ID = "LessonModifyModal_UseOJT_Field";
                this._UseOJTCheckBox.CssClass = "LessonContentTypeHeading";
                this._UseOJTCheckBox.Text = _GlobalResources.OJT + ": ";
                this._UseOJTCheckBox.Attributes.Add("onclick", "UseOJTCheckBoxClick();");

                // ojt details container
                Panel ojtDetailsContainer = new Panel();
                ojtDetailsContainer.ID = "LessonModifyModal_OJT_DetailsContainer";
                ojtDetailsContainer.CssClass = "LessonContentTypeDetailsContainer";

                // ojt error panel
                // building this is an exception to standard form error panel building because of the structure of these form fields
                Panel ojtErrorPanel = new Panel();
                ojtErrorPanel.ID = "LessonModifyModal_LessonOJTContentPackage_ErrorContainer";
                ojtErrorPanel.CssClass = "FormFieldErrorContainer";
                ojtDetailsContainer.Controls.Add(ojtErrorPanel);

                // lesson ojt content package hidden field
                this._SelectedOJTContentPackage = new HiddenField();
                this._SelectedOJTContentPackage.ID = "LessonModifyModal_SelectedOJTContentPackage_Field";
                ojtDetailsContainer.Controls.Add(this._SelectedOJTContentPackage);

                // build a container for the lesson ojt content package listing
                this._LessonSelectedOJTContentPackageContainer = new Panel();
                this._LessonSelectedOJTContentPackageContainer.ID = "LessonModifyModal_LessonSelectedOJTContentPackage_Container";

                // selected ojt content package name - "None" is default
                Panel selectedOJTContentPackageNameContainer = new Panel();
                selectedOJTContentPackageNameContainer.ID = "LessonModifyModal_SelectedOJTContentPackageNameContainer";
                Literal selectedOJTContentPackageName = new Literal();
                selectedOJTContentPackageName.Text = _GlobalResources.None;
                selectedOJTContentPackageNameContainer.Controls.Add(selectedOJTContentPackageName);
                this._LessonSelectedOJTContentPackageContainer.Controls.Add(selectedOJTContentPackageNameContainer);

                ojtDetailsContainer.Controls.Add(this._LessonSelectedOJTContentPackageContainer);

                Panel lessonOJTContentPackageButtonsPanel = new Panel();
                lessonOJTContentPackageButtonsPanel.ID = "LessonModifyModal_OJTContentPackage_ButtonsPanel";

                // select ojt content package button

                // link
                Image selectOJTContentPackageImageForLink = new Image();
                selectOJTContentPackageImageForLink.ID = "LessonModifyModal_LaunchSelectOJTContentPackageModalImage";
                selectOJTContentPackageImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG);
                selectOJTContentPackageImageForLink.CssClass = "MediumIcon";

                Localize selectOJTContentPackageTextForLink = new Localize();
                selectOJTContentPackageTextForLink.Text = _GlobalResources.SelectContentPackage;

                HyperLink selectOJTContentPackageLink = new HyperLink();
                selectOJTContentPackageLink.ID = "LessonModifyModal_LaunchSelectOJTContentPackageModal";
                selectOJTContentPackageLink.CssClass = "ImageLink";
                selectOJTContentPackageLink.NavigateUrl = "javascript: void(0);";
                selectOJTContentPackageLink.Attributes.Add("onclick", "LaunchSelectOJTContentPackageModal();");
                selectOJTContentPackageLink.Controls.Add(selectOJTContentPackageImageForLink);
                selectOJTContentPackageLink.Controls.Add(selectOJTContentPackageTextForLink);
                lessonOJTContentPackageButtonsPanel.Controls.Add(selectOJTContentPackageLink);

                // attach the buttons panel to the container
                ojtDetailsContainer.Controls.Add(lessonOJTContentPackageButtonsPanel);

                // proctors label
                Panel ojtProctorsLabelContainer = new Panel();
                ojtProctorsLabelContainer.ID = "LessonModifyModal_OJTProctorsLabelContainer";
                ojtProctorsLabelContainer.CssClass = "FormFieldLabelContainer";

                Literal ojtProctorsLabel = new Literal();
                ojtProctorsLabel.Text = _GlobalResources.Proctors + ": ";

                ojtProctorsLabelContainer.Controls.Add(ojtProctorsLabel);
                ojtDetailsContainer.Controls.Add(ojtProctorsLabelContainer);

                // allow supervisor as proctor field
                Panel ojtAllowSupervisorAsProctorFieldContainer = new Panel();
                ojtAllowSupervisorAsProctorFieldContainer.ID = "LessonModifyModal_OJTAllowSupervisorAsProctor_FieldContainer";

                this._OJTAllowSupervisorAsProctor = new CheckBox();
                this._OJTAllowSupervisorAsProctor.ID = "LessonModifyModal_OJTAllowSupervisorAsProctor_Field";
                this._OJTAllowSupervisorAsProctor.Text = _GlobalResources.AllowLearnersSupervisor_sToGradeAndOrMarkThisOJTPackageAsCompleted;
                ojtAllowSupervisorAsProctorFieldContainer.Controls.Add(this._OJTAllowSupervisorAsProctor);
                ojtDetailsContainer.Controls.Add(ojtAllowSupervisorAsProctorFieldContainer);

                // allow course expert as proctor field
                Panel ojtAllowCourseExpertAsProctorFieldContainer = new Panel();
                ojtAllowCourseExpertAsProctorFieldContainer.ID = "LessonModifyModal_OJTAllowCourseExpertAsProctor_FieldContainer";

                this._OJTAllowCourseExpertAsProctor = new CheckBox();
                this._OJTAllowCourseExpertAsProctor.ID = "LessonModifyModal_OJTAllowCourseExpertAsProctor_Field";
                this._OJTAllowCourseExpertAsProctor.Text = _GlobalResources.AllowExpert_sForThisCourseToGradeAndOrMarkThisOJTPackageAsCompleted;
                ojtAllowCourseExpertAsProctorFieldContainer.Controls.Add(this._OJTAllowCourseExpertAsProctor);
                ojtDetailsContainer.Controls.Add(ojtAllowCourseExpertAsProctorFieldContainer);

                lessonOJTContentPackageInputControls.Add(ojtDetailsContainer);

                // build modal for selecting content package for the lesson
                this._BuildSelectOJTContentPackageModal();

                contentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LessonModifyModal_LessonOJTContentPackage",
                                                                                        this._UseOJTCheckBox,
                                                                                        lessonOJTContentPackageInputControls,
                                                                                        false,
                                                                                        false)); // dont build an error panel because we build one above    
            }

            // return the content panel
            return contentPanel;
        }
        #endregion
        
        #region _BuildSelectContentPackageModal
        /// <summary>
        /// Builds the modal for selecting content package to for the lesson.
        /// </summary>
        private void _BuildSelectContentPackageModal()
        {            
            // set modal properties
            this._SelectContentPackageForLesson = new ModalPopup("LessonModifyModal_SelectContentPackageForLessonModal");
            this._SelectContentPackageForLesson.Type = ModalPopupType.Form;
            this._SelectContentPackageForLesson.UseConditionalUpdateModeForUpdatePanel = true;
            this._SelectContentPackageForLesson.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG);
            this._SelectContentPackageForLesson.HeaderIconAlt = _GlobalResources.SelectContentPackage;
            this._SelectContentPackageForLesson.HeaderText = _GlobalResources.SelectContentPackage;
            this._SelectContentPackageForLesson.TargetControlID = this._SelectContentPackageModalLaunchButton.ID;
            this._SelectContentPackageForLesson.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectContentPackageForLesson.SubmitButtonCustomText = _GlobalResources.SelectContentPackage;
            this._SelectContentPackageForLesson.SubmitButton.OnClientClick = "javascript:SelectContentPackageForLesson(); return false;";
            this._SelectContentPackageForLesson.CloseButtonTextType = ModalPopupButtonText.Cancel;
            this._SelectContentPackageForLesson.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectContentPackage = new DynamicListBox("LessonModifyModal_SelectContentPackage");
            this._SelectContentPackage.NoRecordsFoundMessage = _GlobalResources.NoContentPackagesFound;
            this._SelectContentPackage.IncludeSelectAllNone = false;
            this._SelectContentPackage.IsMultipleSelect = false;
            this._SelectContentPackage.SearchButton.Command += new CommandEventHandler(this._SearchSelectContentPackageButton_Command);
            this._SelectContentPackage.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectContentPackageButton_Command);
            this._SelectContentPackage.ListBoxControl.DataSource = this._ContentPackagesForSelectList;
            this._SelectContentPackage.ListBoxControl.DataTextField = "name";
            this._SelectContentPackage.ListBoxControl.DataValueField = "idContentPackage";
            this._SelectContentPackage.ListBoxControl.DataBind();

            // add the hidden input to the modal                                    
            this._SelectContentPackageForLesson.AddControlToBody(this._SelectContentPackage);

            // add modal to container
            this.CoursePropertiesActionsPanel.Controls.Add(this._SelectContentPackageForLesson);
        }
        #endregion

        #region _SearchSelectContentPackageButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Content Package" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectContentPackageButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectContentPackageForLesson.ClearFeedback();

            // clear the listbox control
            this._SelectContentPackage.ListBoxControl.Items.Clear();

            // do the search
            this._ContentPackagesForSelectList = Asentia.LMS.Library.ContentPackage.IdsAndNamesForSelectList(this._SelectContentPackage.SearchTextBox.Text);

            // bind the data
            this._SelectContentPackageDataBind();
        }
        #endregion

        #region _ClearSearchSelectContentPackageButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Content Package" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectContentPackageButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectContentPackageForLesson.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectContentPackage.ListBoxControl.Items.Clear();
            this._SelectContentPackage.SearchTextBox.Text = "";

            // clear the search
            this._ContentPackagesForSelectList = Asentia.LMS.Library.ContentPackage.IdsAndNamesForSelectList(null);

            // bind the data
            this._SelectContentPackageDataBind();
        }
        #endregion

        #region _SelectContentPackageDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectContentPackageDataBind()
        {
            this._SelectContentPackage.ListBoxControl.DataSource = this._ContentPackagesForSelectList;
            this._SelectContentPackage.ListBoxControl.DataTextField = "name";
            this._SelectContentPackage.ListBoxControl.DataValueField = "idContentPackage";
            this._SelectContentPackage.ListBoxControl.DataBind();

            // if no records available then disable the submit button
            if (this._SelectContentPackage.ListBoxControl.Items.Count == 0)
            {
                this._SelectContentPackageForLesson.SubmitButton.Enabled = false;
                this._SelectContentPackageForLesson.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectContentPackageForLesson.SubmitButton.Enabled = true;
                this._SelectContentPackageForLesson.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _BuildSelectOJTContentPackageModal
        /// <summary>
        /// Builds the modal for selecting ojt content package for the lesson.
        /// </summary>
        private void _BuildSelectOJTContentPackageModal()
        {
            // set modal properties
            this._SelectOJTContentPackageForLesson = new ModalPopup("LessonModifyModal_SelectOJTContentPackageForLessonModal");
            this._SelectOJTContentPackageForLesson.Type = ModalPopupType.Form;
            this._SelectOJTContentPackageForLesson.UseConditionalUpdateModeForUpdatePanel = true;
            this._SelectOJTContentPackageForLesson.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG);
            this._SelectOJTContentPackageForLesson.HeaderIconAlt = _GlobalResources.SelectContentPackage;
            this._SelectOJTContentPackageForLesson.HeaderText = _GlobalResources.SelectContentPackage;
            this._SelectOJTContentPackageForLesson.TargetControlID = this._SelectOJTContentPackageModalLaunchButton.ID;
            this._SelectOJTContentPackageForLesson.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectOJTContentPackageForLesson.SubmitButtonCustomText = _GlobalResources.SelectContentPackage;
            this._SelectOJTContentPackageForLesson.SubmitButton.OnClientClick = "javascript:SelectOJTContentPackageForLesson(); return false;";
            this._SelectOJTContentPackageForLesson.CloseButtonTextType = ModalPopupButtonText.Cancel;
            this._SelectOJTContentPackageForLesson.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectOJTContentPackage = new DynamicListBox("LessonModifyModal_SelectOJTContentPackage");
            this._SelectOJTContentPackage.NoRecordsFoundMessage = _GlobalResources.NoContentPackagesFound;
            this._SelectOJTContentPackage.IncludeSelectAllNone = false;
            this._SelectOJTContentPackage.IsMultipleSelect = false;
            this._SelectOJTContentPackage.SearchButton.Command += new CommandEventHandler(this._SearchSelectOJTContentPackageButton_Command);
            this._SelectOJTContentPackage.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectOJTContentPackageButton_Command);
            this._SelectOJTContentPackage.ListBoxControl.DataSource = this._ContentPackagesForSelectList;
            this._SelectOJTContentPackage.ListBoxControl.DataTextField = "name";
            this._SelectOJTContentPackage.ListBoxControl.DataValueField = "idContentPackage";
            this._SelectOJTContentPackage.ListBoxControl.DataBind();

            // add the hidden input to the modal
            this._SelectOJTContentPackageForLesson.AddControlToBody(this._SelectOJTContentPackage);

            // add modal to container            
            this.CoursePropertiesActionsPanel.Controls.Add(this._SelectOJTContentPackageForLesson);
        }
        #endregion

        #region _SearchSelectOJTContentPackageButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Content Package" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectOJTContentPackageButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectOJTContentPackageForLesson.ClearFeedback();

            // clear the listbox control
            this._SelectOJTContentPackage.ListBoxControl.Items.Clear();

            // do the search
            this._OJTContentPackagesForSelectList = Asentia.LMS.Library.ContentPackage.IdsAndNamesForSelectList(this._SelectOJTContentPackage.SearchTextBox.Text);

            // bind the data
            this._SelectOJTContentPackageDataBind();
        }
        #endregion

        #region _ClearSearchSelectOJTContentPackageButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Content Package" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectOJTContentPackageButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectOJTContentPackageForLesson.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectOJTContentPackage.ListBoxControl.Items.Clear();
            this._SelectOJTContentPackage.SearchTextBox.Text = "";

            // clear the search
            this._OJTContentPackagesForSelectList = Asentia.LMS.Library.ContentPackage.IdsAndNamesForSelectList(null);

            // bind the data
            this._SelectOJTContentPackageDataBind();
        }
        #endregion

        #region _SelectOJTContentPackageDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectOJTContentPackageDataBind()
        {
            this._SelectOJTContentPackage.ListBoxControl.DataSource = this._OJTContentPackagesForSelectList;
            this._SelectOJTContentPackage.ListBoxControl.DataTextField = "name";
            this._SelectOJTContentPackage.ListBoxControl.DataValueField = "idContentPackage";
            this._SelectOJTContentPackage.ListBoxControl.DataBind();

            // if no records available then disable the submit button
            if (this._SelectOJTContentPackage.ListBoxControl.Items.Count == 0)
            {
                this._SelectOJTContentPackageForLesson.SubmitButton.Enabled = false;
                this._SelectOJTContentPackageForLesson.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectOJTContentPackageForLesson.SubmitButton.Enabled = true;
                this._SelectOJTContentPackageForLesson.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _BuildSelectStandupTrainingModuleModal
        /// <summary>
        /// Builds the modal for selecting content package to for the lesson.
        /// </summary>
        private void _BuildSelectStandupTrainingModuleModal()
        {
            // set modal properties
            this._SelectStandupTrainingModuleForLesson = new ModalPopup("LessonModifyModal_SelectStandupTrainingModuleForLessonModal");
            this._SelectStandupTrainingModuleForLesson.Type = ModalPopupType.Form;
            this._SelectStandupTrainingModuleForLesson.UseConditionalUpdateModeForUpdatePanel = true;
            this._SelectStandupTrainingModuleForLesson.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            this._SelectStandupTrainingModuleForLesson.HeaderIconAlt = _GlobalResources.SelectInstructorLedTrainingModule;
            this._SelectStandupTrainingModuleForLesson.HeaderText = _GlobalResources.SelectInstructorLedTrainingModule;
            this._SelectStandupTrainingModuleForLesson.TargetControlID = this._SelectStandupTrainingModuleModalLaunchButton.ID;
            this._SelectStandupTrainingModuleForLesson.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectStandupTrainingModuleForLesson.SubmitButtonCustomText = _GlobalResources.SelectInstructorLedTrainingModule;
            this._SelectStandupTrainingModuleForLesson.SubmitButton.OnClientClick = "javascript:SelectStandupTrainingModuleForLesson(); return false;";
            this._SelectStandupTrainingModuleForLesson.CloseButtonTextType = ModalPopupButtonText.Cancel;
            this._SelectStandupTrainingModuleForLesson.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectStandupTrainingModule = new DynamicListBox("LessonModifyModal_SelectStandupTrainingModule");
            this._SelectStandupTrainingModule.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectStandupTrainingModule.IncludeSelectAllNone = false;
            this._SelectStandupTrainingModule.IsMultipleSelect = false;
            this._SelectStandupTrainingModule.SearchButton.Command += new CommandEventHandler(this._SearchSelectStandupTrainingModuleButton_Command);
            this._SelectStandupTrainingModule.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectStandupTrainingModuleButton_Command);
            this._SelectStandupTrainingModule.ListBoxControl.DataSource = this._StandupTrainingModulesForSelectList;
            this._SelectStandupTrainingModule.ListBoxControl.DataTextField = "title";
            this._SelectStandupTrainingModule.ListBoxControl.DataValueField = "idStandupTraining";
            this._SelectStandupTrainingModule.ListBoxControl.DataBind();

            // add the hidden input to the modal
            this._SelectStandupTrainingModuleForLesson.AddControlToBody(this._SelectStandupTrainingModule);

            // add modal to container            
            this.CoursePropertiesActionsPanel.Controls.Add(this._SelectStandupTrainingModuleForLesson);
        }
        #endregion

        #region _SearchSelectStandupTrainingModuleButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Standup Training Module" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectStandupTrainingModuleButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectStandupTrainingModuleForLesson.ClearFeedback();

            // clear the listbox control
            this._SelectStandupTrainingModule.ListBoxControl.Items.Clear();

            // do the search
            this._StandupTrainingModulesForSelectList = Asentia.LMS.Library.StandupTraining.IdsAndNamesForSelectList(this._SelectStandupTrainingModule.SearchTextBox.Text);

            // bind the data
            this._SelectStandupTrainingModuleDataBind();
        }
        #endregion

        #region _ClearSearchSelectStandupTrainingModuleButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Standup Training Module" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectStandupTrainingModuleButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectStandupTrainingModuleForLesson.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectStandupTrainingModule.ListBoxControl.Items.Clear();
            this._SelectStandupTrainingModule.SearchTextBox.Text = "";

            // clear the search
            this._StandupTrainingModulesForSelectList = Asentia.LMS.Library.StandupTraining.IdsAndNamesForSelectList(null);

            // bind the data
            this._SelectStandupTrainingModuleDataBind();
        }
        #endregion

        #region _SelectStandupTrainingModuleDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectStandupTrainingModuleDataBind()
        {
            this._SelectStandupTrainingModule.ListBoxControl.DataSource = this._StandupTrainingModulesForSelectList;
            this._SelectStandupTrainingModule.ListBoxControl.DataTextField = "title";
            this._SelectStandupTrainingModule.ListBoxControl.DataValueField = "idStandupTraining";
            this._SelectStandupTrainingModule.ListBoxControl.DataBind();

            // if no records available then disable the submit button
            if (this._SelectStandupTrainingModule.ListBoxControl.Items.Count == 0)
            {
                this._SelectStandupTrainingModuleForLesson.SubmitButton.Enabled = false;
                this._SelectStandupTrainingModuleForLesson.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectStandupTrainingModuleForLesson.SubmitButton.Enabled = true;
                this._SelectStandupTrainingModuleForLesson.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _LoadLessonModifyModalContent
        /// <summary>
        /// Loads content for lesson modify modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadLessonModifyModalContent(object sender, EventArgs e)
        {            
            try
            {
                // clear the modal feedback
                this._LessonModifyModal.ClearFeedback();

                // make the submit and close buttons visible
                this._LessonModifyModal.SubmitButton.Visible = true;
                this._LessonModifyModal.CloseButton.Visible = true;

                // get the header and submit button
                Label modalHeader = (Label)this._LessonModifyModal.FindControl("LessonModifyModalModalPopupHeaderText");
                modalHeader.Text = _GlobalResources.NewModule;

                Button submitButton = (Button)this._LessonModifyModal.FindControl("LessonModifyModalModalPopupSubmitButton");
                submitButton.Text = _GlobalResources.CreateModule;

                // get the lesson id from the hidden input                
                int idLesson = Convert.ToInt32(this._LessonModifyData.Value);

                // reset the form fields
                this._ResetLessonModifyFormFields();
                
                // populate the form if the lesson id is greater than 0
                if (idLesson > 0)
                {
                    // set the submit button text to "Save Changes"
                    submitButton.Text = _GlobalResources.SaveChanges;

                    // show the lesson content change warning panel
                    Panel contentChangeWarningPanel = (Panel)this._LessonModifyModal.FindControl("LessonModifyModal_LessonPropertiesContentChangeWarning");
                    contentChangeWarningPanel.Style.Add("display", "block");

                    // get the lesson object
                    this._LessonObject = new Lesson(idLesson);

                    // set the modal header
                    string lessonTitleInInterfaceLanguage = this._LessonObject.Title;

                    if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        foreach (Lesson.LanguageSpecificProperty lessonLanguageSpecificProperty in this._LessonObject.LanguageSpecificProperties)
                        {
                            if (lessonLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                            { lessonTitleInInterfaceLanguage = lessonLanguageSpecificProperty.Title; }
                        }
                    }

                    modalHeader.Text = lessonTitleInInterfaceLanguage;
                  
                    // LANGUAGE SPECIFIC PROPERTIES

                    // title, short description, long description
                    bool isDefaultPopulated = false;

                    foreach (Lesson.LanguageSpecificProperty lessonLanguageSpecificProperty in this._LessonObject.LanguageSpecificProperties)
                    {
                        // if the language is the default language, populate the control directly attached to this page,
                        // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                        // it; note that if we cannot populate the controls directly attached to this page (default) from
                        // language-specific properties, we will use the values in the properties that come from the base table
                        if (lessonLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            this._LessonTitle.Text = lessonLanguageSpecificProperty.Title;
                            this._LessonShortDescription.Text = lessonLanguageSpecificProperty.ShortDescription;
                            this._LessonLongDescription.Text = lessonLanguageSpecificProperty.LongDescription;
                            this._LessonSearchTags.Text = lessonLanguageSpecificProperty.SearchTags;

                            isDefaultPopulated = true;
                        }
                        else
                        {
                            // get text boxes
                            TextBox languageSpecificLessonTitleTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonTitle.ID + "_" + lessonLanguageSpecificProperty.LangString);
                            TextBox languageSpecificLessonShortDescriptionTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonShortDescription.ID + "_" + lessonLanguageSpecificProperty.LangString);
                            TextBox languageSpecificLessonLongDescriptionTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonLongDescription.ID + "_" + lessonLanguageSpecificProperty.LangString);
                            TextBox languageSpecificLessonSearchTagsTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonSearchTags.ID + "_" + lessonLanguageSpecificProperty.LangString);

                            // if the text boxes were found, set the text box values to the language-specific value
                            if (languageSpecificLessonTitleTextBox != null)
                            { languageSpecificLessonTitleTextBox.Text = lessonLanguageSpecificProperty.Title; }

                            if (languageSpecificLessonShortDescriptionTextBox != null)
                            { languageSpecificLessonShortDescriptionTextBox.Text = lessonLanguageSpecificProperty.ShortDescription; }

                            if (languageSpecificLessonLongDescriptionTextBox != null)
                            { languageSpecificLessonLongDescriptionTextBox.Text = lessonLanguageSpecificProperty.LongDescription; }

                            if (languageSpecificLessonSearchTagsTextBox != null)
                            { languageSpecificLessonSearchTagsTextBox.Text = lessonLanguageSpecificProperty.SearchTags; }
                        }
                    }

                    if (!isDefaultPopulated)
                    {
                        this._LessonTitle.Text = this._LessonObject.Title;
                        this._LessonShortDescription.Text = this._LessonObject.ShortDescription;
                        this._LessonLongDescription.Text = this._LessonObject.LongDescription;
                        this._LessonSearchTags.Text = this._LessonObject.SearchTags;
                    }

                    // NON-LANGUAGE SPECIFIC PROPERTIES

                    // rev code
                    this._LessonRevCode.Text = this._LessonObject.RevCode;

                    // is lesson optional?
                    if (this._LessonObject.IsOptional == null)
                    {
                        this._LessonOptional.Checked = false;
                    }
                    else
                    {
                        this._LessonOptional.Checked = (bool)this._LessonObject.IsOptional;
                    }

                    // show the lesson data reset override field                    
                    Panel lessonModifyOverrideContentChangeLessonDataResetFieldContainer = (Panel)this._LessonModifyModal.FindControl("LessonModifyModal_OverrideContentChangeLessonDataReset_Container");
                    lessonModifyOverrideContentChangeLessonDataResetFieldContainer.Style.Add("display", "block");

                    // CONTENT LINKAGE

                    // content package

                    foreach (Lesson.LessonContent contentItem in this._LessonObject.Content)
                    {
                        if (contentItem.IdContentType == Lesson.LessonContentType.ContentPackage)
                        {
                            this._SelectedContentPackage.Value = contentItem.IdObject.ToString();

                            // selected package
                            this._LessonSelectedContentPackageContainer.Controls.Clear();

                            Panel selectedContentPackageNameContainer = new Panel();
                            selectedContentPackageNameContainer.ID = "LessonModifyModal_SelectedContentPackageNameContainer";

                            // selected content package name
                            Literal selectedContentPackageName = new Literal();

                            if (contentItem.IdObject != null)
                            {
                                this._UseContentPackageCheckBox.Checked = true;

                                ContentPackage contentPackage = new ContentPackage((int)contentItem.IdObject);

                                selectedContentPackageName.Text = contentPackage.Name;
                            }
                            else
                            {
                                selectedContentPackageName.Text = _GlobalResources.None;
                            }

                            // add controls to container
                            selectedContentPackageNameContainer.Controls.Add(selectedContentPackageName);
                            this._LessonSelectedContentPackageContainer.Controls.Add(selectedContentPackageNameContainer);

                            break;
                        }
                    }

                    // standup training module

                    foreach (Lesson.LessonContent contentItem in this._LessonObject.Content)
                    {
                        if (contentItem.IdContentType == Lesson.LessonContentType.StandupTrainingModule)
                        {
                            this._SelectedStandupTrainingModule.Value = contentItem.IdObject.ToString();

                            // selected module
                            this._LessonSelectedStandupTrainingModuleContainer.Controls.Clear();

                            Panel selectedStandupTrainingModuleNameContainer = new Panel();
                            selectedStandupTrainingModuleNameContainer.ID = "LessonModifyModal_SelectedStandupTrainingModuleNameContainer";

                            // selected standup training module name
                            Literal selectedStandupTrainingModuleName = new Literal();

                            if (contentItem.IdObject != null)
                            {
                                this._UseStandupTrainingModuleCheckBox.Checked = true;

                                Library.StandupTraining standupTraining = new Library.StandupTraining((int)contentItem.IdObject);

                                selectedStandupTrainingModuleName.Text = standupTraining.Title;
                            }
                            else
                            {
                                selectedStandupTrainingModuleName.Text = _GlobalResources.None;
                            }

                            // add controls to container
                            selectedStandupTrainingModuleNameContainer.Controls.Add(selectedStandupTrainingModuleName);
                            this._LessonSelectedStandupTrainingModuleContainer.Controls.Add(selectedStandupTrainingModuleNameContainer);

                            break;
                        }
                    }

                    // task

                    foreach (Lesson.LessonContent contentItem in this._LessonObject.Content)
                    {
                        if (contentItem.IdContentType == Lesson.LessonContentType.Task)
                        {
                            this._UseTaskCheckBox.Checked = true;

                            // document type
                            this._TaskAllowedDocumentType.SelectedValue = Convert.ToInt32(contentItem.IdTaskDocumentType).ToString();

                            // uploaded resource
                            this._LessonTaskUploadedResourceContainer.Controls.Clear();

                            Panel uploadedResourceFileNameContainer = new Panel();
                            uploadedResourceFileNameContainer.ID = "LessonModifyModal_UploadedResourceFileNameContainer";

                            // uploaded resource file name
                            if (!String.IsNullOrWhiteSpace(contentItem.TaskResourcePath))
                            {
                                Image uploadedResourceRemoveLink = new Image();
                                uploadedResourceRemoveLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                                uploadedResourceRemoveLink.CssClass = "SmallIcon";
                                uploadedResourceRemoveLink.Style.Add("cursor", "pointer");
                                uploadedResourceRemoveLink.Attributes.Add("onclick", "javascript:RemoveTaskUploadedResource();");
                                uploadedResourceFileNameContainer.Controls.Add(uploadedResourceRemoveLink);

                                HyperLink uploadedResourceFileLink = new HyperLink();
                                uploadedResourceFileLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LESSON + this._LessonObject.Id + "/" + contentItem.TaskResourcePath;
                                uploadedResourceFileLink.Target = "_blank";
                                uploadedResourceFileLink.Text = contentItem.TaskResourcePath;
                                uploadedResourceFileNameContainer.Controls.Add(uploadedResourceFileLink);
                            }
                            else
                            {
                                Literal uploadedResourceFileName = new Literal();
                                uploadedResourceFileName.Text = _GlobalResources.None;
                                uploadedResourceFileNameContainer.Controls.Add(uploadedResourceFileName);
                            }

                            // add control to container
                            this._LessonTaskUploadedResourceContainer.Controls.Add(uploadedResourceFileNameContainer);

                            // proctor settings
                            this._TaskAllowSupervisorAsProctor.Checked = (bool)contentItem.AllowSupervisorsAsProctor;
                            this._TaskAllowCourseExpertAsProctor.Checked = (bool)contentItem.AllowCourseExpertsAsProctor;

                            break;
                        }
                    }

                    // ojt content package

                    foreach (Lesson.LessonContent contentItem in this._LessonObject.Content)
                    {
                        if (contentItem.IdContentType == Lesson.LessonContentType.OJT)
                        {
                            this._SelectedOJTContentPackage.Value = contentItem.IdObject.ToString();

                            // selected package
                            this._LessonSelectedOJTContentPackageContainer.Controls.Clear();

                            Panel selectedOJTContentPackageNameContainer = new Panel();
                            selectedOJTContentPackageNameContainer.ID = "LessonModifyModal_SelectedOJTContentPackageNameContainer";

                            // selected ojt content package name
                            Literal selectedOJTContentPackageName = new Literal();

                            if (contentItem.IdObject != null)
                            {
                                this._UseOJTCheckBox.Checked = true;

                                ContentPackage ojtContentPackage = new ContentPackage((int)contentItem.IdObject);

                                selectedOJTContentPackageName.Text = ojtContentPackage.Name;
                            }
                            else
                            {
                                selectedOJTContentPackageName.Text = _GlobalResources.None;
                            }

                            // add controls to container
                            selectedOJTContentPackageNameContainer.Controls.Add(selectedOJTContentPackageName);
                            this._LessonSelectedOJTContentPackageContainer.Controls.Add(selectedOJTContentPackageNameContainer);

                            // proctor settings
                            this._OJTAllowSupervisorAsProctor.Checked = (bool)contentItem.AllowSupervisorsAsProctor;
                            this._OJTAllowCourseExpertAsProctor.Checked = (bool)contentItem.AllowCourseExpertsAsProctor;

                            break;
                        }
                    }
                }
                
                // build a startup script for handling JS control initialization
                StringBuilder multipleStartUpCallsScript = new StringBuilder();

                multipleStartUpCallsScript.AppendLine(" // initialize lesson modify controls");
                multipleStartUpCallsScript.AppendLine(" UseContentPackageCheckBoxClick();");
                multipleStartUpCallsScript.AppendLine(" UseStandupTrainingModuleCheckBoxClick();");
                multipleStartUpCallsScript.AppendLine(" UseTaskCheckBoxClick();");
                multipleStartUpCallsScript.AppendLine(" UseOJTCheckBoxClick();");
                multipleStartUpCallsScript.AppendLine(" CheckForEmptyContentPackageSelection();");
                multipleStartUpCallsScript.AppendLine(" CheckForEmptyStandupTrainingModuleSelection();");
                multipleStartUpCallsScript.AppendLine(" CheckForEmptyOJTContentPackageSelection();");
                multipleStartUpCallsScript.AppendLine(" ReloadCKEditorForField(\"LessonModifyModal_LessonLongDescription_Field\")");

                ScriptManager.RegisterStartupScript(this, this.GetType(), this._LessonModifyModal.ID + "StartupScript", multipleStartUpCallsScript.ToString(), true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._LessonModifyModal.DisplayFeedback(dnfEx.Message, true);                
                this._LessonModifyModal.SubmitButton.Visible = false;
                this._LessonModifyModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._LessonModifyModal.DisplayFeedback(fnuEx.Message, true);                
                this._LessonModifyModal.SubmitButton.Visible = false;
                this._LessonModifyModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._LessonModifyModal.DisplayFeedback(cpeEx.Message, true);                
                this._LessonModifyModal.SubmitButton.Visible = false;
                this._LessonModifyModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._LessonModifyModal.DisplayFeedback(dEx.Message, true);                
                this._LessonModifyModal.SubmitButton.Visible = false;
                this._LessonModifyModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._LessonModifyModal.DisplayFeedback(ex.Message, true);                
                this._LessonModifyModal.SubmitButton.Visible = false;
                this._LessonModifyModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _ResetLessonModifyFormFields
        /// <summary>
        /// Resets the form fields for the lesson modify modal.
        /// </summary>
        private void _ResetLessonModifyFormFields()
        {
            // reset the error containers
            Panel lessonModifyModalFormTabsPanel = (Panel)this._LessonModifyModal.FindControl("LessonModifyModal_LessonProperties_TabPanelsContainer");
            AsentiaPage.ClearErrorClassFromFieldsRecursive(lessonModifyModalFormTabsPanel);

            // hide the lesson content change warning panel
            Panel contentChangeWarningPanel = (Panel)this._LessonModifyModal.FindControl("LessonModifyModal_LessonPropertiesContentChangeWarning");            
            contentChangeWarningPanel.Style.Add("display", "none");

            // LANGUAGE SPECIFIC PROPERTIES

            // title, short description, long description
            this._LessonTitle.Text = "";
            this._LessonShortDescription.Text = "";
            this._LessonLongDescription.Text = "";
            this._LessonSearchTags.Text = "";
            
            
            // loop through each language available to the site and clear the text boxes for it
            foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
            {
                // get text boxes
                TextBox languageSpecificLessonTitleTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonTitle.ID + "_" + language);
                TextBox languageSpecificLessonShortDescriptionTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonShortDescription.ID + "_" + language);
                TextBox languageSpecificLessonLongDescriptionTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonLongDescription.ID + "_" + language);
                TextBox languageSpecificLessonSearchTagsTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonSearchTags.ID + "_" + language);

                // if the text boxes were found, set the text box values to the language-specific value
                if (languageSpecificLessonTitleTextBox != null)
                { languageSpecificLessonTitleTextBox.Text = ""; }

                if (languageSpecificLessonShortDescriptionTextBox != null)
                { languageSpecificLessonShortDescriptionTextBox.Text = ""; }

                if (languageSpecificLessonLongDescriptionTextBox != null)
                { languageSpecificLessonLongDescriptionTextBox.Text = ""; }

                if (languageSpecificLessonSearchTagsTextBox != null)
                { languageSpecificLessonSearchTagsTextBox.Text = ""; }                
            }

            // NON-LANGUAGE SPECIFIC PROPERTIES

            // rev code
            this._LessonRevCode.Text = "";

            // lesson optional?
            this._LessonOptional.Checked = false;

            // hide the lesson data reset override field                    
            Panel lessonModifyOverrideContentChangeLessonDataResetFieldContainer = (Panel)this._LessonModifyModal.FindControl("LessonModifyModal_OverrideContentChangeLessonDataReset_Container");
            lessonModifyOverrideContentChangeLessonDataResetFieldContainer.Style.Add("display", "none");

            this._OverrideContentChangeLessonDataReset.Checked = false;

            // content package
            this._SelectedContentPackage.Value = "";
            this._LessonSelectedContentPackageContainer.Controls.Clear();
            this._UseContentPackageCheckBox.Checked = false;
            
            // ilt
            this._SelectedStandupTrainingModule.Value = "";
            this._LessonSelectedStandupTrainingModuleContainer.Controls.Clear();
            this._UseStandupTrainingModuleCheckBox.Checked = false;

            // task
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.TASKMODULES_ENABLE))
            {
                this._UseTaskCheckBox.Checked = false;
                this._TaskAllowedDocumentType.SelectedValue = "";
                this._LessonTaskUploadedResourceContainer.Controls.Clear();
                this._TaskAllowSupervisorAsProctor.Checked = false;
                this._TaskAllowCourseExpertAsProctor.Checked = false;
            }

            // ojt
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OJTMODULES_ENABLE))
            {
                this._SelectedOJTContentPackage.Value = "";
                this._LessonSelectedOJTContentPackageContainer.Controls.Clear();
                this._UseOJTCheckBox.Checked = false;
                this._OJTAllowSupervisorAsProctor.Checked = false;
                this._OJTAllowCourseExpertAsProctor.Checked = false;
            }
        }
        #endregion

        #region _ValidateLessonPropertiesForm
        /// <summary>
        /// Validates the lesson properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateLessonPropertiesForm()
        {
            Panel lessonModifyModalFormTabsPanel = (Panel)this._LessonModifyModal.FindControl("LessonModifyModal_LessonProperties_TabPanelsContainer");

            bool isValid = true;
            bool propertiesTabHasErrors = false;
            bool contentTabHasErrors = false;

            // TITLE - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._LessonTitle.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonTitle", _GlobalResources.Title + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);

                // apply error image and class to tabs if they have errors
                if (propertiesTabHasErrors)
                { this.ApplyErrorImageAndClassToTab(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonProperties_Properties_TabLI"); }
            }

            // SHORT DESCRIPTION - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._LessonShortDescription.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonShortDescription", _GlobalResources.ShortDescription + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);

                // apply error image and class to tabs if they have errors
                if (propertiesTabHasErrors)
                { this.ApplyErrorImageAndClassToTab(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonProperties_Properties_TabLI"); }
            }

            // VALIDATE THAT AT LEAST ONE CONTENT TYPE IS SELECTED
            if (!this._UseContentPackageCheckBox.Checked && !this._UseStandupTrainingModuleCheckBox.Checked && !this._UseTaskCheckBox.Checked && !this._UseOJTCheckBox.Checked)
            {
                isValid = false;
                contentTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonContentTypes", _GlobalResources.YouMustSelectAtLeastOneContentTypeForThisModule);

                // apply error image and class to tabs if they have errors
                if (contentTabHasErrors)
                { this.ApplyErrorImageAndClassToTab(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonProperties_Content_TabLI"); }
            }

            // CONTENT PACKAGE
            if (this._UseContentPackageCheckBox.Checked)
            {
                int idContentPackage;

                if (String.IsNullOrWhiteSpace(this._SelectedContentPackage.Value) || !int.TryParse(this._SelectedContentPackage.Value, out idContentPackage))
                {
                    isValid = false;
                    contentTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonContentPackage", _GlobalResources.ContentPackage + " " + _GlobalResources.IsRequired);
                }

                // apply error image and class to tabs if they have errors
                if (contentTabHasErrors)
                { this.ApplyErrorImageAndClassToTab(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonProperties_Content_TabLI"); }
            }

            // STANDUP TRAINING MODULE
            if (this._UseStandupTrainingModuleCheckBox.Checked)
            {
                int idStandupTraining;

                if (String.IsNullOrWhiteSpace(this._SelectedStandupTrainingModule.Value) || !int.TryParse(this._SelectedStandupTrainingModule.Value, out idStandupTraining))
                {
                    isValid = false;
                    contentTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonStandupTrainingModule", _GlobalResources.InstructorLedTrainingModule + " " + _GlobalResources.IsRequired);
                }

                // apply error image and class to tabs if they have errors
                if (contentTabHasErrors)
                { this.ApplyErrorImageAndClassToTab(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonProperties_Content_TabLI"); }
            }

            // TASK
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.TASKMODULES_ENABLE))
            {
                if (this._UseTaskCheckBox.Checked)
                {
                    int idTaskDocumentType;

                    if (String.IsNullOrWhiteSpace(this._TaskAllowedDocumentType.SelectedValue) || !int.TryParse(this._TaskAllowedDocumentType.SelectedValue, out idTaskDocumentType))
                    {
                        isValid = false;
                        contentTabHasErrors = true;
                        this.ApplyErrorMessageToFieldErrorPanel(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonTask", _GlobalResources.DocumentType + " " + _GlobalResources.IsRequired);
                    }

                    // apply error image and class to tabs if they have errors
                    if (contentTabHasErrors)
                    { this.ApplyErrorImageAndClassToTab(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonProperties_Content_TabLI"); }
                }
            }

            // OJT
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OJTMODULES_ENABLE))
            {
                if (this._UseOJTCheckBox.Checked)
                {
                    int idOJTContentPackage;

                    if (String.IsNullOrWhiteSpace(this._SelectedOJTContentPackage.Value) || !int.TryParse(this._SelectedOJTContentPackage.Value, out idOJTContentPackage))
                    {
                        isValid = false;
                        contentTabHasErrors = true;
                        this.ApplyErrorMessageToFieldErrorPanel(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonOJTContentPackage", _GlobalResources.OJT + " " + _GlobalResources.ContentPackage + " " + _GlobalResources.IsRequired);
                    }

                    // apply error image and class to tabs if they have errors
                    if (contentTabHasErrors)
                    { this.ApplyErrorImageAndClassToTab(lessonModifyModalFormTabsPanel, "LessonModifyModal_LessonProperties_Content_TabLI"); }
                }
            }

            if (!isValid)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LessonContentPackageDisable", "UseContentPackageCheckBoxClick();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LessonILTDisable", "UseStandupTrainingModuleCheckBoxClick();", true);

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.TASKMODULES_ENABLE))
                { ScriptManager.RegisterStartupScript(this, this.GetType(), "LessonTaskDisable", "UseTaskCheckBoxClick();", true); }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OJTMODULES_ENABLE))
                { ScriptManager.RegisterStartupScript(this, this.GetType(), "LessonOJTDisable", "UseOJTCheckBoxClick();", true); }
                
            }
            return isValid;
        }
        #endregion

        #region _LessonModifySubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the lesson modify modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _LessonModifySubmit_Command(object sender, CommandEventArgs e)
        {            
            try
            {
                // clear the modal feedback
                this._LessonModifyModal.ClearFeedback();

                // validate the form
                if (!this._ValidateLessonPropertiesForm())
                { throw new AsentiaException(); }

                // instansiate the lesson object for new and existing lesson
                int idLesson = Convert.ToInt32(this._LessonModifyData.Value);

                if (idLesson > 0)
                { this._LessonObject = new Lesson(idLesson); }
                else
                { this._LessonObject = new Lesson(); }
                
                // if editing an existing lesson:
                //  - determine if we need to set lesson data flags for content change, either because content has been changed or removed
                //  - determine if we need to delete any of the existing content types, removal
                //  - also, get the filename of existing task resource in case we need to delete it
                bool setContentChangeLessonDataFlags = false;
                bool removeContentPackage = false;
                bool removeStandupTraining = false;
                bool removeTask = false;
                bool removeOJT = false;
                string existingTaskResourceFileName = null;

                if (this._LessonObject.Id > 0)
                {
                    foreach (Lesson.LessonContent contentItem in this._LessonObject.Content)
                    {
                        // content package
                        if (contentItem.IdContentType == Lesson.LessonContentType.ContentPackage)
                        {
                            if (!this._UseContentPackageCheckBox.Checked)
                            {
                                setContentChangeLessonDataFlags = true;
                                removeContentPackage = true;
                            }
                            else // check for content change
                            {
                                // if the selected package is different from the existing one, set the content change flag
                                if (contentItem.IdObject != Convert.ToInt32(this._SelectedContentPackage.Value))
                                { setContentChangeLessonDataFlags = true; }
                            }
                        }

                        // standup training
                        if (contentItem.IdContentType == Lesson.LessonContentType.StandupTrainingModule)
                        {
                            if (!this._UseStandupTrainingModuleCheckBox.Checked)
                            {
                                setContentChangeLessonDataFlags = true;
                                removeStandupTraining = true;
                            }
                            else // check for content change
                            {
                                // if the selected ilt is different from the existing one, set the content change flag
                                if (contentItem.IdObject != Convert.ToInt32(this._SelectedStandupTrainingModule.Value))
                                { setContentChangeLessonDataFlags = true; }
                            }
                        }

                        // task
                        if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.TASKMODULES_ENABLE))
                        {
                            if (contentItem.IdContentType == Lesson.LessonContentType.Task)
                            {
                                if (!this._UseTaskCheckBox.Checked)
                                {
                                    setContentChangeLessonDataFlags = true;
                                    removeTask = true;
                                }

                                // the check for a content change occurs below where we are checking the existence of an existing
                                // file, it's more efficient to do it down there instead of here

                                if (!String.IsNullOrWhiteSpace(contentItem.TaskResourcePath))
                                { existingTaskResourceFileName = contentItem.TaskResourcePath; }
                            }
                        }

                        // ojt
                        if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OJTMODULES_ENABLE))
                        {
                            if (contentItem.IdContentType == Lesson.LessonContentType.OJT)
                            {
                                if (!this._UseOJTCheckBox.Checked)
                                {
                                    setContentChangeLessonDataFlags = true;
                                    removeOJT = true;
                                }
                                else // check for content change
                                {
                                    // if the selected ojt is different from the existing one, set the content change flag
                                    if (contentItem.IdObject != Convert.ToInt32(this._SelectedOJTContentPackage.Value))
                                    { setContentChangeLessonDataFlags = true; }
                                }
                            }
                        }
                    }
                }

                // populate the object
                this._LessonObject.IdCourse = this._CourseObject.Id;
                this._LessonObject.Title = this._LessonTitle.Text;
                this._LessonObject.ShortDescription = this._LessonShortDescription.Text;

                if (String.IsNullOrWhiteSpace(this._LessonLongDescription.Text))
                { this._LessonObject.LongDescription = null; }
                else
                { this._LessonObject.LongDescription = HttpUtility.HtmlDecode(this._LessonLongDescription.Text); }
                
                if (String.IsNullOrWhiteSpace(this._CourseMaterialSearchTags.Text))
                { this._LessonObject.SearchTags = null; }
                else
                { this._LessonObject.SearchTags = this._LessonSearchTags.Text; }

                this._LessonObject.IsOptional = this._LessonOptional.Checked;

                // save the lesson, save its returned id to viewstate, and set the current lesson object's id
                idLesson = this._LessonObject.Save();
                this._LessonObject.Id = idLesson;
                this._LessonModifyData.Value = idLesson.ToString();
                
                // do lesson language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {                        
                        string lessonTitle = null;
                        string lessonShortDescription = null;
                        string lessonLongDescription = null;
                        string lessonSearchTags = null;

                        // get text boxes
                        TextBox languageSpecificLessonTitleTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonTitle.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificLessonShortDescriptionTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonShortDescription.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificLessonLongDescriptionTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonLongDescription.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificLessonSearchTagsTextBox = (TextBox)this._LessonModifyModal.FindControl(this._LessonSearchTags.ID + "_" + cultureInfo.Name);
                        
                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificLessonTitleTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificLessonTitleTextBox.Text))
                            { lessonTitle = languageSpecificLessonTitleTextBox.Text; }
                        }

                        if (languageSpecificLessonShortDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificLessonShortDescriptionTextBox.Text))
                            { lessonShortDescription = languageSpecificLessonShortDescriptionTextBox.Text; }
                        }

                        if (languageSpecificLessonLongDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificLessonLongDescriptionTextBox.Text))
                            { lessonLongDescription = languageSpecificLessonLongDescriptionTextBox.Text; }
                        }

                        if (languageSpecificLessonSearchTagsTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificLessonSearchTagsTextBox.Text))
                            { lessonSearchTags = languageSpecificLessonSearchTagsTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(lessonTitle) ||
                            !String.IsNullOrWhiteSpace(lessonShortDescription) ||
                            !String.IsNullOrWhiteSpace(lessonLongDescription) ||
                            !String.IsNullOrWhiteSpace(lessonSearchTags))
                        {
                            this._LessonObject.SaveLang(cultureInfo.Name,
                                                        lessonTitle,
                                                        lessonShortDescription,
                                                        HttpUtility.HtmlDecode(lessonLongDescription),
                                                        lessonSearchTags);
                        }
                    }
                }

                // CONTENT TYPES

                // remove existing content types that need to be removed
                if (removeContentPackage)
                { this._LessonObject.RemoveContent(Lesson.LessonContentType.ContentPackage); }

                if (removeStandupTraining)
                { this._LessonObject.RemoveContent(Lesson.LessonContentType.StandupTrainingModule); }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.TASKMODULES_ENABLE))
                {
                    if (removeTask)
                    {
                        this._LessonObject.RemoveContent(Lesson.LessonContentType.Task);

                        // also remove the task resoucre file, if exists
                        if (!String.IsNullOrWhiteSpace(existingTaskResourceFileName))
                        {
                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LESSON + this._LessonObject.Id + "/" + existingTaskResourceFileName)))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LESSON + this._LessonObject.Id + "/" + existingTaskResourceFileName)); }
                        }
                    }
                }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OJTMODULES_ENABLE))
                {
                    if (removeOJT)
                    { this._LessonObject.RemoveContent(Lesson.LessonContentType.OJT); }
                }

                // save content types

                // content package
                if (this._UseContentPackageCheckBox.Checked)
                {
                    this._LessonObject.SaveContent(Convert.ToInt32(this._SelectedContentPackage.Value),
                                                   Lesson.LessonContentType.ContentPackage,
                                                   null,
                                                   null,
                                                   null,
                                                   null);
                }

                // standup training
                if (this._UseStandupTrainingModuleCheckBox.Checked)
                {
                    this._LessonObject.SaveContent(Convert.ToInt32(this._SelectedStandupTrainingModule.Value),
                                                   Lesson.LessonContentType.StandupTrainingModule,
                                                   null,
                                                   null,
                                                   null,
                                                   null);
                }

                // task
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.TASKMODULES_ENABLE))
                {
                    if (this._UseTaskCheckBox.Checked)
                    {
                        Lesson.TaskDocumentType taskDocumentType = (Lesson.TaskDocumentType)Convert.ToInt32(this._TaskAllowedDocumentType.SelectedValue);
                        string uploadedResoucreFilename = existingTaskResourceFileName;

                        // if the uploader has a file, we need to remove any existing file, and save the new file
                        if (this._TaskResourceUploader.SavedFilePath != null)
                        {
                            // check for folder existence and create if necessary
                            if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LESSON + this._LessonObject.Id)))
                            { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LESSON + this._LessonObject.Id)); }

                            // remove old file if it exists
                            if (!String.IsNullOrWhiteSpace(existingTaskResourceFileName))
                            {
                                // the fact that there was an existing file and we're about to remove it means a content change, set the content change flag
                                setContentChangeLessonDataFlags = true;

                                if (File.Exists(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LESSON + this._LessonObject.Id + "/" + existingTaskResourceFileName)))
                                { File.Delete(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LESSON + this._LessonObject.Id + "/" + existingTaskResourceFileName)); }
                            }

                            // save the new file, and remove the uploaded file from the upload folder
                            if (File.Exists(Server.MapPath(this._TaskResourceUploader.SavedFilePath)))
                            {
                                File.Copy(Server.MapPath(this._TaskResourceUploader.SavedFilePath), Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LESSON + this._LessonObject.Id + "/" + this._TaskResourceUploader.FileOriginalName), true);
                                File.Delete(Server.MapPath(this._TaskResourceUploader.SavedFilePath));
                                uploadedResoucreFilename = this._TaskResourceUploader.FileOriginalName;
                            }
                            else
                            { uploadedResoucreFilename = null; }
                        }

                        this._LessonObject.SaveContent(null,
                                                       Lesson.LessonContentType.Task,
                                                       taskDocumentType,
                                                       uploadedResoucreFilename,
                                                       this._TaskAllowSupervisorAsProctor.Checked,
                                                       this._TaskAllowCourseExpertAsProctor.Checked);
                    }
                }

                // ojt
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OJTMODULES_ENABLE))
                {
                    if (this._UseOJTCheckBox.Checked)
                    {
                        this._LessonObject.SaveContent(Convert.ToInt32(this._SelectedOJTContentPackage.Value),
                                                       Lesson.LessonContentType.OJT,
                                                       null,
                                                       null,
                                                       this._OJTAllowSupervisorAsProctor.Checked,
                                                       this._OJTAllowCourseExpertAsProctor.Checked);
                    }
                }

                // check for the "reset lesson data override", note that it is only effective if no content is being removed
                if (this._OverrideContentChangeLessonDataReset.Checked && !removeContentPackage && !removeStandupTraining && !removeTask && !removeOJT)
                { setContentChangeLessonDataFlags = false; }

                // if the content change flag was set, then call the method to set the content change lesson data flags
                if (setContentChangeLessonDataFlags)
                { this._LessonObject.SetContentChangeLessonDataFlags(); }

                // rebind the lesson grid
                this._LessonGrid.BindData();

                // reload the saved lesson
                this._LoadLessonModifyModalContent(sender, e);

                // display the feedback
                this._LessonModifyModal.DisplayFeedback(_GlobalResources.ModuleHasBeenSavedSuccessfully, false);                
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._LessonModifyModal.DisplayFeedback(dnfEx.Message, true);                
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._LessonModifyModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._LessonModifyModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._LessonModifyModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._LessonModifyModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _BuildLessonOptionalModal
        /// <summary>
        /// Builds the confirmation modal to make a lesson optional.
        /// </summary>
        private void _BuildLessonOptionalModal()
        {
            // set modal properties
            this._LessonOptionalModal = new ModalPopup("LessonOptionalModal");
            this._LessonOptionalModal.Type = ModalPopupType.Form;
            this._LessonOptionalModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LESSON, ImageFiles.EXT_PNG);
            this._LessonOptionalModal.HeaderIconAlt = _GlobalResources.MakeModuleOptional;
            this._LessonOptionalModal.HeaderText = _GlobalResources.MakeModuleOptional;
            this._LessonOptionalModal.TargetControlID = this._LessonOptionalModalLaunchButton.ID;
            this._LessonOptionalModal.CloseButton.Visible = false;
            this._LessonOptionalModal.SubmitButton.Visible = false;
            this._LessonOptionalModal.ShowLoadingPlaceholder = true;
            

            // build the data hidden field
            this._LessonOptionalData = new HiddenField();
            this._LessonOptionalData.ID = "LessonOptionalData";

            // build the modal body
            this._LessonOptionalModal.AddControlToBody(this._LessonOptionalModalLoadButton);
            this._LessonOptionalModal.AddControlToBody(this._LessonOptionalData);

            // add modal to container
            this.CoursePropertiesActionsPanel.Controls.Add(this._LessonOptionalModal);
        }
        #endregion

        #region _SetLessonOptional
        /// <summary>
        /// Sets the optional flag for the lesson to true.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _SetLessonOptional(object sender, EventArgs e)
        {
            

            try
            {
                // clear the modal feedback
                this._LessonOptionalModal.ClearFeedback();

                // get the document repository item id
                int idLesson = Convert.ToInt32(this._LessonOptionalData.Value);

                // toggle isOptional
                Lesson lessonObject = new Lesson(idLesson);
                lessonObject.IsOptional = true;
                lessonObject.Save();

                this._LessonOptionalModal.SubmitButton.Visible = false;
                this._LessonOptionalModal.CloseButton.Visible = false;
                this._LessonOptionalModal.DisplayFeedback(_GlobalResources.TheModuleIsNowOptionalForCourseCompletion, false);

                // rebind the course material grid and update
                this._LessonGrid.BindData();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._LessonOptionalModal.DisplayFeedback(dnfEx.Message, true);
                this._LessonOptionalModal.SubmitButton.Visible = false;
                this._LessonOptionalModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._LessonOptionalModal.DisplayFeedback(fnuEx.Message, true);
                this._LessonOptionalModal.SubmitButton.Visible = false;
                this._LessonOptionalModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._LessonOptionalModal.DisplayFeedback(cpeEx.Message, true);
                this._LessonOptionalModal.SubmitButton.Visible = false;
                this._LessonOptionalModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._LessonOptionalModal.DisplayFeedback(dEx.Message, true);
                this._LessonOptionalModal.SubmitButton.Visible = false;
                this._LessonOptionalModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._LessonOptionalModal.DisplayFeedback(ex.Message, true);
                this._LessonOptionalModal.SubmitButton.Visible = false;
                this._LessonOptionalModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildLessonRequiredModal
        /// <summary>
        /// Builds the confirmation modal to make a lesson required.
        /// </summary>
        private void _BuildLessonRequiredModal()
        {
            // set modal properties
            this._LessonRequiredModal = new ModalPopup("LessonRequiredModal");
            this._LessonRequiredModal.Type = ModalPopupType.Form;
            this._LessonRequiredModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LESSON, ImageFiles.EXT_PNG);
            this._LessonRequiredModal.HeaderIconAlt = _GlobalResources.MakeModuleRequired;
            this._LessonRequiredModal.HeaderText = _GlobalResources.MakeModuleRequired;
            this._LessonRequiredModal.TargetControlID = this._LessonRequiredModalLaunchButton.ID;
            this._LessonRequiredModal.CloseButton.Visible = false;
            this._LessonRequiredModal.SubmitButton.Visible = false;
            this._LessonRequiredModal.ShowLoadingPlaceholder = true;


            // build the data hidden field
            this._LessonRequiredData = new HiddenField();
            this._LessonRequiredData.ID = "LessonRequiredData";

            // build the modal body
            this._LessonRequiredModal.AddControlToBody(this._LessonRequiredModalLoadButton);
            this._LessonRequiredModal.AddControlToBody(this._LessonRequiredData);

            // add modal to container
            this.CoursePropertiesActionsPanel.Controls.Add(this._LessonRequiredModal);
        }
        #endregion

        #region _SetLessonRequired
        /// <summary>
        /// Sets the Optional flag for a lesson to false.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _SetLessonRequired(object sender, EventArgs e)
        {


            try
            {
                // clear the modal feedback
                this._LessonRequiredModal.ClearFeedback();

                // get the document repository item id
                int idLesson = Convert.ToInt32(this._LessonRequiredData.Value);

                // toggle isOptional
                Lesson lessonObject = new Lesson(idLesson);
                lessonObject.IsOptional = false;
                lessonObject.Save();

                this._LessonRequiredModal.SubmitButton.Visible = false;
                this._LessonRequiredModal.CloseButton.Visible = false;
                this._LessonRequiredModal.DisplayFeedback(_GlobalResources.TheModuleIsNowRequiredForCourseCompletion, false);

                // rebind the course material grid and update
                this._LessonGrid.BindData();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._LessonRequiredModal.DisplayFeedback(dnfEx.Message, true);
                this._LessonRequiredModal.SubmitButton.Visible = false;
                this._LessonRequiredModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._LessonRequiredModal.DisplayFeedback(fnuEx.Message, true);
                this._LessonRequiredModal.SubmitButton.Visible = false;
                this._LessonRequiredModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._LessonRequiredModal.DisplayFeedback(cpeEx.Message, true);
                this._LessonRequiredModal.SubmitButton.Visible = false;
                this._LessonRequiredModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._LessonRequiredModal.DisplayFeedback(dEx.Message, true);
                this._LessonRequiredModal.SubmitButton.Visible = false;
                this._LessonRequiredModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._LessonRequiredModal.DisplayFeedback(ex.Message, true);
                this._LessonRequiredModal.SubmitButton.Visible = false;
                this._LessonRequiredModal.CloseButton.Visible = false;
            }
        }
        #endregion
        #endregion

        #region Course Material Modify
        #region _BuildCourseMaterialModifyModal
        /// <summary>
        /// Builds the course material modify modal
        /// </summary>
        private void _BuildCourseMaterialModifyModal()
        {
            // set modal properties
            this._CourseMaterialModifyModal = new ModalPopup("CourseMaterialModifyModal");
            this._CourseMaterialModifyModal.Type = ModalPopupType.Form;
            this._CourseMaterialModifyModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG);
            this._CourseMaterialModifyModal.HeaderIconAlt = _GlobalResources.CourseMaterials;
            this._CourseMaterialModifyModal.HeaderText = _GlobalResources.NewCourseMaterial;
            this._CourseMaterialModifyModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._CourseMaterialModifyModal.SubmitButtonCustomText = _GlobalResources.CreateCourseMaterial;
            this._CourseMaterialModifyModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._CourseMaterialModifyModal.TargetControlID = this._CourseMaterialModifyModalLaunchButton.ID;
            this._CourseMaterialModifyModal.SubmitButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicCourseMaterialElements();");
            this._CourseMaterialModifyModal.SubmitButton.Command += new CommandEventHandler(this._CourseMaterialModifySubmit_Command);

            // build and format a page information panel with instructions
            Panel courseMaterialPropertiesInstructionsPanel = new Panel();
            courseMaterialPropertiesInstructionsPanel.ID = "CourseMaterialModifyModal_CourseMaterialPropertiesInstructionsPanel";
            this.FormatPageInformationPanel(courseMaterialPropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfTheCourseMaterial, true);
            this._CourseMaterialModifyModal.AddControlToBody(courseMaterialPropertiesInstructionsPanel);            

            // build the form properties container
            Panel courseMaterialPropertiesContainer = new Panel();
            courseMaterialPropertiesContainer.ID = "CourseMaterialModifyModal_CourseMaterialPropertiesContainer";
            courseMaterialPropertiesContainer.CssClass = "FormContentContainer";
            this._CourseMaterialModifyModal.AddControlToBody(courseMaterialPropertiesContainer);

            // build the course material properties form content
            courseMaterialPropertiesContainer.Controls.Add(this._BuildCourseMaterialFormProperties());

            // build the course material data hidden field - this will hold the id of the course material being edited
            this._CourseMaterialModifyData = new HiddenField();
            this._CourseMaterialModifyData.ID = "CourseMaterialModifyData";

            // add the load button and hidden input to the modal                        
            this._CourseMaterialModifyModal.AddControlToBody(this._CourseMaterialModifyModalLoadButton);
            this._CourseMaterialModifyModal.AddControlToBody(this._CourseMaterialModifyData);

            // add modal to container
            this.CoursePropertiesActionsPanel.Controls.Add(this._CourseMaterialModifyModal);
        }
        #endregion

        #region _BuildCourseMaterialFormProperties
        /// <summary>
        /// Method to build course material form properties controls
        /// </summary>
        private Panel _BuildCourseMaterialFormProperties()
        {
            // build the modal form panel
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "CourseMaterialModifyModal_CourseMaterialProperties";            

            // course material label field
            this._CourseMaterialLabel = new TextBox();
            this._CourseMaterialLabel.ID = "CourseMaterialModifyModal_CourseMaterialLabel_Field";
            this._CourseMaterialLabel.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseMaterialModifyModal_CourseMaterialLabel",
                                                             _GlobalResources.Title,
                                                             this._CourseMaterialLabel.ID,
                                                             this._CourseMaterialLabel,
                                                             true,
                                                             true,
                                                             true));

            // course material file field
            List<Control> fileControls = new List<Control>();

            Panel courseMaterialFileNameFieldContainer = new Panel();
            courseMaterialFileNameFieldContainer.ID = "CourseMaterialModifyModal_FileNameFieldContainer";

            Label courseMaterialFileNameField = new Label();
            courseMaterialFileNameField.ID = "CourseMaterialModifyModal_FileNameField";            
            courseMaterialFileNameFieldContainer.Controls.Add(courseMaterialFileNameField);

            fileControls.Add(courseMaterialFileNameFieldContainer);

            // uploader
            this._CourseMaterialUploader = new UploaderAsync("CourseMaterialModifyModal_CourseMaterialFile_Field", UploadType.CourseMaterial, "CourseMaterialModifyModal_CourseMaterialFile_ErrorContainer");
            fileControls.Add(this._CourseMaterialUploader);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CourseMaterialModifyModal_CourseMaterialFile",
                                                                                 _GlobalResources.File,
                                                                                 fileControls,
                                                                                 false,
                                                                                 true));
            // course material file size field
            Label courseMaterialFileSizeFieldStaticValue = new Label();
            courseMaterialFileSizeFieldStaticValue.ID = "CourseMaterialModifyModal_FileSizeField";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseMaterialModifyModal_CourseMaterialFileSize",
                                                             _GlobalResources.Size,
                                                             courseMaterialFileSizeFieldStaticValue.ID,
                                                             courseMaterialFileSizeFieldStaticValue,
                                                             false,
                                                             false,
                                                             false));

            // course material is private field 
            this._CourseMaterialIsPrivate = new CheckBox();
            this._CourseMaterialIsPrivate.ID = "CourseMaterialModifyModal_CourseMaterialIsPrivate_Field";
            this._CourseMaterialIsPrivate.Text = _GlobalResources.ThisCourseMaterialIsOnlyAvailableToUsersWhoAreEnrolledInTheCourse;
            this._CourseMaterialIsPrivate.Checked = false;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseMaterialModifyModal_CourseMaterialIsPrivate",
                                                             _GlobalResources.Private,
                                                             this._CourseMaterialIsPrivate.ID,
                                                             this._CourseMaterialIsPrivate,
                                                             false,
                                                             false,
                                                             false));

            // course material is all languages field            
            this._CourseMaterialIsAllLanguages = new CheckBox();
            this._CourseMaterialIsAllLanguages.ID = "CourseMaterialModifyModal_CourseMaterialIsAllLanguages_Field";
            this._CourseMaterialIsAllLanguages.Text = _GlobalResources.ThisCourseMaterialIsForAllLanguagesOrIsNotLanguageSpecific;
            this._CourseMaterialIsAllLanguages.Checked = false;
            this._CourseMaterialIsAllLanguages.Attributes.Add("onclick", "CourseMaterialLanguageSelectionClick(this)");

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseMaterialModifyModal_CourseMaterialIsAllLanguages",
                                                             _GlobalResources.AllLanguages,
                                                             this._CourseMaterialIsAllLanguages.ID,
                                                             this._CourseMaterialIsAllLanguages,
                                                             false,
                                                             false,
                                                             false));

            // course material language field
            this._CourseMaterialLanguage = new LanguageSelector(LanguageDropDownType.DataOnly);
            this._CourseMaterialLanguage.ID = "CourseMaterialModifyModal_CourseMaterialLanguage_Field";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseMaterialModifyModal_CourseMaterialLanguage",
                                                             _GlobalResources.Language,
                                                             this._CourseMaterialLanguage.ID,
                                                             this._CourseMaterialLanguage,
                                                             false,
                                                             false,
                                                             false));

            // course material search tags field
            this._CourseMaterialSearchTags = new TextBox();
            this._CourseMaterialSearchTags.ID = "CourseMaterialModifyModal_CourseMaterialSearchTags_Field";
            this._CourseMaterialSearchTags.Style.Add("width", "98%");
            this._CourseMaterialSearchTags.TextMode = TextBoxMode.MultiLine;
            this._CourseMaterialSearchTags.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseMaterialModifyModal_CourseMaterialSearchTags",
                                                             _GlobalResources.SearchTags,
                                                             this._CourseMaterialSearchTags.ID,
                                                             this._CourseMaterialSearchTags,
                                                             false,
                                                             false,
                                                             true));

            // return the properties panel
            return propertiesPanel;
        }
        #endregion

        #region _LoadCourseMaterialModifyModalContent
        /// <summary>
        /// Loads content for course material modify modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadCourseMaterialModifyModalContent(object sender, EventArgs e)
        {
            try
            {
                // clear the modal feedback
                this._CourseMaterialModifyModal.ClearFeedback();

                // make the submit and close buttons visible
                this._CourseMaterialModifyModal.SubmitButton.Visible = true;
                this._CourseMaterialModifyModal.CloseButton.Visible = true;

                // get the header and submit button
                Label modalHeader = (Label)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModalModalPopupHeaderText");
                modalHeader.Text = _GlobalResources.NewCourseMaterial;

                Button submitButton = (Button)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModalModalPopupSubmitButton");
                submitButton.Text = _GlobalResources.CreateCourseMaterial;

                // get the course material id from the hidden input                
                int idCourseMaterial = Convert.ToInt32(this._CourseMaterialModifyData.Value);
                
                // reset the form fields
                this._ResetCourseMaterialModifyFormFields();
                
                // populate the form if the course material id is greater than 0
                if (idCourseMaterial > 0)
                {
                    // set the submit button text to "Save Changes"
                    submitButton.Text = _GlobalResources.SaveChanges;                    

                    // get the course material object
                    this._CourseMaterialObject = new DocumentRepositoryItem(idCourseMaterial);
                    
                    // set the modal header
                    string courseMaterialTitleInInterfaceLanguage = this._CourseMaterialObject.Label;

                    if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        foreach (DocumentRepositoryItem.LanguageSpecificProperty courseMaterialLanguageSpecificProperty in this._CourseMaterialObject.LanguageSpecificProperties)
                        {
                            if (courseMaterialLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                            { courseMaterialTitleInInterfaceLanguage = courseMaterialLanguageSpecificProperty.Label; }
                        }
                    }

                    modalHeader.Text = courseMaterialTitleInInterfaceLanguage;
                    
                    // LANGUAGE SPECIFIC PROPERTIES

                    // title, search tags
                    bool isDefaultPopulated = false;

                    foreach (DocumentRepositoryItem.LanguageSpecificProperty courseMaterialLanguageSpecificProperty in this._CourseMaterialObject.LanguageSpecificProperties)
                    {
                        // if the language is the default language, populate the control directly attached to this page,
                        // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                        // it; note that if we cannot populate the controls directly attached to this page (default) from
                        // language-specific properties, we will use the values in the properties that come from the base table
                        if (courseMaterialLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            this._CourseMaterialLabel.Text = courseMaterialLanguageSpecificProperty.Label;
                            this._CourseMaterialSearchTags.Text = courseMaterialLanguageSpecificProperty.SearchTags;

                            isDefaultPopulated = true;
                        }
                        else
                        {
                            // get text boxes
                            TextBox languageSpecificCourseMaterialTitleTextBox = (TextBox)this._CourseMaterialModifyModal.FindControl(this._CourseMaterialLabel.ID + "_" + courseMaterialLanguageSpecificProperty.LangString);
                            TextBox languageSpecificCourseMaterialSearchTagsTextBox = (TextBox)this._CourseMaterialModifyModal.FindControl(this._CourseMaterialSearchTags.ID + "_" + courseMaterialLanguageSpecificProperty.LangString);

                            // if the text boxes were found, set the text box values to the language-specific value
                            if (languageSpecificCourseMaterialTitleTextBox != null)
                            { languageSpecificCourseMaterialTitleTextBox.Text = courseMaterialLanguageSpecificProperty.Label; }

                            if (languageSpecificCourseMaterialSearchTagsTextBox != null)
                            { languageSpecificCourseMaterialSearchTagsTextBox.Text = courseMaterialLanguageSpecificProperty.SearchTags; }
                        }
                    }

                    if (!isDefaultPopulated)
                    {
                        this._CourseMaterialLabel.Text = this._CourseMaterialObject.Label;
                        this._CourseMaterialSearchTags.Text = this._CourseMaterialObject.SearchTags;
                    }

                    // NON-LANGUAGE SPECIFIC PROPERTIES

                    // populate and show the course material file name field                    
                    Panel courseMaterialModifyFileNameFieldContainer = (Panel)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModal_FileNameFieldContainer");
                    courseMaterialModifyFileNameFieldContainer.Style.Add("display", "block");                    

                    Label courseMaterialModifyFileNameField = (Label)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModal_FileNameField");
                    courseMaterialModifyFileNameField.Text = this._CourseMaterialObject.FileName;

                    // populate and show the course material file size field                    
                    Panel courseMaterialModifyFileSizeFieldContainer = (Panel)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModal_CourseMaterialFileSize_Container");
                    courseMaterialModifyFileSizeFieldContainer.Style.Add("display", "block");

                    Label courseMaterialModifyFileSizeField = (Label)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModal_FileSizeField");
                    courseMaterialModifyFileSizeField.Text = Asentia.Common.Utility.GetSizeStringFromKB(this._CourseMaterialObject.Kb);

                    // is private
                    if (this._CourseMaterialObject.IsPrivate == true)
                    { this._CourseMaterialIsPrivate.Checked = true; }
                    else
                    { this._CourseMaterialIsPrivate.Checked = false; }

                    // is all languages
                    if (this._CourseMaterialObject.IsAllLanguages == true)
                    { this._CourseMaterialIsAllLanguages.Checked = true; }
                    else
                    { this._CourseMaterialIsAllLanguages.Checked = false; }

                    // course material language
                    if (this._CourseMaterialObject.LanguageString != null && this._CourseMaterialLanguage.Items.FindByValue(this._CourseMaterialObject.LanguageString) != null)
                    { this._CourseMaterialLanguage.SelectedValue = this._CourseMaterialObject.LanguageString; }
                }

                // build a startup script for handling JS control initialization
                StringBuilder multipleStartUpCallsScript = new StringBuilder();

                multipleStartUpCallsScript.AppendLine(" // initialize course material modify controls");
                multipleStartUpCallsScript.AppendLine(" CourseMaterialLanguageSelectionClick(document.getElementById(\"CourseMaterialModifyModal_CourseMaterialIsAllLanguages_Field\"));");
                multipleStartUpCallsScript.AppendLine(" ResetCourseMaterialUploaderField();");
                
                ScriptManager.RegisterStartupScript(this, this.GetType(), this._CourseMaterialModifyModal.ID + "StartupScript", multipleStartUpCallsScript.ToString(), true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CourseMaterialModifyModal.DisplayFeedback(dnfEx.Message, true);
                this._CourseMaterialModifyModal.SubmitButton.Visible = false;
                this._CourseMaterialModifyModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CourseMaterialModifyModal.DisplayFeedback(fnuEx.Message, true);
                this._CourseMaterialModifyModal.SubmitButton.Visible = false;
                this._CourseMaterialModifyModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CourseMaterialModifyModal.DisplayFeedback(cpeEx.Message, true);
                this._CourseMaterialModifyModal.SubmitButton.Visible = false;
                this._CourseMaterialModifyModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CourseMaterialModifyModal.DisplayFeedback(dEx.Message, true);
                this._CourseMaterialModifyModal.SubmitButton.Visible = false;
                this._CourseMaterialModifyModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CourseMaterialModifyModal.DisplayFeedback(ex.Message, true);
                this._CourseMaterialModifyModal.SubmitButton.Visible = false;
                this._CourseMaterialModifyModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _ResetCourseMaterialModifyFormFields
        /// <summary>
        /// Resets the form fields for the course material modify modal.
        /// </summary>
        private void _ResetCourseMaterialModifyFormFields()
        {
            // reset the error containers
            Panel courseMaterialModifyModalFormPropertiesPanel = (Panel)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModal_CourseMaterialProperties");
            AsentiaPage.ClearErrorClassFromFieldsRecursive(courseMaterialModifyModalFormPropertiesPanel);            

            // LANGUAGE SPECIFIC PROPERTIES

            // title, search tags
            this._CourseMaterialLabel.Text = "";            
            this._CourseMaterialSearchTags.Text = "";

            // loop through each language available to the site and clear the text boxes for it
            foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
            {
                // get text boxes
                TextBox languageSpecificCourseMaterialTitleTextBox = (TextBox)this._CourseMaterialModifyModal.FindControl(this._CourseMaterialLabel.ID + "_" + language);
                TextBox languageSpecificCourseMaterialSearchTagsTextBox = (TextBox)this._CourseMaterialModifyModal.FindControl(this._CourseMaterialSearchTags.ID + "_" + language);

                // if the text boxes were found, set the text box values to the language-specific value
                if (languageSpecificCourseMaterialTitleTextBox != null)
                { languageSpecificCourseMaterialTitleTextBox.Text = ""; }

                if (languageSpecificCourseMaterialSearchTagsTextBox != null)
                { languageSpecificCourseMaterialSearchTagsTextBox.Text = ""; }
            }

            // NON-LANGUAGE SPECIFIC PROPERTIES

            // clear and hide the course material file name field                    
            Panel courseMaterialModifyFileNameFieldContainer = (Panel)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModal_FileNameFieldContainer");
            courseMaterialModifyFileNameFieldContainer.Style.Add("display", "none");

            Label courseMaterialModifyFileNameField = (Label)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModal_FileNameField");
            courseMaterialModifyFileNameField.Text = "";

            // clear and hide the course material file size field                    
            Panel courseMaterialModifyFileSizeFieldContainer = (Panel)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModal_CourseMaterialFileSize_Container");
            courseMaterialModifyFileSizeFieldContainer.Style.Add("display", "none");

            Label courseMaterialModifyFileSizeField = (Label)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModal_FileSizeField");
            courseMaterialModifyFileSizeField.Text = "";

            // is private            
            this._CourseMaterialIsPrivate.Checked = false;

            // is all languages
            this._CourseMaterialIsAllLanguages.Checked = false;

            // course material language
            this._CourseMaterialLanguage.SelectedIndex = 0;            
        }
        #endregion

        #region _ValidateCourseMaterialPropertiesForm
        /// <summary>
        /// Validates the course material properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateCourseMaterialPropertiesForm()
        {            
            Panel courseMaterialModifyModalFormPropertiesPanel = (Panel)this._CourseMaterialModifyModal.FindControl("CourseMaterialModifyModal_CourseMaterialProperties");

            bool isValid = true;            

            // LABEL - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._CourseMaterialLabel.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(courseMaterialModifyModalFormPropertiesPanel, "CourseMaterialModifyModal_CourseMaterialLabel", _GlobalResources.Label + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // COURSE MATERIAL FILE, required if saving new course material            
            if (Convert.ToInt32(this._CourseMaterialModifyData.Value) == 0)
            {
                if (String.IsNullOrWhiteSpace(this._CourseMaterialUploader.FileSavedName) || !File.Exists(Server.MapPath(this._CourseMaterialUploader.SavedFilePath)))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(courseMaterialModifyModalFormPropertiesPanel, "CourseMaterialModifyModal_CourseMaterialFile", _GlobalResources.PleaseSelectAFileToUpload);
                }
            }

            return isValid;
        }
        #endregion

        #region _CourseMaterialModifySubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the course material modify modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CourseMaterialModifySubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // clear the modal feedback
                this._CourseMaterialModifyModal.ClearFeedback();

                // validate the form
                if (!this._ValidateCourseMaterialPropertiesForm())
                { throw new AsentiaException(); }

                // instansiate the course material object for new and existing course material
                int idCourseMaterial = Convert.ToInt32(this._CourseMaterialModifyData.Value);

                if (idCourseMaterial > 0)
                { this._CourseMaterialObject = new DocumentRepositoryItem(idCourseMaterial); }
                else
                { this._CourseMaterialObject = new DocumentRepositoryItem(); }

                // populate the object
                this._CourseMaterialObject.IdDocumentRepositoryObjectType = DocumentRepositoryObjectType.Course;
                this._CourseMaterialObject.IdObject = this._CourseObject.Id;
                this._CourseMaterialObject.IdOwner = AsentiaSessionState.IdSiteUser;

                this._CourseMaterialObject.IsPrivate = this._CourseMaterialIsPrivate.Checked;

                this._CourseMaterialObject.IsAllLanguages = this._CourseMaterialIsAllLanguages.Checked;

                if (!this._CourseMaterialIsAllLanguages.Checked)
                { this._CourseMaterialObject.LanguageString = this._CourseMaterialLanguage.SelectedValue; }
                else
                { this._CourseMaterialObject.LanguageString = null; }

                this._CourseMaterialObject.Label = this._CourseMaterialLabel.Text;

                if (String.IsNullOrWhiteSpace(this._CourseMaterialSearchTags.Text))
                { this._CourseMaterialObject.SearchTags = null; }
                else
                { this._CourseMaterialObject.SearchTags = this._CourseMaterialSearchTags.Text; }

                if (this._CourseMaterialObject.Id > 0 && this._CourseMaterialUploader.SavedFilePath != null)
                {
                    // get the course's document folder path
                    string fullSavedFilePath = null;
                    fullSavedFilePath = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_COURSE + this._CourseObject.Id + "/";

                    // remove the existing file for this course material
                    if (File.Exists(Server.MapPath(fullSavedFilePath + this._CourseMaterialObject.FileName)))
                    { File.Delete(Server.MapPath(fullSavedFilePath + this._CourseMaterialObject.FileName)); }

                    // save the uploaded file
                    string fileOriginalName = String.Empty;
                    fullSavedFilePath = DocumentRepositoryItem.GetCorrectFileName(fullSavedFilePath + this._CourseMaterialUploader.FileOriginalName, out fileOriginalName);

                    File.Copy(Server.MapPath(this._CourseMaterialUploader.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                    File.Delete(Server.MapPath(this._CourseMaterialUploader.SavedFilePath));

                    this._CourseMaterialObject.FileName = fileOriginalName;
                    this._CourseMaterialObject.Kb = Convert.ToInt32(this._CourseMaterialUploader.FileSize) / 1000;                    
                }
                else if (this._CourseMaterialObject.Id == 0 && this._CourseMaterialUploader.SavedFilePath != null)
                {
                    // check course's documents folder existence and create if necessary
                    if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_COURSE + this._CourseObject.Id)))
                    { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_COURSE + this._CourseObject.Id)); }

                    string fullSavedFilePath = null;

                    if (File.Exists(Server.MapPath(this._CourseMaterialUploader.SavedFilePath)))
                    {
                        fullSavedFilePath = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_COURSE + this._CourseObject.Id + "/" + this._CourseMaterialUploader.FileOriginalName;
                        
                        string fileOriginalName = String.Empty;
                        fullSavedFilePath = DocumentRepositoryItem.GetCorrectFileName(fullSavedFilePath, out fileOriginalName);

                        // move the uploaded file into the course material's folder
                        File.Copy(Server.MapPath(this._CourseMaterialUploader.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                        File.Delete(Server.MapPath(this._CourseMaterialUploader.SavedFilePath));

                        this._CourseMaterialObject.FileName = fileOriginalName;
                        this._CourseMaterialObject.Kb = Convert.ToInt32(this._CourseMaterialUploader.FileSize) / 1000;
                    }
                    else
                    { this._CourseMaterialObject.FileName = null; }
                }

                // save the course material, save its returned id to viewstate, and set the current lesson object's id
                idCourseMaterial = this._CourseMaterialObject.Save();
                this._CourseMaterialObject.Id = idCourseMaterial;
                this._CourseMaterialModifyData.Value = idCourseMaterial.ToString();

                // do course material language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string courseMaterialLabel = null;
                        string courseMaterialSearchTags = null;

                        // get text boxes
                        TextBox languageSpecificCourseMaterialLabelTextBox = (TextBox)this._CourseMaterialModifyModal.FindControl(this._CourseMaterialLabel.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificCourseMaterialSearchTagsTextBox = (TextBox)this._CourseMaterialModifyModal.FindControl(this._CourseMaterialSearchTags.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificCourseMaterialLabelTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCourseMaterialLabelTextBox.Text))
                            { courseMaterialLabel = languageSpecificCourseMaterialLabelTextBox.Text; }
                        }

                        if (languageSpecificCourseMaterialSearchTagsTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCourseMaterialSearchTagsTextBox.Text))
                            { courseMaterialSearchTags = languageSpecificCourseMaterialSearchTagsTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(courseMaterialLabel) ||
                            !String.IsNullOrWhiteSpace(courseMaterialSearchTags))
                        {
                            this._CourseMaterialObject.SaveLang(cultureInfo.Name,
                                                                courseMaterialLabel,
                                                                courseMaterialSearchTags);
                        }
                    }
                }                

                // rebind the course material grid
                this._CourseMaterialGrid.BindData();

                // reload the saved course material
                this._LoadCourseMaterialModifyModalContent(sender, e);

                // display the feedback
                this._CourseMaterialModifyModal.DisplayFeedback(_GlobalResources.CourseMaterialHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CourseMaterialModifyModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CourseMaterialModifyModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CourseMaterialModifyModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CourseMaterialModifyModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CourseMaterialModifyModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion
        #endregion
    }
}