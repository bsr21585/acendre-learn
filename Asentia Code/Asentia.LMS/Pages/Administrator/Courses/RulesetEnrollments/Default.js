﻿// Method to launch the new enrollment options modal
function LaunchNewEnrollmentOptionsModal() {
    $("#NewEnrollmentOptionsModalLaunchButton").click();
}

// Function: InitializeSortableOnRuleSetEnrollmentGrid
// Method to make the rows of the ruleset enrollment grid sortable so that order (priority) can be changed
function InitializeSortableOnRuleSetEnrollmentGrid() {
    // attach "sortable" to the grid    
    $("#RuleSetEnrollmentGrid").sortable({
        items: '.GridDataRow,.GridDataRowAlternate',
        containment: '#RuleSetEnrollmentGrid',
        cursor: 'move',
        update: function (e, ui) {            
            UpdateSortedRuleSetEnrollmentGrid();
        },
        helper: function (event, ui) {
            return RuleSetEnrollmentGridSortableHelper(ui);
        }
    });
}

// Function : UpdateSortedRuleSetEnrollmentGrid
// Updates the ordering of rule set enrollments and the UI elements of the grid so that numbering and alternating row styles are correct.
function UpdateSortedRuleSetEnrollmentGrid() {
    // re-order the numbering of the ruleset enrollments
    $("#RuleSetEnrollmentGrid tr td a span.RuleSetEnrollmentOrderLabel").each(function (index) {
        $(this).text(index + 1);
    });

    // set the alternate row css
    $("#RuleSetEnrollmentGrid .GridDataRow").each(function (index) {
        if ($(this).hasClass("GridDataRowAlternate") && !(index % 2)) {
            $(this).removeClass("GridDataRowAlternate");
        }
        else if (!$(this).hasClass("GridDataRowAlternate") && (index % 2)) {
            $(this).addClass("GridDataRowAlternate");
        }
    });

    // set the ordering in the hidden field and save
    var rulesetEnrollmentOrdering = "";

    $("#RuleSetEnrollmentGrid tr td:first-child input").each(function (index) {
        rulesetEnrollmentOrdering = rulesetEnrollmentOrdering + $(this).val() + "|";
    });

    rulesetEnrollmentOrdering = rulesetEnrollmentOrdering.slice(0, -1);
    $("#RuleSetEnrollmentOrderHiddenField").val(rulesetEnrollmentOrdering);

    $.ajax({
        type: "POST",
        url: "Default.aspx/SaveRuleSetEnrollmentOrdering",
        data: "{rulesetEnrollmentOrdering: \"" + rulesetEnrollmentOrdering + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSaveRuleSetEnrollmentOrderingSuccess,
        failure: function (response) {
        },
        error: function (response) {
        }
    });
}

// Function : RuleSetEnrollmentGridSortableHelper
// Helper function to maintain the look of the table row being dragged.
function RuleSetEnrollmentGridSortableHelper(ui) {
    var $originals = ui.children();
    var $helper = ui.clone();

    $helper.children().each(function (index) {
        // Set helper cell sizes to match the original sizes
        $(this).width($originals.eq(index).width());
    });

    return $helper.clone().appendTo("body");
}

// Function: OnSaveRuleSetEnrollmentOrderingSuccess
// Success handler for AJAX save of ruleset enrollment ordering.
function OnSaveRuleSetEnrollmentOrderingSuccess(response) {
    var responseObject = response.d;
}