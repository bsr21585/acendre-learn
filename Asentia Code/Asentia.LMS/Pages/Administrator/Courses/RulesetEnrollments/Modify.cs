﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.Courses.RulesetEnrollments
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel RuleSetEnrollmentPropertiesFormContentWrapperContainer;
        public Panel CourseObjectMenuContainer;
        public Panel RuleSetEnrollmentPropertiesWrapperContainer;
        public Panel RuleSetEnrollmentPropertiesInstructionsPanel;
        public Panel RuleSetEnrollmentPropertiesFeedbackContainer;
        public Panel RuleSetEnrollmentPropertiesContainer;
        public Panel RuleSetEnrollmentPropertiesActionsPanel;
        public Panel RuleSetEnrollmentPropertiesTabPanelsContainer;
        public Panel ObjectOptionsPanel;
        #endregion

        #region Private Properties
        private Course _CourseObject;
        private RuleSetEnrollment _RuleSetEnrollmentObject;
        private AutoJoinRuleSet _AutoJoinRuleSet;
        private RuleSet _RuleSetObject;

        private bool _IsStartDatePassed = false;
        private bool _IsRecurring;
        private bool _IsFixedDate;
        private bool _RuleSetControlLoaded;

        private CheckBox _IsLockedByPrerequisites;
        private CheckBox _ForceReassignment;

        private TimeZoneSelector _Timezone;
        private DatePicker _DtStart;
        private DatePicker _DtEnd;

        private DateIntervalSelector _Delay;
        private DateIntervalSelector _Due;
        private DateIntervalSelector _Recur;
        private DateIntervalSelector _ExpiresFromStart;
        private DateIntervalSelector _ExpiresFromFirstLaunch;

        private TextBox _Label;
        private TextBox _LabelTextbox;

        private Button _RuleSetEnrollmentPropertiesSaveButton;
        private Button _RuleSetEnrollmentPropertiesCancelButton;
        private Button _AddModifyRuleSetModalButton;
        private Button _SubmitButtonMadal;
        private Button _CloseButtonMadal;

        private RadioButton _MatchAny;
        private RadioButton _MatchAll;

        private LinkButton _DeleteButton;
        private LinkButton _AddModifyRuleSetHiddenButton;

        private DataTable _Rules;
        private Grid _RuleSetsGrid;
     
        private HiddenField _RulesData;
        private HiddenField _IdRulesetModalHidden;
        private HiddenField _IdRulesetEnrollmentHiddenField;
       
        private ModalPopup _GridConfirmAction;
        private ModalPopup _AddModifyRuleSetModal;

        private Panel _RuleSetModalPropertiesContainer;
        private Panel _RuleSetsPanel;
        private Panel _RuleSetGridActionsPanel;
        private PlaceHolder _RuleSetControlPlaceHolder;

        private DataTable _EligibleCoursesForCheckBoxList;
        private DynamicCheckBoxList __SelectEligibleCoursesForRuleSetEnrollmentReplication;
        private ModalPopup _SelectCoursesForRuleSetEnrollmentReplication;

        private HiddenField _ModifyRulesetId;
        #endregion

        #region Page Init Event
        /// <summary>
        /// Page Init event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Init(object sender, EventArgs e)
        {
            this._RulesData = new HiddenField();
            this._RulesData.ID = "RulesData";
            this._IdRulesetModalHidden = new HiddenField();
            this._AddModifyRuleSetHiddenButton = new LinkButton();
            this._AddModifyRuleSetModalButton = new Button();
            
            this._RuleSetControlPlaceHolder = new PlaceHolder();
            this._LabelTextbox = new TextBox();
            this._RuleSetControlLoaded = false;
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.Courses.RulesetEnrollments.Modify.js");

            csm.RegisterStartupScript(typeof(Modify), "JsVariables", "var HeaderTextAddRuleset='" + _GlobalResources.NewRuleset
               + "';var IdRulesetModalHidden ='" + this._IdRulesetModalHidden.ID
               + "';var AddModifyRuleSetHiddenButton ='" + this._AddModifyRuleSetHiddenButton.ID
               + "';var ModifyRuleSetModalButton ='" + this._AddModifyRuleSetModalButton.ID
                + "';var RuleSetLabel_Field ='" + this._LabelTextbox.ID
               + "';", true);

            StringBuilder startUpCallsScript = new StringBuilder();

            if (this._ModifyRulesetId != null)
            {
                if (this._ModifyRulesetId.Value != String.Empty)
                {
                    this._AddModifyRuleSetModal.ShowModal();
                    startUpCallsScript.AppendLine("OnRuleSetModifyLoad('" + this._ModifyRulesetId.Value + "');");
                }
            }

            csm.RegisterStartupScript(typeof(Modify), "startUpCalls", startUpCallsScript.ToString(), true);


            if (!this._RuleSetControlLoaded)
            {
                if (this._RuleSetObject == null)
                {
                    this._AutoJoinRuleSet = new AutoJoinRuleSet();
                }
                else
                {
                    this._AutoJoinRuleSet = new AutoJoinRuleSet(this._RuleSetObject.Id);
                }

                this._AutoJoinRuleSet.ID = "Rule_Field";
                this._AutoJoinRuleSet.ClientIDMode = ClientIDMode.Static;

                this._RuleSetControlPlaceHolder.Controls.Clear();
                this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);
                EnsureChildControls();
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Handles the Page Load Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseEnrollmentManager))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("page-specific/administrator/courses/rulesetenrollments/Modify.css");

            // get the ruleset enrollment and course objects
            this._GetRuleSetEnrollmentAndCourseObjects();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _BuildAddModifyRuleSetModal
        /// <summary>
        /// _BuildAddModifyRuleSetModal
        /// </summary>
        private void _BuildAddModifyRuleSetModal(string targetControlId, Panel modalContainer)
        {
            this.ObjectOptionsPanel.Controls.Clear();
            EnsureChildControls();
            this._AddModifyRuleSetModal = new ModalPopup("AddModifyRuleSetModal");
            this._AddModifyRuleSetModal.ID = "AddModifyRuleSetModal";
            this._AddModifyRuleSetModal.CssClass += " AddModifyRuleSetModal";
            this._AddModifyRuleSetModal.Type = ModalPopupType.Form;
            this._AddModifyRuleSetModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG);
            this._AddModifyRuleSetModal.HeaderIconAlt = _GlobalResources.CreateModifyRuleset;
            this._AddModifyRuleSetModal.HeaderText = _GlobalResources.ModifyRuleset;
            this._AddModifyRuleSetModal.TargetControlID = targetControlId;
            this._AddModifyRuleSetModal.ReloadPageOnClose = false;
            this._AddModifyRuleSetModal.SubmitButton.Visible = false;
            this._AddModifyRuleSetModal.CloseButton.Visible = false;

            //adding custom  own submit button to cahnge text 
            this._SubmitButtonMadal = new Button();
            this._SubmitButtonMadal.Text = _GlobalResources.CreateRuleset;
            this._SubmitButtonMadal.OnClientClick = "getRules(this,'" + this._RulesData.ClientID + "')";
            this._SubmitButtonMadal.Command += new CommandEventHandler(this._AddModifyRuleSetModalSubmitButton_Command);
            this._SubmitButtonMadal.CssClass = "Button ActionButton SaveButton";

            //adding custom  own close button to cahnge text 
            this._CloseButtonMadal = new Button();
            this._CloseButtonMadal.Text = _GlobalResources.Close;
            this._CloseButtonMadal.CssClass = "Button NonActionButton";
            this._CloseButtonMadal.OnClientClick = "HideModalPopup();return false;";

            //adding maintenance submit button to simulate button click.            
            this._AddModifyRuleSetModalButton.ClientIDMode = ClientIDMode.Static;
            this._AddModifyRuleSetModalButton.ID = "ModifyRuleSetModalButton";
            this._AddModifyRuleSetModalButton.Command += new CommandEventHandler(this._AddModifyRuleSetModalButton_Command);
            this._AddModifyRuleSetModalButton.Style.Add("display", "none");

            this._RuleSetModalPropertiesContainer = new Panel();
            this._RuleSetModalPropertiesContainer.ID = "RuleSetModalPropertiesContainer";

            //add controls to container
            this._RuleSetModalPropertiesContainer.Controls.Add(this._AddModifyRuleSetModalButton);

            // name container

            this._LabelTextbox.ID = "Label_Field";
            this._LabelTextbox.CssClass = "InputMedium";

            this._RuleSetModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Label",
                                                             _GlobalResources.Label,
                                                             this._LabelTextbox.ID,
                                                             this._LabelTextbox,
                                                             true,
                                                             true,
                                                             true));

            //Match any container
            Panel matchAnyFieldContainer = new Panel();
            matchAnyFieldContainer.ID = "MatchAny_Container";
            matchAnyFieldContainer.CssClass = "FormFieldContainer";

            // Match any input
            Panel matchAnyFieldInputContainer = new Panel();
            matchAnyFieldInputContainer.ID = "MatchAny_InputContainer";
            matchAnyFieldInputContainer.CssClass = "FormFieldInputContainer";

            this._MatchAny = new RadioButton();
            this._MatchAny.ID = "matchAnyRadioButton";
            this._MatchAny.Text = _GlobalResources.MatchAny;
            this._MatchAny.Checked = true;
            this._MatchAny.GroupName = "matchAny";

            this._MatchAll = new RadioButton();
            this._MatchAll.ID = "MatchAllRadioButton";
            this._MatchAll.Text = _GlobalResources.MatchAll;
            this._MatchAll.GroupName = "matchAny";

            matchAnyFieldInputContainer.Controls.Add(this._MatchAny);
            matchAnyFieldInputContainer.Controls.Add(this._MatchAll);

            // Match any label
            Panel matchAnyFieldLabelContainer = new Panel();
            matchAnyFieldLabelContainer.ID = "MatchAny_LabelContainer";
            matchAnyFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label matchAnyFieldLabel = new Label();
            matchAnyFieldLabel.Text = _GlobalResources.Match + ": ";

            matchAnyFieldLabelContainer.Controls.Add(matchAnyFieldLabel);

            // add controls to container
            matchAnyFieldContainer.Controls.Add(matchAnyFieldLabelContainer);
            matchAnyFieldContainer.Controls.Add(matchAnyFieldInputContainer);

            this._RuleSetModalPropertiesContainer.Controls.Add(matchAnyFieldContainer);

            // rules container
            Panel rulesFieldContainer = new Panel();
            rulesFieldContainer.ID = "Rule_Container";
            rulesFieldContainer.CssClass = "FormFieldContainer";

            // rules input
            Panel rulesFieldInputContainer = new Panel();
            rulesFieldInputContainer.ID = "Rule_InputContainer";
            rulesFieldInputContainer.CssClass = "FormFieldInputContainer";

            rulesFieldInputContainer.Controls.Add(this._RuleSetControlPlaceHolder);

            // rules label
            Panel rulesFieldLabelContainer = new Panel();
            rulesFieldLabelContainer.ID = "Rule_LabelContainer";
            rulesFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label rulesFieldLabel = new Label();
            rulesFieldLabel.Text = _GlobalResources.Rules + ": ";
            rulesFieldLabel.AssociatedControlID = this._RuleSetControlPlaceHolder.ID;

            Label requiredAsterisk = new Label();
            requiredAsterisk.Text = "* ";
            requiredAsterisk.CssClass = "RequiredAsterisk";
            rulesFieldLabelContainer.Controls.Add(requiredAsterisk);

            rulesFieldLabelContainer.Controls.Add(rulesFieldLabel);

            // error panel
            Panel rulesFieldErrorContainer = new Panel();
            rulesFieldErrorContainer.ID = "RuleSetRule_ErrorContainer";
            rulesFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to container
            rulesFieldContainer.Controls.Add(rulesFieldLabelContainer);
            rulesFieldContainer.Controls.Add(rulesFieldErrorContainer);
            rulesFieldContainer.Controls.Add(rulesFieldInputContainer);

            this._RuleSetModalPropertiesContainer.Controls.Add(rulesFieldContainer);
            this._RuleSetModalPropertiesContainer.Controls.Add(this._RulesData);

            this._IdRulesetModalHidden.ClientIDMode = ClientIDMode.Static;
            this._IdRulesetModalHidden.ID = "IdRulesetModalHidden";

            this._RuleSetModalPropertiesContainer.Controls.Add(this._IdRulesetModalHidden);

            this._AddModifyRuleSetModal.AddControlToBody(this._RuleSetModalPropertiesContainer);
            this._AddModifyRuleSetModal.AddControlToBody(this._SubmitButtonMadal);
            this._AddModifyRuleSetModal.AddControlToBody(this._CloseButtonMadal);
            modalContainer.Controls.Add(this._AddModifyRuleSetModal);

            EnsureChildControls();

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _ValidateRuleSetPropertiesForm
        /// <summary>
        /// Validates the rule set properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateRuleSetPropertiesForm()
        {
            bool isValid = true;

            // label field
            if (String.IsNullOrWhiteSpace(this._LabelTextbox.Text.Trim()))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "Label", _GlobalResources.Label + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // rules control
            if (this._AutoJoinRuleSet != null)
            {
                this._Rules = this._AutoJoinRuleSet.GetRulesAfterValidatingData();

                if (this._Rules == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "RuleSetRule", _GlobalResources.OneOrMoreRulesAreInvalidHoverOverField_sToViewSpecificError_s);
                }
                else if (this._Rules.Rows.Count < 1)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "RuleSetRule", _GlobalResources.YouMustHaveAtLeastOneRule);
                }
            }

            return isValid;
        }
        #endregion

        #region _AddModifyRuleSetModalSubmitButton_Command
        /// <summary>
        /// _AddModifyRuleSetModalSubmitButton_Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> </param>
        private void _AddModifyRuleSetModalSubmitButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable rulesData = new DataTable();

                if (!String.IsNullOrWhiteSpace(this._RulesData.Value))
                {
                    rulesData.Columns.Add("idRule", typeof(int));
                    rulesData.Columns.Add("userField", typeof(string));
                    rulesData.Columns.Add("operator", typeof(string));
                    rulesData.Columns.Add("textValue", typeof(string));

                    List<RuleObject> allRules = new JavaScriptSerializer().Deserialize<List<RuleObject>>(this._RulesData.Value);
                    foreach (RuleObject rule in allRules)
                    {
                        int idRule = 0;
                        int.TryParse(rule.IdRule, out idRule);
                        rulesData.Rows.Add(idRule, rule.UserField, rule.Operator, rule.Value);
                    }

                    if (rulesData.Rows.Count > 0)
                    {
                        this._AutoJoinRuleSet = new AutoJoinRuleSet(rulesData);
                        this._AutoJoinRuleSet.ID = "Rule_Field";
                        this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);
                        this._RuleSetControlLoaded = true;
                    }

                    this._RulesData.Value = string.Empty;
                }

                int idRuleSet = Convert.ToInt32(this._IdRulesetModalHidden.Value);

                if (idRuleSet > 0)
                {
                    this._RuleSetObject = new RuleSet(idRuleSet);
                }

                // if there is no rule set object, create one
                if (this._RuleSetObject == null)
                { this._RuleSetObject = new RuleSet(); }

                // validate the form
                if (!this._ValidateRuleSetPropertiesForm())
                { throw new AsentiaException(); }

                // if this is an existing rule set, and not the default language, save language properties only
                // otherwise, save the whole object in its default language
                int id;

                // populate the object
                this._RuleSetObject.Label = this._LabelTextbox.Text;

                if (this._MatchAny.Checked)
                { this._RuleSetObject.IsAny = true; }
                else
                { this._RuleSetObject.IsAny = false; }

                // save the user, save its returned id to viewstate, and 
                // instansiate a new object with the id
                this._RuleSetObject.LinkedObjectId = this._RuleSetEnrollmentObject.Id;
                this._RuleSetObject.LinkedObjectType = RuleSetLinkedObjectType.RuleSetEnrollment;

                id = this._RuleSetObject.Save();

                Asentia.LMS.Library.Rule rules = new Asentia.LMS.Library.Rule();
                rules.IdRuleSet = id;
                if (this._Rules.Rows.Count > 0)
                {
                    rules.Save(this._Rules);
                }

                this._IdRulesetModalHidden.Value = id.ToString();

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string ruleSetTitle = null;

                        // get text boxes
                        TextBox languageSpecificRuleSetTitleTextBox = (TextBox)this._RuleSetModalPropertiesContainer.FindControl(this._LabelTextbox.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificRuleSetTitleTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificRuleSetTitleTextBox.Text))
                            { ruleSetTitle = languageSpecificRuleSetTitleTextBox.Text; }
                        }

                        // save the property if property is populated
                        if (!String.IsNullOrWhiteSpace(ruleSetTitle))
                        {
                            this._RuleSetObject.SaveLang(cultureInfo.Name, ruleSetTitle);
                        }
                    }
                }                

                // load the saved ruleset object
                this._RuleSetObject = new RuleSet(id);

                // rebind the grid and other controls;
                this._RuleSetsGrid.BindData();

                this._SubmitButtonMadal.Text = _GlobalResources.SaveChanges;

                // display the saved feedback
                this._AddModifyRuleSetModal.DisplayFeedback(_GlobalResources.RulesetPropertiesHaveBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dfnuEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dfnuEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _AddModifyRuleSetModalButton_Command
        /// <summary>
        /// _AddModifyRuleSetModalButton_Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AddModifyRuleSetModalButton_Command(object sender, CommandEventArgs e)
        {
            this._AddModifyRuleSetModal.ClearFeedback();
            int idRuleSet = Convert.ToInt32(this._IdRulesetModalHidden.Value);
            if (!this._RuleSetControlLoaded)
            {
                if (idRuleSet > 0)
                {
                    this._RuleSetObject = new RuleSet(idRuleSet);
                    this._AutoJoinRuleSet = new AutoJoinRuleSet(idRuleSet);
                    this._SubmitButtonMadal.Text = _GlobalResources.SaveChanges;
                    this._AddModifyRuleSetModal.HeaderText = _GlobalResources.ModifyRuleset;

                }
                else
                {
                    this._RuleSetObject = new RuleSet();
                    this._AutoJoinRuleSet = new AutoJoinRuleSet();
                    this._SubmitButtonMadal.Text = _GlobalResources.CreateRuleset;
                    this._AddModifyRuleSetModal.HeaderText = _GlobalResources.NewRuleset;
                }
                this._AutoJoinRuleSet.ID = "Rule_Field";
                this._AutoJoinRuleSet.ClientIDMode = ClientIDMode.Static;
                this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);
                EnsureChildControls();
                this._PopulatePropertiesInputElements();
            }
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// _PopulatePropertiesInputElements
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _PopulatePropertiesInputElements()
        {
            if (this._RuleSetObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                //rule set title
                bool isDefaultPopulated = false;

                foreach (RuleSet.LanguageSpecificProperty ruleSetLanguageSpecificProperty in this._RuleSetObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (ruleSetLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._LabelTextbox.Text = ruleSetLanguageSpecificProperty.Label;
                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificRuleSetLabelTextBox = (TextBox)this._RuleSetModalPropertiesContainer.FindControl(this._LabelTextbox.ID + "_" + ruleSetLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificRuleSetLabelTextBox != null)
                        { languageSpecificRuleSetLabelTextBox.Text = ruleSetLanguageSpecificProperty.Label; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._LabelTextbox.Text = this._RuleSetObject.Label;
                }

                //populating language independent properties
                if (this._RuleSetObject.IsAny)
                {
                    this._MatchAny.Checked = true;
                }
                else
                {
                    this._MatchAll.Checked = true;
                }
            }
            else
            {

                this._LabelTextbox.Text = string.Empty;
                this._MatchAny.Checked = true;
            }

        }

        #endregion

        #region _GetRuleSetEnrollmentAndCourseObjects
        /// <summary>
        /// Gets the course and ruleset enrollment objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetRuleSetEnrollmentAndCourseObjects()
        {
            // get the id querystring parameter
            int qsCId = this.QueryStringInt("cid", 0);
            int vsCId = this.ViewStateInt(this.ViewState, "cid", 0);
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            // get course object - course object MUST be specified and exist for this page to load
            if (qsCId > 0 || vsCId > 0)
            {
                int cid = 0;

                if (qsCId > 0)
                { cid = qsCId; }

                if (vsCId > 0)
                { cid = vsCId; }

                try
                {
                    if (cid > 0)
                    { this._CourseObject = new Course(cid); }
                }
                catch
                { Response.Redirect("~/administrator/courses"); }
            }
            else
            { Response.Redirect("~/administrator/courses"); }

            // get ruleset enrollment object (if exists), and set the ruleset enrollment type based on either properties of
            // the ruleset enrollment, or the type specified in a querystring parameter, if no ruleset enrollment object
            // or type cannot be set from querystring parameter, redirect because an invalid ruleset enrollment would be created
            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    {
                        this._RuleSetEnrollmentObject = new RuleSetEnrollment(id);

                        if (this._RuleSetEnrollmentObject.DtStart <= AsentiaSessionState.UtcNow)
                        { this._IsStartDatePassed = true; }

                        if (this._RuleSetEnrollmentObject.RecurInterval > 0)
                        { this._IsRecurring = true; }

                        if (this._RuleSetEnrollmentObject.IsFixedDate)
                        { this._IsFixedDate = true; }
                    }
                }
                catch
                { Response.Redirect("~/administrator/courses/Modify.aspx?id=" + this._CourseObject.Id.ToString()); }
            }
            else
            {
                switch ((RuleSetEnrollmentType)this.QueryStringInt("type"))
                {
                    case RuleSetEnrollmentType.FixedDateOneTime:
                        this._IsRecurring = false;
                        this._IsFixedDate = true;
                        break;
                    case RuleSetEnrollmentType.RelativeDateOneTime:
                        this._IsRecurring = false;
                        this._IsFixedDate = false;
                        break;
                    case RuleSetEnrollmentType.FixedDateRecurring:
                        this._IsRecurring = true;
                        this._IsFixedDate = true;
                        break;
                    case RuleSetEnrollmentType.RelativeDateRecurring:
                        this._IsRecurring = true;
                        this._IsFixedDate = false;
                        break;
                    default:
                        Response.Redirect("~/administrator/courses/Modify.aspx?id=" + this._CourseObject.Id.ToString());
                        break;
                }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Build the form input controls on page
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.RuleSetEnrollmentPropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.RuleSetEnrollmentPropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the course object menu            
            if (this._CourseObject != null)
            {
                CourseObjectMenu courseObjectMenu = new CourseObjectMenu(this._CourseObject);
                courseObjectMenu.SelectedItem = CourseObjectMenu.MenuObjectItem.RulesetEnrollments;

                this.CourseObjectMenuContainer.Controls.Add(courseObjectMenu);
            }

            // build the ruleset enrollment properties form
            this._BuildRuleSetEnrollmentPropertiesForm();

            // build the ruleset enrollment properties form actions panel
            this._BuildRuleSetEnrollmentPropertiesActionsPanel();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get course title information
            string courseTitleInInterfaceLanguage = this._CourseObject.Title;
            string courseImagePath;
            string courseImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Course.LanguageSpecificProperty courseLanguageSpecificProperty in this._CourseObject.LanguageSpecificProperties)
                {
                    if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { courseTitleInInterfaceLanguage = courseLanguageSpecificProperty.Title; }
                }
            }

            if (this._CourseObject.Avatar != null)
            {
                courseImagePath = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + this._CourseObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                courseImageCssClass = "AvatarImage";
            }
            else
            {
                courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            }

            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;            
            string pageTitle;

            if (this._RuleSetEnrollmentObject != null)
            {
                string ruleSetEnrollmentTitleInInterfaceLanguage = this._RuleSetEnrollmentObject.Label;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (RuleSetEnrollment.LanguageSpecificProperty ruleSetEnrollmentLanguageSpecificProperty in this._RuleSetEnrollmentObject.LanguageSpecificProperties)
                    {
                        if (ruleSetEnrollmentLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { ruleSetEnrollmentTitleInInterfaceLanguage = ruleSetEnrollmentLanguageSpecificProperty.Label; }
                    }
                }

                breadCrumbPageTitle = ruleSetEnrollmentTitleInInterfaceLanguage;
                pageTitle = ruleSetEnrollmentTitleInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewRulesetEnrollment;
                pageTitle = _GlobalResources.NewRulesetEnrollment;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Courses, "/administrator/courses"));
            breadCrumbLinks.Add(new BreadcrumbLink(courseTitleInInterfaceLanguage, "/administrator/courses/Dashboard.aspx?id=" + this._CourseObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.RulesetEnrollments, "/administrator/courses/rulesetenrollments/Default.aspx?cid=" + this._CourseObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, courseTitleInInterfaceLanguage, courseImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG), courseImageCssClass);
        }
        #endregion

        #region _BuildRuleSetEnrollmentPropertiesForm
        /// <summary>
        /// Builds the ruleset enrollment Properties form.
        /// </summary>
        private void _BuildRuleSetEnrollmentPropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.RuleSetEnrollmentPropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateRulesetEnrollment, true);

            // clear controls from container
            this.RuleSetEnrollmentPropertiesContainer.Controls.Clear();

            // build the ruleset enrollment properties form tabs
            this._BuildRuleSetEnrollmentPropertiesFormTabs();

            this.RuleSetEnrollmentPropertiesTabPanelsContainer = new Panel();
            this.RuleSetEnrollmentPropertiesTabPanelsContainer.ID = "RuleSetEnrollmentProperties_TabPanelsContainer";
            this.RuleSetEnrollmentPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.RuleSetEnrollmentPropertiesContainer.Controls.Add(this.RuleSetEnrollmentPropertiesTabPanelsContainer);

            // build the "properties" panel of the ruleset enrollment properties form
            this._BuildRuleSetEnrollmentPropertiesFormPropertiesPanel();

            if (this._RuleSetEnrollmentObject != null)
            {
                // build the rulesets form
                this._BuildRuleSetEnrollmentPropertiesFormRulesetsPanel();

                this._BuildRulesetGridActionsPanel();
                
                this._BuildGridActionsModal();
            }

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulateRuleSetEnrollmentPropertiesInputElements();
        }
        #endregion

        #region _BuildRuleSetEnrollmentPropertiesFormTabs
        /// <summary>
        /// Build rule set enrollment properties form tabs
        /// </summary>
        private void _BuildRuleSetEnrollmentPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));

            if (this._RuleSetEnrollmentObject != null)
            { tabs.Enqueue(new KeyValuePair<string, string>("RuleSets", _GlobalResources.Rulesets)); }            

            // build and attach the tabs
            this.RuleSetEnrollmentPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("RuleSetEnrollmentProperties", tabs, null, this.Page));
        }
        #endregion

        #region _BuildRuleSetEnrollmentPropertiesFormPropertiesPanel
        /// <summary>
        /// Build rule set enrollment properties form properties panel
        /// </summary>
        private void _BuildRuleSetEnrollmentPropertiesFormPropertiesPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "RuleSetEnrollmentProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.Attributes.Add("style", "display: block;");

            Panel propertiesOptionsPanel = new Panel();
            propertiesOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "PropertiesOptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // REPLICATE RULESET ENROLLMENT
            if (this._RuleSetEnrollmentObject != null)
            {
                if (this._RuleSetEnrollmentObject.IdParentRuleSetEnrollment == 0)
                {
                    // populate datatables with lists of courses that ruleset enrollment can be replicated to and those it has already been replicated to
                    this._EligibleCoursesForCheckBoxList = RuleSetEnrollment.IdsAndCourseNamesForRuleSetEnrollmentReplicationList(this._RuleSetEnrollmentObject.Id, null);

                    Panel replicateRulesetEnrollmentLink = this.BuildOptionsPanelImageLink("ReplicateRulesetEnrollmentLink",
                                                                                           null,
                                                                                           "javascript: void(0);",
                                                                                           null,
                                                                                           _GlobalResources.ReplicateRulesetEnrollmentToOtherCourses,
                                                                                           null,
                                                                                           ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG),
                                                                                           ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG));
                    
                    optionsPanelLinksContainer.Controls.Add(replicateRulesetEnrollmentLink);

                    propertiesOptionsPanel.Controls.Add(optionsPanelLinksContainer);

                    this._BuildSelectCoursesModal(replicateRulesetEnrollmentLink.ID);

                    propertiesPanel.Controls.Add(propertiesOptionsPanel);


                }
            }

            // ruleset enrollment label field
            this._Label = new TextBox();
            this._Label.ID = "RuleSetEnrollmentLabel_Field";
            this._Label.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentLabel",
                                                             _GlobalResources.Label,
                                                             this._Label.ID,
                                                             this._Label,
                                                             true,
                                                             true,
                                                             true));

            // ruleset enrollment is locked by prerequisites field
            this._IsLockedByPrerequisites = new CheckBox();
            this._IsLockedByPrerequisites.ID = "RuleSetEnrollmentIsLockedByPrerequisites_Field";
            this._IsLockedByPrerequisites.Text = _GlobalResources.PreventLearnersFromLaunchingThisCourseUntilPrerequisitesSatisfied;
            this._IsLockedByPrerequisites.Checked = true;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentIsLockedByPrerequisites",
                                                             _GlobalResources.Prerequisites,
                                                             this._IsLockedByPrerequisites.ID,
                                                             this._IsLockedByPrerequisites,
                                                             false,
                                                             true,
                                                             false));

            // ruleset enrollment force reassignment field
            if (!this._IsRecurring)
            {
                this._ForceReassignment = new CheckBox();
                this._ForceReassignment.ID = "RuleSetEnrollmentForceReassignment_Field";
                this._ForceReassignment.Text = _GlobalResources.ForceAssignmentOfThisRulesetEnrollmentToLearnersWhoHavePreviouslyCompletedAOneTimeEnrollmentOfThisCourse;
                this._ForceReassignment.Checked = true;

                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentForceReassignment",
                                                             _GlobalResources.ForceAssignment,
                                                             this._ForceReassignment.ID,
                                                             this._ForceReassignment,
                                                             false,
                                                             true,
                                                             false));
            }

            // ruleset enrollment date start field
            // if the start date has not passed, its still editable, otherwise it is not
            if (!this._IsStartDatePassed)
            {
                this._DtStart = new DatePicker("RuleSetEnrollmentDtStart_Field", false, true, true);
                this._DtStart.NowCheckboxText = _GlobalResources.StartEnrollmentImmediately;
                this._DtStart.Value = TimeZoneInfo.ConvertTimeFromUtc(AsentiaSessionState.UtcNow.AddMinutes(1), TimeZoneInfo.FindSystemTimeZoneById(new Timezone(AsentiaSessionState.GlobalSiteObject.IdTimezone).dotNetName));

                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentDtStart",
                                                             _GlobalResources.LifespanStart,
                                                             this._DtStart.ID,
                                                             this._DtStart,
                                                             true,
                                                             true,
                                                             false));
            }
            else
            {
                DateTime dateValue = TimeZoneInfo.ConvertTimeFromUtc(this._RuleSetEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetEnrollmentObject.IdTimezone).dotNetName));
                Label ruleSetEnrollmentDtStartFieldStaticValue = new Label();

                ruleSetEnrollmentDtStartFieldStaticValue.Text = dateValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern) + " @ " + dateValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern);

                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentDtStart",
                                                             _GlobalResources.LifespanStart,
                                                             null,
                                                             ruleSetEnrollmentDtStartFieldStaticValue,
                                                             true,
                                                             true,
                                                             false));
            }

            // ruleset enrollment date end field
            if (!this._IsFixedDate || (this._IsFixedDate && this._IsRecurring))
            {
                this._DtEnd = new DatePicker("RuleSetEnrollmentDtEnd_Field", true, false, true);
                this._DtEnd.NoneCheckboxText = _GlobalResources.NoEndDate;
                this._DtEnd.NoneCheckBoxChecked = true;
            }
            else
            {
                this._DtEnd = new DatePicker("RuleSetEnrollmentDtEnd_Field", false, false, false);
            }

            if (this._IsFixedDate && !this._IsRecurring)
            { this._DtEnd.Value = TimeZoneInfo.ConvertTimeFromUtc(AsentiaSessionState.UtcNow.AddYears(1), TimeZoneInfo.FindSystemTimeZoneById(new Timezone(AsentiaSessionState.GlobalSiteObject.IdTimezone).dotNetName)); }

            Label ruleSetEnrollmentDtEndFieldLabel = new Label();
            ruleSetEnrollmentDtEndFieldLabel.Text = _GlobalResources.LifespanEnd + ": ";
            ruleSetEnrollmentDtEndFieldLabel.AssociatedControlID = this._DtEnd.ID;

            if (this._IsFixedDate && !this._IsRecurring)
            {
                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentDtEnd",
                                                             _GlobalResources.LifespanEnd,
                                                             this._DtEnd.ID,
                                                             this._DtEnd,
                                                             true,
                                                             true,
                                                             false));
            }
            else
            {
                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentDtEnd",
                                                             _GlobalResources.LifespanEnd,
                                                             this._DtEnd.ID,
                                                             this._DtEnd,
                                                             false,
                                                             true,
                                                             false));
            }

            // ruleset enrollment timezone field
            this._Timezone = new TimeZoneSelector();
            this._Timezone.ID = "RuleSetEnrollmentTimezone_Field";
            this._Timezone.SelectedValue = AsentiaSessionState.GlobalSiteObject.IdTimezone.ToString();

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentTimezone",
                                                             _GlobalResources.LifespanTimezone,
                                                             this._Timezone.ID,
                                                             this._Timezone,
                                                             true,
                                                             true,
                                                             false));

            if (!this._IsFixedDate)
            {
                // ruleset enrollment delay field
                this._Delay = new DateIntervalSelector("RuleSetEnrollmentDelay_Field", true);
                this._Delay.NoneCheckBoxChecked = true;
                this._Delay.TextBeforeSelector = _GlobalResources.LearnersInheritThisEnrollment;
                this._Delay.TextAfterSelector = _GlobalResources.AfterMeetingRulesetCriteria;
                this._Delay.NoneCheckboxText = _GlobalResources.NoDelay;

                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentDelay",
                                                             _GlobalResources.Delay,
                                                             this._Delay.ID,
                                                             this._Delay,
                                                             false,
                                                             true,
                                                             false));
            }

            // ruleset enrollment due field
            if (this._IsRecurring)
            { this._Due = new DateIntervalSelector("RuleSetEnrollmentDue_Field", false); }
            else
            {
                this._Due = new DateIntervalSelector("RuleSetEnrollmentDue_Field", true);
                this._Due.NoneCheckboxText = _GlobalResources.NoDueDate;
                this._Due.NoneCheckBoxChecked = true;
            }

            this._Due.TextBeforeSelector = _GlobalResources.LearnersMustCompleteTheCourseWithin;
            this._Due.TextAfterSelector = _GlobalResources.AfterEnrollment;

            if (this._IsRecurring)
            {
                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentDue",
                                                             _GlobalResources.Due,
                                                             this._Due.ID,
                                                             this._Due,
                                                             true,
                                                             true,
                                                             false));
            }
            else
            {
                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentDue",
                                                             _GlobalResources.Due,
                                                             this._Due.ID,
                                                             this._Due,
                                                             false,
                                                             true,
                                                             false));
            }

            // ruleset enrollment recur field
            if (this._IsRecurring)
            {
                this._Recur = new DateIntervalSelector("RuleSetEnrollmentRecur_Field", false);
                this._Recur.TextBeforeSelector = _GlobalResources.EnrollmentRepeatsEvery;
                this._Recur.TextAfterSelector = _GlobalResources.EmptyString;

                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentRecur",
                                                             _GlobalResources.Recurrence,
                                                             this._Recur.ID,
                                                             this._Recur,
                                                             true,
                                                             true,
                                                             false));
            }

            // ruleset enrollment expires from start field
            if (!this._IsFixedDate || (this._IsFixedDate && this._IsRecurring))
            {
                if (this._IsRecurring)
                { this._ExpiresFromStart = new DateIntervalSelector("RuleSetEnrollmentExpiresFromStart_Field", false); }
                else
                {
                    this._ExpiresFromStart = new DateIntervalSelector("RuleSetEnrollmentExpiresFromStart_Field", true);
                    this._ExpiresFromStart.NoneCheckboxText = _GlobalResources.Indefinite;
                    this._ExpiresFromStart.NoneCheckBoxChecked = true;
                }

                this._ExpiresFromStart.TextBeforeSelector = _GlobalResources.LearnersHaveAccessToTheCourseFor;
                this._ExpiresFromStart.TextAfterSelector = _GlobalResources.AfterEnrollment;

                if (this._IsRecurring)
                {
                    propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentExpiresFromStart",
                                                             _GlobalResources.AccessFromStart,
                                                             this._ExpiresFromStart.ID,
                                                             this._ExpiresFromStart,
                                                             true,
                                                             true,
                                                             false));
                }
                else
                {
                    propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentExpiresFromStart",
                                                             _GlobalResources.AccessFromStart,
                                                             this._ExpiresFromStart.ID,
                                                             this._ExpiresFromStart,
                                                             false,
                                                             true,
                                                             false));
                }
            }

            // ruleset enrollment expires from first launch field
            this._ExpiresFromFirstLaunch = new DateIntervalSelector("RuleSetEnrollmentExpiresFromFirstLaunch_Field", true);
            this._ExpiresFromFirstLaunch.NoneCheckboxText = _GlobalResources.Indefinite;
            this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = true;
            this._ExpiresFromFirstLaunch.TextBeforeSelector = _GlobalResources.LearnersHaveAccessToTheCourseFor;
            this._ExpiresFromFirstLaunch.TextAfterSelector = _GlobalResources.AfterFirstLaunchOfCourse;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentExpiresFromFirstLaunch",
                                                             _GlobalResources.AccessFromFirstLaunch,
                                                             this._ExpiresFromFirstLaunch.ID,
                                                             this._ExpiresFromFirstLaunch,
                                                             false,
                                                             true,
                                                             false));

            // attach panel to container
            this.RuleSetEnrollmentPropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildSelectCoursesModal
        /// <summary>
        /// Builds the modal for selecting courses to add to the learning path.
        /// </summary>
        private void _BuildSelectCoursesModal(string targetControlId)
        {
            // set modal properties
            this._SelectCoursesForRuleSetEnrollmentReplication = new ModalPopup("SelectCoursesForRuleSetEnrollmentReplicationModal");
            this._SelectCoursesForRuleSetEnrollmentReplication.Type = ModalPopupType.Form;
            this._SelectCoursesForRuleSetEnrollmentReplication.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            this._SelectCoursesForRuleSetEnrollmentReplication.HeaderIconAlt = _GlobalResources.SelectCourse_s;
            this._SelectCoursesForRuleSetEnrollmentReplication.HeaderText = _GlobalResources.SelectCourse_s;
            this._SelectCoursesForRuleSetEnrollmentReplication.TargetControlID = targetControlId;
            this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButtonCustomText = _GlobalResources.ReplicateToCourse_s;
            this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButton.Command += new CommandEventHandler(this._ReplicateToCoursesSelectCoursesButton_Command);
            this._SelectCoursesForRuleSetEnrollmentReplication.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCoursesForRuleSetEnrollmentReplication.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectCoursesForRuleSetEnrollmentReplication.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication = new DynamicCheckBoxList("SelectEligibleCoursesForRuleSetEnrollmentReplication");
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.NoRecordsFoundMessage = _GlobalResources.NoCoursesFound;
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.SearchButton.Command += new CommandEventHandler(this._SearchSelectCoursesButton_Command);
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectCoursesButton_Command);
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataSource = this._EligibleCoursesForCheckBoxList;
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataTextField = "title";
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataValueField = "idCourse";
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataBound += this._SelectCoursesModal_RowDataBound;
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataBind();

            // add controls to body
            this._SelectCoursesForRuleSetEnrollmentReplication.AddControlToBody(this.__SelectEligibleCoursesForRuleSetEnrollmentReplication);

            // add modal to container
            this.RuleSetEnrollmentPropertiesTabPanelsContainer.Controls.Add(this._SelectCoursesForRuleSetEnrollmentReplication);
        }
        #endregion

        #region _SelectCoursesModal_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the "Select Learning Path(s)" searchable checkboxlist control.
        /// </summary>
        public void _SelectCoursesModal_RowDataBound(Object sender, EventArgs e)
        {
            // create hyperlink for those courses that the ruleset enrollment has already been replicated to and is synced with
            foreach (DataRow row in this._EligibleCoursesForCheckBoxList.Rows)
            {
                if (!Convert.IsDBNull(row["idParentRuleSetEnrollment"]))
                {
                    if (Convert.ToInt32(row["idParentRuleSetEnrollment"]) == this._RuleSetEnrollmentObject.Id)
                    {
                        ListItem courseName = this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.Items.FindByValue(row["idCourse"].ToString());
                        courseName.Selected = true;

                        courseName.Text = "<a href = 'Modify.aspx?cid=" + row["idCourse"].ToString() + "&id=" + row["idRuleSetEnrollment"].ToString() + "'>" + row["title"].ToString();
                    }
                }

            }
        }
        #endregion

        #region _SearchSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Course(s)" searchable checkboxlist control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCoursesForRuleSetEnrollmentReplication.ClearFeedback();

            // clear the checkboxlist control
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.Items.Clear();

            // do the search
            this._EligibleCoursesForCheckBoxList = RuleSetEnrollment.IdsAndCourseNamesForRuleSetEnrollmentReplicationList(this._RuleSetEnrollmentObject.Id, this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.SearchTextBox.Text); 

            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataSource = this._EligibleCoursesForCheckBoxList;
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataTextField = "title";
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataValueField = "idCourse";
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataBound += this._SelectCoursesModal_RowDataBound;
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataBind();

            // if no records are available then disable the submit button
            if (this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.Items.Count == 0)
            {
                this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButton.Enabled = false;
                this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButton.Enabled = true;
                this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _ClearSearchSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Courses" searchable checkboxlist control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCoursesForRuleSetEnrollmentReplication.ClearFeedback();

            // clear the checkboxlist control and search text box
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.Items.Clear();
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.SearchTextBox.Text = "";

            // clear the search
            this._EligibleCoursesForCheckBoxList = RuleSetEnrollment.IdsAndCourseNamesForRuleSetEnrollmentReplicationList(this._RuleSetEnrollmentObject.Id, null); 

            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataSource = this._EligibleCoursesForCheckBoxList;
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataTextField = "title";
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataValueField = "idCourse";
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataBound += this._SelectCoursesModal_RowDataBound;
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataBind();

            // if records available then enable the submit button
            if (this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.Items.Count > 0)
            {
                this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButton.Enabled = true;
                this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButton.Enabled = false;
                this._SelectCoursesForRuleSetEnrollmentReplication.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _ReplicateToCoursesSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Courses" searchable checkboxlist control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ReplicateToCoursesSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // delcare data tables to hold selected and not selected courses
            DataTable selectedCourses = new DataTable();
            selectedCourses.Columns.Add("id", typeof(int));

            DataTable notSelectedCourses = new DataTable();
            notSelectedCourses.Columns.Add("id", typeof(int));

            List<string> selectedValues = new List<string>();
            selectedValues = this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.GetSelectedValues();

            List<string> notSelectedValues = new List<string>();
            notSelectedValues = this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.GetNotSelectedValues();

            // put ids into datatable 
            foreach (string courseId in selectedValues)
            {
                selectedCourses.Rows.Add(Convert.ToInt32(courseId));
            }

            foreach (string courseId in notSelectedValues)
            {
                notSelectedCourses.Rows.Add(Convert.ToInt32(courseId));
            }

            RuleSetEnrollment.Replicate(this._RuleSetEnrollmentObject.Id, selectedCourses, notSelectedCourses);

            this._SelectCoursesForRuleSetEnrollmentReplication.DisplayFeedback(_GlobalResources.RuleSetEnrollmentReplicated, false);

            this._EligibleCoursesForCheckBoxList = RuleSetEnrollment.IdsAndCourseNamesForRuleSetEnrollmentReplicationList(this._RuleSetEnrollmentObject.Id, null);

            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataSource = this._EligibleCoursesForCheckBoxList;
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataTextField = "title";
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataValueField = "idCourse";
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataBound += this._SelectCoursesModal_RowDataBound;
            this.__SelectEligibleCoursesForRuleSetEnrollmentReplication.CheckBoxListControl.DataBind();
        }
        #endregion

        #region _BuildRuleSetEnrollmentPropertiesFormRulesetsPanel
        /// <summary>
        /// Build rule set enrollment properties form rulesets panel
        /// </summary>
        private void _BuildRuleSetEnrollmentPropertiesFormRulesetsPanel()
        {
            this._RuleSetsPanel = new Panel();
            this._RuleSetsPanel.ID = "RuleSetEnrollmentProperties_" + "RuleSets" + "_TabPanel";
            this._RuleSetsPanel.Attributes.Add("style", "display: none;");

            UpdatePanel ruleSetsPanel = new UpdatePanel();
            ruleSetsPanel.ID = "RuleSetEnrollmentPropertiesUpdatePanel";

            Panel ruleSetsOptionsPanel = new Panel();
            ruleSetsOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD RULESET
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddRuleSetLink",
                                                null,
                                                "javascript: void(0);",
                                                "OnRuleSetModifyClick(0); return false;",
                                                _GlobalResources.NewRuleset,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            ruleSetsOptionsPanel.Controls.Add(optionsPanelLinksContainer);

            this._AddModifyRuleSetHiddenButton.ClientIDMode = ClientIDMode.Static;
            this._AddModifyRuleSetHiddenButton.ID = "AddModifyRuleSetHiddenButton";            
            this._AddModifyRuleSetHiddenButton.Style.Add("display", "none");

            this._BuildAddModifyRuleSetModal(this._AddModifyRuleSetHiddenButton.ID, this.ObjectOptionsPanel);

            this._IdRulesetEnrollmentHiddenField = new HiddenField();
            this._IdRulesetEnrollmentHiddenField.ID = "IdRulesetEnrollmentHiddenField";
            this._IdRulesetEnrollmentHiddenField.Value = Convert.ToString(this._RuleSetEnrollmentObject.Id);

            this._ModifyRulesetId = new HiddenField();
            this._ModifyRulesetId.ID = "ModifyRulesetId";
            this._ModifyRulesetId.Value = this.QueryStringString("rsid", String.Empty);
            ruleSetsPanel.ContentTemplateContainer.Controls.Add(this._ModifyRulesetId);

            this.ObjectOptionsPanel.Controls.Add(this._IdRulesetEnrollmentHiddenField);
            this.ObjectOptionsPanel.Controls.Add(_AddModifyRuleSetHiddenButton);

            ruleSetsPanel.ContentTemplateContainer.Controls.Add(ruleSetsOptionsPanel);

            // GRID

            this._RuleSetsGrid = new Grid();
            this._RuleSetsGrid.ID = "RuleSetsGrid";

            this._RuleSetsGrid.ShowSearchBox = false;
            this._RuleSetsGrid.StoredProcedure = Library.RuleSet.GridProcedureForRuleSetEnrollment;
            this._RuleSetsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._RuleSetsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._RuleSetsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._RuleSetsGrid.AddFilter("@idRuleSetEnrollment", SqlDbType.Int, 4, this._RuleSetEnrollmentObject.Id);
            this._RuleSetsGrid.IdentifierField = "idRuleSet";
            this._RuleSetsGrid.DefaultSortColumn = "label";

            // data key names
            this._RuleSetsGrid.DataKeyNames = new string[] { "idRuleSet" };

            // columns
            GridColumn label = new GridColumn(_GlobalResources.Label, null); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this._RuleSetsGrid.AddColumn(label);

            // add row data bound event
            this._RuleSetsGrid.RowDataBound += new GridViewRowEventHandler(this._RuleSetsGrid_RowDataBound);

            // attach the grid to the update panel
            ruleSetsPanel.ContentTemplateContainer.Controls.Add(this._RuleSetsGrid);

            // bind the grid
            this._RuleSetsGrid.BindData();
            this._RuleSetsPanel.Controls.Add(ruleSetsPanel);

            this.RuleSetEnrollmentPropertiesTabPanelsContainer.Controls.Add(this._RuleSetsPanel);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildRulesetGridActionsPanel()
        {
            this._RuleSetGridActionsPanel = new Panel();
            this._RuleSetGridActionsPanel.ID = "RuleSetGridActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedRuleset_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this._RuleSetGridActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _RuleSetsGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the rulesets grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RuleSetsGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idRuleSet = Convert.ToInt32(rowView["idRuleSet"]);                

                // AVATAR, LABEL

                string label = rowView["label"].ToString();                

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = label;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // label
                Label labelLabelWrapper = new Label();
                labelLabelWrapper.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(labelLabelWrapper);

                HyperLink labelLink = new HyperLink();
                labelLink.NavigateUrl = "javascript: void(0);";
                labelLink.Attributes.Add("onclick", "OnRuleSetModifyClick(" + idRuleSet.ToString() + ");");
                labelLabelWrapper.Controls.Add(labelLink);

                Literal labelLabel = new Literal();
                labelLabel.Text = label;
                labelLink.Controls.Add(labelLabel);
            }
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmActionModal");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                            ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedRuleset_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);


            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseRuleset_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this._RuleSetGridActionsPanel.Controls.Add(this._GridConfirmAction);

            this._RuleSetsPanel.Controls.Add(this._RuleSetGridActionsPanel);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._RuleSetsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._RuleSetsGrid.Rows[i].FindControl(this._RuleSetsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.RuleSet.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, _GlobalResources.TheSelectedRuleset_sHaveBeenDeletedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, ex.Message, true);
            }
            finally
            {                
                // rebind the grid
                this._RuleSetsGrid.BindData();

                // clear the course object menu container
                this.CourseObjectMenuContainer.Controls.Clear();

                // rebuild the controls
                this._BuildControls();
            }
        }
        #endregion

        #region _BuildRuleSetEnrollmentPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for ruleset enrollment properties actions.
        /// </summary>
        private void _BuildRuleSetEnrollmentPropertiesActionsPanel()
        {
            this.RuleSetEnrollmentPropertiesActionsPanel = new Panel();
            this.RuleSetEnrollmentPropertiesActionsPanel.ID = "RuleSetEnrollmentPropertiesActionsPanel";

            // clear controls from container
            this.RuleSetEnrollmentPropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.RuleSetEnrollmentPropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._RuleSetEnrollmentPropertiesSaveButton = new Button();
            this._RuleSetEnrollmentPropertiesSaveButton.ID = "RuleSetEnrollmentSaveButton";
            this._RuleSetEnrollmentPropertiesSaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._RuleSetEnrollmentObject == null)
            { this._RuleSetEnrollmentPropertiesSaveButton.Text = _GlobalResources.CreateRulesetEnrollment; }
            else
            { this._RuleSetEnrollmentPropertiesSaveButton.Text = _GlobalResources.SaveChanges; }

            this._RuleSetEnrollmentPropertiesSaveButton.Command += new CommandEventHandler(this._RuleSetEnrollmentPropertiesSaveButton_Command);
            this.RuleSetEnrollmentPropertiesActionsPanel.Controls.Add(this._RuleSetEnrollmentPropertiesSaveButton);

            // cancel button
            this._RuleSetEnrollmentPropertiesCancelButton = new Button();
            this._RuleSetEnrollmentPropertiesCancelButton.ID = "CancelButton";
            this._RuleSetEnrollmentPropertiesCancelButton.CssClass = "Button NonActionButton";
            this._RuleSetEnrollmentPropertiesCancelButton.Text = _GlobalResources.Cancel;
            this._RuleSetEnrollmentPropertiesCancelButton.Command += new CommandEventHandler(this._RuleSetEnrollmentPropertiesCancelButton_Command);
            this.RuleSetEnrollmentPropertiesActionsPanel.Controls.Add(this._RuleSetEnrollmentPropertiesCancelButton);

            Panel propertiesFormPanel = (Panel)this.RuleSetEnrollmentPropertiesContainer.FindControl("RuleSetEnrollmentProperties_" + "Properties" + "_TabPanel");
            propertiesFormPanel.Controls.Add(this.RuleSetEnrollmentPropertiesActionsPanel);
        }
        #endregion

        #region _PopulateRuleSetEnrollmentPropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateRuleSetEnrollmentPropertiesInputElements()
        {
            if (this._RuleSetEnrollmentObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // label
                bool isDefaultPopulated = false;

                foreach (RuleSetEnrollment.LanguageSpecificProperty ruleSetEnrollmentLanguageSpecificProperty in this._RuleSetEnrollmentObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (ruleSetEnrollmentLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._Label.Text = ruleSetEnrollmentLanguageSpecificProperty.Label;

                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificRuleSetEnrollmentLabelTextBox = (TextBox)this.RuleSetEnrollmentPropertiesContainer.FindControl(this._Label.ID + "_" + ruleSetEnrollmentLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificRuleSetEnrollmentLabelTextBox != null)
                        { languageSpecificRuleSetEnrollmentLabelTextBox.Text = ruleSetEnrollmentLanguageSpecificProperty.Label; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._Label.Text = this._RuleSetEnrollmentObject.Label;
                }


                // NON-LANGUAGE SPECIFIC PROPERTIES

                // force reassignment
                if (!this._IsRecurring)
                {
                    if (this._RuleSetEnrollmentObject.ForceReassignment != null)
                    { this._ForceReassignment.Checked = (bool)this._RuleSetEnrollmentObject.ForceReassignment; }
                    else
                    { this._ForceReassignment.Checked = false; }
                }

                // is locked by prerequisites
                this._IsLockedByPrerequisites.Checked = this._RuleSetEnrollmentObject.IsLockedByPrerequisites;

                // timezone
                this._Timezone.SelectedValue = this._RuleSetEnrollmentObject.IdTimezone.ToString();

                // date start
                if (!this._IsStartDatePassed)
                { this._DtStart.Value = TimeZoneInfo.ConvertTimeFromUtc(this._RuleSetEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetEnrollmentObject.IdTimezone).dotNetName)); }

                // date end
                if (this._RuleSetEnrollmentObject.DtEnd != null)
                {
                    this._DtEnd.NoneCheckBoxChecked = false;
                    this._DtEnd.Value = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._RuleSetEnrollmentObject.DtEnd, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetEnrollmentObject.IdTimezone).dotNetName));
                }
                else
                {
                    this._DtEnd.NoneCheckBoxChecked = true;
                    this._DtEnd.Value = null;
                }

                // delay
                if (!this._IsFixedDate)
                {
                    if (this._RuleSetEnrollmentObject.DelayInterval != null)
                    {
                        this._Delay.NoneCheckBoxChecked = false;
                        this._Delay.IntervalValue = this._RuleSetEnrollmentObject.DelayInterval.ToString();
                        this._Delay.TimeframeValue = this._RuleSetEnrollmentObject.DelayTimeframe;
                    }
                    else
                    {
                        this._Delay.NoneCheckBoxChecked = true;
                        this._Delay.IntervalValue = null;
                        this._Delay.TimeframeValue = null;
                    }
                }

                // due
                if (this._RuleSetEnrollmentObject.DueInterval != null)
                {
                    this._Due.NoneCheckBoxChecked = false;
                    this._Due.IntervalValue = this._RuleSetEnrollmentObject.DueInterval.ToString();
                    this._Due.TimeframeValue = this._RuleSetEnrollmentObject.DueTimeframe;
                }
                else
                {
                    this._Due.NoneCheckBoxChecked = true;
                    this._Due.IntervalValue = null;
                    this._Due.TimeframeValue = null;
                }

                // recur
                if (this._IsRecurring)
                {
                    this._Recur.IntervalValue = this._RuleSetEnrollmentObject.RecurInterval.ToString();
                    this._Recur.TimeframeValue = this._RuleSetEnrollmentObject.RecurTimeframe;
                }

                // expires from start
                if (!this._IsFixedDate || (this._IsFixedDate && this._IsRecurring))
                {
                    if (this._RuleSetEnrollmentObject.ExpiresFromStartInterval != null)
                    {
                        this._ExpiresFromStart.NoneCheckBoxChecked = false;
                        this._ExpiresFromStart.IntervalValue = this._RuleSetEnrollmentObject.ExpiresFromStartInterval.ToString();
                        this._ExpiresFromStart.TimeframeValue = this._RuleSetEnrollmentObject.ExpiresFromStartTimeframe;
                    }
                    else
                    {
                        this._ExpiresFromStart.NoneCheckBoxChecked = true;
                        this._ExpiresFromStart.IntervalValue = null;
                        this._ExpiresFromStart.TimeframeValue = null;
                    }
                }

                // expires from first launch
                if (this._RuleSetEnrollmentObject.ExpiresFromFirstLaunchInterval != null)
                {
                    this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = false;
                    this._ExpiresFromFirstLaunch.IntervalValue = this._RuleSetEnrollmentObject.ExpiresFromFirstLaunchInterval.ToString();
                    this._ExpiresFromFirstLaunch.TimeframeValue = this._RuleSetEnrollmentObject.ExpiresFromFirstLaunchTimeframe;
                }
                else
                {
                    this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = true;
                    this._ExpiresFromFirstLaunch.IntervalValue = null;
                    this._ExpiresFromFirstLaunch.TimeframeValue = null;
                }
            }
        }
        #endregion

        #region _ValidateRuleSetEnrollmentPropertiesForm
        /// <summary>
        /// Method to validate the enrollment form
        /// </summary>
        /// <returns></returns>
        private bool _ValidateRuleSetEnrollmentPropertiesForm()
        {
            bool isValid = true;

            /* BASIC VALIDATIONS */

            // label field - default language required
            if (String.IsNullOrWhiteSpace(this._Label.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentLabel", _GlobalResources.Label + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // force reassignment - no validation needed

            // is locked by prerequisites - no validation needed

            // timezone - always required, must be a number, and if not valid, its a showstopper, which means return right away
            if (String.IsNullOrWhiteSpace(this._Timezone.SelectedValue))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentTimezone", _GlobalResources.LifespanTimezone + " " + _GlobalResources.IsRequired);
                return isValid;
            }

            int idTimezone;
            if (!int.TryParse(this._Timezone.SelectedValue, out idTimezone))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentTimezone", _GlobalResources.LifespanTimezone + " " + _GlobalResources.IsInvalid);
                return isValid;
            }

            // get the timezone's "dotnetname" from a timezone object.
            string tzDotNetName = new Timezone(Convert.ToInt32(idTimezone)).dotNetName;

            // date start - required, if its not specified its a showstopper, which means return right away
            // only validate if start has not passed
            if (!this._IsStartDatePassed)
            {
                if (this._DtStart.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDtStart", _GlobalResources.LifespanStart + " " + _GlobalResources.IsRequired);
                    return isValid;
                }
            }

            // date end - a value is required for one-time fixed
            if (this._DtEnd.NoneCheckBoxChecked || this._DtEnd.Value == null)
            {
                if (this._IsFixedDate && !this._IsRecurring)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDtEnd", _GlobalResources.LifespanEnd + " " + _GlobalResources.IsRequired);
                }
                else
                {
                    // if the "none" checkbox is not checked, ensure a valid date has been entered, showstopper, return immeadiately
                    if (!this._DtEnd.NoneCheckBoxChecked && this._DtEnd.Value == null)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDtEnd", _GlobalResources.LifespanEnd + " " + _GlobalResources.IsInvalid);
                        return isValid;
                    }
                }
            }

            // delay - only exists for relative dates, never required, interval must be integer
            if (!this._IsFixedDate)
            {
                if (!this._Delay.NoneCheckBoxChecked)
                {
                    int delayInterval;
                    if (String.IsNullOrWhiteSpace(this._Delay.IntervalValue) || !int.TryParse(this._Delay.IntervalValue, out delayInterval))
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDelay", _GlobalResources.Delay + " " + _GlobalResources.IsInvalid);
                    }
                }
            }

            // due - always exists, only required for recurring, interval must be integer
            if (this._Due.NoneCheckBoxChecked)
            {
                if (this._IsRecurring)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.IsRequired);
                }
            }
            else
            {
                int dueInterval;
                if (String.IsNullOrWhiteSpace(this._Due.IntervalValue) || !int.TryParse(this._Due.IntervalValue, out dueInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.IsInvalid);
                }
            }

            // recur - only exists and is required for recurring, interval must be integer
            if (this._IsRecurring)
            {
                int recurInterval;
                if (String.IsNullOrWhiteSpace(this._Recur.IntervalValue) || !int.TryParse(this._Recur.IntervalValue, out recurInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentRecur", _GlobalResources.Recurrence + " " + _GlobalResources.IsInvalid);
                }
            }

            // access from start - exists for relative and recurring, required for recurring, interval must be integer
            if (!this._IsFixedDate || (this._IsFixedDate && this._IsRecurring))
            {
                if (this._ExpiresFromStart.NoneCheckBoxChecked)
                {
                    if (this._IsRecurring)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentExpiresFromStart", _GlobalResources.AccessFromStart + " " + _GlobalResources.IsRequired);
                    }
                }
                else
                {
                    int expiresFromStartInterval;
                    if (String.IsNullOrWhiteSpace(this._ExpiresFromStart.IntervalValue) || !int.TryParse(this._ExpiresFromStart.IntervalValue, out expiresFromStartInterval))
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentExpiresFromStart", _GlobalResources.AccessFromStart + " " + _GlobalResources.IsInvalid);
                    }
                }
            }

            // access from first launch - always exists, never required, interval must be integer
            if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
            {
                int expiresFromFirstLaunchInterval;
                if (String.IsNullOrWhiteSpace(this._ExpiresFromFirstLaunch.IntervalValue) || !int.TryParse(this._ExpiresFromFirstLaunch.IntervalValue, out expiresFromFirstLaunchInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentExpiresFromFirstLaunch", _GlobalResources.AccessFromFirstLaunch + " " + _GlobalResources.IsInvalid);
                }
            }

            /* CONSTRAINT VALIDATIONS BASED ON TYPE */

            // only apply constraint validations if the other validations have passed
            if (isValid)
            {
                // date start - must be in future, only validate if start has not passed
                DateTime dtStart;

                if (!this._IsStartDatePassed)
                {
                    if (this._DtStart.NowCheckBoxChecked)
                    { dtStart = DateTime.UtcNow.AddMinutes(1); }
                    else
                    { dtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName)); }

                    if (dtStart < AsentiaSessionState.UtcNow)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDtStart", _GlobalResources.LifespanStart + " " + _GlobalResources.MustBeInFuture);
                    }
                }
                else // this assumes that the RuleSetEnrollment object exists and is populated, otherwise, this couldn't be fired due to logic in other places
                {
                    // if applying the the selected timezone to the unchanged original date start results in a
                    // new start date that has already passed, fail
                    // this only needs to be done when the timezone has been modified

                    // this is utc
                    dtStart = this._RuleSetEnrollmentObject.DtStart;

                    if (this._RuleSetEnrollmentObject.IdTimezone != Convert.ToInt32(this._Timezone.SelectedValue))
                    {
                        // convert start back to local (from original timezone)
                        DateTime originalLocalDtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetEnrollmentObject.IdTimezone).dotNetName));

                        // convert local start to new utc
                        dtStart = TimeZoneInfo.ConvertTimeToUtc(originalLocalDtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(Convert.ToInt32(this._Timezone.SelectedValue)).dotNetName));

                        if (dtStart <= AsentiaSessionState.UtcNow)
                        {
                            isValid = false;
                            Timezone selectedTimezone = new Timezone(Convert.ToInt32(this._Timezone.SelectedValue));
                            string currentAdjustedStartValue = TimeZoneInfo.ConvertTimeFromUtc(this._RuleSetEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(selectedTimezone.dotNetName)).ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern)
                                                               + " @ "
                                                               + TimeZoneInfo.ConvertTimeFromUtc(this._RuleSetEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(selectedTimezone.dotNetName)).ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern)
                                                               + " "
                                                               + selectedTimezone.displayName;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDtStart", _GlobalResources.LifespanStart + " " + String.Format(_GlobalResources.CannotBeMovedEarlierThanCurrentValue, currentAdjustedStartValue));
                        }
                    }
                }

                // date end - if set, cannot be less than date start
                DateTime? dtEnd = null;

                if (!this._DtEnd.NoneCheckBoxChecked && this._DtEnd.Value != null)
                {
                    dtEnd = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtEnd.Value, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));

                    if (dtEnd <= dtStart)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDtEnd", _GlobalResources.LifespanEnd + " " + _GlobalResources.CannotBeBeforeStartDate);
                    }
                }

                // type-specific validations
                if (!this._IsRecurring && this._IsFixedDate) // FixedDateOneTime
                {
                    // due - calculated value cannot exceed date end, interval cannot exceed expires from first launch interval
                    if (!this._Due.NoneCheckBoxChecked)
                    {
                        // compare calculated due to date end only if date end is not null
                        if (dtEnd != null)
                        {
                            // calculate the effective due date
                            DateTime calculatedDtDue;
                            int dueInterval = Convert.ToInt32(this._Due.IntervalValue);

                            switch (this._Due.TimeframeValue)
                            {
                                case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                    calculatedDtDue = dtStart.AddDays(dueInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                    calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                    calculatedDtDue = dtStart.AddMonths(dueInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                    calculatedDtDue = dtStart.AddYears(dueInterval);
                                    break;
                                default:
                                    // will never fire but needed to avoid "unassigned variable" compiler error
                                    calculatedDtDue = DateTime.MinValue;
                                    break;
                            }

                            // compare calculated date due to date end
                            if (calculatedDtDue > dtEnd)
                            {
                                isValid = false;
                                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CalculatedDateCannotExceedEndDate);
                            }
                        }

                        // compare to expires from first launch interval
                        if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                        {
                            // calculate dates from intervals for comparison by adding interval to utcnow
                            DateTime calculatedDtDue;
                            DateTime calculatedDtExpiresFromFirstLaunch;
                            int dueInterval = Convert.ToInt32(this._Due.IntervalValue);
                            int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                            switch (this._Due.TimeframeValue)
                            {
                                case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                    calculatedDtDue = dtStart.AddDays(dueInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                    calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                    calculatedDtDue = dtStart.AddMonths(dueInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                    calculatedDtDue = dtStart.AddYears(dueInterval);
                                    break;
                                default:
                                    // will never fire but needed to avoid "unassigned variable" compiler error
                                    calculatedDtDue = DateTime.MinValue;
                                    break;
                            }

                            switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                            {
                                case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                    break;
                                default:
                                    // will never fire but needed to avoid "unassigned variable" compiler error
                                    calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                    break;
                            }

                            // compare calculated date due to calculated expires from first launch
                            if (calculatedDtDue > calculatedDtExpiresFromFirstLaunch)
                            {
                                isValid = false;
                                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromFirstLaunch);
                            }
                        }
                    }

                }
                else if (!this._IsRecurring && !this._IsFixedDate) // RelativeDateOneTime
                {
                    // due - interval cannot exceed access from start and access from first launch intervals
                    if (!this._Due.NoneCheckBoxChecked)
                    {
                        // compare to expires from start interval
                        if (!this._ExpiresFromStart.NoneCheckBoxChecked)
                        {
                            // calculate dates from intervals for comparison by adding interval to utcnow
                            DateTime calculatedDtDue;
                            DateTime calculatedDtExpiresFromStart;
                            int dueInterval = Convert.ToInt32(this._Due.IntervalValue);
                            int expiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                            switch (this._Due.TimeframeValue)
                            {
                                case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                    calculatedDtDue = dtStart.AddDays(dueInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                    calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                    calculatedDtDue = dtStart.AddMonths(dueInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                    calculatedDtDue = dtStart.AddYears(dueInterval);
                                    break;
                                default:
                                    // will never fire but needed to avoid "unassigned variable" compiler error
                                    calculatedDtDue = DateTime.MinValue;
                                    break;
                            }

                            switch (this._ExpiresFromStart.TimeframeValue)
                            {
                                case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                    calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                    calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval * 7);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                    calculatedDtExpiresFromStart = dtStart.AddMonths(expiresFromStartInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                    calculatedDtExpiresFromStart = dtStart.AddYears(expiresFromStartInterval);
                                    break;
                                default:
                                    // will never fire but needed to avoid "unassigned variable" compiler error
                                    calculatedDtExpiresFromStart = DateTime.MinValue;
                                    break;
                            }

                            // compare calculated date due to calculated expires from start
                            if (calculatedDtDue > calculatedDtExpiresFromStart)
                            {
                                isValid = false;
                                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromStart);
                            }
                        }

                        // compare to expires from first launch interval
                        if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                        {
                            // calculate dates from intervals for comparison by adding interval to utcnow
                            DateTime calculatedDtDue;
                            DateTime calculatedDtExpiresFromFirstLaunch;
                            int dueInterval = Convert.ToInt32(this._Due.IntervalValue);
                            int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                            switch (this._Due.TimeframeValue)
                            {
                                case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                    calculatedDtDue = dtStart.AddDays(dueInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                    calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                    calculatedDtDue = dtStart.AddMonths(dueInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                    calculatedDtDue = dtStart.AddYears(dueInterval);
                                    break;
                                default:
                                    // will never fire but needed to avoid "unassigned variable" compiler error
                                    calculatedDtDue = DateTime.MinValue;
                                    break;
                            }

                            switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                            {
                                case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                    break;
                                default:
                                    // will never fire but needed to avoid "unassigned variable" compiler error
                                    calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                    break;
                            }

                            // compare calculated date due to calculated expires from first launch
                            if (calculatedDtDue > calculatedDtExpiresFromFirstLaunch)
                            {
                                isValid = false;
                                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromFirstLaunch);
                            }
                        }
                    }

                    // expires from first launch - interval cannot exceed expires from start interval
                    if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                    {
                        // compare to expires from start interval
                        if (!this._ExpiresFromStart.NoneCheckBoxChecked)
                        {
                            // calculate dates from intervals for comparison by adding interval to utcnow
                            DateTime calculatedDtExpiresFromFirstLaunch;
                            DateTime calculatedDtExpiresFromStart;
                            int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);
                            int expiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                            switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                            {
                                case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                    calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                    break;
                                default:
                                    // will never fire but needed to avoid "unassigned variable" compiler error
                                    calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                    break;
                            }

                            switch (this._ExpiresFromStart.TimeframeValue)
                            {
                                case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                    calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                    calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval * 7);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                    calculatedDtExpiresFromStart = dtStart.AddMonths(expiresFromStartInterval);
                                    break;
                                case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                    calculatedDtExpiresFromStart = dtStart.AddYears(expiresFromStartInterval);
                                    break;
                                default:
                                    // will never fire but needed to avoid "unassigned variable" compiler error
                                    calculatedDtExpiresFromStart = DateTime.MinValue;
                                    break;
                            }

                            // compare calculated expires from first launch to calculated expires from start
                            if (calculatedDtExpiresFromFirstLaunch > calculatedDtExpiresFromStart)
                            {
                                isValid = false;
                                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentExpiresFromFirstLaunch", _GlobalResources.AccessFromFirstLaunch + " " + _GlobalResources.CannotExceedAccessFromStart);
                            }
                        }
                    }
                }
                else if (this._IsRecurring && this._IsFixedDate) // FixedDateRecurring
                {
                    // due - interval cannot exceed recur, expires from start, and expires from first launch intervals
                    DateTime calculatedDtDue;
                    int dueInterval = Convert.ToInt32(this._Due.IntervalValue);

                    switch (this._Due.TimeframeValue)
                    {
                        case DateIntervalSelector.INTERVAL_VALUE_DAY:
                            calculatedDtDue = dtStart.AddDays(dueInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                            calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                            calculatedDtDue = dtStart.AddMonths(dueInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                            calculatedDtDue = dtStart.AddYears(dueInterval);
                            break;
                        default:
                            // will never fire but needed to avoid "unassigned variable" compiler error
                            calculatedDtDue = DateTime.MinValue;
                            break;
                    }

                    // compare to recur interval

                    // calculate dates from intervals for comparison by adding interval to utcnow
                    DateTime calculatedDtRecur;
                    int recurInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                    switch (this._ExpiresFromStart.TimeframeValue)
                    {
                        case DateIntervalSelector.INTERVAL_VALUE_DAY:
                            calculatedDtRecur = dtStart.AddDays(recurInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                            calculatedDtRecur = dtStart.AddDays(recurInterval * 7);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                            calculatedDtRecur = dtStart.AddMonths(recurInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                            calculatedDtRecur = dtStart.AddYears(recurInterval);
                            break;
                        default:
                            // will never fire but needed to avoid "unassigned variable" compiler error
                            calculatedDtRecur = DateTime.MinValue;
                            break;
                    }

                    // compare calculated date due to calculated recur
                    if (calculatedDtDue > calculatedDtRecur)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedRecurrence);
                    }

                    // compare to expires from start interval

                    // calculate dates from intervals for comparison by adding interval to utcnow
                    DateTime calculatedDtExpiresFromStart;
                    int expiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                    switch (this._ExpiresFromStart.TimeframeValue)
                    {
                        case DateIntervalSelector.INTERVAL_VALUE_DAY:
                            calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                            calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval * 7);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                            calculatedDtExpiresFromStart = dtStart.AddMonths(expiresFromStartInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                            calculatedDtExpiresFromStart = dtStart.AddYears(expiresFromStartInterval);
                            break;
                        default:
                            // will never fire but needed to avoid "unassigned variable" compiler error
                            calculatedDtExpiresFromStart = DateTime.MinValue;
                            break;
                    }

                    // compare calculated date due to calculated expires from start
                    if (calculatedDtDue > calculatedDtExpiresFromStart)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromStart);
                    }

                    // compare to expires from first launch interval
                    if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        int dtExpiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(dtExpiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(dtExpiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(dtExpiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(dtExpiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        // compare calculated date due to calculated expires from first launch
                        if (calculatedDtDue > calculatedDtExpiresFromFirstLaunch)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromFirstLaunch);
                        }
                    }

                    // recur - interval cannot exceed expires from start and expires from first launch intervals

                    // compare calculated recur to calculated expires from start
                    if (calculatedDtRecur > calculatedDtExpiresFromStart)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentRecur", _GlobalResources.Recurrence + " " + _GlobalResources.CannotExceedAccessFromStart);
                    }

                    // compare to expires from first launch interval
                    if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        int dtExpiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(dtExpiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(dtExpiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(dtExpiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(dtExpiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        // compare calculated date recur to calculated expires from first launch
                        if (calculatedDtRecur > calculatedDtExpiresFromFirstLaunch)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentRecur", _GlobalResources.Recurrence + " " + _GlobalResources.CannotExceedAccessFromFirstLaunch);
                        }
                    }

                    // expires from first launch - interval cannot exceed expires from start interval
                    if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                    {
                        // compare to expires from start interval

                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        // compare calculated expires from first launch to calculated expires from start
                        if (calculatedDtExpiresFromFirstLaunch > calculatedDtExpiresFromStart)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentExpiresFromFirstLaunch", _GlobalResources.AccessFromFirstLaunch + " " + _GlobalResources.CannotExceedAccessFromStart);
                        }
                    }
                }
                else if (this._IsRecurring && !this._IsFixedDate) // RelativeDateRecurring
                {
                    // due - interval cannot exceed recur, expires from start, and expires from first launch intervals
                    DateTime calculatedDtDue;
                    int dueInterval = Convert.ToInt32(this._Due.IntervalValue);

                    switch (this._Due.TimeframeValue)
                    {
                        case DateIntervalSelector.INTERVAL_VALUE_DAY:
                            calculatedDtDue = dtStart.AddDays(dueInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                            calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                            calculatedDtDue = dtStart.AddMonths(dueInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                            calculatedDtDue = dtStart.AddYears(dueInterval);
                            break;
                        default:
                            // will never fire but needed to avoid "unassigned variable" compiler error
                            calculatedDtDue = DateTime.MinValue;
                            break;
                    }

                    // compare to recur interval

                    // calculate dates from intervals for comparison by adding interval to utcnow
                    DateTime calculatedDtRecur;
                    int recurInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                    switch (this._ExpiresFromStart.TimeframeValue)
                    {
                        case DateIntervalSelector.INTERVAL_VALUE_DAY:
                            calculatedDtRecur = dtStart.AddDays(recurInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                            calculatedDtRecur = dtStart.AddDays(recurInterval * 7);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                            calculatedDtRecur = dtStart.AddMonths(recurInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                            calculatedDtRecur = dtStart.AddYears(recurInterval);
                            break;
                        default:
                            // will never fire but needed to avoid "unassigned variable" compiler error
                            calculatedDtRecur = DateTime.MinValue;
                            break;
                    }

                    // compare calculated date due to calculated recur
                    if (calculatedDtDue > calculatedDtRecur)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedRecurrence);
                    }

                    // compare to expires from start interval

                    // calculate dates from intervals for comparison by adding interval to utcnow
                    DateTime calculatedDtExpiresFromStart;
                    int expiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                    switch (this._ExpiresFromStart.TimeframeValue)
                    {
                        case DateIntervalSelector.INTERVAL_VALUE_DAY:
                            calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                            calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval * 7);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                            calculatedDtExpiresFromStart = dtStart.AddMonths(expiresFromStartInterval);
                            break;
                        case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                            calculatedDtExpiresFromStart = dtStart.AddYears(expiresFromStartInterval);
                            break;
                        default:
                            // will never fire but needed to avoid "unassigned variable" compiler error
                            calculatedDtExpiresFromStart = DateTime.MinValue;
                            break;
                    }

                    // compare calculated date due to calculated expires from start
                    if (calculatedDtDue > calculatedDtExpiresFromStart)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromStart);
                    }

                    // compare to expires from first launch interval
                    if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        int dtExpiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(dtExpiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(dtExpiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(dtExpiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(dtExpiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        // compare calculated date due to calculated expires from first launch
                        if (calculatedDtDue > calculatedDtExpiresFromFirstLaunch)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromFirstLaunch);
                        }
                    }

                    // recur - interval cannot exceed expires from start and expires from first launch intervals

                    // compare calculated recur to calculated expires from start
                    if (calculatedDtRecur > calculatedDtExpiresFromStart)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentRecur", _GlobalResources.Recurrence + " " + _GlobalResources.CannotExceedAccessFromStart);
                    }

                    // compare to expires from first launch interval
                    if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        int dtExpiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(dtExpiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(dtExpiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(dtExpiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(dtExpiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        // compare calculated date recur to calculated expires from first launch
                        if (calculatedDtRecur > calculatedDtExpiresFromFirstLaunch)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentRecur", _GlobalResources.Recurrence + " " + _GlobalResources.CannotExceedAccessFromFirstLaunch);
                        }
                    }

                    // expires from first launch - interval cannot exceed expires from start interval
                    if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                    {
                        // compare to expires from start interval

                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                        switch (this._ExpiresFromStart.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        // compare calculated expires from first launch to calculated expires from start
                        if (calculatedDtExpiresFromFirstLaunch > calculatedDtExpiresFromStart)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetEnrollmentPropertiesContainer, "RuleSetEnrollmentExpiresFromFirstLaunch", _GlobalResources.AccessFromFirstLaunch + " " + _GlobalResources.CannotExceedAccessFromStart);
                        }
                    }
                }
                else
                { }
            }

            return isValid;
        }
        #endregion

        #region _RuleSetEnrollmentPropertiesSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for ruleset enrollment properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RuleSetEnrollmentPropertiesSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateRuleSetEnrollmentPropertiesForm())
                { throw new AsentiaException(); }

                // if there is no ruleset enrollment object, create one
                if (this._RuleSetEnrollmentObject == null)
                { this._RuleSetEnrollmentObject = new RuleSetEnrollment(); }

                int id;
                int cid = this._CourseObject.Id;

                // populate the object

                // label
                this._RuleSetEnrollmentObject.Label = this._Label.Text;

                // course id
                this._RuleSetEnrollmentObject.IdCourse = this._CourseObject.Id;

                // force reassignment
                if (!this._IsRecurring)
                { this._RuleSetEnrollmentObject.ForceReassignment = this._ForceReassignment.Checked; }
                else
                { this._RuleSetEnrollmentObject.ForceReassignment = null; }

                // is locked by prerequisites
                this._RuleSetEnrollmentObject.IsLockedByPrerequisites = this._IsLockedByPrerequisites.Checked;

                // timezone
                int oldTimezoneId = 0;

                if (this._RuleSetEnrollmentObject.IdTimezone > 0)
                { oldTimezoneId = this._RuleSetEnrollmentObject.IdTimezone; }

                this._RuleSetEnrollmentObject.IdTimezone = Convert.ToInt32(this._Timezone.SelectedValue);

                // is fixed date
                this._RuleSetEnrollmentObject.IsFixedDate = this._IsFixedDate;

                // date start
                if (!this._IsStartDatePassed)
                {
                    if (this._DtStart.NowCheckBoxChecked)
                    { this._RuleSetEnrollmentObject.DtStart = DateTime.UtcNow.AddMinutes(1); }
                    else
                    { this._RuleSetEnrollmentObject.DtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetEnrollmentObject.IdTimezone).dotNetName)); }
                }
                else
                {
                    // if the timezones have changed, convert start to utc from the original date start
                    if (oldTimezoneId > 0 && oldTimezoneId != this._RuleSetEnrollmentObject.IdTimezone)
                    {
                        // convert start back to local
                        DateTime originalLocalDtStart = TimeZoneInfo.ConvertTimeFromUtc(this._RuleSetEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(oldTimezoneId).dotNetName));

                        // convert local start to new utc
                        this._RuleSetEnrollmentObject.DtStart = TimeZoneInfo.ConvertTimeToUtc(originalLocalDtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetEnrollmentObject.IdTimezone).dotNetName));
                    }
                }

                // date end
                if (this._DtEnd.NoneCheckBoxChecked)
                { this._RuleSetEnrollmentObject.DtEnd = null; }
                else
                { this._RuleSetEnrollmentObject.DtEnd = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtEnd.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetEnrollmentObject.IdTimezone).dotNetName)); }

                // delay
                if (!this._IsFixedDate && !this._Delay.NoneCheckBoxChecked)
                {
                    this._RuleSetEnrollmentObject.DelayInterval = Convert.ToInt32(this._Delay.IntervalValue);
                    this._RuleSetEnrollmentObject.DelayTimeframe = this._Delay.TimeframeValue;
                }
                else
                {
                    this._RuleSetEnrollmentObject.DelayInterval = null;
                    this._RuleSetEnrollmentObject.DelayTimeframe = null;
                }

                // due
                if (this._Due.NoneCheckBoxChecked)
                {
                    this._RuleSetEnrollmentObject.DueInterval = null;
                    this._RuleSetEnrollmentObject.DueTimeframe = null;
                }
                else
                {
                    this._RuleSetEnrollmentObject.DueInterval = Convert.ToInt32(this._Due.IntervalValue);
                    this._RuleSetEnrollmentObject.DueTimeframe = this._Due.TimeframeValue;
                }

                // recur
                if (this._IsRecurring)
                {
                    this._RuleSetEnrollmentObject.RecurInterval = Convert.ToInt32(this._Recur.IntervalValue);
                    this._RuleSetEnrollmentObject.RecurTimeframe = this._Recur.TimeframeValue;
                }
                else
                {
                    this._RuleSetEnrollmentObject.RecurInterval = null;
                    this._RuleSetEnrollmentObject.RecurTimeframe = null;
                }

                // expires from start
                if (!this._IsFixedDate || (this._IsFixedDate && this._IsRecurring))
                {
                    if (this._ExpiresFromStart.NoneCheckBoxChecked)
                    {
                        this._RuleSetEnrollmentObject.ExpiresFromStartInterval = null;
                        this._RuleSetEnrollmentObject.ExpiresFromStartTimeframe = null;
                    }
                    else
                    {
                        this._RuleSetEnrollmentObject.ExpiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);
                        this._RuleSetEnrollmentObject.ExpiresFromStartTimeframe = this._ExpiresFromStart.TimeframeValue;
                    }
                }

                // expires from first launch
                if (this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                {
                    this._RuleSetEnrollmentObject.ExpiresFromFirstLaunchInterval = null;
                    this._RuleSetEnrollmentObject.ExpiresFromFirstLaunchTimeframe = null;
                }
                else
                {
                    this._RuleSetEnrollmentObject.ExpiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);
                    this._RuleSetEnrollmentObject.ExpiresFromFirstLaunchTimeframe = this._ExpiresFromFirstLaunch.TimeframeValue;
                }

                // save the ruleset enrollment, save its returned id to viewstate, and set the current ruleset enrollment object's id
                id = this._RuleSetEnrollmentObject.Save();
                this.ViewState["id"] = id;
                this._RuleSetEnrollmentObject.Id = id;

                // do ruleset enrollment language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string ruleSetEnrollmentLabel = null;

                        // get text boxes
                        TextBox languageSpecificRuleSetEnrollmentLabelTextBox = (TextBox)this.RuleSetEnrollmentPropertiesContainer.FindControl(this._Label.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificRuleSetEnrollmentLabelTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificRuleSetEnrollmentLabelTextBox.Text))
                            { ruleSetEnrollmentLabel = languageSpecificRuleSetEnrollmentLabelTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(ruleSetEnrollmentLabel))
                        {
                            this._RuleSetEnrollmentObject.SaveLang(cultureInfo.Name,
                                                                   ruleSetEnrollmentLabel);
                        }
                    }
                }               

                // load the saved ruleset enrollment object
                this._RuleSetEnrollmentObject = new RuleSetEnrollment(id);

                // determine if the start date has passed based on the saved enrollment information
                if (this._RuleSetEnrollmentObject.DtStart <= AsentiaSessionState.UtcNow)
                { this._IsStartDatePassed = true; }
                else
                { this._IsStartDatePassed = false; }

                // clear controls for containers that have dynamically added elements
                this.CourseObjectMenuContainer.Controls.Clear();

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, _GlobalResources.RulesetEnrollmentHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetEnrollmentPropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _RuleSetEnrollmentPropertiesCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for RuleSetEnrollment Properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RuleSetEnrollmentPropertiesCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("Default.aspx?cid=" + this._CourseObject.Id.ToString());
        }
        #endregion
    }
}
