﻿
$(document).ready(function () {
    // PRINT OPTIONS MENU
    $("#TranscriptPrintOptionsContainer").each(function () {
        $("#" + this.id).click(function (event) {
            event.stopPropagation();
        });
    });

    $("#TranscriptPrintImage").click(function () {
        $("#TranscriptPrintOptionsMenuItemContainer").toggle();
    });

    $(document).click(function () {
        if ($("#TranscriptPrintOptionsMenuItemContainer").is(":visible")) {
            $("#TranscriptPrintOptionsMenuItemContainer").hide();
        }
    });
    // END PRINT OPTIONS MENU
});

function ToggleTranscriptFullViewTab(elementId, isPanelInModal) {

    // create an array of the tab names
    var transcriptTabTypes = new Array();
    transcriptTabTypes[0] = "CourseTranscriptFullView";
    transcriptTabTypes[1] = "LearningPathTranscriptFullView";
    transcriptTabTypes[2] = "InstructorLedTranscriptFullView";

    // extract the transcript tab name from the id of the tab link
    var transcriptTabToShow = elementId.toString();
    transcriptTabToShow = transcriptTabToShow.toString().replace("TabLink", "");

    // loop through the tab name array, show the widget to be shown,
    // and hide the rest.
    for (var i = 0; i < transcriptTabTypes.length; i++) {
        if (transcriptTabToShow == transcriptTabTypes[i]) {
            $("#" + transcriptTabTypes[i] + "TabPanel").show();
            $("#" + transcriptTabTypes[i] + "TabLI").addClass("TabbedListLIOn");
        }
        else {
            $("#" + transcriptTabTypes[i] + "TabPanel").hide();
            $("#" + transcriptTabTypes[i] + "TabLI").removeClass("TabbedListLIOn");
        }
    }

    // re-center the modal
    if (isPanelInModal) {
        SetReCenterOnShownForModal_ReportModalPopupModalPopupExtender();
    }
}

function PrintTranscript(isPrintAll) {

    // create a container to hold the transcript data for printing, and append it to body
    var printableTranscriptContainer = $("<div id=\"PrintableTranscriptContainer\" style=\"position: relative; visibility: hidden;\"></div>");
    $("body").append(printableTranscriptContainer);

    // create a style tag for print use and attach it to the printable div, it will be removed with the div after printing
    // note that we do this dynamically instead of inside of a stylesheet so that we can focus @media print queries to the 
    // specific things we need them for, then have them removed - this is better than having @media print queries in several 
    // different stylesheets and possibly colliding
    var printableStylesheet = "<style>\
    @media print\
    {\
        @page\
        {\
            margin: auto;\
            size: landscape;\
        }\
     \
        #MasterForm\
        {\
            display: none;\
        }\
     \
        #PrintableTranscriptContainer\
        {\
            visibility: visible !important;\
            position: absolute;\
            left: 0;\
            top: 0;\
            background-color: #FFFFFF;\
            color: #000000;\
            font-size: 0.85em;\
            font-family: Helvetica;\
        }\
     \
        #PrintableTranscriptContainerTitle\
        {\
            border-top: 10px solid #FFFFFF;\
            border-bottom: 15px solid #FFFFFF;\
            font-weight: 700;\
            font-size: 2em;\
            font-family: Helvetica;\
        }\
     \
        #PrintableTranscriptContainer .GridTable th:first-child\
        {\
            border-left: 1px solid #D1D1D1;\
        }\
     \
        #PrintableTranscriptContainer .GridTable th:last-child\
        {\
            border-right: 1px solid #D1D1D1;\
        }\
     \
        #PrintableTranscriptContainer .GridTable th\
        {\
            border-top: 1px solid #D1D1D1;\
            border-bottom: 1px solid #D1D1D1;\
            background-color: #666666;\
            color: #000000;\
            font-weight: 700;\
        }\
     \
        #PrintableTranscriptContainer .GridDataRow\
        {\
            border: 1px solid #D1D1D1;\
            background-color: #FFFFFF;\
            color: #000000;\
        }\
     \
        #PrintableTranscriptContainer .GridDataRowAlternate\
        {\
            border: 1px solid #D1D1D1;\
            background-color: #FFFFFF;\
            color: #000000;\
        }\
     \
        #PrintableTranscriptContainer .GridDataRowEmpty\
        {\
            border: 1px solid #D1D1D1;\
            background-color: #FFFFFF;\
            color: #000000;\
        }\
     \
        #PrintableTranscriptContainer .GridDataRowEmpty td\
        {\
            text-align: left;\
        }\
    }\
    </style>";
    $("#PrintableTranscriptContainer").append(printableStylesheet);
    // clone the displayName panels and append them to the printable div
    $("#displayNamePanel").clone().prop("id", "PrintableTranscriptContainerTitle").appendTo("#PrintableTranscriptContainer");

    // clone the transcript tab panels and append them to the printable div
    $("#CourseTranscriptFullViewTabPanel").clone().prop("id", "CourseTranscriptFullViewTabPanelPrintable").appendTo("#PrintableTranscriptContainer");
    $("#LearningPathTranscriptFullViewTabPanel").clone().prop("id", "LearningPathTranscriptFullViewTabPanelPrintable").appendTo("#PrintableTranscriptContainer");
    $("#InstructorLedTranscriptFullViewTabPanel").clone().prop("id", "InstructorLedTranscriptFullViewTabPanelPrintable").appendTo("#PrintableTranscriptContainer");

    $("#PrintableTranscriptContainerTitle").show();

    // if "print all" make sure all divs are visible
    if (isPrintAll) {
        $("#CourseTranscriptFullViewTabPanelPrintable").show();
        $("#LearningPathTranscriptFullViewTabPanelPrintable").show();
        $("#InstructorLedTranscriptFullViewTabPanelPrintable").show();
    }

    // print
    window.print();

    // remove the printable div
    setTimeout(function () { $("#PrintableTranscriptContainer").remove(); }, 400);
}