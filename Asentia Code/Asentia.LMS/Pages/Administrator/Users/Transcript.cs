﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LRS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;

namespace Asentia.LMS.Pages.Administrator.Users
{
    public class Transcript : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel TranscriptFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel TranscriptPageWrapperContainer;
        public Panel TranscriptPanel;
        #endregion

        #region Private Properties
        private User _UserObject;
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Common.ClientScript), "Asentia.Common.TableSorting.js");
            csm.RegisterClientScriptResource(typeof(Transcript), "Asentia.LMS.Pages.Administrator.Users.Transcript.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, EventArgs e)
        {
            // get the user object
            this._GetUserObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/users/Transcript.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle(); 

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.TranscriptFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.TranscriptFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.TranscriptPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu
            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Transcript;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // build transcript panel
            BuildTranscriptFullView(this._UserObject.Id.ToString(), this._UserObject.DisplayName.ToString(), this.TranscriptPanel, false);
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetUserObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("uid", 0);
            int vsId = this.ViewStateInt(this.ViewState, "uid", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._UserObject = new User(id); }
                }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            {
                Response.Redirect("~/administrator/users");
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(userDisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Transcript));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, _GlobalResources.Transcript, ImageFiles.GetIconPath(ImageFiles.ICON_TRANSCRIPT, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion

        #region BuildTranscriptFullView
        /// <summary>
        /// BuildTranscriptFullView
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        public static void BuildTranscriptFullView(string userID, string displayName, Panel transcriptPanel, bool isPanelInModal)
        {
            Panel panelDisplayNameforPrint = new Panel();
            panelDisplayNameforPrint.ID = "displayNamePanel";
            panelDisplayNameforPrint.Attributes.Add("style", "display:none");

            Label labelTitleforPrint = new Label();
            labelTitleforPrint.Text = displayName + ": " + _GlobalResources.Transcript;
            panelDisplayNameforPrint.Controls.Add(labelTitleforPrint);
            transcriptPanel.Controls.Add(panelDisplayNameforPrint);

            // THIS HAS SPECIAL FUNCTIONALITY, SO DO NOT USE THE GENERIC TAB BUILDER METHOD

            // TABS
            Panel transcriptFullViewTabsPanel = new Panel();
            transcriptFullViewTabsPanel.ID = "TranscriptFullViewTabsPanel";
            transcriptFullViewTabsPanel.CssClass = "TabsContainer";

            // UL for tabs
            HtmlGenericControl transcriptTabsUL = new HtmlGenericControl("ul");
            transcriptTabsUL.ID = "TranscriptFullViewTabsUL";
            transcriptTabsUL.Attributes.Add("class", "TabbedList LearningAssets");

            // USER COURSE TRANSCRIPTS TAB
            // "User Course Transcripts" is the default tab, so this is "on" on page load.
            HtmlGenericControl courseTranscriptTabLI = new HtmlGenericControl("li");
            courseTranscriptTabLI.ID = "CourseTranscriptFullViewTabLI";
            courseTranscriptTabLI.Attributes.Add("class", "TabbedListLI TabbedListLIOn");

            HyperLink courseTranscriptTabLink = new HyperLink();
            courseTranscriptTabLink.ID = "CourseTranscriptFullViewTabLink";
            courseTranscriptTabLink.CssClass = "TabLink";
            courseTranscriptTabLink.NavigateUrl = "javascript:void(0);";
            courseTranscriptTabLink.Attributes.Add("onclick", "ToggleTranscriptFullViewTab(this.id, " + isPanelInModal.ToString().ToLower() + ");");
            courseTranscriptTabLink.Text = _GlobalResources.Course;

            // attach controls
            courseTranscriptTabLI.Controls.Add(courseTranscriptTabLink);
            transcriptTabsUL.Controls.Add(courseTranscriptTabLI);
            // END USER COURSE TRANSCRIPTS TAB

            // USER LEARNINING PATH TRANSCRIPTS TAB
            HtmlGenericControl learningPathTabLI = new HtmlGenericControl("li");
            learningPathTabLI.ID = "LearningPathTranscriptFullViewTabLI";
            learningPathTabLI.Attributes.Add("class", "TabbedListLI");

            HyperLink learningPathTabLink = new HyperLink();
            learningPathTabLink.ID = "LearningPathTranscriptFullViewTabLink";
            learningPathTabLink.CssClass = "TabLink";
            learningPathTabLink.NavigateUrl = "javascript:void(0);";
            learningPathTabLink.Attributes.Add("onclick", "ToggleTranscriptFullViewTab(this.id, " + isPanelInModal.ToString().ToLower() + ");");
            learningPathTabLink.Text = _GlobalResources.LearningPath;

            // attach controls
            learningPathTabLI.Controls.Add(learningPathTabLink);
            transcriptTabsUL.Controls.Add(learningPathTabLI);
            // END USER LEARNINING PATH TRANSCRIPTS TAB

            // USER INSTRUCTOR LED TRANSCRIPTS TAB
            HtmlGenericControl instructorLedTabLI = new HtmlGenericControl("li");
            instructorLedTabLI.ID = "InstructorLedTranscriptFullViewTabLI";
            instructorLedTabLI.Attributes.Add("class", "TabbedListLI");

            HyperLink instructorLedTabLink = new HyperLink();
            instructorLedTabLink.ID = "InstructorLedTranscriptFullViewTabLink";
            instructorLedTabLink.CssClass = "TabLink";
            instructorLedTabLink.NavigateUrl = "javascript:void(0);";
            instructorLedTabLink.Attributes.Add("onclick", "ToggleTranscriptFullViewTab(this.id, " + isPanelInModal.ToString().ToLower() + ");");
            instructorLedTabLink.Text = _GlobalResources.ILT;

            // attach controls
            instructorLedTabLI.Controls.Add(instructorLedTabLink);
            transcriptTabsUL.Controls.Add(instructorLedTabLI);
            // END USER INSTRUCTOR LED TRANSCRIPTS TAB

            // attach tab ul control
            transcriptFullViewTabsPanel.Controls.Add(transcriptTabsUL);

            // PRINT OPTIONS

            Panel transcriptPrintOptionsContainer = new Panel();
            transcriptPrintOptionsContainer.ID = "TranscriptPrintOptionsContainer";
            transcriptFullViewTabsPanel.Controls.AddAt(0, transcriptPrintOptionsContainer);
            
            Image transcriptPrintImage = new Image();
            transcriptPrintImage.ID = "TranscriptPrintImage";
            transcriptPrintImage.CssClass = "MediumIcon";
            transcriptPrintImage.Style.Add("cursor", "pointer");
            transcriptPrintImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PRINT, ImageFiles.EXT_PNG);
            transcriptPrintImage.AlternateText = _GlobalResources.Print;
            transcriptPrintOptionsContainer.Controls.Add(transcriptPrintImage);

            // print options menu item container
            Panel transcriptPrintOptionsMenuItemContainer = new Panel();
            transcriptPrintOptionsMenuItemContainer.ID = "TranscriptPrintOptionsMenuItemContainer";
            transcriptPrintOptionsContainer.Controls.Add(transcriptPrintOptionsMenuItemContainer);

            // print tab link
            Panel transcriptPrintTabLinkContainer = new Panel();
            transcriptPrintTabLinkContainer.ID = "TranscriptPrintTabLinkContainer";
            transcriptPrintOptionsMenuItemContainer.Controls.Add(transcriptPrintTabLinkContainer);

            LinkButton transcriptPrintTabLink = new LinkButton();
            transcriptPrintTabLink.ID = "TranscriptPrintTabLink";
            transcriptPrintTabLink.Text = _GlobalResources.PrintTab;
            transcriptPrintTabLink.OnClientClick = "javascript:PrintTranscript(false);return false;";
            transcriptPrintTabLinkContainer.Controls.Add(transcriptPrintTabLink);

            // print tab link
            Panel transcriptPrintAllLinkContainer = new Panel();
            transcriptPrintAllLinkContainer.ID = "TranscriptPrintAllLinkContainer";
            transcriptPrintOptionsMenuItemContainer.Controls.Add(transcriptPrintAllLinkContainer);

            LinkButton transcriptPrintAllLink = new LinkButton();
            transcriptPrintAllLink.ID = "TranscriptPrintAllLink";
            transcriptPrintAllLink.Text = _GlobalResources.PrintAll;
            transcriptPrintAllLink.OnClientClick = "javascript:PrintTranscript(true);return false;";
            transcriptPrintAllLinkContainer.Controls.Add(transcriptPrintAllLink);

            // attach tabs panel
            transcriptPanel.Controls.Add(transcriptFullViewTabsPanel);
            // END TABS

            // BEGIN COURSE TRANSCRIPT TAB CONTENT
            // "Course Transcript" is the default tab, so this is visible on page load.
            Panel courseTranscriptPanel = new Panel();
            courseTranscriptPanel.ID = "CourseTranscriptFullViewTabPanel";
            courseTranscriptPanel.Attributes.Add("style", "display: block;");
            _CourseTranscriptsFullView(userID, courseTranscriptPanel);
            transcriptPanel.Controls.Add(courseTranscriptPanel);
            // END COURSE TRANSCRIPT TAB CONTENT

            // BEGIN LEARNING PATH TRANSCRIPT TAB CONTENT
            Panel learningPathTranscriptPanel = new Panel();
            learningPathTranscriptPanel.ID = "LearningPathTranscriptFullViewTabPanel";
            learningPathTranscriptPanel.Attributes.Add("style", "display: none;");
            _LearningPathTranscriptsFullView(userID, learningPathTranscriptPanel);
            transcriptPanel.Controls.Add(learningPathTranscriptPanel);
            // END COURSE TRANSCRIPT TAB CONTENT

            // BEGIN INSTRUCTOR LED TRANSCRIPT TAB CONTENT
            Panel instructorLedTranscriptPanel = new Panel();
            instructorLedTranscriptPanel.ID = "InstructorLedTranscriptFullViewTabPanel";
            instructorLedTranscriptPanel.Attributes.Add("style", "display: none;");
            _InstructorLedTranscriptsFullView(userID, instructorLedTranscriptPanel);
            transcriptPanel.Controls.Add(instructorLedTranscriptPanel);
            // END COURSE TRANSCRIPT TAB CONTENT       
        }
        #endregion

        #region _CourseTranscriptsFullView
        /// <summary>
        /// Course Transcripts FullView
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private static void _CourseTranscriptsFullView(string userID, Panel tabPanel)
        {
            DataTable dtTranscript = new DataTable();
            Report transcript = new Report();

            // create full view table for transcript modal
            Table tblTranscriptFullView = new Table();
            tblTranscriptFullView.ID = "TableTranscriptFullView";
            tblTranscriptFullView.CssClass = "GridTable";

            // HEADER ROW AND COLUMNS

            // create the header row
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.TableSection = TableRowSection.TableHeader;
            headerRow.CssClass = "GridHeaderRow";

            // course header cell
            TableHeaderCell courseHeader = new TableHeaderCell();
            courseHeader.ID = "CourseTranscriptFullViewCourseHeader";
            HyperLink courseColumnName = new HyperLink();
            courseColumnName.Text = _GlobalResources.Course;
            courseColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableTranscriptFullView'; SortTable(0,'T');";
            courseHeader.Controls.Add(courseColumnName);
            headerRow.Cells.Add(courseHeader);

            // credits header cell
            TableHeaderCell creditsHeader = new TableHeaderCell();
            creditsHeader.ID = "CourseTranscriptFullViewCreditsHeader";
            HyperLink creditsColumnName = new HyperLink();
            creditsColumnName.Text = _GlobalResources.Credits;
            creditsColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableTranscriptFullView'; SortTable(1,'N');";
            creditsHeader.Controls.Add(creditsColumnName);
            headerRow.Cells.Add(creditsHeader);

            // course completed header cell
            TableHeaderCell courseCompletedHeader = new TableHeaderCell();
            courseCompletedHeader.ID = "CourseTranscriptFullViewCourseCompletedHeader";
            HyperLink courseCompletedColumnName = new HyperLink();
            courseCompletedColumnName.Text = _GlobalResources.CourseStatus;
            courseCompletedColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableTranscriptFullView'; SortTable(2,'T');";
            courseCompletedHeader.Controls.Add(courseCompletedColumnName);
            headerRow.Cells.Add(courseCompletedHeader);

            // date completed header cell
            TableHeaderCell dateCompletedHeader = new TableHeaderCell();
            dateCompletedHeader.ID = "CourseTranscriptFullViewDateCompletedHeader";
            HyperLink dateCompletedColumnName = new HyperLink();
            dateCompletedColumnName.Text = _GlobalResources.DateCompleted;
            dateCompletedColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableTranscriptFullView'; SortTable(3,'D');";
            dateCompletedHeader.Controls.Add(dateCompletedColumnName);
            headerRow.Cells.Add(dateCompletedHeader);

            // lesson header cell
            TableHeaderCell lessonHeader = new TableHeaderCell();
            lessonHeader.ID = "CourseTranscriptFullViewLessonHeader";
            HyperLink lessonColumnName = new HyperLink();
            lessonColumnName.Text = _GlobalResources.Lesson;
            lessonColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableTranscriptFullView'; SortTable(4,'T');";
            lessonHeader.Controls.Add(lessonColumnName);
            headerRow.Cells.Add(lessonHeader);

            // lesson status header cell
            TableHeaderCell lessonCompletionHeader = new TableHeaderCell();
            lessonCompletionHeader.ID = "CourseTranscriptFullViewLessonStatusHeader";
            HyperLink lessonCompletionColumnName = new HyperLink();
            lessonCompletionColumnName.Text = _GlobalResources.ModuleStatus;
            lessonCompletionColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableTranscriptFullView'; SortTable(5,'T');";
            lessonCompletionHeader.Controls.Add(lessonCompletionColumnName);
            headerRow.Cells.Add(lessonCompletionHeader);

            // Score header cell
            TableHeaderCell scoreHeader = new TableHeaderCell();
            scoreHeader.ID = "CourseTranscriptFullViewScoreHeader";
            HyperLink scoreColumnName = new HyperLink();
            scoreColumnName.Text = _GlobalResources.Score;
            scoreColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableTranscriptFullView'; SortTable(6,'N');";
            scoreHeader.Controls.Add(scoreColumnName);
            headerRow.Cells.Add(scoreHeader);

            // attach header row to table
            tblTranscriptFullView.Rows.Add(headerRow);

            // loop through user account data tabs and build the table
            int i = 1;
            foreach (DataRow row in transcript.GetPersonalTranscriptFullView(userID, EnumDataSet.UserCourseTranscripts).Rows)
            {
                i++;
                TableRow dataRow = new TableRow();
                // apply css for alternating rows
                if (i % 2 == 0)
                { dataRow.CssClass = "UserField GridDataRow"; }
                else
                { dataRow.CssClass = "UserField GridDataRow GridDataRowAlternate"; }

                TableCell courseCell = new TableCell();
                courseCell.Text = row["Course"].ToString();
                dataRow.Cells.Add(courseCell);

                TableCell creditsCell = new TableCell();
                creditsCell.Text = row["Credits"].ToString();
                dataRow.Cells.Add(creditsCell);

                TableCell courseStatusCell = new TableCell();
                if (!String.IsNullOrWhiteSpace(row["Course Status"].ToString()))
                {
                    string courseStatus = row["Course Status"].ToString();

                    if (courseStatus == "completed")
                    { courseStatusCell.Text = _GlobalResources.Completed; }
                    else if (courseStatus == "expired")
                    { courseStatusCell.Text = _GlobalResources.Expired; }
                    else if (courseStatus == "overdue")
                    { courseStatusCell.Text = _GlobalResources.Overdue; }
                    else if (courseStatus == "future")
                    { courseStatusCell.Text = _GlobalResources.Future; }
                    else if (courseStatus == "enrolled")
                    { courseStatusCell.Text = _GlobalResources.Enrolled; }
                    else
                    { courseStatusCell.Text = String.Empty; }
                }
                else
                { courseStatusCell.Text = String.Empty; }
                dataRow.Cells.Add(courseStatusCell);

                TableCell dateCompletedCell = new TableCell();
                dateCompletedCell.Text = _GetTranscriptFormattedDateTimeString(row["Date Completed"].ToString());
                dataRow.Cells.Add(dateCompletedCell);

                TableCell lessonCell = new TableCell();
                lessonCell.Text = row["Lesson"].ToString();
                dataRow.Cells.Add(lessonCell);

                TableCell lessonCompletionCell = new TableCell();

                if (!String.IsNullOrWhiteSpace(row["Lesson"].ToString()))
                {
                    string lessonCompletionStatus = row["Lesson Completion"].ToString();
                    string lessonSuccessStatus = row["Lesson Success"].ToString();

                    if (lessonCompletionStatus == "completed" && lessonSuccessStatus == "passed")
                    { lessonCompletionCell.Text = _GlobalResources.Completed + ", " + _GlobalResources.Passed; }
                    else if (lessonCompletionStatus == "completed" && lessonSuccessStatus == "failed")
                    { lessonCompletionCell.Text = _GlobalResources.Completed + ", " + _GlobalResources.Failed; }
                    else if (lessonCompletionStatus == "completed" && (lessonSuccessStatus == "unknown" || String.IsNullOrWhiteSpace(lessonSuccessStatus)))
                    { lessonCompletionCell.Text = _GlobalResources.Completed; }
                    else if (lessonCompletionStatus == "incomplete" && lessonSuccessStatus == "failed")
                    { lessonCompletionCell.Text = _GlobalResources.Incomplete + ", " + _GlobalResources.Failed; }
                    else if (lessonCompletionStatus == "incomplete" && (lessonSuccessStatus == "unknown" || String.IsNullOrWhiteSpace(lessonSuccessStatus)))
                    { lessonCompletionCell.Text = _GlobalResources.Incomplete; }
                    else
                    { lessonCompletionCell.Text = _GlobalResources.IncompleteInProgress; }
                }
                else
                { lessonCompletionCell.Text = String.Empty; }

                dataRow.Cells.Add(lessonCompletionCell);

                TableCell scoreCell = new TableCell();
                scoreCell.Text = row["Score"].ToString();
                dataRow.Cells.Add(scoreCell);

                // attach data row to table
                tblTranscriptFullView.Rows.Add(dataRow);
            }
            if (i == 1)
            {
                TableRow dataRow = new TableRow();
                dataRow.CssClass = "GridDataRowEmpty";
                TableCell rowEmptyMessageCell = new TableCell();
                rowEmptyMessageCell.ColumnSpan = 7;
                rowEmptyMessageCell.Text = _GlobalResources.NoRecordsFound;
                dataRow.Cells.Add(rowEmptyMessageCell);
                tblTranscriptFullView.Rows.Add(dataRow);
            }

            tabPanel.Controls.Add(tblTranscriptFullView);
        }
        #endregion

        #region _LearningPathTranscriptsFullView
        /// <summary>
        /// Course Transcripts FullView
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private static void _LearningPathTranscriptsFullView(string userID, Panel tabPanel)
        {
            DataTable dtTranscript = new DataTable();
            Report transcript = new Report();

            // create full view table for transcript modal
            Table tblTranscriptFullView = new Table();
            tblTranscriptFullView.ID = "TableLearningPathTranscriptFullView";
            tblTranscriptFullView.CssClass = "GridTable";

            // HEADER ROW AND COLUMNS

            // create the header row
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.TableSection = TableRowSection.TableHeader;
            headerRow.CssClass = "GridHeaderRow";

            // learning path header cell
            TableHeaderCell learningPathHeader = new TableHeaderCell();
            learningPathHeader.ID = "LearningPathTranscriptFullViewCourseHeader";
            HyperLink learningPathColumnName = new HyperLink();
            learningPathColumnName.Text = _GlobalResources.LearningPath;
            learningPathColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableLearningPathTranscriptFullView'; SortTable(0,'T');";
            learningPathHeader.Controls.Add(learningPathColumnName);
            headerRow.Cells.Add(learningPathHeader);

            // completed status header cell
            TableHeaderCell completionStatusHeader = new TableHeaderCell();
            completionStatusHeader.ID = "LearningPathTranscriptFullViewCompletionStatusHeader";
            HyperLink completionStatusColumnName = new HyperLink();
            completionStatusColumnName.Text = _GlobalResources.Status;
            completionStatusColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableLearningPathTranscriptFullView'; SortTable(1,'T');";
            completionStatusHeader.Controls.Add(completionStatusColumnName);
            headerRow.Cells.Add(completionStatusHeader);


            // date completed header cell
            TableHeaderCell dateCompletedHeader = new TableHeaderCell();
            dateCompletedHeader.ID = "LearningPathTranscriptFullViewDateCompletedHeader";
            HyperLink dateCompletedColumnName = new HyperLink();
            dateCompletedColumnName.Text = _GlobalResources.DateCompleted;
            dateCompletedColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableLearningPathTranscriptFullView'; SortTable(2,'D');";
            dateCompletedHeader.Controls.Add(dateCompletedColumnName);
            headerRow.Cells.Add(dateCompletedHeader);

            // attach header row to table
            tblTranscriptFullView.Rows.Add(headerRow);

            // loop through user account data tabs and build the table
            int i = 1;
            foreach (DataRow row in transcript.GetPersonalTranscriptFullView(userID, EnumDataSet.UserLearningPathTranscripts).Rows)
            {
                i++;
                TableRow dataRow = new TableRow();
                // apply css for alternating rows
                if (i % 2 == 0)
                { dataRow.CssClass = "UserField GridDataRow"; }
                else
                { dataRow.CssClass = "UserField GridDataRow GridDataRowAlternate"; }

                TableCell learningPathCell = new TableCell();
                learningPathCell.Text = row["Learning Path"].ToString();
                dataRow.Cells.Add(learningPathCell);

                TableCell completionStatusCell = new TableCell();
                if (!String.IsNullOrWhiteSpace(row["Learning Path Status"].ToString()))
                {
                    string completionStatus = row["Learning Path Status"].ToString();

                    if (completionStatus == "completed")
                    { completionStatusCell.Text = _GlobalResources.Completed; }
                    else if (completionStatus == "overdue")
                    { completionStatusCell.Text = _GlobalResources.Overdue; }
                    else if (completionStatus == "future")
                    { completionStatusCell.Text = _GlobalResources.Future; }
                    else if (completionStatus == "enrolled")
                    { completionStatusCell.Text = _GlobalResources.Enrolled; }
                    else
                    { completionStatusCell.Text = String.Empty; }
                }
                else
                { completionStatusCell.Text = String.Empty; }
                dataRow.Cells.Add(completionStatusCell);

                TableCell dateCompletedCell = new TableCell();
                dateCompletedCell.Text = _GetTranscriptFormattedDateTimeString(row["Date Completed"].ToString());
                dataRow.Cells.Add(dateCompletedCell);

                // attach data row to table
                tblTranscriptFullView.Rows.Add(dataRow);
            }
            if (i == 1)
            {
                TableRow dataRow = new TableRow();
                dataRow.CssClass = "GridDataRowEmpty";
                TableCell rowEmptyMessageCell = new TableCell();
                rowEmptyMessageCell.ColumnSpan = 5;
                rowEmptyMessageCell.Text = _GlobalResources.NoRecordsFound;
                dataRow.Cells.Add(rowEmptyMessageCell);
                tblTranscriptFullView.Rows.Add(dataRow);
            }

            tabPanel.Controls.Add(tblTranscriptFullView);
        }
        #endregion

        #region _InstructorLedTranscriptsFullView
        /// <summary>
        /// Course Transcripts FullView
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="isInitialLoad"></param>
        private static void _InstructorLedTranscriptsFullView(string userID, Panel tabPanel)
        {
            DataTable dtTranscript = new DataTable();
            Report transcript = new Report();

            // create full view table for transcript modal
            Table tblTranscriptFullView = new Table();
            tblTranscriptFullView.ID = "TableInstructorLedTranscriptFullView";
            tblTranscriptFullView.CssClass = "GridTable";

            // HEADER ROW AND COLUMNS

            // create the header row
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.TableSection = TableRowSection.TableHeader;
            headerRow.CssClass = "GridHeaderRow";

            // module header cell
            TableHeaderCell moduleHeader = new TableHeaderCell();
            moduleHeader.ID = "InstructorLedTranscriptFullViewModuleHeader";
            HyperLink moduleColumnName = new HyperLink();
            moduleColumnName.Text = _GlobalResources.Module;
            moduleColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableInstructorLedTranscriptFullView'; SortTable(0,'T');";
            moduleHeader.Controls.Add(moduleColumnName);
            headerRow.Cells.Add(moduleHeader);

            // session header cell
            TableHeaderCell sessionHeader = new TableHeaderCell();
            sessionHeader.ID = "InstructorLedTranscriptFullViewSessionHeader";
            HyperLink sessionColumnName = new HyperLink();
            sessionColumnName.Text = _GlobalResources.Session;
            sessionColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableInstructorLedTranscriptFullView'; SortTable(1,'T');";
            sessionHeader.Controls.Add(sessionColumnName);
            headerRow.Cells.Add(sessionHeader);

            // completion status header cell
            TableHeaderCell statusHeader = new TableHeaderCell();
            statusHeader.ID = "InstructorLedTranscriptFullViewCompletionStatusHeader";
            HyperLink statusColumnName = new HyperLink();
            statusColumnName.Text = _GlobalResources.Status;
            statusColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableInstructorLedTranscriptFullView'; SortTable(2,'T');";
            statusHeader.Controls.Add(statusColumnName);
            headerRow.Cells.Add(statusHeader);

            // date completed header cell
            TableHeaderCell dateCompletedHeader = new TableHeaderCell();
            dateCompletedHeader.ID = "InstructorLedTranscriptFullViewDateCompletedHeader";
            HyperLink dateCompletedColumnName = new HyperLink();
            dateCompletedColumnName.Text = _GlobalResources.DateCompleted;
            dateCompletedColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableInstructorLedTranscriptFullView'; SortTable(3,'D');";
            dateCompletedHeader.Controls.Add(dateCompletedColumnName);
            headerRow.Cells.Add(dateCompletedHeader);

            // session score header cell
            TableHeaderCell sessionScoreHeader = new TableHeaderCell();
            sessionScoreHeader.ID = "InstructorLedTranscriptFullViewSessionScoreHeader";
            HyperLink sessionScoreColumnName = new HyperLink();
            sessionScoreColumnName.Text = _GlobalResources.Score;
            sessionScoreColumnName.NavigateUrl = "javascript: TableIDvalue = 'TableInstructorLedTranscriptFullView'; SortTable(4,'N');";
            sessionScoreHeader.Controls.Add(sessionScoreColumnName);
            headerRow.Cells.Add(sessionScoreHeader);

            // attach header row to table
            tblTranscriptFullView.Rows.Add(headerRow);

            // loop through user account data tabs and build the table
            int i = 1;
            foreach (DataRow row in transcript.GetPersonalTranscriptFullView(userID, EnumDataSet.UserInstructorLedTranscripts).Rows)
            {
                i++;
                TableRow dataRow = new TableRow();
                // apply css for alternating rows
                if (i % 2 == 0)
                { dataRow.CssClass = "UserField GridDataRow"; }
                else
                { dataRow.CssClass = "UserField GridDataRow GridDataRowAlternate"; }

                TableCell moduleCell = new TableCell();
                moduleCell.Text = row["Module"].ToString();
                dataRow.Cells.Add(moduleCell);

                TableCell sessionCell = new TableCell();
                sessionCell.Text = row["Session"].ToString();
                dataRow.Cells.Add(sessionCell);

                TableCell statusCell = new TableCell();
                string completionStatus = row["Completion Status"].ToString();
                string successStatus = row["Success Status"].ToString();

                if (completionStatus == "completed" && successStatus == "passed")
                { statusCell.Text = _GlobalResources.Completed + ", " + _GlobalResources.Passed; }
                else if (completionStatus == "completed" && successStatus == "failed")
                { statusCell.Text = _GlobalResources.Completed + ", " + _GlobalResources.Failed; }
                else if (completionStatus == "completed" && (successStatus == "unknown" || String.IsNullOrWhiteSpace(successStatus)))
                { statusCell.Text = _GlobalResources.Completed; }
                else if (completionStatus == "incomplete" && successStatus == "failed")
                { statusCell.Text = _GlobalResources.Incomplete + ", " + _GlobalResources.Failed; }
                else if (completionStatus == "incomplete" && (successStatus == "unknown" || String.IsNullOrWhiteSpace(successStatus)))
                { statusCell.Text = _GlobalResources.Incomplete; }
                else
                { statusCell.Text = _GlobalResources.IncompleteInProgress; }

                dataRow.Cells.Add(statusCell);

                TableCell dateCompletedCell = new TableCell();
                dateCompletedCell.Text = _GetTranscriptFormattedDateTimeString(row["Date Completed"].ToString());
                dataRow.Cells.Add(dateCompletedCell);

                TableCell sessionScoreCell = new TableCell();
                sessionScoreCell.Text = row["Session Score"].ToString();
                dataRow.Cells.Add(sessionScoreCell);

                // attach data row to table
                tblTranscriptFullView.Rows.Add(dataRow);
            }
            if (i == 1)
            {
                TableRow dataRow = new TableRow();
                dataRow.CssClass = "GridDataRowEmpty";
                TableCell rowEmptyMessageCell = new TableCell();
                rowEmptyMessageCell.ColumnSpan = 6;
                rowEmptyMessageCell.Text = _GlobalResources.NoRecordsFound;
                dataRow.Cells.Add(rowEmptyMessageCell);
                tblTranscriptFullView.Rows.Add(dataRow);
            }

            tabPanel.Controls.Add(tblTranscriptFullView);
        }
        #endregion

        #region _GetTranscriptFormattedDateTimeString
        /// <summary>
        /// Formats a Date/Time string so that is is enclosed inside of <span> tags.
        /// </summary>
        /// <param name="valueToFormat"></param>
        /// <returns></returns>
        private static string _GetTranscriptFormattedDateTimeString(string valueToFormat)
        {
            string formattedDateTimeString = valueToFormat;

            if (!String.IsNullOrWhiteSpace(valueToFormat))
            {
                // if the data is a date, it should be displayed as a formatted date
                DateTime dateValue;

                if (DateTime.TryParse(valueToFormat, out dateValue))
                {
                    // separate date and time into <spans> so that times could be hidden by CSS if we wanted
                    string formattedDate = "<span class=\"TranscriptFormattedDate\">" + dateValue.ToShortDateString() + "</span>";
                    string formattedTime = "<span class=\"TranscriptFormattedTime\">" + dateValue.ToShortTimeString() + "</span>";

                    // attach date and time <span>s to the row
                    formattedDateTimeString = formattedDate + " " + formattedTime;
                }
            }

            // return
            return formattedDateTimeString;
        }
        #endregion
    }
}
