﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;
using drawing = System.Drawing;
using Newtonsoft.Json.Linq;

namespace Asentia.LMS.Pages.Administrator.Users
{
    public class Certificates : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CertificatesFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel CertificatesPageWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel CertificateRecordGridUpdatePanel;
        public Grid CertificateGrid;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private User _UserObject;

        // award certificate controls
        private ModalPopup _AwardCertificateModal;
        private DropDownList _CertificateAwardCertificateSelector;
        private DatePicker _CertificateAwardDateTimeSelector;
        private TimeZoneSelector _CertificateAwardTimeZoneSelector;
        private Button _AwardCertificateModalHiddenLaunchButton;

        // view certificate controls
        private ModalPopup _ViewCertificateModal;
        private Button _CertificateViewerModalHiddenLaunchButton;

        // export certificate to pdf controls        
        private HiddenField _ExportCertificateToPdfHiddenCertificateId;
        private HiddenField _ExportCertificateToPdfHiddenCertificateRecordId;
        private Button _ExportCertificateToPdfHiddenButton;
        private string _CertificatePdfString;

        // delete certificate record(s) controls
        private ModalPopup _GridConfirmAction;
        private LinkButton _DeleteButton;
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Certificates), "Asentia.LMS.Pages.Administrator.Users.Certificates.js");
            csm.RegisterClientScriptResource(typeof(Certificates), "Asentia.LMS.Controls.CertificateObject.js");

            // build global JS variables
            StringBuilder globalJs = new StringBuilder();            
            globalJs.AppendLine("var DefaultCertificateImagePath =\"" + SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "\";");

            csm.RegisterClientScriptBlock(typeof(Certificates), "GlobalJs", globalJs.ToString(), true);            
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, EventArgs e)
        {
            // get the user object
            this._GetUserObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("Certificate.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/users/Certificates.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CertificatesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CertificatesPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu
            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Certificates;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }            

            // build the object options panel, and controls
            this._BuildObjectOptionsPanel();
            this._BuildControls();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.CertificateGrid.BindData();
            }
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetUserObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("uid", 0);
            int vsId = this.ViewStateInt(this.ViewState, "uid", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._UserObject = new User(id); }
                }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            { Response.Redirect("~/administrator/users"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {            
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(userDisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Certificates));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, _GlobalResources.Certificates, ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // AWARD CERTIFICATE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AwardCertificateLink",
                                                null,
                                                "javascript: AwardCertificate();",
                                                null,
                                                _GlobalResources.AwardCertificate,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildControls
        private void _BuildControls()
        {
            // instansiate award certificate controls
            this._AwardCertificateModalHiddenLaunchButton = new Button();
            this._AwardCertificateModalHiddenLaunchButton.ID = "AwardCertificateModalHiddenLaunchButton";
            this._AwardCertificateModalHiddenLaunchButton.Style.Add("display", "none");

            // instansiate view certificate controls
            this._CertificateViewerModalHiddenLaunchButton = new Button();
            this._CertificateViewerModalHiddenLaunchButton.ID = "CertificateViewerModalHiddenLaunchButton";
            this._CertificateViewerModalHiddenLaunchButton.Style.Add("display", "none");

            // instansiate export certificate to pdf controls
            this._ExportCertificateToPdfHiddenCertificateId = new HiddenField();
            this._ExportCertificateToPdfHiddenCertificateId.ID = "ExportCertificateToPdfHiddenCertificateId";

            this._ExportCertificateToPdfHiddenCertificateRecordId = new HiddenField();
            this._ExportCertificateToPdfHiddenCertificateRecordId.ID = "ExportCertificateToPdfHiddenCertificateRecordId";
         
            this._ExportCertificateToPdfHiddenButton = new Button();
            this._ExportCertificateToPdfHiddenButton.ID = "ExportCertificateToPdfHiddenButton";
            this._ExportCertificateToPdfHiddenButton.Style.Add("display", "none");
            this._ExportCertificateToPdfHiddenButton.Command += new CommandEventHandler(this._ExportCertificateToPDFButton_Command);        

            // build the award and view modals
            this._BuildAwardCertificateModal(this._AwardCertificateModalHiddenLaunchButton.ID);
            this._BuildViewCertificateModal(this._CertificateViewerModalHiddenLaunchButton.ID);

            // attach the controls instansiated above to the container
            this.CertificatesFormContentWrapperContainer.Controls.Add(this._AwardCertificateModalHiddenLaunchButton);
            this.CertificatesFormContentWrapperContainer.Controls.Add(this._CertificateViewerModalHiddenLaunchButton);
            this.CertificatesFormContentWrapperContainer.Controls.Add(this._ExportCertificateToPdfHiddenCertificateId);
            this.CertificatesFormContentWrapperContainer.Controls.Add(this._ExportCertificateToPdfHiddenCertificateRecordId);
            this.CertificatesFormContentWrapperContainer.Controls.Add(this._ExportCertificateToPdfHiddenButton);

            // build the certificates grid, the actions panel, and the actions modal
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();        
        }
        #endregion

        #region _BuildAwardCertificateModal
        /// <summary>
        /// Builds the modal for award certificate.
        /// </summary>
        private void _BuildAwardCertificateModal(string targetControlId)
        {
            // set modal properties
            this._AwardCertificateModal = new ModalPopup("AwardCertificateModal");
            this._AwardCertificateModal.Type = ModalPopupType.Form;
            this._AwardCertificateModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
            this._AwardCertificateModal.HeaderIconAlt = _GlobalResources.AwardCertificate;
            this._AwardCertificateModal.HeaderText = _GlobalResources.AwardCertificate;
            this._AwardCertificateModal.TargetControlID = targetControlId;
            this._AwardCertificateModal.SubmitButton.Command += new CommandEventHandler(this._AwardCertificateButton_Command);
            this._AwardCertificateModal.ReloadPageOnClose = false;

            // certificate selector
            this._CertificateAwardCertificateSelector = new DropDownList();
            this._CertificateAwardCertificateSelector.ID = "CertificateAwardCertificateSelector_Field";

            this._BindCertificateAwardCertificateSelector();

            this._AwardCertificateModal.AddControlToBody(AsentiaPage.BuildFormField("CertificateAwardCertificateSelector",
                                                                                    _GlobalResources.Certificate,
                                                                                    this._CertificateAwardCertificateSelector.ID,
                                                                                    this._CertificateAwardCertificateSelector,
                                                                                    true,
                                                                                    true,
                                                                                    false));

            // certificate award date selector
            List<Control> certificateAwardDateInputControls = new List<Control>();

            // date/time drop down
            this._CertificateAwardDateTimeSelector = new DatePicker("CertificateAwardDateTimeSelector_Field", false, true, true);
            certificateAwardDateInputControls.Add(this._CertificateAwardDateTimeSelector);

            // date/time timezone selector
            this._CertificateAwardTimeZoneSelector = new TimeZoneSelector();
            this._CertificateAwardTimeZoneSelector.ID = "CertificateAwardTimeZoneSelector_Field";            
            this._CertificateAwardTimeZoneSelector.SelectedValue = AsentiaSessionState.GlobalSiteObject.IdTimezone.ToString();
            certificateAwardDateInputControls.Add(this._CertificateAwardTimeZoneSelector);

            this._AwardCertificateModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("CertificateAwardDateTimeSelector",
                                                                                                        _GlobalResources.AwardDate,
                                                                                                        certificateAwardDateInputControls,
                                                                                                        true,
                                                                                                        true,
                                                                                                        true));

            // attach modal to container
            this.CertificatesFormContentWrapperContainer.Controls.Add(this._AwardCertificateModal);
        }
        #endregion

        #region _BindCertificateAwardCertificateSelector
        /// <summary>
        /// Binds the certificate award certificate drop down list.
        /// </summary>
        private void _BindCertificateAwardCertificateSelector()
        {
            DataTable certificateDataTable = new DataTable();
            certificateDataTable = Certificate.IdsAndNamesEligibleForUserAward(this._UserObject.Id);

            this._CertificateAwardCertificateSelector.DataSource = certificateDataTable;
            this._CertificateAwardCertificateSelector.DataTextField = "name";
            this._CertificateAwardCertificateSelector.DataValueField = "idCertificate";
            this._CertificateAwardCertificateSelector.DataBind();
        }

        #endregion

        #region _ValidateAwardCertificateForm
        /// <summary>
        /// Validates the award certificate form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateAwardCertificateForm()
        {
            bool isValid = true;

            // certificate selector
            int idCertificate = 0;
            int.TryParse(this._CertificateAwardCertificateSelector.SelectedValue, out idCertificate);

            if (idCertificate <= 0)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CertificatesFormContentWrapperContainer, "CertificateAwardCertificateSelector", _GlobalResources.Certificate + " " + _GlobalResources.IsRequired);
            }

            // certificate date/time field
            if (this._CertificateAwardDateTimeSelector.Value == null || !(this._CertificateAwardDateTimeSelector.Value is DateTime))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CertificatesFormContentWrapperContainer, "CertificateAwardDateTimeSelector", _GlobalResources.AwardDate + " " + _GlobalResources.IsRequired);
            }
            else
            {
                if (!this._CertificateAwardDateTimeSelector.NowCheckBoxChecked)
                {
                    // ensure that the award date is not in the future, past is ok
                    Timezone tz = new Timezone(Convert.ToInt32(this._CertificateAwardTimeZoneSelector.SelectedValue));

                    if (TimeZoneInfo.ConvertTimeToUtc((DateTime)this._CertificateAwardDateTimeSelector.Value, TimeZoneInfo.FindSystemTimeZoneById(tz.dotNetName)) > AsentiaSessionState.UtcNow)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.CertificatesFormContentWrapperContainer, "CertificateAwardDateTimeSelector", _GlobalResources.AwardDateCannotBeInTheFuture);
                    }
                }
            }

            return isValid;
        }
        #endregion

        #region _AwardCertificateButton_Command
        /// <summary>
        /// Handles the event initiated by the Award Certificate Submit Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _AwardCertificateButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // instansiate a new certificate record object
                CertificateRecord certificateRecord = new CertificateRecord();

                // validate the form
                if (!this._ValidateAwardCertificateForm())
                { throw new AsentiaException(); }

                // set the properties
                certificateRecord.IdCertificate = Convert.ToInt32(this._CertificateAwardCertificateSelector.SelectedValue);
                certificateRecord.IdUser = this._UserObject.Id;
                certificateRecord.IdTimeZone = Convert.ToInt32(this._CertificateAwardTimeZoneSelector.SelectedValue);

                if (this._CertificateAwardDateTimeSelector.NowCheckBoxChecked)
                { certificateRecord.Timestamp = AsentiaSessionState.UtcNow; }
                else
                {
                    Timezone tz = new Timezone(Convert.ToInt32(this._CertificateAwardTimeZoneSelector.SelectedValue));
                    certificateRecord.Timestamp = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._CertificateAwardDateTimeSelector.Value, TimeZoneInfo.FindSystemTimeZoneById(tz.dotNetName));
                }
                
                // save the certificate
                int idCertificateRecord = certificateRecord.Save();

                // rebind the certficate selector
                this._BindCertificateAwardCertificateSelector();

                // rebind the certificates grid
                this.CertificateGrid.BindData();

                // update the grid container
                this.CertificateRecordGridUpdatePanel.Update();

                // display success feedback
                this._AwardCertificateModal.DisplayFeedback(_GlobalResources.CertificateAwardedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AwardCertificateModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AwardCertificateModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dfnuEx)
            {
                // display the failure message
                this._AwardCertificateModal.DisplayFeedback(dfnuEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AwardCertificateModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AwardCertificateModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _BuildViewCertificateModal
        /// <summary>
        /// Builds the modal for viewing the certificate
        /// </summary>
        /// <param name="idCertificateRecord"></param>
        /// <param name="idCertificate"></param>
        /// <param name="targetControlId"></param>
        /// <param name="modalContainer"></param>
        private void _BuildViewCertificateModal(string targetControlId)
        {
            this._ViewCertificateModal = new ModalPopup("ViewCertificateModal");
            this._ViewCertificateModal.Type = ModalPopupType.Information;
            this._ViewCertificateModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
            this._ViewCertificateModal.HeaderIconAlt = _GlobalResources.Certificate;
            this._ViewCertificateModal.HeaderText = _GlobalResources.Certificate;
            this._ViewCertificateModal.TargetControlID = targetControlId;
            this._ViewCertificateModal.ShowLoadingPlaceholder = true;
            
            // create and add certificate wrapper to modal body
            Panel certificateWrapper = new Panel();
            certificateWrapper.ID = "CertificateWrapper";

            this._ViewCertificateModal.AddControlToBody(certificateWrapper);

            // attach modal to container
            this.CertificatesFormContentWrapperContainer.Controls.Add(this._ViewCertificateModal); 
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.CertificateGrid.StoredProcedure = Library.Certificate.GridProcedureForUser;
            this.CertificateGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.CertificateGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.CertificateGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.CertificateGrid.AddFilter("@idUser", SqlDbType.Int, 4, this._UserObject.Id);
            this.CertificateGrid.IdentifierField = "idCertificateRecord";
            this.CertificateGrid.DefaultSortColumn = "timestamp";
            this.CertificateGrid.IsDefaultSortDescending = true;
            this.CertificateGrid.ShowTimeInDateStrings = false;

            // data key names
            this.CertificateGrid.DataKeyNames = new string[] { "idCertificateRecord", "idCertificate" };

            // columns
            GridColumn nameCreditsAwardDateExpirationDate = new GridColumn(_GlobalResources.Certificate + ", " + _GlobalResources.Credits + ", " + _GlobalResources.AwardDate + ", " + _GlobalResources.Expires, null); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.CertificateGrid.AddColumn(nameCreditsAwardDateExpirationDate);
            this.CertificateGrid.AddColumn(options);

            // add row data bound event
            this.CertificateGrid.RowDataBound += new GridViewRowEventHandler(this._CertificateGrid_RowDataBound);
        }
        #endregion

        #region _CertificateGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the certificates grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _CertificateGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idCertificate = Convert.ToInt32(rowView["idCertificate"]);
                int idCertificateRecord = Convert.ToInt32(rowView["idCertificateRecord"]);

                // AVATAR, CERTIFICATE, CREDITS, AWARD DATE, EXPIRES

                string certificateName = rowView["name"].ToString();
                int credits = Convert.ToInt32(rowView["credits"]);
                DateTime dtAward = Convert.ToDateTime(rowView["timestamp"]);
                DateTime? dtExpires = (rowView["dtExpires"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(rowView["dtExpires"]));
                bool isPDFExportOn = Convert.ToBoolean(rowView["isPDFExportOn"]);
                bool isPrintOn = Convert.ToBoolean(rowView["isPrintOn"]);
                bool isViewOn = Convert.ToBoolean(rowView["isViewOn"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = certificateName;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // label
                Label labelLabelWrapper = new Label();
                labelLabelWrapper.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(labelLabelWrapper);

                Literal labelLabel = new Literal();
                labelLabel.Text = certificateName;
                labelLabelWrapper.Controls.Add(labelLabel);

                // credits
                Label creditsLabel = new Label();
                creditsLabel.CssClass = "GridSecondaryLine";
                creditsLabel.Text = credits.ToString() + " " + ((credits != 1) ? _GlobalResources.Credits : _GlobalResources.Credit);
                e.Row.Cells[1].Controls.Add(creditsLabel);

                // award and expires dates
                Label awardExpiresDates = new Label();
                awardExpiresDates.CssClass = "GridSecondaryLine";
                awardExpiresDates.Text = _GlobalResources.AwardDate + ": " + TimeZoneInfo.ConvertTimeFromUtc(dtAward, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
                
                if (dtExpires != null)
                { awardExpiresDates.Text += " | " + _GlobalResources.Expires + ": " + TimeZoneInfo.ConvertTimeFromUtc((DateTime)dtExpires, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern); }
                
                e.Row.Cells[1].Controls.Add(awardExpiresDates);

                // OPTIONS COLUMN

                // pdf
                if (isPDFExportOn)
                {
                    Label pdfSpan = new Label();
                    pdfSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(pdfSpan);

                    HyperLink pdfLink = new HyperLink();
                    pdfLink.NavigateUrl = "javascript: void(0);";
                    pdfLink.Attributes.Add("onclick", "ExportCertificateToPDF(" + idCertificate.ToString() + ", " + idCertificateRecord.ToString() + ");return false;");
                    pdfSpan.Controls.Add(pdfLink);

                    Image pdfIcon = new Image();
                    pdfIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PDF, ImageFiles.EXT_PNG);
                    pdfIcon.AlternateText = _GlobalResources.SaveAsPDF;
                    pdfIcon.ToolTip = _GlobalResources.SaveAsPDF;
                    pdfLink.Controls.Add(pdfIcon);
                }

                // print
                if (isPrintOn)
                {
                    Label printSpan = new Label();
                    printSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(printSpan);

                    HyperLink printLink = new HyperLink();
                    printLink.NavigateUrl = "javascript: void(0);";
                    printLink.Attributes.Add("onclick", "PrintCertificate(" + idCertificateRecord.ToString() + ");return false;");
                    printSpan.Controls.Add(printLink);

                    Image printIcon = new Image();
                    printIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PRINT, ImageFiles.EXT_PNG);
                    printIcon.AlternateText = _GlobalResources.Print;
                    printIcon.ToolTip = _GlobalResources.Print;
                    printLink.Controls.Add(printIcon);
                }

                // view
                if (isViewOn)
                {
                    Label viewSpan = new Label();
                    viewSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(viewSpan);

                    HyperLink viewLink = new HyperLink();
                    viewLink.NavigateUrl = "javascript: void(0);";
                    viewLink.Attributes.Add("onclick", "ViewCertificate(" + idCertificateRecord.ToString() + ");return false;");
                    viewSpan.Controls.Add(viewLink);

                    Image viewIcon = new Image();
                    viewIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_VIEW, ImageFiles.EXT_PNG);
                    viewIcon.AlternateText = _GlobalResources.View;
                    viewIcon.ToolTip = _GlobalResources.View;
                    viewLink.Controls.Add(viewIcon);
                }
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedCertificate_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion        

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup();
            this._GridConfirmAction.ID = "GridConfirmAction";

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedCertificate_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseCertificate_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.CertificateGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.CertificateGrid.Rows[i].FindControl(this.CertificateGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.CertificateRecord.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedCertificatesHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoCertificate_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.CertificateGrid.BindData();

                // update the grid container
                this.CertificateRecordGridUpdatePanel.Update();

                // rebind the certficate selector
                // this is done here so that the award certificate selector accurately reflects certificates that can be awarded
                this._BindCertificateAwardCertificateSelector();
            }
        }
        #endregion

        #region _ExportCertificateToPDFButton_Command
        /// <summary>
        /// Handles the "Export to pdf" button click.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _ExportCertificateToPDFButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(this._ExportCertificateToPdfHiddenCertificateId.Value) && !String.IsNullOrWhiteSpace(this._ExportCertificateToPdfHiddenCertificateRecordId.Value))
                {
                    //Bulid certifiacateId
                    int certificateID = Convert.ToInt32(this._ExportCertificateToPdfHiddenCertificateId.Value);
                    int certificateRecordID = Convert.ToInt32(this._ExportCertificateToPdfHiddenCertificateRecordId.Value);
                    Certificate certificateObject = new Certificate(certificateID);
                    CertificateRecord crObject = new CertificateRecord(certificateRecordID);

                    string certificateName = String.Empty;
                    string pdfFileName = "[FirstName]-[LastName]_[CertificateTitle]_[AwardDateISO].pdf";
                    string firstNameValue = String.Empty;
                    string lastNameValue = String.Empty;

                    foreach (Certificate.LanguageSpecificProperty certificateLanguageSpecificProperty in certificateObject.LanguageSpecificProperties)
                    {
                        if (certificateLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { certificateName = certificateLanguageSpecificProperty.Name; }
                    }

                    pdfFileName = pdfFileName.Replace("[CertificateTitle]", Utility.RemoveSpecialCharactersForFileName(certificateObject.Name));
                    pdfFileName = pdfFileName.Replace("[AwardDateISO]", String.Format("{0:yyyy-MM-dd}", TimeZoneInfo.ConvertTimeFromUtc((DateTime)crObject.Timestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName))));

                    //Build file path
                    string filePath = SitePathConstants.SITE_CERTIFICATES_ROOT + certificateID + "/Certificate.json";

                    //Load html of the selected language
                    if (File.Exists(HttpContext.Current.Server.MapPath(filePath)))
                    {
                        string certLayoutString = crObject.BuildCertificateLayoutJSON();

                        string viewerPDF = String.Empty;

                        //Calculate proportional height and width.
                        double defaultWidth = 800;
                        double defaultHeight = 600;
                        JObject layoutString = JObject.Parse(certLayoutString);

                        double imageWidth = Convert.ToDouble(((string)layoutString["Certificate"]["Container"]["Width"]).Replace("px", ""));
                        double imageHeight = Convert.ToDouble(((string)layoutString["Certificate"]["Container"]["Height"]).Replace("px", ""));

                        double newWidth = imageWidth;
                        double newHeight = imageHeight;
                        double percentIncreased = 0;
                        bool lanscapeOrientation = true;

                        if (newHeight > newWidth)
                        {
                            lanscapeOrientation = false;
                            defaultWidth = 600;
                            defaultHeight = 800;
                        }

                        double widthToIncrease = defaultWidth - imageWidth;
                        double percentWidthToIncrease = (widthToIncrease / imageWidth) * 100;
                        double heightToIncrease = defaultHeight - imageHeight;
                        double percentheightToIncrease = (heightToIncrease / imageHeight) * 100;

                        percentIncreased = (percentWidthToIncrease < percentheightToIncrease) ? percentWidthToIncrease : percentheightToIncrease;

                        newWidth = imageWidth + (imageWidth * percentIncreased) / 100;
                        newHeight = imageHeight + (imageHeight * percentIncreased) / 100;

                        foreach (var element in layoutString["Certificate"]["Elements"])
                        {
                            string left = (string)element["Left"];
                            left = left.Replace("%", "");
                            string top = (string)element["Top"];
                            top = top.Replace("%", "");
                            string width = (string)element["Width"];
                            width = width.Replace("%", "");

                            string viewerElement = element["IsStaticLabel"] + " | ";
                            viewerElement += (((bool)element["IsStaticLabel"]) ? (string)element["Value"] : (string)element["Identifier"]) + " | ";
                            viewerElement += (string)element["FontSize"] + " | ";
                            viewerElement += Convert.ToString(Convert.ToDouble(element["Left"].Value<string>().Replace("%", "")) / 100 * imageWidth) + " | ";
                            viewerElement += Convert.ToString(Convert.ToDouble(element["Top"].Value<string>().Replace("%", "")) / 100 * imageHeight) + " | ";
                            viewerElement += (string)element["Alignment"] + " | ";
                            viewerElement += (string)element["Width"] + " | ";
                            viewerPDF += viewerElement + " ^ ";
                        }

                        this._CertificatePdfString = viewerPDF.Substring(0, viewerPDF.Length - 3);

                        string backgroundImageFilePath = null;
                        if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + certificateID + "/" + certificateObject.FileName)))
                        {
                            backgroundImageFilePath = HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + certificateID + "/" + certificateObject.FileName);
                        }

                        string url;
                        if (backgroundImageFilePath != null && File.Exists(backgroundImageFilePath))
                        {
                            url = backgroundImageFilePath;
                        }
                        else
                        {
                            url = HttpContext.Current.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "default.jpg");
                        }

                        using (var pdfGenerator = new PdfGenerator(url, newWidth, newHeight, PdfGenerator.PageSize.Letter, (lanscapeOrientation) ? PdfGenerator.Orientation.Landscape : PdfGenerator.Orientation.Portrait))
                        {
                            if (!(String.IsNullOrWhiteSpace(this._CertificatePdfString)))
                            {
                                FieldInfo[] fieldInfoArray = this._UserObject.GetType().GetFields();
                                string[] labelsArray = this._CertificatePdfString.Split(new string[] { " ^ " }, StringSplitOptions.None);

                                firstNameValue = fieldInfoArray[0] != null ? fieldInfoArray[1].GetValue(this._UserObject).ToString() : String.Empty;
                                lastNameValue = fieldInfoArray[2] != null ? fieldInfoArray[3].GetValue(this._UserObject).ToString() : String.Empty;

                                pdfFileName = pdfFileName.Replace("[FirstName]", firstNameValue);
                                pdfFileName = pdfFileName.Replace("[LastName]", lastNameValue);

                                foreach (var str in labelsArray)
                                {
                                    var arr = str.Split(new string[] { " | " }, StringSplitOptions.None);
                                    bool isStaticLabel = false;
                                    string label = arr[1];
                                    double fontSize = Convert.ToDouble(arr[2].Replace("em", "")) * 16;
                                    double x = Convert.ToDouble(arr[3]);
                                    double y = Convert.ToDouble(arr[4]);
                                    string textAlignment = arr[5].ToLower();
                                    double width = Convert.ToDouble(arr[6].Replace("%", "")) * newWidth / 100;

                                    if (isStaticLabel)
                                    {
                                        label = label.Replace("[^]", "^").Replace("[|]", "|"); //Replace the marked separators
                                    }
                                    else
                                    {
                                        foreach (FieldInfo fieldInfo in fieldInfoArray)
                                        {
                                            object fieldValue = fieldInfo.GetValue(this._UserObject);
                                            string value = fieldValue != null ? fieldValue.ToString() : String.Empty;

                                            if (fieldInfo.Name.ToLower() == label.ToLower())
                                            {
                                                if (fieldInfo.Name == "DtExpires" || fieldInfo.Name == "DtCreated")
                                                { label = value != String.Empty ? String.Format("{0:MMMM dd, yyyy}", TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(value), TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName))) : String.Empty; }
                                                else
                                                { label = value; }
                                                break;
                                            }
                                        }

                                        if (label == "fullName")
                                            label = firstNameValue + " " + lastNameValue;

                                        switch (label)
                                        {
                                            case "certificateName":
                                                label = certificateName; break;
                                            case "issuingOrganization":
                                                label = certificateObject.IssuingOrganization != null ? certificateObject.IssuingOrganization.ToString() : String.Empty; break;
                                            case "dtAward":
                                                label = crObject.Timestamp != null ? String.Format("{0:MMMM dd, yyyy}", TimeZoneInfo.ConvertTimeFromUtc((DateTime)crObject.Timestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName))) : String.Empty; break;
                                            case "dtCertificateExpires":
                                                label = crObject.Expires != null ? String.Format("{0:MMMM dd, yyyy}", TimeZoneInfo.ConvertTimeFromUtc((DateTime)crObject.Expires, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName))) : String.Empty; break;
                                            case "certificateCode":
                                                label = crObject.Code != null ? crObject.Code.ToString() : String.Empty; break;
                                            case "certificateCredits":
                                                label = crObject.Credits != null ? crObject.Credits.ToString() : String.Empty; break;
                                        }
                                    }

                                    PdfGenerator.Alignment alignment;
                                    switch (textAlignment)
                                    {
                                        case "center":
                                            alignment = PdfGenerator.Alignment.Center;
                                            break;
                                        case "right":
                                            alignment = PdfGenerator.Alignment.Right;
                                            break;
                                        default:
                                            alignment = PdfGenerator.Alignment.Left;
                                            break;
                                    }

                                    width = width + ((width * percentIncreased) / 100);

                                    fontSize = fontSize + ((fontSize * percentIncreased) / 100);

                                    //Write string to the document
                                    pdfGenerator.WriteString(label, x, y, fontSize, alignment, width, fontSize + 12);
                                }
                            }

                            //Send certificate as a pdf.
                            MemoryStream stream = pdfGenerator.GetStream();
                            Page.Response.Clear();
                            Page.Response.ContentType = "application/pdf";
                            Page.Response.AddHeader("Content-Disposition", "attachment; filename=" + pdfFileName + ";");
                            Page.Response.AddHeader("content-length", stream.Length.ToString());
                            Page.Response.BinaryWrite(stream.ToArray());
                            Page.Response.Flush();
                            stream.Close();
                            Page.Response.End();
                        }

                    }
                }
            }
            catch (AsentiaException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion        
    }
}
