﻿///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////        AWARD CERTIFICATE         //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function AwardCertificate() {
    $("#AwardCertificateModalModalPopupFeedbackContainer").hide();
    $("#AwardCertificateModalModalPopupFeedbackContainer").empty();
    $("#AwardCertificateModalHiddenLaunchButton").click();
}

///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////        VIEW CERTIFICATE         ///////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function ViewCertificate(idCertificateRecord) {
    // hide the body, show the loading placeholder    
    $("#ViewCertificateModalModalPopupBody").hide();
    $("#ViewCertificateModalModalPopupPostbackLoadingPlaceholder").show();    

    // clear the modal header and certificate wrapper
    $("#ViewCertificateModalModalPopupHeaderText").text("");
    $("#CertificateWrapper").empty();

    // launch the modal using the hidden button
    $("#CertificateViewerModalHiddenLaunchButton").click();

    // get the certificate html
    GetCertificateHTML(idCertificateRecord);
}

function GetCertificateHTML(idCertificateRecord) {
    $.ajax({
        type: "POST",
        url: "/_util/WidgetService.asmx/BuildCertificateViewerModal",
        data: "{idCertificateRecord: " + idCertificateRecord + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnCertificateHTMLSuccess,
        failure: function (response) {
            //alert(response.d);
            $("#ViewCertificateModalModalPopupPostbackLoadingPlaceholder").hide();
            $("#ViewCertificateModalModalPopupBody").show();
        },
        error: function (response) {
            //alert(response.d);
            $("#ViewCertificateModalModalPopupPostbackLoadingPlaceholder").hide();
            $("#ViewCertificateModalModalPopupBody").show();            
        }
    });
}

function OnCertificateHTMLSuccess(response) {
    // get the response
    var responseObject = response.d;
    
    // hide the loader and show the body    
    $("#ViewCertificateModalModalPopupPostbackLoadingPlaceholder").hide();
    $("#ViewCertificateModalModalPopupBody").show();

    // change the modal header
    $("#ViewCertificateModalModalPopupHeaderText").text(responseObject.certificateTitle);
    
    // build the certificate wrapper
    var LayoutJSONObj = jQuery.parseJSON(responseObject.certificateLayoutJson);
    var AwardDataJSONObj = jQuery.parseJSON(responseObject.certificateAwardDataJson);
    var certificateModeType = "2";
    var builderDictionaryJSON = "{}";
    var BuilderDictionaryJSONObj = jQuery.parseJSON(builderDictionaryJSON);
    var CertificateContainerElement = document.getElementById("CertificateWrapper");
    var certBuilderDefaultWidth = LayoutJSONObj.Certificate.Container.Width;
    var certBuilderDefaultHeight = LayoutJSONObj.Certificate.Container.Height;
    var SaveElement = "";
    
    var CertificateViewer = new CertificateObject(
					LayoutJSONObj,
					AwardDataJSONObj,
					certificateModeType,
					BuilderDictionaryJSONObj,
					CertificateContainerElement,
					DefaultCertificateImagePath,
					certBuilderDefaultWidth,
					certBuilderDefaultHeight,
					SaveElement
            );    
    
    // force the modal to be resized using a bit of trickery because the standard centering callback doesn't want to cooperate
    // we're going to do this by executing the modal centering method every 1/4 second for about 5 seconds, this should make sure
    // that the loaded modal gets centered

    var intervalCounter = 0;
    var intervalFunction = function () {
        if (intervalCounter <= 20) { 
            intervalCounter++;
            SetReCenterOnShownForModal_ViewCertificateModalModalPopupExtender();
        } else {
            clearInterval(intervalFunction);
        }
    }

    setInterval(intervalFunction, 250);    
}

///////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////        PRINT CERTIFICATE         //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function PrintCertificate(idCertificateRecord) {    
    GetCertificateHTMLPrint(idCertificateRecord);
}

function GetCertificateHTMLPrint(idCertificateRecord) {
    $.ajax({
        type: "POST",
        url: "/_util/WidgetService.asmx/BuildCertificateViewerModal",
        data: "{idCertificateRecord: " + idCertificateRecord + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnCertificateHTMLPrintSuccess,
        failure: function (response) {
            //alert(response.d);
        },
        error: function (response) {
            //alert(response.d);
        }
    });
}

function OnCertificateHTMLPrintSuccess(response) {
    // get the response
    var responseObject = response.d;
    var printContainerWidth = jQuery.parseJSON(responseObject.certificateLayoutJson).Certificate.Container.Width;
    // create a container to hold the rendered certificate for printing, and append it to body
    var printableCertificateContainer = $("<div id=\"PrintableCertificateContainer\" style=\"position: relative; visibility: hidden;\"></div>");
    //$("#PrintableCertificateContainer").remove();
    $("body").append(printableCertificateContainer);

    // create a style tag for print use and attach it to the printable div, it will be removed with the div after printing
    // note that we do this dynamically instead of inside of a stylesheet so that we can focus @media print queries to the 
    // specific things we need them for, then have them removed - this is better than having @media print queries in several 
    // different stylesheets and possibly colliding

    var printableStylesheet = "<style>\
    @media print\
    {\
         @page\
        {\
            margin: auto;\
            size: landscape;\
        }\
     \
        #MasterForm\
        {\
            display: none;\
        }\
     \
         #PrintableCertificateContainer\
        {\
            visibility: visible !important;\
            position: absolute;\
            left: 0;\
            top: 0;\
        }\
    }\
    </style>";

    if (printContainerWidth == "600px")
        printableStylesheet = printableStylesheet.replace("landscape", "portrait");

    $("#PrintableCertificateContainer").append(printableStylesheet);

    // build the certificate wrapper
    var LayoutJSONObj = jQuery.parseJSON(responseObject.certificateLayoutJson);
    var AwardDataJSONObj = jQuery.parseJSON(responseObject.certificateAwardDataJson);
    var certificateModeType = "3";
    var builderDictionaryJSON = "{}";
    var BuilderDictionaryJSONObj = jQuery.parseJSON(builderDictionaryJSON);
    var CertificateContainerElement = document.getElementById("PrintableCertificateContainer");
    var certBuilderDefaultWidth = (LayoutJSONObj.Certificate.Container.Width == "800px") ? "1024px" : "768px";
    var certBuilderDefaultHeight = (LayoutJSONObj.Certificate.Container.Width == "800px") ? "768px" : "1024px";
    var SaveElement = "";

    var CertificateViewer = new CertificateObject(
					LayoutJSONObj,
					AwardDataJSONObj,
					certificateModeType,
					BuilderDictionaryJSONObj,
					CertificateContainerElement,
					DefaultCertificateImagePath,
					certBuilderDefaultWidth,
					certBuilderDefaultHeight,
					SaveElement
            );
}

///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////        CERTIFICATE TO PDF         //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function ExportCertificateToPDF(idCertificate, idCertificateRecord) {
    // set the hidden input values
    $('#ExportCertificateToPdfHiddenCertificateId').val(idCertificate);
    $('#ExportCertificateToPdfHiddenCertificateRecordId').val(idCertificateRecord);

    // execute the pdf export using the hidden button
    $("#ExportCertificateToPdfHiddenButton").click();
}