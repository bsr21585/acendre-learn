﻿function LoadCertificationTaskModalContent(idUser, idCertificationToUserLink, idDataCertificationModuleRequirement) {
    $("#TaskData").val(idUser + "|" + idCertificationToUserLink + "|" + idDataCertificationModuleRequirement);

    $("#TaskProctoringModalLaunchButton").click();
    $("#TaskProctoringModalLoadButton").click();
}

function LoadCertificationInitialAwardDateModalContent() {
    
    $("#ModifyCertificationInitialAwardDateModalLaunchButton").click();
    $("#ModifyCertificationInitialAwardDateModalLoadButton").click();
}

function LoadCertificationCurrentExpirationDateModalContent() {

    $("#ModifyCertificationCurrentExpirationDateModalLaunchButton").click();
    $("#ModifyCertificationCurrentExpirationDateModalLoadButton").click();
    
}

function LoadCertificationLastExpirationDateModalContent() {

    $("#ModifyCertificationLastExpirationDateModalLaunchButton").click();
    $("#ModifyCertificationLastExpirationDateModalLoadButton").click();

}