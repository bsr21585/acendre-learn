﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.Users.Certifications
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel CertificationsFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel CertificationsPageWrapperContainer;
        public Panel UserCertificationsPropertiesContainer;
        public UpdatePanel CertificationsGridUpdatePanel;
        public Panel CertificationsGridActionsPanel;  
        #endregion

        #region Private Properties
        private User _UserObject;

        private ModalPopup _CertificationResetConfirmAction;        
        private ModalPopup _CertificationGridConfirmAction;
        private ModalPopup _CertificationJoinModal;

        private HiddenField _HiddenResetButton;
        private HiddenField _ResetCertificationId;

        private DataTable _CertificationsNotForUser;

        private DynamicListBox _AddEligibleCertifications;

        private Button _JoinToCertificationsHiddenButton;

        private LinkButton _CertificationsDeleteButton;
        
        private Grid _CertificationsGrid;              
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Users.Certifications.Default.js");            
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the user object
            this._GetUserObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/users/certifications/Default.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CertificationsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CertificationsPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu
            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Certifications;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }            

            // build the grid, actions panel, and modals
            this._BuildObjectOptionsPanel();
            this._BuildPageControls();

            // if not postback
            if (!IsPostBack)
            { this._CertificationsGrid.BindData(); }
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetUserObject()
        {
            // get the user id querystring parameter
            int qsUId = this.QueryStringInt("uid", 0);

            if (qsUId > 0)
            {
                int uid = 0;

                if (qsUId > 0)
                { uid = qsUId; }

                try
                {
                    if (uid > 0)
                    { this._UserObject = new User(uid); }
                }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            { Response.Redirect("~/administrator/users"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(userDisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Certifications));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, _GlobalResources.Certifications, ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion        

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // JOIN TO CERTIFICATION
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("JoinToCertificationLink",
                                                null,
                                                "javascript:JoinToCertification();",
                                                null,
                                                _GlobalResources.JoinToCertification,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildPageControls
        private void _BuildPageControls()
        {
            this._BuildCertificationsGrid();            
            this._BuildCertificationsGridActionsPanel();            
            this._BuildCertificationGridActionsModal();
            this._BuildCertificationResetConfirmModal();

            this._CertificationsNotForUser = Asentia.LMS.Library.Certification.IdsAndNamesForUserSelectList(this._UserObject.Id, null);

            this._JoinToCertificationsHiddenButton = new Button();
            this._JoinToCertificationsHiddenButton.ID = "JoinToCertificationsHiddenButton";
            this._JoinToCertificationsHiddenButton.Style.Add("display", "none");
            this.UserCertificationsPropertiesContainer.Controls.Add(this._JoinToCertificationsHiddenButton);

            this._BuildJoinCertificationsToUserModal(this._JoinToCertificationsHiddenButton.ID);
        }
        #endregion        

        #region _BuildCertificationsGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildCertificationsGrid()
        {
            this._CertificationsGrid = new Grid();
            this._CertificationsGrid.ID = "CertificationsGrid";            
            this._CertificationsGrid.StoredProcedure = Library.Certification.GridProcedureForUser;
            this._CertificationsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._CertificationsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._CertificationsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._CertificationsGrid.AddFilter("@idUser", SqlDbType.Int, 4, this._UserObject.Id);
            this._CertificationsGrid.IdentifierField = "idCertificationToUserLink";
            this._CertificationsGrid.DefaultSortColumn = "title";

            // data key names
            this._CertificationsGrid.DataKeyNames = new string[] { "idCertificationToUserLink", "idCertification" };

            // columns
            GridColumn title = new GridColumn(_GlobalResources.Certification, null, "title"); // this is calculated dynamically in the RowDataBound method

            GridColumn status = new GridColumn(_GlobalResources.Status, "status", "status");            
            status.AddProperty(new GridColumnProperty("0", _GlobalResources.IncompleteInProgress));
            status.AddProperty(new GridColumnProperty("1", _GlobalResources.Current));
            status.AddProperty(new GridColumnProperty("2", _GlobalResources.Expired));

            GridColumn initialAwardDate = new GridColumn(_GlobalResources.InitialAward, "initialAwardDate", "initialAwardDate");

            GridColumn lastExpirationDate = new GridColumn(_GlobalResources.LastExpiration, "lastExpirationDate", "lastExpirationDate");

            GridColumn currentExpirationDate = new GridColumn(_GlobalResources.CurrentExpiration, "currentExpirationDate", "currentExpirationDate");

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this._CertificationsGrid.AddColumn(title);
            this._CertificationsGrid.AddColumn(status);
            this._CertificationsGrid.AddColumn(initialAwardDate);
            this._CertificationsGrid.AddColumn(lastExpirationDate);
            this._CertificationsGrid.AddColumn(currentExpirationDate);
            this._CertificationsGrid.AddColumn(options);

            // add row data bound event
            this._CertificationsGrid.RowDataBound += new GridViewRowEventHandler(this._CertificationsGrid_RowDataBound);

            // attach to update panel
            this.CertificationsGridUpdatePanel = new UpdatePanel();
            this.CertificationsGridUpdatePanel.ID = "CertificationsGridUpdatePanel";
            this.CertificationsGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this.CertificationsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._CertificationsGrid);

            // attach to container
            this.UserCertificationsPropertiesContainer.Controls.Add(this.CertificationsGridUpdatePanel);
        }
        #endregion

        #region _CertificationsGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the certifications grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _CertificationsGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idCertification = Convert.ToInt32(rowView["idCertification"]);
                int idCertificationToUserLink = Convert.ToInt32(rowView["idCertificationToUserLink"]);

                // AVATAR, CERTIFICATION

                string certificationName = Server.HtmlEncode(rowView["title"].ToString());
                bool isResetOn = Convert.ToBoolean(rowView["isResetOn"]);
                bool isActivityOn = Convert.ToBoolean(rowView["isActivityOn"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = certificationName;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // label
                Label labelLabelWrapper = new Label();
                labelLabelWrapper.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(labelLabelWrapper);

                Literal labelLabel = new Literal();
                labelLabel.Text = certificationName;
                labelLabelWrapper.Controls.Add(labelLabel);

                // OPTIONS COLUMN                

                // reset
                if (isResetOn)
                {
                    Label resetSpan = new Label();
                    resetSpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(resetSpan);

                    HyperLink resetLink = new HyperLink();
                    resetLink.NavigateUrl = "javascript: void(0);";
                    resetLink.Attributes.Add("onclick", "ResetCertification(" + idCertificationToUserLink.ToString() + ");");
                    resetSpan.Controls.Add(resetLink);

                    Image resetIcon = new Image();
                    resetIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RESET, ImageFiles.EXT_PNG);
                    resetIcon.AlternateText = _GlobalResources.Reset;
                    resetIcon.ToolTip = _GlobalResources.Reset;
                    resetLink.Controls.Add(resetIcon);
                }
                else
                {
                    Label resetSpan = new Label();
                    resetSpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(resetSpan);                    

                    Image resetIcon = new Image();
                    resetIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RESET, ImageFiles.EXT_PNG);
                    resetIcon.CssClass = "DimIcon";
                    resetIcon.AlternateText = _GlobalResources.ResetDisabled;
                    resetIcon.ToolTip = _GlobalResources.ResetDisabled;
                    resetSpan.Controls.Add(resetIcon);
                }

                // activity
                if (isActivityOn)
                {
                    Label activitySpan = new Label();
                    activitySpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(activitySpan);

                    HyperLink activityLink = new HyperLink();
                    activityLink.NavigateUrl = "View.aspx?uid=" + this._UserObject.Id.ToString() + "&cid=" + idCertification.ToString() + "&culid=" + idCertificationToUserLink.ToString();
                    activitySpan.Controls.Add(activityLink);

                    Image activityIcon = new Image();
                    activityIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ACTIVITY, ImageFiles.EXT_PNG);
                    activityIcon.AlternateText = _GlobalResources.Activity;
                    activityIcon.ToolTip = _GlobalResources.Activity;
                    activityLink.Controls.Add(activityIcon);
                }
            }
        }
        #endregion

        #region _BuildCertificationsGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildCertificationsGridActionsPanel()
        {            
            this.CertificationsGridActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._CertificationsDeleteButton = new LinkButton();
            this._CertificationsDeleteButton.ID = "CertificationsGridDeleteButton";
            this._CertificationsDeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._CertificationsDeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedCertification_s;
            this._CertificationsDeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.CertificationsGridActionsPanel.Controls.Add(this._CertificationsDeleteButton);
        }
        #endregion

        #region _BuildCertificationGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildCertificationGridActionsModal()
        {
            this._CertificationGridConfirmAction = new ModalPopup("GridConfirmActionModal");

            // set modal properties
            this._CertificationGridConfirmAction.Type = ModalPopupType.Confirm;
            this._CertificationGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._CertificationGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._CertificationGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedCertification_s;
            this._CertificationGridConfirmAction.TargetControlID = this._CertificationsDeleteButton.ID;
            this._CertificationGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._CertificationsDeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseCertification_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._CertificationGridConfirmAction.AddControlToBody(body1Wrapper);

            // attach to container
            this.UserCertificationsPropertiesContainer.Controls.Add(this._CertificationGridConfirmAction);
        }
        #endregion

        #region _BuildCertificationResetConfirmModal
        /// <summary>
        /// Builds the confirmation modal for the reset action performed on Grid data.
        /// </summary>
        private void _BuildCertificationResetConfirmModal()
        {
            this._HiddenResetButton = new HiddenField();
            this._HiddenResetButton.ID = "HiddenResetButton";
            this.CertificationsGridActionsPanel.Controls.Add(this._HiddenResetButton);

            this._ResetCertificationId = new HiddenField();
            this._ResetCertificationId.ID = "ResetCertificationId";
            this._ResetCertificationId.Value = "0";
            this.CertificationsGridActionsPanel.Controls.Add(this._ResetCertificationId);

            this._CertificationResetConfirmAction = new ModalPopup("ResetConfirmActionModal");

            // set modal properties
            this._CertificationResetConfirmAction.Type = ModalPopupType.Confirm;
            this._CertificationResetConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_RESET, ImageFiles.EXT_PNG);
            this._CertificationResetConfirmAction.HeaderIconAlt = _GlobalResources.Reset;
            this._CertificationResetConfirmAction.HeaderText = _GlobalResources.ResetCertification;
            this._CertificationResetConfirmAction.TargetControlID = this._HiddenResetButton.ClientID;
            this._CertificationResetConfirmAction.SubmitButton.Command += new CommandEventHandler(this._ResetButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmResetActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToResetThisCertification;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._CertificationResetConfirmAction.AddControlToBody(body1Wrapper);

            // attach to container
            this.UserCertificationsPropertiesContainer.Controls.Add(this._CertificationResetConfirmAction);
        }
        #endregion        

        #region _ResetButton_Command
        /// <summary>
        /// Resets a certification.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ResetButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int idCertificationToUserLink;

                if (int.TryParse(this._ResetCertificationId.Value, out idCertificationToUserLink))
                {
                    if (idCertificationToUserLink > 0)
                    {                        
                        // reset the certification
                        Certification.ResetForUser(idCertificationToUserLink);

                        // display the success message
                        this.DisplayFeedback(_GlobalResources.TheSelectedCertificationHasBeenResetSuccessfully, false);

                        // rebind the grid
                        this._CertificationsGrid.BindData();
                    }
                    else
                    { throw new AsentiaException(_GlobalResources.InvalidCertificationToUserID); }
                }
                else
                { throw new AsentiaException(_GlobalResources.InvalidCertificationToUserID); }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this._CertificationsGrid.BindData();
                this.CertificationsGridUpdatePanel.Update();
            }
        }
        #endregion        

        #region _CertificationsDeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _CertificationsDeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._CertificationsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._CertificationsGrid.Rows[i].FindControl(this._CertificationsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.Certification.DeleteForUser(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedCertification_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoCertification_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this._CertificationsGrid.BindData();
                this.CertificationsGridUpdatePanel.Update();
            }
        }
        #endregion

        #region _BuildJoinCertificationsToUserModal
        /// <summary>
        /// Builds the modal for joining certifications to the user.
        /// </summary>
        private void _BuildJoinCertificationsToUserModal(string targetControlId)
        {
            // set modal properties
            this._CertificationJoinModal = new ModalPopup("CertificationJoinModal");
            this._CertificationJoinModal.Type = ModalPopupType.Form;
            this._CertificationJoinModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
            this._CertificationJoinModal.HeaderIconAlt = _GlobalResources.JoinUserToCertification;
            this._CertificationJoinModal.HeaderText = _GlobalResources.JoinUserToCertification;
            this._CertificationJoinModal.TargetControlID = targetControlId;
            this._CertificationJoinModal.SubmitButton.Command += new CommandEventHandler(this._JoinUserToCertificationSubmit_Command);
            this._CertificationJoinModal.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the groups listing
            this._AddEligibleCertifications = new DynamicListBox("AddEligibleCertifications");
            this._AddEligibleCertifications.IsMultipleSelect = false;
            this._AddEligibleCertifications.NoRecordsFoundMessage = _GlobalResources.NoCertificationsFound;
            this._AddEligibleCertifications.SearchButton.Command += new CommandEventHandler(this._SearchJoinCertificationsButton_Command);
            this._AddEligibleCertifications.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchJoinCertificationsButton_Command);
            this._AddEligibleCertifications.ListBoxControl.DataSource = this._CertificationsNotForUser;
            this._AddEligibleCertifications.ListBoxControl.DataTextField = "title";
            this._AddEligibleCertifications.ListBoxControl.DataValueField = "idCertification";
            this._AddEligibleCertifications.ListBoxControl.DataBind();

            // add controls to body
            this._CertificationJoinModal.AddControlToBody(this._AddEligibleCertifications);

            // add to container
            this.UserCertificationsPropertiesContainer.Controls.Add(this._CertificationJoinModal);
        }
        #endregion

        #region _SearchJoinCertificationsButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Join Certifications" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchJoinCertificationsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._CertificationJoinModal.ClearFeedback();

            // clear the listbox control
            this._AddEligibleCertifications.ListBoxControl.Items.Clear();

            // do the search
            this._CertificationsNotForUser = Asentia.LMS.Library.Certification.IdsAndNamesForUserSelectList(this._UserObject.Id, this._AddEligibleCertifications.SearchTextBox.Text);

            this._SelectEligibleCertificationsDataBind();
        }
        #endregion

        #region _ClearSearchJoinCertificationsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Join Certifications" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchJoinCertificationsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._CertificationJoinModal.ClearFeedback();

            // clear the listbox control and search text box
            this._AddEligibleCertifications.ListBoxControl.Items.Clear();
            this._AddEligibleCertifications.SearchTextBox.Text = "";

            // clear the search
            this._CertificationsNotForUser = Asentia.LMS.Library.Certification.IdsAndNamesForUserSelectList(this._UserObject.Id, null);

            this._SelectEligibleCertificationsDataBind();
        }
        #endregion

        #region _SelectEligibleCertificationsDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectEligibleCertificationsDataBind()
        {
            this._AddEligibleCertifications.ListBoxControl.DataSource = this._CertificationsNotForUser;
            this._AddEligibleCertifications.ListBoxControl.DataTextField = "title";
            this._AddEligibleCertifications.ListBoxControl.DataValueField = "idCertification";
            this._AddEligibleCertifications.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._AddEligibleCertifications.ListBoxControl.Items.Count == 0)
            {
                this._CertificationJoinModal.SubmitButton.Enabled = false;
                this._CertificationJoinModal.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._CertificationJoinModal.SubmitButton.Enabled = true;
                this._CertificationJoinModal.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _JoinUserToCertificationSubmit_Command
        /// <summary>
        /// Click event handler for Join Certification To User modal submit button.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _JoinUserToCertificationSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get selected groups from the list box
                List<string> selectedCertifications = this._AddEligibleCertifications.GetSelectedValues();

                // throw exception if no users selected
                if (selectedCertifications.Count <= 0)
                { throw new AsentiaException(_GlobalResources.NoCertificationSelected); }

                // get the certification id to add
                int idCertification = 0;

                foreach (string selectedCertification in selectedCertifications)
                { idCertification = Convert.ToInt32(selectedCertification); }

                // join the selected certification to the user
                Certification.JoinUser(idCertification, this._UserObject.Id);                

                // remove the selected groups from the list box so they cannot be re-selected
                this._AddEligibleCertifications.RemoveSelectedItems();

                // rebind the grid and refresh the update panel
                this._CertificationsGrid.BindData();
                this.CertificationsGridUpdatePanel.Update();

                // display success feedback
                this._CertificationJoinModal.DisplayFeedback(_GlobalResources.UserJoinedToCertificationSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CertificationJoinModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CertificationJoinModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CertificationJoinModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CertificationJoinModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CertificationJoinModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion
    }
}