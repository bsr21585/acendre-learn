﻿function ResetCertification(idCertificationToUserLink) {
    var resetCertificationIdHiddenField = $("#ResetCertificationId");
    resetCertificationIdHiddenField.val(idCertificationToUserLink);

    $find('ResetConfirmActionModalModalPopupExtender').show();
}

function JoinToCertification() {
    $("#JoinToCertificationsHiddenButton").click();
}