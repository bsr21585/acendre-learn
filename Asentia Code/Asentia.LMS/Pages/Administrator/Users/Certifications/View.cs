﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Users.Certifications
{    
    public class View : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CertificationFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel CertificationFormWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel CertificationFormContainer;
        public Panel CertificationTabPanelsContainer;        
        #endregion

        #region Private Properties
        // objects
        private User _UserObject;
        private Certification _CertificationObject;
        private int _IdCertificationToUserLink;

        // user certification details
        private DateTime? _InitialAwardDate = null;
        private int _CertificationTrack = 0;
        private DateTime? _LastExpirationDate = null;
        private DateTime? _CurrentExpirationDate = null;
        private string _CertificationIdentifier;

        // update panels
        private UpdatePanel _CertificationPropertiesTabPanel;
        private UpdatePanel _CertificationInitialTabPanel;
        private UpdatePanel _CertificationRenewalTabPanel;        

        // task proctoring modal
        private ModalPopup _TaskProctoringModal;
        private Button _TaskProctoringModalLaunchButton;
        private Button _TaskProctoringModalLoadButton;

        // modify certification initial award date modal
        private ModalPopup _ModifyCertificationInitialAwardDateModal;
        private Button _ModifyCertificationInitialAwardDateModalLaunchButton;
        private Button _ModifyCertificationInitialAwardDateModalLoadButton;

        // modify certification current expiration date modal
        private ModalPopup _ModifyCertificationCurrentExpirationDateModal;
        private Button _ModifyCertificationCurrentExpirationDateModalLaunchButton;
        private Button _ModifyCertificationCurrentExpirationDateModalLoadButton;

        // modify certification last expiration date modal
        private ModalPopup _ModifyCertificationLastExpirationDateModal;
        private Button _ModifyCertificationLastExpirationDateModalLaunchButton;
        private Button _ModifyCertificationLastExpirationDateModalLoadButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(View), "Asentia.LMS.Pages.Administrator.Users.Certifications.View.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the certification and user objects
            this._GetCertificationAndUserObjects();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/users/certifications/View.css");

            // initialize the administrator menu
            this.InitializeAdminMenu();            

            // synchronize certification requirement task data
            Certification.SynchronizeTaskRequirementData(this._IdCertificationToUserLink);

            // get user certification details
            this._GetUserCertificationDetails();

            // build the controls for the page
            this._BuildControls();            
        }
        #endregion

        #region _GetCertificationAndUserObjects
        /// <summary>
        /// Gets the user and certification objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCertificationAndUserObjects()
        {
            // get the id querystring parameter
            int qsUId = this.QueryStringInt("uid", 0);
            int vsUId = this.ViewStateInt(this.ViewState, "uid", 0);
            int qsCId = this.QueryStringInt("cid", 0);
            int vsCId = this.ViewStateInt(this.ViewState, "cid", 0);
            int qsCULId = this.QueryStringInt("culid", 0);
            int vsCULId = this.ViewStateInt(this.ViewState, "culid", 0);

            // get user object - user object MUST be specified and exist for this page to load
            if (qsUId > 0 || vsUId > 0)
            {
                int uid = 0;

                if (qsUId > 0)
                { uid = qsUId; }

                if (vsUId > 0)
                { uid = vsUId; }

                try
                {
                    if (uid > 0)
                    { this._UserObject = new User(uid); }
                }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            { Response.Redirect("~/administrator/users"); }

            // get certificaiton object - certification object MUST be specified and exist for this page to load
            if (qsCId > 0 || vsCId > 0)
            {
                int id = 0;

                if (qsCId > 0)
                { id = qsCId; }

                if (vsCId > 0)
                { id = vsCId; }

                try
                {
                    if (id > 0)
                    { this._CertificationObject = new Certification(id); }
                }
                catch
                { Response.Redirect("~/administrator/users/certifications/?uid=" + this._UserObject.Id.ToString()); }
            }
            else
            { Response.Redirect("~/administrator/users/certifications/?uid=" + this._UserObject.Id.ToString()); }

            // get certification to user link id - MUST be specified for this page to load
            if (qsCULId > 0 || vsCULId > 0)
            {
                int id = 0;

                if (qsCULId > 0)
                { id = qsCULId; }

                if (vsCULId > 0)
                { id = vsCULId; }

                this._IdCertificationToUserLink = id;
            }
            else
            { Response.Redirect("~/administrator/users/certifications/?uid=" + this._UserObject.Id.ToString()); }
        }
        #endregion

        #region _GetUserCertificationDetails
        private void _GetUserCertificationDetails()
        {
            DataTable userCertificationDetails = Certification.GetUserCertificationDetails(this._IdCertificationToUserLink);

            foreach (DataRow userCertificationDetail in userCertificationDetails.Rows)
            {
                if (userCertificationDetail["initialAwardDate"] != DBNull.Value)
                { this._InitialAwardDate = Convert.ToDateTime(userCertificationDetail["initialAwardDate"]); }

                if (userCertificationDetail["certificationTrack"] != DBNull.Value)
                { this._CertificationTrack = Convert.ToInt32(userCertificationDetail["certificationTrack"]); }

                if (userCertificationDetail["lastExpirationDate"] != DBNull.Value)
                { this._LastExpirationDate = Convert.ToDateTime(userCertificationDetail["lastExpirationDate"]); }

                if (userCertificationDetail["currentExpirationDate"] != DBNull.Value)
                { this._CurrentExpirationDate = Convert.ToDateTime(userCertificationDetail["currentExpirationDate"]); }

                if (userCertificationDetail["certificationId"] != DBNull.Value)
                { this._CertificationIdentifier = userCertificationDetail["certificationId"].ToString(); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            // evaluate for breadcrumb and page title information
            string certificationTitleInInterfaceLanguage = this._CertificationObject.Title;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Certification.LanguageSpecificProperty certificationLanguageSpecificProperty in this._CertificationObject.LanguageSpecificProperties)
                {
                    if (certificationLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { certificationTitleInInterfaceLanguage = certificationLanguageSpecificProperty.Title; }
                }
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(userDisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Certifications, "/administrator/users/certifications/?uid=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(certificationTitleInInterfaceLanguage + ": " + _GlobalResources.Activity));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, certificationTitleInInterfaceLanguage + ": " + _GlobalResources.Activity, ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to create the page Breadcrumb, user action menu controls and other controls on page.
        /// </summary>
        private void _BuildControls()
        {
            // instansiate task proctoring controls
            this._TaskProctoringModalLaunchButton = new Button();
            this._TaskProctoringModalLaunchButton.ID = "TaskProctoringModalLaunchButton";
            this._TaskProctoringModalLaunchButton.Style.Add("display", "none");

            this._TaskProctoringModalLoadButton = new Button();
            this._TaskProctoringModalLoadButton.ID = "TaskProctoringModalLoadButton";
            this._TaskProctoringModalLoadButton.Style.Add("display", "none");
            this._TaskProctoringModalLoadButton.Click += this._LoadTaskProctoringModalContent;

            // attach the modal launch controls
            this.CertificationFormWrapperContainer.Controls.Add(this._TaskProctoringModalLaunchButton);

            // instansiate certification initial award date controls
            this._ModifyCertificationInitialAwardDateModalLaunchButton = new Button();
            this._ModifyCertificationInitialAwardDateModalLaunchButton.ID = "ModifyCertificationInitialAwardDateModalLaunchButton";
            this._ModifyCertificationInitialAwardDateModalLaunchButton.Style.Add("display", "none");

            this._ModifyCertificationInitialAwardDateModalLoadButton = new Button();
            this._ModifyCertificationInitialAwardDateModalLoadButton.ID = "ModifyCertificationInitialAwardDateModalLoadButton";
            this._ModifyCertificationInitialAwardDateModalLoadButton.Style.Add("display", "none");
            this._ModifyCertificationInitialAwardDateModalLoadButton.Click += this._LoadModifyCertificationInitialAwardDateModalContent;

            // attach the modal launch controls
            this.CertificationFormWrapperContainer.Controls.Add(this._ModifyCertificationInitialAwardDateModalLaunchButton);

            // instansiate certification current expiration date controls
            this._ModifyCertificationCurrentExpirationDateModalLaunchButton = new Button();
            this._ModifyCertificationCurrentExpirationDateModalLaunchButton.ID = "ModifyCertificationCurrentExpirationDateModalLaunchButton";
            this._ModifyCertificationCurrentExpirationDateModalLaunchButton.Style.Add("display", "none");

            this._ModifyCertificationCurrentExpirationDateModalLoadButton = new Button();
            this._ModifyCertificationCurrentExpirationDateModalLoadButton.ID = "ModifyCertificationCurrentExpirationDateModalLoadButton";
            this._ModifyCertificationCurrentExpirationDateModalLoadButton.Style.Add("display", "none");
            this._ModifyCertificationCurrentExpirationDateModalLoadButton.Click += this._LoadModifyCertificationCurrentExpirationDateModalContent;

            // attach the modal launch controls
            this.CertificationFormWrapperContainer.Controls.Add(this._ModifyCertificationCurrentExpirationDateModalLaunchButton);

            // instansiate certification last expiration date controls
            this._ModifyCertificationLastExpirationDateModalLaunchButton = new Button();
            this._ModifyCertificationLastExpirationDateModalLaunchButton.ID = "ModifyCertificationLastExpirationDateModalLaunchButton";
            this._ModifyCertificationLastExpirationDateModalLaunchButton.Style.Add("display", "none");

            this._ModifyCertificationLastExpirationDateModalLoadButton = new Button();
            this._ModifyCertificationLastExpirationDateModalLoadButton.ID = "ModifyCertificationLastExpirationDateModalLoadButton";
            this._ModifyCertificationLastExpirationDateModalLoadButton.Style.Add("display", "none");
            this._ModifyCertificationLastExpirationDateModalLoadButton.Click += this._LoadModifyCertificationLastExpirationDateModalContent;

            // attach the modal launch controls
            this.CertificationFormWrapperContainer.Controls.Add(this._ModifyCertificationLastExpirationDateModalLaunchButton);

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.CertificationFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CertificationFormWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu
            this.UserObjectMenuContainer.Controls.Clear();
            
            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Certifications;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.TheUsersCurrentCertificationActivityIsDisplayedBelow, true);

            // build the certification form
            this._BuildCertificationForm();            

            // build modal for task proctoring
            this._BuildTaskProctoringModal();      
            
            // build modal for certification initial award date
            this._BuildModifyCertificationInitialAwardDateModal();

            // build modal for certification current expiration date
            this._BuildModifyCertificationCurrentExpirationDateModal();

            // build modal for certification last expiration date
            this._BuildModifyCertificationLastExpirationDateModal();
        }
        #endregion

        #region _BuildCertificationForm
        private void _BuildCertificationForm()
        {
            // clear certification from container
            this.CertificationFormContainer.Controls.Clear();

            // build the tabs
            this._BuildCertificationFormTabs();
            
            this.CertificationTabPanelsContainer = new Panel();
            this.CertificationTabPanelsContainer.ID = "Certification_TabPanelsContainer";
            this.CertificationTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.CertificationFormContainer.Controls.Add(this.CertificationTabPanelsContainer);

            // build the tab panels
            this._BuildCertificationPropertiesTabPanel();
            this._BuildCertificationInitialTabPanel();
            this._BuildCertificationRenewalTabPanel();
        }
        #endregion

        #region _BuildCertificationFormTabs
        /// <summary>
        /// Builds the tabs for the certification.
        /// </summary>
        private void _BuildCertificationFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("Initial", _GlobalResources.Initial));
            tabs.Enqueue(new KeyValuePair<string, string>("Renewal", _GlobalResources.Renewal));            

            // build and attach the tabs
            this.CertificationFormContainer.Controls.Add(AsentiaPage.BuildTabListPanel("Certification", tabs));
        }
        #endregion

        #region _BuildCertificationPropertiesTabPanel
        /// <summary>
        /// Builds the certification properties tab panel.
        /// </summary>
        private void _BuildCertificationPropertiesTabPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            this._CertificationPropertiesTabPanel = new UpdatePanel();
            this._CertificationPropertiesTabPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._CertificationPropertiesTabPanel.ID = "Certification_" + "Properties" + "_TabPanel";
            this._CertificationPropertiesTabPanel.Attributes.Add("class", "TabPanelContainer");
            this._CertificationPropertiesTabPanel.Attributes.Add("style", "display: block;");

            string certificationTitleInLanguage = this._CertificationObject.Title;
            string certificationShortDescriptionInLanguage = this._CertificationObject.ShortDescription;

            foreach (Certification.LanguageSpecificProperty cLSP in this._CertificationObject.LanguageSpecificProperties)
            {
                if (cLSP.LangString == AsentiaSessionState.UserCulture)
                {
                    certificationTitleInLanguage = cLSP.Title;
                    certificationShortDescriptionInLanguage = cLSP.ShortDescription;
                    break;
                }
            }

            // certification title field            
            Literal certificationTitle = new Literal();
            certificationTitle.Text = certificationTitleInLanguage;

            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildFormField("CertificationTitle",
                                                                                        _GlobalResources.Certification,
                                                                                        null,
                                                                                        certificationTitle,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // certification description field            
            Literal certificationDescription = new Literal();
            certificationDescription.Text = certificationShortDescriptionInLanguage;

            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildFormField("CertificationDescription",
                                                                                        _GlobalResources.Description,
                                                                                        null,
                                                                                        certificationDescription,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // certification track field   
            Panel certificationTrackPanel = new Panel();
            certificationTrackPanel.ID = "CertificationTrackPanel";

            Literal certificationTrack = new Literal();
            certificationTrackPanel.Controls.Add(certificationTrack);

            if (this._CertificationTrack == 0)
            { 
                certificationTrack.Text = _GlobalResources.Initial;
            }
            else
            {
                certificationTrack.Text = _GlobalResources.Renewal;
            }

            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildFormField("CertificationTrack",
                                                                                        _GlobalResources.CertificationTrack,
                                                                                        null,
                                                                                        certificationTrackPanel,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // initial award date         
            HtmlGenericControl initialAwardDateSpan = new HtmlGenericControl("Span");
            initialAwardDateSpan.ID = "InitialAwardDateSpan";

            Literal initialAwardDate = new Literal();
            initialAwardDateSpan.Controls.Add(initialAwardDate);

            if (this._InitialAwardDate != null)
            {
                DateTime initialAwardDateLocal = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._InitialAwardDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                initialAwardDate.Text = initialAwardDateLocal.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
            }
            else
            { initialAwardDate.Text = _GlobalResources.None; }

            Image modifyInitialAwardDateIcon = new Image();
            modifyInitialAwardDateIcon.ID = "ModifyInitialAwardDateIcon";
            modifyInitialAwardDateIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                   ImageFiles.EXT_PNG);
            modifyInitialAwardDateIcon.CssClass = "XSmallIcon";
            modifyInitialAwardDateIcon.Attributes.Add("onClick", "LoadCertificationInitialAwardDateModalContent(); return false;");

            List<Control> initialAwardDateControls = new List<Control>();
            initialAwardDateControls.Add(initialAwardDateSpan);
            initialAwardDateControls.Add(modifyInitialAwardDateIcon);


            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("InitialAwardDate_Field",
                                                                                        _GlobalResources.InitialAwardDate,
                                                                                        initialAwardDateControls,
                                                                                        false,
                                                                                        false));


            // last expiration date          
            HtmlGenericControl lastExpirationDateSpan = new HtmlGenericControl("Span");
            lastExpirationDateSpan.ID = "LastExpirationDateSpan";

            Literal lastExpirationDate = new Literal();
            lastExpirationDateSpan.Controls.Add(lastExpirationDate);

            if (this._LastExpirationDate != null)
            {
                DateTime lastExpirationDateLocal = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._LastExpirationDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                lastExpirationDate.Text = lastExpirationDateLocal.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
            }
            else
            { lastExpirationDate.Text = _GlobalResources.None; }

            Image modifyLastExpirationDateIcon = new Image();
            modifyLastExpirationDateIcon.ID = "ModifyLastExpirationDateIcon";
            modifyLastExpirationDateIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                   ImageFiles.EXT_PNG);
            modifyLastExpirationDateIcon.CssClass = "XSmallIcon";
            modifyLastExpirationDateIcon.Attributes.Add("onClick", "LoadCertificationLastExpirationDateModalContent(); return false;");

            List<Control> lastExpirationDateControls = new List<Control>();
            lastExpirationDateControls.Add(lastExpirationDateSpan);
            lastExpirationDateControls.Add(modifyLastExpirationDateIcon);


            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LastExpirationDate_Field",
                                                                                        _GlobalResources.LastExpirationDate,
                                                                                        lastExpirationDateControls,
                                                                                        false,
                                                                                        false));

            // current expiration date          
            HtmlGenericControl currentExpirationDateSpan = new HtmlGenericControl("Span");
            currentExpirationDateSpan.ID = "CurrentExpirationDateSpan";

            Literal currentExpirationDate = new Literal();
            currentExpirationDateSpan.Controls.Add(currentExpirationDate);

            if (this._CurrentExpirationDate != null)
            {
                DateTime currentExpirationDateLocal = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._CurrentExpirationDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                currentExpirationDate.Text = currentExpirationDateLocal.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
            }
            else
            { currentExpirationDate.Text = _GlobalResources.None; }

            Image modifyCurrentExpirationDateIcon = new Image();
            modifyCurrentExpirationDateIcon.ID = "ModifyCurrentExpirationDateIcon";
            modifyCurrentExpirationDateIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                   ImageFiles.EXT_PNG);
            modifyCurrentExpirationDateIcon.CssClass = "XSmallIcon";
            modifyCurrentExpirationDateIcon.Attributes.Add("onClick", "LoadCertificationCurrentExpirationDateModalContent(); return false;");

            List<Control> currentExpirationDateControls = new List<Control>();
            currentExpirationDateControls.Add(currentExpirationDateSpan);
            currentExpirationDateControls.Add(modifyCurrentExpirationDateIcon);


            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CurrentExpirationDate_Field",
                                                                                        _GlobalResources.CurrentExpirationDate,
                                                                                        currentExpirationDateControls,
                                                                                        false,
                                                                                        false));

            // attach panel to container
            this.CertificationTabPanelsContainer.Controls.Add(this._CertificationPropertiesTabPanel);
        }
        #endregion

        #region _BuildCertificationInitialTabPanel
        /// <summary>
        /// Builds the certification initial tab panel.
        /// </summary>
        private void _BuildCertificationInitialTabPanel()
        {   
            // declare the panel
            this._CertificationInitialTabPanel = new UpdatePanel();
            this._CertificationInitialTabPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._CertificationInitialTabPanel.ID = "Certification_" + "Initial" + "_TabPanel";
            this._CertificationInitialTabPanel.Attributes.Add("class", "TabPanelContainer");
            this._CertificationInitialTabPanel.Attributes.Add("style", "display: none;");

            // build the panel content
            this._BuildCertificationRequirementTabPanelContent(true, this._CertificationInitialTabPanel);

            // attach panel to container
            this.CertificationTabPanelsContainer.Controls.Add(this._CertificationInitialTabPanel);
        }
        #endregion        

        #region _BuildCertificationRenewalTabPanel
        /// <summary>
        /// Builds the certification renewal tab panel.
        /// </summary>
        private void _BuildCertificationRenewalTabPanel()
        {
            this._CertificationRenewalTabPanel = new UpdatePanel();
            this._CertificationRenewalTabPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._CertificationRenewalTabPanel.ID = "Certification_" + "Renewal" + "_TabPanel";
            this._CertificationRenewalTabPanel.Attributes.Add("class", "TabPanelContainer");
            this._CertificationRenewalTabPanel.Attributes.Add("style", "display: none;");

            // build the panel content
            this._BuildCertificationRequirementTabPanelContent(false, this._CertificationRenewalTabPanel);

            // attach panel to container
            this.CertificationTabPanelsContainer.Controls.Add(this._CertificationRenewalTabPanel);
        }
        #endregion

        #region _BuildCertificationRequirementTabPanelContent
        private void _BuildCertificationRequirementTabPanelContent(bool isInitialRequirements, UpdatePanel tabUpdatePanelContainer)
        {
            // clear the controls
            tabUpdatePanelContainer.ContentTemplateContainer.Controls.Clear();

            // begin building
            DataTable requirementStatuses = Certification.GetRequirementStatusesForUser(this._UserObject.Id, this._IdCertificationToUserLink, isInitialRequirements);
            DataTable certificationModules = this._CertificationObject.GetModuleIds();
            bool isCertificationComplete = false;

            // INITIAL REQUIREMENTS STATUS
            Panel certificationTitleContainer = new Panel();
            certificationTitleContainer.ID = tabUpdatePanelContainer.ID + "_CertificationTitleContainer";
            certificationTitleContainer.CssClass = "CertificationTitleContainer";

            foreach (DataRow row in requirementStatuses.Rows)
            {
                if (row["objectType"].ToString() == "certification" && Convert.ToInt32(row["idObject"]) == this._CertificationObject.Id)
                {
                    isCertificationComplete = Convert.ToBoolean(row["isRequirementMet"]);
                    break;
                }
            }

            Label certificationCompletionStatusImageContainer = new Label();
            certificationCompletionStatusImageContainer.Controls.Add(this._GetCompletionStatusImage(isCertificationComplete, "SmallIcon"));
            certificationTitleContainer.Controls.Add(certificationCompletionStatusImageContainer);

            Label certificationTitle = new Label();
            
            if (isInitialRequirements)
            { certificationTitle.Text = _GlobalResources.InitialRequirements; }
            else
            { certificationTitle.Text = _GlobalResources.RenewalRequirements; }

            certificationTitleContainer.Controls.Add(certificationTitle);
            tabUpdatePanelContainer.ContentTemplateContainer.Controls.Add(certificationTitleContainer);

            // CERTIFICATION MODULES            
            foreach (DataRow moduleRow in certificationModules.Rows)
            {
                int idCertificationModule = Convert.ToInt32(moduleRow["idCertificationModule"]);
                CertificationModule certificationModuleObject = new CertificationModule(idCertificationModule);
                bool isCertificationModuleComplete = false;

                if ((certificationModuleObject.IsInitialRequirement && isInitialRequirements) || (!certificationModuleObject.IsInitialRequirement && !isInitialRequirements))
                {
                    // container
                    Panel certificationModuleContainer = new Panel();
                    certificationModuleContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleContainer_" + certificationModuleObject.Id.ToString();
                    certificationModuleContainer.CssClass = "CertificationModuleContainer";

                    // language-specific properties
                    string certificationModuleTitleInLanguage = certificationModuleObject.Title;
                    string certificationModuleShortDescriptionInLanguage = certificationModuleObject.ShortDescription;

                    foreach (CertificationModule.LanguageSpecificProperty certModuleLSP in certificationModuleObject.LanguageSpecificProperties)
                    {
                        if (certModuleLSP.LangString == AsentiaSessionState.UserCulture)
                        {
                            certificationModuleTitleInLanguage = certModuleLSP.Title;
                            certificationModuleShortDescriptionInLanguage = certModuleLSP.ShortDescription;
                            break;
                        }
                    }

                    // title
                    Panel certificationModuleTitleContainer = new Panel();
                    certificationModuleTitleContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleTitleContainer_" + certificationModuleObject.Id.ToString();
                    certificationModuleTitleContainer.CssClass = "CertificationModuleTitleContainer";

                    foreach (DataRow row in requirementStatuses.Rows)
                    {
                        if (row["objectType"].ToString() == "module" && Convert.ToInt32(row["idObject"]) == certificationModuleObject.Id)
                        {
                            isCertificationModuleComplete = Convert.ToBoolean(row["isRequirementMet"]);
                            break;
                        }
                    }

                    Label certificationModuleCompletionStatusImageContainer = new Label();
                    certificationModuleCompletionStatusImageContainer.Controls.Add(this._GetCompletionStatusImage(isCertificationModuleComplete, "SmallIcon"));
                    certificationModuleTitleContainer.Controls.Add(certificationModuleCompletionStatusImageContainer);

                    Label certificationModuleTitle = new Label();                    
                    certificationModuleTitle.Text = _GlobalResources.Unit + ": " + certificationModuleTitleInLanguage;
                    certificationModuleTitle.CssClass = "CertificationModuleTitle";
                    certificationModuleTitleContainer.Controls.Add(certificationModuleTitle);

                    certificationModuleContainer.Controls.Add(certificationModuleTitleContainer);

                    // short description
                    Panel certificationModuleShortDescriptionContainer = new Panel();
                    certificationModuleShortDescriptionContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleShortDescriptionContainer_" + certificationModuleObject.Id.ToString();
                    certificationModuleShortDescriptionContainer.CssClass = "CertificationModuleShortDescriptionContainer";

                    Label certificationModuleShortDescription = new Label();
                    certificationModuleShortDescription.Text = certificationModuleShortDescriptionInLanguage;
                    certificationModuleShortDescriptionContainer.Controls.Add(certificationModuleShortDescription);

                    certificationModuleTitleContainer.Controls.Add(certificationModuleShortDescriptionContainer); // put the short description in the title container
                    
                    // CERTIFICATION REQUIREMENT SETS
                    DataTable certificationRequirementSets = certificationModuleObject.GetRequirementSetIds();
                    int moduleRequirementSetIndex = 0;

                    foreach (DataRow requirementSetRow in certificationRequirementSets.Rows)
                    {
                        int idCertificationModuleRequirementSet = Convert.ToInt32(requirementSetRow["idCertificationModuleRequirementSet"]);
                        CertificationModuleRequirementSet certificationModuleRequirementSetObject = new CertificationModuleRequirementSet(idCertificationModuleRequirementSet);
                        bool isCertificationModuleRequirementSetComplete = false;
                        moduleRequirementSetIndex++;

                        // container
                        Panel certificationModuleRequirementSetContainer = new Panel();
                        certificationModuleRequirementSetContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleRequirementSetContainer_" + certificationModuleRequirementSetObject.Id.ToString();
                        certificationModuleRequirementSetContainer.CssClass = "CertificationModuleRequirementSetContainer";

                        // language-specific properties
                        string certificationModuleRequirementSetLabelInLanguage = certificationModuleRequirementSetObject.Label;

                        foreach (CertificationModuleRequirementSet.LanguageSpecificProperty certModuleRSLSP in certificationModuleRequirementSetObject.LanguageSpecificProperties)
                        {
                            if (certModuleRSLSP.LangString == AsentiaSessionState.UserCulture)
                            {
                                certificationModuleRequirementSetLabelInLanguage = certModuleRSLSP.Label;                                
                                break;
                            }
                        }

                        // label
                        Panel certificationModuleRequirementSetLabelContainer = new Panel();
                        certificationModuleRequirementSetLabelContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleRequirementSetLabelContainer_" + certificationModuleRequirementSetObject.Id.ToString();
                        certificationModuleRequirementSetLabelContainer.CssClass = "CertificationModuleRequirementSetLabelContainer";

                        foreach (DataRow row in requirementStatuses.Rows)
                        {
                            if (row["objectType"].ToString() == "requirementset" && Convert.ToInt32(row["idObject"]) == certificationModuleRequirementSetObject.Id)
                            {
                                isCertificationModuleRequirementSetComplete = Convert.ToBoolean(row["isRequirementMet"]);
                                break;
                            }
                        }

                        Label certificationModuleRequirementSetCompletionStatusImageContainer = new Label();
                        certificationModuleRequirementSetCompletionStatusImageContainer.Controls.Add(this._GetCompletionStatusImage(isCertificationModuleRequirementSetComplete, "SmallIcon"));
                        certificationModuleRequirementSetLabelContainer.Controls.Add(certificationModuleRequirementSetCompletionStatusImageContainer);

                        Label certificationModuleRequirementSetLabel = new Label();
                        certificationModuleRequirementSetLabel.Text = _GlobalResources.Segment + ": " + certificationModuleRequirementSetLabelInLanguage;
                        certificationModuleRequirementSetLabelContainer.Controls.Add(certificationModuleRequirementSetLabel);

                        certificationModuleRequirementSetContainer.Controls.Add(certificationModuleRequirementSetLabelContainer);
                        
                        // CERTIFICATION REQUIREMENTS
                        DataTable certificationRequirements = certificationModuleRequirementSetObject.GetRequirementIds();
                        int moduleRequirementIndex = 0;

                        foreach (DataRow requirementRow in certificationRequirements.Rows)
                        {
                            moduleRequirementIndex++;

                            int idCertificationModuleRequirement = Convert.ToInt32(requirementRow["idCertificationModuleRequirement"]);
                            CertificationModuleRequirement certificationModuleRequirementObject = new CertificationModuleRequirement(idCertificationModuleRequirement);
                            bool isCertificationModuleRequirementComplete = false;
                            int? requirementType = null;
                            bool? courseCompletionIsAny = null;
                            double? numberCreditsRequired = null;
                            double? numberCreditsEarned = null;
                            bool? courseCreditEligibleCourseIsAll = null;
                            bool? documentationApprovalRequired = null;
                            bool? allowSupervisorsToApproveDocumentation = null;
                            string completionDocumentationFilePath = null;
                            DateTime? dtSubmitted = null;
                            DateTime? dtApproved = null;
                            int? idDataCertificationModuleRequirement = null;
                            string[] idsOfCoursesCompleted = new string[0];

                            // container
                            Panel certificationModuleRequirementContainer = new Panel();
                            certificationModuleRequirementContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleRequirementContainer_" + certificationModuleRequirementObject.Id.ToString();
                            certificationModuleRequirementContainer.CssClass = "CertificationModuleRequirementContainer";

                            // language-specific properties
                            string certificationModuleRequirementLabelInLanguage = certificationModuleRequirementObject.Label;

                            foreach (CertificationModuleRequirement.LanguageSpecificProperty certModuleRLSP in certificationModuleRequirementObject.LanguageSpecificProperties)
                            {
                                if (certModuleRLSP.LangString == AsentiaSessionState.UserCulture)
                                {
                                    certificationModuleRequirementLabelInLanguage = certModuleRLSP.Label;
                                    break;
                                }
                            }

                            // label
                            Panel certificationModuleRequirementLabelContainer = new Panel();
                            certificationModuleRequirementLabelContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleRequirementLabelContainer_" + certificationModuleRequirementObject.Id.ToString();
                            certificationModuleRequirementLabelContainer.CssClass = "CertificationModuleRequirementLabelContainer";

                            foreach (DataRow row in requirementStatuses.Rows)
                            {
                                if (row["objectType"].ToString() == "requirement" && Convert.ToInt32(row["idObject"]) == certificationModuleRequirementObject.Id)
                                {
                                    isCertificationModuleRequirementComplete = Convert.ToBoolean(row["isRequirementMet"]);
                                    
                                    if (row["requirementType"] != null)
                                    { requirementType = Convert.ToInt32(row["requirementType"]); }

                                    if (row["courseCompletionIsAny"] != DBNull.Value)
                                    { courseCompletionIsAny = Convert.ToBoolean(row["courseCompletionIsAny"]); }

                                    if (row["numberCreditsRequired"] != DBNull.Value)
                                    { numberCreditsRequired = Convert.ToDouble(row["numberCreditsRequired"]); }

                                    if (row["numberCreditsEarned"] != DBNull.Value)
                                    { numberCreditsEarned = Convert.ToDouble(row["numberCreditsEarned"]); }

                                    if (row["courseCreditEligibleCourseIsAll"] != DBNull.Value)
                                    { courseCreditEligibleCourseIsAll = Convert.ToBoolean(row["courseCreditEligibleCourseIsAll"]); }

                                    if (row["documentationApprovalRequired"] != DBNull.Value)
                                    { documentationApprovalRequired = Convert.ToBoolean(row["documentationApprovalRequired"]); }

                                    if (row["allowSupervisorsToApproveDocumentation"] != DBNull.Value)
                                    { allowSupervisorsToApproveDocumentation = Convert.ToBoolean(row["allowSupervisorsToApproveDocumentation"]); }

                                    if (row["completionDocumentationFilePath"] != DBNull.Value)
                                    { completionDocumentationFilePath = row["completionDocumentationFilePath"].ToString(); }

                                    if (row["dtSubmitted"] != DBNull.Value)
                                    { dtSubmitted = Convert.ToDateTime(row["dtSubmitted"]); }

                                    if (row["dtApproved"] != DBNull.Value)
                                    { dtApproved = Convert.ToDateTime(row["dtApproved"]); }

                                    if (row["idDataCertificationModuleRequirement"] != DBNull.Value)
                                    { idDataCertificationModuleRequirement = Convert.ToInt32(row["idDataCertificationModuleRequirement"]); }

                                    if (row["idsOfCoursesCompleted"] != DBNull.Value)
                                    { idsOfCoursesCompleted = row["idsOfCoursesCompleted"].ToString().Split(','); }
                                    
                                    break;
                                }
                            }

                            Label certificationModuleRequirementCompletionStatusImageContainer = new Label();
                            certificationModuleRequirementCompletionStatusImageContainer.Controls.Add(this._GetCompletionStatusImage(isCertificationModuleRequirementComplete, "SmallIcon"));
                            certificationModuleRequirementLabelContainer.Controls.Add(certificationModuleRequirementCompletionStatusImageContainer);

                            Label certificationModuleRequirementLabel = new Label();
                            certificationModuleRequirementLabel.Text = _GlobalResources.Requirement + ": " + certificationModuleRequirementLabelInLanguage;
                            certificationModuleRequirementLabelContainer.Controls.Add(certificationModuleRequirementLabel);

                            certificationModuleRequirementContainer.Controls.Add(certificationModuleRequirementLabelContainer);

                            // specifics of the requirement including document links and proctoring                            
                            Panel requirementDetailsContainer = new Panel();
                            requirementDetailsContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleRequirementDetailsContainer_" + certificationModuleRequirementObject.Id.ToString();
                            requirementDetailsContainer.CssClass = "CertificationModuleRequirementDetailsContainer";

                            if (requirementType == 0) // course
                            {
                                Literal completeFollowingCourses = new Literal();
                                requirementDetailsContainer.Controls.Add(completeFollowingCourses);

                                if ((bool)courseCompletionIsAny)
                                { completeFollowingCourses.Text = _GlobalResources.CompleteAnyOfTheseCourse_s; }
                                else
                                { completeFollowingCourses.Text = _GlobalResources.CompleteAllOfTheseCourse_s; }

                                DataTable requirementCourses = certificationModuleRequirementObject.GetCourseRequirementCourseIds();

                                HtmlGenericControl courseUL = new HtmlGenericControl("ul");
                                requirementDetailsContainer.Controls.Add(courseUL);

                                foreach (DataRow requirementCourse in requirementCourses.Rows)
                                {
                                    HtmlGenericControl courseLI = new HtmlGenericControl("li");

                                    if (Array.IndexOf(idsOfCoursesCompleted, requirementCourse["idCourse"].ToString()) > -1)
                                    { courseLI.Controls.Add(this._GetCompletionStatusImage(true, "XSmallIcon")); }
                                    else
                                    { courseLI.Controls.Add(this._GetCompletionStatusImage(false, "XSmallIcon")); }

                                    Literal courseLIText = new Literal();
                                    courseLIText.Text = requirementCourse["title"].ToString();
                                    courseLI.Controls.Add(courseLIText);
                                    
                                    courseUL.Controls.Add(courseLI);
                                }
                            }
                            else if (requirementType == 1) // credit
                            {
                                Literal creditsEarnedOfCreditsRequired = new Literal();
                                creditsEarnedOfCreditsRequired.Text = String.Format(_GlobalResources.XOfXCreditsEarnedOfTheFollowingCourse_s, numberCreditsEarned.ToString(), numberCreditsRequired.ToString());
                                requirementDetailsContainer.Controls.Add(creditsEarnedOfCreditsRequired);

                                DataTable requirementCourses;

                                if ((bool)courseCreditEligibleCourseIsAll)
                                { requirementCourses = this._CertificationObject.GetAttachedCourses(); }
                                else
                                { requirementCourses = certificationModuleRequirementObject.GetCourseRequirementCourseIds(); }

                                HtmlGenericControl courseUL = new HtmlGenericControl("ul");
                                requirementDetailsContainer.Controls.Add(courseUL);

                                foreach (DataRow requirementCourse in requirementCourses.Rows)
                                {                                    
                                    HtmlGenericControl courseLI = new HtmlGenericControl("li");

                                    if (Array.IndexOf(idsOfCoursesCompleted, requirementCourse["idCourse"].ToString()) > -1)
                                    { courseLI.Controls.Add(this._GetCompletionStatusImage(true, "XSmallIcon")); }
                                    else
                                    { courseLI.Controls.Add(this._GetCompletionStatusImage(false, "XSmallIcon")); }

                                    Literal courseLIText = new Literal();
                                    courseLIText.Text = requirementCourse["title"].ToString() + " (" + requirementCourse["credits"].ToString() + " " + _GlobalResources.Credits.ToLower() + ")";
                                    courseLI.Controls.Add(courseLIText);

                                    courseUL.Controls.Add(courseLI);
                                }
                            }
                            else if (requirementType == 2) // task
                            {
                                if (dtSubmitted != null)
                                {
                                    if ((bool)documentationApprovalRequired && !String.IsNullOrEmpty(completionDocumentationFilePath))
                                    {
                                        Panel uploadedTaskDocumentLinkContainer = new Panel();
                                        requirementDetailsContainer.Controls.Add(uploadedTaskDocumentLinkContainer);

                                        Literal uploadedDocumentText = new Literal();
                                        uploadedDocumentText.Text = _GlobalResources.UploadedDocument;

                                        HyperLink uploadedTaskDocumentLink = new HyperLink();
                                        uploadedTaskDocumentLink.NavigateUrl = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id.ToString() + "/CertificationTasks/" + idDataCertificationModuleRequirement.ToString() + "/" + completionDocumentationFilePath;
                                        uploadedTaskDocumentLink.Target = "_blank";
                                        uploadedTaskDocumentLink.Text = completionDocumentationFilePath;
                                        uploadedTaskDocumentLinkContainer.Controls.Add(uploadedTaskDocumentLink);

                                        if (dtApproved == null)
                                        {
                                            Panel uploadedTaskProctorLinkContainer = new Panel();
                                            requirementDetailsContainer.Controls.Add(uploadedTaskProctorLinkContainer);

                                            HyperLink uploadedTaskProctorLink = new HyperLink();
                                            uploadedTaskProctorLink.NavigateUrl = "javascript: LoadCertificationTaskModalContent(" + this._UserObject.Id.ToString() + ", " + this._IdCertificationToUserLink + ", " + idDataCertificationModuleRequirement + ");";
                                            uploadedTaskProctorLink.Text = _GlobalResources.ProctorTask;
                                            uploadedTaskProctorLinkContainer.Controls.Add(uploadedTaskProctorLink);
                                        }
                                    }
                                }
                            }
                            else
                            { }

                            // attach the requirement details to the requirement container
                            certificationModuleRequirementContainer.Controls.Add(requirementDetailsContainer);

                            // attach module requirement container to module requirement set panel
                            certificationModuleRequirementSetContainer.Controls.Add(certificationModuleRequirementContainer);

                            // create and attach and/or label for module requirement
                            if (moduleRequirementIndex < certificationRequirements.Rows.Count)
                            {
                                Panel certificationModuleRequirementAndOrContainer = new Panel();
                                certificationModuleRequirementAndOrContainer.CssClass = "CertificationAndOrContainer";

                                Literal certificationModuleRequirementAndOrText = new Literal();
                                certificationModuleRequirementAndOrContainer.Controls.Add(certificationModuleRequirementAndOrText);

                                if (certificationModuleRequirementSetObject.IsAny)
                                { certificationModuleRequirementAndOrText.Text = _GlobalResources.OR_UPPER; }
                                else
                                { certificationModuleRequirementAndOrText.Text = _GlobalResources.AND_UPPER; }

                                certificationModuleRequirementSetContainer.Controls.Add(certificationModuleRequirementAndOrContainer);
                            }
                        }

                        // attach module requirement set container to module panel
                        certificationModuleContainer.Controls.Add(certificationModuleRequirementSetContainer);

                        // create and attach and/or label for module requirement set
                        if (moduleRequirementSetIndex < certificationRequirementSets.Rows.Count)
                        {
                            Panel certificationModuleRequirementSetAndOrContainer = new Panel();
                            certificationModuleRequirementSetAndOrContainer.CssClass = "CertificationAndOrContainer";

                            Literal certificationModuleRequirementSetAndOrText = new Literal();
                            certificationModuleRequirementSetAndOrContainer.Controls.Add(certificationModuleRequirementSetAndOrText);

                            if (certificationModuleObject.IsAnyRequirementSet)
                            { certificationModuleRequirementSetAndOrText.Text = _GlobalResources.OR_UPPER; }
                            else
                            { certificationModuleRequirementSetAndOrText.Text = _GlobalResources.AND_UPPER; }

                            certificationModuleContainer.Controls.Add(certificationModuleRequirementSetAndOrContainer);
                        }
                    }

                    // attach module container to tab panel
                    tabUpdatePanelContainer.ContentTemplateContainer.Controls.Add(certificationModuleContainer);

                    // create and attach and/or label for module
                    Panel certificationModuleAndOrContainer = new Panel();
                    certificationModuleAndOrContainer.CssClass = "CertificationAndOrContainer";

                    Literal certificationModuleAndOrText = new Literal();
                    certificationModuleAndOrContainer.Controls.Add(certificationModuleAndOrText);

                    if (this._CertificationObject.IsAnyModule)
                    { certificationModuleAndOrText.Text = _GlobalResources.OR_UPPER; }
                    else
                    { certificationModuleAndOrText.Text = _GlobalResources.AND_UPPER; }

                    tabUpdatePanelContainer.ContentTemplateContainer.Controls.Add(certificationModuleAndOrContainer);
                }
            }

            // take out the last control, which should be the last and/or container
            tabUpdatePanelContainer.ContentTemplateContainer.Controls.RemoveAt(tabUpdatePanelContainer.ContentTemplateContainer.Controls.Count - 1);
        }
        #endregion

        #region _GetCompletionStatusImage
        private Image _GetCompletionStatusImage(bool isRequirementMet, string iconClass)
        {
            Image completionStatusImage = new Image();
            completionStatusImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
            completionStatusImage.CssClass = iconClass + " CertificationCompletionStatusImage";
            completionStatusImage.AlternateText = _GlobalResources.Status;

            if (!isRequirementMet)
            { completionStatusImage.CssClass += " DimIcon"; }

            return completionStatusImage;
        }
        #endregion

        #region _BuildTaskProctoringModal
        /// <summary>
        /// 
        /// </summary>
        private void _BuildTaskProctoringModal()
        {
            // set modal properties
            this._TaskProctoringModal = new ModalPopup("TaskProctoringModal");
            this._TaskProctoringModal.Type = ModalPopupType.Form;
            this._TaskProctoringModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
            this._TaskProctoringModal.HeaderIconAlt = _GlobalResources.ProctorTask;
            this._TaskProctoringModal.HeaderText = _GlobalResources.ProctorTask;
            this._TaskProctoringModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._TaskProctoringModal.TargetControlID = this._TaskProctoringModalLaunchButton.ID;
            this._TaskProctoringModal.SubmitButton.Command += new CommandEventHandler(this._TaskProctoringSubmit_Command);

            // build the modal form panel
            Panel taskModalFormPanel = new Panel();
            taskModalFormPanel.ID = "TaskProctoringModalFormPanel";
            taskModalFormPanel.Visible = false; // don't render this, we will render in the _LoadTaskProctoringModalContent method

            // note, we need to add the control for setting approval, it registers properly in the control stack

            // approve

            CheckBox approveCheckbox = new CheckBox();
            approveCheckbox.ID = "TaskProctoringModal_Approve_Field";
            approveCheckbox.Text = _GlobalResources.SignoffOnThisTaskAsCompleted;

            taskModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("TaskProctoringModal_Approve",
                                                                       _GlobalResources.Signoff,
                                                                       approveCheckbox.ID,
                                                                       approveCheckbox,
                                                                       false,
                                                                       false,
                                                                       false));           

            // build the task data hidden field
            HiddenField taskData = new HiddenField();
            taskData.ID = "TaskData";

            // build the modal body
            this._TaskProctoringModal.AddControlToBody(taskModalFormPanel);
            this._TaskProctoringModal.AddControlToBody(this._TaskProctoringModalLoadButton);
            this._TaskProctoringModal.AddControlToBody(taskData);

            // add modal to container
            this.CertificationFormWrapperContainer.Controls.Add(this._TaskProctoringModal);
        }
        #endregion

        #region _LoadTaskProctoringModalContent
        /// <summary>
        /// Loads content for task proctoring modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadTaskProctoringModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField taskData = (HiddenField)this._TaskProctoringModal.FindControl("TaskData");
            Panel taskModalFormPanel = (Panel)this._TaskProctoringModal.FindControl("TaskProctoringModalFormPanel");

            try
            {
                // clear the modal feedback
                this._TaskProctoringModal.ClearFeedback();

                // make the submit and close buttons visible
                this._TaskProctoringModal.SubmitButton.Visible = true;
                this._TaskProctoringModal.CloseButton.Visible = true;

                // if the sender is TaskProctoringModalLoadButton, reset the approve form field
                Button senderButton = (Button)sender;

                if (senderButton.ID == "TaskProctoringModalLoadButton")
                {
                    CheckBox approveCheckboxData = (CheckBox)taskModalFormPanel.FindControl("TaskProctoringModal_Approve_Field");

                    approveCheckboxData.Checked = false;
                }
                
                // get the user id, certification user link id, certification requirement data id
                string[] taskDataItems = taskData.Value.Split('|');
                int idUser = Convert.ToInt32(taskDataItems[0]);
                int idCertificationToUserLink = Convert.ToInt32(taskDataItems[1]);
                int idDataCertificationModuleRequirement = Convert.ToInt32(taskDataItems[2]);

                // set the form panel so it will render
                taskModalFormPanel.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(dnfEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(fnuEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(cpeEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(dEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(ex.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _TaskProctoringSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the task proctoring modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _TaskProctoringSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField taskData = (HiddenField)this._TaskProctoringModal.FindControl("TaskData");
            Panel taskModalFormPanel = (Panel)this._TaskProctoringModal.FindControl("TaskProctoringModalFormPanel");

            try
            {
                // clear the modal feedback
                this._TaskProctoringModal.ClearFeedback();

                // get the user id, certification user link id, certification requirement data id
                string[] taskDataItems = taskData.Value.Split('|');
                int idUser = Convert.ToInt32(taskDataItems[0]);
                int idCertificationToUserLink = Convert.ToInt32(taskDataItems[1]);
                int idDataCertificationModuleRequirement = Convert.ToInt32(taskDataItems[2]);

                // get the values from the approval control
                CheckBox approveCheckboxData = (CheckBox)taskModalFormPanel.FindControl("TaskProctoringModal_Approve_Field");

                bool approve = Convert.ToBoolean(approveCheckboxData.Checked);

                // save the data
                CertificationModuleRequirement.ProctorTask(idCertificationToUserLink, idDataCertificationModuleRequirement, approve);

                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
                this._TaskProctoringModal.DisplayFeedback(_GlobalResources.TheTaskDataHasBeenSavedSuccessfully, false);

                // rebind and update
                this._BuildCertificationRequirementTabPanelContent(true, this._CertificationInitialTabPanel);
                this._BuildCertificationRequirementTabPanelContent(false, this._CertificationRenewalTabPanel);
                this._CertificationInitialTabPanel.Update();
                this._CertificationRenewalTabPanel.Update();                
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(dnfEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(fnuEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(cpeEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(dEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(ex.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildModifyCertificationInitialAwardDateModal
        /// <summary>
        /// Builds the modal for modifying the certification initial award date
        /// </summary>
        private void _BuildModifyCertificationInitialAwardDateModal(){

            // set modal properties
            this._ModifyCertificationInitialAwardDateModal = new ModalPopup("ModifyCertificationInitialAwardDateModal");
            this._ModifyCertificationInitialAwardDateModal.Type = ModalPopupType.Form;
            this._ModifyCertificationInitialAwardDateModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
            this._ModifyCertificationInitialAwardDateModal.HeaderIconAlt = _GlobalResources.InitialAwardDate;
            this._ModifyCertificationInitialAwardDateModal.HeaderText = _GlobalResources.InitialAwardDate;
            this._ModifyCertificationInitialAwardDateModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._ModifyCertificationInitialAwardDateModal.TargetControlID = this._ModifyCertificationInitialAwardDateModalLaunchButton.ID;
            this._ModifyCertificationInitialAwardDateModal.SubmitButton.Command += new CommandEventHandler(this._ModifyCertificationInitialAwardDateSubmit_Command);

            // build the modal form panel
            Panel certificationInitialAwardDateModalFormPanel = new Panel();
            certificationInitialAwardDateModalFormPanel.ID = "CertificationInitialAwardDateModalFormPanel";
            //certificationInitialAwardDateModalFormPanel.Visible = false; // don't render this, we will render in the _LoadCertificationInitialAwardDateModalContent method

            DatePicker certificationInitialAwardDate = new DatePicker("CertificationInitialAwardDatePicker", true, true, false);

            certificationInitialAwardDateModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("CertificationInitialAwardDateModal_Field",
                                                                       _GlobalResources.InitialAwardDate,
                                                                       certificationInitialAwardDate.ID,
                                                                       certificationInitialAwardDate,
                                                                       false,
                                                                       false,
                                                                       false));
            

            // build the modal body
            this._ModifyCertificationInitialAwardDateModal.AddControlToBody(certificationInitialAwardDateModalFormPanel);
            this._ModifyCertificationInitialAwardDateModal.AddControlToBody(this._ModifyCertificationInitialAwardDateModalLoadButton);

            // add modal to container
            this.CertificationFormWrapperContainer.Controls.Add(this._ModifyCertificationInitialAwardDateModal);
        }

        #endregion

        #region _LoadModifyCertificationInitialAwardDateModalContent
        /// <summary>
        /// Loads content for certification initial award date modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadModifyCertificationInitialAwardDateModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel certificationInitialAwardDateModalFormPanel = (Panel)this._ModifyCertificationInitialAwardDateModal.FindControl("CertificationInitialAwardDateModalFormPanel");
            DatePicker certificationInitialAwardDatePicker = (DatePicker)AsentiaPage.FindControlRecursive(certificationInitialAwardDateModalFormPanel, "CertificationInitialAwardDatePicker");
            
            try
            {
               
                // clear the modal feedback
                this._ModifyCertificationInitialAwardDateModal.ClearFeedback();

                // make the submit and close buttons visible
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = true;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = true;

                // get the certification user link id
                int idCertificationToUserLink = this._IdCertificationToUserLink;

                DataTable certificationDetails = Certification.GetUserCertificationDetails(idCertificationToUserLink);

                if (certificationDetails.Rows[0]["initialAwardDate"] != DBNull.Value)
                {
                    certificationInitialAwardDatePicker.Value = Convert.ToDateTime(certificationDetails.Rows[0]["initialAwardDate"]);
                }



                // set the form panel so it will render
                certificationInitialAwardDateModalFormPanel.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(dnfEx.Message, true);
                certificationInitialAwardDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(fnuEx.Message, true);
                certificationInitialAwardDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(cpeEx.Message, true);
                certificationInitialAwardDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(dEx.Message, true);
                certificationInitialAwardDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(ex.Message, true);
                certificationInitialAwardDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
            }
            
        }
        #endregion

        #region _ModifyCertificationInitialAwardDateSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the modify certification initial award date modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _ModifyCertificationInitialAwardDateSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel certificationInitialAwardDateModalFormPanel = (Panel)this._ModifyCertificationInitialAwardDateModal.FindControl("CertificationInitialAwardDateModalFormPanel");
            DatePicker certificationInitialAwardDatePicker = (DatePicker)AsentiaPage.FindControlRecursive(certificationInitialAwardDateModalFormPanel, "CertificationInitialAwardDatePicker");

            try
            {
                // clear the modal feedback
                this._ModifyCertificationInitialAwardDateModal.ClearFeedback();
        
                // get the certification user link id
                int idCertificationToUserLink = this._IdCertificationToUserLink;

                // get the values from the initial award date box
                DatePicker initialAwardDatePicker = (DatePicker)certificationInitialAwardDateModalFormPanel.FindControl("CertificationInitialAwardDatePicker");
                
                if(initialAwardDatePicker.Value != null && (initialAwardDatePicker.Value > this._CurrentExpirationDate || initialAwardDatePicker.Value > this._LastExpirationDate || initialAwardDatePicker.Value > DateTime.Now)){
                    this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(_GlobalResources.InitialAwardDateMustBeBeforeTodaysDateAndBeforeExpirationDate_s, true);
                }else{
                    // save the data
                    DateTime? initialAwardDate = new DateTime?();
                    if (initialAwardDatePicker.Value != null)
                    {initialAwardDate = TimeZoneInfo.ConvertTimeToUtc((DateTime)initialAwardDatePicker.Value, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));}

                    Certification.UpdateUserCertificationDates(idCertificationToUserLink, initialAwardDate, 1);

                    // synchronize user certification task data
                    Certification.SynchronizeTaskRequirementData(idCertificationToUserLink);

                    HtmlGenericControl initialAwardDateSpan = (HtmlGenericControl)this._CertificationPropertiesTabPanel.FindControl("InitialAwardDateSpan");
                    Panel certificationTrackPanel = (Panel)this._CertificationPropertiesTabPanel.FindControl("CertificationTrackPanel");
                
                    Literal certificationTrack = new Literal();
                
                
                    if (initialAwardDatePicker.Value != null)
                    {
                        initialAwardDateSpan.InnerText = ((DateTime)initialAwardDatePicker.Value).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
                        certificationTrack.Text = _GlobalResources.Renewal;
                    }
                    else
                    {
                        initialAwardDateSpan.InnerText = _GlobalResources.None;
                    }
                    this._InitialAwardDate = initialAwardDatePicker.Value;

                    certificationTrackPanel.Controls.Clear();
                    certificationTrackPanel.Controls.Add(certificationTrack);

                    certificationInitialAwardDateModalFormPanel.Controls.Clear();
                    this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                    this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
                    this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(_GlobalResources.TheCertificationInitialAwardDateHasBeenUpdatedSuccessfully, false);
                
                    // rebind and update
                    this._BuildCertificationRequirementTabPanelContent(true, this._CertificationInitialTabPanel);
                    this._BuildCertificationRequirementTabPanelContent(false, this._CertificationRenewalTabPanel);
                    this._CertificationPropertiesTabPanel.Update();
                    this._CertificationInitialTabPanel.Update();
                    this._CertificationRenewalTabPanel.Update();
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(dnfEx.Message, true);
                certificationInitialAwardDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(fnuEx.Message, true);
                certificationInitialAwardDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(cpeEx.Message, true);
                certificationInitialAwardDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(dEx.Message, true);
                certificationInitialAwardDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ModifyCertificationInitialAwardDateModal.DisplayFeedback(ex.Message, true);
                certificationInitialAwardDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationInitialAwardDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationInitialAwardDateModal.CloseButton.Visible = false;
            }
            
        }
        #endregion

        #region _BuildModifyCertificationCurrentExpirationDateModal
        /// <summary>
        /// Builds the modal for modifying the certification current expiration date
        /// </summary>
        private void _BuildModifyCertificationCurrentExpirationDateModal()
        {

            // set modal properties
            this._ModifyCertificationCurrentExpirationDateModal = new ModalPopup("ModifyCertificationCurrentExpirationDateModal");
            this._ModifyCertificationCurrentExpirationDateModal.Type = ModalPopupType.Form;
            this._ModifyCertificationCurrentExpirationDateModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
            this._ModifyCertificationCurrentExpirationDateModal.HeaderIconAlt = _GlobalResources.CurrentExpirationDate;
            this._ModifyCertificationCurrentExpirationDateModal.HeaderText = _GlobalResources.CurrentExpirationDate;
            this._ModifyCertificationCurrentExpirationDateModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._ModifyCertificationCurrentExpirationDateModal.TargetControlID = this._ModifyCertificationCurrentExpirationDateModalLaunchButton.ID;
            this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Command += new CommandEventHandler(this._ModifyCertificationCurrentExpirationDateSubmit_Command);

            // build the modal form panel
            Panel certificationCurrentExpirationDateModalFormPanel = new Panel();
            certificationCurrentExpirationDateModalFormPanel.ID = "CertificationCurrentExpirationDateModalFormPanel";
            //certificationCurrentExpirationDateModalFormPanel.Visible = false; // don't render this, we will render in the _LoadCertificationCurrentExpirationDateModalContent method

            DatePicker certificationCurrentExpirationDate = new DatePicker("CertificationCurrentExpirationDatePicker", true, true, false);

            certificationCurrentExpirationDateModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("CertificationCurrentExpirationDateModal_Field",
                                                                       _GlobalResources.CurrentExpirationDate,
                                                                       certificationCurrentExpirationDate.ID,
                                                                       certificationCurrentExpirationDate,
                                                                       false,
                                                                       false,
                                                                       false));


            // build the modal body
            this._ModifyCertificationCurrentExpirationDateModal.AddControlToBody(certificationCurrentExpirationDateModalFormPanel);
            this._ModifyCertificationCurrentExpirationDateModal.AddControlToBody(this._ModifyCertificationCurrentExpirationDateModalLoadButton);

            // add modal to container
            this.CertificationFormWrapperContainer.Controls.Add(this._ModifyCertificationCurrentExpirationDateModal);
        }

        #endregion

        #region _LoadModifyCertificationCurrentExpirationDateModalContent
        /// <summary>
        /// Loads content for certification expiration date modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadModifyCertificationCurrentExpirationDateModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel certificationCurrentExpirationDateModalFormPanel = (Panel)this._ModifyCertificationCurrentExpirationDateModal.FindControl("CertificationCurrentExpirationDateModalFormPanel");
            DatePicker certificationCurrentExpirationDatePicker = (DatePicker)AsentiaPage.FindControlRecursive(certificationCurrentExpirationDateModalFormPanel, "CertificationCurrentExpirationDatePicker");

            try
            {

                // clear the modal feedback
                this._ModifyCertificationCurrentExpirationDateModal.ClearFeedback();

                // make the submit and close buttons visible
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = true;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = true;

                // get the certification user link id
                int idCertificationToUserLink = this._IdCertificationToUserLink;

                DataTable certificationDetails = Certification.GetUserCertificationDetails(idCertificationToUserLink);

                if (certificationDetails.Rows[0]["currentExpirationDate"] != DBNull.Value)
                {
                    certificationCurrentExpirationDatePicker.Value = Convert.ToDateTime(certificationDetails.Rows[0]["currentExpirationDate"]);
                }



                // set the form panel so it will render
                certificationCurrentExpirationDateModalFormPanel.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(dnfEx.Message, true);
                certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(fnuEx.Message, true);
                certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(cpeEx.Message, true);
                certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(dEx.Message, true);
                certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(ex.Message, true);
                certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
            }

        }
        #endregion

        #region _ModifyCertificationCurrentExpirationDateSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the modify certification expiration date modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _ModifyCertificationCurrentExpirationDateSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel certificationCurrentExpirationDateModalFormPanel = (Panel)this._ModifyCertificationCurrentExpirationDateModal.FindControl("CertificationCurrentExpirationDateModalFormPanel");
            DatePicker certificationCurrentExpirationDatePicker = (DatePicker)AsentiaPage.FindControlRecursive(certificationCurrentExpirationDateModalFormPanel, "CertificationCurrentExpirationDatePicker");

            try
            {
                // clear the modal feedback
                this._ModifyCertificationCurrentExpirationDateModal.ClearFeedback();

                // get the certification user link id
                int idCertificationToUserLink = this._IdCertificationToUserLink;

                // get the values from the current expiration date box
                DatePicker currentExpirationDatePicker = (DatePicker)certificationCurrentExpirationDateModalFormPanel.FindControl("CertificationCurrentExpirationDatePicker");

                if (currentExpirationDatePicker.Value != null && (currentExpirationDatePicker.Value < this._InitialAwardDate || currentExpirationDatePicker.Value < this._LastExpirationDate || currentExpirationDatePicker.Value < DateTime.Now))
                {
                    this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(_GlobalResources.CurrentExpirationDateMustBeAfterTodaysDateLastExpirationAndInitialAwardDates, true);
                }
                else
                {
                    // save the data
                    DateTime? currentExpirationDate = new DateTime?();
                    if (currentExpirationDatePicker.Value != null)
                    { currentExpirationDate = TimeZoneInfo.ConvertTimeToUtc((DateTime)currentExpirationDatePicker.Value, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)); }

                    Certification.UpdateUserCertificationDates(idCertificationToUserLink, currentExpirationDate, 2);

                    // synchronize user certification task data
                    Certification.SynchronizeTaskRequirementData(idCertificationToUserLink);

                    HtmlGenericControl currentExpirationDateSpan = (HtmlGenericControl)this._CertificationPropertiesTabPanel.FindControl("CurrentExpirationDateSpan");



                    if (currentExpirationDatePicker.Value != null)
                    {
                        currentExpirationDateSpan.InnerText = ((DateTime)currentExpirationDatePicker.Value).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
                    }
                    else
                    {
                        currentExpirationDateSpan.InnerText = _GlobalResources.None;
                    }
                    this._CurrentExpirationDate = currentExpirationDatePicker.Value;

                    certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                    this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                    this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
                    this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(_GlobalResources.TheCertificationCurrentExpirationDateHasBeenUpdatedSuccessfully, false);

                    // rebind and update
                    this._BuildCertificationRequirementTabPanelContent(true, this._CertificationInitialTabPanel);
                    this._BuildCertificationRequirementTabPanelContent(false, this._CertificationRenewalTabPanel);
                    this._CertificationPropertiesTabPanel.Update();
                    this._CertificationInitialTabPanel.Update();
                    this._CertificationRenewalTabPanel.Update();
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(dnfEx.Message, true);
                certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(fnuEx.Message, true);
                certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(cpeEx.Message, true);
                certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(dEx.Message, true);
                certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ModifyCertificationCurrentExpirationDateModal.DisplayFeedback(ex.Message, true);
                certificationCurrentExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationCurrentExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationCurrentExpirationDateModal.CloseButton.Visible = false;
            }

        }
        #endregion

        #region _BuildModifyCertificationLastExpirationDateModal
        /// <summary>
        /// Builds the modal for modifying the certification last expiration date
        /// </summary>
        private void _BuildModifyCertificationLastExpirationDateModal()
        {

            // set modal properties
            this._ModifyCertificationLastExpirationDateModal = new ModalPopup("ModifyCertificationLastExpirationDateModal");
            this._ModifyCertificationLastExpirationDateModal.Type = ModalPopupType.Form;
            this._ModifyCertificationLastExpirationDateModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
            this._ModifyCertificationLastExpirationDateModal.HeaderIconAlt = _GlobalResources.LastExpirationDate;
            this._ModifyCertificationLastExpirationDateModal.HeaderText = _GlobalResources.LastExpirationDate;
            this._ModifyCertificationLastExpirationDateModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._ModifyCertificationLastExpirationDateModal.TargetControlID = this._ModifyCertificationLastExpirationDateModalLaunchButton.ID;
            this._ModifyCertificationLastExpirationDateModal.SubmitButton.Command += new CommandEventHandler(this._ModifyCertificationLastExpirationDateSubmit_Command);

            // build the modal form panel
            Panel certificationLastExpirationDateModalFormPanel = new Panel();
            certificationLastExpirationDateModalFormPanel.ID = "CertificationLastExpirationDateModalFormPanel";
            //certificationLastExpirationDateModalFormPanel.Visible = false; // don't render this, we will render in the _LoadCertificationLastExpirationDateModalContent method

            DatePicker certificationLastExpirationDate = new DatePicker("CertificationLastExpirationDatePicker", true, true, false);

            certificationLastExpirationDateModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("CertificationLastExpirationDateModal_Field",
                                                                       _GlobalResources.LastExpirationDate,
                                                                       certificationLastExpirationDate.ID,
                                                                       certificationLastExpirationDate,
                                                                       false,
                                                                       false,
                                                                       false));


            // build the modal body
            this._ModifyCertificationLastExpirationDateModal.AddControlToBody(certificationLastExpirationDateModalFormPanel);
            this._ModifyCertificationLastExpirationDateModal.AddControlToBody(this._ModifyCertificationLastExpirationDateModalLoadButton);

            // add modal to container
            this.CertificationFormWrapperContainer.Controls.Add(this._ModifyCertificationLastExpirationDateModal);
        }

        #endregion

        #region _LoadModifyCertificationLastExpirationDateModalContent
        /// <summary>
        /// Loads content for certification last date modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadModifyCertificationLastExpirationDateModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel certificationLastExpirationDateModalFormPanel = (Panel)this._ModifyCertificationLastExpirationDateModal.FindControl("CertificationLastExpirationDateModalFormPanel");
            DatePicker certificationLastExpirationDatePicker = (DatePicker)AsentiaPage.FindControlRecursive(certificationLastExpirationDateModalFormPanel, "CertificationLastExpirationDatePicker");

            try
            {

                // clear the modal feedback
                this._ModifyCertificationLastExpirationDateModal.ClearFeedback();

                // make the submit and close buttons visible
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = true;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = true;

                // get the certification user link id
                int idCertificationToUserLink = this._IdCertificationToUserLink;

                DataTable certificationDetails = Certification.GetUserCertificationDetails(idCertificationToUserLink);

                if (certificationDetails.Rows[0]["lastExpirationDate"] != DBNull.Value)
                {
                    certificationLastExpirationDatePicker.Value = Convert.ToDateTime(certificationDetails.Rows[0]["lastExpirationDate"]);
                }



                // set the form panel so it will render
                certificationLastExpirationDateModalFormPanel.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(dnfEx.Message, true);
                certificationLastExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(fnuEx.Message, true);
                certificationLastExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(cpeEx.Message, true);
                certificationLastExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(dEx.Message, true);
                certificationLastExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(ex.Message, true);
                certificationLastExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
            }

        }
        #endregion

        #region _ModifyCertificationLastExpirationDateSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the modify certification last expiration date modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _ModifyCertificationLastExpirationDateSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel certificationLastExpirationDateModalFormPanel = (Panel)this._ModifyCertificationLastExpirationDateModal.FindControl("CertificationLastExpirationDateModalFormPanel");
            DatePicker certificationLastExpirationDatePicker = (DatePicker)AsentiaPage.FindControlRecursive(certificationLastExpirationDateModalFormPanel, "CertificationLastExpirationDatePicker");

            try
            {
                // clear the modal feedback
                this._ModifyCertificationLastExpirationDateModal.ClearFeedback();

                // get the certification user link id
                int idCertificationToUserLink = this._IdCertificationToUserLink;

                // get the values from the last expiration date box
                DatePicker lastExpirationDatePicker = (DatePicker)certificationLastExpirationDateModalFormPanel.FindControl("CertificationLastExpirationDatePicker");

                if (lastExpirationDatePicker.Value != null && (lastExpirationDatePicker.Value < this._InitialAwardDate || lastExpirationDatePicker.Value > this._CurrentExpirationDate || lastExpirationDatePicker.Value > DateTime.Now))
                {
                    this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(_GlobalResources.LastExpirationDateMustBeBeforeTodaysDateBeforeTheCurrentExpirationDateAndAfterTheInitialAwardDate, true);
                }
                else
                {
                    // save the data
                    DateTime? lastExpirationDate = new DateTime?();
                    if (lastExpirationDatePicker.Value != null)
                    { lastExpirationDate = TimeZoneInfo.ConvertTimeToUtc((DateTime)lastExpirationDatePicker.Value, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)); }

                    Certification.UpdateUserCertificationDates(idCertificationToUserLink, lastExpirationDate, 3);

                    // synchronize user certification task data
                    Certification.SynchronizeTaskRequirementData(idCertificationToUserLink);

                    HtmlGenericControl lastExpirationDateSpan = (HtmlGenericControl)this._CertificationPropertiesTabPanel.FindControl("LastExpirationDateSpan");



                    if (lastExpirationDatePicker.Value != null)
                    {
                        lastExpirationDateSpan.InnerText = ((DateTime)lastExpirationDatePicker.Value).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
                    }
                    else
                    {
                        lastExpirationDateSpan.InnerText = _GlobalResources.None;
                    }
                    this._LastExpirationDate = lastExpirationDatePicker.Value;

                    certificationLastExpirationDateModalFormPanel.Controls.Clear();
                    this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                    this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
                    this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(_GlobalResources.TheCertificationLastExpirationDateHasBeenUpdatedSuccessfully, false);

                    // rebind and update
                    this._BuildCertificationRequirementTabPanelContent(true, this._CertificationInitialTabPanel);
                    this._BuildCertificationRequirementTabPanelContent(false, this._CertificationRenewalTabPanel);
                    this._CertificationPropertiesTabPanel.Update();
                    this._CertificationInitialTabPanel.Update();
                    this._CertificationRenewalTabPanel.Update();
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(dnfEx.Message, true);
                certificationLastExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(fnuEx.Message, true);
                certificationLastExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(cpeEx.Message, true);
                certificationLastExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(dEx.Message, true);
                certificationLastExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ModifyCertificationLastExpirationDateModal.DisplayFeedback(ex.Message, true);
                certificationLastExpirationDateModalFormPanel.Controls.Clear();
                this._ModifyCertificationLastExpirationDateModal.SubmitButton.Visible = false;
                this._ModifyCertificationLastExpirationDateModal.CloseButton.Visible = false;
            }

        }
        #endregion

    }
}