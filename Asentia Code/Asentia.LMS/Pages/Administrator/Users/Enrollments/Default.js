﻿function ResetEnrollment(enrollmentId) {
    var resetEnrollmentIdHiddenField = $("#ResetEnrollmentId");
    resetEnrollmentIdHiddenField.val(enrollmentId);

    $find('ResetConfirmActionModalModalPopupExtender').show();
}

function TogglePrerequisiteLockForEnrollment(enrollmentId) {
    var togglePrerequisiteEnrollmentIdHiddenField = $("#TogglePrerequisiteEnrollmentId");
    togglePrerequisiteEnrollmentIdHiddenField.val(enrollmentId);

    $find('TogglePrerequisiteLockConfirmActionModalModalPopupExtender').show();
}