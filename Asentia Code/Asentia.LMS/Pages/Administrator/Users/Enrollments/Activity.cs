﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Users.Enrollments
{
    public class Activity : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel EnrollmentFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel EnrollmentFormWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel EnrollmentFormContainer;
        public Panel ActionsPanel;
        public UpdatePanel ActivityGridUpdatePanel;
        public Grid ActivityGrid;
        #endregion

        #region Private Properties
        private User _UserObject;
        private Enrollment _EnrollmentObject;

        private DropDownList _CompletionStatus;
        private DatePicker _CompletionDate;

        private Button _SaveButton;
        private Button _CancelButton;

        // task proctoring modal
        private ModalPopup _TaskProctoringModal;
        private Button _TaskProctoringModalLaunchButton;
        private Button _TaskProctoringModalLoadButton;

        // administrator override modal
        private ModalPopup _AdministratorOverrideModal;
        private Button _AdministratorOverrideModalLaunchButton;
        private Button _AdministratorOverrideModalLoadButton;

        // reset lesson data confirmation modal
        private ModalPopup _ResetLessonDataConfirmationModal;
        private Button _ResetLessonDataConfirmationModalLaunchButton;
        private Button _ResetLessonDataConfirmationModalLoadButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.Users.Enrollments.Activity.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {            
            // get the enrollment and user objects
            this._GetEnrollmentAndUserObjects();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/users/enrollments/Activity.css");

            // initialize the administrator menu
            this.InitializeAdminMenu();
            
            // check and fill in lesson data completions for standup training, only if the enrollment has not been completed
            if (this._EnrollmentObject.DtCompleted == null)
            { this._EnrollmentObject.SetLessonDataCompletionForStandupTraining(); }

            // build the controls for the page
            this._BuildControls();

            // build Activity grid
            this._BuildGrid();

            if (!Page.IsPostBack)
            {
                this.ActivityGrid.BindData();
            }
        }
        #endregion

        #region _GetEnrollmentAndUserObjects
        /// <summary>
        /// Gets the user and enrollment objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetEnrollmentAndUserObjects()
        {
            // get the id querystring parameter
            int qsUId = this.QueryStringInt("uid", 0);
            int vsUId = this.ViewStateInt(this.ViewState, "uid", 0);
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            // get user object - user object MUST be specified and exist for this page to load
            if (qsUId > 0 || vsUId > 0)
            {
                int uid = 0;

                if (qsUId > 0)
                { uid = qsUId; }

                if (vsUId > 0)
                { uid = vsUId; }

                try
                {
                    if (uid > 0)
                    { this._UserObject = new User(uid); }
                }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            { Response.Redirect("~/administrator/users"); }

            // get enrollment object - enrollment object MUST be specified and exist for this page to load
            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._EnrollmentObject = new Enrollment(id); }
                }
                catch
                { Response.Redirect("~/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString()); }
            }
            else
            { Response.Redirect("~/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString()); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            // evaluate for breadcrumb and page title information
            string pageTitle = this._EnrollmentObject.Title + ": " + _GlobalResources.Activity;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(this._UserObject.DisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Enrollments, "/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(pageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to create the page Breadcrumb, user action menu controls and other controls on page.
        /// </summary>
        private void _BuildControls()
        {
            // instansiate task proctoring controls
            this._TaskProctoringModalLaunchButton = new Button();
            this._TaskProctoringModalLaunchButton.ID = "TaskProctoringModalLaunchButton";
            this._TaskProctoringModalLaunchButton.Style.Add("display", "none");

            this._TaskProctoringModalLoadButton = new Button();
            this._TaskProctoringModalLoadButton.ID = "TaskProctoringModalLoadButton";
            this._TaskProctoringModalLoadButton.Style.Add("display", "none");
            this._TaskProctoringModalLoadButton.Click += this._LoadTaskProctoringModalContent;

            // instansiate lesson administrator override controls
            this._AdministratorOverrideModalLaunchButton = new Button();
            this._AdministratorOverrideModalLaunchButton.ID = "AdministratorOverrideModalLaunchButton";
            this._AdministratorOverrideModalLaunchButton.Style.Add("display", "none");

            this._AdministratorOverrideModalLoadButton = new Button();
            this._AdministratorOverrideModalLoadButton.ID = "AdministratorOverrideModalLoadButton";
            this._AdministratorOverrideModalLoadButton.Style.Add("display", "none");
            this._AdministratorOverrideModalLoadButton.Click += this._LoadAdministratorOverrideModalContent;

            // instansiate lesson data reset controls
            this._ResetLessonDataConfirmationModalLaunchButton = new Button();
            this._ResetLessonDataConfirmationModalLaunchButton.ID = "ResetLessonDataConfirmationModalLaunchButton";
            this._ResetLessonDataConfirmationModalLaunchButton.Style.Add("display", "none");

            this._ResetLessonDataConfirmationModalLoadButton = new Button();
            this._ResetLessonDataConfirmationModalLoadButton.ID = "ResetLessonDataConfirmationModalLoadButton";
            this._ResetLessonDataConfirmationModalLoadButton.Style.Add("display", "none");
            this._ResetLessonDataConfirmationModalLoadButton.Click += this._LoadResetLessonDataConfirmationModalContent;

            // attach the modal launch controls
            this.EnrollmentFormWrapperContainer.Controls.Add(this._TaskProctoringModalLaunchButton);
            this.EnrollmentFormWrapperContainer.Controls.Add(this._AdministratorOverrideModalLaunchButton);
            this.EnrollmentFormWrapperContainer.Controls.Add(this._ResetLessonDataConfirmationModalLaunchButton);

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.EnrollmentFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.EnrollmentFormWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu container
            this.UserObjectMenuContainer.Controls.Clear();

            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Enrollments;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.TheUsersCurrentCourseEnrollmentActivityIsDisplayedBelow, true);

            // build the enrollment form
            this._BuildEnrollmentForm();

            // build the enrollment form actions panel
            this._BuildEnrollmentActionsPanel();

            // build modals for lesson data proctoring, admin override, and reset
            this._BuildTaskProctoringModal();
            this._BuildAdministratorOverrideModal();
            this._BuildResetConfirmationModal();
        }
        #endregion

        #region _BuildEnrollmentForm
        private void _BuildEnrollmentForm()
        {
            // clear enrollment from container
            this.EnrollmentFormContainer.Controls.Clear();

            // get the timezone of the enrollment
            Timezone enrollmentTimezone = new Timezone(this._EnrollmentObject.IdTimezone);

            // enrollment date start value
            DateTime dateStartValue = TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(enrollmentTimezone.dotNetName));

            Label enrollmentDtStartFieldStaticValue = new Label();
            enrollmentDtStartFieldStaticValue.ID = "EnrollmentDtStartFieldStaticValue";
            enrollmentDtStartFieldStaticValue.Text = dateStartValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern) + " @ " + dateStartValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern);

            this.EnrollmentFormContainer.Controls.Add(AsentiaPage.BuildFormField("EnrollmentDtStart",
                                                                          _GlobalResources.Start,
                                                                          enrollmentDtStartFieldStaticValue.ID,
                                                                          enrollmentDtStartFieldStaticValue,
                                                                          false,
                                                                          false,
                                                                          false));

            // enrollment date expires value
            Label enrollmentDtExpiresFieldStaticValue = new Label();
            enrollmentDtExpiresFieldStaticValue.ID = "EnrollmentDtExpiresFieldStaticValue";
            enrollmentDtExpiresFieldStaticValue.Text = _GlobalResources.Never;

            if (this._EnrollmentObject.DtEffectiveExpires != null)
            {
                DateTime dateExpiresValue = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._EnrollmentObject.DtEffectiveExpires, TimeZoneInfo.FindSystemTimeZoneById(enrollmentTimezone.dotNetName));
                enrollmentDtExpiresFieldStaticValue.Text = dateExpiresValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern) + " @ " + dateExpiresValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern);
            }

            this.EnrollmentFormContainer.Controls.Add(AsentiaPage.BuildFormField("EnrollmentDtExpires",
                                                                          _GlobalResources.Expires,
                                                                          enrollmentDtExpiresFieldStaticValue.ID,
                                                                          enrollmentDtExpiresFieldStaticValue,
                                                                          false,
                                                                          false,
                                                                          false));

            // enrollment timezone value
            Label enrollmentTimezoneFieldStaticValue = new Label();
            enrollmentTimezoneFieldStaticValue.ID = "EnrollmentTimezoneFieldStaticValue";
            enrollmentTimezoneFieldStaticValue.Text = enrollmentTimezone.displayName;

            this.EnrollmentFormContainer.Controls.Add(AsentiaPage.BuildFormField("EnrollmentTimezone",
                                                                          _GlobalResources.Timezone,
                                                                          enrollmentTimezoneFieldStaticValue.ID,
                                                                          enrollmentTimezoneFieldStaticValue,
                                                                          false,
                                                                          false,
                                                                          false));

            // enrollment completion status container
            List<Control> enrollmentCompletionInputControls = new List<Control>();

            // completion status field
            this._CompletionStatus = new DropDownList();
            this._CompletionStatus.ID = "EnrollmentCompletionStatus_Field";
            this._CompletionStatus.Items.Add(new ListItem(_GlobalResources.IncompleteInProgress, Convert.ToInt32(Lesson.LessonCompletionStatus.Incomplete).ToString()));
            this._CompletionStatus.Items.Add(new ListItem(_GlobalResources.Completed, Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString()));

            if (this._EnrollmentObject.DtCompleted != null)
            { this._CompletionStatus.SelectedValue = Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString(); }
            else
            { this._CompletionStatus.SelectedValue = Convert.ToInt32(Lesson.LessonCompletionStatus.Incomplete).ToString(); }

            this._CompletionStatus.Attributes.Add("onchange", "CompletionStatusDropDownChange();");

            // disable this field if the enrollment is already completed
            if (this._EnrollmentObject.DtCompleted != null)
            {  this._CompletionStatus.Enabled = false; }

            enrollmentCompletionInputControls.Add(this._CompletionStatus);

            // "on" label
            Label enrollmentCompletionStatusFieldOnStaticValue = new Label();
            enrollmentCompletionStatusFieldOnStaticValue.ID = "OnLabel";
            enrollmentCompletionStatusFieldOnStaticValue.Text = _GlobalResources.On;
            enrollmentCompletionInputControls.Add(enrollmentCompletionStatusFieldOnStaticValue);

            // completed date field
            this._CompletionDate = new DatePicker("EnrollmentCompletionDate_Field", false, false, true);

            // always initialize the field as disabled
            this._CompletionDate.Enabled = false;

            // fill in the date if this enrollment is already completed
            if (this._EnrollmentObject.DtCompleted != null)
            { this._CompletionDate.Value = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._EnrollmentObject.DtCompleted, TimeZoneInfo.FindSystemTimeZoneById(enrollmentTimezone.dotNetName)); }

            enrollmentCompletionInputControls.Add(this._CompletionDate);

            //add the multiple contol list to main panel
            this.EnrollmentFormContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("EnrollmentCompletionStatus",
                                                                                              _GlobalResources.CompletionStatus,
                                                                                              enrollmentCompletionInputControls,
                                                                                              true,
                                                                                              true,
                                                                                              true));
        }
        #endregion

        #region _BuildEnrollmentActionsPanel
        /// <summary>
        /// Builds the container and buttons for enrollment form actions.
        /// </summary>
        private void _BuildEnrollmentActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "EnrollmentSaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._EnrollmentFormSaveButton_Command);

            // disable the save button if the enrollment is completed
            if (this._EnrollmentObject.DtCompleted != null)
            {
                this._SaveButton.CssClass = "Button ActionButton DisabledButton";
                this._SaveButton.Enabled = false; 
            }

            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._EnrollmentFormCancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the Activity listing.
        /// </summary>
        private void _BuildGrid()
        {
            this.ActivityGrid.StoredProcedure = Library.DataLesson.GridProcedure;
            this.ActivityGrid.ShowSearchBox = false;
            this.ActivityGrid.ShowRecordsPerPageSelectbox = false;
            this.ActivityGrid.AddCheckboxColumn = false;
            this.ActivityGrid.DefaultRecordsPerPage = 100;
            this.ActivityGrid.AllowPaging = false;

            this.ActivityGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.ActivityGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.ActivityGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.ActivityGrid.AddFilter("@idEnrollment", SqlDbType.Int, 4, this._EnrollmentObject.Id);
            this.ActivityGrid.IdentifierField = "idData-Lesson";
            this.ActivityGrid.DefaultSortColumn = "order";

            // data key names
            this.ActivityGrid.DataKeyNames = new string[] { "idData-Lesson", "idData-SCO", "idContentPackage", "idContentPackageOJT" };

            // columns
            GridColumn lessonTitle = new GridColumn(_GlobalResources.Lesson, "title", "title");
           
            GridColumn lessonCompletionStatus = new GridColumn(_GlobalResources.Completion, "contentCompletionStatus", true);
            lessonCompletionStatus.AddProperty(new GridColumnProperty(Lesson.LessonCompletionStatus.Unknown.ToString().ToLower(), _GlobalResources.Unknown));
            lessonCompletionStatus.AddProperty(new GridColumnProperty(Lesson.LessonCompletionStatus.NotAttempted.ToString().ToLower(), _GlobalResources.NotAttempted));
            lessonCompletionStatus.AddProperty(new GridColumnProperty(Lesson.LessonCompletionStatus.Completed.ToString().ToLower(), _GlobalResources.Completed));
            lessonCompletionStatus.AddProperty(new GridColumnProperty(Lesson.LessonCompletionStatus.Incomplete.ToString().ToLower(), _GlobalResources.Incomplete));

            GridColumn lessonSuccessStatus = new GridColumn(_GlobalResources.Success, "contentSuccessStatus", true);
            lessonSuccessStatus.AddProperty(new GridColumnProperty(Lesson.LessonSuccessStatus.Unknown.ToString().ToLower(), _GlobalResources.Unknown));
            lessonSuccessStatus.AddProperty(new GridColumnProperty(Lesson.LessonSuccessStatus.Passed.ToString().ToLower(), _GlobalResources.Passed));
            lessonSuccessStatus.AddProperty(new GridColumnProperty(Lesson.LessonSuccessStatus.Failed.ToString().ToLower(), _GlobalResources.Failed));

            GridColumn lessonScore = new GridColumn(_GlobalResources.Score, "contentScore", true); // contents of this column will be populated by the method that handles RowDataBound
            GridColumn lessonAttemptCount = new GridColumn(_GlobalResources.Attempts, null, true); // contents of this column will be populated by the method that handles RowDataBound
            GridColumn lessonElapsedTime = new GridColumn(_GlobalResources.ElapsedTime, null, true); // contents of this column will be populated by the method that handles RowDataBound
            
            GridColumn lessonCommittedContentType = new GridColumn(_GlobalResources.CommittedContentType, "contentTypeCommittedTo", true);
            lessonCommittedContentType.AddProperty(new GridColumnProperty(null, "-"));
            lessonCommittedContentType.AddProperty(new GridColumnProperty("0", "-"));
            lessonCommittedContentType.AddProperty(new GridColumnProperty(Convert.ToInt32(Lesson.LessonContentType.ContentPackage).ToString(), "<img class=\"SmallIcon\" src=\""
                                                                                                                                               + ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE,
                                                                                                                                                                        ImageFiles.EXT_PNG)
                                                                                                                                               + "\" alt=\"" + _GlobalResources.ContentPackage + "\" />"));
            lessonCommittedContentType.AddProperty(new GridColumnProperty(Convert.ToInt32(Lesson.LessonContentType.StandupTrainingModule).ToString(), "<img class=\"SmallIcon\" src=\""
                                                                                                                                                      + ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING,
                                                                                                                                                                               ImageFiles.EXT_PNG)
                                                                                                                                                      + "\" alt=\"" + _GlobalResources.InstructorLedTraining + "\" />"));
            lessonCommittedContentType.AddProperty(new GridColumnProperty(Convert.ToInt32(Lesson.LessonContentType.Task).ToString(), "<img class=\"SmallIcon\" src=\""
                                                                                                                                                   + ImageFiles.GetIconPath(ImageFiles.ICON_TASK,
                                                                                                                                                                            ImageFiles.EXT_PNG)
                                                                                                                                                   + "\" alt=\"" + _GlobalResources.Task + "\" />"));
            lessonCommittedContentType.AddProperty(new GridColumnProperty(Convert.ToInt32(Lesson.LessonContentType.OJT).ToString(), "<img class=\"SmallIcon\" src=\""
                                                                                                                                    + ImageFiles.GetIconPath(ImageFiles.ICON_OJT,
                                                                                                                                                             ImageFiles.EXT_PNG)
                                                                                                                                    + "\" alt=\"" + _GlobalResources.OJT + "\" />"));
            
            GridColumn lessonActionProctor = new GridColumn(_GlobalResources.Action, null, true); // contents of this column will be populated by the method that handles RowDataBound
            GridColumn lessonReset = new GridColumn(_GlobalResources.Reset, null, true); // contents of this column will be populated by the method that handles RowDataBound

            // add columns to data grid
            this.ActivityGrid.AddColumn(lessonTitle);
            this.ActivityGrid.AddColumn(lessonCompletionStatus);
            this.ActivityGrid.AddColumn(lessonSuccessStatus);
            this.ActivityGrid.AddColumn(lessonScore);
            this.ActivityGrid.AddColumn(lessonAttemptCount);
            this.ActivityGrid.AddColumn(lessonElapsedTime);
            this.ActivityGrid.AddColumn(lessonCommittedContentType);
            this.ActivityGrid.AddColumn(lessonActionProctor);
            this.ActivityGrid.AddColumn(lessonReset);
            this.ActivityGrid.RowDataBound += new GridViewRowEventHandler(this.ActivityGridView_RowDataBound);
        }
        #endregion

        #region ActivityGridView_RowDataBound
        /// <summary>
        /// Handles activity grid row data bind event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ActivityGridView_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idDataLesson = Convert.ToInt32(rowView["idData-Lesson"]); // will never be null
                
                // if the lesson is orphaned (lesson data record is for a lesson that doesn't exist), this could be null
                // note that this would only happen in a situation where data was migrated from Inquisiq
                int? idLesson = null;
                
                if (rowView["idLesson"] != null)
                { idLesson = Convert.ToInt32(rowView["idLesson"]); }                
                
                bool isResetOn = Convert.ToBoolean(rowView["isResetOn"]); // will never be null
                string lessonCompletionStatus = rowView["dtCompleted"].ToString();

                string contentCompletionStatus = rowView["contentCompletionStatus"].ToString();
                string contentSuccessStatus = rowView["contentSuccessStatus"].ToString();

                bool hasMultipleContentMethods = Convert.ToBoolean(rowView["hasMultipleContentMethods"]); // will never be null
                bool hasOJT = Convert.ToBoolean(rowView["hasOJT"]); // will never be null
                bool hasOnlyOJT = false;

                if (!hasMultipleContentMethods && hasOJT)
                { hasOnlyOJT = true; }
                
                int? idDataTask = null;
                if (!String.IsNullOrWhiteSpace(rowView["idData-Task"].ToString()))
                { idDataTask = Convert.ToInt32(rowView["idData-Task"]); }

                string uploadedTaskFilename = rowView["uploadedTaskFilename"].ToString();
                string taskUploadedDate = rowView["taskUploadedDate"].ToString();

                Lesson.LessonContentType? contentTypeCommittedTo = null;
                if (!String.IsNullOrWhiteSpace(rowView["contentTypeCommittedTo"].ToString()))
                { contentTypeCommittedTo = (Lesson.LessonContentType)Convert.ToInt32(rowView["contentTypeCommittedTo"]); }

                // SCORE
                // if no score, replace with "-"; otherwise, append 
                string contentScore = rowView["contentScore"].ToString();

                if (String.IsNullOrWhiteSpace(contentScore))
                { e.Row.Cells[3].Text = "-"; }
                else
                { e.Row.Cells[3].Text += "%"; }

                // ATTEMPTS
                // if no attempts, replace with "-"
                string attemptCount = rowView["attemptCount"].ToString();

                if (String.IsNullOrWhiteSpace(attemptCount) || attemptCount == "0")
                { e.Row.Cells[4].Text = "-"; }
                else
                { e.Row.Cells[4].Text = attemptCount; }

                // ELAPSED TIME
                float? elapsedTime = null;

                if (!String.IsNullOrWhiteSpace(rowView["contentPackageTotalTime"].ToString()))
                { elapsedTime = Convert.ToSingle(rowView["contentPackageTotalTime"].ToString()); }

                if (elapsedTime == null || elapsedTime == 0)
                { e.Row.Cells[5].Text = "-"; }
                else
                {
                    int elapsedTimeInt = (int)elapsedTime;
                    
                    string hh = "00";
                    int h = 0;
                    
                    string mm = "00";
                    int m = 0;
                    
                    string ss = "00";
                    int s = 0;

                    // hours
                    if (elapsedTimeInt >= 3600)
                    {
                        h = (elapsedTimeInt / 3600);
                        elapsedTimeInt = elapsedTimeInt - (h * 3600);
                    }

                    if (h < 10)
                    { hh = "0" + h.ToString(); }
                    else
                    { hh = h.ToString(); }

                    // minutes
                    if (elapsedTimeInt >= 60)
                    {
                        m = (elapsedTimeInt / 60);
                        elapsedTimeInt = elapsedTimeInt - (m * 60);
                    }

                    if (m < 10)
                    { mm = "0" + m.ToString(); }
                    else
                    { mm = m.ToString(); }

                    // seconds - that's what's remaining in elapsedTimeInt
                    s = elapsedTimeInt;

                    if (s < 10)
                    { ss = "0" + s.ToString(); }
                    else
                    { ss = s.ToString(); }

                    // display the time in the cell
                    e.Row.Cells[5].Text = hh + ":" + mm + ":" + ss;
                }

                // ACTION/PROCTOR

                // if the course or lesson have been completed, or the lesson is orphaned, apply a "no action" button
                // else, evaluate each lesson and apply appropriate action/override links
                if (this._EnrollmentObject.DtCompleted != null || !String.IsNullOrWhiteSpace(lessonCompletionStatus) || idLesson == null)
                {
                    // no action button
                    LinkButton noActionLink = new LinkButton();
                    noActionLink.ID = "NoActionLink_" + idDataLesson.ToString();
                    noActionLink.OnClientClick = "return false;";

                    Image noActionLinkImage = new Image();
                    noActionLinkImage.ID = "NoActionLinkImage_" + idDataLesson.ToString();
                    noActionLinkImage.CssClass = "SmallIcon";
                    noActionLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                    noActionLinkImage.AlternateText = _GlobalResources.AdministratorOverride;

                    // disable it
                    noActionLink.Enabled = false;
                    noActionLinkImage.CssClass += " DimIcon";

                    noActionLink.Controls.Add(noActionLinkImage);

                    e.Row.Cells[7].Controls.Add(noActionLink);
                }
                else
                {
                    // nothing (null), or content package -- allow admin to override and just mark complete (content committed type will then become 0)
                    // links for that go in "Completion" and "Success" columns
                    // if nothing (null) and the lesson has only an OJT, add a button for launching the OJT
                    if (contentTypeCommittedTo == null || contentTypeCommittedTo == Lesson.LessonContentType.ContentPackage)
                    {                        
                        // text links on "Completion" and "Success" columns

                        // "Completion" column
                        LinkButton adminOverrideLinkCompletion = new LinkButton();
                        adminOverrideLinkCompletion.ID = "AdminOverrideLinkCompletion_" + idDataLesson.ToString();
                        adminOverrideLinkCompletion.OnClientClick = "LoadAdministratorOverrideModalContent(" + this._EnrollmentObject.IdUser + ", " + this._EnrollmentObject.Id + ", " + idDataLesson + ", " + idLesson + "); return false;";
                        adminOverrideLinkCompletion.ToolTip = _GlobalResources.AdministratorOverride;

                        if (contentCompletionStatus == Lesson.LessonCompletionStatus.NotAttempted.ToString().ToLower())
                        { adminOverrideLinkCompletion.Text = _GlobalResources.NotAttempted; }
                        else if (contentCompletionStatus == Lesson.LessonCompletionStatus.Completed.ToString().ToLower())
                        { adminOverrideLinkCompletion.Text = _GlobalResources.Completed; }
                        else if (contentCompletionStatus == Lesson.LessonCompletionStatus.Incomplete.ToString().ToLower())
                        { adminOverrideLinkCompletion.Text = _GlobalResources.Incomplete; }
                        else
                        { adminOverrideLinkCompletion.Text = _GlobalResources.Unknown; }

                        e.Row.Cells[1].Controls.Add(adminOverrideLinkCompletion);

                        // "Success" column
                        LinkButton adminOverrideLinkSuccess = new LinkButton();
                        adminOverrideLinkSuccess.ID = "AdminOverrideLinkSuccess_" + idDataLesson.ToString();
                        adminOverrideLinkSuccess.OnClientClick = "LoadAdministratorOverrideModalContent(" + this._EnrollmentObject.IdUser + ", " + this._EnrollmentObject.Id + ", " + idDataLesson + ", " + idLesson + "); return false;";
                        adminOverrideLinkSuccess.ToolTip = _GlobalResources.AdministratorOverride;

                        if (contentSuccessStatus == Lesson.LessonSuccessStatus.Passed.ToString().ToLower())
                        { adminOverrideLinkSuccess.Text = _GlobalResources.Passed; }
                        else if (contentSuccessStatus == Lesson.LessonSuccessStatus.Failed.ToString().ToLower())
                        { adminOverrideLinkSuccess.Text = _GlobalResources.Failed; }
                        else
                        { adminOverrideLinkSuccess.Text = _GlobalResources.Unknown; }

                        e.Row.Cells[2].Controls.Add(adminOverrideLinkSuccess);

                        // if nothing has been committed to and there is only an ojt, add ojt launch button to "Action" column
                        // otherwise, add a "no action" button
                        if (contentTypeCommittedTo == null && hasOnlyOJT)
                        {
                            // build the OJT launch link
                            ContentPackage.ContentPackageType contentPackageTypeOJT = (ContentPackage.ContentPackageType)Convert.ToInt32(rowView["contentPackageTypeOJT"]); // will never be null, if we get here
                            int idContentPackageOJT = Convert.ToInt32(rowView["idContentPackageOJT"]); // will never be null, if we get here

                            if (contentPackageTypeOJT == ContentPackage.ContentPackageType.SCORM)
                            {
                                HyperLink launchOJTPackageLink = new HyperLink();
                                launchOJTPackageLink.ID = "LaunchOJTPackageLink_" + idDataLesson.ToString();
                                launchOJTPackageLink.NavigateUrl = "/launch/Scorm.aspx?id=" + idDataLesson.ToString() + "&cpid=" + idContentPackageOJT.ToString() + "&launchType=ojt";
                                launchOJTPackageLink.ToolTip = _GlobalResources.LaunchOJT;

                                Image launchOJTPackageLinkImage = new Image();
                                launchOJTPackageLinkImage.ID = "LaunchOJTPackageLinkImage_" + idDataLesson.ToString();
                                launchOJTPackageLinkImage.CssClass = "SmallIcon";
                                launchOJTPackageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                                launchOJTPackageLinkImage.AlternateText = _GlobalResources.OJT;

                                launchOJTPackageLink.Controls.Add(launchOJTPackageLinkImage);

                                e.Row.Cells[7].Controls.Add(launchOJTPackageLink);
                            }
                            else if (contentPackageTypeOJT == ContentPackage.ContentPackageType.xAPI)
                            {
                                HyperLink launchOJTPackageLink = new HyperLink();
                                launchOJTPackageLink.ID = "LaunchOJTPackageLink_" + idDataLesson.ToString();
                                launchOJTPackageLink.NavigateUrl = "/launch/Tincan.aspx?id=" + idDataLesson.ToString() + "&cpid=" + idContentPackageOJT.ToString() + "&launchType=ojt";
                                launchOJTPackageLink.ToolTip = _GlobalResources.LaunchOJT;

                                Image launchOJTPackageLinkImage = new Image();
                                launchOJTPackageLinkImage.ID = "LaunchOJTPackageLinkImage_" + idDataLesson.ToString();
                                launchOJTPackageLinkImage.CssClass = "SmallIcon";
                                launchOJTPackageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                                launchOJTPackageLinkImage.AlternateText = _GlobalResources.OJT;

                                launchOJTPackageLink.Controls.Add(launchOJTPackageLinkImage);

                                e.Row.Cells[7].Controls.Add(launchOJTPackageLink);
                            }
                        }
                        else
                        {
                            // no action button
                            LinkButton noActionLink = new LinkButton();
                            noActionLink.ID = "NoActionLink_" + idDataLesson.ToString();
                            noActionLink.OnClientClick = "return false;";

                            Image noActionLinkImage = new Image();
                            noActionLinkImage.ID = "NoActionLinkImage_" + idDataLesson.ToString();
                            noActionLinkImage.CssClass = "SmallIcon";
                            noActionLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                            noActionLinkImage.AlternateText = _GlobalResources.AdministratorOverride;

                            // disable it
                            noActionLink.Enabled = false;
                            noActionLinkImage.CssClass += " DimIcon";

                            noActionLink.Controls.Add(noActionLinkImage);

                            e.Row.Cells[7].Controls.Add(noActionLink);
                        }
                    }

                    // admin override -- admin has already overridden content and marked as completed, do not allow action
                    // reset frees this up for learner to take content, or admin to override with different completion data
                    // OR
                    // instructor led training -- do not allow action, this is marked from the manage roster page in ILT session
                    if (contentTypeCommittedTo == Lesson.LessonContentType.AdministratorOverride || contentTypeCommittedTo == Lesson.LessonContentType.StandupTrainingModule)
                    {
                        // no action button
                        LinkButton noActionLink = new LinkButton();
                        noActionLink.ID = "NoActionLink_" + idDataLesson.ToString();
                        noActionLink.OnClientClick = "return false;";

                        Image noActionLinkImage = new Image();
                        noActionLinkImage.ID = "NoActionLinkImage_" + idDataLesson.ToString();
                        noActionLinkImage.CssClass = "SmallIcon";
                        noActionLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                        noActionLinkImage.AlternateText = _GlobalResources.AdministratorOverride;

                        // disable it
                        noActionLink.Enabled = false;
                        noActionLinkImage.CssClass += " DimIcon";

                        noActionLink.Controls.Add(noActionLinkImage);

                        e.Row.Cells[7].Controls.Add(noActionLink);
                    }

                    // task -- allow for grading of task from here
                    if (contentTypeCommittedTo == Lesson.LessonContentType.Task)
                    {
                        // button
                        LinkButton proctorTaskLink = new LinkButton();
                        proctorTaskLink.ID = "ProctorTaskLink_" + idDataLesson.ToString();
                        proctorTaskLink.OnClientClick = "LoadTaskProctoringModalContent(" + this._EnrollmentObject.IdUser + ", " + this._EnrollmentObject.Id + ", " + idDataLesson + ", " + idDataTask + ", " + idLesson + ", \"" + uploadedTaskFilename + "\", \"" + taskUploadedDate + "\"); return false;";
                        proctorTaskLink.ToolTip = _GlobalResources.ProctorTask;

                        Image proctorTaskLinkImage = new Image();
                        proctorTaskLinkImage.ID = "ProctorTaskLinkImage_" + idDataLesson.ToString();
                        proctorTaskLinkImage.CssClass = "SmallIcon";
                        proctorTaskLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                        proctorTaskLinkImage.AlternateText = _GlobalResources.Task;

                        proctorTaskLink.Controls.Add(proctorTaskLinkImage);

                        e.Row.Cells[7].Controls.Add(proctorTaskLink);
                    }

                    // ojt -- allow direct launch of ojt from here
                    if (contentTypeCommittedTo == Lesson.LessonContentType.OJT)
                    {
                        ContentPackage.ContentPackageType contentPackageTypeOJT = (ContentPackage.ContentPackageType)Convert.ToInt32(rowView["contentPackageTypeOJT"]); // will never be null, if we get here
                        int idContentPackageOJT = Convert.ToInt32(rowView["idContentPackageOJT"]); // will never be null, if we get here

                        if (contentPackageTypeOJT == ContentPackage.ContentPackageType.SCORM)
                        {
                            HyperLink launchOJTPackageLink = new HyperLink();
                            launchOJTPackageLink.ID = "LaunchOJTPackageLink_" + idDataLesson.ToString();
                            launchOJTPackageLink.NavigateUrl = "/launch/Scorm.aspx?id=" + idDataLesson.ToString() + "&cpid=" + idContentPackageOJT.ToString() + "&launchType=ojt";
                            launchOJTPackageLink.ToolTip = _GlobalResources.LaunchOJT;

                            Image launchOJTPackageLinkImage = new Image();
                            launchOJTPackageLinkImage.ID = "LaunchOJTPackageLinkImage_" + idDataLesson.ToString();
                            launchOJTPackageLinkImage.CssClass = "SmallIcon";
                            launchOJTPackageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                            launchOJTPackageLinkImage.AlternateText = _GlobalResources.OJT;

                            launchOJTPackageLink.Controls.Add(launchOJTPackageLinkImage);

                            e.Row.Cells[7].Controls.Add(launchOJTPackageLink);
                        }
                        else if (contentPackageTypeOJT == ContentPackage.ContentPackageType.xAPI)
                        {
                            HyperLink launchOJTPackageLink = new HyperLink();
                            launchOJTPackageLink.ID = "LaunchOJTPackageLink_" + idDataLesson.ToString();
                            launchOJTPackageLink.NavigateUrl = "/launch/Tincan.aspx?id=" + idDataLesson.ToString() + "&cpid=" + idContentPackageOJT.ToString() + "&launchType=ojt";
                            launchOJTPackageLink.ToolTip = _GlobalResources.LaunchOJT;

                            Image launchOJTPackageLinkImage = new Image();
                            launchOJTPackageLinkImage.ID = "LaunchOJTPackageLinkImage_" + idDataLesson.ToString();
                            launchOJTPackageLinkImage.CssClass = "SmallIcon";
                            launchOJTPackageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                            launchOJTPackageLinkImage.AlternateText = _GlobalResources.OJT;

                            launchOJTPackageLink.Controls.Add(launchOJTPackageLinkImage);

                            e.Row.Cells[7].Controls.Add(launchOJTPackageLink);
                        }
                    }
                }

                // RESET

                if (isResetOn)
                {
                    // button
                    LinkButton lessonDataResetLink = new LinkButton();
                    lessonDataResetLink.ID = "LessonDataResetLink_" + idDataLesson.ToString();
                    lessonDataResetLink.OnClientClick = "LoadResetLessonDataConfirmationModalContent(" + this._EnrollmentObject.IdUser + ", " + this._EnrollmentObject.Id + ", " + idDataLesson + "); return false;";
                    lessonDataResetLink.ToolTip = _GlobalResources.ResetModuleData;

                    Image lessonDataResetLinkImage = new Image();
                    lessonDataResetLinkImage.ID = "LessonDataResetLinkImage_" + idDataLesson.ToString();
                    lessonDataResetLinkImage.CssClass = "SmallIcon";
                    lessonDataResetLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RESET, ImageFiles.EXT_PNG);
                    lessonDataResetLinkImage.AlternateText = _GlobalResources.Reset;

                    lessonDataResetLink.Controls.Add(lessonDataResetLinkImage);

                    e.Row.Cells[8].Controls.Add(lessonDataResetLink);
                }
                else
                {
                    // button
                    LinkButton lessonDataResetLink = new LinkButton();
                    lessonDataResetLink.ID = "LessonDataResetLink_" + idDataLesson.ToString();
                    lessonDataResetLink.OnClientClick = "return false;";

                    Image lessonDataResetLinkImage = new Image();
                    lessonDataResetLinkImage.ID = "LessonDataResetLinkImage_" + idDataLesson.ToString();
                    lessonDataResetLinkImage.CssClass = "SmallIcon";
                    lessonDataResetLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RESET, ImageFiles.EXT_PNG);
                    lessonDataResetLinkImage.AlternateText = _GlobalResources.Reset;

                    // disable it
                    lessonDataResetLink.Enabled = false;
                    lessonDataResetLinkImage.CssClass += " DimIcon";

                    lessonDataResetLink.Controls.Add(lessonDataResetLinkImage);

                    e.Row.Cells[8].Controls.Add(lessonDataResetLink);
                }
            }
        }
        #endregion

        #region _BuildTaskProctoringModal
        /// <summary>
        /// 
        /// </summary>
        private void _BuildTaskProctoringModal()
        {
            // set modal properties
            this._TaskProctoringModal = new ModalPopup("TaskProctoringModal");
            this._TaskProctoringModal.Type = ModalPopupType.Form;
            this._TaskProctoringModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_TASK, ImageFiles.EXT_PNG);
            this._TaskProctoringModal.HeaderIconAlt = _GlobalResources.ProctorTask;
            this._TaskProctoringModal.HeaderText = _GlobalResources.ProctorTask;
            this._TaskProctoringModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._TaskProctoringModal.TargetControlID = this._TaskProctoringModalLaunchButton.ID;
            this._TaskProctoringModal.SubmitButton.Command += new CommandEventHandler(this._TaskProctoringSubmit_Command);

            // build the modal form panel
            Panel taskModalFormPanel = new Panel();
            taskModalFormPanel.ID = "TaskProctoringModalFormPanel";
            taskModalFormPanel.Visible = false; // don't render this, we will render in the _LoadTaskProctoringModalContent method

            // note, we need to add the controls for setting completion, status, and score so that they register properly in the control stack

            // completion status

            DropDownList completionStatusDropDown = new DropDownList();
            completionStatusDropDown.ID = "TaskProctoringModal_CompletionStatus_Field";
            completionStatusDropDown.Items.Add(new ListItem(_GlobalResources.IncompleteInProgress, Convert.ToInt32(Lesson.LessonCompletionStatus.Incomplete).ToString()));
            completionStatusDropDown.Items.Add(new ListItem(_GlobalResources.Completed, Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString()));

            taskModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("TaskProctoringModal_CompletionStatus",
                                                                       _GlobalResources.CompletionStatus,
                                                                       completionStatusDropDown.ID,
                                                                       completionStatusDropDown,
                                                                       false,
                                                                       false,
                                                                       false));

            // success status

            DropDownList successStatusDropDown = new DropDownList();
            successStatusDropDown.ID = "TaskProctoringModal_SuccessStatus_Field";
            successStatusDropDown.Items.Add(new ListItem(_GlobalResources.Unknown, Convert.ToInt32(Lesson.LessonSuccessStatus.Unknown).ToString()));
            successStatusDropDown.Items.Add(new ListItem(_GlobalResources.Passed, Convert.ToInt32(Lesson.LessonSuccessStatus.Passed).ToString()));
            successStatusDropDown.Items.Add(new ListItem(_GlobalResources.Failed, Convert.ToInt32(Lesson.LessonSuccessStatus.Failed).ToString()));

            taskModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("TaskProctoringModal_SuccessStatus",
                                                                       _GlobalResources.SuccessStatus,
                                                                       successStatusDropDown.ID,
                                                                       successStatusDropDown,
                                                                       false,
                                                                       false,
                                                                       false));

            // score

            TextBox score = new TextBox();
            score.ID = "TaskProctoringModal_Score_Field";
            score.CssClass = "InputXShort";

            taskModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("TaskProctoringModal_Score",
                                                                       _GlobalResources.Score,
                                                                       score.ID,
                                                                       score,
                                                                       false,
                                                                       true,
                                                                       false));

            // build the task data hidden field
            HiddenField taskData = new HiddenField();
            taskData.ID = "TaskData";

            // build the modal body
            this._TaskProctoringModal.AddControlToBody(taskModalFormPanel);
            this._TaskProctoringModal.AddControlToBody(this._TaskProctoringModalLoadButton);
            this._TaskProctoringModal.AddControlToBody(taskData);

            // add modal to container
            this.EnrollmentFormWrapperContainer.Controls.Add(this._TaskProctoringModal);
        }
        #endregion

        #region _LoadTaskProctoringModalContent
        /// <summary>
        /// Loads content for task proctoring modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadTaskProctoringModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField taskData = (HiddenField)this._TaskProctoringModal.FindControl("TaskData");
            Panel taskModalFormPanel = (Panel)this._TaskProctoringModal.FindControl("TaskProctoringModalFormPanel");

            try
            {
                // clear the modal feedback
                this._TaskProctoringModal.ClearFeedback();

                // make the submit and close buttons visible
                this._TaskProctoringModal.SubmitButton.Visible = true;
                this._TaskProctoringModal.CloseButton.Visible = true;

                // if the sender is TaskProctoringModalLoadButton, reset the completion, success, and score form fields
                Button senderButton = (Button)sender;

                if (senderButton.ID == "TaskProctoringModalLoadButton")
                {
                    DropDownList completionStatusData = (DropDownList)taskModalFormPanel.FindControl("TaskProctoringModal_CompletionStatus_Field");
                    DropDownList successStatusData = (DropDownList)taskModalFormPanel.FindControl("TaskProctoringModal_SuccessStatus_Field");
                    TextBox scoreData = (TextBox)taskModalFormPanel.FindControl("TaskProctoringModal_Score_Field");

                    completionStatusData.SelectedIndex = 0;
                    successStatusData.SelectedIndex = 0;
                    scoreData.Text = String.Empty;
                }

                // get the user id, enrollment id, lesson data id, task id, lesson id, uploaded task filename, and task uploaded date
                string[] taskDataItems = taskData.Value.Split('|');
                int idUser = Convert.ToInt32(taskDataItems[0]);
                int idEnrollment = Convert.ToInt32(taskDataItems[1]);
                int idDataLesson = Convert.ToInt32(taskDataItems[2]);
                int idDataTask = Convert.ToInt32(taskDataItems[3]);
                int idLesson = Convert.ToInt32(taskDataItems[4]);


                string uploadedTaskFilename = taskDataItems[5];

                DateTime? taskUploadedDate = null;
                if (!String.IsNullOrWhiteSpace(taskDataItems[6]))
                {
                    taskUploadedDate = Convert.ToDateTime(taskDataItems[6]);

                    // convert to user's local time
                    taskUploadedDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)taskUploadedDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                }

                // build the form
                if (!String.IsNullOrWhiteSpace(uploadedTaskFilename))
                {
                    Panel taskInstructionsPanel = new Panel();
                    taskInstructionsPanel.ID = "TaskProctoringModal_TaskInstructionsPanel";
                    this.FormatPageInformationPanel(taskInstructionsPanel, _GlobalResources.ViewTheSubmittedTaskAndGradeItUsingTheFormBelow, true);
                    taskModalFormPanel.Controls.AddAt(0, taskInstructionsPanel); // we use AddAt because there are other controls already in the panel, and we want this to render before them

                    // UPLOADED TASK

                    List<Control> uploadedTaskControls = new List<Control>();

                    // uploaded task link
                    HyperLink uploadedTaskLink = new HyperLink();
                    uploadedTaskLink.NavigateUrl = SitePathConstants.SITE_USERS_ROOT + idUser.ToString() + "/Tasks/" + idDataLesson.ToString() + "/" + uploadedTaskFilename;
                    uploadedTaskLink.Target = "_blank";
                    uploadedTaskLink.Text = uploadedTaskFilename;
                    uploadedTaskControls.Add(uploadedTaskLink);

                    // uploaded on
                    Panel uploadedTaskUploadDateContainer = new Panel();
                    uploadedTaskUploadDateContainer.ID = "TaskProctoringModal_UploadedTaskUploadDateContainer";

                    Literal uploadedTaskUploadDate = new Literal();
                    uploadedTaskUploadDate.Text = _GlobalResources.TheTaskWasSubmittedOn + " " + taskUploadedDate.ToString();
                    uploadedTaskUploadDateContainer.Controls.Add(uploadedTaskUploadDate);

                    uploadedTaskControls.Add(uploadedTaskUploadDateContainer);

                    // we use AddAt because there are other controls already in the panel, and we want this to render before them
                    taskModalFormPanel.Controls.AddAt(1, AsentiaPage.BuildMultipleInputControlFormField("TaskProctoringModal_UploadedTask",
                                                                                                        _GlobalResources.UploadedTask,
                                                                                                        uploadedTaskControls,
                                                                                                        false,
                                                                                                        false));

                    // set the form panel so it will render
                    taskModalFormPanel.Visible = true;
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(dnfEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(fnuEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(cpeEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(dEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(ex.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _TaskProctoringSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the task proctoring modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _TaskProctoringSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField taskData = (HiddenField)this._TaskProctoringModal.FindControl("TaskData");
            Panel taskModalFormPanel = (Panel)this._TaskProctoringModal.FindControl("TaskProctoringModalFormPanel");

            try
            {
                // clear the modal feedback
                this._TaskProctoringModal.ClearFeedback();

                // get the enrollment id, lesson data id, and task id
                string[] taskDataItems = taskData.Value.Split('|');
                int idEnrollment = Convert.ToInt32(taskDataItems[1]);
                int idDataLesson = Convert.ToInt32(taskDataItems[2]);
                int idDataTask = Convert.ToInt32(taskDataItems[3]);

                // get the values from the completion, success, and score controls

                DropDownList completionStatusData = (DropDownList)taskModalFormPanel.FindControl("TaskProctoringModal_CompletionStatus_Field");
                DropDownList successStatusData = (DropDownList)taskModalFormPanel.FindControl("TaskProctoringModal_SuccessStatus_Field");
                TextBox scoreData = (TextBox)taskModalFormPanel.FindControl("TaskProctoringModal_Score_Field");

                int completion = Convert.ToInt32(completionStatusData.SelectedValue);
                int success = Convert.ToInt32(successStatusData.SelectedValue);
                int? score = null;

                // validate the data and save

                // if success is passed, completion must be completed
                if ((Lesson.LessonSuccessStatus)success == Lesson.LessonSuccessStatus.Passed && (Lesson.LessonCompletionStatus)completion != Lesson.LessonCompletionStatus.Completed)
                {
                    this._LoadTaskProctoringModalContent(sender, e);
                    this._TaskProctoringModal.DisplayFeedback(_GlobalResources.WhenSuccessStatusIsMarkedAsPassedCompletionStatusMustBeMarkedAsCompleted, true);
                }
                else
                {
                    bool isValid = true;

                    // validate score
                    int parsedScore = 0;

                    if ((!String.IsNullOrWhiteSpace(scoreData.Text) && !int.TryParse(scoreData.Text, out parsedScore)))
                    {
                        isValid = false;
                        this._LoadTaskProctoringModalContent(sender, e);
                        this.ApplyErrorMessageToFieldErrorPanel(taskModalFormPanel, "TaskProctoringModal_Score", _GlobalResources.Score + " " + _GlobalResources.IsInvalid);
                        this._TaskProctoringModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                    }
                    else
                    { 
                        // now if the score is not empty, validate value between 0 and 100
                        if (!String.IsNullOrWhiteSpace(scoreData.Text))
                        {
                            if (parsedScore >= 0 && parsedScore <= 100)
                            { score = parsedScore; }
                            else
                            {
                                isValid = false;
                                this._LoadTaskProctoringModalContent(sender, e);
                                this.ApplyErrorMessageToFieldErrorPanel(taskModalFormPanel, "TaskProctoringModal_Score", _GlobalResources.Score + " " + _GlobalResources.IsInvalid);
                                this._TaskProctoringModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                            }
                        }
                    }

                    if (isValid)
                    {
                        DataLesson.ProctorTask(idEnrollment, idDataLesson, idDataTask, completion, success, score);

                        taskModalFormPanel.Controls.Clear();
                        this._TaskProctoringModal.SubmitButton.Visible = false;
                        this._TaskProctoringModal.CloseButton.Visible = false;
                        this._TaskProctoringModal.DisplayFeedback(_GlobalResources.TheTaskDataHasBeenSavedSuccessfully, false);

                        // rebind the activity data grid and update
                        this.ActivityGrid.BindData();
                        this.ActivityGridUpdatePanel.Update();
                    }
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(dnfEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(fnuEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(cpeEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(dEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._TaskProctoringModal.DisplayFeedback(ex.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskProctoringModal.SubmitButton.Visible = false;
                this._TaskProctoringModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildAdministratorOverrideModal
        /// <summary>
        /// 
        /// </summary>
        private void _BuildAdministratorOverrideModal()
        {
            // set modal properties
            this._AdministratorOverrideModal = new ModalPopup("AdministratorOverrideModal");
            this._AdministratorOverrideModal.Type = ModalPopupType.Form;
            this._AdministratorOverrideModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LESSON, ImageFiles.EXT_PNG);
            this._AdministratorOverrideModal.HeaderIconAlt = _GlobalResources.OverrideModuleCompletion;
            this._AdministratorOverrideModal.HeaderText = _GlobalResources.OverrideModuleCompletion;
            this._AdministratorOverrideModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._AdministratorOverrideModal.TargetControlID = this._AdministratorOverrideModalLaunchButton.ID;
            this._AdministratorOverrideModal.SubmitButton.Command += new CommandEventHandler(this._AdministratorOverrideSubmit_Command);

            // build the modal form panel
            Panel administratorOverrideModalFormPanel = new Panel();
            administratorOverrideModalFormPanel.ID = "AdministratorOverrideModalFormPanel";

            // note, we need to add the control for setting completion, and timestamp so that they register properly in the control stack

            // completion status

            DropDownList completionStatusDropDown = new DropDownList();
            completionStatusDropDown.ID = "AdministratorOverrideModal_CompletionStatus_Field";
            //completionStatusDropDown.Items.Add(new ListItem(_GlobalResources.IncompleteInProgress, Convert.ToInt32(Lesson.LessonCompletionStatus.Incomplete).ToString()));
            completionStatusDropDown.Items.Add(new ListItem(_GlobalResources.Completed, Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString()));

            administratorOverrideModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("AdministratorOverrideModal_CompletionStatus",
                                                                                        _GlobalResources.CompletionStatus,
                                                                                        completionStatusDropDown.ID,
                                                                                        completionStatusDropDown,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // success status

            DropDownList successStatusDropDown = new DropDownList();
            successStatusDropDown.ID = "AdministratorOverrideModal_SuccessStatus_Field";
            successStatusDropDown.Items.Add(new ListItem(_GlobalResources.Unknown, Convert.ToInt32(Lesson.LessonSuccessStatus.Unknown).ToString()));
            successStatusDropDown.Items.Add(new ListItem(_GlobalResources.Passed, Convert.ToInt32(Lesson.LessonSuccessStatus.Passed).ToString()));
            //successStatusDropDown.Items.Add(new ListItem(_GlobalResources.Failed, Convert.ToInt32(Lesson.LessonSuccessStatus.Failed).ToString()));

            administratorOverrideModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("AdministratorOverrideModal_SuccessStatus",
                                                                                        _GlobalResources.SuccessStatus,
                                                                                        successStatusDropDown.ID,
                                                                                        successStatusDropDown,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // score

            TextBox score = new TextBox();
            score.ID = "AdministratorOverrideModal_Score_Field";
            score.CssClass = "InputXShort";

            administratorOverrideModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("AdministratorOverrideModal_Score",
                                                                                        _GlobalResources.Score,
                                                                                        score.ID,
                                                                                        score,
                                                                                        false,
                                                                                        true,
                                                                                        false));

            // timestamp

            DatePicker timestamp = new DatePicker("AdministratorOverrideModal_Timestamp_Field", false, true, true);

            administratorOverrideModalFormPanel.Controls.Add(AsentiaPage.BuildFormField("AdministratorOverrideModal_Timestamp",
                                                                                        _GlobalResources.Timestamp,
                                                                                        timestamp.ID,
                                                                                        timestamp,
                                                                                        false,
                                                                                        true,
                                                                                        false));

            // build the administrator override data hidden field
            HiddenField administratorOverrideData = new HiddenField();
            administratorOverrideData.ID = "AdministratorOverrideData";

            // build the modal body
            this._AdministratorOverrideModal.AddControlToBody(administratorOverrideModalFormPanel);
            this._AdministratorOverrideModal.AddControlToBody(this._AdministratorOverrideModalLoadButton);
            this._AdministratorOverrideModal.AddControlToBody(administratorOverrideData);

            // add modal to container
            this.EnrollmentFormWrapperContainer.Controls.Add(this._AdministratorOverrideModal);
        }
        #endregion

        #region _LoadAdministratorOverrideModalContent
        /// <summary>
        /// Loads content for lesson administrator override modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadAdministratorOverrideModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField administratorOverrideData = (HiddenField)this._AdministratorOverrideModal.FindControl("AdministratorOverrideData");
            Panel administratorOverrideModalFormPanel = (Panel)this._AdministratorOverrideModal.FindControl("AdministratorOverrideModalFormPanel");

            try
            {
                // clear the modal feedback
                this._AdministratorOverrideModal.ClearFeedback();

                // make the submit and close buttons visible
                this._AdministratorOverrideModal.SubmitButton.Visible = true;
                this._AdministratorOverrideModal.CloseButton.Visible = true;

                // if the sender is AdministratorOverrideModalLoadButton, reset the completion, success, and score form fields
                Button senderButton = (Button)sender;

                if (senderButton.ID == "AdministratorOverrideModalLoadButton")
                {
                    DropDownList completionStatusData = (DropDownList)administratorOverrideModalFormPanel.FindControl("AdministratorOverrideModal_CompletionStatus_Field");
                    DropDownList successStatusData = (DropDownList)administratorOverrideModalFormPanel.FindControl("AdministratorOverrideModal_SuccessStatus_Field");
                    TextBox score = (TextBox)administratorOverrideModalFormPanel.FindControl("AdministratorOverrideModal_Score_Field");
                    DatePicker timestamp = (DatePicker)administratorOverrideModalFormPanel.FindControl("AdministratorOverrideModal_Timestamp_Field");

                    completionStatusData.SelectedIndex = 0;
                    successStatusData.SelectedIndex = 0;
                    timestamp.Value = null;
                }

                // get the user id, enrollment id, lesson data id, and lesson id
                string[] administratorOverrideDataItems = administratorOverrideData.Value.Split('|');
                int idUser = Convert.ToInt32(administratorOverrideDataItems[0]);
                int idEnrollment = Convert.ToInt32(administratorOverrideDataItems[1]);
                int idDataLesson = Convert.ToInt32(administratorOverrideDataItems[2]);
                int idLesson = Convert.ToInt32(administratorOverrideDataItems[3]);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AdministratorOverrideModal.DisplayFeedback(dnfEx.Message, true);
                administratorOverrideModalFormPanel.Controls.Clear();
                this._AdministratorOverrideModal.SubmitButton.Visible = false;
                this._AdministratorOverrideModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._AdministratorOverrideModal.DisplayFeedback(fnuEx.Message, true);
                administratorOverrideModalFormPanel.Controls.Clear();
                this._AdministratorOverrideModal.SubmitButton.Visible = false;
                this._AdministratorOverrideModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AdministratorOverrideModal.DisplayFeedback(cpeEx.Message, true);
                administratorOverrideModalFormPanel.Controls.Clear();
                this._AdministratorOverrideModal.SubmitButton.Visible = false;
                this._AdministratorOverrideModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AdministratorOverrideModal.DisplayFeedback(dEx.Message, true);
                administratorOverrideModalFormPanel.Controls.Clear();
                this._AdministratorOverrideModal.SubmitButton.Visible = false;
                this._AdministratorOverrideModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AdministratorOverrideModal.DisplayFeedback(ex.Message, true);
                administratorOverrideModalFormPanel.Controls.Clear();
                this._AdministratorOverrideModal.SubmitButton.Visible = false;
                this._AdministratorOverrideModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _AdministratorOverrideSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the lesson administrator override modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _AdministratorOverrideSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField administratorOverrideData = (HiddenField)this._AdministratorOverrideModal.FindControl("AdministratorOverrideData");
            Panel administratorOverrideModalFormPanel = (Panel)this._AdministratorOverrideModal.FindControl("AdministratorOverrideModalFormPanel");

            try
            {
                // clear the modal feedback
                this._AdministratorOverrideModal.ClearFeedback();

                // get the enrollment id, lesson data id, and lesson id
                string[] administratorOverrideDataItems = administratorOverrideData.Value.Split('|');
                int idEnrollment = Convert.ToInt32(administratorOverrideDataItems[1]);
                int idDataLesson = Convert.ToInt32(administratorOverrideDataItems[2]);
                int idLesson = Convert.ToInt32(administratorOverrideDataItems[3]);

                // get the values from the completion, success, score, and timestamp controls

                DropDownList completionStatusData = (DropDownList)administratorOverrideModalFormPanel.FindControl("AdministratorOverrideModal_CompletionStatus_Field");
                DropDownList successStatusData = (DropDownList)administratorOverrideModalFormPanel.FindControl("AdministratorOverrideModal_SuccessStatus_Field");
                TextBox scoreData = (TextBox)administratorOverrideModalFormPanel.FindControl("AdministratorOverrideModal_Score_Field");
                DatePicker timestampData = (DatePicker)administratorOverrideModalFormPanel.FindControl("AdministratorOverrideModal_Timestamp_Field");

                int completion = Convert.ToInt32(completionStatusData.SelectedValue);
                int success = Convert.ToInt32(successStatusData.SelectedValue);

                // validate the data and save

                bool isValid = true;
                DateTime? timestampUTC = null;
                int parsedScore = 0;
                double? scoreScaled = null;

                // validate and scale the score, if necessary
                if ((!String.IsNullOrWhiteSpace(scoreData.Text) && !int.TryParse(scoreData.Text, out parsedScore)))
                {
                    isValid = false;
                    this._LoadAdministratorOverrideModalContent(sender, e);
                    this.ApplyErrorMessageToFieldErrorPanel(administratorOverrideModalFormPanel, "AdministratorOverrideModal_Score", _GlobalResources.Score + " " + _GlobalResources.IsInvalid);
                    this._AdministratorOverrideModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                }
                else
                {
                    // now if the score is not empty, validate value between 0 and 100, and also scale it
                    if (!String.IsNullOrWhiteSpace(scoreData.Text))
                    {
                        if (parsedScore >= 0 && parsedScore <= 100)
                        { scoreScaled = (double)((double)parsedScore / 100); }
                        else
                        {
                            isValid = false;
                            this._LoadAdministratorOverrideModalContent(sender, e);
                            this.ApplyErrorMessageToFieldErrorPanel(administratorOverrideModalFormPanel, "AdministratorOverrideModal_Score", _GlobalResources.Score + " " + _GlobalResources.IsInvalid);
                            this._AdministratorOverrideModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                        }
                    }
                }

                // validate timestamp
                if (timestampData.Value == null)
                {
                    isValid = false;
                    this._LoadAdministratorOverrideModalContent(sender, e);
                    this.ApplyErrorMessageToFieldErrorPanel(administratorOverrideModalFormPanel, "AdministratorOverrideModal_Timestamp", _GlobalResources.Timestamp + " " + _GlobalResources.IsRequired);
                    this._AdministratorOverrideModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                }
                else // ensure it is between start and end of the enrollment and not in the future
                {
                    // convert timestamp to UTC from local time using the enrollment's timezone
                    if (timestampData.NowCheckBoxChecked)
                    { timestampUTC = AsentiaSessionState.UtcNow; }
                    else
                    { timestampUTC = TimeZoneInfo.ConvertTimeToUtc((DateTime)timestampData.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName)); }

                    // validate against enrollment start and end dates and current utc
                    if (timestampUTC < this._EnrollmentObject.DtStart || timestampUTC >= this._EnrollmentObject.DtEffectiveExpires || timestampUTC > AsentiaSessionState.UtcNow)
                    {
                        isValid = false;
                        this._LoadAdministratorOverrideModalContent(sender, e);
                        this.ApplyErrorMessageToFieldErrorPanel(administratorOverrideModalFormPanel, "AdministratorOverrideModal_Timestamp", _GlobalResources.TimestampMustBeBetweenTheEnrollment_sStartAndEndDateAndCannotBeInTheFuture);
                        this._AdministratorOverrideModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                    }
                }

                if (isValid)
                {
                    // mark the lesson as completed
                    DataLesson.OverrideLessonCompletion(idEnrollment, idDataLesson, completion, success, scoreScaled, timestampUTC);

                    administratorOverrideModalFormPanel.Controls.Clear();
                    this._AdministratorOverrideModal.SubmitButton.Visible = false;
                    this._AdministratorOverrideModal.CloseButton.Visible = false;
                    this._AdministratorOverrideModal.DisplayFeedback(_GlobalResources.TheModuleCompletionDataHasBeenSavedSuccessfully, false);

                    // rebind the activity data grid and update
                    this.ActivityGrid.BindData();
                    this.ActivityGridUpdatePanel.Update();
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AdministratorOverrideModal.DisplayFeedback(dnfEx.Message, true);
                administratorOverrideModalFormPanel.Controls.Clear();
                this._AdministratorOverrideModal.SubmitButton.Visible = false;
                this._AdministratorOverrideModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._AdministratorOverrideModal.DisplayFeedback(fnuEx.Message, true);
                administratorOverrideModalFormPanel.Controls.Clear();
                this._AdministratorOverrideModal.SubmitButton.Visible = false;
                this._AdministratorOverrideModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AdministratorOverrideModal.DisplayFeedback(cpeEx.Message, true);
                administratorOverrideModalFormPanel.Controls.Clear();
                this._AdministratorOverrideModal.SubmitButton.Visible = false;
                this._AdministratorOverrideModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AdministratorOverrideModal.DisplayFeedback(dEx.Message, true);
                administratorOverrideModalFormPanel.Controls.Clear();
                this._AdministratorOverrideModal.SubmitButton.Visible = false;
                this._AdministratorOverrideModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AdministratorOverrideModal.DisplayFeedback(ex.Message, true);
                administratorOverrideModalFormPanel.Controls.Clear();
                this._AdministratorOverrideModal.SubmitButton.Visible = false;
                this._AdministratorOverrideModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildResetConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the reset action performed on lesson data.
        /// </summary>
        private void _BuildResetConfirmationModal()
        {
            // set modal properties
            this._ResetLessonDataConfirmationModal = new ModalPopup("ResetLessonDataConfirmationModal");
            this._ResetLessonDataConfirmationModal.Type = ModalPopupType.Form;
            this._ResetLessonDataConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_RESET, ImageFiles.EXT_PNG);
            this._ResetLessonDataConfirmationModal.HeaderIconAlt = _GlobalResources.ResetModuleData;
            this._ResetLessonDataConfirmationModal.HeaderText = _GlobalResources.ResetModuleData;
            this._ResetLessonDataConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._ResetLessonDataConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._ResetLessonDataConfirmationModal.TargetControlID = this._ResetLessonDataConfirmationModalLaunchButton.ID;
            this._ResetLessonDataConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._ResetLessonDataSubmit_Command);

            // build the modal form panel
            Panel resetLessonDataConfirmationModalFormPanel = new Panel();
            resetLessonDataConfirmationModalFormPanel.ID = "ResetLessonDataConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmResetActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToResetThisModuleData;

            body1Wrapper.Controls.Add(body1);
            resetLessonDataConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // warning about resetting lesson data belonging to a completed enrollment
            if (this._EnrollmentObject.DtCompleted != null)
            {
                HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
                Literal body2 = new Literal();

                body2Wrapper.ID = "ConfirmResetActionBody2";
                body2.Text = _GlobalResources.WarningResettingThisModuleDataWillRevokeTheCompletedStatusOfThisCourseEnrollment;

                body2Wrapper.Controls.Add(body2);
                resetLessonDataConfirmationModalFormPanel.Controls.Add(body2Wrapper);
            }

            // build the lesson reset data hidden field
            HiddenField lessonResetData = new HiddenField();
            lessonResetData.ID = "LessonResetData";

            // build the modal body
            this._ResetLessonDataConfirmationModal.AddControlToBody(resetLessonDataConfirmationModalFormPanel);
            this._ResetLessonDataConfirmationModal.AddControlToBody(this._ResetLessonDataConfirmationModalLoadButton);
            this._ResetLessonDataConfirmationModal.AddControlToBody(lessonResetData);

            // add modal to container
            this.EnrollmentFormWrapperContainer.Controls.Add(this._ResetLessonDataConfirmationModal);
        }
        #endregion

        #region _LoadResetLessonDataConfirmationModalContent
        /// <summary>
        /// Loads content for lesson data reset confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadResetLessonDataConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel resetLessonDataConfirmationModalFormPanel = (Panel)this._ResetLessonDataConfirmationModal.FindControl("ResetLessonDataConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._ResetLessonDataConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = true;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ResetLessonDataConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ResetLessonDataConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ResetLessonDataConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ResetLessonDataConfirmationModal.DisplayFeedback(dEx.Message, true);
                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ResetLessonDataConfirmationModal.DisplayFeedback(ex.Message, true);
                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _ResetLessonDataSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the lesson data reset confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _ResetLessonDataSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField lessonResetData = (HiddenField)this._ResetLessonDataConfirmationModal.FindControl("LessonResetData");
            Panel resetLessonDataConfirmationModalFormPanel = (Panel)this._ResetLessonDataConfirmationModal.FindControl("ResetLessonDataConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._ResetLessonDataConfirmationModal.ClearFeedback();

                // get the enrollment id, and lesson data id
                string[] lessonResetDataItems = lessonResetData.Value.Split('|');
                int idUser = Convert.ToInt32(lessonResetDataItems[0]);
                int idEnrollment = Convert.ToInt32(lessonResetDataItems[1]);
                int idDataLesson = Convert.ToInt32(lessonResetDataItems[2]);

                // remove the sco data log files
                DataLesson.DeleteSCOLogDataFiles(idUser, idDataLesson);

                // reset the lesson data
                DataLesson.Reset(idEnrollment, idDataLesson);

                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
                this._ResetLessonDataConfirmationModal.DisplayFeedback(_GlobalResources.TheModuleDataHasBeenResetSuccessfully, false);

                // rebind the activity data grid and update
                this.ActivityGrid.BindData();
                this.ActivityGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ResetLessonDataConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ResetLessonDataConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ResetLessonDataConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ResetLessonDataConfirmationModal.DisplayFeedback(dEx.Message, true);
                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ResetLessonDataConfirmationModal.DisplayFeedback(ex.Message, true);
                resetLessonDataConfirmationModalFormPanel.Controls.Clear();
                this._ResetLessonDataConfirmationModal.SubmitButton.Visible = false;
                this._ResetLessonDataConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _ValidateEnrollmentForm
        /// <summary>
        /// Method to validate the activity form field value.
        /// </summary>
        /// <returns></returns>
        private bool _ValidateEnrollmentForm()
        {
            bool isValid = true;

            if (this._CompletionStatus.SelectedValue == Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString())
            {
                if (this._CompletionDate.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentCompletionStatus", _GlobalResources.CompletionStatus + " " + _GlobalResources.RequiresADateWhenCompletedIsSelected);
                    this._CompletionDate.Enabled = true;
                    return isValid;
                }
                else // ensure it is between start and end of the enrollment and not in the future
                {
                    DateTime? timestampUTC = null;

                    // convert timestamp to UTC from local time using the enrollment's timezone
                    timestampUTC = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._CompletionDate.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName));

                    // validate against enrollment start and end dates and current utc
                    if (timestampUTC < this._EnrollmentObject.DtStart || timestampUTC >= this._EnrollmentObject.DtEffectiveExpires || timestampUTC > AsentiaSessionState.UtcNow)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentCompletionStatus", _GlobalResources.TimestampMustBeBetweenTheEnrollment_sStartAndEndDateAndCannotBeInTheFuture);
                        this._CompletionDate.Enabled = true;
                        return isValid;
                    }
                }
            }

            return isValid;
        }
        #endregion

        #region _EnrollmentFormSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for enrollment form.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _EnrollmentFormSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateEnrollmentForm())
                { throw new AsentiaException(); }

                int id;

                // populate the enrollment completed date property based on the selected completion status value
                if (this._CompletionStatus.SelectedValue == Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString())
                { this._EnrollmentObject.DtCompleted = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._CompletionDate.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName)); }
                else
                { this._EnrollmentObject.DtCompleted = null; }

                // save the enrollment's completion status, save its returned id to viewstate, and 
                // instansiate a new enrollment object with the id
                id = this._EnrollmentObject.SaveCompletionStatus();
                this.ViewState["id"] = id;
                this._EnrollmentObject.Id = id;

                // load the saved enrollment object
                this._EnrollmentObject = new Enrollment(id);

                // build the enrollment form
                this._BuildEnrollmentForm();

                // build the enrollment form actions panel
                this._BuildEnrollmentActionsPanel();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.EnrollmentActivityHasBeenSavedSuccessfully, false);

                // rebind the activity grid
                this.ActivityGrid.BindData();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dnfEx.Message, true);

                // rebind the activity grid
                this.ActivityGrid.BindData();
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, fnuEx.Message, true);

                // rebind the activity grid
                this.ActivityGrid.BindData();
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, cpeEx.Message, true);

                // rebind the activity grid
                this.ActivityGrid.BindData();
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dEx.Message, true);

                // rebind the activity grid
                this.ActivityGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);

                // rebind the activity grid
                this.ActivityGrid.BindData();
            }
        }
        #endregion

        #region _EnrollmentFormCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for Enrollment form.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _EnrollmentFormCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/users/enrollments/?uid=" + this._UserObject.Id.ToString());
        }
        #endregion
    }
}
