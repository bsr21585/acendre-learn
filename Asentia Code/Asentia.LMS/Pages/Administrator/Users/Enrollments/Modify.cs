﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.Users.Enrollments
{    
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel EnrollmentFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel EnrollmentFormWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel InheritedFromGroupAlertPanel;
        public Panel InheritedFromRuleSetAlertPanel;
        public Panel EnrollmentFormContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private User _UserObject;
        private Enrollment _EnrollmentObject;

        private bool _IsExistingEnrollment = false;
        private bool _IsStartDatePassed = false;

        private Panel _CoursesListContainer;
        private ModalPopup _SelectCourses;

        private DataTable _EligibleCoursesForSelectList;
        private DynamicListBox _SelectEligibleCourses;

        private HiddenField _SelectedCourses;

        private CheckBox _IsLockedByPrerequisites;
        private TimeZoneSelector _Timezone;
        private DatePicker _DtStart;

        private DateIntervalSelector _Due;
        private DateIntervalSelector _ExpiresFromStart;
        private DateIntervalSelector _ExpiresFromFirstLaunch;

        private Button _SaveButton;
        private Button _CancelButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.Users.Enrollments.Modify.js");

            // build global JS variables for Social Media elements
            StringBuilder smGlobalJS = new StringBuilder();

            smGlobalJS.AppendLine("DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");

            csm.RegisterClientScriptBlock(typeof(Modify), "GlobalJS", smGlobalJS.ToString(), true);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// page load method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the enrollment and user objects
            this._GetEnrollmentAndUserObjects();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/users/enrollments/Modify.css");

            // initialize the administrator menu
            this.InitializeAdminMenu();            

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetEnrollmentAndUserObjects
        /// <summary>
        /// Gets the user and enrollment objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetEnrollmentAndUserObjects()
        {
            // get the id querystring parameter
            int qsUId = this.QueryStringInt("uid", 0);
            int vsUId = this.ViewStateInt(this.ViewState, "uid", 0);
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            // get user object - user object MUST be specified and exist for this page to load
            if (qsUId > 0 || vsUId > 0)
            {
                int uid = 0;

                if (qsUId > 0)
                { uid = qsUId; }

                if (vsUId > 0)
                { uid = vsUId; }

                try
                {
                    if (uid > 0)
                    { this._UserObject = new User(uid); }
                }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            { Response.Redirect("~/administrator/users"); }

            // get enrollment object (if exists)
            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    {
                        this._EnrollmentObject = new Enrollment(id);

                        this._IsExistingEnrollment = true;

                        if (this._EnrollmentObject.DtStart <= AsentiaSessionState.UtcNow)
                        { this._IsStartDatePassed = true; }
                    }
                }
                catch
                { Response.Redirect("~/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString()); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            // evaluate for breadcrumb and page title information
            string pageTitle;            

            if (this._IsExistingEnrollment)
            {
                string courseTitleInInterfaceLanguage = this._EnrollmentObject.Title;
                pageTitle = courseTitleInInterfaceLanguage + ": " + _GlobalResources.Modify;                
            }
            else
            { pageTitle = _GlobalResources.NewCourseEnrollment; }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(this._UserObject.DisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Enrollments, "/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(pageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// build the page title, bredcrumb and controls on the page
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.EnrollmentFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.EnrollmentFormWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Enrollments;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.AddModifyPropertiesOfThisEnrollmentUsingFormBelow, true);

            if (this._IsExistingEnrollment)
            {
                if (this._EnrollmentObject.IdGroupEnrollment != null)
                {
                    // format a page information panel for alert to inform user about changing an enrollment inherited from group
                    this.FormatSectionInformationPanel(this.InheritedFromGroupAlertPanel, _GlobalResources.ThisEnrollmentHasBeenInheritedFromAGroupEnrollmentChangesWillSeparateIt, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));
                }

                if (this._EnrollmentObject.IdRuleSetEnrollment != null)
                {
                    // format a page information panel for alert to inform user about changing an enrollment inherited from ruleset
                    this.FormatSectionInformationPanel(this.InheritedFromRuleSetAlertPanel, _GlobalResources.ThisEnrollmentHasBeenInheritedFromARulesetEnrollmentChangedWillSeparateIt, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));
                }
            }

            // clear controls from container
            this.EnrollmentFormContainer.Controls.Clear();

            // build the enrollment form
            this._BuildEnrollmentForm();

            // build the enrollment form actions panel
            this._BuildEnrollmentActionsPanel();

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulateEnrollmentFormInputElements();
        }
        #endregion

        #region _BuildEnrollmentForm
        /// <summary>
        /// Method to cerate the enrollment form.
        /// </summary>
        private void _BuildEnrollmentForm()
        {
            // courses container
            // if this is not for an existing enrollment, then we're adding a new enrollment, so display the course select box
            if (!this._IsExistingEnrollment)
            {
                List<Control> enrollmentCoursesInputControls = new List<Control>();
                this._EligibleCoursesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForSelectList(null);

                // build a container for the courses listing
                this._CoursesListContainer = new Panel();
                this._CoursesListContainer.ID = "EnrollmentCoursesList_Container";
                this._CoursesListContainer.CssClass = "ItemListingContainer";
                
                enrollmentCoursesInputControls.Add(this._CoursesListContainer);

                // select courses button
                Image selectCoursesImageForLink = new Image();
                selectCoursesImageForLink.ID = "LaunchSelectCoursesModalImage";
                selectCoursesImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
                selectCoursesImageForLink.CssClass = "MediumIcon";

                Localize selectCoursesTextForLink = new Localize();
                selectCoursesTextForLink.Text = _GlobalResources.SelectCourse_s;

                LinkButton selectCoursesLink = new LinkButton();
                selectCoursesLink.ID = "LaunchSelectCoursesModal";
                selectCoursesLink.CssClass = "ImageLink";
                selectCoursesLink.Controls.Add(selectCoursesImageForLink);
                selectCoursesLink.Controls.Add(selectCoursesTextForLink);

                enrollmentCoursesInputControls.Add(selectCoursesLink);

                // selected courses hidden field
                this._SelectedCourses = new HiddenField();
                this._SelectedCourses.ID = "SelectedCourses_Field";

                enrollmentCoursesInputControls.Add(this._SelectedCourses);

                this.EnrollmentFormContainer.Controls.Add(BuildMultipleInputControlFormField("EnrollmentCourses",
                                                                                      _GlobalResources.Course_s,
                                                                                      enrollmentCoursesInputControls,
                                                                                      true,
                                                                                      true,
                                                                                      false));

                // build modal for adding and removing courses to/from the enrollment
                this._BuildSelectCoursesModal(selectCoursesLink.ID);
            }
            else
            {
                Label enrollmentCourseFieldStaticValue = new Label();
                enrollmentCourseFieldStaticValue.Text = this._EnrollmentObject.Title;

                this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentCourses",
                                                                  _GlobalResources.Course_s, 
                                                                  enrollmentCourseFieldStaticValue.ID, 
                                                                  enrollmentCourseFieldStaticValue, 
                                                                  false, 
                                                                  false, 
                                                                  false));
            }

            // enrollment is locked by prerequisites container
            this._IsLockedByPrerequisites = new CheckBox();
            this._IsLockedByPrerequisites.ID = "EnrollmentIsLockedByPrerequisites_Field";
            this._IsLockedByPrerequisites.Text = _GlobalResources.PreventLearnerFromLaunchingThisCourseUntilPrerequisitesSatisfied;
            this._IsLockedByPrerequisites.Checked = true;

            this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentIsLockedByPrerequisites",
                                                              _GlobalResources.Prerequisites,
                                                              this._IsLockedByPrerequisites.ID,
                                                              this._IsLockedByPrerequisites,
                                                              false,
                                                              false,
                                                              false));

            // enrollment date start container
            // if the start date has not passed, its still editable, otherwise it is not
            if (!this._IsStartDatePassed)
            {
                this._DtStart = new DatePicker("EnrollmentDtStart_Field", false, true, true);
                this._DtStart.NowCheckboxText = _GlobalResources.StartEnrollmentImmediately;
                this._DtStart.Value = TimeZoneInfo.ConvertTimeFromUtc(AsentiaSessionState.UtcNow.AddMinutes(1), TimeZoneInfo.FindSystemTimeZoneById(new Timezone(AsentiaSessionState.GlobalSiteObject.IdTimezone).dotNetName));

                //enrollmentDtStartFieldInputContainer.Controls.Add(this._DtStart);

                this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentDtStart",
                                                                  _GlobalResources.Start,
                                                                  this._DtStart.ID,
                                                                  this._DtStart,
                                                                  true,
                                                                  true,
                                                                  false));
            }
            else
            {
                DateTime dateValue = TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName));
                Label enrollmentDtStartFieldStaticValue = new Label();

                enrollmentDtStartFieldStaticValue.Text = dateValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern) + " @ " + dateValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern);

                this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentDtStart",
                                                                  _GlobalResources.Start,
                                                                  enrollmentDtStartFieldStaticValue.ID,
                                                                  enrollmentDtStartFieldStaticValue,
                                                                  true,
                                                                  true,
                                                                  false));
            }
            
            // enrollment timezone container
            this._Timezone = new TimeZoneSelector();
            this._Timezone.ID = "EnrollmentTimezone_Field";
            this._Timezone.SelectedValue = AsentiaSessionState.GlobalSiteObject.IdTimezone.ToString();

            this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentTimezone",
                                                              _GlobalResources.Timezone,
                                                              this._Timezone.ID,
                                                              this._Timezone,
                                                              true,
                                                              true,
                                                              false));

            // enrollment due container
            this._Due = new DateIntervalSelector("EnrollmentDue_Field", true);
            this._Due.NoneCheckboxText = _GlobalResources.NoDueDate;
            this._Due.NoneCheckBoxChecked = true;

            this._Due.TextBeforeSelector = _GlobalResources.LearnerMustCompleteCourseWithin;
            this._Due.TextAfterSelector = _GlobalResources.AfterEnrollment;

            this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentDue",
                                                              _GlobalResources.Due,
                                                              this._Due.ID,
                                                              this._Due,
                                                              true,
                                                              true,
                                                              false));

            // enrollment expires from start container
            this._ExpiresFromStart = new DateIntervalSelector("EnrollmentExpiresFromStart_Field", true);
            this._ExpiresFromStart.NoneCheckboxText = _GlobalResources.Indefinite;
            this._ExpiresFromStart.NoneCheckBoxChecked = true;

            this._ExpiresFromStart.TextBeforeSelector = _GlobalResources.LearnerHasAccessToTheCourseFor;
            this._ExpiresFromStart.TextAfterSelector = _GlobalResources.AfterEnrollment;

            this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentExpiresFromStart",
                                                              _GlobalResources.AccessFromStart,
                                                              this._ExpiresFromStart.ID,
                                                              this._ExpiresFromStart,
                                                              true,
                                                              true,
                                                              false));


            // enrollment expires from first launch input
            this._ExpiresFromFirstLaunch = new DateIntervalSelector("EnrollmentExpiresFromFirstLaunch_Field", true);
            this._ExpiresFromFirstLaunch.NoneCheckboxText = _GlobalResources.Indefinite;
            this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = true;
            this._ExpiresFromFirstLaunch.TextBeforeSelector = _GlobalResources.LearnerHasAccessToTheCourseFor;
            this._ExpiresFromFirstLaunch.TextAfterSelector = _GlobalResources.AfterFirstLaunchOfCourse;

            this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentExpiresFromFirstLaunch",
                                                              _GlobalResources.AccessFromFirstLaunch,
                                                              this._ExpiresFromFirstLaunch.ID,
                                                              this._ExpiresFromFirstLaunch,
                                                              true,
                                                              true,
                                                              false));
        }
        #endregion

        #region _BuildSelectCoursesModal
        /// <summary>
        /// Builds the modal for selecting courses to add to the enrollment.
        /// </summary>
        private void _BuildSelectCoursesModal(string targetControlId)
        {
            // set modal properties
            this._SelectCourses = new ModalPopup("SelectCoursesModal");
            this._SelectCourses.Type = ModalPopupType.Form;
            this._SelectCourses.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE,
                                                                        ImageFiles.EXT_PNG);
            this._SelectCourses.HeaderIconAlt = _GlobalResources.SelectCourse_s;
            this._SelectCourses.HeaderText = _GlobalResources.SelectCourse_s;
            this._SelectCourses.TargetControlID = targetControlId;
            this._SelectCourses.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCourses.SubmitButtonCustomText = _GlobalResources.AddCourse_s;
            this._SelectCourses.SubmitButton.OnClientClick = "javascript:AddCoursesToEnrollment(); return false;";
            this._SelectCourses.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCourses.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectCourses.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the course listing
            this._SelectEligibleCourses = new DynamicListBox("SelectEligibleCourses");
            this._SelectEligibleCourses.NoRecordsFoundMessage = _GlobalResources.NoCoursesFound;
            this._SelectEligibleCourses.SearchButton.Command += new CommandEventHandler(this._SearchSelectCoursesButton_Command);
            this._SelectEligibleCourses.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectCoursesButton_Command);
            this._SelectEligibleCourses.ListBoxControl.DataSource = this._EligibleCoursesForSelectList;
            this._SelectEligibleCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleCourses.ListBoxControl.DataBind();

            // add controls to body
            this._SelectCourses.AddControlToBody(this._SelectEligibleCourses);

            // add modal to container
            this.EnrollmentFormContainer.Controls.Add(this._SelectCourses);
        }
        #endregion

        #region _SearchSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Course(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCourses.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleCourses.ListBoxControl.Items.Clear();

            // do the search
            this._EligibleCoursesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForSelectList(this._SelectEligibleCourses.SearchTextBox.Text);

            _SelectEligibleCoursesDataBind();
        }
        #endregion

        #region _ClearSearchSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Course(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCourses.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleCourses.ListBoxControl.Items.Clear();
            this._SelectEligibleCourses.SearchTextBox.Text = "";

            // clear the search
            this._EligibleCoursesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForSelectList(null);

            _SelectEligibleCoursesDataBind();
        }
        #endregion

        #region _SelectEligibleCoursesDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectEligibleCoursesDataBind()
        {
            this._SelectEligibleCourses.ListBoxControl.DataSource = this._EligibleCoursesForSelectList;
            this._SelectEligibleCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleCourses.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._SelectEligibleCourses.ListBoxControl.Items.Count == 0)
            {
                this._SelectCourses.SubmitButton.Enabled = false;
                this._SelectCourses.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectCourses.SubmitButton.Enabled = true;
                this._SelectCourses.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _BuildEnrollmentActionsPanel
        /// <summary>
        /// Builds the container and buttons for enrollment form actions.
        /// </summary>
        private void _BuildEnrollmentActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "EnrollmentSaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._EnrollmentObject == null)
            { this._SaveButton.Text = _GlobalResources.CreateEnrollment; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(this._EnrollmentFormSaveButton_Command);
            this._SaveButton.Attributes.Add("onclick", "GetSelectedCoursesForHiddenField();");
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._EnrollmentFormCancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _PopulateEnrollmentFormInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateEnrollmentFormInputElements()
        {
            if (this._EnrollmentObject != null)
            {
                // is locked by prerequisites
                this._IsLockedByPrerequisites.Checked = this._EnrollmentObject.IsLockedByPrerequisites;

                // timezone
                this._Timezone.SelectedValue = this._EnrollmentObject.IdTimezone.ToString();

                // date start
                if (!this._IsStartDatePassed)
                { this._DtStart.Value = TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName)); }

                // due
                if (this._EnrollmentObject.DueInterval != null)
                {
                    this._Due.NoneCheckBoxChecked = false;
                    this._Due.IntervalValue = this._EnrollmentObject.DueInterval.ToString();
                    this._Due.TimeframeValue = this._EnrollmentObject.DueTimeframe;
                }
                else
                {
                    this._Due.NoneCheckBoxChecked = true;
                    this._Due.IntervalValue = null;
                    this._Due.TimeframeValue = null;
                }

                // expires from start
                if (this._EnrollmentObject.ExpiresFromStartInterval != null)
                {
                    this._ExpiresFromStart.NoneCheckBoxChecked = false;
                    this._ExpiresFromStart.IntervalValue = this._EnrollmentObject.ExpiresFromStartInterval.ToString();
                    this._ExpiresFromStart.TimeframeValue = this._EnrollmentObject.ExpiresFromStartTimeframe;
                }
                else
                {
                    this._ExpiresFromStart.NoneCheckBoxChecked = true;
                    this._ExpiresFromStart.IntervalValue = null;
                    this._ExpiresFromStart.TimeframeValue = null;
                }

                // expires from first launch
                if (this._EnrollmentObject.ExpiresFromFirstLaunchInterval != null)
                {
                    this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = false;
                    this._ExpiresFromFirstLaunch.IntervalValue = this._EnrollmentObject.ExpiresFromFirstLaunchInterval.ToString();
                    this._ExpiresFromFirstLaunch.TimeframeValue = this._EnrollmentObject.ExpiresFromFirstLaunchTimeframe;
                }
                else
                {
                    this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = true;
                    this._ExpiresFromFirstLaunch.IntervalValue = null;
                    this._ExpiresFromFirstLaunch.TimeframeValue = null;
                }
            }
        }
        #endregion

        #region _SetEnrollmentFormAfterMultipleCourseSave
        /// <summary>
        /// load the enrollment for data after the course enrollment is saved.
        /// </summary>
        private void _SetEnrollmentFormAfterMultipleCourseSave()
        {
            // disable form fields
            this._IsLockedByPrerequisites.Enabled = false;
            this._DtStart.Enabled = false;
            this._Timezone.Enabled = false;
            this._Due.Enabled = false;
            this._ExpiresFromStart.Enabled = false;
            this._ExpiresFromFirstLaunch.Enabled = false;

            // hide the save button
            this._SaveButton.Visible = false;

            // change the cancel button text and set an onclientclick action to return to enrollments page
            this._CancelButton.Text = _GlobalResources.Done;
            this._CancelButton.OnClientClick = "javascript:window.location='/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString() + "'; return false;";

            // list courses enrollments were created for
            Panel enrollmentCoursesFieldContainer = (Panel)this.EnrollmentFormContainer.FindControl("EnrollmentCourses_Container");

            if (enrollmentCoursesFieldContainer != null)
            {
                // build a container for the courses listing
                this._CoursesListContainer = new Panel();
                this._CoursesListContainer.ID = "EnrollmentCoursesList_Container";
                this._CoursesListContainer.CssClass = "ItemListingContainer";

                if (!String.IsNullOrWhiteSpace(this._SelectedCourses.Value))
                {
                    // split the "value" of the hidden field to get an array of course prerequisite ids
                    string[] selectedCourseIds = this._SelectedCourses.Value.Split(',');

                    if (selectedCourseIds.Length > 1)
                    {
                        // loop
                        foreach (string selectedCourseId in selectedCourseIds)
                        {
                            // container
                            Panel courseNameContainer = new Panel();
                            courseNameContainer.ID = "Course_" + selectedCourseId;

                            // course title
                            Course courseObject = new Course(Convert.ToInt32(selectedCourseId));
                            Literal courseName = new Literal();

                            // get course title information
                            string courseTitleInInterfaceLanguage = courseObject.Title;

                            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                            {
                                foreach (Course.LanguageSpecificProperty courseLanguageSpecificProperty in courseObject.LanguageSpecificProperties)
                                {
                                    if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                                    { courseTitleInInterfaceLanguage = courseLanguageSpecificProperty.Title; }
                                }
                            }

                            courseName.Text = courseTitleInInterfaceLanguage;

                            // add controls to container
                            courseNameContainer.Controls.Add(courseName);
                            this._CoursesListContainer.Controls.Add(courseNameContainer);
                        }
                    }

                    // add the list container to the field container
                    enrollmentCoursesFieldContainer.Controls.Add(this._CoursesListContainer);
                }
            }
        }
        #endregion

        #region _ValidateEnrollmentForm
        /// <summary>
        /// validate the enrollment form values before saving them
        /// </summary>
        /// <returns></returns>
        private bool _ValidateEnrollmentForm()
        {
            bool isValid = true;

            /* BASIC VALIDATIONS */

            // course - if this is a new enrollment, at least one must have been selected, if not, it is a showstopper, which means return right away
            if (!this._IsExistingEnrollment)
            {
                if (String.IsNullOrWhiteSpace(this._SelectedCourses.Value))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentCourses", _GlobalResources.YouMustSelectOneOrMoreCourses);
                    return isValid;
                }
            }

            // is locked by prerequisites - no validation needed

            // timezone - always required, must be a number, and if not valid, its a showstopper, which means return right away
            if (String.IsNullOrWhiteSpace(this._Timezone.SelectedValue))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentTimezone", _GlobalResources.Timezone + " " + _GlobalResources.IsRequired);
                return isValid;
            }

            int idTimezone;
            if (!int.TryParse(this._Timezone.SelectedValue, out idTimezone))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentTimezone", _GlobalResources.Timezone + " " + _GlobalResources.IsInvalid);
                return isValid;
            }

            // get the timezone's "dotnetname" from a timezone object.
            string tzDotNetName = new Timezone(Convert.ToInt32(idTimezone)).dotNetName;

            // date start - required, if its not specified its a showstopper, which means return right away
            // only validate if start has not passed
            if (!this._IsStartDatePassed)
            {
                if (this._DtStart.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDtStart", _GlobalResources.Start + " " + _GlobalResources.IsRequired);
                    return isValid;
                }
            }

            // due - always exists, never required, interval must be integer
            if (!this._Due.NoneCheckBoxChecked)
            {
                int dueInterval;
                if (String.IsNullOrWhiteSpace(this._Due.IntervalValue) || !int.TryParse(this._Due.IntervalValue, out dueInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.IsInvalid);
                }
            }

            // access from start - always exists, never required, interval must be integer
            if (!this._ExpiresFromStart.NoneCheckBoxChecked)
            {
                int expiresFromStartInterval;
                if (String.IsNullOrWhiteSpace(this._ExpiresFromStart.IntervalValue) || !int.TryParse(this._ExpiresFromStart.IntervalValue, out expiresFromStartInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentExpiresFromStart", _GlobalResources.AccessFromStart + " " + _GlobalResources.IsInvalid);
                }
            }

            // access from first launch - always exists, never required, interval must be integer
            if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
            {
                int expiresFromFirstLaunchInterval;
                if (String.IsNullOrWhiteSpace(this._ExpiresFromFirstLaunch.IntervalValue) || !int.TryParse(this._ExpiresFromFirstLaunch.IntervalValue, out expiresFromFirstLaunchInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentExpiresFromFirstLaunch", _GlobalResources.AccessFromFirstLaunch + " " + _GlobalResources.IsInvalid);
                }
            }

            /* CONSTRAINT VALIDATIONS BASED ON TYPE */

            // only apply constraint validations if the other validations have passed
            if (isValid)
            {
                // date start - must be in future, only validate if start has not passed
                DateTime dtStart;

                if (!this._IsStartDatePassed)
                {
                    if (this._DtStart.NowCheckBoxChecked)
                    { dtStart = DateTime.UtcNow.AddMinutes(1); }
                    else
                    { dtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName)); }

                    if (dtStart < AsentiaSessionState.UtcNow)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDtStart", _GlobalResources.Start + " " + _GlobalResources.MustBeInFuture);
                    }
                }
                else // this assumes that the Enrollment object exists and is populated, otherwise, this couldn't be fired due to logic in other places
                {
                    // if applying the the selected timezone to the unchanged original date start results in a
                    // new start date that has already passed, fail
                    // this only needs to be done when the timezone has been modified

                    // this is utc
                    dtStart = this._EnrollmentObject.DtStart;

                    if (this._EnrollmentObject.IdTimezone != Convert.ToInt32(this._Timezone.SelectedValue))
                    {
                        // convert start back to local (from original timezone)
                        DateTime originalLocalDtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName));

                        // convert local start to new utc
                        dtStart = TimeZoneInfo.ConvertTimeToUtc(originalLocalDtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(Convert.ToInt32(this._Timezone.SelectedValue)).dotNetName));

                        if (dtStart <= AsentiaSessionState.UtcNow)
                        {
                            isValid = false;
                            Timezone selectedTimezone = new Timezone(Convert.ToInt32(this._Timezone.SelectedValue));
                            string currentAdjustedStartValue = TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(selectedTimezone.dotNetName)).ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern)
                                                               + " @ "
                                                               + TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(selectedTimezone.dotNetName)).ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern)
                                                               + " "
                                                               + selectedTimezone.displayName;
                            this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDtStart", _GlobalResources.Start + " " + String.Format(_GlobalResources.CannotBeMovedEarlierThanCurrentValue, currentAdjustedStartValue));
                        }
                    }
                }

                // due - interval cannot exceed access from start and access from first launch intervals
                if (!this._Due.NoneCheckBoxChecked)
                {
                    // compare to expires from start interval
                    if (!this._ExpiresFromStart.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtDue;
                        DateTime calculatedDtExpiresFromStart;
                        int dueInterval = Convert.ToInt32(this._Due.IntervalValue);
                        int expiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                        switch (this._Due.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtDue = dtStart.AddDays(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtDue = dtStart.AddMonths(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtDue = dtStart.AddYears(dueInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtDue = DateTime.MinValue;
                                break;
                        }

                        switch (this._ExpiresFromStart.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromStart = dtStart.AddMonths(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromStart = dtStart.AddYears(expiresFromStartInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromStart = DateTime.MinValue;
                                break;
                        }

                        // compare calculated date due to calculated expires from start
                        if (calculatedDtDue > calculatedDtExpiresFromStart)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromStart);
                        }
                    }

                    // compare to expires from first launch interval
                    if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtDue;
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        int dueInterval = Convert.ToInt32(this._Due.IntervalValue);
                        int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                        switch (this._Due.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtDue = dtStart.AddDays(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtDue = dtStart.AddMonths(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtDue = dtStart.AddYears(dueInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtDue = DateTime.MinValue;
                                break;
                        }

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        // compare calculated date due to calculated expires from first launch
                        if (calculatedDtDue > calculatedDtExpiresFromFirstLaunch)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromFirstLaunch);
                        }
                    }
                }

                // expires from first launch - interval cannot exceed expires from start interval
                if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                {
                    // compare to expires from start interval
                    if (!this._ExpiresFromStart.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        DateTime calculatedDtExpiresFromStart;
                        int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);
                        int expiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        switch (this._ExpiresFromStart.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromStart = dtStart.AddMonths(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromStart = dtStart.AddYears(expiresFromStartInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromStart = DateTime.MinValue;
                                break;
                        }

                        // compare calculated expires from first launch to calculated expires from start
                        if (calculatedDtExpiresFromFirstLaunch > calculatedDtExpiresFromStart)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentExpiresFromFirstLaunch", _GlobalResources.AccessFromFirstLaunch + " " + _GlobalResources.CannotExceedAccessFromStart);
                        }
                    }
                }
            }

            // if this is invalid, we need to pre-populate multi-select fields with the objects that were selected
            if (!isValid && this._CoursesListContainer != null)
            {
                /* SELECTED COURSES */

                if (!String.IsNullOrWhiteSpace(this._SelectedCourses.Value))
                {
                    // split the "value" of the hidden field to get an array of course ids
                    string[] selectedCoursesIds = this._SelectedCourses.Value.Split(',');

                    // loop through the array and add each course to the listing container
                    foreach (string courseId in selectedCoursesIds)
                    {
                        if (this._CoursesListContainer.FindControl("Course_" + courseId) == null)
                        {
                            // course object
                            Course courseObject = new Course(Convert.ToInt32(courseId));

                            // container
                            Panel courseNameContainer = new Panel();
                            courseNameContainer.ID = "Course_" + courseObject.Id.ToString();

                            // remove course button
                            Image removeCourseImage = new Image();
                            removeCourseImage.ID = "Course_" + courseObject.Id.ToString() + "_RemoveImage";
                            removeCourseImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                ImageFiles.EXT_PNG);
                            removeCourseImage.CssClass = "SmallIcon";
                            removeCourseImage.Attributes.Add("onClick", "javascript:RemoveCourseFromEnrollment('" + courseObject.Id.ToString() + "');");
                            removeCourseImage.Style.Add("cursor", "pointer");

                            // course name
                            Literal courseName = new Literal();
                            courseName.Text = courseObject.Title;

                            foreach (Course.LanguageSpecificProperty courseLanguageSpecificProperty in courseObject.LanguageSpecificProperties)
                            {
                                if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                                {
                                    courseName.Text = courseLanguageSpecificProperty.Title;
                                    break;
                                }
                            }

                            // add controls to container
                            courseNameContainer.Controls.Add(removeCourseImage);
                            courseNameContainer.Controls.Add(courseName);
                            this._CoursesListContainer.Controls.Add(courseNameContainer);
                        }
                    }
                }
            }

            return isValid;
        }
        #endregion

        #region _EnrollmentFormSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for enrollment form.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _EnrollmentFormSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateEnrollmentForm())
                { throw new AsentiaException(); }

                // if there is no enrollment object, create one
                if (this._EnrollmentObject == null)
                { this._EnrollmentObject = new Enrollment(); }

                int id;
                int uid = this._UserObject.Id;

                // populate the object

                // user id
                this._EnrollmentObject.IdUser = this._UserObject.Id;

                // is locked by prerequisites
                this._EnrollmentObject.IsLockedByPrerequisites = this._IsLockedByPrerequisites.Checked;

                // timezone
                int oldTimezoneId = 0;

                if (this._EnrollmentObject.IdTimezone > 0)
                { oldTimezoneId = this._EnrollmentObject.IdTimezone; }

                this._EnrollmentObject.IdTimezone = Convert.ToInt32(this._Timezone.SelectedValue);

                // date start
                if (!this._IsStartDatePassed)
                {
                    if (this._DtStart.NowCheckBoxChecked)
                    { this._EnrollmentObject.DtStart = DateTime.UtcNow.AddMinutes(1); }
                    else
                    { this._EnrollmentObject.DtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName)); }
                }
                else
                {
                    // if the timezones have changed, convert start to utc from the original date start
                    if (oldTimezoneId > 0 && oldTimezoneId != this._EnrollmentObject.IdTimezone)
                    {
                        // convert start back to local
                        DateTime originalLocalDtStart = TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(oldTimezoneId).dotNetName));

                        // convert local start to new utc
                        this._EnrollmentObject.DtStart = TimeZoneInfo.ConvertTimeToUtc(originalLocalDtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName));
                    }
                }

                // due
                if (this._Due.NoneCheckBoxChecked)
                {
                    this._EnrollmentObject.DueInterval = null;
                    this._EnrollmentObject.DueTimeframe = null;
                }
                else
                {
                    this._EnrollmentObject.DueInterval = Convert.ToInt32(this._Due.IntervalValue);
                    this._EnrollmentObject.DueTimeframe = this._Due.TimeframeValue;
                }

                // expires from start
                if (this._ExpiresFromStart.NoneCheckBoxChecked)
                {
                    this._EnrollmentObject.ExpiresFromStartInterval = null;
                    this._EnrollmentObject.ExpiresFromStartTimeframe = null;
                }
                else
                {
                    this._EnrollmentObject.ExpiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);
                    this._EnrollmentObject.ExpiresFromStartTimeframe = this._ExpiresFromStart.TimeframeValue;
                }

                // expires from first launch
                if (this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                {
                    this._EnrollmentObject.ExpiresFromFirstLaunchInterval = null;
                    this._EnrollmentObject.ExpiresFromFirstLaunchTimeframe = null;
                }
                else
                {
                    this._EnrollmentObject.ExpiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);
                    this._EnrollmentObject.ExpiresFromFirstLaunchTimeframe = this._ExpiresFromFirstLaunch.TimeframeValue;
                }

                // if this is an existing enrollment and was inherited by group or ruleset, detach it from the group or ruleset enrollment
                if (this._IsExistingEnrollment && (this._EnrollmentObject.IdRuleSetEnrollment != null || this._EnrollmentObject.IdGroupEnrollment != null))
                {
                    this._EnrollmentObject.IdRuleSetEnrollment = null;
                    this._EnrollmentObject.IdGroupEnrollment = null;
                }

                bool saveForMultipleCourses = false;
                DataTable courses = new DataTable();
                courses.Columns.Add("id", typeof(int));

                // do course(s) only if this is a new enrollment
                if (!this._IsExistingEnrollment)
                {
                    if (!String.IsNullOrWhiteSpace(this._SelectedCourses.Value))
                    {
                        // split the "value" of the hidden field to get an array of course prerequisite ids
                        string[] selectedCoursePrerequisiteIds = this._SelectedCourses.Value.Split(',');

                        // if there is more than one course selected, we need to put them in a datatable and save for multiple
                        if (selectedCoursePrerequisiteIds.Length > 1)
                        {
                            // put ids into datatable 
                            foreach (string selectedCoursePrerequisiteId in selectedCoursePrerequisiteIds)
                            { courses.Rows.Add(Convert.ToInt32(selectedCoursePrerequisiteId)); }

                            saveForMultipleCourses = true;
                        }
                        else if (selectedCoursePrerequisiteIds.Length == 1)
                        { this._EnrollmentObject.IdCourse = Convert.ToInt32(selectedCoursePrerequisiteIds[0]); }
                    }
                }

                if (saveForMultipleCourses)
                {
                    // save enrollments for multiple courses
                    this._EnrollmentObject.SaveForMultipleCourses(courses);

                    // set the existing enrollment flag
                    this._IsExistingEnrollment = true;

                    // clear group actions menu
                    this.UserObjectMenuContainer.Controls.Clear();

                    // build the page controls
                    this._BuildControls();

                    // set the enrollment form after saving for multiple courses, this disables fields, saving etc.
                    this._SetEnrollmentFormAfterMultipleCourseSave();

                    // display the saved feedback
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.EnrollmentsHaveBeenSavedSuccessfully, false);
                }
                else
                {
                    // save the enrollment, save its returned id to viewstate, and 
                    // instansiate a new enrollment object with the id
                    id = this._EnrollmentObject.Save();
                    this.ViewState["id"] = id;
                    this._EnrollmentObject.Id = id;                    

                    // load the saved enrollment object
                    this._EnrollmentObject = new Enrollment(id);

                    // set the existing enrollment flag
                    this._IsExistingEnrollment = true;

                    // determine if the start date has passed based on the saved enrollment information
                    if (this._EnrollmentObject.DtStart <= AsentiaSessionState.UtcNow)
                    { this._IsStartDatePassed = true; }
                    else
                    { this._IsStartDatePassed = false; }

                    // clear group actions menu
                    this.UserObjectMenuContainer.Controls.Clear();

                    // build the page controls
                    this._BuildControls();

                    // display the saved feedback
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.EnrollmentHasBeenSavedSuccessfully, false);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _EnrollmentFormCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for Enrollment form.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _EnrollmentFormCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString());
        }
        #endregion
    }
}