﻿function AddLearningPathsToEnrollment() {
    var learningPathsListContainer = $("#" + EnrollmentLearningPathsList_Container);
    var selectedLearningPaths = $('select#' + SelectEligibleLearningPaths + "ListBox").val();

    if (selectedLearningPaths != null) {
        for (var i = 0; i < selectedLearningPaths.length; i++) {
            if (!$("#LearningPath_" + selectedLearningPaths[i]).length) {
                // add the selected course to the course list container
                var itemContainerDiv = $("<div id=\"LearningPath_" + selectedLearningPaths[i] + "\">" + "<img onclick=\"javascript:RemoveLearningPathFromEnrollment('" + selectedLearningPaths[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" class=\"SmallIcon\" />" + $("#" + SelectEligibleLearningPaths + "ListBox option[value='" + selectedLearningPaths[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(learningPathsListContainer);
            }

            // remove the course from the select list
            $("#" + SelectEligibleLearningPaths + "ListBox option[value='" + selectedLearningPaths[i] + "']").remove();
        }
    }
}

function RemoveLearningPathFromEnrollment(learningPathId) {
    $("#LearningPath_" + learningPathId).remove();
}

function GetSelectedLearningPathsForHiddenField() {
    var learningpathsListContainer = $("#" + EnrollmentLearningPathsList_Container);
    var selectedLearningPathsField = $("#" + SelectedLearningPaths_Field);
    var selectedLearningPaths = "";

    learningpathsListContainer.children().each(function () {
        selectedLearningPaths = selectedLearningPaths + $(this).prop("id").replace("LearningPath_", "") + ",";
    });

    if (selectedLearningPaths.length > 0)
    { selectedLearningPaths = selectedLearningPaths.substring(0, selectedLearningPaths.length - 1); }

    selectedLearningPathsField.val(selectedLearningPaths);
}

function PopulateHiddenFieldsForDynamicElements() {
    GetSelectedLearningPathsForHiddenField();
}