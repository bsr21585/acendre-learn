﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.Users.Enrollments
{
    public class ModifyLearningPath : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel EnrollmentFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel EnrollmentFormWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel InheritedFromGroupAlertPanel;
        public Panel InheritedFromRuleSetAlertPanel;
        public Panel EnrollmentFormContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private User _UserObject;
        private LearningPathEnrollment _EnrollmentObject;

        private bool _IsExistingEnrollment = false;
        private bool _IsStartDatePassed = false;

        private Panel _LearningPathsListContainer;
        private ModalPopup _SelectLearningPaths;

        private DataTable _EligibleLearningPathsForSelectList;
        private DynamicListBox _SelectEligibleLearningPaths;

        private HiddenField _SelectedLearningPaths;

        private TimeZoneSelector _Timezone;
        private DatePicker _DtStart;

        private DateIntervalSelector _Due;
        private DateIntervalSelector _ExpiresFromStart;
        private DateIntervalSelector _ExpiresFromFirstLaunch;

        private Button _SaveButton;
        private Button _CancelButton;
        #endregion        

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(ModifyLearningPath), "Asentia.LMS.Pages.Administrator.Users.Enrollments.ModifyLearningPath.js");

            // build global JS variables for Social Media elements
            StringBuilder imGlobalJS = new StringBuilder();
            imGlobalJS.AppendLine("DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            if (!this._IsExistingEnrollment)
            {
                imGlobalJS.AppendLine("EnrollmentLearningPathsList_Container = \"" + this._LearningPathsListContainer.ClientID + "\";");
                imGlobalJS.AppendLine("SelectEligibleLearningPaths = \"" + this._SelectEligibleLearningPaths.ClientID + "\";");
                imGlobalJS.AppendLine("SelectedLearningPaths_Field = \"" + this._SelectedLearningPaths.ClientID + "\";");
            }
            csm.RegisterClientScriptBlock(typeof(Modify), "GlobalJS", imGlobalJS.ToString(), true);
        }
        #endregion

        #region Page Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get the enrollment and user objects
            this._GetEnrollmentAndUserObjects();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/users/enrollments/ModifyLearningPath.css");

            // initialize the administrator menu
            this.InitializeAdminMenu();            

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetEnrollmentAndUserObjects
        /// <summary>
        /// Gets the user and enrollment objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetEnrollmentAndUserObjects()
        {
            // get the id querystring parameter
            int qsUId = this.QueryStringInt("uid", 0);
            int vsUId = this.ViewStateInt(this.ViewState, "uid", 0);
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            // get user object - user object MUST be specified and exist for this page to load
            if (qsUId > 0 || vsUId > 0)
            {
                int uid = 0;

                if (qsUId > 0)
                { uid = qsUId; }

                if (vsUId > 0)
                { uid = vsUId; }

                try
                {
                    if (uid > 0)
                    { this._UserObject = new User(uid); }
                }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            { Response.Redirect("~/administrator/users"); }

            // get enrollment object (if exists)
            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    {
                        this._EnrollmentObject = new LearningPathEnrollment(id);

                        this._IsExistingEnrollment = true;

                        if (this._EnrollmentObject.DtStart <= AsentiaSessionState.UtcNow)
                        { this._IsStartDatePassed = true; }
                    }
                }
                catch
                { Response.Redirect("~/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString()); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {            
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            // evaluate for breadcrumb and page title information
            string pageTitle;

            if (this._IsExistingEnrollment)
            {
                string courseTitleInInterfaceLanguage = this._EnrollmentObject.Title;
                pageTitle = courseTitleInInterfaceLanguage + ": " + _GlobalResources.Modify;
            }
            else
            { pageTitle = _GlobalResources.NewLearningPathEnrollment; }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(this._UserObject.DisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Enrollments, "/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(pageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion

        #region _BuildControls
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.EnrollmentFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.EnrollmentFormWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Enrollments;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.AddModifyPropertiesOfThisEnrollmentUsingFormBelow, true);

            if (this._IsExistingEnrollment)
            {
                if (this._EnrollmentObject.IdRuleSetLearningPathEnrollment != null)
                {
                    // format a page information panel for alert to inform user about changing an enrollment inherited from ruleset
                    this.FormatSectionInformationPanel(this.InheritedFromRuleSetAlertPanel, _GlobalResources.ThisEnrollmentHasBeenInheritedFromARulesetEnrollmentChangedWillSeparateIt, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));
                }
            }

            // clear controls from container
            this.EnrollmentFormContainer.Controls.Clear();

            // build the enrollment form
            this._BuildEnrollmentForm();

            // build the enrollment form actions panel
            this._BuildEnrollmentActionsPanel();

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulateEnrollmentFormInputElements();
        }
        #endregion

        #region _BuildEnrollmentForm
        /// <summary>
        /// Method to create the enrollment form fields
        /// </summary>
        private void _BuildEnrollmentForm()
        {
            // learningPaths container
            // if this is not for an existing enrollment, then we're adding a new enrollment, so display the learningPath select box
            if (!this._IsExistingEnrollment)
            {
                List<Control> enrollmentLearningPathsInputControls = new List<Control>();
                this._EligibleLearningPathsForSelectList = Asentia.LMS.Library.LearningPath.IdsAndNamesForSelectList(null);

                // build a container for the learningPaths listing
                this._LearningPathsListContainer = new Panel();
                this._LearningPathsListContainer.ID = "EnrollmentLearningPathsList_Container";
                this._LearningPathsListContainer.CssClass = "ItemListingContainer";
                this._LearningPathsListContainer.ClientIDMode = ClientIDMode.Static;

                //Add control to list
                enrollmentLearningPathsInputControls.Add(this._LearningPathsListContainer);

                // select learningPaths button
                // link
                Image selectLearningPathsImageForLink = new Image();
                selectLearningPathsImageForLink.ID = "LaunchSelectLearningPathsModalImage";
                selectLearningPathsImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
                selectLearningPathsImageForLink.CssClass = "MediumIcon";

                Localize selectLearningPathsTextForLink = new Localize();
                selectLearningPathsTextForLink.Text = _GlobalResources.AddLearningPath_s;

                LinkButton selectLearningPathsLink = new LinkButton();
                selectLearningPathsLink.ID = "LaunchSelectLearningPathsModal";
                selectLearningPathsLink.CssClass = "ImageLink";
                selectLearningPathsLink.Controls.Add(selectLearningPathsImageForLink);
                selectLearningPathsLink.Controls.Add(selectLearningPathsTextForLink);

                //Add control to list
                enrollmentLearningPathsInputControls.Add(selectLearningPathsLink);

                // selected learningPaths hidden field
                this._SelectedLearningPaths = new HiddenField();
                this._SelectedLearningPaths.ID = "SelectedLearningPaths_Field";
                this._SelectedLearningPaths.ClientIDMode = ClientIDMode.Static;

                //Add control to list
                enrollmentLearningPathsInputControls.Add(this._SelectedLearningPaths);

                // add controls to container
                this.EnrollmentFormContainer.Controls.Add(BuildMultipleInputControlFormField("EnrollmentLearningPaths",
                                                                                      _GlobalResources.LearningPath_s,
                                                                                      enrollmentLearningPathsInputControls,
                                                                                      true,
                                                                                      true,
                                                                                      false));

                // build modal for adding and removing learningPaths to/from the enrollment
                this._BuildSelectLearningPathsModal(selectLearningPathsLink.ID);
            }
            else
            {
                Label enrollmentLearningPathFieldStaticValue = new Label();
                enrollmentLearningPathFieldStaticValue.Text = this._EnrollmentObject.Title;

                // add controls to container
                this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentLearningPaths",
                                                                  _GlobalResources.LearningPath_s,
                                                                  enrollmentLearningPathFieldStaticValue.ID,
                                                                  enrollmentLearningPathFieldStaticValue,
                                                                  false,
                                                                  false,
                                                                  false));
            }

            // enrollment date start container
            // if the start date has not passed, its still editable, otherwise it is not
            if (!this._IsStartDatePassed)
            {
                this._DtStart = new DatePicker("EnrollmentDtStart_Field", false, true, true);
                this._DtStart.NowCheckboxText = _GlobalResources.StartEnrollmentImmediately;
                this._DtStart.Value = TimeZoneInfo.ConvertTimeFromUtc(AsentiaSessionState.UtcNow.AddMinutes(1), TimeZoneInfo.FindSystemTimeZoneById(new Timezone(AsentiaSessionState.GlobalSiteObject.IdTimezone).dotNetName));

                // add controls to container
                this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentDtStart",
                                                                  _GlobalResources.Start,
                                                                  this._DtStart.ID,
                                                                  this._DtStart,
                                                                  true,
                                                                  true,
                                                                  false));
            }
            else
            {
                DateTime dateValue = TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName));
                Label enrollmentDtStartFieldStaticValue = new Label();

                enrollmentDtStartFieldStaticValue.Text = dateValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern) + " @ " + dateValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern);

                // add controls to container
                this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentDtStart",
                                                                  _GlobalResources.Start,
                                                                  enrollmentDtStartFieldStaticValue.ID,
                                                                  enrollmentDtStartFieldStaticValue,
                                                                  true,
                                                                  true,
                                                                  false));
            }

            // enrollment timezone container
            this._Timezone = new TimeZoneSelector();
            this._Timezone.ID = "EnrollmentTimezone_Field";
            this._Timezone.SelectedValue = AsentiaSessionState.GlobalSiteObject.IdTimezone.ToString();

            this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentTimezone",
                                                              _GlobalResources.Timezone,
                                                              this._Timezone.ID,
                                                              this._Timezone,
                                                              true,
                                                              true,
                                                              false));

            // enrollment due container
            this._Due = new DateIntervalSelector("EnrollmentDue_Field", true);
            this._Due.NoneCheckboxText = _GlobalResources.NoDueDate;
            this._Due.NoneCheckBoxChecked = true;

            this._Due.TextBeforeSelector = _GlobalResources.LearnerMustCompleteTheLearningPathWithin;
            this._Due.TextAfterSelector = _GlobalResources.AfterEnrollment;

            this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentDue",
                                                              _GlobalResources.Due,
                                                              this._Due.ID,
                                                              this._Due,
                                                              true,
                                                              true,
                                                              false));

            // enrollment expires from start container
            this._ExpiresFromStart = new DateIntervalSelector("EnrollmentExpiresFromStart_Field", true);
            this._ExpiresFromStart.NoneCheckboxText = _GlobalResources.Indefinite;
            this._ExpiresFromStart.NoneCheckBoxChecked = true;

            this._ExpiresFromStart.TextBeforeSelector = _GlobalResources.LearnerHasAccessToTheLearningPathFor;
            this._ExpiresFromStart.TextAfterSelector = _GlobalResources.AfterEnrollment;

            this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentExpiresFromStart",
                                                              _GlobalResources.AccessFromStart,
                                                              this._ExpiresFromStart.ID,
                                                              this._ExpiresFromStart,
                                                              true,
                                                              true,
                                                              false));


            // enrollment expires from first launch input
            this._ExpiresFromFirstLaunch = new DateIntervalSelector("EnrollmentExpiresFromFirstLaunch_Field", true);
            this._ExpiresFromFirstLaunch.NoneCheckboxText = _GlobalResources.Indefinite;
            this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = true;
            this._ExpiresFromFirstLaunch.TextBeforeSelector = _GlobalResources.LearnerHasAccessToTheLearningPathFor;
            this._ExpiresFromFirstLaunch.TextAfterSelector = _GlobalResources.AfterFirstLaunchOfLearningPath;

            this.EnrollmentFormContainer.Controls.Add(BuildFormField("EnrollmentExpiresFromFirstLaunch",
                                                              _GlobalResources.AccessFromFirstLaunch,
                                                              this._ExpiresFromFirstLaunch.ID,
                                                              this._ExpiresFromFirstLaunch,
                                                              true,
                                                              true,
                                                              false));

        }
        #endregion

        #region _BuildSelectLearningPathsModal
        /// <summary>
        /// Builds the modal for selecting learningPaths to add to the enrollment.
        /// </summary>
        private void _BuildSelectLearningPathsModal(string targetControlId)
        {
            // set modal properties
            this._SelectLearningPaths = new ModalPopup("SelectLearningPathModal");
            this._SelectLearningPaths.Type = ModalPopupType.Form;
            this._SelectLearningPaths.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH,
                                                                        ImageFiles.EXT_PNG);
            this._SelectLearningPaths.HeaderIconAlt = _GlobalResources.AddLearningPath_s;
            this._SelectLearningPaths.HeaderText = _GlobalResources.AddLearningPath_s;
            this._SelectLearningPaths.TargetControlID = targetControlId;
            this._SelectLearningPaths.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectLearningPaths.SubmitButtonCustomText = _GlobalResources.AddLearningPath_s;
            this._SelectLearningPaths.SubmitButton.OnClientClick = "javascript:AddLearningPathsToEnrollment(); return false;";
            this._SelectLearningPaths.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectLearningPaths.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectLearningPaths.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the learningPath listing
            this._SelectEligibleLearningPaths = new DynamicListBox("SelectEligibleLearningPaths");
            this._SelectEligibleLearningPaths.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectEligibleLearningPaths.SearchButton.Command += new CommandEventHandler(this._SearchSelectLearningPathsButton_Command);
            this._SelectEligibleLearningPaths.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectLearningPathsButton_Command);
            this._SelectEligibleLearningPaths.ListBoxControl.DataSource = this._EligibleLearningPathsForSelectList;
            this._SelectEligibleLearningPaths.ListBoxControl.DataTextField = "title";
            this._SelectEligibleLearningPaths.ListBoxControl.DataValueField = "idLearningPath";
            this._SelectEligibleLearningPaths.ListBoxControl.DataBind();

            // add controls to body
            this._SelectLearningPaths.AddControlToBody(this._SelectEligibleLearningPaths);

            // add modal to container
            this.EnrollmentFormContainer.Controls.Add(this._SelectLearningPaths);
        }
        #endregion

        #region _SearchSelectLearningPathsButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select LearningPath(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectLearningPathsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectLearningPaths.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleLearningPaths.ListBoxControl.Items.Clear();

            // do the search
            this._EligibleLearningPathsForSelectList = Asentia.LMS.Library.LearningPath.IdsAndNamesForSelectList(this._SelectEligibleLearningPaths.SearchTextBox.Text);

            _SelectEligibleLearningPathsDataBind();
        }
        #endregion

        #region _ClearSearchSelectLearningPathsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select LearningPath(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectLearningPathsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectLearningPaths.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleLearningPaths.ListBoxControl.Items.Clear();
            this._SelectEligibleLearningPaths.SearchTextBox.Text = "";

            // clear the search
            this._EligibleLearningPathsForSelectList = Asentia.LMS.Library.LearningPath.IdsAndNamesForSelectList(null);

            _SelectEligibleLearningPathsDataBind();
        }
        #endregion

        #region _SelectEligibleLearningPathsDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectEligibleLearningPathsDataBind()
        {
            this._SelectEligibleLearningPaths.ListBoxControl.DataSource = this._EligibleLearningPathsForSelectList;
            this._SelectEligibleLearningPaths.ListBoxControl.DataTextField = "title";
            this._SelectEligibleLearningPaths.ListBoxControl.DataValueField = "idLearningPath";
            this._SelectEligibleLearningPaths.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._SelectEligibleLearningPaths.ListBoxControl.Items.Count == 0)
            {
                this._SelectLearningPaths.SubmitButton.Enabled = false;
                this._SelectLearningPaths.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectLearningPaths.SubmitButton.Enabled = true;
                this._SelectLearningPaths.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _BuildEnrollmentActionsPanel
        /// <summary>
        /// Builds the container and buttons for enrollment form actions.
        /// </summary>
        private void _BuildEnrollmentActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "EnrollmentSaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._EnrollmentObject == null)
            { this._SaveButton.Text = _GlobalResources.CreateEnrollment; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(this._EnrollmentFormSaveButton_Command);
            this._SaveButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicElements();");
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._EnrollmentFormCancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _PopulateEnrollmentFormInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateEnrollmentFormInputElements()
        {
            if (this._EnrollmentObject != null)
            {

                // timezone
                this._Timezone.SelectedValue = this._EnrollmentObject.IdTimezone.ToString();

                // date start
                if (!this._IsStartDatePassed)
                { this._DtStart.Value = TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName)); }

                // due
                if (this._EnrollmentObject.DueInterval != null)
                {
                    this._Due.NoneCheckBoxChecked = false;
                    this._Due.IntervalValue = this._EnrollmentObject.DueInterval.ToString();
                    this._Due.TimeframeValue = this._EnrollmentObject.DueTimeframe;
                }
                else
                {
                    this._Due.NoneCheckBoxChecked = true;
                    this._Due.IntervalValue = null;
                    this._Due.TimeframeValue = null;
                }

                // expires from start
                if (this._EnrollmentObject.ExpiresFromStartInterval != null)
                {
                    this._ExpiresFromStart.NoneCheckBoxChecked = false;
                    this._ExpiresFromStart.IntervalValue = this._EnrollmentObject.ExpiresFromStartInterval.ToString();
                    this._ExpiresFromStart.TimeframeValue = this._EnrollmentObject.ExpiresFromStartTimeframe;
                }
                else
                {
                    this._ExpiresFromStart.NoneCheckBoxChecked = true;
                    this._ExpiresFromStart.IntervalValue = null;
                    this._ExpiresFromStart.TimeframeValue = null;
                }

                // expires from first launch
                if (this._EnrollmentObject.ExpiresFromFirstLaunchInterval != null)
                {
                    this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = false;
                    this._ExpiresFromFirstLaunch.IntervalValue = this._EnrollmentObject.ExpiresFromFirstLaunchInterval.ToString();
                    this._ExpiresFromFirstLaunch.TimeframeValue = this._EnrollmentObject.ExpiresFromFirstLaunchTimeframe;
                }
                else
                {
                    this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = true;
                    this._ExpiresFromFirstLaunch.IntervalValue = null;
                    this._ExpiresFromFirstLaunch.TimeframeValue = null;
                }
            }
        }
        #endregion

        #region _SetEnrollmentFormAfterMultipleLearningPathSave
        private void _SetEnrollmentFormAfterMultipleLearningPathSave()
        {
            // disable form fields
            this._DtStart.Enabled = false;
            this._Timezone.Enabled = false;
            this._Due.Enabled = false;
            this._ExpiresFromStart.Enabled = false;
            this._ExpiresFromFirstLaunch.Enabled = false;

            // hide the save button
            this._SaveButton.Visible = false;

            // change the cancel button text and set an onclientclick action to return to enrollments page
            this._CancelButton.Text = _GlobalResources.Done;
            this._CancelButton.OnClientClick = "javascript:window.location='/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString() + "'; return false;";

            // list learningPaths enrollments were created for
            Panel enrollmentLearningPathsFieldContainer = (Panel)this.EnrollmentFormContainer.FindControl("EnrollmentLearningPaths_Container");

            if (enrollmentLearningPathsFieldContainer != null)
            {
                // build a container for the learningPaths listing
                this._LearningPathsListContainer = new Panel();
                this._LearningPathsListContainer.ID = "EnrollmentLearningPathsList_Container";
                this._LearningPathsListContainer.CssClass = "ItemListingContainer";

                if (!String.IsNullOrWhiteSpace(this._SelectedLearningPaths.Value))
                {
                    // split the "value" of the hidden field to get an array of learningPath prerequisite ids
                    string[] selectedLearningPathIds = this._SelectedLearningPaths.Value.Split(',');

                    if (selectedLearningPathIds.Length > 1)
                    {
                        // loop
                        foreach (string selectedLearningPathId in selectedLearningPathIds)
                        {
                            // container
                            Panel learningPathNameContainer = new Panel();
                            learningPathNameContainer.ID = "LearningPath_" + selectedLearningPathId;

                            // learningPath title
                            LearningPath learningPathObject = new LearningPath(Convert.ToInt32(selectedLearningPathId));
                            Literal learningPathName = new Literal();

                            // get learningPath title information
                            string learningPathTitleInInterfaceLanguage = learningPathObject.Name;

                            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                            {
                                foreach (LearningPath.LanguageSpecificProperty learningPathLanguageSpecificProperty in learningPathObject.LanguageSpecificProperties)
                                {
                                    if (learningPathLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                                    { learningPathTitleInInterfaceLanguage = learningPathLanguageSpecificProperty.Name; }
                                }
                            }

                            learningPathName.Text = learningPathTitleInInterfaceLanguage;

                            // add controls to container
                            learningPathNameContainer.Controls.Add(learningPathName);
                            this._LearningPathsListContainer.Controls.Add(learningPathNameContainer);
                        }
                    }

                    // add the list container to the field container
                    enrollmentLearningPathsFieldContainer.Controls.Add(this._LearningPathsListContainer);
                }
            }
        }
        #endregion

        #region _ValidateEnrollmentForm
        private bool _ValidateEnrollmentForm()
        {
            bool isValid = true;

            /* BASIC VALIDATIONS */

            // learningPath - if this is a new enrollment, at least one must have been selected, if not, it is a showstopper, which means return right away
            if (!this._IsExistingEnrollment)
            {
                if (String.IsNullOrWhiteSpace(this._SelectedLearningPaths.Value))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "SelectedLearningPaths", _GlobalResources.YouMustSelectOneOrMoreLearningPaths);
                    return isValid;
                }
            }

            // is locked by prerequisites - no validation needed

            // timezone - always required, must be a number, and if not valid, its a showstopper, which means return right away
            if (String.IsNullOrWhiteSpace(this._Timezone.SelectedValue))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentTimezone", _GlobalResources.Timezone + " " + _GlobalResources.IsRequired);
                return isValid;
            }

            int idTimezone;
            if (!int.TryParse(this._Timezone.SelectedValue, out idTimezone))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentTimezone", _GlobalResources.Timezone + " " + _GlobalResources.IsInvalid);
                return isValid;
            }

            // get the timezone's "dotnetname" from a timezone object.
            string tzDotNetName = new Timezone(Convert.ToInt32(idTimezone)).dotNetName;

            // date start - required, if its not specified its a showstopper, which means return right away
            // only validate if start has not passed
            if (!this._IsStartDatePassed)
            {
                if (this._DtStart.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDtStart", _GlobalResources.Start + " " + _GlobalResources.IsRequired);
                    return isValid;
                }
            }

            // due - always exists, never required, interval must be integer
            if (!this._Due.NoneCheckBoxChecked)
            {
                int dueInterval;
                if (String.IsNullOrWhiteSpace(this._Due.IntervalValue) || !int.TryParse(this._Due.IntervalValue, out dueInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.IsInvalid);
                }
            }

            // access from start - always exists, never required, interval must be integer
            if (!this._ExpiresFromStart.NoneCheckBoxChecked)
            {
                int expiresFromStartInterval;
                if (String.IsNullOrWhiteSpace(this._ExpiresFromStart.IntervalValue) || !int.TryParse(this._ExpiresFromStart.IntervalValue, out expiresFromStartInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentExpiresFromStart", _GlobalResources.AccessFromStart + " " + _GlobalResources.IsInvalid);
                }
            }

            // access from first launch - always exists, never required, interval must be integer
            if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
            {
                int expiresFromFirstLaunchInterval;
                if (String.IsNullOrWhiteSpace(this._ExpiresFromFirstLaunch.IntervalValue) || !int.TryParse(this._ExpiresFromFirstLaunch.IntervalValue, out expiresFromFirstLaunchInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentExpiresFromFirstLaunch", _GlobalResources.AccessFromFirstLaunch + " " + _GlobalResources.IsInvalid);
                }
            }

            /* CONSTRAINT VALIDATIONS BASED ON TYPE */

            // only apply constraint validations if the other validations have passed
            if (isValid)
            {
                // date start - must be in future, only validate if start has not passed
                DateTime dtStart;

                if (!this._IsStartDatePassed)
                {
                    if (this._DtStart.NowCheckBoxChecked)
                    { dtStart = DateTime.UtcNow.AddMinutes(1); }
                    else
                    { dtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName)); }

                    if (dtStart < AsentiaSessionState.UtcNow)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDtStart", _GlobalResources.Start + " " + _GlobalResources.MustBeInFuture);
                    }
                }
                else // this assumes that the Enrollment object exists and is populated, otherwise, this couldn't be fired due to logic in other places
                {
                    // if applying the the selected timezone to the unchanged original date start results in a
                    // new start date that has already passed, fail
                    // this only needs to be done when the timezone has been modified

                    // this is utc
                    dtStart = this._EnrollmentObject.DtStart;

                    if (this._EnrollmentObject.IdTimezone != Convert.ToInt32(this._Timezone.SelectedValue))
                    {
                        // convert start back to local (from original timezone)
                        DateTime originalLocalDtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName));

                        // convert local start to new utc
                        dtStart = TimeZoneInfo.ConvertTimeToUtc(originalLocalDtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(Convert.ToInt32(this._Timezone.SelectedValue)).dotNetName));

                        if (dtStart <= AsentiaSessionState.UtcNow)
                        {
                            isValid = false;
                            Timezone selectedTimezone = new Timezone(Convert.ToInt32(this._Timezone.SelectedValue));
                            string currentAdjustedStartValue = TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(selectedTimezone.dotNetName)).ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern)
                                                               + " @ "
                                                               + TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(selectedTimezone.dotNetName)).ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern)
                                                               + " "
                                                               + selectedTimezone.displayName;
                            this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDtStart", _GlobalResources.Start + " " + String.Format(_GlobalResources.CannotBeMovedEarlierThanCurrentValue, currentAdjustedStartValue));
                        }
                    }
                }

                // due - interval cannot exceed access from start and access from first launch intervals
                if (!this._Due.NoneCheckBoxChecked)
                {
                    // compare to expires from start interval
                    if (!this._ExpiresFromStart.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtDue;
                        DateTime calculatedDtExpiresFromStart;
                        int dueInterval = Convert.ToInt32(this._Due.IntervalValue);
                        int expiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                        switch (this._Due.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtDue = dtStart.AddDays(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtDue = dtStart.AddMonths(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtDue = dtStart.AddYears(dueInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtDue = DateTime.MinValue;
                                break;
                        }

                        switch (this._ExpiresFromStart.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromStart = dtStart.AddMonths(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromStart = dtStart.AddYears(expiresFromStartInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromStart = DateTime.MinValue;
                                break;
                        }

                        // compare calculated date due to calculated expires from start
                        if (calculatedDtDue > calculatedDtExpiresFromStart)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromStart);
                        }
                    }

                    // compare to expires from first launch interval
                    if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtDue;
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        int dueInterval = Convert.ToInt32(this._Due.IntervalValue);
                        int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                        switch (this._Due.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtDue = dtStart.AddDays(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtDue = dtStart.AddMonths(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtDue = dtStart.AddYears(dueInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtDue = DateTime.MinValue;
                                break;
                        }

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        // compare calculated date due to calculated expires from first launch
                        if (calculatedDtDue > calculatedDtExpiresFromFirstLaunch)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromFirstLaunch);
                        }
                    }
                }

                // expires from first launch - interval cannot exceed expires from start interval
                if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                {
                    // compare to expires from start interval
                    if (!this._ExpiresFromStart.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        DateTime calculatedDtExpiresFromStart;
                        int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);
                        int expiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        switch (this._ExpiresFromStart.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromStart = dtStart.AddMonths(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromStart = dtStart.AddYears(expiresFromStartInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromStart = DateTime.MinValue;
                                break;
                        }

                        // compare calculated expires from first launch to calculated expires from start
                        if (calculatedDtExpiresFromFirstLaunch > calculatedDtExpiresFromStart)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentExpiresFromFirstLaunch", _GlobalResources.AccessFromFirstLaunch + " " + _GlobalResources.CannotExceedAccessFromStart);
                        }
                    }
                }
            }

            // if this is invalid, we need to pre-populate multi-select fields with the objects that were selected
            if (!isValid && this._LearningPathsListContainer != null)
            {
                /* SELECTED LEARNINGPATHS */

                if (!String.IsNullOrWhiteSpace(this._SelectedLearningPaths.Value))
                {
                    // split the "value" of the hidden field to get an array of learningPath ids
                    string[] selectedLearningPathsIds = this._SelectedLearningPaths.Value.Split(',');

                    // loop through the array and add each learningPath to the listing container
                    foreach (string learningPathId in selectedLearningPathsIds)
                    {
                        if (this._LearningPathsListContainer.FindControl("LearningPaths_" + learningPathId) == null)
                        {
                            // learning path object
                            LearningPath learningPathObject = new LearningPath(Convert.ToInt32(learningPathId));

                            // container
                            Panel learningPathNameContainer = new Panel();
                            learningPathNameContainer.ID = "LearningPath_" + learningPathObject.Id.ToString();

                            // remove learningPath button
                            Image removeLearningPathImage = new Image();
                            removeLearningPathImage.ID = "LearningPath_" + learningPathObject.Id.ToString() + "_RemoveImage";
                            removeLearningPathImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                ImageFiles.EXT_PNG);
                            removeLearningPathImage.CssClass = "SmallIcon";
                            removeLearningPathImage.Attributes.Add("onClick", "javascript:RemoveLearningPathFromEnrollment('" + learningPathObject.Id.ToString() + "');");
                            removeLearningPathImage.Style.Add("cursor", "pointer");

                            // learning path name
                            Literal learningPathName = new Literal();
                            learningPathName.Text = learningPathObject.Name;

                            foreach (LearningPath.LanguageSpecificProperty learningPathLanguageSpecificProperty in learningPathObject.LanguageSpecificProperties)
                            {
                                if (learningPathLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                                {
                                    learningPathName.Text = learningPathLanguageSpecificProperty.Name;
                                    break;
                                }
                            }

                            // add controls to container
                            learningPathNameContainer.Controls.Add(removeLearningPathImage);
                            learningPathNameContainer.Controls.Add(learningPathName);
                            this._LearningPathsListContainer.Controls.Add(learningPathNameContainer);
                        }
                    }
                }
            }
            return isValid;
        }
        #endregion

        #region _EnrollmentFormSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for enrollment form.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _EnrollmentFormSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateEnrollmentForm())
                { throw new AsentiaException(); }

                // if there is no enrollment object, create one
                if (this._EnrollmentObject == null)
                { this._EnrollmentObject = new LearningPathEnrollment(); }

                int id;
                int uid = this._UserObject.Id;

                // populate the object

                // user id
                this._EnrollmentObject.IdUser = this._UserObject.Id;

                // timezone
                int oldTimezoneId = 0;

                if (this._EnrollmentObject.IdTimezone > 0)
                { oldTimezoneId = this._EnrollmentObject.IdTimezone; }

                this._EnrollmentObject.IdTimezone = Convert.ToInt32(this._Timezone.SelectedValue);

                // date start
                if (!this._IsStartDatePassed)
                {
                    if (this._DtStart.NowCheckBoxChecked)
                    { this._EnrollmentObject.DtStart = DateTime.UtcNow.AddMinutes(1); }
                    else
                    { this._EnrollmentObject.DtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName)); }
                }
                else
                {
                    // if the timezones have changed, convert start to utc from the original date start
                    if (oldTimezoneId > 0 && oldTimezoneId != this._EnrollmentObject.IdTimezone)
                    {
                        // convert start back to local
                        DateTime originalLocalDtStart = TimeZoneInfo.ConvertTimeFromUtc(this._EnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(oldTimezoneId).dotNetName));

                        // convert local start to new utc
                        this._EnrollmentObject.DtStart = TimeZoneInfo.ConvertTimeToUtc(originalLocalDtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._EnrollmentObject.IdTimezone).dotNetName));
                    }
                }

                // due
                if (this._Due.NoneCheckBoxChecked)
                {
                    this._EnrollmentObject.DueInterval = null;
                    this._EnrollmentObject.DueTimeframe = null;
                }
                else
                {
                    this._EnrollmentObject.DueInterval = Convert.ToInt32(this._Due.IntervalValue);
                    this._EnrollmentObject.DueTimeframe = this._Due.TimeframeValue;
                }

                // expires from start
                if (this._ExpiresFromStart.NoneCheckBoxChecked)
                {
                    this._EnrollmentObject.ExpiresFromStartInterval = null;
                    this._EnrollmentObject.ExpiresFromStartTimeframe = null;
                }
                else
                {
                    this._EnrollmentObject.ExpiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);
                    this._EnrollmentObject.ExpiresFromStartTimeframe = this._ExpiresFromStart.TimeframeValue;
                }

                // expires from first launch
                if (this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                {
                    this._EnrollmentObject.ExpiresFromFirstLaunchInterval = null;
                    this._EnrollmentObject.ExpiresFromFirstLaunchTimeframe = null;
                }
                else
                {
                    this._EnrollmentObject.ExpiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);
                    this._EnrollmentObject.ExpiresFromFirstLaunchTimeframe = this._ExpiresFromFirstLaunch.TimeframeValue;
                }

                // if this is an existing enrollment and was inherited by ruleset, detach it from the ruleset enrollment
                if (this._IsExistingEnrollment && this._EnrollmentObject.IdRuleSetLearningPathEnrollment != null)
                {
                    this._EnrollmentObject.IdRuleSetLearningPathEnrollment = null;                    
                }

                bool saveForMultipleLearningPaths = false;
                DataTable learningPaths = new DataTable();
                learningPaths.Columns.Add("id", typeof(int));

                // do learningPath(s) only if this is a new enrollment
                if (!this._IsExistingEnrollment)
                {
                    if (!String.IsNullOrWhiteSpace(this._SelectedLearningPaths.Value))
                    {
                        // split the "value" of the hidden field to get an array of learningPath prerequisite ids
                        string[] selectedLearningPathIds = this._SelectedLearningPaths.Value.Split(',');

                        // if there is more than one learningPath selected, we need to put them in a datatable and save for multiple
                        if (selectedLearningPathIds.Length > 1)
                        {
                            // put ids into datatable 
                            foreach (string selectedLearningPathPrerequisiteId in selectedLearningPathIds)
                            { learningPaths.Rows.Add(Convert.ToInt32(selectedLearningPathPrerequisiteId)); }

                            saveForMultipleLearningPaths = true;
                        }
                        else if (selectedLearningPathIds.Length == 1)
                        { this._EnrollmentObject.IdLearningPath = Convert.ToInt32(selectedLearningPathIds[0]); }
                    }
                }

                if (saveForMultipleLearningPaths)
                {
                    // save enrollments for multiple learningPaths
                    this._EnrollmentObject.SaveForMultipleLearningPaths(learningPaths);

                    // set the existing enrollment flag
                    this._IsExistingEnrollment = true;

                    // clear group actions menu
                    this.UserObjectMenuContainer.Controls.Clear();

                    // build the page controls
                    this._BuildControls();

                    // set the enrollment form after saving for multiple learningPaths, this disables fields, saving etc.
                    this._SetEnrollmentFormAfterMultipleLearningPathSave();

                    // display the saved feedback
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.EnrollmentsHaveBeenSavedSuccessfully, false);
                }
                else
                {
                    // save the enrollment, save its returned id to viewstate, and 
                    // instansiate a new enrollment object with the id
                    id = this._EnrollmentObject.Save(true);
                    this.ViewState["id"] = id;
                    this._EnrollmentObject.Id = id;

                    // load the saved enrollment object
                    this._EnrollmentObject = new LearningPathEnrollment(id);

                    // set the existing enrollment flag
                    this._IsExistingEnrollment = true;

                    // determine if the start date has passed based on the saved enrollment information
                    if (this._EnrollmentObject.DtStart <= AsentiaSessionState.UtcNow)
                    { this._IsStartDatePassed = true; }
                    else
                    { this._IsStartDatePassed = false; }

                    // clear group actions menu
                    this.UserObjectMenuContainer.Controls.Clear();

                    // build the page controls
                    this._BuildControls();

                    // display the saved feedback
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.EnrollmentHasBeenSavedSuccessfully, false);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _EnrollmentFormCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for Enrollment form.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _EnrollmentFormCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString());
        }
        #endregion
    }
}
