﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Users.Enrollments
{
    public class LearningPathActivity : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel EnrollmentFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel EnrollmentFormWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel EnrollmentFormContainer;
        public Panel ActionsPanel;
        public UpdatePanel ActivityGridUpdatePanel;
        public Grid LearningPathActivityGrid;
        #endregion

        #region Private Properties
        private User _UserObject;
        private LearningPathEnrollment _LearningPathEnrollmentObject;

        private DropDownList _CompletionStatus;
        private DatePicker _CompletionDate;

        private Button _SaveButton;
        private Button _CancelButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.Users.Enrollments.Activity.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the enrollment and user objects
            this._GetEnrollmentAndUserObjects();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/users/enrollments/LearningPathActivity.css");

            // initialize the administrator menu
            this.InitializeAdminMenu();           

            // build the controls for the page
            this._BuildControls();

            // build Activity grid
            this._BuildGrid();

            if (!Page.IsPostBack)
            {
                this.LearningPathActivityGrid.BindData();
            }
        }
        #endregion

        #region _GetEnrollmentAndUserObjects
        /// <summary>
        /// Gets the user and enrollment objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetEnrollmentAndUserObjects()
        {
            // get the id querystring parameter
            int qsUId = this.QueryStringInt("uid", 0);
            int vsUId = this.ViewStateInt(this.ViewState, "uid", 0);
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            // get user object - user object MUST be specified and exist for this page to load
            if (qsUId > 0 || vsUId > 0)
            {
                int uid = 0;

                if (qsUId > 0)
                { uid = qsUId; }

                if (vsUId > 0)
                { uid = vsUId; }

                try
                {
                    if (uid > 0)
                    { this._UserObject = new User(uid); }
                }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else
            { Response.Redirect("~/administrator/users"); }

            // get enrollment object - enrollment object MUST be specified and exist for this page to load
            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    {
                        this._LearningPathEnrollmentObject = new LearningPathEnrollment(id);
                    }
                }
                catch
                { Response.Redirect("~/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString()); }
            }
            else
            { Response.Redirect("~/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString()); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {           
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            // evaluate for breadcrumb and page title information
            string pageTitle = this._LearningPathEnrollmentObject.Title + ": " + _GlobalResources.Activity;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(this._UserObject.DisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Enrollments, "/administrator/users/enrollments/Default.aspx?uid=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(pageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to create the page Breadcrumb, user action menu controls and other controls on page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.EnrollmentFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.EnrollmentFormWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu container
            this.UserObjectMenuContainer.Controls.Clear();

            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Enrollments;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.TheUsersCurrentLearningPathEnrollmentActivityIsDisplayedBelow, true);

            // clear controls from container
            this.EnrollmentFormContainer.Controls.Clear();

            // build the enrollment form
            this._BuildLearningPathEnrollmentForm();

            // build the enrollment form actions panel
            this._BuildEnrollmentActionsPanel();
        }
        #endregion

        #region _BuildLearningPathEnrollmentForm
        /// <summary>
        /// Learning path enrollment form
        /// </summary>
        private void _BuildLearningPathEnrollmentForm()
        {
            // get the timezone of the enrollment
            Timezone enrollmentTimezone = new Timezone(this._LearningPathEnrollmentObject.IdTimezone);

            // enrollment date start container
            DateTime dateStartValue = TimeZoneInfo.ConvertTimeFromUtc(this._LearningPathEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(enrollmentTimezone.dotNetName));

            Label enrollmentDtStartFieldStaticValue = new Label();
            enrollmentDtStartFieldStaticValue.Text = dateStartValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern) + " @ " + dateStartValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern);

            this.EnrollmentFormContainer.Controls.Add(AsentiaPage.BuildFormField("EnrollmentDtStart",
                                                                          _GlobalResources.Start,
                                                                          enrollmentDtStartFieldStaticValue.ID,
                                                                          enrollmentDtStartFieldStaticValue,
                                                                          false,
                                                                          false,
                                                                          false));

            // enrollment timezone container
            Label enrollmentTimezoneFieldStaticValue = new Label();
            enrollmentTimezoneFieldStaticValue.Text = enrollmentTimezone.displayName;

            this.EnrollmentFormContainer.Controls.Add(AsentiaPage.BuildFormField("EnrollmentTimezone",
                                                                          _GlobalResources.Timezone,
                                                                          enrollmentTimezoneFieldStaticValue.ID,
                                                                          enrollmentTimezoneFieldStaticValue,
                                                                          false,
                                                                          false,
                                                                          false));

            // enrollment completion status input
            List<Control> enrollmentCompletionInputControls = new List<Control>();

            // completion status field
            this._CompletionStatus = new DropDownList();
            this._CompletionStatus.ID = "EnrollmentCompletionStatus_Field";
            this._CompletionStatus.Items.Add(new ListItem(_GlobalResources.IncompleteInProgress, Convert.ToInt32(Lesson.LessonCompletionStatus.Incomplete).ToString()));
            this._CompletionStatus.Items.Add(new ListItem(_GlobalResources.Completed, Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString()));

            if (this._LearningPathEnrollmentObject.DtCompleted != null)
            { this._CompletionStatus.SelectedValue = Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString(); }
            else
            { this._CompletionStatus.SelectedValue = Convert.ToInt32(Lesson.LessonCompletionStatus.Incomplete).ToString(); }

            this._CompletionStatus.Attributes.Add("onchange", "CompletionStatusDropDownChange('Enrollment');");

            enrollmentCompletionInputControls.Add(this._CompletionStatus);

            // "on" label            
            Label enrollmentCompletionStatusFieldOnStaticValue = new Label();
            enrollmentCompletionStatusFieldOnStaticValue.ID = "OnLabel";
            enrollmentCompletionStatusFieldOnStaticValue.Text = " " + _GlobalResources.On + " ";
            enrollmentCompletionInputControls.Add(enrollmentCompletionStatusFieldOnStaticValue);

            // completed on field
            this._CompletionDate = new DatePicker("EnrollmentCompletionDate_Field", false, false, true);

            if (this._LearningPathEnrollmentObject.DtCompleted != null)
            { this._CompletionDate.Value = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._LearningPathEnrollmentObject.DtCompleted, TimeZoneInfo.FindSystemTimeZoneById(enrollmentTimezone.dotNetName)); }
            else
            { this._CompletionDate.Enabled = false; }

            enrollmentCompletionInputControls.Add(this._CompletionDate);

            //add the multiple contol list to main panel
            this.EnrollmentFormContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("EnrollmentCompletionStatus",
                                                                                     _GlobalResources.CompletionStatus,
                                                                                     enrollmentCompletionInputControls,
                                                                                     true,
                                                                                     true,
                                                                                     true));
        }
        #endregion

        #region _BuildEnrollmentActionsPanel
        /// <summary>
        /// Builds the container and buttons for enrollment form actions.
        /// </summary>
        private void _BuildEnrollmentActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "EnrollmentSaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._EnrollmentFormSaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._EnrollmentFormCancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the Activity listing.
        /// </summary>
        private void _BuildGrid()
        {
            this.LearningPathActivityGrid.AddCheckboxColumn = false;
            this.LearningPathActivityGrid.ShowSearchBox = false;
            this.LearningPathActivityGrid.AllowPaging = false;
            this.LearningPathActivityGrid.ShowRecordsPerPageSelectbox = false;

            this.LearningPathActivityGrid.StoredProcedure = Library.LearningPathEnrollment.GridProcedureForLearnersStatus;
            this.LearningPathActivityGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.LearningPathActivityGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.LearningPathActivityGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.LearningPathActivityGrid.AddFilter("@idLearningPathEnrollment", SqlDbType.Int, 4, this._LearningPathEnrollmentObject.Id);
            this.LearningPathActivityGrid.IdentifierField = "idLearningPathEnrollment";
            this.LearningPathActivityGrid.DefaultSortColumn = "order";

            // data key names
            this.LearningPathActivityGrid.DataKeyNames = new string[] { "idLearningPathEnrollment" };

            // columns
            GridColumn courseTitle = new GridColumn(_GlobalResources.CourseTitle, "courseTitle");

            GridColumn courseCompletionStatus = new GridColumn(_GlobalResources.Status, "isCompleted");
            courseCompletionStatus.AddProperty(new GridColumnProperty("True", _GlobalResources.Completed));
            courseCompletionStatus.AddProperty(new GridColumnProperty("False", _GlobalResources.IncompleteInProgress));

            // add columns to data grid 
            this.LearningPathActivityGrid.AddColumn(courseTitle);
            this.LearningPathActivityGrid.AddColumn(courseCompletionStatus);
        }
        #endregion

        #region _ValidateEnrollmentForm
        /// <summary>
        /// Method to validate the enrollment form inputs from the user before saving 
        /// </summary>
        /// <returns></returns>
        private bool _ValidateEnrollmentForm()
        {
            bool isValid = true;

            if (this._CompletionStatus.SelectedValue == Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString())
            {
                if (this._CompletionDate.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EnrollmentFormContainer, "EnrollmentCompletionStatus", _GlobalResources.CompletionStatus + " " + _GlobalResources.RequiresADateWhenCompletedIsSelected);
                    return isValid;
                }
            }

            return isValid;
        }
        #endregion

        #region _EnrollmentFormSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for enrollment form.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _EnrollmentFormSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateEnrollmentForm())
                { throw new AsentiaException(); }

                int id;

                // populate the enrollment completed date property based on the selected completion status value
                if (this._CompletionStatus.SelectedValue == Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString())
                { this._LearningPathEnrollmentObject.DtCompleted = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._CompletionDate.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._LearningPathEnrollmentObject.IdTimezone).dotNetName)); }
                else
                { this._LearningPathEnrollmentObject.DtCompleted = null; }

                // save the enrollment's completion status, save its returned id to viewstate, and 
                // instansiate a new learning path enrollment object object with the id
                id = this._LearningPathEnrollmentObject.SaveCompletionStatus();
                this.ViewState["id"] = id;
                this._LearningPathEnrollmentObject.Id = id;

                // load the saved enrollment object
                this._LearningPathEnrollmentObject = new LearningPathEnrollment(id);

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.EnrollmentActivityHasBeenSavedSuccessfully, false);

                // rebind the activity grid
                this.LearningPathActivityGrid.BindData();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dnfEx.Message, true);

                // rebind the activity grid
                this.LearningPathActivityGrid.BindData();
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, fnuEx.Message, true);

                // rebind the activity grid
                this.LearningPathActivityGrid.BindData();
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, cpeEx.Message, true);

                // rebind the activity grid
                this.LearningPathActivityGrid.BindData();
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dEx.Message, true);

                // rebind the activity grid
                this.LearningPathActivityGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);

                // rebind the activity grid
                this.LearningPathActivityGrid.BindData();
            }
        }
        #endregion

        #region _EnrollmentFormCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for Enrollment form.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _EnrollmentFormCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/users/enrollments/?uid=" + this._UserObject.Id.ToString());
        }
        #endregion
    }
}
