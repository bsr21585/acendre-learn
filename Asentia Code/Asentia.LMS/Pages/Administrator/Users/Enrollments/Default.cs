﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.Users.Enrollments
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel EnrollmentsFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel EnrollmentsPageWrapperContainer;
        public UpdatePanel EnrollmentsGridUpdatePanel;
        public Panel UserEnrollmentsPropertiesContainer;
        #endregion

        #region Private Properties
        private User _UserObject;

        private ModalPopup _CourseResetConfirmAction;
        private ModalPopup _TogglePrerequisiteLockConfirmAction;
        private ModalPopup _CourseGridConfirmAction;
        private ModalPopup _LearningPathGridConfirmAction;

        private HiddenField _HiddenResetButton;
        private HiddenField _ResetEnrollmentId;
        private HiddenField _HiddenTogglePrerequisiteLockButton;
        private HiddenField _TogglePrerequisiteEnrollmentId;

        private LinkButton _CourseDeleteButton;
        private LinkButton _LearningPathDeleteButton;        

        private Grid _CourseEnrollmentsGrid;
        private Grid _LearningPathEnrollmentsGrid;

        private Panel _CourseGridActionsPanel;
        private Panel _LearningPathGridActionsPanel;
        private Panel _UserEnrollmentsTabPanelsContainer;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Users.Enrollments.Default.js");
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the user object
            this._GetUserObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/users/enrollments/Default.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.EnrollmentsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.EnrollmentsPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu
            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.Enrollments;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // build the grid, actions panel, and modals
            this._BuildObjectOptionsPanel();
            this._BuildPageControls();

            this._BuildTogglePrerequisiteLockConfirmModal();

            // if not postback
            if (!IsPostBack)
            {
                // Course Enrollments grid bind data
                this._CourseEnrollmentsGrid.BindData();

                // Learning Path grid bind data
                this._LearningPathEnrollmentsGrid.BindData();
            }
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetUserObject()
        {
            // get the user id querystring parameter
            int qsUId = this.QueryStringInt("uid", 0);

            if (qsUId > 0)
            {
                int uid = 0;

                if (qsUId > 0)
                { uid = qsUId; }

                try
                {
                    if (uid > 0)
                    { this._UserObject = new User(uid); }
                }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {            
            // get user name information
            string userDisplayName = this._UserObject.DisplayName;
            string userImagePath;
            string userImageCssClass = null;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                userImageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(userDisplayName, "/administrator/users/Dashboard.aspx?id=" + this._UserObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Enrollments));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, userDisplayName, userImagePath, _GlobalResources.Enrollments, ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG), userImageCssClass);
        }
        #endregion        

        #region _BuildPageControls
        private void _BuildPageControls()
        {
            // build user enrollemnts tabs
            this._BuildTabsList();

            this._UserEnrollmentsTabPanelsContainer = new Panel();
            this._UserEnrollmentsTabPanelsContainer.ID = "UserEnrollments_TabPanelsContainer";
            this._UserEnrollmentsTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.UserEnrollmentsPropertiesContainer.Controls.Add(this._UserEnrollmentsTabPanelsContainer);

            this._BuildUserEnrollmentsListPanel();
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD COURSE ENROLLMENT
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddCourseEnrollmentLink",
                                                null,
                                                "Modify.aspx?uid=" + this._UserObject.Id.ToString(),
                                                null,
                                                _GlobalResources.NewCourseEnrollment,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            // ADD LEARNING PATH ENROLLMENT
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddLearningPathEnrollmentLink",
                                                null,
                                                "ModifyLearningPath.aspx?uid=" + this._UserObject.Id.ToString(),
                                                null,
                                                _GlobalResources.NewLearningPathEnrollment,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for the form.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("CourseList", _GlobalResources.Course));
            tabs.Enqueue(new KeyValuePair<string, string>("LearningPathList", _GlobalResources.LearningPath));

            // build and attach the tabs
            this.UserEnrollmentsPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("UserEnrollments", tabs));
        }



        #endregion

        #region _BuildUserEnrollmentsListPanel
        /// <summary>
        /// Builds the properties form.
        /// </summary>
        private void _BuildUserEnrollmentsListPanel()
        {
            #region Course Enrollments

            // "course enrollments" is the default tab, so this is visible on page load.
            Panel courseEnrollmentsPanel = new Panel();
            courseEnrollmentsPanel.ID = "UserEnrollments_" + "CourseList" + "_TabPanel";
            courseEnrollmentsPanel.Attributes.Add("style", "display: block;");

            // list of course enrollments tab controls
            List<Control> courseEnrollmentsListsInputControls = new List<Control>();

            // build course enrollments grid
            this._BuildCourseEnrollmentsGrid();
            UpdatePanel courseEnrollmentsGridUpdatePanel = new UpdatePanel();
            courseEnrollmentsGridUpdatePanel.ID = "CourseEnrollmentsGridUpdatePanel";
            courseEnrollmentsGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            courseEnrollmentsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._CourseEnrollmentsGrid);

            // adding update panel to controls list
            courseEnrollmentsListsInputControls.Add(courseEnrollmentsGridUpdatePanel);

            // build course enrollment grid action panel 
            this._BuildCourseGridActionsPanel();
            courseEnrollmentsListsInputControls.Add(this._CourseGridActionsPanel);

            // build course enrollment grid action modal popup
            this._BuildCourseGridActionsModal();
            courseEnrollmentsListsInputControls.Add(this._CourseGridConfirmAction);

            this._BuildCourseResetConfirmModal();
            courseEnrollmentsListsInputControls.Add(this._CourseResetConfirmAction);

            //add controls to panel
            courseEnrollmentsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Course_List",
                                                                                               String.Empty,
                                                                                               courseEnrollmentsListsInputControls,
                                                                                               false,
                                                                                               true));

            #endregion

            #region Learning Path Enrollments

            Panel lerningPathEnrollmentsPanel = new Panel();
            lerningPathEnrollmentsPanel.ID = "UserEnrollments_" + "LearningPathList" + "_TabPanel";
            lerningPathEnrollmentsPanel.Attributes.Add("style", "display: none;");

            // list of learning path enrollments tab controls
            List<Control> learningPathEnrollmentsListsInputControls = new List<Control>();

            // build learning path enrollments grid
            this._BuildLearningPathEnrollmentsGrid();
            UpdatePanel learningPathEnrollmentsGridUpdatePanel = new UpdatePanel();
            learningPathEnrollmentsGridUpdatePanel.ID = "LearningPathEnrollmentsGridUpdatePanel";
            learningPathEnrollmentsGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            learningPathEnrollmentsGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._LearningPathEnrollmentsGrid);

            learningPathEnrollmentsListsInputControls.Add(learningPathEnrollmentsGridUpdatePanel);

            // build learning path enrollment grid action panel 
            this._BuildLearningPathGridActionsPanel();
            learningPathEnrollmentsListsInputControls.Add(this._LearningPathGridActionsPanel);

            // build learning path enrollment grid action modal popup
            this._BuildLearningPathGridActionsModal();
            learningPathEnrollmentsListsInputControls.Add(this._LearningPathGridConfirmAction);

            // add controls to panel
            lerningPathEnrollmentsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LearningPath_List",
                                                                                                    String.Empty,
                                                                                                    learningPathEnrollmentsListsInputControls,
                                                                                                    false,
                                                                                                    true));

            #endregion

            //add controls to page
            this._UserEnrollmentsTabPanelsContainer.Controls.Add(courseEnrollmentsPanel);
            this._UserEnrollmentsTabPanelsContainer.Controls.Add(lerningPathEnrollmentsPanel);
        }
        #endregion

        #region Course
        #region _BuildCourseEnrollmentsGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildCourseEnrollmentsGrid()
        {
            this._CourseEnrollmentsGrid = new Grid();
            this._CourseEnrollmentsGrid.ID = "CourseEnrollmentsGrid";
            this._CourseEnrollmentsGrid.StoredProcedure = Library.Enrollment.GridProcedure;
            this._CourseEnrollmentsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._CourseEnrollmentsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._CourseEnrollmentsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._CourseEnrollmentsGrid.AddFilter("@idUser", SqlDbType.Int, 4, this._UserObject.Id);
            this._CourseEnrollmentsGrid.IdentifierField = "idEnrollment";
            this._CourseEnrollmentsGrid.DefaultSortColumn = "title";

            // data key names
            this._CourseEnrollmentsGrid.DataKeyNames = new string[] { "idEnrollment" };

            // columns
            GridColumn title = new GridColumn(_GlobalResources.Course + ", " + _GlobalResources.Type, null, "title"); // this is calculated dynamically in the RowDataBound method            

            GridColumn dtStart = new GridColumn(_GlobalResources.Start, "dtStart", "dtStart");

            GridColumn dtDue = new GridColumn(_GlobalResources.Due, "dtDue", "dtDue");

            GridColumn dtExpires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires");

            GridColumn enrollmentStatus = new GridColumn(_GlobalResources.Status, "enrollmentStatus");
            enrollmentStatus.AddProperty(new GridColumnProperty(Convert.ToInt32(EnrollmentStatus.Enrolled).ToString(), _GlobalResources.Enrolled));
            enrollmentStatus.AddProperty(new GridColumnProperty(Convert.ToInt32(EnrollmentStatus.Completed).ToString(), _GlobalResources.Completed));
            enrollmentStatus.AddProperty(new GridColumnProperty(Convert.ToInt32(EnrollmentStatus.Overdue).ToString(), _GlobalResources.Overdue));
            enrollmentStatus.AddProperty(new GridColumnProperty(Convert.ToInt32(EnrollmentStatus.Expired).ToString(), _GlobalResources.Expired));
            enrollmentStatus.AddProperty(new GridColumnProperty(Convert.ToInt32(EnrollmentStatus.Future).ToString(), _GlobalResources.Future));

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this._CourseEnrollmentsGrid.AddColumn(title);
            this._CourseEnrollmentsGrid.AddColumn(dtStart);
            this._CourseEnrollmentsGrid.AddColumn(dtDue);
            this._CourseEnrollmentsGrid.AddColumn(dtExpires);
            this._CourseEnrollmentsGrid.AddColumn(enrollmentStatus);
            this._CourseEnrollmentsGrid.AddColumn(options);

            // add row data bound event
            this._CourseEnrollmentsGrid.RowDataBound += new GridViewRowEventHandler(this._CourseEnrollmentsGrid_RowDataBound);
        }
        #endregion

        #region _CourseEnrollmentsGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the course enrollments grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _CourseEnrollmentsGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                
                // disable the check box if the enrollment is completed because completed enrollments cannot be deleted
                int enrollmentStatus = Convert.ToInt32(rowView["enrollmentStatus"]);
                if (enrollmentStatus == Convert.ToInt32(EnrollmentStatus.Completed))
                {
                    CheckBox enrollmentSelectCheckBox = (CheckBox)e.Row.Cells[0].FindControl("CourseEnrollmentsGrid_GridSelectRecord_" + e.Row.RowIndex.ToString());
                    enrollmentSelectCheckBox.Enabled = false;
                }

                int idEnrollment = Convert.ToInt32(rowView["idEnrollment"]);                

                // AVATAR, COURSE, TYPE

                string courseName = rowView["title"].ToString();
                EnrollmentType enrollmentType = (EnrollmentType)Convert.ToInt32(rowView["enrollmentType"]);
                bool isResetOn = Convert.ToBoolean(rowView["isResetOn"]);
                bool togglePrerequisites = Convert.ToBoolean(rowView["togglePrerequisites"]);
                bool isActivityOn = Convert.ToBoolean(rowView["isActivityOn"]);
                bool isModifyOn = Convert.ToBoolean(rowView["isModifyOn"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = courseName;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // label
                Label labelLabelWrapper = new Label();
                labelLabelWrapper.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(labelLabelWrapper);

                Literal labelLabel = new Literal();
                labelLabel.Text = courseName;
                labelLabelWrapper.Controls.Add(labelLabel);

                // type
                Label typeLabelWrapper = new Label();
                typeLabelWrapper.CssClass = "GridSecondaryLine";
                e.Row.Cells[1].Controls.Add(typeLabelWrapper);

                Literal typeLabel = new Literal();                
                typeLabelWrapper.Controls.Add(typeLabel);

                switch (enrollmentType)
                {
                    case EnrollmentType.RuleSetInheritedOneTime:
                        typeLabel.Text = _GlobalResources.OneTimeRulesetInherited;
                        break;
                    case EnrollmentType.RuleSetInheritedRecurring:
                        typeLabel.Text = _GlobalResources.RecurringRulesetInherited;
                        break;
                    case EnrollmentType.GroupInheritedOneTime:
                        typeLabel.Text = _GlobalResources.OneTimeGroupInherited;
                        break;
                    case EnrollmentType.ManuallyAssignedOneTime:
                        typeLabel.Text = _GlobalResources.OneTime;
                        break;
                    default:
                        break;
                }

                // OPTIONS COLUMN  
                
                // reset
                if (isResetOn)
                {
                    Label resetSpan = new Label();
                    resetSpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(resetSpan);

                    HyperLink resetLink = new HyperLink();
                    resetLink.NavigateUrl = "javascript: void(0);";
                    resetLink.Attributes.Add("onclick", "ResetEnrollment(" + idEnrollment.ToString() + ");");
                    resetSpan.Controls.Add(resetLink);

                    Image resetIcon = new Image();
                    resetIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RESET, ImageFiles.EXT_PNG);
                    resetIcon.AlternateText = _GlobalResources.Reset;
                    resetIcon.ToolTip = _GlobalResources.Reset;
                    resetLink.Controls.Add(resetIcon);
                }
                else
                {
                    Label resetSpan = new Label();
                    resetSpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(resetSpan);

                    Image resetIcon = new Image();
                    resetIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RESET, ImageFiles.EXT_PNG);
                    resetIcon.CssClass = "DimIcon";
                    resetIcon.AlternateText = _GlobalResources.ResetDisabled;
                    resetIcon.ToolTip = _GlobalResources.ResetDisabled;
                    resetSpan.Controls.Add(resetIcon);
                }

                // toggle prerequisites
                if (togglePrerequisites)
                {
                    Label togglePrerequisitesSpan = new Label();
                    togglePrerequisitesSpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(togglePrerequisitesSpan);

                    HyperLink togglePrerequisitesLink = new HyperLink();
                    togglePrerequisitesLink.NavigateUrl = "javascript: void(0);";
                    togglePrerequisitesLink.Attributes.Add("onclick", "TogglePrerequisiteLockForEnrollment(" + idEnrollment.ToString() + ");");
                    togglePrerequisitesSpan.Controls.Add(togglePrerequisitesLink);

                    Image togglePrerequisitesIcon = new Image();
                    togglePrerequisitesIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD, ImageFiles.EXT_PNG);
                    togglePrerequisitesIcon.AlternateText = _GlobalResources.PrerequisitesPending;
                    togglePrerequisitesIcon.ToolTip = _GlobalResources.UnlockEnrollmentPrerequisites;
                    togglePrerequisitesLink.Controls.Add(togglePrerequisitesIcon);
                }
                else
                {
                    Label togglePrerequisitesSpan = new Label();
                    togglePrerequisitesSpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(togglePrerequisitesSpan);                    

                    Image togglePrerequisitesIcon = new Image();
                    togglePrerequisitesIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_CHECK, ImageFiles.EXT_PNG);                    
                    togglePrerequisitesIcon.AlternateText = _GlobalResources.PrerequisitesCompleted;
                    togglePrerequisitesIcon.ToolTip = _GlobalResources.PrerequisitesCompleted;
                    togglePrerequisitesSpan.Controls.Add(togglePrerequisitesIcon);
                }                

                // activity
                if (isActivityOn)
                {
                    Label activitySpan = new Label();
                    activitySpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(activitySpan);

                    HyperLink activityLink = new HyperLink();
                    activityLink.NavigateUrl = "Activity.aspx?uid=" + this._UserObject.Id.ToString() + "&id=" + idEnrollment.ToString();
                    activitySpan.Controls.Add(activityLink);

                    Image activityIcon = new Image();
                    activityIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ACTIVITY, ImageFiles.EXT_PNG);
                    activityIcon.AlternateText = _GlobalResources.Activity;
                    activityIcon.ToolTip = _GlobalResources.Activity;
                    activityLink.Controls.Add(activityIcon);
                }
                else
                {
                    Label activitySpan = new Label();
                    activitySpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(activitySpan);

                    Image activityIcon = new Image();
                    activityIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ACTIVITY, ImageFiles.EXT_PNG);
                    activityIcon.CssClass = "DimIcon";
                    activityIcon.AlternateText = _GlobalResources.ActivityDisabled;
                    activityIcon.ToolTip = _GlobalResources.ActivityDisabled;
                    activitySpan.Controls.Add(activityIcon);
                }

                // modify
                if (isModifyOn)
                {
                    Label modifySpan = new Label();
                    modifySpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(modifySpan);

                    HyperLink modifyLink = new HyperLink();
                    modifyLink.NavigateUrl = "Modify.aspx?uid=" + this._UserObject.Id.ToString() + "&id=" + idEnrollment.ToString();
                    modifySpan.Controls.Add(modifyLink);

                    Image modifyIcon = new Image();
                    modifyIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
                    modifyIcon.AlternateText = _GlobalResources.Modify;
                    modifyIcon.ToolTip = _GlobalResources.Modify;
                    modifyLink.Controls.Add(modifyIcon);
                }
                else
                {
                    Label modifySpan = new Label();
                    modifySpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(modifySpan);

                    Image modifyIcon = new Image();
                    modifyIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
                    modifyIcon.CssClass = "DimIcon";
                    modifyIcon.AlternateText = _GlobalResources.ModifyDisabled;
                    modifyIcon.ToolTip = _GlobalResources.ModifyDisabled;
                    modifySpan.Controls.Add(modifyIcon);
                }
            }
        }
        #endregion

        #region _BuildCourseGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildCourseGridActionsPanel()
        {
            this._CourseGridActionsPanel = new Panel();
            this._CourseGridActionsPanel.ID = "CourseGridActionsPanel";
            this._CourseGridActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._CourseDeleteButton = new LinkButton();
            this._CourseDeleteButton.ID = "CourseGridDeleteButton";
            this._CourseDeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._CourseDeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedEnrollment_s;
            this._CourseDeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this._CourseGridActionsPanel.Controls.Add(this._CourseDeleteButton);
        }
        #endregion

        #region _BuildCourseGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildCourseGridActionsModal()
        {
            this._CourseGridConfirmAction = new ModalPopup("GridConfirmActionModal");

            // set modal properties
            this._CourseGridConfirmAction.Type = ModalPopupType.Confirm;
            this._CourseGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._CourseGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._CourseGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedEnrollment_s;
            this._CourseGridConfirmAction.TargetControlID = this._CourseDeleteButton.ClientID;
            this._CourseGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._CourseDeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseEnrollment_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._CourseGridConfirmAction.AddControlToBody(body1Wrapper);
        }
        #endregion

        #region _BuildCourseResetConfirmModal
        /// <summary>
        /// Builds the confirmation modal for the reset action performed on Grid data.
        /// </summary>
        private void _BuildCourseResetConfirmModal()
        {
            this._HiddenResetButton = new HiddenField();
            this._HiddenResetButton.ID = "HiddenResetButton";
            this._CourseGridActionsPanel.Controls.Add(this._HiddenResetButton);

            this._ResetEnrollmentId = new HiddenField();
            this._ResetEnrollmentId.ID = "ResetEnrollmentId";
            this._ResetEnrollmentId.Value = "0";
            this._CourseGridActionsPanel.Controls.Add(this._ResetEnrollmentId);

            this._CourseResetConfirmAction = new ModalPopup("ResetConfirmActionModal");

            // set modal properties
            this._CourseResetConfirmAction.Type = ModalPopupType.Confirm;
            this._CourseResetConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_RESET, ImageFiles.EXT_PNG);
            this._CourseResetConfirmAction.HeaderIconAlt = _GlobalResources.Reset;
            this._CourseResetConfirmAction.HeaderText = _GlobalResources.ResetEnrollment;
            this._CourseResetConfirmAction.TargetControlID = this._HiddenResetButton.ClientID;
            this._CourseResetConfirmAction.SubmitButton.Command += new CommandEventHandler(this._ResetButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmResetActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToResetThisEnrollment;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._CourseResetConfirmAction.AddControlToBody(body1Wrapper);
        }
        #endregion

        #region _BuildTogglePrerequisiteLockConfirmModal
        /// <summary>
        /// Builds the confirmation modal for the toggle prerequisite lock action performed on Grid data.
        /// </summary>
        private void _BuildTogglePrerequisiteLockConfirmModal()
        {
            this._HiddenTogglePrerequisiteLockButton = new HiddenField();
            this._HiddenTogglePrerequisiteLockButton.ID = "HiddenTogglePrerequisiteLockButton";
            this._CourseGridActionsPanel.Controls.Add(this._HiddenTogglePrerequisiteLockButton);

            this._TogglePrerequisiteEnrollmentId = new HiddenField();
            this._TogglePrerequisiteEnrollmentId.ID = "TogglePrerequisiteEnrollmentId";
            this._TogglePrerequisiteEnrollmentId.Value = "0";
            this._CourseGridActionsPanel.Controls.Add(this._TogglePrerequisiteEnrollmentId);

            this._TogglePrerequisiteLockConfirmAction = new ModalPopup("TogglePrerequisiteLockConfirmActionModal");

            // set modal properties
            this._TogglePrerequisiteLockConfirmAction.Type = ModalPopupType.Confirm;
            this._TogglePrerequisiteLockConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED, ImageFiles.EXT_PNG);
            this._TogglePrerequisiteLockConfirmAction.HeaderIconAlt = _GlobalResources.PrerequisiteLock;
            this._TogglePrerequisiteLockConfirmAction.HeaderText = _GlobalResources.UnlockEnrollment;
            this._TogglePrerequisiteLockConfirmAction.TargetControlID = this._HiddenTogglePrerequisiteLockButton.ClientID;
            this._TogglePrerequisiteLockConfirmAction.SubmitButton.Command += new CommandEventHandler(this._TogglePrerequisiteLockButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmTogglePrerequisitesActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToUnlockThisEnrollment;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._TogglePrerequisiteLockConfirmAction.AddControlToBody(body1Wrapper);

            this._CourseGridActionsPanel.Controls.Add(this._TogglePrerequisiteLockConfirmAction);
        }
        #endregion

        #region _ResetButton_Command
        /// <summary>
        /// Resets a course enrollment.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ResetButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int idEnrollment;

                if (int.TryParse(this._ResetEnrollmentId.Value, out idEnrollment))
                {
                    if (idEnrollment > 0)
                    {
                        // remove the sco data log files for all lesson data within this enrollment
                        DataTable lessonDataIds = Enrollment.GetLessonDataIds(idEnrollment);

                        foreach (DataRow dataRow in lessonDataIds.Rows)
                        {
                            int idUser = Convert.ToInt32(dataRow["idUser"]);
                            int idDataLesson = Convert.ToInt32(dataRow["idData-Lesson"]);
                            DataLesson.DeleteSCOLogDataFiles(idUser, idDataLesson);
                        }

                        // reset the enrollment
                        Enrollment.Reset(idEnrollment);

                        // display the success message
                        this.DisplayFeedback(_GlobalResources.TheSelectedEnrollmentHasBeenResetSuccessfully, false);
                    }
                    else
                    { throw new AsentiaException(_GlobalResources.InvalidEnrollmentID); }
                }
                else
                { throw new AsentiaException(_GlobalResources.InvalidEnrollmentID); }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this._CourseEnrollmentsGrid.BindData();
            }
        }
        #endregion

        #region _TogglePrerequisiteLockButton_Command
        /// <summary>
        /// Toggles the prerequisite lock for a course enrollment.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _TogglePrerequisiteLockButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int idEnrollment;

                if (int.TryParse(this._TogglePrerequisiteEnrollmentId.Value, out idEnrollment))
                {
                    if (idEnrollment > 0)
                    {
                        // reset the enrollment
                        Library.Enrollment.TogglePrerequisiteLock(idEnrollment);

                        // display the success message
                        this.DisplayFeedback(_GlobalResources.TheSelectedEnrollmentHasBeenUnlockedSuccessfully, false);                        
                    }
                    else
                    { throw new AsentiaException(_GlobalResources.InvalidEnrollmentID); }
                }
                else
                { throw new AsentiaException(_GlobalResources.InvalidEnrollmentID); }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this._CourseEnrollmentsGrid.BindData();
            }
        }
        #endregion

        #region _CourseDeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _CourseDeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._CourseEnrollmentsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._CourseEnrollmentsGrid.Rows[i].FindControl(this._CourseEnrollmentsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.Enrollment.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedEnrollment_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoCourseEnrollment_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);                
            }
            finally
            {
                // rebind the grid
                this._CourseEnrollmentsGrid.BindData();
            }
        }
        #endregion
        #endregion

        #region LearningPath
        #region _BuildLearningPathEnrollmentsGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildLearningPathEnrollmentsGrid()
        {
            this._LearningPathEnrollmentsGrid = new Grid();
            this._LearningPathEnrollmentsGrid.ID = "LearningPathEnrollmentsGrid";
            this._LearningPathEnrollmentsGrid.StoredProcedure = Library.LearningPathEnrollment.GridProcedure;
            this._LearningPathEnrollmentsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._LearningPathEnrollmentsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._LearningPathEnrollmentsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._LearningPathEnrollmentsGrid.AddFilter("@idUser", SqlDbType.Int, 4, this._UserObject.Id);
            this._LearningPathEnrollmentsGrid.IdentifierField = "idLearningPathEnrollment";
            this._LearningPathEnrollmentsGrid.DefaultSortColumn = "title";

            // data key names
            this._LearningPathEnrollmentsGrid.DataKeyNames = new string[] { "idLearningPathEnrollment" };

            // columns
            GridColumn title = new GridColumn(_GlobalResources.LearningPath, null, "title"); // this is calculated dynamically in the RowDataBound method

            GridColumn dtStart = new GridColumn(_GlobalResources.Start, "dtStart", "dtStart");

            GridColumn dtDue = new GridColumn(_GlobalResources.Due, "dtDue", "dtDue");

            GridColumn dtExpires = new GridColumn(_GlobalResources.Expires, "dtExpires", "dtExpires");

            GridColumn enrollmentStatus = new GridColumn(_GlobalResources.Status, "status");
            enrollmentStatus.AddProperty(new GridColumnProperty(Convert.ToInt32(EnrollmentStatus.Enrolled).ToString(), _GlobalResources.Enrolled));
            enrollmentStatus.AddProperty(new GridColumnProperty(Convert.ToInt32(EnrollmentStatus.Completed).ToString(), _GlobalResources.Completed));
            enrollmentStatus.AddProperty(new GridColumnProperty(Convert.ToInt32(EnrollmentStatus.Overdue).ToString(), _GlobalResources.Overdue));
            enrollmentStatus.AddProperty(new GridColumnProperty(Convert.ToInt32(EnrollmentStatus.Expired).ToString(), _GlobalResources.Expired));
            enrollmentStatus.AddProperty(new GridColumnProperty(Convert.ToInt32(EnrollmentStatus.Future).ToString(), _GlobalResources.Future));

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method            

            // add columns to data grid
            this._LearningPathEnrollmentsGrid.AddColumn(title);
            this._LearningPathEnrollmentsGrid.AddColumn(dtStart);
            this._LearningPathEnrollmentsGrid.AddColumn(dtDue);
            this._LearningPathEnrollmentsGrid.AddColumn(dtExpires);
            this._LearningPathEnrollmentsGrid.AddColumn(enrollmentStatus);
            this._LearningPathEnrollmentsGrid.AddColumn(options);

            // add row data bound event
            this._LearningPathEnrollmentsGrid.RowDataBound += new GridViewRowEventHandler(this._LearningPathEnrollmentsGrid_RowDataBound);
        }
        #endregion

        #region _LearningPathEnrollmentsGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the learning path enrollments grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LearningPathEnrollmentsGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;

                // disable the check box if the enrollment is completed because completed enrollments cannot be deleted
                int enrollmentStatus = Convert.ToInt32(rowView["status"]);
                if (enrollmentStatus == Convert.ToInt32(EnrollmentStatus.Completed))
                {
                    CheckBox enrollmentSelectCheckBox = (CheckBox)e.Row.Cells[0].FindControl("LearningPathEnrollmentsGrid_GridSelectRecord_" + e.Row.RowIndex.ToString());
                    enrollmentSelectCheckBox.Enabled = false;
                }

                int idLearningPathEnrollment = Convert.ToInt32(rowView["idLearningPathEnrollment"]);

                // AVATAR, LEARNING PATH

                string learningPathName = rowView["title"].ToString();                
                bool isActivityOn = Convert.ToBoolean(rowView["isActivityOn"]);
                bool isModifyOn = Convert.ToBoolean(rowView["isModifyOn"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = learningPathName;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // label
                Label labelLabelWrapper = new Label();
                labelLabelWrapper.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(labelLabelWrapper);

                Literal labelLabel = new Literal();
                labelLabel.Text = learningPathName;
                labelLabelWrapper.Controls.Add(labelLabel);

                // OPTIONS COLUMN

                // activity
                if (isActivityOn)
                {
                    Label activitySpan = new Label();
                    activitySpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(activitySpan);

                    HyperLink activityLink = new HyperLink();
                    activityLink.NavigateUrl = "LearningPathActivity.aspx?uid=" + this._UserObject.Id.ToString() + "&id=" + idLearningPathEnrollment.ToString();
                    activitySpan.Controls.Add(activityLink);

                    Image activityIcon = new Image();
                    activityIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ACTIVITY, ImageFiles.EXT_PNG);
                    activityIcon.AlternateText = _GlobalResources.Activity;
                    activityIcon.ToolTip = _GlobalResources.Activity;
                    activityLink.Controls.Add(activityIcon);
                }
                else
                {
                    Label activitySpan = new Label();
                    activitySpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(activitySpan);

                    Image activityIcon = new Image();
                    activityIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ACTIVITY, ImageFiles.EXT_PNG);
                    activityIcon.CssClass = "DimIcon";
                    activityIcon.AlternateText = _GlobalResources.ActivityDisabled;
                    activityIcon.ToolTip = _GlobalResources.ActivityDisabled;
                    activitySpan.Controls.Add(activityIcon);
                }

                // modify
                if (isModifyOn)
                {
                    Label modifySpan = new Label();
                    modifySpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(modifySpan);

                    HyperLink modifyLink = new HyperLink();
                    modifyLink.NavigateUrl = "ModifyLearningPath.aspx?uid=" + this._UserObject.Id.ToString() + "&id=" + idLearningPathEnrollment.ToString();
                    modifySpan.Controls.Add(modifyLink);

                    Image modifyIcon = new Image();
                    modifyIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
                    modifyIcon.AlternateText = _GlobalResources.Modify;
                    modifyIcon.ToolTip = _GlobalResources.Modify;
                    modifyLink.Controls.Add(modifyIcon);
                }
                else
                {
                    Label modifySpan = new Label();
                    modifySpan.CssClass = "GridImageLink";
                    e.Row.Cells[6].Controls.Add(modifySpan);

                    Image modifyIcon = new Image();
                    modifyIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
                    modifyIcon.CssClass = "DimIcon";
                    modifyIcon.AlternateText = _GlobalResources.ModifyDisabled;
                    modifyIcon.ToolTip = _GlobalResources.ModifyDisabled;
                    modifySpan.Controls.Add(modifyIcon);
                }
            }
        }
        #endregion

        #region _BuildLearningPathGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildLearningPathGridActionsPanel()
        {
            this._LearningPathGridActionsPanel = new Panel();
            this._LearningPathGridActionsPanel.ID = "LearningPathGridActionsPanel";
            this._LearningPathGridActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._LearningPathDeleteButton = new LinkButton();
            this._LearningPathDeleteButton.ID = "LearningPathDeleteButton";
            this._LearningPathDeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "LearningPathGridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._LearningPathDeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedEnrollment_s;
            this._LearningPathDeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this._LearningPathGridActionsPanel.Controls.Add(this._LearningPathDeleteButton);
        }
        #endregion

        #region _BuildLearningPathGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildLearningPathGridActionsModal()
        {
            this._LearningPathGridConfirmAction = new ModalPopup("LearningPathGridConfirmAction");

            // set modal properties
            this._LearningPathGridConfirmAction.Type = ModalPopupType.Confirm;
            this._LearningPathGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._LearningPathGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._LearningPathGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedEnrollment_s;
            this._LearningPathGridConfirmAction.TargetControlID = this._LearningPathDeleteButton.ClientID;
            this._LearningPathGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._LearningPathDeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseEnrollment_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._LearningPathGridConfirmAction.AddControlToBody(body1Wrapper);
        }
        #endregion

        #region _LearningPathDeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _LearningPathDeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._LearningPathEnrollmentsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._LearningPathEnrollmentsGrid.Rows[i].FindControl(this._LearningPathEnrollmentsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.LearningPathEnrollment.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedEnrollment_sHaveBeenDeletedSuccessfully, false);                    
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoLearningPathEnrollment_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this._CourseEnrollmentsGrid.BindData();
                this._LearningPathEnrollmentsGrid.BindData();
            }
        }
        #endregion
        #endregion        
    }
}