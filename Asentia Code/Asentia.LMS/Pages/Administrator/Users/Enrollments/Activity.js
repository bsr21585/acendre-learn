﻿function CompletionStatusDropDownChange() {
    var statusSelector = $("#EnrollmentCompletionStatus_Field");
    var datePicker = $("#EnrollmentCompletionDate_Field");
    var dateInput = $("#EnrollmentCompletionDate_Field_DateInputControl");
    var hourInput = $("#EnrollmentCompletionDate_Field_HourInputControl");
    var minuteInput = $("#EnrollmentCompletionDate_Field_MinuteInputControl");
    var ampmInput = $("#EnrollmentCompletionDate_Field_AMPMInputControl");

    if (statusSelector.val() == "2") {
        datePicker.prop("disabled", false);
        dateInput.prop("disabled", false);
        hourInput.prop("disabled", false);
        minuteInput.prop("disabled", false);
        ampmInput.prop("disabled", false);
    }
    else {
        dateInput.prop("disabled", true);
        hourInput.prop("disabled", true);
        minuteInput.prop("disabled", true);
        ampmInput.prop("disabled", true);

        dateInput.val("");
        hourInput.val("12");
        minuteInput.val("00");
        ampmInput.val("AM");
    }
}

function LoadTaskProctoringModalContent(idUser, idEnrollment, idDataLesson, idDataTask, idLesson, uploadedTaskFilename, taskUploadedDate) {
    $("#TaskData").val(idUser + "|" + idEnrollment + "|" + idDataLesson + "|" + idDataTask + "|" + idLesson + "|" + uploadedTaskFilename + "|" + taskUploadedDate);

    $("#TaskProctoringModalLaunchButton").click();
    $("#TaskProctoringModalLoadButton").click();
}

function LoadAdministratorOverrideModalContent(idUser, idEnrollment, idDataLesson, idLesson) {
    $("#AdministratorOverrideData").val(idUser + "|" + idEnrollment + "|" + idDataLesson + "|" + idLesson);

    $("#AdministratorOverrideModalLaunchButton").click();
    $("#AdministratorOverrideModalLoadButton").click();
}

function LoadResetLessonDataConfirmationModalContent(idUser, idEnrollment, idDataLesson) {
    $("#LessonResetData").val(idUser + "|" + idEnrollment + "|" + idDataLesson);

    $("#ResetLessonDataConfirmationModalLaunchButton").click();
    $("#ResetLessonDataConfirmationModalLoadButton").click();
}