﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Users
{
    public class Dashboard : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel UserDashboardFormContentWrapperContainer;
        public Panel UserObjectMenuContainer;
        public Panel UserDashboardWrapperContainer;
        public Panel UserDashboardInstructionsPanel;
        public Panel UserDashboardFeedbackContainer;
        #endregion

        #region Private Properties
        private User _UserObject;
        private ObjectDashboard _UserObjectDashboard;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            //csm.RegisterClientScriptResource(typeof(Dashboard), "Asentia.LMS.Pages.Administrator.Users.Dashboard.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the user object
            this._GetUserObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("ObjectDashboard.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/users/Dashboard.css");            

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetUserObject
        /// <summary>
        /// Gets a user object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetUserObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._UserObject = new User(id); }
                }
                catch
                { Response.Redirect("~/administrator/users"); }
            }
            else { Response.Redirect("~/administrator/users"); }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // set container classes
            this.UserDashboardFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.UserDashboardWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu
            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.UserDashboard;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }            

            // build the user object dashboard
            this._UserObjectDashboard = new ObjectDashboard("UserObjectDashboard");

            this._BuildInformationWidget();
            this._BuildActionsWidget();
            this._BuildCourseEnrollmentsWidget();
            this._BuildLearningPathEnrollmentsWidget();
            this._BuildModuleStatsWidget();
            this._BuildRecentActivityWidget();

            // attach object widget to wrapper container
            this.UserDashboardWrapperContainer.Controls.Add(this._UserObjectDashboard);
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information            
            string userImagePath;
            string imageCssClass = null;
            string pageTitle;

            if (this._UserObject.Avatar != null)
            {
                userImagePath = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id + "/" + this._UserObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                imageCssClass = "AvatarImage";
            }
            else
            {
                if (this._UserObject.Gender == "f")
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                else
                { userImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
            }

            pageTitle = this._UserObject.DisplayName;            

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Users, "/administrator/users"));
            breadCrumbLinks.Add(new BreadcrumbLink(pageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, pageTitle, userImagePath, imageCssClass);
        }
        #endregion

        #region _BuildInformationWidget
        /// <summary>
        /// Builds the information widget.
        /// </summary>
        private void _BuildInformationWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> informationWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // CREATED DATE
            string formattedCreatedDate = TimeZoneInfo.ConvertTimeFromUtc(this._UserObject.DtCreated, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", ""));
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("CreatedDate", _GlobalResources.Created + ":", formattedCreatedDate, ObjectDashboard.WidgetItemType.Text));

            // EXPIRATION DATE
            string formattedExpirationDate = (this._UserObject.DtExpires == null) ? _GlobalResources.Never : TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._UserObject.DtExpires, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", ""));
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("ExpirationDate", _GlobalResources.Expires + ":", formattedExpirationDate, ObjectDashboard.WidgetItemType.Text));

            // STATUS
            string formattedStatus = (this._UserObject.IsActive) ? _GlobalResources.Active : _GlobalResources.Disabled;
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("Status", _GlobalResources.Status + ":", formattedStatus, ObjectDashboard.WidgetItemType.Text));
            
            // SUPERVISOR(S)
            DataTable supervisorsDt = this._UserObject.GetSupervisors(null);

            string formattedSupervisorList = String.Empty;

            foreach (DataRow row in supervisorsDt.Rows)
            { formattedSupervisorList += "<div>" + row["displayName"].ToString() + "</div>"; }

            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("Supervisors", _GlobalResources.Supervisors + ":", formattedSupervisorList, ObjectDashboard.WidgetItemType.Text));

            // add the widget
            this._UserObjectDashboard.AddWidget("InformationWidget", _GlobalResources.Information, informationWidgetItems);
        }
        #endregion

        #region _BuildActionsWidget
        /// <summary>
        /// Builds the actions widget.
        /// </summary>
        private void _BuildActionsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> actionsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // NEW COURSE ENROLLMENT LINK

            Panel createCourseEnrollmentLinkWrapper = new Panel();

            HyperLink createCourseEnrollmentLink = new HyperLink();
            createCourseEnrollmentLink.NavigateUrl = "enrollments/Modify.aspx?uid=" + this._UserObject.Id.ToString();
            createCourseEnrollmentLinkWrapper.Controls.Add(createCourseEnrollmentLink);

            Image createCourseEnrollmentLinkImage = new Image();
            createCourseEnrollmentLinkImage.CssClass = "SmallIcon";
            createCourseEnrollmentLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
            createCourseEnrollmentLink.Controls.Add(createCourseEnrollmentLinkImage);

            Image createCourseEnrollmentOverlayLinkImage = new Image();
            createCourseEnrollmentOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
            createCourseEnrollmentOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            createCourseEnrollmentLink.Controls.Add(createCourseEnrollmentOverlayLinkImage);

            Literal createCourseEnrollmentLinkText = new Literal();
            createCourseEnrollmentLinkText.Text = _GlobalResources.CreateANewCourseEnrollment;
            createCourseEnrollmentLink.Controls.Add(createCourseEnrollmentLinkText);

            actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateCourseEnrollment", null, createCourseEnrollmentLinkWrapper, ObjectDashboard.WidgetItemType.Link));

            // NEW LEARNING PATH ENROLLMENT LINK

            Panel createLearningPathEnrollmentLinkWrapper = new Panel();

            HyperLink createLearningPathEnrollmentLink = new HyperLink();
            createLearningPathEnrollmentLink.NavigateUrl = "enrollments/ModifyLearningPath.aspx?uid=" + this._UserObject.Id.ToString();
            createLearningPathEnrollmentLinkWrapper.Controls.Add(createLearningPathEnrollmentLink);

            Image createLearningPathEnrollmentLinkImage = new Image();
            createLearningPathEnrollmentLinkImage.CssClass = "SmallIcon";
            createLearningPathEnrollmentLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
            createLearningPathEnrollmentLink.Controls.Add(createLearningPathEnrollmentLinkImage);

            Image createLearningPathEnrollmentOverlayLinkImage = new Image();
            createLearningPathEnrollmentOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
            createLearningPathEnrollmentOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            createLearningPathEnrollmentLink.Controls.Add(createLearningPathEnrollmentOverlayLinkImage);

            Literal createLearningPathEnrollmentLinkText = new Literal();
            createLearningPathEnrollmentLinkText.Text = _GlobalResources.CreateANewLearningPathEnrollment;
            createLearningPathEnrollmentLink.Controls.Add(createLearningPathEnrollmentLinkText);

            actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateLearningPathEnrollment", null, createLearningPathEnrollmentLinkWrapper, ObjectDashboard.WidgetItemType.Link));

            // add the widget
            this._UserObjectDashboard.AddWidget("ActionsWidget", _GlobalResources.Actions, actionsWidgetItems);
        }
        #endregion        

        #region _BuildCourseEnrollmentsWidget
        /// <summary>
        /// Builds the course enrollments widget.
        /// </summary>
        private void _BuildCourseEnrollmentsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> courseEnrollmentsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the data and build a chart
            DataTable courseEnrollmentsStatusDt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            Chart courseEnrollmentsPieChart;

            // chart colors
            List<string> courseEnrollmentsPieChartColors = new List<string>();
            courseEnrollmentsPieChartColors.Add("#2E7DBD");
            courseEnrollmentsPieChartColors.Add("#55BD86");
            courseEnrollmentsPieChartColors.Add("#FED155");
            courseEnrollmentsPieChartColors.Add("#EC6E61");

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idUser", this._UserObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.CourseEnrollmentStatistics]", true);

                // load recordset
                courseEnrollmentsStatusDt.Load(sdr);

                sdr.Close();

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // draw the chart
                courseEnrollmentsPieChart = new Chart("CourseEnrollmentsPieChart", ChartType.Doughnut, courseEnrollmentsStatusDt, courseEnrollmentsPieChartColors, _GlobalResources.CourseEnrollments, null);
                courseEnrollmentsPieChart.ShowTotalInTitle = true;
                courseEnrollmentsPieChart.ShowTotalAndPercentageInLegend = true;
                courseEnrollmentsPieChart.IsResponsive = false;
                courseEnrollmentsPieChart.CanvasWidth = 125;
                courseEnrollmentsPieChart.CanvasHeight = 125;
            }
            catch // on errors, just draw an empty chart
            {
                // draw an empty chart
                DataTable emptyDataTable = new DataTable();
                emptyDataTable.Columns.Add("_Total_", typeof(int));
                emptyDataTable.Rows.Add(0);

                courseEnrollmentsPieChart = new Chart("CourseEnrollmentsPieChart", ChartType.Doughnut, emptyDataTable, courseEnrollmentsPieChartColors, _GlobalResources.CourseEnrollments, null);
                courseEnrollmentsPieChart.ShowTotalInTitle = true;
                courseEnrollmentsPieChart.ShowTotalAndPercentageInLegend = false;
                courseEnrollmentsPieChart.IsResponsive = false;
                courseEnrollmentsPieChart.CanvasWidth = 125;
                courseEnrollmentsPieChart.CanvasHeight = 125;
            }
            finally
            { databaseObject.Dispose(); }

            // add the chart to the widget item list
            courseEnrollmentsWidgetItems.Add(new ObjectDashboard.WidgetItem("CourseEnrollmentStats", null, courseEnrollmentsPieChart, ObjectDashboard.WidgetItemType.Object));

            // add the widget
            this._UserObjectDashboard.AddWidget("CourseEnrollmentsWidget", _GlobalResources.CourseEnrollments, courseEnrollmentsWidgetItems);
        }
        #endregion

        #region _BuildLearningPathEnrollmentsWidget
        /// <summary>
        /// Builds the learning path enrollments widget.
        /// </summary>
        private void _BuildLearningPathEnrollmentsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> learningPathEnrollmentsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the data and build a chart
            DataTable learningPathEnrollmentsStatusDt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            Chart learningPathEnrollmentsPieChart;

            // chart colors
            List<string> learningPathEnrollmentsPieChartColors = new List<string>();
            learningPathEnrollmentsPieChartColors.Add("#A6729B");
            learningPathEnrollmentsPieChartColors.Add("#45848C");
            learningPathEnrollmentsPieChartColors.Add("#A69B72");
            learningPathEnrollmentsPieChartColors.Add("#F17B85");

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idUser", this._UserObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.LearningPathEnrollmentStatistics]", true);

                // load recordset
                learningPathEnrollmentsStatusDt.Load(sdr);

                sdr.Close();

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // draw the chart
                learningPathEnrollmentsPieChart = new Chart("LearningPathEnrollmentsPieChart", ChartType.Doughnut, learningPathEnrollmentsStatusDt, learningPathEnrollmentsPieChartColors, _GlobalResources.LearningPathEnrollments, null);
                learningPathEnrollmentsPieChart.ShowTotalInTitle = true;
                learningPathEnrollmentsPieChart.ShowTotalAndPercentageInLegend = true;
                learningPathEnrollmentsPieChart.IsResponsive = false;
                learningPathEnrollmentsPieChart.CanvasWidth = 125;
                learningPathEnrollmentsPieChart.CanvasHeight = 125;
            }
            catch // on errors, just draw an empty chart
            {
                // draw an empty chart
                DataTable emptyDataTable = new DataTable();
                emptyDataTable.Columns.Add("_Total_", typeof(int));
                emptyDataTable.Rows.Add(0);

                learningPathEnrollmentsPieChart = new Chart("LearningPathEnrollmentsPieChart", ChartType.Doughnut, emptyDataTable, learningPathEnrollmentsPieChartColors, _GlobalResources.LearningPathEnrollments, null);
                learningPathEnrollmentsPieChart.ShowTotalInTitle = true;
                learningPathEnrollmentsPieChart.ShowTotalAndPercentageInLegend = false;
                learningPathEnrollmentsPieChart.IsResponsive = false;
                learningPathEnrollmentsPieChart.CanvasWidth = 125;
                learningPathEnrollmentsPieChart.CanvasHeight = 125;
            }
            finally
            { databaseObject.Dispose(); }

            // add the chart to the widget item list
            learningPathEnrollmentsWidgetItems.Add(new ObjectDashboard.WidgetItem("LearningPathEnrollmentStats", null, learningPathEnrollmentsPieChart, ObjectDashboard.WidgetItemType.Object));

            // add the widget
            this._UserObjectDashboard.AddWidget("LearningPathEnrollmentsWidget", _GlobalResources.LearningPathEnrollments, learningPathEnrollmentsWidgetItems);
        }
        #endregion

        #region _BuildModuleStatsWidget
        /// <summary>
        /// Builds the module stats widget.
        /// </summary>
        private void _BuildModuleStatsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> moduleStatsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the data and add it to the widget items            
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            string modulesPassed = "-";
            string modulesFailed = "-";
            string averageScore = "-";
            string totalTrainingTime = "-";
            string averageTimePerLesson = "-";

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idUser", this._UserObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@modulesPassed", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@modulesFailed", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@averageScore", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@totalTrainingTime", null, SqlDbType.NVarChar, 8, ParameterDirection.Output);
                databaseObject.AddParameter("@averageTimePerLesson", null, SqlDbType.NVarChar, 8, ParameterDirection.Output);

                databaseObject.ExecuteNonQuery("[User.LessonStatistics]", true);

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // format the data strings
                modulesPassed = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@modulesPassed"].Value).ToString();
                
                modulesFailed = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@modulesFailed"].Value).ToString();

                if (AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@averageScore"].Value) != null)
                { averageScore = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@averageScore"].Value).ToString() + "%"; }

                if (!String.IsNullOrWhiteSpace(AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@totalTrainingTime"].Value)))
                { totalTrainingTime = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@totalTrainingTime"].Value); }

                if (!String.IsNullOrWhiteSpace(AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@averageTimePerLesson"].Value)))
                { averageTimePerLesson = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@averageTimePerLesson"].Value); }
            }
            catch // on errors, just bury it
            { }
            finally
            { databaseObject.Dispose(); }

            // add the items to the widget item list
            moduleStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("ModulesPassed", _GlobalResources.ModulesPassed + ":", modulesPassed, ObjectDashboard.WidgetItemType.Text));
            moduleStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("ModulesFailed", _GlobalResources.ModulesFailed + ":", modulesFailed, ObjectDashboard.WidgetItemType.Text));
            moduleStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("AverageScore", _GlobalResources.AverageScore + ":", averageScore, ObjectDashboard.WidgetItemType.Text));
            moduleStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("TotalTrainingTime", _GlobalResources.TotalTrainingTime + ":", totalTrainingTime, ObjectDashboard.WidgetItemType.Text));
            moduleStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("AverageTimePerModule", _GlobalResources.AverageTimeModule + ":", averageTimePerLesson, ObjectDashboard.WidgetItemType.Text));

            // add the widget
            this._UserObjectDashboard.AddWidget("ModuleStatsWidget", _GlobalResources.ModuleStats, moduleStatsWidgetItems);
        }
        #endregion        

        #region _BuildRecentActivityWidget
        /// <summary>
        /// Builds the recent activity widget.
        /// </summary>
        private void _BuildRecentActivityWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> recentActivityWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // LAST LOGIN DATE
            string formattedLastLoginDate = _GlobalResources.Never;

            if (this._UserObject.DtLastLogin != null)
            { formattedLastLoginDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._UserObject.DtLastLogin, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", "")); }

            recentActivityWidgetItems.Add(new ObjectDashboard.WidgetItem("LastLoginDate", _GlobalResources.LastLogin + ":", formattedLastLoginDate, ObjectDashboard.WidgetItemType.Text));            

            // add the widget
            this._UserObjectDashboard.AddWidget("RecentActivityWidget", _GlobalResources.RecentActivity, recentActivityWidgetItems);
        }
        #endregion
    }
}
