﻿//adds owner to resource in owner list container
function AddOwnersToResourceOwnerListContainer() {
    var resourceOwnerListContainer = $("#ResourceOwnerList_Container");
    var selectedUserNameContainer = $("#" + UserNameContainer);

    var selectedUser = $('select#SelectEligibleUsersListBox').val();

    if (selectedUser != null) {
        //remove existing user
        resourceOwnerListContainer.children().remove();

        // add selected user 
        var itemContainerDiv = $("<div id=\"Owner_" + selectedUser + "\">" + $("#SelectEligibleUsersListBox option[value='" + parseInt(selectedUser) + "']").text() + "</div>");
        itemContainerDiv.appendTo(resourceOwnerListContainer);

        // remove the resource from the select list
        $("#SelectEligibleUsersListBox option[value='" + selectedUser + "']").remove();

        // set the hidden field to the value of the selected user
        $("#SelectedResourceOwner_Field").val(selectedUser);
    }
}

//adds parent to resource in parent list container
function AddParentToResourceParentListContainer() {
    var resourceParentListContainer = $("#ResourceParentList_Container");

    var selectedParent = $('select#SelectEligibleParentListBox').val();

    if (selectedParent != null) {

        resourceParentListContainer.children().remove();

        // add selected parent resource
        var itemContainerDiv = $("<div id=\"Parent_" + selectedParent + "\">" + "<img onclick=\"javascript:RemoveParentFromResource('" + selectedParent + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" class=\"SmallIcon\" />" + $("#SelectEligibleParentListBox option[value='" + parseInt(selectedParent) + "']").text() + "</div>");
        itemContainerDiv.appendTo(resourceParentListContainer);

        // remove the resource from the select list
        $("#SelectEligibleParentListBox option[value='" + selectedParent + "']").remove();

        // set the hidden field to the value of the selected parent resource
        $("#SelectedResourceParent_Field").val(selectedParent);
    }
}

//removes parents from resource
function RemoveParentFromResource(ParentId) {
    $("#Parent_" + ParentId).remove();
}

//toggle owner type 
function ToggleOwnerTypePanel() {
    var toggle = $("input[id*='ResourceOwnerType_Field']:checked").val();
    $("div[id^='ResourceProperties_Owner_']").hide();
    $("div[id^='ResourceProperties_Owner_" + toggle + "']").show();

    if (toggle == "user") {
        $("#LaunchSelectOwner_Container").show();
    }
    else { $("#LaunchSelectOwner_Container").hide(); }

}

//gets selected owner
function GetSelectedOwnerForHiddenField() {
    var toggle = $("input[id*='ResourceOwnerType_Field']:checked").val();
    var resourceOwnersListContainer = $("#ResourceOwnerList_Container");
    var selectedResourceOwnerField = $("#SelectedResourceOwner_Field");

    if (toggle == "user") {
        if ($("#ResourceOwnerList_Container").children().length > 0) {
            var selectedResourceOwner = resourceOwnersListContainer.children().prop("id").replace("Owner_", "");

            selectedResourceOwnerField.val(selectedResourceOwner);
        }
        else {
            selectedResourceOwnerField.val(0);
        }
    }
}

//gets selected parent resource
function GetSelectedParentForHiddenField() {
    var resourceParentsListContainer = $("#ResourceParentList_Container");
    var selectedResourceParentsField = $("#SelectedResourceParent_Field");
    if ($("#ResourceParentList_Container").children().length > 0) {
        var selectedResourceParent = resourceParentsListContainer.children().prop("id").replace("Parent_", "");

        selectedResourceParentsField.val(selectedResourceParent);
    }
    else {
        selectedResourceParentsField.val(0);
    }
}

//populates hidden fields
function PopulateHiddenFieldsForDynamicElements() {
    GetSelectedOwnerForHiddenField();
    GetSelectedParentForHiddenField();
}

