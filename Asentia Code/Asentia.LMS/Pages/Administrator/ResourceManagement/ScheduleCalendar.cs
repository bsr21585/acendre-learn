﻿using System;
using System.Collections;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using System.Globalization;
using System.Data;

namespace Asentia.LMS.Pages.Administrator.ResourceManagement
{
    public class ScheduleCalendar : AsentiaAuthenticatedPage
    {
        #region Properties
        #region public Properties
        public Panel ResourceCalendarFormContentWrapperContainer;
        public Panel ResourceCalendarWrapperContainer;
        public Panel UserActionsContainer;
        public Panel ObjectOptionsPanel;
        public Panel OutSideUseScheduleContainer;
        public Panel MaintenanceScheduleContainer;
        public Panel ActionsPanel;
        #endregion

        #region private Properties
        private Resource _ResourceObject;
        private ResourceToObjectLink _ResourceToObjectLinkObject;
        private ModalPopup _OutsideUseModal;
        private Button _OutsideUseModalButton;
        private ModalPopup _MaintenanceModal;
        private Button _MaintenanceModalButton;
        private DatePicker _OutsideUseDtStart;
        private DatePicker _OutsideUseDtEnd;
        private DatePicker _MaintenanceDtStart;
        private DatePicker _MaintenanceDtEnd;
        private Panel _OutsideUseContainer;
        private Panel _MaintenanceContainer;
        private Localize _Month = new Localize();
        private Localize _TimeZoneMessage = new Localize();
        private Localize _CurrMonth = new Localize();

        private LinkButton _ThisMonth = new LinkButton();
        private LinkButton _PrevMonth = new LinkButton();
        private LinkButton _NextMonth = new LinkButton();

        private Image _PreviousMonthLinkIcon = new Image();
        private Image _ThisMonthLinkIcon = new Image();
        private Image _NextMonthLinkIcon = new Image();

        private Panel _CalendarContainer;
        private DataTable _CalendarItems;
        private ModalPopup _ScheduleUpdateModal;
        private Label _ScheduleType;
        private LinkButton _ScheduleUpdateHiddenLink = new LinkButton();
        private HiddenField _IdResourceToObjectHidden = new HiddenField();
        private UpdatePanel _CalendarModalUpdatePanel;
        private Panel _ScheduleUpdateModalContainer;
        private DatePicker _ScheduleUpdateModalDtStart;
        private DatePicker _ScheduleUpdateModalDtEnd;
        private Button _ScheduleUpdateModalButton;
        private ModalPopup _DeleteConfirmAction;
        private LinkButton _ScheduleDeleteHiddenLink = new LinkButton();
        private HiddenField _ScrollPositionHidden = new HiddenField();

        private ModalPopup _ResourceObjectModal;
        private LinkButton _ResourceObjectHiddenLink = new LinkButton();
        private Button _ResourceObjectModalButton;
        private HiddenField _IdObjectHidden = new HiddenField();
        private Literal _ModuleTitle;
        private Literal _SessionTitle;
        private DatePicker _SessionDtStart;
        private DatePicker _SessionDtEnd;

        Library.StandupTrainingInstance _StandupTrainingInstanceObject;
        Library.StandupTraining _StandupTrainingObject;
        #endregion

        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(ScheduleCalendar), "Asentia.LMS.Pages.Administrator.ResourceManagement.ScheduleCalendar.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Calendar.js");

        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_ResourceManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/resourcemanagement/ScheduleCalendar.css");

            // get the group object
            this._GetResourceObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.ResourceCalendarFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ResourceCalendarWrapperContainer.CssClass = "FormContentContainer";

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();

            if (!IsPostBack)
            {
                AsentiaSessionState.DateTimeForCalendar = AsentiaSessionState.UserTimezoneCurrentLocalTime;
            }

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetResourceObject
        /// <summary>
        /// Gets the resource object.
        /// </summary>
        private void _GetResourceObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);

            if (qsId > 0)
            {
                try
                {
                    this._ResourceObject = new Resource(qsId);
                }
                catch
                { Response.Redirect("~/administrator/resourcemanagement"); }
            }
            else
            { Response.Redirect("~/administrator/resourcemanagement"); }
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.Controls.Clear();

            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // SCHEDULE OUTSIDE USE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("ScheduleOutsideUseLink",
                                                null,
                                                "javascript: void(0);",
                                                null,
                                                _GlobalResources.ScheduleOutsideUse,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            // SCHEDULE MAINTENANCE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("ScheduleMaintenanceLink",
                                                null,
                                                "javascript: void(0);",
                                                null,
                                                _GlobalResources.ScheduleMaintenance,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);

            // build the schedule outside use modal popup
            this._BuildOutsideUseModal("ScheduleOutsideUseLink_Link");

            // builds the schedule maintenance modal popup
            this._BuildMaintenanceModal("ScheduleMaintenanceLink_Link");
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the user actions panel
            this._BuildUserActionsPanelDivs();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string courseImagePath;
            string pageTitle;

            if (this._ResourceObject != null)
            {
                breadCrumbPageTitle = this._ResourceObject.Name;

                courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR,
                                                         ImageFiles.EXT_PNG);

                pageTitle = this._ResourceObject.Name;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.ResourceSchedule;

                courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR,
                                                         ImageFiles.EXT_PNG);

                pageTitle = _GlobalResources.ResourceSchedule;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Resources, "/administrator/resourcemanagement"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, pageTitle, courseImagePath);
        }
        #endregion

        #region _BuildOutsideUseModal
        /// <summary>
        /// Builds the build out side use modal for actions performed .
        /// </summary>
        private void _BuildOutsideUseModal(string targetControlId)
        {
            OutSideUseScheduleContainer.Controls.Clear();

            // set modal properties 
            this._OutsideUseModal = new ModalPopup("OutsideUseModal");
            this._OutsideUseModal.Type = ModalPopupType.Form;
            this._OutsideUseModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR,
                                                                          ImageFiles.EXT_PNG);
            this._OutsideUseModal.HeaderIconAlt = _GlobalResources.Close;
            this._OutsideUseModal.HeaderText = _GlobalResources.ScheduleOutsideUse;

            //adding hidden out side use submit button to simulate button click.         
            this._OutsideUseModalButton = new Button();
            this._OutsideUseModalButton.ID = "OutsideUseModalButton";
            this._OutsideUseModalButton.Text = _GlobalResources.Submit;
            this._OutsideUseModalButton.Style.Add("display", "none");
            this.ActionsPanel.Controls.Add(this._OutsideUseModalButton);

            this._OutsideUseModal.TargetControlID = targetControlId;
            this._OutsideUseModal.SubmitButton.Command += new CommandEventHandler(this._OutsideUseModalSubmit_Command);
            this._OutsideUseModal.ReloadPageOnClose = false;

            // build the modal body  
            this._OutsideUseContainer = new Panel();
            this._OutsideUseContainer.ID = "OutsideUseContainer";

            Panel outsideUseStartLabelContainer = new Panel();
            outsideUseStartLabelContainer.ID = "OutsideuseStartLabelContainer";
            outsideUseStartLabelContainer.CssClass = "FormFieldLabelContainer";

            Label outsideUseStart = new Label();
            outsideUseStart.Text = _GlobalResources.Start + ":";

            //required astrick
            Label outsideUseStartRequiredAsterisk = new Label();
            outsideUseStartRequiredAsterisk.Text = "* ";
            outsideUseStartRequiredAsterisk.CssClass = "RequiredAsterisk";

            outsideUseStartLabelContainer.Controls.Add(outsideUseStartRequiredAsterisk);

            outsideUseStartLabelContainer.Controls.Add(outsideUseStart);

            // out side use start date error panel
            Panel outsideUseStartFieldErrorContainer = new Panel();
            outsideUseStartFieldErrorContainer.ID = "OutsideUseStartDate_ErrorContainer";
            outsideUseStartFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            //control
            this._OutsideUseDtStart = new DatePicker("OutsideUseDtStart", false, false, true);
            this._OutsideUseDtStart.ID = "OutsideUseDtStart";

            Label outsideUseEnd = new Label();
            outsideUseEnd.Text = _GlobalResources.End + ":";

            Panel outsideUseEndLabelContainer = new Panel();
            outsideUseEndLabelContainer.ID = "OutSideuseEndLabelContainer";
            outsideUseEndLabelContainer.CssClass = "FormFieldLabelContainer";

            //required astrick
            Label outsideUseEndRequiredAsterisk = new Label();
            outsideUseEndRequiredAsterisk.Text = "* ";
            outsideUseEndRequiredAsterisk.CssClass = "RequiredAsterisk";

            outsideUseEndLabelContainer.Controls.Add(outsideUseEndRequiredAsterisk);

            outsideUseEndLabelContainer.Controls.Add(outsideUseEnd);

            // out side use end date error panel
            Panel outsideUseEndFieldErrorContainer = new Panel();
            outsideUseEndFieldErrorContainer.ID = "OutsideUseEndDate_ErrorContainer";
            outsideUseEndFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            //control
            this._OutsideUseDtEnd = new DatePicker("OutsideUseDtEnd", false, false, true);
            this._OutsideUseDtEnd.ID = "OutsideUseDtEnd";


            this._OutsideUseContainer.Controls.Add(outsideUseStartLabelContainer);
            this._OutsideUseContainer.Controls.Add(outsideUseStartFieldErrorContainer);
            this._OutsideUseContainer.Controls.Add(this._OutsideUseDtStart);

            this._OutsideUseContainer.Controls.Add(outsideUseEndLabelContainer);
            this._OutsideUseContainer.Controls.Add(outsideUseEndFieldErrorContainer);
            this._OutsideUseContainer.Controls.Add(this._OutsideUseDtEnd);

            //add controls to modal
            this._OutsideUseModal.AddControlToBody(this._OutsideUseContainer);

            // add modal to container
            this.OutSideUseScheduleContainer.Controls.Add(this._OutsideUseModal);
        }
        #endregion

        #region _ValidateOutsideUseProperties
        /// <summary>
        ///  Outside use validations
        /// </summary>
        private bool _ValidateOutsideUseProperties()
        {
            bool isValid = true;

            //validation for out side use start date time
            if ((this._OutsideUseDtStart.Value == null || String.IsNullOrWhiteSpace(this._OutsideUseDtStart.Value.ToString())) || this._OutsideUseDtStart.Value < DateTime.Now)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._OutsideUseContainer, "OutsideUseStartDate", _GlobalResources.Start + " " + _GlobalResources.IsRequiredAndMustBeEqualToOrGreaterThanCurrentDate);
            }

            //validation for out side use end date time
            if ((this._OutsideUseDtEnd.Value == null || String.IsNullOrWhiteSpace(this._OutsideUseDtEnd.Value.ToString())) || this._OutsideUseDtEnd.Value <= this._OutsideUseDtStart.Value)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._OutsideUseContainer, "OutsideUseEndDate", _GlobalResources.End + " " + _GlobalResources.IsRequiredAndMustBeGreaterThanStartDate);
            }
            return isValid;
        }
        #endregion

        #region _OutsideUseModalSubmit_Command
        /// <summary>
        ///  Outside use submit command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _OutsideUseModalSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (this._ResourceObject != null)
                {
                    #region Validation

                    // validate the form
                    if (!this._ValidateOutsideUseProperties())
                    { throw new AsentiaException(); }
                    #endregion

                    //creating ResourceToObjectLink class object
                    this._ResourceToObjectLinkObject = new ResourceToObjectLink();

                    //converts the start and end date into utc date and sets values into object
                    this._ResourceToObjectLinkObject.DtStart = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(this._OutsideUseDtStart.Value), TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                    this._ResourceToObjectLinkObject.DtEnd = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(this._OutsideUseDtEnd.Value), TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                    this._ResourceToObjectLinkObject.IdResource = this._ResourceObject.IdResource;
                    this._ResourceToObjectLinkObject.IsOutsideUse = true;
                    this._ResourceToObjectLinkObject.IsMaintenance = false;
                    this._ResourceToObjectLinkObject.IdObject = 0;
                    this._ResourceToObjectLinkObject.ObjectType = null;

                    //save the schedule for outside use
                    this._ResourceToObjectLinkObject.Save();

                    //Reload the calender
                    this._BuildUserActionsPanelDivs();
                    this._CalendarModalUpdatePanel.Update();

                    //display success message
                    this._OutsideUseModal.DisplayFeedback(_GlobalResources.RecordSavedSuccessfully, false);
                }
                else
                {
                    Response.Redirect("~/administrator/resourcemanagement");
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._OutsideUseModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._OutsideUseModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._OutsideUseModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._OutsideUseModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._OutsideUseModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {
                this._ResourceToObjectLinkObject = null;
            }
        }
        #endregion

        #region _BuildMaintenanceModal
        /// <summary>
        /// Builds the maintenance modal for actions performed .
        /// </summary>
        private void _BuildMaintenanceModal(string targetControlId)
        {
            this.MaintenanceScheduleContainer.Controls.Clear();

            // set modal properties 
            this._MaintenanceModal = new ModalPopup("MaintenanceModal");
            this._MaintenanceModal.Type = ModalPopupType.Form;
            this._MaintenanceModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR,
                                                                           ImageFiles.EXT_PNG);
            this._MaintenanceModal.HeaderIconAlt = _GlobalResources.Close;
            this._MaintenanceModal.HeaderText = _GlobalResources.ScheduleMaintenance;

            //adding hidden maintenance submit button to simulate button click.            
            this._MaintenanceModalButton = new Button();
            this._MaintenanceModalButton.ID = "MaintenanceModalButton";
            this._MaintenanceModalButton.Text = _GlobalResources.Submit;
            this._MaintenanceModalButton.Style.Add("display", "none");
            this.ActionsPanel.Controls.Add(this._MaintenanceModalButton);

            this._MaintenanceModal.TargetControlID = targetControlId;
            this._MaintenanceModal.SubmitButton.Command += new CommandEventHandler(this._MaintenanceModalSubmit_Command);
            this._MaintenanceModal.ReloadPageOnClose = false;

            // build the modal body
            this._MaintenanceContainer = new Panel();
            this._MaintenanceContainer.ID = "MaintenanceContainer";

            Panel maintenanceStartLabelContainer = new Panel();
            maintenanceStartLabelContainer.ID = "MaintenanceStartLabelContainer";
            maintenanceStartLabelContainer.CssClass = "FormFieldLabelContainer";

            //maintenance start date label
            Label maintenanceStart = new Label();
            maintenanceStart.Text = _GlobalResources.Start + ":";

            //required astrick
            Label maintenanceStartRequiredAsterisk = new Label();
            maintenanceStartRequiredAsterisk.Text = "* ";
            maintenanceStartRequiredAsterisk.CssClass = "RequiredAsterisk";

            maintenanceStartLabelContainer.Controls.Add(maintenanceStartRequiredAsterisk);

            maintenanceStartLabelContainer.Controls.Add(maintenanceStart);

            // maintenance start date error panel
            Panel maintenanceStartFieldErrorContainer = new Panel();
            maintenanceStartFieldErrorContainer.ID = "MaintenanceStartDate_ErrorContainer";
            maintenanceStartFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            //maintenance start date picker
            this._MaintenanceDtStart = new DatePicker("MaintenanceDtStart", false, false, true);
            this._MaintenanceDtStart.ID = "MaintenanceDtStart";

            Panel maintenanceEndLabelContainer = new Panel();
            maintenanceEndLabelContainer.ID = "MaintenanceEndLabelContainer";
            maintenanceEndLabelContainer.CssClass = "FormFieldLabelContainer";

            //maintenance end date label
            Label maintenanceEnd = new Label();
            maintenanceEnd.Text = _GlobalResources.End + ":";

            //required astrick
            Label maintenanceEndRequiredAsterisk = new Label();
            maintenanceEndRequiredAsterisk.Text = "* ";
            maintenanceEndRequiredAsterisk.CssClass = "RequiredAsterisk";

            maintenanceEndLabelContainer.Controls.Add(maintenanceEndRequiredAsterisk);
            maintenanceEndLabelContainer.Controls.Add(maintenanceEnd);

            // maintenance start date error panel
            Panel maintenanceEndFieldErrorContainer = new Panel();
            maintenanceEndFieldErrorContainer.ID = "MaintenanceEndDate_ErrorContainer";
            maintenanceEndFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            //maintenance end date picker
            this._MaintenanceDtEnd = new DatePicker("MaintenanceDtEnd", false, false, true);
            this._MaintenanceDtEnd.ID = "MaintenanceDtEnd";

            //add controls to panel
            this._MaintenanceContainer.Controls.Add(maintenanceStartLabelContainer);
            this._MaintenanceContainer.Controls.Add(maintenanceStartFieldErrorContainer);
            this._MaintenanceContainer.Controls.Add(this._MaintenanceDtStart);

            this._MaintenanceContainer.Controls.Add(maintenanceEndLabelContainer);
            this._MaintenanceContainer.Controls.Add(maintenanceEndFieldErrorContainer);
            this._MaintenanceContainer.Controls.Add(this._MaintenanceDtEnd);

            //add controls to modal
            this._MaintenanceModal.AddControlToBody(this._MaintenanceContainer);

            // add modal to container
            this.MaintenanceScheduleContainer.Controls.Add(this._MaintenanceModal);
        }
        #endregion

        #region _ValidateMaintenanceProperties
        /// <summary>
        ///  Maintenance validations
        /// </summary>
        private bool _ValidateMaintenanceProperties()
        {
            bool isValid = true;

            //validation for maintenance start date
            if ((this._MaintenanceDtStart.Value == null || String.IsNullOrWhiteSpace(this._MaintenanceDtStart.Value.ToString())) || this._MaintenanceDtStart.Value < DateTime.Now)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._MaintenanceContainer, "MaintenanceStartDate", _GlobalResources.Start + " " + _GlobalResources.IsRequiredAndMustBeEqualToOrGreaterThanCurrentDate);
            }

            //validation for maintenance end date
            if ((this._MaintenanceDtEnd.Value == null || String.IsNullOrWhiteSpace(this._MaintenanceDtEnd.Value.ToString())) || this._MaintenanceDtEnd.Value <= this._MaintenanceDtStart.Value)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._MaintenanceContainer, "MaintenanceEndDate", _GlobalResources.End + " " + _GlobalResources.IsRequiredAndMustBeGreaterThanStartDate);
            }
            return isValid;
        }
        #endregion

        #region _MaintenanceModalSubmit_Command
        /// <summary>
        /// Maintenance submit command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MaintenanceModalSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (this._ResourceObject != null)
                {
                    #region Validation

                    // validate the form
                    if (!this._ValidateMaintenanceProperties())
                    { throw new AsentiaException(); }
                    #endregion

                    //creating resource to object link class object
                    this._ResourceToObjectLinkObject = new ResourceToObjectLink();

                    //converts the start and end date into utc date and sets values into object
                    this._ResourceToObjectLinkObject.DtStart = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(this._MaintenanceDtStart.Value), TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                    this._ResourceToObjectLinkObject.DtEnd = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(this._MaintenanceDtEnd.Value), TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                    this._ResourceToObjectLinkObject.IdResource = this._ResourceObject.IdResource;
                    this._ResourceToObjectLinkObject.IsMaintenance = true;
                    this._ResourceToObjectLinkObject.IsOutsideUse = false;
                    this._ResourceToObjectLinkObject.IdObject = 0;
                    this._ResourceToObjectLinkObject.ObjectType = null;

                    //save the schedule for maintenance
                    this._ResourceToObjectLinkObject.Save();

                    //Reload the calender
                    this._BuildUserActionsPanelDivs();
                    this._CalendarModalUpdatePanel.Update();

                    //display success message
                    this._MaintenanceModal.DisplayFeedback(_GlobalResources.RecordSavedSuccessfully, false);
                }
                else
                {
                    Response.Redirect("~/administrator/resourcemanagement");
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._MaintenanceModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dnfEx)
            {
                // display the failure message
                this._MaintenanceModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._MaintenanceModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._MaintenanceModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._MaintenanceModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {
                this._ResourceToObjectLinkObject = null;
            }
        }
        #endregion

        #region _BuildUserActionsPanelDivs
        /// <summary>
        /// Builds the calendar container.
        /// </summary>
        private void _BuildUserActionsPanelDivs()
        {
            this.UserActionsContainer.Controls.Clear();
            this.ActionsPanel.Controls.Clear();

            //main calendar container
            this._CalendarContainer = new Panel();
            this._CalendarContainer.ID = "CalendarContainer";

            // build the modal body
            this._CalendarModalUpdatePanel = new UpdatePanel();
            this._CalendarModalUpdatePanel.ID = "CalendarModalUpdatePanel";
            this._CalendarModalUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            Panel calendarBodyContent = new Panel();
            calendarBodyContent.ID = "CalendarBodyContent";

            //hidden button to simulate the action on click of update 
            this._ScheduleUpdateHiddenLink.ClientIDMode = ClientIDMode.Static;
            this._ScheduleUpdateHiddenLink.ID = "ScheduleUpdateHiddenLink";
            this._ScheduleUpdateHiddenLink.Text = _GlobalResources.UpdateSchedule;
            this._ScheduleUpdateHiddenLink.Style.Add("display", "none");

            this._IdResourceToObjectHidden.ClientIDMode = ClientIDMode.Static;
            this._IdResourceToObjectHidden.ID = "IdResourceToObjectHidden";

            this.ActionsPanel.Controls.Add(this._ScheduleUpdateHiddenLink);
            this.ActionsPanel.Controls.Add(this._IdResourceToObjectHidden);

            //build the update modal
            this._BuildCalendarIconModal(this._ScheduleUpdateHiddenLink.ID);

            //hidden button to simulate the action on click of calendar standup icon 
            this._ResourceObjectHiddenLink.ClientIDMode = ClientIDMode.Static;
            this._ResourceObjectHiddenLink.ID = "ResourceObjectHiddenLink";
            this._ResourceObjectHiddenLink.Text = _GlobalResources.ClickHere;
            this._ResourceObjectHiddenLink.Style.Add("display", "none");

            this._IdObjectHidden.ClientIDMode = ClientIDMode.Static;
            this._IdObjectHidden.ID = "IdObjectHidden";

            this.ActionsPanel.Controls.Add(this._IdObjectHidden);
            this.ActionsPanel.Controls.Add(this._ResourceObjectHiddenLink);

            //build the resource object modal
            this._BuildResourceObjectModal(this._ResourceObjectHiddenLink.ID);

            //hidden button to simulate the action on click of delete
            this._ScheduleDeleteHiddenLink.ClientIDMode = ClientIDMode.Static;
            this._ScheduleDeleteHiddenLink.ID = "ScheduleDeleteHiddenLink";
            this._ScheduleDeleteHiddenLink.Text = _GlobalResources.DeleteSchedule;
            this._ScheduleDeleteHiddenLink.Style.Add("display", "none");

            this.ActionsPanel.Controls.Add(this._ScheduleDeleteHiddenLink);

            //build the delete modal
            this._BuildDeleteActionsModal(this._ScheduleDeleteHiddenLink.ID);

            // TOP DIV
            Panel calendarBodyContentTop = new Panel();
            calendarBodyContentTop.ID = "CalendarBodyContentTop";

            Panel panelTableRow = new Panel();
            panelTableRow.ID = "CalendarBodyContentTopRow";

            // current month label
            this._CurrMonth.Text = AsentiaSessionState.DateTimeForCalendar.ToString("MMMM")
                                   + " "
                                   + AsentiaSessionState.DateTimeForCalendar.Year;

            Panel panelCalendarCurrentMonthLabel = new Panel();
            panelCalendarCurrentMonthLabel.Controls.Add(this._CurrMonth);
            panelCalendarCurrentMonthLabel.Attributes.Add("class", "CalendarCurrentMonthLabelModal");

            Literal spacer = new Literal();
            spacer.Text = "&nbsp;";
            panelCalendarCurrentMonthLabel.Controls.Add(spacer);

            // this month button
            this._ThisMonth.ID = "CalendarThisMonth";

            this._ThisMonthLinkIcon.CssClass = "SmallIcon ";
            this._ThisMonthLinkIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PROMOTE,
                                                                   ImageFiles.EXT_PNG);
            this._ThisMonthLinkIcon.AlternateText = AsentiaSessionState.DateTimeForCalendar.ToString("MMMM")
                                            + " "
                                            + AsentiaSessionState.DateTimeForCalendar.Year;
            this._ThisMonthLinkIcon.ToolTip = AsentiaSessionState.DateTimeForCalendar.ToString("MMMM")
                                           + " "
                                           + AsentiaSessionState.DateTimeForCalendar.Year;
            this._ThisMonth.Controls.Add(this._ThisMonthLinkIcon);
            this._ThisMonth.OnClientClick = "BeginCalendarPageRequest();";
            this._ThisMonth.Command += new CommandEventHandler(this._ThisMonthButton_Command);
            panelCalendarCurrentMonthLabel.Controls.Add(this._ThisMonth);

            // previous month button
            Panel panelPrevMonth = new Panel();
            panelPrevMonth.CssClass = "CalendarBodyContentTopLeftCorner";
            this._PrevMonth.ID = "CalendarPrevMonth";

            this._PreviousMonthLinkIcon.CssClass = "SmallIcon ";
            this._PreviousMonthLinkIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PREVIOUS,
                                                                   ImageFiles.EXT_PNG);
            this._PreviousMonthLinkIcon.AlternateText = AsentiaSessionState.DateTimeForCalendar.AddMonths(-1).ToString("MMMM")
                                           + " "
                                           + AsentiaSessionState.DateTimeForCalendar.AddMonths(-1).Year;
            this._PreviousMonthLinkIcon.ToolTip = AsentiaSessionState.DateTimeForCalendar.AddMonths(-1).ToString("MMMM")
                                           + " "
                                           + AsentiaSessionState.DateTimeForCalendar.AddMonths(-1).Year;
            this._PrevMonth.Controls.Add(this._PreviousMonthLinkIcon);

            this._PrevMonth.OnClientClick = "BeginCalendarPageRequest();";
            this._PrevMonth.Command += new CommandEventHandler(this._PrevMonthButton_Command);
            panelPrevMonth.Controls.Add(this._PrevMonth);

            // next month button
            Panel panelNextMonth = new Panel();
            panelNextMonth.CssClass = "CalendarBodyContentTopRightCorner";
            this._NextMonth.ID = "CalendarNextMonth";

            this._NextMonthLinkIcon.CssClass = "SmallIcon ";
            this._NextMonthLinkIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_NEXT,
                                                       ImageFiles.EXT_PNG);
            this._NextMonthLinkIcon.AlternateText = AsentiaSessionState.DateTimeForCalendar.AddMonths(1).ToString("MMMM")
                                           + " "
                                           + AsentiaSessionState.DateTimeForCalendar.AddMonths(1).Year;
            this._NextMonthLinkIcon.ToolTip = AsentiaSessionState.DateTimeForCalendar.AddMonths(1).ToString("MMMM")
                                           + " "
                                           + AsentiaSessionState.DateTimeForCalendar.AddMonths(1).Year;
            this._NextMonth.Controls.Add(this._NextMonthLinkIcon);

            this._NextMonth.OnClientClick = "BeginCalendarPageRequest();";
            this._NextMonth.Command += new CommandEventHandler(this._NextMonthButton_Command);
            panelNextMonth.Controls.Add(this._NextMonth);

            // add controls to div
            panelTableRow.Controls.Add(panelPrevMonth);
            panelTableRow.Controls.Add(panelCalendarCurrentMonthLabel);
            panelTableRow.Controls.Add(panelNextMonth);

            calendarBodyContentTop.Controls.Add(panelTableRow);

            // MIDDLE DIV
            Panel calendarBodyContentMiddle = new Panel();
            calendarBodyContentMiddle.ID = "CalendarBodyContentMiddle";

            // the calendar
            this._Month.Text = this._BuildCalendarForMonth(new DateTime(AsentiaSessionState.DateTimeForCalendar.Year, AsentiaSessionState.DateTimeForCalendar.Month, 1));

            // add controls to div
            calendarBodyContentMiddle.Controls.Add(this._Month);

            // BOTTOM DIV
            Panel calendarBodyContentBottom = new Panel();
            calendarBodyContentBottom.ID = "CalendarBodyContentBottom";

            // note indicating time zone
            this._TimeZoneMessage.Text = _GlobalResources.DatesAndTimesForResourceBookingAreInX.ToString().Replace("##TIMEZONE_NAME##", AsentiaSessionState.UserTimezoneDisplayName);

            // add controls to div
            calendarBodyContentBottom.Controls.Add(this._TimeZoneMessage);

            // LOADING DIV - shown when paging between months
            Panel calendarBodyLoading = new Panel();
            calendarBodyLoading.ID = "CalendarPostbackLoadingPlaceholder";
            calendarBodyLoading.Style.Add("display", "none");

            // loading icon
            HtmlGenericControl calendarBodyLoadingImageWrapper = new HtmlGenericControl("p");
            calendarBodyLoadingImageWrapper.ID = "CalendarBodyLoadingImageWrapper";
            calendarBodyLoadingImageWrapper.Attributes.Add("class", "CalendarBodyLoadingModal");

            Image calendarBodyLoadingImage = new Image();
            calendarBodyLoadingImage.ID = "CalendarBodyLoadingImage";
            calendarBodyLoadingImage.AlternateText = _GlobalResources.Loading;
            calendarBodyLoadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING,
                                                                       ImageFiles.EXT_GIF);
            calendarBodyLoadingImage.CssClass = "LargeIcon";

            calendarBodyLoadingImageWrapper.Controls.Add(calendarBodyLoadingImage);
            calendarBodyLoading.Controls.Add(calendarBodyLoadingImageWrapper);

            // loading text
            HtmlGenericControl calendarBodyLoadingTextWrapper = new HtmlGenericControl("p");
            calendarBodyLoadingTextWrapper.ID = "CalendarBodyLoadingTextWrapper";
            calendarBodyLoadingTextWrapper.Attributes.Add("class", "CalendarBodyLoadingModal");

            Localize calendarBodyLoadingText = new Localize();
            calendarBodyLoadingText.Text = _GlobalResources.Loading;

            calendarBodyLoadingTextWrapper.Controls.Add(calendarBodyLoadingText);
            calendarBodyLoading.Controls.Add(calendarBodyLoadingTextWrapper);

            // add controls to body
            calendarBodyContent.Controls.Add(calendarBodyContentTop);
            calendarBodyContent.Controls.Add(calendarBodyContentMiddle);
            calendarBodyContent.Controls.Add(calendarBodyContentBottom);

            this._CalendarModalUpdatePanel.ContentTemplateContainer.Controls.Add(calendarBodyLoading);
            this._CalendarModalUpdatePanel.ContentTemplateContainer.Controls.Add(calendarBodyContent);

            //add to calendar container
            this._CalendarContainer.Controls.Add(this._CalendarModalUpdatePanel);

            this._ScrollPositionHidden.ClientIDMode = ClientIDMode.Static;
            this._ScrollPositionHidden.ID = "ScrollPositionHidden";
            this._CalendarContainer.Controls.Add(this._ScrollPositionHidden);

            //add to main container
            this.UserActionsContainer.Controls.Add(this._CalendarContainer);
        }
        #endregion

        #region _ThisMonthButton_Command
        /// <summary>
        /// Handles the event initiated by the Calendar "This Month" Link Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _ThisMonthButton_Command(object sender, CommandEventArgs e)
        {
            // reset to this month
            DateTime dateForCalendar = AsentiaSessionState.UtcNow;
            AsentiaSessionState.DateTimeForCalendar = dateForCalendar;

            // set "month" properties
            this._Month.Text = this._BuildCalendarForMonth(new DateTime(dateForCalendar.Year,
                                                                        dateForCalendar.Month,
                                                                        1));
            this._PreviousMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._PreviousMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._CurrMonth.Text = dateForCalendar.ToString("MMMM")
                              + " "
                              + dateForCalendar.Year;
            this._NextMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(1).ToString("MMMM")
                                        + " "
                                        + dateForCalendar.AddMonths(1).Year;
            this._NextMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(1).ToString("MMMM")
                              + " "
                              + dateForCalendar.AddMonths(1).Year;

            // register script to handle the end of the calandar page request
            if (!this.Page.ClientScript.IsClientScriptBlockRegistered("CalendarPageRequestPrevMonth"))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                                                 "CalendarPageRequestPrevMonth",
                                                                 "EndCalendarPageRequest();");
            }

        }
        #endregion

        #region _PrevMonthButton_Command
        /// <summary>
        /// Handles the event initiated by the Calendar "Previous Month" Link Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _PrevMonthButton_Command(object sender, CommandEventArgs e)
        {
            // subtract the currently selected date by 1 month and set the session variable to it
            DateTime dateForCalendar = AsentiaSessionState.DateTimeForCalendar.AddMonths(-1);
            AsentiaSessionState.DateTimeForCalendar = dateForCalendar;

            // set "month" properties
            this._Month.Text = this._BuildCalendarForMonth(new DateTime(dateForCalendar.Year,
                                                                        dateForCalendar.Month,
                                                                        1));
            this._PreviousMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                                + " "
                                                + dateForCalendar.AddMonths(-1).Year;
            this._PreviousMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._CurrMonth.Text = dateForCalendar.ToString("MMMM")
                              + " "
                              + dateForCalendar.Year;
            this._NextMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(1).ToString("MMMM")
                                        + " "
                                        + dateForCalendar.AddMonths(1).Year;
            this._NextMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(1).ToString("MMMM")
                              + " "
                              + dateForCalendar.AddMonths(1).Year;

            // register script to handle the end of the calandar page request
            if (!this.Page.ClientScript.IsClientScriptBlockRegistered("CalendarPageRequestPrevMonth"))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                                                 "CalendarPageRequestPrevMonth",
                                                                 "EndCalendarPageRequest();");
            }
        }
        #endregion

        #region _NextMonthButton_Command
        /// <summary>
        /// Handles the event initiated by the Calendar "Next Month" Link Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _NextMonthButton_Command(object sender, CommandEventArgs e)
        {
            // add to the currently selected date by 1 month and set the session variable to it
            DateTime dateForCalendar = AsentiaSessionState.DateTimeForCalendar.AddMonths(1);
            AsentiaSessionState.DateTimeForCalendar = dateForCalendar;

            // set "month" properties
            this._Month.Text = this._BuildCalendarForMonth(new DateTime(dateForCalendar.Year,
                                                                        dateForCalendar.Month,
                                                                        1));
            this._PreviousMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._PreviousMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._CurrMonth.Text = dateForCalendar.ToString("MMMM")
                              + " "
                              + dateForCalendar.Year;
            this._NextMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(1).ToString("MMMM")
                                        + " "
                                        + dateForCalendar.AddMonths(1).Year;
            this._NextMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(1).ToString("MMMM")
                              + " "
                              + dateForCalendar.AddMonths(1).Year;

            // register script to handle the end of the calandar page request
            if (!this.Page.ClientScript.IsClientScriptBlockRegistered("CalendarPageRequestNextMonth"))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                                                 "CalendarPageRequestNextMonth",
                                                                 "EndCalendarPageRequest();");
            }
        }
        #endregion

        #region _BuildCalendarForMonth
        /// <summary>
        /// Builds a calendar table for the month of the given "currentDate."
        /// 
        /// Note: The method we use to build this calendar, "build then return a 
        /// string of HTML that represents the calendar," goes against what we 
        /// normally do with building controls and adding them to the "stack."
        /// We are doing this because we do not want the calendar added to the
        /// "control stack," this makes it easier for us to page between calendar
        /// months.
        /// </summary>
        /// <param name="currentDate">date used to build calendar from</param>
        /// <param name="calendarType">the type of calendar table to render</param>
        private string _BuildCalendarForMonth(DateTime currentDate)
        {
            // get the calendar items for the user
            this._CalendarItems = ResourceToObjectLink.GetItemsForCalendar(this._ResourceObject.IdResource);

            // create a string builder
            StringBuilder sb = new StringBuilder();

            sb.Append("<div id=\"WidgetCalendarTableDiv\">");


            sb.Append("<table id=\"ModalCalendarTable\" class=\"ModalCalendarTable\">");
            sb.Append("<thead>");

            for (int i = 0; i < DateTimeFormatInfo.CurrentInfo.DayNames.Length; i++)
            {
                sb.Append("<th>");
                sb.Append(DateTimeFormatInfo.CurrentInfo.DayNames[i]);
                sb.Append("</th>");
            }

            sb.Append("</thead>");


            // build the body
            sb.Append("<tbody>");

            // set variables to calculate out the calendar
            int currentMonth = currentDate.Month;
            int dayOfWeek = (_GetNumberOfDayInWeek(currentDate.DayOfWeek.ToString())) % 7;
            currentDate = currentDate.AddDays(-(dayOfWeek + (dayOfWeek < 0 ? 7 : 0)));

            int count = 0;

            for (int i = 0; i < 6; i++) // 6 rows in calendar
            {
                sb.Append("<tr>");

                for (int j = 0; j < 7; j++) // 7 columns in calendar
                {
                    TableCell dayColumn = new TableCell();

                    if (currentMonth == currentDate.Month)
                    {
                        // if the date being drawn is today, apply styling
                        if (currentDate.ToString("MM/dd/yyyy") == AsentiaSessionState.UserTimezoneCurrentLocalTime.ToString("MM/dd/yyyy"))
                        {
                            sb.Append("<td class=\"CurrentDayCell\">");
                            sb.Append("<div class=\"DayNumberDiv\">");
                            sb.Append(currentDate.Day.ToString());
                            sb.Append("</div>");
                        }
                        else
                        {
                            sb.Append("<td class=\"DayInMonthCell\">");
                            sb.Append("<div class=\"DayNumberDiv\">");
                            sb.Append(currentDate.Day.ToString());
                            sb.Append("</div>");
                        }

                        // if there are items for the calendar, go through them
                        if (this._CalendarItems.Rows.Count > 0)
                        {
                            foreach (DataRow dataRow in this._CalendarItems.Rows)
                            {

                                int idResourceToObject = Convert.ToInt32(dataRow["idResourceToObject"]);

                                // convert the event datetime to local datetime
                                DateTime startDate = Convert.ToDateTime(dataRow["dtStart"]);
                                DateTime endDate = Convert.ToDateTime(dataRow["dtEnd"]);
                                startDate = TimeZoneInfo.ConvertTimeFromUtc(startDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                                endDate = TimeZoneInfo.ConvertTimeFromUtc(endDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                                string outsideUseOrMaintenance = string.Empty;
                                if (Convert.ToBoolean(dataRow["isOutsideUse"]) || Convert.ToBoolean(dataRow["isMaintenance"]))
                                {
                                    outsideUseOrMaintenance = Convert.ToBoolean(dataRow["isOutsideUse"]) == true ? _GlobalResources.OutsideUse : _GlobalResources.Maintenance;
                                }

                                // if event falls on the current day being rendered, add it
                                if ((startDate.ToString("MM/dd/yyyy") == currentDate.ToString("MM/dd/yyyy") || endDate.ToString("MM/dd/yyyy") == currentDate.ToString("MM/dd/yyyy"))
                                    || (startDate < currentDate && endDate > currentDate))
                                {
                                    sb.Append("<div class=\"EventInfoDiv\">");
                                    sb.Append("<span>");

                                    if (outsideUseOrMaintenance == _GlobalResources.OutsideUse || outsideUseOrMaintenance == _GlobalResources.Maintenance) // JCQA: Why is this a resource and not hard-coded?
                                    {

                                        count++;

                                        sb.Append("<a href=\"javascript:void(0);\" title=\" " + _GlobalResources.BookedFor + " " + outsideUseOrMaintenance + " "
                                              + startDate.ToString() + " - " + endDate.ToString()
                                              + "\">");

                                        sb.Append("<img id=\"imgTrigger" + count + "\" onclick=\"ShowContextMenu(this.id);\" class=\"SmallIcon\" src=\"" + this._GetItemIcon(dataRow["event"].ToString()) + "\" /></a></span>");
                                        sb.Append("<div id=\"imgItemContainer" + count + "\" class=\"AccordionMenuContextMenuContainer ImageItemContainer\">");
                                        sb.Append("<div><a class=\"CursorPointer\" onclick=\"OnScheduleUpdate('" + idResourceToObject.ToString() + "');\" >" + _GlobalResources.Update + "</a></div>");
                                        sb.Append("<div><a onclick=\"OnScheduleDelete('" + idResourceToObject.ToString() + "'); \" class=\"CursorPointer\">" + _GlobalResources.Delete + "</a></div>");
                                        sb.Append("</div>");

                                    }
                                    else if (Convert.ToString(dataRow["objectType"]) == ObjectType.standuptraining.ToString())
                                    {
                                        sb.Append("<a href=\"javascript:void(0);\">");
                                        sb.Append("<img class=\"SmallIcon\" onclick=\"OnResourceObjectIconClick('" + Convert.ToInt32(dataRow["idObject"]) + "','" + idResourceToObject.ToString() + "');\" src=\"" + this._GetItemIcon("standupTraining") + "\" /></a></span>");
                                    }

                                    sb.Append("</div>");
                                }
                            }
                        }
                        // close column
                        sb.Append("</td>");
                    }
                    else // for days NOT in the month being rendered
                    {
                        sb.Append("<td class=\"DayNotInMonthCell\">");
                        sb.Append("<div class=\"DayNumberDiv\">");
                        sb.Append(currentDate.Day.ToString());
                        sb.Append("</td>");
                    }
                    currentDate = currentDate.AddDays(1);
                }

                // close row
                sb.Append("</tr>");
            }

            // close tags
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div>");

            // return HTML
            return sb.ToString();
        }
        #endregion

        #region _BuildDeleteActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed.
        /// </summary>
        private void _BuildDeleteActionsModal(string targetControlId)
        {
            this._DeleteConfirmAction = new ModalPopup("DeleteConfirmAction");

            // set modal properties
            this._DeleteConfirmAction.Type = ModalPopupType.Confirm;
            this._DeleteConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                              ImageFiles.EXT_PNG);
            this._DeleteConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._DeleteConfirmAction.HeaderText = _GlobalResources.DeleteSchedule;
            this._DeleteConfirmAction.TargetControlID = targetControlId;
            this._DeleteConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl bodyWrapper = new HtmlGenericControl("p");
            Literal body = new Literal();

            bodyWrapper.ID = "DeleteConfirmActionModalBody";
            body.Text = _GlobalResources.AreYouSureYouWantToDeleteThisSchedule;

            bodyWrapper.Controls.Add(body);

            // add controls to body
            this._DeleteConfirmAction.AddControlToBody(bodyWrapper);

            this.UserActionsContainer.Controls.Add(this._DeleteConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                //add row to datatable
                recordsToDelete.Rows.Add(Convert.ToInt32(this._IdResourceToObjectHidden.Value));

                // delete the records
                ResourceToObjectLink.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedback(_GlobalResources.ScheduleDeletedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {
                //rebuild after delete the schedule
                this._BuildUserActionsPanelDivs();
            }
        }
        #endregion

        #region _BuildCalendarIconModal
        /// <summary>
        /// Builds the modal for update schedule.
        /// </summary>
        private void _BuildCalendarIconModal(string targetControlId)
        {
            // set modal properties
            this._ScheduleUpdateModal = new ModalPopup("ScheduleUpdateModal");
            this._ScheduleUpdateModal.Type = ModalPopupType.Form;
            this._ScheduleUpdateModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR,
                                                                              ImageFiles.EXT_PNG);


            this._ScheduleUpdateModal.HeaderIconAlt = _GlobalResources.UpdateSchedule;
            this._ScheduleUpdateModal.HeaderText = _GlobalResources.UpdateSchedule;

            this._ScheduleUpdateModal.SubmitButtonTextType = ModalPopupButtonText.Submit;

            //adding hidden maintenance submit button to simulate button click.            
            this._ScheduleUpdateModalButton = new Button();
            this._ScheduleUpdateModalButton.ID = "ScheduleUpdateModalButton";
            this._ScheduleUpdateModalButton.Text = _GlobalResources.UpdateSchedule;

            this._ScheduleUpdateModalButton.Command += new CommandEventHandler(this._ScheduleUpdateModalButton_Command);
            this._ScheduleUpdateModalButton.Style.Add("display", "none");

            this._ScheduleUpdateModal.TargetControlID = targetControlId;
            this._ScheduleUpdateModal.SubmitButton.Command += new CommandEventHandler(this._ScheduleUpdateModalSubmit_Command);
            this._ScheduleUpdateModal.CloseButtonTextType = ModalPopupButtonText.Close;
            this._ScheduleUpdateModal.ReloadPageOnClose = true;

            // build the modal body
            this._ScheduleUpdateModalContainer = new Panel();
            this._ScheduleUpdateModalContainer.ID = "ScheduleUpdateModalContainer";

            Panel scheduleTypeContainer = new Panel();
            scheduleTypeContainer.ID = "ScheduleTypeContainer";
            scheduleTypeContainer.CssClass = "FormFieldLabelContainer";

            this._ScheduleType = new Label();
            scheduleTypeContainer.Controls.Add(this._ScheduleType);

            Panel scheduleUpdateModalStartLabelContainer = new Panel();
            scheduleUpdateModalStartLabelContainer.ID = "ScheduleUpdateModalStartLabelContainer";
            scheduleUpdateModalStartLabelContainer.CssClass = "FormFieldLabelContainer";

            // start date label
            Label scheduleUpdateModalStart = new Label();
            scheduleUpdateModalStart.Text = _GlobalResources.Start + ":";

            //required astrick
            Label scheduleUpdateModalStartRequiredAsterisk = new Label();
            scheduleUpdateModalStartRequiredAsterisk.Text = "* ";
            scheduleUpdateModalStartRequiredAsterisk.CssClass = "RequiredAsterisk";

            scheduleUpdateModalStartLabelContainer.Controls.Add(scheduleUpdateModalStartRequiredAsterisk);

            scheduleUpdateModalStartLabelContainer.Controls.Add(scheduleUpdateModalStart);

            // start date error panel
            Panel scheduleUpdateModalStartErrorContainer = new Panel();
            scheduleUpdateModalStartErrorContainer.ID = "ScheduleUpdateModalStartDate_ErrorContainer";
            scheduleUpdateModalStartErrorContainer.CssClass = "FormFieldErrorContainer";

            //start date picker
            this._ScheduleUpdateModalDtStart = new DatePicker("ScheduleUpdateModalDtStart", false, false, true);
            this._ScheduleUpdateModalDtStart.ID = "ScheduleUpdateModalDtStart";

            Panel scheduleUpdateModalEndLabelContainer = new Panel();
            scheduleUpdateModalEndLabelContainer.ID = "ScheduleUpdateModalEndLabelContainer";
            scheduleUpdateModalEndLabelContainer.CssClass = "FormFieldLabelContainer";

            //end date label
            Label scheduleUpdateModalEnd = new Label();
            scheduleUpdateModalEnd.Text = _GlobalResources.End + ":";

            //required astrick
            Label scheduleUpdateModalEndRequiredAsterisk = new Label();
            scheduleUpdateModalEndRequiredAsterisk.Text = "* ";
            scheduleUpdateModalEndRequiredAsterisk.CssClass = "RequiredAsterisk";

            scheduleUpdateModalEndLabelContainer.Controls.Add(scheduleUpdateModalEndRequiredAsterisk);
            scheduleUpdateModalEndLabelContainer.Controls.Add(scheduleUpdateModalEnd);

            // start date error panel
            Panel scheduleUpdateModalEndErrorContainer = new Panel();
            scheduleUpdateModalEndErrorContainer.ID = "ScheduleUpdateModalEndDate_ErrorContainer";
            scheduleUpdateModalEndErrorContainer.CssClass = "FormFieldErrorContainer";

            //end date picker
            this._ScheduleUpdateModalDtEnd = new DatePicker("ScheduleUpdateModalDtEnd", false, false, true);
            this._ScheduleUpdateModalDtEnd.ID = "ScheduleUpdateModalDtEnd";

            //add controls to panel
            this._ScheduleUpdateModalContainer.Controls.Add(this._ScheduleUpdateModalButton);

            this._ScheduleUpdateModalContainer.Controls.Add(scheduleTypeContainer);

            this._ScheduleUpdateModalContainer.Controls.Add(scheduleUpdateModalStartLabelContainer);
            this._ScheduleUpdateModalContainer.Controls.Add(scheduleUpdateModalStartErrorContainer);
            this._ScheduleUpdateModalContainer.Controls.Add(this._ScheduleUpdateModalDtStart);

            this._ScheduleUpdateModalContainer.Controls.Add(scheduleUpdateModalEndLabelContainer);
            this._ScheduleUpdateModalContainer.Controls.Add(scheduleUpdateModalEndErrorContainer);
            this._ScheduleUpdateModalContainer.Controls.Add(this._ScheduleUpdateModalDtEnd);

            //add controls to modal
            this._ScheduleUpdateModal.AddControlToBody(this._ScheduleUpdateModalContainer);

            // add modal to container
            this.UserActionsContainer.Controls.Add(this._ScheduleUpdateModal);
        }
        #endregion

        #region _ScheduleUpdateModalButton_Command
        /// <summary>
        /// Handles the event initiated by the hidden Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _ScheduleUpdateModalButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                //fills the modal pop up controls on update of any resource schedule
                //get resourceToObjectLink object with id
                if (!String.IsNullOrWhiteSpace(this._IdResourceToObjectHidden.Value))
                {
                    this._ResourceToObjectLinkObject = new ResourceToObjectLink(Convert.ToInt32(this._IdResourceToObjectHidden.Value));

                    //bind controls
                    this._ScheduleUpdateModalDtStart.Value = TimeZoneInfo.ConvertTimeFromUtc(this._ResourceToObjectLinkObject.DtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                    this._ScheduleUpdateModalDtEnd.Value = TimeZoneInfo.ConvertTimeFromUtc(this._ResourceToObjectLinkObject.DtEnd.Value, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                    this._ScheduleUpdateModal.HeaderText = this._ResourceToObjectLinkObject.IsOutsideUse == true ? _GlobalResources.OutsideUse : _GlobalResources.Maintenance;
                    this._ScheduleUpdateModal.HeaderIconAlt = this._ResourceToObjectLinkObject.IsOutsideUse == true ? _GlobalResources.OutsideUse : _GlobalResources.Maintenance;

                    this._ScheduleType.Text = _GlobalResources.BookedFor + ": " + (this._ResourceToObjectLinkObject.IsOutsideUse == true ? _GlobalResources.OutsideUse : _GlobalResources.Maintenance);

                    this._IdResourceToObjectHidden.Value = string.Empty;
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ScheduleUpdateModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ScheduleUpdateModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ScheduleUpdateModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ScheduleUpdateModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ScheduleUpdateModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _ValidateCalendarIconModalProperties
        /// <summary>
        ///  Calendar icon validations
        /// </summary>
        private bool _ValidateCalendarIconModalProperties()
        {
            bool isValid = true;

            //validation for schedule update start date
            if ((this._ScheduleUpdateModalDtStart.Value == null || String.IsNullOrWhiteSpace(this._ScheduleUpdateModalDtStart.Value.ToString())) || this._ScheduleUpdateModalDtStart.Value < DateTime.Now)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._ScheduleUpdateModalContainer, "ScheduleUpdateModalStartDate", _GlobalResources.Start + " " + _GlobalResources.IsRequiredAndMustBeEqualToOrGreaterThanCurrentDate);
            }

            //validation for schedule update end date
            if ((this._ScheduleUpdateModalDtEnd.Value == null || String.IsNullOrWhiteSpace(this._ScheduleUpdateModalDtEnd.Value.ToString())) || this._ScheduleUpdateModalDtEnd.Value <= this._ScheduleUpdateModalDtStart.Value)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._ScheduleUpdateModalContainer, "ScheduleUpdateModalEndDate", _GlobalResources.End + " " + _GlobalResources.IsRequiredAndMustBeGreaterThanStartDate);
            }
            return isValid;
        }
        #endregion

        #region _CalendarIconModalSubmit_Command
        /// <summary>
        /// calendar icon submit command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ScheduleUpdateModalSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (this._ResourceObject != null)
                {
                    #region Validation

                    // validate the form
                    if (!this._ValidateCalendarIconModalProperties())
                    { throw new AsentiaException(); }
                    #endregion

                    //creating resource to object link class object
                    this._ResourceToObjectLinkObject = new ResourceToObjectLink(Convert.ToInt32(this._IdResourceToObjectHidden.Value));

                    //converts the start and end date into utc date and sets values into object
                    this._ResourceToObjectLinkObject.DtStart = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(this._ScheduleUpdateModalDtStart.Value), TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                    this._ResourceToObjectLinkObject.DtEnd = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(this._ScheduleUpdateModalDtEnd.Value), TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                    //update schedule
                    this._ResourceToObjectLinkObject.Save();

                    //display success message
                    this._ScheduleUpdateModal.DisplayFeedback(_GlobalResources.RecordSavedSuccessfully, false);
                }
                else
                {
                    Response.Redirect("~/administrator/resourcemanagement");
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ScheduleUpdateModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ScheduleUpdateModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ScheduleUpdateModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ScheduleUpdateModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ScheduleUpdateModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {
                this._ResourceToObjectLinkObject = null;
            }
        }
        #endregion

        #region _BuildResourceObjectModal
        /// <summary>
        /// Builds the modal for resource object.
        /// </summary>
        private void _BuildResourceObjectModal(string targetControlId)
        {
            // set modal properties
            this._ResourceObjectModal = new ModalPopup("ResourceObjectModal");
            this._ResourceObjectModal.Type = ModalPopupType.Form;
            this._ResourceObjectModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION,
                                                                              ImageFiles.EXT_PNG);
            this._ResourceObjectModal.HeaderIconAlt = _GlobalResources.InstructorLedTrainingSession;
            this._ResourceObjectModal.HeaderText = _GlobalResources.InstructorLedTrainingSession;
            this._ResourceObjectModal.SubmitButtonTextType = ModalPopupButtonText.Submit;
            this._ResourceObjectModal.SubmitButton.Style.Add("display", "none");

            this._ResourceObjectModal.TargetControlID = targetControlId;
            this._ResourceObjectModal.CloseButtonTextType = ModalPopupButtonText.Close;
            this._ResourceObjectModal.ReloadPageOnClose = false;

            // build the modal body
            Panel resourceObjectModalContainer = new Panel();
            resourceObjectModalContainer.ID = "ResourceObjectModalContainer";

            //adding hidden maintenance submit button to simulate button click.            
            this._ResourceObjectModalButton = new Button();
            this._ResourceObjectModalButton.ID = "ResourceObjectModalButton";
            this._ResourceObjectModalButton.Text = _GlobalResources.ClickHere;

            this._ResourceObjectModalButton.Command += new CommandEventHandler(this._ResourceObjectModalButton_Command);
            this._ResourceObjectModalButton.Style.Add("display", "none");

            resourceObjectModalContainer.Controls.Add(this._ResourceObjectModalButton);

            //Module title label container
            Panel moduleTitleLabelContainer = new Panel();
            moduleTitleLabelContainer.ID = "ModuleTitleLabelContainer";
            moduleTitleLabelContainer.CssClass = "FormFieldLabelContainer";

            //title label
            Label objectTitleLabel = new Label();
            objectTitleLabel.Text = _GlobalResources.Module + ": ";

            moduleTitleLabelContainer.Controls.Add(objectTitleLabel);

            //Module title container
            Panel moduleTitleContainer = new Panel();
            moduleTitleContainer.ID = "ModuleTitleContainer";

            this._ModuleTitle = new Literal();
            this._ModuleTitle.ID = "ModuleTitle";

            moduleTitleContainer.Controls.Add(this._ModuleTitle);

            //session title label container
            Panel sessionTitleLabelContainerLabelContainer = new Panel();
            sessionTitleLabelContainerLabelContainer.ID = "SessionTitle_LabelContainer";
            sessionTitleLabelContainerLabelContainer.CssClass = "FormFieldLabelContainer";

            //session title label
            Label sessionTitleLabel = new Label();
            sessionTitleLabel.Text = _GlobalResources.Session + ": ";

            sessionTitleLabelContainerLabelContainer.Controls.Add(sessionTitleLabel);

            //session sitle container
            Panel sessionTitleContainer = new Panel();
            sessionTitleContainer.ID = "SessionTitleContainer";

            this._SessionTitle = new Literal();
            this._SessionTitle.ID = "SessionTitle";

            sessionTitleContainer.Controls.Add(this._SessionTitle);

            resourceObjectModalContainer.Controls.Add(moduleTitleLabelContainer);
            resourceObjectModalContainer.Controls.Add(moduleTitleContainer);
            resourceObjectModalContainer.Controls.Add(sessionTitleLabelContainerLabelContainer);
            resourceObjectModalContainer.Controls.Add(sessionTitleContainer);

            Panel sessionStartLabelContainer = new Panel();
            sessionStartLabelContainer.ID = "SessionStart_LabelContainer";
            sessionStartLabelContainer.CssClass = "FormFieldLabelContainer";

            // start label
            Label sessionStartLabel = new Label();
            sessionStartLabel.Text = _GlobalResources.Start + ":";

            sessionStartLabelContainer.Controls.Add(sessionStartLabel);

            //start date picker
            this._SessionDtStart = new DatePicker("SessionDtStart", false, false, true);
            this._SessionDtStart.ID = "SessionDtStart";
            this._SessionDtStart.Enabled = false;

            Panel sessionEndLabelContainer = new Panel();
            sessionEndLabelContainer.ID = "SessionEnd_LabelContainer";
            sessionEndLabelContainer.CssClass = "FormFieldLabelContainer";

            //end label
            Label sessionEnd = new Label();
            sessionEnd.Text = _GlobalResources.End + ":";

            sessionEndLabelContainer.Controls.Add(sessionEnd);

            //end date picker
            this._SessionDtEnd = new DatePicker("SessionDtEnd", false, false, true);
            this._SessionDtEnd.ID = "SessionDtEnd";
            this._SessionDtEnd.Enabled = false;

            //add controls to panel
            resourceObjectModalContainer.Controls.Add(sessionStartLabelContainer);
            resourceObjectModalContainer.Controls.Add(this._SessionDtStart);
            resourceObjectModalContainer.Controls.Add(sessionEndLabelContainer);
            resourceObjectModalContainer.Controls.Add(this._SessionDtEnd);

            //add controls to modal
            this._ResourceObjectModal.AddControlToBody(resourceObjectModalContainer);

            // add modal to container
            this.UserActionsContainer.Controls.Add(this._ResourceObjectModal);
        }
        #endregion

        #region _ResourceObjectModalButton_Command
        /// <summary>
        /// Handles the event initiated by the hidden Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _ResourceObjectModalButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                this._ResourceObjectModal.ClearFeedback();
                this._SessionDtStart.Value = null;
                this._SessionDtEnd.Value = null;
                this._ModuleTitle.Text = string.Empty;
                this._SessionTitle.Text = string.Empty;

                if (!String.IsNullOrWhiteSpace(this._IdObjectHidden.Value))
                {
                    int objectId = Convert.ToInt32(this._IdObjectHidden.Value);

                    //initialization of the object
                    this._StandupTrainingInstanceObject = new Library.StandupTrainingInstance(objectId);

                    //initialization of the object
                    this._StandupTrainingObject = new Library.StandupTraining(this._StandupTrainingInstanceObject.IdStandupTraining);

                    //binding controls
                    this._ModuleTitle.Text = this._StandupTrainingObject.Title;
                    this._SessionTitle.Text = this._StandupTrainingInstanceObject.Title;

                    if (!String.IsNullOrWhiteSpace(this._IdResourceToObjectHidden.Value))
                    {
                        this._ResourceToObjectLinkObject = new ResourceToObjectLink(Convert.ToInt32(this._IdResourceToObjectHidden.Value));

                        DateTime startDate = TimeZoneInfo.ConvertTimeFromUtc(this._ResourceToObjectLinkObject.DtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                        DateTime endDate = TimeZoneInfo.ConvertTimeFromUtc(this._ResourceToObjectLinkObject.DtEnd.Value, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        this._SessionDtStart.Value = startDate;
                        this._SessionDtEnd.Value = endDate;
                    }
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ResourceObjectModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ResourceObjectModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ResourceObjectModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ResourceObjectModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ResourceObjectModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _GetNumberOfDayInWeek
        /// <summary>
        /// Returns the array index number of a particular day in a week.
        /// </summary>
        /// <param name="day">the name of the weekday</param>
        private int _GetNumberOfDayInWeek(string day)
        {
            switch (day)
            {
                case "Sunday":
                    return 0;
                case "Monday":
                    return 1;
                case "Tuesday":
                    return 2;
                case "Wednesday":
                    return 3;
                case "Thursday":
                    return 4;
                case "Friday":
                    return 5;
                case "Saturday":
                    return 6;
                default:
                    return 0;
            }
        }
        #endregion

        #region _GetItemIcon
        /// <summary>
        /// Gets the icon for the event item on the calendar.
        /// </summary>
        /// <param name="eventType">the event type</param>
        /// <returns>string with path to icon</returns>
        private string _GetItemIcon(string eventType)
        {
            switch (eventType)
            {
                case "resourceBooked":
                    return ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR,
                                                  ImageFiles.EXT_PNG);
                case "standupTraining":
                    return ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION,
                                                  ImageFiles.EXT_PNG);


                default:
                    return String.Empty;
            }
        }
        #endregion
    }
}
