﻿//Handles resource type creation and modification process
function OnModifyResourceType(idResourceToObject) {

    //set idResourceToObject into hidden field
    $("#" + IdResourceTypeHidden).val(idResourceToObject);

    //click the update hidden button to show  modal popup
    document.getElementById(ResourceTypeHiddenLink).click();

    if ($('#' + IdResourceTypeHidden).val() == "" || $('#' + IdResourceTypeHidden).val() == "0") {
        $("#" + ResourceTypeModal + "ModalPopupHeaderText").text(AddNewHeaderText);

        //set moadal submit button text
        $("#" + ResourceTypeModal + "ModalPopupSubmitButton").val(AddModalSubmitButtonText)
    }


    //click the hidden button to fill the controls of  modal popup
    document.getElementById(SaveResourceTypeModalButton).click();
}

function pageLoad() {

    var idResourceType = $("#"+IdResourceTypeHidden).val();

    if (idResourceType == "" || idResourceType == "0") {
        $('#ResourceTypeModalModalPopupHeaderText').html(AddNewHeaderText);
        $("#" + ResourceTypeModal + "ModalPopupSubmitButton").val(AddModalSubmitButtonText)
    }
}