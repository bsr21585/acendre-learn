﻿using System;
using System.Text;
using System.Globalization;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.ResourceManagement
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ResourcePropertiesFormContentWrapperContainer;
        public Panel PropertiesHeaderContainer;
        public Panel PropertiesWrapperContainer;
        public Panel PropertiesInstructionsPanel;
        public Panel PropertiesFeedbackContainer;
        public Panel PropertiesContainer;
        public Panel PropertiesActionsPanel;
        public Panel PropertiesTabPanelsContainer;
        public Panel ResourceOwnersListContainer;
        #endregion

        #region Private Properties
        private Resource _ResourceObject;
        private TextBox _ResourceName;
        private TextBox _ResourceDescription;
        private DataTable _ResourceTypeListForResource;
        private Button _SaveButton;
        private Button _CancelButton;
        private DropDownList _SelectResourcesType;
        private TextBox _ResourceBuilding;
        private TextBox _ResourceRoom;
        private TextBox _ResourceCity;
        private TextBox _ResourceProvince;
        private ModalPopup _SelectOwnerForResource;
        private DataTable _EligibleResourceOwnersForSelectList;
        private DynamicListBox _SelectEligibleOwnerUsers;
        private DynamicListBox _SelectEligibleParent;
        private HiddenField _SelectedResourceOwner;
        private TextBox _ResourceCountry;
        private RadioButtonList _ResourceMovable;
        private RadioButtonList _ResourceAvailable;
        private RadioButtonList _ResourceOwnerType;
        private TextBox _ResourceIdentifier;
        private TextBox _ResourceOwnerName;
        private TextBox _ResourceOwnerEmail;
        private TextBox _ResourceOwnerContactInfo;
        private HiddenField _SelectedResourceParent;
        private Panel _ResourceParentListContainer;
        private ModalPopup _SelectParentForResource;
        private DataTable _EligibleResourceParentForSelectList;
        private HyperLink _LocationTabLink;
        private HyperLink _OwnerTabLink;
        private TextBox _ResourceCapacity;
        private Panel _OwnerNameContainer = new Panel();
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.ResourceManagement.Modify.js");

            StringBuilder sbResourceGlobalJS = new StringBuilder();
            sbResourceGlobalJS.AppendLine("DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            sbResourceGlobalJS.AppendLine("UserNameContainer = \"" + this._OwnerNameContainer.ClientID + "\";");
            csm.RegisterClientScriptBlock(typeof(Modify), "ResourceGlobalJS", sbResourceGlobalJS.ToString(), true);

            // for multiple "start up" scripts, build start up calls for MCE and add to the Page_Load 
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_ResourceManager))
            { Response.Redirect("/"); }

            // get the group object
            this._GetResourceObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.ResourcePropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.PropertiesWrapperContainer.CssClass = "FormContentContainer";

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetResourceObject
        /// <summary>
        /// Gets Resource object
        /// </summary>
        private void _GetResourceObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._ResourceObject = new Resource(id); }

                }
                catch
                { Response.Redirect("~/administrator/ResourceManagement"); }
            }
            else { this._ResourceObject = new Resource(); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string pageTitle;

            if (this._ResourceObject != null && !String.IsNullOrWhiteSpace(this._ResourceObject.Name))
            {
                breadCrumbPageTitle = this._ResourceObject.Name;
                pageTitle = this._ResourceObject.Name;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewResource;
                pageTitle = _GlobalResources.NewResource;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ResourceManagement, "/administrator/resourcemanagement/"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_RESOURCE, ImageFiles.EXT_PNG));
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds page controls
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the properties form
            this._BuildPropertiesForm();

            // build the properties form actions panel
            this._BuildPropertiesActionsPanel();

        }
        #endregion

        #region _BuildPropertiesActionsPanel
        /// <summary>
        /// Builds properties action controls panel
        /// </summary>
        private void _BuildPropertiesActionsPanel()
        {
            // clear controls from container
            this.PropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.PropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._ResourceObject == null)
            { this._SaveButton.Text = _GlobalResources.CreateResource; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this._SaveButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicElements();");
            this.PropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.PropertiesActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _BuildPropertiesForm
        /// <summary>
        /// Builds page properties form
        /// </summary>
        private void _BuildPropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PropertiesInstructionsPanel, _GlobalResources.CreateOfModifyTheResourcePropertiesUsingTheFormBelow, true);

            // clear controls from container
            this.PropertiesContainer.Controls.Clear();

            // build the Resource properties form tabs
            this._BuildResourcePropertiesFormTabs();

            this.PropertiesTabPanelsContainer = new Panel();
            this.PropertiesTabPanelsContainer.ID = "Properties_TabPanelsContainer";
            this.PropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.PropertiesContainer.Controls.Add(this.PropertiesTabPanelsContainer);

            // build the "properties" panel of the Resource properties form
            this._BuildResourceFormPropertiesPanel();

            // build the "location" panel of the Resource properties form
            this._BuildResourcePropertiesFormLocationPanel();

            // build the "owner" panel of the Resource properties form
            this._BuildResourcePropertiesFormOwnersPanel();


            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _BuildResourcePropertiesFormTabs
        /// <summary>
        /// Builds page properties form tabs
        /// </summary>
        private void _BuildResourcePropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("Location", _GlobalResources.Location));
            tabs.Enqueue(new KeyValuePair<string, string>("Owners", _GlobalResources.Owner));

            // build and attach the tabs
            this.PropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("ResourceProperties", tabs, null, this.Page, null));
        }
        #endregion

        #region _BuildResourcePropertiesFormPropertiesPanel
        /// <summary>
        /// Builds resource form Properties Panel
        /// </summary>
        private void _BuildResourceFormPropertiesPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "ResourceProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.Attributes.Add("style", "display: block;");

            #region Name field
            // name
            this._ResourceName = new TextBox();
            this._ResourceName.ID = "Name_Field";
            this._ResourceName.CssClass = "InputMedium";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("Name",
                                                             _GlobalResources.Name,
                                                             this._ResourceName.ID,
                                                             this._ResourceName,
                                                             true,
                                                             true,
                                                             true));
            #endregion

            #region Description field
            // description field
            this._ResourceDescription = new TextBox();
            this._ResourceDescription.ID = "Description_Field";
            this._ResourceDescription.Style.Add("width", "98%");
            this._ResourceDescription.TextMode = TextBoxMode.MultiLine;
            this._ResourceDescription.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("Description",
                                                             _GlobalResources.Description,
                                                             this._ResourceDescription.ID,
                                                             this._ResourceDescription,
                                                             false,
                                                             true,
                                                             true));
            #endregion

            #region Resource Type select container
            // resource type
            this._ResourceTypeListForResource = this._ResourceTypeListForResource ?? new DataTable();
            this._ResourceTypeListForResource = this._ResourceObject.GetResourceTypeList(false, null);

            this._SelectResourcesType = new DropDownList();
            this._SelectResourcesType.ID = "ResourceType_Field";
            this._SelectResourcesType.DataSource = this._ResourceTypeListForResource;
            this._SelectResourcesType.DataTextField = "ResourceType";
            this._SelectResourcesType.DataValueField = "idResourceType";
            this._SelectResourcesType.DataBind();

            ListItem lstitem = new ListItem(_GlobalResources.Select, "0");
            this._SelectResourcesType.Items.Insert(0, lstitem);

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("ResourceType",
                                                             _GlobalResources.ResourceType,
                                                             this._SelectResourcesType.ID,
                                                             this._SelectResourcesType,
                                                             false,
                                                             true,
                                                             false));


            #endregion

            #region Identifier field
            // identifier
            this._ResourceIdentifier = new TextBox();
            this._ResourceIdentifier.ID = "Identifier_Field";
            this._ResourceIdentifier.CssClass = "InputMedium";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("Identifier",
                                                 _GlobalResources.Identifier,
                                                 this._ResourceIdentifier.ID,
                                                 this._ResourceIdentifier,
                                                 false,
                                                 true,
                                                 false));
            #endregion

            #region Is Movable Field
            // Resource is Movable
            this._ResourceMovable = new RadioButtonList();
            this._ResourceMovable.ID = "ResourcesMovable_Field";
            this._ResourceMovable.Items.Add(new ListItem(_GlobalResources.Yes, "True"));
            this._ResourceMovable.Items.Add(new ListItem(_GlobalResources.No, "False"));
            this._ResourceMovable.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this._ResourceMovable.SelectedIndex = 1;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("ResourcesMovable",
                                                 _GlobalResources.Movable,
                                                 this._ResourceMovable.ID,
                                                 this._ResourceMovable,
                                                 false,
                                                 true,
                                                 false));
            #endregion

            #region Is Available Field
            this._ResourceAvailable = new RadioButtonList();
            this._ResourceAvailable.ID = "CourseAvailable_Field";
            this._ResourceAvailable.Items.Add(new ListItem(_GlobalResources.Yes, "True"));
            this._ResourceAvailable.Items.Add(new ListItem(_GlobalResources.No, "False"));
            this._ResourceAvailable.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this._ResourceAvailable.SelectedIndex = 1;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CourseAvailable",
                                                 _GlobalResources.Available,
                                                 this._ResourceAvailable.ID,
                                                 this._ResourceAvailable,
                                                 false,
                                                 true,
                                                 false));
            #endregion

            #region Capacity field
            // capacity
            this._ResourceCapacity = new TextBox();
            this._ResourceCapacity.ID = "Capacity_Field";
            this._ResourceCapacity.CssClass = "InputMedium";
            this._ResourceCapacity.MaxLength = 10;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("Capacity",
                                                 _GlobalResources.Capacity,
                                                 this._ResourceCapacity.ID,
                                                 this._ResourceCapacity,
                                                 false,
                                                 true,
                                                 false));
            #endregion

            // build the "parent resource" panel of the Resource properties form
            this._BuildResourcePropertiesFormSelectParentPanel(propertiesPanel);

            // attach panel to container
            this.PropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildResourcePropertiesFormLocationPanel
        /// <summary>
        ///  Build Location Panel
        /// </summary>
        private void _BuildResourcePropertiesFormLocationPanel()
        {
            Panel locationPanel = new Panel();
            locationPanel.ID = "ResourceProperties_" + "Location" + "_TabPanel";
            locationPanel.Attributes.Add("style", "display: none;");

            #region Room field
            // room
            this._ResourceRoom = new TextBox();
            this._ResourceRoom.ID = "room_Field";
            this._ResourceRoom.CssClass = "InputMedium";

            locationPanel.Controls.Add(AsentiaPage.BuildFormField("Room",
                                                 _GlobalResources.Room,
                                                 this._ResourceRoom.ID,
                                                 this._ResourceRoom,
                                                 false,
                                                 true,
                                                 false));
            #endregion

            #region Building field
            // Building
            this._ResourceBuilding = new TextBox();
            this._ResourceBuilding.ID = "Building_Field";
            this._ResourceBuilding.CssClass = "InputMedium";

            locationPanel.Controls.Add(AsentiaPage.BuildFormField("Building",
                                                 _GlobalResources.Building,
                                                 this._ResourceBuilding.ID,
                                                 this._ResourceBuilding,
                                                 false,
                                                 true,
                                                 false));
            #endregion

            #region City field container
            // City container
            this._ResourceCity = new TextBox();
            this._ResourceCity.ID = "City_Field";
            this._ResourceCity.CssClass = "InputMedium";

            locationPanel.Controls.Add(AsentiaPage.BuildFormField("City",
                                                 _GlobalResources.City,
                                                 this._ResourceCity.ID,
                                                 this._ResourceCity,
                                                 false,
                                                 true,
                                                 false));
            #endregion

            #region Province/ State  field
            // Province
            this._ResourceProvince = new TextBox();
            this._ResourceProvince.ID = "Province_Field";
            this._ResourceProvince.CssClass = "InputMedium";

            locationPanel.Controls.Add(AsentiaPage.BuildFormField("Province",
                                                 _GlobalResources.StateProvince,
                                                 this._ResourceProvince.ID,
                                                 this._ResourceProvince,
                                                 false,
                                                 true,
                                                 false));
            #endregion

            #region Country field
            // Country
            this._ResourceCountry = new TextBox();
            this._ResourceCountry.ID = "Country_Field";
            this._ResourceCountry.CssClass = "InputMedium";

            locationPanel.Controls.Add(AsentiaPage.BuildFormField("Country",
                                                 _GlobalResources.Country,
                                                 this._ResourceCountry.ID,
                                                 this._ResourceCountry,
                                                 false,
                                                 true,
                                                 false));
            #endregion

            // attach panel to container
            this.PropertiesTabPanelsContainer.Controls.Add(locationPanel);
        }
        #endregion

        #region _BuildResourcePropertiesFormSelectParentPanel
        /// <summary>
        ///  Builds select parent resource panel
        /// </summary>
        private void _BuildResourcePropertiesFormSelectParentPanel(Panel panelParent)
        {
            // populate datatables with lists of users who are resource experts and who are not
            if (this._ResourceObject != null)
            {
                this._EligibleResourceParentForSelectList = Resource.IdsAndNamesForResourceParentSelectList(this._ResourceObject.IdResource, null);
            }
            else
            {
                this._EligibleResourceParentForSelectList = Resource.IdsAndNamesForResourceParentSelectList(0, null);
            }

            this._AddResourceParentControls(panelParent);
        }
        #endregion

        #region _BuildResourcePropertiesFormOwnersPanel
        /// <summary>
        ///  Build Owners Panel
        /// </summary>
        private void _BuildResourcePropertiesFormOwnersPanel()
        {
            // populate datatables with lists of users who are resource experts and who are not
            if (this._ResourceObject != null)
            {
                this._EligibleResourceOwnersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForResourceOwnersSelectList(this._ResourceObject.IdResource, null);
            }
            else
            {
                this._EligibleResourceOwnersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForResourceOwnersSelectList(0, null);
            }

            Panel ownersPanel = new Panel();
            ownersPanel.ID = "ResourceProperties_" + "Owners" + "_TabPanel";
            ownersPanel.Attributes.Add("style", "display: none;");

            #region Resource OwnerType Field
            // Resource ownerType
            this._ResourceOwnerType = new RadioButtonList();
            this._ResourceOwnerType.ID = "ResourceOwnerType_Field";
            ListItem liOwnerTypeUser = new ListItem(_GlobalResources.UserWithinSystem, "user");
            liOwnerTypeUser.Attributes.Add("onClick", "javascript:ToggleOwnerTypePanel();");
            ListItem liOwnerTypeDefine = new ListItem(_GlobalResources.SpecifiedBelow, "define");
            liOwnerTypeDefine.Attributes.Add("onClick", "javascript:ToggleOwnerTypePanel();");
            this._ResourceOwnerType.Items.Add(liOwnerTypeUser);
            this._ResourceOwnerType.Items.Add(liOwnerTypeDefine);
            this._ResourceOwnerType.RepeatDirection = RepeatDirection.Horizontal;
            this._ResourceOwnerType.SelectedIndex = 1;

            ownersPanel.Controls.Add(AsentiaPage.BuildFormField("ResourceOwnerType",
                                            null,
                                            this._ResourceOwnerType.ID,
                                            this._ResourceOwnerType,
                                            false,
                                            true,
                                            false));
            #endregion

            // Add Controls for Specific Owner Details in specified panel
            this._AddOwnerSpecificDetailsControls(ownersPanel);

            // Add Control for select Owner-Cum-User from with in system 
            this._AddOwnerWithinSystemControls(ownersPanel);

            // attach panel to container
            this.PropertiesTabPanelsContainer.Controls.Add(ownersPanel);
        }
        #endregion

        #region _AddOwnerSpecificDetailsControls
        /// <summary>
        ///  Build Owner Specific Details Controls
        /// </summar
        private void _AddOwnerSpecificDetailsControls(Panel parent)
        {
            Panel ownerSpecificPanel = new Panel();
            ownerSpecificPanel.ID = "ResourceProperties_" + "Owner" + "_define";
            ownerSpecificPanel.Attributes.Add("style", "display: none;");

            #region Owner Name field
            this._ResourceOwnerName = new TextBox();
            this._ResourceOwnerName.ID = "ownerName_Field";
            this._ResourceOwnerName.CssClass = "InputMedium";

            ownerSpecificPanel.Controls.Add(AsentiaPage.BuildFormField("OwnerName",
                                                 _GlobalResources.Name,
                                                 this._ResourceOwnerName.ID,
                                                 this._ResourceOwnerName,
                                                 true,
                                                 true,
                                                 false));
            #endregion

            #region OwnerEmail field
            this._ResourceOwnerEmail = new TextBox();
            this._ResourceOwnerEmail.ID = "OwnerEmail_Field";
            this._ResourceOwnerEmail.CssClass = "InputMedium";

            ownerSpecificPanel.Controls.Add(AsentiaPage.BuildFormField("OwnerEmail",
                                                 _GlobalResources.Email,
                                                 this._ResourceOwnerEmail.ID,
                                                 this._ResourceOwnerEmail,
                                                 true,
                                                 true,
                                                 false));
            #endregion

            #region OwnerContactInfo field
            // description field
            this._ResourceOwnerContactInfo = new TextBox();
            this._ResourceOwnerContactInfo.TextMode = TextBoxMode.MultiLine;
            this._ResourceOwnerContactInfo.Rows = 6;
            this._ResourceOwnerContactInfo.ID = "OwnerContactInfo_Field";
            this._ResourceOwnerContactInfo.CssClass = "ckeditor InputMedium";
            this._ResourceOwnerContactInfo.Style.Add("width", "95%");

            ownerSpecificPanel.Controls.Add(AsentiaPage.BuildFormField("OwnerContactInfo",
                                                             _GlobalResources.ContactInformation,
                                                             this._ResourceOwnerContactInfo.ID,
                                                             this._ResourceOwnerContactInfo,
                                                             true,
                                                             true,
                                                             false));
            #endregion

            parent.Controls.Add(ownerSpecificPanel);
        }
        #endregion

        #region _AddOwnerWithinSystemControls
        /// <summary>
        /// Add Owner with in System Controls
        /// </summary>
        /// <param name="parent"></param>
        private void _AddOwnerWithinSystemControls(Panel parent)
        {

            //owner Field
            List<Control> ownerFieldContainer = new List<Control>();

            Panel resourceOwnerContainer = new Panel();
            resourceOwnerContainer.ID = "ResourceProperties_" + "Owner" + "_user";

            Panel ownerButtonsPanel = new Panel();
            ownerButtonsPanel.ID = "SelectOwner_ButtonsPanel";

            // resource selected owner hidden field
            this._SelectedResourceOwner = new HiddenField();
            this._SelectedResourceOwner.ID = "SelectedResourceOwner_Field";

            // build a container for the owner
            this.ResourceOwnersListContainer = new Panel();
            this.ResourceOwnersListContainer.ID = "ResourceOwnerList_Container";

            // select owner button
            // link
            Image selectOwnerImageForLink = new Image();
            selectOwnerImageForLink.ID = "LaunchSelectOwnerModalImage";
            selectOwnerImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR,
                                                                           ImageFiles.EXT_PNG);
            selectOwnerImageForLink.CssClass = "MediumIcon";

            Localize selectOwnerTextForLink = new Localize();
            selectOwnerTextForLink.Text = _GlobalResources.SelectOwner;

            LinkButton selectOwnerLink = new LinkButton();
            selectOwnerLink.ID = "LaunchSelectOwnerModal";
            selectOwnerLink.CssClass = "ImageLink";
            selectOwnerLink.Controls.Add(selectOwnerImageForLink);
            selectOwnerLink.Controls.Add(selectOwnerTextForLink);
            ownerButtonsPanel.Controls.Add(selectOwnerLink);

            // build modal for selecting owner
            this._BuildSelectOwnerModal(selectOwnerLink.ID);

            // add owner controls to field container
            resourceOwnerContainer.Controls.Add(this._SelectedResourceOwner);
            resourceOwnerContainer.Controls.Add(this.ResourceOwnersListContainer);
            resourceOwnerContainer.Controls.Add(ownerButtonsPanel);

            // add controls to container
            ownerFieldContainer.Add(resourceOwnerContainer);
            parent.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LaunchSelectOwner",
                                                                               _GlobalResources.Owner,
                                                                               ownerFieldContainer,
                                                                               false,
                                                                               true));

        }

        #endregion

        #region _AddResourceParentControls
        /// <summary>
        /// Add Resource Parent List Controls
        /// </summary>
        /// <param name="parent"></param>
        private void _AddResourceParentControls(Panel parent)
        {
            // resource Parent Field
            List<Control> resourceParentListPanel = new List<Control>();

            // resource selected Parent hidden field
            this._SelectedResourceParent = new HiddenField();
            this._SelectedResourceParent.ID = "SelectedResourceParent_Field";

            // build a container for the resource membership listing
            this._ResourceParentListContainer = new Panel();
            this._ResourceParentListContainer.ID = "ResourceParentList_Container";

            Panel resourceParentButtonsPanel = new Panel();
            resourceParentButtonsPanel.ID = "ResourceParent_ButtonsPanel";

            // select Parent button

            // link
            Image selectParentImageForLink = new Image();
            selectParentImageForLink.ID = "LaunchSelectParentModalImage";
            selectParentImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR,
                                                                       ImageFiles.EXT_PNG);
            selectParentImageForLink.CssClass = "MediumIcon";

            Localize selectParentTextForLink = new Localize();
            selectParentTextForLink.Text = _GlobalResources.SelectParentResource;

            LinkButton selectParentLink = new LinkButton();
            selectParentLink.ID = "LaunchSelectParentModal";
            selectParentLink.CssClass = "ImageLink";
            selectParentLink.Controls.Add(selectParentImageForLink);
            selectParentLink.Controls.Add(selectParentTextForLink);
            resourceParentButtonsPanel.Controls.Add(selectParentLink);

            // attach panels to the container
            resourceParentListPanel.Add(this._SelectedResourceParent);
            resourceParentListPanel.Add(this._ResourceParentListContainer);
            resourceParentListPanel.Add(resourceParentButtonsPanel);

            // builds modal for selecting parent resource
            this._BuildSelectParentModal(selectParentLink.ClientID);

            parent.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ResourceParent",
                                                                               _GlobalResources.ParentResource,
                                                                               resourceParentListPanel,
                                                                               false,
                                                                               true));

        }

        #endregion

        #region _BuildSelectParentModal
        /// <summary>
        /// Builds the modal for selecting parent resource
        /// </summary>
        private void _BuildSelectParentModal(string targetControlId)
        {
            // set modal properties
            this._SelectParentForResource = new ModalPopup("SelectParentForResourceModal");
            this._SelectParentForResource.Type = ModalPopupType.Form;
            this._SelectParentForResource.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR,
                                                                                  ImageFiles.EXT_PNG);
            this._SelectParentForResource.HeaderIconAlt = _GlobalResources.SelectParentResource;
            this._SelectParentForResource.HeaderText = _GlobalResources.SelectParentResource;
            this._SelectParentForResource.TargetControlID = targetControlId;
            this._SelectParentForResource.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectParentForResource.SubmitButtonCustomText = _GlobalResources.AddResource;
            this._SelectParentForResource.SubmitButton.OnClientClick = "javascript:AddParentToResourceParentListContainer(); return false;";
            this._SelectParentForResource.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectParentForResource.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectParentForResource.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleParent = new DynamicListBox("SelectEligibleParent");
            this._SelectEligibleParent.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectEligibleParent.IncludeSelectAllNone = false;
            this._SelectEligibleParent.IsMultipleSelect = false;
            this._SelectEligibleParent.SearchButton.Command += new CommandEventHandler(this._SearchSelectParentButton_Command);
            this._SelectEligibleParent.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectParentButton_Command);
            this._SelectEligibleParent.ListBoxControl.DataSource = this._EligibleResourceParentForSelectList;
            this._SelectEligibleParent.ListBoxControl.DataTextField = "name";
            this._SelectEligibleParent.ListBoxControl.DataValueField = "idResource";
            this._SelectEligibleParent.ListBoxControl.DataBind();

            // add controls to body
            this._SelectParentForResource.AddControlToBody(this._SelectEligibleParent);

            // add modal to container
            this.PropertiesContainer.Controls.Add(this._SelectParentForResource);
        }
        #endregion

        #region _SearchSelectParentButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Parent" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectParentButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectParentForResource.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleParent.ListBoxControl.Items.Clear();

            // do the search
            if (this._ResourceObject != null)
            { this._EligibleResourceParentForSelectList = Resource.IdsAndNamesForResourceParentSelectList(this._ResourceObject.IdResource, this._SelectEligibleParent.SearchTextBox.Text); }
            else
            { this._EligibleResourceParentForSelectList = Resource.IdsAndNamesForResourceParentSelectList(0, this._SelectEligibleParent.SearchTextBox.Text); }

            _SelectEligibleParentDataBind();
        }
        #endregion

        #region _ClearSearchSelectParentButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Parent" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectParentButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectParentForResource.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleParent.ListBoxControl.Items.Clear();
            this._SelectEligibleParent.SearchTextBox.Text = string.Empty;

            // clear the search
            if (this._ResourceObject != null)
            { this._EligibleResourceParentForSelectList = Resource.IdsAndNamesForResourceParentSelectList(this._ResourceObject.IdResource, null); }
            else
            { this._EligibleResourceParentForSelectList = Resource.IdsAndNamesForResourceParentSelectList(0, null); }

            _SelectEligibleParentDataBind();
        }
        #endregion

        #region _SelectEligibleParentDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectEligibleParentDataBind()
        {
            this._SelectEligibleParent.ListBoxControl.DataSource = this._EligibleResourceParentForSelectList;
            this._SelectEligibleParent.ListBoxControl.DataTextField = "name";
            this._SelectEligibleParent.ListBoxControl.DataValueField = "idResource";
            this._SelectEligibleParent.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._SelectEligibleParent.ListBoxControl.Items.Count == 0)
            {
                this._SelectParentForResource.SubmitButton.Enabled = false;
                this._SelectParentForResource.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectParentForResource.SubmitButton.Enabled = true;
                this._SelectParentForResource.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _BuildSelectOwnerModal
        /// <summary>
        /// Builds the modal for selecting owner to add to the resource.
        /// </summary>
        private void _BuildSelectOwnerModal(string targetControlId)
        {
            // set modal properties
            this._SelectOwnerForResource = new ModalPopup("SelectOwnersForResourceModal");
            this._SelectOwnerForResource.Type = ModalPopupType.Form;
            this._SelectOwnerForResource.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR,
                                                                                  ImageFiles.EXT_PNG);
            this._SelectOwnerForResource.HeaderIconAlt = _GlobalResources.SelectOwner;
            this._SelectOwnerForResource.HeaderText = _GlobalResources.SelectOwner;
            this._SelectOwnerForResource.TargetControlID = targetControlId;
            this._SelectOwnerForResource.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectOwnerForResource.SubmitButtonCustomText = _GlobalResources.AddOwner;
            this._SelectOwnerForResource.SubmitButton.OnClientClick = "javascript:AddOwnersToResourceOwnerListContainer(); return false;";
            this._SelectOwnerForResource.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectOwnerForResource.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectOwnerForResource.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleOwnerUsers = new DynamicListBox("SelectEligibleUsers");
            this._SelectEligibleOwnerUsers.NoRecordsFoundMessage = _GlobalResources.NoUsersFound;
            this._SelectEligibleOwnerUsers.IncludeSelectAllNone = false;
            this._SelectEligibleOwnerUsers.IsMultipleSelect = false;
            this._SelectEligibleOwnerUsers.SearchButton.Command += new CommandEventHandler(this._SearchSelectOwnerButton_Command);
            this._SelectEligibleOwnerUsers.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectOwnerButton_Command);
            this._SelectEligibleOwnerUsers.ListBoxControl.DataSource = this._EligibleResourceOwnersForSelectList;
            this._SelectEligibleOwnerUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleOwnerUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleOwnerUsers.ListBoxControl.DataBind();

            // add controls to body
            this._SelectOwnerForResource.AddControlToBody(this._SelectEligibleOwnerUsers);

            // add modal to container
            this.PropertiesContainer.Controls.Add(this._SelectOwnerForResource);
        }
        #endregion

        #region _SearchSelectOwnerButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Owner" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectOwnerButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectOwnerForResource.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleOwnerUsers.ListBoxControl.Items.Clear();

            // do the search
            if (this._ResourceObject != null)
            { this._EligibleResourceOwnersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForResourceOwnersSelectList(this._ResourceObject.IdResource, this._SelectEligibleOwnerUsers.SearchTextBox.Text); }
            else
            { this._EligibleResourceOwnersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForResourceOwnersSelectList(0, this._SelectEligibleOwnerUsers.SearchTextBox.Text); }

            _SelectEligibleOwnerUsersDataBind();
        }
        #endregion

        #region _ClearSearchSelectOwnerButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Owner" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectOwnerButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectOwnerForResource.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleOwnerUsers.ListBoxControl.Items.Clear();
            this._SelectEligibleOwnerUsers.SearchTextBox.Text = "";

            // clear the search
            if (this._ResourceObject != null)
            { this._EligibleResourceOwnersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForResourceOwnersSelectList(this._ResourceObject.IdResource, null); }
            else
            { this._EligibleResourceOwnersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForResourceOwnersSelectList(0, null); }

            _SelectEligibleOwnerUsersDataBind();
        }
        #endregion

        #region _SelectEligibleOwnerUsersDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectEligibleOwnerUsersDataBind()
        {
            this._SelectEligibleOwnerUsers.ListBoxControl.DataSource = this._EligibleResourceOwnersForSelectList;
            this._SelectEligibleOwnerUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleOwnerUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleOwnerUsers.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._SelectEligibleOwnerUsers.ListBoxControl.Items.Count == 0)
            {
                this._SelectOwnerForResource.SubmitButton.Enabled = false;
                this._SelectOwnerForResource.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectOwnerForResource.SubmitButton.Enabled = true;
                this._SelectOwnerForResource.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // if there is no resource object, create one
                if (this._ResourceObject == null)
                { this._ResourceObject = new Resource(); }

                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                // populate the object
                this._ResourceObject.Name = this._ResourceName.Text;
                this._ResourceObject.Description = this._ResourceDescription.Text;
                this._ResourceObject.IdResourceType = Convert.ToInt32(this._SelectResourcesType.SelectedItem.Value);
                this._ResourceObject.Identifier = this._ResourceIdentifier.Text;
                this._ResourceObject.IsMovable = Convert.ToBoolean(this._ResourceMovable.SelectedValue);
                this._ResourceObject.IsAvailable = Convert.ToBoolean(this._ResourceAvailable.SelectedValue);

                if (!String.IsNullOrWhiteSpace(this._ResourceCapacity.Text))
                { this._ResourceObject.Capacity = Convert.ToInt32(this._ResourceCapacity.Text); }
                else
                { this._ResourceObject.Capacity = null; }

                this._ResourceObject.LocationRoom = this._ResourceRoom.Text;
                this._ResourceObject.LocationBuilding = this._ResourceBuilding.Text;
                this._ResourceObject.LocationCity = this._ResourceCity.Text;
                this._ResourceObject.LocationProvince = this._ResourceProvince.Text;
                this._ResourceObject.LocationCountry = this._ResourceCountry.Text;

                // Do Resource Parent
                // do Resource Owner
                if (!String.IsNullOrWhiteSpace(this._SelectedResourceParent.Value))
                {
                    this._ResourceObject.IdParentResource = Convert.ToInt32(this._SelectedResourceParent.Value);
                }
                if (this._ResourceOwnerType.SelectedValue == "user")
                {
                    this._ResourceObject.OwnerWithinSystem = true;
                    // do Resource Owner
                    if (!String.IsNullOrWhiteSpace(this._SelectedResourceOwner.Value))
                    {
                        this._ResourceObject.IdOwner = Convert.ToInt32(this._SelectedResourceOwner.Value);
                    }
                }
                else if (this._ResourceOwnerType.SelectedValue == "define")
                {
                    this._ResourceObject.OwnerWithinSystem = false;
                    this._ResourceObject.OwnerName = this._ResourceOwnerName.Text;
                    this._ResourceObject.OwnerEmail = this._ResourceOwnerEmail.Text;
                    this._ResourceObject.OwnerContactInfo = HttpUtility.HtmlDecode(this._ResourceOwnerContactInfo.Text);
                }

                // save the resource, save its returned id to viewstate, and 
                // instansiate a new resource object with the id
                int id;
                id = this._ResourceObject.Save();
                this.ViewState["id"] = id;
                this._ResourceObject.IdResource = id;

                // do resource language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string resourceName = null;
                        string resourceDescription = null;

                        // get text boxes
                        TextBox languageSpecificResourceNameTextBox = (TextBox)this.PropertiesContainer.FindControl(this._ResourceName.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificResourceDescriptionTextBox = (TextBox)this.PropertiesContainer.FindControl(this._ResourceDescription.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificResourceNameTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificResourceNameTextBox.Text))
                            { resourceName = languageSpecificResourceNameTextBox.Text; }
                        }

                        if (languageSpecificResourceDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificResourceDescriptionTextBox.Text))
                            { resourceDescription = languageSpecificResourceDescriptionTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(resourceName) ||
                            !String.IsNullOrWhiteSpace(resourceDescription))
                        {
                            this._ResourceObject.SaveLang(cultureInfo.Name,
                                                          resourceName,
                                                          resourceDescription);
                        }
                    }
                }

                // load the saved resource object
                this._ResourceObject = new Resource(id);

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.PropertiesFeedbackContainer, _GlobalResources.ResourceHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            if (this._ResourceObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // resource name, description
                bool isDefaultPopulated = false;

                foreach (Resource.LanguageSpecificProperty resourceLanguageSpecificProperty in this._ResourceObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (resourceLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._ResourceName.Text = resourceLanguageSpecificProperty.Name;
                        this._ResourceDescription.Text = resourceLanguageSpecificProperty.Description;
                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificCourseTitleTextBox = (TextBox)this.PropertiesContainer.FindControl(this._ResourceName.ID + "_" + resourceLanguageSpecificProperty.LangString);
                        TextBox languageSpecificCourseShortDescriptionTextBox = (TextBox)this.PropertiesContainer.FindControl(this._ResourceDescription.ID + "_" + resourceLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificCourseTitleTextBox != null)
                        { languageSpecificCourseTitleTextBox.Text = resourceLanguageSpecificProperty.Name; }

                        if (languageSpecificCourseShortDescriptionTextBox != null)
                        { languageSpecificCourseShortDescriptionTextBox.Text = resourceLanguageSpecificProperty.Description; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._ResourceName.Text = this._ResourceObject.Name;
                    this._ResourceDescription.Text = this._ResourceObject.Description;
                }

                // NON-LANGUAGE SPECIFIC PROPERTIES
                this._SelectResourcesType.SelectedValue = this._ResourceObject.IdResourceType.ToString();
                this._ResourceIdentifier.Text = this._ResourceObject.Identifier;
                this._ResourceMovable.SelectedValue = this._ResourceObject.IsMovable.ToString();
                this._ResourceAvailable.SelectedValue = this._ResourceObject.IsAvailable.ToString();
                this._ResourceCapacity.Text = Convert.ToString(this._ResourceObject.Capacity);
                this._ResourceRoom.Text = this._ResourceObject.LocationRoom;
                this._ResourceBuilding.Text = this._ResourceObject.LocationBuilding;
                this._ResourceCity.Text = this._ResourceObject.LocationCity;
                this._ResourceProvince.Text = this._ResourceObject.LocationProvince;
                this._ResourceCountry.Text = this._ResourceObject.LocationCountry;
                ScriptManager.RegisterStartupScript(this, GetType(), "ToggleOwnerTypePanel", "ToggleOwnerTypePanel();", true);

                /* RESOURCE OWNER */

                if (this._ResourceObject.OwnerWithinSystem && this._ResourceObject.IdOwner > 0)
                {
                    this._ResourceOwnerType.SelectedValue = "user";

                    // loop through the datatable and add each member to the listing container
                    this._OwnerNameContainer.Controls.Clear();
                    this._OwnerNameContainer.ID = "Owner_" + Convert.ToInt32(this._ResourceObject.IdOwner);

                    // owner name
                    Literal userName = new Literal();
                    UMS.Library.User user = new UMS.Library.User((int)this._ResourceObject.IdOwner);
                    userName.Text = user.DisplayName;

                    // add controls to container
                    this._OwnerNameContainer.Controls.Add(userName);
                    ResourceOwnersListContainer.Controls.Add(this._OwnerNameContainer);
                }
                else
                {
                    this._ResourceOwnerType.SelectedValue = "define";
                    this._ResourceOwnerName.Text = this._ResourceObject.OwnerName;
                    this._ResourceOwnerEmail.Text = this._ResourceObject.OwnerEmail;
                    this._ResourceOwnerContactInfo.Text = HttpUtility.HtmlDecode(this._ResourceObject.OwnerContactInfo);
                }

                /* RESOURCE Parent */
                if (this._ResourceObject.IdParentResource > 0)
                {
                    // container
                    Panel parentNameContainer = new Panel();
                    parentNameContainer.ID = "Parent_" + this._ResourceObject.IdParentResource.ToString();

                    // remove parent resource button
                    Image removeParentImage = new Image();
                    removeParentImage.ID = "Parent_" + this._ResourceObject.IdParentResource.ToString() + "_RemoveImage";
                    removeParentImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                        ImageFiles.EXT_PNG);
                    removeParentImage.CssClass = "SmallIcon";
                    removeParentImage.Attributes.Add("onClick", "javascript:RemoveParentFromResource('" + this._ResourceObject.IdParentResource.ToString() + "');");
                    removeParentImage.Style.Add("cursor", "pointer");

                    // resource name
                    Literal userName = new Literal();
                    Library.Resource resource = new Library.Resource((int)this._ResourceObject.IdParentResource);
                    userName.Text = resource.Name;

                    // add controls to container
                    parentNameContainer.Controls.Add(removeParentImage);
                    parentNameContainer.Controls.Add(userName);
                    this._ResourceParentListContainer.Controls.Add(parentNameContainer);
                }
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;
            bool ownerTabHasErrors = false;

            // name field
            if (String.IsNullOrWhiteSpace(this._ResourceName.Text))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "Name", _GlobalResources.Name + " " + _GlobalResources.IsRequired);
            }

            // resource type field
            if (this._SelectResourcesType.SelectedIndex == 0)
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "ResourceType", _GlobalResources.ResourceType + " " + _GlobalResources.IsRequired);
            }

            if (!String.IsNullOrWhiteSpace(this._ResourceCapacity.Text))
            {
                int capacity;
                if (int.TryParse(this._ResourceCapacity.Text, out capacity) && Convert.ToInt32(this._ResourceCapacity.Text) > 0)
                {
                    isValid = true;
                }
                else
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "Capacity", _GlobalResources.Capacity + " " + _GlobalResources.IsInvalid);
                }
            }

            if (this._ResourceOwnerType.SelectedValue == "define")
            {
                if (String.IsNullOrWhiteSpace(this._ResourceOwnerName.Text))
                {
                    ownerTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "OwnerName", _GlobalResources.OwnerName + " " + _GlobalResources.IsRequired);
                }
                if (String.IsNullOrWhiteSpace(this._ResourceOwnerEmail.Text))
                {
                    ownerTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "OwnerEmail", _GlobalResources.OwnerEmail + " " + _GlobalResources.IsRequired);
                }
                if (String.IsNullOrWhiteSpace(this._ResourceOwnerContactInfo.Text))
                {
                    ownerTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "OwnerContactInfo", _GlobalResources.OwnerContact + " " + _GlobalResources.IsRequired);
                }
            }
            else if (this._ResourceOwnerType.SelectedValue == "user" && (String.IsNullOrWhiteSpace(this._SelectedResourceOwner.Value) || Convert.ToInt32(this._SelectedResourceOwner.Value) == 0))
            {
                ownerTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "ResourceOwnerType", _GlobalResources.PleaseSelectAnOwnerForThisResource);
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.PropertiesContainer, "ResourceProperties_Properties_TabLI"); }

            if (ownerTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.PropertiesContainer, "ResourceProperties_Owners_TabLI"); }

            // if this is invalid, we need to pre-populate multi-select fields with the objects that were selected
            if (!isValid)
            {
                /*SELECTED PARENT RESOURCE */
                if (!String.IsNullOrWhiteSpace(this._SelectedResourceParent.Value) && Convert.ToInt32(this._SelectedResourceParent.Value) > 0)
                {
                    Library.Resource resource = new Library.Resource(Convert.ToInt32(this._SelectedResourceParent.Value));

                    // container
                    Panel parentNameContainer = new Panel();
                    parentNameContainer.ID = "Parent_" + resource.IdResource.ToString();

                    // remove parent resource button
                    Image removeParentImage = new Image();
                    removeParentImage.ID = "Parent_" + resource.IdResource.ToString() + "_RemoveImage";
                    removeParentImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                        ImageFiles.EXT_PNG);
                    removeParentImage.CssClass = "SmallIcon";
                    removeParentImage.Attributes.Add("onClick", "javascript:RemoveParentFromResource('" + resource.IdResource.ToString() + "');");
                    removeParentImage.Style.Add("cursor", "pointer");

                    // resource name
                    Literal userName = new Literal();
                    userName.Text = resource.Name;

                    // add controls to container
                    parentNameContainer.Controls.Add(removeParentImage);
                    parentNameContainer.Controls.Add(userName);
                    this._ResourceParentListContainer.Controls.Add(parentNameContainer);
                }

                if (this._ResourceOwnerType.SelectedValue == "user" && !String.IsNullOrWhiteSpace(this._SelectedResourceOwner.Value) && Convert.ToInt32(this._SelectedResourceOwner.Value) > 0)
                {
                    this._OwnerNameContainer.Controls.Clear();
                    this._OwnerNameContainer.ID = "Owner_" + Convert.ToInt32(this._SelectedResourceOwner.Value);

                    // owner name
                    Literal userName = new Literal();
                    UMS.Library.User user = new UMS.Library.User(Convert.ToInt32(this._SelectedResourceOwner.Value));
                    userName.Text = user.DisplayName;

                    // add controls to container
                    this._OwnerNameContainer.Controls.Add(userName);
                    this.ResourceOwnersListContainer.Controls.Clear();
                    this.ResourceOwnersListContainer.Controls.Add(this._OwnerNameContainer);
                }

            }

            return isValid;
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/ResourceManagement/Default.aspx");
        }
        #endregion
    }
}
