﻿//Save scroll position into hidden field on window scrolling
$(window).scroll(function () {
    $('#ScrollPositionHidden').val($(window).scrollTop());
});

//Set window scroll position
function pageLoad() {
    var scrollTop = $('#ScrollPositionHidden').val();
    $(window).scrollTop(scrollTop);
}

function ShowContextMenu(contextMenuTriggerId) {
    // get the id of the context menu
    var contextMenuId = contextMenuTriggerId.replace("Trigger", "ItemContainer");

    // show the context menu
    $("#" + contextMenuId).show();

    // set the "on hover out" action
    $("#" + contextMenuId).hover(function () { }, function () {
        $(this).fadeOut(200);
    });
}

//Handles schedule update process
function OnScheduleUpdate(idResourceToObject) {
    //set idResourceToObject into hidden field
    $("#IdResourceToObjectHidden").val(idResourceToObject);

    //click the update hidden button to show update modal
    document.getElementById("ScheduleUpdateHiddenLink").click();

    //click the hidden button to fill the controls on update modal
    document.getElementById("ScheduleUpdateModalButton").click();
}

//Handles schedule delete process
function OnScheduleDelete(idResourceToObject) {
    //set idResourceToObject into hidden field
    $("#IdResourceToObjectHidden").val(idResourceToObject);

    //click the delete hidden button to show delete modal
    document.getElementById("ScheduleDeleteHiddenLink").click();
}

function OnResourceObjectIconClick(idObject,idResourceToObject) {
    //set idResourceToObject into hidden field
    $("#IdObjectHidden").val(idObject);
    $("#IdResourceToObjectHidden").val(idResourceToObject);
    
    //click the update hidden button to show update modal
    document.getElementById("ResourceObjectHiddenLink").click();

    //click the hidden button to fill the controls on update modal
    document.getElementById("ResourceObjectModalButton").click();

}
