﻿using System;
using System.Text;
using Asentia.Common;
using Asentia.Controls;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Globalization;

namespace Asentia.LMS.Pages.Administrator.ResourceManagement
{
    /// <summary>
    /// Resource Type listing page
    /// </summary>
    public class ResourceType : AsentiaAuthenticatedPage
    {
        #region Public Properties
        public Panel ResourceTypeFormContentWrapperContainer;
        public Panel ResourceTypeOptionsPanel;
        public UpdatePanel ResourceTypeUpdatePanel;
        public Grid ResourceTypeGrid;
        public Panel ActionsPanel;
        public Panel ModalPropertiesContainer = new Panel();
        #endregion

        #region Private Properties
        private LinkButton _ResourceTypeDeleteButton = new LinkButton();
        private LinkButton _ResourceTypeHiddenLink = new LinkButton();

        private ModalPopup _ResourceTypeGridConfirmAction;
        private ModalPopup _AddResourceTypeModal;

        private TextBox _ResourceTypeName;
        private Button _SaveResourceTypeModalButton;
        private HiddenField _IdResourceTypeHidden = new HiddenField();
        private Library.ResourceType _ResourceTypeObject;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(ResourceType), "Asentia.LMS.Pages.Administrator.ResourceManagement.ResourceType.js");

            StringBuilder globalJS = new StringBuilder();
            globalJS.AppendLine("ModiFyHeaderText = \"" + _GlobalResources.ModifyResourceType + "\";");
            globalJS.AppendLine("ModiFyModalSubmitButtonText = \"" + _GlobalResources.SaveChanges + "\";");
            globalJS.AppendLine("AddNewHeaderText = \"" + _GlobalResources.NewResourceType + "\";");
            globalJS.AppendLine("AddModalSubmitButtonText = \"" + _GlobalResources.CreateResourceType + "\";");
            globalJS.AppendLine("SaveResourceTypeModalButton = \"" + this._SaveResourceTypeModalButton.ID + "\";");
            globalJS.AppendLine("ResourceTypeHiddenLink = \"" + this._ResourceTypeHiddenLink.ID + "\";");
            globalJS.AppendLine("IdResourceTypeHidden = \"" + this._IdResourceTypeHidden.ID + "\";");
            globalJS.AppendLine("ResourceTypeModal = \"" + this._AddResourceTypeModal.ID + "\";");
            csm.RegisterClientScriptBlock(typeof(Default), "GlobalJS", globalJS.ToString(), true);

        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_ResourceManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/resourcemanagement/ResourceType.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ResourceManagement, "/administrator/ResourceManagement/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ManageResourceTypes));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, _GlobalResources.ManageResourceTypes, ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG,
                                                                                                                                  ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.ResourceTypeFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the resource type grid, actions panel, and modal
            this._BuildResourceTypeOptionsPanel();
            this._BuildResourceTypeGrid();
            this._BuildResourceTypeActionsPanel();
            this._BuildResourceTypeGridActionsModal();
            this._ResourceTypeActionModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data Resource Type grid
                this.ResourceTypeGrid.BindData();
            }
        }

        #endregion

        #region _BuildResourceTypeGridActionsModal
        /// <summary>
        /// Builds resource type grid action modal
        /// </summary>
        private void _BuildResourceTypeGridActionsModal()
        {
            this._ResourceTypeGridConfirmAction = new ModalPopup("ResourceTypeGridConfirmAction");

            // set modal properties
            this._ResourceTypeGridConfirmAction.Type = ModalPopupType.Confirm;
            this._ResourceTypeGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                        ImageFiles.EXT_PNG);
            this._ResourceTypeGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._ResourceTypeGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedResourceType_s;
            this._ResourceTypeGridConfirmAction.TargetControlID = this._ResourceTypeDeleteButton.ClientID;
            this._ResourceTypeGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._ResourceTypeDeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ResourceTypeGridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseResourceType_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._ResourceTypeGridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._ResourceTypeGridConfirmAction);
        }
        #endregion

        #region _BuildResourceTypeActionsPanel
        /// <summary>
        /// Builds resource type actions panel
        /// </summary>
        private void _BuildResourceTypeActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._ResourceTypeDeleteButton.ID = "ResourceTypeGridDeleteButton";
            this._ResourceTypeDeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "ResourceTypeGridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._ResourceTypeDeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedResourceType_s;
            this._ResourceTypeDeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._ResourceTypeDeleteButton);
        }
        #endregion

        #region _BuildResourceTypeGrid
        /// <summary>
        /// Builds the Resource Grid for the page.
        /// </summary>
        private void _BuildResourceTypeGrid()
        {
            this.ResourceTypeUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.ResourceTypeGrid.StoredProcedure = Library.ResourceType.GridProcedure;
            this.ResourceTypeGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.ResourceTypeGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.ResourceTypeGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.ResourceTypeGrid.IdentifierField = "idResourceType";
            this.ResourceTypeGrid.DefaultSortColumn = "ResourceType";

            // data key names
            this.ResourceTypeGrid.DataKeyNames = new string[] { "idResourceType" };

            // columns
            GridColumn ResourceType = new GridColumn(_GlobalResources.Name, "ResourceType", "ResourceType");

            GridColumn modify = new GridColumn(_GlobalResources.Modify, "isModifyOn", true);

            modify.AddProperty(new GridColumnProperty("True", "<a class=\"CursorPointer\" href=\"javascript:void(0);\" onclick=\"OnModifyResourceType('##idResourceType##');\" >"
                                                             + "<img class=\"SmallIcon\" src=\""
                                                             + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                      ImageFiles.EXT_PNG)
                                                             + "\" alt=\"" + _GlobalResources.Modify + "\" />"
                                                             + "</a>"));
            modify.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.ModifyDisabled + "\" />"));

            // add columns to data grid
            this.ResourceTypeGrid.AddColumn(ResourceType);
            this.ResourceTypeGrid.AddColumn(modify);
        }

        #endregion

        #region _BuildResourceTypeOptionsPanel
        /// <summary>
        /// Builds resource type options panel
        /// </summary>
        private void _BuildResourceTypeOptionsPanel()
        {
            this.ResourceTypeOptionsPanel.Controls.Clear();

            this.ResourceTypeOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD RESOURCE TYPE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddResourceTypeLink",
                                                null,
                                                "javascript: void(0);",
                                                "OnModifyResourceType(0);",
                                                _GlobalResources.NewResourceType,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_RESOURCE_TYPE, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ResourceTypeOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }

        #endregion

        #region _ResourceTypeDeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _ResourceTypeDeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.ResourceTypeGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.ResourceTypeGrid.Rows[i].FindControl(this.ResourceTypeGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.ResourceType.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedback(_GlobalResources.TheSelectedResourceType_sHaveBeenDeletedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.ResourceTypeGrid.BindData();
            }
        }
        #endregion

        #region _ResourceTypeActionModal
        /// <summary>
        ///Builds resource type action modal
        /// </summary>
        private void _ResourceTypeActionModal()
        {
            //hidden button to simulate the action on click of update 
            this._ResourceTypeHiddenLink.ClientIDMode = ClientIDMode.Static;
            this._ResourceTypeHiddenLink.ID = "ResourceTypeHiddenLink";
            this._ResourceTypeHiddenLink.Text = _GlobalResources.CreateModifyResourceType;
            this._ResourceTypeHiddenLink.Style.Add("display", "none");

            this.ResourceTypeOptionsPanel.Controls.Add(this._ResourceTypeHiddenLink);

            this._BuildAddResourceTypeModal(this._ResourceTypeHiddenLink.ID);
        }
        #endregion

        #region _BuildAddResourceTypeModal
        /// <summary>
        /// Builds the modal for creating/modifying a resporce type.
        /// </summary>
        private void _BuildAddResourceTypeModal(string targetControlId)
        {
            // set modal properties
            this._AddResourceTypeModal = new ModalPopup("ResourceTypeModal");
            this._AddResourceTypeModal.Type = ModalPopupType.Form;
            this._AddResourceTypeModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG,
                                                                               ImageFiles.EXT_PNG);
            this._AddResourceTypeModal.HeaderIconAlt = _GlobalResources.ModifyResourceType;
            this._AddResourceTypeModal.HeaderText = _GlobalResources.ModifyResourceType;
            this._AddResourceTypeModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._AddResourceTypeModal.SubmitButtonCustomText = _GlobalResources.SaveChanges;

            //adding hidden button for saving resource type          
            this._SaveResourceTypeModalButton = new Button();
            this._SaveResourceTypeModalButton.ID = "SaveResourceTypeModalButton";
            this._SaveResourceTypeModalButton.Text = _GlobalResources.AddUpdate;
            this._SaveResourceTypeModalButton.Command += new CommandEventHandler(this._ResourceTypeFetchingModalButton_Command);
            this._SaveResourceTypeModalButton.Style.Add("display", "none");

            this._AddResourceTypeModal.TargetControlID = targetControlId;
            this._AddResourceTypeModal.SubmitButton.Command += new CommandEventHandler(this._AddandModifyResourceTypeModalSubmit_Command);
            this._AddResourceTypeModal.CloseButtonTextType = ModalPopupButtonText.Close;

            // build the modal body
            // course title field
            ModalPropertiesContainer = new Panel();
            ModalPropertiesContainer.ID = "ModalProperties_Container";

            //adding hidden field to use for modify process
            this._IdResourceTypeHidden = new HiddenField();
            this._IdResourceTypeHidden.ClientIDMode = ClientIDMode.Static;
            this._IdResourceTypeHidden.ID = "IdResourceTypeHidden";
            ModalPropertiesContainer.Controls.Add(this._IdResourceTypeHidden);

            this._ResourceTypeName = new TextBox();
            this._ResourceTypeName.ID = "Name_Field";
            this._ResourceTypeName.CssClass = "InputLong";

            //add control to panel
            ModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Name",
                                                             _GlobalResources.Name,
                                                             this._ResourceTypeName.ID,
                                                             this._ResourceTypeName,
                                                             true,
                                                             true,
                                                             true));

            //add controls to modal
            ModalPropertiesContainer.Controls.Add(this._SaveResourceTypeModalButton);
            this._AddResourceTypeModal.AddControlToBody(ModalPropertiesContainer);

            //// add modal to container
            this.PageContentContainer.Controls.Add(this._AddResourceTypeModal);
        }
        #endregion

        #region _ResourceTypeFetchingModalButton_Command
        /// <summary>
        /// Handles the event initiated by the hidden Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _ResourceTypeFetchingModalButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                this._AddResourceTypeModal.ClearFeedback();

                //fills the modal pop up controls on update of any resource type
                //get resourceType object with id
                if (!String.IsNullOrWhiteSpace(this._IdResourceTypeHidden.Value) && Convert.ToInt32(this._IdResourceTypeHidden.Value) > 0)
                {
                    this._ResourceTypeObject = new Library.ResourceType(Convert.ToInt32(this._IdResourceTypeHidden.Value));

                    //bind controls
                    // if the language is the default language, populate the control directly attached to this page,
                    //otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    foreach (Library.ResourceType.LanguageSpecificProperty resourceTypeLanguageSpecificProperty in this._ResourceTypeObject.LanguageSpecificProperties)
                    {
                        if (resourceTypeLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                        { this._ResourceTypeName.Text = resourceTypeLanguageSpecificProperty.Name; }

                        else
                        {
                            TextBox languageSpecificResourceTypeNameTextBox = (TextBox)this.ModalPropertiesContainer.FindControl(this._ResourceTypeName.ID + "_" + resourceTypeLanguageSpecificProperty.LangString);

                            // if the text boxes were found, set the text box values to the language-specific value
                            if (languageSpecificResourceTypeNameTextBox != null)
                            { languageSpecificResourceTypeNameTextBox.Text = resourceTypeLanguageSpecificProperty.Name; }
                        }
                    }
                }
                else
                {
                    this._ResourceTypeName.Text = string.Empty;
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddResourceTypeModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dnfEx)
            {
                // display the failure message
                this._AddResourceTypeModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddResourceTypeModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddResourceTypeModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException aEx)
            {
                // display the failure message
                this._AddResourceTypeModal.DisplayFeedback(aEx.Message, true);
            }
        }
        #endregion

        #region _AddandModifyResourceTypeModalSubmit_Command
        /// <summary>
        ///Handels "Add and Modify" resource type madal pop up submit click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AddandModifyResourceTypeModalSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                #region Validation

                // validate the resource type modal fields
                if (!this._ValidateResourceTypeModalProperties())
                { throw new AsentiaException(); }
                #endregion

                if (!String.IsNullOrWhiteSpace(this._IdResourceTypeHidden.Value) && Convert.ToInt32(this._IdResourceTypeHidden.Value) > 0)
                { this._ResourceTypeObject = new Library.ResourceType(Convert.ToInt32(this._IdResourceTypeHidden.Value)); }
                else
                {
                    this._ResourceTypeObject = new Library.ResourceType();
                }

                this._ResourceTypeObject.Name = this._ResourceTypeName.Text;

                //update schedule
                this._ResourceTypeObject.IdResourceType = this._ResourceTypeObject.Save();
                this._IdResourceTypeHidden.Value = Convert.ToString(this._ResourceTypeObject.IdResourceType);

                // do course language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string resourceTypeName = null;

                        // get text boxes
                        TextBox languageSpecificCourseTitleTextBox = (TextBox)this.ModalPropertiesContainer.FindControl(this._ResourceTypeName.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificCourseTitleTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCourseTitleTextBox.Text))
                            { resourceTypeName = languageSpecificCourseTitleTextBox.Text; }
                        }
                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(resourceTypeName))
                        {
                            this._ResourceTypeObject.SaveLang(cultureInfo.Name,
                                                        resourceTypeName
                                                     );
                        }
                    }
                }

                //display success message
                this._AddResourceTypeModal.DisplayFeedback(_GlobalResources.ResourceTypeHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddResourceTypeModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._AddResourceTypeModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddResourceTypeModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddResourceTypeModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AddResourceTypeModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {
                // rebind data Resource Type grid
                this.ResourceTypeGrid.BindData();
                this.ResourceTypeUpdatePanel.Update();
            }
        }
        #endregion

        #region _ValidateResourceTypeModalProperties
        /// <summary>
        /// Validates resource type madal properties
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateResourceTypeModalProperties()
        {
            bool isValid = true;

            // name field
            if (String.IsNullOrWhiteSpace(this._ResourceTypeName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.ModalPropertiesContainer, "Name", _GlobalResources.Name + " " + _GlobalResources.IsRequired);
            }

            return isValid;
        }
        #endregion
    }
}
