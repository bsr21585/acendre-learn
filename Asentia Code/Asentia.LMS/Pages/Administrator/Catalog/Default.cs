﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.Catalog
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CatalogFormContentWrapperContainer;
        public Panel CatalogWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel ActionsPanel;
        public Panel ObjectLanguageSelectorContainer;
        public LanguageSelector LanguageSelectorControl;
        public Panel AdminCatalogContent;
        #endregion

        #region Private Properties
        private EcommerceSettings _EcommerceSettings;

        private UpdatePanel _CatalogContentUpdatePanel;
        private UpdatePanel _RightPanel;
        private PlaceHolder _LeftPanel;
        private Panel _ModalPopupPanel;
        private Panel _NewCatalogFields;

        private HiddenField _SelectedCatalog;
        private HiddenField _SelectedCourse;
        private HiddenField _ActionClicked;
        private HiddenField _SelectedCourseParent;
        private HiddenField _SelectedCatalogSection;
        private HiddenField _CatalogOrderJsonData;
        private HiddenField _CourseOrderJsonData;
        private HiddenField _LongDescriptionHiddenField;
        private HiddenField _SelectedCatalogId;
        private HiddenField _ClearAvatar;

        private ModalPopup _NewCatalogModal;
        private ModalPopup _NewCourseModal;
        private ModalPopup _DeleteCatalog;
        private ModalPopup _RemoveCourseFromCatalog;

        private TextBox _CatalogNameTextBox;
        private TextBox _IdCatalogTextBox;
        private TextBox _IdParentTextBox;
        private TextBox _LongDescriptionTextBox;
        private TextBox _Shortcode;

        private UploaderAsync _CatalogAvatar;
        private Image _AvatarImage;

        private RadioButton _PrivateRadioButton;
        private RadioButton _PublicRadioButton;
        private RadioButton _ClosedRadioButton;
        private RadioButton _NotClosedRadioButton;

        private RadioButton _FreeRadioButton;
        private RadioButton _FixedPriceRadioButton;
        private RadioButton _TotalAllCourseRadioButton;
        private RadioButton _DiscountedTotalRadioButton;
        private TextBox _CatalogCostTextBox;
        private HtmlTextArea _ShortDescriptionTextArea;
        private HtmlTextArea _SearchTagsTextArea;

        private LinkButton _NewCatalogButton;
        private LinkButton _ModifyCatalogButton;
        private LinkButton _DeleteCatalogButton;
        private LinkButton _AddCourseToCatalogButton;
        private LinkButton _RemoveCourseFromCatalogButton;
        private LinkButton _NoActionButton;
        private LinkButton _AddCoursesModaTargetButton;

        private Button _SaveButton;
        private Button _CancelButton;
        private Button _HiddenLaunchModalButton;
        private Button _PopulateCoursesHiddenButton;

        private Library.Catalog _CatalogObject;
        private Library.Catalog _ModifyCatalogObject;
        private DynamicListBox _CourseList;
        private HtmlGenericControl _CatalogHtml;

        private bool _DisableNonLanguageFields;

        private HyperLink _ShortcodeLink;
        private string _SelectedLanguage;
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseCatalog))
            { Response.Redirect("/"); }            

            //Initializing private properties
            this._IntializePrivateProperties();

            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/catalog/default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Catalog));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, _GlobalResources.Catalog, ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG,
                                                                                                                      ImageFiles.EXT_PNG));

            this.InitializeAdminMenu();

            this.CatalogFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CatalogWrapperContainer.CssClass = "FormContentContainer";

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.CreateUpdateOrModifyOrderOfCatalogsHere, true);

            //Set the default value of the flag variable
            this._DisableNonLanguageFields = true;

            // get the language querystring parameter
            this._SelectedLanguage = this.QueryStringString("lang", AsentiaSessionState.GlobalSiteObject.LanguageString);

            // DISABLE NON-LANGUAGE-SPECIFIC FORM ELEMENTS IF WE'RE NOT EDITING IN THE DEFAULT LANGUAGE
            if (this._SelectedLanguage == AsentiaSessionState.GlobalSiteObject.LanguageString)
            { this._DisableNonLanguageFields = false; }

            //build Actions panel
            this._BuildActionsPanel();

            //build the child controls
            this._BuildChildControls();

            if (!this.IsPostBack)
            {
                this._UpdateCourseCatalogs();
            }

            // build the language selector
            this._BuildLanguageSelectorContainer();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Catalog.Default.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");

            // build start up call for MCE and add to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));
            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);

            bool ecommerceOn = false;

            if (this._EcommerceSettings.IsEcommerceSet)
            { ecommerceOn = true; }

            csm.RegisterStartupScript(typeof(Default), "JsVariables", "var idCatalogTextBox='" + this._IdCatalogTextBox.ClientID
                + "';var catalogNameTextBox='" + this._CatalogNameTextBox.ClientID
                + "';var catalogShortCode='" + this._Shortcode.ClientID
                + "';var catalogShortCodeLink='" + this._ShortcodeLink.ClientID
                + "';var catalogAvatarUploader='" + this._CatalogAvatar.ClientID
                + "';var jsonData='" + this._CatalogOrderJsonData.ClientID
                + "';var courseOrderJsonData='" + this._CourseOrderJsonData.ClientID
                + "';var populateCourses='" + this._PopulateCoursesHiddenButton.ClientID
                + "';var selectedCourse='" + this._SelectedCourse.ClientID
                + "';var actionClicked='" + this._ActionClicked.ClientID
                + "';var selectedCatalog='" + this._SelectedCatalog.ClientID
                + "';var selectedCourseParent='" + this._SelectedCourseParent.ClientID
                + "';var selectedCatalogSection='" + this._SelectedCatalogSection.ClientID
                + "';var newCourseButton='" + this._AddCourseToCatalogButton.ClientID
                + "';var idParentTextBox='" + this._IdParentTextBox.ClientID
                + "';var privateRadioButton='" + this._PrivateRadioButton.ClientID
                + "';var publicRadioButton='" + this._PublicRadioButton.ClientID
                + "';var closedRadioButton='" + this._ClosedRadioButton.ClientID
                + "';var notClosedRadioButton='" + this._NotClosedRadioButton.ClientID
                + "';var shortDescriptionTextArea='" + this._ShortDescriptionTextArea.ClientID
                + "';var longDescriptionTextArea='" + this._LongDescriptionTextBox.ClientID
                + "';var longDescriptionHiddenField='" + this._LongDescriptionHiddenField.ClientID
                + "';var searchTagsTextArea='" + this._SearchTagsTextArea.ClientID
                + "';var selectedCatalogId='" + this._SelectedCatalogId.ClientID                
                + "';var modifyCatalogCaption ='" + _GlobalResources.ModifyThisCatalog
                + "';var addModifyCatalogCaption ='" + _GlobalResources.NewCatalog
                + "';var merchantSettingsOn ='" + ecommerceOn.ToString().ToLower()
                + "';var newCatalogCaption ='" + _GlobalResources.NewCatalog + "';", true);
        }
        #endregion

        #region _BuildActionLinks
        /// <summary>
        /// Builds the container and links for actions.
        /// </summary>
        private void _BuildActionLinks()
        {
            #region "New Catalog" link button
            this._NewCatalogButton.ID = "NewCatalogButton";
            this._NewCatalogButton.CssClass = "ImageLink";
            this._NewCatalogButton.Attributes.Add("onclick", "ClearCatalogTextFields();return false;");

            //Catalog icon image
            Image newCatalogImageForLink = new Image();
            newCatalogImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG,
                                                                     ImageFiles.EXT_PNG);
            //disable non lingual fields
            if (this._DisableNonLanguageFields)
            {
                newCatalogImageForLink.CssClass = "DimIcon";
                this._NewCatalogButton.Enabled = false;
                this._NewCatalogButton.Attributes.Remove("onclick");
            }

            //Link text
            Localize newCatalogTextForLink = new Localize();
            newCatalogTextForLink.Text = _GlobalResources.NewCatalog;

            //Add icon image and text to link
            this._NewCatalogButton.Controls.Add(newCatalogImageForLink);
            this._NewCatalogButton.Controls.Add(newCatalogTextForLink);
            #endregion

            #region Modify link button
            this._ModifyCatalogButton.ID = "ModifyCatalogButton";
            this._ModifyCatalogButton.Attributes.Add("class", "modifyCatalogButton ImageLink");
            //this._ModifyCatalogButton.OnClientClick = "OnModifyLinkClick();postbackElement=$(this).attr('id'); return false;";
            this._ModifyCatalogButton.Attributes.Add("onclick", "OnModifyLinkClick();postbackElement=$(this).attr('id'); return false;");

            Image modifyCatalogImageForLink = new Image();
            modifyCatalogImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                        ImageFiles.EXT_PNG);
            Localize modifyCatalogTextForLink = new Localize();
            modifyCatalogTextForLink.Text = _GlobalResources.ModifyThisCatalog;

            //Add controls
            this._ModifyCatalogButton.Controls.Add(modifyCatalogImageForLink);
            this._ModifyCatalogButton.Controls.Add(modifyCatalogTextForLink);
            #endregion

            #region Delete link button
            this._DeleteCatalogButton.ID = "DeleteCatalogButton";
            this._DeleteCatalogButton.CssClass = "ImageLink";

            Image deleteCatalogImageForLink = new Image();
            deleteCatalogImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                        ImageFiles.EXT_PNG);
            if (this._DisableNonLanguageFields)
            {
                deleteCatalogImageForLink.CssClass = "DimIcon";
                this._DeleteCatalogButton.Enabled = false;
            }

            Localize deleteCatalogTextForLink = new Localize();
            deleteCatalogTextForLink.Text = _GlobalResources.DeleteThisCatalog;

            //Add image and text to controls
            this._DeleteCatalogButton.Controls.Add(deleteCatalogImageForLink);
            this._DeleteCatalogButton.Controls.Add(deleteCatalogTextForLink);
            #endregion

            #region "Add Course" link button
            this._AddCourseToCatalogButton.ID = "NewCourseButton";
            this._AddCourseToCatalogButton.CssClass = "ImageLink";
            this._AddCourseToCatalogButton.Attributes.Add("onclick", "PopulateCoursesInCourseListing(); return false;");

            //link image
            Image addCoursesToCatalogImageForLink = new Image();
            addCoursesToCatalogImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE,
                                                                              ImageFiles.EXT_PNG);
            if (this._DisableNonLanguageFields)
            {
                addCoursesToCatalogImageForLink.CssClass = "DimIcon";
                this._AddCourseToCatalogButton.Enabled = false;
                this._AddCourseToCatalogButton.Attributes.Remove("onclick");
            }

            //link text
            Localize addCoursesToCatalogTextForLink = new Localize();
            addCoursesToCatalogTextForLink.Text = _GlobalResources.AddCourse_sToCatalog;

            //Add image and text to link
            this._AddCourseToCatalogButton.Controls.Add(addCoursesToCatalogImageForLink);
            this._AddCourseToCatalogButton.Controls.Add(addCoursesToCatalogTextForLink);
            #endregion

            #region "Remove course from catalog" link button
            this._RemoveCourseFromCatalogButton.ID = "DeleteCourseButton";
            this._RemoveCourseFromCatalogButton.Attributes.Add("class", "deleteCourseButton ImageLink");

            Image removeCoursesToCatalogImageForLink = new Image();
            removeCoursesToCatalogImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                 ImageFiles.EXT_PNG);
            Localize removeCoursesToCatalogTextForLink = new Localize();
            removeCoursesToCatalogTextForLink.Text = _GlobalResources.RemoveCourseFromCatalog;

            //Add image and text to link
            this._RemoveCourseFromCatalogButton.Controls.Add(removeCoursesToCatalogImageForLink);
            this._RemoveCourseFromCatalogButton.Controls.Add(removeCoursesToCatalogTextForLink);
            #endregion

            #region "No Action" link button
            this._NoActionButton.ID = "noActionButton";
            //this._NoActionButton.OnClientClick = "return false;";
            this._NoActionButton.Attributes.Add("onclick", "return false;");
            this._NoActionButton.Attributes.Add("class", "noActionButton ImageLink");

            Image noActionImageForLink = new Image();
            noActionImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ERROR,
                                                                   ImageFiles.EXT_PNG);
            Localize noActionTextForLink = new Localize();
            noActionTextForLink.Text = _GlobalResources.NoActionsAvailable;

            //Add image and text to link
            this._NoActionButton.Controls.Add(noActionImageForLink);
            this._NoActionButton.Controls.Add(noActionTextForLink);
            #endregion

            #region Add All buttons to the action panel
            HtmlGenericControl actionLinks = new HtmlGenericControl("div");
            actionLinks.Attributes.Add("class", "ActionLinks actionLinks-nocss");

            HtmlGenericControl rightPanelButtonsUl = new HtmlGenericControl("ul");

            HtmlGenericControl newCatalogListItem = new HtmlGenericControl("li");
            HtmlGenericControl modifyCatalogListItem = new HtmlGenericControl("li");
            HtmlGenericControl deleteCatalogListItem = new HtmlGenericControl("li");
            HtmlGenericControl newCourseListItem = new HtmlGenericControl("li");
            HtmlGenericControl deleteCourseListItem = new HtmlGenericControl("li");
            HtmlGenericControl noActionListItem = new HtmlGenericControl("li");

            newCatalogListItem.Attributes.Add("class", "rightPanelListItem");
            modifyCatalogListItem.Attributes.Add("class", "rightPanelListItem");
            deleteCatalogListItem.Attributes.Add("class", "rightPanelListItem");
            newCourseListItem.Attributes.Add("class", "rightPanelListItem");
            deleteCourseListItem.Attributes.Add("class", "rightPanelListItem");
            noActionListItem.Attributes.Add("class", "rightPanelListItem");

            newCatalogListItem.Controls.Add(this._NewCatalogButton);
            modifyCatalogListItem.Controls.Add(this._ModifyCatalogButton);
            deleteCatalogListItem.Controls.Add(this._DeleteCatalogButton);
            newCourseListItem.Controls.Add(this._AddCourseToCatalogButton);
            deleteCourseListItem.Controls.Add(this._RemoveCourseFromCatalogButton);
            noActionListItem.Controls.Add(this._NoActionButton);
            noActionListItem.Controls.Add(this._AddCoursesModaTargetButton);

            rightPanelButtonsUl.Controls.Add(newCatalogListItem);
            rightPanelButtonsUl.Controls.Add(modifyCatalogListItem);
            rightPanelButtonsUl.Controls.Add(deleteCatalogListItem);
            rightPanelButtonsUl.Controls.Add(newCourseListItem);
            rightPanelButtonsUl.Controls.Add(deleteCourseListItem);
            rightPanelButtonsUl.Controls.Add(noActionListItem);
            actionLinks.Controls.Add(rightPanelButtonsUl);
            #endregion

            //Add action panel to the right panel
            this._RightPanel.ContentTemplateContainer.Controls.Add(actionLinks);
            
        }
        #endregion

        #region Initialize Private Properties
        /// <summary>
        /// Initializes the private properties
        /// </summary>
        private void _IntializePrivateProperties()
        {
            this._CatalogContentUpdatePanel = new UpdatePanel();
            this._RightPanel = new UpdatePanel();
            this._LeftPanel = new PlaceHolder();
            this._ModalPopupPanel = new Panel();

            this._SelectedCatalog = new HiddenField();
            this._SelectedCourse = new HiddenField();
            this._ActionClicked = new HiddenField();            
            this._PopulateCoursesHiddenButton = new Button();
            this._SelectedCourseParent = new HiddenField();
            this._SelectedCatalogSection = new HiddenField();
            this._CatalogOrderJsonData = new HiddenField();
            this._CourseOrderJsonData = new HiddenField();
            this._LongDescriptionHiddenField = new HiddenField();
            this._SelectedCatalogId = new HiddenField();
            
            this._CatalogNameTextBox = new TextBox();
            this._IdCatalogTextBox = new TextBox();
            this._IdParentTextBox = new TextBox();
            this._Shortcode = new TextBox();

            this._PrivateRadioButton = new RadioButton();
            this._PublicRadioButton = new RadioButton();
            this._ClosedRadioButton = new RadioButton();
            this._NotClosedRadioButton = new RadioButton();
            this._ShortDescriptionTextArea = new HtmlTextArea();
            this._SearchTagsTextArea = new HtmlTextArea();

            this._CatalogHtml = new HtmlGenericControl("div");

            this._NewCatalogButton = new LinkButton();
            this._ModifyCatalogButton = new LinkButton();
            this._DeleteCatalogButton = new LinkButton();
            this._AddCourseToCatalogButton = new LinkButton();
            this._RemoveCourseFromCatalogButton = new LinkButton();
            this._NoActionButton = new LinkButton();
            this._AddCoursesModaTargetButton = new LinkButton();

            this._CatalogObject = new Library.Catalog();
        }
        #endregion

        #region BuildChildControls
        /// <summary>
        /// Builds the child controls
        /// </summary>        
        /// <returns></returns>
        private void _BuildChildControls()
        {
            //build the cataol panel and catalog details on the page
            this.AdminCatalogContent.ID = "CatalogTreePanel";

            //left panel
            this._LeftPanel.ID = "leftPanel";

            //right panel for catalog details
            this._RightPanel.ID = "rightPanel";
            this._RightPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            //build action links
            this._BuildActionLinks();

            this._AddCoursesModaTargetButton.ID = "populateCoursesButton";
            this._AddCoursesModaTargetButton.Attributes.Add("style", "display:none;");

            //_SelectedCatalog hiddenfield 
            this._SelectedCatalog.ID = "selectedCatalog";
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._SelectedCatalog);

            //_SelectedCourse hiddenfield 
            this._SelectedCourse.ID = "selectedCourse";
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._SelectedCourse);

            //_SelectedCourse hiddenfield 
            this._ActionClicked.ID = "actionClicked";
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._ActionClicked);

            //_SelectedCourseParent hiddenfield 
            this._SelectedCourseParent.ID = "selectedCourseParent";
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._SelectedCourseParent);

            //_SelectedCatalogSection hiddenfield 
            this._SelectedCatalogSection.ID = "selectedCatalogSection";
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._SelectedCatalogSection);

            //_LongDescriptionHiddenField hiddenfield 
            this._LongDescriptionHiddenField.ID = "longDescriptionHiddenField";
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._LongDescriptionHiddenField);

            //_SelectedCatalogId hiddenfield that holds the value of catalog id on which action is performed
            this._SelectedCatalogId.ID = "SelectedCatalogId";
            this._RightPanel.ContentTemplateContainer.Controls.Add(this._SelectedCatalogId);
            
            //_CatalogOrderJsonData hiddenfield to hold the catalog items order 
            this._CatalogOrderJsonData.ID = "jsonData";
            this._CourseOrderJsonData.ID = "courseOrderJsonData";

            //Modal popup Panel
            this._ModalPopupPanel.ID = "modalPopupPanel";

            this._CatalogHtml.ID = "CatalogTree";
            this._CatalogHtml.Attributes.Add("class", "TreeView");
            this._LeftPanel.Controls.Add(this._CatalogHtml);

            //add panels to main panel
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._LeftPanel);
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._SelectedCatalog);
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._CatalogOrderJsonData);
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._CourseOrderJsonData);
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._SelectedCourse);
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._SelectedCourseParent);
            this._CatalogContentUpdatePanel.ContentTemplateContainer.Controls.Add(this._SelectedCatalogSection);


            this._CatalogContentUpdatePanel.ID = "CatalogContentUpdatePanel";
            this._CatalogContentUpdatePanel.Attributes.Add("class", "CatalogContentUpdatePanel");
            this._CatalogContentUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            this._RightPanel.ContentTemplateContainer.Controls.Add(this._ModalPopupPanel);

            this.AdminCatalogContent.Controls.Add(this.PageFeedbackContainer);
            this.AdminCatalogContent.Controls.Add(this._CatalogContentUpdatePanel);
            this.AdminCatalogContent.Controls.Add(this._RightPanel);

            //Attach all modal popups to the page 
            this._BuildNewCatalogModal();
            this._BuildAddCourseToCatalogModal();
            this._BuildDeleteCatalogModal(this._DeleteCatalogButton.ID);
            this._BuildRemoveCourseFromCatalogModal(this._RemoveCourseFromCatalogButton.ID);
        }
        #endregion

        #region _GetHtmlOfCatalogsAndCourses
        /// <summary>
        /// Gets the html of Catalogs and Courses to draw in the tree
        /// </summary>
        /// <param name="output">HtmlTextWriter</param>
        private StringBuilder _GetHtmlOfCatalogsAndCourses()
        {
            DataTable catalogsDataTable = this._CatalogObject.GetParents(this._SelectedLanguage, true);
            string catalogImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG, ImageFiles.EXT_PNG);
            string courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            StringBuilder sb = new StringBuilder();
            sb.Append("<div id='CatalogTreeRootElement' cid='root0' class='Item'><a href='javascript:void(0);' class='catalogTreeRootElement-nocss ImageLink' onclick='CatalogSectionClicked(this);'>");
            sb.Append("<img src='" + catalogImagePath + "'>");
            sb.Append(_GlobalResources.CourseCatalog + "</a>");
            sb.Append("<div id='CatalogTreeRootSubGroup' class='SubGroup'>");
            sb.Append("<div class='catalogs-nocss' cid='" + 0 + "'>");

            if (catalogsDataTable.Rows.Count > 0)
            {
                foreach (DataRow catalogDataRow in catalogsDataTable.Rows)
                {
                    int idParent = 0;
                    if (catalogDataRow["idParent"] != DBNull.Value)
                    {
                        idParent = Convert.ToInt32(catalogDataRow["idParent"]);
                    }
                    sb.Append(this._GetCatalogsAndInternalCourses(Convert.ToInt32(catalogDataRow["idCatalog"]), Convert.ToString(catalogDataRow["title"]), idParent, true, Convert.ToInt32(catalogDataRow["catalogCount"]), Convert.ToInt32(catalogDataRow["courseCount"]), catalogImagePath, courseImagePath));
                }
            }
            sb.Append("</div>");
            //add all unassigned courses in catalog tree as tree nodes
            sb.Append(this._GetUnassignedCourses());
            sb.Append("</div></div>");
            return sb;
        }
        #endregion

        #region _GetCatalogsAndInternalCourses
        /// <summary>
        /// Gets Catalogs and their internal Courses
        /// </summary>
        /// <param name="idCatalog">int</param>
        /// <param name="catalogTitle">string</param>
        /// <param name="idParent">int</param>
        /// <param name="isParentCatalog">bool</param>
        private StringBuilder _GetCatalogsAndInternalCourses(int idCatalog, string catalogTitle, int idParent, bool isParentCatalog, int catalogCount, int courseCount, string catalogImagePath, string courseImagePath)
        {
            //local variables
            string display = String.Empty;
            string parentCatalog = String.Empty;
            string innerParent = String.Empty;

            bool isInnerParent = false;
            int internalIdParent = 0;

            DataTable coursesDataTable;
            DataTable childCatalogsDataTable;

            StringBuilder catalogsListing = new StringBuilder();
            StringBuilder internalCatalog = new StringBuilder();

            if (!isParentCatalog)
            {
                display = "display:none;";
            }
            else
            {
                parentCatalog = "parentCatalog-nocss";
            }

            if (parentCatalog == String.Empty)
            {
                innerParent = "innerParent-nocss";
            }

            // add catalog node to tree
            catalogsListing.Append("<div idparent='" + idParent + "' cid='" + idCatalog + "' class='Item catalog-nocss " + parentCatalog + "" + innerParent + "' style='" + display + "'>");
            catalogsListing.Append("<a href='javascript:void(0);' class='catalogNode-nocss ImageLink' onclick='CatalogClicked(this);'>");
            catalogsListing.Append("<img src='" + catalogImagePath + "'>");
            catalogsListing.Append(catalogTitle);
            catalogsListing.Append("</a>");

            if (catalogCount > 0 || courseCount > 0)
            {
                catalogsListing.Append("<div class='SubGroup SubGroupElements-nocss' id='sg_" + idCatalog + "'>");
                display = "display:none;";
            }

            //if catalog is a parent catalog then add child catalog within catalog node
            if (catalogCount > 0)
            {
                childCatalogsDataTable = this._CatalogObject.GetChildren(idCatalog, this._SelectedLanguage, true);
                isInnerParent = true;

                if (isInnerParent)
                {
                    if (childCatalogsDataTable.Rows.Count > 0)
                    {
                        foreach (DataRow childCatalogDataRow in childCatalogsDataTable.Rows)
                        {
                            internalIdParent = 0;
                            if (childCatalogDataRow["idParent"] != DBNull.Value)
                            {
                                internalIdParent = Convert.ToInt32(childCatalogDataRow["idParent"]);
                            }
                            internalCatalog.Append(this._GetCatalogsAndInternalCourses(Convert.ToInt32(childCatalogDataRow["idCatalog"]), Convert.ToString(childCatalogDataRow["title"]), internalIdParent, false, Convert.ToInt32(childCatalogDataRow["catalogCount"]), Convert.ToInt32(childCatalogDataRow["courseCount"]), catalogImagePath, courseImagePath));
                        }
                    }
                }

                //append child cataog node in the parent catalog node 
                catalogsListing.Append(internalCatalog);
            }

            //if catalog contains direct course inside it ,add all course within catalog node
            if (courseCount > 0)
            {
                coursesDataTable = this._CatalogObject.GetCoursesInDataTable(idCatalog);
                if (coursesDataTable.Rows.Count > 0)
                {
                    catalogsListing.Append("<div class='courses-nocss'>");
                    foreach (DataRow coursesDataRow in coursesDataTable.Rows)
                    {
                        catalogsListing.Append("<div courseId='" + Convert.ToString(coursesDataRow["idCourse"]) + "' class='Item courseNode-nocss' style='" + display + "' >");
                        catalogsListing.Append("<a href='javascript:void(0);' class='courseNameNode-nocss ImageLink' onclick='CourseClicked(this);'>");
                        catalogsListing.Append("<img src='" + courseImagePath + "'>");
                        catalogsListing.Append(Convert.ToString(coursesDataRow["title"]));
                        catalogsListing.Append("</a>");
                        catalogsListing.Append("</div>");
                    }
                    catalogsListing.Append("</div>");
                }
            }

            if (catalogCount > 0 || courseCount > 0)
            {
                catalogsListing.Append("</div>");
            }

            catalogsListing.Append("</div>");
            return catalogsListing;
        }
        #endregion

        #region _GetUnassignedCourses
        /// <summary>
        /// Gets all unassigned courses
        /// </summary>
        private StringBuilder _GetUnassignedCourses()
        {
            StringBuilder coursesListing = new StringBuilder();
            DataTable coursesDataTable = new DataTable();
            string courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            coursesDataTable = Course.IdsAndNamesNotMembersOfAnyCatalog(null);

            if (coursesDataTable.Rows.Count > 0)
            {
                coursesListing.Append("<div class='courseListing-nocss'>");

                foreach (DataRow courseDataRow in coursesDataTable.Rows)
                {
                    coursesListing.Append("<div class='Item unassignedCourse-nocss' courseId='" + Convert.ToString(courseDataRow["idCourse"]) + "'>");
                    coursesListing.Append("<a class='courseNameNode-nocss ImageLink'>");
                    coursesListing.Append("<img src='" + courseImagePath + "'>");
                    coursesListing.Append(Convert.ToString(courseDataRow["title"]));
                    coursesListing.Append("</a>");
                    coursesListing.Append("</div>");
                }
                coursesListing.Append("</div>");
            }

            return coursesListing;
        }
        #endregion

        #region _NewCatalogModalButton_Command
        /// <summary>
        /// Handles the event initiated by the New Catalog Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _NewCatalogModalButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                // transfer the long description hidden field into the text box
                this._LongDescriptionTextBox.Text = this._LongDescriptionHiddenField.Value;

                // declare a catalog object
                Library.Catalog catalogObject = new Library.Catalog();

                int id;

                // determine if this is a new catalog or an existing one
                bool isNewCatalog = true;
                
                if (!String.IsNullOrWhiteSpace(this._IdCatalogTextBox.Text) && this._IdCatalogTextBox.Text != "0")
                { 
                    isNewCatalog = false;
                    catalogObject = new Library.Catalog(Convert.ToInt32(this._IdCatalogTextBox.Text));
                    //catalogObject.IdCatalog = Convert.ToInt32(this._IdCatalogTextBox.Text);
                }                              

                // populate the object
                catalogObject.Title = this._CatalogNameTextBox.Text;

                if (!String.IsNullOrWhiteSpace(this._Shortcode.Text))
                { catalogObject.Shortcode = this._Shortcode.Text; }
                else
                { catalogObject.Shortcode = null; }

                if (!String.IsNullOrWhiteSpace(this._Shortcode.Text))
                {
                    this._ShortcodeLink.NavigateUrl = "https://" + AsentiaSessionState.GlobalSiteObject.Hostname + "." + Config.AccountSettings.BaseDomain + "/catalog/?type=catalog&sc=" + this._Shortcode.Text;
                    this._ShortcodeLink.Text = "https://" + AsentiaSessionState.GlobalSiteObject.Hostname + "." + Config.AccountSettings.BaseDomain + "/catalog/?type=catalog&sc=" + this._Shortcode.Text;
                }
                else
                {
                    this._ShortcodeLink.NavigateUrl = String.Empty;
                    this._ShortcodeLink.Text = String.Empty;
                }

                
                if (this._PublicRadioButton.Checked)
                { catalogObject.IsPrivate = false; }
                else
                { catalogObject.IsPrivate = true; }
                
                if (this._ClosedRadioButton.Checked)
                {
                    catalogObject.IsClosed = true;
                    catalogObject.Cost = 0;
                    catalogObject.CostType = Convert.ToInt32(CatalogCostType.Free);
                }
                else
                {
                    catalogObject.IsClosed = false;

                    if (this._FreeRadioButton.Checked)
                    {
                        catalogObject.Cost = 0;
                        catalogObject.CostType = Convert.ToInt32(CatalogCostType.Free);
                    }
                    else if (this._FixedPriceRadioButton.Checked)
                    {
                        catalogObject.Cost = Convert.ToDouble(this._CatalogCostTextBox.Text);
                        catalogObject.CostType = Convert.ToInt32(CatalogCostType.FixedPrice);
                    }
                    else if (this._TotalAllCourseRadioButton.Checked)
                    {
                        catalogObject.Cost = 0;
                        catalogObject.CostType = Convert.ToInt32(CatalogCostType.TotalAllCourse);
                    }
                    else if (this._DiscountedTotalRadioButton.Checked)
                    {
                        catalogObject.Cost = Convert.ToDouble(this._CatalogCostTextBox.Text);
                        catalogObject.CostType = Convert.ToInt32(CatalogCostType.DiscountedTotal);
                    }
                }

                catalogObject.ShortDescription = this._ShortDescriptionTextArea.InnerText;

                if (!String.IsNullOrWhiteSpace(this._LongDescriptionHiddenField.Value))
                { catalogObject.LongDescription = HttpUtility.HtmlDecode(this._LongDescriptionHiddenField.Value); }
                else
                { catalogObject.LongDescription = null; }

                if (!String.IsNullOrWhiteSpace(this._SearchTagsTextArea.InnerText))
                { catalogObject.SearchTags = this._SearchTagsTextArea.InnerText; }
                else
                { catalogObject.SearchTags = null; }

                // set parent catalog property
                if (isNewCatalog)
                {
                    if (!String.IsNullOrWhiteSpace(this._SelectedCatalog.Value) && this._SelectedCatalog.Value != "root0")
                    { catalogObject.IdParent = Convert.ToInt32(this._SelectedCatalog.Value); }
                    else
                    { catalogObject.IdParent = null; }
                }                
                else
                {                    
                    if (!String.IsNullOrWhiteSpace(this._IdParentTextBox.Text))
                    { catalogObject.IdParent = Convert.ToInt32(this._IdParentTextBox.Text); }                    
                }

                // do avatar if existing catalog, if its a new catalog, we'll do it after initial save                
                string avatarFilename = "";
                string avatarFilenameSmall = "";

                if (!isNewCatalog)
                {                    
                    if (this._CatalogAvatar.SavedFilePath != null)
                    {
                        // check user folder existence and create if necessary
                        if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog)))
                        { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog)); }

                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._CatalogAvatar.SavedFilePath)))
                        {
                            avatarFilename = "avatar" + Path.GetExtension(this._CatalogAvatar.SavedFilePath);
                            avatarFilenameSmall = "avatar.small" + Path.GetExtension(this._CatalogAvatar.SavedFilePath);

                            fullSavedFilePath = SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog + "/" + avatarFilename;
                            fullSavedFilePathSmall = SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog + "/" + avatarFilenameSmall;

                            // delete existing avatar images if any
                            if (catalogObject.Avatar != null)
                            {
                                // find avatar images with "avatar" in the filename
                                string avatarImageFilename = @"avatar*" + Path.GetExtension(catalogObject.Avatar);
                                string[] avatarImageList = Directory.GetFiles(Server.MapPath(SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog), avatarImageFilename).Select(file => Path.GetFileName(file)).ToArray();

                                foreach (string file in avatarImageList)
                                {
                                    File.Delete(Server.MapPath(SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog + "/" + file));
                                }

                            }

                            // move the uploaded file into the catalog's folder
                            File.Copy(Server.MapPath(this._CatalogAvatar.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._CatalogAvatar.SavedFilePath));
                            catalogObject.Avatar = avatarFilename;

                            // create a smaller version of the avatar fo use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            catalogObject.SaveAvatar();
                        }
                        else
                        {
                            catalogObject.Avatar = null;
                        }
                    }
                    else if (this._ClearAvatar != null)
                    {
                        if (this._ClearAvatar.Value == "true")
                        {
                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog + "/" + "avatar" + Path.GetExtension(catalogObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog + "/" + "avatar" + Path.GetExtension(catalogObject.Avatar))); }

                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog + "/" + "avatar.small" + Path.GetExtension(catalogObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog + "/" + "avatar.small" + Path.GetExtension(catalogObject.Avatar))); }

                            catalogObject.Avatar = null;

                            catalogObject.SaveAvatar();
                        }
                    }
                    else
                    { }
                }

                // save the catalog - or just the language if we're doing an existing one in another language
                if (!String.IsNullOrWhiteSpace(this._SelectedCatalog.Value) && this._SelectedLanguage != AsentiaSessionState.GlobalSiteObject.LanguageString)
                { catalogObject.SaveLang(this._SelectedLanguage); }
                else
                { id = catalogObject.Save(); }

                // if this was a new catalog we just saved, we now have the id so that
                // we can create the catalog's folder and update the avatar if necessary.
                if (isNewCatalog)
                {
                    // check catalog folder existence and create if necessary
                    if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog)))
                    { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog)); }

                    // if there is an avatar, move it and update the database record
                    if (this._CatalogAvatar.SavedFilePath != null)
                    {
                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._CatalogAvatar.SavedFilePath)))
                        {
                            avatarFilename = "avatar" + Path.GetExtension(this._CatalogAvatar.SavedFilePath);
                            avatarFilenameSmall = "avatar.small" + Path.GetExtension(this._CatalogAvatar.SavedFilePath);

                            fullSavedFilePath = SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog + "/" + "avatar" + Path.GetExtension(this._CatalogAvatar.SavedFilePath);
                            fullSavedFilePathSmall = SitePathConstants.SITE_CATALOGS_ROOT + catalogObject.IdCatalog + "/" + "avatar.small" + Path.GetExtension(this._CatalogAvatar.SavedFilePath);

                            // move the uploaded file into the user's folder
                            File.Copy(Server.MapPath(this._CatalogAvatar.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._CatalogAvatar.SavedFilePath));
                            catalogObject.Avatar = "avatar" + Path.GetExtension(this._CatalogAvatar.SavedFilePath);

                            // create a smaller version of the avatar for use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            catalogObject.SaveAvatar();
                        }
                    }
                }

                // display success feedback
                this._NewCatalogModal.DisplayFeedback(_GlobalResources.CatalogHasBeenSavedSuccessfully, false);

                // if this is a new catalog, clear the fields so we can continue to add catalogs
                if (isNewCatalog)
                { 
                    this._ClearCatalogModalFields();

                    // register a script to clear the avatar fields
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ClearCatalogAvatarFields", "ClearAvatarFields();", true);
                }

                // update the catalog tree in background
                this._UpdateCourseCatalogs();
                this._CatalogContentUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._NewCatalogModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._NewCatalogModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dfnuEx)
            {
                // display the failure message
                this._NewCatalogModal.DisplayFeedback(dfnuEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._NewCatalogModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._NewCatalogModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _BindCourseListing
        /// <summary>
        /// Draws updates Catalogs and Courses
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _BindCourseListing(string searchText, int idCatalog)
        {
            Course courseObject = new Course();
            this._CourseList.ListBoxControl.DataSource = Course.IdsAndNamesForCatalogSelectList(idCatalog, searchText);
            this._CourseList.ListBoxControl.DataTextField = "title";
            this._CourseList.ListBoxControl.DataValueField = "idCourse";
            this._CourseList.ListBoxControl.DataBind();
        }
        #endregion

        #region _NewCourseModalButton_Command
        /// <summary>
        /// Handles the event initiated by the New Course Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _NewCourseModalButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get selected courses from the list box
                List<string> selectedCourses = this._CourseList.GetSelectedValues();

                // throw exception if no users selected
                if (selectedCourses.Count <= 0)
                { throw new AsentiaException(_GlobalResources.NoCourse_sSelected); }

                // add selected users to data table
                DataTable coursesToAdd = new DataTable();
                coursesToAdd.Columns.Add("id", typeof(int));

                foreach (string selectedCourse in selectedCourses)
                { coursesToAdd.Rows.Add(Convert.ToInt32(selectedCourse)); }

                this._CatalogObject.IdCatalog = Convert.ToInt32(this._SelectedCatalog.Value);
                this._CatalogObject.JoinCourses(coursesToAdd);

                // remove the selected users from the list box so they cannot be re-selected
                this._CourseList.RemoveSelectedItems();

                //Update the tree in back
                this._UpdateCourseCatalogs();
                this._CatalogContentUpdatePanel.Update();

                // display success feedback
                this._NewCourseModal.DisplayFeedback(_GlobalResources.Course_sAddedSuccessfully, false);

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._NewCourseModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._NewCourseModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._NewCourseModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._NewCourseModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region clearButton_Click
        /// <summary>
        /// Handles the event initiated by the clear Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void clearButton_Click(object sender, EventArgs e)
        {
            // clear the modal's feedback container
            this._NewCourseModal.ClearFeedback();

            // clear the listbox control and search text box
            this._CourseList.ListBoxControl.Items.Clear();
            this._CourseList.SearchTextBox.Text = string.Empty;

            this._BindCourseListing(this._CourseList.SearchTextBox.Text, Convert.ToInt32(this._SelectedCatalog.Value));
        }
        #endregion

        #region searchButton_Click
        /// <summary>
        /// Handles the event initiated by the New Search Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void searchButton_Click(object sender, EventArgs e)
        {
            // clear the modal's feedback container
            this._NewCourseModal.ClearFeedback();

            // clear the listbox control
            this._CourseList.ListBoxControl.Items.Clear();

            //rebind the course listing
            this._BindCourseListing(this._CourseList.SearchTextBox.Text, Convert.ToInt32(this._SelectedCatalog.Value));
        }
        #endregion

        #region _ClearCatalogModalFields
        /// <summary>
        /// Clears the fields in Catalog Modal Popup
        /// </summary>        
        private void _ClearCatalogModalFields()
        {
            //Reset the values of input controls
            this._IdCatalogTextBox.Text = String.Empty;
            this._CatalogNameTextBox.Text = String.Empty;
            this._Shortcode.Text = String.Empty;
            this._ShortcodeLink.Text = String.Empty;
            this._ShortcodeLink.NavigateUrl = String.Empty;
            this._PublicRadioButton.Checked = true;
            this._PrivateRadioButton.Checked = false;
            this._ClosedRadioButton.Checked = true;
            this._NotClosedRadioButton.Checked = false;
            this._ShortDescriptionTextArea.InnerText = String.Empty;
            this._LongDescriptionTextBox.Text = String.Empty;
            this._SearchTagsTextArea.InnerText = String.Empty;
        }
        #endregion

        #region ModalHiddenButton_Command
        /// <summary>
        /// Handles the event initiated by the hidden button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void ModalHiddenButton_Command(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(this._SelectedCatalog.Value))
            {
                this._ModifyCatalogObject = new Library.Catalog(Convert.ToInt32(this._SelectedCatalog.Value));
                this._PopulatePropertiesInputElements();
            }
        }
        #endregion

        #region _DeleteCatalog_Command
        /// <summary>
        /// Handles the event initiated by the Delete Catalog.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _DeleteCatalog_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable catalogsToDelete = new DataTable();
                catalogsToDelete.Columns.Add("id", typeof(int));
                catalogsToDelete.Rows.Add(this._SelectedCatalog.Value);

                //Delete catalod
                Library.Catalog.Delete(catalogsToDelete);

                //Update the tree in back
                this._UpdateCourseCatalogs();
                this._CatalogContentUpdatePanel.Update();

                // display the success message
                this.DisplayFeedback(_GlobalResources.TheCatalogHasBeenDeletedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _UpdateCourseCatalogs
        /// <summary>
        /// Updates Course Catalogs updatepanel.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _UpdateCourseCatalogs()
        {
            //Attach the html code of catalog tree to _CatalogHtml
            this._CatalogHtml.InnerHtml = Convert.ToString(this._GetHtmlOfCatalogsAndCourses());
        }
        #endregion

        #region _RemoveCourseFromCatalog_Command
        /// <summary>
        /// Handles the event initiated by the Remove Course confirmation pop up button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _RemoveCourseFromCatalog_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(this._SelectedCourseParent.Value))
                {
                    Library.Catalog catalogObjectCourses = new Library.Catalog();
                    DataTable courseToDelete = new DataTable();
                    courseToDelete.Columns.Add("id", typeof(int));

                    catalogObjectCourses.IdCatalog = Convert.ToInt32(this._SelectedCourseParent.Value);
                    courseToDelete.Rows.Add(this._SelectedCourse.Value);
                    catalogObjectCourses.RemoveCourses(courseToDelete);
                }

                //Update the tree in back
                this._UpdateCourseCatalogs();
                this._CatalogContentUpdatePanel.Update();

                // display the success message
                this.DisplayFeedback(_GlobalResources.CourseRemovedFromCatalogSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the event initiated by the Save Changes Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _SaveButton_Command(object sender, EventArgs e)
        {
            string jsonStringifiedCatalogData = this._CatalogOrderJsonData.Value;
            string jsonStringifiedCourseData = this._CourseOrderJsonData.Value;

            try
            {
                DataTable catalogsWithOrdering = new DataTable();
                catalogsWithOrdering.Columns.Add("id", typeof(int));
                catalogsWithOrdering.Columns.Add("order", typeof(int));

                List<CatalogOrderObject> AllCatalogOrderObjects = new JavaScriptSerializer().Deserialize<List<CatalogOrderObject>>(jsonStringifiedCatalogData);
                foreach (CatalogOrderObject catOrderObject in AllCatalogOrderObjects)
                {
                    catalogsWithOrdering.Rows.Add(catOrderObject.idCatalog, catOrderObject.order);
                }

                DataTable coursesWithOrdering = new DataTable();
                coursesWithOrdering.Columns.Add("idCatalog", typeof(int));
                coursesWithOrdering.Columns.Add("idCourse", typeof(int));
                coursesWithOrdering.Columns.Add("order", typeof(int));

                List<CourseOrderObject> AllCourseOrderObjects = new JavaScriptSerializer().Deserialize<List<CourseOrderObject>>(jsonStringifiedCourseData);
                foreach (CourseOrderObject corseOrderObject in AllCourseOrderObjects)
                {
                    coursesWithOrdering.Rows.Add(corseOrderObject.idCatalog, corseOrderObject.idCourse, corseOrderObject.order);
                }

                Library.Catalog catalogObject = new Library.Catalog();
                catalogObject.UpdateOrdering(catalogsWithOrdering, coursesWithOrdering);

                //Update the tree in back
                this._UpdateCourseCatalogs();
                this._CatalogContentUpdatePanel.Update();

                this.DisplayFeedback(_GlobalResources.OrderUpdatedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the event initiated by the Cancel Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CancelButton_Command(object sender, EventArgs e)
        {
            Response.Redirect("~/dashboard/");
        }
        #endregion

        #region _BuildDeleteCatalogModal
        /// <summary>
        /// Builds the modal for deleting the catalog.
        /// </summary>
        private void _BuildDeleteCatalogModal(string targetControlId)
        {
            //set modal properties
            this._DeleteCatalog = new ModalPopup("DeleteCatalogModal");
            this._DeleteCatalog.Type = ModalPopupType.Confirm;
            this._DeleteCatalog.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                        ImageFiles.EXT_PNG);
            this._DeleteCatalog.HeaderIconAlt = _GlobalResources.DeleteCatalog;
            this._DeleteCatalog.HeaderText = _GlobalResources.DeleteCatalog;
            this._DeleteCatalog.TargetControlID = targetControlId;
            this._DeleteCatalog.SubmitButton.Command += new CommandEventHandler(this._DeleteCatalog_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");

            Literal body1 = new Literal();

            body1Wrapper.ID = "DeleteCatalogBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteThisCatalog;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._DeleteCatalog.AddControlToBody(body1Wrapper);

            // add modal to container
            this._ModalPopupPanel.Controls.Add(this._DeleteCatalog);
        }
        #endregion

        #region _BuildRemoveCourseFromCatalogModal
        /// <summary>
        /// Builds the modal for removing course from catalog.
        /// </summary>
        /// <param name="targetControlId">Target Control Id</param>
        private void _BuildRemoveCourseFromCatalogModal(string targetControlId)
        {
            //set modal properties
            this._RemoveCourseFromCatalog = new ModalPopup("RemoveCourseFromCatalogModal");
            this._RemoveCourseFromCatalog.Type = ModalPopupType.Confirm;
            this._RemoveCourseFromCatalog.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                  ImageFiles.EXT_PNG);
            this._RemoveCourseFromCatalog.HeaderIconAlt = _GlobalResources.RemoveCourseFromCatalog;
            this._RemoveCourseFromCatalog.HeaderText = _GlobalResources.RemoveCourseFromCatalog;
            this._RemoveCourseFromCatalog.TargetControlID = targetControlId;
            this._RemoveCourseFromCatalog.SubmitButton.Command += new CommandEventHandler(this._RemoveCourseFromCatalog_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");

            Literal body1 = new Literal();

            body1Wrapper.ID = "DeleteCatalogBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToRemoveThisCourseFromThisCatalog;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._RemoveCourseFromCatalog.AddControlToBody(body1Wrapper);

            // add modal to container
            this._ModalPopupPanel.Controls.Add(this._RemoveCourseFromCatalog);
        }
        #endregion

        #region _BuildNewCatalogModal
        /// <summary>
        /// Creates Modal Popup for Catalog
        /// </summary>
        private void _BuildNewCatalogModal()
        {
            this._NewCatalogModal = new ModalPopup("newCatalogModal");
            this._NewCatalogModal.Type = ModalPopupType.Form;
            this._NewCatalogModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG,
                                                                          ImageFiles.EXT_PNG);
            this._NewCatalogModal.HeaderIconAlt = _GlobalResources.NewCatalog;
            this._NewCatalogModal.HeaderText = _GlobalResources.NewCatalog;
            this._NewCatalogModal.TargetControlID = this._HiddenLaunchModalButton.ID;

            this._NewCatalogModal.SubmitButton.OnClientClick = "return AddLongDescription(this);";
            this._NewCatalogModal.SubmitButton.Command += new CommandEventHandler(this._NewCatalogModalButton_Command);
            this._NewCatalogModal.ReloadPageOnClose = false;

            //Addin controls to modal popup
            #region Hidden controls

            // build the modal body
            this._NewCatalogFields = new Panel();
            this._NewCatalogFields.ID = "catalogFieldsDiv";
            this._NewCatalogFields.CssClass = "CatalogTree";

            this._IdCatalogTextBox.Text = String.Empty;
            this._IdCatalogTextBox.ID = "idCatalogTextBox";
            this._IdCatalogTextBox.Attributes.Add("style", "display:none;");
            this._NewCatalogFields.Controls.Add(this._IdCatalogTextBox);

            this._IdParentTextBox.Text = String.Empty;
            this._IdParentTextBox.ID = "idParentTextBox";
            this._IdParentTextBox.Attributes.Add("style", "display:none;");
            this._NewCatalogFields.Controls.Add(this._IdParentTextBox);

            //Hidden button for populating the modifying catalog property in modal popup
            Button modalHiddenButton = new Button();
            modalHiddenButton.ID = "ModalHiddenButton";
            modalHiddenButton.ClientIDMode = ClientIDMode.Static;
            modalHiddenButton.Click += new EventHandler(ModalHiddenButton_Command);
            modalHiddenButton.Style.Add("display", "none");
            this._NewCatalogModal.AddControlToBody(modalHiddenButton);

            //Hidden button for populating the modifying catalog property in modal popup
            Button clearFieldsHiddenButton = new Button();
            clearFieldsHiddenButton.ID = "clearFieldsHiddenButton";
            clearFieldsHiddenButton.Click += ClearFieldsHiddenButton_Click;
            clearFieldsHiddenButton.ClientIDMode = ClientIDMode.Static;
            clearFieldsHiddenButton.Style.Add("display", "none");
            this._NewCatalogModal.AddControlToBody(clearFieldsHiddenButton);

            #endregion

            #region Name input
            this._CatalogNameTextBox = new TextBox();
            this._CatalogNameTextBox.ID = "catalogNameTextBox";
            this._CatalogNameTextBox.CssClass = "InputLong";

            // add controls to container
            this._NewCatalogFields.Controls.Add(AsentiaPage.BuildFormField("catalogNameTextBox",
                                                                        _GlobalResources.Name,
                                                                        this._CatalogNameTextBox.ID,
                                                                        this._CatalogNameTextBox,
                                                                        true,
                                                                        true,
                                                                        false));
            #endregion

            #region Shortcode        
            List<Control> shortcodeInputControls = new List<Control>();

            this._Shortcode = new TextBox();
            this._Shortcode.ID = "CatalogShortcode_Field";
            this._Shortcode.CssClass = "InputShort";
            this._Shortcode.MaxLength = 10;
            shortcodeInputControls.Add(this._Shortcode);

            if (this._CatalogObject != null)
            {
               // build the shortcode link
               Panel shortcodeUrlLinkContainer = new Panel();
               shortcodeUrlLinkContainer.ID = "ShortcodeUrlLinkContainer";

               this._ShortcodeLink = new HyperLink();
               _ShortcodeLink.ID = "ShortcodeLink";
               _ShortcodeLink.Target = "_blank";

               shortcodeUrlLinkContainer.Controls.Add(this._ShortcodeLink);
               shortcodeInputControls.Add(shortcodeUrlLinkContainer);

               // attach an onchange event to the shortcode text box so the link gets updated as it is being changed
               this._Shortcode.Attributes.Add("onkeyup", "UpdateShortcodeLink(this.id);");
            }

            // add controls to container
            this._NewCatalogFields.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CatalogShortcode",
                                                                                 _GlobalResources.CatalogShortcode,
                                                                                 shortcodeInputControls,
                                                                                 false,
                                                                                 true));
            #endregion
            #region Avatar Field
            // catalog avatar field
            List<Control> avatarInputControls = new List<Control>();
            
            // catalog avatar image panel
            Panel avatarImageContainer = new Panel();
            avatarImageContainer.ID = "CatalogAvatar_Field_ImageContainer";
            avatarImageContainer.Attributes.CssStyle.Add("display", "none");
            avatarImageContainer.CssClass = "AvatarImageContainer";

            // avatar image
            this._AvatarImage = new Image();
            avatarImageContainer.Controls.Add(this._AvatarImage);
            avatarInputControls.Add(avatarImageContainer);

            Panel avatarImageDeleteButtonContainer = new Panel();
            avatarImageDeleteButtonContainer.ID = "AvatarImageDeleteButtonContainer";
            avatarImageDeleteButtonContainer.CssClass = "AvatarDeleteButtonContainer";
            avatarImageContainer.Controls.Add(avatarImageDeleteButtonContainer);

            // delete catalog avatar image
            Image deleteAvatarImage = new Image();
            deleteAvatarImage.ID = "AvatarImageDeleteButton";
            deleteAvatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteAvatarImage.CssClass = "SmallIcon";
            deleteAvatarImage.Attributes.Add("onClick", "javascript:DeleteCatalogAvatar();");
            deleteAvatarImage.Style.Add("cursor", "pointer");

            avatarImageDeleteButtonContainer.Controls.Add(deleteAvatarImage);

            // clear avatar hidden field
            this._ClearAvatar = new HiddenField();
            this._ClearAvatar.ID = "ClearAvatar_Field";
            this._ClearAvatar.Value = "false";
            avatarInputControls.Add(this._ClearAvatar);            

            // catalog avatar image upload
            this._CatalogAvatar = new UploaderAsync("CatalogAvatar_Field", UploadType.Avatar, "CatalogAvatar_ErrorContainer");

            // set params to resize the catalog avatar upon upload
            this._CatalogAvatar.ResizeOnUpload = true;
            this._CatalogAvatar.ResizeMaxWidth = 256;
            this._CatalogAvatar.ResizeMaxHeight = 256;

            avatarInputControls.Add(this._CatalogAvatar);

            this._NewCatalogFields.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CatalogAvatar",
                                                                                        _GlobalResources.Avatar,
                                                                                        avatarInputControls,
                                                                                        false,
                                                                                        true));
            #endregion

            #region Privacy container
            List<Control> privacyInputControls = new List<Control>();

            this._PrivateRadioButton = new RadioButton();
            this._PrivateRadioButton.ID = "privateRadioButton";
            this._PrivateRadioButton.Text = _GlobalResources.Private;
            this._PrivateRadioButton.Checked = false;
            this._PrivateRadioButton.GroupName = "IsPrivate";

            this._PublicRadioButton = new RadioButton();
            this._PublicRadioButton.ID = "publicRadioButton";
            this._PublicRadioButton.Text = _GlobalResources.Public;
            this._PublicRadioButton.Checked = true;
            this._PublicRadioButton.GroupName = "IsPrivate";

            privacyInputControls.Add(this._PrivateRadioButton);
            privacyInputControls.Add(this._PublicRadioButton);

            this._NewCatalogFields.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Privacy",
                                                                                     _GlobalResources.Privacy,
                                                                                     privacyInputControls,
                                                                                     false,
                                                                                     true));
            #endregion

            #region Closed container

            List<Control> closedInputControls = new List<Control>();

            // Closed input
            this._ClosedRadioButton = new RadioButton();
            this._ClosedRadioButton.ID = "closedRadioButton";
            this._ClosedRadioButton.Text = _GlobalResources.Yes;
            this._ClosedRadioButton.Checked = true;
            this._ClosedRadioButton.GroupName = "IsClosed";
            this._ClosedRadioButton.Attributes.Add("onclick", "ClosedRadioButtonClick(this)");

            this._NotClosedRadioButton = new RadioButton();
            this._NotClosedRadioButton.ID = "notClosedRadioButton";
            this._NotClosedRadioButton.Text = _GlobalResources.No;
            this._NotClosedRadioButton.Checked = false;
            this._NotClosedRadioButton.GroupName = "IsClosed";
            this._NotClosedRadioButton.Attributes.Add("onclick", "NotClosedRadioButtonClick(this)");

            //Add the closed field input radio buttons
            closedInputControls.Add(this._ClosedRadioButton);
            closedInputControls.Add(this._NotClosedRadioButton);

            Panel closedDescriptionContainer = new Panel();
            closedDescriptionContainer.CssClass = "closedDescription";

            Image closedDescriptionImage = new Image();
            closedDescriptionImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW,
                                                                     ImageFiles.EXT_PNG);
            closedDescriptionImage.CssClass = "SmallIcon";
            closedDescriptionContainer.Controls.Add(closedDescriptionImage);

            Label closedDescriptionLabel = new Label();
            closedDescriptionLabel.Text = _GlobalResources.ClosedCatalogDescription;
            closedDescriptionContainer.Controls.Add(closedDescriptionLabel);

            //Add the information image and description
            closedInputControls.Add(closedDescriptionContainer);

            Panel catalogCostTypeContainer = new Panel();
            catalogCostTypeContainer.ID = "catalogCostTypeContainer";

            Panel freeRadioButtonContainer = new Panel();
            this._FreeRadioButton = new RadioButton();
            this._FreeRadioButton.ID = "freeRadioButton";
            this._FreeRadioButton.Text = _GlobalResources.Free;
            this._FreeRadioButton.Checked = true;
            this._FreeRadioButton.GroupName = "CatalogCostType";
            this._FreeRadioButton.Attributes.Add("onclick", "FreeRadioButtonClick(this)");
            freeRadioButtonContainer.Controls.Add(this._FreeRadioButton);
            catalogCostTypeContainer.Controls.Add(freeRadioButtonContainer);

            Panel fixedPriceRadioButtonContainer = new Panel();
            this._FixedPriceRadioButton = new RadioButton();
            this._FixedPriceRadioButton.ID = "fixedPriceRadioButton";
            this._FixedPriceRadioButton.Text = String.Format(_GlobalResources.FixedPriceEGX25, this._EcommerceSettings.CurrencySymbol);
            this._FixedPriceRadioButton.GroupName = "CatalogCostType";
            this._FixedPriceRadioButton.Attributes.Add("onclick", "FixedPriceRadioButtonClick(this)");

            fixedPriceRadioButtonContainer.Controls.Add(this._FixedPriceRadioButton);
            catalogCostTypeContainer.Controls.Add(fixedPriceRadioButtonContainer);

            Panel totalAllCourseRadioButtonContainer = new Panel();
            this._TotalAllCourseRadioButton = new RadioButton();
            this._TotalAllCourseRadioButton.ID = "totalAllCourseRadioButton";
            this._TotalAllCourseRadioButton.Text = _GlobalResources.TotalOfAllCoursePricesInCatalog;
            this._TotalAllCourseRadioButton.GroupName = "CatalogCostType";
            this._TotalAllCourseRadioButton.Attributes.Add("onclick", "TotalAllCourseRadioButtonClick(this)");
            totalAllCourseRadioButtonContainer.Controls.Add(this._TotalAllCourseRadioButton);
            catalogCostTypeContainer.Controls.Add(totalAllCourseRadioButtonContainer);

            Panel discountedTotalRadioButtonContainer = new Panel();
            this._DiscountedTotalRadioButton = new RadioButton();
            this._DiscountedTotalRadioButton.ID = "discountedTotalRadioButton";
            this._DiscountedTotalRadioButton.Text = _GlobalResources.DiscountedTotalEG30Off;
            this._DiscountedTotalRadioButton.GroupName = "CatalogCostType";
            this._DiscountedTotalRadioButton.Attributes.Add("onclick", "DiscountedTotalRadioButtonClick(this)");

            discountedTotalRadioButtonContainer.Controls.Add(this._DiscountedTotalRadioButton);
            catalogCostTypeContainer.Controls.Add(discountedTotalRadioButtonContainer);

            Panel catalogCostTextBoxContainer = new Panel();
            catalogCostTextBoxContainer.ID = "catalogCostTextBoxContainer";

            Label discountedTotalLabel = new Label();
            discountedTotalLabel.ID = "discountedTotalLabel";
            discountedTotalLabel.CssClass = "bold";
            discountedTotalLabel.Text = _GlobalResources.DiscountedTotal + ": ";

            Label fixedPriceLabel = new Label();
            fixedPriceLabel.ID = "fixedPriceLabel";
            fixedPriceLabel.CssClass = "bold";
            fixedPriceLabel.Text = _GlobalResources.FixedPrice + ": ";

            Label currencySymbolLabel = new Label();
            currencySymbolLabel.ID = "currencySymbolLabel";
            currencySymbolLabel.Text = this._EcommerceSettings.CurrencySymbol + " ";

            this._CatalogCostTextBox = new TextBox();
            this._CatalogCostTextBox.ID = "catalogCostTextBox";
            this._CatalogCostTextBox.MaxLength = 8;
            this._CatalogCostTextBox.Width = Unit.Pixel(60);

            Label currencyCodeLabel = new Label();
            currencyCodeLabel.ID = "currencyCodeLabel";
            currencyCodeLabel.Text = " " + "(" + this._EcommerceSettings.CurrencyCode + ")";

            Label percentLabel = new Label();
            percentLabel.ID = "percentLabel";
            percentLabel.Text = " " + "%";

            catalogCostTextBoxContainer.Controls.Add(discountedTotalLabel);
            catalogCostTextBoxContainer.Controls.Add(fixedPriceLabel);
            catalogCostTextBoxContainer.Controls.Add(currencySymbolLabel);
            catalogCostTextBoxContainer.Controls.Add(this._CatalogCostTextBox);
            catalogCostTextBoxContainer.Controls.Add(currencyCodeLabel);
            catalogCostTextBoxContainer.Controls.Add(percentLabel);

            catalogCostTypeContainer.Controls.Add(catalogCostTextBoxContainer);

            closedInputControls.Add(catalogCostTypeContainer);

            this._NewCatalogFields.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Closed",
                                                                                     _GlobalResources.Closed,
                                                                                     closedInputControls,
                                                                                     false,
                                                                                     true));
            #endregion

            #region Short description container

            this._ShortDescriptionTextArea = new HtmlTextArea();
            this._ShortDescriptionTextArea.ID = "shortDescriptionTextArea";
            this._ShortDescriptionTextArea.Rows = 3;
            this._ShortDescriptionTextArea.Cols = 40;

            this._NewCatalogFields.Controls.Add(AsentiaPage.BuildFormField("shortDescriptionTextArea",
                                                                 _GlobalResources.ShortDescription,
                                                                 this._ShortDescriptionTextArea.ID,
                                                                 this._ShortDescriptionTextArea,
                                                                 true,
                                                                 true,
                                                                 false));
            #endregion

            #region Long Description container

            // html box
            this._LongDescriptionTextBox = new TextBox();
            this._LongDescriptionTextBox.ClientIDMode = ClientIDMode.Static;
            this._LongDescriptionTextBox.ID = "HtmlTextBox";
            this._LongDescriptionTextBox.CssClass = "ckeditor InputMedium";
            this._LongDescriptionTextBox.TextMode = TextBoxMode.MultiLine;
            this._LongDescriptionTextBox.Rows = 3;

            this._NewCatalogFields.Controls.Add(AsentiaPage.BuildFormField("LongDescription",
                                                                 _GlobalResources.Description,
                                                                 this._LongDescriptionTextBox.ID,
                                                                 this._LongDescriptionTextBox,
                                                                 false,
                                                                 false,
                                                                 false));
            #endregion

            #region Search tags container

            this._SearchTagsTextArea = new HtmlTextArea();
            this._SearchTagsTextArea.ID = "SearchTagsTextArea";
            this._SearchTagsTextArea.Rows = 3;
            this._SearchTagsTextArea.Cols = 40;

            this._NewCatalogFields.Controls.Add(AsentiaPage.BuildFormField("SearchTagsTextArea",
                                                                 _GlobalResources.SearchTags,
                                                                 this._SearchTagsTextArea.ID,
                                                                 this._SearchTagsTextArea,
                                                                 false,
                                                                 false,
                                                                 false));
            #endregion

            #region Add controls to body
            this._NewCatalogModal.AddControlToBody(this._NewCatalogFields);

            // add modal to container
            this._ModalPopupPanel.Controls.Add(this._NewCatalogModal);

            if (this._DisableNonLanguageFields)
            {
                Panel privacyFieldContainerPanel = (Panel)_NewCatalogModal.FindControl("Privacy_Container");

                if (privacyFieldContainerPanel != null)
                {
                    privacyFieldContainerPanel.Enabled = false;
                }

                Panel closedFieldContainerPanel = (Panel)_NewCatalogModal.FindControl("Closed_Container");

                if (closedFieldContainerPanel != null)
                {
                    closedFieldContainerPanel.Enabled = false;
                }
            }
        }
            #endregion


        #endregion

        #region ClearFieldsHiddenButton_Click
        /// <summary>
        /// Handles the event initiated by the hidden button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void ClearFieldsHiddenButton_Click(object sender, EventArgs e)
        {
            this._NewCatalogModal.ClearFeedback();
            this._ShortDescriptionTextArea.InnerText = "";
            this._LongDescriptionTextBox.Text = "";
            this._SearchTagsTextArea.InnerText = "";
            this._ShortcodeLink.NavigateUrl = String.Empty;
            this._ShortcodeLink.Text = String.Empty;

            // register a script to clear the avatar fields
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ClearCatalogAvatarFields", "ClearAvatarFields();", true);
        }
        #endregion

        #region _BuildAddCourseToCatalogModal
        /// <summary>
        /// Creates Modal Popup for Course
        /// </summary>
        private void _BuildAddCourseToCatalogModal()
        {        
            this._NewCourseModal = new ModalPopup("NewCourseModal");
            this._NewCourseModal.Type = ModalPopupType.Form;
            this._NewCourseModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE,
                                                                         ImageFiles.EXT_PNG);

            this._NewCourseModal.HeaderIconAlt = _GlobalResources.AddCourse_sToCatalog;
            this._NewCourseModal.HeaderText = _GlobalResources.AddCourse_sToCatalog;
            this._NewCourseModal.TargetControlID = this._AddCoursesModaTargetButton.ID;
            this._NewCourseModal.SubmitButton.OnClientClick = "SetSelectedNode();";
            this._NewCourseModal.SubmitButton.Command += new CommandEventHandler(this._NewCourseModalButton_Command);
            this._NewCourseModal.ReloadPageOnClose = false;

            //build modal body
            //bind _PopulateCourses hiddenfield value change event
            this._PopulateCoursesHiddenButton.ID = "populateCourses";
            this._PopulateCoursesHiddenButton.Style.Add("display","none");
            this._PopulateCoursesHiddenButton.Click += _PopulateCoursesHiddenButton_Click;
            this._NewCourseModal.AddControlToBody(this._PopulateCoursesHiddenButton);

            // build a container for the user listing
            this._CourseList = new DynamicListBox("AddEligibleUsers");
            this._CourseList.NoRecordsFoundMessage = _GlobalResources.NoCoursesFound;
            this._CourseList.SearchButton.Command += new CommandEventHandler(this.searchButton_Click);
            this._CourseList.ClearSearchButton.Command += new CommandEventHandler(this.clearButton_Click);

            // add controls to body
            this._NewCourseModal.AddControlToBody(this._CourseList);

            // add modal to container
            this._ModalPopupPanel.Controls.Add(this._NewCourseModal);
        }
        #endregion

        #region _PopulateCoursesHiddenButton_Click
        /// <summary>
        /// Method to populate the course listing in the add course to catalog modal popup
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void _PopulateCoursesHiddenButton_Click(object sender, EventArgs e)
        {
            //clear the feed back panel
            this._NewCourseModal.ClearFeedback();

            //Rebind the course listing on populateCourses hiddenfield value change event
            this._BindCourseListing(String.Empty, Convert.ToInt32(this._SelectedCatalog.Value));
        } 
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            //Action panel
            this.ActionsPanel.ID = "ActionsPanel";
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.OnClientClick = "return SaveChangesToServer(this);";
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);


            // cancel button
            this._HiddenLaunchModalButton = new Button();
            this._HiddenLaunchModalButton.ID = "HiddenLaunchModalButton";
            this._HiddenLaunchModalButton.Style[HtmlTextWriterStyle.Display] = "none";
            this._RightPanel.ContentTemplateContainer.Controls.Add(this._HiddenLaunchModalButton);
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;

            // name field
            if (String.IsNullOrWhiteSpace(this._CatalogNameTextBox.Text.Trim()))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._NewCatalogFields, "catalogNameTextBox", _GlobalResources.Name + " " + _GlobalResources.IsRequired);
            }

            if (String.IsNullOrWhiteSpace(this._ShortDescriptionTextArea.InnerText.Trim()))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._NewCatalogFields, "shortDescriptionTextArea", _GlobalResources.ShortDescription + " " + _GlobalResources.IsRequired);
            }


            if (this._NotClosedRadioButton.Checked && this._EcommerceSettings.IsEcommerceSet)
            {
                double cost = 0;
                bool validCost = double.TryParse(this._CatalogCostTextBox.Text, out cost);

                if (this._FixedPriceRadioButton.Checked)
                {
                    if (validCost && cost < 1)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this._NewCatalogFields, "closed", string.Format(_GlobalResources.FixedPriceMustBeX100OrGreater, this._EcommerceSettings.CurrencySymbol));
                    }
                    else if (!validCost)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this._NewCatalogFields, "closed", _GlobalResources.FixedPrice + " " + _GlobalResources.IsInvalid);
                    }
                }
                else if (this._DiscountedTotalRadioButton.Checked)
                {
                    if (validCost && (cost < 1 || cost > 99))
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this._NewCatalogFields, "closed", _GlobalResources.PercentOffMustBeBetween1And99);
                    }
                    else if (!validCost)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this._NewCatalogFields, "closed", _GlobalResources.PercentOff + " " + _GlobalResources.IsInvalid);
                    }
                }
            }

            return isValid;
        }
        #endregion

        #region _BuildLanguageSelectorContainer
        /// <summary>
        /// Builds a language selector control for selecting which language you are editing an object in.
        /// </summary>
        private void _BuildLanguageSelectorContainer()
        {
            // clear controls from container
            this.ObjectLanguageSelectorContainer.Controls.Clear();

            // apply css to container
            this.ObjectLanguageSelectorContainer.CssClass = "ObjectLanguageSelectorContainer";

            // build the label
            Localize languageSelectorLabel = new Localize();
            languageSelectorLabel.Text = _GlobalResources.LanguageSelector;

            // build the control
            this.LanguageSelectorControl = new LanguageSelector(LanguageDropDownType.Object);

            // set the selected value
            this.LanguageSelectorControl.SelectedValue = this._SelectedLanguage;

            // attach the controls to the container
            this.ObjectLanguageSelectorContainer.Controls.Add(languageSelectorLabel);
            this.ObjectLanguageSelectorContainer.Controls.Add(this.LanguageSelectorControl);
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            this._NewCatalogModal.ClearFeedback();

            if (this._ModifyCatalogObject != null)
            {
                this._IdCatalogTextBox.Text = Convert.ToString(this._ModifyCatalogObject.IdCatalog);
                this._IdParentTextBox.Text = Convert.ToString(this._ModifyCatalogObject.IdParent);
                this._Shortcode.Text = this._ModifyCatalogObject.Shortcode;
                if (!String.IsNullOrWhiteSpace(this._ModifyCatalogObject.Shortcode))
                {
                    this._ShortcodeLink.NavigateUrl = "https://" + AsentiaSessionState.GlobalSiteObject.Hostname + "." + Config.AccountSettings.BaseDomain + "/catalog/?type=catalog&sc=" + this._ModifyCatalogObject.Shortcode;
                    this._ShortcodeLink.Text = "https://" + AsentiaSessionState.GlobalSiteObject.Hostname + "." + Config.AccountSettings.BaseDomain + "/catalog/?type=catalog&sc=" + this._ModifyCatalogObject.Shortcode;
                }  
                else
                {
                    this._ShortcodeLink.NavigateUrl = String.Empty;
                    this._ShortcodeLink.Text = String.Empty;
                }

                // language specific properties - title, short description and long description
                if (this._SelectedLanguage != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (Library.Catalog.LanguageSpecificProperty CatalogLanguageSpecificProperty in this._ModifyCatalogObject.LanguageSpecificProperties)
                    {
                        if (CatalogLanguageSpecificProperty.LangString == this._SelectedLanguage)
                        {
                            this._CatalogNameTextBox.Text = CatalogLanguageSpecificProperty.Title;
                            this._ShortDescriptionTextArea.InnerText = CatalogLanguageSpecificProperty.ShortDescription;
                            this._LongDescriptionTextBox.Text = CatalogLanguageSpecificProperty.LongDescription;
                            this._SearchTagsTextArea.InnerText = CatalogLanguageSpecificProperty.SearchTags;
                        }
                    }
                }
                else
                {
                    this._CatalogNameTextBox.Text = this._ModifyCatalogObject.Title;
                    
                    // avatar
                    Panel avatarPanel = (Panel)_NewCatalogModal.FindControl("CatalogAvatar_Field_ImageContainer");

                    if (this._ModifyCatalogObject.Avatar != null)
                    {
                        this._AvatarImage.ImageUrl = SitePathConstants.SITE_CATALOGS_ROOT + this._ModifyCatalogObject.IdCatalog + "/" + this._ModifyCatalogObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

                        if (avatarPanel != null)
                        { avatarPanel.Attributes.CssStyle.Remove("display"); }

                        if (avatarPanel != null)
                        { avatarPanel.Attributes.CssStyle.Add("display", "inline-block"); }
                    }
                    else
                    {
                        this._AvatarImage.ImageUrl = String.Empty;

                        if (avatarPanel != null)
                        { avatarPanel.Attributes.CssStyle.Remove("display"); }

                        if (avatarPanel != null)
                        { avatarPanel.Attributes.CssStyle.Add("display", "none"); }
                    }

                    if (this._ModifyCatalogObject.IsPrivate)
                    {
                        this._PrivateRadioButton.Checked = true;
                        this._PublicRadioButton.Checked = false;
                    }
                    else
                    {
                        this._PublicRadioButton.Checked = true;
                        this._PrivateRadioButton.Checked = false;
                    }

                    if (this._ModifyCatalogObject.IsClosed == true)
                    {
                        this._ClosedRadioButton.Checked = true;
                        this._NotClosedRadioButton.Checked = false;
                    }
                    else
                    {
                        this._NotClosedRadioButton.Checked = true;
                        this._ClosedRadioButton.Checked = false;

                        CatalogCostType costType = (CatalogCostType)Enum.Parse(typeof(CatalogCostType), this._ModifyCatalogObject.CostType.ToString());

                        this._FreeRadioButton.Checked = false;
                        this._FixedPriceRadioButton.Checked = false;
                        this._TotalAllCourseRadioButton.Checked = false;
                        this._DiscountedTotalRadioButton.Checked = false;

                        switch (costType)
                        {
                            case CatalogCostType.Free:
                                this._FreeRadioButton.Checked = true;
                                break;

                            case CatalogCostType.FixedPrice:
                                this._FixedPriceRadioButton.Checked = true;
                                this._CatalogCostTextBox.Text = Convert.ToString(this._ModifyCatalogObject.Cost);
                                break;

                            case CatalogCostType.TotalAllCourse:
                                this._TotalAllCourseRadioButton.Checked = true;
                                break;

                            case CatalogCostType.DiscountedTotal:
                                this._DiscountedTotalRadioButton.Checked = true;
                                this._CatalogCostTextBox.Text = Convert.ToString(this._ModifyCatalogObject.Cost);
                                break;

                            default:
                                this._FreeRadioButton.Checked = true;
                                break;
                        }
                    }

                    this._ShortDescriptionTextArea.InnerText = this._ModifyCatalogObject.ShortDescription;
                    this._LongDescriptionTextBox.Text = this._ModifyCatalogObject.LongDescription;
                    this._SearchTagsTextArea.InnerText = this._ModifyCatalogObject.SearchTags;
                }
            }
        }
        #endregion
    }

    #region CatalogOrderObject Class
    /// <summary>
    /// Used to receive the json stringified data with Sorting/Ordering changes.
    /// </summary>    
    internal class CatalogOrderObject
    {
        #region Private Properties
        /// <summary>
        /// Represents idCatalog of Catalog.
        /// </summary>
        /// <type>String</type>
        /// <initial value>Empty String </initial value>
        private string _idCatalog = String.Empty;
        public string idCatalog
        {
            get { return _idCatalog; }
            set { _idCatalog = value; }
        }

        /// <summary>
        /// Represents idParent of the Catalog.
        /// </summary>
        /// <type>String</type>
        /// <initial value>Empty String </initial value>
        private string _idParent = String.Empty;
        public string idParent
        {
            get { return _idParent; }
            set { _idParent = value; }
        }

        /// <summary>
        /// Represents order of the Catalog within the parent.
        /// </summary>
        /// <type>String</type>
        /// <initial value>Empty String </initial value>
        private string _order = String.Empty;
        public string order
        {
            get { return _order; }
            set { _order = value; }
        }
        #endregion
    }
    #endregion

    #region CatalogOrderObject Class
    /// <summary>
    /// Used to receive the json stringified data with Sorting/Ordering changes.
    /// </summary>    
    internal class CourseOrderObject
    {
        #region Private Properties
        /// <summary>
        /// Represents idCatalog of Catalog.
        /// </summary>
        /// <type>String</type>
        /// <initial value>Empty String </initial value>
        private string _idCatalog = String.Empty;
        public string idCatalog
        {
            get { return _idCatalog; }
            set { _idCatalog = value; }
        }

        /// <summary>
        /// Represents idCourse of the Catalog.
        /// </summary>
        /// <type>String</type>
        /// <initial value>Empty String </initial value>
        private string _idCourse = String.Empty;
        public string idCourse
        {
            get { return _idCourse; }
            set { _idCourse = value; }
        }

        /// <summary>
        /// Represents order of the Catalog within the parent.
        /// </summary>
        /// <type>String</type>
        /// <initial value>Empty String </initial value>
        private string _order = String.Empty;
        public string order
        {
            get { return _order; }
            set { _order = value; }
        }
        #endregion
    }
    #endregion
}


