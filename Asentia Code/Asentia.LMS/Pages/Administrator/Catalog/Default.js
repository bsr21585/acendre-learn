﻿/* CatalogTree v0.1 */

var openedCatalogs = "";
var postbackElement = "";
var catalogs = null;
var unassignedCourse = null;

$(function () {
    ShowRightSideActionButtons();
});

//function: ShowRightSideActionButtons
//Handles the visibility of right side action control buttons
function ShowRightSideActionButtons() {
    $(".actionLinks-nocss").find(".modifyCatalogButton").hide();
    $(".actionLinks-nocss").find(".newCatalogButton").hide();
    $(".actionLinks-nocss").find(".deleteCatalogButton").hide();
    $(".actionLinks-nocss").find(".newCourseButton").hide();
    $(".actionLinks-nocss").find(".deleteCourseButton").hide();
    $(".actionLinks-nocss").find(".noActionButton").hide();
}

//function: SortCourseCatalogs
//Handles the Sorting/Ordering of Catalogs/Courses
function SortCourseCatalogs() {
    $(".catalogs-nocss").sortable({
        distance: 5,
        delay: 300,
        opacity: 0.6,
        cursor: 'move'
    });

    $(".catalogs-nocss").disableSelection();

    $(".courseListing-nocss").sortable({
        distance: 5,
        delay: 300,
        opacity: 0.6,
        cursor: 'move'
    });

    $(".courseListing-nocss").disableSelection();

    $(".parentCatalog-nocss").sortable({
        items: "> div.catalog-nocss",
        distance: 5,
        delay: 300,
        opacity: 0.6,
        cursor: 'move'
    });
    $(".parentCatalog-nocss").disableSelection();

    $(".courses-nocss").sortable({
        placeholder: "course-sortable-placeholder",
        distance: 5,
        delay: 300,
        opacity: 0.6,
        cursor: 'move'
    });
    $(".courses-nocss").disableSelection();

    $(".innerParent-nocss").sortable({
        items: "> div.catalog-nocss",
        distance: 5,
        delay: 300,
        opacity: 0.6,
        cursor: 'move'
    });
    $(".innerParent-nocss").disableSelection();

    $(".SubGroupElements-nocss").sortable({
        items: "> div.catalog-nocss",
        distance: 5,
        delay: 300,
        opacity: 0.6,
        cursor: 'move'
    });
    $(".SubGroupElements-nocss").disableSelection();
}

$(document).ready(function () {
    CatalogSectionClicked(document.getElementsByClassName('catalogTreeRootElement-nocss'));
});

//function: pageLoad
//handles pages load
function pageLoad() {

    //set ck editor on HtmlTextBox after every postback    
    $('.unassignedCourse-nocss .courseNameNode-nocss').unbind().click(function (event) { UnassignedCourseClicked(this) });

    SortCourseCatalogs();
    jQuery.each(openedCatalogs.split(","), function (index, obj) {
        $(".catalogs-nocss").find(".catalog-nocss").each(function () {
            if ($(this).attr("cid") == obj) {
                $(this).find(".catalogNode-nocss").siblings(".courses-nocss").slideDown();
                $(this).find(".catalogNode-nocss").siblings(".courses-nocss").find("div.courseNode-nocss").slideDown();
                $(this).find(".catalogNode-nocss").siblings("div.catalog-nocss").slideDown();
                $(this).find(".catalogNode-nocss").siblings("div.catalog-nocss").children().not('a.catalogNode-nocss').hide();
            }
        });
    });
    if (postbackElement == "ModifyCatalogButton") {
        $("#newCatalogModalModalPopupHeaderText").html(modifyCatalogCaption);        
        //Fill the selected node id
        SetSelectedNode();
    }
    postbackElement = "";

    var closedRadioButton = document.getElementById("closedRadioButton");
    var notClosedRadioButton = document.getElementById("notClosedRadioButton");

    ClosedRadioButtonClick(closedRadioButton);
    NotClosedRadioButtonClick(notClosedRadioButton);
    CKEDITOR.replace("HtmlTextBox");

    //If hidden field value is not undefined then select the catalog with id as hidden field value else select root catalog 
    if ($("#" + selectedCatalogId).val() != undefined && $("#" + selectedCatalogId).val() != null && $("#" + selectedCatalogId).val() != '') {
        CatalogClicked($("div[cid=" + $("#" + selectedCatalogId).val() + "]").find('a').eq(0));

        //Logic for maintaning the scroll position th the selected element
        var container = $('#CatalogTree'), scrollTo = $("div[cid=" + $("#" + selectedCatalogId).val() + "]")
        container.scrollTop(
            scrollTo.offset().top - container.offset().top + container.scrollTop()
        );

        $("#" + selectedCatalogId).val('');
    }
    else {
        //set root as selected
        CatalogSectionClicked(document.getElementsByClassName('catalogTreeRootElement-nocss'));
    }
}

// function: ClearCatalogTextFields
// resests the catalog modal popup fields
function ClearCatalogTextFields() {
    $("#" + idCatalogTextBox).val("");
    $("#" + catalogNameTextBox).val("");
    $("#" + catalogShortCode).val("");

    //Fill the selected node id
    SetSelectedNode();

    document.getElementById(publicRadioButton).Checked = true;
    document.getElementById(privateRadioButton).Checked = false;
    document.getElementById(closedRadioButton).Checked = true;
    document.getElementById(notClosedRadioButton).Checked = false;

    $("#newCatalogModalModalPopupHeaderText").html(addModifyCatalogCaption);

    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
        CKEDITOR.instances[instance].setData('');
    }

    actionClicked = "Modify";

    document.getElementById('HiddenLaunchModalButton').click();

    ////after show modal click hidden button to clear data from controls
    document.getElementById('clearFieldsHiddenButton').click();

    ////after show modal click hidden button to bind data to controls
    //document.getElementById('ModalHiddenButton').click();
}

// function: OnModifyLinkClick
//  hidden button click
function OnModifyLinkClick() {

    actionClicked = "Modify";

    // clear the avatar fields
    ClearAvatarFields();

    document.getElementById('HiddenLaunchModalButton').click();

    //on catalog update click first click on New Catalog link to show modal

    //Change new catalog modal popup header text to modify catalog text    
    $("#newCatalogModalModalPopupHeaderText").html(modifyCatalogCaption);

    ////after show modal click hidden button to bind data to controls
    document.getElementById('ModalHiddenButton').click();
}

// function: SetSelectedNode
// fill the selected node id to a hidden field to select that again and scroll to its location
function SetSelectedNode() {
    $("#" + selectedCatalogId).val($($('.SelectedItem')).attr('cid'));
}


// function: PopulateCoursesInCourseListing
// populates courses into course listing
function PopulateCoursesInCourseListing() {
    //Fill the selected node id
    SetSelectedNode();

    $("#NewCourseModalModalPopupExtender_backgroundElement").show();
    $("#NewCourseModal").show();
    ShowCatalogActionButtons();
    $("#NewCatalogModalPopupExtender_backgroundElement").hide();
    $("#populateCourses").click();
}

// function: CourseClicked
// handles course click event
// Param: name = "elem"
// Param: value = represents a particular course node
function CourseClicked(elem) {
    $(".Item").removeClass("SelectedItem");
    $(elem).parent().addClass("SelectedItem");

    $(".actionLinks-nocss").find(".modifyCatalogButton").hide();
    $(".actionLinks-nocss").find(".newCatalogButton").hide();
    $(".actionLinks-nocss").find(".deleteCatalogButton").hide();
    $(".actionLinks-nocss").find(".newCourseButton").hide();
    $(".actionLinks-nocss").find(".noActionButton").hide();

    $(".actionLinks-nocss").find(".deleteCourseButton").show();
    $(".actionLinks-nocss").find("li").removeClass("rightPanelListItem");

    $("#" + selectedCourseParent).val($(elem).closest("div.catalog-nocss").attr("cid"));
    $("#" + selectedCatalogSection).val("0");
    $("#" + selectedCourse).val($(elem).parent().attr("courseId"));
}

// function: CatalogClicked
// handles catalog click event
// Param: name = "elem"
// Param: value = represents a particular catalog node
function CatalogClicked(elem) {
    $(elem).siblings(".SubGroupElements-nocss").children('.catalog-nocss').slideToggle();
    $(elem).siblings(".SubGroupElements-nocss").children('.courses-nocss').find("div.courseNode-nocss").slideToggle();

    $(".Item").removeClass("SelectedItem");
    $(elem).parent().addClass("SelectedItem");

    $(".actionLinks-nocss").find(".modifyCatalogButton").show();
    $(".actionLinks-nocss").find(".newCatalogButton").show();
    $(".actionLinks-nocss").find(".deleteCatalogButton").show();
    $(".actionLinks-nocss").find(".newCourseButton").show();
    $(".actionLinks-nocss").find(".noActionButton").hide();

    $(".actionLinks-nocss").find(".deleteCourseButton").hide();
    $(".actionLinks-nocss").find("li").addClass("rightPanelListItem");

    $("#" + selectedCatalogSection).val("0");
    $("#" + selectedCatalog).val($(elem).parent().attr("cid"));
    openedCatalogs += $(elem).parent().attr("cid") + ",";
}

// function: CatalogSectionClicked
// handles root click event
// Param: name = "elem"
// Param: value = represents root node
function CatalogSectionClicked(elem) {

    $(".actionLinks-nocss").find(".modifyCatalogButton").hide();
    $(".actionLinks-nocss").find(".newCatalogButton").show();
    $(".actionLinks-nocss").find(".deleteCatalogButton").hide();
    $(".actionLinks-nocss").find(".newCourseButton").hide();
    $(".actionLinks-nocss").find(".deleteCourseButton").hide();
    $(".actionLinks-nocss").find(".noActionButton").hide();

    $(".Item").removeClass("SelectedItem");
    $(elem).parent().addClass("SelectedItem");

    $("#" + selectedCatalogSection).val("1");
}

// function: UnassignedCourseClicked
// handles unassigned course click event
// Param: name = "elem"
// Param: value = represents a particular course node
function UnassignedCourseClicked(elem) {
    $(".actionLinks-nocss").find(".modifyCatalogButton").hide();
    $(".actionLinks-nocss").find(".newCatalogButton").hide();
    $(".actionLinks-nocss").find(".deleteCatalogButton").hide();
    $(".actionLinks-nocss").find(".newCourseButton").hide();
    $(".actionLinks-nocss").find(".deleteCourseButton").hide();

    // hide the No Actions Available link from panel when clicked on course items
    $(".actionLinks-nocss").find(".noActionButton").hide();

    $(".Item").removeClass("SelectedItem");
    $(elem).parent().addClass("SelectedItem");

    $(".actionLinks-nocss").find("li").removeClass("rightPanelListItem");
}

// function: ShowCatalogActionButtons
// shows catalog action buttons
function ShowCatalogActionButtons() {
    $(".actionLinks-nocss").find(".modifyCatalogButton").show();
    $(".actionLinks-nocss").find(".newCatalogButton").show();
    $(".actionLinks-nocss").find(".deleteCatalogButton").show();
    $(".actionLinks-nocss").find(".newCourseButton").show();
    $(".actionLinks-nocss").find(".deleteCourseButton").hide();
    $(".actionLinks-nocss").find(".noActionButton").hide();
}

// function: showCourseActionButtons
// shows course action buttons
function showCourseActionButtons() {
    $(".actionLinks-nocss").find(".modifyCatalogButton").hide();
    $(".actionLinks-nocss").find(".newCatalogButton").hide();
    $(".actionLinks-nocss").find(".deleteCatalogButton").hide();
    $(".actionLinks-nocss").find(".newCourseButton").hide();
    $(".actionLinks-nocss").find(".deleteCourseButton").show();
    $(".actionLinks-nocss").find(".noActionButton").hide();
}

// Function: SaveChangesToServer
// saving the sorting/ordering into json object
// Param: name = "elem"
// Param: value = represents a particular catalog
function SaveChangesToServer(elem) {

    catalogs = new Array();
    $(".catalogs-nocss .parentCatalog-nocss").each(function (index, obj) {
        catalogs.push(GetCatalogIdAndOrder($(this), index));
    });

    var stringified = JSON.stringify(catalogs);
    $("#" + jsonData).val(stringified);

    SaveChangesForCoursesToServer();
    var stringifiedUnasignedCourse = JSON.stringify(unassignedCourse);
    $("#" + courseOrderJsonData).val(stringifiedUnasignedCourse);
}


// Function: AddLongDescription
// filling the long description from hidden field to catalog object 
// Param: name = "elem"
function AddLongDescription(elem) {

    //Fill the selected node id
    SetSelectedNode();

    $("#" + longDescriptionHiddenField).val('');
    var longDescription = CKEDITOR.instances.HtmlTextBox.getData();
    $("#" + longDescriptionHiddenField).val(longDescription);
}


// Function: SaveChangesForCoursesToServer
// saving the sorting/ordering into json object for course
function SaveChangesForCoursesToServer() {

    unassignedCourse = new Array();
    $(".courseListing-nocss .unassignedCourse-nocss").each(function (index, obj) {
        var course = new CourseOrderObject();
        course.idCatalog = 0;
        course.idCourse = $(this).attr("courseid");
        course.order = index;
        unassignedCourse.push(course);
    });

    $(".catalogs-nocss .parentCatalog-nocss").each(function (index, obj) {
        GetCourseIdAndOrder($(this), index);
    });
}

// Function: GetCourseIdAndOrder
// gets course id and order
// Param: name = "elem"
// Param: value = represents a particular catalog
// Param: value = index of a particular catalog
function GetCourseIdAndOrder(elem, index) {

    $(elem).children(".SubGroupElements-nocss").children(".catalog-nocss").each(function (index, obj) {
        GetCourseIdAndOrder($(this), index);
    });


    var idCatalog = $(elem).attr("cid");
    $(elem).children(".SubGroupElements-nocss").children(".courses-nocss").children(".courseNode-nocss").each(function (index, obj) {
        var course = new CourseOrderObject();
        course.idCatalog = idCatalog;
        course.idCourse = $(this).attr("courseid");
        course.order = index;
        unassignedCourse.push(course);
    });
}

// Function: GetCatalogIdAndOrder
// gets catalog id and order
// Param: name = "elem"
// Param: value = represents a particular catalog
// Param: value = index of a particular catalog
function GetCatalogIdAndOrder(elem, index) {

    $(elem).children(".SubGroupElements-nocss").children(".catalog-nocss").each(function (index, obj) {
        catalogs.push(GetCatalogIdAndOrder($(this), index));
    });

    var catalog = new CatalogOrderObject();
    catalog.idCatalog = $(elem).attr("cid");
    catalog.idParent = $(elem).attr("idparent");
    catalog.order = index;
    return catalog;

}

// class: CatalogOrderObject
// used for populating the catalog ids and orders
function CatalogOrderObject() {
    //represents the id of a catalog
    this.idCatalog = null;
    //represents the id of a catalog's parent
    this.idParent = null;
    //represents the order of a catalog
    this.order = null;
}

// class: CourseOrderObject
// used for populating the course ids and orders with catalogid
function CourseOrderObject() {
    //represents the id of a catalog
    this.idCourse = null;
    //represents the id of a catalog's parent
    this.idCatalog = null;
    //represents the order of a catalog
    this.order = null;
}

function ClosedRadioButtonClick(elem) {
    if (elem.checked) {
        $("#catalogCostTypeContainer").hide();

        var freeRadioButton = document.getElementById("freeRadioButton");
        freeRadioButton.checked = true;
        FreeRadioButtonClick(freeRadioButton);
    }
}

function NotClosedRadioButtonClick(elem) {
    if (elem.checked && merchantSettingsOn == "true") {
        $("#catalogCostTypeContainer").show();

        var freeRadioButton = document.getElementById("freeRadioButton");
        var fixedPriceRadioButton = document.getElementById("fixedPriceRadioButton");
        var totalAllCourseRadioButton = document.getElementById("totalAllCourseRadioButton");
        var discountedTotalRadioButton = document.getElementById("discountedTotalRadioButton");
        if (freeRadioButton.checked) {
            FreeRadioButtonClick(freeRadioButton);
        }
        else if (fixedPriceRadioButton.checked) {
            FixedPriceRadioButtonClick(fixedPriceRadioButton);
        }
        else if (totalAllCourseRadioButton.checked) {
            TotalAllCourseRadioButtonClick(totalAllCourseRadioButton);
        }
        else if (discountedTotalRadioButton.checked) {
            DiscountedTotalRadioButtonClick(discountedTotalRadioButton);
        }

    }
    else {
        $("#catalogCostTypeContainer").hide();
    }
}

function FreeRadioButtonClick(elem) {
    if (elem.checked) {
        $("#catalogCostTextBoxContainer").hide();
        $("#catalogCostTextBox").val("");
    }
}

function FixedPriceRadioButtonClick(elem) {
    if (elem.checked) {
        $("#catalogCostTextBoxContainer").show();
        $("#fixedPriceLabel").show();
        $("#currencySymbolLabel").show();
        $("#currencyCodeLabel").show();

        $("#discountedTotalLabel").hide();
        $("#percentLabel").hide();
    }
    else {
        $("#catalogCostTextBoxContainer").hide();

    }
}

function TotalAllCourseRadioButtonClick(elem) {
    if (elem.checked) {
        $("#catalogCostTextBoxContainer").hide();
    }
}

function DiscountedTotalRadioButtonClick(elem) {
    if (elem.checked) {
        $("#catalogCostTextBoxContainer").show();
        $("#fixedPriceLabel").hide();
        $("#currencySymbolLabel").hide();
        $("#currencyCodeLabel").hide();

        $("#discountedTotalLabel").show();
        $("#percentLabel").show();
    }
    else {
        $("#catalogCostTextBoxContainer").hide();

    }
}

//Method to clear out the avatar fields
function ClearAvatarFields() {
    // image container
    $("#CatalogAvatar_Field_ImageContainer").css("display", "none");
    $("#CatalogAvatar_Field_ImageContainer img").prop("src", "");

    // uploader
    $("#CatalogAvatar_Field_CompletedPanel").html("");
    $("#CatalogAvatar_ErrorContainer").html("");
    $("#CatalogAvatar_Field_UploadControl input").attr("style", "");
    $("#CatalogAvatar_Field_UploadHiddenFieldOriginalFileName").val("");
    $("#CatalogAvatar_Field_UploadHiddenField").val("");
    $("#CatalogAvatar_Field_UploadHiddenFieldFileSize").val("");

    // clear avatar flag
    $("#ClearAvatar_Field").val("false");
}

//Method to remove a catalog avatar
function DeleteCatalogAvatar() {
    $("#CatalogAvatar_Field_ImageContainer").remove();
    $("#ClearAvatar_Field").val("true");
}