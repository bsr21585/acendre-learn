﻿using System;
using System.Collections;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using System.Data;
using System.Data.SqlClient;
using Asentia.LMS.Controls;

namespace Asentia.LMS.Pages.Administrator.Leaderboards
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel LeaderboardFormContentWrapperContainer;
        public Panel LeaderboardWrapperContainer;
        #endregion

        #region Private Properties
        /// <summary>
        /// Hidden field that stores the type of leaderboard to render. 
        /// This just sits in the codebehind to be able to pass the selected leaderboard type to the rendering function. 
        /// This does not get attached to instansiating page's controls. 
        /// </summary>
        private HiddenField _LeaderboardType = new HiddenField();

        /// <summary>
        /// Hidden field that stores the start type that the leaderboard should render data for, for the selected leaderboard type.
        /// This just sits in the codebehind to be able to pass the selected leaderboard start type to the rendering function. 
        /// This does not get attached to instansiating page's controls. 
        /// </summary>
        private HiddenField _LeaderboardStartType = new HiddenField();
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_Leaderboards))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.TeamLeaderboards));
            this.BuildBreadcrumb(breadCrumbLinks);

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/leaderboards/Default.css");

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, _GlobalResources.TeamLeaderboardsTop100, ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD,
                                                                                                                                     ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.LeaderboardFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // load the leaderboard
            this._LoadLeaderboard();
        }
        #endregion

        #region _LoadLeaderboard
        /// <summary>
        /// 
        /// </summary>
        private void _LoadLeaderboard()
        {
            // clear the leaderboard wrapper container
            this.LeaderboardWrapperContainer.Controls.Clear();

            this.LeaderboardWrapperContainer.CssClass = "FormContentContainer";

            // make sure the leaderboard type is a valid one, if it's not, default to "enrollment_count"
            string leaderboardType = this._LeaderboardType.Value.ToString();

            if (String.IsNullOrWhiteSpace(leaderboardType)
                || (leaderboardType != "enrollment_count"
                && leaderboardType != "enrollment_credits"
                && leaderboardType != "certificate_count"
                && leaderboardType != "certificate_credits"))
            { leaderboardType = "enrollment_count"; }

            // make sure the leaderboard start type is a valid one, if it's not, default to "all_time"
            string leaderboardStartType = this._LeaderboardStartType.Value.ToString();

            if (String.IsNullOrWhiteSpace(leaderboardStartType)
                || (leaderboardStartType != "all_time"
                && leaderboardStartType != "this_year"
                && leaderboardStartType != "this_month"
                && leaderboardStartType != "this_week"))
            { leaderboardStartType = "all_time"; }

            // LEADERBOARD OPTIONS
            Panel leaderboardOptionsContainer = new Panel();
            leaderboardOptionsContainer.ID = "LeaderboardOptionsContainer";

            // leaderboard type selector
            DropDownList leaderboardTypeSelector = new DropDownList();
            leaderboardTypeSelector.ID = "LeaderboardTypeSelector";

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSETOTAL_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCourseTotal, "enrollment_count")); }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSECREDITS_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCourseCredits, "enrollment_credits")); }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATETOTAL_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCertificateTotal, "certificate_count")); }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATECREDITS_ENABLE))
            { leaderboardTypeSelector.Items.Add(new ListItem(_GlobalResources.ByCertificateCredits, "certificate_credits")); }

            leaderboardTypeSelector.AutoPostBack = true;
            leaderboardTypeSelector.SelectedIndexChanged += this._LeaderboardType_SelectedIndexChanged;
            leaderboardTypeSelector.SelectedValue = leaderboardType;

            leaderboardOptionsContainer.Controls.Add(leaderboardTypeSelector);

            // leaderboard start type selector
            DropDownList leaderboardStartTypeSelector = new DropDownList();
            leaderboardStartTypeSelector.ID = "LeaderboardStartTypeSelector";

            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.alltime_lower, "all_time"));
            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.thisyear_lower, "this_year"));
            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.thismonth_lower, "this_month"));
            leaderboardStartTypeSelector.Items.Add(new ListItem(_GlobalResources.thisweek_lower, "this_week"));

            leaderboardStartTypeSelector.AutoPostBack = true;
            leaderboardStartTypeSelector.SelectedIndexChanged += this._LeaderboardStartType_SelectedIndexChanged;
            leaderboardStartTypeSelector.SelectedValue = leaderboardStartType;

            leaderboardOptionsContainer.Controls.Add(leaderboardStartTypeSelector);

            // attach controls to container
            this.LeaderboardWrapperContainer.Controls.Add(leaderboardOptionsContainer);

            // DATA
            DataTable leaderboardDt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@numRecords", 100, SqlDbType.Int, 4, ParameterDirection.Input);

                if (leaderboardStartType == "all_time")
                { databaseObject.AddParameter("@dtStart", null, SqlDbType.DateTime, 8, ParameterDirection.Input); }
                else
                {
                    DateTime startDate = new DateTime();

                    if (leaderboardStartType == "this_year")
                    { startDate = new DateTime(AsentiaSessionState.UtcNow.Year, 1, 1); }
                    else if (leaderboardStartType == "this_month")
                    { startDate = new DateTime(AsentiaSessionState.UtcNow.Year, AsentiaSessionState.UtcNow.Month, 1); }
                    else if (leaderboardStartType == "this_week")
                    {
                        int dowStartDiff = AsentiaSessionState.UtcNow.DayOfWeek - DayOfWeek.Sunday;

                        if (dowStartDiff < 0)
                        { dowStartDiff += 7; }

                        startDate = AsentiaSessionState.UtcNow.AddDays(-1 * dowStartDiff).Date;
                    }
                    else
                    { }

                    databaseObject.AddParameter("@dtStart", startDate, SqlDbType.DateTime, 8, ParameterDirection.Input);
                }

                databaseObject.AddParameter("@leaderboardType", leaderboardType, SqlDbType.NVarChar, 20, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Site.GetLeaderboard]", true);

                // load leaderboard rs
                leaderboardDt.Load(sdr);

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            { throw; }
            finally
            { databaseObject.Dispose(); }

            // LEADERBOARD TABLE
            Panel leaderboardTableContainer = new Panel();
            leaderboardTableContainer.ID = "LeaderboardTableContainer";

            Table leaderboardTable = new Table();
            leaderboardTable.ID = "LeaderboardTable";
            leaderboardTable.CssClass = "GridTable";

            TableHeaderRow leaderboardTableHeaderRow = new TableHeaderRow();
            leaderboardTableHeaderRow.CssClass = "GridHeaderRow";

            TableHeaderCell leaderboardTableNumberHeaderCell = new TableHeaderCell();
            leaderboardTableNumberHeaderCell.CssClass = "centered";
            Label numberLabel = new Label();
            numberLabel.Text = _GlobalResources.NumberSymbol;
            leaderboardTableNumberHeaderCell.Controls.Add(numberLabel);
            leaderboardTableHeaderRow.Cells.Add(leaderboardTableNumberHeaderCell);

            TableHeaderCell leaderboardTableNameHeaderCell = new TableHeaderCell();
            Label nameLabel = new Label();
            nameLabel.Text = _GlobalResources.Name;
            leaderboardTableNameHeaderCell.Controls.Add(nameLabel);
            leaderboardTableHeaderRow.Cells.Add(leaderboardTableNameHeaderCell);

            TableHeaderCell leaderboardTableScoreHeaderCell = new TableHeaderCell();
            Label scoreLabel = new Label();
            scoreLabel.Text = _GlobalResources.Score;
            leaderboardTableScoreHeaderCell.Controls.Add(scoreLabel);
            leaderboardTableHeaderRow.Cells.Add(leaderboardTableScoreHeaderCell);

            leaderboardTable.Rows.Add(leaderboardTableHeaderRow);

            int i = 1;

            foreach (DataRow dr in leaderboardDt.Rows)
            {
                TableRow tr = new TableRow();

                // add css for alternating row
                if (i % 2 == 1)
                { tr.CssClass = "GridDataRow GridDataRowAlternate"; }
                else
                { tr.CssClass = "GridDataRow"; }

                TableCell positionCell = new TableCell();
                positionCell.CssClass = "centered";
                Label positionLabel = new Label();
                positionLabel.Text = dr["position"].ToString();
                positionCell.Controls.Add(positionLabel);
                tr.Cells.Add(positionCell);

                TableCell learnerNameCell = new TableCell();

                int position = Convert.ToInt32(dr["position"]);

                if (position <= 3)
                {
                    Image trophyImage = new Image();
                    trophyImage.CssClass = "XSmallIcon LeaderboardTrophyIcon";

                    switch (position)
                    {
                        case 1:
                            trophyImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD_GOLD, ImageFiles.EXT_PNG);
                            learnerNameCell.Controls.Add(trophyImage);
                            break;
                        case 2:
                            trophyImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD_SILVER, ImageFiles.EXT_PNG);
                            learnerNameCell.Controls.Add(trophyImage);
                            break;
                        case 3:
                            trophyImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD_BRONZE, ImageFiles.EXT_PNG);
                            learnerNameCell.Controls.Add(trophyImage);
                            break;
                        default:
                            break;
                    }
                }

                Label learnerNameLabel = new Label();
                learnerNameLabel.Text = dr["name"].ToString().Replace("[others]", _GlobalResources.others_lower);
                learnerNameCell.Controls.Add(learnerNameLabel);
                tr.Cells.Add(learnerNameCell);

                TableCell totalCell = new TableCell();
                Label totalLabel = new Label();
                totalLabel.Text = dr["total"].ToString();
                totalCell.Controls.Add(totalLabel);
                tr.Cells.Add(totalCell);

                leaderboardTable.Rows.Add(tr);
                i++;
            }

            TableRow topCoursesByCompletionsLastRow = new TableRow();
            leaderboardTable.Rows.Add(topCoursesByCompletionsLastRow);

            leaderboardTableContainer.Controls.Add(leaderboardTable);

            // attach table to widget
            this.LeaderboardWrapperContainer.Controls.Add(leaderboardTableContainer);
        }
        #endregion

        #region _LeaderboardType_SelectedIndexChanged
        private void _LeaderboardType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get the leaderboard type selector
            DropDownList leaderboardTypeSelector = (DropDownList)sender;

            // declare variable for leaderboard start type selector
            DropDownList leaderboardStartTypeSelector;

            if (leaderboardTypeSelector != null)
            {
                // set leaderboard type hidden field value
                this._LeaderboardType.Value = leaderboardTypeSelector.SelectedValue.ToString();

                // also get and set value for the start type selector
                leaderboardStartTypeSelector = (DropDownList)this.LeaderboardWrapperContainer.FindControl("LeaderboardStartTypeSelector");
                if (leaderboardStartTypeSelector != null)
                { this._LeaderboardStartType.Value = leaderboardStartTypeSelector.SelectedValue.ToString(); }

                // call leaderboard builder method
                this._LoadLeaderboard();
            }
        }
        #endregion

        #region _LeaderboardStartType_SelectedIndexChanged
        private void _LeaderboardStartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get the leaderboard start type selector
            DropDownList leaderboardStartTypeSelector = (DropDownList)sender;

            // declare variable for leaderboard type selector
            DropDownList leaderboardTypeSelector;

            if (leaderboardStartTypeSelector != null)
            {
                // set leaderboard start type hidden field value
                this._LeaderboardStartType.Value = leaderboardStartTypeSelector.SelectedValue.ToString();

                // also get and set value for the type selector
                leaderboardTypeSelector = (DropDownList)this.LeaderboardWrapperContainer.FindControl("LeaderboardTypeSelector");
                if (leaderboardTypeSelector != null)
                { this._LeaderboardType.Value = leaderboardTypeSelector.SelectedValue.ToString(); }

                // call leaderboard builder method
                this._LoadLeaderboard();
            }
        }
        #endregion
    }
}
