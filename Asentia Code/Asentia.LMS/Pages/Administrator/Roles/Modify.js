﻿/*

PAGE LOAD

*/

function pageLoad() {
    if ($('#' + IdRulesetModalHidden).val() == "" || $('#' + IdRulesetModalHidden).val() == "0") {
        $('#AddModifyRuleSetModalModalPopupHeaderText').text(HeaderTextAddRuleset);
    }

    // initialize permission checkbox states
    InitializePermissionsCheckBoxStates();

    // if this role is view only, disable all inputs for it
    if (IsRoleViewOnly) {
        $("#RoleName_Container input").prop("disabled", true);
        $("#RolePermissions_Container input").prop("disabled", true);
    }

}

/*

RULESET METHODS

*/

function OnRuleSetModifyClick(IdRuleset) {
    $('#' + IdRulesetModalHidden).val(IdRuleset);

    //click the add modify ruleset hidden link button to show modal
    document.getElementById(AddModifyRuleSetHiddenButton).click();

    if ($('#' + IdRulesetModalHidden).val() == "" || $('#' + IdRulesetModalHidden).val() == "0") {
        $('#AddModifyRuleSetModalModalPopupHeaderText').text(HeaderTextAddRuleset);
    }

    //clear the name field
    $("input[id^='" + RuleSetLabel_Field + "']").val("");

    // fire click for existing Ruleset to Modify else show as new RuleSet
    //click the hidden button to fill the controls on update modal
    document.getElementById(ModifyRuleSetModalButton).click();
    $('#AddModifyRuleSetModalModalPopupFeedbackContainer').removeClass("ModalFeedbackSuccess");
    $('#AddModifyRuleSetModalModalPopupFeedbackContainer').removeClass("ModalFeedbackError");
    var ruleRows = $("tr.RuleRow");

    if (ruleRows != null && ruleRows.length <= 1) {
        $(".RemoveRuleButton").hide();
    }
    return false;
}

function OnRuleSetModifyLoad(IdRuleset) {
    Helper.ToggleTab('RuleSets', 'RoleProperties');

    $('#' + IdRulesetModalHidden).val(IdRuleset);

    if ($('#' + IdRulesetModalHidden).val() == "" || $('#' + IdRulesetModalHidden).val() == "0") {
        $('#AddModifyRuleSetModalModalPopupHeaderText').text(HeaderTextAddRuleset);
    }

    //clear the name field
    $("input[id^='" + RuleSetLabel_Field + "']").val("");

    // fire click for existing Ruleset to Modify else show as new RuleSet
    //click the hidden button to fill the controls on update modal
    document.getElementById(ModifyRuleSetModalButton).click();
    $('#AddModifyRuleSetModalModalPopupFeedbackContainer').removeClass("ModalFeedbackSuccess");
    $('#AddModifyRuleSetModalModalPopupFeedbackContainer').removeClass("ModalFeedbackError");
    var ruleRows = $("tr.RuleRow");

    if (ruleRows != null && ruleRows.length <= 1) {
        $(".RemoveRuleButton").hide();
    }
    return false;
}


function HideModalPopup() {
    $("#AddModifyRuleSetModal").hide();
    $('#AddModifyRuleSetModalModalPopupExtender_backgroundElement').css("display", "none");
    return false;
}

/* 

PERMISSION CHECKBOXES AND OPTIONS

*/

var SystemPermissionsCheckBoxCheckAllId = "SystemPermissionsSelectAll";
var UsersAndGroupsPermissionsCheckBoxCheckAllId = "UsersAndGroupsPermissionsSelectAll";
var InterfaceAndLayoutPermissionsCheckBoxCheckAllId = "InterfaceAndLayoutPermissionsSelectAll";
var LearningAssetsPermissionsCheckBoxCheckAllId = "LearningAssetsPermissionsSelectAll";
var ReportingPermissionsCheckBoxCheckAllId = "ReportingPermissionsSelectAll";

var SystemPermissionsCheckBoxes = [];
SystemPermissionsCheckBoxes[0] = ["System_AccountSettings", false];
SystemPermissionsCheckBoxes[1] = ["System_Configuration", false];
SystemPermissionsCheckBoxes[2] = ["System_Ecommerce", false];
SystemPermissionsCheckBoxes[3] = ["System_CouponCodes", false];
SystemPermissionsCheckBoxes[4] = ["System_TrackingCodes", false];
SystemPermissionsCheckBoxes[5] = ["System_EmailNotifications", false];
SystemPermissionsCheckBoxes[6] = ["System_WebMeetingIntegration", false];
SystemPermissionsCheckBoxes[7] = ["System_API", false];
SystemPermissionsCheckBoxes[8] = ["System_xAPIEndpoints", false];
SystemPermissionsCheckBoxes[9] = ["System_Logs", false];
SystemPermissionsCheckBoxes[10] = ["System_UserFieldConfiguration", false];
SystemPermissionsCheckBoxes[11] = ["System_RulesEngine", false];

var UsersAndGroupsPermissionsCheckBoxes = [];
UsersAndGroupsPermissionsCheckBoxes[0] = ["UsersAndGroups_UserFieldConfiguration", false];
UsersAndGroupsPermissionsCheckBoxes[1] = ["UsersAndGroups_UserRegistrationApproval", false];
UsersAndGroupsPermissionsCheckBoxes[2] = ["UsersAndGroups_UserCreator", false];
UsersAndGroupsPermissionsCheckBoxes[3] = ["UsersAndGroups_UserDeleter", true];
UsersAndGroupsPermissionsCheckBoxes[4] = ["UsersAndGroups_UserEditor", true];
UsersAndGroupsPermissionsCheckBoxes[5] = ["UsersAndGroups_UserManager", true];
UsersAndGroupsPermissionsCheckBoxes[6] = ["UsersAndGroups_UserImpersonator", true];
UsersAndGroupsPermissionsCheckBoxes[7] = ["UsersAndGroups_GroupCreator", false];
UsersAndGroupsPermissionsCheckBoxes[8] = ["UsersAndGroups_GroupDeleter", false];
UsersAndGroupsPermissionsCheckBoxes[9] = ["UsersAndGroups_GroupEditor", true];
UsersAndGroupsPermissionsCheckBoxes[10] = ["UsersAndGroups_GroupManager", true];
UsersAndGroupsPermissionsCheckBoxes[11] = ["UsersAndGroups_ActivityImport", false];
UsersAndGroupsPermissionsCheckBoxes[12] = ["UsersAndGroups_CertificateImport", false];
UsersAndGroupsPermissionsCheckBoxes[13] = ["UsersAndGroups_Leaderboards", false];

var InterfaceAndLayoutPermissionsCheckBoxes = [];
InterfaceAndLayoutPermissionsCheckBoxes[0] = ["InterfaceAndLayout_HomePage", false];
InterfaceAndLayoutPermissionsCheckBoxes[1] = ["InterfaceAndLayout_Masthead", false];
InterfaceAndLayoutPermissionsCheckBoxes[2] = ["InterfaceAndLayout_Footer", false];
InterfaceAndLayoutPermissionsCheckBoxes[3] = ["InterfaceAndLayout_CSSEditor", false];
InterfaceAndLayoutPermissionsCheckBoxes[4] = ["InterfaceAndLayout_ImageEditor", false];

var LearningAssetsPermissionsCheckBoxes = [];
LearningAssetsPermissionsCheckBoxes[0] = ["LearningAssets_CourseCatalog", false];
LearningAssetsPermissionsCheckBoxes[1] = ["LearningAssets_SelfEnrollmentApproval", false];
LearningAssetsPermissionsCheckBoxes[2] = ["LearningAssets_CourseManager", false];
LearningAssetsPermissionsCheckBoxes[3] = ["LearningAssets_LearningPathManager", false];
LearningAssetsPermissionsCheckBoxes[4] = ["LearningAssets_CertificationsManager", false];
LearningAssetsPermissionsCheckBoxes[5] = ["LearningAssets_ContentPackageManager", false];
LearningAssetsPermissionsCheckBoxes[6] = ["LearningAssets_QuizAndSurveyManager", false];
LearningAssetsPermissionsCheckBoxes[7] = ["LearningAssets_CertificateTemplateManager", false];
LearningAssetsPermissionsCheckBoxes[8] = ["LearningAssets_InstructorLedTrainingManager", false];
LearningAssetsPermissionsCheckBoxes[9] = ["LearningAssets_ResourceManager", false];
LearningAssetsPermissionsCheckBoxes[10] = ["LearningAssets_CourseEnrollmentManager", false];
LearningAssetsPermissionsCheckBoxes[11] = ["LearningAssets_LearningPathEnrollmentManager", false];

var ReportingPermissionsCheckBoxes = [];
ReportingPermissionsCheckBoxes[0] = ["Reporting_Reporter_UserDemographics", true];
ReportingPermissionsCheckBoxes[1] = ["Reporting_Reporter_UserCourseTranscripts", true];
ReportingPermissionsCheckBoxes[2] = ["Reporting_Reporter_UserLearningPathTranscripts", true];
ReportingPermissionsCheckBoxes[3] = ["Reporting_Reporter_UserInstructorLedTrainingTranscripts", true];
ReportingPermissionsCheckBoxes[4] = ["Reporting_Reporter_CatalogAndCourseInformation", true];
ReportingPermissionsCheckBoxes[5] = ["Reporting_Reporter_Certificates", true];
//ReportingPermissionsCheckBoxes[6] = ["Reporting_Reporter_XAPI", true];
ReportingPermissionsCheckBoxes[6] = ["Reporting_Reporter_Purchases", true];
ReportingPermissionsCheckBoxes[7] = ["Reporting_Reporter_UserCertificationTranscripts", true];

function InitializePermissionsCheckBoxStates() {
    // SYSTEM
    for (var i = 0; i < SystemPermissionsCheckBoxes.length; i++) {
        TogglePermission(SystemPermissionsCheckBoxes[i][0], SystemPermissionsCheckBoxes, SystemPermissionsCheckBoxCheckAllId, SystemPermissionsCheckBoxes[i][1]);
    }

    // USERS AND GROUPS
    for (var i = 0; i < UsersAndGroupsPermissionsCheckBoxes.length; i++) {
        TogglePermission(UsersAndGroupsPermissionsCheckBoxes[i][0], UsersAndGroupsPermissionsCheckBoxes, UsersAndGroupsPermissionsCheckBoxCheckAllId, UsersAndGroupsPermissionsCheckBoxes[i][1]);
    }

    // INTERFACE AND LAYOUT
    for (var i = 0; i < InterfaceAndLayoutPermissionsCheckBoxes.length; i++) {
        TogglePermission(InterfaceAndLayoutPermissionsCheckBoxes[i][0], InterfaceAndLayoutPermissionsCheckBoxes, InterfaceAndLayoutPermissionsCheckBoxCheckAllId, InterfaceAndLayoutPermissionsCheckBoxes[i][1]);
    }

    // LEARNING ASSETS
    for (var i = 0; i < LearningAssetsPermissionsCheckBoxes.length; i++) {
        TogglePermission(LearningAssetsPermissionsCheckBoxes[i][0], LearningAssetsPermissionsCheckBoxes, LearningAssetsPermissionsCheckBoxCheckAllId, LearningAssetsPermissionsCheckBoxes[i][1]);
    }

    // REPORTING
    for (var i = 0; i < ReportingPermissionsCheckBoxes.length; i++) {
        TogglePermission(ReportingPermissionsCheckBoxes[i][0], ReportingPermissionsCheckBoxes, ReportingPermissionsCheckBoxCheckAllId, ReportingPermissionsCheckBoxes[i][1]);
    }
}

function ToggleSelectAllPermissionsForCategory(selectAllCheckboxId, permissionCheckBoxIdsArray) {
    if ($("#" + selectAllCheckboxId).is(":checked")) {
        for (var i = 0; i < permissionCheckBoxIdsArray.length; i++) {
            $("#" + permissionCheckBoxIdsArray[i][0]).prop("checked", true);

            if (permissionCheckBoxIdsArray[i][1] == true) {
                ToggleGlobalScopeOptionsForPermission(permissionCheckBoxIdsArray[i][0]);
            }
        }
    }
    else {
        for (var i = 0; i < permissionCheckBoxIdsArray.length; i++) {
            $("#" + permissionCheckBoxIdsArray[i][0]).prop("checked", false);

            if (permissionCheckBoxIdsArray[i][1] == true) {
                ToggleGlobalScopeOptionsForPermission(permissionCheckBoxIdsArray[i][0]);
            }
        }
    }
}

function TogglePermission(permissionCheckBoxId, permissionCheckBoxIdsArray, selectAllCheckboxId, hasScope) {
    var allCategoryCheckboxesChecked = true;

    for (var i = 0; i < permissionCheckBoxIdsArray.length; i++) {
        if (!$("#" + permissionCheckBoxIdsArray[i][0]).is(":checked")) {
            allCategoryCheckboxesChecked = false;
            break;
        }
    }

    $("#" + selectAllCheckboxId).prop("checked", allCategoryCheckboxesChecked);

    if (hasScope) {
        ToggleGlobalScopeOptionsForPermission(permissionCheckBoxId);
    }
}

function ToggleGlobalScopeOptionsForPermission(permissionCheckBoxId)
{
    if ($("#" + permissionCheckBoxId).is(":checked")) {
        $("#" + permissionCheckBoxId + "_ScopeType_0").prop("disabled", false);
        $("#" + permissionCheckBoxId + "_ScopeType_1").prop("disabled", false);

        if (!$("#" + permissionCheckBoxId + "_ScopeType_0").is(":checked") && !$("#" + permissionCheckBoxId + "_ScopeType_1").is(":checked")) {
            $("#" + permissionCheckBoxId + "_ScopeType_0").prop("checked", true);
        }

        if ($("#" + permissionCheckBoxId + "_ScopeType_1").is(":checked")) {
            $("#" + permissionCheckBoxId + "_ScopedItemsContainer input").each(function () {
                $(this).prop("disabled", false);
            });

            $("#" + permissionCheckBoxId + "_SelectAllScope").prop("disabled", false);
            $("#" + permissionCheckBoxId + "_SelectNoneScope").prop("disabled", false);
        }

        if ($("#" + permissionCheckBoxId + "_ScopeType_0").is(":checked")) {
            $("#" + permissionCheckBoxId + "_ScopedItemsContainer input").each(function () {
                $(this).prop("disabled", true);
                $(this).prop("checked", false);
            });
        }
    }
    else {
        $("#" + permissionCheckBoxId + "_ScopeType_0").prop("disabled", true);
        $("#" + permissionCheckBoxId + "_ScopeType_0").prop("checked", false);
        $("#" + permissionCheckBoxId + "_ScopeType_1").prop("disabled", true);
        $("#" + permissionCheckBoxId + "_ScopeType_1").prop("checked", false);

        $("#" + permissionCheckBoxId + "_ScopedItemsContainer input").each(function () {
            $(this).prop("disabled", true);
            $(this).prop("checked", false);
        });
    }
}

function SelectScopesForPermission(permissionCheckBoxId, isSelectAll)
{
    if (isSelectAll) {
        $("#" + permissionCheckBoxId + "_ScopedItemsContainer input").each(function () {
            if (!$(this).prop("disabled")) {
                $(this).prop("checked", true);
            }
        });
    }
    else {
        $("#" + permissionCheckBoxId + "_ScopedItemsContainer input").each(function () {
            if (!$(this).prop("disabled")) {
                $(this).prop("checked", false);
            }
        });
    }
}

function PopulateHiddenFieldsForPermissionScopes() {
    // SYSTEM
    for (var i = 0; i < SystemPermissionsCheckBoxes.length; i++) {
        if (SystemPermissionsCheckBoxes[i][1]) {
            $("#" + SystemPermissionsCheckBoxes[i][0] + "_ScopedItemsHiddenField").val("");

            if ($("#" + SystemPermissionsCheckBoxes[i][0] + "_ScopeType_1").is(":checked")) {
                var scopedItems = "";

                $("#" + SystemPermissionsCheckBoxes[i][0] + "_ScopedItemsContainer input").each(function () {
                    if (!$(this).prop("disabled") && $(this).is(":checked")) {
                        scopedItems = scopedItems + $(this).prop("id").replace(SystemPermissionsCheckBoxes[i][0] + "_Scope_", "") + ",";
                    }
                });

                if (scopedItems.length > 0)
                { scopedItems = scopedItems.substring(0, scopedItems.length - 1); }

                $("#" + SystemPermissionsCheckBoxes[i][0] + "_ScopedItemsHiddenField").val(scopedItems);
            }
        }
    }

    // USERS AND GROUPS
    for (var i = 0; i < UsersAndGroupsPermissionsCheckBoxes.length; i++) {
        if (UsersAndGroupsPermissionsCheckBoxes[i][1]) {
            $("#" + UsersAndGroupsPermissionsCheckBoxes[i][0] + "_ScopedItemsHiddenField").val("");

            if ($("#" + UsersAndGroupsPermissionsCheckBoxes[i][0] + "_ScopeType_1").is(":checked")) {
                var scopedItems = "";

                $("#" + UsersAndGroupsPermissionsCheckBoxes[i][0] + "_ScopedItemsContainer input").each(function () {
                    if (!$(this).prop("disabled") && $(this).is(":checked")) {
                        scopedItems = scopedItems + $(this).prop("id").replace(UsersAndGroupsPermissionsCheckBoxes[i][0] + "_Scope_", "") + ",";
                    }
                });

                if (scopedItems.length > 0)
                { scopedItems = scopedItems.substring(0, scopedItems.length - 1); }

                $("#" + UsersAndGroupsPermissionsCheckBoxes[i][0] + "_ScopedItemsHiddenField").val(scopedItems);
            }
        }
    }

    // INTERFACE AND LAYOUT
    for (var i = 0; i < InterfaceAndLayoutPermissionsCheckBoxes.length; i++) {
        if (InterfaceAndLayoutPermissionsCheckBoxes[i][1]) {
            $("#" + InterfaceAndLayoutPermissionsCheckBoxes[i][0] + "_ScopedItemsHiddenField").val("");

            if ($("#" + InterfaceAndLayoutPermissionsCheckBoxes[i][0] + "_ScopeType_1").is(":checked")) {
                var scopedItems = "";

                $("#" + InterfaceAndLayoutPermissionsCheckBoxes[i][0] + "_ScopedItemsContainer input").each(function () {
                    if (!$(this).prop("disabled") && $(this).is(":checked")) {
                        scopedItems = scopedItems + $(this).prop("id").replace(InterfaceAndLayoutPermissionsCheckBoxes[i][0] + "_Scope_", "") + ",";
                    }
                });

                if (scopedItems.length > 0)
                { scopedItems = scopedItems.substring(0, scopedItems.length - 1); }

                $("#" + InterfaceAndLayoutPermissionsCheckBoxes[i][0] + "_ScopedItemsHiddenField").val(scopedItems);
            }
        }
    }

    // LEARNING ASSETS
    for (var i = 0; i < LearningAssetsPermissionsCheckBoxes.length; i++) {
        if (LearningAssetsPermissionsCheckBoxes[i][1]) {
            $("#" + LearningAssetsPermissionsCheckBoxes[i][0] + "_ScopedItemsHiddenField").val("");

            if ($("#" + LearningAssetsPermissionsCheckBoxes[i][0] + "_ScopeType_1").is(":checked")) {
                var scopedItems = "";

                $("#" + LearningAssetsPermissionsCheckBoxes[i][0] + "_ScopedItemsContainer input").each(function () {
                    if (!$(this).prop("disabled") && $(this).is(":checked")) {
                        scopedItems = scopedItems + $(this).prop("id").replace(LearningAssetsPermissionsCheckBoxes[i][0] + "_Scope_", "") + ",";
                    }
                });

                if (scopedItems.length > 0)
                { scopedItems = scopedItems.substring(0, scopedItems.length - 1); }

                $("#" + LearningAssetsPermissionsCheckBoxes[i][0] + "_ScopedItemsHiddenField").val(scopedItems);
            }
        }
    }

    // REPORTING
    for (var i = 0; i < ReportingPermissionsCheckBoxes.length; i++) {
        if (ReportingPermissionsCheckBoxes[i][1]) {
            $("#" + ReportingPermissionsCheckBoxes[i][0] + "_ScopedItemsHiddenField").val("");

            if ($("#" + ReportingPermissionsCheckBoxes[i][0] + "_ScopeType_1").is(":checked")) {
                var scopedItems = "";

                $("#" + ReportingPermissionsCheckBoxes[i][0] + "_ScopedItemsContainer input").each(function () {
                    if (!$(this).prop("disabled") && $(this).is(":checked")) {
                        scopedItems = scopedItems + $(this).prop("id").replace(ReportingPermissionsCheckBoxes[i][0] + "_Scope_", "") + ",";
                    }
                });

                if (scopedItems.length > 0)
                { scopedItems = scopedItems.substring(0, scopedItems.length - 1); }

                $("#" + ReportingPermissionsCheckBoxes[i][0] + "_ScopedItemsHiddenField").val(scopedItems);
            }
        }
    }
}