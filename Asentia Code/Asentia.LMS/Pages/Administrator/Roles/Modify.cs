﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.Roles
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel RolePropertiesFeedbackContainer;
        public Panel RolePropertiesInstructionsPanel;
        public Panel RolePropertiesContainer;
        public Panel RolePropertiesActionsPanel;
        public Panel RolePropertiesTabPanelsContainer;
        #endregion

        #region Private Properties
        private EcommerceSettings _EcommerceSettings;
        private Role _RoleObject;
        private bool _IsViewOnly = false;
        private TextBox _Name;
        private Button _RolePropertiesSaveButton;
        private Button _RolePropertiesCancelButton;

        // ROLE PERMISSIONS
        private CheckBox _System_AccountSettings;
        private CheckBox _System_Configuration;
        private CheckBox _System_UserFieldConfiguration;
        private CheckBox _System_Ecommerce;
        private CheckBox _System_CouponCodes;
        private CheckBox _System_TrackingCodes;
        private CheckBox _System_EmailNotifications;
        private CheckBox _System_API;
        private CheckBox _System_xAPIEndpoints;
        private CheckBox _System_Logs;
        private CheckBox _System_WebMeetingIntegration;
        private CheckBox _System_RulesEngine;

        private CheckBox _UsersAndGroups_UserCreator;
        private CheckBox _UsersAndGroups_UserDeleter;
        private CheckBox _UsersAndGroups_UserEditor;
        private CheckBox _UsersAndGroups_UserManager;
        private CheckBox _UsersAndGroups_UserImpersonator;
        private CheckBox _UsersAndGroups_GroupCreator;
        private CheckBox _UsersAndGroups_GroupDeleter;
        private CheckBox _UsersAndGroups_GroupEditor;
        private CheckBox _UsersAndGroups_GroupManager;
        private CheckBox _UsersAndGroups_RoleManager;
        private CheckBox _UsersAndGroups_ActivityImport;
        private CheckBox _UsersAndGroups_CertificateImport;
        private CheckBox _UsersAndGroups_Leaderboards;
        private CheckBox _UsersAndGroups_UserRegistrationApproval;

        private CheckBox _InterfaceAndLayout_HomePage;
        private CheckBox _InterfaceAndLayout_Masthead;
        private CheckBox _InterfaceAndLayout_Footer;
        private CheckBox _InterfaceAndLayout_CSSEditor;
        private CheckBox _InterfaceAndLayout_ImageEditor;

        private CheckBox _LearningAssets_CourseCatalog;
        private CheckBox _LearningAssets_CourseManager;
        private CheckBox _LearningAssets_LearningPathManager;
        private CheckBox _LearningAssets_CertificationsManager;
        private CheckBox _LearningAssets_ContentPackageManager;
        private CheckBox _LearningAssets_QuizAndSurveyManager;
        private CheckBox _LearningAssets_CertificateTemplateManager;
        private CheckBox _LearningAssets_InstructorLedTrainingManager;
        private CheckBox _LearningAssets_ResourceManager;
        private CheckBox _LearningAssets_SelfEnrollmentApproval;
        private CheckBox _LearningAssets_CourseEnrollmentManager;
        private CheckBox _LearningAssets_LearningPathEnrollmentManager;

        private CheckBox _Reporting_Reporter_UserDemographics;
        private CheckBox _Reporting_Reporter_UserCourseTranscripts;
        private CheckBox _Reporting_Reporter_UserLearningPathTranscripts;
        private CheckBox _Reporting_Reporter_UserInstructorLedTrainingTranscripts;
        private CheckBox _Reporting_Reporter_CatalogAndCourseInformation;
        private CheckBox _Reporting_Reporter_Certificates;
        private CheckBox _Reporting_Reporter_XAPI;
        private CheckBox _Reporting_Reporter_Purchases;
        private CheckBox _Reporting_Reporter_UserCertificationTranscripts;

        // RULESETS
        private Grid _RuleSetsGrid;
        private ModalPopup _AddModifyRuleSetModal;
        private Button _AddModifyRuleSetModalButton;
        private Button _SubmitButtonMadal;
        private Button _CloseButtonMadal;
        private Panel _RuleSetModalPropertiesContainer;
        private TextBox _RuleSetLabel;
        private RadioButton _MatchAny;
        private RadioButton _MatchAll;
        private RuleSet _RuleSetObject;
        private HiddenField _RulesData;
        private PlaceHolder _RuleSetControlPlaceHolder;
        private HiddenField _IdRulesetModalHidden;
        private LinkButton _AddModifyRuleSetHiddenButton;
        private bool _RuleSetControlLoaded;
        private HiddenField _IdRoleHiddenField;
        private AutoJoinRuleSet _AutoJoinRuleSet;
        private DataTable _Rules;
        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmAction;

        private HiddenField _ModifyRulesetId;
        #endregion

        #region Page Init Event
        public void Page_Init(object sender, EventArgs e)
        {
            this._RulesData = new HiddenField();
            this._IdRulesetModalHidden = new HiddenField();
            this._AddModifyRuleSetHiddenButton = new LinkButton();
            this._AddModifyRuleSetModalButton = new Button();
            this._RulesData.ID = "RulesData";

            this._RuleSetControlPlaceHolder = new PlaceHolder();

            this._RuleSetControlLoaded = false;
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.Roles.Modify.js");

            

            if (!this._IsViewOnly && this._RoleObject != null)
            {
                csm.RegisterStartupScript(typeof(Modify), "JsVariables", "var HeaderTextAddRuleset='" + _GlobalResources.NewRuleset
                   + "';var IdRulesetModalHidden ='" + this._IdRulesetModalHidden.ID
                   + "';var AddModifyRuleSetHiddenButton ='" + this._AddModifyRuleSetHiddenButton.ID
                   + "';var ModifyRuleSetModalButton ='" + this._AddModifyRuleSetModalButton.ID
                   + "';var RuleSetLabel_Field ='" + this._RuleSetLabel.ID
                   + "';var IsRoleViewOnly =" + this._IsViewOnly.ToString().ToLower()
                   + ";", true);
            }
            else
            {
                csm.RegisterStartupScript(typeof(Modify), "JsVariables", "var HeaderTextAddRuleset='" + _GlobalResources.NewRuleset
                   + "';var IdRulesetModalHidden ='"
                   + "';var AddModifyRuleSetHiddenButton ='"
                   + "';var ModifyRuleSetModalButton ='"
                   + "';var RuleSetLabel_Field ='"
                   + "';var IsRoleViewOnly =" + this._IsViewOnly.ToString().ToLower()
                   + ";", true);
            }


            StringBuilder startUpCallsScript = new StringBuilder();

            if (this._ModifyRulesetId != null)
            {
                if (this._ModifyRulesetId.Value != String.Empty)
                {
                    this._AddModifyRuleSetModal.ShowModal();
                    startUpCallsScript.AppendLine("OnRuleSetModifyLoad('" + this._ModifyRulesetId.Value + "');");
                }
            }

            csm.RegisterStartupScript(typeof(Modify), "startUpCalls", startUpCallsScript.ToString(), true);

            if (!this._RuleSetControlLoaded)
            {
                if (this._RuleSetObject == null)
                {
                    this._AutoJoinRuleSet = new AutoJoinRuleSet();
                }
                else
                {
                    this._AutoJoinRuleSet = new AutoJoinRuleSet(this._RuleSetObject.Id);
                }

                this._AutoJoinRuleSet.ID = "Rule_Field";
                this._AutoJoinRuleSet.ClientIDMode = ClientIDMode.Static;

                this._RuleSetControlPlaceHolder.Controls.Clear();
                this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);
                EnsureChildControls();
            }
        }
        #endregion

        #region Page Load
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/roles/Modify.css");

            // get ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // get the role object
            this._GetRoleObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetRoleObject
        /// <summary>
        /// Gets a role object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetRoleObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { 
                        this._RoleObject = new Role(id);

                        // if the role id is less than 100, it is a system role and is therefore view only
                        if (this._RoleObject.Id < 100)
                        { this._IsViewOnly = true; }
                    }
                }
                catch
                { Response.Redirect("~/administrator/roles"); }
            }
        }
        #endregion

        #region _BuildControls
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the role properties form
            this._BuildRolePropertiesForm();

            // build the role properties form actions panel
            this._BuildRolePropertiesActionsPanel();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string roleImagePath;
            string imageCssClass = null;
            string pageTitle;

            roleImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION,
                                                   ImageFiles.EXT_PNG);

            if (this._RoleObject != null)
            {
                string roleTitleInInterfaceLanguage = this._RoleObject.Name;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (Role.LanguageSpecificProperty roleLanguageSpecificProperty in this._RoleObject.LanguageSpecificProperties)
                    {
                        if (roleLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { roleTitleInInterfaceLanguage = roleLanguageSpecificProperty.Name; }
                    }
                }

                breadCrumbPageTitle = roleTitleInInterfaceLanguage;
                pageTitle = roleTitleInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewRole;
                pageTitle = _GlobalResources.NewRole;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Roles, "/administrator/roles"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, pageTitle, roleImagePath, imageCssClass);
        }
        #endregion

        #region _BuildRolePropertiesForm
        /// <summary>
        /// Builds the role properties form.
        /// </summary>
        private void _BuildRolePropertiesForm()
        {
            // format a page information panel with page instructions
            if (this._IsViewOnly)
            { this.FormatPageInformationPanel(this.RolePropertiesInstructionsPanel, _GlobalResources.ThisRoleIsABuiltInSystemRoleAndCannotBeModified, true); }
            else
            { this.FormatPageInformationPanel(this.RolePropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisRole, true); }

            // clear controls from container
            this.RolePropertiesContainer.Controls.Clear();

            // set css class for container
            this.RolePropertiesContainer.CssClass = "FormContentContainer";

            // build the role properties form tabs
            this._BuildRolePropertiesFormTabs();

            this.RolePropertiesTabPanelsContainer = new Panel();
            this.RolePropertiesTabPanelsContainer.ID = "RoleProperties_TabPanelsContainer";
            this.RolePropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.RolePropertiesContainer.Controls.Add(this.RolePropertiesTabPanelsContainer);

            // build the "properties" panel of the role properties form
            this._BuildRolePropertiesFormPropertiesPanel();

            if (this._RoleObject != null && !this._IsViewOnly)
            {
                // build the rulesets form
                this._BuildRolePropertiesFormRulesetsPanel();
            }

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulateRolePropertiesInputElements();
        }
        #endregion

        #region _BuildRolePropertiesFormTabs
        /// <summary>
        /// 
        /// </summary>
        private void _BuildRolePropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));

            if (this._RoleObject != null && !this._IsViewOnly && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ROLESRULES_ENABLE))
            { tabs.Enqueue(new KeyValuePair<string, string>("RuleSets", _GlobalResources.Rulesets)); }

            // build and attach the tabs
            this.RolePropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("RoleProperties", tabs));
        }
        #endregion

        #region _BuildRolePropertiesFormPropertiesPanel
        /// <summary>
        /// 
        /// </summary>
        private void _BuildRolePropertiesFormPropertiesPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "RoleProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.Attributes.Add("style", "display: block;");

            // ruleset enrollment label field
            this._Name = new TextBox();
            this._Name.ID = "RoleName_Field";
            this._Name.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RoleName",
                                                             _GlobalResources.Name,
                                                             this._Name.ID,
                                                             this._Name,
                                                             true,
                                                             true,
                                                             true));

            // get the groups list for group scoped permissions
            DataTable groups = Group.IdsAndNamesForPermissionScopeList();

            // role permissions
            List<Control> rolePermissionsInputControls = new List<Control>();

            // build and attach permissions containers
            rolePermissionsInputControls.Add(this._BuildSystemPermissionsContainer(groups));
            rolePermissionsInputControls.Add(this._BuildUsersAndGroupsPermissionsContainer(groups));
            rolePermissionsInputControls.Add(this._BuildInterfaceAndLayoutPermissionsContainer(groups));
            rolePermissionsInputControls.Add(this._BuildLearningAssetsPermissionsContainer(groups));
            rolePermissionsInputControls.Add(this._BuildReportingPermissionsContainer(groups));

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("RolePermissions",
                                                                                        _GlobalResources.Permissions,
                                                                                        rolePermissionsInputControls,
                                                                                        false,
                                                                                        true));

            // attach panel to container
            this.RolePropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildSystemPermissionsContainer
        /// <summary>
        /// Builds the container for System permission checkboxes/options.
        /// </summary>
        /// <returns></returns>
        private Panel _BuildSystemPermissionsContainer(DataTable groups)
        {
            // container
            Panel permissionsPanel = new Panel();
            permissionsPanel.ID = "SystemPermissionsContainer";
            permissionsPanel.CssClass = "PermissionsContainer";

            // table
            Table permissionsTable = new Table();

            // header row
            TableRow headerRow = new TableRow();

            // header cell
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = 2;

            // header image and label
            Panel categoryHeaderContainer = new Panel();
            categoryHeaderContainer.CssClass = "PermissionsCategoryHeaderContainer";

            Image categoryImage = new Image();
            categoryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM, ImageFiles.EXT_PNG);
            categoryImage.CssClass = "SmallIcon";
            categoryHeaderContainer.Controls.Add(categoryImage);

            Label categoryLabel = new Label();
            categoryLabel.Text = _GlobalResources.System;
            categoryHeaderContainer.Controls.Add(categoryLabel);

            headerCell.Controls.Add(categoryHeaderContainer);

            // select all checkbox
            Panel categorySelectAllCheckboxContainer = new Panel();
            categorySelectAllCheckboxContainer.CssClass = "PermissionsCategorySelectAllCheckboxContainer";

            CheckBox categorySelectAllCheckbox = new CheckBox();
            categorySelectAllCheckbox.ID = "SystemPermissionsSelectAll";
            categorySelectAllCheckbox.Text = _GlobalResources.Selectall;
            categorySelectAllCheckbox.Attributes.Add("onclick", "ToggleSelectAllPermissionsForCategory(\"" + categorySelectAllCheckbox.ID + "\", SystemPermissionsCheckBoxes);");
            categorySelectAllCheckboxContainer.Controls.Add(categorySelectAllCheckbox);

            headerCell.Controls.Add(categorySelectAllCheckboxContainer);

            headerRow.Cells.Add(headerCell);
            permissionsTable.Rows.Add(headerRow);

            // permissions checkbox rows

            // account settings
            TableRow row1 = new TableRow();
            TableCell row1Cell1 = new TableCell();
            TableCell row1Cell2 = new TableCell();

            this._System_AccountSettings = new CheckBox();
            this._System_AccountSettings.ID = "System_AccountSettings";
            this._System_AccountSettings.Text = _GlobalResources.AccountSettingsAllowsAccessToConfigureSettingsForThePortal;
            this._System_AccountSettings.Attributes.Add("onclick", "TogglePermission(\"" + this._System_AccountSettings.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
            row1Cell1.Controls.Add(this._System_AccountSettings);

            Panel row1Cell2GlobalLabelContainer = new Panel();
            Literal row1Cell2GlobalLabel = new Literal();
            row1Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row1Cell2GlobalLabelContainer.Controls.Add(row1Cell2GlobalLabel);
            row1Cell2.Controls.Add(row1Cell2GlobalLabelContainer);

            row1.Cells.Add(row1Cell1);
            row1.Cells.Add(row1Cell2);
            permissionsTable.Rows.Add(row1);

            // configuration
            TableRow row2 = new TableRow();
            TableCell row2Cell1 = new TableCell();
            TableCell row2Cell2 = new TableCell();

            this._System_Configuration = new CheckBox();
            this._System_Configuration.ID = "System_Configuration";
            this._System_Configuration.Text = _GlobalResources.ConfigurationAllowsAccessToConfigurationItemsForLoginWidgetsCatalogCourseLeaderboardsAndCourseRatings;
            this._System_Configuration.Attributes.Add("onclick", "TogglePermission(\"" + this._System_Configuration.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
            row2Cell1.Controls.Add(this._System_Configuration);

            Panel row2Cell2GlobalLabelContainer = new Panel();
            Literal row2Cell2GlobalLabel = new Literal();
            row2Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row2Cell2GlobalLabelContainer.Controls.Add(row2Cell2GlobalLabel);
            row2Cell2.Controls.Add(row2Cell2GlobalLabelContainer);

            row2.Cells.Add(row2Cell1);
            row2.Cells.Add(row2Cell2);
            permissionsTable.Rows.Add(row2);

            // user field configuration
            TableRow row10 = new TableRow();
            TableCell row10Cell1 = new TableCell();
            TableCell row10Cell2 = new TableCell();

            this._System_UserFieldConfiguration = new CheckBox();
            this._System_UserFieldConfiguration.ID = "System_UserFieldConfiguration";
            this._System_UserFieldConfiguration.Text = _GlobalResources.UserFieldConfigurationAllowsAccessToConfigureUserFieldPropertiesForUserProfiles;
            this._System_UserFieldConfiguration.Attributes.Add("onclick", "TogglePermission(\"" + this._System_UserFieldConfiguration.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
            row10Cell1.Controls.Add(this._System_UserFieldConfiguration);

            Panel row10Cell2GlobalLabelContainer = new Panel();
            Literal row10Cell2GlobalLabel = new Literal();
            row10Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row10Cell2GlobalLabelContainer.Controls.Add(row10Cell2GlobalLabel);
            row10Cell2.Controls.Add(row10Cell2GlobalLabelContainer);

            row10.Cells.Add(row10Cell1);
            row10.Cells.Add(row10Cell2);
            permissionsTable.Rows.Add(row10);

            // rules engine
            TableRow row12 = new TableRow();
            TableCell row12Cell1 = new TableCell();
            TableCell row12Cell2 = new TableCell();

            this._System_RulesEngine = new CheckBox();
            this._System_RulesEngine.ID = "System_RulesEngine";
            this._System_RulesEngine.Text = _GlobalResources.RulesEngineAllowsAccessToRulesEngineOverview;
            this._System_RulesEngine.Attributes.Add("onclick", "TogglePermission(\"" + this._System_RulesEngine.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
            row12Cell1.Controls.Add(this._System_RulesEngine);

            Panel row12Cell2GlobalLabelContainer = new Panel();
            Literal row12Cell2GlobalLabel = new Literal();
            row12Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row12Cell2GlobalLabelContainer.Controls.Add(row12Cell2GlobalLabel);
            row12Cell2.Controls.Add(row12Cell2GlobalLabelContainer);

            row12.Cells.Add(row12Cell1);
            row12.Cells.Add(row12Cell2);
            permissionsTable.Rows.Add(row12);

            // ecommerce
            TableRow row3 = new TableRow();
            TableCell row3Cell1 = new TableCell();
            TableCell row3Cell2 = new TableCell();

            this._System_Ecommerce = new CheckBox();
            this._System_Ecommerce.ID = "System_Ecommerce";
            this._System_Ecommerce.Text = _GlobalResources.ECommerceAllowsAccessToEcommerceSettingsForThePortal;
            this._System_Ecommerce.Attributes.Add("onclick", "TogglePermission(\"" + this._System_Ecommerce.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
            row3Cell1.Controls.Add(this._System_Ecommerce);

            Panel row3Cell2GlobalLabelContainer = new Panel();
            Literal row3Cell2GlobalLabel = new Literal();
            row3Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row3Cell2GlobalLabelContainer.Controls.Add(row3Cell2GlobalLabel);
            row3Cell2.Controls.Add(row3Cell2GlobalLabelContainer);

            row3.Cells.Add(row3Cell1);
            row3.Cells.Add(row3Cell2);
            permissionsTable.Rows.Add(row3);
            
            // coupon codes
            TableRow row4 = new TableRow();
            TableCell row4Cell1 = new TableCell();
            TableCell row4Cell2 = new TableCell();

            this._System_CouponCodes = new CheckBox();
            this._System_CouponCodes.ID = "System_CouponCodes";
            this._System_CouponCodes.Text = _GlobalResources.CouponCodesAllowsAccessToCreateAndModifyCouponCodes;
            this._System_CouponCodes.Attributes.Add("onclick", "TogglePermission(\"" + this._System_CouponCodes.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
            row4Cell1.Controls.Add(this._System_CouponCodes);

            Panel row4Cell2GlobalLabelContainer = new Panel();
            Literal row4Cell2GlobalLabel = new Literal();
            row4Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row4Cell2GlobalLabelContainer.Controls.Add(row4Cell2GlobalLabel);
            row4Cell2.Controls.Add(row4Cell2GlobalLabelContainer);

            row4.Cells.Add(row4Cell1);
            row4.Cells.Add(row4Cell2);
            permissionsTable.Rows.Add(row4);
            
            // tracking codes
            TableRow row5 = new TableRow();
            TableCell row5Cell1 = new TableCell();
            TableCell row5Cell2 = new TableCell();

            this._System_TrackingCodes = new CheckBox();
            this._System_TrackingCodes.ID = "System_TrackingCodes";
            this._System_TrackingCodes.Text = _GlobalResources.TrackingCodesAllowsAccessToConfigureGoogleAnalyticsCode;
            this._System_TrackingCodes.Attributes.Add("onclick", "TogglePermission(\"" + this._System_TrackingCodes.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
            row5Cell1.Controls.Add(this._System_TrackingCodes);

            Panel row5Cell2GlobalLabelContainer = new Panel();
            Literal row5Cell2GlobalLabel = new Literal();
            row5Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row5Cell2GlobalLabelContainer.Controls.Add(row5Cell2GlobalLabel);
            row5Cell2.Controls.Add(row5Cell2GlobalLabelContainer);

            row5.Cells.Add(row5Cell1);
            row5.Cells.Add(row5Cell2);
            permissionsTable.Rows.Add(row5);
            
            // email notifications
            TableRow row6 = new TableRow();
            TableCell row6Cell1 = new TableCell();
            TableCell row6Cell2 = new TableCell();

            this._System_EmailNotifications = new CheckBox();
            this._System_EmailNotifications.ID = "System_EmailNotifications";
            this._System_EmailNotifications.Text = _GlobalResources.EmailNotificationsAllowsAccessToCreateAndModifyEmailNotificationsAtTheSystemLevel;
            this._System_EmailNotifications.Attributes.Add("onclick", "TogglePermission(\"" + this._System_EmailNotifications.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
            row6Cell1.Controls.Add(this._System_EmailNotifications);

            Panel row6Cell2GlobalLabelContainer = new Panel();
            Literal row6Cell2GlobalLabel = new Literal();
            row6Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row6Cell2GlobalLabelContainer.Controls.Add(row6Cell2GlobalLabel);
            row6Cell2.Controls.Add(row6Cell2GlobalLabelContainer);

            row6.Cells.Add(row6Cell1);
            row6.Cells.Add(row6Cell2);
            permissionsTable.Rows.Add(row6);
            
            // web meeting integration
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE)
                || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE)
                || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE)
                || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE))
            {
                TableRow row11 = new TableRow();
                TableCell row11Cell1 = new TableCell();
                TableCell row11Cell2 = new TableCell();

                this._System_WebMeetingIntegration = new CheckBox();
                this._System_WebMeetingIntegration.ID = "System_WebMeetingIntegration";
                this._System_WebMeetingIntegration.Text = _GlobalResources.WebMeetingIntegrationAllowsAccessToConfigurationSettingsForIntegrationWithWebMeetingSoftware;
                this._System_WebMeetingIntegration.Attributes.Add("onclick", "TogglePermission(\"" + this._System_WebMeetingIntegration.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
                row10Cell1.Controls.Add(this._System_WebMeetingIntegration);

                Panel row11Cell2GlobalLabelContainer = new Panel();
                Literal row11Cell2GlobalLabel = new Literal();
                row10Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row10Cell2GlobalLabelContainer.Controls.Add(row10Cell2GlobalLabel);
                row10Cell2.Controls.Add(row10Cell2GlobalLabelContainer);

                row10.Cells.Add(row10Cell1);
                row10.Cells.Add(row10Cell2);
                permissionsTable.Rows.Add(row10);
            }
            
            // api
            TableRow row7 = new TableRow();
            TableCell row7Cell1 = new TableCell();
            TableCell row7Cell2 = new TableCell();

            this._System_API = new CheckBox();
            this._System_API.ID = "System_API";
            this._System_API.Text = _GlobalResources.APIAllowsAccessToConfigureAPISettings;
            this._System_API.Attributes.Add("onclick", "TogglePermission(\"" + this._System_API.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
            row7Cell1.Controls.Add(this._System_API);

            Panel row7Cell2GlobalLabelContainer = new Panel();
            Literal row7Cell2GlobalLabel = new Literal();
            row7Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row7Cell2GlobalLabelContainer.Controls.Add(row7Cell2GlobalLabel);
            row7Cell2.Controls.Add(row7Cell2GlobalLabelContainer);

            row7.Cells.Add(row7Cell1);
            row7.Cells.Add(row7Cell2);
            permissionsTable.Rows.Add(row7);
            
            // xapi endpoints
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.XAPIENDPOINTS_ENABLE))
            {
                TableRow row8 = new TableRow();
                TableCell row8Cell1 = new TableCell();
                TableCell row8Cell2 = new TableCell();

                this._System_xAPIEndpoints = new CheckBox();
                this._System_xAPIEndpoints.ID = "System_xAPIEndpoints";
                this._System_xAPIEndpoints.Text = _GlobalResources.xAPIEndpointsAllowsAccessToConfigureEndpointsForxAPI;
                this._System_xAPIEndpoints.Attributes.Add("onclick", "TogglePermission(\"" + this._System_xAPIEndpoints.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
                row8Cell1.Controls.Add(this._System_xAPIEndpoints);

                Panel row8Cell2GlobalLabelContainer = new Panel();
                Literal row8Cell2GlobalLabel = new Literal();
                row8Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row8Cell2GlobalLabelContainer.Controls.Add(row8Cell2GlobalLabel);
                row8Cell2.Controls.Add(row8Cell2GlobalLabelContainer);

                row8.Cells.Add(row8Cell1);
                row8.Cells.Add(row8Cell2);
                permissionsTable.Rows.Add(row8);
            }

            // logs
            TableRow row9 = new TableRow();
            TableCell row9Cell1 = new TableCell();
            TableCell row9Cell2 = new TableCell();

            this._System_Logs = new CheckBox();
            this._System_Logs.ID = "System_Logs";
            this._System_Logs.Text = _GlobalResources.LogsAllowsAccessToSystemLogs;
            this._System_Logs.Attributes.Add("onclick", "TogglePermission(\"" + this._System_Logs.ID + "\", SystemPermissionsCheckBoxes, \"SystemPermissionsSelectAll\", false)");
            row9Cell1.Controls.Add(this._System_Logs);

            Panel row9Cell2GlobalLabelContainer = new Panel();
            Literal row9Cell2GlobalLabel = new Literal();
            row9Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row9Cell2GlobalLabelContainer.Controls.Add(row9Cell2GlobalLabel);
            row9Cell2.Controls.Add(row9Cell2GlobalLabelContainer);

            row9.Cells.Add(row9Cell1);
            row9.Cells.Add(row9Cell2);
            permissionsTable.Rows.Add(row9);

            // attach the table and return
            permissionsPanel.Controls.Add(permissionsTable);
            return permissionsPanel;
        }
        #endregion

        #region _BuildUsersAndGroupsPermissionsContainer
        /// <summary>
        /// Builds the container for Users & Groups permission checkboxes/options.
        /// </summary>
        /// <returns></returns>
        private Panel _BuildUsersAndGroupsPermissionsContainer(DataTable groups)
        {
            // container
            Panel permissionsPanel = new Panel();
            permissionsPanel.ID = "UsersAndGroupsPermissionsContainer";
            permissionsPanel.CssClass = "PermissionsContainer";

            // table
            Table permissionsTable = new Table();

            // header row
            TableRow headerRow = new TableRow();

            // header cell
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = 2;

            // header image and label
            Panel categoryHeaderContainer = new Panel();
            categoryHeaderContainer.CssClass = "PermissionsCategoryHeaderContainer";

            Image categoryImage = new Image();
            categoryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERSANDGROUPS, ImageFiles.EXT_PNG);
            categoryImage.CssClass = "SmallIcon";
            categoryHeaderContainer.Controls.Add(categoryImage);

            Label categoryLabel = new Label();
            categoryLabel.Text = _GlobalResources.UsersAndGroups;
            categoryHeaderContainer.Controls.Add(categoryLabel);

            headerCell.Controls.Add(categoryHeaderContainer);

            // select all checkbox
            Panel categorySelectAllCheckboxContainer = new Panel();
            categorySelectAllCheckboxContainer.CssClass = "PermissionsCategorySelectAllCheckboxContainer";

            CheckBox categorySelectAllCheckbox = new CheckBox();
            categorySelectAllCheckbox.ID = "UsersAndGroupsPermissionsSelectAll";
            categorySelectAllCheckbox.Text = _GlobalResources.Selectall;
            categorySelectAllCheckbox.Attributes.Add("onclick", "ToggleSelectAllPermissionsForCategory(\"" + categorySelectAllCheckbox.ID + "\", UsersAndGroupsPermissionsCheckBoxes);");
            categorySelectAllCheckboxContainer.Controls.Add(categorySelectAllCheckbox);

            headerCell.Controls.Add(categorySelectAllCheckboxContainer);

            headerRow.Cells.Add(headerCell);
            permissionsTable.Rows.Add(headerRow);

            // permissions checkbox rows

            // user registration approval
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERREGISTRATIONAPPROVAL_ENABLE))
            {
                TableRow row15 = new TableRow();
                TableCell row15Cell1 = new TableCell();
                TableCell row15Cell2 = new TableCell();

                this._UsersAndGroups_UserRegistrationApproval = new CheckBox();
                this._UsersAndGroups_UserRegistrationApproval.ID = "UsersAndGroups_UserRegistrationApproval";
                this._UsersAndGroups_UserRegistrationApproval.Text = _GlobalResources.UserRegistrationApproverAllowsTheAbilityToApproveOrRejectUserRegistrations;
                this._UsersAndGroups_UserRegistrationApproval.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_UserRegistrationApproval.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", false)");
                row15Cell1.Controls.Add(this._UsersAndGroups_UserRegistrationApproval);

                Panel row15Cell2GlobalLabelContainer = new Panel();
                Literal row15Cell2GlobalLabel = new Literal();
                row15Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row15Cell2GlobalLabelContainer.Controls.Add(row15Cell2GlobalLabel);
                row15Cell2.Controls.Add(row15Cell2GlobalLabelContainer);

                row15.Cells.Add(row15Cell1);
                row15.Cells.Add(row15Cell2);
                permissionsTable.Rows.Add(row15);
            }

            // user creator
            TableRow row2 = new TableRow();
            TableCell row2Cell1 = new TableCell();
            TableCell row2Cell2 = new TableCell();

            this._UsersAndGroups_UserCreator = new CheckBox();
            this._UsersAndGroups_UserCreator.ID = "UsersAndGroups_UserCreator";
            this._UsersAndGroups_UserCreator.Text = _GlobalResources.UserCreatorAllowsToCreateNewUserAccounts;
            this._UsersAndGroups_UserCreator.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_UserCreator.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", false)");
            row2Cell1.Controls.Add(this._UsersAndGroups_UserCreator);

            Panel row2Cell2GlobalLabelContainer = new Panel();
            Literal row2Cell2GlobalLabel = new Literal();
            row2Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row2Cell2GlobalLabelContainer.Controls.Add(row2Cell2GlobalLabel);
            row2Cell2.Controls.Add(row2Cell2GlobalLabelContainer);

            row2.Cells.Add(row2Cell1);
            row2.Cells.Add(row2Cell2);
            permissionsTable.Rows.Add(row2);

            // user deleter
            TableRow row3 = new TableRow();
            TableCell row3Cell1 = new TableCell();
            TableCell row3Cell2 = new TableCell();

            this._UsersAndGroups_UserDeleter = new CheckBox();
            this._UsersAndGroups_UserDeleter.ID = "UsersAndGroups_UserDeleter";
            this._UsersAndGroups_UserDeleter.Text = _GlobalResources.UserDeleterAllowsAccessToDeleteUserAccounts;
            this._UsersAndGroups_UserDeleter.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_UserDeleter.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", true)");
            row3Cell1.Controls.Add(this._UsersAndGroups_UserDeleter);

            // defined scope
            row3Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_UserDeleter", _GlobalResources.ThisUserMayDelete, groups));

            row3.Cells.Add(row3Cell1);
            row3.Cells.Add(row3Cell2);
            permissionsTable.Rows.Add(row3);

            // user editor
            TableRow row4 = new TableRow();
            TableCell row4Cell1 = new TableCell();
            TableCell row4Cell2 = new TableCell();

            this._UsersAndGroups_UserEditor = new CheckBox();
            this._UsersAndGroups_UserEditor.ID = "UsersAndGroups_UserEditor";
            this._UsersAndGroups_UserEditor.Text = _GlobalResources.UserEditorAllowsAccessToModifyUserAccountData;
            this._UsersAndGroups_UserEditor.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_UserEditor.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", true)");
            row4Cell1.Controls.Add(this._UsersAndGroups_UserEditor);

            // defined scope
            row4Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_UserEditor", _GlobalResources.ThisUserMayEdit, groups));

            row4.Cells.Add(row4Cell1);
            row4.Cells.Add(row4Cell2);
            permissionsTable.Rows.Add(row4);

            // user manager
            TableRow row5 = new TableRow();
            TableCell row5Cell1 = new TableCell();
            TableCell row5Cell2 = new TableCell();

            this._UsersAndGroups_UserManager = new CheckBox();
            this._UsersAndGroups_UserManager.ID = "UsersAndGroups_UserManager";
            this._UsersAndGroups_UserManager.Text = _GlobalResources.UserManagerAllowsAccessToPerformUserManagementFunctionsSuchAsAssigningAndRevokingUserEnrollmentsAwardingAndRevokingCertificatesAndImportingExternalTrainingHistories;
            this._UsersAndGroups_UserManager.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_UserManager.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", true)");
            row5Cell1.Controls.Add(this._UsersAndGroups_UserManager);

            // defined scope
            row5Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_UserManager", _GlobalResources.ThisUserMayManage, groups));

            row5.Cells.Add(row5Cell1);
            row5.Cells.Add(row5Cell2);
            permissionsTable.Rows.Add(row5);

            // user impersonator
            TableRow row6 = new TableRow();
            TableCell row6Cell1 = new TableCell();
            TableCell row6Cell2 = new TableCell();

            this._UsersAndGroups_UserImpersonator = new CheckBox();
            this._UsersAndGroups_UserImpersonator.ID = "UsersAndGroups_UserImpersonator";
            this._UsersAndGroups_UserImpersonator.Text = _GlobalResources.UserImpersonatorAllowsAccessToImpersonateLoginAsOtherUsers;
            this._UsersAndGroups_UserImpersonator.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_UserImpersonator.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", true)");
            row6Cell1.Controls.Add(this._UsersAndGroups_UserImpersonator);

            // defined scope
            row6Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_UserImpersonator", _GlobalResources.ThisUserMayImpersonate, groups));

            row6.Cells.Add(row6Cell1);
            row6.Cells.Add(row6Cell2);
            permissionsTable.Rows.Add(row6);

            // group creator
            TableRow row7 = new TableRow();
            TableCell row7Cell1 = new TableCell();
            TableCell row7Cell2 = new TableCell();

            this._UsersAndGroups_GroupCreator = new CheckBox();
            this._UsersAndGroups_GroupCreator.ID = "UsersAndGroups_GroupCreator";
            this._UsersAndGroups_GroupCreator.Text = _GlobalResources.GroupCreatorAllowsToCreateNewGroups;
            this._UsersAndGroups_GroupCreator.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_GroupCreator.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", false)");
            row7Cell1.Controls.Add(this._UsersAndGroups_GroupCreator);

            Panel row7Cell2GlobalLabelContainer = new Panel();
            Literal row7Cell2GlobalLabel = new Literal();
            row7Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row7Cell2GlobalLabelContainer.Controls.Add(row7Cell2GlobalLabel);
            row7Cell2.Controls.Add(row7Cell2GlobalLabelContainer);

            row7.Cells.Add(row7Cell1);
            row7.Cells.Add(row7Cell2);
            permissionsTable.Rows.Add(row7);

            // group deleter
            TableRow row8 = new TableRow();
            TableCell row8Cell1 = new TableCell();
            TableCell row8Cell2 = new TableCell();

            this._UsersAndGroups_GroupDeleter = new CheckBox();
            this._UsersAndGroups_GroupDeleter.ID = "UsersAndGroups_GroupDeleter";
            this._UsersAndGroups_GroupDeleter.Text = _GlobalResources.GroupDeleterAllowsAccessToDeleteGroups;
            this._UsersAndGroups_GroupDeleter.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_GroupDeleter.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", true)");
            row8Cell1.Controls.Add(this._UsersAndGroups_GroupDeleter);

            Panel row8Cell2GlobalLabelContainer = new Panel();
            Literal row8Cell2GlobalLabel = new Literal();
            row8Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row8Cell2GlobalLabelContainer.Controls.Add(row8Cell2GlobalLabel);
            row8Cell2.Controls.Add(row8Cell2GlobalLabelContainer);

            row8.Cells.Add(row8Cell1);
            row8.Cells.Add(row8Cell2);
            permissionsTable.Rows.Add(row8);

            // group editor
            TableRow row9 = new TableRow();
            TableCell row9Cell1 = new TableCell();
            TableCell row9Cell2 = new TableCell();

            this._UsersAndGroups_GroupEditor = new CheckBox();
            this._UsersAndGroups_GroupEditor.ID = "UsersAndGroups_GroupEditor";
            this._UsersAndGroups_GroupEditor.Text = _GlobalResources.GroupEditorAllowsAccessToModifyGroupProperties;
            this._UsersAndGroups_GroupEditor.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_GroupEditor.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", true)");
            row9Cell1.Controls.Add(this._UsersAndGroups_GroupEditor);

            // defined scope
            row9Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_GroupEditor", _GlobalResources.ThisUserMayEdit, groups, false));

            row9.Cells.Add(row9Cell1);
            row9.Cells.Add(row9Cell2);
            permissionsTable.Rows.Add(row9);

            // group manager
            TableRow row10 = new TableRow();
            TableCell row10Cell1 = new TableCell();
            TableCell row10Cell2 = new TableCell();

            this._UsersAndGroups_GroupManager = new CheckBox();
            this._UsersAndGroups_GroupManager.ID = "UsersAndGroups_GroupManager";
            this._UsersAndGroups_GroupManager.Text = _GlobalResources.GroupManagerAllowsAccessToPerformGroupManagementFunctionsSuchAsAddingAndRemovingUsersFromGroupsAssigningAndRevokingGroupEnrollmentsCreatingAndModifyingAutoJoinRulesAndManagingGroupDiscussions;
            this._UsersAndGroups_GroupManager.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_GroupManager.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", true)");
            row10Cell1.Controls.Add(this._UsersAndGroups_GroupManager);

            // defined scope
            row10Cell2.Controls.Add(this._BuildScopeSelectorControls("UsersAndGroups_GroupManager", _GlobalResources.ThisUserMayManage, groups, false));

            row10.Cells.Add(row10Cell1);
            row10.Cells.Add(row10Cell2);
            permissionsTable.Rows.Add(row10);

            // role manager
            TableRow row11 = new TableRow();
            TableCell row11Cell1 = new TableCell();
            TableCell row11Cell2 = new TableCell();

            this._UsersAndGroups_RoleManager = new CheckBox();
            this._UsersAndGroups_RoleManager.ID = "UsersAndGroups_RoleManager";
            this._UsersAndGroups_RoleManager.Enabled = false; // disable it, as it is only available to "System Administrator"
            this._UsersAndGroups_RoleManager.Text = _GlobalResources.RoleManagerAllowsAccessToPerformRoleManagementFunctionsSuchAsCreatingAndModifyingRolesCreatingAndModifyingAutoJoinRulesAndAssigningRolesForUsersAndGroups;
            row11Cell1.Controls.Add(this._UsersAndGroups_RoleManager);

            Panel row11Cell2GlobalLabelContainer = new Panel();
            Literal row11Cell2GlobalLabel = new Literal();
            row11Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row11Cell2GlobalLabelContainer.Controls.Add(row11Cell2GlobalLabel);
            row11Cell2.Controls.Add(row11Cell2GlobalLabelContainer);

            row11.Cells.Add(row11Cell1);
            row11.Cells.Add(row11Cell2);
            permissionsTable.Rows.Add(row11);

            // activity import
            TableRow row12 = new TableRow();
            TableCell row12Cell1 = new TableCell();
            TableCell row12Cell2 = new TableCell();

            this._UsersAndGroups_ActivityImport = new CheckBox();
            this._UsersAndGroups_ActivityImport.ID = "UsersAndGroups_ActivityImport";
            this._UsersAndGroups_ActivityImport.Text = _GlobalResources.ActivityImportAllowsAccessToUploadAndManageExternalTrainingHistoriesForMultipleUsers;
            this._UsersAndGroups_ActivityImport.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_ActivityImport.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", false)");
            row12Cell1.Controls.Add(this._UsersAndGroups_ActivityImport);

            Panel row12Cell2GlobalLabelContainer = new Panel();
            Literal row12Cell2GlobalLabel = new Literal();
            row12Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row12Cell2GlobalLabelContainer.Controls.Add(row12Cell2GlobalLabel);
            row12Cell2.Controls.Add(row12Cell2GlobalLabelContainer);

            row12.Cells.Add(row12Cell1);
            row12.Cells.Add(row12Cell2);
            permissionsTable.Rows.Add(row12);

            // certificate import
            TableRow row13 = new TableRow();
            TableCell row13Cell1 = new TableCell();
            TableCell row13Cell2 = new TableCell();

            this._UsersAndGroups_CertificateImport = new CheckBox();
            this._UsersAndGroups_CertificateImport.ID = "UsersAndGroups_CertificateImport";
            this._UsersAndGroups_CertificateImport.Text = _GlobalResources.CertificateImportAllowsAccessToUploadAndManageCertificateHistoriesForMultipleUsers;
            this._UsersAndGroups_CertificateImport.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_CertificateImport.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", false)");
            row13Cell1.Controls.Add(this._UsersAndGroups_CertificateImport);

            Panel row13Cell2GlobalLabelContainer = new Panel();
            Literal row13Cell2GlobalLabel = new Literal();
            row13Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row13Cell2GlobalLabelContainer.Controls.Add(row13Cell2GlobalLabel);
            row13Cell2.Controls.Add(row13Cell2GlobalLabelContainer);

            row13.Cells.Add(row13Cell1);
            row13.Cells.Add(row13Cell2);
            permissionsTable.Rows.Add(row13);

            // leaderboards
            TableRow row14 = new TableRow();
            TableCell row14Cell1 = new TableCell();
            TableCell row14Cell2 = new TableCell();

            this._UsersAndGroups_Leaderboards = new CheckBox();
            this._UsersAndGroups_Leaderboards.ID = "UsersAndGroups_Leaderboards";
            this._UsersAndGroups_Leaderboards.Text = _GlobalResources.TeamLeaderboardsUsersWithThisPermissionCanViewLeaderboards;
            this._UsersAndGroups_Leaderboards.Attributes.Add("onclick", "TogglePermission(\"" + this._UsersAndGroups_Leaderboards.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", false)");
            row14Cell1.Controls.Add(this._UsersAndGroups_Leaderboards);

            Panel row14Cell2GlobalLabelContainer = new Panel();
            Literal row14Cell2GlobalLabel = new Literal();
            row14Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row14Cell2GlobalLabelContainer.Controls.Add(row14Cell2GlobalLabel);
            row14Cell2.Controls.Add(row14Cell2GlobalLabelContainer);

            row14.Cells.Add(row14Cell1);
            row14.Cells.Add(row14Cell2);
            permissionsTable.Rows.Add(row14);

            // attach the table and return
            permissionsPanel.Controls.Add(permissionsTable);
            return permissionsPanel;
        }
        #endregion

        #region _BuildInterfaceAndLayoutPermissionsContainer
        /// <summary>
        /// Builds the container for Interface & Layout permission checkboxes/options.
        /// </summary>
        /// <returns></returns>
        private Panel _BuildInterfaceAndLayoutPermissionsContainer(DataTable groups)
        {
            // container
            Panel permissionsPanel = new Panel();
            permissionsPanel.ID = "InterfaceAndLayoutPermissionsContainer";
            permissionsPanel.CssClass = "PermissionsContainer";

            // table
            Table permissionsTable = new Table();

            // header row
            TableRow headerRow = new TableRow();

            // header cell
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = 2;

            // header image and label
            Panel categoryHeaderContainer = new Panel();
            categoryHeaderContainer.CssClass = "PermissionsCategoryHeaderContainer";

            Image categoryImage = new Image();
            categoryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_APPLICATION, ImageFiles.EXT_PNG);
            categoryImage.CssClass = "SmallIcon";
            categoryHeaderContainer.Controls.Add(categoryImage);

            Label categoryLabel = new Label();
            categoryLabel.Text = _GlobalResources.InterfaceAndLayout;
            categoryHeaderContainer.Controls.Add(categoryLabel);

            headerCell.Controls.Add(categoryHeaderContainer);

            // select all checkbox
            Panel categorySelectAllCheckboxContainer = new Panel();
            categorySelectAllCheckboxContainer.CssClass = "PermissionsCategorySelectAllCheckboxContainer";

            CheckBox categorySelectAllCheckbox = new CheckBox();
            categorySelectAllCheckbox.ID = "InterfaceAndLayoutPermissionsSelectAll";
            categorySelectAllCheckbox.Text = _GlobalResources.Selectall;
            categorySelectAllCheckbox.Attributes.Add("onclick", "ToggleSelectAllPermissionsForCategory(\"" + categorySelectAllCheckbox.ID + "\", InterfaceAndLayoutPermissionsCheckBoxes);");
            categorySelectAllCheckboxContainer.Controls.Add(categorySelectAllCheckbox);

            headerCell.Controls.Add(categorySelectAllCheckboxContainer);

            headerRow.Cells.Add(headerCell);
            permissionsTable.Rows.Add(headerRow);

            // permissions checkbox rows

            // home page
            TableRow row1 = new TableRow();
            TableCell row1Cell1 = new TableCell();
            TableCell row1Cell2 = new TableCell();

            this._InterfaceAndLayout_HomePage = new CheckBox();
            this._InterfaceAndLayout_HomePage.ID = "InterfaceAndLayout_HomePage";
            this._InterfaceAndLayout_HomePage.Text = _GlobalResources.HomePageAllowsAccessToModifyThePortalsHomePage;
            this._InterfaceAndLayout_HomePage.Attributes.Add("onclick", "TogglePermission(\"" + this._InterfaceAndLayout_HomePage.ID + "\", InterfaceAndLayoutPermissionsCheckBoxes, \"InterfaceAndLayoutPermissionsSelectAll\", false)");
            row1Cell1.Controls.Add(this._InterfaceAndLayout_HomePage);

            Panel row1Cell2GlobalLabelContainer = new Panel();
            Literal row1Cell2GlobalLabel = new Literal();
            row1Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row1Cell2GlobalLabelContainer.Controls.Add(row1Cell2GlobalLabel);
            row1Cell2.Controls.Add(row1Cell2GlobalLabelContainer);

            row1.Cells.Add(row1Cell1);
            row1.Cells.Add(row1Cell2);
            permissionsTable.Rows.Add(row1);

            // masthead
            TableRow row2 = new TableRow();
            TableCell row2Cell1 = new TableCell();
            TableCell row2Cell2 = new TableCell();

            this._InterfaceAndLayout_Masthead = new CheckBox();
            this._InterfaceAndLayout_Masthead.ID = "InterfaceAndLayout_Masthead";
            this._InterfaceAndLayout_Masthead.Text = _GlobalResources.MastheadAllowsAccessToModifyThePortalsMasthead;
            this._InterfaceAndLayout_Masthead.Attributes.Add("onclick", "TogglePermission(\"" + this._InterfaceAndLayout_Masthead.ID + "\", InterfaceAndLayoutPermissionsCheckBoxes, \"InterfaceAndLayoutPermissionsSelectAll\", false)");
            row2Cell1.Controls.Add(this._InterfaceAndLayout_Masthead);

            Panel row2Cell2GlobalLabelContainer = new Panel();
            Literal row2Cell2GlobalLabel = new Literal();
            row2Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row2Cell2GlobalLabelContainer.Controls.Add(row2Cell2GlobalLabel);
            row2Cell2.Controls.Add(row2Cell2GlobalLabelContainer);

            row2.Cells.Add(row2Cell1);
            row2.Cells.Add(row2Cell2);
            permissionsTable.Rows.Add(row2);

            // footer
            TableRow row3 = new TableRow();
            TableCell row3Cell1 = new TableCell();
            TableCell row3Cell2 = new TableCell();

            this._InterfaceAndLayout_Footer = new CheckBox();
            this._InterfaceAndLayout_Footer.ID = "InterfaceAndLayout_Footer";
            this._InterfaceAndLayout_Footer.Text = _GlobalResources.FooterAllowsAccessToModifyThePortalsFooter;
            this._InterfaceAndLayout_Footer.Attributes.Add("onclick", "TogglePermission(\"" + this._InterfaceAndLayout_Footer.ID + "\", InterfaceAndLayoutPermissionsCheckBoxes, \"InterfaceAndLayoutPermissionsSelectAll\", false)");
            row3Cell1.Controls.Add(this._InterfaceAndLayout_Footer);

            Panel row3Cell2GlobalLabelContainer = new Panel();
            Literal row3Cell2GlobalLabel = new Literal();
            row3Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row3Cell2GlobalLabelContainer.Controls.Add(row3Cell2GlobalLabel);
            row3Cell2.Controls.Add(row3Cell2GlobalLabelContainer);

            row3.Cells.Add(row3Cell1);
            row3.Cells.Add(row3Cell2);
            permissionsTable.Rows.Add(row3);

            // css editor
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CSSEDITOR_ENABLE))
            {
                TableRow row4 = new TableRow();
                TableCell row4Cell1 = new TableCell();
                TableCell row4Cell2 = new TableCell();

                this._InterfaceAndLayout_CSSEditor = new CheckBox();
                this._InterfaceAndLayout_CSSEditor.ID = "InterfaceAndLayout_CSSEditor";
                this._InterfaceAndLayout_CSSEditor.Text = _GlobalResources.CSSEditorAllowsAccessToModifyCSSFilesForThePortal;
                this._InterfaceAndLayout_CSSEditor.Attributes.Add("onclick", "TogglePermission(\"" + this._InterfaceAndLayout_CSSEditor.ID + "\", InterfaceAndLayoutPermissionsCheckBoxes, \"InterfaceAndLayoutPermissionsSelectAll\", false)");
                row4Cell1.Controls.Add(this._InterfaceAndLayout_CSSEditor);

                Panel row4Cell2GlobalLabelContainer = new Panel();
                Literal row4Cell2GlobalLabel = new Literal();
                row4Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row4Cell2GlobalLabelContainer.Controls.Add(row4Cell2GlobalLabel);
                row4Cell2.Controls.Add(row4Cell2GlobalLabelContainer);

                row4.Cells.Add(row4Cell1);
                row4.Cells.Add(row4Cell2);
                permissionsTable.Rows.Add(row4);
            }

            // image editor
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.IMAGE_EDITOR_ENABLE))
            {
                TableRow row5 = new TableRow();
                TableCell row5Cell1 = new TableCell();
                TableCell row5Cell2 = new TableCell();

                this._InterfaceAndLayout_ImageEditor = new CheckBox();
                this._InterfaceAndLayout_ImageEditor.ID = "InterfaceAndLayout_ImageEditor";
                this._InterfaceAndLayout_ImageEditor.Text = _GlobalResources.ImageEditorAllowsAccessToModifyIconsAndImagesForThePortal;
                this._InterfaceAndLayout_ImageEditor.Attributes.Add("onclick", "TogglePermission(\"" + this._InterfaceAndLayout_ImageEditor.ID + "\", InterfaceAndLayoutPermissionsCheckBoxes, \"InterfaceAndLayoutPermissionsSelectAll\", false)");
                row5Cell1.Controls.Add(this._InterfaceAndLayout_ImageEditor);

                Panel row5Cell2GlobalLabelContainer = new Panel();
                Literal row5Cell2GlobalLabel = new Literal();
                row5Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row5Cell2GlobalLabelContainer.Controls.Add(row5Cell2GlobalLabel);
                row5Cell2.Controls.Add(row5Cell2GlobalLabelContainer);

                row5.Cells.Add(row5Cell1);
                row5.Cells.Add(row5Cell2);
                permissionsTable.Rows.Add(row5);
            }

            // attach the table and return
            permissionsPanel.Controls.Add(permissionsTable);
            return permissionsPanel;
        }
        #endregion

        #region _BuildLearningAssetsPermissionsContainer
        /// <summary>
        /// Builds the container for Learning Assets permission checkboxes/options.
        /// </summary>
        /// <returns></returns>
        private Panel _BuildLearningAssetsPermissionsContainer(DataTable groups)
        {
            // container
            Panel permissionsPanel = new Panel();
            permissionsPanel.ID = "LearningAssetsPermissionsContainer";
            permissionsPanel.CssClass = "PermissionsContainer";

            // table
            Table permissionsTable = new Table();

            // header row
            TableRow headerRow = new TableRow();

            // header cell
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = 2;

            // header image and label
            Panel categoryHeaderContainer = new Panel();
            categoryHeaderContainer.CssClass = "PermissionsCategoryHeaderContainer";

            Image categoryImage = new Image();
            categoryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGASSETS, ImageFiles.EXT_PNG);
            categoryImage.CssClass = "SmallIcon";
            categoryHeaderContainer.Controls.Add(categoryImage);

            Label categoryLabel = new Label();
            categoryLabel.Text = _GlobalResources.LearningAssets;
            categoryHeaderContainer.Controls.Add(categoryLabel);

            headerCell.Controls.Add(categoryHeaderContainer);

            // select all checkbox
            Panel categorySelectAllCheckboxContainer = new Panel();
            categorySelectAllCheckboxContainer.CssClass = "PermissionsCategorySelectAllCheckboxContainer";

            CheckBox categorySelectAllCheckbox = new CheckBox();
            categorySelectAllCheckbox.ID = "LearningAssetsPermissionsSelectAll";
            categorySelectAllCheckbox.Text = _GlobalResources.Selectall;
            categorySelectAllCheckbox.Attributes.Add("onclick", "ToggleSelectAllPermissionsForCategory(\"" + categorySelectAllCheckbox.ID + "\", LearningAssetsPermissionsCheckBoxes);");
            categorySelectAllCheckboxContainer.Controls.Add(categorySelectAllCheckbox);

            headerCell.Controls.Add(categorySelectAllCheckboxContainer);

            headerRow.Cells.Add(headerCell);
            permissionsTable.Rows.Add(headerRow);

            // permissions checkbox rows

            // course catalog
            TableRow row1 = new TableRow();
            TableCell row1Cell1 = new TableCell();
            TableCell row1Cell2 = new TableCell();

            this._LearningAssets_CourseCatalog = new CheckBox();
            this._LearningAssets_CourseCatalog.ID = "LearningAssets_CourseCatalog";
            this._LearningAssets_CourseCatalog.Text = _GlobalResources.CourseCatalogAllowsAccessToManageThePortalsCourseCatalog;
            this._LearningAssets_CourseCatalog.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_CourseCatalog.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
            row1Cell1.Controls.Add(this._LearningAssets_CourseCatalog);

            Panel row1Cell2GlobalLabelContainer = new Panel();
            Literal row1Cell2GlobalLabel = new Literal();
            row1Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row1Cell2GlobalLabelContainer.Controls.Add(row1Cell2GlobalLabel);
            row1Cell2.Controls.Add(row1Cell2GlobalLabelContainer);

            row1.Cells.Add(row1Cell1);
            row1.Cells.Add(row1Cell2);
            permissionsTable.Rows.Add(row1);

            // self-enrollment approval
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
            {
                TableRow row10 = new TableRow();
                TableCell row10Cell1 = new TableCell();
                TableCell row10Cell2 = new TableCell();

                this._LearningAssets_SelfEnrollmentApproval = new CheckBox();
                this._LearningAssets_SelfEnrollmentApproval.ID = "LearningAssets_SelfEnrollmentApproval";
                this._LearningAssets_SelfEnrollmentApproval.Text = _GlobalResources.SelfEnrollmentApproverAllowsTheAbilityToApproveOrRejectSelfEnrollmentsForCoursesThatRequireSelfEnrollmentApproval;
                this._LearningAssets_SelfEnrollmentApproval.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_SelfEnrollmentApproval.ID + "\", UsersAndGroupsPermissionsCheckBoxes, \"UsersAndGroupsPermissionsSelectAll\", false)");
                row10Cell1.Controls.Add(this._LearningAssets_SelfEnrollmentApproval);

                Panel row10Cell2GlobalLabelContainer = new Panel();
                Literal row10Cell2GlobalLabel = new Literal();
                row10Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row10Cell2GlobalLabelContainer.Controls.Add(row10Cell2GlobalLabel);
                row10Cell2.Controls.Add(row10Cell2GlobalLabelContainer);

                row10.Cells.Add(row10Cell1);
                row10.Cells.Add(row10Cell2);
                permissionsTable.Rows.Add(row10);
            }

            // course content manager
            TableRow row2 = new TableRow();
            TableCell row2Cell1 = new TableCell();
            TableCell row2Cell2 = new TableCell();

            this._LearningAssets_CourseManager = new CheckBox();
            this._LearningAssets_CourseManager.ID = "LearningAssets_CourseManager";
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE))
            {
                this._LearningAssets_CourseManager.Text = _GlobalResources.CourseContentManagerAllowsAccessToCreateAndModifyCoursesManageCourseSpecificEmailNotificationsAndCertificatesAndManageCourseDiscussions;
            }
            else
            {
                this._LearningAssets_CourseManager.Text = _GlobalResources.CourseContentManagerAllowsAccessToCreateAndModifyCoursesManageCourseSpecificCertificatesAndManageCourseDiscussions;
            }
            this._LearningAssets_CourseManager.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_CourseManager.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
            row2Cell1.Controls.Add(this._LearningAssets_CourseManager);

            Panel row2Cell2GlobalLabelContainer = new Panel();
            Literal row2Cell2GlobalLabel = new Literal();
            row2Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row2Cell2GlobalLabelContainer.Controls.Add(row2Cell2GlobalLabel);
            row2Cell2.Controls.Add(row2Cell2GlobalLabelContainer);

            row2.Cells.Add(row2Cell1);
            row2.Cells.Add(row2Cell2);
            permissionsTable.Rows.Add(row2);

            // course enrollment manager
            TableRow row3 = new TableRow();
            TableCell row3Cell1 = new TableCell();
            TableCell row3Cell2 = new TableCell();

            this._LearningAssets_CourseEnrollmentManager = new CheckBox();
            this._LearningAssets_CourseEnrollmentManager.ID = "LearningAssets_CourseEnrollmentManager";
            this._LearningAssets_CourseEnrollmentManager.Text = _GlobalResources.CourseEnrollmentManagerAllowsAccessToCreateCourseEnrollmentsAndEnrollmentRules;
            this._LearningAssets_CourseEnrollmentManager.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_CourseEnrollmentManager.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
            row3Cell1.Controls.Add(this._LearningAssets_CourseEnrollmentManager);

            Panel row3Cell2GlobalLabelContainer = new Panel();
            Literal row3Cell2GlobalLabel = new Literal();
            row3Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row3Cell2GlobalLabelContainer.Controls.Add(row3Cell2GlobalLabel);
            row3Cell2.Controls.Add(row3Cell2GlobalLabelContainer);

            row3.Cells.Add(row3Cell1);
            row3.Cells.Add(row3Cell2);
            permissionsTable.Rows.Add(row3);

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                // learning path content manager
                TableRow row4 = new TableRow();
                TableCell row4Cell1 = new TableCell();
                TableCell row4Cell2 = new TableCell();

                this._LearningAssets_LearningPathManager = new CheckBox();
                this._LearningAssets_LearningPathManager.ID = "LearningAssets_LearningPathManager";
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE))
                {
                    this._LearningAssets_LearningPathManager.Text = _GlobalResources.LearningPathContentManagerAllowsAccessToCreateAndModifyLearningPathsAndManageLearningPathSpecificEmailNotificationsAndCertificates;
                }
                else
                {
                    this._LearningAssets_LearningPathManager.Text = _GlobalResources.LearningPathContentManagerAllowsAccessToCreateAndModifyLearningPathsAndManageLearningPathSpecificCertificates;
                }
                this._LearningAssets_LearningPathManager.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_LearningPathManager.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
                row4Cell1.Controls.Add(this._LearningAssets_LearningPathManager);

                Panel row4Cell2GlobalLabelContainer = new Panel();
                Literal row4Cell2GlobalLabel = new Literal();
                row4Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row4Cell2GlobalLabelContainer.Controls.Add(row4Cell2GlobalLabel);
                row4Cell2.Controls.Add(row4Cell2GlobalLabelContainer);

                row4.Cells.Add(row4Cell1);
                row4.Cells.Add(row4Cell2);
                permissionsTable.Rows.Add(row4);
            
                // learning path enrollment manager
                TableRow row5 = new TableRow();
                TableCell row5Cell1 = new TableCell();
                TableCell row5Cell2 = new TableCell();

                this._LearningAssets_LearningPathEnrollmentManager = new CheckBox();
                this._LearningAssets_LearningPathEnrollmentManager.ID = "LearningAssets_LearningPathEnrollmentManager";
                this._LearningAssets_LearningPathEnrollmentManager.Text = _GlobalResources.LearningPathEnrollmentManagerAllowsAccessToCreateLearningPathEnrollmentsAndEnrollmentRules;
                this._LearningAssets_LearningPathEnrollmentManager.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_LearningPathEnrollmentManager.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
                row5Cell1.Controls.Add(this._LearningAssets_LearningPathEnrollmentManager);

                Panel row5Cell2GlobalLabelContainer = new Panel();
                Literal row5Cell2GlobalLabel = new Literal();
                row5Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row5Cell2GlobalLabelContainer.Controls.Add(row5Cell2GlobalLabel);
                row5Cell2.Controls.Add(row5Cell2GlobalLabelContainer);

                row5.Cells.Add(row5Cell1);
                row5.Cells.Add(row5Cell2);
                permissionsTable.Rows.Add(row5);
            }

            // certifications manager
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
            {
                TableRow row6 = new TableRow();
                TableCell row6Cell1 = new TableCell();
                TableCell row6Cell2 = new TableCell();

                this._LearningAssets_CertificationsManager = new CheckBox();
                this._LearningAssets_CertificationsManager.ID = "LearningAssets_CertificationsManager";
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE))
                {
                    this._LearningAssets_CertificationsManager.Text = _GlobalResources.CertificationsManagerAllowsAccessToCreateAndModifyCertificationsCreateCertificationEnrollmentsAndEnrollmentRulesAndManageCertificationSpecificEmailNotifications;
                }
                else
                {
                    this._LearningAssets_CertificationsManager.Text = _GlobalResources.CertificationsManagerAllowsAccessToCreateAndModifyCertificationsCreateCertificationEnrollmentsAndEnrollmentRules;
                }
                this._LearningAssets_CertificationsManager.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_CertificationsManager.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
                row6Cell1.Controls.Add(this._LearningAssets_CertificationsManager);

                Panel row6Cell2GlobalLabelContainer = new Panel();
                Literal row6Cell2GlobalLabel = new Literal();
                row6Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row6Cell2GlobalLabelContainer.Controls.Add(row6Cell2GlobalLabel);
                row6Cell2.Controls.Add(row6Cell2GlobalLabelContainer);

                row6.Cells.Add(row6Cell1);
                row6.Cells.Add(row6Cell2);
                permissionsTable.Rows.Add(row6);
            }

            // content package manager
            TableRow row7 = new TableRow();
            TableCell row7Cell1 = new TableCell();
            TableCell row7Cell2 = new TableCell();

            this._LearningAssets_ContentPackageManager = new CheckBox();
            this._LearningAssets_ContentPackageManager.ID = "LearningAssets_ContentPackageManager";
            this._LearningAssets_ContentPackageManager.Text = _GlobalResources.ContentPackageManagerAllowsAccessToUploadAndManageContentPackages;
            this._LearningAssets_ContentPackageManager.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_ContentPackageManager.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
            row7Cell1.Controls.Add(this._LearningAssets_ContentPackageManager);

            Panel row7Cell2GlobalLabelContainer = new Panel();
            Literal row7Cell2GlobalLabel = new Literal();
            row7Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row7Cell2GlobalLabelContainer.Controls.Add(row7Cell2GlobalLabel);
            row7Cell2.Controls.Add(row7Cell2GlobalLabelContainer);

            row7.Cells.Add(row7Cell1);
            row7.Cells.Add(row7Cell2);
            permissionsTable.Rows.Add(row7);

            // quiz and survey manager
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE))
            {
                TableRow row8 = new TableRow();
                TableCell row8Cell1 = new TableCell();
                TableCell row8Cell2 = new TableCell();

                this._LearningAssets_QuizAndSurveyManager = new CheckBox();
                this._LearningAssets_QuizAndSurveyManager.ID = "LearningAssets_QuizAndSurveyManager";
                this._LearningAssets_QuizAndSurveyManager.Text = _GlobalResources.QuizAndSurveyManagerAllowsAccessToCreateAndManageQuizzesAndSurveys;
                this._LearningAssets_QuizAndSurveyManager.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_QuizAndSurveyManager.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
                row8Cell1.Controls.Add(this._LearningAssets_QuizAndSurveyManager);

                Panel row8Cell2GlobalLabelContainer = new Panel();
                Literal row8Cell2GlobalLabel = new Literal();
                row8Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row8Cell2GlobalLabelContainer.Controls.Add(row8Cell2GlobalLabel);
                row8Cell2.Controls.Add(row8Cell2GlobalLabelContainer);

                row8.Cells.Add(row8Cell1);
                row8.Cells.Add(row8Cell2);
                permissionsTable.Rows.Add(row8);
            }

            // certificate template manager
            TableRow row9 = new TableRow();
            TableCell row9Cell1 = new TableCell();
            TableCell row9Cell2 = new TableCell();

            this._LearningAssets_CertificateTemplateManager = new CheckBox();
            this._LearningAssets_CertificateTemplateManager.ID = "LearningAssets_CertificateTemplateManager";
            this._LearningAssets_CertificateTemplateManager.Text = _GlobalResources.CertificateTemplateManagerAllowsAccessToCreateAndModifyCertificateTemplates;
            this._LearningAssets_CertificateTemplateManager.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_CertificateTemplateManager.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
            row9Cell1.Controls.Add(this._LearningAssets_CertificateTemplateManager);

            Panel row9Cell2GlobalLabelContainer = new Panel();
            Literal row9Cell2GlobalLabel = new Literal();
            row9Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row9Cell2GlobalLabelContainer.Controls.Add(row9Cell2GlobalLabel);
            row9Cell2.Controls.Add(row9Cell2GlobalLabelContainer);

            row9.Cells.Add(row9Cell1);
            row9.Cells.Add(row9Cell2);
            permissionsTable.Rows.Add(row9);

            // instructor led training manager
            TableRow row11 = new TableRow();
            TableCell row11Cell1 = new TableCell();
            TableCell row11Cell2 = new TableCell();

            this._LearningAssets_InstructorLedTrainingManager = new CheckBox();
            this._LearningAssets_InstructorLedTrainingManager.ID = "LearningAssets_InstructorLedTrainingManager";
            this._LearningAssets_InstructorLedTrainingManager.Text = _GlobalResources.InstructorLedTrainingManagerAllowsAccessToCreateAndModifyInstructorLedTrainingModulesAndSessionsAndManageInstructorLedTrainingRosters;
            this._LearningAssets_InstructorLedTrainingManager.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_InstructorLedTrainingManager.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
            row11Cell1.Controls.Add(this._LearningAssets_InstructorLedTrainingManager);

            Panel row11Cell2GlobalLabelContainer = new Panel();
            Literal row11Cell2GlobalLabel = new Literal();
            row11Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
            row11Cell2GlobalLabelContainer.Controls.Add(row11Cell2GlobalLabel);
            row11Cell2.Controls.Add(row11Cell2GlobalLabelContainer);

            row11.Cells.Add(row11Cell1);
            row11.Cells.Add(row11Cell2);
            permissionsTable.Rows.Add(row11);

            // resource manager
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE))
            {
                TableRow row12 = new TableRow();
                TableCell row12Cell1 = new TableCell();
                TableCell row12Cell2 = new TableCell();

                this._LearningAssets_ResourceManager = new CheckBox();
                this._LearningAssets_ResourceManager.ID = "LearningAssets_ResourceManager";
                this._LearningAssets_ResourceManager.Text = _GlobalResources.ResourceManagerAllowsAccessToCreateAndModifyResourcesToBeUsedInInstructorLedTrainingModules;
                this._LearningAssets_ResourceManager.Attributes.Add("onclick", "TogglePermission(\"" + this._LearningAssets_ResourceManager.ID + "\", LearningAssetsPermissionsCheckBoxes, \"LearningAssetsPermissionsSelectAll\", false)");
                row12Cell1.Controls.Add(this._LearningAssets_ResourceManager);

                Panel row12Cell2GlobalLabelContainer = new Panel();
                Literal row12Cell2GlobalLabel = new Literal();
                row12Cell2GlobalLabel.Text = _GlobalResources.ThisPermissionIsGlobal;
                row12Cell2GlobalLabelContainer.Controls.Add(row12Cell2GlobalLabel);
                row12Cell2.Controls.Add(row12Cell2GlobalLabelContainer);

                row12.Cells.Add(row12Cell1);
                row12.Cells.Add(row12Cell2);
                permissionsTable.Rows.Add(row12);
            }

            // attach the table and return
            permissionsPanel.Controls.Add(permissionsTable);
            return permissionsPanel;
        }
        #endregion

        #region _BuildReportingPermissionsContainer
        /// <summary>
        /// Builds the container for Reporting permission checkboxes/options.
        /// </summary>
        /// <returns></returns>
        private Panel _BuildReportingPermissionsContainer(DataTable groups)
        {
            // container
            Panel permissionsPanel = new Panel();
            permissionsPanel.ID = "ReportingPermissionsContainer";
            permissionsPanel.CssClass = "PermissionsContainer";

            // table
            Table permissionsTable = new Table();

            // header row
            TableRow headerRow = new TableRow();

            // header cell
            TableCell headerCell = new TableCell();
            headerCell.ColumnSpan = 2;

            // header image and label
            Panel categoryHeaderContainer = new Panel();
            categoryHeaderContainer.CssClass = "PermissionsCategoryHeaderContainer";

            Image categoryImage = new Image();
            categoryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GRAPH, ImageFiles.EXT_PNG);
            categoryImage.CssClass = "SmallIcon";
            categoryHeaderContainer.Controls.Add(categoryImage);

            Label categoryLabel = new Label();
            categoryLabel.Text = _GlobalResources.Reporting;
            categoryHeaderContainer.Controls.Add(categoryLabel);

            headerCell.Controls.Add(categoryHeaderContainer);

            // select all checkbox
            Panel categorySelectAllCheckboxContainer = new Panel();
            categorySelectAllCheckboxContainer.CssClass = "PermissionsCategorySelectAllCheckboxContainer";

            CheckBox categorySelectAllCheckbox = new CheckBox();
            categorySelectAllCheckbox.ID = "ReportingPermissionsSelectAll";
            categorySelectAllCheckbox.Text = _GlobalResources.Selectall;
            categorySelectAllCheckbox.Attributes.Add("onclick", "ToggleSelectAllPermissionsForCategory(\"" + categorySelectAllCheckbox.ID + "\", ReportingPermissionsCheckBoxes);");
            categorySelectAllCheckboxContainer.Controls.Add(categorySelectAllCheckbox);

            headerCell.Controls.Add(categorySelectAllCheckboxContainer);

            headerRow.Cells.Add(headerCell);
            permissionsTable.Rows.Add(headerRow);

            // permissions checkbox rows

            // reporter - user demographics dataset
            TableRow row1 = new TableRow();
            TableCell row1Cell1 = new TableCell();
            TableCell row1Cell2 = new TableCell();

            this._Reporting_Reporter_UserDemographics = new CheckBox();
            this._Reporting_Reporter_UserDemographics.ID = "Reporting_Reporter_UserDemographics";
            this._Reporting_Reporter_UserDemographics.Text = _GlobalResources.ReportsUserDemographics;
            this._Reporting_Reporter_UserDemographics.Attributes.Add("onclick", "TogglePermission(\"" + this._Reporting_Reporter_UserDemographics.ID + "\", ReportingPermissionsCheckBoxes, \"ReportingPermissionsSelectAll\", true)");
            row1Cell1.Controls.Add(this._Reporting_Reporter_UserDemographics);

            // defined scope
            row1Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_UserDemographics", _GlobalResources.ThisUserMaySee, groups));

            row1.Cells.Add(row1Cell1);
            row1.Cells.Add(row1Cell2);
            permissionsTable.Rows.Add(row1);

            // reporter - user course transcripts dataset
            TableRow row2 = new TableRow();
            TableCell row2Cell1 = new TableCell();
            TableCell row2Cell2 = new TableCell();

            this._Reporting_Reporter_UserCourseTranscripts = new CheckBox();
            this._Reporting_Reporter_UserCourseTranscripts.ID = "Reporting_Reporter_UserCourseTranscripts";
            this._Reporting_Reporter_UserCourseTranscripts.Text = _GlobalResources.ReportsUserCourseTranscripts;
            this._Reporting_Reporter_UserCourseTranscripts.Attributes.Add("onclick", "TogglePermission(\"" + this._Reporting_Reporter_UserCourseTranscripts.ID + "\", ReportingPermissionsCheckBoxes, \"ReportingPermissionsSelectAll\", true)");
            row2Cell1.Controls.Add(this._Reporting_Reporter_UserCourseTranscripts);

            // defined scope
            row2Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_UserCourseTranscripts", _GlobalResources.ThisUserMaySee, groups));

            row2.Cells.Add(row2Cell1);
            row2.Cells.Add(row2Cell2);
            permissionsTable.Rows.Add(row2);

            // reporter - user learning path transcripts dataset
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                TableRow row3 = new TableRow();
                TableCell row3Cell1 = new TableCell();
                TableCell row3Cell2 = new TableCell();

                this._Reporting_Reporter_UserLearningPathTranscripts = new CheckBox();
                this._Reporting_Reporter_UserLearningPathTranscripts.ID = "Reporting_Reporter_UserLearningPathTranscripts";
                this._Reporting_Reporter_UserLearningPathTranscripts.Text = _GlobalResources.ReportsUserLearningPathTranscripts;
                this._Reporting_Reporter_UserLearningPathTranscripts.Attributes.Add("onclick", "TogglePermission(\"" + this._Reporting_Reporter_UserLearningPathTranscripts.ID + "\", ReportingPermissionsCheckBoxes, \"ReportingPermissionsSelectAll\", true)");
                row3Cell1.Controls.Add(this._Reporting_Reporter_UserLearningPathTranscripts);

                // defined scope
                row3Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_UserLearningPathTranscripts", _GlobalResources.ThisUserMaySee, groups));

                row3.Cells.Add(row3Cell1);
                row3.Cells.Add(row3Cell2);
                permissionsTable.Rows.Add(row3);
            }

            // reporter - user instructor led training transcripts dataset
            TableRow row4 = new TableRow();
            TableCell row4Cell1 = new TableCell();
            TableCell row4Cell2 = new TableCell();

            this._Reporting_Reporter_UserInstructorLedTrainingTranscripts = new CheckBox();
            this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.ID = "Reporting_Reporter_UserInstructorLedTrainingTranscripts";
            this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.Text = _GlobalResources.ReportsUserInstructorLedTrainingTranscripts;
            this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.Attributes.Add("onclick", "TogglePermission(\"" + this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.ID + "\", ReportingPermissionsCheckBoxes, \"ReportingPermissionsSelectAll\", true)");
            row4Cell1.Controls.Add(this._Reporting_Reporter_UserInstructorLedTrainingTranscripts);

            // defined scope
            row4Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_UserInstructorLedTrainingTranscripts", _GlobalResources.ThisUserMaySee, groups));

            row4.Cells.Add(row4Cell1);
            row4.Cells.Add(row4Cell2);
            permissionsTable.Rows.Add(row4);


            // reporter - catalog and course information dataset
            TableRow row5 = new TableRow();
            TableCell row5Cell1 = new TableCell();
            TableCell row5Cell2 = new TableCell();

            this._Reporting_Reporter_CatalogAndCourseInformation = new CheckBox();
            this._Reporting_Reporter_CatalogAndCourseInformation.ID = "Reporting_Reporter_CatalogAndCourseInformation";
            this._Reporting_Reporter_CatalogAndCourseInformation.Text = _GlobalResources.ReportsCatalogAndCourseInformation;
            this._Reporting_Reporter_CatalogAndCourseInformation.Attributes.Add("onclick", "TogglePermission(\"" + this._Reporting_Reporter_CatalogAndCourseInformation.ID + "\", ReportingPermissionsCheckBoxes, \"ReportingPermissionsSelectAll\", true)");
            row5Cell1.Controls.Add(this._Reporting_Reporter_CatalogAndCourseInformation);

            // defined scope
            row5Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_CatalogAndCourseInformation", _GlobalResources.ThisUserMaySee, groups));

            row5.Cells.Add(row5Cell1);
            row5.Cells.Add(row5Cell2);
            permissionsTable.Rows.Add(row5);

            // reporter - certificates dataset
            TableRow row6 = new TableRow();
            TableCell row6Cell1 = new TableCell();
            TableCell row6Cell2 = new TableCell();

            this._Reporting_Reporter_Certificates = new CheckBox();
            this._Reporting_Reporter_Certificates.ID = "Reporting_Reporter_Certificates";
            this._Reporting_Reporter_Certificates.Text = _GlobalResources.ReportsCertificates;
            this._Reporting_Reporter_Certificates.Attributes.Add("onclick", "TogglePermission(\"" + this._Reporting_Reporter_Certificates.ID + "\", ReportingPermissionsCheckBoxes, \"ReportingPermissionsSelectAll\", true)");
            row6Cell1.Controls.Add(this._Reporting_Reporter_Certificates);

            // defined scope
            row6Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_Certificates", _GlobalResources.ThisUserMaySee, groups));

            row6.Cells.Add(row6Cell1);
            row6.Cells.Add(row6Cell2);
            permissionsTable.Rows.Add(row6);

            /*
            // reporter - xAPI dataset
            TableRow row7 = new TableRow();
            TableCell row7Cell1 = new TableCell();
            TableCell row7Cell2 = new TableCell();

            this._Reporting_Reporter_XAPI = new CheckBox();
            this._Reporting_Reporter_XAPI.ID = "Reporting_Reporter_XAPI";
            this._Reporting_Reporter_XAPI.Text = _GlobalResources.ReportsxAPIReports;
            this._Reporting_Reporter_XAPI.Attributes.Add("onclick", "TogglePermission(\"" + this._Reporting_Reporter_XAPI.ID + "\", ReportingPermissionsCheckBoxes, \"ReportingPermissionsSelectAll\", true)");
            row7Cell1.Controls.Add(this._Reporting_Reporter_XAPI);

            // defined scope
            row7Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_XAPI", _GlobalResources.ThisUserMaySee, groups));

            row7.Cells.Add(row7Cell1);
            row7.Cells.Add(row7Cell2);
            permissionsTable.Rows.Add(row7);
            */

            // reporter - purchases dataset - only build if ecommerce is set and verified
            if (this._EcommerceSettings.IsEcommerceSetAndVerified)
            {
                TableRow row8 = new TableRow();
                TableCell row8Cell1 = new TableCell();
                TableCell row8Cell2 = new TableCell();

                this._Reporting_Reporter_Purchases = new CheckBox();
                this._Reporting_Reporter_Purchases.ID = "Reporting_Reporter_Purchases";
                this._Reporting_Reporter_Purchases.Text = _GlobalResources.ReportsPurchases;
                this._Reporting_Reporter_Purchases.Attributes.Add("onclick", "TogglePermission(\"" + this._Reporting_Reporter_Purchases.ID + "\", ReportingPermissionsCheckBoxes, \"ReportingPermissionsSelectAll\", true)");
                row8Cell1.Controls.Add(this._Reporting_Reporter_Purchases);

                // defined scope
                row8Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_Purchases", _GlobalResources.ThisUserMaySee, groups));

                row8.Cells.Add(row8Cell1);
                row8.Cells.Add(row8Cell2);
                permissionsTable.Rows.Add(row8);
            }

            // reporter - certifications dataset - only build if certifications is enabled
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
            {
                TableRow row9 = new TableRow();
                TableCell row9Cell1 = new TableCell();
                TableCell row9Cell2 = new TableCell();

                this._Reporting_Reporter_UserCertificationTranscripts = new CheckBox();
                this._Reporting_Reporter_UserCertificationTranscripts.ID = "Reporting_Reporter_UserCertificationTranscripts";
                this._Reporting_Reporter_UserCertificationTranscripts.Text = _GlobalResources.ReportsUserCertificationTranscripts;
                this._Reporting_Reporter_UserCertificationTranscripts.Attributes.Add("onclick", "TogglePermission(\"" + this._Reporting_Reporter_UserCertificationTranscripts.ID + "\", ReportingPermissionsCheckBoxes, \"ReportingPermissionsSelectAll\", true)");
                row9Cell1.Controls.Add(this._Reporting_Reporter_UserCertificationTranscripts);

                // defined scope
                row9Cell2.Controls.Add(this._BuildScopeSelectorControls("Reporting_Reporter_UserCertificationTranscripts", _GlobalResources.ThisUserMaySee, groups));

                row9.Cells.Add(row9Cell1);
                row9.Cells.Add(row9Cell2);
                permissionsTable.Rows.Add(row9);
            }

            // attach the table and return
            permissionsPanel.Controls.Add(permissionsTable);
            return permissionsPanel;
        }
        #endregion

        #region _BuildScopeSelectorControls
        private Panel _BuildScopeSelectorControls(string idPrefix, string headingText, DataTable groups, bool definedScopeIsUsersWithinGroups = true)
        {
            // container
            Panel scopeSelectorContainer = new Panel();
            scopeSelectorContainer.ID = idPrefix + "_ScopeSelectorContainer";

            // heading label
            Panel scopeSelectorHeadingContainer = new Panel();
            Literal scopeSelectorHeading = new Literal();
            scopeSelectorHeading.Text = headingText + ":";
            scopeSelectorHeadingContainer.Controls.Add(scopeSelectorHeading);
            scopeSelectorContainer.Controls.Add(scopeSelectorHeadingContainer);

            // scope type radio buttons
            RadioButtonList scopeType = new RadioButtonList();
            scopeType.ID = idPrefix + "_ScopeType";
            scopeType.CssClass = "ScopeTypeContainer";

            ListItem globalScope = new ListItem();

            if (definedScopeIsUsersWithinGroups)
            { globalScope.Text = _GlobalResources.anyuser; }
            else
            { globalScope.Text = _GlobalResources.anygroup; }

            globalScope.Value = "global";
            globalScope.Attributes.Add("onclick", "ToggleGlobalScopeOptionsForPermission(\"" + idPrefix + "\")");
            scopeType.Items.Add(globalScope);

            ListItem definedScope = new ListItem();

            if (definedScopeIsUsersWithinGroups)
            { definedScope.Text = _GlobalResources.onlyuserswithinthegroupsselectedbelow; }
            else
            { definedScope.Text = _GlobalResources.onlythegroupsselectedbelow; }

            definedScope.Value = "scope";
            definedScope.Attributes.Add("onclick", "ToggleGlobalScopeOptionsForPermission(\"" + idPrefix + "\")");
            scopeType.Items.Add(definedScope);

            scopeSelectorContainer.Controls.Add(scopeType);

            // scoped items
            Panel scopedItemsContainer = new Panel();
            scopedItemsContainer.ID = idPrefix + "_ScopedItemsContainer";

            Panel scopedItemsCheckboxesContainer = new Panel();
            scopedItemsCheckboxesContainer.CssClass = "ScopedItemsCheckboxContainer";

            // loop through groups and add them to the selector
            foreach (DataRow row in groups.Rows)
            {
                CheckBox groupScope = new CheckBox();
                groupScope.ID = idPrefix + "_Scope_" + row["idGroup"].ToString();
                groupScope.Text = row["name"].ToString();
                scopedItemsCheckboxesContainer.Controls.Add(groupScope);
            }

            scopedItemsContainer.Controls.Add(scopedItemsCheckboxesContainer);

            // all/none selector
            Panel scopedItemsAllNoneSelectorContainer = new Panel();
            scopedItemsAllNoneSelectorContainer.ID = idPrefix + "_ScopedItemsAllNoneSelectorContainer";

            Label selectLabel = new Label();
            selectLabel.Text = _GlobalResources.Select + ": ";
            scopedItemsAllNoneSelectorContainer.Controls.Add(selectLabel);

            HyperLink selectAll = new HyperLink();
            selectAll.ID = idPrefix + "_SelectAllScope";
            selectAll.Text = _GlobalResources.All;
            selectAll.NavigateUrl = "javascript:void(0);";
            selectAll.Attributes.Add("onclick", "SelectScopesForPermission(\"" + idPrefix + "\", true);");
            scopedItemsAllNoneSelectorContainer.Controls.Add(selectAll);

            Literal separator = new Literal();
            separator.Text = " | ";
            scopedItemsAllNoneSelectorContainer.Controls.Add(separator);

            HyperLink selectNone = new HyperLink();
            selectNone.ID = idPrefix + "_SelectNoneScope";
            selectNone.Text = _GlobalResources.None;
            selectNone.NavigateUrl = "javascript:void(0);";
            selectNone.Attributes.Add("onclick", "SelectScopesForPermission(\"" + idPrefix + "\", false);");
            scopedItemsAllNoneSelectorContainer.Controls.Add(selectNone);

            scopedItemsContainer.Controls.Add(scopedItemsAllNoneSelectorContainer);

            // attach scoped items container to scope selector
            scopeSelectorContainer.Controls.Add(scopedItemsContainer);

            // hidden field for scoped items
            HiddenField scopedItemsHiddenField = new HiddenField();
            scopedItemsHiddenField.ID = idPrefix + "_ScopedItemsHiddenField";
            scopeSelectorContainer.Controls.Add(scopedItemsHiddenField);
            
            // return
            return scopeSelectorContainer;
        }
        #endregion

        #region _BuildRolePropertiesFormRulesetsPanel
        private void _BuildRolePropertiesFormRulesetsPanel()
        {
            Panel ruleSetsContainer = new Panel();
            ruleSetsContainer.ID = "RoleProperties_" + "RuleSets" + "_TabPanel";
            ruleSetsContainer.Attributes.Add("style", "display: none;");

            Panel ruleSetsOptionsPanel = new Panel();
            ruleSetsOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD RULESET
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddRuleSetLink",
                                                null,
                                                "javascript: void(0);",
                                                "OnRuleSetModifyClick(0); return false;",
                                                _GlobalResources.NewRuleset,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            ruleSetsOptionsPanel.Controls.Add(optionsPanelLinksContainer);

            // RULESETS GRID

            UpdatePanel ruleSetsUpdatePanel = new UpdatePanel();
            ruleSetsUpdatePanel.ID = "RolePropertiesGridUpdatePanel";

            this._RuleSetsGrid = new Grid();
            this._RuleSetsGrid.ID = "RuleSetsGrid";
            this._RuleSetsGrid.ShowSearchBox = false;
            this._RuleSetsGrid.StoredProcedure = Library.RuleSet.GridProcedureForRoles;
            this._RuleSetsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._RuleSetsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._RuleSetsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._RuleSetsGrid.AddFilter("@idRole", SqlDbType.Int, 4, this._RoleObject.Id);
            this._RuleSetsGrid.IdentifierField = "idRuleSet";
            this._RuleSetsGrid.DefaultSortColumn = "name";

            // data key names
            this._RuleSetsGrid.DataKeyNames = new string[] { "idRuleSet" };

            // columns
            GridColumn label = new GridColumn(_GlobalResources.Label, "label", "label");

            GridColumn modify = new GridColumn(_GlobalResources.Modify, "isModifyOn", true);
            modify.AddProperty(new GridColumnProperty("True", "<a href=\"javascript:void(0);\" onclick=\"OnRuleSetModifyClick(##idRuleSet##);return false;\">"
                                                                + "<img class=\"SmallIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.Modify + "\" />"
                                                                + "</a>"));
            modify.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.ModifyDisabled + "\" />"));

            // add columns to data grid
            this._RuleSetsGrid.AddColumn(label);
            this._RuleSetsGrid.AddColumn(modify);

            // attach grid to update panel
            ruleSetsUpdatePanel.ContentTemplateContainer.Controls.Add(ruleSetsOptionsPanel);
            ruleSetsUpdatePanel.ContentTemplateContainer.Controls.Add(this._RuleSetsGrid);

            // attach update panel to container
            ruleSetsContainer.Controls.Add(ruleSetsUpdatePanel);

            // bind the grid
            this._RuleSetsGrid.BindData();

            // GRID ACTIONS PANEL

            Panel gridActionsPanel = new Panel();
            gridActionsPanel.ID = "GridActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedRuleset_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            gridActionsPanel.Controls.Add(this._DeleteButton);

            // attach panel to container
            ruleSetsContainer.Controls.Add(gridActionsPanel);

            // GRID ACTIONS MODAL

            this._BuildGridActionsModal(ruleSetsContainer);

            // ADD/MODIFY RULESET MODAL

            Panel addModifyRuleSetModalContainer = new Panel();
            addModifyRuleSetModalContainer.ID = "AddModifyRuleSetModalContainer";

            this._AddModifyRuleSetHiddenButton.ClientIDMode = ClientIDMode.Static;
            this._AddModifyRuleSetHiddenButton.ID = "AddModifyRuleSetHiddenButton";
            this._AddModifyRuleSetHiddenButton.Style.Add("display", "none");

            this._BuildAddModifyRuleSetModal(this._AddModifyRuleSetHiddenButton.ID, addModifyRuleSetModalContainer);

            this._IdRoleHiddenField = new HiddenField();
            this._IdRoleHiddenField.ID = "IdRoleHiddenField";
            this._IdRoleHiddenField.Value = Convert.ToString(this._RoleObject.Id);

            this._ModifyRulesetId = new HiddenField();
            this._ModifyRulesetId.ID = "ModifyRulesetId";
            this._ModifyRulesetId.Value = this.QueryStringString("rsid", String.Empty);
            ruleSetsContainer.Controls.Add(this._ModifyRulesetId);

            ruleSetsContainer.Controls.Add(this._IdRoleHiddenField);
            ruleSetsContainer.Controls.Add(this._AddModifyRuleSetHiddenButton);

            ruleSetsContainer.Controls.Add(addModifyRuleSetModalContainer);

            // attach rule sets container to tab panels container
            this.RolePropertiesTabPanelsContainer.Controls.Add(ruleSetsContainer);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal(Panel container)
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmActionModal");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                            ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedRuleset_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteRuleSetsButton_Command);


            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseRuleset_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            container.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteRuleSetsButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteRuleSetsButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._RuleSetsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._RuleSetsGrid.Rows[i].FindControl(this._RuleSetsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.RuleSet.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this.RolePropertiesFeedbackContainer, _GlobalResources.TheSelectedRuleset_sHaveBeenDeletedSuccessfully, false);

                    // rebind the grid
                    this._RuleSetsGrid.BindData();
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoRole_sSelectedForDeletion, true);
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RolePropertiesFeedbackContainer, ex.Message, true);

                // rebind the grid
                this._RuleSetsGrid.BindData();
            }
        }
        #endregion

        #region _BuildAddModifyRuleSetModal
        /// <summary>
        /// _BuildAddModifyRuleSetModal
        /// </summary>
        private void _BuildAddModifyRuleSetModal(string targetControlId, Panel modalContainer)
        {
            modalContainer.Controls.Clear();

            EnsureChildControls();

            this._AddModifyRuleSetModal = new ModalPopup("AddModifyRuleSetModal");
            this._AddModifyRuleSetModal.ID = "AddModifyRuleSetModal";
            this._AddModifyRuleSetModal.CssClass += " AddModifyRuleSetModal";
            this._AddModifyRuleSetModal.Type = ModalPopupType.Form;
            this._AddModifyRuleSetModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT,
                                                                                ImageFiles.EXT_PNG);
            this._AddModifyRuleSetModal.HeaderIconAlt = _GlobalResources.CreateModifyRuleset;
            this._AddModifyRuleSetModal.HeaderText = _GlobalResources.ModifyRuleset;
            this._AddModifyRuleSetModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._AddModifyRuleSetModal.SubmitButtonCustomText = _GlobalResources.CreateRuleset;
            this._AddModifyRuleSetModal.SubmitButton.Command += new CommandEventHandler(this._AddModifyRuleSetModalSubmitButton_Command);
            this._AddModifyRuleSetModal.CloseButtonTextType = ModalPopupButtonText.Close;
            this._AddModifyRuleSetModal.SubmitButton.OnClientClick = "getRules(this,'" + this._RulesData.ClientID + "')";
            this._AddModifyRuleSetModal.TargetControlID = targetControlId;

            this._AddModifyRuleSetModal.SubmitButton.Visible = false;
            this._AddModifyRuleSetModal.CloseButton.Visible = false;

            //adding custom  own submit button to cahnge text 
            this._SubmitButtonMadal = new Button();
            this._SubmitButtonMadal.Text = _GlobalResources.CreateRuleset;
            this._SubmitButtonMadal.OnClientClick = "getRules(this,'" + this._RulesData.ClientID + "')";
            this._SubmitButtonMadal.Command += new CommandEventHandler(this._AddModifyRuleSetModalSubmitButton_Command);
            this._SubmitButtonMadal.CssClass = "Button ActionButton SaveButton";

            //adding custom  own close button to cahnge text 
            this._CloseButtonMadal = new Button();
            this._CloseButtonMadal.Text = _GlobalResources.Close;
            this._CloseButtonMadal.CssClass = "Button NonActionButton";
            this._CloseButtonMadal.OnClientClick = "HideModalPopup();return false;";

            //adding hidden maintenance submit button to simulate button click.            
            this._AddModifyRuleSetModalButton.ClientIDMode = ClientIDMode.Static;
            this._AddModifyRuleSetModalButton.ID = "ModifyRuleSetModalButton";
            this._AddModifyRuleSetModalButton.Command += new CommandEventHandler(this._AddModifyRuleSetModalButton_Command);
            this._AddModifyRuleSetModalButton.Style.Add("display", "none");

            this._AddModifyRuleSetModal.ReloadPageOnClose = false;

            this._RuleSetModalPropertiesContainer = new Panel();
            this._RuleSetModalPropertiesContainer.ID = "RuleSetModalPropertiesContainer";

            //add controls to container
            this._RuleSetModalPropertiesContainer.Controls.Add(this._AddModifyRuleSetModalButton);

            // name container
            this._RuleSetLabel = new TextBox();
            this._RuleSetLabel.ID = "RuleSetLabel_Field";
            this._RuleSetLabel.CssClass = "InputMedium";

            this._RuleSetModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("RuleSetLabel",
                                                             _GlobalResources.Name,
                                                             this._RuleSetLabel.ID,
                                                             this._RuleSetLabel,
                                                             true,
                                                             true,
                                                             true));

            //Match any container
            Panel matchAnyFieldContainer = new Panel();
            matchAnyFieldContainer.ID = "MatchAny_Container";
            matchAnyFieldContainer.CssClass = "FormFieldContainer";

            // Match any input
            Panel matchAnyFieldInputContainer = new Panel();
            matchAnyFieldInputContainer.ID = "MatchAny_InputContainer";
            matchAnyFieldInputContainer.CssClass = "FormFieldInputContainer";

            this._MatchAny = new RadioButton();
            this._MatchAny.ID = "matchAnyRadioButton";
            this._MatchAny.Text = _GlobalResources.MatchAny;
            this._MatchAny.Checked = true;
            this._MatchAny.GroupName = "matchAny";

            this._MatchAll = new RadioButton();
            this._MatchAll.ID = "MatchAllRadioButton";
            this._MatchAll.Text = _GlobalResources.MatchAll;
            this._MatchAll.GroupName = "matchAny";

            matchAnyFieldInputContainer.Controls.Add(this._MatchAny);
            matchAnyFieldInputContainer.Controls.Add(this._MatchAll);

            // Match any label
            Panel matchAnyFieldLabelContainer = new Panel();
            matchAnyFieldLabelContainer.ID = "MatchAny_LabelContainer";
            matchAnyFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label matchAnyFieldLabel = new Label();
            matchAnyFieldLabel.Text = _GlobalResources.Match + ": ";

            matchAnyFieldLabelContainer.Controls.Add(matchAnyFieldLabel);

            // add controls to container
            matchAnyFieldContainer.Controls.Add(matchAnyFieldLabelContainer);
            matchAnyFieldContainer.Controls.Add(matchAnyFieldInputContainer);

            this._RuleSetModalPropertiesContainer.Controls.Add(matchAnyFieldContainer);

            // rules container
            Panel rulesFieldContainer = new Panel();
            rulesFieldContainer.ID = "Rule_Container";
            rulesFieldContainer.CssClass = "FormFieldContainer";

            // rules input
            Panel rulesFieldInputContainer = new Panel();
            rulesFieldInputContainer.ID = "Rule_InputContainer";
            rulesFieldInputContainer.CssClass = "FormFieldInputContainer";

            rulesFieldInputContainer.Controls.Add(this._RuleSetControlPlaceHolder);

            // rules label
            Panel rulesFieldLabelContainer = new Panel();
            rulesFieldLabelContainer.ID = "Rule_LabelContainer";
            rulesFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label rulesFieldLabel = new Label();
            rulesFieldLabel.Text = _GlobalResources.Name + ": ";
            rulesFieldLabel.AssociatedControlID = this._RuleSetControlPlaceHolder.ID;

            Label requiredAsterisk = new Label();
            requiredAsterisk.Text = "* ";
            requiredAsterisk.CssClass = "RequiredAsterisk";
            rulesFieldLabelContainer.Controls.Add(requiredAsterisk);

            rulesFieldLabelContainer.Controls.Add(rulesFieldLabel);

            // error panel
            Panel rulesFieldErrorContainer = new Panel();
            rulesFieldErrorContainer.ID = "RuleSetRule_ErrorContainer";
            rulesFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to container
            rulesFieldContainer.Controls.Add(rulesFieldLabelContainer);
            rulesFieldContainer.Controls.Add(rulesFieldErrorContainer);
            rulesFieldContainer.Controls.Add(rulesFieldInputContainer);

            this._RuleSetModalPropertiesContainer.Controls.Add(rulesFieldContainer);
            this._RuleSetModalPropertiesContainer.Controls.Add(this._RulesData);

            this._IdRulesetModalHidden.ClientIDMode = ClientIDMode.Static;
            this._IdRulesetModalHidden.ID = "IdRulesetModalHidden";

            this._RuleSetModalPropertiesContainer.Controls.Add(this._IdRulesetModalHidden);

            this._AddModifyRuleSetModal.AddControlToBody(this._RuleSetModalPropertiesContainer);
            this._AddModifyRuleSetModal.AddControlToBody(this._SubmitButtonMadal);
            this._AddModifyRuleSetModal.AddControlToBody(this._CloseButtonMadal);
            modalContainer.Controls.Add(this._AddModifyRuleSetModal);

            EnsureChildControls();

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulateRuleSetPropertiesInputElements();
        }
        #endregion

        #region _AddModifyRuleSetModalSubmitButton_Command
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> </param>
        private void _AddModifyRuleSetModalSubmitButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable rulesData = new DataTable();

                if (!String.IsNullOrWhiteSpace(this._RulesData.Value))
                {
                    rulesData.Columns.Add("idRule", typeof(int));
                    rulesData.Columns.Add("userField", typeof(string));
                    rulesData.Columns.Add("operator", typeof(string));
                    rulesData.Columns.Add("textValue", typeof(string));

                    List<RuleObject> allRules = new JavaScriptSerializer().Deserialize<List<RuleObject>>(this._RulesData.Value);

                    foreach (RuleObject rule in allRules)
                    {
                        int idRule = 0;
                        int.TryParse(rule.IdRule, out idRule);
                        rulesData.Rows.Add(idRule, rule.UserField, rule.Operator, rule.Value);
                    }

                    if (rulesData.Rows.Count > 0)
                    {
                        this._AutoJoinRuleSet = new AutoJoinRuleSet(rulesData);
                        this._AutoJoinRuleSet.ID = "Rule_Field";
                        this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);
                        this._RuleSetControlLoaded = true;
                    }

                    this._RulesData.Value = string.Empty;
                }

                int idRuleSet = Convert.ToInt32(this._IdRulesetModalHidden.Value);

                if (idRuleSet > 0)
                {
                    this._RuleSetObject = new RuleSet(idRuleSet);
                }

                // if there is no rule set object, create one
                if (this._RuleSetObject == null)
                { this._RuleSetObject = new RuleSet(); }

                // validate the form
                if (!this._ValidateRuleSetPropertiesForm())
                { throw new AsentiaException(); }

                // if this is an existing rule set, and not the default language, save language properties only
                // otherwise, save the whole object in its default language
                int id;

                // populate the object
                this._RuleSetObject.Label = this._RuleSetLabel.Text;

                if (this._MatchAny.Checked)
                { this._RuleSetObject.IsAny = true; }
                else
                { this._RuleSetObject.IsAny = false; }

                // save the user, save its returned id to viewstate, and 
                // instansiate a new user object with the id
                this._RuleSetObject.LinkedObjectId = this._RoleObject.Id;
                this._RuleSetObject.LinkedObjectType = RuleSetLinkedObjectType.Role;

                id = this._RuleSetObject.Save();

                Asentia.LMS.Library.Rule rules = new Asentia.LMS.Library.Rule();
                rules.IdRuleSet = id;

                if (this._Rules.Rows.Count > 0)
                {
                    rules.Save(this._Rules);
                }

                this._IdRulesetModalHidden.Value = id.ToString();

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string ruleSetTitle = null;

                        // get text boxes
                        TextBox languageSpecificRuleSetTitleTextBox = (TextBox)this._RuleSetModalPropertiesContainer.FindControl(this._RuleSetLabel.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificRuleSetTitleTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificRuleSetTitleTextBox.Text))
                            { ruleSetTitle = languageSpecificRuleSetTitleTextBox.Text; }
                        }

                        // save the property if property is populated
                        if (!String.IsNullOrWhiteSpace(ruleSetTitle))
                        {
                            this._RuleSetObject.SaveLang(cultureInfo.Name,
                                                         ruleSetTitle);
                        }
                    }
                }

                this._AddModifyRuleSetModal.SubmitButton.Text = _GlobalResources.SaveChanges;

                // load the saved ruleset object
                this._RuleSetObject = new RuleSet(id);

                // rebind the grid and other controls;
                this._BuildControls();

                this._AddModifyRuleSetModal.SubmitButton.Text = _GlobalResources.SaveChanges;

                // display the saved feedback
                this._AddModifyRuleSetModal.DisplayFeedback(_GlobalResources.RulesetPropertiesHaveBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dfnuEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dfnuEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _AddModifyRuleSetModalButton_Command
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AddModifyRuleSetModalButton_Command(object sender, CommandEventArgs e)
        {
            this._AddModifyRuleSetModal.CssClass = String.Empty;

            int idRuleSet = Convert.ToInt32(this._IdRulesetModalHidden.Value);

            if (!this._RuleSetControlLoaded)
            {
                if (idRuleSet > 0)
                {
                    this._RuleSetObject = new RuleSet(idRuleSet);
                    this._AutoJoinRuleSet = new AutoJoinRuleSet(idRuleSet);
                    this._SubmitButtonMadal.Text = _GlobalResources.SaveChanges;
                    this._AddModifyRuleSetModal.HeaderText = _GlobalResources.ModifyRuleset;
                }
                else
                {
                    this._RuleSetObject = new RuleSet();
                    this._AutoJoinRuleSet = new AutoJoinRuleSet();
                    this._SubmitButtonMadal.Text = _GlobalResources.CreateRuleset;
                    this._AddModifyRuleSetModal.HeaderText = _GlobalResources.NewRuleset;
                }

                this._AutoJoinRuleSet.ID = "Rule_Field";
                this._AutoJoinRuleSet.ClientIDMode = ClientIDMode.Static;
                this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);

                EnsureChildControls();

                this._PopulateRuleSetPropertiesInputElements();
            }
        }
        #endregion

        #region _PopulateRuleSetPropertiesInputElements
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _PopulateRuleSetPropertiesInputElements()
        {
            if (this._RuleSetObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // ruleset label
                bool isDefaultPopulated = false;

                foreach (RuleSet.LanguageSpecificProperty ruleSetLanguageSpecificProperty in this._RuleSetObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (ruleSetLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._RuleSetLabel.Text = ruleSetLanguageSpecificProperty.Label;
                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificRuleSetLabelTextBox = (TextBox)this._RuleSetModalPropertiesContainer.FindControl(this._RuleSetLabel.ID + "_" + ruleSetLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificRuleSetLabelTextBox != null)
                        { languageSpecificRuleSetLabelTextBox.Text = ruleSetLanguageSpecificProperty.Label; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._RuleSetLabel.Text = this._RuleSetObject.Label;
                }

                // NON LANGUAGE SPECIFIC PROPERTIES
                if (this._RuleSetObject.IsAny)
                {
                    this._MatchAny.Checked = true;
                }
                else
                {
                    this._MatchAll.Checked = true;
                }
            }
            else
            {
                this._RuleSetLabel.Text = String.Empty;
                this._MatchAny.Checked = true;
            }

        }
        #endregion

        #region _ValidateRuleSetPropertiesForm
        /// <summary>
        /// Validates the ruleset properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateRuleSetPropertiesForm()
        {
            bool isValid = true;

            // label field
            if (String.IsNullOrWhiteSpace(this._RuleSetLabel.Text.Trim()))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "Label", _GlobalResources.Label + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // rules control
            if (this._AutoJoinRuleSet != null)
            {
                this._Rules = this._AutoJoinRuleSet.GetRulesAfterValidatingData();

                if (this._Rules == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "RuleSetRule", _GlobalResources.OneOrMoreRulesAreInvalidHoverOverField_sToViewSpecificError_s);
                }
                else if (this._Rules.Rows.Count < 1)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "RuleSetRule", _GlobalResources.YouMustHaveAtLeastOneRule);
                }
            }

            return isValid;
        }
        #endregion

        #region _BuildRolePropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for role properties actions.
        /// </summary>
        private void _BuildRolePropertiesActionsPanel()
        {
            this.RolePropertiesActionsPanel = new Panel();
            this.RolePropertiesActionsPanel.ID = "RolePropertiesActionsPanel";

            // clear controls from container
            this.RolePropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.RolePropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button - only if this role is not view only (system built-in)
            if (!this._IsViewOnly)
            {
                this._RolePropertiesSaveButton = new Button();
                this._RolePropertiesSaveButton.ID = "RolePropertiesSaveButton";
                this._RolePropertiesSaveButton.CssClass = "Button ActionButton SaveButton";

                // if the object is null, it's a new object, so make button text say "Create"
                if (this._RoleObject == null)
                { this._RolePropertiesSaveButton.Text = _GlobalResources.CreateRole; }
                else
                { this._RolePropertiesSaveButton.Text = _GlobalResources.SaveChanges; }

                this._RolePropertiesSaveButton.Command += new CommandEventHandler(this._RolePropertiesSaveButton_Command);
                this._RolePropertiesSaveButton.Attributes.Add("onclick", "PopulateHiddenFieldsForPermissionScopes();");
                this.RolePropertiesActionsPanel.Controls.Add(this._RolePropertiesSaveButton);
            }

            // cancel button - done button if "view only"
            this._RolePropertiesCancelButton = new Button();
            this._RolePropertiesCancelButton.ID = "RolePropertiesCancelButton";
            this._RolePropertiesCancelButton.CssClass = "Button NonActionButton";

            if (this._IsViewOnly)
            {
                this._RolePropertiesCancelButton.CssClass = "Button ActionButton SaveButton";
                this._RolePropertiesCancelButton.Text = _GlobalResources.Done; 
            }
            else
            { this._RolePropertiesCancelButton.Text = _GlobalResources.Cancel; }
            
            this._RolePropertiesCancelButton.Command += new CommandEventHandler(this._RolePropertiesCancelButton_Command);
            this.RolePropertiesActionsPanel.Controls.Add(this._RolePropertiesCancelButton);

            Panel propertiesFormPanel = (Panel)this.RolePropertiesContainer.FindControl("RoleProperties_" + "Properties" + "_TabPanel");
            propertiesFormPanel.Controls.Add(this.RolePropertiesActionsPanel);
        }
        #endregion

        #region _PopulateRolePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateRolePropertiesInputElements()
        {
            if (this._RoleObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // name
                bool isDefaultPopulated = false;

                foreach (Role.LanguageSpecificProperty roleLanguageSpecificProperty in this._RoleObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (roleLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._Name.Text = roleLanguageSpecificProperty.Name;

                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificRoleNameTextBox = (TextBox)this.RolePropertiesContainer.FindControl(this._Name.ID + "_" + roleLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificRoleNameTextBox != null)
                        { languageSpecificRoleNameTextBox.Text = roleLanguageSpecificProperty.Name; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._Name.Text = this._RoleObject.Name;
                }

                // PERMISSIONS

                foreach (Role.PermissionWithScope permission in this._RoleObject.Permissions)
                {
                    switch (permission.PermissionCode)
                    {
                        case AsentiaPermission.System_AccountSettings:
                            this._System_AccountSettings.Checked = true;
                            this._PopulatePermissionScope(this._System_AccountSettings.ID, permission.Scope);
                            break;
                        case AsentiaPermission.System_Configuration:
                            this._System_Configuration.Checked = true;
                            this._PopulatePermissionScope(this._System_Configuration.ID, permission.Scope);
                            break;
                        case AsentiaPermission.System_UserFieldConfiguration:
                            this._System_UserFieldConfiguration.Checked = true;
                            this._PopulatePermissionScope(this._System_UserFieldConfiguration.ID, permission.Scope);
                            break;
                        case AsentiaPermission.System_RulesEngine:
                            this._System_RulesEngine.Checked = true;
                            this._PopulatePermissionScope(this._System_RulesEngine.ID, permission.Scope);
                            break;
                        case AsentiaPermission.System_Ecommerce:
                            this._System_Ecommerce.Checked = true;
                            this._PopulatePermissionScope(this._System_Ecommerce.ID, permission.Scope);
                            break;
                        case AsentiaPermission.System_CouponCodes:
                            this._System_CouponCodes.Checked = true;
                            this._PopulatePermissionScope(this._System_CouponCodes.ID, permission.Scope);
                            break;
                        case AsentiaPermission.System_TrackingCodes:
                            this._System_TrackingCodes.Checked = true;
                            this._PopulatePermissionScope(this._System_TrackingCodes.ID, permission.Scope);
                            break;
                        case AsentiaPermission.System_EmailNotifications:
                            this._System_EmailNotifications.Checked = true;
                            this._PopulatePermissionScope(this._System_EmailNotifications.ID, permission.Scope);
                            break;
                        case AsentiaPermission.System_API:
                            this._System_API.Checked = true;
                            this._PopulatePermissionScope(this._System_API.ID, permission.Scope);
                            break;
                        case AsentiaPermission.System_xAPIEndpoints:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.XAPIENDPOINTS_ENABLE))
                            {
                                this._System_xAPIEndpoints.Checked = true;
                                this._PopulatePermissionScope(this._System_xAPIEndpoints.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.System_Logs:
                            this._System_Logs.Checked = true;
                            this._PopulatePermissionScope(this._System_Logs.ID, permission.Scope);
                            break;
                        case AsentiaPermission.System_WebMeetingIntegration:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE)
                                || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE)
                                || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE)
                                || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE))
                            {
                                this._System_WebMeetingIntegration.Checked = true;
                                this._PopulatePermissionScope(this._System_WebMeetingIntegration.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.UsersAndGroups_UserCreator:
                            this._UsersAndGroups_UserCreator.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_UserCreator.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_UserDeleter:
                            this._UsersAndGroups_UserDeleter.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_UserDeleter.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_UserEditor:
                            this._UsersAndGroups_UserEditor.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_UserEditor.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_UserManager:
                            this._UsersAndGroups_UserManager.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_UserManager.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_UserImpersonator:
                            this._UsersAndGroups_UserImpersonator.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_UserImpersonator.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_GroupCreator:
                            this._UsersAndGroups_GroupCreator.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_GroupCreator.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_GroupDeleter:
                            this._UsersAndGroups_GroupDeleter.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_GroupDeleter.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_GroupEditor:
                            this._UsersAndGroups_GroupEditor.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_GroupEditor.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_GroupManager:
                            this._UsersAndGroups_GroupManager.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_GroupManager.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_RoleManager:
                            this._UsersAndGroups_RoleManager.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_RoleManager.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_ActivityImport:
                            this._UsersAndGroups_ActivityImport.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_ActivityImport.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_CertificateImport:
                            this._UsersAndGroups_CertificateImport.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_CertificateImport.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_Leaderboards:
                            this._UsersAndGroups_Leaderboards.Checked = true;
                            this._PopulatePermissionScope(this._UsersAndGroups_Leaderboards.ID, permission.Scope);
                            break;
                        case AsentiaPermission.UsersAndGroups_UserRegistrationApproval:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERREGISTRATIONAPPROVAL_ENABLE))
                            {
                                this._UsersAndGroups_UserRegistrationApproval.Checked = true;
                                this._PopulatePermissionScope(this._UsersAndGroups_UserRegistrationApproval.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.InterfaceAndLayout_HomePage:
                            this._InterfaceAndLayout_HomePage.Checked = true;
                            this._PopulatePermissionScope(this._InterfaceAndLayout_HomePage.ID, permission.Scope);
                            break;
                        case AsentiaPermission.InterfaceAndLayout_Masthead:
                            this._InterfaceAndLayout_Masthead.Checked = true;
                            this._PopulatePermissionScope(this._InterfaceAndLayout_Masthead.ID, permission.Scope);
                            break;
                        case AsentiaPermission.InterfaceAndLayout_Footer:
                            this._InterfaceAndLayout_Footer.Checked = true;
                            this._PopulatePermissionScope(this._InterfaceAndLayout_Footer.ID, permission.Scope);
                            break;
                        case AsentiaPermission.InterfaceAndLayout_CSSEditor:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CSSEDITOR_ENABLE))
                            {
                                this._InterfaceAndLayout_CSSEditor.Checked = true;
                                this._PopulatePermissionScope(this._InterfaceAndLayout_CSSEditor.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.InterfaceAndLayout_ImageEditor:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.IMAGE_EDITOR_ENABLE))
                            {
                                this._InterfaceAndLayout_ImageEditor.Checked = true;
                                this._PopulatePermissionScope(this._InterfaceAndLayout_ImageEditor.ID, permission.Scope);
                            }
            
                            break;
                        case AsentiaPermission.LearningAssets_CourseCatalog:
                            this._LearningAssets_CourseCatalog.Checked = true;
                            this._PopulatePermissionScope(this._LearningAssets_CourseCatalog.ID, permission.Scope);
                            break;
                        case AsentiaPermission.LearningAssets_CourseContentManager:
                            this._LearningAssets_CourseManager.Checked = true;
                            this._PopulatePermissionScope(this._LearningAssets_CourseManager.ID, permission.Scope);
                            break;
                        case AsentiaPermission.LearningAssets_CourseEnrollmentManager:
                            this._LearningAssets_CourseEnrollmentManager.Checked = true;
                            this._PopulatePermissionScope(this._LearningAssets_CourseEnrollmentManager.ID, permission.Scope);
                            break;
                        case AsentiaPermission.LearningAssets_LearningPathContentManager:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                            {
                                this._LearningAssets_LearningPathManager.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_LearningPathManager.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.LearningAssets_LearningPathEnrollmentManager:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                            {
                                this._LearningAssets_LearningPathEnrollmentManager.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_LearningPathEnrollmentManager.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.LearningAssets_CertificationsManager:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                            {
                                this._LearningAssets_CertificationsManager.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_CertificationsManager.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.LearningAssets_ContentPackageManager:
                            this._LearningAssets_ContentPackageManager.Checked = true;
                            this._PopulatePermissionScope(this._LearningAssets_ContentPackageManager.ID, permission.Scope);
                            break;
                        case AsentiaPermission.LearningAssets_QuizAndSurveyManager:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE))
                            {
                                this._LearningAssets_QuizAndSurveyManager.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_QuizAndSurveyManager.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.LearningAssets_CertificateTemplateManager:
                            this._LearningAssets_CertificateTemplateManager.Checked = true;
                            this._PopulatePermissionScope(this._LearningAssets_CertificateTemplateManager.ID, permission.Scope);
                            break;
                        case AsentiaPermission.LearningAssets_InstructorLedTrainingManager:
                            this._LearningAssets_InstructorLedTrainingManager.Checked = true;
                            this._PopulatePermissionScope(this._LearningAssets_InstructorLedTrainingManager.ID, permission.Scope);
                            break;
                        case AsentiaPermission.LearningAssets_ResourceManager:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE))
                            {
                                this._LearningAssets_ResourceManager.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_ResourceManager.ID, permission.Scope);
                            }
                            break;
                        case AsentiaPermission.LearningAssets_SelfEnrollmentApproval:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
                            {
                                this._LearningAssets_SelfEnrollmentApproval.Checked = true;
                                this._PopulatePermissionScope(this._LearningAssets_SelfEnrollmentApproval.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.Reporting_Reporter_UserDemographics:
                            this._Reporting_Reporter_UserDemographics.Checked = true;
                            this._PopulatePermissionScope(this._Reporting_Reporter_UserDemographics.ID, permission.Scope);
                            break;
                        case AsentiaPermission.Reporting_Reporter_UserCourseTranscripts:
                            this._Reporting_Reporter_UserCourseTranscripts.Checked = true;
                            this._PopulatePermissionScope(this._Reporting_Reporter_UserCourseTranscripts.ID, permission.Scope);
                            break;
                        case AsentiaPermission.Reporting_Reporter_UserLearningPathTranscripts:
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                            {
                                this._Reporting_Reporter_UserLearningPathTranscripts.Checked = true;
                                this._PopulatePermissionScope(this._Reporting_Reporter_UserLearningPathTranscripts.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.Reporting_Reporter_UserInstructorLedTrainingTranscripts:
                            this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.Checked = true;
                            this._PopulatePermissionScope(this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.ID, permission.Scope);
                            break;
                        case AsentiaPermission.Reporting_Reporter_CatalogAndCourseInformation:
                            this._Reporting_Reporter_CatalogAndCourseInformation.Checked = true;
                            this._PopulatePermissionScope(this._Reporting_Reporter_CatalogAndCourseInformation.ID, permission.Scope);
                            break;
                        case AsentiaPermission.Reporting_Reporter_Certificates:
                            this._Reporting_Reporter_Certificates.Checked = true;
                            this._PopulatePermissionScope(this._Reporting_Reporter_Certificates.ID, permission.Scope);
                            break;
                        //case AsentiaPermission.Reporting_Reporter_XAPI:
                            //this._Reporting_Reporter_XAPI.Checked = true;
                        //this._PopulatePermissionScope(this._Reporting_Reporter_XAPI.ID, permission.Scope);
                            //break;
                        case AsentiaPermission.Reporting_Reporter_Purchases:
                            // only populate if ecommerce is set and verified
                            if (this._EcommerceSettings.IsEcommerceSetAndVerified)
                            {
                                this._Reporting_Reporter_Purchases.Checked = true;
                                this._PopulatePermissionScope(this._Reporting_Reporter_Purchases.ID, permission.Scope);
                            }

                            break;
                        case AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts:
                            // only populate if certifications is enabled
                            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                            {
                                this._Reporting_Reporter_UserCertificationTranscripts.Checked = true;
                                this._PopulatePermissionScope(this._Reporting_Reporter_UserCertificationTranscripts.ID, permission.Scope);
                            }

                            break;
                    }
                }
            }
        }
        #endregion

        #region _PopulatePermissionScope
        /// <summary>
        /// Populates the scoping options for the permission inputs.
        /// </summary>
        /// <param name="idPrefix">id of the permission checkbox</param>
        /// <param name="scope">scope list</param>
        private void _PopulatePermissionScope(string idPrefix, List<int> scope)
        {
            // get the global scope/item scope radio list
            RadioButtonList scopeType = (RadioButtonList)FindControlRecursive(this.RolePropertiesTabPanelsContainer, idPrefix + "_ScopeType");

            if (scope != null) // item scope
            {
                if (scopeType != null)
                {
                    // select "scope"
                    scopeType.SelectedValue = "scope";

                    // select each item in the scope list
                    foreach (int scopeItem in scope)
                    {
                        CheckBox scopeItemCheckBox = (CheckBox)FindControlRecursive(this.RolePropertiesTabPanelsContainer, idPrefix + "_Scope_" + scopeItem.ToString());

                        if (scopeItemCheckBox != null)
                        { scopeItemCheckBox.Checked = true; }
                    }
                }
            }
            else // global scope
            {
                // select "global"
                if (scopeType != null)
                { scopeType.SelectedValue = "global"; }
            }
        }
        #endregion

        #region _ValidateRolePropertiesForm
        private bool _ValidateRolePropertiesForm()
        {
            bool isValid = true;

            /* BASIC VALIDATIONS */

            // name field - default language required
            if (String.IsNullOrWhiteSpace(this._Name.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.RolePropertiesContainer, "RoleName", _GlobalResources.Label + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            return isValid;
        }
        #endregion

        #region _RolePropertiesSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for role properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RolePropertiesSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateRolePropertiesForm())
                { throw new AsentiaException(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain); }

                // if there is no role object, create one
                if (this._RoleObject == null)
                { this._RoleObject = new Role(); }

                // do not allow modification of system built-in roles
                if (this._RoleObject.Id >= 1 && this._RoleObject.Id < 100)
                { throw new AsentiaException(_GlobalResources.ThisRoleIsABuiltInSystemRoleAndCannotBeModified); }

                int id;

                // populate the object

                // name
                this._RoleObject.Name = this._Name.Text;

                // save the role, save its returned id to viewstate, and set the current role object's id
                id = this._RoleObject.Save();
                this.ViewState["id"] = id;
                this._RoleObject.Id = id;

                // do role language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string roleName = null;

                        // get text boxes
                        TextBox languageSpecificRoleNameTextBox = (TextBox)this.RolePropertiesContainer.FindControl(this._Name.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificRoleNameTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificRoleNameTextBox.Text))
                            { roleName = languageSpecificRoleNameTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(roleName))
                        {
                            this._RoleObject.SaveLang(cultureInfo.Name,
                                                      roleName);
                        }
                    }
                }

                // SAVE PERMISSIONS

                DataTable permissions = new DataTable(); ;
                permissions.Columns.Add("id", typeof(int));
                permissions.Columns.Add("scope", typeof(string));

                // SYSTEM

                if (this._System_AccountSettings.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_AccountSettings), this._GetSelectedScopeForPermission(this._System_AccountSettings.ID)); }

                if (this._System_Configuration.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_Configuration), this._GetSelectedScopeForPermission(this._System_Configuration.ID)); }

                if (this._System_UserFieldConfiguration.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_UserFieldConfiguration), this._GetSelectedScopeForPermission(this._System_UserFieldConfiguration.ID)); }

                if(this._System_RulesEngine.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_RulesEngine), this._GetSelectedScopeForPermission(this._System_RulesEngine.ID)); }
                
                if (this._System_Ecommerce.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_Ecommerce), this._GetSelectedScopeForPermission(this._System_Ecommerce.ID)); }

                if (this._System_CouponCodes.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_CouponCodes), this._GetSelectedScopeForPermission(this._System_CouponCodes.ID)); }

                if (this._System_TrackingCodes.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_TrackingCodes), this._GetSelectedScopeForPermission(this._System_TrackingCodes.ID)); }

                if (this._System_EmailNotifications.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_EmailNotifications), this._GetSelectedScopeForPermission(this._System_EmailNotifications.ID)); }

                if (this._System_API.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_API), this._GetSelectedScopeForPermission(this._System_API.ID)); }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.XAPIENDPOINTS_ENABLE))
                {
                    if (this._System_xAPIEndpoints.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_xAPIEndpoints), this._GetSelectedScopeForPermission(this._System_xAPIEndpoints.ID)); }
                }

                if (this._System_Logs.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_Logs), this._GetSelectedScopeForPermission(this._System_Logs.ID)); }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE))
                {
                    if (this._System_WebMeetingIntegration.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.System_WebMeetingIntegration), this._GetSelectedScopeForPermission(this._System_WebMeetingIntegration.ID)); }
                }

                // USERS AND GROUPS

                if (this._UsersAndGroups_UserCreator.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_UserCreator), this._GetSelectedScopeForPermission(this._UsersAndGroups_UserCreator.ID)); }

                if (this._UsersAndGroups_UserDeleter.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_UserDeleter), this._GetSelectedScopeForPermission(this._UsersAndGroups_UserDeleter.ID)); }

                if (this._UsersAndGroups_UserEditor.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_UserEditor), this._GetSelectedScopeForPermission(this._UsersAndGroups_UserEditor.ID)); }

                if (this._UsersAndGroups_UserManager.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_UserManager), this._GetSelectedScopeForPermission(this._UsersAndGroups_UserManager.ID)); }

                if (this._UsersAndGroups_UserImpersonator.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_UserImpersonator), this._GetSelectedScopeForPermission(this._UsersAndGroups_UserImpersonator.ID)); }

                if (this._UsersAndGroups_GroupCreator.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_GroupCreator), this._GetSelectedScopeForPermission(this._UsersAndGroups_GroupCreator.ID)); }

                if (this._UsersAndGroups_GroupDeleter.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_GroupDeleter), this._GetSelectedScopeForPermission(this._UsersAndGroups_GroupDeleter.ID)); }

                if (this._UsersAndGroups_GroupEditor.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_GroupEditor), this._GetSelectedScopeForPermission(this._UsersAndGroups_GroupEditor.ID)); }

                if (this._UsersAndGroups_GroupManager.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_GroupManager), this._GetSelectedScopeForPermission(this._UsersAndGroups_GroupManager.ID)); }

                // skip RoleManager, it's not saveable, it is built-in on "System Administrator" only

                if (this._UsersAndGroups_ActivityImport.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_ActivityImport), this._GetSelectedScopeForPermission(this._UsersAndGroups_ActivityImport.ID)); }

                if (this._UsersAndGroups_CertificateImport.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_CertificateImport), this._GetSelectedScopeForPermission(this._UsersAndGroups_CertificateImport.ID)); }

                if (this._UsersAndGroups_Leaderboards.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_Leaderboards), this._GetSelectedScopeForPermission(this._UsersAndGroups_Leaderboards.ID)); }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERREGISTRATIONAPPROVAL_ENABLE))
                {
                    if (this._UsersAndGroups_UserRegistrationApproval.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.UsersAndGroups_UserRegistrationApproval), this._GetSelectedScopeForPermission(this._UsersAndGroups_UserRegistrationApproval.ID)); }
                }

                // INTERFACE AND LAYOUT

                if (this._InterfaceAndLayout_HomePage.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.InterfaceAndLayout_HomePage), this._GetSelectedScopeForPermission(this._InterfaceAndLayout_HomePage.ID)); }

                if (this._InterfaceAndLayout_Masthead.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.InterfaceAndLayout_Masthead), this._GetSelectedScopeForPermission(this._InterfaceAndLayout_Masthead.ID)); }

                if (this._InterfaceAndLayout_Footer.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.InterfaceAndLayout_Footer), this._GetSelectedScopeForPermission(this._InterfaceAndLayout_Footer.ID)); }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CSSEDITOR_ENABLE))
                {
                    if (this._InterfaceAndLayout_CSSEditor.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.InterfaceAndLayout_CSSEditor), this._GetSelectedScopeForPermission(this._InterfaceAndLayout_CSSEditor.ID)); }
                }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.IMAGE_EDITOR_ENABLE))
                {
                    if (this._InterfaceAndLayout_ImageEditor.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.InterfaceAndLayout_ImageEditor), this._GetSelectedScopeForPermission(this._InterfaceAndLayout_ImageEditor.ID)); }
                }

                // LEARNING ASSETS

                if (this._LearningAssets_CourseCatalog.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_CourseCatalog), this._GetSelectedScopeForPermission(this._LearningAssets_CourseCatalog.ID)); }

                if (this._LearningAssets_CourseManager.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_CourseContentManager), this._GetSelectedScopeForPermission(this._LearningAssets_CourseManager.ID)); }

                if (this._LearningAssets_CourseEnrollmentManager.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_CourseEnrollmentManager), this._GetSelectedScopeForPermission(this._LearningAssets_CourseEnrollmentManager.ID)); }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    if (this._LearningAssets_LearningPathManager.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_LearningPathContentManager), this._GetSelectedScopeForPermission(this._LearningAssets_LearningPathManager.ID)); }

                    if (this._LearningAssets_LearningPathEnrollmentManager.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_LearningPathEnrollmentManager), this._GetSelectedScopeForPermission(this._LearningAssets_LearningPathEnrollmentManager.ID)); }
                }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                {
                    if (this._LearningAssets_CertificationsManager.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_CertificationsManager), this._GetSelectedScopeForPermission(this._LearningAssets_CertificationsManager.ID)); }
                }

                if (this._LearningAssets_ContentPackageManager.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_ContentPackageManager), this._GetSelectedScopeForPermission(this._LearningAssets_ContentPackageManager.ID)); }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE))
                {
                    if (this._LearningAssets_QuizAndSurveyManager.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_QuizAndSurveyManager), this._GetSelectedScopeForPermission(this._LearningAssets_QuizAndSurveyManager.ID)); }
                }

                if (this._LearningAssets_CertificateTemplateManager.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_CertificateTemplateManager), this._GetSelectedScopeForPermission(this._LearningAssets_CertificateTemplateManager.ID)); }

                if (this._LearningAssets_InstructorLedTrainingManager.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_InstructorLedTrainingManager), this._GetSelectedScopeForPermission(this._LearningAssets_InstructorLedTrainingManager.ID)); }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE))
                {
                    if (this._LearningAssets_ResourceManager.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_ResourceManager), this._GetSelectedScopeForPermission(this._LearningAssets_ResourceManager.ID)); }
                }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SELFENROLLMENTAPPROVAL_ENABLE))
                {
                    if (this._LearningAssets_SelfEnrollmentApproval.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.LearningAssets_SelfEnrollmentApproval), this._GetSelectedScopeForPermission(this._LearningAssets_SelfEnrollmentApproval.ID)); }
                }

                // REPORTING

                if (this._Reporting_Reporter_UserDemographics.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.Reporting_Reporter_UserDemographics), this._GetSelectedScopeForPermission(this._Reporting_Reporter_UserDemographics.ID)); }

                if (this._Reporting_Reporter_UserCourseTranscripts.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.Reporting_Reporter_UserCourseTranscripts), this._GetSelectedScopeForPermission(this._Reporting_Reporter_UserCourseTranscripts.ID)); }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    if (this._Reporting_Reporter_UserLearningPathTranscripts.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.Reporting_Reporter_UserLearningPathTranscripts), this._GetSelectedScopeForPermission(this._Reporting_Reporter_UserLearningPathTranscripts.ID)); }
                }

                if (this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.Reporting_Reporter_UserInstructorLedTrainingTranscripts), this._GetSelectedScopeForPermission(this._Reporting_Reporter_UserInstructorLedTrainingTranscripts.ID)); }

                if (this._Reporting_Reporter_CatalogAndCourseInformation.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.Reporting_Reporter_CatalogAndCourseInformation), this._GetSelectedScopeForPermission(this._Reporting_Reporter_CatalogAndCourseInformation.ID)); }

                if (this._Reporting_Reporter_Certificates.Checked)
                { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.Reporting_Reporter_Certificates), this._GetSelectedScopeForPermission(this._Reporting_Reporter_Certificates.ID)); }

                //if (this._Reporting_Reporter_XAPI.Checked)
                //{ permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.Reporting_Reporter_XAPI), this._GetSelectedScopeForPermission(this._Reporting_Reporter_XAPI.ID)); }

                // only save if ecommerce is set and verified
                if (this._EcommerceSettings.IsEcommerceSetAndVerified)
                {
                    if (this._Reporting_Reporter_Purchases.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.Reporting_Reporter_Purchases), this._GetSelectedScopeForPermission(this._Reporting_Reporter_Purchases.ID)); }
                }

                // only save if certifications is enabled
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                {
                    if (this._Reporting_Reporter_UserCertificationTranscripts.Checked)
                    { permissions.Rows.Add(Convert.ToInt32(AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts), this._GetSelectedScopeForPermission(this._Reporting_Reporter_UserCertificationTranscripts.ID)); }
                }

                // if the datatable has rows, save the permissions
                if (permissions.Rows.Count > 0)
                { this._RoleObject.SavePermissions(permissions); }

                // load the saved role object
                this._RoleObject = new Role(id);

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.RolePropertiesFeedbackContainer, _GlobalResources.RoleHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RolePropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RolePropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RolePropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RolePropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                if (!String.IsNullOrWhiteSpace(ex.Message))
                { this.DisplayFeedbackInSpecifiedContainer(this.RolePropertiesFeedbackContainer, ex.Message, true); }
                else
                { this.DisplayFeedbackInSpecifiedContainer(this.RolePropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true); }
            }
        }
        #endregion

        #region _GetSelectedScopeForPermission
        /// <summary>
        /// Gets the selected scope for a permission.
        /// </summary>
        /// <param name="idPrefix">id of the permission checkbox</param>
        /// <returns></returns>
        private string _GetSelectedScopeForPermission(string idPrefix)
        {
            string permissionScope = null;

            // get the scope items hidden field for the permission
            HiddenField selectedScopeItems = (HiddenField)FindControlRecursive(this.RolePropertiesTabPanelsContainer, idPrefix + "_ScopedItemsHiddenField");

            if (selectedScopeItems != null)
            {
                if (!String.IsNullOrWhiteSpace(selectedScopeItems.Value))
                { permissionScope = selectedScopeItems.Value; }
            }

            return permissionScope;
        }
        #endregion

        #region _RolePropertiesCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for Role Properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RolePropertiesCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        #endregion
    }
}
