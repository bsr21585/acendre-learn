﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
namespace Asentia.LMS.Pages.Administrator.Roles
{
    public class AutoJoinRules : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public UpdatePanel RuleSetRoleGridUpdatePanel;
        public Grid RuleSetRoleGrid;
        public Panel ActionsPanel;
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup GridConfirmAction = new ModalPopup();
        #endregion

        #region Private Properties
        private Role _RoleObject;
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
            { Response.Redirect("/"); }

            // get the role object
            this._GetRoleObject();

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Roles, "/administrator/roles"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Rulesets + ": " + this._RoleObject.Name));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, _GlobalResources.Rulesets + ": " + this._RoleObject.Name, ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION,
                                                                                                                                                      ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.RuleSetRoleGrid.BindData();
            }
        }

        #endregion

        #region _GetRoleObject
        /// <summary>
        /// Gets a role object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetRoleObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._RoleObject = new Role(id); }
                    else
                    { Response.Redirect("~/administrator/roles"); }
                }
                catch
                { Response.Redirect("~/administrator/roles"); }
            }
            else
            { Response.Redirect("~/administrator/roles"); }
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD RULESET
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddRuleSetLink",
                                                null,
                                                "ModifyRuleSet.aspx",
                                                null,
                                                _GlobalResources.NewRuleset,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.RuleSetRoleGrid.StoredProcedure = Library.RuleSet.GridProcedureForRoles;
            this.RuleSetRoleGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.RuleSetRoleGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.RuleSetRoleGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.RuleSetRoleGrid.AddFilter("@id", SqlDbType.Int, 4, this._RoleObject.Id);
            this.RuleSetRoleGrid.IdentifierField = "idRuleSet";
            this.RuleSetRoleGrid.DefaultSortColumn = "label";

            // data key names
            this.RuleSetRoleGrid.DataKeyNames = new string[] { "idRuleSet" };

            // columns
            GridColumn rulesetTitle = new GridColumn(_GlobalResources.Title, "label", "label");

            GridColumn modify = new GridColumn(_GlobalResources.Modify, "isModifyOn", true);
            modify.AddProperty(new GridColumnProperty("True", "<a href=\"ModifyRuleSet.aspx?idRole=" + this._RoleObject.Id + "&id=##idRuleSet##\">"
                                                                + "<img class=\"SmallIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.Modify + "\" />"
                                                                + "</a>"));
            modify.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.ModifyDisabled + "\" />"));

            // add columns to data grid
            this.RuleSetRoleGrid.AddColumn(rulesetTitle);
            this.RuleSetRoleGrid.AddColumn(modify);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedRuleset_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this.DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction.ID = "GridConfirmAction";

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedRuleset_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseRuleset_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.RuleSetRoleGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.RuleSetRoleGrid.Rows[i].FindControl(this.RuleSetRoleGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.RuleSet.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedback(_GlobalResources.TheSelectedRuleset_sHaveBeenDeletedSuccessfully, false);

                // rebind the grid
                this.RuleSetRoleGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this.RuleSetRoleGrid.BindData();
            }
        }
        #endregion

    }
}
