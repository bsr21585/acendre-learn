﻿function pageLoad() {
    $(".catalog").slideDown();
    $("[id*=CatalogTree] input[id^='Public_']").css("color", "gray");
    $("[id*=CatalogTree] input[id^='Public_']").css("background-color", "gray");


    $("input[id^='Private_chb']").change(function (elm) {
        var elm = this;
        if (elm.checked) {
            //Do stuff
            var id = elm.id;
            if (id.search("Private_chb_") >= 0) {

                var elmPar = $($($(elm).parent()).parent());

                while (elmPar != null) {

                    if ($(elmPar).hasClass('innerParent')) {
                        var parentId = elmPar.attr('id');
                        var elmm = $("div#" + parentId + " input[id^='Private_chb_" + parentId.replace('chbDiv_', '') + "']");
                        $(elmm).prop("checked", true);
                        elmPar = $(elmPar).parent();
                    }
                    else if ($(elmPar).hasClass('parentCatalog')) {
                        var parentId = elmPar.attr('id');
                        var elmm = $("div#" + parentId + " input[id^='Private_chb_" + parentId.replace('chbDiv_', '') + "']");
                        $(elmm).prop("checked", true);
                        elmPar = null;
                    }
                    else {
                        elmPar = null;
                    }
                }
            }

        }
        else {
            var id = elm.id;
            $("[id^=chbDiv_" + id.replace('Private_chb_', '') + "]").find("input[id^='Private_chb_']").prop("checked", false)
        }

    });

    $('input:checkbox[id=All_chb_0]').change(function (elm) {
        var elm = this;
        if (elm.checked) {
            //Do stuff
            $("input[id^='Private_chb']").attr('onclick', 'return false;');
            $("input[id^='Private_chb']").parent().css("color", "lightgray");
            $('input:checkbox[id^="Private_chb"]').each(function () {
                this.checked = elm.checked;
                $(this).attr("checked", elm.checked);
            });
            SaveAllChbListToHiddenFeild();
        }
        else {
            $("input[id^='Private_chb']").removeAttr('onclick');
            $("input[id^='Private_chb']").parent().css("color", "gray");
            $("#CheckBoxPrivateAccess").val("");
            $('input:checkbox[id^="Private_chb"]').each(function () {
                this.checked = elm.checked;
                $(this).attr("checked", elm.checked);
            });
        }
    });
}

function SaveAllChbListToHiddenFeild() {
    var accessible_Catalog_ids = "";
    var length = $("[id*=CatalogTree] input[id^='Private_']").length;

    for (var index = 0; index < length ; index++) {
        var id = $("[id*=CatalogTree] input[id^='Private_']")[index].id;
        accessible_Catalog_ids += id.replace("Private_chb_", "") + ",";
    }

    $("#CheckBoxPrivateAccess").val(accessible_Catalog_ids);
}

function SaveListToHiddenFeild() {
    var accessible_Catalog_ids = "";
    var length = $("[id*=CatalogTree] input[id^='Private_']").length;

    for (var index = 0; index < length ; index++) {
        var isChecked = $("[id*=CatalogTree] input[id^='Private_']")[index].checked;
        if (isChecked) {
            var id = $("[id*=CatalogTree] input[id^='Private_']")[index].id;
            accessible_Catalog_ids += id.replace("Private_chb_", "") + ",";
        }
    }

    $("#CheckBoxPrivateAccess").val(accessible_Catalog_ids);
}