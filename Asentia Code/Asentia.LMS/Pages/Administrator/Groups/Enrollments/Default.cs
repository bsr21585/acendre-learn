﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Groups.Enrollments
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel EnrollmentsPageContentWrapperContainer;
        public Panel GroupObjectMenuContainer;
        public Panel EnrollmentsPageWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel EnrollmentsGridUpdatePanel;
        public Grid EnrollmentsGrid;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private Group _GroupObject;

        private ModalPopup _TogglePrerequisiteLockConfirmAction;
        private HiddenField _HiddenTogglePrerequisiteLockButton;
        private HiddenField _TogglePrerequisiteEnrollmentId;

        private ModalPopup _GridConfirmAction;
        private LinkButton _DeleteButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Groups.Enrollments.Default.js");
        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get the group object
            this._GetGroupObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/enrollments/Default.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.EnrollmentsPageContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.EnrollmentsPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the group object menu
            if (this._GroupObject != null)
            {
                GroupObjectMenu groupObjectMenu = new GroupObjectMenu(this._GroupObject);
                groupObjectMenu.SelectedItem = GroupObjectMenu.MenuObjectItem.Enrollments;

                this.GroupObjectMenuContainer.Controls.Add(groupObjectMenu);
            }

            // build the grid, actions panel, and modals
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildGridActionsPanel();
            this._BuildGridActionsModal();
            this._BuildTogglePrerequisiteLockConfirmModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.EnrollmentsGrid.BindData();
            }
        }
        #endregion

        #region _GetGroupObject
        /// <summary>
        /// Gets a group object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetGroupObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("gid", 0);

            if (qsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                try
                {
                    if (id > 0)
                    { this._GroupObject = new Group(id); }
                }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get group name information
            string groupNameInInterfaceLanguage = this._GroupObject.Name;
            string groupImagePath;
            string groupImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                }
            }

            if (this._GroupObject.Avatar != null)
            {
                groupImagePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                groupImageCssClass = "AvatarImage";
            }
            else
            {
                groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            }
            
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups, "/administrator/groups"));
            breadCrumbLinks.Add(new BreadcrumbLink(groupNameInInterfaceLanguage, "/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Enrollments));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, groupNameInInterfaceLanguage, groupImagePath, _GlobalResources.Enrollments, ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG), groupImageCssClass);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD ENROLLMENT
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddEnrollmentLink",
                                                null,
                                                "Modify.aspx?gid=" + this._GroupObject.Id.ToString(),
                                                null,
                                                _GlobalResources.NewEnrollment,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.EnrollmentsGrid.StoredProcedure = Library.GroupEnrollment.GridProcedure;
            this.EnrollmentsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.EnrollmentsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.EnrollmentsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.EnrollmentsGrid.AddFilter("@idGroup", SqlDbType.Int, 4, this._GroupObject.Id);
            this.EnrollmentsGrid.IdentifierField = "idGroupEnrollment";
            this.EnrollmentsGrid.DefaultSortColumn = "title";

            // data key names
            this.EnrollmentsGrid.DataKeyNames = new string[] { "idGroupEnrollment" };

            // columns
            GridColumn courseStartDate = new GridColumn(_GlobalResources.Course + ", " + _GlobalResources.StartDate, null); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.EnrollmentsGrid.AddColumn(courseStartDate);
            this.EnrollmentsGrid.AddColumn(options);

            // add row data bound event
            this.EnrollmentsGrid.RowDataBound += new GridViewRowEventHandler(this._EnrollmentsGrid_RowDataBound);
        }
        #endregion

        #region _EnrollmentsGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the enrollments grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _EnrollmentsGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idGroupEnrollment = Convert.ToInt32(rowView["idGroupEnrollment"]);                

                // AVATAR, TITLE, CONTENT TYPE(S)

                string title = Server.HtmlEncode(rowView["title"].ToString());
                DateTime dtStart = Convert.ToDateTime(rowView["dtStart"]);
                bool isLockedByPrerequisites = Convert.ToBoolean(rowView["togglePrerequisites"]);                

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_SERIES, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = title;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // label
                Label labelLabelWrapper = new Label();
                labelLabelWrapper.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(labelLabelWrapper);

                HyperLink labelLink = new HyperLink();
                labelLink.NavigateUrl = "Modify.aspx?gid=" + this._GroupObject.Id.ToString() + "&id=" + idGroupEnrollment.ToString();
                labelLabelWrapper.Controls.Add(labelLink);                

                Literal labelLabel = new Literal();
                labelLabel.Text = title;
                labelLink.Controls.Add(labelLabel);

                // start date
                Label startDateLabel = new Label();
                startDateLabel.CssClass = "GridSecondaryLine";
                e.Row.Cells[1].Controls.Add(startDateLabel);

                Literal startDateText = new Literal();
                startDateText.Text = _GlobalResources.StartDate + ": " + TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)); ;
                startDateLabel.Controls.Add(startDateText);

                // OPTIONS COLUMN 

                // toggle prerequisite lock
                if (isLockedByPrerequisites)
                {
                    Label courseTogglePrerequisitesSpan = new Label();
                    courseTogglePrerequisitesSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(courseTogglePrerequisitesSpan);

                    LinkButton prerequisitesUnenforceLink = new LinkButton();
                    prerequisitesUnenforceLink.ID = "PrerequisitesUnenforceLink_" + idGroupEnrollment.ToString();
                    prerequisitesUnenforceLink.OnClientClick = "TogglePrerequisiteLockForGroupEnrollment(" + idGroupEnrollment + "); return false;";
                    prerequisitesUnenforceLink.ToolTip = _GlobalResources.UnlockGroupEnrollment;

                    Image prerequisitesUnenforceLinkImage = new Image();
                    prerequisitesUnenforceLinkImage.ID = "PrerequisitesUnenforceLinkImage_" + idGroupEnrollment.ToString();
                    prerequisitesUnenforceLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD, ImageFiles.EXT_PNG);
                    prerequisitesUnenforceLinkImage.AlternateText = _GlobalResources.PrerequisiteLock;

                    prerequisitesUnenforceLink.Controls.Add(prerequisitesUnenforceLinkImage);

                    courseTogglePrerequisitesSpan.Controls.Add(prerequisitesUnenforceLink);
                }
                else
                {
                    Label courseTogglePrerequisitesSpan = new Label();
                    courseTogglePrerequisitesSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(courseTogglePrerequisitesSpan);                    

                    Image prerequisitesCompletedImage = new Image();
                    prerequisitesCompletedImage.ID = "PrerequisitesCompletedImage_" + idGroupEnrollment.ToString();
                    prerequisitesCompletedImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                    prerequisitesCompletedImage.ToolTip = _GlobalResources.PrerequisitesCompleted;
                    prerequisitesCompletedImage.AlternateText = _GlobalResources.PrerequisiteLock;

                    courseTogglePrerequisitesSpan.Controls.Add(prerequisitesCompletedImage);
                }
            }
        }
        #endregion        

        #region _BuildGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedEnrollment_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmActionModal");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedEnrollment_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseGroupEnrollment_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            // add modal to actions panel
            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _BuildTogglePrerequisiteLockConfirmModal
        /// <summary>
        /// Builds the confirmation modal for the toggle prerequisite lock action performed on Grid data.
        /// </summary>
        private void _BuildTogglePrerequisiteLockConfirmModal()
        {
            this._HiddenTogglePrerequisiteLockButton = new HiddenField();
            this._HiddenTogglePrerequisiteLockButton.ID = "HiddenTogglePrerequisiteLockButton";
            this.ActionsPanel.Controls.Add(this._HiddenTogglePrerequisiteLockButton);

            this._TogglePrerequisiteEnrollmentId = new HiddenField();
            this._TogglePrerequisiteEnrollmentId.ID = "TogglePrerequisiteEnrollmentId";
            this._TogglePrerequisiteEnrollmentId.Value = "0";
            this.ActionsPanel.Controls.Add(this._TogglePrerequisiteEnrollmentId);

            this._TogglePrerequisiteLockConfirmAction = new ModalPopup("TogglePrerequisiteLockConfirmActionModal");

            // set modal properties
            this._TogglePrerequisiteLockConfirmAction.Type = ModalPopupType.Confirm;
            this._TogglePrerequisiteLockConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED, ImageFiles.EXT_PNG);
            this._TogglePrerequisiteLockConfirmAction.HeaderIconAlt = _GlobalResources.PrerequisiteLock;
            this._TogglePrerequisiteLockConfirmAction.HeaderText = _GlobalResources.UnlockGroupEnrollment;
            this._TogglePrerequisiteLockConfirmAction.TargetControlID = this._HiddenTogglePrerequisiteLockButton.ClientID;
            this._TogglePrerequisiteLockConfirmAction.SubmitButton.Command += new CommandEventHandler(this._TogglePrerequisiteLockButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmTogglePrerequisitesActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToUnlockThisGroupEnrollment;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._TogglePrerequisiteLockConfirmAction.AddControlToBody(body1Wrapper);

            // add modal to actions panel
            this.ActionsPanel.Controls.Add(this._TogglePrerequisiteLockConfirmAction);
        }
        #endregion

        #region _TogglePrerequisiteLockButton_Command
        /// <summary>
        /// Toggle prerequisite lock button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _TogglePrerequisiteLockButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                int idGroupEnrollment;

                if (int.TryParse(this._TogglePrerequisiteEnrollmentId.Value, out idGroupEnrollment))
                {
                    if (idGroupEnrollment > 0)
                    {
                        // reset the enrollment
                        Library.GroupEnrollment.TogglePrerequisiteLock(idGroupEnrollment);

                        // display the success message
                        this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.TheSelectedGroupEnrollmentHasBeenUnlockedSuccessfully, false);

                    }
                    else
                    { throw new AsentiaException(_GlobalResources.InvalidGroupEnrollmentID); }
                }
                else
                { throw new AsentiaException(_GlobalResources.InvalidGroupEnrollmentID); }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.EnrollmentsGrid.BindData();
            }
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.EnrollmentsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.EnrollmentsGrid.Rows[i].FindControl(this.EnrollmentsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.GroupEnrollment.Delete(recordsToDelete, true);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedGroupEnrollment_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message                    
                    this.DisplayFeedback(_GlobalResources.NoGroupEnrollment_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.EnrollmentsGrid.BindData();
            }
        }
        #endregion
    }
}
