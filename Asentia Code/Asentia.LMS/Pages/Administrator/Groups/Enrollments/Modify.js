﻿function AddCoursesToEnrollment() {
    var coursesListContainer = $("#EnrollmentCoursesList_Container");
    var selectedCourses = $('select#SelectEligibleCoursesListBox').val();

    if (selectedCourses != null) {
        for (var i = 0; i < selectedCourses.length; i++) {
            if (!$("#Course_" + selectedCourses[i]).length) {
                // add the selected course to the course list container
                var itemContainerDiv = $("<div id=\"Course_" + selectedCourses[i] + "\">" + "<img onclick=\"javascript:RemoveCourseFromEnrollment('" + selectedCourses[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" class=\"SmallIcon\" />" + $("#SelectEligibleCoursesListBox option[value='" + selectedCourses[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(coursesListContainer);
            }

            // remove the course from the select list
            $("#SelectEligibleCoursesListBox option[value='" + selectedCourses[i] + "']").remove();
        }
    }
}

function RemoveCourseFromEnrollment(courseId) {
    $("#Course_" + courseId).remove();
}

function GetSelectedCoursesForHiddenField() {
    var coursesListContainer = $("#EnrollmentCoursesList_Container");
    var selectedCoursesField = $("#SelectedCourses_Field");
    var selectedCourses = "";

    coursesListContainer.children().each(function () {
        selectedCourses = selectedCourses + $(this).prop("id").replace("Course_", "") + ",";
    });

    if (selectedCourses.length > 0)
    { selectedCourses = selectedCourses.substring(0, selectedCourses.length - 1); }

    selectedCoursesField.val(selectedCourses);
}

function PopulateHiddenFieldsForDynamicElements() {
    GetSelectedCoursesForHiddenField();
}