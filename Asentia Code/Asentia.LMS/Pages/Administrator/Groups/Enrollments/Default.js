﻿function TogglePrerequisiteLockForGroupEnrollment(enrollmentId) {
    var togglePrerequisiteEnrollmentIdHiddenField = $("#TogglePrerequisiteEnrollmentId");
    togglePrerequisiteEnrollmentIdHiddenField.val(enrollmentId);

    $find('TogglePrerequisiteLockConfirmActionModalModalPopupExtender').show();
}