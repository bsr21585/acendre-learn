﻿/* DOCUMENT FOLDERS LISTING and MODIFY METHODS */
/* Add javascript methods for Document folders listing/add new/ modify below. */

/*
METHOD: OnModifyDocumentFolderClick
Method that is used to open Modal popup for Modify document repository folder click.
*/
function OnModifyDocumentFolder(idDocumnetFolder) {

    //set idDocumnetFolder into hidden field
    $("#" + IdDocumentFolderHidden).val(idDocumnetFolder);

    //click the update hidden button to show  modal popup
    document.getElementById(DocumentFolderHiddenLink).click();

    if ($('#' + IdDocumentFolderHidden).val() == "" || $('#' + IdDocumentFolderHidden).val() == "0") {
        $("#" + DocumentFolderModal + "ModalPopupHeaderText").text(AddNewHeaderText);
        $('#DocumentFolderModalModalPopupFeedbackContainer').hide();
        //set moadal submit button text
        $("#" + DocumentFolderModal + "ModalPopupSubmitButton").val(AddModalSubmitButtonText)
    }

    //clear the name field
    $("input[id^='Name_Field']").val("");

    //click the hidden button to fill the controls of  modal popup
    document.getElementById(SaveDocumentFolderModalButton).click();

    return false;
}

function pageLoad() {
    var idDocumentFolder = $('#' + IdDocumentFolderHidden).val();

    if (idDocumentFolder == "" || idDocumentFolder == "0") {
        $("#" + DocumentFolderModal + "ModalPopupHeaderText").text(AddNewHeaderText);
        $("#" + DocumentFolderModal + "ModalPopupSubmitButton").val(AddModalSubmitButtonText)
    }
}