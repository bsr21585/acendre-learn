﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Groups.DocumentRepository
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel GroupDocumentRepositoryFoldersContentWrapperContainer;
        public Panel GroupObjectMenuContainer;
        public Panel GroupDocumentRepositoryFoldersWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel GroupDocumentRepositoryFolderGridUpdatePanel;
        public Grid GroupDocumentRepositoryFolderGrid;
        public Panel ActionsPanel;
        public Panel ModalPropertiesContainer;
        #endregion

        #region Private Properties
        private Group _GroupObject;
        private DocumentRepositoryFolder _DocumentFolderObject;

        private LinkButton _DeleteButton;
        private LinkButton _DocumentFolderHiddenLink = new LinkButton();

        private ModalPopup _GridConfirmAction;
        private ModalPopup _AddDocumentFolderModal;

        private TextBox _FolderName;
        private Button _SaveDocumentFolderModalButton;

        private HiddenField _IdDocumentFolderHidden = new HiddenField();
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Groups.DocumentRepository.Default.js");

            StringBuilder globalJS = new StringBuilder();
            globalJS.AppendLine("ModiFyHeaderText = \"" + _GlobalResources.ModifyDocumentFolder + "\";");
            globalJS.AppendLine("ModiFyModalSubmitButtonText = \"" + _GlobalResources.SaveChanges + "\";");
            globalJS.AppendLine("AddNewHeaderText = \"" + _GlobalResources.NewDocumentFolder + "\";");
            globalJS.AppendLine("AddModalSubmitButtonText = \"" + _GlobalResources.CreateFolder + "\";");
            globalJS.AppendLine("SaveDocumentFolderModalButton = \"" + this._SaveDocumentFolderModalButton.ID + "\";");
            globalJS.AppendLine("DocumentFolderHiddenLink = \"" + this._DocumentFolderHiddenLink.ID + "\";");
            globalJS.AppendLine("IdDocumentFolderHidden = \"" + this._IdDocumentFolderHidden.ID + "\";");
            globalJS.AppendLine("DocumentFolderModal = \"" + this._AddDocumentFolderModal.ID + "\";");

            csm.RegisterClientScriptBlock(typeof(Default), "GlobalJS", globalJS.ToString(), true);
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the group object
            this._GetGroupObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/documentrepository/Default.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.GroupDocumentRepositoryFoldersContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.GroupDocumentRepositoryFoldersWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the group object menu
            if (this._GroupObject != null)
            {
                GroupObjectMenu groupObjectMenu = new GroupObjectMenu(this._GroupObject);
                groupObjectMenu.SelectedItem = GroupObjectMenu.MenuObjectItem.Documents;

                this.GroupObjectMenuContainer.Controls.Add(groupObjectMenu);
            }

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();
            this._DocumentFolderActionModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.GroupDocumentRepositoryFolderGrid.BindData();
            }
        }
        #endregion

        #region _GetGroupObject
        /// <summary>
        /// Gets a Group object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetGroupObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("gid", 0);

            if (qsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                try
                {
                    if (id > 0)
                    { this._GroupObject = new Group(id); }
                }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get group name information
            string groupNameInInterfaceLanguage = this._GroupObject.Name;
            string groupImagePath;
            string groupImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (UMS.Library.Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                }
            }

            if (this._GroupObject.Avatar != null)
            {
                groupImagePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                groupImageCssClass = "AvatarImage";
            }
            else
            {
                groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            }   
            
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups, "/administrator/groups"));
            breadCrumbLinks.Add(new BreadcrumbLink(groupNameInInterfaceLanguage, "/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Documents, "/administrator/groups/documents/Default.aspx?gid=" + this._GroupObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.DocumentFolders));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, groupNameInInterfaceLanguage, groupImagePath, _GlobalResources.DocumentFolders, ImageFiles.GetIconPath(ImageFiles.ICON_FOLDER, ImageFiles.EXT_PNG), groupImageCssClass);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {            
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD DOCUMENT FOLDER
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddDocumentFolderLink",
                                                null,
                                                "javascript: void(0);",
                                                "OnModifyDocumentFolder(0); return false;",
                                                _GlobalResources.NewDocumentFolder,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_FOLDER, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.GroupDocumentRepositoryFolderGrid.StoredProcedure = DocumentRepositoryFolder.GridProcedure;
            this.GroupDocumentRepositoryFolderGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.GroupDocumentRepositoryFolderGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.GroupDocumentRepositoryFolderGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.GroupDocumentRepositoryFolderGrid.AddFilter("@idDocumentRepositoryObjectType", SqlDbType.Int, 4, DocumentRepositoryObjectType.Group);
            this.GroupDocumentRepositoryFolderGrid.AddFilter("@idObject", SqlDbType.Int, 4, this._GroupObject.Id);
            this.GroupDocumentRepositoryFolderGrid.IdentifierField = "idDocumentRepositoryFolder";
            this.GroupDocumentRepositoryFolderGrid.DefaultSortColumn = "name";

            // data key names
            this.GroupDocumentRepositoryFolderGrid.DataKeyNames = new string[] { "idDocumentRepositoryFolder" };

            // columns
            GridColumn folder = new GridColumn(_GlobalResources.Folder, null, "name");

            // add columns to data grid
            this.GroupDocumentRepositoryFolderGrid.AddColumn(folder);

            // add row data bound event
            this.GroupDocumentRepositoryFolderGrid.RowDataBound += new GridViewRowEventHandler(this._GroupDocumentRepositoryFolderGrid_RowDataBound);
        }
        #endregion

        #region _GroupDocumentRepositoryFolderGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the group document repository folder grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _GroupDocumentRepositoryFolderGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idDocumentRepositoryFolder = Convert.ToInt32(rowView["idDocumentRepositoryFolder"]);

                // AVATAR, NAME

                string name = Server.HtmlEncode(rowView["name"].ToString());

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_FOLDER, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = name;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink nameLink = new HyperLink();
                nameLink.NavigateUrl = "javascript: void(0);";
                nameLink.Attributes.Add("onclick", "OnModifyDocumentFolder(" + idDocumentRepositoryFolder.ToString() + ");return false;");
                nameLink.Text = name;
                nameLabel.Controls.Add(nameLink);
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedDocumentFolder_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmActionModal");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedDocumentFolder_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseDocumentFolder_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.GroupDocumentRepositoryFolderGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.GroupDocumentRepositoryFolderGrid.Rows[i].FindControl(this.GroupDocumentRepositoryFolderGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    DocumentRepositoryFolder.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedDocumentFolder_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message                    
                    this.DisplayFeedback(_GlobalResources.NoDocumentFolder_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException aEx)
            {
                // display the failure message
                this.DisplayFeedback(aEx.Message, true);
            }
            finally
            {
                // rebind the grid
                this.GroupDocumentRepositoryFolderGrid.BindData();
            }
        }
        #endregion

        #region _DocumentFolderActionModal
        /// <summary>
        /// Builds document folder action modal pop up
        /// </summary>
        private void _DocumentFolderActionModal()
        {
            // hidden button to simulate the action on click of update 
            this._DocumentFolderHiddenLink.ClientIDMode = ClientIDMode.Static;
            this._DocumentFolderHiddenLink.ID = "DocumentFolderHiddenLink";
            this._DocumentFolderHiddenLink.Text = _GlobalResources.CreateModifyResourceType;
            this._DocumentFolderHiddenLink.Style.Add("display", "none");
            this.ObjectOptionsPanel.Controls.Add(this._DocumentFolderHiddenLink);

            this._BuildAddDocumentFolderModal(this._DocumentFolderHiddenLink.ID);
        }
        #endregion

        #region _BuildAddDocumentFolderModal
        /// <summary>
        /// Builds the modal for creating/modifying a documnet folder type.
        /// </summary>
        private void _BuildAddDocumentFolderModal(string targetControlId)
        {
            // set modal properties
            this._AddDocumentFolderModal = new ModalPopup("DocumentFolderModal");
            this._AddDocumentFolderModal.Type = ModalPopupType.Form;
            this._AddDocumentFolderModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_FOLDER, ImageFiles.EXT_PNG);
            this._AddDocumentFolderModal.HeaderIconAlt = _GlobalResources.ModifyDocumentFolder;
            this._AddDocumentFolderModal.HeaderText = _GlobalResources.ModifyDocumentFolder;

            this._AddDocumentFolderModal.TargetControlID = targetControlId;
            this._AddDocumentFolderModal.ReloadPageOnClose = false;

            this._AddDocumentFolderModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._AddDocumentFolderModal.SubmitButtonCustomText = _GlobalResources.SaveChanges;

            // adding button for saving  document folder      
            this._SaveDocumentFolderModalButton = new Button();
            this._SaveDocumentFolderModalButton.ID = "SaveDocumentFolderModalButton";
            this._SaveDocumentFolderModalButton.Text = _GlobalResources.AddUpdate;

            this._SaveDocumentFolderModalButton.Command += new CommandEventHandler(this._DocumentFolderFetchingModalButton_Command);
            this._SaveDocumentFolderModalButton.Style.Add("display", "none");

            this._AddDocumentFolderModal.SubmitButton.Command += new CommandEventHandler(this._AddandModifyDocumentFolderModalSubmit_Command);
            this._AddDocumentFolderModal.CloseButtonTextType = ModalPopupButtonText.Close;

            // build the modal body
            // course title field
            this.ModalPropertiesContainer = new Panel();
            this.ModalPropertiesContainer.ID = "ModalProperties_Container";

            this._IdDocumentFolderHidden = new HiddenField();
            this._IdDocumentFolderHidden.ClientIDMode = ClientIDMode.Static;
            this._IdDocumentFolderHidden.ID = "IdDocumentFolderHidden";
            this.ModalPropertiesContainer.Controls.Add(this._IdDocumentFolderHidden);

            this._FolderName = new TextBox();
            this._FolderName.ID = "Name_Field";
            this._FolderName.CssClass = "InputLong";

            // add control to panel
            this.ModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Name",
                                                                           _GlobalResources.FolderName,
                                                                           this._FolderName.ID,
                                                                           this._FolderName,
                                                                           true,
                                                                           true,
                                                                           false));

            // add controls to modal
            this.ModalPropertiesContainer.Controls.Add(this._SaveDocumentFolderModalButton);
            this._AddDocumentFolderModal.AddControlToBody(this.ModalPropertiesContainer);

            // add modal to container
            this.ActionsPanel.Controls.Add(this._AddDocumentFolderModal);

            EnsureChildControls();
        }
        #endregion

        #region _DocumentFolderFetchingModalButton_Command
        /// <summary>
        /// Handles the event initiated by the hidden Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _DocumentFolderFetchingModalButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                this._AddDocumentFolderModal.ClearFeedback();

                //fills the modal pop up controls on update of any document folder
                //get document folder object with id
                if (!String.IsNullOrWhiteSpace(this._IdDocumentFolderHidden.Value) && Convert.ToInt32(this._IdDocumentFolderHidden.Value) > 0)
                {
                    this._DocumentFolderObject = new DocumentRepositoryFolder(Convert.ToInt32(this._IdDocumentFolderHidden.Value));

                    //bind controls
                    this._FolderName.Text = this._DocumentFolderObject.FolderNmae;
                }
                else
                {
                    this._FolderName.Text = string.Empty;
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddDocumentFolderModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dnfEx)
            {
                // display the failure message
                this._AddDocumentFolderModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddDocumentFolderModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddDocumentFolderModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException aEx)
            {
                // display the failure message
                this._AddDocumentFolderModal.DisplayFeedback(aEx.Message, true);
            }
        }
        #endregion

        #region _AddandModifyDocumentFolderModalSubmit_Command
        /// <summary>
        /// Handles add and modify documnet folder modal pop up submit button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AddandModifyDocumentFolderModalSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                #region Validation
                // validate the document folder modal fields
                if (!this._ValidateDocumentFolderModalProperties())
                { throw new AsentiaException(); }
                #endregion

                if (!String.IsNullOrWhiteSpace(this._IdDocumentFolderHidden.Value) && Convert.ToInt32(this._IdDocumentFolderHidden.Value) > 0)
                { this._DocumentFolderObject = new DocumentRepositoryFolder(Convert.ToInt32(this._IdDocumentFolderHidden.Value)); }
                else
                {
                    this._DocumentFolderObject = new DocumentRepositoryFolder();
                }

                this._DocumentFolderObject.FolderNmae = this._FolderName.Text;
                this._DocumentFolderObject.IdobjectType = DocumentRepositoryObjectType.Group;
                this._DocumentFolderObject.IdObject = this._GroupObject.Id;
                
                this._DocumentFolderObject.Id = this._DocumentFolderObject.Save();
                this._DocumentFolderObject = new DocumentRepositoryFolder(this._DocumentFolderObject.Id);

                this._IdDocumentFolderHidden.Value = Convert.ToString(this._DocumentFolderObject.Id);

                // display success message
                this._AddDocumentFolderModal.DisplayFeedback(_GlobalResources.GroupDocumentFolderHasBeenSavedSuccessfully, false);

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddDocumentFolderModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dnfEx)
            {
                // display the failure message
                this._AddDocumentFolderModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddDocumentFolderModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddDocumentFolderModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AddDocumentFolderModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {
                this.GroupDocumentRepositoryFolderGrid.BindData();
                this.GroupDocumentRepositoryFolderGridUpdatePanel.Update();
            }
        }
        #endregion

        #region _ValidateDocumentFolderModalProperties
        /// <summary>
        /// Validates form fileds values
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateDocumentFolderModalProperties()
        {
            bool isValid = true;

            // name field
            if (String.IsNullOrWhiteSpace(this._FolderName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.ModalPropertiesContainer, "Name", _GlobalResources.FolderName + " " + _GlobalResources.IsRequired);
            }

            return isValid;
        }
        #endregion
    }
}
