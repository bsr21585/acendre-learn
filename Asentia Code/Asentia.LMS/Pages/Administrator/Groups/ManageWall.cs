﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.Groups
{
    public class ManageWall : AsentiaAuthenticatedPage
    {
        #region Public Properties
        public Panel ManageWallFormContentWrapperContainer;
        public Panel GroupObjectMenuContainer;
        public Panel ManageWallWrapperContainer;
        public Panel WallPropertiesInstructionsPanel;
        public Panel WallPropertiesFeedbackContainer;
        public Panel WallPropertiesContainer;
        public Panel WallPropertiesTabPanelsContainer;
        #endregion

        #region Private Properties
        private Group _GroupObject;

        // wall controls
        private const int _WallMessagesPageSize = 10;

        private Panel _WallMessagesTabPanel;
        private Panel _WallMessagesContainer;
        private Button _HiddenButtonForPostedForModerationModal;
        private ModalPopup _MessagePostedForModerationModal;
        private Button _HiddenButtonForMessageApprovedModal;
        private ModalPopup _MessageApprovedModal;
        private Button _HiddenButtonForMessageDeletedModal;
        private ModalPopup _MessageDeletedModal;

        // moderators controls
        private ModalPopup _SelectModerators;
        private HiddenField _SelectedWallModerators;
        private Panel _WallModeratorsListContainer;
        private DynamicListBox _SelectEligibleUsers;
        private DataTable _WallModerators;
        private DataTable _EligibleModeratorsForSelectList;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(ManageWall), "Asentia.LMS.Pages.Administrator.Groups.ManageWall.js");

            // build start up and global js           
            StringBuilder globalJs = new StringBuilder();

            globalJs.AppendLine("var LastRecord = \"" + AsentiaSessionState.UtcNow + "\";");
            globalJs.AppendLine("var GroupId = " + this._GroupObject.Id + ";");
            globalJs.AppendLine("var IsLoading = false;");
            globalJs.AppendLine("var DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            globalJs.AppendLine("GetRecords();");
            globalJs.AppendLine("$(window).scroll(function () {");
            globalJs.AppendLine("    if ($(window).scrollTop() == $(document).height() - $(window).height()) {");
            globalJs.AppendLine("       if (!IsLoading) {");
            globalJs.AppendLine("           GetRecords();");
            globalJs.AppendLine("       }");
            globalJs.AppendLine("    }");
            globalJs.AppendLine("});");

            csm.RegisterStartupScript(typeof(ManageWall), "GlobalJs", globalJs.ToString(), true);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the group object
            this._GetGroupObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("Wall.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/ManageWall.css");

            // if this group does not have a wall or group discussion is not enabled, bounce back to the group dashboard
            if (!(bool)this._GroupObject.IsFeedActive || !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
            { Response.Redirect("~/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id.ToString()); }

            // initialize the administrator menu
            this.InitializeAdminMenu();            

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetGroupObject
        /// <summary>
        /// Gets a group object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetGroupObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._GroupObject = new Group(id); }
                }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
            else { Response.Redirect("~/administrator/groups"); }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {            
            this.ManageWallFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ManageWallWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the group object menu
            if (this._GroupObject != null)
            {
                GroupObjectMenu groupObjectMenu = new GroupObjectMenu(this._GroupObject);
                groupObjectMenu.SelectedItem = GroupObjectMenu.MenuObjectItem.Wall;

                this.GroupObjectMenuContainer.Controls.Add(groupObjectMenu);
            }

            // clear controls from wrapper container
            this.WallPropertiesContainer.Controls.Clear();

            // build the group properties form tabs
            this._BuildWallPropertiesFormTabs();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.WallPropertiesInstructionsPanel, _GlobalResources.ManageTheGroupsDiscussionUsingTheFormBelow, true);

            this.WallPropertiesTabPanelsContainer = new Panel();
            this.WallPropertiesTabPanelsContainer.ID = "WallProperties_TabPanelsContainer";
            this.WallPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.WallPropertiesContainer.Controls.Add(this.WallPropertiesTabPanelsContainer);

            // build the wall messages panel
            this._BuildMessageFeedPanel();

            // only build moderators panel if group wall is moderated
            if (this._GroupObject.IsFeedModerated == true)
            { this._BuildModeratorsPanel(); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get group name information
            string groupNameInInterfaceLanguage = this._GroupObject.Name;
            string groupImagePath;
            string groupImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                }
            }

            if (this._GroupObject.Avatar != null)
            {
                groupImagePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                groupImageCssClass = "AvatarImage";
            }
            else
            {
                groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            }            

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups, "/administrator/groups"));
            breadCrumbLinks.Add(new BreadcrumbLink(groupNameInInterfaceLanguage, "/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ManageDiscussion));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, groupNameInInterfaceLanguage, groupImagePath, _GlobalResources.ManageDiscussion, ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG), groupImageCssClass);
        }
        #endregion

        #region _BuildWallPropertiesFormTabs
        /// <summary>
        /// Method to build the wall properties form tabs
        /// </summary>
        private void _BuildWallPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("MessageFeed", _GlobalResources.MessageFeed));

            if (this._GroupObject != null && this._GroupObject.IsFeedModerated == true)
            { tabs.Enqueue(new KeyValuePair<string, string>("Moderators", _GlobalResources.Moderators)); }

            // build and attach the tabs
            this.WallPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("WallProperties", tabs, null, this.Page));
        }
        #endregion

        #region _BuildMessageFeedPanel
        /// <summary>
        /// Builds the message feed panel.
        /// </summary>
        private void _BuildMessageFeedPanel()
        {
            // "Message Feed" is the default tab, so this is visible on page load.
            this._WallMessagesTabPanel = new Panel();
            this._WallMessagesTabPanel.ID = "WallProperties_" + "MessageFeed" + "_TabPanel";
            this._WallMessagesTabPanel.Attributes.Add("style", "display: block;");

            // make an unclickable button that will be the default button for the wall messages panel
            // so that we can control postbacks when enter is pressed in the comment fields
            Button wallMessagesContainerDefaultButton = new Button();
            wallMessagesContainerDefaultButton.ID = "WallMessagesContainerDefaultButton";
            wallMessagesContainerDefaultButton.OnClientClick = "return false;";
            wallMessagesContainerDefaultButton.Style.Add("display", "none");
            this._WallMessagesTabPanel.Controls.Add(wallMessagesContainerDefaultButton);

            // NEW MESSAGE

            Panel newMessageContainer = new Panel();
            newMessageContainer.ID = "NewMessageContainer";
            newMessageContainer.CssClass = "NewMessageContainer";
            newMessageContainer.DefaultButton = wallMessagesContainerDefaultButton.ID;

            // avatar
            Panel newMessageSenderAvatarContainer = new Panel();
            newMessageSenderAvatarContainer.ID = "NewMessageSenderAvatarContainer";
            newMessageSenderAvatarContainer.CssClass = "NewMessageAvatar";

            Image newMessageSenderAvatar = new Image();
            newMessageSenderAvatar.ID = "NewMessageSenderAvatar";
            newMessageSenderAvatar.CssClass = "MediumIcon";
            newMessageSenderAvatar.AlternateText = String.Empty;
            newMessageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);

            newMessageSenderAvatarContainer.Controls.Add(newMessageSenderAvatar);

            // attach avatar to new message container
            newMessageContainer.Controls.Add(newMessageSenderAvatarContainer);

            // message field
            TextBox newMessageField = new TextBox();
            newMessageField.ID = "NewMessageField";
            newMessageField.CssClass = "NewMessageField";
            newMessageField.Attributes.Add("placeholder", _GlobalResources.PostMessage);
            newMessageField.Attributes.Add("onkeyup", "PostNewMessage(event);");

            // attach field to reply comment container
            newMessageContainer.Controls.Add(newMessageField);

            // post message button
            Panel postMessageButtonContainer = new Panel();
            postMessageButtonContainer.ID = "PostMessageButtonContainer";
            postMessageButtonContainer.CssClass = "PostMessageButtonContainer";

            Button postMessageButton = new Button();
            postMessageButton.ID = "PostMessageButton";
            postMessageButton.CssClass = "Button ActionButton";
            postMessageButton.Text = _GlobalResources.PostMessage;
            postMessageButton.OnClientClick = "PostNewMessage(event); return false;";

            postMessageButtonContainer.Controls.Add(postMessageButton);

            // attach post message button to new messgae container
            newMessageContainer.Controls.Add(postMessageButtonContainer);

            // attach new message container to panel
            this._WallMessagesTabPanel.Controls.Add(newMessageContainer);

            // MESSAGES

            this._WallMessagesContainer = new Panel();
            this._WallMessagesContainer.ID = "WallMessagesContainer";
            this._WallMessagesContainer.DefaultButton = wallMessagesContainerDefaultButton.ID;
            this._WallMessagesTabPanel.Controls.Add(this._WallMessagesContainer);

            // LOADING PANEL - OLDER MESSAGES

            Panel olderMessagesLoadingPanel = new Panel();
            olderMessagesLoadingPanel.ID = "OlderMessagesLoadingPanel";
            olderMessagesLoadingPanel.CssClass = "WallLoadingPanel";
            olderMessagesLoadingPanel.Style.Add("display", "none");

            Image olderMessagesLoadingImage = new Image();
            olderMessagesLoadingImage.ID = "OlderMessagesLoadingImage";
            olderMessagesLoadingImage.CssClass = "MediumIcon";
            olderMessagesLoadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING, ImageFiles.EXT_GIF);
            olderMessagesLoadingImage.AlternateText = _GlobalResources.Loading;

            Label olderMessagesLoadingText = new Label();
            olderMessagesLoadingText.Text = _GlobalResources.LoadingMessages;

            olderMessagesLoadingPanel.Controls.Add(olderMessagesLoadingImage);
            olderMessagesLoadingPanel.Controls.Add(olderMessagesLoadingText);

            this._WallMessagesTabPanel.Controls.Add(olderMessagesLoadingPanel);

            // MODALS - FOR CONFIRMATION OF MESSAGE POSTING, APPROVAL, AND DELETION

            // message posted for moderation
            this._HiddenButtonForPostedForModerationModal = new Button();
            this._HiddenButtonForPostedForModerationModal.ID = "HiddenButtonForPostedForModerationModal";
            this._HiddenButtonForPostedForModerationModal.Style.Add("display", "none");
            this._WallMessagesTabPanel.Controls.Add(this._HiddenButtonForPostedForModerationModal);

            this._BuildMessagePostedForModerationModal();

            // message approved
            this._HiddenButtonForMessageApprovedModal = new Button();
            this._HiddenButtonForMessageApprovedModal.ID = "HiddenButtonForMessageApprovedModal";
            this._HiddenButtonForMessageApprovedModal.Style.Add("display", "none");
            this._WallMessagesTabPanel.Controls.Add(this._HiddenButtonForMessageApprovedModal);

            this._BuildMessageApprovedModal();

            // message deleted
            this._HiddenButtonForMessageDeletedModal = new Button();
            this._HiddenButtonForMessageDeletedModal.ID = "HiddenButtonForMessageDeletedModal";
            this._HiddenButtonForMessageDeletedModal.Style.Add("display", "none");
            this._WallMessagesTabPanel.Controls.Add(this._HiddenButtonForMessageDeletedModal);

            this._BuildMessageDeletedModal();

            // attach update panel to container
            this.WallPropertiesTabPanelsContainer.Controls.Add(this._WallMessagesTabPanel);
        }
        #endregion

        #region _BuildMessagePostedForModerationModal
        private void _BuildMessagePostedForModerationModal()
        {
            // set modal properties
            this._MessagePostedForModerationModal = new ModalPopup("MessagePostedForModerationModal");
            this._MessagePostedForModerationModal.Type = ModalPopupType.Information;
            this._MessagePostedForModerationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG);
            this._MessagePostedForModerationModal.HeaderIconAlt = _GlobalResources.Discussion;
            this._MessagePostedForModerationModal.HeaderText = _GlobalResources.MessagePosted;
            this._MessagePostedForModerationModal.TargetControlID = this._HiddenButtonForPostedForModerationModal.ID;

            // build the modal body
            this._MessagePostedForModerationModal.DisplayFeedback(_GlobalResources.YourMessageHasBeenPostedForModerationOnceApprovedByAModeratorYourMessageWillBeDisplayedInTheDiscussion, false);

            // add modal to container
            this._WallMessagesTabPanel.Controls.Add(this._MessagePostedForModerationModal);
        }
        #endregion

        #region _BuildMessageApprovedModal
        private void _BuildMessageApprovedModal()
        {
            // set modal properties
            this._MessageApprovedModal = new ModalPopup("MessageApprovedModal");
            this._MessageApprovedModal.Type = ModalPopupType.Information;
            this._MessageApprovedModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG);
            this._MessageApprovedModal.HeaderIconAlt = _GlobalResources.Discussion;
            this._MessageApprovedModal.HeaderText = _GlobalResources.MessageApproved;
            this._MessageApprovedModal.TargetControlID = this._HiddenButtonForMessageApprovedModal.ID;

            // build the modal body
            this._MessageApprovedModal.DisplayFeedback(_GlobalResources.TheMessageHasBeenApprovedItWillNowBeDisplayedInTheDiscussion, false);

            // add modal to container
            this._WallMessagesTabPanel.Controls.Add(this._MessageApprovedModal);
        }
        #endregion

        #region _BuildMessageDeletedModal
        private void _BuildMessageDeletedModal()
        {
            // set modal properties
            this._MessageDeletedModal = new ModalPopup("MessageDeletedModal");
            this._MessageDeletedModal.Type = ModalPopupType.Information;
            this._MessageDeletedModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG);
            this._MessageDeletedModal.HeaderIconAlt = _GlobalResources.Discussion;
            this._MessageDeletedModal.HeaderText = _GlobalResources.MessageDeleted;
            this._MessageDeletedModal.TargetControlID = this._HiddenButtonForMessageDeletedModal.ID;

            // build the modal body
            this._MessageDeletedModal.DisplayFeedback(_GlobalResources.TheMessageHasBeenDeletedSuccessfully, false);

            // add modal to container
            this._WallMessagesTabPanel.Controls.Add(this._MessageDeletedModal);
        }
        #endregion

        #region _WallMessagesJsonDataStruct
        public struct _WallMessagesJsonData
        {
            public bool actionSuccessful;
            public string html;
            public string exception;
            public string lastRecord;
            public int? idMessage;
            public int? idParentMessage;
        }
        #endregion

        #region BuildFeedMessages
        [WebMethod(EnableSession = true)]
        public static _WallMessagesJsonData BuildFeedMessages(int idGroup, DateTime dtQuery, bool getMessagesNewerThanDtQuery)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            try
            {
                DateTime lastRecord;
                DataTable messages = UMS.Library.Group.GetFeedMessages(idGroup, _WallMessagesPageSize, dtQuery, getMessagesNewerThanDtQuery, false, out lastRecord);

                foreach (DataRow row in messages.Rows)
                {
                    int idGroupFeedMessage = Convert.ToInt32(row["idGroupFeedMessage"]);
                    bool isMessageApproved = Convert.ToBoolean(row["isApproved"]);

                    Panel messageContainer = new Panel();
                    messageContainer.ID = "MessageContainer_" + idGroupFeedMessage.ToString();
                    messageContainer.CssClass = "MessageContainer";

                    // MESSAGE HEADER

                    Panel messageHeaderContainer = new Panel();
                    messageHeaderContainer.ID = "MessageHeaderContainer_" + idGroupFeedMessage.ToString();
                    messageHeaderContainer.CssClass = "MessageHeaderContainer";

                    // avatar
                    Panel messageSenderAvatarContainer = new Panel();
                    messageSenderAvatarContainer.ID = "MessageSenderAvatarContainer_" + idGroupFeedMessage.ToString();
                    messageSenderAvatarContainer.CssClass = "MessageAvatar";

                    Image messageSenderAvatar = new Image();
                    messageSenderAvatar.ID = "MessageSenderAvatar_" + idGroupFeedMessage.ToString();
                    messageSenderAvatar.CssClass = "MediumIcon";
                    messageSenderAvatar.AlternateText = row["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                    if (!String.IsNullOrWhiteSpace(row["avatar"].ToString()))
                    { messageSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + Convert.ToInt32(row["idAuthor"]) + "/" + row["avatar"].ToString(); }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(row["gender"].ToString()) && row["gender"].ToString() == "f")
                        { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                        else
                        { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                    }

                    messageSenderAvatarContainer.Controls.Add(messageSenderAvatar);

                    // attach avatar to header container
                    messageHeaderContainer.Controls.Add(messageSenderAvatarContainer);

                    // sender information
                    Panel messageSenderInformationContainer = new Panel();
                    messageSenderInformationContainer.ID = "MessageSenderInformationContainer_" + idGroupFeedMessage.ToString();
                    messageSenderInformationContainer.CssClass = "MessageSenderInformationContainer";

                    Panel messageSenderNameContainer = new Panel();
                    messageSenderNameContainer.ID = "MessageSenderNameContainer_" + idGroupFeedMessage.ToString();
                    messageSenderNameContainer.CssClass = "MessageSenderNameContainer";

                    Literal messageSenderName = new Literal();
                    messageSenderName.Text = row["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                    messageSenderNameContainer.Controls.Add(messageSenderName);
                    messageSenderInformationContainer.Controls.Add(messageSenderNameContainer);

                    Panel messageSentDateTimeContainer = new Panel();
                    messageSentDateTimeContainer.ID = "MessageSentDateTimeContainer_" + idGroupFeedMessage.ToString();
                    messageSentDateTimeContainer.CssClass = "MessageSentDateTimeContainer";

                    DateTime messageTimestamp = Convert.ToDateTime(row["timestamp"]);
                    messageTimestamp = TimeZoneInfo.ConvertTimeFromUtc(messageTimestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                    Literal messageSentDateTime = new Literal();
                    messageSentDateTime.Text = messageTimestamp.ToString();


                    messageSentDateTimeContainer.Controls.Add(messageSentDateTime);
                    messageSenderInformationContainer.Controls.Add(messageSentDateTimeContainer);

                    // attach sender information to header container
                    messageHeaderContainer.Controls.Add(messageSenderInformationContainer);

                    // approve and delete/dis-approve buttons
                    Panel messageActionButtonsContainer = new Panel();
                    messageActionButtonsContainer.ID = "MessageActionButtonsContainer_" + idGroupFeedMessage.ToString();
                    messageActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                    // approve button -- if not yet approved AND you are a moderator
                    if (!isMessageApproved)
                    {
                        Panel messageApproveButtonContainer = new Panel();
                        messageApproveButtonContainer.ID = "MessageApproveButtonContainer_" + idGroupFeedMessage.ToString();

                        Image messageApproveButton = new Image();
                        messageApproveButton.ID = "MessageApproveButton_" + idGroupFeedMessage.ToString();
                        messageApproveButton.CssClass = "XSmallIcon";
                        messageApproveButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                        messageApproveButton.AlternateText = _GlobalResources.Approve;
                        messageApproveButton.Style.Add("cursor", "pointer");
                        messageApproveButton.Attributes.Add("onclick", "ApproveMessage(\"" + idGroupFeedMessage.ToString() + "\");");

                        messageApproveButtonContainer.Controls.Add(messageApproveButton);
                        messageActionButtonsContainer.Controls.Add(messageApproveButtonContainer);
                    }

                    // delete/dis-approve button
                    Panel messageDeleteButtonContainer = new Panel();
                    messageDeleteButtonContainer.ID = "MessageDeleteButtonContainer_" + idGroupFeedMessage.ToString();

                    Image messageDeleteButton = new Image();
                    messageDeleteButton.ID = "MessageDeleteButton_" + idGroupFeedMessage.ToString();
                    messageDeleteButton.CssClass = "XSmallIcon";
                    messageDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    messageDeleteButton.AlternateText = _GlobalResources.Delete;
                    messageDeleteButton.Style.Add("cursor", "pointer");
                    messageDeleteButton.Attributes.Add("onclick", "DeleteMessage(\"" + idGroupFeedMessage.ToString() + "\");");

                    messageDeleteButtonContainer.Controls.Add(messageDeleteButton);
                    messageActionButtonsContainer.Controls.Add(messageDeleteButtonContainer);

                    // attach buttons container to header container
                    messageHeaderContainer.Controls.Add(messageActionButtonsContainer);

                    // attach header to message container
                    messageContainer.Controls.Add(messageHeaderContainer);

                    // MESSAGE DATA

                    Panel messageDataContainer = new Panel();
                    messageDataContainer.ID = "MessageDataContainer_" + idGroupFeedMessage.ToString();
                    messageDataContainer.CssClass = "MessageDataContainer";

                    Literal messageData = new Literal();
                    messageData.Text = row["message"].ToString();

                    messageDataContainer.Controls.Add(messageData);
                    messageContainer.Controls.Add(messageDataContainer);

                    // COMMENTS

                    Panel messageCommentsContainer = new Panel();
                    messageCommentsContainer.ID = "MessageCommentsContainer_" + idGroupFeedMessage.ToString();

                    DataTable messageComments = LMS.Library.GroupFeedMessage.GetComments(idGroupFeedMessage, false);

                    foreach (DataRow commentRow in messageComments.Rows)
                    {
                        int idGroupFeedMessageComment = Convert.ToInt32(commentRow["idGroupFeedMessage"]);
                        bool isCommentApproved = Convert.ToBoolean(commentRow["isApproved"]);

                        Panel messageCommentContainer = new Panel();
                        messageCommentContainer.ID = "MessageCommentContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentContainer.CssClass = "MessageCommentContainer";

                        // avatar
                        Panel messageCommentSenderAvatarContainer = new Panel();
                        messageCommentSenderAvatarContainer.ID = "MessageCommentSenderAvatarContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentSenderAvatarContainer.CssClass = "MessageCommentAvatar";

                        Image messageCommentSenderAvatar = new Image();
                        messageCommentSenderAvatar.ID = "MessageCommentSenderAvatar_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentSenderAvatar.CssClass = "SmallIcon";
                        messageCommentSenderAvatar.AlternateText = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                        if (!String.IsNullOrWhiteSpace(commentRow["avatar"].ToString()))
                        { messageCommentSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + Convert.ToInt32(commentRow["idAuthor"]) + "/" + commentRow["avatar"].ToString(); }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(commentRow["gender"].ToString()) && commentRow["gender"].ToString() == "f")
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                            else
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                        }

                        messageCommentSenderAvatarContainer.Controls.Add(messageCommentSenderAvatar);

                        // attach avatar to container
                        messageCommentContainer.Controls.Add(messageCommentSenderAvatarContainer);

                        // comment data
                        Panel messageCommentDataContainer = new Panel();
                        messageCommentDataContainer.ID = "MessageCommentDataContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentDataContainer.CssClass = "MessageCommentDataContainer";

                        Panel messageCommentData = new Panel();
                        messageCommentData.ID = "MessageCommentData_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentData.CssClass = "MessageCommentData";

                        Label messageCommentSenderName = new Label();
                        messageCommentSenderName.CssClass = "MessageCommentSenderName";
                        messageCommentSenderName.Text = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);
                        messageCommentData.Controls.Add(messageCommentSenderName);

                        Label messageCommentMessage = new Label();
                        messageCommentMessage.Text = commentRow["message"].ToString();
                        messageCommentData.Controls.Add(messageCommentMessage);

                        // approve and delete/dis-approve buttons
                        Panel messageCommentActionButtonsContainer = new Panel();
                        messageCommentActionButtonsContainer.ID = "MessageCommentActionButtonsContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                        // approve button -- if not yet approved AND you are a moderator
                        if (!isCommentApproved)
                        {
                            Panel messageCommentApproveButtonContainer = new Panel();
                            messageCommentApproveButtonContainer.ID = "MessageCommentApproveButtonContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();

                            Image messageCommentApproveButton = new Image();
                            messageCommentApproveButton.ID = "MessageCommentApproveButton_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentApproveButton.CssClass = "XSmallIcon";
                            messageCommentApproveButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                            messageCommentApproveButton.AlternateText = _GlobalResources.Approve;
                            messageCommentApproveButton.Style.Add("cursor", "pointer");
                            messageCommentApproveButton.Attributes.Add("onclick", "ApproveComment(\"" + idGroupFeedMessageComment.ToString() + "\", \"" + idGroupFeedMessage.ToString() + "\");");

                            messageCommentApproveButtonContainer.Controls.Add(messageCommentApproveButton);
                            messageCommentActionButtonsContainer.Controls.Add(messageCommentApproveButtonContainer);
                        }

                        // delete/dis-approve button
                        Panel messageCommentDeleteButtonContainer = new Panel();
                        messageCommentDeleteButtonContainer.ID = "MessageCommentDeleteButtonContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();

                        Image messageCommentDeleteButton = new Image();
                        messageCommentDeleteButton.ID = "MessageCommentDeleteButton_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentDeleteButton.CssClass = "XSmallIcon";
                        messageCommentDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        messageCommentDeleteButton.AlternateText = _GlobalResources.Delete;
                        messageCommentDeleteButton.Style.Add("cursor", "pointer");
                        messageCommentDeleteButton.Attributes.Add("onclick", "DeleteComment(\"" + idGroupFeedMessageComment.ToString() + "\", \"" + idGroupFeedMessage.ToString() + "\");");

                        messageCommentDeleteButtonContainer.Controls.Add(messageCommentDeleteButton);
                        messageCommentActionButtonsContainer.Controls.Add(messageCommentDeleteButtonContainer);

                        // attach buttons container to header container
                        messageCommentData.Controls.Add(messageCommentActionButtonsContainer);

                        messageCommentDataContainer.Controls.Add(messageCommentData);

                        // timestamp
                        Panel messageCommentTimestampContainer = new Panel();
                        messageCommentTimestampContainer.ID = "MessageCommentTimestampContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentTimestampContainer.CssClass = "MessageCommentTimestamp";

                        DateTime messageCommentTimestampDt = Convert.ToDateTime(commentRow["timestamp"]);
                        messageCommentTimestampDt = TimeZoneInfo.ConvertTimeFromUtc(messageCommentTimestampDt, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        Literal messageCommentTimestamp = new Literal();
                        messageCommentTimestamp.Text = messageCommentTimestampDt.ToString();
                        messageCommentTimestampContainer.Controls.Add(messageCommentTimestamp);

                        messageCommentDataContainer.Controls.Add(messageCommentTimestampContainer);

                        // attach comment data to comment container
                        messageCommentContainer.Controls.Add(messageCommentDataContainer);

                        // attach comment container to comments container
                        messageCommentsContainer.Controls.Add(messageCommentContainer);
                    }

                    // attach comments container to message container
                    messageContainer.Controls.Add(messageCommentsContainer);

                    // REPLY WITH COMMENT

                    Panel messageReplyCommentContainer = new Panel();
                    messageReplyCommentContainer.ID = "MessageReplyCommentContainer_" + idGroupFeedMessage.ToString();
                    messageReplyCommentContainer.CssClass = "MessageReplyCommentContainer";

                    // avatar
                    Panel messageReplyCommentSenderAvatarContainer = new Panel();
                    messageReplyCommentSenderAvatarContainer.ID = "MessageReplyCommentSenderAvatarContainer_" + idGroupFeedMessage.ToString();
                    messageReplyCommentSenderAvatarContainer.CssClass = "MessageReplyCommentAvatar";

                    Image messageReplyCommentSenderAvatar = new Image();
                    messageReplyCommentSenderAvatar.ID = "MessageReplyCommentSenderAvatar_" + idGroupFeedMessage.ToString();
                    messageReplyCommentSenderAvatar.CssClass = "SmallIcon";
                    messageReplyCommentSenderAvatar.AlternateText = String.Empty;
                    messageReplyCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);

                    messageReplyCommentSenderAvatarContainer.Controls.Add(messageReplyCommentSenderAvatar);

                    // attach avatar to reply comment container
                    messageReplyCommentContainer.Controls.Add(messageReplyCommentSenderAvatarContainer);

                    // comment field
                    TextBox messageCommentField = new TextBox();
                    messageCommentField.ID = "MessageCommentField_" + idGroupFeedMessage.ToString();
                    messageCommentField.CssClass = "MessageReplyCommentField";
                    messageCommentField.Attributes.Add("placeholder", _GlobalResources.PostComment);
                    messageCommentField.Attributes.Add("onkeyup", "PostComment(this, event);");
                    messageCommentField.AutoPostBack = false;

                    // if the mesage has not been approved, it cannot be commented on
                    if (!isMessageApproved)
                    { messageCommentField.Enabled = false; }

                    // attach field to reply comment container
                    messageReplyCommentContainer.Controls.Add(messageCommentField);

                    // attach reply comment container to message container
                    messageContainer.Controls.Add(messageReplyCommentContainer);

                    // write the data to a text writer for json output
                    TextWriter textWriter = new StringWriter();
                    HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                    messageContainer.RenderControl(htmlTextWriter);

                    jsonData.html += textWriter.ToString();
                }

                jsonData.actionSuccessful = true;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = lastRecord.ToString();
                jsonData.idMessage = null;
                jsonData.idParentMessage = null;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = dtQuery.ToString();
                jsonData.idMessage = null;
                jsonData.idParentMessage = null;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region SaveMessage
        [WebMethod(EnableSession = true)]
        public static _WallMessagesJsonData SaveMessage(int idGroup, string message, int? idParentMessage)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();
            bool isModerated = false; // set isModerated flag to false here because messages saved from this page are not subject to moderation

            try
            {
                // build and save the group feed message
                GroupFeedMessage groupFeedMessage = new GroupFeedMessage();
                groupFeedMessage.IdGroup = idGroup;
                groupFeedMessage.IdAuthor = AsentiaSessionState.IdSiteUser;

                if (idParentMessage != null)
                { groupFeedMessage.IdParentGroupFeedMessage = idParentMessage; }

                groupFeedMessage.Message = message;

                int idGroupFeedMessage = groupFeedMessage.Save(isModerated);

                if (!isModerated)
                {
                    // get author information or just hard code it if user is "administrator"
                    // note that the author is always the currently logged in user
                    string authorName = String.Empty;
                    string authorAvatar = String.Empty;
                    string authorGender = String.Empty;

                    if (groupFeedMessage.IdAuthor == 1)
                    { authorName = _GlobalResources.Administrator; }
                    else
                    {
                        authorName = AsentiaSessionState.UserFirstName + " " + AsentiaSessionState.UserLastName;
                        authorAvatar = AsentiaSessionState.UserAvatar;
                        authorGender = AsentiaSessionState.UserGender;
                    }

                    if (groupFeedMessage.IdParentGroupFeedMessage == null) // new message
                    {
                        Panel messageContainer = new Panel();
                        messageContainer.ID = "MessageContainer_" + idGroupFeedMessage.ToString();
                        messageContainer.CssClass = "MessageContainer";

                        // MESSAGE HEADER

                        Panel messageHeaderContainer = new Panel();
                        messageHeaderContainer.ID = "MessageHeaderContainer_" + idGroupFeedMessage.ToString();
                        messageHeaderContainer.CssClass = "MessageHeaderContainer";

                        // avatar
                        Panel messageSenderAvatarContainer = new Panel();
                        messageSenderAvatarContainer.ID = "MessageSenderAvatarContainer_" + idGroupFeedMessage.ToString();
                        messageSenderAvatarContainer.CssClass = "MessageAvatar";

                        Image messageSenderAvatar = new Image();
                        messageSenderAvatar.ID = "MessageSenderAvatar_" + idGroupFeedMessage.ToString();
                        messageSenderAvatar.CssClass = "MediumIcon";
                        messageSenderAvatar.AlternateText = authorName;

                        if (!String.IsNullOrWhiteSpace(authorAvatar))
                        { messageSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + groupFeedMessage.IdAuthor + "/" + authorAvatar; }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(authorGender) && authorGender == "f")
                            { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                            else
                            { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                        }

                        messageSenderAvatarContainer.Controls.Add(messageSenderAvatar);

                        // attach avatar to header container
                        messageHeaderContainer.Controls.Add(messageSenderAvatarContainer);

                        // sender information
                        Panel messageSenderInformationContainer = new Panel();
                        messageSenderInformationContainer.ID = "MessageSenderInformationContainer_" + idGroupFeedMessage.ToString();
                        messageSenderInformationContainer.CssClass = "MessageSenderInformationContainer";

                        Panel messageSenderNameContainer = new Panel();
                        messageSenderNameContainer.ID = "MessageSenderNameContainer_" + idGroupFeedMessage.ToString();
                        messageSenderNameContainer.CssClass = "MessageSenderNameContainer";

                        Literal messageSenderName = new Literal();
                        messageSenderName.Text = authorName;

                        messageSenderNameContainer.Controls.Add(messageSenderName);
                        messageSenderInformationContainer.Controls.Add(messageSenderNameContainer);

                        Panel messageSentDateTimeContainer = new Panel();
                        messageSentDateTimeContainer.ID = "MessageSentDateTimeContainer_" + idGroupFeedMessage.ToString();
                        messageSentDateTimeContainer.CssClass = "MessageSentDateTimeContainer";

                        DateTime messageTimestamp = DateTime.UtcNow;
                        messageTimestamp = TimeZoneInfo.ConvertTimeFromUtc(messageTimestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        Literal messageSentDateTime = new Literal();
                        messageSentDateTime.Text = messageTimestamp.ToString();


                        messageSentDateTimeContainer.Controls.Add(messageSentDateTime);
                        messageSenderInformationContainer.Controls.Add(messageSentDateTimeContainer);

                        // attach sender information to header container
                        messageHeaderContainer.Controls.Add(messageSenderInformationContainer);

                        // approve and delete/dis-approve buttons
                        Panel messageActionButtonsContainer = new Panel();
                        messageActionButtonsContainer.ID = "MessageActionButtonsContainer_" + idGroupFeedMessage.ToString();
                        messageActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                        // approve button -- if not yet approved AND you are a moderator
                        if (isModerated)
                        {
                            Panel messageApproveButtonContainer = new Panel();
                            messageApproveButtonContainer.ID = "MessageApproveButtonContainer_" + idGroupFeedMessage.ToString();

                            Image messageApproveButton = new Image();
                            messageApproveButton.ID = "MessageApproveButton_" + idGroupFeedMessage.ToString();
                            messageApproveButton.CssClass = "XSmallIcon";
                            messageApproveButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                            messageApproveButton.AlternateText = _GlobalResources.Approve;
                            messageApproveButton.Style.Add("cursor", "pointer");
                            messageApproveButton.Attributes.Add("onclick", "ApproveMessage(\"" + idGroupFeedMessage.ToString() + "\");");

                            messageApproveButtonContainer.Controls.Add(messageApproveButton);
                            messageActionButtonsContainer.Controls.Add(messageApproveButtonContainer);
                        }

                        // delete/dis-approve button
                        Panel messageDeleteButtonContainer = new Panel();
                        messageDeleteButtonContainer.ID = "MessageDeleteButtonContainer_" + idGroupFeedMessage.ToString();

                        Image messageDeleteButton = new Image();
                        messageDeleteButton.ID = "MessageDeleteButton_" + idGroupFeedMessage.ToString();
                        messageDeleteButton.CssClass = "XSmallIcon";
                        messageDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        messageDeleteButton.AlternateText = _GlobalResources.Delete;
                        messageDeleteButton.Style.Add("cursor", "pointer");
                        messageDeleteButton.Attributes.Add("onclick", "DeleteMessage(\"" + idGroupFeedMessage.ToString() + "\");");

                        messageDeleteButtonContainer.Controls.Add(messageDeleteButton);
                        messageActionButtonsContainer.Controls.Add(messageDeleteButtonContainer);

                        // attach buttons container to header container
                        messageHeaderContainer.Controls.Add(messageActionButtonsContainer);

                        // attach header to message container
                        messageContainer.Controls.Add(messageHeaderContainer);

                        // MESSAGE DATA

                        Panel messageDataContainer = new Panel();
                        messageDataContainer.ID = "MessageDataContainer_" + idGroupFeedMessage.ToString();
                        messageDataContainer.CssClass = "MessageDataContainer";

                        Literal messageData = new Literal();
                        messageData.Text = message;

                        messageDataContainer.Controls.Add(messageData);
                        messageContainer.Controls.Add(messageDataContainer);

                        // COMMENTS

                        Panel messageCommentsContainer = new Panel();
                        messageCommentsContainer.ID = "MessageCommentsContainer_" + idGroupFeedMessage.ToString();

                        DataTable messageComments = LMS.Library.GroupFeedMessage.GetComments(idGroupFeedMessage, false);

                        foreach (DataRow commentRow in messageComments.Rows)
                        {
                            int idGroupFeedMessageComment = Convert.ToInt32(commentRow["idGroupFeedMessage"]);
                            bool isCommentApproved = Convert.ToBoolean(commentRow["isApproved"]);

                            Panel messageCommentContainer = new Panel();
                            messageCommentContainer.ID = "MessageCommentContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentContainer.CssClass = "MessageCommentContainer";

                            // avatar
                            Panel messageCommentSenderAvatarContainer = new Panel();
                            messageCommentSenderAvatarContainer.ID = "MessageCommentSenderAvatarContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentSenderAvatarContainer.CssClass = "MessageCommentAvatar";

                            Image messageCommentSenderAvatar = new Image();
                            messageCommentSenderAvatar.ID = "MessageCommentSenderAvatar_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentSenderAvatar.CssClass = "SmallIcon";
                            messageCommentSenderAvatar.AlternateText = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                            if (!String.IsNullOrWhiteSpace(commentRow["avatar"].ToString()))
                            { messageCommentSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + Convert.ToInt32(commentRow["idAuthor"]) + "/" + commentRow["avatar"].ToString(); }
                            else
                            {
                                if (!String.IsNullOrWhiteSpace(commentRow["gender"].ToString()) && commentRow["gender"].ToString() == "f")
                                { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                                else
                                { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                            }

                            messageCommentSenderAvatarContainer.Controls.Add(messageCommentSenderAvatar);

                            // attach avatar to container
                            messageCommentContainer.Controls.Add(messageCommentSenderAvatarContainer);

                            // comment data
                            Panel messageCommentDataContainer = new Panel();
                            messageCommentDataContainer.ID = "MessageCommentDataContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentDataContainer.CssClass = "MessageCommentDataContainer";

                            Panel messageCommentData = new Panel();
                            messageCommentData.ID = "MessageCommentData_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentData.CssClass = "MessageCommentData";

                            Label messageCommentSenderName = new Label();
                            messageCommentSenderName.CssClass = "MessageCommentSenderName";
                            messageCommentSenderName.Text = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);
                            messageCommentData.Controls.Add(messageCommentSenderName);

                            Label messageCommentMessage = new Label();
                            messageCommentMessage.Text = commentRow["message"].ToString();
                            messageCommentData.Controls.Add(messageCommentMessage);

                            // approve and delete/dis-approve buttons
                            Panel messageCommentActionButtonsContainer = new Panel();
                            messageCommentActionButtonsContainer.ID = "MessageCommentActionButtonsContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                            // approve button -- if not yet approved AND you are a moderator
                            if (!isCommentApproved)
                            {
                                Panel messageCommentApproveButtonContainer = new Panel();
                                messageCommentApproveButtonContainer.ID = "MessageCommentApproveButtonContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();

                                Image messageCommentApproveButton = new Image();
                                messageCommentApproveButton.ID = "MessageCommentApproveButton_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                                messageCommentApproveButton.CssClass = "XSmallIcon";
                                messageCommentApproveButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                                messageCommentApproveButton.AlternateText = _GlobalResources.Approve;
                                messageCommentApproveButton.Style.Add("cursor", "pointer");
                                messageCommentApproveButton.Attributes.Add("onclick", "ApproveComment(\"" + idGroupFeedMessageComment.ToString() + "\", \"" + idGroupFeedMessage.ToString() + "\");");

                                messageCommentApproveButtonContainer.Controls.Add(messageCommentApproveButton);
                                messageCommentActionButtonsContainer.Controls.Add(messageCommentApproveButtonContainer);
                            }

                            // delete/dis-approve button
                            Panel messageCommentDeleteButtonContainer = new Panel();
                            messageCommentDeleteButtonContainer.ID = "MessageCommentDeleteButtonContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();

                            Image messageCommentDeleteButton = new Image();
                            messageCommentDeleteButton.ID = "MessageCommentDeleteButton_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentDeleteButton.CssClass = "XSmallIcon";
                            messageCommentDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                            messageCommentDeleteButton.AlternateText = _GlobalResources.Delete;
                            messageCommentDeleteButton.Style.Add("cursor", "pointer");
                            messageCommentDeleteButton.Attributes.Add("onclick", "DeleteComment(\"" + idGroupFeedMessageComment.ToString() + "\", \"" + idGroupFeedMessage.ToString() + "\");");

                            messageCommentDeleteButtonContainer.Controls.Add(messageCommentDeleteButton);
                            messageCommentActionButtonsContainer.Controls.Add(messageCommentDeleteButtonContainer);

                            // attach buttons container to header container
                            messageCommentData.Controls.Add(messageCommentActionButtonsContainer);

                            messageCommentDataContainer.Controls.Add(messageCommentData);

                            // timestamp
                            Panel messageCommentTimestampContainer = new Panel();
                            messageCommentTimestampContainer.ID = "MessageCommentTimestampContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentTimestampContainer.CssClass = "MessageCommentTimestamp";

                            DateTime messageCommentTimestampDt = Convert.ToDateTime(commentRow["timestamp"]);
                            messageCommentTimestampDt = TimeZoneInfo.ConvertTimeFromUtc(messageCommentTimestampDt, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                            Literal messageCommentTimestamp = new Literal();
                            messageCommentTimestamp.Text = messageCommentTimestampDt.ToString();
                            messageCommentTimestampContainer.Controls.Add(messageCommentTimestamp);

                            messageCommentDataContainer.Controls.Add(messageCommentTimestampContainer);

                            // attach comment data to comment container
                            messageCommentContainer.Controls.Add(messageCommentDataContainer);

                            // attach comment container to comments container
                            messageCommentsContainer.Controls.Add(messageCommentContainer);
                        }

                        // attach comments container to message container
                        messageContainer.Controls.Add(messageCommentsContainer);

                        // REPLY WITH COMMENT

                        Panel messageReplyCommentContainer = new Panel();
                        messageReplyCommentContainer.ID = "MessageReplyCommentContainer_" + idGroupFeedMessage.ToString();
                        messageReplyCommentContainer.CssClass = "MessageReplyCommentContainer";

                        // avatar
                        Panel messageReplyCommentSenderAvatarContainer = new Panel();
                        messageReplyCommentSenderAvatarContainer.ID = "MessageReplyCommentSenderAvatarContainer_" + idGroupFeedMessage.ToString();
                        messageReplyCommentSenderAvatarContainer.CssClass = "MessageReplyCommentAvatar";

                        Image messageReplyCommentSenderAvatar = new Image();
                        messageReplyCommentSenderAvatar.ID = "MessageReplyCommentSenderAvatar_" + idGroupFeedMessage.ToString();
                        messageReplyCommentSenderAvatar.CssClass = "SmallIcon";
                        messageReplyCommentSenderAvatar.AlternateText = String.Empty;
                        messageReplyCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);

                        messageReplyCommentSenderAvatarContainer.Controls.Add(messageReplyCommentSenderAvatar);

                        // attach avatar to reply comment container
                        messageReplyCommentContainer.Controls.Add(messageReplyCommentSenderAvatarContainer);

                        // comment field
                        TextBox messageCommentField = new TextBox();
                        messageCommentField.ID = "MessageCommentField_" + idGroupFeedMessage.ToString();
                        messageCommentField.CssClass = "MessageReplyCommentField";
                        messageCommentField.Attributes.Add("placeholder", _GlobalResources.PostComment);
                        messageCommentField.Attributes.Add("onkeyup", "PostComment(this, event);");
                        messageCommentField.AutoPostBack = false;

                        // attach field to reply comment container
                        messageReplyCommentContainer.Controls.Add(messageCommentField);

                        // attach reply comment container to message container
                        messageContainer.Controls.Add(messageReplyCommentContainer);

                        // write the data to a text writer for json output
                        TextWriter textWriter = new StringWriter();
                        HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                        messageContainer.RenderControl(htmlTextWriter);

                        jsonData.html += textWriter.ToString();
                    }
                    else // just a comment
                    {
                        Panel messageCommentContainer = new Panel();
                        messageCommentContainer.ID = "MessageCommentContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentContainer.CssClass = "MessageCommentContainer";

                        // avatar
                        Panel messageCommentSenderAvatarContainer = new Panel();
                        messageCommentSenderAvatarContainer.ID = "MessageCommentSenderAvatarContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentSenderAvatarContainer.CssClass = "MessageCommentAvatar";

                        Image messageCommentSenderAvatar = new Image();
                        messageCommentSenderAvatar.ID = "MessageCommentSenderAvatar_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentSenderAvatar.CssClass = "SmallIcon";
                        messageCommentSenderAvatar.AlternateText = authorName;

                        if (!String.IsNullOrWhiteSpace(authorAvatar))
                        { messageCommentSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + groupFeedMessage.IdAuthor + "/" + authorAvatar; }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(authorGender) && authorGender == "f")
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                            else
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                        }

                        messageCommentSenderAvatarContainer.Controls.Add(messageCommentSenderAvatar);

                        // attach avatar to container
                        messageCommentContainer.Controls.Add(messageCommentSenderAvatarContainer);

                        // comment data
                        Panel messageCommentDataContainer = new Panel();
                        messageCommentDataContainer.ID = "MessageCommentDataContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentDataContainer.CssClass = "MessageCommentDataContainer";

                        Panel messageCommentData = new Panel();
                        messageCommentData.ID = "MessageCommentData_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentData.CssClass = "MessageCommentData";

                        Label messageCommentSenderName = new Label();
                        messageCommentSenderName.CssClass = "MessageCommentSenderName";
                        messageCommentSenderName.Text = authorName;
                        messageCommentData.Controls.Add(messageCommentSenderName);

                        Label messageCommentMessage = new Label();
                        messageCommentMessage.Text = message;
                        messageCommentData.Controls.Add(messageCommentMessage);

                        // approve and delete/dis-approve buttons
                        Panel messageCommentActionButtonsContainer = new Panel();
                        messageCommentActionButtonsContainer.ID = "MessageCommentActionButtonsContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                        // approve button -- if not yet approved AND you are a moderator
                        if (!isModerated)
                        {
                            Panel messageCommentApproveButtonContainer = new Panel();
                            messageCommentApproveButtonContainer.ID = "MessageCommentApproveButtonContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();

                            Image messageCommentApproveButton = new Image();
                            messageCommentApproveButton.ID = "MessageCommentApproveButton_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                            messageCommentApproveButton.CssClass = "XSmallIcon";
                            messageCommentApproveButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                            messageCommentApproveButton.AlternateText = _GlobalResources.Approve;
                            messageCommentApproveButton.Style.Add("cursor", "pointer");
                            messageCommentApproveButton.Attributes.Add("onclick", "ApproveComment(\"" + idGroupFeedMessage.ToString() + "\", \"" + idParentMessage.ToString() + "\");");

                            messageCommentApproveButtonContainer.Controls.Add(messageCommentApproveButton);
                            messageCommentActionButtonsContainer.Controls.Add(messageCommentApproveButtonContainer);
                        }

                        // delete/dis-approve button
                        Panel messageCommentDeleteButtonContainer = new Panel();
                        messageCommentDeleteButtonContainer.ID = "MessageCommentDeleteButtonContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();

                        Image messageCommentDeleteButton = new Image();
                        messageCommentDeleteButton.ID = "MessageCommentDeleteButton_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentDeleteButton.CssClass = "XSmallIcon";
                        messageCommentDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        messageCommentDeleteButton.AlternateText = _GlobalResources.Delete;
                        messageCommentDeleteButton.Style.Add("cursor", "pointer");
                        messageCommentDeleteButton.Attributes.Add("onclick", "DeleteComment(\"" + idGroupFeedMessage.ToString() + "\", \"" + idParentMessage.ToString() + "\");");

                        messageCommentDeleteButtonContainer.Controls.Add(messageCommentDeleteButton);
                        messageCommentActionButtonsContainer.Controls.Add(messageCommentDeleteButtonContainer);

                        // attach buttons container to header container
                        messageCommentData.Controls.Add(messageCommentActionButtonsContainer);

                        messageCommentDataContainer.Controls.Add(messageCommentData);

                        // timestamp
                        Panel messageCommentTimestampContainer = new Panel();
                        messageCommentTimestampContainer.ID = "MessageCommentTimestampContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentTimestampContainer.CssClass = "MessageCommentTimestamp";

                        DateTime messageCommentTimestampDt = DateTime.UtcNow;
                        messageCommentTimestampDt = TimeZoneInfo.ConvertTimeFromUtc(messageCommentTimestampDt, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        Literal messageCommentTimestamp = new Literal();
                        messageCommentTimestamp.Text = messageCommentTimestampDt.ToString();
                        messageCommentTimestampContainer.Controls.Add(messageCommentTimestamp);

                        messageCommentDataContainer.Controls.Add(messageCommentTimestampContainer);

                        // attach comment data to comment container
                        messageCommentContainer.Controls.Add(messageCommentDataContainer);

                        // write the data to a text writer for json output
                        TextWriter textWriter = new StringWriter();
                        HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                        messageCommentContainer.RenderControl(htmlTextWriter);

                        jsonData.html += textWriter.ToString();
                    }
                }
                else // empty html json data, used to just show a "message saved for moderation" modal
                { jsonData.html = String.Empty; }

                jsonData.actionSuccessful = true;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = null;
                jsonData.idMessage = idGroupFeedMessage;
                jsonData.idParentMessage = idParentMessage;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = null;
                jsonData.idMessage = null;
                jsonData.idParentMessage = idParentMessage;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region ApproveMessage
        [WebMethod(EnableSession = true)]
        public static _WallMessagesJsonData ApproveMessage(int idMessage, int? idParentMessage)
        {
            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            try
            {
                GroupFeedMessage.Approve(idMessage);

                jsonData.actionSuccessful = true;
                jsonData.html = String.Empty;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region DeleteMessage
        [WebMethod(EnableSession = true)]
        public static _WallMessagesJsonData DeleteMessage(int idMessage, int? idParentMessage)
        {
            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            try
            {
                GroupFeedMessage.Delete(idMessage);

                jsonData.actionSuccessful = true;
                jsonData.html = String.Empty;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region _BuildModeratorsPanel
        /// <summary>
        /// Builds the moderators panel.
        /// </summary>
        private void _BuildModeratorsPanel()
        {
            // populate datatables with lists of users who are moderators and who are not
            this._WallModerators = this._GroupObject.GetWallModerators(null);
            this._EligibleModeratorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForGroupWallModeratorsSelectList(this._GroupObject.Id, null);

            Panel moderatorsPanel = new Panel();
            moderatorsPanel.ID = "WallProperties_" + "Moderators" + "_TabPanel";
            moderatorsPanel.Attributes.Add("style", "display: none;");

            // wall moderators
            List<Control> wallModeratorsInputControls = new List<Control>();

            // selected wall moderators hidden field
            this._SelectedWallModerators = new HiddenField();
            this._SelectedWallModerators.ID = "SelectedWallModerators_Field";
            wallModeratorsInputControls.Add(this._SelectedWallModerators);

            // build a container for the wall moderators listing
            this._WallModeratorsListContainer = new Panel();
            this._WallModeratorsListContainer.ID = "WallModeratorsList_Container";
            this._WallModeratorsListContainer.CssClass = "ItemListingContainer";

            wallModeratorsInputControls.Add(this._WallModeratorsListContainer);

            Panel wallModeratorsButtonsPanel = new Panel();
            wallModeratorsButtonsPanel.ID = "WallModerators_ButtonsPanel";

            // select moderators button

            // link
            Image selectModeratorsImageForLink = new Image();
            selectModeratorsImageForLink.ID = "LaunchSelectModeratorsModalImage";
            selectModeratorsImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                           ImageFiles.EXT_PNG);
            selectModeratorsImageForLink.CssClass = "MediumIcon";

            Localize selectModeratorsTextForLink = new Localize();
            selectModeratorsTextForLink.Text = _GlobalResources.SelectModerator_s;

            LinkButton selectModeratorsLink = new LinkButton();
            selectModeratorsLink.ID = "LaunchSelectModeratorssModal";
            selectModeratorsLink.CssClass = "ImageLink";
            selectModeratorsLink.Controls.Add(selectModeratorsImageForLink);
            selectModeratorsLink.Controls.Add(selectModeratorsTextForLink);
            wallModeratorsButtonsPanel.Controls.Add(selectModeratorsLink);

            // attach the buttons panel to the container
            wallModeratorsInputControls.Add(wallModeratorsButtonsPanel);

            // build modals for adding and removing moderators
            this._BuildSelectModeratorsModal(selectModeratorsLink.ID);

            moderatorsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("WallModerators",
                                                                                        String.Empty,
                                                                                        wallModeratorsInputControls,
                                                                                        false,
                                                                                        true));

            // build actions panel
            Panel moderatorsActionsPanel = new Panel();
            moderatorsActionsPanel.ID = "ModeratorsActionsPanel";
            moderatorsActionsPanel.CssClass = "ActionsPanel";

            // save button
            Button moderatorsSaveButton = new Button();
            moderatorsSaveButton.ID = "ModeratorsSaveButton";
            moderatorsSaveButton.CssClass = "Button ActionButton";
            moderatorsSaveButton.Text = _GlobalResources.SaveChanges;
            moderatorsSaveButton.Command += new CommandEventHandler(this._ModeratorsSaveButtonCommand);
            moderatorsSaveButton.Attributes.Add("onclick", "PopulateModeratorsHiddenField();");
            moderatorsActionsPanel.Controls.Add(moderatorsSaveButton);
            moderatorsPanel.DefaultButton = moderatorsSaveButton.ID;

            // cancel button
            Button moderatorsCancelButton = new Button();
            moderatorsCancelButton.ID = "ModeratorsCancelButton";
            moderatorsCancelButton.CssClass = "Button NonActionButton";
            moderatorsCancelButton.Text = _GlobalResources.Cancel;
            moderatorsCancelButton.Command += new CommandEventHandler(this._ModeratorsCancelButtonCommand);
            moderatorsActionsPanel.Controls.Add(moderatorsCancelButton);

            moderatorsPanel.Controls.Add(moderatorsActionsPanel);

            // attach panel to container
            this.WallPropertiesTabPanelsContainer.Controls.Add(moderatorsPanel);

            // populate the wall moderators list box
            this._PopulateWallModerators();
        }
        #endregion

        #region _BuildSelectModeratorsModal
        private void _BuildSelectModeratorsModal(string targetControlId)
        {
            // set modal properties
            this._SelectModerators = new ModalPopup("SelectModeratorsModal");
            this._SelectModerators.Type = ModalPopupType.Form;
            this._SelectModerators.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                           ImageFiles.EXT_PNG);
            this._SelectModerators.HeaderIconAlt = _GlobalResources.SelectModerator_s;
            this._SelectModerators.HeaderText = _GlobalResources.SelectModerator_s;
            this._SelectModerators.TargetControlID = targetControlId;
            this._SelectModerators.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectModerators.SubmitButtonCustomText = _GlobalResources.AddModerator_s;
            this._SelectModerators.SubmitButton.OnClientClick = "javascript:AddModerators(); return false;";
            this._SelectModerators.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectModerators.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectModerators.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleUsers = new DynamicListBox("SelectEligibleUsers");
            this._SelectEligibleUsers.NoRecordsFoundMessage = _GlobalResources.NoUsersFound;
            this._SelectEligibleUsers.SearchButton.Command += new CommandEventHandler(this._SearchSelectModeratorsButton_Command);
            this._SelectEligibleUsers.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectModeratorsButton_Command);
            this._SelectEligibleUsers.ListBoxControl.DataSource = this._EligibleModeratorsForSelectList;
            this._SelectEligibleUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleUsers.ListBoxControl.DataBind();

            // add controls to body
            this._SelectModerators.AddControlToBody(this._SelectEligibleUsers);

            // add modal to container
            this.WallPropertiesTabPanelsContainer.Controls.Add(this._SelectModerators);
        }
        #endregion

        #region _SearchSelectModeratorsButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Moderators(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectModeratorsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectModerators.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleUsers.ListBoxControl.Items.Clear();

            // do the search
            this._EligibleModeratorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForGroupWallModeratorsSelectList(this._GroupObject.Id, this._SelectEligibleUsers.SearchTextBox.Text);
            this._SelectEligibleUsersDataBind();
        }
        #endregion

        #region _ClearSearchSelectModeratorsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Moderator(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectModeratorsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectModerators.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleUsers.ListBoxControl.Items.Clear();
            this._SelectEligibleUsers.SearchTextBox.Text = "";

            // clear the search
            this._EligibleModeratorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForGroupWallModeratorsSelectList(this._GroupObject.Id, null);
            this._SelectEligibleUsersDataBind();
        }
        #endregion

        #region _SelectEligibleUsersDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectEligibleUsersDataBind()
        {
            this._SelectEligibleUsers.ListBoxControl.DataSource = this._EligibleModeratorsForSelectList;
            this._SelectEligibleUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleUsers.ListBoxControl.DataBind();

            //if no records available then disable the list
            if (this._SelectEligibleUsers.ListBoxControl.Items.Count == 0)
            {
                this._SelectModerators.SubmitButton.Enabled = false;
                this._SelectModerators.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectModerators.SubmitButton.Enabled = true;
                this._SelectModerators.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _PopulateWallModerators
        /// <summary>
        /// Populates the wall moderators list box with the wall moderators.
        /// </summary>
        private void _PopulateWallModerators()
        {
            this._WallModeratorsListContainer.Controls.Clear();

            this._WallModerators = this._GroupObject.GetWallModerators(null);

            // loop through the data table and add each moderator to the listing container
            foreach (DataRow row in this._WallModerators.Rows)
            {
                // container
                Panel userNameContainer = new Panel();
                userNameContainer.ID = "Moderator_" + row["idUser"].ToString();

                // remove moderator button
                Image removeModeratorImage = new Image();
                removeModeratorImage.ID = "Moderator_" + row["idUser"].ToString() + "_RemoveImage";
                removeModeratorImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                       ImageFiles.EXT_PNG);
                removeModeratorImage.CssClass = "SmallIcon";
                removeModeratorImage.Attributes.Add("onClick", "javascript:RemoveModerator('" + row["idUser"].ToString() + "');");
                removeModeratorImage.Style.Add("cursor", "pointer");

                // moderator name
                Literal userName = new Literal();
                userName.Text = row["displayName"].ToString();

                // add controls to container
                userNameContainer.Controls.Add(removeModeratorImage);
                userNameContainer.Controls.Add(userName);
                this._WallModeratorsListContainer.Controls.Add(userNameContainer);
            }
        }
        #endregion

        #region _ModeratorsSaveButtonCommand
        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ModeratorsSaveButtonCommand(object sender, CommandEventArgs e)
        {
            try
            {
                // split the "value" of the hidden field to get an array of moderator ids
                string[] selectedWallModeratorIds;

                // delcare data table
                DataTable wallModeratorsToSave = new DataTable(); ;
                wallModeratorsToSave.Columns.Add("id", typeof(int));

                if (!String.IsNullOrWhiteSpace(this._SelectedWallModerators.Value))
                {
                    selectedWallModeratorIds = this._SelectedWallModerators.Value.Split(',');

                    // put ids into datatable 
                    foreach (string wallModeratorId in selectedWallModeratorIds)
                    { wallModeratorsToSave.Rows.Add(Convert.ToInt32(wallModeratorId)); }
                }

                // save the selected moderators
                this._GroupObject.SaveWallModerators(wallModeratorsToSave);

                // remove the selected users from the list box so they cannot be re-selected
                this._SelectEligibleUsers.RemoveSelectedItems();

                // only build moderators panel if group wall is moderated
                if (this._GroupObject.IsFeedModerated == true)
                {
                    // populate data tables with lists of users who are moderators and who are not
                    this._EligibleModeratorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForGroupWallModeratorsSelectList(this._GroupObject.Id, null);

                    this._PopulateWallModerators();

                    this._SelectEligibleUsers.ListBoxControl.DataSource = this._EligibleModeratorsForSelectList;
                    this._SelectEligibleUsers.ListBoxControl.DataTextField = "displayName";
                    this._SelectEligibleUsers.ListBoxControl.DataValueField = "idUser";
                    this._SelectEligibleUsers.ListBoxControl.DataBind();
                }

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.WallPropertiesFeedbackContainer, _GlobalResources.ModeratorsHaveBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.WallPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.WallPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.WallPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.WallPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.WallPropertiesFeedbackContainer, ex.Message, true);
            }
        }
        #endregion

        #region _ModeratorsCancelButtonCommand
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ModeratorsCancelButtonCommand(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/groups/ManageWall.aspx?id=" + this._GroupObject.Id.ToString());
        }
        #endregion
    }
}
