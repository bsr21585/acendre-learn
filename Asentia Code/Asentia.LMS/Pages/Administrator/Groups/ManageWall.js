﻿function GetRecords() {
    IsLoading = true;
    $("#OlderMessagesLoadingPanel").show();

    $.ajax({
        type: "POST",
        url: "ManageWall.aspx/BuildFeedMessages",
        data: "{idGroup: " + GroupId + ", dtQuery: \"" + LastRecord + "\", getMessagesNewerThanDtQuery: " + false + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnGetRecordsSuccess,
        failure: function (response) {
            $("#OlderMessagesLoadingPanel").hide();
            IsLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            $("#OlderMessagesLoadingPanel").hide();
            IsLoading = false;
            //alert(response.d);
        }
    });
}

function OnGetRecordsSuccess(response) {
    var responseObject = response.d;

    $(responseObject.html).hide().appendTo("#WallMessagesContainer").fadeIn(1000);
    LastRecord = responseObject.lastRecord;

    $("#OlderMessagesLoadingPanel").hide();
    IsLoading = false;
}

function PostNewMessage(e) {
    var enterKey = 13;
    var leftMouseClick = 1; // for click of post message button

    if (e.which == enterKey || e.which == leftMouseClick) {
        var textBoxValue = $("#NewMessageField").val().replace(/\"/g, '\\\"');

        if (textBoxValue != "") {

            IsLoading = true;

            $.ajax({
                type: "POST",
                url: "ManageWall.aspx/SaveMessage",
                data: "{idGroup: " + GroupId + ", message: \"" + textBoxValue + "\", idParentMessage: " + null + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPostNewMessageSuccess,
                failure: function (response) {
                    IsLoading = false;
                    //alert(response.d);
                },
                error: function (response) {
                    IsLoading = false;
                    //alert(response.d);
                }
            });

            $("#NewMessageField").val("");
        }
    }
}

function OnPostNewMessageSuccess(response) {
    var responseObject = response.d;

    if (responseObject.html != "") {
        $(responseObject.html).hide().prependTo("#WallMessagesContainer").fadeIn(1000);
    }
    else {
        $("#HiddenButtonForPostedForModerationModal").click();
    }

    IsLoading = false;
}

function PostComment(sender, e) {
    var enterKey = 13;

    if (e.which == enterKey) {
        var textBoxValue = $("#" + sender.id).val();

        if (textBoxValue != "") {
            var idParentMessage = sender.id.replace("MessageCommentField_", "");

            IsLoading = true;

            $.ajax({
                type: "POST",
                url: "ManageWall.aspx/SaveMessage",
                data: "{idGroup: " + GroupId + ", message: \"" + textBoxValue + "\", idParentMessage: " + idParentMessage + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPostCommentSuccess,
                failure: function (response) {
                    IsLoading = false;
                    //alert(response.d);
                },
                error: function (response) {
                    IsLoading = false;
                    //alert(response.d);
                }
            });

            $("#" + sender.id).val("");
        }
    }
}

function OnPostCommentSuccess(response) {
    var responseObject = response.d;

    if (responseObject.html != "") {
        $(responseObject.html).hide().appendTo("#MessageCommentsContainer_" + responseObject.idParentMessage).fadeIn(1000);
    }
    else {
        $("#HiddenButtonForPostedForModerationModal").click();
    }

    IsLoading = false;
}

function ApproveMessage(idMessage) {
    IsLoading = true;

    var idParentMessage = null;

    $.ajax({
        type: "POST",
        url: "ManageWall.aspx/ApproveMessage",
        data: "{idMessage: " + idMessage + ", idParentMessage: " + idParentMessage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnApproveMessageSuccess,
        failure: function (response) {
            IsLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            IsLoading = false;
            //alert(response.d);
        }
    });
}

function OnApproveMessageSuccess(response) {
    var responseObject = response.d;

    $("#MessageApproveButtonContainer_" + responseObject.idMessage).remove();
    $("#MessageCommentField_" + responseObject.idMessage).prop('disabled', false);
    $("#HiddenButtonForMessageApprovedModal").click();
    IsLoading = false;
}

function DeleteMessage(idMessage) {
    IsLoading = true;

    var idParentMessage = null;

    $.ajax({
        type: "POST",
        url: "ManageWall.aspx/DeleteMessage",
        data: "{idMessage: " + idMessage + ", idParentMessage: " + idParentMessage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnDeleteMessageSuccess,
        failure: function (response) {
            IsLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            IsLoading = false;
            //alert(response.d);
        }
    });
}

function OnDeleteMessageSuccess(response) {
    var responseObject = response.d;

    $("#MessageContainer_" + responseObject.idMessage).remove();
    $("#HiddenButtonForMessageDeletedModal").click();
    IsLoading = false;
}

function ApproveComment(idMessage, idParentMessage) {
    IsLoading = true;

    $.ajax({
        type: "POST",
        url: "ManageWall.aspx/ApproveMessage",
        data: "{idMessage: " + idMessage + ", idParentMessage: " + idParentMessage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnApproveCommentSuccess,
        failure: function (response) {
            IsLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            IsLoading = false;
            //alert(response.d);
        }
    });
}

function OnApproveCommentSuccess(response) {
    var responseObject = response.d;

    $("#MessageCommentApproveButton_" + responseObject.idParentMessage + "_" + responseObject.idMessage).remove();
    $("#HiddenButtonForMessageApprovedModal").click();
    IsLoading = false;
}

function DeleteComment(idMessage, idParentMessage) {
    IsLoading = true;

    $.ajax({
        type: "POST",
        url: "ManageWall.aspx/DeleteMessage",
        data: "{idMessage: " + idMessage + ", idParentMessage: " + idParentMessage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnDeleteCommentSuccess,
        failure: function (response) {
            IsLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            IsLoading = false;
            //alert(response.d);
        }
    });
}

function OnDeleteCommentSuccess(response) {
    var responseObject = response.d;

    $("#MessageCommentContainer_" + responseObject.idParentMessage + "_" + responseObject.idMessage).remove();
    $("#HiddenButtonForMessageDeletedModal").click();
    IsLoading = false;
}

function AddModerators() {
    var wallModeratorsListContainer = $("#WallModeratorsList_Container");
    var selectedUsers = $('select#SelectEligibleUsersListBox').val();

    if (selectedUsers != null) {
        for (var i = 0; i < selectedUsers.length; i++) {
            if (!$("#Moderator_" + selectedUsers[i]).length) {
                // add the selected user to the course expert list container
                var itemContainerDiv = $("<div id=\"Moderator_" + selectedUsers[i] + "\">" + "<img class='SmallIcon' onclick=\"javascript:RemoveModerator('" + selectedUsers[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" />" + $("#SelectEligibleUsersListBox option[value='" + selectedUsers[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(wallModeratorsListContainer);
            }

            // remove the user from the select list
            $("#SelectEligibleUsersListBox option[value='" + selectedUsers[i] + "']").remove();
        }
    }
}

function RemoveModerator(expertId) {
    $("#Moderator_" + expertId).remove();
}

function PopulateModeratorsHiddenField() {
    var wallModeratorsListContainer = $("#WallModeratorsList_Container");
    var selectedWallModeratorsField = $("#SelectedWallModerators_Field");
    var selectedWallModerators = "";

    wallModeratorsListContainer.children().each(function () {
        selectedWallModerators = selectedWallModerators + $(this).prop("id").replace("Moderator_", "") + ",";
    });

    if (selectedWallModerators.length > 0)
    { selectedWallModerators = selectedWallModerators.substring(0, selectedWallModerators.length - 1); }

    selectedWallModeratorsField.val(selectedWallModerators);
}