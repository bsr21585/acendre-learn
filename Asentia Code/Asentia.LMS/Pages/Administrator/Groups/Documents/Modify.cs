﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.Groups.Documents
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel GroupDocumentModifyContentWrapperContainer;
        public Panel GroupObjectMenuContainer;
        public Panel GroupDocumentModifyWrapperContainer;
        public Panel GroupDocumentPropertiesInstructionsPanel;
        public Panel GroupDocumentPropertiesContainer;
        public Panel GroupDocumentPropertiesActionsPanel;
        #endregion

        #region Private Properties
        private Group _GroupObject;
        private DocumentRepositoryItem _GroupDocumentObject;

        private bool _IsExistingGroupDocument = false;

        private UploaderAsync _GroupDocumentUploader;
        private TextBox _GroupDocumentLabel;
        private TextBox _GroupDocumentSearchTags;
        private DropDownList _FolderNameList;
        private LanguageSelector _GroupDocumentLanguage;
        private CheckBox _GroupDocumentIsAllLanguages;

        private Button _GroupDocumentPropertiesSaveButton;
        private Button _GroupDocumentPropertiesCancelButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Groups.Documents.Modify.js");
        }
        #endregion

        #region Page Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get the group document and group objects
            this._GetGroupDocumentAndGroupObjects();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            { Response.Redirect("/"); }

            // if group documents feature is not enabled, bounce user to group dashboard
            if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE))
            { Response.Redirect("~/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/documents/Modify.css");

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetGroupDocumentAndGroupObjects
        /// <summary>
        /// Gets a Group Document and Group objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetGroupDocumentAndGroupObjects()
        {
            // get the id querystring parameter
            int qsgid = this.QueryStringInt("gid", 0);
            int vsgid = this.ViewStateInt(this.ViewState, "gid", 0);
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            // get Group object - Group object MUST be specified and exist for this page to load
            if (qsgid > 0 || vsgid > 0)
            {
                int gid = 0;

                if (qsgid > 0)
                { gid = qsgid; }

                if (vsgid > 0)
                { gid = vsgid; }

                try
                {
                    if (gid > 0)
                    { this._GroupObject = new Group(gid); }
                }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
            else
            { Response.Redirect("~/administrator/groups"); }

            // get group document object (if exists)
            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    {
                        this._GroupDocumentObject = new DocumentRepositoryItem(id);

                        this._IsExistingGroupDocument = true;
                    }
                }
                catch
                { Response.Redirect("~/administrator/groups/Modify.aspx?id=" + this._GroupObject.Id.ToString()); }
            }
        }
        #endregion

        #region _BuildControls
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.GroupDocumentModifyContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.GroupDocumentModifyWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the group object menu            
            if (this._GroupObject != null)
            {
                GroupObjectMenu groupObjectMenu = new GroupObjectMenu(this._GroupObject);
                groupObjectMenu.SelectedItem = GroupObjectMenu.MenuObjectItem.Documents;

                this.GroupObjectMenuContainer.Controls.Add(groupObjectMenu);
            }

            // build the group document properties form
            this._BuildGroupDocumentPropertiesForm();

            // build the group document properties form actions panel
            this._BuildGroupDocumentPropertiesActionsPanel();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get group name information
            string groupNameInInterfaceLanguage = this._GroupObject.Name;
            string groupImagePath;
            string groupImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                }
            }

            if (this._GroupObject.Avatar != null)
            {
                groupImagePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                groupImageCssClass = "AvatarImage";
            }
            else
            {
                groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            }

            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;            
            string pageTitle;

            if (this._GroupDocumentObject != null)
            {
                // get group material name information
                string groupMaterialNameInInterfaceLanguage = this._GroupDocumentObject.Label;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (DocumentRepositoryItem.LanguageSpecificProperty groupDocumentLanguageSpecificProperty in this._GroupDocumentObject.LanguageSpecificProperties)
                    {
                        if (groupDocumentLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { groupMaterialNameInInterfaceLanguage = groupDocumentLanguageSpecificProperty.Label; }
                    }
                }

                breadCrumbPageTitle = groupMaterialNameInInterfaceLanguage;
                pageTitle = groupMaterialNameInInterfaceLanguage;
            }
            else
            {
                pageTitle = _GlobalResources.NewDocument;
                breadCrumbPageTitle = _GlobalResources.NewDocument;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups, "/administrator/groups"));
            breadCrumbLinks.Add(new BreadcrumbLink(groupNameInInterfaceLanguage, "/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Documents, "/administrator/groups/documents/Default.aspx?gid=" + this._GroupObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, groupNameInInterfaceLanguage, groupImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_MYACCOUNT, ImageFiles.EXT_PNG), groupImageCssClass);
        }
        #endregion

        #region _BuildGroupDocumentPropertiesForm
        /// <summary>
        /// Builds the Group Document Properties form.
        /// </summary>
        private void _BuildGroupDocumentPropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.GroupDocumentPropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfTheGroupDocument, true);

            // clear controls from container
            this.GroupDocumentPropertiesContainer.Controls.Clear();

            // group document label field
            this._GroupDocumentLabel = new TextBox();
            this._GroupDocumentLabel.ID = "GroupDocumentlLabel_Field";
            this._GroupDocumentLabel.CssClass = "InputLong";

            this.GroupDocumentPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("GroupDocumentlLabel",
                                                                                   _GlobalResources.Name,
                                                                                   this._GroupDocumentLabel.ID,
                                                                                   this._GroupDocumentLabel,
                                                                                   true,
                                                                                   true,
                                                                                   true));

            // file field
            List<Control> fileControls = new List<Control>();

            if (this._IsExistingGroupDocument)
            {
                Panel GroupDocumentLabelFieldContainer = new Panel();
                GroupDocumentLabelFieldContainer.ID = "LabelFieldContainer";

                Label GroupDocumentLabelField = new Label();
                GroupDocumentLabelField.Text = this._GroupDocumentObject.FileName;
                GroupDocumentLabelFieldContainer.Controls.Add(GroupDocumentLabelField);

                fileControls.Add(GroupDocumentLabelFieldContainer);
            }

            // do uploader
            this._GroupDocumentUploader = new UploaderAsync("GroupDocumentFile_Field", UploadType.GroupDocument, "GroupDocumentFile_ErrorContainer");
            fileControls.Add(this._GroupDocumentUploader);

            //Add file controls to properties panel
            this.GroupDocumentPropertiesContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("GroupDocumentFile",
                                                                                                       _GlobalResources.File,
                                                                                                       fileControls,
                                                                                                       false,
                                                                                                       true));

            // file size - only if existing document
            if (this._IsExistingGroupDocument)
            {                
                Label groupDocumentSizeFieldStaticValue = new Label();
                groupDocumentSizeFieldStaticValue.ID = "FileSizeField";
                groupDocumentSizeFieldStaticValue.Text = Asentia.Common.Utility.GetSizeStringFromKB(this._GroupDocumentObject.Kb);                

                this.GroupDocumentPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("GroupDocumentSize",
                                                                                       _GlobalResources.Size,
                                                                                       groupDocumentSizeFieldStaticValue.ID,
                                                                                       groupDocumentSizeFieldStaticValue,
                                                                                       false,
                                                                                       false,
                                                                                       false));
            }

            // folder name field
            this._FolderNameList = new DropDownList();
            this._FolderNameList.ID = "FolderNameList_Field";

            this.GroupDocumentPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("FolderName",
                                                                                   _GlobalResources.Folder,
                                                                                   this._FolderNameList.ID,
                                                                                   this._FolderNameList,
                                                                                   false,
                                                                                   false,
                                                                                   false));

            // document is all languages field
            this._GroupDocumentIsAllLanguages = new CheckBox();
            this._GroupDocumentIsAllLanguages.ID = "GroupDocumentIsAllLanguages_Field";
            this._GroupDocumentIsAllLanguages.Text = _GlobalResources.ThisDocumentIsForAllLanguagesOrIsNotLanguageSpecific;
            this._GroupDocumentIsAllLanguages.Checked = false;
            this._GroupDocumentIsAllLanguages.Attributes.Add("onClick", "LanguageSelectionClick(this)");

            this.GroupDocumentPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("GroupDocumentIsAllLanguages",
                                                                                   _GlobalResources.AllLanguages,
                                                                                   this._GroupDocumentIsAllLanguages.ID,
                                                                                   this._GroupDocumentIsAllLanguages,
                                                                                   false,
                                                                                   false,
                                                                                   false));

            // document language field
            this._GroupDocumentLanguage = new LanguageSelector(LanguageDropDownType.DataOnly);
            this._GroupDocumentLanguage.ID = "GroupDocumentLanguage_Field";
            this._GroupDocumentLanguage.SelectedValue = AsentiaSessionState.GlobalSiteObject.IdTimezone.ToString();

            this.GroupDocumentPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("GroupDocumentLanguage",
                                                                                   _GlobalResources.Language,
                                                                                   this._GroupDocumentLanguage.ID,
                                                                                   this._GroupDocumentLanguage,
                                                                                   false,
                                                                                   true,
                                                                                   false));

            // search tags field
            this._GroupDocumentSearchTags = new TextBox();
            this._GroupDocumentSearchTags.ID = "GroupDocumentSearchTags_Field";
            this._GroupDocumentSearchTags.Style.Add("width", "98%");
            this._GroupDocumentSearchTags.TextMode = TextBoxMode.MultiLine;
            this._GroupDocumentSearchTags.Rows = 5;

            this.GroupDocumentPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("GroupDocumentSearchTags",
                                                                                   _GlobalResources.SearchTags,
                                                                                   this._GroupDocumentSearchTags.ID,
                                                                                   this._GroupDocumentSearchTags,
                                                                                   false,
                                                                                   false,
                                                                                   true));

            // populate group document folder select list
            this._PopulateGroupDocumentRepositoryFolderSelectList();

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulateGroupDocumentPropertiesInputElements();
        }
        #endregion

        #region _BuildGroupDocumentPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for group document properties actions.
        /// </summary>
        private void _BuildGroupDocumentPropertiesActionsPanel()
        {
            // clear controls from container
            this.GroupDocumentPropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.GroupDocumentPropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._GroupDocumentPropertiesSaveButton = new Button();
            this._GroupDocumentPropertiesSaveButton.ID = "GroupDocumentSaveButton";
            this._GroupDocumentPropertiesSaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._GroupDocumentObject == null)
            { this._GroupDocumentPropertiesSaveButton.Text = _GlobalResources.CreateGroupDocument; }
            else
            { this._GroupDocumentPropertiesSaveButton.Text = _GlobalResources.SaveChanges; }

            this._GroupDocumentPropertiesSaveButton.Command += new CommandEventHandler(this._GroupDocumentPropertiesSaveButton_Command);
            this.GroupDocumentPropertiesActionsPanel.Controls.Add(this._GroupDocumentPropertiesSaveButton);

            // cancel button
            this._GroupDocumentPropertiesCancelButton = new Button();
            this._GroupDocumentPropertiesCancelButton.ID = "CancelButton";
            this._GroupDocumentPropertiesCancelButton.CssClass = "Button NonActionButton";
            this._GroupDocumentPropertiesCancelButton.Text = _GlobalResources.Cancel;
            this._GroupDocumentPropertiesCancelButton.Command += new CommandEventHandler(this._GroupDocumentPropertiesCancelButton_Command);
            this.GroupDocumentPropertiesActionsPanel.Controls.Add(this._GroupDocumentPropertiesCancelButton);
        }
        #endregion

        #region _PopulateGroupDocumentRepositoryFolderSelectList
        /// <summary>
        /// Populates the document repository folder select list.
        /// </summary>
        private void _PopulateGroupDocumentRepositoryFolderSelectList()
        {
            this._FolderNameList.Controls.Clear();

            //getting the documents repository folder list 
            DataTable dtFolderList = new DataTable();
            dtFolderList = DocumentRepositoryFolder.IdsAndNamesForDocumentRepositorySelectList(this._GroupObject.Id, Convert.ToInt32(DocumentRepositoryObjectType.Group));

            this._FolderNameList.DataSource = dtFolderList;
            this._FolderNameList.DataValueField = "idDocumentRepositoryFolder";
            this._FolderNameList.DataTextField = "name";
            this._FolderNameList.DataBind();
            this._FolderNameList.Items.Insert(0, "-- " + _GlobalResources.Select + " --");
        }
        #endregion

        #region _PopulateGroupPropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateGroupDocumentPropertiesInputElements()
        {
            if (this._GroupDocumentObject != null)
            {
                // label
                bool isDefaultPopulated = false;

                foreach (DocumentRepositoryItem.LanguageSpecificProperty driLanguageSpecificProperty in this._GroupDocumentObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (driLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._GroupDocumentLabel.Text = driLanguageSpecificProperty.Label;
                        this._GroupDocumentSearchTags.Text = driLanguageSpecificProperty.SearchTags;

                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificGroupDocumentLabelTextBox = (TextBox)this.GroupDocumentPropertiesContainer.FindControl(this._GroupDocumentLabel.ID + "_" + driLanguageSpecificProperty.LangString);
                        TextBox languageSpecificGroupDocumentSearchTagsTextBox = (TextBox)this.GroupDocumentPropertiesContainer.FindControl(this._GroupDocumentSearchTags.ID + "_" + driLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificGroupDocumentLabelTextBox != null)
                        { languageSpecificGroupDocumentLabelTextBox.Text = driLanguageSpecificProperty.Label; }

                        if (languageSpecificGroupDocumentSearchTagsTextBox != null)
                        { languageSpecificGroupDocumentSearchTagsTextBox.Text = driLanguageSpecificProperty.SearchTags; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._GroupDocumentLabel.Text = this._GroupDocumentObject.Label;
                    this._GroupDocumentSearchTags.Text = this._GroupDocumentObject.SearchTags;
                }

                // is all languages
                if (this._GroupDocumentObject.IsAllLanguages == true)
                { this._GroupDocumentIsAllLanguages.Checked = true; }
                else
                { this._GroupDocumentIsAllLanguages.Checked = false; }

                // document language
                if (this._GroupDocumentObject.LanguageString != null)
                { this._GroupDocumentLanguage.SelectedValue = this._GroupDocumentObject.LanguageString; }

                // document repository folder
                if (this._GroupDocumentObject.IdDocumentRepositoryFolder > 0)
                { this._FolderNameList.SelectedValue = this._GroupDocumentObject.IdDocumentRepositoryFolder.ToString(); }
            }
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;

            // LABEL - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._GroupDocumentLabel.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.GroupDocumentPropertiesContainer, "GroupDocumentLabel", _GlobalResources.Name + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // file
            if (!this._IsExistingGroupDocument && String.IsNullOrWhiteSpace(this._GroupDocumentUploader.SavedFilePath))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.GroupDocumentPropertiesContainer, "GroupDocumentFile", _GlobalResources.PleaseSelectAFileToUpload);
            }

            return isValid;
        }
        #endregion

        #region _GroupDocumentPropertiesSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for group document properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _GroupDocumentPropertiesSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                // if there is no Group Document object, create one
                if (this._GroupDocumentObject == null)
                { this._GroupDocumentObject = new DocumentRepositoryItem(); }

                int id;
                int gid = this._GroupObject.Id;

                // populate the object
                // set the object type, object id, and owner id
                this._GroupDocumentObject.IdDocumentRepositoryObjectType = DocumentRepositoryObjectType.Group;
                this._GroupDocumentObject.IdObject = this._GroupObject.Id;
                this._GroupDocumentObject.IdOwner = AsentiaSessionState.IdSiteUser;
                
                if (this._FolderNameList.SelectedIndex != 0)
                { this._GroupDocumentObject.IdDocumentRepositoryFolder = Convert.ToInt32(this._FolderNameList.SelectedValue); }

                this._GroupDocumentObject.IsPrivate = true;

                this._GroupDocumentObject.IsAllLanguages = this._GroupDocumentIsAllLanguages.Checked;

                if (!this._GroupDocumentIsAllLanguages.Checked)
                { this._GroupDocumentObject.LanguageString = this._GroupDocumentLanguage.SelectedValue; }
                else
                { this._GroupDocumentObject.LanguageString = null; }

                this._GroupDocumentObject.Label = this._GroupDocumentLabel.Text;
                this._GroupDocumentObject.SearchTags = this._GroupDocumentSearchTags.Text;

                // do file 
                if (this._GroupDocumentObject.Id > 0 && this._GroupDocumentUploader.SavedFilePath != null)
                {
                    // check user folder existence and create if necessary(this checking is not required for updating a course material as file is mandatory while saving first time)
                    string fullSavedFilePath = null;
                    fullSavedFilePath = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_GROUP + this._GroupObject.Id + "/";
                    if (File.Exists(Server.MapPath(fullSavedFilePath + this._GroupDocumentObject.FileName)))
                    {
                        // move the uploaded file into the course material's folder
                        File.Delete(Server.MapPath(fullSavedFilePath + this._GroupDocumentObject.FileName));
                    }
                    //check if file already exist with same name then apply incremented suffix tp its name
                    string fileOriginalname = string.Empty;
                    string updatedFilePath = DocumentRepositoryItem.GetCorrectFileName(fullSavedFilePath + this._GroupDocumentUploader.FileOriginalName, out fileOriginalname);

                    File.Copy(Server.MapPath(this._GroupDocumentUploader.SavedFilePath), Server.MapPath(updatedFilePath), true);
                    File.Delete(Server.MapPath(this._GroupDocumentUploader.SavedFilePath));

                    this._GroupDocumentObject.FileName = fileOriginalname;
                    this._GroupDocumentObject.Kb = Convert.ToInt32(this._GroupDocumentUploader.FileSize) / 1000;
                }
                else if (this._GroupDocumentUploader.SavedFilePath != null)
                {
                    // check user folder existence and create if necessary
                    if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_GROUP + this._GroupObject.Id)))
                    { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_GROUP + this._GroupObject.Id)); }

                    string fullSavedFilePath = null;

                    if (File.Exists(Server.MapPath(this._GroupDocumentUploader.SavedFilePath)))
                    {
                        fullSavedFilePath = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_GROUP + this._GroupObject.Id + "/" + this._GroupDocumentUploader.FileOriginalName;

                        //check if file already exist with same name then apply incremented suffix to its name
                        string fileOriginalname = string.Empty;
                        string updatedFilePath = DocumentRepositoryItem.GetCorrectFileName(fullSavedFilePath, out fileOriginalname);

                        // move the uploaded file into the course material's folder
                        File.Copy(Server.MapPath(this._GroupDocumentUploader.SavedFilePath), Server.MapPath(updatedFilePath), true);
                        File.Delete(Server.MapPath(this._GroupDocumentUploader.SavedFilePath));
                        this._GroupDocumentObject.FileName = fileOriginalname;
                        this._GroupDocumentObject.Kb = Convert.ToInt32(this._GroupDocumentUploader.FileSize) / 1000;
                    }
                    else
                    { this._GroupDocumentObject.FileName = null; }

                }

                // save the Group Document, save its returned id to viewstate, and 
                // instansiate a new Group Document object with the id
                id = this._GroupDocumentObject.Save();
                this.ViewState["id"] = id;
                this._GroupDocumentObject.Id = id;

                // do course material language-specific properties
                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string groupDocumentLabel = null;
                        string groupDocumentSearchTags = null;

                        // get text boxes
                        TextBox languageSpecificGroupDocumentLabelTextBox = (TextBox)this.GroupDocumentPropertiesContainer.FindControl(this._GroupDocumentLabel.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificGroupDocumentSearchTagsTextBox = (TextBox)this.GroupDocumentPropertiesContainer.FindControl(this._GroupDocumentSearchTags.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificGroupDocumentLabelTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificGroupDocumentLabelTextBox.Text))
                            { groupDocumentLabel = languageSpecificGroupDocumentLabelTextBox.Text; }
                        }

                        if (languageSpecificGroupDocumentSearchTagsTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificGroupDocumentSearchTagsTextBox.Text))
                            { groupDocumentSearchTags = languageSpecificGroupDocumentSearchTagsTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(groupDocumentLabel) ||
                            !String.IsNullOrWhiteSpace(groupDocumentSearchTags))
                        {
                            this._GroupDocumentObject.SaveLang(cultureInfo.Name,
                                                               groupDocumentLabel,
                                                               groupDocumentSearchTags);
                        }
                    }
                }

                // load the saved Group Document object
                this._GroupDocumentObject = new DocumentRepositoryItem(id);

                // set the existing Group Document flag
                this._IsExistingGroupDocument = true;

                // clear the group object menu container
                this.GroupObjectMenuContainer.Controls.Clear();

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.GroupDocumentHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _GroupDocumentPropertiesCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for Group Document Properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _GroupDocumentPropertiesCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/groups/documents/Default.aspx?gid=" + this._GroupObject.Id.ToString());
        }
        #endregion        
    }
}
