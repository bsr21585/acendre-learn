﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Groups.Documents
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel GroupDocumentsContentWrapperContainer;
        public Panel GroupObjectMenuContainer;
        public Panel GroupDocumentsWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel GroupDocumentGridUpdatePanel;
        public Grid GroupDocumentGrid;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private Group _GroupObject;

        private ModalPopup _GridConfirmAction;
        private LinkButton _DeleteButton;        
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get the group object
            this._GetGroupObject();

            // if this group discussion is not enabled, bounce back to the group dashboard
            if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE))
            { Response.Redirect("~/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id.ToString()); }

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/documents/Default.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.GroupDocumentsContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.GroupDocumentsWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the group object menu
            if (this._GroupObject != null)
            {
                GroupObjectMenu groupObjectMenu = new GroupObjectMenu(this._GroupObject);
                groupObjectMenu.SelectedItem = GroupObjectMenu.MenuObjectItem.Documents;

                this.GroupObjectMenuContainer.Controls.Add(groupObjectMenu);
            }

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.GroupDocumentGrid.BindData();
            }
        }
        #endregion

        #region _GetGroupObject
        /// <summary>
        /// Gets a group object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetGroupObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("gid", 0);

            if (qsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                try
                {
                    if (id > 0)
                    { this._GroupObject = new Group(id); }
                }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get group name information
            string groupNameInInterfaceLanguage = this._GroupObject.Name;
            string groupImagePath;
            string groupImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                }
            }

            if (this._GroupObject.Avatar != null)
            {
                groupImagePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                groupImageCssClass = "AvatarImage";
            }
            else
            {
                groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            }            

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups, "/administrator/groups"));
            breadCrumbLinks.Add(new BreadcrumbLink(groupNameInInterfaceLanguage, "/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Documents));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, groupNameInInterfaceLanguage, groupImagePath, _GlobalResources.Documents, ImageFiles.GetIconPath(ImageFiles.ICON_MYACCOUNT, ImageFiles.EXT_PNG), groupImageCssClass);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD GROUP DOCUMENT REPOSITORY FOLDER
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddDocumentRepositoryFolderLink",
                                                null,
                                                "/administrator/groups/documentrepository/Default.aspx?gid=" + this._GroupObject.Id.ToString(),
                                                null,
                                                _GlobalResources.DocumentFolders,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_FOLDER, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_FOLDER, ImageFiles.EXT_PNG))
                );

            // ADD GROUP DOCUMENT
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddDocumentLink",
                                                null,
                                                "Modify.aspx?gid=" + this._GroupObject.Id.ToString(),
                                                null,
                                                _GlobalResources.NewGroupDocument,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_MYACCOUNT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.GroupDocumentGrid.StoredProcedure = DocumentRepositoryItem.GridProcedure;
            this.GroupDocumentGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.GroupDocumentGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.GroupDocumentGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.GroupDocumentGrid.AddFilter("@idDocumentRepositoryObjectType", SqlDbType.Int, 4, DocumentRepositoryObjectType.Group);
            this.GroupDocumentGrid.AddFilter("@idObject", SqlDbType.Int, 4, this._GroupObject.Id);
            this.GroupDocumentGrid.IdentifierField = "idDocumentRepositoryItem";
            this.GroupDocumentGrid.DefaultSortColumn = "name";

            // data key names
            this.GroupDocumentGrid.DataKeyNames = new string[] { "idDocumentRepositoryItem" };

            // columns
            GridColumn nameFileSize = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.File + " (" + _GlobalResources.Size + ")", null, "label"); // this is calculated dynamically in the RowDataBound method

            GridColumn folder = new GridColumn(_GlobalResources.Folder, null, "name"); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.GroupDocumentGrid.AddColumn(nameFileSize);
            this.GroupDocumentGrid.AddColumn(folder);

            // add row data bound event
            this.GroupDocumentGrid.RowDataBound += new GridViewRowEventHandler(this._GroupDocumentGrid_RowDataBound);
        }
        #endregion

        #region _GroupDocumentGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the group document grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _GroupDocumentGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idDocumentRepositoryItem = Convert.ToInt32(rowView["idDocumentRepositoryItem"]);                

                // AVATAR, TITLE, FILE (SIZE) COLUMN

                string title = Server.HtmlEncode(rowView["label"].ToString());
                string fileName = Server.HtmlEncode(rowView["fileName"].ToString());
                string folderName = Server.HtmlEncode(rowView["name"].ToString());
                string kb = Server.HtmlEncode(rowView["kb"].ToString());                

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_DOCUMENT, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = title;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // title
                Label titleLabel = new Label();
                titleLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(titleLabel);

                HyperLink titleLink = new HyperLink();
                titleLink.NavigateUrl = "Modify.aspx?gid=" + this._GroupObject.Id.ToString() + "&id=" + idDocumentRepositoryItem.ToString();
                titleLink.Text = title;
                titleLabel.Controls.Add(titleLink);

                // file name
                Label fileNameLabel = new Label();
                fileNameLabel.CssClass = "GridSecondaryLine";
                e.Row.Cells[1].Controls.Add(fileNameLabel);

                HyperLink documentLink = new HyperLink();
                documentLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_GROUP + this._GroupObject.Id.ToString() + "/" + fileName;
                documentLink.Target = "_blank";
                documentLink.Text = fileName;
                fileNameLabel.Controls.Add(documentLink);

                Literal documentSize = new Literal();
                //documentSize.Text = " (" + Asentia.Common.Utility.GetSizeStringFromKB(kb) + ")";
                documentSize.Text = " (" + kb + ")";
                fileNameLabel.Controls.Add(documentSize);

                // FOLDER COLUMN 

                // folder name
                Label folderNameLabel = new Label();
                folderNameLabel.CssClass = "GridSecondaryTitle";
                folderNameLabel.Text = folderName;
                e.Row.Cells[2].Controls.Add(folderNameLabel);
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedDocument_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmActionModal");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedDocument_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseDocument_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.GroupDocumentGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.GroupDocumentGrid.Rows[i].FindControl(this.GroupDocumentGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {                    
                    DocumentRepositoryItem.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedDocument_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message                    
                    this.DisplayFeedback(_GlobalResources.NoDocument_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.GroupDocumentGrid.BindData();
            }
        }
        #endregion
    }
}
