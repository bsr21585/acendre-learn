﻿$(document).ready(function () {
    // on page load
    LanguageSelectionClick(document.getElementById("GroupDocumentIsAllLanguages_Field"));
});

function LanguageSelectionClick(ele) {
    if (ele.checked == true) {
        $("#GroupDocumentLanguage_Field").attr("disabled", "disabled")
    }
    else {
        $("#GroupDocumentLanguage_Field").removeAttr("disabled");
    }
}