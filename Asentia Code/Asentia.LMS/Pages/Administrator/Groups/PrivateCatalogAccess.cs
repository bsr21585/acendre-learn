﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Groups
{
    public class PrivateCatalogAccess : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel PrivateCatalogAccessContentWrapperContainer;
        public Panel GroupObjectMenuContainer;
        public Panel PrivateCatalogAccessWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel PrivateCatalogAccessContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private Group _GroupObject;
        private Library.Catalog _CatalogObject;

        private Panel _MainPanel;
        private Panel _CatalogTreePanel;
        private UpdatePanel _CatalogTreeUpdatePanel;

        private Button _SaveButton;
        private Button _CancelButton;
     
        private HiddenField _CheckBoxPrivateAccess;
        private HtmlGenericControl _CatalogHtml;      
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(PrivateCatalogAccess), "Asentia.LMS.Pages.Administrator.Groups.PrivateCatalogAccess.js");            
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the group object
            this._GetGroupObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/PrivateCatalogAccess.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // Initialize the administrator menu
            this.InitializeAdminMenu();

            this.PrivateCatalogAccessContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.PrivateCatalogAccessWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the group object menu
            if (this._GroupObject != null)
            {
                GroupObjectMenu groupObjectMenu = new GroupObjectMenu(this._GroupObject);
                groupObjectMenu.SelectedItem = GroupObjectMenu.MenuObjectItem.PrivateCatalogAccess;

                this.GroupObjectMenuContainer.Controls.Add(groupObjectMenu);
            }

            // Initialize the private properties
            this._IntializePrivateProperties();

            // Build the child controls
            this._BuildChildControls();

            // Build Actions panel
            this._BuildActionsPanel();

            // Build the catalog menu
            this._UpdateCourseCatalogs();
        }
        #endregion

        #region _GetGroupObject
        /// <summary>
        /// Gets a group object based on querystring if exists, redirects if not.
        /// </summary>
        private void _GetGroupObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("gid", 0);

            if (qsId > 0)
            {
                try
                { this._GroupObject = new Group(qsId); }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
            else
            { Response.Redirect("~/administrator/groups"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get group name information
            string groupNameInInterfaceLanguage = this._GroupObject.Name;
            string groupImagePath;
            string groupImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                }
            }

            if (this._GroupObject.Avatar != null)
            {
                groupImagePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                groupImageCssClass = "AvatarImage";
            }
            else
            {
                groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            }            

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups, "/administrator/groups"));
            breadCrumbLinks.Add(new BreadcrumbLink(groupNameInInterfaceLanguage, "/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.PrivateCatalogAccess));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, groupNameInInterfaceLanguage, groupImagePath, _GlobalResources.PrivateCatalogAccess, ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG_PRIVATE, ImageFiles.EXT_PNG), groupImageCssClass);
        }
        #endregion

        #region _IntializePrivateProperties
        /// <summary>
        /// Initializes the private properties
        /// </summary>
        private void _IntializePrivateProperties()
        {
            this._CatalogTreeUpdatePanel = new UpdatePanel();
            this._CatalogTreePanel = new Panel();
            this._MainPanel = new Panel();
            this._CatalogHtml = new HtmlGenericControl("div");
            this._CatalogObject = new Library.Catalog();
            this._CheckBoxPrivateAccess = new HiddenField();
            this._MainPanel.Controls.Add(this._CheckBoxPrivateAccess);
            this._CheckBoxPrivateAccess.ID = "CheckBoxPrivateAccess";
        }
        #endregion

        #region _UpdateCourseCatalogs
        /// <summary>
        /// Updates Course Catalogs updatepanel.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _UpdateCourseCatalogs()
        {
            this._CatalogHtml.InnerHtml = Convert.ToString(this._GetHtmlOfCatalogsAndCourses());
        }
        #endregion

        #region _GetHtmlOfCatalogsAndCourses
        /// <summary>
        /// Gets the html of Catalogs and Courses to draw in the tree
        /// </summary>
        /// <param name="output">HtmlTextWriter</param>
        private StringBuilder _GetHtmlOfCatalogsAndCourses()
        {
            DataTable catalogsDataTable = this._CatalogObject.GetParentsForGroup(AsentiaSessionState.UserCulture, this._GroupObject.Id);

            string catalogImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG, ImageFiles.EXT_PNG);
            string privateCatalogImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG_PRIVATE, ImageFiles.EXT_PNG);

            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='catalogsSection'>");
            sb.Append("<div class='catalogs' cid='" + 0 + "'>");
            sb.Append("<div class='catalog parentCatalog'><input type='checkbox' id= 'All_chb_0'>" + _GlobalResources.AllCatalogs + "");
            if (catalogsDataTable.Rows.Count > 0)
            {
                foreach (DataRow catalogDataRow in catalogsDataTable.Rows)
                {
                    int idParent = 0;
                    if (catalogDataRow["idParent"] != DBNull.Value)
                    {
                        idParent = Convert.ToInt32(catalogDataRow["idParent"]);
                    }
                    sb.Append(this._GetCatalogsAndInternalCourses(Convert.ToInt32(catalogDataRow["idCatalog"]), Convert.ToString(catalogDataRow["title"]), idParent, true, Convert.ToInt32(catalogDataRow["catalogCount"]), Convert.ToInt32(catalogDataRow["courseCount"]), Convert.ToBoolean(catalogDataRow["isPrivateAfterJoin"]), Convert.ToBoolean(catalogDataRow["isPrivate"]), catalogImagePath, privateCatalogImagePath));
                }
            }
            sb.Append("</div></div>");
            sb.Append("</div>");
            return sb;
        }
        #endregion

        #region _GetCatalogsAndInternalCourses
        /// <summary>
        /// Gets Catalogs and their internal Courses
        /// </summary>
        /// <param name="idCatalog">int</param>
        /// <param name="catalogTitle">string</param>
        /// <param name="idParent">int</param>
        /// <param name="isParentCatalog">bool</param>
        private StringBuilder _GetCatalogsAndInternalCourses(int idCatalog, string catalogTitle, int idParent, bool isParentCatalog, int catalogCount, int courseCount, Boolean isPrivateAfterJoin, Boolean isPrivate, string catalogImagePath, string privateCatalogImagePath)
        {
            bool isInnerParent = false;
            string display = String.Empty;
            string parentCatalog = String.Empty;
            string innerParent = String.Empty;
            int internalIdParent = 0;
            DataTable childCatalogsDataTable;
            StringBuilder catalogsListing = new StringBuilder();
            StringBuilder internalCatalog = new StringBuilder();

            if (!isParentCatalog)
            {
                display = "display:none;";
            }
            else
            {
                parentCatalog = "parentCatalog";
            }

            if (parentCatalog == String.Empty)
            {
                innerParent = "innerParent";
            }

            string privateCatalog = string.Empty;

            if (isPrivate == false)
            {
                display += "color: gray;";

            }
            else
            {
                display += "color: black;";
                privateCatalog = "privateCatalog";
            }
            catalogsListing.Append("<div id = chbDiv_" + idCatalog + "  idparent='" + idParent + "' cid='" + idCatalog + "' class='catalog " + parentCatalog + "" + innerParent + " " + privateCatalog + "' style='" + display + ";'>");

            if (isPrivate == isPrivateAfterJoin && isPrivateAfterJoin == true) { catalogsListing.Append("<input type='checkbox' id= Private_chb_" + idCatalog + ">"); }
            else if (isPrivate != isPrivateAfterJoin && isPrivateAfterJoin == false) { catalogsListing.Append("<input type='checkbox' id= Private_chb_" + idCatalog + " checked  >"); }
            else { catalogsListing.Append("<input type='checkbox' id= Public_chb_" + idCatalog + " disabled checked  onkeydown='return false;' title='" + _GlobalResources.CannotUncheckPublicCatalog + "' onclick= 'return false;'>"); }

            if (privateCatalog == "privateCatalog") { catalogsListing.Append("<img src='" + privateCatalogImagePath + "' class='MediumIcon'>"); }
            else { catalogsListing.Append("<img src='" + catalogImagePath + "' class='MediumIcon'>"); }

            catalogsListing.Append(catalogTitle);

            if (catalogCount > 0)
            {
                childCatalogsDataTable = this._CatalogObject.GetChildrenForGroup(idCatalog, AsentiaSessionState.UserCulture, this._GroupObject.Id);
                isInnerParent = true;

                if (isInnerParent)
                {
                    if (childCatalogsDataTable.Rows.Count > 0)
                    {
                        foreach (DataRow childCatalogDataRow in childCatalogsDataTable.Rows)
                        {
                            internalIdParent = 0;
                            if (childCatalogDataRow["idParent"] != DBNull.Value)
                            {
                                internalIdParent = Convert.ToInt32(childCatalogDataRow["idParent"]);
                            }
                            internalCatalog.Append(this._GetCatalogsAndInternalCourses(Convert.ToInt32(childCatalogDataRow["idCatalog"]), Convert.ToString(childCatalogDataRow["title"]), internalIdParent, false, Convert.ToInt32(childCatalogDataRow["catalogCount"]), Convert.ToInt32(childCatalogDataRow["courseCount"]), Convert.ToBoolean(childCatalogDataRow["isPrivateAfterJoin"]), Convert.ToBoolean(childCatalogDataRow["isPrivate"]), catalogImagePath, privateCatalogImagePath));
                        }
                    }
                }
                catalogsListing.Append(internalCatalog);
            }
            catalogsListing.Append("</div>");
            return catalogsListing;
        }
        #endregion

        #region _BuildChildControls
        /// <summary>
        /// Builds the child controls
        /// </summary>        
        /// <returns></returns>
        private void _BuildChildControls()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.CheckTheBoxesNextToThePrivateCatalogsThatUsersWithinThisGroupShouldHaveAccessTo, true);

            // clear controls from container
            this.PrivateCatalogAccessContainer.Controls.Clear();

            this._CatalogTreePanel.ID = "CatalogTreePanel";
            this._CatalogTreePanel.CssClass = "AdminCatalogTreePanel";

            this._MainPanel.ID = "CatalogTree";
            this._MainPanel.Attributes.Add("class", "CatalogTree");

            this._MainPanel.Controls.Add(this._CatalogHtml);

            this._CatalogTreeUpdatePanel.ID = "updatePanel";

            this._CatalogTreeUpdatePanel.ContentTemplateContainer.Controls.Add(this._MainPanel);
            this._CatalogTreeUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            this._CatalogTreePanel.Controls.Add(this._CatalogTreeUpdatePanel);

            // attach the user membership listing to the container
            this.PrivateCatalogAccessContainer.Controls.Add(this._CatalogTreePanel);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.OnClientClick = "return SaveListToHiddenFeild();";
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the event initiated by the Save Changes Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _SaveButton_Command(object sender, EventArgs e)
        {
            try
            {
                string accessiblePrivateCatalogIds = this._CheckBoxPrivateAccess.Value;
                string[] catalogIds = accessiblePrivateCatalogIds.Split(',');
                DataTable privateCatalogsToAdd = new DataTable(); ;
                privateCatalogsToAdd.Columns.Add("id", typeof(int));

                foreach (string id in catalogIds)
                {
                    if (!String.IsNullOrWhiteSpace(id)) { privateCatalogsToAdd.Rows.Add(Convert.ToInt32(id)); }
                }

                // join the selected roles to the group
                this._GroupObject.JoinCatalogs(privateCatalogsToAdd);
                this._UpdateCourseCatalogs();
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.PrivateCatalogAccessForThisGroupHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, ex.Message, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the event initiated by the Cancel Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CancelButton_Command(object sender, EventArgs e)
        {
            Response.Redirect("~/administrator/groups/Modify.aspx?id=" + this._GroupObject.Id.ToString());
        }
        #endregion
    }
}
