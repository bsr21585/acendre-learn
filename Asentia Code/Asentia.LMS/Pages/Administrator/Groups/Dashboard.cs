﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Groups
{
    public class Dashboard : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel GroupDashboardFormContentWrapperContainer;
        public Panel GroupObjectMenuContainer;
        public Panel GroupDashboardWrapperContainer;
        public Panel GroupDashboardInstructionsPanel;
        public Panel GroupDashboardFeedbackContainer;
        #endregion

        #region Private Properties
        private Group _GroupObject;
        private EcommerceSettings _EcommerceSettings;
        private ObjectDashboard _GroupObjectDashboard;        
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            //csm.RegisterClientScriptResource(typeof(Dashboard), "Asentia.LMS.Pages.Administrator.Groups.Dashboard.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the group object
            this._GetGroupObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("ObjectDashboard.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/Dashboard.css");            

            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetGroupObject
        /// <summary>
        /// Gets a group object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetGroupObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._GroupObject = new Group(id); }
                }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
            else { Response.Redirect("~/administrator/groups"); }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // set container classes
            this.GroupDashboardFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.GroupDashboardWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the group object menu
            if (this._GroupObject != null)
            {
                GroupObjectMenu groupObjectMenu = new GroupObjectMenu(this._GroupObject);
                groupObjectMenu.SelectedItem = GroupObjectMenu.MenuObjectItem.GroupDashboard;

                this.GroupObjectMenuContainer.Controls.Add(groupObjectMenu);
            }            

            // build the course object dashboard
            this._GroupObjectDashboard = new ObjectDashboard("GroupObjectDashboard");

            this._BuildInformationWidget();
            this._BuildActionsWidget();
            this._BuildCourseEnrollmentsWidget();
            this._BuildLearningPathEnrollmentsWidget();

            // attach object widget to wrapper container
            this.GroupDashboardWrapperContainer.Controls.Add(this._GroupObjectDashboard);
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get group name information
            string groupNameInInterfaceLanguage = this._GroupObject.Name;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                }
            }

            // get group avatar information
            string groupImagePath;
            string imageCssClass = null;

            if (this._GroupObject.Avatar != null)
            {
                groupImagePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                imageCssClass = "AvatarImage";
            }
            else
            {
                groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD, ImageFiles.EXT_PNG);
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups, "/administrator/groups"));
            breadCrumbLinks.Add(new BreadcrumbLink(groupNameInInterfaceLanguage));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, groupNameInInterfaceLanguage, groupImagePath, imageCssClass);
        }
        #endregion

        #region _BuildInformationWidget
        /// <summary>
        /// Builds the information widget.
        /// </summary>
        private void _BuildInformationWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> informationWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // NUMBER OF MEMBERS
            string numberOfMembers = this._GroupObject.NumberOfMembers.ToString();
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("NumberOfMembers", _GlobalResources.Members + ":", numberOfMembers, ObjectDashboard.WidgetItemType.Text));

            // SELF-JOIN ALLOWED
            string selfJoinAllowedMessage = (this._GroupObject.IsSelfJoinAllowed == true) ? _GlobalResources.Yes : _GlobalResources.No;
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("SelfJoinAllowed", _GlobalResources.SelfJoinAllowed + ":", selfJoinAllowedMessage, ObjectDashboard.WidgetItemType.Text));

            // add the widget
            this._GroupObjectDashboard.AddWidget("InformationWidget", _GlobalResources.Information, informationWidgetItems);
        }
        #endregion

        #region _BuildActionsWidget
        /// <summary>
        /// Builds the actions widget.
        /// </summary>
        private void _BuildActionsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> actionsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // NEW DOCUMENT LINK - Only if group documents feature is enabled

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE))
            {
                Panel addDocumentLinkWrapper = new Panel();

                HyperLink addDocumentLink = new HyperLink();
                addDocumentLink.NavigateUrl = "documents/Modify.aspx?gid=" + this._GroupObject.Id.ToString();
                addDocumentLinkWrapper.Controls.Add(addDocumentLink);

                Image addDocumentLinkImage = new Image();
                addDocumentLinkImage.CssClass = "SmallIcon";
                addDocumentLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DOCUMENT, ImageFiles.EXT_PNG);
                addDocumentLink.Controls.Add(addDocumentLinkImage);

                Image addDocumentOverlayLinkImage = new Image();
                addDocumentOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
                addDocumentOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
                addDocumentLink.Controls.Add(addDocumentOverlayLinkImage);

                Literal addDocumentLinkText = new Literal();
                addDocumentLinkText.Text = _GlobalResources.AddAGroupDocument;
                addDocumentLink.Controls.Add(addDocumentLinkText);

                actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("AddGroupDocument", null, addDocumentLinkWrapper, ObjectDashboard.WidgetItemType.Link));
            }

            // NEW AUTO-JOIN RULE

            Panel createAutoJoinRuleLinkWrapper = new Panel();

            HyperLink createAutoJoinRuleLink = new HyperLink();
            createAutoJoinRuleLink.NavigateUrl = "AutoJoinRules.aspx?gid=" + this._GroupObject.Id.ToString() + "&action=NewRuleset";
            createAutoJoinRuleLinkWrapper.Controls.Add(createAutoJoinRuleLink);

            Image createAutoJoinRuleLinkImage = new Image();
            createAutoJoinRuleLinkImage.CssClass = "SmallIcon";
            createAutoJoinRuleLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG);
            createAutoJoinRuleLink.Controls.Add(createAutoJoinRuleLinkImage);

            Image createAutoJoinRuleOverlayLinkImage = new Image();
            createAutoJoinRuleOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
            createAutoJoinRuleOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            createAutoJoinRuleLink.Controls.Add(createAutoJoinRuleOverlayLinkImage);

            Literal createAutoJoinRuleLinkText = new Literal();
            createAutoJoinRuleLinkText.Text = _GlobalResources.CreateANewAutoJoinRule;
            createAutoJoinRuleLink.Controls.Add(createAutoJoinRuleLinkText);

            actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateAutoJoinRule", null, createAutoJoinRuleLinkWrapper, ObjectDashboard.WidgetItemType.Link));

            // add the widget
            this._GroupObjectDashboard.AddWidget("ActionsWidget", _GlobalResources.Actions, actionsWidgetItems);
        }
        #endregion        

        #region _BuildCourseEnrollmentsWidget
        /// <summary>
        /// Builds the course enrollments widget.
        /// </summary>
        private void _BuildCourseEnrollmentsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> courseEnrollmentsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the data and build a chart
            DataTable courseEnrollmentsStatusDt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            Chart courseEnrollmentsPieChart;

            // chart colors
            List<string> courseEnrollmentsPieChartColors = new List<string>();
            courseEnrollmentsPieChartColors.Add("#2E7DBD");
            courseEnrollmentsPieChartColors.Add("#55BD86");
            courseEnrollmentsPieChartColors.Add("#FED155");
            courseEnrollmentsPieChartColors.Add("#EC6E61");

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idGroup", this._GroupObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Group.CourseEnrollmentStatistics]", true);

                // load recordset
                courseEnrollmentsStatusDt.Load(sdr);

                sdr.Close();

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // draw the chart
                courseEnrollmentsPieChart = new Chart("CourseEnrollmentsPieChart", ChartType.Doughnut, courseEnrollmentsStatusDt, courseEnrollmentsPieChartColors, _GlobalResources.CourseEnrollments, null);
                courseEnrollmentsPieChart.ShowTotalInTitle = true;
                courseEnrollmentsPieChart.ShowTotalAndPercentageInLegend = true;
                courseEnrollmentsPieChart.IsResponsive = false;
                courseEnrollmentsPieChart.CanvasWidth = 125;
                courseEnrollmentsPieChart.CanvasHeight = 125;
            }
            catch // on errors, just draw an empty chart
            {
                // draw an empty chart
                DataTable emptyDataTable = new DataTable();
                emptyDataTable.Columns.Add("_Total_", typeof(int));
                emptyDataTable.Rows.Add(0);

                courseEnrollmentsPieChart = new Chart("CourseEnrollmentsPieChart", ChartType.Doughnut, emptyDataTable, courseEnrollmentsPieChartColors, _GlobalResources.CourseEnrollments, null);
                courseEnrollmentsPieChart.ShowTotalInTitle = true;
                courseEnrollmentsPieChart.ShowTotalAndPercentageInLegend = false;
                courseEnrollmentsPieChart.IsResponsive = false;
                courseEnrollmentsPieChart.CanvasWidth = 125;
                courseEnrollmentsPieChart.CanvasHeight = 125;
            }
            finally
            { databaseObject.Dispose(); }

            // add the chart to the widget item list
            courseEnrollmentsWidgetItems.Add(new ObjectDashboard.WidgetItem("CourseEnrollmentStats", null, courseEnrollmentsPieChart, ObjectDashboard.WidgetItemType.Object));

            // add the widget
            this._GroupObjectDashboard.AddWidget("CourseEnrollmentsWidget", _GlobalResources.CourseEnrollments, courseEnrollmentsWidgetItems);
        }
        #endregion

        #region _BuildLearningPathEnrollmentsWidget
        /// <summary>
        /// Builds the learning path enrollments widget.
        /// </summary>
        private void _BuildLearningPathEnrollmentsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> learningPathEnrollmentsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the data and build a chart
            DataTable learningPathEnrollmentsStatusDt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            Chart learningPathEnrollmentsPieChart;

            // chart colors
            List<string> learningPathEnrollmentsPieChartColors = new List<string>();
            learningPathEnrollmentsPieChartColors.Add("#A6729B");
            learningPathEnrollmentsPieChartColors.Add("#45848C");
            learningPathEnrollmentsPieChartColors.Add("#A69B72");
            learningPathEnrollmentsPieChartColors.Add("#F17B85");

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idGroup", this._GroupObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Group.LearningPathEnrollmentStatistics]", true);

                // load recordset
                learningPathEnrollmentsStatusDt.Load(sdr);

                sdr.Close();

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // draw the chart
                learningPathEnrollmentsPieChart = new Chart("LearningPathEnrollmentsPieChart", ChartType.Doughnut, learningPathEnrollmentsStatusDt, learningPathEnrollmentsPieChartColors, _GlobalResources.LearningPathEnrollments, null);
                learningPathEnrollmentsPieChart.ShowTotalInTitle = true;
                learningPathEnrollmentsPieChart.ShowTotalAndPercentageInLegend = true;
                learningPathEnrollmentsPieChart.IsResponsive = false;
                learningPathEnrollmentsPieChart.CanvasWidth = 125;
                learningPathEnrollmentsPieChart.CanvasHeight = 125;
            }
            catch // on errors, just draw an empty chart
            {
                // draw an empty chart
                DataTable emptyDataTable = new DataTable();
                emptyDataTable.Columns.Add("_Total_", typeof(int));
                emptyDataTable.Rows.Add(0);

                learningPathEnrollmentsPieChart = new Chart("LearningPathEnrollmentsPieChart", ChartType.Doughnut, emptyDataTable, learningPathEnrollmentsPieChartColors, _GlobalResources.LearningPathEnrollments, null);
                learningPathEnrollmentsPieChart.ShowTotalInTitle = true;
                learningPathEnrollmentsPieChart.ShowTotalAndPercentageInLegend = false;
                learningPathEnrollmentsPieChart.IsResponsive = false;
                learningPathEnrollmentsPieChart.CanvasWidth = 125;
                learningPathEnrollmentsPieChart.CanvasHeight = 125;
            }
            finally
            { databaseObject.Dispose(); }

            // add the chart to the widget item list
            learningPathEnrollmentsWidgetItems.Add(new ObjectDashboard.WidgetItem("LearningPathEnrollmentStats", null, learningPathEnrollmentsPieChart, ObjectDashboard.WidgetItemType.Object));

            // add the widget
            this._GroupObjectDashboard.AddWidget("LearningPathEnrollmentsWidget", _GlobalResources.LearningPathEnrollments, learningPathEnrollmentsWidgetItems);
        }
        #endregion
    }
}
