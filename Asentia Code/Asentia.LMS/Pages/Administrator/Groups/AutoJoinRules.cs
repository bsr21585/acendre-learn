﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.Groups
{
    public class AutoJoinRules : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel AutoJoinRulesFormContentWrapperContainer;
        public Panel GroupObjectMenuContainer;
        public Panel AutoJoinRulesPageWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel RuleSetGroupGridUpdatePanel;
        public Grid RuleSetGroupGrid;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private HiddenField _PageAction;
        private HiddenField _ModifyRulesetId;

        private Group _GroupObject;
        private RuleSet _RuleSetObject;
        private AutoJoinRuleSet _AutoJoinRuleSet;
        private bool _RuleSetControlLoaded;

        // MODIFY RULESET MODAL

        private TextBox _LabelTextbox;

        private RadioButton _MatchAny;
        private RadioButton _MatchAll;

        private Button _SubmitButtonMadal;
        private Button _CloseButtonMadal;
        private Button _AddModifyRuleSetModalButton;

        private DataTable _Rules;

        private HiddenField _RulesData;
        private HiddenField _IdGroupHiddenField;
        private HiddenField _IdRulesetModalHidden;

        private LinkButton _AddModifyRuleSetHiddenButton;
        private LinkButton _DeleteButton;

        private ModalPopup _GridConfirmAction;
        private ModalPopup _AddModifyRuleSetModal;

        private PlaceHolder _RuleSetControlPlaceHolder;
        private Panel _RuleSetModalPropertiesContainer;
        #endregion

        #region Page Init Event
        /// <summary>
        /// Page init event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Init(object sender, EventArgs e)
        {
            this._LabelTextbox = new TextBox();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(AutoJoinRules), "Asentia.LMS.Pages.Administrator.Groups.AutoJoinRules.js");

            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");

            // javascript for page action querystring, i.e. New Ruleset
            if (this._PageAction.Value == "NewRuleset")
            {
                multipleStartUpCallsScript.AppendLine("");
                multipleStartUpCallsScript.AppendLine(" if ($(\"#PageAction\").val() == \"NewRuleset\") {");
                multipleStartUpCallsScript.AppendLine("     $(\"#PageAction\").val(\"\");");
                multipleStartUpCallsScript.AppendLine("     OnRuleSetModifyClick(0);");
                multipleStartUpCallsScript.AppendLine(" }");
            }
            else if (this._PageAction.Value == "ModifyRuleset" &&
                     this._ModifyRulesetId.Value != String.Empty)
            {
                multipleStartUpCallsScript.AppendLine("");
                multipleStartUpCallsScript.AppendLine(" if ($(\"#PageAction\").val() == \"ModifyRuleset\") {");
                multipleStartUpCallsScript.AppendLine("     $(\"#PageAction\").val(\"\");");
                multipleStartUpCallsScript.AppendLine("     OnRuleSetModifyClick(" + this._ModifyRulesetId.Value + ");");
                multipleStartUpCallsScript.AppendLine(" }");
            }

            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine("});");
            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine("var ModifyRuleSetModalButton = \"" + this._AddModifyRuleSetModalButton.ClientID + "\";");
            multipleStartUpCallsScript.AppendLine("var AddRuleSetModalHeader = \"" + _GlobalResources.NewRuleset + "\";");
            multipleStartUpCallsScript.AppendLine("var RuleSetLabel_Field = \"" + this._LabelTextbox.ID + "\";");
            multipleStartUpCallsScript.AppendLine("var IdRulesetModalHidden = \"" + this._IdRulesetModalHidden.ClientID + "\";");

            csm.RegisterStartupScript(typeof(AutoJoinRules), "Page_Load", multipleStartUpCallsScript.ToString(), true);

            if (!this._RuleSetControlLoaded)
            {
                if (this._RuleSetObject == null)
                {
                    this._AutoJoinRuleSet = new AutoJoinRuleSet();
                }
                else
                {
                    this._AutoJoinRuleSet = new AutoJoinRuleSet(this._RuleSetObject.Id);
                }

                this._AutoJoinRuleSet.ID = "Rule_Field";
                this._AutoJoinRuleSet.ClientIDMode = ClientIDMode.Static;

                this._RuleSetControlPlaceHolder.Controls.Clear();
                this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);
            }
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the group object
            this._GetGroupObject();

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager, this._GroupObject.Id))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/groups/AutoJoinRules.css");

            this._RulesData = new HiddenField();
            this._RulesData.ID = "RulesData";

            this._RuleSetControlPlaceHolder = new PlaceHolder();
            this._RuleSetControlPlaceHolder.ID = "RuleSetControlPlaceHolder";

            this._RuleSetControlLoaded = false;

            // get the rule set object
            this._GetRuleSetObject();

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.AutoJoinRulesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.AutoJoinRulesPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the group object menu
            if (this._GroupObject != null)
            {
                GroupObjectMenu groupObjectMenu = new GroupObjectMenu(this._GroupObject);
                groupObjectMenu.SelectedItem = GroupObjectMenu.MenuObjectItem.AutoJoinRules;

                this.GroupObjectMenuContainer.Controls.Add(groupObjectMenu);
            }

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.RuleSetGroupGrid.BindData();
            }
        }
        #endregion        

        #region _BuildAddModifyRuleSetModal
        /// <summary>
        /// Builds the modal popup for adding/modifying rulesets.
        /// </summary>
        /// <param name="targetControlId"></param>
        /// <param name="modalContainer"></param>
        private void _BuildAddModifyRuleSetModal(string targetControlId, Panel modalContainer)
        {
            EnsureChildControls();

            this._AddModifyRuleSetModal = new ModalPopup("AddModifyRuleSetModal");
            this._AddModifyRuleSetModal.ID = "AddModifyRuleSetModal";
            this._AddModifyRuleSetModal.CssClass += " AddModifyRuleSetModal";
            this._AddModifyRuleSetModal.Type = ModalPopupType.Form;
            this._AddModifyRuleSetModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG);
            this._AddModifyRuleSetModal.HeaderIconAlt = _GlobalResources.ModifyRuleset;
            this._AddModifyRuleSetModal.HeaderText = _GlobalResources.ModifyRuleset;
            this._AddModifyRuleSetModal.TargetControlID = targetControlId;
            this._AddModifyRuleSetModal.SubmitButton.Visible = false;
            this._AddModifyRuleSetModal.CloseButton.Visible = false;
            this._AddModifyRuleSetModal.ReloadPageOnClose = false;

            // custom submit button
            this._SubmitButtonMadal = new Button();
            this._SubmitButtonMadal.ClientIDMode = ClientIDMode.Static;
            this._SubmitButtonMadal.Text = _GlobalResources.CreateRuleset;
            this._SubmitButtonMadal.OnClientClick = "getRules(this,'" + this._RulesData.ClientID + "')";
            this._SubmitButtonMadal.Command += new CommandEventHandler(this._AddModifyRuleSetModalSubmitButton_Command);
            this._SubmitButtonMadal.CssClass = "Button ActionButton";

            // custom close button
            this._CloseButtonMadal = new Button();
            this._CloseButtonMadal.Text = _GlobalResources.Close;
            this._CloseButtonMadal.CssClass = "Button NonActionButton";
            this._CloseButtonMadal.OnClientClick = "HideModalPopup();return false;";

            // hidden load button           
            this._AddModifyRuleSetModalButton = new Button();
            this._AddModifyRuleSetModalButton.ClientIDMode = ClientIDMode.Static;
            this._AddModifyRuleSetModalButton.ID = "ModifyRuleSetModalButton";
            this._AddModifyRuleSetModalButton.Command += new CommandEventHandler(this._AddModifyRulesetModalButton_Command);
            this._AddModifyRuleSetModalButton.Style.Add("display", "none");

            this._RuleSetModalPropertiesContainer = new Panel();
            this._RuleSetModalPropertiesContainer.ID = "RuleSetModalPropertiesContainer";
            
            this._RuleSetModalPropertiesContainer.Controls.Add(this._AddModifyRuleSetModalButton);

            // add hidden field for ruleset id
            this._IdRulesetModalHidden = new HiddenField();
            this._IdRulesetModalHidden.ClientIDMode = ClientIDMode.Static;
            this._IdRulesetModalHidden.ID = "IdRulesetModalHidden";
            this._RuleSetModalPropertiesContainer.Controls.Add(this._IdRulesetModalHidden);

            // name container
            this._LabelTextbox.ID = "Label_Field";
            this._LabelTextbox.CssClass = "InputMedium";

            this._RuleSetModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Label",
                                                                                   _GlobalResources.Label,
                                                                                   this._LabelTextbox.ID,
                                                                                   this._LabelTextbox,
                                                                                   true,
                                                                                   true,
                                                                                   true));

            // match any container
            Panel matchAnyFieldContainer = new Panel();
            matchAnyFieldContainer.ID = "MatchAny_Container";
            matchAnyFieldContainer.CssClass = "FormFieldContainer";

            // match any input
            List<Control> matchingFieldInputContainer = new List<Control>();

            this._MatchAny = new RadioButton();
            this._MatchAny.ID = "matchAnyRadioButton";
            this._MatchAny.Text = _GlobalResources.MatchAny;
            this._MatchAny.Checked = true;
            this._MatchAny.GroupName = "matchAny";

            this._MatchAll = new RadioButton();
            this._MatchAll.ID = "MatchAllRadioButton";
            this._MatchAll.Text = _GlobalResources.MatchAll;
            this._MatchAll.GroupName = "matchAny";

            matchingFieldInputContainer.Add(this._MatchAny);
            matchingFieldInputContainer.Add(this._MatchAll);

            this._RuleSetModalPropertiesContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("MatchInputFiled",
                                                                                                       _GlobalResources.Match,
                                                                                                       matchingFieldInputContainer,
                                                                                                       false,
                                                                                                       true));

            // rules container
            List<Control> rulesControlContainer = new List<Control>();

            rulesControlContainer.Add(this._RuleSetControlPlaceHolder);

            this._RuleSetModalPropertiesContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Rule",
                                                                                                       _GlobalResources.Rules,
                                                                                                       rulesControlContainer,
                                                                                                       true,
                                                                                                       true));

            this._RuleSetModalPropertiesContainer.Controls.Add(this._RulesData);
            this._AddModifyRuleSetModal.AddControlToBody(this._RuleSetModalPropertiesContainer);
            this._AddModifyRuleSetModal.AddControlToBody(this._SubmitButtonMadal);
            this._AddModifyRuleSetModal.AddControlToBody(this._CloseButtonMadal);

            modalContainer.Controls.Add(this._AddModifyRuleSetModal);

            EnsureChildControls();

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _AddModifyRulesetModalButton_Command
        /// <summary>
        /// Rule set modal popup hidden button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AddModifyRulesetModalButton_Command(object sender, CommandEventArgs e)
        {
            this._AddModifyRuleSetModal.ClearFeedback();

            int idRuleSet = Convert.ToInt32(this._IdRulesetModalHidden.Value);

            if (!this._RuleSetControlLoaded)
            {
                if (idRuleSet > 0)
                {
                    this._RuleSetObject = new RuleSet(idRuleSet);
                    this._AutoJoinRuleSet = new AutoJoinRuleSet(idRuleSet);
                    this._SubmitButtonMadal.Text = _GlobalResources.SaveChanges;
                    this._AddModifyRuleSetModal.HeaderText = _GlobalResources.ModifyRuleset;
                }
                else
                {
                    this._RuleSetObject = new RuleSet();
                    this._AutoJoinRuleSet = new AutoJoinRuleSet();
                    this._SubmitButtonMadal.Text = _GlobalResources.CreateRuleset;
                    this._AddModifyRuleSetModal.HeaderText = _GlobalResources.NewRuleset;
                }

                this._AutoJoinRuleSet.ID = "Rule_Field";
                this._AutoJoinRuleSet.ClientIDMode = ClientIDMode.Static;
                this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);

                EnsureChildControls();

                this._PopulatePropertiesInputElements();
            }
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populating input controls
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            if (this._RuleSetObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // rule set title
                bool isDefaultPopulated = false;

                foreach (RuleSet.LanguageSpecificProperty ruleSetLanguageSpecificProperty in this._RuleSetObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (ruleSetLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._LabelTextbox.Text = ruleSetLanguageSpecificProperty.Label;
                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificRuleSetLabelTextBox = (TextBox)this._RuleSetModalPropertiesContainer.FindControl(this._LabelTextbox.ID + "_" + ruleSetLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificRuleSetLabelTextBox != null)
                        { languageSpecificRuleSetLabelTextBox.Text = ruleSetLanguageSpecificProperty.Label; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._LabelTextbox.Text = this._RuleSetObject.Label;
                }

                // populating language independent properties
                if (this._RuleSetObject.IsAny)
                {
                    this._MatchAny.Checked = true;
                }
                else
                {
                    this._MatchAll.Checked = true;
                }
            }
            else
            {
                this._LabelTextbox.Text = String.Empty;
                this._MatchAny.Checked = true;
            }
        }
        #endregion

        #region _AddModifyRuleSetModalSubmitButton_Command
        /// <summary>
        /// Rule set modal popup submit button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> </param>
        private void _AddModifyRuleSetModalSubmitButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable rulesData = new DataTable();

                if (!String.IsNullOrWhiteSpace(this._RulesData.Value))
                {
                    rulesData.Columns.Add("idRule", typeof(int));
                    rulesData.Columns.Add("userField", typeof(string));
                    rulesData.Columns.Add("operator", typeof(string));
                    rulesData.Columns.Add("textValue", typeof(string));

                    List<RuleObject> allRules = new JavaScriptSerializer().Deserialize<List<RuleObject>>(this._RulesData.Value);

                    foreach (RuleObject rule in allRules)
                    {
                        int idRule = 0;
                        int.TryParse(rule.IdRule, out idRule);
                        rulesData.Rows.Add(idRule, rule.UserField, rule.Operator, rule.Value);
                    }

                    if (rulesData.Rows.Count > 0)
                    {
                        this._AutoJoinRuleSet = new AutoJoinRuleSet(rulesData);
                        this._AutoJoinRuleSet.ID = "Rule_Field";
                        this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);
                        this._RuleSetControlLoaded = true;
                    }

                    this._RulesData.Value = string.Empty;
                }

                int idRuleSet = Convert.ToInt32(this._IdRulesetModalHidden.Value);

                if (idRuleSet > 0)
                {
                    this._RuleSetObject = new RuleSet(idRuleSet);
                }

                // if there is no rule set object, create one
                if (this._RuleSetObject == null)
                { this._RuleSetObject = new RuleSet(); }

                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                // if this is an existing rule set, and not the default language, save language properties only
                // otherwise, save the whole object in its default language
                int id;

                // populate the object
                this._RuleSetObject.Label = this._LabelTextbox.Text;

                if (_MatchAny.Checked)
                { this._RuleSetObject.IsAny = true; }
                else
                { this._RuleSetObject.IsAny = false; }

                // save the user, save its returned id to viewstate, and 
                // instansiate a new user object with the id
                this._RuleSetObject.LinkedObjectId = this._GroupObject.Id;
                this._RuleSetObject.LinkedObjectType = RuleSetLinkedObjectType.Group;

                id = this._RuleSetObject.Save();

                Asentia.LMS.Library.Rule rules = new Asentia.LMS.Library.Rule();
                rules.IdRuleSet = id;
                if (this._Rules.Rows.Count > 0)
                {
                    rules.Save(this._Rules);
                }

                this._IdRulesetModalHidden.Value = id.ToString();
                //this._RuleSetObject.Id = id;

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string ruleSetTitle = null;

                        // get text boxes
                        TextBox languageSpecificRuleSetTitleTextBox = (TextBox)this._RuleSetModalPropertiesContainer.FindControl(this._LabelTextbox.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificRuleSetTitleTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificRuleSetTitleTextBox.Text))
                            { ruleSetTitle = languageSpecificRuleSetTitleTextBox.Text; }
                        }

                        // save the property if property is populated
                        if (!String.IsNullOrWhiteSpace(ruleSetTitle))
                        {
                            this._RuleSetObject.SaveLang(cultureInfo.Name, ruleSetTitle);
                        }
                    }
                }

                // load the saved user object
                this._RuleSetObject = new RuleSet(id);

                // rebind the grid and other controls;
                this.RuleSetGroupGrid.BindData();
                this.RuleSetGroupGridUpdatePanel.Update();

                this._SubmitButtonMadal.Text = _GlobalResources.SaveChanges;

                // display the saved feedback
                this._AddModifyRuleSetModal.DisplayFeedback(_GlobalResources.RulesetPropertiesHaveBeenSavedSuccessfully, false);

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dfnuEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dfnuEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;

            // name field
            if (String.IsNullOrWhiteSpace(this._LabelTextbox.Text.Trim()))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "Label", _GlobalResources.Label + " " + _GlobalResources.IsRequired);
            }

            // rules control
            if (this._AutoJoinRuleSet != null)
            {
                this._Rules = this._AutoJoinRuleSet.GetRulesAfterValidatingData();
                if (this._Rules == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "Rule", _GlobalResources.OneOrMoreRulesAreInvalidHoverOverField_sToViewSpecificError_s);
                }
                else if (this._Rules.Rows.Count < 1)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "Rule", _GlobalResources.YouMustHaveAtLeastOneRule);
                }
            }

            return isValid;
        }
        #endregion

        #region _GetRuleSetObject
        /// <summary>
        /// Gets a rule set object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetRuleSetObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._RuleSetObject = new RuleSet(id); }
                }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
        }
        #endregion

        #region _GetGroupObject
        /// <summary>
        /// Gets a group object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetGroupObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("gid", 0);
            int vsId = this.ViewStateInt(this.ViewState, "gid", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._GroupObject = new Group(id); }
                }
                catch
                { Response.Redirect("~/administrator/groups"); }
            }
            else
            { Response.Redirect("~/administrator/groups"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get group name information
            string groupNameInInterfaceLanguage = this._GroupObject.Name;
            string groupImagePath;
            string groupImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                }
            }

            if (this._GroupObject.Avatar != null)
            {
                groupImagePath = SitePathConstants.SITE_GROUPS_ROOT + this._GroupObject.Id + "/" + this._GroupObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                groupImageCssClass = "AvatarImage";
            }
            else
            {
                groupImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
            }
            
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Groups, "/administrator/groups"));
            breadCrumbLinks.Add(new BreadcrumbLink(groupNameInInterfaceLanguage, "/administrator/groups/Dashboard.aspx?id=" + this._GroupObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Rulesets));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, groupNameInInterfaceLanguage, groupImagePath, _GlobalResources.Rulesets, ImageFiles.GetIconPath(ImageFiles.ICON_SYNCHRONIZE, ImageFiles.EXT_PNG), groupImageCssClass);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            // ADD RULESET
            this.ObjectOptionsPanel.Controls.Add(
                this.BuildOptionsPanelImageLink("AddRuleSetLink",
                                                null,
                                                "javascript: void(0);",
                                                "OnRuleSetModifyClick(0); return false;",
                                                _GlobalResources.NewRuleset,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this._AddModifyRuleSetHiddenButton = new LinkButton();
            this._AddModifyRuleSetHiddenButton.ClientIDMode = ClientIDMode.Static;
            this._AddModifyRuleSetHiddenButton.ID = "AddModifyRuleSetHiddenButton";
            this._AddModifyRuleSetHiddenButton.Style.Add("display", "none");

            this._BuildAddModifyRuleSetModal(this._AddModifyRuleSetHiddenButton.ID, this.ActionsPanel);

            this._IdGroupHiddenField = new HiddenField();
            this._IdGroupHiddenField.ID = "IdGroupHiddenField";
            this._IdGroupHiddenField.Value = Convert.ToString(this._GroupObject.Id);

            this._PageAction = new HiddenField();
            this._PageAction.ID = "PageAction";
            this._PageAction.Value = this.QueryStringString("action", String.Empty);

            this._ModifyRulesetId = new HiddenField();
            this._ModifyRulesetId.ID = "ModifyRulesetId";
            this._ModifyRulesetId.Value = this.QueryStringString("rsid", String.Empty);

            this.ObjectOptionsPanel.Controls.Add(this._IdGroupHiddenField);
            this.ObjectOptionsPanel.Controls.Add(this._AddModifyRuleSetHiddenButton);
            this.ObjectOptionsPanel.Controls.Add(this._PageAction);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.RuleSetGroupGrid.ShowSearchBox = false;
            this.RuleSetGroupGrid.StoredProcedure = Library.RuleSet.GridProcedureForGroup;
            this.RuleSetGroupGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.RuleSetGroupGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.RuleSetGroupGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.RuleSetGroupGrid.AddFilter("@id", SqlDbType.Int, 4, this._GroupObject.Id);
            this.RuleSetGroupGrid.IdentifierField = "idRuleSet";
            this.RuleSetGroupGrid.DefaultSortColumn = "label";

            // data key names
            this.RuleSetGroupGrid.DataKeyNames = new string[] { "idRuleSet" };

            // columns
            GridColumn label = new GridColumn(_GlobalResources.Label, null); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.RuleSetGroupGrid.AddColumn(label);

            // add row data bound event
            this.RuleSetGroupGrid.RowDataBound += new GridViewRowEventHandler(this._RuleSetGroupGrid_RowDataBound);
        }
        #endregion

        #region _RuleSetGroupGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the rulesets grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RuleSetGroupGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idRuleSet = Convert.ToInt32(rowView["idRuleSet"]);

                // AVATAR, LABEL

                string label = rowView["label"].ToString();

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = label;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // label
                Label labelLabelWrapper = new Label();
                labelLabelWrapper.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(labelLabelWrapper);

                HyperLink labelLink = new HyperLink();
                labelLink.NavigateUrl = "javascript: void(0);";
                labelLink.Attributes.Add("onclick", "OnRuleSetModifyClick(" + idRuleSet.ToString() + "); return false;");
                labelLabelWrapper.Controls.Add(labelLink);

                Literal labelLabel = new Literal();
                labelLabel.Text = label;
                labelLink.Controls.Add(labelLabel);
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedRuleset_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmActionModal");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedRuleset_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseRuleset_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.RuleSetGroupGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.RuleSetGroupGrid.Rows[i].FindControl(this.RuleSetGroupGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.RuleSet.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedback(_GlobalResources.TheSelectedRuleset_sHaveBeenDeletedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.RuleSetGroupGrid.BindData();
            }
        }
        #endregion
    }
}
