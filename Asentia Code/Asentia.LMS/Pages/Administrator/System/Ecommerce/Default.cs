﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.System.Ecommerce
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel EcommerceSettingsFormContentWrapperContainer;
        public Panel EcommerceSettingsWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel EcommerceSettingsFormContainer;        
        public Panel EcommerceSettingsFormTabPanelsContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        // GLOBAL
        private DropDownList _ProcessingMethod;        

        // PAYPAL
        private DropDownList _PayPalCurrency;
        private RadioButtonList _PayPalMode;
        private TextBox _PayPalLogin;

        // AUTHORIZE.NET
        private DropDownList _AuthorizeNetCurrency;
        private RadioButtonList _AuthorizeNetMode;
        private TextBox _AuthorizeNetLogin;
        private TextBox _AuthorizeNetTransactionKey;
        private TextBox _AuthorizeNetPublicKey;
        private Label _AuthorizeNetVerification;
        private Panel _AuthorizeNetVertificationButtonContainer;
        private HiddenField _AuthorizeValidateApiLogin;
        private HiddenField _AuthorizeValidateApiPublicKey;
        private HiddenField _AuthorizeValidateCreditCardNumber;
        private HiddenField _AuthorizeValidateExpiryMonth;
        private HiddenField _AuthorizeValidateExpiryYear;
        private HiddenField _AuthorizeValidateResponseStatus;
        private HiddenField _AuthorizeValidateResponseErrorMessages;
        private HiddenField _AuthorizeValidateResponseDataToken;
        private HiddenField _AuthorizeValidateResponseDataDescriptor;
        
        // FORM PROPERTIES - AUTHORIZE.NET
        private CheckBox _VisaEnable;
        private CheckBox _MasterCardEnable;
        private CheckBox _AMEXEnable;
        private CheckBox _DiscoverCardEnable;

        private CheckBox _RequireName;
        private CheckBox _RequireCreditCardNumber;
        private CheckBox _RequireExpirationDate;
        private CheckBox _RequireCVV2;
        private CheckBox _RequireCompany;
        private CheckBox _RequireAddress;
        private CheckBox _RequireCity;
        private CheckBox _RequireStateProvince;
        private CheckBox _RequirePostalCode;
        private CheckBox _RequireCountry;
        private CheckBox _RequireEmail;

        private TextBox _ChargeDescription;
        private TextBox _TermsAndConditions;        
        
        // BUTTONS
        private Button _SaveButton;
        private Button _CancelButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.System.Ecommerce.Default.js");

            // if processing method is Authorize.net and is unverified, include JS resources for validating Authorize.net
            if (
                AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD) == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE
                && !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD_VERIFIED)
               )
            {
                string authorizeAcceptJSURL = String.Empty;

                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_MODE) == EcommerceSettings.PROCESSING_MODE_LIVE) // live
                { authorizeAcceptJSURL = Config.EcommerceSettings.AuthorizeNetAcceptJSLiveURL; }
                else // developer/sandbox
                { authorizeAcceptJSURL = Config.EcommerceSettings.AuthorizeNetAcceptJSTestURL; }

                csm.RegisterClientScriptInclude(authorizeAcceptJSURL, authorizeAcceptJSURL);
                csm.RegisterClientScriptResource(typeof(Asentia.Common.ClientScript), "Asentia.Common.AuthorizeNetAccept.js");                
            }

            // build start up calls and add to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            // if processing method is Authorize.net and is unverified, instansiate JS resources for validating Authorize.net
            if (
                AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD) == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE
                && !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD_VERIFIED)
               )
            {
                // dictionary of error messages for Accept.js
                multipleStartUpCallsScript.AppendLine(" var acceptJSErrorMessages = {");
                multipleStartUpCallsScript.AppendLine("     \"I_WC_01\" : \"" + _GlobalResources.Success + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_01\" : \"" + _GlobalResources.AcceptJSLibraryIsNotSourcedFromCDNPleaseContactAnAdministrator + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_02\" : \"" + _GlobalResources.TransactionsMustBeProcessedOverHTTPS + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_03\" : \"" + _GlobalResources.AcceptJSLibraryHasNotBeenLoadedCorrectlyPleaseContactAnAdministrator + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_04\" : \"" + _GlobalResources.CreditCardNumberExpirationDateAndCVV2AreRequired + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_05\" : \"" + _GlobalResources.CreditCardNumberIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_06\" : \"" + _GlobalResources.ExpirationMonthIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_07\" : \"" + _GlobalResources.ExpirationYearIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_08\" : \"" + _GlobalResources.ExpirationDateMustBeInTheFuture + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_10\" : \"" + _GlobalResources.AuthorizeNetAPILoginIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_14\" : \"" + _GlobalResources.AcceptJSEncryptionFailedPleaseContactAnAdministrator + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_15\" : \"" + _GlobalResources.CVV2IsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_16\" : \"" + _GlobalResources.CardholderZipCodeIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_17\" : \"" + _GlobalResources.CardholderNameIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_18\" : \"" + _GlobalResources.AuthorizeNetPublicKeyIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_A0\" : \"" + _GlobalResources.AuthenticationFailedDueToInvalidAPILoginAndOrPublicKey + "\"");
                multipleStartUpCallsScript.AppendLine(" };");
                multipleStartUpCallsScript.AppendLine("");

                // Accept.js object
                multipleStartUpCallsScript.AppendLine(" var objAuthorizeNetAcceptValidate = new AuthorizeNetAccept (");
                multipleStartUpCallsScript.AppendLine(" \"ValidateGateway\",");
                multipleStartUpCallsScript.AppendLine(" Accept,");
                multipleStartUpCallsScript.AppendLine(" \"HandleAuthorizeAcceptValidateResponse\",");
                multipleStartUpCallsScript.AppendLine(" acceptJSErrorMessages,");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AUTHORIZEVALIDATE_APILOGIN\").value,");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AUTHORIZEVALIDATE_APIPUBLICKEY\").value,");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AUTHORIZEVALIDATE_CREDITCARDNUMBER\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AUTHORIZEVALIDATE_EXPIRYMONTH\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AUTHORIZEVALIDATE_EXPIRYYEAR\"),");
                multipleStartUpCallsScript.AppendLine(" null,");
                multipleStartUpCallsScript.AppendLine(" null,");
                multipleStartUpCallsScript.AppendLine(" null,");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AUTHORIZEVALIDATE_RESPONSESTATUS\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AUTHORIZEVALIDATE_RESPONSEERRORMESSAGES\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AUTHORIZEVALIDATE_RESPONSEDATA_DESCRIPTOR\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AUTHORIZEVALIDATE_RESPONSEDATA_TOKEN\"),");
                multipleStartUpCallsScript.AppendLine(" AuthorizeNetVerificationResponseCallback");
                multipleStartUpCallsScript.AppendLine(" );");
                multipleStartUpCallsScript.AppendLine("");
                multipleStartUpCallsScript.AppendLine(" window.HandleAuthorizeAcceptValidateResponse = function(response) {");
                multipleStartUpCallsScript.AppendLine("     objAuthorizeNetAcceptValidate.HandleResponse(response);");
                multipleStartUpCallsScript.AppendLine(" }");
                multipleStartUpCallsScript.AppendLine("");
            }

            // MCE
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));        

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine(" ProcessingMethodChanged();"); // does initial show/hide of form controls based on selected processing method
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);
        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // check to ensure ecommerce is enabled on the portal
            if (!AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_ENABLED) ?? false)
            { Response.Redirect("/"); }
            
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_Ecommerce))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/system/ecommerce/Default.css");

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.EcommerceSettingsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.EcommerceSettingsWrapperContainer.CssClass = "FormContentContainer";

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string configurationImagePath;
            string pageTitle;

            breadCrumbPageTitle = _GlobalResources.ECommerce;

            configurationImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_ECOMMERCE, ImageFiles.EXT_PNG);

            pageTitle = _GlobalResources.ECommerce;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, pageTitle, configurationImagePath);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the form
            this._BuildForm();

            // build the form actions panel
            this._BuildActionsPanel();

            // populate the form inputs
            this._PopulateInputControls();
        }
        #endregion

        #region _BuildForm
        /// <summary>
        /// Builds the form.
        /// </summary>
        private void _BuildForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.SelectAndConfigureYourPaymentProcessorUsingTheFormBelow, true);

            // clear controls from container
            this.EcommerceSettingsFormContainer.Controls.Clear();

            // build the form tabs
            this._BuildFormTabs();

            // build tab panels container
            this.EcommerceSettingsFormTabPanelsContainer = new Panel();
            this.EcommerceSettingsFormTabPanelsContainer.ID = "EcommerceSettingsForm_TabPanelsContainer";
            this.EcommerceSettingsFormTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.EcommerceSettingsFormContainer.Controls.Add(this.EcommerceSettingsFormTabPanelsContainer);

            // build input controls
            this._BuildGatewayProcessorPanel();
            this._BuildFormPropertiesPanel();
        }
        #endregion

        #region _BuildFormTabs
        /// <summary>
        /// Builds the tabs for the form.
        /// </summary>
        private void _BuildFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("GatewayProcessor", _GlobalResources.GatewayProcessor));
            tabs.Enqueue(new KeyValuePair<string, string>("FormProperties", _GlobalResources.FormProperties));            

            // build and attach the tabs
            this.EcommerceSettingsFormContainer.Controls.Add(AsentiaPage.BuildTabListPanel("EcommerceSettingsForm", tabs, null, this.Page, null));
        }
        #endregion

        #region _BuildGatewayProcessorPanel
        /// <summary>
        /// Method to build the properties for gateway/processor tab.
        /// </summary>
        private void _BuildGatewayProcessorPanel()
        {
            // "Gateway/Processor" is the default tab, so this is visible on page load.
            Panel gatewayProcessorPanel = new Panel();
            gatewayProcessorPanel.ID = "EcommerceSettingsForm_" + "GatewayProcessor" + "_TabPanel";
            gatewayProcessorPanel.Attributes.Add("style", "display: block;");
            this.EcommerceSettingsFormTabPanelsContainer.Controls.Add(gatewayProcessorPanel);

            // processing method
            this._ProcessingMethod = new DropDownList();
            this._ProcessingMethod.ID = "ProcessingMethod_Field";
            this._ProcessingMethod.Items.Add(new ListItem(_GlobalResources.None, EcommerceSettings.PROCESSING_METHOD_NONE));
            this._ProcessingMethod.Items.Add(new ListItem(_GlobalResources.AuthorizeNet, EcommerceSettings.PROCESSING_METHOD_AUTHORIZE));
            this._ProcessingMethod.Items.Add(new ListItem(_GlobalResources.PayPalWebsitePaymentsStandard, EcommerceSettings.PROCESSING_METHOD_PAYPAL));
            this._ProcessingMethod.Attributes.Add("onchange", "ProcessingMethodChanged();");

            gatewayProcessorPanel.Controls.Add(AsentiaPage.BuildFormField("ProcessingMethod",
                                                                   _GlobalResources.ProcessingMethod,
                                                                   this._ProcessingMethod.ID,
                                                                   this._ProcessingMethod,
                                                                   true,
                                                                   true,
                                                                   false));

            // BEGIN PAYPAL SPECIFIC FORM CONTROLS

            Panel payPalSpecificGatewayProcessorControls = new Panel();
            payPalSpecificGatewayProcessorControls.ID = "PayPalSpecificGatewayProcessorControls";
            payPalSpecificGatewayProcessorControls.Style.Add("display", "none");
            gatewayProcessorPanel.Controls.Add(payPalSpecificGatewayProcessorControls);

            // currency
            List<Control> payPalCurrencyControls = new List<Control>();

            // information
            Panel payPalCurrencyInfoPanel = new Panel();
            this.FormatFormInformationPanel(payPalCurrencyInfoPanel, _GlobalResources.ChangingTheCurrencyWillClearAllUsersShoppingCarts, false);
            payPalCurrencyControls.Add(payPalCurrencyInfoPanel);

            // control
            this._PayPalCurrency = new DropDownList();
            this._PayPalCurrency.ID = "PayPalCurrency_Field";

            foreach (EcommerceSettings.Currency key in EcommerceSettings.Currencies.Keys)
            {
                if (EcommerceSettings.Currencies[key].IsPayPalEnabled)
                { this._PayPalCurrency.Items.Add(new ListItem(EcommerceSettings.Currencies[key].Label, EcommerceSettings.Currencies[key].Code)); }
            }

            payPalCurrencyControls.Add(this._PayPalCurrency);


            payPalSpecificGatewayProcessorControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("PayPalCurrency",
                                                                                                        _GlobalResources.Currency,
                                                                                                        payPalCurrencyControls,
                                                                                                        true,
                                                                                                        true));

            // processing mode
            this._PayPalMode = new RadioButtonList();
            this._PayPalMode.ID = "PayPalMode_Field";
            this._PayPalMode.Items.Add(new ListItem(_GlobalResources.Live, EcommerceSettings.PROCESSING_MODE_LIVE));
            this._PayPalMode.Items.Add(new ListItem(_GlobalResources.Test, EcommerceSettings.PROCESSING_MODE_TEST));
            this._PayPalMode.SelectedValue = EcommerceSettings.PROCESSING_MODE_LIVE;

            payPalSpecificGatewayProcessorControls.Controls.Add(AsentiaPage.BuildFormField("PayPalMode",
                                                                                    _GlobalResources.TransactionMode,
                                                                                    this._PayPalMode.ID,
                                                                                    this._PayPalMode,
                                                                                    true,
                                                                                    true,
                                                                                    false));

            // paypal login
            this._PayPalLogin = new TextBox();
            this._PayPalLogin.ID = "PayPalLogin_Field";
            this._PayPalLogin.CssClass = "InputLong";

            payPalSpecificGatewayProcessorControls.Controls.Add(AsentiaPage.BuildFormField("PayPalLogin",
                                                                                    _GlobalResources.PayPalLogin,
                                                                                    this._PayPalLogin.ID,
                                                                                    this._PayPalLogin,
                                                                                    true,
                                                                                    true,
                                                                                    false));

            // END PAYPAL SPECIFIC FORM CONTROLS

            // BEGIN AUTHORIZE.NET SPECIFIC CONTROLS

            Panel authorizeNetSpecificGatewayProcessorControls = new Panel();
            authorizeNetSpecificGatewayProcessorControls.ID = "AuthorizeNetSpecificGatewayProcessorControls";
            authorizeNetSpecificGatewayProcessorControls.Style.Add("display", "none");
            gatewayProcessorPanel.Controls.Add(authorizeNetSpecificGatewayProcessorControls);

            // currency
            List<Control> authorizeNetCurrencyControls = new List<Control>();

            // information
            Panel authorizeNetCurrencyInfoPanel = new Panel();
            this.FormatFormInformationPanel(authorizeNetCurrencyInfoPanel, _GlobalResources.ChangingTheCurrencyWillClearAllUsersShoppingCarts, false);
            authorizeNetCurrencyControls.Add(authorizeNetCurrencyInfoPanel);

            // control
            this._AuthorizeNetCurrency = new DropDownList();
            this._AuthorizeNetCurrency.ID = "AuthorizeNetCurrency_Field";

            foreach (EcommerceSettings.Currency key in EcommerceSettings.Currencies.Keys)
            {
                if (EcommerceSettings.Currencies[key].IsAuthorizeEnabled)
                { this._AuthorizeNetCurrency.Items.Add(new ListItem(EcommerceSettings.Currencies[key].Label, EcommerceSettings.Currencies[key].Code)); }
            }

            authorizeNetCurrencyControls.Add(this._AuthorizeNetCurrency);


            authorizeNetSpecificGatewayProcessorControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("AuthorizeNetCurrency",
                                                                                                              _GlobalResources.Currency,
                                                                                                              authorizeNetCurrencyControls,
                                                                                                              true,
                                                                                                              true));

            // processing mode
            this._AuthorizeNetMode = new RadioButtonList();
            this._AuthorizeNetMode.ID = "AuthorizeNetMode_Field";
            this._AuthorizeNetMode.Items.Add(new ListItem(_GlobalResources.Live, EcommerceSettings.PROCESSING_MODE_LIVE));
            this._AuthorizeNetMode.Items.Add(new ListItem(_GlobalResources.Test, EcommerceSettings.PROCESSING_MODE_TEST));
            this._AuthorizeNetMode.Items.Add(new ListItem(_GlobalResources.DeveloperAccount, EcommerceSettings.PROCESSING_MODE_DEVELOPER));
            this._AuthorizeNetMode.SelectedValue = EcommerceSettings.PROCESSING_MODE_LIVE;

            authorizeNetSpecificGatewayProcessorControls.Controls.Add(AsentiaPage.BuildFormField("AuthorizeNetMode",
                                                                                          _GlobalResources.TransactionMode,
                                                                                          this._AuthorizeNetMode.ID,
                                                                                          this._AuthorizeNetMode,
                                                                                          true,
                                                                                          true,
                                                                                          false));

            // authorize api login
            this._AuthorizeNetLogin = new TextBox();
            this._AuthorizeNetLogin.ID = "AuthorizeNetLogin_Field";
            this._AuthorizeNetLogin.CssClass = "InputLong";

            authorizeNetSpecificGatewayProcessorControls.Controls.Add(AsentiaPage.BuildFormField("AuthorizeNetLogin",
                                                                                          _GlobalResources.APILogin,
                                                                                          this._AuthorizeNetLogin.ID,
                                                                                          this._AuthorizeNetLogin,
                                                                                          true,
                                                                                          true,
                                                                                          false));

            // authorize transaction key
            List<Control> authorizeNetTransactionKeyControls = new List<Control>();

            // information
            Panel authorizeNetTransactionKeyInfoPanel = new Panel();
            this.FormatFormInformationPanel(authorizeNetTransactionKeyInfoPanel, _GlobalResources.NoteThisIsNotYourAccountPassword, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));
            authorizeNetTransactionKeyControls.Add(authorizeNetTransactionKeyInfoPanel);

            // control
            this._AuthorizeNetTransactionKey = new TextBox();
            this._AuthorizeNetTransactionKey.ID = "AuthorizeNetTransactionKey_Field";
            this._AuthorizeNetTransactionKey.CssClass = "InputLong";
            authorizeNetTransactionKeyControls.Add(this._AuthorizeNetTransactionKey);

            authorizeNetSpecificGatewayProcessorControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("AuthorizeNetTransactionKey",
                                                                                                              _GlobalResources.TransactionKey,
                                                                                                              authorizeNetTransactionKeyControls,
                                                                                                              true,
                                                                                                              true));

            // authorize public key
            List<Control> authorizeNetPublicKeyControls = new List<Control>();

            // control
            this._AuthorizeNetPublicKey = new TextBox();
            this._AuthorizeNetPublicKey.ID = "AuthorizeNetPublicKey_Field";
            this._AuthorizeNetPublicKey.CssClass = "InputXXLong";
            authorizeNetPublicKeyControls.Add(this._AuthorizeNetPublicKey);

            authorizeNetSpecificGatewayProcessorControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("AuthorizeNetPublicKey",
                                                                                                              _GlobalResources.PublicKey,
                                                                                                              authorizeNetPublicKeyControls,
                                                                                                              true,
                                                                                                              true));

            // settings verification status
            List<Control> authorizeNetVerificationControls = new List<Control>();

            // control
            this._AuthorizeNetVerification = new Label();
            this._AuthorizeNetVerification.ID = "AuthorizeNetVerification_Field";
            this._AuthorizeNetVerification.Text = _GlobalResources.NotVerified;
            authorizeNetVerificationControls.Add(this._AuthorizeNetVerification);

            // verification button container
            this._AuthorizeNetVertificationButtonContainer = new Panel();
            authorizeNetVerificationControls.Add(this._AuthorizeNetVertificationButtonContainer);

            authorizeNetSpecificGatewayProcessorControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("AuthorizeNetVerification",
                                                                                                              _GlobalResources.Status,
                                                                                                              authorizeNetVerificationControls,
                                                                                                              false,
                                                                                                              true));

            // END AUTHORIZE.NET SPECIFIC CONTROLS
        }
        #endregion

        #region _BuildFormPropertiesPanel
        /// <summary>
        /// Method to build the properties for form properties tab.
        /// </summary>
        private void _BuildFormPropertiesPanel()
        {
            Panel formPropertiesPanel = new Panel();
            formPropertiesPanel.ID = "EcommerceSettingsForm_" + "FormProperties" + "_TabPanel";
            formPropertiesPanel.Attributes.Add("style", "display: none;");
            this.EcommerceSettingsFormTabPanelsContainer.Controls.Add(formPropertiesPanel);

            // BEGIN "NONE" SPECIFIC FORM CONTROLS

            Panel noneSpecificFormPropertiesControls = new Panel();
            noneSpecificFormPropertiesControls.ID = "NoneSpecificFormPropertiesControls";
            formPropertiesPanel.Controls.Add(noneSpecificFormPropertiesControls);

            Panel noneFormPropertiesInfoPanel = new Panel();
            this.FormatFormInformationPanel(noneFormPropertiesInfoPanel, _GlobalResources.NoFormOptionsAreAvailableWhenNoneIsSelectedAsTheProcessingMethod, false);
            noneSpecificFormPropertiesControls.Controls.Add(noneFormPropertiesInfoPanel);

            // END "NONE" SPECIFIC FORM CONTROLS

            // BEGIN PAYPAL SPECIFIC FORM CONTROLS

            Panel payPalSpecificFormPropertiesControls = new Panel();
            payPalSpecificFormPropertiesControls.ID = "PayPalSpecificFormPropertiesControls";
            payPalSpecificFormPropertiesControls.Style.Add("display", "none");
            formPropertiesPanel.Controls.Add(payPalSpecificFormPropertiesControls);

            Panel payPalFormPropertiesInfoPanel = new Panel();
            this.FormatFormInformationPanel(payPalFormPropertiesInfoPanel, _GlobalResources.NoFormOptionsAreAvailableWhenPayPalIsSelectedAsTheProcessingMethod, false);
            payPalSpecificFormPropertiesControls.Controls.Add(payPalFormPropertiesInfoPanel);

            // END PAYPAL SPECIFIC FORM CONTROLS

            // BEGIN OTHER PROCESSOR SPECIFIC CONTROLS (AUTHORIZE.NET)

            Panel otherProcessorsFormPropertiesControls = new Panel();
            otherProcessorsFormPropertiesControls.ID = "OtherProcessorsFormPropertiesControls";
            otherProcessorsFormPropertiesControls.Style.Add("display", "none");
            formPropertiesPanel.Controls.Add(otherProcessorsFormPropertiesControls);

            // cards 
            List<Control> otherProcessorsCardAcceptanceCheckboxControls = new List<Control>();

            // information
            Panel cardAcceptanceInfoPanel = new Panel();
            this.FormatFormInformationPanel(cardAcceptanceInfoPanel, _GlobalResources.SelectTheCardsThatYourPaymentGatewayAccountAccepts, false);
            otherProcessorsCardAcceptanceCheckboxControls.Add(cardAcceptanceInfoPanel);

            // visa
            this._VisaEnable = new CheckBox();
            this._VisaEnable.ID = "VisaEnable_Field";
            this._VisaEnable.Text = String.Format("<img onclick=\"SelectCardCheckboxIE(this, '" + this._VisaEnable.ID + "');\" src=\"{0}\" />", EcommerceSettings.CREDITCARDICON_VISA);
            this._VisaEnable.Checked = true; // visa checked by default
            otherProcessorsCardAcceptanceCheckboxControls.Add(this._VisaEnable);

            // mastercard
            this._MasterCardEnable = new CheckBox();
            this._MasterCardEnable.ID = "MasterCardEnable_Field";
            this._MasterCardEnable.Text = String.Format("<img onclick=\"SelectCardCheckboxIE(this, '" + this._MasterCardEnable.ID + "');\" src=\"{0}\" />", EcommerceSettings.CREDITCARDICON_MASTERCARD);
            this._MasterCardEnable.Checked = true; // mastercard checked by default
            otherProcessorsCardAcceptanceCheckboxControls.Add(this._MasterCardEnable);

            // amex
            this._AMEXEnable = new CheckBox();
            this._AMEXEnable.ID = "AMEXEnable_Field";
            this._AMEXEnable.Text = String.Format("<img onclick=\"SelectCardCheckboxIE(this, '" + this._AMEXEnable.ID + "');\" src=\"{0}\" />", EcommerceSettings.CREDITCARDICON_AMEX);
            otherProcessorsCardAcceptanceCheckboxControls.Add(this._AMEXEnable);

            // discover card
            this._DiscoverCardEnable = new CheckBox();
            this._DiscoverCardEnable.ID = "DiscoverCardEnable_Field";
            this._DiscoverCardEnable.Text = String.Format("<img onclick=\"SelectCardCheckboxIE(this, '" + this._DiscoverCardEnable.ID + "');\" src=\"{0}\" />", EcommerceSettings.CREDITCARDICON_DISCOVERCARD);
            otherProcessorsCardAcceptanceCheckboxControls.Add(this._DiscoverCardEnable);

            otherProcessorsFormPropertiesControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CardAcceptanceControls",
                                                                                                       _GlobalResources.CardsAccepted,
                                                                                                       otherProcessorsCardAcceptanceCheckboxControls,
                                                                                                       false,
                                                                                                       true));

            // required fields 
            List<Control> otherProcessorsRequiredFieldsCheckboxControls = new List<Control>();

            // information
            Panel requiredFieldsInfoPanel = new Panel();
            this.FormatFormInformationPanel(requiredFieldsInfoPanel, _GlobalResources.SelectTheFieldsThatYourPaymentGatewayRequiresForProcessing, false);
            otherProcessorsRequiredFieldsCheckboxControls.Add(requiredFieldsInfoPanel);

            // name
            this._RequireName = new CheckBox();
            this._RequireName.ID = "RequireName_Field";
            this._RequireName.Text = _GlobalResources.Name;
            this._RequireName.Checked = true; // name is always required
            this._RequireName.Enabled = false;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequireName);

            // credit card number
            this._RequireCreditCardNumber = new CheckBox();
            this._RequireCreditCardNumber.ID = "RequireCreditCardNumber_Field";
            this._RequireCreditCardNumber.Text = _GlobalResources.CreditCardNumber;
            this._RequireCreditCardNumber.Checked = true; // credit card number is always required
            this._RequireCreditCardNumber.Enabled = false;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequireCreditCardNumber);

            // expiration date
            this._RequireExpirationDate = new CheckBox();
            this._RequireExpirationDate.ID = "RequireExpirationDate_Field";
            this._RequireExpirationDate.Text = _GlobalResources.ExpirationDate;
            this._RequireExpirationDate.Checked = true; // expiration date is always required
            this._RequireExpirationDate.Enabled = false;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequireExpirationDate);

            // cvv2
            this._RequireCVV2 = new CheckBox();
            this._RequireCVV2.ID = "RequireCVV2_Field";
            this._RequireCVV2.Text = _GlobalResources.CVV2;
            this._RequireCVV2.Checked = true; // cvv2 is always required
            this._RequireCVV2.Enabled = false;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequireCVV2);

            // company
            this._RequireCompany = new CheckBox();
            this._RequireCompany.ID = "RequireCompany_Field";
            this._RequireCompany.Text = _GlobalResources.Company;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequireCompany);

            // address
            this._RequireAddress = new CheckBox();
            this._RequireAddress.ID = "RequireAddress_Field";
            this._RequireAddress.Text = _GlobalResources.Address;
            this._RequireAddress.Checked = true;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequireAddress);

            // city
            this._RequireCity = new CheckBox();
            this._RequireCity.ID = "RequireCity_Field";
            this._RequireCity.Text = _GlobalResources.City;
            this._RequireCity.Checked = true;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequireCity);

            // state/province
            this._RequireStateProvince = new CheckBox();
            this._RequireStateProvince.ID = "RequireStateProvince_Field";
            this._RequireStateProvince.Text = _GlobalResources.StateProvince;
            this._RequireStateProvince.Checked = true;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequireStateProvince);

            // postal code
            this._RequirePostalCode = new CheckBox();
            this._RequirePostalCode.ID = "RequirePostalCode_Field";
            this._RequirePostalCode.Text = _GlobalResources.PostalCode;
            this._RequirePostalCode.Checked = true;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequirePostalCode);

            // country
            this._RequireCountry = new CheckBox();
            this._RequireCountry.ID = "RequireCountry_Field";
            this._RequireCountry.Text = _GlobalResources.Country;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequireCountry);

            // email
            this._RequireEmail = new CheckBox();
            this._RequireEmail.ID = "RequireEmail_Field";
            this._RequireEmail.Text = _GlobalResources.Email;
            otherProcessorsRequiredFieldsCheckboxControls.Add(this._RequireEmail);

            otherProcessorsFormPropertiesControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("RequiredFormFieldsControls",
                                                                                                       _GlobalResources.RequiredFormFields,
                                                                                                       otherProcessorsRequiredFieldsCheckboxControls,
                                                                                                       false,
                                                                                                       true));

            // charge description
            List<Control> chargeDescriptionControls = new List<Control>();

            // information
            Panel chargeDescriptionInfoPanel = new Panel();
            this.FormatFormInformationPanel(chargeDescriptionInfoPanel, _GlobalResources.InformPurchasersHowYourChargesAppearOnTheirCreditCardStatement + " \"" + _GlobalResources.ThisChargeWillAppearOnYourCreditCardAs + "\"", false);
            chargeDescriptionControls.Add(chargeDescriptionInfoPanel);

            // control
            this._ChargeDescription = new TextBox();
            this._ChargeDescription.ID = "ChargeDescription_Field";
            this._ChargeDescription.CssClass = "InputLong";            
            chargeDescriptionControls.Add(this._ChargeDescription);

            otherProcessorsFormPropertiesControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ChargeDescription",
                                                                                                       _GlobalResources.ChargeDescription,
                                                                                                       chargeDescriptionControls,
                                                                                                       false,
                                                                                                       true));

            // terms and conditions
            List<Control> termsAndConditionsControls = new List<Control>();

            // information
            Panel termsAndConditionsInfoPanel = new Panel();
            this.FormatFormInformationPanel(termsAndConditionsInfoPanel, _GlobalResources.PurchasersMustAgreeToTheTermsAndConditionsYouEnterHereToCompletePurchases, false);
            termsAndConditionsControls.Add(termsAndConditionsInfoPanel);

            // control
            this._TermsAndConditions = new TextBox();
            this._TermsAndConditions.ID = "TermsAndConditions_Field";
            this._TermsAndConditions.CssClass = "ckeditor";
            this._TermsAndConditions.Style.Add("width", "98%");
            this._TermsAndConditions.TextMode = TextBoxMode.MultiLine;
            this._TermsAndConditions.Rows = 10;
            termsAndConditionsControls.Add(this._TermsAndConditions);

            otherProcessorsFormPropertiesControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("TermsAndConditions",
                                                                                                       _GlobalResources.TermsAndConditions,
                                                                                                       termsAndConditionsControls,
                                                                                                       false,
                                                                                                       true));

            // END OTHER PROCESSOR SPECIFIC CONTROLS (AUTHORIZE.NET)
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // clear controls from container
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion        

        #region _PopulateInputControls
        /// <summary>
        /// Loads ecommerce settings from the database and populates the controls.
        /// </summary>
        private void _PopulateInputControls()
        {
            string processingMethod = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD);

            if (!String.IsNullOrWhiteSpace(processingMethod))
            { this._ProcessingMethod.SelectedValue = processingMethod; }

            // Gateway/Processor controls

            if (processingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
            {
                // currency
                string payPalCurrency = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_CURRENCY);

                if (!String.IsNullOrWhiteSpace(payPalCurrency))
                { this._PayPalCurrency.SelectedValue = payPalCurrency; }

                // paypal mode
                string payPalMode = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PAYPAL_MODE);

                if (!String.IsNullOrWhiteSpace(payPalMode))
                { this._PayPalMode.SelectedValue = payPalMode; }

                // paypal login
                string payPalLogin = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PAYPAL_LOGIN);

                if (!String.IsNullOrWhiteSpace(payPalLogin))
                { this._PayPalLogin.Text = payPalLogin; }
            }
            else if (processingMethod == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE)
            {
                // currency
                string authorizeNetCurrency = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_CURRENCY);

                if (!String.IsNullOrWhiteSpace(authorizeNetCurrency))
                { this._AuthorizeNetCurrency.SelectedValue = authorizeNetCurrency; }

                // authorize mode
                string authorizeNetMode = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_MODE);

                if (!String.IsNullOrWhiteSpace(authorizeNetMode))
                { this._AuthorizeNetMode.SelectedValue = authorizeNetMode; }

                // authorize api login
                string authorizeNetApiLogin = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_LOGIN);

                if (!String.IsNullOrWhiteSpace(authorizeNetApiLogin))
                { this._AuthorizeNetLogin.Text = authorizeNetApiLogin; }

                // authorize transaction key
                string authorizeNetTransactionKey = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_TRANSACTIONKEY);

                if (!String.IsNullOrWhiteSpace(authorizeNetTransactionKey))
                { this._AuthorizeNetTransactionKey.Text = authorizeNetTransactionKey; }

                // authorize public key
                string authorizeNetPublicKey = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_PUBLICKEY);

                if (!String.IsNullOrWhiteSpace(authorizeNetPublicKey))
                { this._AuthorizeNetPublicKey.Text = authorizeNetPublicKey; }

                // authorize verification status
                bool authorizeNetIsVerified = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD_VERIFIED) ?? false;

                if (authorizeNetIsVerified)
                {
                    this._AuthorizeNetVerification.CssClass = "EcommerceVerifiedText";
                    this._AuthorizeNetVerification.Text = _GlobalResources.Verified;
                }
                else
                {
                    this._AuthorizeNetVerification.CssClass = "EcommerceNotVerifiedText";
                    this._AuthorizeNetVerification.Text = _GlobalResources.NotVerified;

                    // add verification inputs and button
                    this._AuthorizeNetVertificationButtonContainer.CssClass = "VerificationButtonContainer";

                    // hidden inputs
                    this._AuthorizeValidateApiLogin = new HiddenField();
                    this._AuthorizeValidateApiLogin.ID = "AUTHORIZEVALIDATE_APILOGIN";
                    this._AuthorizeValidateApiLogin.Value = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_LOGIN);
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(this._AuthorizeValidateApiLogin);

                    this._AuthorizeValidateApiPublicKey = new HiddenField();
                    this._AuthorizeValidateApiPublicKey.ID = "AUTHORIZEVALIDATE_APIPUBLICKEY";
                    this._AuthorizeValidateApiPublicKey.Value = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_PUBLICKEY);
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(this._AuthorizeValidateApiPublicKey);

                    this._AuthorizeValidateCreditCardNumber = new HiddenField();
                    this._AuthorizeValidateCreditCardNumber.ID = "AUTHORIZEVALIDATE_CREDITCARDNUMBER";
                    this._AuthorizeValidateCreditCardNumber.Value = "4111111111111111";
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(this._AuthorizeValidateCreditCardNumber);

                    this._AuthorizeValidateExpiryMonth = new HiddenField();
                    this._AuthorizeValidateExpiryMonth.ID = "AUTHORIZEVALIDATE_EXPIRYMONTH";
                    this._AuthorizeValidateExpiryMonth.Value = AsentiaSessionState.UtcNow.AddDays(1).ToString("MM");
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(this._AuthorizeValidateExpiryMonth);

                    this._AuthorizeValidateExpiryYear = new HiddenField();
                    this._AuthorizeValidateExpiryYear.ID = "AUTHORIZEVALIDATE_EXPIRYYEAR";
                    this._AuthorizeValidateExpiryYear.Value = AsentiaSessionState.UtcNow.AddDays(1).ToString("yy");
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(this._AuthorizeValidateExpiryYear);

                    this._AuthorizeValidateResponseStatus = new HiddenField();
                    this._AuthorizeValidateResponseStatus.ID = "AUTHORIZEVALIDATE_RESPONSESTATUS";
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(this._AuthorizeValidateResponseStatus);

                    this._AuthorizeValidateResponseErrorMessages = new HiddenField();
                    this._AuthorizeValidateResponseErrorMessages.ID = "AUTHORIZEVALIDATE_RESPONSEERRORMESSAGES";
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(this._AuthorizeValidateResponseErrorMessages);

                    this._AuthorizeValidateResponseDataToken = new HiddenField();
                    this._AuthorizeValidateResponseDataToken.ID = "AUTHORIZEVALIDATE_RESPONSEDATA_TOKEN";
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(this._AuthorizeValidateResponseDataToken);

                    this._AuthorizeValidateResponseDataDescriptor = new HiddenField();
                    this._AuthorizeValidateResponseDataDescriptor.ID = "AUTHORIZEVALIDATE_RESPONSEDATA_DESCRIPTOR";
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(this._AuthorizeValidateResponseDataDescriptor);

                    // button
                    Image authorizeNetVerificationImageForLink = new Image();
                    authorizeNetVerificationImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RELOAD, ImageFiles.EXT_PNG);
                    authorizeNetVerificationImageForLink.CssClass = "MediumIcon";

                    Localize authorizeNetVerificationTextForLink = new Localize();
                    authorizeNetVerificationTextForLink.Text = _GlobalResources.VerifyPaymentGateway;

                    // visible button - for JS 
                    HyperLink authorizeNetVerificationButton = new HyperLink();
                    authorizeNetVerificationButton.ID = "AuthorizeNetVerificationButton";
                    authorizeNetVerificationButton.CssClass = "ImageLink";
                    authorizeNetVerificationButton.NavigateUrl = "javascript:void(0);";
                    authorizeNetVerificationButton.Attributes.Add("onclick", "DoAuthorizeNetVerification();");
                    authorizeNetVerificationButton.Controls.Add(authorizeNetVerificationImageForLink);
                    authorizeNetVerificationButton.Controls.Add(authorizeNetVerificationTextForLink);
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(authorizeNetVerificationButton);

                    // hidden button for postback - has to be done this way so we can control when this is fires, i.e. after JS is finshed
                    Button authorizeNetVerificationHiddenPostbackButton = new Button();
                    authorizeNetVerificationHiddenPostbackButton.ID = "AuthorizeNetVerificationHiddenPostbackButton";
                    authorizeNetVerificationHiddenPostbackButton.Style.Add("display", "none");
                    authorizeNetVerificationHiddenPostbackButton.Command += this._AuthorizeNetVerificationButton_Command;
                    this._AuthorizeNetVertificationButtonContainer.Controls.Add(authorizeNetVerificationHiddenPostbackButton);
                }
            }
            else
            { }

            // Form Properties controls

            if (processingMethod == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE)
            {
                // card acceptance
                this._VisaEnable.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_VISA_ENABLED) ?? false;
                this._MasterCardEnable.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_MASTERCARD_ENABLED) ?? false;
                this._AMEXEnable.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_AMEX_ENABLED) ?? false;
                this._DiscoverCardEnable.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_DISCOVERCARD_ENABLED) ?? false;

                // required fields                
                this._RequireCompany.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_COMPANY_REQUIRED) ?? false;
                this._RequireAddress.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_ADDRESS_REQUIRED) ?? false;
                this._RequireCity.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_CITY_REQUIRED) ?? false;
                this._RequireStateProvince.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_STATEPROVINCE_REQUIRED) ?? false;
                this._RequirePostalCode.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_POSTALCODE_REQUIRED) ?? false;                
                this._RequireCountry.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_COUNTRY_REQUIRED) ?? false;                
                this._RequireEmail.Checked = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_EMAIL_REQUIRED) ?? false;
                
                // charge description
                this._ChargeDescription.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_CHARGEDESCRIPTION);

                // terms and conditions
                if (File.Exists(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + EcommerceSettings.TERMS_AND_CONDITIONS_FILENAME)))
                {
                    using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + EcommerceSettings.TERMS_AND_CONDITIONS_FILENAME)))
                    { this._TermsAndConditions.Text = reader.ReadToEnd(); } 
                }
            }
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;
            bool gatewayProcessorTabHasErrors = false;
            bool formPropertiesTabHasErrors = false;

            // Gateway/Processor controls

            if (this._ProcessingMethod.SelectedValue == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
            {
                // validate paypal login field
                if (String.IsNullOrWhiteSpace(this._PayPalLogin.Text))
                {
                    gatewayProcessorTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EcommerceSettingsFormContainer, "PayPalLogin", _GlobalResources.PayPalLogin + " " + _GlobalResources.IsRequired);
                }
                else
                {
                    if (!RegExValidation.ValidateEmailAddress(this._PayPalLogin.Text))
                    {
                        gatewayProcessorTabHasErrors = true;
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.EcommerceSettingsFormContainer, "PayPalLogin", _GlobalResources.PayPalLogin + " " + _GlobalResources.IsInvalid);
                    }
                }
            }
            else if (this._ProcessingMethod.SelectedValue == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE)
            {
                // validate authorize.net api login field
                if (String.IsNullOrWhiteSpace(this._AuthorizeNetLogin.Text))
                {
                    gatewayProcessorTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EcommerceSettingsFormContainer, "AuthorizeNetLogin", _GlobalResources.APILogin + " " + _GlobalResources.IsRequired);
                }

                // validate authorize.net transaction key field
                if (String.IsNullOrWhiteSpace(this._AuthorizeNetTransactionKey.Text))
                {
                    gatewayProcessorTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EcommerceSettingsFormContainer, "AuthorizeNetTransactionKey", _GlobalResources.TransactionKey + " " + _GlobalResources.IsRequired);
                }

                // validate authorize.net public key field
                if (String.IsNullOrWhiteSpace(this._AuthorizeNetPublicKey.Text))
                {
                    gatewayProcessorTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EcommerceSettingsFormContainer, "AuthorizeNetPublicKey", _GlobalResources.PublicKey + " " + _GlobalResources.IsRequired);
                }
            }
            else
            { }

            // Form Properties controls

            if (this._ProcessingMethod.SelectedValue == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE)
            {
                // validate credit card acceptance checkboxes - at least one must be selected
                if (!this._VisaEnable.Checked && !this._MasterCardEnable.Checked && !this._AMEXEnable.Checked && this._DiscoverCardEnable.Checked)
                {
                    formPropertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EcommerceSettingsFormContainer, "CardAcceptanceControls", _GlobalResources.AtLeastOneCreditCardTypeMustBeSelected);
                }
            }

            // apply error image and class to tabs if they have errors
            if (gatewayProcessorTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.EcommerceSettingsFormContainer, "EcommerceSettingsForm_GatewayProcessor_TabLI"); }

            if (formPropertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.EcommerceSettingsFormContainer, "EcommerceSettingsForm_FormProperties_TabLI"); }

            return isValid;
        }
        #endregion

        #region _AuthorizeNetVerificationButton_Command
        private void _AuthorizeNetVerificationButton_Command(object sender, CommandEventArgs e)
        {
            bool isValidated = false;
            string validationErrors = String.Empty;

            if (this._AuthorizeValidateResponseStatus.Value == "SUCCESS")
            {
                try
                {
                    // authenticate the server-side gateway, if it fails, an exception will be thrown and caught
                    AuthorizeDotNetAIM.AuthenticateGateway(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_LOGIN), AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_TRANSACTIONKEY));

                    // if we get here without an exception, validation passed
                    isValidated = true;
                }
                catch (AsentiaException ex)
                {
                    isValidated = false;
                    validationErrors = ex.Message.Replace("|", "<br />");
                }
                catch (Exception exc) // other exception that hasn't otherwise been handled
                {
                    isValidated = false;
                    validationErrors = exc.Message;
                }
            }
            else
            {
                isValidated = false;
                validationErrors = this._AuthorizeValidateResponseErrorMessages.Value.Replace("|", "<br />");
            }

            // update verified status
            DataTable siteParamsSave = new DataTable();
            siteParamsSave.Columns.Add("key");
            siteParamsSave.Columns.Add("value");

            DataRow siteParamSaveRow = siteParamsSave.NewRow();
            siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_PROCESSING_METHOD_VERIFIED;

            if (isValidated)
            { siteParamSaveRow["value"] = "True"; }
            else
            { siteParamSaveRow["value"] = "False"; }

            siteParamsSave.Rows.Add(siteParamSaveRow);
            AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, siteParamsSave);

            // re-load the global site object
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite);

            // build the page controls
            this._BuildControls();

            // display feedback
            if (isValidated)
            { this.DisplayFeedback(_GlobalResources.GatewayValidationWasSuccessful, false); }
            else
            { this.DisplayFeedback(_GlobalResources.GatewayValidationFailedDueToTheFollowingError_s + "<br /><br />" + validationErrors, true); }
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Performs the save action on the page.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _SaveButton_Command(object sender, EventArgs e)
        {
            try
            {
                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                DataTable siteParamsSave = new DataTable();
                siteParamsSave.Columns.Add("key");
                siteParamsSave.Columns.Add("value");

                DataTable siteParamsDelete = new DataTable();
                siteParamsDelete.Columns.Add("key");
                siteParamsDelete.Columns.Add("value");

                string existingProcessingMethod = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD);
                string newProcessingMethod = this._ProcessingMethod.SelectedValue;
                string existingCurrency = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_CURRENCY);
                string newCurrency = String.Empty; // will be populated when doing site param Ecommerce.Currency

                DataRow siteParamSaveRow;
                DataRow siteParamDeleteRow;
                bool deleteTermsAndConditions = false;
                bool clearPurchases = false;                

                // GATEWAY/PROCESSOR SETTINGS

                // Ecommerce.ProcessingMethod
                if (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_NONE)
                {
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_PROCESSING_METHOD;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);
                }
                else
                {
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_PROCESSING_METHOD;
                    siteParamSaveRow["value"] = newProcessingMethod;
                    siteParamsSave.Rows.Add(siteParamSaveRow);                    
                }

                // Ecommerce.ProcessingMethod.Verified
                if (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_NONE)
                {
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_PROCESSING_METHOD_VERIFIED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);
                }
                else
                {
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_PROCESSING_METHOD_VERIFIED;
                    
                    if (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL) // verified always true for PayPal since there is nothing to verify
                    { siteParamSaveRow["value"] = "True"; }
                    else
                    {
                        // if the processor (Authorize.Net) was already verified, and no settings have changed, keep it verified
                        // otherwise, set verified to false
                        if (
                            (
                             (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD_VERIFIED)
                             && existingProcessingMethod == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE 
                             && newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE
                             && AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_MODE) == this._AuthorizeNetMode.SelectedValue
                             && AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_LOGIN) == this._AuthorizeNetLogin.Text
                             && AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_TRANSACTIONKEY) == this._AuthorizeNetTransactionKey.Text
                             && AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_PUBLICKEY) == this._AuthorizeNetPublicKey.Text
                            )
                           )
                        { siteParamSaveRow["value"] = "True"; }
                        else
                        { siteParamSaveRow["value"] = "False"; }
                    }

                    siteParamsSave.Rows.Add(siteParamSaveRow);
                }

                // Ecommerce.Currency
                if (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_NONE)
                {
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_CURRENCY;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);
                }
                else
                {
                    if (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
                    { newCurrency = this._PayPalCurrency.SelectedValue; }
                    else if (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE)
                    { newCurrency = this._AuthorizeNetCurrency.SelectedValue; }
                    else
                    { }

                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_CURRENCY;
                    siteParamSaveRow["value"] = newCurrency;
                    siteParamsSave.Rows.Add(siteParamSaveRow);
                }

                // Ecommerce.AuthorizeNet.Mode
                // Ecommerce.AuthorizeNet.Login
                // Ecommerce.AuthorizeNet.TransactionKey
                // Ecommerce.AuthorizeNet.PublicKey
                if (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE)
                {
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_AUTHORIZENET_MODE;
                    siteParamSaveRow["value"] = this._AuthorizeNetMode.SelectedValue;
                    siteParamsSave.Rows.Add(siteParamSaveRow);
                    
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_AUTHORIZENET_LOGIN;
                    siteParamSaveRow["value"] = this._AuthorizeNetLogin.Text;
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_AUTHORIZENET_TRANSACTIONKEY;
                    siteParamSaveRow["value"] = this._AuthorizeNetTransactionKey.Text;
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_AUTHORIZENET_PUBLICKEY;
                    siteParamSaveRow["value"] = this._AuthorizeNetPublicKey.Text;
                    siteParamsSave.Rows.Add(siteParamSaveRow);
                }
                else
                {
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_AUTHORIZENET_MODE;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);
                    
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_AUTHORIZENET_LOGIN;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_AUTHORIZENET_TRANSACTIONKEY;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_AUTHORIZENET_PUBLICKEY;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);
                }

                // Ecommerce.PayPal.Mode
                // Ecommerce.PayPal.Login
                if (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
                {
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_PAYPAL_MODE;
                    siteParamSaveRow["value"] = this._PayPalMode.SelectedValue;
                    siteParamsSave.Rows.Add(siteParamSaveRow);
                    
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_PAYPAL_LOGIN;
                    siteParamSaveRow["value"] = this._PayPalLogin.Text;
                    siteParamsSave.Rows.Add(siteParamSaveRow);
                }
                else
                {
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_PAYPAL_MODE;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_PAYPAL_LOGIN;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);
                }

                // FORM PROPERTIES SETTINGS - Remove them if "None" or "PayPal"

                if (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_NONE || newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
                {
                    // Ecommerce.Visa.Enabled
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_VISA_ENABLED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.MasterCard.Enabled
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_MASTERCARD_ENABLED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.AMEX.Enabled
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_AMEX_ENABLED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.DiscoverCard.Enabled
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_DISCOVERCARD_ENABLED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.Name.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_NAME_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.CreditCardNum.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_CREDITCARDNUM_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.ExpirationDate.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_EXPIRATIONDATE_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.CVV2.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_CVV2_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.Company.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_COMPANY_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.Address.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_ADDRESS_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.City.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_CITY_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.StateProvince.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_STATEPROVINCE_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.PostalCode.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_POSTALCODE_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.Country.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_COUNTRY_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // Ecommerce.Field.Email.Required
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_EMAIL_REQUIRED;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);                    

                    // Ecommerce.ChargeDescription
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_CHARGEDESCRIPTION;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);

                    // set the flag to delete terms and conditions
                    deleteTermsAndConditions = true;
                }
                else // other processor (Authorize.Net) - save the settings
                {
                    // Ecommerce.Visa.Enabled
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_VISA_ENABLED;
                    siteParamSaveRow["value"] = this._VisaEnable.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.MasterCard.Enabled
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_MASTERCARD_ENABLED;
                    siteParamSaveRow["value"] = this._MasterCardEnable.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.AMEX.Enabled
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_AMEX_ENABLED;
                    siteParamSaveRow["value"] = this._AMEXEnable.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.DiscoverCard.Enabled
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_DISCOVERCARD_ENABLED;
                    siteParamSaveRow["value"] = this._DiscoverCardEnable.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.Name.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_NAME_REQUIRED;
                    siteParamSaveRow["value"] = this._RequireName.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.CreditCardNum.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_CREDITCARDNUM_REQUIRED;
                    siteParamSaveRow["value"] = this._RequireCreditCardNumber.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.ExpirationDate.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_EXPIRATIONDATE_REQUIRED;
                    siteParamSaveRow["value"] = this._RequireExpirationDate.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.CVV2.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_CVV2_REQUIRED;
                    siteParamSaveRow["value"] = this._RequireCVV2.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.Company.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_COMPANY_REQUIRED;
                    siteParamSaveRow["value"] = this._RequireCompany.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.Address.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_ADDRESS_REQUIRED;
                    siteParamSaveRow["value"] = this._RequireAddress.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.City.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_CITY_REQUIRED;
                    siteParamSaveRow["value"] = this._RequireCity.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.StateProvince.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_STATEPROVINCE_REQUIRED;
                    siteParamSaveRow["value"] = this._RequireStateProvince.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.PostalCode.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_POSTALCODE_REQUIRED;
                    siteParamSaveRow["value"] = this._RequirePostalCode.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.Country.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_COUNTRY_REQUIRED;
                    siteParamSaveRow["value"] = this._RequireCountry.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.Field.Email.Required
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_FIELD_EMAIL_REQUIRED;
                    siteParamSaveRow["value"] = this._RequireEmail.Checked.ToString();
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // Ecommerce.ChargeDescription
                    siteParamSaveRow = siteParamsSave.NewRow();
                    siteParamSaveRow["key"] = SiteParamConstants.ECOMMERCE_CHARGEDESCRIPTION;
                    siteParamSaveRow["value"] = this._ChargeDescription.Text;
                    siteParamsSave.Rows.Add(siteParamSaveRow);

                    // determine if terms and conditions should be saved or reset
                    if (String.IsNullOrWhiteSpace(this._TermsAndConditions.Text))
                    { deleteTermsAndConditions = true; }
                }

                // Ecommerce.CouponCode.GracePeriod - delete if processing method is "None"
                if (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_NONE)
                {
                    siteParamDeleteRow = siteParamsDelete.NewRow();
                    siteParamDeleteRow["key"] = SiteParamConstants.ECOMMERCE_COUPONCODE_GRACEPERIOD;
                    siteParamsDelete.Rows.Add(siteParamDeleteRow);
                }
                
                // DETERMINE IF OPEN CARTS SHOULD BE CLEARED, THEY SHOULD BE CLEARED IF:
                // 1) Processing method is "None."
                // 2) Currency changes
                // 3) Processing method changes from Authorize.Net to PayPal
                if (
                    (newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_NONE)
                    || (existingCurrency != newCurrency)
                    || (existingProcessingMethod == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE && newProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
                   )
                {
                    clearPurchases = true;
                }

                // save params
                if (siteParamsSave.Rows.Count > 0)
                { AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, siteParamsSave); }

                // delete params
                if (siteParamsDelete.Rows.Count > 0)
                { AsentiaSite.DeleteSiteParams(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, siteParamsDelete); }

                // delete or save the terms and contitions
                if (deleteTermsAndConditions)
                {
                    if (File.Exists(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + EcommerceSettings.TERMS_AND_CONDITIONS_FILENAME)))
                    { File.Delete(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + EcommerceSettings.TERMS_AND_CONDITIONS_FILENAME)); }
                }
                else // save
                {
                    using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + EcommerceSettings.TERMS_AND_CONDITIONS_FILENAME)))
                    { writer.Write(HttpUtility.HtmlDecode(this._TermsAndConditions.Text)); }
                }

                // clear purchases if they need to be cleared
                // note: this only clears active carts, completed purchases are retained
                if (clearPurchases)
                { Purchase.ClearAllUnconfirmed(); }

                // re-load the global site object
                AsentiaSessionState.GlobalSiteObject = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite);

                // build the page controls
                this._BuildControls();              

                // display the saved feedback
                this.DisplayFeedback(_GlobalResources.ECommerceSettingsHaveBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, EventArgs e)
        {
            Response.Redirect("~/dashboard");
        }
        #endregion
    }
}