﻿// Function to show/hide portions of the form based on the processing method selected
function ProcessingMethodChanged() {
    var selectedProcessingMethod = $("#ProcessingMethod_Field option:selected").val();

    if (selectedProcessingMethod == "authorize") {
        $("#PayPalSpecificGatewayProcessorControls").hide();        
        $("#NoneSpecificFormPropertiesControls").hide();
        $("#PayPalSpecificFormPropertiesControls").hide();

        $("#AuthorizeNetSpecificGatewayProcessorControls").show();
        $("#OtherProcessorsFormPropertiesControls").show();
    }
    else if (selectedProcessingMethod == "paypal") {        
        $("#AuthorizeNetSpecificGatewayProcessorControls").hide();
        $("#NoneSpecificFormPropertiesControls").hide();
        $("#OtherProcessorsFormPropertiesControls").hide();

        $("#PayPalSpecificGatewayProcessorControls").show();
        $("#PayPalSpecificFormPropertiesControls").show();        
    }
    else { // none
        $("#PayPalSpecificGatewayProcessorControls").hide();
        $("#AuthorizeNetSpecificGatewayProcessorControls").hide();        
        $("#PayPalSpecificFormPropertiesControls").hide();
        $("#OtherProcessorsFormPropertiesControls").hide();

        $("#NoneSpecificFormPropertiesControls").show();
    }
}

// Function used to select the checkbox on click of credit card image for Internet Explorer browser
function SelectCardCheckboxIE(ele, checkboxId) {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))
        if ($("#" + checkboxId).prop('checked')) {
            $("#" + checkboxId).prop('checked', false);
        }
        else {
            $("#" + checkboxId).prop('checked', true);
        }
}

// Function to kick off the Authorize.net verification
function DoAuthorizeNetVerification() {
    // validate the client-side gateway
    objAuthorizeNetAcceptValidate.SendPaymentData();
}

// Callback function to initiate Authorize.net verification postback after client-side verification is finished.
function AuthorizeNetVerificationResponseCallback() {
    // clear the credit card information from the fields so that it doesnt hit the server on postback
    $("#AUTHORIZEVALIDATE_CREDITCARDNUMBER").val("");
    $("#AUTHORIZEVALIDATE_EXPIRYMONTH").val("");
    $("#AUTHORIZEVALIDATE_EXPIRYYEAR").val("");

    // initiate the postback
    $("#AuthorizeNetVerificationHiddenPostbackButton").click();
}