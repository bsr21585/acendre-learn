﻿using System;
using System.IO;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.System.EmailNotifications
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel EmailNotificationsFormContentWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel EmailNotificationGridUpdatePanel;
        public Grid EmailNotificationGrid;
        public Panel ActionsPanel;
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup GridConfirmAction = new ModalPopup();
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_EmailNotifications))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.EmailNotifications));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.EmailNotifications, ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL,
                                                                                                                         ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.EmailNotificationsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the actions panel, and modal
            this._BuildGridActionsPanel();
            this._BuildGridActionsModal();

            // build and bind the grid
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            EmailNotificationGrid.BindData();

        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD EMAIL NOTIFICATION
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddEmailNotificationLink",
                                                null,
                                                "Modify.aspx",
                                                null,
                                                _GlobalResources.NewEmailNotification,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // apply css class to container
            this.EmailNotificationGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.EmailNotificationGrid.StoredProcedure = Library.EventEmailNotification.GridProcedure;
            this.EmailNotificationGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.EmailNotificationGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.EmailNotificationGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.EmailNotificationGrid.IdentifierField = "idEventEmailNotification";
            this.EmailNotificationGrid.DefaultSortColumn = "name";
            this.EmailNotificationGrid.SearchBoxPlaceholderText = _GlobalResources.SearchEmailNotifications;

            // data key names
            this.EmailNotificationGrid.DataKeyNames = new string[] { "idEventEmailNotification" };

            // columns
            GridColumn nameEventType = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.EventType + "", null, "name"); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.EmailNotificationGrid.AddColumn(nameEventType);

            // row data bound event
            this.EmailNotificationGrid.RowDataBound += new GridViewRowEventHandler(_EmailNotificationGrid_RowDataBound);
        }
        #endregion

        #region _EmailNotificationGrid_RowDataBound
        /// <summary>
        /// Row data bound event for email notifications grid. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _EmailNotificationGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idEventEmailNotification = Convert.ToInt32(rowView["idEventEmailNotification"]); // will never be null

                // AVATAR, NAME, EVENT TYPE

                string name = Server.HtmlEncode(rowView["name"].ToString());
                string eventType = EventEmailNotificationForm.GetEventTypeText(Convert.ToInt32(rowView["eventType"].ToString()));

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = name;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink nameLink = new HyperLink();
                nameLink.NavigateUrl = "Modify.aspx?id=" + idEventEmailNotification.ToString();
                nameLink.Text = name;
                nameLabel.Controls.Add(nameLink);

                // event type
                Label eventTypeLabel = new Label();
                eventTypeLabel.CssClass = "GridSecondaryLine";
                eventTypeLabel.Text = eventType;
                e.Row.Cells[1].Controls.Add(eventTypeLabel);
            }
        }
        #endregion

        #region _BuildGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "XSmallIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedEmailNotifications_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this.DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction.ID = "GridConfirmAction";

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedEmailNotifications_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            Literal body2 = new Literal();
            body1Wrapper.ID = "GridConfirmActionModalBody1";

            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseEmailNotification_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.EmailNotificationGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.EmailNotificationGrid.Rows[i].FindControl(this.EmailNotificationGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                if (recordsToDelete.Rows.Count > 0)
                {
                    // delete the records
                    Library.EventEmailNotification.Delete(recordsToDelete);

                    // delete related xml file
                    foreach (DataRow row in recordsToDelete.Rows)
                    {
                        string fullFilePath = Server.MapPath(SitePathConstants.SITE_EMAILNOTIFICATIONS_ROOT + row["id"].ToString() + "/EmailNotification.xml");
                        if (File.Exists(fullFilePath))
                            File.Delete(fullFilePath);
                    }

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedEmailNotification_sHaveBeenDeletedSuccessfully, false);

                    // rebind the grid
                    this.EmailNotificationGrid.BindData();
                }
                else
                {
                    //Display an error message that at least one record must be selected.
                    this.DisplayFeedback(_GlobalResources.PleaseSelectAtLeastOneRecordToDelete, true);
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this.EmailNotificationGrid.BindData();
            }
        }
        #endregion        
    }
}