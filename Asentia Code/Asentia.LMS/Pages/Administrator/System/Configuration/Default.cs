﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.System.Configuration
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ConfigurationFormContentWrapperContainer;
        public Panel ConfigurationWrapperContainer;
        public Panel PropertiesInstructionsPanel;
        public Panel ConfigurationPropertiesContainer;
        public Panel ConfigurationPropertiesTabPanelsContainer;
        public Panel PropertiesActionsPanel;
        public Panel UserAgreementContainer;
        #endregion

        #region Private Properties

        // LOGIN SETUP
        private RadioButtonList _SelfRegistrationList;
        private RadioButtonList _LoginPriorityList;
        private RadioButtonList _DefaultLandingPageList;
        private DropDownList _SelfRegistrationUrlProtocol;
        private UserAccountData _UserAccountDataObject;

        private TextBox _SelfRegistrationUrl;
        private TextBox _DefaultLandingPageLocalPath;
        private TextBox _UserAgreementTextBox;

        private CheckBox _AllowPasswordReset;
        private CheckBox _AllowSelfRegistration;
        private CheckBox _AllowSimultaneousLogin;
        private CheckBox _RequireRegistrationApproval;
        private CheckBox _AllowLockout;
        private DropDownList _LockoutMinutes;
        private DropDownList _LockoutAttempts;

        // WIDGETS
        private CheckBox _EnableEnrollmentsWidget;
        private RadioButtonList _EnrollmentsWidgetMode;
        private CheckBox _EnablePurchasesWidget;
        private CheckBox _EnableCertificatesWidget;
        private CheckBox _EnableCalendarWidget;
        private CheckBox _EnableFeedWidget;
        private CheckBox _EnableTranscriptWidget;
        private CheckBox _EnableMyCommunitiesWidget;
        private CheckBox _EnableMyLearningPathsWidget;
        private CheckBox _EnableLearnerLeaderboardsWidget;
        private CheckBox _EnableEnrollmentStatisticsWidget;
        private CheckBox _EnableAdministratorLeaderboardsWidget;
        private CheckBox _EnableAdministratorWallModerationWidget;
        //private CheckBox _EnableAdministratorTaskProctoringWidget; THIS IS PART OF ADMINISTRATIVE TASKS WIDGET
        //private CheckBox _EnableAdministratorOJTProctoringWidget; THIS IS PART OF ADMINISTRATIVE TASKS WIDGET
        private CheckBox _EnableAdministratorILTRosterManagementWidget;
        private CheckBox _EnableReportSubscriptionsWidget;
        private CheckBox _EnableReportShortcutsWidget;
        //private CheckBox _EnablePendingUserRegistrationsWidget; THIS IS PART OF ADMINISTRATIVE TASKS WIDGET
        //private CheckBox _EnablePendingCourseEnrollmentsWidget; THIS IS PART OF ADMINISTRATIVE TASKS WIDGET
        private CheckBox _EnableMyCertificationsWidget;
        //private CheckBox _EnableAdministratorCertificationTaskProctoringWidget; THIS IS PART OF ADMINISTRATIVE TASKS WIDGET
        private CheckBox _EnableDocumentsWidget;
        private CheckBox _EnableILTSessionsWidget;
        private CheckBox _EnableAdministrativeTasksWidget;

        // CATALOGS
        private CheckBox _EnableCatalog;
        private CheckBox _EnableCatalogSearch;
        private CheckBox _EnableCatalogEventCalendar;
        private CheckBox _CatalogRequireLogin;
        private CheckBox _ListPrivateCourseCatalogs;
        private CheckBox _HideCourseCodes;
        private CheckBox _EnableStandupTrainingBrowsing;
        private CheckBox _EnableLearningPathBrowsing;
        private CheckBox _EnableCommunityBrowsing;

        // LEADERBOARDS
        private CheckBox _EnableCourseTotalLeaderboard;
        private CheckBox _EnableCourseCreditsLeaderboard;
        private CheckBox _EnableCertificateTotalLeaderboard;
        private CheckBox _EnableCertificateCreditsLeaderboard;

        // RATINGS
        private CheckBox _EnableCourseRatings;
        private CheckBox _PublicizeCourseRatings;

        // PRIVACY
        private CheckBox _RequireCookieConsent;
        private CheckBox _AllowUserEmailOptOut;
        private CheckBox _PermanentlyRemoveDeletedUsers;
        private CheckBox _UserAgreementDisplayCheckBox;
        private CheckBox _UserAgreementRequiredCheckBox;
        private CheckBox _AllowSelfDeletion;

        private Button _SaveButton;
        private Button _CancelButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// On Prerender event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.System.Configuration.Default.js");

            // for multiple "start up" scripts, we need to build start up calls
            // and add them to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));            

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);

        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_Configuration))
            { Response.Redirect("/"); }

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.ConfigurationFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ConfigurationWrapperContainer.CssClass = "FormContentContainer";

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string configurationImagePath;
            string pageTitle;

            breadCrumbPageTitle = _GlobalResources.Configuration;

            configurationImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CONFIGURATION,
                                                            ImageFiles.EXT_PNG);

            pageTitle = _GlobalResources.Configuration;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, pageTitle, configurationImagePath);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the properties form
            this._BuildPropertiesForm();

            // build the properties form actions panel
            this._BuildPropertiesActionsPanel();

            // populate the form inputs
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _BuildPropertiesForm
        /// <summary>
        /// Builds the Properties form.
        /// </summary>
        private void _BuildPropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PropertiesInstructionsPanel, _GlobalResources.ConfigureThePropertiesUsingTheFormBelow, true);

            // clear controls from container
            this.ConfigurationPropertiesContainer.Controls.Clear();

            // build the properties form tabs
            this._BuildPropertiesFormTabs();

            // build tab panels container
            this.ConfigurationPropertiesTabPanelsContainer = new Panel();
            this.ConfigurationPropertiesTabPanelsContainer.ID = "ConfigurationProperties_TabPanelsContainer";
            this.ConfigurationPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.ConfigurationPropertiesContainer.Controls.Add(this.ConfigurationPropertiesTabPanelsContainer);

            // build input controls
            this._BuildPropertiesFormLoginSetupPanel();

            // only build the widgets panel if the dashboard mode is standard.  widgets cannot be configured in restricted dashboard mode.
            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard")
            {
            this._BuildPropertiesFormWidgetsPanel();
            }
            this._BuildPropertiesFormCatalogPanel();
            this._BuildPropertiesFormLeaderboardsPanel();
            this._BuildPropertiesFormRatingsPanel();
            this._BuildPropertiesFormPrivacyPanel();
        }
        #endregion

        #region _BuildPropertiesFormTabs
        /// <summary>
        /// Method to create the configuration page property tabs
        /// </summary>
        private void _BuildPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("LoginSetup", _GlobalResources.LoginSetup));

            // only build the widgets tab if the dashboard mode is standard.  widgets cannot be configured in restricted dashboard mode.
            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard")
            {
            tabs.Enqueue(new KeyValuePair<string, string>("Widgets", _GlobalResources.Widgets));
            }

            tabs.Enqueue(new KeyValuePair<string, string>("Catalog", _GlobalResources.Catalog));
            tabs.Enqueue(new KeyValuePair<string, string>("Leaderboards", _GlobalResources.Leaderboards));
            tabs.Enqueue(new KeyValuePair<string, string>("Ratings", _GlobalResources.Ratings));
            tabs.Enqueue(new KeyValuePair<string, string>("Privacy", _GlobalResources.Privacy));
            // build and attach the tabs
            this.ConfigurationPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("ConfigurationProperties", tabs, null, this.Page, null));
        }
        #endregion

        #region _BuildPropertiesFormLoginSetupPanel
        /// <summary>
        /// Method to build the properties for login setup tab
        /// </summary>
        private void _BuildPropertiesFormLoginSetupPanel()
        {
            // "Login Setup" is the default tab, so this is visible on page load.
            Panel loginSetupPanel = new Panel();
            loginSetupPanel.ID = "ConfigurationProperties_" + "LoginSetup" + "_TabPanel";
            loginSetupPanel.Attributes.Add("style", "display: block;");

            // self registration field
            List<Control> selfRegistrationInputControls = new List<Control>();

            // allow self-registration input
            this._AllowSelfRegistration = new CheckBox();
            this._AllowSelfRegistration.ID = "AllowSelfRegistration_Field";
            this._AllowSelfRegistration.Text = _GlobalResources.AllowUsersToCreateTheirOwnAccounts;
            this._AllowSelfRegistration.Attributes.Add("onclick", "AllowSelfRegistrationClick(this.id);");
            selfRegistrationInputControls.Add(this._AllowSelfRegistration);

            // require registration approval input
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERREGISTRATIONAPPROVAL_ENABLE))
            {
                this._RequireRegistrationApproval = new CheckBox();
                this._RequireRegistrationApproval.ID = "RequireRegistrationApproval_Field";
                this._RequireRegistrationApproval.Text = _GlobalResources.RequireNewUserRegistrationsToBeApprovedByAnAdministrator;

                this._RequireRegistrationApproval.Attributes.Add("onclick", "EnablePendingUserRegistrationsClick('RequireRegistrationApproval_Field')");
                selfRegistrationInputControls.Add(this._RequireRegistrationApproval);
            }

            // self registration options list
            this._SelfRegistrationList = new RadioButtonList();
            this._SelfRegistrationList.ID = "SelfRegistrationList_Field";
            this._SelfRegistrationList.RepeatDirection = RepeatDirection.Vertical;
            this._SelfRegistrationList.Items.Add(new ListItem(_GlobalResources.UseBuiltInRegistrationPage, "builtin"));
            this._SelfRegistrationList.Items.Add(new ListItem(_GlobalResources.UseCustomExternalInterface, "custom"));
            this._SelfRegistrationList.Attributes.Add("onclick", "SelfRegistrationListClick('SelfRegistrationList_Field');");
            selfRegistrationInputControls.Add(this._SelfRegistrationList);

            // self registration custom interface info
            Panel selfRegistrationCustomInfoPanel = new Panel();
            selfRegistrationCustomInfoPanel.ID = "SelfRegistrationCustomInfoPanel";
            this.FormatFormInformationPanel(selfRegistrationCustomInfoPanel, _GlobalResources.IfYouAreUsingAnExternalInterfaceForSelfRegistrationEnterTheURLBelow);
            selfRegistrationInputControls.Add(selfRegistrationCustomInfoPanel);


            // self registration custom interface url
            Panel selfRegistrationCustomUrlPanel = new Panel();
            selfRegistrationCustomUrlPanel.ID = "SelfRegistrationCustomUrlPanel";

            // protocol
            this._SelfRegistrationUrlProtocol = new DropDownList();
            this._SelfRegistrationUrlProtocol.ID = "SelfRegistrationUrlProtocol_Field";
            this._SelfRegistrationUrlProtocol.Items.Add(new ListItem("http://", "http://"));
            this._SelfRegistrationUrlProtocol.Items.Add(new ListItem("https://", "https://"));
            selfRegistrationCustomUrlPanel.Controls.Add(this._SelfRegistrationUrlProtocol);

            // url
            this._SelfRegistrationUrl = new TextBox();
            this._SelfRegistrationUrl.ID = "SelfRegistrationUrl_Field";
            this._SelfRegistrationUrl.CssClass = "InputLong";
            selfRegistrationCustomUrlPanel.Controls.Add(this._SelfRegistrationUrl);

            selfRegistrationInputControls.Add(selfRegistrationCustomUrlPanel);

            loginSetupPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("SelfRegistration",
                                                                                 _GlobalResources.SelfRegistration,
                                                                                 selfRegistrationInputControls,
                                                                                 false,
                                                                                 true));

            // reset password field
            this._AllowPasswordReset = new CheckBox();
            this._AllowPasswordReset.ID = "AllowPasswordReset_Field";
            this._AllowPasswordReset.Text = _GlobalResources.AllowUsersToResetForgottenPasswords;

            loginSetupPanel.Controls.Add(AsentiaPage.BuildFormField("AllowPasswordReset",
                                                             _GlobalResources.ResetPassword,
                                                             this._AllowPasswordReset.ID,
                                                             this._AllowPasswordReset,
                                                             false,
                                                             true,
                                                             false));

            // default landing page field
            List<Control> defaultLandingPageInputControls = new List<Control>();

            // default landing page options list
            this._DefaultLandingPageList = new RadioButtonList();
            this._DefaultLandingPageList.ID = "DefaultLandingPageList_Field";
            this._DefaultLandingPageList.RepeatDirection = RepeatDirection.Vertical;
            this._DefaultLandingPageList.Items.Add(new ListItem(_GlobalResources.Home, "/"));
            this._DefaultLandingPageList.Items.Add(new ListItem(_GlobalResources.Catalog, "/catalog"));
            this._DefaultLandingPageList.Items.Add(new ListItem(_GlobalResources.MyDashboard, "/dashboard"));
            this._DefaultLandingPageList.Items.Add(new ListItem(_GlobalResources.Other, "other"));
            this._DefaultLandingPageList.Attributes.Add("onClick", "DefaultLandingPageListClick('DefaultLandingPageList_Field')");
            defaultLandingPageInputControls.Add(this._DefaultLandingPageList);

            // default landing page local path
            Panel defaultLandingPageLocalPathPanel = new Panel();
            defaultLandingPageLocalPathPanel.ID = "DefaultLandingPageLocalPathPanel";

            // label
            Label defaultLandingPageLocalPathLabel = new Label();
            defaultLandingPageLocalPathLabel.Text = _GlobalResources.LocalPath;
            defaultLandingPageLocalPathPanel.Controls.Add(defaultLandingPageLocalPathLabel);

            // text box
            this._DefaultLandingPageLocalPath = new TextBox();
            this._DefaultLandingPageLocalPath.ID = "DefaultLandingPageLocalPath_Field";
            this._DefaultLandingPageLocalPath.CssClass = "InputMedium";
            this._DefaultLandingPageLocalPath.Text = "/";
            defaultLandingPageLocalPathPanel.Controls.Add(this._DefaultLandingPageLocalPath);

            defaultLandingPageInputControls.Add(defaultLandingPageLocalPathPanel);


            loginSetupPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("DefaultLandingPage",
                                                                                 _GlobalResources.DefaultLandingPage,
                                                                                 defaultLandingPageInputControls,
                                                                                 true,
                                                                                 true));

            // allow simultaneous login field
            this._AllowSimultaneousLogin = new CheckBox();
            this._AllowSimultaneousLogin.ID = "AllowSimultaneousLogin_Field";
            this._AllowSimultaneousLogin.Text = _GlobalResources.AllowMultipleSessionsOriginatingFromDifferentBrowserSessionsOrWorkstaionsToExistSimultaneously;
            this._AllowSimultaneousLogin.Attributes.Add("onClick", "AllowSimultaneousLoginClick(this.id)");

            loginSetupPanel.Controls.Add(AsentiaPage.BuildFormField("AllowSimultaneousLogin",
                                                             _GlobalResources.SimultaneousLogin,
                                                             this._AllowSimultaneousLogin.ID,
                                                             this._AllowSimultaneousLogin,
                                                             false,
                                                             true,
                                                             false));

            // login priority field
            this._LoginPriorityList = new RadioButtonList();
            this._LoginPriorityList.ID = "LoginPriority_Field";
            this._LoginPriorityList.RepeatDirection = RepeatDirection.Vertical;
            this._LoginPriorityList.Items.Add(new ListItem(_GlobalResources.FirstLogin, "first"));
            this._LoginPriorityList.Items.Add(new ListItem(_GlobalResources.LatestLogin, "latest"));
            this._LoginPriorityList.SelectedValue = "first";


            loginSetupPanel.Controls.Add(AsentiaPage.BuildFormField("LoginPriority",
                                                             _GlobalResources.LoginPriority,
                                                             this._LoginPriorityList.ID,
                                                             this._LoginPriorityList,
                                                             false,
                                                             true,
                                                             false));

            // lockout setting
            List<Control> lockoutInputControls = new List<Control>();

            // allow lockout
            this._AllowLockout = new CheckBox();
            this._AllowLockout.ID = "AllowLockout_Field";
            this._AllowLockout.Text = _GlobalResources.AllowLockout;
            this._AllowLockout.Attributes.Add("onclick", "AllowLockoutClick(this.id);");
            lockoutInputControls.Add(this._AllowLockout);

            Label lockUserAccountsFor = new Label();
            lockUserAccountsFor.Text = _GlobalResources.LockUserAccountsFor + " ";
            lockoutInputControls.Add(lockUserAccountsFor);

            // lockout minutes
            this._LockoutMinutes = new DropDownList();
            this._LockoutMinutes.ID = "LockoutMinutes_Field";
            this._LockoutMinutes.Items.Add(new ListItem("1", "1"));
            this._LockoutMinutes.Items.Add(new ListItem("3", "3"));
            this._LockoutMinutes.Items.Add(new ListItem("5", "5"));
            this._LockoutMinutes.Items.Add(new ListItem("10", "10"));
            this._LockoutMinutes.Items.Add(new ListItem("15", "15"));
            this._LockoutMinutes.Items.Add(new ListItem("30", "30"));
            this._LockoutMinutes.Items.Add(new ListItem("60", "60"));
            lockoutInputControls.Add(this._LockoutMinutes);

            Label minutesAfter = new Label();
            minutesAfter.Text = " " + _GlobalResources.Minute_sAfter + " ";
            lockoutInputControls.Add(minutesAfter);

            // lockout attempts
            this._LockoutAttempts = new DropDownList();
            this._LockoutAttempts.ID = "LockoutAttempts_Field";
            this._LockoutAttempts.Items.Add(new ListItem("2", "2"));
            this._LockoutAttempts.Items.Add(new ListItem("3", "3"));
            this._LockoutAttempts.Items.Add(new ListItem("4", "4"));
            this._LockoutAttempts.Items.Add(new ListItem("5", "5"));
            this._LockoutAttempts.Items.Add(new ListItem("6", "6"));
            this._LockoutAttempts.Items.Add(new ListItem("7", "7"));
            this._LockoutAttempts.Items.Add(new ListItem("8", "8"));
            this._LockoutAttempts.Items.Add(new ListItem("9", "9"));
            this._LockoutAttempts.Items.Add(new ListItem("10", "10"));
            lockoutInputControls.Add(this._LockoutAttempts);

            Label failedLoginAttempts = new Label();
            failedLoginAttempts.Text = " " + _GlobalResources.FailedLoginAttempt_s;
            lockoutInputControls.Add(failedLoginAttempts);

            loginSetupPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Lockout",
                                                                     _GlobalResources.Lockout,
                                                                     lockoutInputControls,
                                                                     false,
                                                                     true));

            // attach panel to container
            this.ConfigurationPropertiesTabPanelsContainer.Controls.Add(loginSetupPanel);
        }
        #endregion

        #region _BuildPropertiesFormWidgetsPanel
        /// <summary>
        /// Method to build the controls for widget tab
        /// </summary>
        private void _BuildPropertiesFormWidgetsPanel()
        {
            Panel widgetsPanel = new Panel();
            widgetsPanel.ID = "ConfigurationProperties_" + "Widgets" + "_TabPanel";
            widgetsPanel.Attributes.Add("style", "display: none;");

            // learner dashboard widgets field
            List<Control> learnerDashboardWidgetsInputControls = new List<Control>();

            // enrollments widget
            this._EnableEnrollmentsWidget = new CheckBox();
            this._EnableEnrollmentsWidget.ID = "EnableEnrollmentsWidget_Field";
            this._EnableEnrollmentsWidget.Text = _GlobalResources.EnrollmentsWidget;
            this._EnableEnrollmentsWidget.Enabled = false;
            this._EnableEnrollmentsWidget.Checked = true;
            learnerDashboardWidgetsInputControls.Add(this._EnableEnrollmentsWidget);

            // enrollments widget mode
            this._EnrollmentsWidgetMode = new RadioButtonList();
            this._EnrollmentsWidgetMode.ID = "EnrollmentsWidgetMode_Field";
            this._EnrollmentsWidgetMode.RepeatDirection = RepeatDirection.Vertical;
            this._EnrollmentsWidgetMode.Items.Add(new ListItem(_GlobalResources.ListViewEnrollmentsDisplayedInASortableList, "list"));
            this._EnrollmentsWidgetMode.Items.Add(new ListItem(_GlobalResources.TileViewEnrollmentsAreDisplayedAsGraphicalTiles, "tile"));

            Panel enrollmentsWidgetModePanel = AsentiaPage.BuildFormField("EnrollmentsWidgetMode",
                                                             null,
                                                             this._EnrollmentsWidgetMode.ID,
                                                             this._EnrollmentsWidgetMode,
                                                             false,
                                                             false,
                                                             false);

            // hide the form field separator
            ((Panel)AsentiaPage.FindControlRecursive(enrollmentsWidgetModePanel, "EnrollmentsWidgetMode_Container")).CssClass = "";

            learnerDashboardWidgetsInputControls.Add(enrollmentsWidgetModePanel);

            // purchases widget
            this._EnablePurchasesWidget = new CheckBox();
            this._EnablePurchasesWidget.ID = "EnablePurchasesWidget_Field";
            this._EnablePurchasesWidget.Text = _GlobalResources.PurchasesWidget;
            learnerDashboardWidgetsInputControls.Add(this._EnablePurchasesWidget);

            // certificates widget
            this._EnableCertificatesWidget = new CheckBox();
            this._EnableCertificatesWidget.ID = "EnableCertificatesWidget_Field";
            this._EnableCertificatesWidget.Text = _GlobalResources.CertificatesWidget;
            learnerDashboardWidgetsInputControls.Add(this._EnableCertificatesWidget);

            // calendar widget
            this._EnableCalendarWidget = new CheckBox();
            this._EnableCalendarWidget.ID = "EnableCalendarWidget_Field";
            this._EnableCalendarWidget.Text = _GlobalResources.CalendarWidget;
            learnerDashboardWidgetsInputControls.Add(this._EnableCalendarWidget);

            // feed widget
            this._EnableFeedWidget = new CheckBox();
            this._EnableFeedWidget.ID = "EnableFeedWidget_Field";
            this._EnableFeedWidget.Text = _GlobalResources.DiscussionFeedWidget;
            learnerDashboardWidgetsInputControls.Add(this._EnableFeedWidget);

            // transcript widget
            this._EnableTranscriptWidget = new CheckBox();
            this._EnableTranscriptWidget.ID = "EnableTranscriptWidget_Field";
            this._EnableTranscriptWidget.Text = _GlobalResources.TranscriptWidget;
            learnerDashboardWidgetsInputControls.Add(this._EnableTranscriptWidget);

            // my communities widget
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
            {
                this._EnableMyCommunitiesWidget = new CheckBox();
                this._EnableMyCommunitiesWidget.ID = "EnableMyCommunitiesWidget_Field";
                this._EnableMyCommunitiesWidget.Text = _GlobalResources.MyCommunitiesWidget;
                learnerDashboardWidgetsInputControls.Add(this._EnableMyCommunitiesWidget);
            }

            // my learning paths widget
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                this._EnableMyLearningPathsWidget = new CheckBox();
                this._EnableMyLearningPathsWidget.ID = "EnableMyLearningPathsWidget_Field";
                this._EnableMyLearningPathsWidget.Text = _GlobalResources.MyLearningPathsWidget;
                learnerDashboardWidgetsInputControls.Add(this._EnableMyLearningPathsWidget);
            }

            // my learner leaderboards widget
            this._EnableLearnerLeaderboardsWidget = new CheckBox();
            this._EnableLearnerLeaderboardsWidget.ID = "EnableLearnerLeaderboardsWidget_Field";
            this._EnableLearnerLeaderboardsWidget.Text = _GlobalResources.MyLeaderboardsWidget;
            learnerDashboardWidgetsInputControls.Add(this._EnableLearnerLeaderboardsWidget);

            // my certifications widget
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
            {
                this._EnableMyCertificationsWidget = new CheckBox();
                this._EnableMyCertificationsWidget.ID = "EnableMyCertificationsWidget_Field";
                this._EnableMyCertificationsWidget.Text = _GlobalResources.MyCertificationsWidget;
                learnerDashboardWidgetsInputControls.Add(this._EnableMyCertificationsWidget);
            }

            // my documents widget
            this._EnableDocumentsWidget = new CheckBox();
            this._EnableDocumentsWidget.ID = "EnableDocumentsWidget_Field";
            this._EnableDocumentsWidget.Text = _GlobalResources.DocumentsWidget;
            learnerDashboardWidgetsInputControls.Add(this._EnableDocumentsWidget);

            // instructor led training sessions
            this._EnableILTSessionsWidget = new CheckBox();
            this._EnableILTSessionsWidget.ID = "EnableILTSessionsWidget_Field";
            this._EnableILTSessionsWidget.Text = _GlobalResources.InstructorLedTrainingSessionsWidget;
            learnerDashboardWidgetsInputControls.Add(this._EnableILTSessionsWidget);

            widgetsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LearnerDashboardWidgets",
                                                                              _GlobalResources.LearnerWidgets,
                                                                              learnerDashboardWidgetsInputControls,
                                                                              false,
                                                                              true,
                                                                              true));
            
            // administrator dashboard widgets field
            List<Control> administratorDashboardWidgetsInputControls = new List<Control>();

            // enrollment statistics widget
            this._EnableEnrollmentStatisticsWidget = new CheckBox();
            this._EnableEnrollmentStatisticsWidget.ID = "_EnableEnrollmentStatisticsWidget_Field";
            this._EnableEnrollmentStatisticsWidget.Text = _GlobalResources.EnrollmentStatisticsWidget;
            administratorDashboardWidgetsInputControls.Add(this._EnableEnrollmentStatisticsWidget);

            // administrator leaderboards widget
            this._EnableAdministratorLeaderboardsWidget = new CheckBox();
            this._EnableAdministratorLeaderboardsWidget.ID = "EnableAdministratorMyLeaderboardsWidget_Field";
            this._EnableAdministratorLeaderboardsWidget.Text = _GlobalResources.TeamLeaderboardsWidget;
            administratorDashboardWidgetsInputControls.Add(this._EnableAdministratorLeaderboardsWidget);

            // administrator wall moderation widget
            this._EnableAdministratorWallModerationWidget = new CheckBox();
            this._EnableAdministratorWallModerationWidget.ID = "EnableAdministratorWallModerationWidget_Field";
            this._EnableAdministratorWallModerationWidget.Text = _GlobalResources.DiscussionModerationWidget;
            administratorDashboardWidgetsInputControls.Add(this._EnableAdministratorWallModerationWidget);

            /**************************************THIS IS PART OF ADMINISTRATIVE TASKS WIDGET************************
            // administrator task proctoring widget
            this._EnableAdministratorTaskProctoringWidget = new CheckBox();
            this._EnableAdministratorTaskProctoringWidget.ID = "EnableAdministratorTaskProctoringWidget_Field";
            this._EnableAdministratorTaskProctoringWidget.Text = _GlobalResources.TaskProctoringWidget;
            administratorDashboardWidgetsInputControls.Add(this._EnableAdministratorTaskProctoringWidget);
            *********************************************************************************************************/

            /**************************************THIS IS PART OF ADMINISTRATIVE TASKS WIDGET************************
            // administrator ojt proctoring widget
            this._EnableAdministratorOJTProctoringWidget = new CheckBox();
            this._EnableAdministratorOJTProctoringWidget.ID = "EnableAdministratorOJTProctoringWidget_Field";
            this._EnableAdministratorOJTProctoringWidget.Text = _GlobalResources.OJTProctoringWidget;
            administratorDashboardWidgetsInputControls.Add(this._EnableAdministratorOJTProctoringWidget);
            **********************************************************************************************************/

            // administrator ilt roster management widget
            this._EnableAdministratorILTRosterManagementWidget = new CheckBox();
            this._EnableAdministratorILTRosterManagementWidget.ID = "EnableAdministratorILTRosterManagementWidget_Field";
            this._EnableAdministratorILTRosterManagementWidget.Text = _GlobalResources.ILTRosterManagementWidget;
            administratorDashboardWidgetsInputControls.Add(this._EnableAdministratorILTRosterManagementWidget);

            /**************************************THIS IS PART OF ADMINISTRATIVE TASKS WIDGET************************
            // pending user registrations
            this._EnablePendingUserRegistrationsWidget = new CheckBox();
            this._EnablePendingUserRegistrationsWidget.ID = "PendingUserRegistrationsWidget_Field";
            this._EnablePendingUserRegistrationsWidget.Text = _GlobalResources.PendingUserRegistrations;
            this._EnablePendingUserRegistrationsWidget.Checked = true;  // Always will be checked, this is the only means to approve registrations
            this._EnablePendingUserRegistrationsWidget.Enabled = false; // Disable the check box so user cannot disable the widget
            administratorDashboardWidgetsInputControls.Add(this._EnablePendingUserRegistrationsWidget);
            ***********************************************************************************************************/

            /**************************************THIS IS PART OF ADMINISTRATIVE TASKS WIDGET************************
            // pending user registrations
            this._EnablePendingCourseEnrollmentsWidget = new CheckBox();
            this._EnablePendingCourseEnrollmentsWidget.ID = "PendingCourseEnrollmentsWidget_Field";
            this._EnablePendingCourseEnrollmentsWidget.Text = _GlobalResources.PendingCourseEnrollments;
            administratorDashboardWidgetsInputControls.Add(this._EnablePendingCourseEnrollmentsWidget);
            ***********************************************************************************************************/

            /**************************************THIS IS PART OF ADMINISTRATIVE TASKS WIDGET************************
            // certification task proctoring widget
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
            {
                this._EnableAdministratorCertificationTaskProctoringWidget = new CheckBox();
                this._EnableAdministratorCertificationTaskProctoringWidget.ID = "EnableAdministratorCertificationTaskProctoringWidget_Field";
                this._EnableAdministratorCertificationTaskProctoringWidget.Text = _GlobalResources.CertificationTaskProctoringWidget;
                administratorDashboardWidgetsInputControls.Add(this._EnableAdministratorCertificationTaskProctoringWidget);
            }
             *************************************************************************************************************/

            // administrative tasks widget
            this._EnableAdministrativeTasksWidget = new CheckBox();
            this._EnableAdministrativeTasksWidget.ID = "EnableAdministrativeTasksWidget_Field";
            this._EnableAdministrativeTasksWidget.Text = _GlobalResources.AdministrativeTasksWidget;
            administratorDashboardWidgetsInputControls.Add(this._EnableAdministrativeTasksWidget);


            widgetsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("AdministratorDashboardWidgets",
                                                                              _GlobalResources.AdministratorWidgets,
                                                                              administratorDashboardWidgetsInputControls,
                                                                              false,
                                                                              true,
                                                                              true));

            // reporter dashboard widgets field
            List<Control> reporterDashboardWidgetsInputControls = new List<Control>();

            // report subscription widget
            this._EnableReportSubscriptionsWidget = new CheckBox();
            this._EnableReportSubscriptionsWidget.ID = "EnableReportSubscriptionsWidget_Field";
            this._EnableReportSubscriptionsWidget.Text = _GlobalResources.ReportSubscriptionsWidget;
            reporterDashboardWidgetsInputControls.Add(this._EnableReportSubscriptionsWidget);

            // report shortcuts widget
            this._EnableReportShortcutsWidget = new CheckBox();
            this._EnableReportShortcutsWidget.ID = "EnableReportShortcutsWidget_Field";
            this._EnableReportShortcutsWidget.Text = _GlobalResources.ReportShortcutsWidget;
            reporterDashboardWidgetsInputControls.Add(this._EnableReportShortcutsWidget);

            widgetsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ReporterDashboardWidgets",
                                                                              _GlobalResources.ReporterWidgets,
                                                                              reporterDashboardWidgetsInputControls,
                                                                              false,
                                                                              true,
                                                                              true));

            // attach panel to container
            this.ConfigurationPropertiesTabPanelsContainer.Controls.Add(widgetsPanel);

        }
        #endregion

        #region _BuildPropertiesFormCatalogPanel
        /// <summary>
        /// Method to build the controls for catalog tab
        /// </summary>
        private void _BuildPropertiesFormCatalogPanel()
        {
            Panel catalogPanel = new Panel();
            catalogPanel.ID = "ConfigurationProperties_" + "Catalog" + "_TabPanel";
            catalogPanel.Attributes.Add("style", "display: none;");

            // enable catalog field
            this._EnableCatalog = new CheckBox();
            this._EnableCatalog.ID = "EnableCatalog_Field";
            this._EnableCatalog.ClientIDMode = ClientIDMode.Static;
            this._EnableCatalog.Attributes.Add("onClick", "EnableCatalogClick(this.id)");
            this._EnableCatalog.Text = _GlobalResources.LeavingThisUncheckedMeansThatUsersWillNotBeAbleToBrowseTheCatalog;

            catalogPanel.Controls.Add(AsentiaPage.BuildFormField("EnableCatalog",
                                                          _GlobalResources.EnableCatalog,
                                                          this._EnableCatalog.ID,
                                                          this._EnableCatalog,
                                                          false,
                                                          true,
                                                          false));

            // enable catalog search field
            this._EnableCatalogSearch = new CheckBox();
            this._EnableCatalogSearch.ID = "EnableCatalogSearch_Field";
            this._EnableCatalogSearch.ClientIDMode = ClientIDMode.Static;
            this._EnableCatalogSearch.Text = _GlobalResources.AllowUsersToSearchTheCatalog;

            catalogPanel.Controls.Add(AsentiaPage.BuildFormField("EnableCatalogSearch",
                                                          _GlobalResources.EnableCatalogSearch,
                                                          this._EnableCatalogSearch.ID,
                                                          this._EnableCatalogSearch,
                                                          false,
                                                          true,
                                                          false));

            // enable catalog event calendar field
            this._EnableCatalogEventCalendar = new CheckBox();
            this._EnableCatalogEventCalendar.ID = "EnableCatalogEventCalendar_Field";
            this._EnableCatalogEventCalendar.ClientIDMode = ClientIDMode.Static;
            this._EnableCatalogEventCalendar.Text = _GlobalResources.AllowUsersToViewTheLiveEventInstructorLedTrainingSchedule;

            catalogPanel.Controls.Add(AsentiaPage.BuildFormField("EnableCatalogEventCalendar",
                                                          _GlobalResources.EnableCatalogEventCalendar,
                                                          this._EnableCatalogEventCalendar.ID,
                                                          this._EnableCatalogEventCalendar,
                                                          false,
                                                          true,
                                                          false));

            // require login field
            this._CatalogRequireLogin = new CheckBox();
            this._CatalogRequireLogin.ID = "CatalogRequireLogin_Field";
            this._CatalogRequireLogin.ClientIDMode = ClientIDMode.Static;
            this._CatalogRequireLogin.Text = _GlobalResources.CheckToRequireUsersToLoginBeforeTheyAreAllowedToBrowseOrSearchTheCatalog;

            catalogPanel.Controls.Add(AsentiaPage.BuildFormField("CatalogRequireLogin",
                                                          _GlobalResources.RequireLogin,
                                                          this._CatalogRequireLogin.ID,
                                                          this._CatalogRequireLogin,
                                                          false,
                                                          true,
                                                          false));

            // list private course catalogs field
            this._ListPrivateCourseCatalogs = new CheckBox();
            this._ListPrivateCourseCatalogs.ClientIDMode = ClientIDMode.Static;
            this._ListPrivateCourseCatalogs.ID = "ListPrivateCourseCatalogs_Field";
            this._ListPrivateCourseCatalogs.Text = _GlobalResources.ListPrivateCourseCatalogNamesForUsersWhoDoNotHaveAccessRatherThanHidingThem;

            catalogPanel.Controls.Add(AsentiaPage.BuildFormField("ListPrivateCourseCatalogs",
                                                          _GlobalResources.ListPrivateCourseCatalogs,
                                                          this._ListPrivateCourseCatalogs.ID,
                                                          this._ListPrivateCourseCatalogs,
                                                          false,
                                                          true,
                                                          false));

            // hide course codes field
            this._HideCourseCodes = new CheckBox();
            this._HideCourseCodes.ID = "HideCourseCodes_Field";
            this._HideCourseCodes.ClientIDMode = ClientIDMode.Static;
            this._HideCourseCodes.Text = _GlobalResources.HideCourseCodesForPublicCatalogView;

            catalogPanel.Controls.Add(AsentiaPage.BuildFormField("HideCourseCodes",
                                                          _GlobalResources.HideCourseCodes,
                                                          this._HideCourseCodes.ID,
                                                          this._HideCourseCodes,
                                                          false,
                                                          true,
                                                          false));

            // enable standup training field
            this._EnableStandupTrainingBrowsing = new CheckBox();
            this._EnableStandupTrainingBrowsing.ClientIDMode = ClientIDMode.Static;
            this._EnableStandupTrainingBrowsing.ID = "EnableStandupTraining_Field";
            this._EnableStandupTrainingBrowsing.Text = _GlobalResources.LeavingThisUncheckMeansThatUsersWillNotBeAbleToBrowseTheInstructorLedTrainingTab;

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE))
            {
                catalogPanel.Controls.Add(AsentiaPage.BuildFormField("EnableStandupTraining",
                                                              _GlobalResources.EnableInstructorLedTrainingBrowsing,
                                                              this._EnableStandupTrainingBrowsing.ID,
                                                              this._EnableStandupTrainingBrowsing,
                                                              false,
                                                              true,
                                                              false));
            }

            // enable learning path field
            this._EnableLearningPathBrowsing = new CheckBox();
            this._EnableLearningPathBrowsing.ID = "EnableLearningPath_Field";
            this._EnableLearningPathBrowsing.ClientIDMode = ClientIDMode.Static;
            this._EnableLearningPathBrowsing.Text = _GlobalResources.LeavingThisUncheckedMeansThatUsersWillNotBeAbleToBrowseTheLearningPathsTab;

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                catalogPanel.Controls.Add(AsentiaPage.BuildFormField("EnableLearningPath",
                                                              _GlobalResources.EnableLearningPathBrowsing,
                                                              this._EnableLearningPathBrowsing.ID,
                                                              this._EnableLearningPathBrowsing,
                                                              false,
                                                              true,
                                                              false));
            }

            // enable community field
            this._EnableCommunityBrowsing = new CheckBox();
            this._EnableCommunityBrowsing.ID = "EnableCommunity_Field";
            this._EnableCommunityBrowsing.ClientIDMode = ClientIDMode.Static;
            this._EnableCommunityBrowsing.Text = _GlobalResources.LeavingThisUncheckedMeansThatUsersWillNotBeAbleToBrowseTheCommunitiesTab;

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
            {
                catalogPanel.Controls.Add(AsentiaPage.BuildFormField("EnableCommunity",
                                                              _GlobalResources.EnableCommunityBrowsing,
                                                              this._EnableCommunityBrowsing.ID,
                                                              this._EnableCommunityBrowsing,
                                                              false,
                                                              true,
                                                              false));
            }

            // attach panel to container
            this.ConfigurationPropertiesTabPanelsContainer.Controls.Add(catalogPanel);
        }
        #endregion

        #region _BuildPropertiesFormLeaderboardsPanel
        /// <summary>
        /// Method to build the controls for leadger board tab
        /// </summary>
        private void _BuildPropertiesFormLeaderboardsPanel()
        {
            Panel leaderboardsPanel = new Panel();
            leaderboardsPanel.ID = "ConfigurationProperties_" + "Leaderboards" + "_TabPanel";
            leaderboardsPanel.Attributes.Add("style", "display: none;");

            // enable course total leaderboard field
            this._EnableCourseTotalLeaderboard = new CheckBox();
            this._EnableCourseTotalLeaderboard.ID = "EnableCourseTotalLeaderboard_Field";
            this._EnableCourseTotalLeaderboard.ClientIDMode = ClientIDMode.Static;
            this._EnableCourseTotalLeaderboard.Text = _GlobalResources.LeadersByTotalNumberOfCoursesCompleted;

            leaderboardsPanel.Controls.Add(AsentiaPage.BuildFormField("EnableCourseTotalLeaderboard",
                                                                      _GlobalResources.CourseTotal,
                                                                      this._EnableCourseTotalLeaderboard.ID,
                                                                      this._EnableCourseTotalLeaderboard,
                                                                      false,
                                                                      true,
                                                                      false));


            // enable course credits leaderboard field
            this._EnableCourseCreditsLeaderboard = new CheckBox();
            this._EnableCourseCreditsLeaderboard.ID = "EnableCourseCreditsLeaderboard_Field";
            this._EnableCourseCreditsLeaderboard.ClientIDMode = ClientIDMode.Static;
            this._EnableCourseCreditsLeaderboard.Text = _GlobalResources.LeadersByTotalNumberOfCourseCreditsEarned;

            leaderboardsPanel.Controls.Add(AsentiaPage.BuildFormField("EnableCourseCreditsLeaderboard",
                                                                      _GlobalResources.CourseCredits,
                                                                      this._EnableCourseCreditsLeaderboard.ID,
                                                                      this._EnableCourseCreditsLeaderboard,
                                                                      false,
                                                                      true,
                                                                      false));

            // enable certificate total leaderboard field
            this._EnableCertificateTotalLeaderboard = new CheckBox();
            this._EnableCertificateTotalLeaderboard.ClientIDMode = ClientIDMode.Static;
            this._EnableCertificateTotalLeaderboard.ID = "EnableCertificateTotalLeaderboard_Field";
            this._EnableCertificateTotalLeaderboard.Text = _GlobalResources.LeadersByTotalNumberOfCertificatesEarned;

            leaderboardsPanel.Controls.Add(AsentiaPage.BuildFormField("EnableCertificateTotalLeaderboard",
                                                                      _GlobalResources.CertificateTotal,
                                                                      this._EnableCertificateTotalLeaderboard.ID,
                                                                      this._EnableCertificateTotalLeaderboard,
                                                                      false,
                                                                      true,
                                                                      false));

            // enable certificate credits leaderboard field
            this._EnableCertificateCreditsLeaderboard = new CheckBox();
            this._EnableCertificateCreditsLeaderboard.ID = "EnableCertificateCreditsLeaderboard_Field";
            this._EnableCertificateCreditsLeaderboard.ClientIDMode = ClientIDMode.Static;
            this._EnableCertificateCreditsLeaderboard.Text = _GlobalResources.LeadersByTotalNumberOfCertificateCreditsEarned;

            leaderboardsPanel.Controls.Add(AsentiaPage.BuildFormField("EnableCertificateCreditsLeaderboard",
                                                          _GlobalResources.CertificateCredits,
                                                          this._EnableCertificateCreditsLeaderboard.ID,
                                                          this._EnableCertificateCreditsLeaderboard,
                                                          false,
                                                          true,
                                                          false));

            // attach panel to container
            this.ConfigurationPropertiesTabPanelsContainer.Controls.Add(leaderboardsPanel);
        }
        #endregion

        #region _BuildPropertiesFormRatingsPanel
        /// <summary>
        /// Method to build the controls for rating tab
        /// </summary>
        private void _BuildPropertiesFormRatingsPanel()
        {
            Panel ratingsPanel = new Panel();
            ratingsPanel.ID = "ConfigurationProperties_" + "Ratings" + "_TabPanel";
            ratingsPanel.Attributes.Add("style", "display: none;");

            // course ratings field
            List<Control> courseRatingsInputControls = new List<Control>();

            // enable course ratings
            this._EnableCourseRatings = new CheckBox();
            this._EnableCourseRatings.ID = "EnableCourseRatings_Field";
            this._EnableCourseRatings.ClientIDMode = ClientIDMode.Static;
            this._EnableCourseRatings.Text = _GlobalResources.EnableCourseRatings;
            this._EnableCourseRatings.Attributes.Add("onclick", "EnableCourseRatingsClick('EnableCourseRatings_Field');");
            courseRatingsInputControls.Add(this._EnableCourseRatings);

            // publicize course ratings
            this._PublicizeCourseRatings = new CheckBox();
            this._PublicizeCourseRatings.ID = "PublicizeCourseRatings_Field";
            this._PublicizeCourseRatings.ClientIDMode = ClientIDMode.Static;
            this._PublicizeCourseRatings.Text = _GlobalResources.PublicizeCourseRatingsAverageScoreIsVisibleToUsers;
            courseRatingsInputControls.Add(this._PublicizeCourseRatings);

            ratingsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("EnableCourseRatings",
                                                                              _GlobalResources.CourseRatings,
                                                                              courseRatingsInputControls,
                                                                              false,
                                                                              true,
                                                                              true));

            // attach panel to container
            this.ConfigurationPropertiesTabPanelsContainer.Controls.Add(ratingsPanel);
        }
        #endregion

        #region _BuildPropertiesFormPrivacyPanel
        /// <summary>
        /// Method to build the controls for Privacy tab
        /// </summary>
        private void _BuildPropertiesFormPrivacyPanel()
        {
            // Privacy field
            Panel privacyPanel = new Panel();
            privacyPanel.ID = "ConfigurationProperties_" + "Privacy" + "_TabPanel";
            privacyPanel.Attributes.Add("style", "display: none;");

            // Cookie Alert
            List<Control> cookieAlertInputControls = new List<Control>();

            this._RequireCookieConsent = new CheckBox();
            this._RequireCookieConsent.ID = "RequireCookieConsent_Field";
            this._RequireCookieConsent.ClientIDMode = ClientIDMode.Static;
            this._RequireCookieConsent.Text = _GlobalResources.DisplayTheCookieConsentAlertToAllUsersWhenTheyFirstAccessThisSystem;
            cookieAlertInputControls.Add(this._RequireCookieConsent);

            privacyPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("RequireCookieConsent",
                                                                              _GlobalResources.RequireCookieConsent,
                                                                              cookieAlertInputControls,
                                                                              false,
                                                                              true,
                                                                              true));

            List<Control> gdprInputControls = new List<Control>();

            // allow user email optout
            this._AllowUserEmailOptOut = new CheckBox();
            this._AllowUserEmailOptOut.ID = "AllowUsersToOptOutEmailNotifications_Field";
            this._AllowUserEmailOptOut.ClientIDMode = ClientIDMode.Static;
            this._AllowUserEmailOptOut.Text = _GlobalResources.AllowUsersToOptOutOfEmailNotifications;
            gdprInputControls.Add(this._AllowUserEmailOptOut);

            privacyPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("AllowUsersToOptOutEmailNotifications",
                                                                              _GlobalResources.Communications,
                                                                              gdprInputControls,
                                                                              false,
                                                                              true,
                                                                              true));

            // User Deletion
            List<Control> userDeletionInputControls = new List<Control>();

            // permanently remove deleted users from system (hard delete)
            this._PermanentlyRemoveDeletedUsers = new CheckBox();
            this._PermanentlyRemoveDeletedUsers.ID = "PermanentlyRemoveDeletedUsersFromSystem_Field";
            this._PermanentlyRemoveDeletedUsers.ClientIDMode = ClientIDMode.Static;
            this._PermanentlyRemoveDeletedUsers.Text = _GlobalResources.PermanentlyRemoveDeletedUsersFromSystem;
            userDeletionInputControls.Add(this._PermanentlyRemoveDeletedUsers);

            privacyPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("PermanentlyRemoveDeletedUsersFromSystem",
                                                                              _GlobalResources.UserDeletion,
                                                                              userDeletionInputControls,
                                                                              false,
                                                                              true,
                                                                              true));

            // User Agreement
            List<Control> userAgreementControls = new List<Control>();

            Panel userAgreementRequiredCheckBoxContainer = new Panel();
            userAgreementRequiredCheckBoxContainer.ID = "uad_UserAgreementRequiredCheckBoxContainer";
            userAgreementRequiredCheckBoxContainer.CssClass = "UserAgreementRequiredCheckBoxContainer";

            this._UserAgreementDisplayCheckBox = new CheckBox();
            this._UserAgreementDisplayCheckBox.ID = "uad_UserAgreementDisplayCheckBox";
            this._UserAgreementDisplayCheckBox.ClientIDMode = ClientIDMode.Static;
            this._UserAgreementDisplayCheckBox.Text = _GlobalResources.DisplayTheUserAgreementToUsers;
            this._UserAgreementDisplayCheckBox.Attributes.Add("onClick", "EnableDisableUserAgreementCheckBox();");
            userAgreementControls.Add(this._UserAgreementDisplayCheckBox);

            this._UserAgreementRequiredCheckBox = new CheckBox();
            this._UserAgreementRequiredCheckBox.ID = "uad_UserAgreementRequiredCheckBox";
            this._UserAgreementRequiredCheckBox.Text = _GlobalResources.RequireUsersToAgreeWhenRegistering;
            userAgreementControls.Add(this._UserAgreementRequiredCheckBox);

            privacyPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("UserAgreementOptions",
                                                                  _GlobalResources.UserAgreementOptions,
                                                                  userAgreementControls,
                                                                  false,
                                                                  true,
                                                                  true));

            // User Agreement Text Box
            this._UserAgreementTextBox = new TextBox();
            this._UserAgreementTextBox.ID = "UserAgreementTextBox_Field";
            this._UserAgreementTextBox.CssClass = "ckeditor";
            this._UserAgreementTextBox.Style.Add("width", "100%");
            this._UserAgreementTextBox.TextMode = TextBoxMode.MultiLine;
            this._UserAgreementTextBox.Rows = 10;

            privacyPanel.Controls.Add(AsentiaPage.BuildFormField("UserAgreementTextBox",
                                                             null,
                                                             this._UserAgreementTextBox.ID,
                                                             this._UserAgreementTextBox,
                                                             false,
                                                             true,
                                                             true));

            // Allow Self-Deletion
            List<Control> allowSelfDeletion = new List<Control>();

            this._AllowSelfDeletion = new CheckBox();
            this._AllowSelfDeletion.ID = "AllowSelfDeletionCheckBox";
            this._AllowSelfDeletion.ClientIDMode = ClientIDMode.Static;
            this._AllowSelfDeletion.Text = _GlobalResources.AllowUsersToPermanentlyDeleteTheirOwnProfile;
            allowSelfDeletion.Add(this._AllowSelfDeletion);

            privacyPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("AllowSelfDeletion",
                                                                              _GlobalResources.AllowSelfDeletion,
                                                                              allowSelfDeletion,
                                                                              false,
                                                                              true,
                                                                              true));

            // attach panel to container
            this.ConfigurationPropertiesTabPanelsContainer.Controls.Add(privacyPanel);
        }
        #endregion

        #region _BuildPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for properties actions.
        /// </summary>
        private void _BuildPropertiesActionsPanel()
        {
            // clear controls from container
            this.PropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.PropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this.PropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.PropertiesActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            // LOGIN SETUP

            // allow self registration
            this._AllowSelfRegistration.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_SELFREGISTRATION_ENABLED);

            // self registration option and url
            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_SELFREGISTRATION_TYPE) == "custom")
            {
                this._SelfRegistrationList.SelectedValue = "custom";
                this._SelfRegistrationUrlProtocol.SelectedValue = AsentiaSessionState.GlobalSiteObject .ParamString(SiteParamConstants.SYSTEM_SELFREGISTRATION_URLPREFIX);
                this._SelfRegistrationUrl.Text = AsentiaSessionState.GlobalSiteObject .ParamString(SiteParamConstants.SYSTEM_SELFREGISTRATION_URL);
            }
            else
            {
                this._SelfRegistrationList.SelectedValue = "builtin";
                this._SelfRegistrationUrlProtocol.SelectedValue = "http://";
                this._SelfRegistrationUrl.Text = String.Empty;
            }

            // allow reset password
            this._AllowPasswordReset.Checked = (bool)AsentiaSessionState.GlobalSiteObject .ParamBool(SiteParamConstants.SYSTEM_PASSWORDRESET_ENABLED);

            // default landing page
            this._DefaultLandingPageList.SelectedValue = AsentiaSessionState.GlobalSiteObject .ParamString(SiteParamConstants.SYSTEM_DEFAULTLANDINGPAGE);

            if (this._DefaultLandingPageList.SelectedValue == "other")
            { this._DefaultLandingPageLocalPath.Text = AsentiaSessionState.GlobalSiteObject .ParamString(SiteParamConstants.SYSTEM_DEFAULTLANDINGPAGE_LOCALPATH); }
            else
            { this._DefaultLandingPageLocalPath.Text = this._DefaultLandingPageList.SelectedValue; }

            // allow simultaneous login
            this._AllowSimultaneousLogin.Checked = (bool)AsentiaSessionState.GlobalSiteObject .ParamBool(SiteParamConstants.SYSTEM_SIMULTANEOUSLOGIN_ENABLED);

            // login priority
            this._LoginPriorityList.SelectedValue = AsentiaSessionState.GlobalSiteObject .ParamString(SiteParamConstants.SYSTEM_LOGINPRIORITY);

            // require account registration approval
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERREGISTRATIONAPPROVAL_ENABLE))
            {
                this._RequireRegistrationApproval.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_ACCOUNTREGISTRATIONAPPROVAL);
            }

            // lockout seeting
            // allow lockout
            this._AllowLockout.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_LOGOUT_ENABLED);
            
            // lockout minutes and failed login attempts
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_LOGOUT_ENABLED))
            {
                this._LockoutMinutes.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_LOGOUT_MINUTES);
                this._LockoutAttempts.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_LOGOUT_ATTEMPTS);
            }

            // only populate the widgets checkboxes if the dashboard mode is standard.  widgets cannot be configured in restricted dashboard mode.
            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard")
            {
            // WIDGETS - LEARNER

            // enrollments widget mode
            this._EnrollmentsWidgetMode.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WIDGET_LEARNER_ENROLLMENTS_MODE);

            // purchases
                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD) == "none")
            {
                this._EnablePurchasesWidget.Checked = false;
                this._EnablePurchasesWidget.Enabled = false;
            }
            else
                { this._EnablePurchasesWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_PURCHASES_ENABLED); }

            // certificates
                this._EnableCertificatesWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_CERTIFICATES_ENABLED);

            // calendar
                this._EnableCalendarWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_CALENDAR_ENABLED);

            // feed
                this._EnableFeedWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_FEED_ENABLED);

            // transcript
                this._EnableTranscriptWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_TRANSCRIPT_ENABLED);

            // my communities
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
            { this._EnableMyCommunitiesWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_MYCOMMUNITIES_ENABLED); }
                
            // my learning paths
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            { this._EnableMyLearningPathsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_MYLEARNINGPATHS_ENABLED); }

            // leaderboards (LEARNER)
            this._EnableLearnerLeaderboardsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_LEADERBOARDS_ENABLED);

            // my certifications
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
            { this._EnableMyCertificationsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_MYCERTIFICATIONS_ENABLED); }

            // documents
            this._EnableDocumentsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_DOCUMENTS_ENABLED);

            // ilt sessions
            this._EnableILTSessionsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_LEARNER_ILTSESSIONS_ENABLED);

            // WIDGETS - ADMINISTRATOR

            // enrollment statistics
            this._EnableEnrollmentStatisticsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_ENROLLMENTSTATISTICS_ENABLED);

            // leaderboards (ADMINISTRATOR)
            this._EnableAdministratorLeaderboardsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_LEADERBOARDS_ENABLED);

            // wall moderation
            this._EnableAdministratorWallModerationWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_WALLMODERATION_ENABLED);

            /**************************************THIS IS PART OF ADMINISTRATIVE TASKS WIDGET************************
            // task proctoring
            this._EnableAdministratorTaskProctoringWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_TASKPROCTORING_ENABLED);

            // ojt proctoring
            this._EnableAdministratorOJTProctoringWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_OJTPROCTORING_ENABLED);
             * *******************************************************************************************************/

            // ilt roster management
            this._EnableAdministratorILTRosterManagementWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_ILTROSTERMANAGEMENT_ENABLED);

            /**************************************THIS IS PART OF ADMINISTRATIVE TASKS WIDGET************************
            // pending user registrations - don't get the value from param, it will be controlled by the state of "require registrations to be approved" 
            // this._EnablePendingUserRegistrationsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_PENDINGUSERREGISTRATIONS_ENABLED);

            // pending course enrollments
            this._EnablePendingCourseEnrollmentsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_PENDINGCOURSEENROLLMENTS_ENABLED);

            // certification task proctoring
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
            { this._EnableAdministratorCertificationTaskProctoringWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_CERTIFICATIONTASKPROCTORING_ENABLED); }
             **************************************************************************************************************/

            // administrative tasks
            this._EnableAdministrativeTasksWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_ADMINISTRATOR_ADMINISTRATIVETASKS_ENABLED);
            // WIDGETS - REPORTER

            // report subscriptions
            this._EnableReportSubscriptionsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_REPORTER_REPORTSUBSCRIPTIONS_ENABLED);

            // report shortcuts
            this._EnableReportShortcutsWidget.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WIDGET_REPORTER_REPORTSHORTCUTS_ENABLED);
            }
            // CATALOG

            // enable catalog
            this._EnableCatalog.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_ENABLE);

            // enable catalog search
            this._EnableCatalogSearch.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_SEARCH_ENABLE);

            // enable catalog event calendar
            this._EnableCatalogEventCalendar.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_EVENTCALENDAR_ENABLE);

            // require login
            this._CatalogRequireLogin.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_REQUIRE_LOGIN);

            // list private course catalogs
            this._ListPrivateCourseCatalogs.Checked = (bool)AsentiaSessionState.GlobalSiteObject .ParamBool(SiteParamConstants.CATALOG_SHOW_PRIVATECOURSECATALOGS);

            // hide course codes
            this._HideCourseCodes.Checked = (bool)AsentiaSessionState.GlobalSiteObject .ParamBool(SiteParamConstants.CATALOG_HIDE_COURSE_CODES);

            // enable standuptraining browsing
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE))
            { this._EnableStandupTrainingBrowsing.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_STANDUPTRAINING_ENABLE); }

            // enable learning paths browsing
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            { this._EnableLearningPathBrowsing.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_LEARNINGPATHS_ENABLE); }

            // enable community browsing
            this._EnableCommunityBrowsing.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_COMMUNITY_ENABLE);

            // LEADERBOARDS

            // enable course total leaderboard
            this._EnableCourseTotalLeaderboard.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSETOTAL_ENABLE);

            // enable course credits leaderboard
            this._EnableCourseCreditsLeaderboard.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSECREDITS_ENABLE);

            // enable certificate total leaderboard
            this._EnableCertificateTotalLeaderboard.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATETOTAL_ENABLE);

            // enable certificate credits leaderboard
            this._EnableCertificateCreditsLeaderboard.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATECREDITS_ENABLE);

            // RATINGS

            // course ratings
            this._EnableCourseRatings.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RATINGS_COURSE_ENABLE);
            this._PublicizeCourseRatings.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RATINGS_COURSE_PUBLICIZE);

            // Privacy

            // require cookie consent
            this._RequireCookieConsent.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.PRIVACY_REQUIRECOOKIECONSENT);

            // allow users to opt out of email notifications
            this._AllowUserEmailOptOut.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.PRIVACY_ALLOWUSEREMAILOPTOUT);
            // permanently remove deleted users from system
            this._PermanentlyRemoveDeletedUsers.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.PRIVACY_PERMANENTLYREMOVEDELETEDUSERS);

            // User Agreement Options
            // create the user account data object
            this._UserAccountDataObject = new UserAccountData(UserAccountDataFileType.Site, true, false);

            // User Agreement
            foreach (string availableLanguage in this.GetArrayListOfSiteAvailableInstalledLanguages())
            {
                TextBox languageSpecificUserAgreementTextBox = (TextBox)this.ConfigurationPropertiesContainer.FindControl((availableLanguage == AsentiaSessionState.GlobalSiteObject.LanguageString) ? this._UserAgreementTextBox.ID : this._UserAgreementTextBox.ID + "_" + availableLanguage);

                // if the text boxes were found and they have values, set the properties
                if (languageSpecificUserAgreementTextBox != null)
                {
                    languageSpecificUserAgreementTextBox.Text = this._UserAccountDataObject.GetUserAgreementHTMLInLanguage(availableLanguage);
                }
            }

            this._UserAgreementDisplayCheckBox.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERAGREEMENT_DISPLAY);
            this._UserAgreementRequiredCheckBox.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERAGREEMENT_REQUIRED);
            this._AllowSelfDeletion.Checked = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.PRIVACY_ALLOWSELFDELETION);
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;
            bool loginSetupTabHasErrors = false;
            bool widgetsTabHasErrors = false;
            bool catalogTabHasErrors = false;
            bool privacyTabHasErrors = false;

            // LOGIN SETUP

            // SELF REGISTRATION - IF "CUSTOM", THERE MUST BE A VALID URL
            if (this._AllowSelfRegistration.Checked && this._SelfRegistrationList.SelectedValue == "custom")
            {
                string fullUrl = this._SelfRegistrationUrlProtocol.Text + this._SelfRegistrationUrl.Text;

                if (!String.IsNullOrWhiteSpace(fullUrl) && !RegExValidation.ValidateUrl(fullUrl))
                {
                    isValid = false;
                    loginSetupTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.ConfigurationPropertiesContainer, "SelfRegistration", _GlobalResources.SelfRegistrationURLIsInvalid);
                }
            }

            // DEFAULT LANDING PAGE - IF "OTHER", THERE MUST BE A LOCAL PATH SPECIFIED
            if (this._DefaultLandingPageList.SelectedValue == "other")
            {
                if (String.IsNullOrWhiteSpace(this._DefaultLandingPageLocalPath.Text))
                {
                    isValid = false;
                    loginSetupTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.ConfigurationPropertiesContainer, "DefaultLandingPage", _GlobalResources.DefaultLandingPage + " " + _GlobalResources.IsRequired);
                }
            }

            TextBox userAgreementSiteLanguage = (TextBox)this.ConfigurationPropertiesContainer.FindControl(this._UserAgreementTextBox.ID);

            // USER AGREEMENT REQUIRED AND DEFAULT LANGUAGE USER AGREEMENT IS EMPTY
            if (this._UserAgreementRequiredCheckBox.Checked)
            {
                if (String.IsNullOrEmpty(this._UserAgreementTextBox.Text))
                {
                    isValid = false;
                    privacyTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.ConfigurationPropertiesContainer, "UserAgreementTextBox", _GlobalResources.UserAgreementInSiteDefaultLanguageIsRequired);                
                }
            }

            // apply error image and class to tabs if they have errors
            if (loginSetupTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.ConfigurationPropertiesContainer, "ConfigurationProperties_LoginSetup_TabLI"); }

            if (widgetsTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.ConfigurationPropertiesContainer, "ConfigurationProperties_Widgets_TabLI"); }

            if (catalogTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.ConfigurationPropertiesContainer, "ConfigurationProperties_Catalog_TabLI"); }

            if (privacyTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.ConfigurationPropertiesContainer, "ConfigurationProperties_Privacy_TabLI"); }

            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                DataTable siteParams = new DataTable();
                siteParams.Columns.Add("key");
                siteParams.Columns.Add("value");

                DataRow newSiteParamRow;

                // LOGIN SETUP

                // allow self registration
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_SELFREGISTRATION_ENABLED;
                newSiteParamRow["value"] = this._AllowSelfRegistration.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // self registration option
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_SELFREGISTRATION_TYPE;

                if (this._AllowSelfRegistration.Checked)
                { newSiteParamRow["value"] = this._SelfRegistrationList.SelectedValue; }
                else
                { newSiteParamRow["value"] = "builtin"; }

                siteParams.Rows.Add(newSiteParamRow);

                // self registration protocol
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_SELFREGISTRATION_URLPREFIX;

                if (this._AllowSelfRegistration.Checked)
                { newSiteParamRow["value"] = this._SelfRegistrationUrlProtocol.Text; }
                else
                { newSiteParamRow["value"] = "http://"; }

                siteParams.Rows.Add(newSiteParamRow);

                // self registration url
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_SELFREGISTRATION_URL;

                if (this._AllowSelfRegistration.Checked)
                { newSiteParamRow["value"] = this._SelfRegistrationUrl.Text; }
                else
                { newSiteParamRow["value"] = String.Empty; }

                siteParams.Rows.Add(newSiteParamRow);

                // allow password reset
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_PASSWORDRESET_ENABLED;
                newSiteParamRow["value"] = this._AllowPasswordReset.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // default landing page option
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_DEFAULTLANDINGPAGE;
                newSiteParamRow["value"] = this._DefaultLandingPageList.SelectedValue;
                siteParams.Rows.Add(newSiteParamRow);

                // default landing page local path
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_DEFAULTLANDINGPAGE_LOCALPATH;

                if (this._DefaultLandingPageList.SelectedValue == "other")
                { newSiteParamRow["value"] = this._DefaultLandingPageLocalPath.Text; }
                else
                { newSiteParamRow["value"] = this._DefaultLandingPageList.SelectedValue; }

                siteParams.Rows.Add(newSiteParamRow);

                // simultaneous login
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_SIMULTANEOUSLOGIN_ENABLED;
                newSiteParamRow["value"] = this._AllowSimultaneousLogin.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // login priority
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_LOGINPRIORITY;
                newSiteParamRow["value"] = this._LoginPriorityList.SelectedValue;
                siteParams.Rows.Add(newSiteParamRow);

                // require account registration approval
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERREGISTRATIONAPPROVAL_ENABLE))
                {
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.SYSTEM_ACCOUNTREGISTRATIONAPPROVAL;
                    newSiteParamRow["value"] = this._RequireRegistrationApproval.Checked.ToString();
                    siteParams.Rows.Add(newSiteParamRow);
                }

                // allow lockout
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_LOGOUT_ENABLED;
                newSiteParamRow["value"] = this._AllowLockout.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // lockout minutes
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_LOGOUT_MINUTES;
                if (this._AllowLockout.Checked)
                { newSiteParamRow["value"] = this._LockoutMinutes.SelectedValue; }
                else
                { newSiteParamRow["value"] = ""; }
                siteParams.Rows.Add(newSiteParamRow);

                // failed login attempts
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.SYSTEM_LOGOUT_ATTEMPTS;
                if (this._AllowLockout.Checked)
                { newSiteParamRow["value"] = this._LockoutAttempts.SelectedValue; }
                else
                { newSiteParamRow["value"] = ""; }
                siteParams.Rows.Add(newSiteParamRow);

                // only save the widgets checkboxes if the dashboard mode is standard.  widgets cannot be configured in restricted dashboard mode.
                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FEATURE_DASHBOARDMODE) == "Standard")
                {
                /**************************************THIS IS PART OF ADMINISTRATIVE TASKS WIDGET************************
                // if require registration approval is true, ensure the widget for it is enabled
                // if not, ensure it is disabled
                if (this._RequireRegistrationApproval.Checked)
                { this._EnablePendingUserRegistrationsWidget.Checked = true; }
                else
                { this._EnablePendingUserRegistrationsWidget.Checked = false; }
                 ***********************************************************************************************************/

                // WIDGETS - LEARNER

                // enrollments mode
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_ENROLLMENTS_MODE;
                newSiteParamRow["value"] = this._EnrollmentsWidgetMode.SelectedValue;
                siteParams.Rows.Add(newSiteParamRow);

                // purchases
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_PURCHASES_ENABLED;
                newSiteParamRow["value"] = this._EnablePurchasesWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // certificates
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_CERTIFICATES_ENABLED;
                newSiteParamRow["value"] = this._EnableCertificatesWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // calendar
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_CALENDAR_ENABLED;
                newSiteParamRow["value"] = this._EnableCalendarWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // feed
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_FEED_ENABLED;
                newSiteParamRow["value"] = this._EnableFeedWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // transcript
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_TRANSCRIPT_ENABLED;
                newSiteParamRow["value"] = this._EnableTranscriptWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // my communities
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
                {
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_MYCOMMUNITIES_ENABLED;
                    newSiteParamRow["value"] = this._EnableMyCommunitiesWidget.Checked.ToString();
                    siteParams.Rows.Add(newSiteParamRow);
                }

                // my learning paths
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_MYLEARNINGPATHS_ENABLED;
                    newSiteParamRow["value"] = this._EnableMyLearningPathsWidget.Checked.ToString();
                    siteParams.Rows.Add(newSiteParamRow);
                }

                // my learner leaderboard
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_LEADERBOARDS_ENABLED;
                newSiteParamRow["value"] = this._EnableLearnerLeaderboardsWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // my certifications
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                {                    
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_MYCERTIFICATIONS_ENABLED;
                    newSiteParamRow["value"] = this._EnableMyCertificationsWidget.Checked.ToString();
                    siteParams.Rows.Add(newSiteParamRow);
                }

                // my documents
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_DOCUMENTS_ENABLED;
                newSiteParamRow["value"] = this._EnableDocumentsWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // ilt sessions
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_LEARNER_ILTSESSIONS_ENABLED;
                newSiteParamRow["value"] = this._EnableILTSessionsWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // WIDGETS - ADMINISTRATOR

                // enrollment statistics
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_ADMINISTRATOR_ENROLLMENTSTATISTICS_ENABLED;
                newSiteParamRow["value"] = this._EnableEnrollmentStatisticsWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // administrator leaderboard
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_ADMINISTRATOR_LEADERBOARDS_ENABLED;
                newSiteParamRow["value"] = this._EnableAdministratorLeaderboardsWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // administrator wall moderation
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_ADMINISTRATOR_WALLMODERATION_ENABLED;
                newSiteParamRow["value"] = this._EnableAdministratorWallModerationWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                /**************************************THIS IS PART OF ADMINISTRATIVE TASKS WIDGET************************
                // administrator task proctoring
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_ADMINISTRATOR_TASKPROCTORING_ENABLED;
                newSiteParamRow["value"] = this._EnableAdministratorTaskProctoringWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // administrator ojt proctoring
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_ADMINISTRATOR_OJTPROCTORING_ENABLED;
                newSiteParamRow["value"] = this._EnableAdministratorOJTProctoringWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);
                 ***********************************************************************************************************/

                // administrator ilt roster management
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_ADMINISTRATOR_ILTROSTERMANAGEMENT_ENABLED;
                newSiteParamRow["value"] = this._EnableAdministratorILTRosterManagementWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                /**************************************THIS IS PART OF ADMINISTRATIVE TASKS WIDGET************************
                // pending user registrations
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_ADMINISTRATOR_PENDINGUSERREGISTRATIONS_ENABLED;
                newSiteParamRow["value"] = this._EnablePendingUserRegistrationsWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // pending course enrollments
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_ADMINISTRATOR_PENDINGCOURSEENROLLMENTS_ENABLED;
                newSiteParamRow["value"] = this._EnablePendingCourseEnrollmentsWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // certification task proctoring
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                {
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WIDGET_ADMINISTRATOR_CERTIFICATIONTASKPROCTORING_ENABLED;
                    newSiteParamRow["value"] = this._EnableAdministratorCertificationTaskProctoringWidget.Checked.ToString();
                    siteParams.Rows.Add(newSiteParamRow);
                }
                 *************************************************************************************************************/

                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_ADMINISTRATOR_ADMINISTRATIVETASKS_ENABLED;
                newSiteParamRow["value"] = this._EnableAdministrativeTasksWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // WIDGETS - REPORTER

                // report subscriptions
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_REPORTER_REPORTSUBSCRIPTIONS_ENABLED;
                newSiteParamRow["value"] = this._EnableReportSubscriptionsWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // report shortcuts
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.WIDGET_REPORTER_REPORTSHORTCUTS_ENABLED;
                newSiteParamRow["value"] = this._EnableReportShortcutsWidget.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);
                }

                // CATALOG

                // enable catalog
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.CATALOG_ENABLE;
                newSiteParamRow["value"] = this._EnableCatalog.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // enable catalog search
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.CATALOG_SEARCH_ENABLE;
                newSiteParamRow["value"] = this._EnableCatalogSearch.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // enable catalog vent calendar
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.CATALOG_EVENTCALENDAR_ENABLE;
                newSiteParamRow["value"] = this._EnableCatalogEventCalendar.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // require login
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.CATALOG_REQUIRE_LOGIN;
                newSiteParamRow["value"] = this._CatalogRequireLogin.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // list private catalogs
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.CATALOG_SHOW_PRIVATECOURSECATALOGS;
                newSiteParamRow["value"] = this._ListPrivateCourseCatalogs.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // hide course codes
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.CATALOG_HIDE_COURSE_CODES;
                newSiteParamRow["value"] = this._HideCourseCodes.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // enable standup training browsing
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE))
                {
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.CATALOG_STANDUPTRAINING_ENABLE;
                    newSiteParamRow["value"] = this._EnableStandupTrainingBrowsing.Checked.ToString();
                    siteParams.Rows.Add(newSiteParamRow);
                }

                // enable learning paths browsing
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.CATALOG_LEARNINGPATHS_ENABLE;
                    newSiteParamRow["value"] = this._EnableLearningPathBrowsing.Checked.ToString();
                    siteParams.Rows.Add(newSiteParamRow);
                }

                // enable community browsing
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.COMMUNITIES_ENABLE))
                {
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.CATALOG_COMMUNITY_ENABLE;
                    newSiteParamRow["value"] = this._EnableCommunityBrowsing.Checked.ToString();
                    siteParams.Rows.Add(newSiteParamRow);
                }
                // LEADERBOARDS

                // enable course total leaderboard
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.LEADERBOARDS_COURSETOTAL_ENABLE;
                newSiteParamRow["value"] = this._EnableCourseTotalLeaderboard.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // enable course credits leaderboard
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.LEADERBOARDS_COURSECREDITS_ENABLE;
                newSiteParamRow["value"] = this._EnableCourseCreditsLeaderboard.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // enable certificate total leaderboard
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.LEADERBOARDS_CERTIFICATETOTAL_ENABLE;
                newSiteParamRow["value"] = this._EnableCertificateTotalLeaderboard.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // enable certificate credits leaderboard
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.LEADERBOARDS_CERTIFICATECREDITS_ENABLE;
                newSiteParamRow["value"] = this._EnableCertificateCreditsLeaderboard.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // RATINGS

                // enable course ratings
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.RATINGS_COURSE_ENABLE;
                newSiteParamRow["value"] = this._EnableCourseRatings.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // publicize course ratings
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.RATINGS_COURSE_PUBLICIZE;
                newSiteParamRow["value"] = this._PublicizeCourseRatings.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // privacy

                // require cookie consent
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.PRIVACY_REQUIRECOOKIECONSENT;
                newSiteParamRow["value"] = this._RequireCookieConsent.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // allow users to opt out of email notifications
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.PRIVACY_ALLOWUSEREMAILOPTOUT;
                newSiteParamRow["value"] = this._AllowUserEmailOptOut.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // When site not allow users to opt out of email notification, remove all records about opting out information from tblUser for this site
                if (!this._AllowUserEmailOptOut.Checked)
                  { Asentia.UMS.Library.User.ClearTableColumns("optOutOfEmailNotifications"); }

                // Permanently remove deleted users from system
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.PRIVACY_PERMANENTLYREMOVEDELETEDUSERS;
                newSiteParamRow["value"] = this._PermanentlyRemoveDeletedUsers.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // allow user self deletion
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.PRIVACY_ALLOWSELFDELETION;
                newSiteParamRow["value"] = this._AllowSelfDeletion.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // display user agreement
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.USERAGREEMENT_DISPLAY;
                newSiteParamRow["value"] = this._UserAgreementDisplayCheckBox.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // user agreement required
                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.USERAGREEMENT_REQUIRED;
                newSiteParamRow["value"] = this._UserAgreementRequiredCheckBox.Checked.ToString();
                siteParams.Rows.Add(newSiteParamRow);

                // save the site params
                AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, siteParams);
                
                // save user agreement section & insert the modified date if any change
                this._SaveUserAgreement();

                // reload global site object
                AsentiaSessionState.GlobalSiteObject = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, true);

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedback(_GlobalResources.ConfigurationHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _SaveUserAgreement
        /// <summary>
        /// Save the user agreement under privacy tab to the xml file
        /// </summary>
        private void _SaveUserAgreement()
        {
            bool isUserAgreementModified = false;
            XmlDocument userAccountDataFile = new XmlDocument();

            if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + "UserAccountData.xml")))
            {
                userAccountDataFile.Load(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + "UserAccountData.xml"));

                // Display User Agreement to users
                XmlElement displayUserAgreementElement = userAccountDataFile.SelectSingleNode("/userAccountData/displayUserAgreement") as XmlElement;
                if (displayUserAgreementElement != null)
                {
                    XmlCDataSection cdataDisplay = userAccountDataFile.CreateCDataSection(this._UserAgreementDisplayCheckBox.Checked.ToString());
                    displayUserAgreementElement.InnerXml = cdataDisplay.OuterXml;
                }

                // Require users to agree when registering
                XmlElement requiredUserAgreementElement = userAccountDataFile.SelectSingleNode("/userAccountData/requireUserAgreement") as XmlElement;
                if (requiredUserAgreementElement != null)
                {
                    XmlCDataSection cdataRequired = userAccountDataFile.CreateCDataSection(this._UserAgreementRequiredCheckBox.Checked.ToString());
                    requiredUserAgreementElement.InnerXml = cdataRequired.OuterXml;
                }

                // Save User Agreement Textbox
                XmlNodeList useragreementNodeList;
                XmlNode useragreementNode = userAccountDataFile.DocumentElement;

                useragreementNodeList = useragreementNode.SelectNodes("/userAccountData/userAgreement");

                if (useragreementNodeList != null)
                {
                    foreach (string availableLanguage in this.GetArrayListOfSiteAvailableInstalledLanguages())
                    {
                        XmlElement UserAgreementElement = userAccountDataFile.SelectSingleNode("/userAccountData/userAgreement[@lang='" + availableLanguage + "']") as XmlElement;
                        TextBox languageSpecificUserAgreementTextBox = (TextBox)this.ConfigurationPropertiesContainer.FindControl((availableLanguage == AsentiaSessionState.GlobalSiteObject.LanguageString) ? this._UserAgreementTextBox.ID : this._UserAgreementTextBox.ID + "_" + availableLanguage);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificUserAgreementTextBox != null && (languageSpecificUserAgreementTextBox.Text != this._UserAccountDataObject.GetUserAgreementHTMLInLanguage(availableLanguage)))
                        {
                             XmlCDataSection cdataUserAgreement = userAccountDataFile.CreateCDataSection(languageSpecificUserAgreementTextBox.Text);
                             UserAgreementElement.InnerXml = cdataUserAgreement.OuterXml;
                             isUserAgreementModified = true;
                        }
                    }
                }

                if (isUserAgreementModified)
                    AsentiaSite.SaveUserAgreementModifiedDate(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser);

                userAccountDataFile.Save(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + "UserAccountData.xml"));
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("/dashboard");
        }
        #endregion
    }
}