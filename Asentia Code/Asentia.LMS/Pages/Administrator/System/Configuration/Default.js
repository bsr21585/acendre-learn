﻿//docment ready fnction to bind the events on page load
$(document).ready(function () {
    //setting site params on page load
    AllowSelfRegistrationClick("AllowSelfRegistration_Field");
    SelfRegistrationListClick("SelfRegistrationList_Field");
    DefaultLandingPageListClick("DefaultLandingPageList_Field");
    AllowSimultaneousLoginClick("AllowSimultaneousLogin_Field");
    EnableCatalogClick("EnableCatalog_Field");
    EnableCourseRatingsClick("EnableCourseRatings_Field");
    EnablePendingUserRegistrationsClick("RequireRegistrationApproval_Field");
    EnableDisableUserAgreementCheckBox();
    AllowLockoutClick("AllowLockout_Field");
});

//Function for enble/disable controls as per self registration checkbox value
function AllowSelfRegistrationClick(elementId) {
    var selfRegistrationType = $("#SelfRegistrationList_Field").find(":checked").val();

    if ($("#" + elementId).prop("checked") && selfRegistrationType == "builtin") {
        $("#RequireRegistrationApproval_Field").prop("disabled", false);
        $("#SelfRegistrationList_Field").find("input").removeAttr("disabled");
        $("#SelfRegistrationUrlProtocol_Field").prop("disabled", "disabled");
        $("#SelfRegistrationUrl_Field").prop("disabled", "disabled");
    }
    else if ($("#" + elementId).prop("checked") && selfRegistrationType == "custom") {
        $("#RequireRegistrationApproval_Field").prop("disabled", false);
        $("#SelfRegistrationList_Field").find("input").removeAttr("disabled");
        $("#SelfRegistrationUrlProtocol_Field").removeAttr("disabled");
        $("#SelfRegistrationUrl_Field").removeAttr("disabled");
    }
    else if ($("#" + elementId).prop("checked", false)) {
        $("#RequireRegistrationApproval_Field").prop("checked", false);
        $("#RequireRegistrationApproval_Field").prop("disabled", true);
        EnablePendingUserRegistrationsClick("RequireRegistrationApproval_Field");
        $("#SelfRegistrationList_Field").find("input").prop("disabled", "disabled");
        $("#SelfRegistrationUrlProtocol_Field").prop("disabled", "disabled");
        $("#SelfRegistrationUrlProtocol_Field").val("http://");
        $("#SelfRegistrationUrl_Field").prop("disabled", "disabled");
        $("#SelfRegistrationUrl_Field").val("");
    }
}

//Function for enble/disable controls as per self registration checkbox value
function SelfRegistrationListClick(elementId) {
    var registrationType = $("#" + elementId).find(":checked").val();

    if (registrationType == "custom") {
        $("#SelfRegistrationUrlProtocol_Field").removeAttr("disabled");
        $("#SelfRegistrationUrl_Field").removeAttr("disabled");   
    }
    else {
        $("#SelfRegistrationUrlProtocol_Field").prop("disabled", "disabled");
        $("#SelfRegistrationUrlProtocol_Field").val("http://");
        $("#SelfRegistrationUrl_Field").prop("disabled", "disabled");
        $("#SelfRegistrationUrl_Field").val("");
    }
}

//function to fill the text value in the text box as per the 
//default landing page option value selected
function DefaultLandingPageListClick(elementId) {
    var defaultLandingPageType = $("#" + elementId).find(":checked").val();

    if (defaultLandingPageType == "/") {
        $("#DefaultLandingPageLocalPath_Field").val("/");
        $("#DefaultLandingPageLocalPath_Field").prop("disabled", "disabled");
    }
    else if (defaultLandingPageType == "/catalog") {
        $("#DefaultLandingPageLocalPath_Field").val("/catalog");
        $("#DefaultLandingPageLocalPath_Field").prop("disabled", "disabled");

    }
    else if (defaultLandingPageType == "/dashboard") {
        $("#DefaultLandingPageLocalPath_Field").val("/dashboard");
        $("#DefaultLandingPageLocalPath_Field").prop("disabled", "disabled");

    }
    else {
        $("#DefaultLandingPageLocalPath_Field").val("/");
        $("#DefaultLandingPageLocalPath_Field").removeAttr("disabled");
    }
}
//function to enable/disable the login priority radio buttons on 
//Simultaneous Login checkbox value change
function AllowSimultaneousLoginClick(elementId) {
    if ($("#" + elementId).prop("checked")) {
        $("#LoginPriority_Field").find("input").prop("disabled", "disabled");
    }
    else {
        $("#LoginPriority_Field").find("input").removeAttr("disabled");
    }
}

//function to enable/disable the user agreement required
//only when display user agreement is checked
function EnableDisableUserAgreementCheckBox() {
    if ($("#uad_UserAgreementDisplayCheckBox").prop("checked")) {
        $("#uad_UserAgreementRequiredCheckBox").removeAttr("disabled");
    }
    else {
        $("#uad_UserAgreementRequiredCheckBox").prop("checked", false);
        $("#uad_UserAgreementRequiredCheckBox").prop("disabled", "disabled");
    }
}

//function to enable/disable the other filters in the catalog tab on 
//Enable Catalog checkbox value change
function EnableCatalogClick(elementId) {
    if ($("#" + elementId).prop("checked")) {
        $("#EnableCatalogSearch_Field").removeAttr("disabled");
        $("#EnableCatalogEventCalendar_Field").removeAttr("disabled");
        $("#CatalogRequireLogin_Field").removeAttr("disabled");
        $("#ListPrivateCourseCatalogs_Field").removeAttr("disabled");
        $("#HideCourseCodes_Field").removeAttr("disabled");
        $("#EnableStandupTraining_Field").removeAttr("disabled");
        $("#EnableLearningPath_Field").removeAttr("disabled");
        $("#EnableCommunity_Field").removeAttr("disabled");
    }
    else {
        $("#EnableCatalogSearch_Field").prop("checked", false);
        $("#EnableCatalogSearch_Field").prop("disabled", "disabled");

        $("#EnableCatalogEventCalendar_Field").prop("checked", false);
        $("#EnableCatalogEventCalendar_Field").prop("disabled", "disabled");

        $("#CatalogRequireLogin_Field").prop("checked", false);
        $("#CatalogRequireLogin_Field").prop("disabled", "disabled");

        $("#ListPrivateCourseCatalogs_Field").prop("checked", false);
        $("#ListPrivateCourseCatalogs_Field").prop("disabled", "disabled");

        $("#HideCourseCodes_Field").prop("checked", false);
        $("#HideCourseCodes_Field").prop("disabled", "disabled");

        $("#EnableStandupTraining_Field").prop("checked", false);
        $("#EnableStandupTraining_Field").prop("disabled", "disabled");

        $("#EnableLearningPath_Field").prop("checked", false);
        $("#EnableLearningPath_Field").prop("disabled", "disabled");

        $("#EnableCommunity_Field").prop("checked", false);
        $("#EnableCommunity_Field").prop("disabled", "disabled");
    }
}

//function to enable/disable the other filter in the rating tab on 
//Course Ratings checkbox value change
function EnableCourseRatingsClick(elementId) {
    if ($("#" + elementId).prop("checked")) {
        $("#PublicizeCourseRatings_Field").removeAttr("disabled");
    }
    else {
        $("#PublicizeCourseRatings_Field").prop("checked", false);
        $("#PublicizeCourseRatings_Field").prop("disabled", "disabled");
    }
}

// Function to enable/disable the pending user registrations enabler checkbox on 
// Require Account Approval checkbox value change
function EnablePendingUserRegistrationsClick(elementId) {
    if ($("#" + elementId).prop("checked")) {
        $("#PendingUserRegistrationsWidget_Field_Container").show();
    }
    else {
        $("#PendingUserRegistrationsWidget_Field_Container").hide();
    }
}

//Function for enble/disable controls as per lockout checkbox value
function AllowLockoutClick(elementId) {
    if ($("#" + elementId).prop("checked")) {
        $("#LockoutMinutes_Field").removeAttr("disabled");
        $("#LockoutAttempts_Field").removeAttr("disabled");
    }
    else
    {
        $("#LockoutMinutes_Field").prop("disabled", "disabled");
        $("#LockoutAttempts_Field").prop("disabled", "disabled");
    }

}
