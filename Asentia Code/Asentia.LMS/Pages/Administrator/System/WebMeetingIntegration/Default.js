﻿function SetWebMeetingIntegrationOnOff(idPrefix) {
    var selectedOnOffValue = $("#" + idPrefix + "On_Field").find(":checked").val();
    
    if (selectedOnOffValue == "True") {
        $("#" + idPrefix + "ApplicationKey_Field").removeAttr("disabled");
        $("#" + idPrefix + "ConsumerSecret_Field").removeAttr("disabled");
        $("#" + idPrefix + "Username_Field").removeAttr("disabled");
        $("#" + idPrefix + "Password_Field").removeAttr("disabled");
        $("#" + idPrefix + "Plan_Field input").removeAttr("disabled");
    }
    else {
        $("#" + idPrefix + "ApplicationKey_Field").prop("disabled", "disabled");
        $("#" + idPrefix + "ConsumerSecret_Field").prop("disabled", "disabled");
        $("#" + idPrefix + "Username_Field").prop("disabled", "disabled");
        $("#" + idPrefix + "Password_Field").prop("disabled", "disabled");
        $("#" + idPrefix + "Plan_Field input").prop("disabled", "disabled");

        $("#" + idPrefix + "ApplicationKey_Field").val("");
        $("#" + idPrefix + "ConsumerSecret_Field").val("");
        $("#" + idPrefix + "Username_Field").val("");
        $("#" + idPrefix + "Password_Field").val("");
        $("#" + idPrefix + "Plan_Field input").prop("checked", false);
    }
}