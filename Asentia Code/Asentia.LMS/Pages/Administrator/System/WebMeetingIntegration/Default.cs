﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Management;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.System.WebMeetingIntegration
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel WebMeetingIntegrationFormContentWrapperContainer;
        public Panel WebMeetingIntegrationWrapperContainer;
        public Panel PropertiesInstructionsPanel;
        public Panel WebMeetingIntegrationPropertiesContainer;
        public Panel WebMeetingIntegrationPropertiesTabPanelsContainer;
        public Panel PropertiesActionsPanel;
        #endregion

        #region Private Properties
        private bool _IsGoToMeetingEnabled;
        private bool _IsGoToWebinarEnabled;
        private bool _IsGoToTrainingEnabled;
        private bool _IsWebExEnabled;

        // GO TO MEETING
        private RadioButtonList _GoToMeetingOn;
        private TextBox _GoToMeetingApplicationKey;
        private TextBox _GoToMeetingConsumerSecret;
        private TextBox _GoToMeetingUsername;
        private TextBox _GoToMeetingPassword;
        private RadioButtonList _GoToMeetingPlan;

        // GO TO WEBINAR
        private RadioButtonList _GoToWebinarOn;
        private TextBox _GoToWebinarApplicationKey;
        private TextBox _GoToWebinarConsumerSecret;
        private TextBox _GoToWebinarUsername;
        private TextBox _GoToWebinarPassword;
        private RadioButtonList _GoToWebinarPlan;

        // GO TO TRAINING
        private RadioButtonList _GoToTrainingOn;
        private TextBox _GoToTrainingApplicationKey;
        private TextBox _GoToTrainingConsumerSecret;
        private TextBox _GoToTrainingUsername;
        private TextBox _GoToTrainingPassword;
        private RadioButtonList _GoToTrainingPlan;

        // WEBEX
        private RadioButtonList _WebExOn;
        private TextBox _WebExApplicationKey;
        private TextBox _WebExUsername;
        private TextBox _WebExPassword;
        private RadioButtonList _WebExPlan;

        private Button _SaveButton;
        private Button _CancelButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// On Prerender event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.System.WebMeetingIntegration.Default.js");

            // register JS start up script for on/off toggles
            StringBuilder sb = new StringBuilder();

            if (this._IsGoToMeetingEnabled)
            { sb.AppendLine("SetWebMeetingIntegrationOnOff(\"GoToMeeting\");"); }

            if (this._IsGoToWebinarEnabled)
            { sb.AppendLine("SetWebMeetingIntegrationOnOff(\"GoToWebinar\");"); }

            if (this._IsGoToTrainingEnabled)
            { sb.AppendLine("SetWebMeetingIntegrationOnOff(\"GoToTraining\");"); }

            if (this._IsWebExEnabled)
            { sb.AppendLine("SetWebMeetingIntegrationOnOff(\"WebEx\");"); }

            csm.RegisterStartupScript(typeof(Default), "SetIntegrationOnOffToggles", sb.ToString(), true);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_WebMeetingIntegration))
            { Response.Redirect("/"); }

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.WebMeetingIntegrationFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.WebMeetingIntegrationWrapperContainer.CssClass = "FormContentContainer";

            // get webinar platform enabled settings
            this._IsGoToMeetingEnabled = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE);
            this._IsGoToWebinarEnabled = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE);
            this._IsGoToTrainingEnabled = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE);
            this._IsWebExEnabled = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE);

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string webMeetingIntegrationImagePath;
            string pageTitle;

            breadCrumbPageTitle = _GlobalResources.WebMeetingIntegration;

            webMeetingIntegrationImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_WEBMEETING, ImageFiles.EXT_PNG);

            pageTitle = _GlobalResources.WebMeetingIntegration;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, pageTitle, webMeetingIntegrationImagePath);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {            
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the properties form
            this._BuildPropertiesForm();

            // build the properties form actions panel
            this._BuildPropertiesActionsPanel();

            // populate the form inputs
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _BuildPropertiesForm
        /// <summary>
        /// Builds the Properties form.
        /// </summary>
        private void _BuildPropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PropertiesInstructionsPanel, _GlobalResources.ConfigureThePropertiesUsingTheFormBelow, true);

            // clear controls from container
            this.WebMeetingIntegrationPropertiesContainer.Controls.Clear();

            // build the properties form tabs
            this._BuildPropertiesFormTabs();

            // build tab panels container
            this.WebMeetingIntegrationPropertiesTabPanelsContainer = new Panel();
            this.WebMeetingIntegrationPropertiesTabPanelsContainer.ID = "WebMeetingIntegrationProperties_TabPanelsContainer";
            this.WebMeetingIntegrationPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.WebMeetingIntegrationPropertiesContainer.Controls.Add(this.WebMeetingIntegrationPropertiesTabPanelsContainer);

            // build input controls

            if (this._IsGoToMeetingEnabled)
            { this._BuildPropertiesFormGoToMeetingPanel(); }

            if (this._IsGoToWebinarEnabled)
            { this._BuildPropertiesFormGoToWebinarPanel(); }

            if (this._IsGoToTrainingEnabled)
            { this._BuildPropertiesFormGoToTrainingPanel(); }

            if (this._IsWebExEnabled)
            { this._BuildPropertiesFormWebExPanel(); }
        }
        #endregion

        #region _BuildPropertiesFormTabs
        /// <summary>
        /// Method to create the configuration page property tabs
        /// </summary>
        private void _BuildPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            if (this._IsGoToMeetingEnabled)
            { tabs.Enqueue(new KeyValuePair<string, string>("GoToMeeting", _GlobalResources.GoToMeeting)); }

            if (this._IsGoToWebinarEnabled)
            { tabs.Enqueue(new KeyValuePair<string, string>("GoToWebinar", _GlobalResources.GoToWebinar)); }

            if (this._IsGoToTrainingEnabled)
            { tabs.Enqueue(new KeyValuePair<string, string>("GoToTraining", _GlobalResources.GoToTraining)); }

            if (this._IsWebExEnabled)
            { tabs.Enqueue(new KeyValuePair<string, string>("WebEx", _GlobalResources.WebEx)); }

            // build and attach the tabs
            this.WebMeetingIntegrationPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("WebMeetingIntegrationProperties", tabs, null, this.Page, null));
        }
        #endregion

        #region _BuildPropertiesFormGoToMeetingPanel
        /// <summary>
        /// Method to build the controls for GoToMeeting tab
        /// </summary>
        private void _BuildPropertiesFormGoToMeetingPanel()
        {
            Panel goToMeetingPanel = new Panel();
            goToMeetingPanel.ID = "WebMeetingIntegrationProperties_" + "GoToMeeting" + "_TabPanel";
            goToMeetingPanel.Attributes.Add("style", "display: block;"); 
           
            // on/off field
            this._GoToMeetingOn = new RadioButtonList();
            this._GoToMeetingOn.ID = "GoToMeetingOn_Field";            
            this._GoToMeetingOn.CssClass = "HorizontalRadioList";
            this._GoToMeetingOn.Items.Add(new ListItem(_GlobalResources.On, "True"));
            this._GoToMeetingOn.Items.Add(new ListItem(_GlobalResources.Off, "False"));
            this._GoToMeetingOn.Attributes.Add("onclick", "SetWebMeetingIntegrationOnOff('GoToMeeting');");

            goToMeetingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToMeetingOn",
                                                               _GlobalResources.GoToMeetingIntegration,
                                                               this._GoToMeetingOn.ID,
                                                               this._GoToMeetingOn,
                                                               true,
                                                               true,
                                                               false));

            // application key field
            this._GoToMeetingApplicationKey = new TextBox();
            this._GoToMeetingApplicationKey.ID = "GoToMeetingApplicationKey_Field";
            this._GoToMeetingApplicationKey.CssClass = "InputLong";

            goToMeetingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToMeetingApplicationKey",
                                                               _GlobalResources.GoToMeetingApplicationKey,
                                                               this._GoToMeetingApplicationKey.ID,
                                                               this._GoToMeetingApplicationKey,
                                                               true,
                                                               true,
                                                               false));

            // consumer secret field
            this._GoToMeetingConsumerSecret = new TextBox();
            this._GoToMeetingConsumerSecret.ID = "GoToMeetingConsumerSecret_Field";
            this._GoToMeetingConsumerSecret.CssClass = "InputMedium";

            goToMeetingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToMeetingConsumerSecret",
                                                                _GlobalResources.ConsumerSecret,
                                                                this._GoToMeetingConsumerSecret.ID,
                                                                this._GoToMeetingConsumerSecret,
                                                                true,
                                                                true,
                                                                false));

            // username field
            this._GoToMeetingUsername = new TextBox();
            this._GoToMeetingUsername.ID = "GoToMeetingUsername_Field";
            this._GoToMeetingUsername.CssClass = "InputMedium";

            goToMeetingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToMeetingUsername",
                                                               _GlobalResources.DefaultOrganizerUsername,
                                                               this._GoToMeetingUsername.ID,
                                                               this._GoToMeetingUsername,
                                                               true,
                                                               true,
                                                               false));

            // password field
            this._GoToMeetingPassword = new TextBox();
            this._GoToMeetingPassword.ID = "GoToMeetingPassword_Field";
            this._GoToMeetingPassword.TextMode = TextBoxMode.Password;
            this._GoToMeetingPassword.CssClass = "InputShort";

            goToMeetingPanel.Controls.Add(AsentiaPage.BuildFormField("DefaultOrganizerPassword",
                                                               _GlobalResources.GoToMeetingPassword,
                                                               this._GoToMeetingPassword.ID,
                                                               this._GoToMeetingPassword,
                                                               true,
                                                               true,
                                                               false));

            // plan field
            this._GoToMeetingPlan = new RadioButtonList();
            this._GoToMeetingPlan.ID = "GoToMeetingPlan_Field";
            this._GoToMeetingPlan.Items.Add(new ListItem(_GlobalResources.GTMFree3Attendees, GoToMeetingAPI.FREE_PLAN_NAME));
            this._GoToMeetingPlan.Items.Add(new ListItem(_GlobalResources.GTMStarter10Attendees, GoToMeetingAPI.STARTER_PLAN_NAME));
            this._GoToMeetingPlan.Items.Add(new ListItem(_GlobalResources.GTMPro50Attendees, GoToMeetingAPI.PRO_PLAN_NAME));
            this._GoToMeetingPlan.Items.Add(new ListItem(_GlobalResources.GTMPlus100Attendees, GoToMeetingAPI.PLUS_PLAN_NAME));

            goToMeetingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToMeetingPlan",
                                                               _GlobalResources.GoToMeetingPlan,
                                                               this._GoToMeetingPlan.ID,
                                                               this._GoToMeetingPlan,
                                                               true,
                                                               true,
                                                               false));

            // attach panel to container
            this.WebMeetingIntegrationPropertiesTabPanelsContainer.Controls.Add(goToMeetingPanel);
        }
        #endregion

        #region _BuildPropertiesFormGoToWebinarPanel
        /// <summary>
        /// Method to build the controls for GoToWebinar tab
        /// </summary>
        private void _BuildPropertiesFormGoToWebinarPanel()
        {
            Panel goToWebinarPanel = new Panel();
            goToWebinarPanel.ID = "WebMeetingIntegrationProperties_" + "GoToWebinar" + "_TabPanel";

            if (!this._IsGoToMeetingEnabled)
            { goToWebinarPanel.Attributes.Add("style", "display: block;"); }
            else
            { goToWebinarPanel.Attributes.Add("style", "display: none;"); }

            // on/off field
            this._GoToWebinarOn = new RadioButtonList();
            this._GoToWebinarOn.ID = "GoToWebinarOn_Field";
            this._GoToWebinarOn.CssClass = "HorizontalRadioList";
            this._GoToWebinarOn.Items.Add(new ListItem(_GlobalResources.On, "True"));
            this._GoToWebinarOn.Items.Add(new ListItem(_GlobalResources.Off, "False"));
            this._GoToWebinarOn.Attributes.Add("onclick", "SetWebMeetingIntegrationOnOff('GoToWebinar');");

            goToWebinarPanel.Controls.Add(AsentiaPage.BuildFormField("GoToWebinarOn",
                                                               _GlobalResources.GoToWebinarIntegration,
                                                               this._GoToWebinarOn.ID,
                                                               this._GoToWebinarOn,
                                                               true,
                                                               true,
                                                               false));

            // application key field
            this._GoToWebinarApplicationKey = new TextBox();
            this._GoToWebinarApplicationKey.ID = "GoToWebinarApplicationKey_Field";
            this._GoToWebinarApplicationKey.CssClass = "InputLong";

            goToWebinarPanel.Controls.Add(AsentiaPage.BuildFormField("GoToWebinarApplicationKey",
                                                               _GlobalResources.GoToWebinarApplicationKey,
                                                               this._GoToWebinarApplicationKey.ID,
                                                               this._GoToWebinarApplicationKey,
                                                               true,
                                                               true,
                                                               false));

            // consumer secret field
            this._GoToWebinarConsumerSecret = new TextBox();
            this._GoToWebinarConsumerSecret.ID = "GoToWebinarConsumerSecret_Field";
            this._GoToWebinarConsumerSecret.CssClass = "InputMedium";

            goToWebinarPanel.Controls.Add(AsentiaPage.BuildFormField("GoToWebinarConsumerSecret",
                                                                _GlobalResources.ConsumerSecret,
                                                                this._GoToWebinarConsumerSecret.ID,
                                                                this._GoToWebinarConsumerSecret,
                                                                true,
                                                                true,
                                                                false));

            // username field
            this._GoToWebinarUsername = new TextBox();
            this._GoToWebinarUsername.ID = "GoToWebinarUsername_Field";
            this._GoToWebinarUsername.CssClass = "InputMedium";

            goToWebinarPanel.Controls.Add(AsentiaPage.BuildFormField("GoToWebinarUsername",
                                                               _GlobalResources.DefaultOrganizerUsername,
                                                               this._GoToWebinarUsername.ID,
                                                               this._GoToWebinarUsername,
                                                               true,
                                                               true,
                                                               false));

            // password field
            this._GoToWebinarPassword = new TextBox();
            this._GoToWebinarPassword.ID = "GoToWebinarPassword_Field";
            this._GoToWebinarPassword.TextMode = TextBoxMode.Password;
            this._GoToWebinarPassword.CssClass = "InputShort";

            goToWebinarPanel.Controls.Add(AsentiaPage.BuildFormField("GoToWebinarPassword",
                                                               _GlobalResources.DefaultOrganizerPassword,
                                                               this._GoToWebinarPassword.ID,
                                                               this._GoToWebinarPassword,
                                                               true,
                                                               true,
                                                               false));

            // plan field
            this._GoToWebinarPlan = new RadioButtonList();
            this._GoToWebinarPlan.ID = "GoToWebinarPlan_Field";            
            this._GoToWebinarPlan.Items.Add(new ListItem(_GlobalResources.GTWStarter100Attendees, GoToWebinarAPI.STARTER_PLAN_NAME));
            this._GoToWebinarPlan.Items.Add(new ListItem(_GlobalResources.GTWPro500Attendees, GoToWebinarAPI.PRO_PLAN_NAME));
            this._GoToWebinarPlan.Items.Add(new ListItem(_GlobalResources.GTWPlus2000Attendees, GoToWebinarAPI.PLUS_PLAN_NAME));

            goToWebinarPanel.Controls.Add(AsentiaPage.BuildFormField("GoToWebinarPlan",
                                                               _GlobalResources.GoToWebinarPlan,
                                                               this._GoToWebinarPlan.ID,
                                                               this._GoToWebinarPlan,
                                                               true,
                                                               true,
                                                               false));

            // attach panel to container
            this.WebMeetingIntegrationPropertiesTabPanelsContainer.Controls.Add(goToWebinarPanel);
        }
        #endregion

        #region _BuildPropertiesFormGoToTrainingPanel
        /// <summary>
        /// Method to build the controls for GoToTraining tab
        /// </summary>
        private void _BuildPropertiesFormGoToTrainingPanel()
        {
            Panel goToTrainingPanel = new Panel();
            goToTrainingPanel.ID = "WebMeetingIntegrationProperties_" + "GoToTraining" + "_TabPanel";

            if (!this._IsGoToMeetingEnabled && !this._IsGoToWebinarEnabled)
            { goToTrainingPanel.Attributes.Add("style", "display: block;"); }
            else
            { goToTrainingPanel.Attributes.Add("style", "display: none;"); }

            // on/off field
            this._GoToTrainingOn = new RadioButtonList();
            this._GoToTrainingOn.ID = "GoToTrainingOn_Field";
            this._GoToTrainingOn.CssClass = "HorizontalRadioList";
            this._GoToTrainingOn.Items.Add(new ListItem(_GlobalResources.On, "True"));
            this._GoToTrainingOn.Items.Add(new ListItem(_GlobalResources.Off, "False"));
            this._GoToTrainingOn.Attributes.Add("onclick", "SetWebMeetingIntegrationOnOff('GoToTraining');");

            goToTrainingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToTrainingOn",
                                                                _GlobalResources.GoToTrainingIntegration,
                                                                this._GoToTrainingOn.ID,
                                                                this._GoToTrainingOn,
                                                                true,
                                                                true,
                                                                false));

            // application key field
            this._GoToTrainingApplicationKey = new TextBox();
            this._GoToTrainingApplicationKey.ID = "GoToTrainingApplicationKey_Field";
            this._GoToTrainingApplicationKey.CssClass = "InputLong";

            goToTrainingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToTrainingApplicationKey",
                                                                _GlobalResources.GoToTrainingApplicationKey,
                                                                this._GoToTrainingApplicationKey.ID,
                                                                this._GoToTrainingApplicationKey,
                                                                true,
                                                                true,
                                                                false));

            // consumer secret field
            this._GoToTrainingConsumerSecret = new TextBox();
            this._GoToTrainingConsumerSecret.ID = "GoToTrainingConsumerSecret_Field";
            this._GoToTrainingConsumerSecret.CssClass = "InputMedium";

            goToTrainingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToTrainingConsumerSecret",
                                                                _GlobalResources.ConsumerSecret,
                                                                this._GoToTrainingConsumerSecret.ID,
                                                                this._GoToTrainingConsumerSecret,
                                                                true,
                                                                true,
                                                                false));

            // username field
            this._GoToTrainingUsername = new TextBox();
            this._GoToTrainingUsername.ID = "GoToTrainingUsername_Field";
            this._GoToTrainingUsername.CssClass = "InputMedium";

            goToTrainingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToTrainingUsername",
                                                                _GlobalResources.DefaultOrganizerUsername,
                                                                this._GoToTrainingUsername.ID,
                                                                this._GoToTrainingUsername,
                                                                true,
                                                                true,
                                                                false));

            // password field
            this._GoToTrainingPassword = new TextBox();
            this._GoToTrainingPassword.ID = "GoToTrainingPassword_Field";
            this._GoToTrainingPassword.TextMode = TextBoxMode.Password;
            this._GoToTrainingPassword.CssClass = "InputShort";

            goToTrainingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToTrainingPassword",
                                                                _GlobalResources.DefaultOrganizerPassword,
                                                                this._GoToTrainingPassword.ID,
                                                                this._GoToTrainingPassword,
                                                                true,
                                                                true,
                                                                false));            

            // plan field
            this._GoToTrainingPlan = new RadioButtonList();
            this._GoToTrainingPlan.ID = "GoToTrainingPlan_Field";
            this._GoToTrainingPlan.Items.Add(new ListItem(_GlobalResources.GTTStarter25Attendees, GoToTrainingAPI.STARTER_PLAN_NAME));
            this._GoToTrainingPlan.Items.Add(new ListItem(_GlobalResources.GTTPro50Attendees, GoToTrainingAPI.PRO_PLAN_NAME));
            this._GoToTrainingPlan.Items.Add(new ListItem(_GlobalResources.GTTPlus200Attendees, GoToTrainingAPI.PLUS_PLAN_NAME));

            goToTrainingPanel.Controls.Add(AsentiaPage.BuildFormField("GoToTrainingPlan",
                                                                _GlobalResources.GoToTrainingPlan,
                                                                this._GoToTrainingPlan.ID,
                                                                this._GoToTrainingPlan,
                                                                true,
                                                                true,
                                                                false));

            // attach panel to container
            this.WebMeetingIntegrationPropertiesTabPanelsContainer.Controls.Add(goToTrainingPanel);
        }
        #endregion

        #region _BuildPropertiesFormWebExPanel
        /// <summary>
        /// Method to build the controls for WebEx tab
        /// </summary>
        private void _BuildPropertiesFormWebExPanel()
        {
            Panel webExPanel = new Panel();
            webExPanel.ID = "WebMeetingIntegrationProperties_" + "WebEx" + "_TabPanel";

            if (!this._IsGoToMeetingEnabled && !this._IsGoToWebinarEnabled && !this._IsGoToTrainingEnabled)
            { webExPanel.Attributes.Add("style", "display: block;"); }
            else
            { webExPanel.Attributes.Add("style", "display: none;"); }

            // on/off field
            this._WebExOn = new RadioButtonList();
            this._WebExOn.ID = "WebExOn_Field";
            this._WebExOn.CssClass = "HorizontalRadioList";
            this._WebExOn.Items.Add(new ListItem(_GlobalResources.On, "True"));
            this._WebExOn.Items.Add(new ListItem(_GlobalResources.Off, "False"));
            this._WebExOn.Attributes.Add("onclick", "SetWebMeetingIntegrationOnOff('WebEx');");

            webExPanel.Controls.Add(AsentiaPage.BuildFormField("WebExOn",
                                                         _GlobalResources.WebExIntegration,
                                                         this._WebExOn.ID,
                                                         this._WebExOn,
                                                         true,
                                                         true,
                                                         false));

            // application key field
            this._WebExApplicationKey = new TextBox();
            this._WebExApplicationKey.ID = "WebExApplicationKey_Field";
            this._WebExApplicationKey.CssClass = "InputLong";

            webExPanel.Controls.Add(AsentiaPage.BuildFormField("WebExApplicationKey",
                                                         _GlobalResources.WebExSiteName,
                                                         this._WebExApplicationKey.ID,
                                                         this._WebExApplicationKey,
                                                         true,
                                                         true,
                                                         false));

            // username field
            this._WebExUsername = new TextBox();
            this._WebExUsername.ID = "WebExUsername_Field";
            this._WebExUsername.CssClass = "InputMedium";

            webExPanel.Controls.Add(AsentiaPage.BuildFormField("WebExUsername",
                                                         _GlobalResources.DefaultOrganizerUsername,
                                                         this._WebExUsername.ID,
                                                         this._WebExUsername,
                                                         true,
                                                         true,
                                                         false));

            // password field
            this._WebExPassword = new TextBox();
            this._WebExPassword.ID = "WebExPassword_Field";
            this._WebExPassword.TextMode = TextBoxMode.Password;
            this._WebExPassword.CssClass = "InputShort";

            webExPanel.Controls.Add(AsentiaPage.BuildFormField("WebExPassword",
                                                         _GlobalResources.DefaultOrganizerPassword,
                                                         this._WebExPassword.ID,
                                                         this._WebExPassword,
                                                         true,
                                                         true,
                                                         false));

            // plan field
            this._WebExPlan = new RadioButtonList();
            this._WebExPlan.ID = "WebExPlan_Field";
            this._WebExPlan.Items.Add(new ListItem(_GlobalResources.WebExStarter50Attendees, WebExAPI.STARTER_PLAN_NAME));
            this._WebExPlan.Items.Add(new ListItem(_GlobalResources.WebExPlus100Attendees, WebExAPI.PLUS_PLAN_NAME));
            this._WebExPlan.Items.Add(new ListItem(_GlobalResources.WebExBusiness200Attendees, WebExAPI.BUSINESS_PLAN_NAME));
            this._WebExPlan.Items.Add(new ListItem(_GlobalResources.WebExEnterprise1000Attendees, WebExAPI.ENTERPRISE_PLAN_NAME));
            this._WebExPlan.Items.Add(new ListItem(_GlobalResources.WebExPremium8Attendees, WebExAPI.PREMIUM8_PLAN_NAME));
            this._WebExPlan.Items.Add(new ListItem(_GlobalResources.WebExPremium25Attendees, WebExAPI.PREMIUM25_PLAN_NAME));
            this._WebExPlan.Items.Add(new ListItem(_GlobalResources.WebExPremium200Attendees, WebExAPI.PREMIUM200_PLAN_NAME));

            webExPanel.Controls.Add(AsentiaPage.BuildFormField("WebExPlan",
                                                         _GlobalResources.WebExPlan,
                                                         this._WebExPlan.ID,
                                                         this._WebExPlan,
                                                         true,
                                                         true,
                                                         false));

            // attach panel to container
            this.WebMeetingIntegrationPropertiesTabPanelsContainer.Controls.Add(webExPanel);
        }
        #endregion

        #region _BuildPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for properties actions.
        /// </summary>
        private void _BuildPropertiesActionsPanel()
        {
            // clear controls from container
            this.PropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.PropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this.PropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.PropertiesActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            // GO TO MEETING
            if (this._IsGoToMeetingEnabled)
            {
                this._GoToMeetingOn.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_ON);
                this._GoToMeetingApplicationKey.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_APPLICATION);
                this._GoToMeetingConsumerSecret.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_CONSUMERSECRET);
                this._GoToMeetingUsername.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_USERNAME);
                this._GoToMeetingPlan.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN);
            }

            // GO TO WEBINAR
            if (this._IsGoToWebinarEnabled)
            {
                this._GoToWebinarOn.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_ON);
                this._GoToWebinarApplicationKey.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION);
                this._GoToWebinarConsumerSecret.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET);
                this._GoToWebinarUsername.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_USERNAME);
                this._GoToWebinarPlan.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PLAN);
            }

            // GO TO TRAINING
            if (this._IsGoToTrainingEnabled)
            {
                this._GoToTrainingOn.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_ON);
                this._GoToTrainingApplicationKey.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION);
                this._GoToTrainingConsumerSecret.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET);
                this._GoToTrainingUsername.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_USERNAME);
                this._GoToTrainingPlan.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PLAN);
            }

            // WEBEX
            if (this._IsWebExEnabled)
            {
                this._WebExOn.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_ON);
                this._WebExApplicationKey.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION);
                this._WebExUsername.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_USERNAME);
                this._WebExPlan.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PLAN);
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            // clear the error class from all fields, this is done in the event of multiple postbacks where
            // a field that previously had an error no longer has an error
            AsentiaPage.ClearErrorClassFromFieldsRecursive(this.WebMeetingIntegrationPropertiesContainer);

            bool isValid = true;
            bool gtmTabHasErrors = false;
            bool gtwTabHasErrors = false;
            bool gttTabHasErrors = false;
            bool webExTabHasErrors = false;                           

            // GO TO MEETING

            if (this._IsGoToMeetingEnabled && this._GoToMeetingOn.SelectedValue == "True")
            {                
                if (String.IsNullOrWhiteSpace(this._GoToMeetingApplicationKey.Text))
                {
                    isValid = false;
                    gtmTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToMeetingApplicationKey", _GlobalResources.ApplicationKey + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._GoToMeetingUsername.Text))
                {
                    isValid = false;
                    gtmTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToMeetingUsername", _GlobalResources.Username + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._GoToMeetingPassword.Text) && String.IsNullOrWhiteSpace(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PASSWORD)))
                {
                    isValid = false;
                    gtmTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToMeetingPassword", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._GoToMeetingPlan.SelectedValue))
                {
                    isValid = false;
                    gtmTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToMeetingPlan", _GlobalResources.Plan + " " + _GlobalResources.IsRequired);
                }
            }

            // GO TO WEBINAR

            if (this._IsGoToWebinarEnabled && this._GoToWebinarOn.SelectedValue == "True")
            {
                if (String.IsNullOrWhiteSpace(this._GoToWebinarApplicationKey.Text))
                {
                    isValid = false;
                    gtwTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToWebinarApplicationKey", _GlobalResources.ApplicationKey + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._GoToWebinarUsername.Text))
                {
                    isValid = false;
                    gtwTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToWebinarUsername", _GlobalResources.Username + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._GoToWebinarPassword.Text) && String.IsNullOrWhiteSpace(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PASSWORD)))
                {
                    isValid = false;
                    gtwTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToWebinarPassword", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._GoToWebinarPlan.SelectedValue))
                {
                    isValid = false;
                    gtwTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToWebinarPlan", _GlobalResources.Plan + " " + _GlobalResources.IsRequired);
                }
            }

            // GO TO TRAINING

            if (this._IsGoToTrainingEnabled && this._GoToTrainingOn.SelectedValue == "True")
            {
                if (String.IsNullOrWhiteSpace(this._GoToTrainingApplicationKey.Text))
                {
                    isValid = false;
                    gttTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToTrainingApplicationKey", _GlobalResources.ApplicationKey + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._GoToTrainingUsername.Text))
                {
                    isValid = false;
                    gttTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToTrainingUsername", _GlobalResources.Username + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._GoToTrainingPassword.Text) && String.IsNullOrWhiteSpace(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PASSWORD)))
                {
                    isValid = false;
                    gttTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToTrainingPassword", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._GoToTrainingPlan.SelectedValue))
                {
                    isValid = false;
                    gttTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "GoToTrainingPlan", _GlobalResources.Plan + " " + _GlobalResources.IsRequired);
                }
            }

            // WEBEX

            if (this._IsWebExEnabled && this._WebExOn.SelectedValue == "True")
            {
                if (String.IsNullOrWhiteSpace(this._WebExApplicationKey.Text))
                {
                    isValid = false;
                    webExTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "WebExApplicationKey", _GlobalResources.ApplicationKey + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._WebExUsername.Text))
                {
                    isValid = false;
                    webExTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "WebExUsername", _GlobalResources.Username + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._WebExPassword.Text) && String.IsNullOrWhiteSpace(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PASSWORD)))
                {
                    isValid = false;
                    webExTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "WebExPassword", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._WebExPlan.SelectedValue))
                {
                    isValid = false;
                    webExTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.WebMeetingIntegrationPropertiesContainer, "WebExPlan", _GlobalResources.Plan + " " + _GlobalResources.IsRequired);
                }
            }

            // apply error image and class to tabs if they have errors
            if (gtmTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.WebMeetingIntegrationPropertiesContainer, "WebMeetingIntegrationProperties_GoToMeeting_TabLI"); }

            if (gtwTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.WebMeetingIntegrationPropertiesContainer, "WebMeetingIntegrationProperties_GoToWebinar_TabLI"); }

            if (gttTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.WebMeetingIntegrationPropertiesContainer, "WebMeetingIntegrationProperties_GoToTraining_TabLI"); }

            if (webExTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.WebMeetingIntegrationPropertiesContainer, "WebMeetingIntegrationProperties_WebEx_TabLI"); }

            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                DataTable siteParams = new DataTable();
                siteParams.Columns.Add("key");
                siteParams.Columns.Add("value");

                DataRow newSiteParamRow;
                AsentiaAESEncryption cryptoProvider = new AsentiaAESEncryption();
                
                // GO TO MEETING

                if (this._IsGoToMeetingEnabled)
                {
                    // on/off
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTM_ON;
                    newSiteParamRow["value"] = this._GoToMeetingOn.SelectedValue;
                    siteParams.Rows.Add(newSiteParamRow);

                    // application key
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTM_APPLICATION;
                    newSiteParamRow["value"] = this._GoToMeetingOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToMeetingApplicationKey.Text) ? null : this._GoToMeetingApplicationKey.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // consumer secret
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTM_CONSUMERSECRET;
                    newSiteParamRow["value"] = this._GoToMeetingOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToMeetingConsumerSecret.Text) ? null : this._GoToMeetingConsumerSecret.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // username
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTM_USERNAME;
                    newSiteParamRow["value"] = this._GoToMeetingOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToMeetingUsername.Text) ? null : this._GoToMeetingUsername.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // password
                    if (!String.IsNullOrWhiteSpace(this._GoToMeetingPassword.Text) && this._GoToMeetingOn.SelectedValue == "True")
                    {
                        newSiteParamRow = siteParams.NewRow();
                        newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTM_PASSWORD;
                        newSiteParamRow["value"] = cryptoProvider.Encrypt(this._GoToMeetingPassword.Text);
                        siteParams.Rows.Add(newSiteParamRow);
                    }
                    else if (this._GoToMeetingOn.SelectedValue == "False")
                    {
                        newSiteParamRow = siteParams.NewRow();
                        newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTM_PASSWORD;
                        newSiteParamRow["value"] = null;
                        siteParams.Rows.Add(newSiteParamRow);
                    }

                    // plan
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTM_PLAN;
                    newSiteParamRow["value"] = this._GoToMeetingOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToMeetingPlan.SelectedValue) ? null : this._GoToMeetingPlan.SelectedValue;
                    siteParams.Rows.Add(newSiteParamRow);
                }

                // GO TO WEBINAR

                if (this._IsGoToWebinarEnabled)
                {
                    // on/off
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTW_ON;
                    newSiteParamRow["value"] = this._GoToWebinarOn.SelectedValue;
                    siteParams.Rows.Add(newSiteParamRow);

                    // application key
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTW_APPLICATION;
                    newSiteParamRow["value"] = this._GoToWebinarOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToWebinarApplicationKey.Text) ? null : this._GoToWebinarApplicationKey.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // consumer secret
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET;
                    newSiteParamRow["value"] = this._GoToWebinarOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToWebinarConsumerSecret.Text) ? null : this._GoToWebinarConsumerSecret.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // username
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTW_USERNAME;
                    newSiteParamRow["value"] = this._GoToWebinarOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToWebinarUsername.Text) ? null : this._GoToWebinarUsername.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // password
                    if (!String.IsNullOrWhiteSpace(this._GoToWebinarPassword.Text) && this._GoToWebinarOn.SelectedValue == "True")
                    {
                        newSiteParamRow = siteParams.NewRow();
                        newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTW_PASSWORD;
                        newSiteParamRow["value"] = cryptoProvider.Encrypt(this._GoToWebinarPassword.Text);
                        siteParams.Rows.Add(newSiteParamRow);
                    }
                    else if (this._GoToWebinarOn.SelectedValue == "False")
                    {
                        newSiteParamRow = siteParams.NewRow();
                        newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTW_PASSWORD;
                        newSiteParamRow["value"] = null;
                        siteParams.Rows.Add(newSiteParamRow);
                    }

                    // plan
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTW_PLAN;
                    newSiteParamRow["value"] = this._GoToWebinarOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToWebinarPlan.SelectedValue) ? null : this._GoToWebinarPlan.SelectedValue;
                    siteParams.Rows.Add(newSiteParamRow);
                }

                // GO TO TRAINING

                if (this._IsGoToTrainingEnabled)
                {
                    // on/off
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTT_ON;
                    newSiteParamRow["value"] = this._GoToTrainingOn.SelectedValue;
                    siteParams.Rows.Add(newSiteParamRow);

                    // application key
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTT_APPLICATION;
                    newSiteParamRow["value"] = this._GoToTrainingOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToTrainingApplicationKey.Text) ? null : this._GoToTrainingApplicationKey.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // consumer secret
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET;
                    newSiteParamRow["value"] = this._GoToTrainingOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToTrainingConsumerSecret.Text) ? null : this._GoToTrainingConsumerSecret.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // username
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTT_USERNAME;
                    newSiteParamRow["value"] = this._GoToTrainingOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToTrainingUsername.Text) ? null : this._GoToTrainingUsername.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // password
                    if (!String.IsNullOrWhiteSpace(this._GoToTrainingPassword.Text) && this._GoToTrainingOn.SelectedValue == "True")
                    {
                        newSiteParamRow = siteParams.NewRow();
                        newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTT_PASSWORD;
                        newSiteParamRow["value"] = cryptoProvider.Encrypt(this._GoToTrainingPassword.Text);
                        siteParams.Rows.Add(newSiteParamRow);
                    }
                    else if (this._GoToTrainingOn.SelectedValue == "False")
                    {
                        newSiteParamRow = siteParams.NewRow();
                        newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTT_PASSWORD;
                        newSiteParamRow["value"] = null;
                        siteParams.Rows.Add(newSiteParamRow);
                    }

                    // plan
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_GTT_PLAN;
                    newSiteParamRow["value"] = this._GoToTrainingOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._GoToTrainingPlan.SelectedValue) ? null : this._GoToTrainingPlan.SelectedValue;
                    siteParams.Rows.Add(newSiteParamRow);
                }

                // WEBEX

                if (this._IsWebExEnabled)
                {
                    // on/off
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_WEBEX_ON;
                    newSiteParamRow["value"] = this._WebExOn.SelectedValue;                    
                    siteParams.Rows.Add(newSiteParamRow);

                    // application key
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_WEBEX_APPLICATION;
                    newSiteParamRow["value"] = this._WebExOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._WebExApplicationKey.Text) ? null : this._WebExApplicationKey.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // username
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_WEBEX_USERNAME;
                    newSiteParamRow["value"] = this._WebExOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._WebExUsername.Text) ? null : this._WebExUsername.Text;
                    siteParams.Rows.Add(newSiteParamRow);

                    // password
                    if (!String.IsNullOrWhiteSpace(this._WebExPassword.Text) && this._WebExOn.SelectedValue == "True")
                    {
                        newSiteParamRow = siteParams.NewRow();
                        newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_WEBEX_PASSWORD;
                        newSiteParamRow["value"] = cryptoProvider.Encrypt(this._WebExPassword.Text);
                        siteParams.Rows.Add(newSiteParamRow);
                    }
                    else if (this._WebExOn.SelectedValue == "False")
                    {
                        newSiteParamRow = siteParams.NewRow();
                        newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_WEBEX_PASSWORD;
                        newSiteParamRow["value"] = null;
                        siteParams.Rows.Add(newSiteParamRow);
                    }

                    // plan
                    newSiteParamRow = siteParams.NewRow();
                    newSiteParamRow["key"] = SiteParamConstants.WEBMEETING_WEBEX_PLAN;
                    newSiteParamRow["value"] = this._WebExOn.SelectedValue == "False" || String.IsNullOrWhiteSpace(this._WebExPlan.SelectedValue) ? null : this._WebExPlan.SelectedValue;
                    siteParams.Rows.Add(newSiteParamRow);
                }                

                // save the site params
                AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, siteParams);

                // reload global site object
                AsentiaSessionState.GlobalSiteObject = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, true);

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedback(_GlobalResources.ConfigurationHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }            
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("/dashboard");
        }
        #endregion
    }
}