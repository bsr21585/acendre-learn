﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.System.Logs
{
    public class ExceptionLog : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel LogFormContentWrapperContainer;
        public Panel ActionsPanel;
        public UpdatePanel ExceptionLogGridUpdatePanel;
        public Grid ExceptionLogGrid;
        public ModalPopup GridConfirmAction = new ModalPopup();
        public LinkButton DeleteButton = new LinkButton();
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_Logs))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Logs, "/administrator/system/logs"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ExceptionLog));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.ExceptionLog, ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EXCEPTION,
                                                                                                                   ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.LogFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the grid
            this._BuildGrid();

            // bind the grid
            this.ExceptionLogGrid.BindData();
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // apply css class to container
            this.ExceptionLogGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.ExceptionLogGrid.StoredProcedure = Library.ExceptionLog.GridProcedure;
            this.ExceptionLogGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.ExceptionLogGrid.AddFilter("@idSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.ExceptionLogGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.ExceptionLogGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.ExceptionLogGrid.IdentifierField = "idExceptionLog";
            this.ExceptionLogGrid.DefaultSortColumn = "timestamp";
            this.ExceptionLogGrid.AddCheckboxColumn = false;
            this.ExceptionLogGrid.ShowTimeInDateStrings = true;

            // data key names
            this.ExceptionLogGrid.DataKeyNames = new string[] { "idExceptionLog", "exceptionMessage", "page" };

            // columns
            GridColumn exceptionMessage = new GridColumn(_GlobalResources.Message, "exceptionMessage", "exceptionMessage");
            exceptionMessage.AddProperty(new GridColumnProperty("<a href=\"##page##\">##exceptionMessage##</a>"));
            GridColumn exceptionType = new GridColumn(_GlobalResources.Type, "exceptionType", "exceptionType");
            GridColumn user = new GridColumn(_GlobalResources.User, "user", "user");
            GridColumn timestamp = new GridColumn(_GlobalResources.Timestamp, "timestamp", "timestamp");

            // add columns to data grid
            this.ExceptionLogGrid.AddColumn(exceptionMessage);
            this.ExceptionLogGrid.AddColumn(exceptionType);
            this.ExceptionLogGrid.AddColumn(user);
            this.ExceptionLogGrid.AddColumn(timestamp);          
        }
        #endregion
    }
}