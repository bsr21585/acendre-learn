﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml;
using System.IO;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.System.Logs
{
    public class EmailLog : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel LogFormContentWrapperContainer;
        public Panel LogWrapperContainer;
        public Panel ObjectOptionsPanel;
        public Panel FailedFeedbackContainer;
        public UpdatePanel SentGridUpdatePanel, FailedGridUpdatePanel;
        public Panel TabContainer, TabContentWrapperPanel;
        public Grid SentGrid, FailedGrid;
        public Panel ActionsPanel;

        public LinkButton DeleteButton = new LinkButton();
        public LinkButton RetryButton = new LinkButton();

        public ModalPopup GridConfirmAction;
        public ModalPopup GridRetryConfirmAction;
        #endregion

        #region Private Properties
        private Panel _EventLogTabPanelsContainer;

        private ModalPopup _EmailNotificationDetailsModal;
        private Button _HiddenEmailNotificationDetailsModalLaunchButton;

        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_Logs))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/system/logs/EmailLog.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Logs, "/administrator/system/logs"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.EmailNotificationsLog));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.EmailNotificationsLog, ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EMAIL,
                                                                                                                            ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.LogFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.LogWrapperContainer.CssClass = "FormContentContainer";

            this._BuildTabsList();
            this._LoadPageControls(); 

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.SentGrid.BindData();
                this.FailedGrid.BindData();
            }
        }
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ScriptManager.RegisterClientScriptResource(this.Page, typeof(EmailLog), "Asentia.LMS.Pages.Administrator.System.Logs.EmailLog.js");

        }
        #endregion
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for each grid.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Sent", _GlobalResources.Sent));
            tabs.Enqueue(new KeyValuePair<string, string>("Failed", _GlobalResources.Failed));

            // build and attach the tabs
            this.TabContainer.Controls.Add(AsentiaPage.BuildTabListPanel("EmailLog", tabs));
        }
        #endregion

        #region _LoadPageControls
        /// <summary>
        /// Setup the Tags/Controls on the page
        /// </summary>
        private void _LoadPageControls()
        {
            this.TabContentWrapperPanel.ID = this.ID + "_" + "TabContentWrapperPanel";
            this.TabContentWrapperPanel.CssClass = "TabPanelsContentContainer";
            
            this._BuildGridContent();
            this._BuildEmailNotificationDetailsModal();
        }
        #endregion

        #region _BuildGridContent
        /// <summary>
        /// Builds the tab content container for specific purpose of each tab
        /// </summary>
        private void _BuildGridContent()
        {
            //Sent tab panel
            this._BuildSentTabContent();

            //Failed tab panel
            this._BuildFailedTabContent();

            this._EventLogTabPanelsContainer = new Panel();
            this._EventLogTabPanelsContainer.ID = "EmailLog_TabPanelsContainer";
            this._EventLogTabPanelsContainer.CssClass = "TabPanelsContentContainer";

            //Email Notification Details Hidden Modal Launch Button
            this._HiddenEmailNotificationDetailsModalLaunchButton = new Button();
            this._HiddenEmailNotificationDetailsModalLaunchButton.ID = "HiddenEmailNotificationDetailsModalLaunchButton";
            this._HiddenEmailNotificationDetailsModalLaunchButton.Style.Add("display", "none");
            this._EventLogTabPanelsContainer.Controls.Add(this._HiddenEmailNotificationDetailsModalLaunchButton);

            //Add panels to body
            this.TabContainer.Controls.Add(this._EventLogTabPanelsContainer);
        }
        #endregion

        #region _BuildSentTabContent
        /// <summary>
        /// Builds the tab content container for Sent tab
        /// </summary>
        private void _BuildSentTabContent()
        {
            this.SentGridUpdatePanel.ID = "EmailLog_" + "Sent" + "_TabPanel";
            this.SentGridUpdatePanel.Attributes.Add("style", "display: block;");
            this.SentGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            // build and bind the sent grid
            this._BuildSentGrid();
        }
        #endregion

        #region _BuildFailedTabContent
        /// <summary>
        /// Builds the tab content container for Failed tab
        /// </summary>
        private void _BuildFailedTabContent()
        {
            this.FailedGridUpdatePanel.ID = "EmailLog_" + "Failed" + "_TabPanel";
            this.FailedGridUpdatePanel.Attributes.Add("style", "display: none;");
            this.FailedGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            // build and bind the sent grid
            this._BuildFailedGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();
            this._BuildGridRetryActionsModal();
        }
        #endregion

        #region _BuildSentGrid
        /// <summary>
        /// Builds the Sent Grid for the page.
        /// </summary>
        private void _BuildSentGrid()
        {
            // apply css class to container
            this.SentGridUpdatePanel.Attributes.Add("class", "FormContentContainer");
            this.SentGrid.StoredProcedure = Library.EventEmailQueue.SentGridProcedure;
            this.SentGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.SentGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.SentGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.SentGrid.IdentifierField = "idEventEmailQueue";
            this.SentGrid.AddCheckboxColumn = false;
            this.SentGrid.DefaultSortColumn = "dtSent";
            this.SentGrid.IsDefaultSortDescending = true;
            this.SentGrid.ShowTimeInDateStrings = true;

            // data key names
            this.SentGrid.DataKeyNames = new string[] { "idEventEmailQueue" };

            // columns
            GridColumn fullName = new GridColumn(_GlobalResources.FullName + " (" + _GlobalResources.Username + "), " + _GlobalResources.EmailAddress, null, "fullName");
            GridColumn notificationName = new GridColumn(_GlobalResources.Notification, null, "notificationName");
            GridColumn sentDate = new GridColumn(_GlobalResources.SentDate, "dtSent", "dtSent");

            // add columns to data grid
            this.SentGrid.AddColumn(fullName);
            this.SentGrid.AddColumn(notificationName);
            this.SentGrid.AddColumn(sentDate);

            this.SentGrid.RowDataBound += _SentGrid_RowDataBound;

        }

        /// <summary>
        /// Row Data Bound event for the sent grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _SentGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                string fullName = rowView["fullName"].ToString();
                string username = rowView["username"].ToString();
                string email = rowView["email"].ToString();                

                // full name
                Label fullNameLabel = new Label();
                fullNameLabel.CssClass = "GridBaseTitle";
                fullNameLabel.Text = fullName;
                e.Row.Cells[0].Controls.Add(fullNameLabel);

                // username
                Label usernameLabel = new Label();
                usernameLabel.CssClass = "GridSecondaryTitle";
                usernameLabel.Text = "(" + username + ")";
                e.Row.Cells[0].Controls.Add(usernameLabel);

                // email
                Label emailLabel = new Label();
                emailLabel.CssClass = "GridSecondaryLine";
                e.Row.Cells[0].Controls.Add(emailLabel);

                HyperLink emailLink = new HyperLink();
                emailLink.NavigateUrl = "mailto:" + email;
                emailLink.Text = email;
                emailLabel.Controls.Add(emailLink);

                // notification name
                string notificationName = rowView["notificationName"].ToString();
                
                Label notificationNameLabel = new Label();                
                notificationNameLabel.Text = notificationName;

                string idEventEmailQueue = rowView["idEventEmailQueue"].ToString();

                // details link to show the email details
                HyperLink detailsLink = new HyperLink();
                detailsLink.ID = "EmailNotificationDetails_" + idEventEmailQueue;
                detailsLink.ToolTip = _GlobalResources.ClickToViewEmailDetails;
                detailsLink.Text = notificationName;
                detailsLink.Attributes.Add("onclick", "EmailNotificationDetails(" + idEventEmailQueue + ");");

                if (!Convert.ToBoolean(rowView["isDeleted"]))
                { e.Row.Cells[1].Controls.Add(detailsLink); }
                else
                { e.Row.Cells[1].Controls.Add(notificationNameLabel); }
            }
        }
        #endregion

        #region _BuildFailedGrid
        /// <summary>
        /// Builds the Failed Grid for the page.
        /// </summary>
        private void _BuildFailedGrid()
        {
            this.FailedGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.FailedGrid.StoredProcedure = Library.EventEmailQueue.FailedGridProcedure;
            this.FailedGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.FailedGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.FailedGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.FailedGrid.IdentifierField = "idEventEmailQueue";
            this.FailedGrid.DefaultSortColumn = "dtAction";
            this.FailedGrid.IsDefaultSortDescending = true;
            this.FailedGrid.ShowTimeInDateStrings = true;

            // data key names
            this.FailedGrid.DataKeyNames = new string[] { "idEventEmailQueue" };

            // columns
            GridColumn fullName = new GridColumn(_GlobalResources.FullName + " (" + _GlobalResources.Username + "), " + _GlobalResources.EmailAddress, null, "fullName");
            GridColumn notificationName = new GridColumn(_GlobalResources.Notification, "notificationName", "notificationName");
            GridColumn actionDate = new GridColumn(_GlobalResources.AttemptDate, "dtAction", "dtAction");
            GridColumn status = new GridColumn(_GlobalResources.ErrorStatus, "status", "status");

            // add columns to data grid
            this.FailedGrid.AddColumn(fullName);
            this.FailedGrid.AddColumn(notificationName);
            this.FailedGrid.AddColumn(actionDate);
            this.FailedGrid.AddColumn(status);

            this.FailedGrid.RowDataBound += _FailedGrid_RowDataBound;

        }

        /// <summary>
        /// Row Data Bound event for the failed grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _FailedGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowFailedView = (DataRowView)e.Row.DataItem;
                string fullName = rowFailedView["fullName"].ToString();
                string username = rowFailedView["username"].ToString();
                string email = rowFailedView["email"].ToString();

                // full name
                Label fullNameLabel = new Label();
                fullNameLabel.CssClass = "GridBaseTitle";
                fullNameLabel.Text = fullName;
                e.Row.Cells[1].Controls.Add(fullNameLabel);

                // username
                Label usernameLabel = new Label();
                usernameLabel.CssClass = "GridSecondaryTitle";
                usernameLabel.Text = "(" + username + ")";
                e.Row.Cells[1].Controls.Add(usernameLabel);

                // email
                Label emailLabel = new Label();
                emailLabel.CssClass = "GridSecondaryLine";
                e.Row.Cells[1].Controls.Add(emailLabel);

                HyperLink emailLink = new HyperLink();
                emailLink.NavigateUrl = "mailto:" + email;
                emailLink.Text = email;
                emailLabel.Controls.Add(emailLink);
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Failed Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this.DeleteButton);

            // retry button
            this.RetryButton.ID = "GridRetryButton";

            // retry button image
            Image retryImage = new Image();
            retryImage.ID = "GridRetryButtonImage";
            retryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RESET,
                                                         ImageFiles.EXT_PNG);
            retryImage.CssClass = "MediumIcon";
            retryImage.AlternateText = _GlobalResources.Retry;
            this.RetryButton.Controls.Add(retryImage);

            // retry button text
            Literal retryText = new Literal();
            retryText.Text = _GlobalResources.Retry;
            this.RetryButton.Controls.Add(retryText);

            // add retry button to panel
            this.ActionsPanel.Controls.Add(this.RetryButton);

        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedQueuedItem_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheSelectedQueuedItem_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _BuildGridRetryActionsModal
        /// <summary>
        /// Builds the confirmation modal for action retry performed on Grid data.
        /// </summary>
        private void _BuildGridRetryActionsModal()
        {
            this.GridRetryConfirmAction = new ModalPopup("GridRetryConfirmAction"); ;

            // set modal properties
            this.GridRetryConfirmAction.Type = ModalPopupType.Confirm;
            this.GridRetryConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_RESET,
                                                                                ImageFiles.EXT_PNG);
            this.GridRetryConfirmAction.HeaderIconAlt = _GlobalResources.Retry;
            this.GridRetryConfirmAction.HeaderText = _GlobalResources.RetrySelectedNotification_s;
            this.GridRetryConfirmAction.TargetControlID = this.RetryButton.ClientID;
            this.GridRetryConfirmAction.SubmitButton.Command += new CommandEventHandler(this._RetryButton_Command);

            // build the modal body
            HtmlGenericControl bodyWrapper = new HtmlGenericControl("p");
            bodyWrapper.ID = "GridRetryConfirmActionModalBody1";

            Literal body = new Literal();
            body.Text = _GlobalResources.AreYouSureYouWantToAttemptToSendTheseItem_sAgain;

            bodyWrapper.Controls.Add(body);

            // add controls to body
            this.GridRetryConfirmAction.AddControlToBody(bodyWrapper);

            this.ActionsPanel.Controls.Add(this.GridRetryConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.FailedGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.FailedGrid.Rows[i].FindControl(this.FailedGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.EventEmailQueue.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, _GlobalResources.TheSelectedQueuedItem_sHaveBeenDeletedSuccessfully, false);

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.FailedGrid.BindData();
            }
        }
        #endregion

        #region _RetryButton_Command
        /// <summary>
        /// Performs the retry action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _RetryButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToRetry = new DataTable();
                recordsToRetry.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.FailedGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.FailedGrid.Rows[i].FindControl(this.FailedGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToRetry.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.EventEmailQueue.Retry(recordsToRetry);

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, _GlobalResources.TheSelectedItem_sHaveBeenSentSuccessfully, false);

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.FailedGrid.BindData();
            }
        }
        #endregion

        #region _BuildEmailNotificationDetailsModal
        /// <summary>
        /// Builds the modal used to view email notification details.
        /// </summary>        
        private void _BuildEmailNotificationDetailsModal()
        {
            this._EmailNotificationDetailsModal = new ModalPopup("EmailNotificationDetailsModal");
            this._EmailNotificationDetailsModal.Type = ModalPopupType.Information;
            this._EmailNotificationDetailsModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EMAIL, ImageFiles.EXT_PNG);
            this._EmailNotificationDetailsModal.HeaderIconAlt = _GlobalResources.EmailNotificationDetails;
            this._EmailNotificationDetailsModal.HeaderText = _GlobalResources.EmailNotificationDetails;
            this._EmailNotificationDetailsModal.TargetControlID = "HiddenEmailNotificationDetailsModalLaunchButton";
            this._EmailNotificationDetailsModal.ShowLoadingPlaceholder = true;

            // build the email notification details container
            Panel emailNotificationDetailsPanel = new Panel();
            emailNotificationDetailsPanel.ID = "EmailNotificationDetailsModal_EmailNotificationDetailsPanel";

            // sent to label
            Label toLabel = new Label();
            toLabel.ID = "EmailNotificationSentTo";
            emailNotificationDetailsPanel.Controls.Add(AsentiaPage.BuildFormField("EmailNotificationSentTo",
                                                                    _GlobalResources.To, null, toLabel, false, false, false));

            // sent date label
            Label sentDateLabel = new Label();
            sentDateLabel.ID = "EmailNotificationSentDate";
            emailNotificationDetailsPanel.Controls.Add(AsentiaPage.BuildFormField("EmailNotificationSentDate",
                                                                    _GlobalResources.SentDate, null, sentDateLabel, false, false, false));

            // subject label
            Label subjectLabel = new Label();
            subjectLabel.ID = "EmailNotificationSubject";
            emailNotificationDetailsPanel.Controls.Add(AsentiaPage.BuildFormField("EmailNotificationSubject",
                                                                    _GlobalResources.Subject, null, subjectLabel, false, false, false));

            // body panel
            Panel bodyPanel = new Panel();
            bodyPanel.ID = "EmailNotificationBody";
            emailNotificationDetailsPanel.Controls.Add(AsentiaPage.BuildFormField("EmailNotificationBody",
                                                                    _GlobalResources.Body, null, bodyPanel, false, false, false));

            this._EmailNotificationDetailsModal.AddControlToBody(emailNotificationDetailsPanel);

            // attach modal to container
            this._EventLogTabPanelsContainer.Controls.Add(this._EmailNotificationDetailsModal);
        }
        #endregion
    }
}