﻿function EmailNotificationDetails(idEventEmailQueue) {
    // clean the modal, launch the modal
    $('#EmailNotificationSentTo').val('');
    $('#EmailNotificationSentDate').val('');
    $('#EmailNotificationSubject').val(''); 
    $('#EmailNotificationBody').val('');
    $('#HiddenEmailNotificationDetailsModalLaunchButton').click();
    $('#EmailNotificationDetailsModalModalPopupBody').hide();
    $('#EmailNotificationDetailsModalModalPopupPostbackLoadingPlaceholder').show();
    $.ajax({
        type: "POST",
        url: "/_util/UtilityServices.asmx/GetEventEmailDetail",
        data: "{idEventEmailQueue:" + idEventEmailQueue + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: EmailNotificationDetailsSuccess,
        failure: function (response) {
            alert('failure');
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
}

function EmailNotificationDetailsSuccess(response) {
    $('#EmailNotificationDetailsModalModalPopupPostbackLoadingPlaceholder').hide();
    $('#EmailNotificationDetailsModalModalPopupBody').show();
    var responseObject = response.d;
    $('#EmailNotificationSentTo').text(responseObject.sentTo);
    $('#EmailNotificationSentDate').text(responseObject.sentDate);
    $('#EmailNotificationSubject').text(responseObject.emailSubject);
    $('#EmailNotificationBody').html(responseObject.emailBody);
}