﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.System.Logs
{
    public class EventLog : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel LogFormContentWrapperContainer;
        public Panel LogWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel EventLogGridUpdatePanel;
        public Grid EventLogGrid;
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_Logs))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Logs, "/administrator/system/logs"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.SystemEventLog));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.SystemEventLog, ImageFiles.GetIconPath(ImageFiles.ICON_LOG_SYSTEM,
                                                                                                                     ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.LogFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.LogWrapperContainer.CssClass = "FormContentContainer";

            // build and bind the grid
            this._BuildGrid();
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.EventLogGrid.StoredProcedure = Library.EventLog.SystemEventLogGridProcedure;
            this.EventLogGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.EventLogGrid.AddFilter("@idSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.EventLogGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.EventLogGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.EventLogGrid.IdentifierField = "idEventLog";
            this.EventLogGrid.DefaultSortColumn = "dtAction";
            this.EventLogGrid.IsDefaultSortDescending = true;
            this.EventLogGrid.AddCheckboxColumn = false;
            this.EventLogGrid.SearchStringFormatFTQ = false;

            // data key names
            this.EventLogGrid.DataKeyNames = new string[] { "idEventLog" };
            this.EventLogGrid.ShowTimeInDateStrings = true;

            // row data bound event
            this.EventLogGrid.RowDataBound += new GridViewRowEventHandler(_EventLogGrid_RowDataBound);

            // columns
            GridColumn eventType = new GridColumn(_GlobalResources.EventType, "idEventType", "idEventType");
            GridColumn objectName = new GridColumn(_GlobalResources.ObjectName, "objectName", "objectName");
            GridColumn targetUser = new GridColumn(_GlobalResources.TargetUser, "targetUser", "targetUser");
            GridColumn executingUser = new GridColumn(_GlobalResources.ExecutingUser, "executingUser", "executingUser");
            GridColumn timeStamp = new GridColumn(_GlobalResources.Timestamp, "dtAction", "dtAction");
            
            // add columns to data grid
            this.EventLogGrid.AddColumn(eventType);
            this.EventLogGrid.AddColumn(objectName);
            this.EventLogGrid.AddColumn(targetUser);
            this.EventLogGrid.AddColumn(executingUser);
            this.EventLogGrid.AddColumn(timeStamp);

            EventLogGrid.BindData();
        }
        #endregion

        #region _EventLogGrid_RowDataBound
        /// <summary>
        ///  Grid Row Bind Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _EventLogGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                #region Event Type Check
                TableCell eventTypeCell = e.Row.Cells[0];
                eventTypeCell.Text = EventEmailNotificationForm.GetEventTypeText(Convert.ToInt32(eventTypeCell.Text));
                #endregion
            }
        }
        #endregion   
    }
}
