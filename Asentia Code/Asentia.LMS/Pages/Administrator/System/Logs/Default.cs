﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.System.Logs
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel LogsFormContentWrapperContainer;
        public Panel LogsWrapperContainer;
        public Panel PageLinksContainer;
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_Logs))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Logs));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.Logs, ImageFiles.GetIconPath(ImageFiles.ICON_LOG,
                                                                                                           ImageFiles.EXT_PNG));

            // initialize admin menu
            this.InitializeAdminMenu();

            this.LogsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.LogsWrapperContainer.CssClass = "FormContentContainer";

            //build links
            this._BuildLinks();
        }
        #endregion

        #region Private Methods
        
        #region _BuildLinks
        /// <summary>
        /// Method to build links
        /// </summary>
        private void _BuildLinks()
        {
            // Email Notification Log
            Panel plEmailLog = new Panel();
            plEmailLog.CssClass = "LogLinksContainer";

            Image emailLogImage = new Image();
            emailLogImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EMAIL,
                                                            ImageFiles.EXT_PNG);
            emailLogImage.CssClass = "MediumIcon";
            plEmailLog.Controls.Add(emailLogImage);

            HyperLink addEmailNotificationLink = new HyperLink();
            addEmailNotificationLink.Text = _GlobalResources.EmailLog;
            addEmailNotificationLink.NavigateUrl = "EmailLog.aspx";
            plEmailLog.Controls.Add(addEmailNotificationLink);

            Panel plEmailDescription = new Panel();
            Label dataSetName = new Label();
            dataSetName.Text = _GlobalResources.ListingOfEmailNotificationsThatHaveBeenSentOrHaveFailedToBeSent;
            plEmailDescription.Controls.Add(dataSetName);
            plEmailLog.Controls.Add(plEmailDescription);

            // Report Subscription Log
            Panel plReportLog = new Panel();
            plReportLog.CssClass = "LogLinksContainer";

            Image reportLogImage = new Image();
            reportLogImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOG_REPORT,
                                                             ImageFiles.EXT_PNG);
            reportLogImage.CssClass = "MediumIcon";
            plReportLog.Controls.Add(reportLogImage);

            HyperLink reportLink = new HyperLink();
            reportLink.Text = _GlobalResources.ReportSubscriptionLog;
            reportLink.NavigateUrl = "ReportSubscriptionLog.aspx";
            plReportLog.Controls.Add(reportLink);

            Panel plReoprtDescription = new Panel();
            Label labelDescription = new Label();
            labelDescription.Text = _GlobalResources.ListingOfReportSubscriptionsThatHaveBeenSentOrHaveFailedToBeSent;
            plReoprtDescription.Controls.Add(labelDescription);
            plReportLog.Controls.Add(plReoprtDescription);

            // Exception Log
            Panel plExceptionLog = new Panel();
            plExceptionLog.CssClass = "LogLinksContainer";

            Image exceptionLogImage = new Image();
            exceptionLogImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EXCEPTION,
                                                                ImageFiles.EXT_PNG);
            exceptionLogImage.CssClass = "MediumIcon";
            plExceptionLog.Controls.Add(exceptionLogImage);

            HyperLink exceptionLink = new HyperLink();
            exceptionLink.Text = _GlobalResources.ExceptionLog;
            exceptionLink.NavigateUrl = "ExceptionLog.aspx";
            plExceptionLog.Controls.Add(exceptionLink);

            Panel plExceptionDescription = new Panel();
            Label labelExceptionDescription = new Label();
            labelExceptionDescription.Text = _GlobalResources.ListingOfSystemExceptionsErrorsThatHaveOccurred;
            plExceptionDescription.Controls.Add(labelExceptionDescription);
            plExceptionLog.Controls.Add(plExceptionDescription);

            // System Event Log
            Panel plSystemLog = new Panel();
            plSystemLog.CssClass = "LogLinksContainer";

            Image systemLogImage = new Image();
            systemLogImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOG_SYSTEM,
                                                             ImageFiles.EXT_PNG);
            systemLogImage.CssClass = "MediumIcon";
            plSystemLog.Controls.Add(systemLogImage);

            HyperLink systemLink = new HyperLink();
            systemLink.Text = _GlobalResources.SystemEventLog;
            systemLink.NavigateUrl = "EventLog.aspx";
            plSystemLog.Controls.Add(systemLink);

            Panel plSystemDescription = new Panel();
            Label labelSystemDescription = new Label();
            labelSystemDescription.Text = _GlobalResources.ListingOfSystemEventsThatHaveOccurred;
            plSystemDescription.Controls.Add(labelSystemDescription);
            plSystemLog.Controls.Add(plSystemDescription);

            this.PageLinksContainer.Controls.Add(plEmailLog);
            this.PageLinksContainer.Controls.Add(plReportLog);
            this.PageLinksContainer.Controls.Add(plExceptionLog);
            this.PageLinksContainer.Controls.Add(plSystemLog);
        }
        #endregion
        #endregion
    }
}