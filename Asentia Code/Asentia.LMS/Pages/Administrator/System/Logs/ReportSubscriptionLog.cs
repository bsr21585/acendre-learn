﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.System.Logs
{
    public class ReportSubscriptionLog : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel LogFormContentWrapperContainer;
        public Panel LogWrapperContainer;
        public Panel ObjectOptionsPanel;
        public Panel TabContainer = new Panel();
        public Panel FailedFeedbackContainer = new Panel();

        public LinkButton DeleteButton = new LinkButton();
        public LinkButton RetryButton = new LinkButton();

        public ModalPopup GridConfirmAction;
        public ModalPopup GridRetryConfirmAction;       
        #endregion

        #region Private Properties
        private UpdatePanel _SentGridUpdatePanel = new UpdatePanel();
        private UpdatePanel _FailedGridUpdatePanel = new UpdatePanel();

        private Grid _SentGrid;
        private Grid _FailedGrid;

        private Panel _ActionsPanel;
        private Panel _EventLogTabPanelsContainer;
        private Panel _TabSentContentPanel;
        private Panel _TabFailedContentPanel;
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_Logs))
            { Response.Redirect("/"); }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Logs, "/administrator/system/logs"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ReportSubscriptionLog));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.ReportSubscriptionLog, ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EMAIL,
                                                                                                                            ImageFiles.EXT_PNG));
            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.LogFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.LogWrapperContainer.CssClass = "FormContentContainer";

            this._BuildTabsList();
            this._BuildGridContent();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this._SentGrid.BindData();
                this._FailedGrid.BindData();
            }
        }
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for each grid.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Sent", _GlobalResources.Sent));
            tabs.Enqueue(new KeyValuePair<string, string>("Failed", _GlobalResources.Failed));            

            // build and attach the tabs
            this.TabContainer.Controls.Add(AsentiaPage.BuildTabListPanel("ReportSubscriptionLog", tabs));
        }
        #endregion

        #region _BuildGridContent
        /// <summary>
        /// Builds the tab content container for specific purpose of each tab
        /// </summary>
        private void _BuildGridContent()
        {
            //Sent tab panel
            this._TabSentContentPanel = new Panel();
            this._TabSentContentPanel.ID = "ReportSubscriptionLog_Sent_TabPanel";
            this._TabSentContentPanel.CssClass = "SentabContentPanel";
            this._BuildSentTabContent();

            //Failed tab panel
            this._TabFailedContentPanel = new Panel();
            this._TabFailedContentPanel.ID = "ReportSubscriptionLog_Failed_TabPanel";
            this._TabFailedContentPanel.CssClass = "ManageTabContentPanel";
            this._TabFailedContentPanel.Style.Add("display", "none");
            this._BuildFailedTabContent();

            //Add both tab panels to Event Log Tab Panel
            this._EventLogTabPanelsContainer = new Panel();
            this._EventLogTabPanelsContainer.ID = "ReportSubscriptionLog_TabPanelsContainer";
            this._EventLogTabPanelsContainer.CssClass = "TabPanelsContentContainer";

            this._EventLogTabPanelsContainer.Controls.Add(this._TabSentContentPanel);
            this._EventLogTabPanelsContainer.Controls.Add(this._TabFailedContentPanel);

            //Add panels to body
            this.TabContainer.Controls.Add(this._EventLogTabPanelsContainer);
        }
        #endregion

        #region _BuildSentTabContent
        /// <summary>
        /// Builds the tab content container for Sent tab
        /// </summary>
        private void _BuildSentTabContent()
        {
            this._SentGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Always;

            this._TabSentContentPanel.Controls.Add(this._SentGridUpdatePanel);

            // build and bind the sent grid
            this._BuildSentGrid();
        }
        #endregion

        #region _BuildFailedTabContent
        /// <summary>
        /// Builds the tab content container for Failed tab
        /// </summary>
        private void _BuildFailedTabContent()
        {
            this._FailedGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            this._TabFailedContentPanel.Controls.Add(this._FailedGridUpdatePanel);

            // build and bind the sent grid
            this._BuildFailedGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();
            this._BuildGridRetryActionsModal();
        }
        #endregion

        #region _BuildSentGrid
        /// <summary>
        /// Builds the Sent Grid for the page.
        /// </summary>
        private void _BuildSentGrid()
        {
            this._SentGrid = new Grid();
            this._SentGrid.StoredProcedure = Library.ReportSubscriptionQueue.SentGridProcedure;
            this._SentGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._SentGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._SentGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._SentGrid.IdentifierField = "idReportSubscriptionQueue";
            this._SentGrid.AddCheckboxColumn = false;
            this._SentGrid.DefaultSortColumn = "dtSent";
            this._SentGrid.IsDefaultSortDescending = true;
            this._SentGrid.ShowTimeInDateStrings = true;

            // data key names
            this._SentGrid.DataKeyNames = new string[] { "idReportSubscriptionQueue", "fullName", "email" };

            // columns
            GridColumn fullName = new GridColumn(_GlobalResources.To, "fullName", "fullName");
            fullName.AddProperty(new GridColumnProperty("##fullName## (<a href=\"mailto: ##email##\">##email##</a>)"));
            GridColumn notificationName = new GridColumn(_GlobalResources.Report, "reportName", "reportName");
            GridColumn sentDate = new GridColumn(_GlobalResources.SentDate, "dtSent", "dtSent");

            // add columns to data grid
            this._SentGrid.AddColumn(fullName);
            this._SentGrid.AddColumn(notificationName);
            this._SentGrid.AddColumn(sentDate);

            //Add grid to update panel
            this._SentGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._SentGrid);
        }
        #endregion

        #region _BuildFailedGrid
        /// <summary>
        /// Builds the Failed Grid for the page.
        /// </summary>
        private void _BuildFailedGrid()
        {
            this._FailedGrid = new Grid();
            this._FailedGrid.StoredProcedure = Library.ReportSubscriptionQueue.FailedGridProcedure;
            this._FailedGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._FailedGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._FailedGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._FailedGrid.IdentifierField = "idReportSubscriptionQueue";
            this._FailedGrid.DefaultSortColumn = "dtAction";
            this._FailedGrid.IsDefaultSortDescending = true;

            // data key names
            this._FailedGrid.DataKeyNames = new string[] { "idReportSubscriptionQueue", "fullName", "email", "status" };

            // columns
            GridColumn fullName = new GridColumn(_GlobalResources.To, "fullName", "fullName");
            fullName.AddProperty(new GridColumnProperty("##fullName## (<a href=\"mailto: ##email##\">##email##</a>)"));
            GridColumn notificationName = new GridColumn(_GlobalResources.Report, "reportName", "reportName");
            GridColumn actionDate = new GridColumn(_GlobalResources.AttemptDate, "dtAction", "dtAction");
            GridColumn status = new GridColumn(_GlobalResources.ErrorStatus, "status", "status");

            // add columns to data grid
            this._FailedGrid.AddColumn(fullName);
            this._FailedGrid.AddColumn(notificationName);
            this._FailedGrid.AddColumn(actionDate);
            this._FailedGrid.AddColumn(status);

            //Add grid to update panel
            this._FailedGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._FailedGrid);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Failed Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this._ActionsPanel = new Panel();
            this._ActionsPanel.ID = "ActionsPanel";
            this._ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this._ActionsPanel.Controls.Add(this.DeleteButton);

            // retry button
            this.RetryButton.ID = "GridRetryButton";

            // retry button image
            Image retryImage = new Image();
            retryImage.ID = "GridRetryButtonImage";
            retryImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RESET,
                                                         ImageFiles.EXT_PNG);
            retryImage.CssClass = "MediumIcon";
            retryImage.AlternateText = _GlobalResources.Retry;
            this.RetryButton.Controls.Add(retryImage);

            // retry button text
            Literal retryText = new Literal();
            retryText.Text = _GlobalResources.Retry;
            this.RetryButton.Controls.Add(retryText);

            // add retry button to panel
            this._ActionsPanel.Controls.Add(this.RetryButton);

            this._FailedGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._ActionsPanel);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedQueuedItem_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl bodyWrapper = new HtmlGenericControl("p");
            bodyWrapper.ID = "GridConfirmActionModalBody1";

            Literal body = new Literal();
            body.Text = _GlobalResources.AreYouSureYouWantToDeleteTheSelectedQueuedItem_s;
            bodyWrapper.Controls.Add(body);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(bodyWrapper);

            this._ActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _BuildGridRetryActionsModal
        /// <summary>
        /// Builds the confirmation modal for action retry performed on Grid data.
        /// </summary>
        private void _BuildGridRetryActionsModal()
        {
            this.GridRetryConfirmAction = new ModalPopup("GridRetryConfirmAction");

            // set modal properties
            this.GridRetryConfirmAction.Type = ModalPopupType.Confirm;
            this.GridRetryConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_RESET,
                                                                                ImageFiles.EXT_PNG);
            this.GridRetryConfirmAction.HeaderIconAlt = _GlobalResources.Retry;
            this.GridRetryConfirmAction.HeaderText = _GlobalResources.RetrySelectedReportSubscriptions_s;
            this.GridRetryConfirmAction.TargetControlID = this.RetryButton.ClientID;
            this.GridRetryConfirmAction.SubmitButton.Command += new CommandEventHandler(this._RetryButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridRetryConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToAttemptToSendTheseItem_sAgain;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridRetryConfirmAction.AddControlToBody(body1Wrapper);

            this._ActionsPanel.Controls.Add(this.GridRetryConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._FailedGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._FailedGrid.Rows[i].FindControl(this._FailedGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.ReportSubscriptionQueue.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, _GlobalResources.TheSelectedQueuedItem_sHaveBeenDeletedSuccessfully, false);

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this._FailedGrid.BindData();
            }
        }
        #endregion

        #region _RetryButton_Command
        /// <summary>
        /// Performs the retry action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _RetryButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToRetry = new DataTable();
                recordsToRetry.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._FailedGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._FailedGrid.Rows[i].FindControl(this._FailedGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToRetry.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                // Library.EventEmailQueue.Retry(recordsToRetry);

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, _GlobalResources.TheSelectedItem_sHaveBeenSentSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.FailedFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this._FailedGrid.BindData();
            }
        }
        #endregion
    }
}
