﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using System.IO;
using System.Linq;

namespace Asentia.LMS.Pages.Administrator.System.Account
{
    public class Settings : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel AccountSettingsFormContentWrapperContainer;
        public Panel AccountSettingsWrapperContainer;
        public Panel PropertiesInstructionsPanel;
        public Panel PropertiesContainer;
        public Panel PropertiesActionsPanel;
        #endregion

        #region Private Properties
        private AsentiaSite _SiteObject;
        private TextBox _PortalName;
        private UploaderAsync _Favicon;
        private Image _FaviconImage;
        private HiddenField _ClearFavicon;
        private TextBox _AccountName;
        private LanguageSelector _DefaultLanguage;
        private TimeZoneSelector _DefaultTimezone;
        private TextBox _ChangePassword;
        private TextBox _ConfirmPassword;
        private TextBox _Name;
        private TextBox _Company;
        private TextBox _Email;

        private Button _SaveButton;
        private Button _CancelButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Settings), "Asentia.LMS.Pages.Administrator.System.Account.Settings.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page Load
        private void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_AccountSettings))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/system/account/Settings.css");

            // get the site object
            this._GetSiteObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.AccountSettingsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.AccountSettingsWrapperContainer.CssClass = "FormContentContainer";

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetSiteObject
        /// <summary>
        /// Gets the site object.
        /// </summary>
        private void _GetSiteObject()
        {
            try
            { this._SiteObject = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, true); }
            catch
            { Response.Redirect("~/dashboard"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string settingsImagePath;
            string pageTitle;

            breadCrumbPageTitle = _GlobalResources.AccountSettings;

            settingsImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY,
                                                       ImageFiles.EXT_PNG);

            pageTitle = _GlobalResources.AccountSettings;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, pageTitle, settingsImagePath);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the properties form
            this._BuildPropertiesForm();

            // build the properties form actions panel
            this._BuildPropertiesActionsPanel();

            // populate the form inputs
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _BuildPropertiesForm
        /// <summary>
        /// Builds the properties form.
        /// </summary>
        private void _BuildPropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PropertiesInstructionsPanel, _GlobalResources.ModifyTheAccountPropertiesUsingTheFormBelow, true);

            // clear controls from container
            this.PropertiesContainer.Controls.Clear();

            // portal name field
            this._PortalName = new TextBox();
            this._PortalName.ID = "PortalName_Field";
            this._PortalName.CssClass = "InputLong";

            this.PropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("PortalName",
                                                                      _GlobalResources.PortalName,
                                                                      this._PortalName.ID,
                                                                      this._PortalName,
                                                                      true,
                                                                      true,
                                                                      true));

            // favorite icon field (favicon)

            List<Control> faviconInputControls = new List<Control>();

            if (!String.IsNullOrWhiteSpace(this._SiteObject.Favicon))
            {
                // favicon image panel
                Panel faviconImageContainer = new Panel();
                faviconImageContainer.ID = "Favicon_Field_ImageContainer";
                faviconImageContainer.CssClass = "AvatarImageContainer";

                // favicon image
                this._FaviconImage = new Image();
                faviconImageContainer.Controls.Add(this._FaviconImage);
                faviconInputControls.Add(faviconImageContainer);

                Panel faviconImageDeleteButtonContainer = new Panel();
                faviconImageDeleteButtonContainer.ID = "FaviconImageDeleteButtonContainer";
                faviconImageDeleteButtonContainer.CssClass = "AvatarDeleteButtonContainer";
                faviconImageContainer.Controls.Add(faviconImageDeleteButtonContainer);

                // delete favicon image
                Image deleteFaviconImage = new Image();
                deleteFaviconImage.ID = "FaviconImageDeleteButton";
                deleteFaviconImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                            ImageFiles.EXT_PNG);
                deleteFaviconImage.CssClass = "SmallIcon";
                deleteFaviconImage.Attributes.Add("onClick", "javascript:DeleteFavicon();");
                deleteFaviconImage.Style.Add("cursor", "pointer");

                faviconImageDeleteButtonContainer.Controls.Add(deleteFaviconImage);

                // clear favicon hidden field
                this._ClearFavicon = new HiddenField();
                this._ClearFavicon.ID = "ClearFavicon_Field";
                this._ClearFavicon.Value = "false";
                faviconInputControls.Add(this._ClearFavicon);
            }

            // favicon image upload

            // check favicon folder existence and create if necessary
            if (!Directory.Exists(Server.MapPath(SitePathConstants.UPLOAD_FAVICON)))
            { Directory.CreateDirectory(Server.MapPath(SitePathConstants.UPLOAD_FAVICON)); }

            this._Favicon = new UploaderAsync("Favicon_Field", UploadType.Favicon, "Favicon_ErrorContainer");

            // set params to resize the favicon upon upload
            this._Favicon.ResizeOnUpload = true;
            this._Favicon.ResizeMaxWidth = 100;
            this._Favicon.ResizeMaxHeight = 100;

            faviconInputControls.Add(this._Favicon);

            this.PropertiesContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Favicon",
                                                                                 _GlobalResources.FavoriteIcon,
                                                                                 faviconInputControls,
                                                                                 false,
                                                                                 true));   

            // account name field
            List<Control> accountNameInputControls = new List<Control>();

            Panel accountNameChangeInfoPanel = new Panel();
            accountNameChangeInfoPanel.ID = "AccountNameChangeInfoPanel";
            this.FormatFormInformationPanel(accountNameChangeInfoPanel, _GlobalResources.YourPortalHostnameMayOnlyBeChangedByContactingTheAdministrator);
            accountNameInputControls.Add(accountNameChangeInfoPanel);

            this._AccountName = new TextBox();
            this._AccountName.ID = "AccountName_Field";
            this._AccountName.CssClass = "InputMedium";
            this._AccountName.Enabled = false;
            accountNameInputControls.Add(this._AccountName);            

            Panel portalUrlWrapperContainer = new Panel();
            portalUrlWrapperContainer.ID = "PortalUrlWrapperContainer";

            Panel portalUrlLabelContainer = new Panel();
            portalUrlLabelContainer.ID = "PortalUrlLabelContainer";

            Label portalUrlLabel = new Label();
            portalUrlLabel.Text = _GlobalResources.YourPortalURLIs + ":";

            portalUrlLabelContainer.Controls.Add(portalUrlLabel);
            portalUrlWrapperContainer.Controls.Add(portalUrlLabelContainer);

            Panel portalUrlLinkContainer = new Panel();
            portalUrlLinkContainer.ID = "PortalUrlLinkContainer";

            string portalUrl = String.Empty;

            portalUrl =  "https://" + AsentiaSessionState.GlobalSiteObject.Hostname + "." + Config.AccountSettings.BaseDomain;

            HyperLink portalLink = new HyperLink();
            portalLink.NavigateUrl = portalUrl;
            portalLink.Text = portalUrl;
            portalLink.Target = "_blank";

            portalUrlLinkContainer.Controls.Add(portalLink);
            portalUrlWrapperContainer.Controls.Add(portalUrlLinkContainer);

            accountNameInputControls.Add(portalUrlWrapperContainer);

            this.PropertiesContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("AccountName",
                                                                                 _GlobalResources.PortalHostname,
                                                                                 accountNameInputControls,
                                                                                 false,
                                                                                 true));

            // default language field
            this._DefaultLanguage = new LanguageSelector(LanguageDropDownType.DataOnly);
            this._DefaultLanguage.ID = "DefaultLanguage_Field";

            this.PropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("DefaultLanguage",
                                                                      _GlobalResources.DefaultLanguage,
                                                                      this._DefaultLanguage.ID,
                                                                      this._DefaultLanguage,
                                                                      false,
                                                                      true,
                                                                      false));

            // default timezone field
            this._DefaultTimezone = new TimeZoneSelector();
            this._DefaultTimezone.ID = "DefaultTimezone_Field";

            this.PropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("DefaultTimezone",
                                                                      _GlobalResources.DefaultTimezone,
                                                                      this._DefaultTimezone.ID,
                                                                      this._DefaultTimezone,
                                                                      false,
                                                                      true,
                                                                      false));

            // login field
            Label loginName = new Label();
            loginName.Text = "administrator"; // leave this hardcoded exactly as is, it is not language specific

            this.PropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Login",
                                                                      _GlobalResources.Login,
                                                                      null,
                                                                      loginName,
                                                                      false,
                                                                      false,
                                                                      false));

            // change password field - only the ADMINISTRATOR user can do this
            if (AsentiaSessionState.IdSiteUser == 1)
            {
                List<Control> changePasswordInputControls = new List<Control>();

                this._ChangePassword = new TextBox();
                this._ChangePassword.TextMode = TextBoxMode.Password;
                this._ChangePassword.ID = "ChangePassword_Field";
                this._ChangePassword.CssClass = "InputShort";
                changePasswordInputControls.Add(this._ChangePassword);

                Panel confirmPasswordLabelContainer = new Panel();
                confirmPasswordLabelContainer.ID = "ConfirmPasswordLabelContainer";

                Label confirmPasswordLabel = new Label();
                confirmPasswordLabel.Text = _GlobalResources.ConfirmByEnteringAgain + ":";

                confirmPasswordLabelContainer.Controls.Add(confirmPasswordLabel);
                changePasswordInputControls.Add(confirmPasswordLabelContainer);

                this._ConfirmPassword = new TextBox();
                this._ConfirmPassword.TextMode = TextBoxMode.Password;
                this._ConfirmPassword.ID = "ConfirmPassword_Field";
                this._ConfirmPassword.CssClass = "InputShort";
                changePasswordInputControls.Add(this._ConfirmPassword);

                this.PropertiesContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ChangePassword",
                                                                                              _GlobalResources.Password,
                                                                                              changePasswordInputControls,
                                                                                              false,
                                                                                              true));
            }

            // company field
            this._Company = new TextBox();
            this._Company.ID = "Company_Field";
            this._Company.CssClass = "InputMedium";

            this.PropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Company",
                                                                      _GlobalResources.Company,
                                                                      this._Company.ID,
                                                                      this._Company,
                                                                      true,
                                                                      true,
                                                                      true));

            // contact name field
            this._Name = new TextBox();
            this._Name.ID = "Name_Field";
            this._Name.CssClass = "InputMedium";

            this.PropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Name",
                                                                      _GlobalResources.ContactName,
                                                                      this._Name.ID,
                                                                      this._Name,
                                                                      true,
                                                                      true,
                                                                      false));

            // contact email field
            this._Email = new TextBox();
            this._Email.ID = "Email_Field";
            this._Email.CssClass = "InputLong";

            this.PropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Email",
                                                                      _GlobalResources.Email,
                                                                      this._Email.ID,
                                                                      this._Email,
                                                                      false,
                                                                      true,
                                                                      false));

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _BuildPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for properties actions.
        /// </summary>
        private void _BuildPropertiesActionsPanel()
        {
            // clear controls from container
            this.PropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.PropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this.PropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.PropertiesActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            // LANGUAGE SPECIFIC PROPERTIES

            // name, company
            bool isDefaultPopulated = false;

            foreach (AsentiaSite.LanguageSpecificProperty siteLanguageSpecificProperty in this._SiteObject.LanguageSpecificProperties)
            {
                // if the language is the default language, populate the control directly attached to this page,
                // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                // it; note that if we cannot populate the controls directly attached to this page (default) from
                // language-specific properties, we will use the values in the properties that come from the base table
                if (siteLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    this._PortalName.Text = siteLanguageSpecificProperty.Title;
                    this._Company.Text = siteLanguageSpecificProperty.Company;

                    isDefaultPopulated = true;
                }
                else
                {
                    // get text boxes
                    TextBox languageSpecificPortalNameTextBox = (TextBox)this.PropertiesContainer.FindControl(this._PortalName.ID + "_" + siteLanguageSpecificProperty.LangString);
                    TextBox languageSpecificCompanyTextBox = (TextBox)this.PropertiesContainer.FindControl(this._Company.ID + "_" + siteLanguageSpecificProperty.LangString);

                    // if the text boxes were found, set the text box values to the language-specific value
                    if (languageSpecificPortalNameTextBox != null)
                    { languageSpecificPortalNameTextBox.Text = siteLanguageSpecificProperty.Title; }

                    if (languageSpecificCompanyTextBox != null)
                    { languageSpecificCompanyTextBox.Text = siteLanguageSpecificProperty.Company; }
                }
            }

            if (!isDefaultPopulated)
            {
                this._PortalName.Text = AsentiaSessionState.GlobalSiteObject.Title;
                this._Company.Text = AsentiaSessionState.GlobalSiteObject.Company;
            }

            // NON-LANGUAGE SPECIFIC PROPERTIES

            // account name
            this._AccountName.Text = AsentiaSessionState.GlobalSiteObject.Hostname;

            // favicon image
            if (!String.IsNullOrWhiteSpace(this._SiteObject.Favicon))
            {
                this._FaviconImage.ImageUrl = SitePathConstants.SITE_CONFIG_ROOT + this._SiteObject.Favicon;
            }
 
            // default language
            this._DefaultLanguage.SelectedValue = AsentiaSessionState.GlobalSiteObject.LanguageString;

            // default timezone
            this._DefaultTimezone.SelectedValue = AsentiaSessionState.GlobalSiteObject.IdTimezone.ToString();

            // contact name
            this._Name.Text = AsentiaSessionState.GlobalSiteObject.ContactName;

            // contact email
            this._Email.Text = AsentiaSessionState.GlobalSiteObject.ContactEmail;
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;

            // PORTAL NAME - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._PortalName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "PortalName", _GlobalResources.PortalName + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // COMPANY - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._Company.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "Company", _GlobalResources.Company + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // CONTACT NAME - REQUIRED
            if (String.IsNullOrWhiteSpace(this._Name.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "Name", _GlobalResources.ContactName + " " + _GlobalResources.IsRequired);
            }

            // CONTACT EMAIL - IF SPECIFIED, MUST BE VALID
            if (!String.IsNullOrWhiteSpace(this._Email.Text) && !RegExValidation.ValidateEmailAddress(this._Email.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "Email", _GlobalResources.Email + " " + _GlobalResources.IsInvalid);
            }

            // CHANGE PASSWORD -- IF CHANGED, PASSWORD AND CONFIRMATION MUST MATCH
            if (AsentiaSessionState.IdSiteUser == 1)
            {
                string newPassword = this._ChangePassword.Text.Trim();
                string confirmPassword = this._ConfirmPassword.Text.Trim();

                if (!String.IsNullOrWhiteSpace(newPassword))
                {
                    if (newPassword != confirmPassword)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.PropertiesContainer, "ChangePassword", _GlobalResources.Password + " " + _GlobalResources.MustMatchConfirmationField);
                    }
                }
            }

            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                // DO NOT check for existence of a site object, it is required to exist on this page

                int id;

                // populate the object
                this._SiteObject.Title = this._PortalName.Text;
                this._SiteObject.Company = this._Company.Text;
                this._SiteObject.ContactName = this._Name.Text;
                this._SiteObject.ContactEmail = this._Email.Text;
                this._SiteObject.LanguageString = this._DefaultLanguage.SelectedValue;
                this._SiteObject.IdTimezone = Convert.ToInt32(this._DefaultTimezone.SelectedValue);

                if (AsentiaSessionState.IdSiteUser == 1)
                {
                    string password = this._ChangePassword.Text.Trim();

                    if (!String.IsNullOrWhiteSpace(password))
                    { this._SiteObject.Password = password; }
                }

                // do favicon
                string faviconFilename = "";

                if (this._SiteObject.Id > 0)
                {
                    if (this._Favicon.SavedFilePath != null)
                    {
                        // check user folder existence and create if necessary
                        if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT)))
                        { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT)); }

                        string fullSavedFilePath = null;

                        if (File.Exists(Server.MapPath(this._Favicon.SavedFilePath)))
                        {
                            faviconFilename = "favicon" + Path.GetExtension(this._Favicon.SavedFilePath);

                            fullSavedFilePath = SitePathConstants.SITE_CONFIG_ROOT + faviconFilename;

                            // delete existing favicon images if any
                            if (this._SiteObject.Favicon != null)
                            {
                                // find favicon images with "favicon" in the filename
                                string faviconImageFilename = @"favicon*" + Path.GetExtension(this._SiteObject.Favicon);
                                string[] faviconImageList = Directory.GetFiles(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT), faviconImageFilename).Select(file => Path.GetFileName(file)).ToArray();

                                foreach (string file in faviconImageList)
                                {
                                    File.Delete(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + file));
                                }

                            }

                            // move the uploaded file into the config folder
                            File.Copy(Server.MapPath(this._Favicon.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._Favicon.SavedFilePath));
                            this._SiteObject.Favicon = faviconFilename;
                        }

                        else
                        {
                            this._SiteObject.Favicon = null;
                        }
                    }

                    else if (this._ClearFavicon != null)
                    {
                        if (this._ClearFavicon.Value == "true")
                        {
                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + "favicon" + Path.GetExtension(this._SiteObject.Favicon))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + "favicon" + Path.GetExtension(this._SiteObject.Favicon))); }

                            this._SiteObject.Favicon = null;
                        }
                    }
                    else
                    { }
                }

                // do site language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string portalName = string.Empty;
                        string company = string.Empty;

                        // get text boxes
                        TextBox languageSpecificPortalNameTextBox = (TextBox)this.PropertiesContainer.FindControl(this._PortalName.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificCompanyTextBox = (TextBox)this.PropertiesContainer.FindControl(this._Company.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificPortalNameTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificPortalNameTextBox.Text))
                            { portalName = languageSpecificPortalNameTextBox.Text; }
                        }

                        if (languageSpecificCompanyTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificCompanyTextBox.Text))
                            { company = languageSpecificCompanyTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(portalName) ||
                            !String.IsNullOrWhiteSpace(company))
                        {
                            this._SiteObject.SavePropertiesInLanguage(AsentiaSessionState.IdSite,
                                                                      AsentiaSessionState.UserCulture,
                                                                      AsentiaSessionState.IdSiteUser,
                                                                      cultureInfo.Name,
                                                                      portalName,
                                                                      company);
                        }
                    }
                }

                // save the site object
                this._SiteObject.Save(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser);

                // load the site object
                this._SiteObject = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, true);

                // reload global site object
                AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedback(_GlobalResources.AccountSettingsHaveBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/dashboard");
        }
        #endregion
    }
}
