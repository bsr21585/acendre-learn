﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.ActivityImport
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ActivityImportFormContentWrapperContainer;
        public Panel ObjectOptionsPanel;
        public Panel UserObjectMenuContainer;
        public Panel ActivityImportPageWrapperContainer;
        public Panel PanelMsgTable = new Panel();
        public Panel TabContainer, TabContentWrapperPanel;
        public UpdatePanel ActivityUpdatePanel, ActivityGridUpdatePanel;
        public Panel ErrorTablePanel, ManageActivityFeedbackContainer;
        public Panel ManagementInstructionPanel;
        
        public Grid ActivityGrid;
        public LinkButton DeleteButton = new LinkButton();
        public Panel ActionsPanel;
        public ModalPopup GridConfirmAction = new ModalPopup();

        /// <summary>
        /// Collection of columns for activity import table
        /// These are the system imposed constraints on each field.
        /// </summary>
        public List<Activity> ActivityImportColumns;
        #endregion 

        #region Private Properties
        private User _UserObject;

        private int _ErrorNum = 0;
        private int _RowNum = 0;
        private const int _MAX_RECORDS_FOR_IMPORT = 10000;
        private string _ErrorList = String.Empty;
        private string _UsernameImported = String.Empty;

        private Panel _UploadTotalPanel = new Panel();
        private Panel _UploadDetailedInstruction = new Panel();
        private LinkButton _UploadActivityButton;

        private Table _ActivityColumnsInfoTable = new Table();
        private Table _ActivityImportTable = new Table();
     
        private Button _PreviousErrorButton = new Button();
        private Button _NextErrorButton = new Button();

        private TableHeaderRow _ActivityDataTableHeader = new TableHeaderRow();
        private List<string> _UsernameNotExistList = new List<string>();
        private List<string> _CourseCodeNotExistList = new List<string>();
        private UploaderAsync _ActivityUploader;
        private ModalPopup _ActivityFileModal;
     
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            StringBuilder globalJs = new StringBuilder();
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.ActivityImport.Default.js");
            globalJs.AppendLine("     Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler); ");
            globalJs.AppendLine("         function EndRequestHandler(sender, args){ ");
            globalJs.AppendLine("         if (args.get_error() != undefined){ ");
            globalJs.AppendLine("          var url = window.location.href; ");
            globalJs.AppendLine("           if (url.indexOf('?') > -1){  ");
            globalJs.AppendLine("              url += '&timeout=1'  ");
            globalJs.AppendLine("              }else{  ");
            globalJs.AppendLine("               url += '?timeout=1'  ");
            globalJs.AppendLine("              }  ");
            globalJs.AppendLine("           window.location.href = url;  ");
            globalJs.AppendLine("             } ");
            globalJs.AppendLine("         } ");

            // add start up script
            csm.RegisterStartupScript(typeof(Default), "GlobalJs", globalJs.ToString(), true);

        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // set timeout for the activity import
            Server.ScriptTimeout = 600;

            // get the usernameImported if there is an assigned userID
            this._UsernameImported = _GetUserNameImported();

            // check permissions
            if (
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_ActivityImport)
                && (!String.IsNullOrWhiteSpace(this._UsernameImported) && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
                )
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/activityimport/Default.css");            

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ActivityImport));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, _GlobalResources.ActivityImport, ImageFiles.GetIconPath(ImageFiles.ICON_IMPORTACTIVITYDATA, ImageFiles.EXT_PNG));

            // build the object options panel
            this._BuildObjectOptionsPanel();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.ActivityImportFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ActivityImportPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu container
            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.ImportActivityData;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // load activity table column info
            this.ActivityImportColumns = Activity.GetActivityColumnsInfo();

            // load tab content
            if (String.IsNullOrWhiteSpace(this._UsernameImported))
            { this._BuildTabsList(); }

            // load the page controls
            this._LoadPageControls();   
            
            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.ActivityGrid.BindData();
            }
        }
        #endregion

        #region _LoadPageControls
        /// <summary>
        /// Setup the Tags/Controls on the page
        /// </summary>
        private void _LoadPageControls()
        {
            this.TabContentWrapperPanel.ID = this.ID + "_" + "TabContentWrapperPanel";
            this.TabContentWrapperPanel.CssClass = "TabPanelsContentContainer";
            this._BuildActivityTableColumn();

            // build the table
            this._BuildActivityContent();
            this._BuildActivityFileUploadModal();
        }
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for the form.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Upload", _GlobalResources.UploadActivityData));
            tabs.Enqueue(new KeyValuePair<string, string>("Manage", _GlobalResources.ManageActivityData));

            this.TabContainer.Controls.Add(AsentiaPage.BuildTabListPanel("ActivityData", tabs));
        }
        #endregion

        #region _GetUserImported
        /// <summary>
        /// Gets Object User Imported
        /// </summary>
        private string _GetUserNameImported()
        {
            string usernameImported = String.Empty;
            User userImported;

            // get the id querystring parameter
            int qsId = this.QueryStringInt("uid", 0);
            int vsId = this.ViewStateInt(this.ViewState, "uid", 0);
            int id = 0;

            if (qsId > 0 || vsId > 0)
            {
                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }
            }
            if (id > 0)
            {
                this._UserObject = new User(id);
                userImported = new User(id);
                usernameImported = userImported.Username;
            }

            return usernameImported;
        }
        #endregion

        #region _BuildActivityContent
        /// <summary>
        /// Builds the tab content container for specific purpose of each tab
        /// </summary>
        private void _BuildActivityContent()
        {
            this.ActivityUpdatePanel.ID = "ActivityData_" + "Upload" + "_TabPanel";
            this.ActivityUpdatePanel.Attributes.Add("style", "display: block;");
            _BuildUploadTabContent(this.ActivityUpdatePanel.ContentTemplateContainer);

            this.ActivityGridUpdatePanel.ID = "ActivityData_" + "Manage" + "_TabPanel";
            this.ActivityGridUpdatePanel.Attributes.Add("style", "display: none;");
            _BuildManageTabContent();
        }
        #endregion
                
        #region _BuildUploadTabContent
        /// <summary>
        /// Builds the tab content container for Upload tab
        /// </summary>
        private void _BuildUploadTabContent(Control UploadControls)
        {
            _BuildUploadPanel(UploadControls);
            _BuildActivityColumnsTable();
        }
        #endregion

        #region _BuildManageTabContent
        /// <summary>
        /// Builds the tab content container for Manage tab
        /// </summary>
        private void _BuildManageTabContent()
        {
            this.FormatPageInformationPanel(ManagementInstructionPanel, _GlobalResources.AllActivityDataImportsAreListedInTheTableBelow, true);

            this.ActivityGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the Manage Activity Tab.
        /// </summary>
        private void _BuildGrid()
        {
            this.ActivityGrid.StoredProcedure = Library.Activity.GridProcedure;
            this.ActivityGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.ActivityGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.ActivityGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.ActivityGrid.IdentifierField = "idActivityImport";
            this.ActivityGrid.DefaultSortColumn = "importFileName";
            this.ActivityGrid.ShowTimeInDateStrings = true;
            this.ActivityGrid.ShowSearchBox = false;
            this.ActivityGrid.ShowRecordsPerPageSelectbox = false;
            this.ActivityGrid.PagerSettings.Position = PagerPosition.Bottom;
            this.ActivityGrid.ID = "ActivityGrid";

            // data key names
            this.ActivityGrid.DataKeyNames = new string[] { "idActivityImport", "importFileName" };

            // columns
            GridColumn fileName = new GridColumn(_GlobalResources.ImportFileName, "importFileName", "importFileName");
            GridColumn importTime = new GridColumn(_GlobalResources.ImportDateTime, "timestamp", "timestamp");
            GridColumn performedBy = new GridColumn(_GlobalResources.ImportPerformedBy, "performedBy", "performedBy");
            GridColumn performedFor = new GridColumn(_GlobalResources.ImportPerformedFor, "performedFor", "performedFor");  

            // add columns to data grid
            this.ActivityGrid.AddColumn(fileName);
            this.ActivityGrid.AddColumn(importTime);
            this.ActivityGrid.AddColumn(performedBy);
            this.ActivityGrid.AddColumn(performedFor);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedActivityImport_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this.DeleteButton);
        }
        #endregion

        #region _BuildActivityColumnsTable
        /// <summary>
        /// Builds the ActivityColumnsTable for the page.
        /// </summary>
        private void _BuildActivityColumnsTable()
        {
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.CssClass = "GridHeaderRow";
            TableHeaderCell columnOrder = new TableHeaderCell();
            TableHeaderCell dataHeader = new TableHeaderCell();
            TableHeaderCell requiredHeader = new TableHeaderCell();
            TableHeaderCell typeHeader = new TableHeaderCell();
            TableHeaderCell lengthHeader = new TableHeaderCell();
            TableHeaderCell formatHeader = new TableHeaderCell();

            int columnNum = 0;
            columnOrder.Text = _GlobalResources.Column;
            dataHeader.Text = _GlobalResources.Data;
            requiredHeader.Text = _GlobalResources.Required;
            typeHeader.Text = _GlobalResources.Type;
            lengthHeader.Text = _GlobalResources.Length;
            formatHeader.Text = _GlobalResources.Format;

            headerRow.Controls.Add(columnOrder);
            headerRow.Controls.Add(dataHeader);
            headerRow.Controls.Add(requiredHeader);
            headerRow.Controls.Add(typeHeader);
            headerRow.Controls.Add(lengthHeader);
            headerRow.Controls.Add(formatHeader);

            _ActivityColumnsInfoTable.CssClass = "GridTable";
            _ActivityColumnsInfoTable.Controls.Add(headerRow);

            foreach (Activity activityColumn in ActivityImportColumns)
            {
                columnNum++;
                string reference = String.Empty;
                string creditnote = String.Empty;
                switch (columnNum)
                {
                    case 2:
                        reference = "<sup>(1)</sup>";
                        break;
                    case 4:
                        creditnote = "<sup>(3)</sup>";
                        break;
                    case 6:
                    case 7:
                    case 8:
                    case 10:
                        reference = "<sup>(2)</sup>";
                        break;
                }
                
                string columnOrderText = "Column " + ((columnNum > 26) ? ((Char)(64 + (columnNum - 1) / 26)).ToString() : String.Empty) + ((Char)(64 + columnNum % 26)).ToString();
                TableRow activityColumnTableRow = new TableRow();
                activityColumnTableRow.CssClass = ((columnNum % 2) == 0) ? "GridDataRow GridDataRowAlternate" : "GridDataRow";

                // Column Order
                TableCell columnTableCell = new TableCell();
                columnTableCell.Text = columnOrderText.Replace('@', 'Z');
                activityColumnTableRow.Controls.Add(columnTableCell);

                // Column Identifier
                TableCell labelTableCell = new TableCell();
                labelTableCell.Text = activityColumn.ColumnName + creditnote;
                labelTableCell.CssClass = "ColumnDataName";
                activityColumnTableRow.Controls.Add(labelTableCell);

                // Column Always Required
                TableCell requiredTableCell = new TableCell();
                requiredTableCell.Text = ((activityColumn.AlwaysRequired) ? _GlobalResources.Yes : _GlobalResources.No) + reference;
                activityColumnTableRow.Controls.Add(requiredTableCell);

                if (activityColumn.Identifier != _GlobalResources.ROWEND_UPPER)
                {
                    // Field Type
                    TableCell typeTableCell = new TableCell();

                    if (activityColumn.DataType == typeof(string))
                    { typeTableCell.Text = _GlobalResources.Text; }
                    else if (activityColumn.DataType == typeof(DateTime))
                    { typeTableCell.Text = _GlobalResources.DateTime; }
                    else if (activityColumn.DataType == typeof(bool))
                    { typeTableCell.Text = _GlobalResources.Boolean; }
                    else if (activityColumn.DataType == typeof(int))
                    { typeTableCell.Text = _GlobalResources.Integer; }
                    else if (activityColumn.DataType == typeof(double))
                    { typeTableCell.Text = _GlobalResources.Number; }
                    else
                    { typeTableCell.Text = _GlobalResources.Text; }

                    activityColumnTableRow.Controls.Add(typeTableCell);

                    // Field Length
                    TableCell lengthTableCell = new TableCell();
                    lengthTableCell.Text = (activityColumn.DataType == typeof(string)) ? activityColumn.MaxLength.ToString() : " -";
                    activityColumnTableRow.Controls.Add(lengthTableCell);

                    // Field Format
                    TableCell formatTableCell = new TableCell();
                    formatTableCell.Text = activityColumn.Format;
                    activityColumnTableRow.Controls.Add(formatTableCell);
                }
                else
                {
                    // Field Format specified for Row End
                    TableCell formatTableCell = new TableCell();
                    formatTableCell.Text = activityColumn.Format;
                    formatTableCell.ColumnSpan = 3;
                    activityColumnTableRow.Controls.Add(formatTableCell);
                }
                this._ActivityColumnsInfoTable.Controls.Add(activityColumnTableRow);

            }
            this._UploadTotalPanel.Controls.Add(this._ActivityColumnsInfoTable);

                HtmlGenericControl ol = new HtmlGenericControl("ol");
                ol.Attributes.Add("type", "i");
                HtmlGenericControl li1 = new HtmlGenericControl("li");
                li1.InnerHtml = "<sup>(1)</sup>&nbsp;" + _GlobalResources.IfACourseCodeIsEnteredTheImportWillTryToMatch;
                ol.Controls.Add(li1);

                HtmlGenericControl li2 = new HtmlGenericControl("li");
                li2.InnerHtml = "<sup>(2)</sup>&nbsp;" + _GlobalResources.IfANYColumnFGHIOrJContainDataThenALLColumnsFGHAndJMustContainData;
                ol.Controls.Add(li2);

                HtmlGenericControl li3 = new HtmlGenericControl("li");
                li3.InnerHtml = "<sup>(3)</sup>&nbsp;" + _GlobalResources.IfCourseCreditsAreEntered;
                ol.Controls.Add(li3);

                this._UploadTotalPanel.Controls.Add(ol);
                this.ActivityUpdatePanel.ContentTemplateContainer.Controls.Add(this._UploadTotalPanel);
        }
        #endregion

        #region _BuildActivityTableColumn
        /// <summary>
        /// Builds datatable columns for 
        /// </summary>
        private void _BuildActivityTableColumn()
        {
           
            int columnNum = 0;

            TableHeaderCell cellColumnRowHeader = new TableHeaderCell();
            _ActivityDataTableHeader.Controls.Add(cellColumnRowHeader);

            foreach (Activity activityColumn in this.ActivityImportColumns)
            {
                    columnNum++;
                    string columnOrderText = ((columnNum > 26) ? ((Char)(64 + (columnNum - 1) / 26)).ToString() : String.Empty) + ((Char)(64 + columnNum % 26)).ToString();

                    //Create header row for the _ActivityDataTableHeader
                    TableHeaderCell cellColumnHeader = new TableHeaderCell();
                    cellColumnHeader.Text = columnOrderText.Replace('@', 'Z'); ;
                    _ActivityDataTableHeader.Controls.Add(cellColumnHeader);
            }

             _ActivityImportTable.CssClass = "ExcelTableFormat";
             _ActivityImportTable.Controls.Add(_ActivityDataTableHeader);
        }

        #endregion

        #region _BuildUploadPanel
        /// <summary>
        /// Builds upload icon for the activity batch file
        /// </summary>
        private void _BuildUploadPanel(Control uploadControls)
        {
            Panel activityFileUploadInstructionsPanel = new Panel();
            this.FormatPageInformationPanel(activityFileUploadInstructionsPanel, _GlobalResources.UseTheTableBelowAsReferenceForCreatingTabDelimitedActivityFile, true);
            uploadControls.Controls.Add(activityFileUploadInstructionsPanel);

            #region Detail Instruction Panel

            //Detail Instruction Panel
            _UploadDetailedInstruction.ID = "ActivityImportExpandableParagraphContainer";
            _UploadDetailedInstruction.CssClass = "ExpandableParagraphContainer";

            Panel sectionTitlePanel1 = new Panel();
            HtmlGenericControl sectionTitle1 = new HtmlGenericControl("h2");
            sectionTitle1.InnerText = _GlobalResources.Important + ":";
            sectionTitlePanel1.Controls.Add(sectionTitle1);
            _UploadDetailedInstruction.Controls.Add(sectionTitlePanel1);

            Panel sectionDetail1 = new Panel();
            this.FormatFormInformationPanel(sectionDetail1, _GlobalResources.UsernameColumnCommentForActivityImport, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            Panel sectionDetail2 = new Panel();
            this.FormatFormInformationPanel(sectionDetail2, _GlobalResources.YourDataWillBeValidatedAgainstAllTheDataTypes, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            Panel sectionDetail3 = new Panel();
            this.FormatFormInformationPanel(sectionDetail3, _GlobalResources.AMaximumOf10000RecordsActivity, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            _UploadDetailedInstruction.Controls.Add(sectionDetail1);
            _UploadDetailedInstruction.Controls.Add(sectionDetail2);
            _UploadDetailedInstruction.Controls.Add(sectionDetail3);

            Panel sectionTitlePanel2 = new Panel();
            HtmlGenericControl sectionTitle2 = new HtmlGenericControl("h2");
            sectionTitle2.InnerText = _GlobalResources.ColumnDetails + ":";
            sectionTitlePanel2.Controls.Add(sectionTitle2);
            _UploadDetailedInstruction.Controls.Add(sectionTitlePanel2);

            Panel sectionDetailPanel2 = new Panel();
            HtmlGenericControl ul2 = new HtmlGenericControl("ul");
            HtmlGenericControl li12 = new HtmlGenericControl("li");
            li12.InnerHtml = "<span class=\"bold\">" + _GlobalResources.Required + "</span> - " + _GlobalResources.YESIndicatesThatAllRowsMustContainData;
            ul2.Controls.Add(li12);

            HtmlGenericControl li22 = new HtmlGenericControl("li");
            li22.InnerHtml = "<span class=\"bold\">" + _GlobalResources.Length + "</span> - " + _GlobalResources.TheMaximumNumberOfCharacters;
            ul2.Controls.Add(li22);

            HtmlGenericControl li33 = new HtmlGenericControl("li");
            li33.InnerHtml = "<span class=\"bold\">" + _GlobalResources.Format + "</span> - " + _GlobalResources.DisplaysFormattingInformationForTheColumn;
            ul2.Controls.Add(li33);
            sectionDetailPanel2.Controls.Add(ul2);
            _UploadDetailedInstruction.Controls.Add(sectionDetailPanel2);

            Panel sectionTitlePanel4 = new Panel();
            HtmlGenericControl sectionTitle4 = new HtmlGenericControl("h2");
            sectionTitle4.InnerText = _GlobalResources.ImportantFormattingNotes + ":";
            sectionTitlePanel4.Controls.Add(sectionTitle4);
            _UploadDetailedInstruction.Controls.Add(sectionTitlePanel4);

            Panel sectionDetail4 = new Panel();
            this.FormatFormInformationPanel(sectionDetail4, _GlobalResources.NoteTheFollowingBeforeCreatingYourDataFile, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            HtmlGenericControl ul = new HtmlGenericControl("ul");
            HtmlGenericControl li1 = new HtmlGenericControl("li");
            li1.InnerHtml = _GlobalResources.UploadedFileMustBeATabDelimitedTextFileWithExtensionTXT;
            ul.Controls.Add(li1);

            HtmlGenericControl li2 = new HtmlGenericControl("li");
            li2.InnerHtml = _GlobalResources.IncludeAllDataColumns;
            ul.Controls.Add(li2);

            HtmlGenericControl li3 = new HtmlGenericControl("li");
            li3.InnerHtml = _GlobalResources.AllRequiredFieldsMust;
            ul.Controls.Add(li3);

            HtmlGenericControl li4 = new HtmlGenericControl("li");
            li4.InnerHtml = _GlobalResources.BeSureThatNoExtraEmptyLines;
            ul.Controls.Add(li4);

            HtmlGenericControl li5 = new HtmlGenericControl("li");
            li5.InnerHtml = _GlobalResources.DoNotIncludeColumnHeaders;
            ul.Controls.Add(li5);

            HtmlGenericControl li6 = new HtmlGenericControl("li");
            li6.InnerHtml = _GlobalResources.DoNotEncloseCellValuesInQuotations;
            ul.Controls.Add(li6);

            HtmlGenericControl li7 = new HtmlGenericControl("li");
            li7.InnerHtml = _GlobalResources.DoNotIncludeTabsInCellValues;
            ul.Controls.Add(li7);

            sectionDetail4.Controls.Add(ul);
            _UploadDetailedInstruction.Controls.Add(sectionDetail4);

            uploadControls.Controls.Add(_UploadDetailedInstruction);

            #endregion
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            //Upload Image Panel
            ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";
            if (Session["Imported"] != null)
            {
                if (Session["Imported"].ToString() == "Success")
                    this.DisplayFeedback(_GlobalResources.ActivityDataHasBeenImportedSuccessfully, false);
                else if (Session["Imported"].ToString() == "NotMatched")
                    this.DisplayFeedback(_GlobalResources.TheNumberOfColumnsDoesNotMatchPleaseTryAgain, true);
                else
                    this.DisplayFeedback(Session["Imported"].ToString(), true);
                Session.Remove("Imported");
            }
            // UPLOAD ACTIVITY IMPORT
            this._UploadActivityButton = new LinkButton();
            this._UploadActivityButton.ID = "ActivityUploadButton";

            ObjectOptionsPanel.Controls.Add(
                this.BuildOptionsPanelImageLink("UploadActivityImportLink",
                                                this._UploadActivityButton,
                                                null,
                                                "$('#PageFeedbackContainer').hide();",
                                                _GlobalResources.UploadActivityDataFile,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG)) // ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG)
                );
            string qsTimeOut = this.QueryStringString("timeout", String.Empty);

            if (!String.IsNullOrWhiteSpace(qsTimeOut))
                this.DisplayFeedback(_GlobalResources.ThePageHasReachedItsExecutionTimeoutLimitBecauseTheFileYouAreUploadingContainsTooManyRecords, true);
        }
        #endregion

        #region _BuildActivityFileUploadModal
        /// <summary>
        /// Pop up window for activity to upload batch file
        /// </summary>
        private void _BuildActivityFileUploadModal()
        {
            this._ActivityFileModal = new ModalPopup("ActivityFileModal");
            this._ActivityFileModal.ID = "ActivityFileModal";
            this._ActivityFileModal.Type = ModalPopupType.Form;
            this._ActivityFileModal.HeaderText = _GlobalResources.UploadActivityDataFile;
            this._ActivityFileModal.TargetControlID = this._UploadActivityButton.ID;

            this._ActivityFileModal.SubmitButtonCustomText = _GlobalResources.Submit;
            this._ActivityFileModal.SubmitButton.ID = "ActivityFileModalSubmitButton";
            this._ActivityFileModal.SubmitButton.OnClientClick = "javascript:  document.getElementById('ActivityFileModalModalPopupHeader').innerHTML = '" + _GlobalResources.ProcessingActivityDataFile + "';";
            this._ActivityFileModal.SubmitButton.Command += new CommandEventHandler(this._ActivityDataSubmitButton_Command);

            // build uploader controls
            List<Control> activityFileInputControls = new List<Control>();

            this._ActivityUploader = new UploaderAsync("ActivityUploader", UploadType.UserActivityData, "ActivityUploader_ErrorContainer");

            // add controls to body
            activityFileInputControls.Add(this._ActivityUploader);

            this._ActivityFileModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("ActivityUploader",
                                                                             _GlobalResources.File,
                                                                             activityFileInputControls,
                                                                             false,
                                                                             true));

            this.PageContentContainer.Controls.Add(this._ActivityFileModal);
        }
        #endregion

        #region _ActivityDataSubmitButton_Command
        /// <summary>
        /// Handles the event initiated by the Submit Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _ActivityDataSubmitButton_Command(object sender, CommandEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(_ActivityUploader.SavedFilePath) && File.Exists(Server.MapPath(_ActivityUploader.SavedFilePath)))
            {
                string count = String.Empty;
                bool columnMatched = true;

                List<string> lines = File.ReadAllLines(Server.MapPath(_ActivityUploader.SavedFilePath), Encoding.Default).ToList();

                if (lines.Count > _MAX_RECORDS_FOR_IMPORT)
                {
                    this.DisplayFeedback(_GlobalResources.YourActivityDataImportFileContainsXrecordsWhichExceedsTheaximumNumberOfLRecordsAllowedPleaseBreakYourActivityDataImportIntoMultipleFiles.Replace("##X##", lines.Count.ToString()).Replace("##L##", _MAX_RECORDS_FOR_IMPORT.ToString()), true);
                }
                else
                {
                    try
                    {
                        PanelMsgTable.ID = "PanelErrorMsgTable";
                        PanelMsgTable.CssClass = "ExcelTableWrapper";
                        PanelMsgTable.ScrollBars = ScrollBars.Auto;

                        DataTable activityDataTable = new DataTable();
                        DataTable usernameDT = new DataTable();
                        usernameDT.Columns.Add("username", typeof(string));
                        DataTable coursecodeDT = new DataTable();
                        coursecodeDT.Columns.Add("coursecode", typeof(string));

                        foreach (string line in lines)
                        {
                            DataRow usernameRow = usernameDT.NewRow();
                            usernameRow[0] = line.Replace(Environment.NewLine, "").Split('\t')[0];
                            usernameDT.Rows.Add(usernameRow);
                        }

                        foreach (string line in lines)
                        {
                            DataRow coursecodeRow = coursecodeDT.NewRow();
                            coursecodeRow[0] = line.Replace(Environment.NewLine, "").Split('\t')[1];
                            coursecodeDT.Rows.Add(coursecodeRow);
                        }

                        _UsernameNotExistList = Asentia.UMS.Library.User.UserListNotExistForCurrentSite(usernameDT).AsEnumerable().Select(u => u.Field<string>("username")).ToList();

                        _CourseCodeNotExistList = Asentia.LMS.Library.Course.CourseCodeListNotExistForCurrentSite(coursecodeDT).AsEnumerable().Select(courseCode => courseCode.Field<string>("courseCode")).ToList();

                        //build datatable columns
                        foreach (Activity activityColumn in ActivityImportColumns)
                        {
                            if (activityColumn.DataType == null)
                            { activityColumn.DataType = typeof(string); }

                            DataColumn dcActivity;
                            dcActivity = new DataColumn(activityColumn.ColumnName, activityColumn.DataType);
                            activityDataTable.Columns.Add(dcActivity);
                        }

                        int lineNum = 0;
                        bool lineBreakFlag = false;
                        bool totalLineBreakFlag = false;
                        string lineBreakError = string.Empty;

                        //read file line by line for line break checking
                        foreach (string line in lines)
                        {
                            lineNum++;
                            string firstItemWithValue = string.Empty;

                            string[] docColumns = line.Replace(Environment.NewLine, "").Split('\t');

                            for (int i = 0; i < docColumns.Length; i++)
                            {
                                if (!String.IsNullOrEmpty(docColumns[i].Trim()) && docColumns[i].Trim() != "\"")
                                {
                                    firstItemWithValue = docColumns[i];
                                    break;
                                }
                            }

                            if (lineBreakFlag)
                            {
                                lineBreakError += "\"" + firstItemWithValue + "\"";
                                lineBreakFlag = false;
                            }
                            else if (docColumns.Length != activityDataTable.Columns.Count)
                            {
                                if (lineNum == 1)
                                {
                                    totalLineBreakFlag = true;
                                    columnMatched = false;
                                    Session["Imported"] = _GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain;
                                    Response.Redirect(Request.RawUrl);
                                }

                                if (docColumns[docColumns.Length - 1].ToUpper() != _GlobalResources.END_UPPER)
                                {
                                    lineBreakError += "<br>" + _GlobalResources.AtLine + " " + lineNum.ToString() + ":" + _GlobalResources.ThereAreLineBreakOrNewLineCharactersInYourDataBetween + " \"" + (string.IsNullOrEmpty(docColumns[docColumns.Length - 1]) ? docColumns[docColumns.Length - 2] : docColumns[docColumns.Length - 1]) + "\" " + _GlobalResources.and_lower + " ";
                                    lineBreakFlag = true;
                                    totalLineBreakFlag = true;
                                }
                                else
                                {
                                    lineBreakError += "<br>" + _GlobalResources.AtLine + lineNum.ToString() + ": " + _GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(lineBreakError))
                        {
                            if (totalLineBreakFlag)
                            {                                
                                Session["Imported"] = _GlobalResources.ThereAreErrorsInTheFollowingPlaces + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain + ":" + lineBreakError;
                            }
                            else
                            {
                                Session["Imported"] = _GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain;
                            }
                            Response.Redirect(Request.RawUrl);
                        }

                        //read file line by line
                        foreach (string line in lines)
                        {
                            bool differentCompleteDate = false;
                            bool sameLessonName = false;
                            bool lessonTimeStampLaterThanCourseCompletion = false;
                            string[] columns = line.Replace(Environment.NewLine, "").Split('\t');

                            try
                            {
                                DataRow activityRow = activityDataTable.NewRow();
                                //#datatype conversion
                                for (int i = 0; i < columns.Length; i++)
                                {
                                    if (String.IsNullOrWhiteSpace(columns[i]) || columns[i].ToLower() == "null")
                                        activityRow[i] = DBNull.Value;
                                    else if (i == 8) // convert the scores to scaled scores
                                        activityRow[i] = Convert.ToDouble(columns[i]) / 100;
                                    else if (activityDataTable.Columns[i].DataType == typeof(DateTime))
                                        activityRow[i] = Convert.ToDateTime(columns[i]);
                                    else
                                        activityRow[i] = columns[i];
                                }

                                activityDataTable.Rows.Add(activityRow);

                                string selectString = string.Format("([{0}]='{1}')", activityDataTable.Columns[0].ColumnName, columns[0]);

                                if (!String.IsNullOrWhiteSpace(columns[1]))
                                {
                                    //make sure only one course code/completion combo exists for each user
                                    selectString += string.Format(" AND ([{0}]='{1}')", activityDataTable.Columns[1].ColumnName, columns[1]);
                                }
                                else if (!String.IsNullOrWhiteSpace(columns[2]))
                                {
                                    //make sure only one course name/completion combo exists for each user
                                    selectString += string.Format(" AND ([{0}]='{1}')", activityDataTable.Columns[2].ColumnName, columns[2]);
                                }

                                if (!String.IsNullOrWhiteSpace(columns[5]))
                                {
                                    string selectString5 = selectString + string.Format(" AND ([{0}]='{1}')", activityDataTable.Columns[5].ColumnName, columns[5]);
                                    sameLessonName = (activityDataTable.Select(selectString5).Count() > 1) ? true : false;
                                }

                                string selectString4 = selectString + string.Format(" AND ([{0}]='{1}')", activityDataTable.Columns[4].ColumnName, columns[4]);
                                differentCompleteDate = (activityDataTable.Select(selectString4).Count() < activityDataTable.Select(selectString).Count()) ? true : false;

                                if (!String.IsNullOrWhiteSpace(columns[9]))
                                {
                                    DateTime courseCompletion = DateTime.Parse(columns[4]);
                                    DateTime lessonTimestamp = DateTime.Parse(columns[9]);

                                    lessonTimeStampLaterThanCourseCompletion = (lessonTimestamp > courseCompletion) ? true : false;
                                }

                            }
                            catch (AsentiaException ex)
                            {
                                // display the failure message
                                this.DisplayFeedback(ex.Message, true);
                            }
                            catch (Exception)
                            { ;}

                            this._ColumnValueValidation(columns, differentCompleteDate, sameLessonName, lessonTimeStampLaterThanCourseCompletion);

                            PanelMsgTable.Controls.Add(_ActivityImportTable);
                        }

                        if (_ErrorNum > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeExpandableParagraphs", "Helper.ExpandableParagraphContainer();$('#Upload_TabLink').click();", true);
                            this.DisplayFeedback(_GlobalResources.ThisFileCannotBeImportedDueToMistakesPleaseTryAgain.Replace("NUMBER", _ErrorNum.ToString()), true);
                            this._PreviousErrorButton.ID = "PreviousErrorButton";
                            this._PreviousErrorButton.CssClass = "Button ActionButton";
                            this._PreviousErrorButton.Attributes.Add("onclick", "PreviousError();return false;");
                            this._PreviousErrorButton.Text = _GlobalResources.PreviousError;

                            this._NextErrorButton.ID = "NextErrorButton";
                            this._NextErrorButton.CssClass = "Button ActionButton";
                            this._NextErrorButton.Attributes.Add("onclick", "NextError();return false;");
                            this._NextErrorButton.Text = _GlobalResources.NextError;

                            this.ErrorTablePanel.Controls.Add(this._PreviousErrorButton);
                            this.ErrorTablePanel.Controls.Add(this._NextErrorButton);

                            this.ErrorTablePanel.Controls.Add(this.PanelMsgTable);
                        }
                        else if (columnMatched)
                        {
                            using (TransactionScope scope = new TransactionScope())
                            {
                                int idActivityImport = Library.Activity.ImportRecord((this._UserObject != null) ? this._UserObject.Id.ToString() : AsentiaSessionState.IdSiteUser.ToString(), _ActivityUploader.FileOriginalName);
                                if (idActivityImport > 0)
                                {
                                    activityDataTable.Columns.RemoveAt(activityDataTable.Columns.Count - 1);
                                    Library.Activity.Import(idActivityImport, activityDataTable);
                                }

                                scope.Complete();
                                Session["Imported"] = "Success";
                                Response.Redirect(Request.RawUrl);
                            }

                            File.Delete(Server.MapPath(_ActivityUploader.SavedFilePath));
                        }
                    }
                    catch (AsentiaException ex)
                    {
                        // display the failure message
                        this.DisplayFeedback(ex.Message, true);
                    }
                    catch (Exception)
                    { ;}
                }
            }
            this._ActivityFileModal.HideModal();
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction.ID = "GridConfirmAction";

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedActivityImport_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseActivityImport_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);
            this.ActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.ActivityGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.ActivityGrid.Rows[i].FindControl(this.ActivityGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.Activity.Delete(recordsToDelete);

                    // rebind the grid
                    this.ActivityGrid.BindData();

                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this.ManageActivityFeedbackContainer, _GlobalResources.TheSelectedActivityImport_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedbackInSpecifiedContainer(this.ManageActivityFeedbackContainer, _GlobalResources.NoActivityImportFile_sSelectedForDeletion, true);
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.ManageActivityFeedbackContainer, ex.Message, true);

                // rebind the grid
                this.ActivityGrid.BindData();
            }
        }
        #endregion

        #region _ColumnValueValidation
        /// <summary>
        /// Validation for each activity field based on the datatype or its special format
        /// </summary>
        private void _ColumnValueValidation(string[] columns, bool differentCompleteDate, bool sameLessonName, bool lessonTimeStampLaterThanCourseCompletion)
        {
            _RowNum++;
            string originalData = String.Empty;
            double chkNumber;
            int n;
            bool containLessonInfo = (!String.IsNullOrWhiteSpace(_CheckNullValue(columns[5]))) ||
                                     (!String.IsNullOrWhiteSpace(_CheckNullValue(columns[6]))) ||
                                     (!String.IsNullOrWhiteSpace(_CheckNullValue(columns[7]))) ||
                                     (!String.IsNullOrWhiteSpace(_CheckNullValue(columns[8]))) ||
                                     (!String.IsNullOrWhiteSpace(_CheckNullValue(columns[9]))); 
            DateTime dateValue;
            TableRow rowActivityData = new TableRow();

            TableCell rowNum = new TableCell();
            rowNum.Text = _RowNum.ToString();
            rowNum.CssClass = "ExcelTableRowHeader";
            rowActivityData.Controls.Add(rowNum);

            for (int i = 0; i < columns.Length; i++)
            {
                TableCell cellActivityData = new TableCell();
                cellActivityData.CssClass = "ExcelTableData";
                string cellText = _CheckNullValue(columns[i]);
                string columnIdentifier = ActivityImportColumns[i].ColumnName;
                string errorText = "<a href='#' id='ErrorPositionError#'><font color=red>" + cellText + "<sup>Error#</sup></font></ a>";

                //Check if the columns allow null value, if not, check the import value for that column.
                if (_ActivityColumnsInfoTable.Rows[i + 1].Cells[2].Text.Contains(_GlobalResources.Yes) && String.IsNullOrWhiteSpace(cellText))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_IsARequiredFieldThisFieldMustContainAValue.Replace("##FIELD##", columnIdentifier);
                }
                //Check the String length
                else if (_ActivityColumnsInfoTable.Rows[i + 1].Cells[3].Text.Contains(_GlobalResources.Text) && columnIdentifier != _GlobalResources.CourseCode && !String.IsNullOrWhiteSpace(cellText) && int.TryParse(_ActivityColumnsInfoTable.Rows[i + 1].Cells[4].Text, out n) && cellText.Length > Convert.ToInt16(_ActivityColumnsInfoTable.Rows[i + 1].Cells[4].Text))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.TheLengthOfThisEntryIsExeedingItsMaxLength.Replace("##X##", _ActivityColumnsInfoTable.Rows[i + 1].Cells[4].Text);
                }
                //Check the Datetime format
                else if (_ActivityColumnsInfoTable.Rows[i + 1].Cells[3].Text.Contains(_GlobalResources.DateTime) && !String.IsNullOrWhiteSpace(cellText) && !DateTime.TryParse(cellText, out dateValue))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_MustBeAValidDateInTheISODateTimeFormat.Replace("##FIELD##", columnIdentifier);
                }
                //Check the credit Format
                else if (columnIdentifier == _GlobalResources.CourseCredits && !String.IsNullOrWhiteSpace(cellText) && (!double.TryParse(cellText, out chkNumber) || chkNumber > 999 || chkNumber < 0))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_MustContainANumberBetween0And999.Replace("##FIELD##", columnIdentifier);
                }
                //Check the score Format
                else if (columnIdentifier == _GlobalResources.ModuleScore && !String.IsNullOrWhiteSpace(cellText) && (!double.TryParse(cellText, out chkNumber) || chkNumber > 100 || chkNumber < 0))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_MustContainANumberBetween0And100.Replace("##FIELD##", columnIdentifier);
                }
                //Check Lesson Completion Status
                else if (columnIdentifier == _GlobalResources.ModuleCompletionStatus && !String.IsNullOrWhiteSpace(cellText) && !Enum.IsDefined(typeof(Activity.LessonCompletionTypes), cellText.First().ToString().ToUpper() + cellText.Substring(1).ToLower()))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_MustContainTheValueCompleteIncompleteOrUnknown.Replace("##FIELD##", columnIdentifier);
                }
                //Check Lesson Success
                else if (columnIdentifier == _GlobalResources.LessonSuccess && !String.IsNullOrWhiteSpace(cellText) && !Enum.IsDefined(typeof(Activity.LessonSuccessTypes), cellText.First().ToString().ToUpper() + cellText.Substring(1).ToLower()))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_MustContainTheValuePassedFailedOrUnknown.Replace("##FIELD##", columnIdentifier);
                }
                //Check the userImported
                else if (columnIdentifier == _GlobalResources.Username && !String.IsNullOrWhiteSpace(cellText) && !String.IsNullOrWhiteSpace(this._UsernameImported) && (this._UsernameImported != cellText))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.OnlyDataForTheUsername_username_CanBeImported.Replace("##username##", this._UsernameImported);
                }
                //Check if the username existed
                else if (columnIdentifier == _GlobalResources.Username && !String.IsNullOrWhiteSpace(cellText) && _UsernameNotExistList.Contains(cellText))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.TheUserWithTheUsername_username_DoesNotExistInThePortal.Replace("##username##", cellText);
                }
                //Check if the coursecode existed
                else if (columnIdentifier == _GlobalResources.CourseCode && !String.IsNullOrWhiteSpace(cellText) && _CourseCodeNotExistList.Contains(cellText))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.TheCourseWithTheCode_DoesNotExistInThePortal.Replace("##coursecode##", cellText);
                }
                //Check the endRow
                else if (columnIdentifier == _GlobalResources.ROWEND_UPPER && !String.IsNullOrWhiteSpace(cellText) && (cellText.ToUpper() != _GlobalResources.END_UPPER))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_MustContainTheTextEND.Replace("##FIELD##", columnIdentifier);
                }

                //Check required field if containLessonInfo is true
                else if (containLessonInfo && (columnIdentifier == _GlobalResources.LessonName || columnIdentifier == _GlobalResources.ModuleActivityTimestamp || columnIdentifier == _GlobalResources.ModuleCompletionStatus || columnIdentifier == _GlobalResources.LessonSuccess) && String.IsNullOrWhiteSpace(cellText))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_IsARequiredFieldThisFieldMustContainAValue.Replace("##FIELD##", columnIdentifier);
                }
                //Check different completionDate
                else if (differentCompleteDate && (columnIdentifier == _GlobalResources.CourseCompletionDate))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_ContainsACompletionDateThatIsDifferentThanTheCompletionDateForOtherRowsForTheSameUserAndCourse.Replace("##FIELD##", columnIdentifier);
                }
                //Check completionDate not in the future
                else if ((columnIdentifier == _GlobalResources.CourseCompletionDate) && (Convert.ToDateTime(cellText) > DateTime.Now))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_ContainsACompletionDateThatIsInTheFuture.Replace("##FIELD##", columnIdentifier);
                }
                //Check different lesson Name
                else if (sameLessonName && (columnIdentifier == _GlobalResources.LessonName))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.CannotContainIdenticalModuleNamesWithOtherRownUnderTheSameUserAndCourse;
                }
                //Check lesson timestamp valid or not
                else if (lessonTimeStampLaterThanCourseCompletion && (columnIdentifier == _GlobalResources.ModuleActivityTimestamp))
                {
                    _ErrorNum++;
                    cellActivityData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellActivityData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellActivityData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.ModuleTimestampShouldBePrior;
                }
                else
                {
                    cellActivityData.Text = columns[i];
                }

                rowActivityData.Controls.Add(cellActivityData);
            }

            _ActivityImportTable.Controls.Add(rowActivityData);

        }
        #endregion

        #region _CheckNullValue
        /// <summary>
        /// Check if an input is null value or not.
        /// </summary>
        private string _CheckNullValue(string value)
        {
            string returnValue = value;
            if (value.ToLower() == "null" || String.IsNullOrWhiteSpace(value) || String.IsNullOrWhiteSpace(value))
                returnValue = null;
            return returnValue;
        }
        #endregion
    }
}
