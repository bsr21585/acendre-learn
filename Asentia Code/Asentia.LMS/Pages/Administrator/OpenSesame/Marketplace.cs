﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using Asentia.LMS.Library;
using Opensesame.Api;

namespace Asentia.LMS.Pages.Administrator.OpenSesame
{
    public class Marketplace : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel MarketplaceFormContentWrapperContainer;        
        #endregion

        #region Private Properties
        private User _UserObject = new User();
        private bool _AllowProvision = true;

        string _TenantID = String.Empty;
        string _TenantKey = String.Empty;
        string _TenantSecret = String.Empty;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Marketplace), "Asentia.LMS.Pages.Administrator.OpenSesame.Marketplace.js");

            // build start up call to fire session keep alive calls
            StringBuilder startupJS = new StringBuilder();
            startupJS.AppendLine("setInterval(function () { Helper.SessionKeepAlive(" + AsentiaSessionState.IdSiteUser.ToString() + "); }, 300000);"); // hit the keep alive every 5 minutes
            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "SessionKeepAliveStartup", startupJS.ToString(), true);
        }
        #endregion        

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {  
            // ensure that OpenSesame is enabled on this site
            if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OPENSESAME_ENABLE))
            { Response.Redirect("/"); }

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_ContentPackageManager))
            { Response.Redirect("/"); }
            
            // ensure that the user has at least an email address so we can provision
            if (AsentiaSessionState.IdSiteUser == 1)
            {
                if (String.IsNullOrWhiteSpace(AsentiaSessionState.GlobalSiteObject.ContactEmail))
                {
                    this.DisplayFeedback(_GlobalResources.TheAdministratorAccountDoesNotHaveAnEmailAddressAssociatedWithItThisIsRequiredToLoadTheOpenSesameMarketplace, true);
                    this._AllowProvision = false;
                }
            }
            else
            {
                this._UserObject = new User(AsentiaSessionState.IdSiteUser);

                if (String.IsNullOrWhiteSpace(this._UserObject.Email))
                {
                    this.DisplayFeedback(_GlobalResources.YourUserProfileDoesNotHaveAnEmailAddressAssociatedWithItThisIsRequiredToLoadTheOpenSesameMarketplace, true);
                    this._AllowProvision = false;
                }
            }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("page-specific/administrator/opensesame/Marketplace.css");
            
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.OpenSesameMarketplace));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, _GlobalResources.OpenSesameMarketplace, ImageFiles.GetIconPath(ImageFiles.ICON_OPENSESAME, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.MarketplaceFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            if (this._AllowProvision)
            {
                // provision the tenant
                this._ProvisionTenant();

                // provision the user
                string ssoURL = this._ProvisionUser();

                // set the pull sync url
                string pullSyncUrl = HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "") + "/administrator/opensesame/PullSync.aspx?sync=opensesameiframe";                

                // attach the OpenSesame iframe
                HtmlIframe openSesameIframe = new HtmlIframe();
                openSesameIframe.ID = "OpenSesameMarketplaceFrame";
                openSesameIframe.Src = ssoURL + "?PullContentSyncUrl=" + Server.UrlEncode(pullSyncUrl);
                this.MarketplaceFormContentWrapperContainer.Controls.Add(openSesameIframe);
            }
        }
        #endregion

        #region _ProvisionTenant
        /// <summary>
        /// Provisions the OpenSesame tenant for this portal if it hasn't already been provisioned. 
        /// </summary>
        private void _ProvisionTenant()
        {
            // integration information - just put these in local variables so they're easier to work with in code below           
            string integrationName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_INTEGRATIONNAME);
            string integrationKey = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_INTEGRATIONKEY);
            string integrationSecret = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_INTEGRATIONSECRET);

            // tenant information
            this._TenantID = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_TENANTID);
            this._TenantKey = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_TENANTCONSUMERKEY);
            this._TenantSecret = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_TENANTCONSUMERSECRET);

            // if the tenant information is blank, provision the tenant
            if (String.IsNullOrWhiteSpace(this._TenantID) && String.IsNullOrWhiteSpace(this._TenantKey) && String.IsNullOrWhiteSpace(this._TenantSecret))
            {  
                // generate the tenant information
                string newTenantID = AsentiaSessionState.IdAccount + "-" + AsentiaSessionState.IdSite + "-" + AsentiaSessionState.SiteHostname;
                string newTenantName = AsentiaSessionState.GlobalSiteObject.Title;

                // provision the tenant
                Opensesame.Api.Tenant tenant = Opensesame.Api.Api.ProvisionTenant(integrationName, newTenantID, newTenantName, Config.ApplicationSettings.OpenSesameTenantMode, integrationKey, integrationSecret);

                // set the id, key, and secret returned from provisioning
                this._TenantID = tenant.TenantId;
                this._TenantKey = tenant.ConsumerKey;
                this._TenantSecret = tenant.ConsumerSecret;

                // put the provisioned tenant information into site params
                DataTable siteParams = new DataTable();
                siteParams.Columns.Add("key");
                siteParams.Columns.Add("value");

                DataRow newSiteParamRow;

                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.OPENSESAME_TENANTID;
                newSiteParamRow["value"] = this._TenantID;
                siteParams.Rows.Add(newSiteParamRow);

                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.OPENSESAME_TENANTCONSUMERKEY;
                newSiteParamRow["value"] = this._TenantKey;
                siteParams.Rows.Add(newSiteParamRow);

                newSiteParamRow = siteParams.NewRow();
                newSiteParamRow["key"] = SiteParamConstants.OPENSESAME_TENANTCONSUMERSECRET;
                newSiteParamRow["value"] = this._TenantSecret;
                siteParams.Rows.Add(newSiteParamRow);

                // save the site params
                AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, siteParams);
            }
        }
        #endregion

        #region _ProvisionUser
        /// <summary>
        /// Provisions the OpenSesame user for the "OpenSesame Marketplace" session. User is based on the current session user. 
        /// </summary>
        /// <returns>string containing the OpenSesame SSO URL for iframe</returns>
        private string _ProvisionUser()
        {
            // integration information - just put these in local variables so they're easier to work with in code below           
            string integrationName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_INTEGRATIONNAME);
            string integrationKey = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_INTEGRATIONKEY);
            string integrationSecret = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_INTEGRATIONSECRET);

            // declare properties for user
            string userFirstName = String.Empty;
            string userLastName = String.Empty;
            string userEmail = String.Empty;
            string userIdentifier = String.Empty;

            // site admin gets provisioned differently than "normal user"
            if (AsentiaSessionState.IdSiteUser == 1)
            {
                userFirstName = "Administrator";
                userLastName = "Account";
                userEmail = AsentiaSessionState.GlobalSiteObject.ContactEmail;
                userIdentifier = AsentiaSessionState.IdAccount + "-" + AsentiaSessionState.IdSite + "-" + AsentiaSessionState.SiteHostname + "-administrator";
            }
            else
            {
                userFirstName = !String.IsNullOrWhiteSpace(this._UserObject.FirstName) ? this._UserObject.FirstName : "[EMPTY]";
                userLastName = !String.IsNullOrWhiteSpace(this._UserObject.LastName) ? this._UserObject.LastName : "[EMPTY]";
                userEmail = this._UserObject.Email;
                userIdentifier = AsentiaSessionState.IdAccount + "-" + AsentiaSessionState.IdSite + "-" + AsentiaSessionState.SiteHostname + "-" + this._UserObject.Username;
            }

            // provision the user and return the sso url
            return Opensesame.Api.Api.ProvisionUser(integrationName, this._TenantID, this._TenantKey, this._TenantSecret, userFirstName, userLastName, userEmail, userIdentifier);
        }
        #endregion        
    }
}
