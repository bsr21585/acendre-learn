﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using Asentia.LMS.Library;
using Opensesame.Api;

namespace Asentia.LMS.Pages.Administrator.OpenSesame
{
    public class PullSync : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel PullSyncFormContentWrapperContainer;
        #endregion

        #region Private Properties
        private User _UserObject = new User();
        private bool _HaltExecution = false;

        private string _IntegrationName = String.Empty;
        private string _TenantID = String.Empty;
        private string _TenantKey = String.Empty;
        private string _TenantSecret = String.Empty;

        private ModalPopup _SynchronizeOpenSesameContentModal;
        private Button _SynchronizeOpenSesameContentModalHiddenLaunchButton;
        private Button _SynchronizeOpenSesameContentModalHiddenLoadButton;

        private const string _SCORM_MANIFEST_NAME = "imsmanifest.xml";
        private const string _XAPI_MANIFEST_NAME = "tincan.xml";
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!this._HaltExecution)
            {
                // register the embedded javascript resource(s)
                ClientScriptManager csm = this.Page.ClientScript;
                csm.RegisterClientScriptResource(typeof(PullSync), "Asentia.LMS.Pages.Administrator.OpenSesame.PullSync.js");

                // set up javascript globals 
                StringBuilder globalJS = new StringBuilder();
                globalJS.AppendLine("var SynchronizingLabel = \"" + _GlobalResources.Synchronizing + "\";");
                globalJS.AppendLine("var SynchronizeOpenSesameContentModalHiddenLoadButtonId = \"" + this._SynchronizeOpenSesameContentModalHiddenLoadButton.ID + "\";");
                csm.RegisterClientScriptBlock(typeof(PullSync), "PullSyncGlobalJs", globalJS.ToString(), true);

                // build start up call to fire the sync
                StringBuilder startupJS = new StringBuilder();
                startupJS.AppendLine("setTimeout(FireOpenSesameContentSync, 500);");
                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "FireOpenSesameContentSyncStartup", startupJS.ToString(), true);
            }
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {            
            // include page-specific css files            
            this.IncludePageSpecificCssFile("page-specific/administrator/opensesame/PullSync.css");               

            // hide interface elements, this is exclusively a "callback page" for iframed OpenSesame, so no interface
            this._HideAsentiaMainInterfaceElements();            

            // format the content wrapper
            this.PullSyncFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // NOTE, SINCE THE INTENT IS FOR THIS TO BE EXCLUSIVELY INCLUDED IN AN IFRAME, WE DO NOT WANT TO SHOW ANY INTERFACE AT ALL
            // SO, PERMISSIONS AND OTHER SANITY CHECKS SHOULD BE MET WITH A HALT IN EXECUTION RATHER THAN A RE-DIRECT

            // ensure that OpenSesame is enabled on this site
            if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OPENSESAME_ENABLE))
            { this._HaltExecution = true; }

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_ContentPackageManager))
            { this._HaltExecution = true; }

            // this sync is keyed to a querystring parameter so that this page cannot be hit directly, so check for that
            if (this.QueryStringString("sync") != "opensesameiframe")
            { this._HaltExecution = true; }

            // ensure that all integration/tenant parameters are present      
            this._IntegrationName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_INTEGRATIONNAME);
            this._TenantID = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_TENANTID);
            this._TenantKey = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_TENANTCONSUMERKEY);
            this._TenantSecret = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.OPENSESAME_TENANTCONSUMERSECRET);

            if (String.IsNullOrWhiteSpace(this._IntegrationName) || String.IsNullOrWhiteSpace(this._TenantID) || String.IsNullOrWhiteSpace(this._TenantKey) || String.IsNullOrWhiteSpace(this._TenantSecret))
            { this._HaltExecution = true; }

            if (!this._HaltExecution)
            {
                this._BuildModal();
                this._SynchronizeOpenSesameContentModal.ShowModal();
            }
            else
            { }
        }
        #endregion

        #region _HideAsentiaMainInterfaceElements
        /// <summary>
        /// Hides all main Asentia interface elements from the page. This is called when we need to hide
        /// the Asentia interface because this page was called by the OpenSesame iframe. 
        /// </summary>
        private void _HideAsentiaMainInterfaceElements()
        {
            Control mastheadContainer = AsentiaPage.FindControlRecursive(this.Page, "MastheadContainer");
            Control breadcrumbContainer = AsentiaPage.FindControlRecursive(this.Page, "BreadcrumbContainer");
            Control pageTitleContainer = AsentiaPage.FindControlRecursive(this.Page, "PageTitleContainer");
            Control footerContainer = AsentiaPage.FindControlRecursive(this.Page, "FooterContainer");

            if (mastheadContainer != null)
            { mastheadContainer.Visible = false; }

            if (breadcrumbContainer != null)
            { breadcrumbContainer.Visible = false; }

            if (pageTitleContainer != null)
            { pageTitleContainer.Visible = false; }

            if (footerContainer != null)
            { footerContainer.Visible = false; }
        }
        #endregion

        #region _BuildModal
        /// <summary>
        /// Builds the synchronize modal.
        /// </summary>
        private void _BuildModal()
        {
            // THE SUPPORTING ELEMENTS - TRIGGERS, HIDDEN FIELDS, ETC.

            // instansiate enroll in instructor led training controls
            this._SynchronizeOpenSesameContentModalHiddenLaunchButton = new Button();
            this._SynchronizeOpenSesameContentModalHiddenLaunchButton.ID = "SynchronizeOpenSesameContentModalHiddenLaunchButton";
            this._SynchronizeOpenSesameContentModalHiddenLaunchButton.Style.Add("display", "none");
            this._SynchronizeOpenSesameContentModalHiddenLaunchButton.OnClientClick = "return false;";

            this._SynchronizeOpenSesameContentModalHiddenLoadButton = new Button();
            this._SynchronizeOpenSesameContentModalHiddenLoadButton.ID = "SynchronizeOpenSesameContentModalHiddenLoadButton";
            this._SynchronizeOpenSesameContentModalHiddenLoadButton.Style.Add("display", "none");
            this._SynchronizeOpenSesameContentModalHiddenLoadButton.Click += this._SynchronizeOpenSesameContent;

            // add trigger button to container
            this.PullSyncFormContentWrapperContainer.Controls.Add(this._SynchronizeOpenSesameContentModalHiddenLaunchButton);

            // THE MODAL

            // set modal properties
            this._SynchronizeOpenSesameContentModal = new ModalPopup("SynchronizeOpenSesameContentModal");
            this._SynchronizeOpenSesameContentModal.Type = ModalPopupType.Form;
            this._SynchronizeOpenSesameContentModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_OPENSESAME, ImageFiles.EXT_PNG);
            this._SynchronizeOpenSesameContentModal.HeaderIconAlt = _GlobalResources.SynchronizeOpenSesameContent;
            this._SynchronizeOpenSesameContentModal.HeaderText = _GlobalResources.SynchronizeOpenSesameContent;            
            this._SynchronizeOpenSesameContentModal.TargetControlID = this._SynchronizeOpenSesameContentModalHiddenLaunchButton.ID;            
            this._SynchronizeOpenSesameContentModal.SubmitButton.Visible = false;
            this._SynchronizeOpenSesameContentModal.CloseButton.Visible = false;
            this._SynchronizeOpenSesameContentModal.ShowCloseIcon = false;         

            // build the modal body
            this._SynchronizeOpenSesameContentModal.AddControlToBody(this._SynchronizeOpenSesameContentModalHiddenLoadButton);            

            // add modal to container
            this.PullSyncFormContentWrapperContainer.Controls.Add(this._SynchronizeOpenSesameContentModal);
        }
        #endregion

        #region _SynchronizeOpenSesameContent
        /// <summary>
        /// Downloads and synchronizes Open Sesame content.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _SynchronizeOpenSesameContent(object sender, EventArgs e)
        {
            try
            {               
                // generate a extraction foldername and filename for the sync package
                string zipExtractionFolderName = "OpenSesameSync-" + AsentiaSessionState.SiteHostname + AsentiaSessionState.UtcNow.ToString("yyyy-MM-dd-HH-mm-ss");
                string zipFileName = zipExtractionFolderName + ".zip";

                // download the sync package
                MemoryStream ms = Opensesame.Api.Api.PullSync(this._IntegrationName, this._TenantID, this._TenantKey, this._TenantSecret);

                // if we got something from the memory stream, save it
                if (ms != null)
                {
                    using (FileStream fs = new FileStream(Server.MapPath(SitePathConstants.DOWNLOAD) + zipFileName, FileMode.Create))
                    {
                        ms.Position = 0;
                        ms.Read(ms.ToArray(), 0, (int)ms.Length);
                        fs.Write(ms.ToArray(), 0, ms.ToArray().Length);
                        ms.Close();
                        fs.Flush();
                        fs.Close();
                    }
                }
                else
                { throw new AsentiaException(_GlobalResources.ThereAreNoContentPackagesToSynchronize); }

                // extract the "main" sync zip file
                ZipFile.ExtractToDirectory(Server.MapPath(SitePathConstants.DOWNLOAD) + zipFileName, Server.MapPath(SitePathConstants.DOWNLOAD) + zipExtractionFolderName);

                // extract the "courses" zip file
                if (File.Exists(Server.MapPath(SitePathConstants.DOWNLOAD + zipExtractionFolderName + "/courses.zip")))
                {
                    ZipFile.ExtractToDirectory(Server.MapPath(SitePathConstants.DOWNLOAD + zipExtractionFolderName + "/courses.zip"), Server.MapPath(SitePathConstants.DOWNLOAD + zipExtractionFolderName + "/courses"));

                    // load the main inventory metadata file and sync the packages
                    if (File.Exists(Server.MapPath(SitePathConstants.DOWNLOAD + zipExtractionFolderName + "/metadata.json")))
                    {
                        string metadataFileContents = File.ReadAllText(Server.MapPath(SitePathConstants.DOWNLOAD + zipExtractionFolderName + "/metadata.json"), Encoding.UTF8);

                        if (!String.IsNullOrWhiteSpace(metadataFileContents))
                        {
                            // get a list of already synched packages
                            DataTable synchedOpenSesameGUIDsDT = ContentPackage.GetOpenSesameGUIDs();
                            List<string> synchedOpenSesameGUIDs = new List<string>();
                        
                            foreach (DataRow row in synchedOpenSesameGUIDsDT.Rows)
                            { synchedOpenSesameGUIDs.Add(row["openSesameGUID"].ToString()); }

                            // since the file contains [{JSON}], newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so make it look like an object
                            string metadataJsonString = "{\"Root\" : " + metadataFileContents + "}";

                            // create a json object from the string
                            Newtonsoft.Json.Linq.JObject metadataJsonObject = Newtonsoft.Json.Linq.JObject.Parse(metadataJsonString);

                            // get an array of the sync items from the object
                            Newtonsoft.Json.Linq.JArray metadataItemsArray = (Newtonsoft.Json.Linq.JArray)metadataJsonObject["Root"];

                            // if the array is not null, loop through it and sync each item; otherwise, throw an exception
                            if (metadataItemsArray != null)
                            {
                                foreach (Newtonsoft.Json.Linq.JObject metadataItem in metadataItemsArray)
                                {
                                    string zipFilename = metadataItem["zipFilename"].ToString();
                                    string title = metadataItem["title"].ToString();
                                    string openSesameGUID = metadataItem["externalId"].ToString();

                                    if (!String.IsNullOrWhiteSpace(zipFilename) && !String.IsNullOrWhiteSpace(title) && !String.IsNullOrWhiteSpace(openSesameGUID))
                                    {
                                        if (!synchedOpenSesameGUIDs.Contains(openSesameGUID))
                                        {
                                            // sync the item
                                            this._SynchronizePackage(zipExtractionFolderName, zipFilename, title, openSesameGUID);
                                        }
                                    }
                                }
                            }
                            else
                            { throw new AsentiaException(_GlobalResources.TheMetadataFileDoesNotContainAnyPackageInformationForSynchronization); }
                        }
                    }
                    else
                    { throw new AsentiaException(_GlobalResources.TheOpenSesameMetadataFileCouldNotBeFound); }
                }
                else
                { throw new AsentiaException(_GlobalResources.TheOpenSesameCourseContentCouldNotBeFound); }

                // clean up the downloaded content
                this._CleanUpDownloadedContent(zipExtractionFolderName, zipFileName);

                // the package synchronization is complete, alert it
                this._SynchronizeOpenSesameContentModal.DisplayFeedback(_GlobalResources.TheContentPackage_sHaveBeenSynchronizedSuccessfullyOpenSesame, false);
            }
            catch (AsentiaException aEx)
            { this._SynchronizeOpenSesameContentModal.DisplayFeedback(aEx.Message, true); }
        }
        #endregion

        #region _SynchronizePackage
        /// <summary>
        /// Synchronizes/imports an Open Sesame content package.
        /// </summary>
        /// <param name="zipExtractionFolderName">the folder the package is located in</param>
        /// <param name="zipFilename">the package zip filename</param>
        /// <param name="title">the title of the package</param>
        /// <param name="openSesameGUID">the Open Sesame GUID of the package</param>
        private void _SynchronizePackage(string zipExtractionFolderName, string zipFilename, string title, string openSesameGUID)
        {
            if (!String.IsNullOrWhiteSpace(zipFilename) && !String.IsNullOrWhiteSpace(title) && !String.IsNullOrWhiteSpace(openSesameGUID))
            {
                if (File.Exists(Server.MapPath(SitePathConstants.DOWNLOAD + zipExtractionFolderName + "/courses/" + zipFilename)))
                {
                    try
                    {
                        // get the date time down to ms
                        string dateTimeDownToMilliseconds = AsentiaSessionState.UtcNow.ToString("o").Replace(":", "-");

                        // get md5 hash of original file name and the date time down to ms
                        string md5HashForPath = Cryptography.GetHash(zipFilename + dateTimeDownToMilliseconds, Cryptography.HashType.MD5);

                        // get current date time formatted
                        string currentDateTime = AsentiaSessionState.UtcNow.ToString("yyyy-MM-dd-hh-mm-ss");

                        // build the folder path to extract the zip file to
                        string destinationFolderPath = AsentiaSessionState.IdAccount
                                                       + "-"
                                                       + AsentiaSessionState.IdSite
                                                       + "-"
                                                       + md5HashForPath
                                                       + "-"
                                                       + currentDateTime;

                        // extract the package to the content package upload directory
                        ZipFile.ExtractToDirectory(Server.MapPath(SitePathConstants.DOWNLOAD + zipExtractionFolderName + "/courses/" + zipFilename), Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath));

                        // VALIDATE AND SAVE THE PACKAGE

                        bool hasManifestFile = false;
                        string manifestFileContent = String.Empty;
                        string manifestNonFatalErrorDescriptions = String.Empty;
                        ContentPackage.ContentPackageType contentPackageType = 0;
                        ContentPackage.SCORMPackageType? scormPackageType = null;

                        // look for tincan (xAPI) manifest
                        if (File.Exists(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _XAPI_MANIFEST_NAME)))
                        {
                            // identify the package as an xAPI package
                            contentPackageType = ContentPackage.ContentPackageType.xAPI;

                            // show that we found a manifest
                            hasManifestFile = true;

                            // NO VALIDATION IS NEEDED FOR A TINCAN PACKAGE

                            // read the entire manifest into the manifest file content string
                            using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _XAPI_MANIFEST_NAME)))
                            { manifestFileContent = reader.ReadToEnd(); }
                        }
                        // look for a scorm manifest
                        else if (File.Exists(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _SCORM_MANIFEST_NAME)))
                        {
                            // show that we found a manifest
                            hasManifestFile = true;

                            // identify the package as a SCORM package
                            contentPackageType = ContentPackage.ContentPackageType.SCORM;

                            // validate the SCORM manifest for fatal and non-fatal errors
                            Library.SCORM.ManifestValidator manifestValidator = new Library.SCORM.ManifestValidator();

                            // validate to fatal errors
                            ArrayList manifestFatalErrors = manifestValidator.ValidateManifestFileForFatalErrors(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _SCORM_MANIFEST_NAME));

                            // if there are errors, quietly die
                            // since this is a sync of multiple packages, we don't want to stop it
                            if (manifestFatalErrors != null && manifestFatalErrors.Count > 0)
                            {
                                // since manifest validation failed, delete the directory.
                                Directory.Delete(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath), true);

                                // exit
                                return;
                            }

                            // validate to non-fatal errors
                            bool isResourcePackage;

                            ArrayList manifestNonFatalErrors = manifestValidator.ValidateManifestFileToXSD(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _SCORM_MANIFEST_NAME), out isResourcePackage);

                            // if there are errors, manifest is not valid, but the errors are not fatal, so we log them and proceed
                            if (manifestNonFatalErrors != null && manifestNonFatalErrors.Count > 0)
                            {
                                // provide feedback to the user for the invalid manifest                        
                                foreach (Library.SCORM.ManifestValidator.ValidationError error in manifestNonFatalErrors)
                                { manifestNonFatalErrorDescriptions += "Line " + error.LineNumber + ": " + error.Desc + Environment.NewLine; }
                            }

                            // read the entire manifest into the manifest file content string
                            using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _SCORM_MANIFEST_NAME)))
                            { manifestFileContent = reader.ReadToEnd(); }

                            // set the scorm package type
                            if (isResourcePackage)
                            { scormPackageType = ContentPackage.SCORMPackageType.Resource; }
                            else
                            { scormPackageType = ContentPackage.SCORMPackageType.Content; }
                        }
                        else
                        { }

                        // if there was no manifest file, quietly die
                        // since this is a sync of multiple packages, we don't want to stop it
                        if (!hasManifestFile)
                        {
                            // since there is no manifest, delete the directory.
                            Directory.Delete(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath), true);

                            // exit
                            return;
                        }

                        // create a package object
                        ContentPackage packageObject = new ContentPackage();
                        packageObject.IdSite = AsentiaSessionState.IdSite;
                        packageObject.Name = title;
                        packageObject.Path = SitePathConstants.WAREHOUSE + destinationFolderPath;
                        packageObject.Manifest = manifestFileContent;
                        packageObject.IdContentPackageType = contentPackageType;
                        packageObject.HasManifestComplianceErrors = false;
                        packageObject.OpenSesameGUID = openSesameGUID;

                        if (contentPackageType == ContentPackage.ContentPackageType.SCORM)
                        { packageObject.IdSCORMPackageType = scormPackageType; }
                        else
                        { packageObject.IdSCORMPackageType = null; }

                        // move the unzipped directory to its final destination in warehouse
                        Directory.Move(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath), Server.MapPath(SitePathConstants.WAREHOUSE + destinationFolderPath));

                        // get the size in kb of the content directory
                        packageObject.Kb = Utility.CalculateDirectorySizeInKb(Server.MapPath(SitePathConstants.WAREHOUSE + destinationFolderPath));

                        // if there were some "non-fatal" manifest errors, flag the package and record them
                        if (!String.IsNullOrWhiteSpace(manifestNonFatalErrorDescriptions))
                        {
                            packageObject.HasManifestComplianceErrors = true;
                            packageObject.ManifestComplianceErrors = manifestNonFatalErrorDescriptions;
                        }

                        // save the package to database
                        int idPackage = packageObject.Save();
                    }
                    catch (Exception ex)
                    { throw new AsentiaException(ex.Message); }
                }
            }
        }
        #endregion

        #region _CleanUpDownloadedContent
        /// <summary>
        /// Deletes the files and folders that were downloaded for the sync.
        /// </summary>
        /// <param name="zipExtractionFolderName">the folder name</param>
        /// <param name="zipFileName">the zip file name</param>
        private void _CleanUpDownloadedContent(string zipExtractionFolderName, string zipFileName)
        {
            // deleted the extracted folder
            if (Directory.Exists(Server.MapPath(SitePathConstants.DOWNLOAD) + zipExtractionFolderName))
            { Directory.Delete(Server.MapPath(SitePathConstants.DOWNLOAD) + zipExtractionFolderName, true); }

            // delete the zip file
            if (File.Exists(Server.MapPath(SitePathConstants.DOWNLOAD) + zipFileName))
            { File.Delete(Server.MapPath(SitePathConstants.DOWNLOAD) + zipFileName); }
        }
        #endregion
    }
}
