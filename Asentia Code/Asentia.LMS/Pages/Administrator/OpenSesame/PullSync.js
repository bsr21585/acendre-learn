﻿function FireOpenSesameContentSync() {
    $("#SynchronizeOpenSesameContentModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper").html(SynchronizingLabel);
    $("#" + SynchronizeOpenSesameContentModalHiddenLoadButtonId).click();
}

function RedirectToContentPackagesHome() {
    window.parent.location = "/administrator/packages/";
}