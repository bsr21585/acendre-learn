﻿using System;
using System.Collections;
using System.Data;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.StandupTraining.Sessions
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel SessionsFormContentWrapperContainer;
        public Panel StandupTrainingObjectMenuContainer;
        public Panel SessionsWrapperContainer;
        public Panel ObjectOptionsPanel;
        public Panel ActionsPanel;
        public UpdatePanel StandupTrainingSessionsGridUpdatePanel;
        public Grid StandupTrainingSessionsGrid;        
        #endregion

        #region Private Properties
        private Library.StandupTraining _StandupTrainingObject;

        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmActionModal;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            //ClientScriptManager csm = this.Page.ClientScript;
            //csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.StandupTraining.Sessions.Default.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_InstructorLedTrainingManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/standuptraining/sessions/Default.css");

            // get the standup training object
            this._GetStandupTrainingObject();

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // set container classes
            this.SessionsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.SessionsWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the standup training object menu
            if (this._StandupTrainingObject != null)
            {
                StandupTrainingObjectMenu standupTrainingObjectMenu = new StandupTrainingObjectMenu(this._StandupTrainingObject);
                standupTrainingObjectMenu.SelectedItem = StandupTrainingObjectMenu.MenuObjectItem.Sessions;

                this.StandupTrainingObjectMenuContainer.Controls.Add(standupTrainingObjectMenu);
            }

            // build the grid, actions panel, and modals
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();            

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.StandupTrainingSessionsGrid.BindData();
            }
        }
        #endregion

        #region _GetStandupTrainingObject
        /// <summary>
        /// Gets a standup training object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetStandupTrainingObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("stid", 0);
            int vsId = this.ViewStateInt(this.ViewState, "stid", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._StandupTrainingObject = new Library.StandupTraining(id); }
                }
                catch
                { Response.Redirect("~/administrator/standuptraining"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get standup training title information
            string standupTrainingTitleInInterfaceLanguage = this._StandupTrainingObject.Title;
            string standupTrainingImagePath;
            string standupTrainingImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Library.StandupTraining.LanguageSpecificProperty standupTrainingLanguageSpecificProperty in this._StandupTrainingObject.LanguageSpecificProperties)
                {
                    if (standupTrainingLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { standupTrainingTitleInInterfaceLanguage = standupTrainingLanguageSpecificProperty.Title; }
                }
            }

            if (this._StandupTrainingObject.Avatar != null)
            {
                standupTrainingImagePath = SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + this._StandupTrainingObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                standupTrainingImageCssClass = "AvatarImage";
            }
            else
            {
                standupTrainingImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.InstructorLedTrainingModules, "/administrator/standuptraining"));
            breadCrumbLinks.Add(new BreadcrumbLink(standupTrainingTitleInInterfaceLanguage, "/administrator/standuptraining/Dashboard.aspx?id=" + this._StandupTrainingObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Sessions));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, standupTrainingTitleInInterfaceLanguage, standupTrainingImagePath, _GlobalResources.Sessions, ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION, ImageFiles.EXT_PNG), standupTrainingImageCssClass);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD STANDUP TRAINING SESSION
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddStandupTrainingSessionLink",
                                                null,
                                                "Modify.aspx?stid=" + this._StandupTrainingObject.Id.ToString(),
                                                null,
                                                _GlobalResources.NewSession,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the sessions Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {            
            this.StandupTrainingSessionsGrid.StoredProcedure = Library.StandupTrainingInstance.GridProcedure;
            this.StandupTrainingSessionsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.StandupTrainingSessionsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.StandupTrainingSessionsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.StandupTrainingSessionsGrid.AddFilter("@idStandupTraining", SqlDbType.Int, 4, this._StandupTrainingObject.Id);
            this.StandupTrainingSessionsGrid.IdentifierField = "idStandupTrainingInstance";
            this.StandupTrainingSessionsGrid.DefaultSortColumn = "dtStart";
            this.StandupTrainingSessionsGrid.IsDefaultSortDescending = true;
            this.StandupTrainingSessionsGrid.SearchBoxPlaceholderText = _GlobalResources.SearchSessions;

            // data key names
            this.StandupTrainingSessionsGrid.DataKeyNames = new string[] { "idStandupTrainingInstance" };

            // columns
            GridColumn nameSeatsStartDate = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.Seats + ", " + _GlobalResources.WaitingSeats + ", " + _GlobalResources.StartDate + "", null, "dtStart"); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method            

            // add columns to data grid
            this.StandupTrainingSessionsGrid.AddColumn(nameSeatsStartDate);
            this.StandupTrainingSessionsGrid.AddColumn(options);

            // add row data bound event
            this.StandupTrainingSessionsGrid.RowDataBound += this._StandupTrainingSessionsGrid_RowDataBound;            
        }
        #endregion

        #region _StandupTrainingSessionsGrid_RowDataBound
        private void _StandupTrainingSessionsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idStandupTrainingInstance = Convert.ToInt32(rowView["idStandupTrainingInstance"]);

                // AVATAR, NAME, SEATS

                string name = Server.HtmlEncode(rowView["title"].ToString());
                int seats = Convert.ToInt32(rowView["seats"]);
                int seatsFilled = Convert.ToInt32(rowView["seatsFilled"]);
                int waitingSeats = Convert.ToInt32(rowView["waitingSeats"]);
                int waitingSeatsFilled = Convert.ToInt32(rowView["waitingSeatsFilled"]);
                StandupTrainingInstance.MeetingType type = (StandupTrainingInstance.MeetingType)Convert.ToInt32(rowView["type"]);
                Timezone sessionTimezone = new Timezone(Convert.ToInt32(rowView["idTimezone"]));
                DateTime sessionStart = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(rowView["dtStart"]), TimeZoneInfo.FindSystemTimeZoneById(sessionTimezone.dotNetName));
                string sessionStartDateFormatted = sessionStart.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
                string sessionStartTimeFormatted = sessionStart.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern);
                
                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CLASSROOM, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                if (type == StandupTrainingInstance.MeetingType.Online)
                { avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_WEBMEETING, ImageFiles.EXT_PNG); }
                else if (type == StandupTrainingInstance.MeetingType.GoToMeeting)
                { avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOMEETING, ImageFiles.EXT_PNG); }
                else if (type == StandupTrainingInstance.MeetingType.GoToTraining)
                { avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOTRAINING, ImageFiles.EXT_PNG); }
                else if (type == StandupTrainingInstance.MeetingType.GoToWebinar)
                { avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOWEBINAR, ImageFiles.EXT_PNG); }
                else if (type == StandupTrainingInstance.MeetingType.WebEx)
                { avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_WEBEX, ImageFiles.EXT_PNG); }
                else
                { }

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = name;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink nameLink = new HyperLink();
                nameLink.NavigateUrl = "Modify.aspx?stid=" + this._StandupTrainingObject.Id.ToString() + "&id=" + idStandupTrainingInstance.ToString();
                nameLink.Text = name;
                nameLabel.Controls.Add(nameLink);

                // seats
                Label seatsLabel = new Label();
                seatsLabel.CssClass = "GridSecondaryLine";
                seatsLabel.Text = _GlobalResources.Seats + ": " + seatsFilled.ToString() + "/" + seats.ToString();
                e.Row.Cells[1].Controls.Add(seatsLabel);

                // waiting seats
                Label waitingSeatsLabel = new Label();
                waitingSeatsLabel.CssClass = "GridSecondaryLine";
                waitingSeatsLabel.Text = _GlobalResources.WaitingSeats + ": " + waitingSeatsFilled.ToString() + "/" + waitingSeats.ToString();
                e.Row.Cells[1].Controls.Add(waitingSeatsLabel);

                // start date
                Label startDateLabel = new Label();
                startDateLabel.CssClass = "GridSecondaryLine";
                startDateLabel.Text = _GlobalResources.StartDate + ": " + sessionStartDateFormatted + " @ " + sessionStartTimeFormatted;
                e.Row.Cells[1].Controls.Add(startDateLabel);

                // OPTIONS COLUMN

                // manage roster
                Label manageRosterSpan = new Label();
                manageRosterSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(manageRosterSpan);

                HyperLink manageRosterLink = new HyperLink();
                manageRosterLink.NavigateUrl = "ManageRoster.aspx?stid=" + this._StandupTrainingObject.Id.ToString() + "&id=" + idStandupTrainingInstance.ToString();
                manageRosterSpan.Controls.Add(manageRosterLink);

                Image manageRosterIcon = new Image();
                manageRosterIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SESSION_BUTTON, ImageFiles.EXT_PNG);
                manageRosterIcon.AlternateText = _GlobalResources.ManageRoster;
                manageRosterIcon.ToolTip = _GlobalResources.ManageRoster;
                manageRosterLink.Controls.Add(manageRosterIcon);
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedSession_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmActionModal = new ModalPopup("GridConfirmActionModal");

            // set modal properties
            this._GridConfirmActionModal.Type = ModalPopupType.Confirm;
            this._GridConfirmActionModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmActionModal.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmActionModal.HeaderText = _GlobalResources.DeleteSelectedSession_s;
            this._GridConfirmActionModal.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmActionModal.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseSession_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmActionModal.AddControlToBody(body1Wrapper);

            // add modal to container
            this.ActionsPanel.Controls.Add(this._GridConfirmActionModal);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.StandupTrainingSessionsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.StandupTrainingSessionsGrid.Rows[i].FindControl(this.StandupTrainingSessionsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        {
                            int idStandupTrainingInstance = Convert.ToInt32(checkBox.InputAttributes["value"]);

                            // attempt to delete the session from it's corresponding API
                            this._DeleteWebMeeting(idStandupTrainingInstance);

                            // add the record to the delete table
                            recordsToDelete.Rows.Add(idStandupTrainingInstance); 
                        }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.StandupTrainingInstance.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedSession_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoSession_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {                
                // rebind the grid
                this.StandupTrainingSessionsGrid.BindData();
            }
        }
        #endregion

        #region _DeleteWebMeeting
        /// <summary>
        /// Deletes a web meeting registrant from GTW, GTT, or WebEx by connecting to the appropriate API.
        /// </summary>
        /// <param name="idStandupTrainingInstance">the session id</param>
        private void _DeleteWebMeeting(int idStandupTrainingInstance)
        {
            try // this is wrapped in a try/catch because we dont want to die on error because an error here doesn't matter much
            {
                StandupTrainingInstance stiObject = new StandupTrainingInstance(idStandupTrainingInstance);

                bool hasSessionAlreadyOccurred = false;

                if (stiObject.MeetingTimes.Count > 0)
                { hasSessionAlreadyOccurred = ((Library.StandupTrainingInstance.MeetingTimeProperty)(stiObject.MeetingTimes[0])).DtStart < DateTime.UtcNow; }
                else
                { hasSessionAlreadyOccurred = true; }

                if (!hasSessionAlreadyOccurred && stiObject.IntegratedObjectKey != null && stiObject.IntegratedObjectKey > 0)
                {
                    AsentiaAESEncryption cryptoProvider = new AsentiaAESEncryption();
                    string organizerUsername = null;
                    string organizerPassword = null;

                    if (stiObject.IdWebMeetingOrganizer != null && stiObject.IdWebMeetingOrganizer > 0)
                    {
                        WebMeetingOrganizer webMeetingOrganizer = new WebMeetingOrganizer((int)stiObject.IdWebMeetingOrganizer);
                        organizerUsername = webMeetingOrganizer.Username;
                        organizerPassword = cryptoProvider.Decrypt(webMeetingOrganizer.Password);
                    }

                    if (stiObject.Type == StandupTrainingInstance.MeetingType.GoToMeeting) // GTM
                    {
                        if (organizerUsername == null)
                        {
                            GoToMeetingAPI gtmApi = new GoToMeetingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_APPLICATION),
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_USERNAME),
                                                                           cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PASSWORD)),
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN),
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_CONSUMERSECRET));

                            gtmApi.DeleteMeeting(Convert.ToInt64(stiObject.IntegratedObjectKey));
                        }
                        else
                        {
                            GoToMeetingAPI gtmApi = new GoToMeetingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_APPLICATION),
                                                                           organizerUsername,
                                                                           organizerPassword,
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN),
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_CONSUMERSECRET));

                            gtmApi.DeleteMeeting(Convert.ToInt64(stiObject.IntegratedObjectKey));
                        }
                    }
                    else if (stiObject.Type == StandupTrainingInstance.MeetingType.GoToTraining) // GTT
                    {
                        if (organizerUsername == null)
                        {
                            GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                         AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_USERNAME),
                                                                         cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PASSWORD)),
                                                                         AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                            gttApi.DeleteTraining(Convert.ToInt64(stiObject.IntegratedObjectKey));
                        }
                        else
                        {
                            GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                         organizerUsername,
                                                                         organizerPassword,
                                                                         AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                            gttApi.DeleteTraining(Convert.ToInt64(stiObject.IntegratedObjectKey));
                        }
                    }
                    else if (stiObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar) // GTW
                    {
                        if (organizerUsername == null)
                        {
                            GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_USERNAME),
                                                                       cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PASSWORD)),
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                            gtwApi.DeleteWebinar(Convert.ToInt64(stiObject.IntegratedObjectKey));
                        }
                        else
                        {
                            GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                       organizerUsername,
                                                                       organizerPassword,
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                            gtwApi.DeleteWebinar(Convert.ToInt64(stiObject.IntegratedObjectKey));
                        }
                    }
                    else if (stiObject.Type == StandupTrainingInstance.MeetingType.WebEx) // WebEx
                    {
                        if (organizerUsername == null)
                        {
                            WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_USERNAME),
                                                           cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PASSWORD)));

                            wbxApi.DeleteMeeting(Convert.ToInt64(stiObject.IntegratedObjectKey));
                        }
                        else
                        {
                            WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                           organizerUsername,
                                                           organizerPassword);

                            wbxApi.DeleteMeeting(Convert.ToInt64(stiObject.IntegratedObjectKey));
                        }
                    }
                    else
                    { }
                }
            }
            catch // just bury the error
            { }
        }
        #endregion
    }
}
