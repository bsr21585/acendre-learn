﻿// Function: InitializeSortableOnWaitlistGrid
// Initializes jQuery sortable on the waitlist grid so that the waitlist order can be modified.
function InitializeSortableOnWaitlistGrid() {
    $("#WaitlistGrid").sortable({
        items: '.GridDataRow,.GridDataRowAlternate', // attach rows which are not header,footer
        cursor: 'move',        
        stop: function (e, ui) {
            ShowElementsAfterDragging(ui);            
        },
        update: function (e, ui) {
            // we need to change identifiers for the re-located row to an ordinal that is out of
            // range so that re-seeding of ordinals works properly
            var tableId = "WaitlistGrid";
            var currentRowId = ui.item.attr("id");
            var currentRowOrdinal = ui.item.attr("id").replace(tableId + "_GridRow_", "");
            var rowOrdinalRegEx = new RegExp(currentRowOrdinal, "g");
            var index = $("#" + tableId + " .GridDataRow").size() + 1;

            // change row id
            ui.item.attr("id", tableId + "_GridRow_" + index);

            // change row attributes
            $(ui.item).attr("onmouseover", $(ui.item).attr("onmouseover").replace(rowOrdinalRegEx, index));
            $(ui.item).attr("onmouseout", $(ui.item).attr("onmouseout").replace(rowOrdinalRegEx, index));
            $(ui.item).attr("onclick", $(ui.item).attr("onclick").replace(rowOrdinalRegEx, index));

            // change ordinal index for row checkbox                
            $("#" + tableId + "_GridSelectRecord_" + currentRowOrdinal).attr("onclick", $("#" + tableId + "_GridSelectRecord_" + currentRowOrdinal).attr("onclick").replace(rowOrdinalRegEx, index));
            $("#" + tableId + "_GridSelectRecord_" + currentRowOrdinal).attr("name", $("#" + tableId + "_GridSelectRecord_" + currentRowOrdinal).attr("name").replace(rowOrdinalRegEx, index));
            $("#" + tableId + "_GridSelectRecord_" + currentRowOrdinal).attr("id", tableId + "_GridSelectRecord_" + index);

            // change ordinal index for row completion status dropdown                
            $("#" + tableId + "_CompletionStatusDropDown_" + currentRowOrdinal).attr("name", $("#" + tableId + "_CompletionStatusDropDown_" + currentRowOrdinal).attr("name").replace(rowOrdinalRegEx, index));
            $("#" + tableId + "_CompletionStatusDropDown_" + currentRowOrdinal).attr("id", tableId + "_CompletionStatusDropDown_" + index);

            // change ordinal index for score textbox
            $("#" + tableId + "_ScoreTextBox_" + currentRowOrdinal).attr("name", $("#" + tableId + "_ScoreTextBox_" + currentRowOrdinal).attr("name").replace(rowOrdinalRegEx, index));
            $("#" + tableId + "_ScoreTextBox_" + currentRowOrdinal).attr("id", tableId + "_ScoreTextBox_" + index);

            // change ordinal index for promote button
            $("#" + tableId + "_PromoteButton_" + currentRowOrdinal).attr("onclick", $("#" + tableId + "_PromoteButton_" + currentRowOrdinal).attr("onclick").replace(rowOrdinalRegEx, index));
            $("#" + tableId + "_PromoteButton_" + currentRowOrdinal).attr("id", tableId + "_PromoteButton_" + index);            

            // change ordinal index for drop button
            $("#" + tableId + "_DropButton_" + currentRowOrdinal).attr("onclick", $("#" + tableId + "_DropButton_" + currentRowOrdinal).attr("onclick").replace(rowOrdinalRegEx, index));
            $("#" + tableId + "_DropButton_" + currentRowOrdinal).attr("id", tableId + "_DropButton_" + index);

            // re-seed the row ordinals
            ReseedRowOrdinals("WaitlistGrid", true);            
        },
        helper: function (event, ui) {
            HideElementsWhileDragging(ui);
            return ui.clone().appendTo("body"); // this needs to be done so that the page will scroll when a field is dragged downward
        }
    });
}

// Function : HideElementsWhileDragging
// Hides all <td>s, with the exception of the identifier <td> for a waitlist grid row
// when the waitlist grid row is being dragged.
function HideElementsWhileDragging(ui) {
    // loop through children of the row (<td>s) and hide if not the identifier <td>
    ui.children().each(function (index) {
        if (index != 1) {
            $(this).children().hide();
        }
    });
}

// Function : ShowElementsAfterDragging
// Shows the hidden <td>s of a waitlist grid row after the row has stopped being dragged.
function ShowElementsAfterDragging(ui) {
    //finding a grid row tr from object 
    var gridRow = ui.item;

    // loop through children of the row (<td>s) and hide if not the identifier <td>
    gridRow.children().each(function (index) {
        $(this).children().show();
    });
}

// Function AddUsersToEnrolledList
// Adds user(s) to the enrolled grid from the "Add User(s)" select box.
function AddUsersToEnrolledList(hasSessionAlreadyOccurred, enrolledTableId, waitlistTableId) {

    var rowsInTable = $("#" + enrolledTableId).find("tr").length - 1;
    var isGridEmpty = false;

    if (rowsInTable == 1 && $("#" + enrolledTableId + "_GridRow_0").hasClass("GridDataRowEmpty")) {
        rowsInTable = 0;
        isGridEmpty = true;
    }

    if (rowsInTable < parseInt(AvailableSeatsForEnrollment)) {
        var selectedUsers = $("select#SelectManageRosterUsersListBox").val();

        if (selectedUsers == null) {
            return;
        }
        
        if (selectedUsers.length + rowsInTable <= parseInt(AvailableSeatsForEnrollment)) {

            // loop through selected users and attach to grid
            for (var i = 0; i < selectedUsers.length; i++) {

                // get selected user information
                var idUser = $("#SelectManageRosterUsersListBox option[value='" + selectedUsers[i] + "']").val();
                var userName = $("#SelectManageRosterUsersListBox option[value='" + selectedUsers[i] + "']").text();
                var registrantEmail = $("#SelectManageRosterUsersListBox option[value='" + selectedUsers[i] + "']").attr("email");

                // get existing grid row information
                var gridRows = $("#" + enrolledTableId + "_HeaderRow").nextAll(".GridDataRow");
                var gridRowsCount = gridRows.length;

                var newRowOrdinalIndex = gridRowsCount;

                // if the grid is empty ("no records") we need to remove that row, and set the ordinal index to 0
                if (isGridEmpty) {
                    RemoveUserFromList(enrolledTableId, 0, false); // this drops the "no records" row
                    newRowOrdinalIndex = 0;
                }
                
                // select record checkbox
                var selectRecordCheckboxHtml = "<input id=\"" + enrolledTableId + "_GridSelectRecord_" + newRowOrdinalIndex + "\" type=\"checkbox\" name=\"Master$PageContentPlaceholder$" + enrolledTableId + "_GridSelectRecord_" + newRowOrdinalIndex + "\" onclick=\"Grid.ToggleSelectRecord('" + enrolledTableId + "', this, " + newRowOrdinalIndex + ", false);\" value=\"" + idUser + "\" registrantkey=\"\" registrantemail=\"" + registrantEmail + "\" liststatus=\"added\" tabindex=\"0\">";

                // completion status drop down html
                var completionStatusDropDownHtml = "<select name=\"Master$PageContentPlaceholder$" + enrolledTableId + "_CompletionStatusDropDown_" + newRowOrdinalIndex + "\" id=\"" + enrolledTableId + "_CompletionStatusDropDown_" + newRowOrdinalIndex + "\" tabindex \"0\">";
                completionStatusDropDownHtml += "<option value=\"0\" selected=\"selected\">" + LabelUnknown + "</option>";
                completionStatusDropDownHtml += "<option value=\"2\">" + LabelCompleted + "</option>";
                completionStatusDropDownHtml += "<option value=\"3\">" + LabelIncomplete + "</option>";
                completionStatusDropDownHtml += "</select>";

                // score text box html
                var scoreTextBoxHtml = "<input name=\"Master$PageContentPlaceholder$" + enrolledTableId + "_ScoreTextBox_" + newRowOrdinalIndex + "\" type=\"text\" id=\"" + enrolledTableId + "_ScoreTextBox_" + newRowOrdinalIndex + "\" class=\"InputXShort\" tabindex=\"0\">";

                // demote button html
                var demoteButtonHtml = "<img id=\"" + enrolledTableId + "_DemoteButton_" + newRowOrdinalIndex + "\" class=\"SmallIcon\" onclick=\"DemoteUserFromEnrolledList(" + hasSessionAlreadyOccurred + ", '" + enrolledTableId + "', '" + waitlistTableId + "', " + newRowOrdinalIndex + ");\" src=\"" + DemoteImagePath + "\" style=\"cursor:pointer;\">";

                // drop button html
                var dropButtonHtml = "<img id=\"" + enrolledTableId + "_DropButton_" + newRowOrdinalIndex + "\" class=\"SmallIcon\" onclick=\"RemoveUserFromList('" + enrolledTableId + "', " + newRowOrdinalIndex + ", false);\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\">";

                // build row html
                rowClass = "GridDataRow";

                if (gridRowsCount > 0) {
                    var lastRowClass = $(gridRows[gridRowsCount - 1]).attr("class");

                    if (!$(gridRows[gridRowsCount - 1]).hasClass("GridDataRowAlternate")) {
                        rowClass = "GridDataRow GridDataRowAlternate";
                    }
                }
                
                var hideColumnsStyle = ">";

                if (!hasSessionAlreadyOccurred) {
                    hideColumnsStyle = " style=\"display:none;\">";
                }
                
                var tableRowHtml = "<tr id=\"" + enrolledTableId + "_GridRow_" + newRowOrdinalIndex + "\" class=\"" + rowClass + "\" onmouseover=\"Grid.HoverRowOn(this);\" onmouseout=\"Grid.HoverRowOff('" + enrolledTableId + "', this, " + newRowOrdinalIndex + ");\" onclick=\"Grid.ToggleSelectRecord('" + enrolledTableId + "', document.getElementById('" + enrolledTableId + "_GridSelectRecord_" + newRowOrdinalIndex + "'), " + newRowOrdinalIndex + ", true);\" style=\"cursor:pointer;\">";
                tableRowHtml += "<td class=\"GridCheckboxColumn\">" + selectRecordCheckboxHtml + "</td>";
                tableRowHtml += "<td>" + userName + "</td>";
                tableRowHtml += "<td" + hideColumnsStyle + completionStatusDropDownHtml + "</td>";
                tableRowHtml += "<td" + hideColumnsStyle + scoreTextBoxHtml + "</td>";
                tableRowHtml += "<td class=\"centered\">" + demoteButtonHtml + "</td>";
                tableRowHtml += "<td class=\"centered\">" + dropButtonHtml + "</td>";
                tableRowHtml += "</tr>";

                // attach the row
                if (gridRowsCount > 0) {
                    $(gridRows[gridRowsCount - 1]).after(tableRowHtml);
                    isGridEmpty = false;
                }
                else {
                    $("#" + enrolledTableId + "_HeaderRow").after(tableRowHtml);
                    isGridEmpty = false;
                }                

                // remove the user from the select list
                $("#SelectManageRosterUsersListBox option[value='" + selectedUsers[i] + "']").remove();
            }

            // re-seed row ordinals
            ReseedRowOrdinals(enrolledTableId, false);
        }
        else {
            ShowSeatsExceededMessage(true);
        }
    }
    else {
        ShowSeatsExceededMessage(true);
    }
}

// Function: PromoteUserFromWaitList
// Promotes a user from wait list to enrolled list grid. 
function PromoteUserFromWaitList(hasSessionAlreadyOccurred, enrolledTableId, waitlistTableId, rowOrdinal) {    

    var rowsInTable = $("#" + enrolledTableId).find("tr").length - 1;
    var isGridEmpty = false;

    if (rowsInTable == 1 && $("#" + enrolledTableId + "_GridRow_0").hasClass("GridDataRowEmpty")) {
        rowsInTable = 0;
        isGridEmpty = true;
    }

    if (rowsInTable < parseInt(AvailableSeatsForEnrollment)) {                        

        // get existing enrolled grid information
        var gridRows = $("#" + enrolledTableId + "_HeaderRow").nextAll(".GridDataRow");
        var gridRowsCount = gridRows.length;

        var newRowOrdinalIndex = gridRowsCount;

        // if the enrolled grid is empty ("no records") we need to remove that row, and set the ordinal index to 0
        if (isGridEmpty) {
            RemoveUserFromList(enrolledTableId, 0, false); // this drops the "no records" row
            newRowOrdinalIndex = 0;
        }

        // change all information in the promoted row to enrolled grid information (ids, classes, etc)
        var rowOrdinalRegEx = new RegExp(rowOrdinal, "g");
        var waitlistTableIdRegEx = new RegExp(waitlistTableId, "g");

        // row information

        // if the session has occurred, unhide the status and score columns
        if (hasSessionAlreadyOccurred) {
            $("#" + waitlistTableId + "_GridRow_" + rowOrdinal + " td").eq(5).attr("style", "");
            $("#" + waitlistTableId + "_GridRow_" + rowOrdinal + " td").eq(6).attr("style", "");
        }

        // row attributes
        var rowClass = "GridDataRow";

        if (gridRowsCount > 0) {
            var lastRowClass = $(gridRows[gridRowsCount - 1]).attr("class");

            if (!$(gridRows[gridRowsCount - 1]).hasClass("GridDataRowAlternate")) {
                rowClass = "GridDataRow GridDataRowAlternate";
            }
        }

        $('.' + waitlistTableId + 'Ordinal').eq(rowOrdinal).removeClass('WaitlistGridOrdinal').addClass('EnrolledGridOrdinal')
        $("#" + waitlistTableId + "_GridRow_" + rowOrdinal).attr("class", rowClass);
        $("#" + waitlistTableId + "_GridRow_" + rowOrdinal).attr("onmouseover", $("#" + waitlistTableId + "_GridRow_" + rowOrdinal).attr("onmouseover").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(waitlistTableIdRegEx, enrolledTableId));
        $("#" + waitlistTableId + "_GridRow_" + rowOrdinal).attr("onmouseout", $("#" + waitlistTableId + "_GridRow_" + rowOrdinal).attr("onmouseout").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(waitlistTableIdRegEx, enrolledTableId));
        $("#" + waitlistTableId + "_GridRow_" + rowOrdinal).attr("onclick", $("#" + waitlistTableId + "_GridRow_" + rowOrdinal).attr("onclick").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(waitlistTableIdRegEx, enrolledTableId));
        $("#" + waitlistTableId + "_GridRow_" + rowOrdinal).attr("id", enrolledTableId + "_GridRow_" + newRowOrdinalIndex);

        // row checkbox
        $("#" + waitlistTableId + "_GridSelectRecord_" + rowOrdinal).attr("onclick", $("#" + waitlistTableId + "_GridSelectRecord_" + rowOrdinal).attr("onclick").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(waitlistTableIdRegEx, enrolledTableId));
        $("#" + waitlistTableId + "_GridSelectRecord_" + rowOrdinal).attr("name", $("#" + waitlistTableId + "_GridSelectRecord_" + rowOrdinal).attr("name").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(waitlistTableIdRegEx, enrolledTableId));
        $("#" + waitlistTableId + "_GridSelectRecord_" + rowOrdinal).attr("listStatus", "promoted");
        $("#" + waitlistTableId + "_GridSelectRecord_" + rowOrdinal).attr("id", enrolledTableId + "_GridSelectRecord_" + newRowOrdinalIndex);

        // row completion status dropdown                
        $("#" + waitlistTableId + "_CompletionStatusDropDown_" + rowOrdinal).attr("name", $("#" + waitlistTableId + "_CompletionStatusDropDown_" + rowOrdinal).attr("name").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(waitlistTableIdRegEx, enrolledTableId));
        $("#" + waitlistTableId + "_CompletionStatusDropDown_" + rowOrdinal).attr("id", enrolledTableId + "_CompletionStatusDropDown_" + newRowOrdinalIndex);

        // row score textbox
        $("#" + waitlistTableId + "_ScoreTextBox_" + rowOrdinal).attr("name", $("#" + waitlistTableId + "_ScoreTextBox_" + rowOrdinal).attr("name").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(waitlistTableIdRegEx, enrolledTableId));
        $("#" + waitlistTableId + "_ScoreTextBox_" + rowOrdinal).attr("id", enrolledTableId + "_ScoreTextBox_" + newRowOrdinalIndex);

        // change row promote button to demote button        
        $("#" + waitlistTableId + "_PromoteButton_" + rowOrdinal).attr("onclick", "DemoteUserFromEnrolledList(" + hasSessionAlreadyOccurred + ", '" + enrolledTableId + "', '" + waitlistTableId + "', " + newRowOrdinalIndex + ");");
        $("#" + waitlistTableId + "_PromoteButton_" + rowOrdinal).attr("src", DemoteImagePath);
        $("#" + waitlistTableId + "_PromoteButton_" + rowOrdinal).attr("id", enrolledTableId + "_DemoteButton_" + newRowOrdinalIndex);        

        // row drop button
        $("#" + waitlistTableId + "_DropButton_" + rowOrdinal).attr("onclick", $("#" + waitlistTableId + "_DropButton_" + rowOrdinal).attr("onclick").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(waitlistTableIdRegEx, enrolledTableId));
        $("#" + waitlistTableId + "_DropButton_" + rowOrdinal).attr("id", enrolledTableId + "_DropButton_" + newRowOrdinalIndex);

        // attach the row        
        var promotedRow = $("#" + enrolledTableId + "_GridRow_" + newRowOrdinalIndex);

        if (gridRowsCount > 0) {
            $(gridRows[gridRowsCount - 1]).after(promotedRow);
        }
        else {
            $("#" + enrolledTableId + "_HeaderRow").after(promotedRow);
        }

        // if the wait list grid doesn't have any rows, add the "empty" row
        if ($("#" + waitlistTableId + "_HeaderRow").nextAll(".GridDataRow").length == 0) {

            waitlistEmptyRowHtml = "<tr id=\"" + waitlistTableId + "_GridRow_0\" class=\"GridDataRowEmpty\"><td colspan=\"4\">" + LabelNoRecordsFound + "</td></tr>";
            $("#" + waitlistTableId + "_HeaderRow").after(waitlistEmptyRowHtml);
        }

        // re-seed row ordinals for both grids
        ReseedRowOrdinals(enrolledTableId, false);
        ReseedRowOrdinals(waitlistTableId, true);
    }
    else {
        ShowSeatsExceededMessage(true);
    }
}

// Function: DemoteUserFromEnrolledList
// Demotes a user from enrolled list to wait list grid. 
function DemoteUserFromEnrolledList(hasSessionAlreadyOccurred, enrolledTableId, waitlistTableId, rowOrdinal) {

    var rowsInTable = $("#" + waitlistTableId).find("tr").length - 1;
    var isGridEmpty = false;

    if (rowsInTable == 1 && $("#" + waitlistTableId + "_GridRow_0").hasClass("GridDataRowEmpty")) {
        rowsInTable = 0;
        isGridEmpty = true;
    }

    if (rowsInTable < parseInt(AvailableSeatsForWaitingList)) {

        // get existing waitlist grid information
        var gridRows = $("#" + waitlistTableId + "_HeaderRow").nextAll(".GridDataRow");
        var gridRowsCount = gridRows.length;

        var newRowOrdinalIndex = gridRowsCount;

        // if the waitlist grid is empty ("no records") we need to remove that row, and set the ordinal index to 0
        if (isGridEmpty) {
            RemoveUserFromList(waitlistTableId, 0, true); // this drops the "no records" row
            newRowOrdinalIndex = 0;
        }

        // change all information in the demoted row to waitlist grid information (ids, classes, etc)
        var rowOrdinalRegEx = new RegExp(rowOrdinal, "g");
        var enrolledTableIdRegEx = new RegExp(enrolledTableId, "g");

        // row information

        // if the session has occurred, hide the status and score columns
        if (hasSessionAlreadyOccurred) {
            $("#" + enrolledTableId + "_GridRow_" + rowOrdinal + " td").eq(5).attr("style", "display:none;");
            $("#" + enrolledTableId + "_GridRow_" + rowOrdinal + " td").eq(6).attr("style", "display:none;");
        }

        // row attributes
        var rowClass = "GridDataRow";

        if (gridRowsCount > 0) {
            var lastRowClass = $(gridRows[gridRowsCount - 1]).attr("class");

            if (!$(gridRows[gridRowsCount - 1]).hasClass("GridDataRowAlternate")) {
                rowClass = "GridDataRow GridDataRowAlternate";
            }
        }

        $('.' + enrolledTableId + 'Ordinal').eq(rowOrdinal).removeClass('EnrolledGridOrdinal').addClass('WaitlistGridOrdinal')
        $("#" + enrolledTableId + "_GridRow_" + rowOrdinal).attr("class", rowClass);
        $("#" + enrolledTableId + "_GridRow_" + rowOrdinal).attr("onmouseover", $("#" + enrolledTableId + "_GridRow_" + rowOrdinal).attr("onmouseover").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(enrolledTableIdRegEx, waitlistTableId));
        $("#" + enrolledTableId + "_GridRow_" + rowOrdinal).attr("onmouseout", $("#" + enrolledTableId + "_GridRow_" + rowOrdinal).attr("onmouseout").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(enrolledTableIdRegEx, waitlistTableId));
        $("#" + enrolledTableId + "_GridRow_" + rowOrdinal).attr("onclick", $("#" + enrolledTableId + "_GridRow_" + rowOrdinal).attr("onclick").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(enrolledTableIdRegEx, waitlistTableId));
        $("#" + enrolledTableId + "_GridRow_" + rowOrdinal).attr("id", waitlistTableId + "_GridRow_" + newRowOrdinalIndex);

        // row checkbox
        $("#" + enrolledTableId + "_GridSelectRecord_" + rowOrdinal).attr("onclick", $("#" + enrolledTableId + "_GridSelectRecord_" + rowOrdinal).attr("onclick").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(enrolledTableIdRegEx, waitlistTableId));
        $("#" + enrolledTableId + "_GridSelectRecord_" + rowOrdinal).attr("name", $("#" + enrolledTableId + "_GridSelectRecord_" + rowOrdinal).attr("name").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(enrolledTableIdRegEx, waitlistTableId));
        $("#" + enrolledTableId + "_GridSelectRecord_" + rowOrdinal).attr("listStatus", "demoted");
        $("#" + enrolledTableId + "_GridSelectRecord_" + rowOrdinal).attr("id", waitlistTableId + "_GridSelectRecord_" + newRowOrdinalIndex);

        // row completion status dropdown                
        $("#" + enrolledTableId + "_CompletionStatusDropDown_" + rowOrdinal).attr("name", $("#" + enrolledTableId + "_CompletionStatusDropDown_" + rowOrdinal).attr("name").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(enrolledTableIdRegEx, waitlistTableId));
        $("#" + enrolledTableId + "_CompletionStatusDropDown_" + rowOrdinal).attr("id", waitlistTableId + "_CompletionStatusDropDown_" + newRowOrdinalIndex);

        // row score textbox
        $("#" + enrolledTableId + "_ScoreTextBox_" + rowOrdinal).attr("name", $("#" + enrolledTableId + "_ScoreTextBox_" + rowOrdinal).attr("name").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(enrolledTableIdRegEx, waitlistTableId));
        $("#" + enrolledTableId + "_ScoreTextBox_" + rowOrdinal).attr("id", waitlistTableId + "_ScoreTextBox_" + newRowOrdinalIndex);

        // change row demote button to promote button        
        $("#" + enrolledTableId + "_DemoteButton_" + rowOrdinal).attr("onclick", "PromoteUserFromWaitList(" + hasSessionAlreadyOccurred + ", '" + enrolledTableId + "', '" + waitlistTableId + "', " + newRowOrdinalIndex + ");");
        $("#" + enrolledTableId + "_DemoteButton_" + rowOrdinal).attr("src", PromoteImagePath);
        $("#" + enrolledTableId + "_DemoteButton_" + rowOrdinal).attr("id", waitlistTableId + "_PromoteButton_" + newRowOrdinalIndex);

        // row drop button
        $("#" + enrolledTableId + "_DropButton_" + rowOrdinal).attr("onclick", $("#" + enrolledTableId + "_DropButton_" + rowOrdinal).attr("onclick").replace(rowOrdinalRegEx, newRowOrdinalIndex).replace(enrolledTableIdRegEx, waitlistTableId));
        $("#" + enrolledTableId + "_DropButton_" + rowOrdinal).attr("id", waitlistTableId + "_DropButton_" + newRowOrdinalIndex);

        // attach the row        
        var demotedRow = $("#" + waitlistTableId + "_GridRow_" + newRowOrdinalIndex);

        if (gridRowsCount > 0) {
            $(gridRows[gridRowsCount - 1]).after(demotedRow);
        }
        else {
            $("#" + waitlistTableId + "_HeaderRow").after(demotedRow);
        }

        // if the enrolled list grid doesn't have any rows, add the "empty" row
        if ($("#" + enrolledTableId + "_HeaderRow").nextAll(".GridDataRow").length == 0) {
            var colspan = "4";

            if (hasSessionAlreadyOccurred) {
                colspan = "6";
            }

            enrolledEmptyRowHtml = "<tr id=\"" + enrolledTableId + "_GridRow_0\" class=\"GridDataRowEmpty\"><td colspan=\"" + colspan + "\">" + LabelNoRecordsFound + "</td></tr>";
            $("#" + enrolledTableId + "_HeaderRow").after(enrolledEmptyRowHtml);
        }

        // re-seed row ordinals for both grids
        ReseedRowOrdinals(enrolledTableId, false);
        ReseedRowOrdinals(waitlistTableId, true);
    }
    else {
        ShowSeatsExceededMessage(false);
    }
}

// Function: ShowSeatsExceededMessage
// Shows the message alerts the user that the number of seats is being exceeded.
function ShowSeatsExceededMessage(isEnrolledGrid) {
    // Hide the batch update modal
    $("#BatchUpdateModalPopupModalPopupExtender_backgroundElement").hide();
    $("#BatchUpdateModalPopup").hide();

    if (isEnrolledGrid) {
        $("#" + EnrolledListSeatsExceededPanelId).show();
        $("#" + WaitlistSeatsExceededPanelId).hide();
        $("#" + LaunchSeatsExceededButtonId).click();
    }
    else {
        $("#" + WaitlistSeatsExceededPanelId).show();
        $("#" + EnrolledListSeatsExceededPanelId).hide();        
        $("#" + LaunchSeatsExceededButtonId).click();
    }
}

// Function: RemoveUserFromList
// Removes a user from either the enrolled grid or waitlist grid.
function RemoveUserFromList(tableId, rowOrdinal, logDroppedRegistrantKey) {

    // if we are logging the registrant key for the dropped user (because we need to delete via a web meeting api), log it
    if (logDroppedRegistrantKey) {
        var registrantKey = $("#" + tableId + "_GridSelectRecord_" + rowOrdinal).attr("registrantkey");

        if (registrantKey != null && registrantKey != "") {
            var droppedRegistrantKeyData = $("#DroppedRegistrantKeys").val();

            if (droppedRegistrantKeyData == "") {
                $("#DroppedRegistrantKeys").val(registrantKey);
            }
            else {
                $("#DroppedRegistrantKeys").val(droppedRegistrantKeyData + "|" + registrantKey);
            }
        }            
    }

    // drop the row
    $("#" + tableId + "_GridRow_" + rowOrdinal).remove();

    // re-seed row ordinals
    if (tableId == "EnrolledGrid") {
        ReseedRowOrdinals(tableId, false);
    }
    else {
        ReseedRowOrdinals(tableId, true);
    }
}

// Function: ReseedRowOrdinals
// Re-seeds the row numbers in the grid, this is needed because we need to maintain
// order when things are added and dropped.
function ReseedRowOrdinals(tableId, hasPromoteButton) {
    $("#" + tableId + " .GridDataRow").each(function (index, obj) {
        var currentRowId = this.id;
        var currentRowOrdinal = this.id.replace(tableId + "_GridRow_", "");
        var rowOrdinalRegEx = new RegExp(currentRowOrdinal, "g");

        // change row id
        this.id = tableId + "_GridRow_" + index;

        // change row attributes
        $(this).attr("onmouseover", $(this).attr("onmouseover").replace(rowOrdinalRegEx, index));
        $(this).attr("onmouseout", $(this).attr("onmouseout").replace(rowOrdinalRegEx, index));
        $(this).attr("onclick", $(this).attr("onclick").replace(rowOrdinalRegEx, index));        

        // change ordinal index for row checkbox                
        $("#" + tableId + "_GridSelectRecord_" + currentRowOrdinal).attr("onclick", $("#" + tableId + "_GridSelectRecord_" + currentRowOrdinal).attr("onclick").replace(rowOrdinalRegEx, index));
        $("#" + tableId + "_GridSelectRecord_" + currentRowOrdinal).attr("name", $("#" + tableId + "_GridSelectRecord_" + currentRowOrdinal).attr("name").replace(rowOrdinalRegEx, index));
        $("#" + tableId + "_GridSelectRecord_" + currentRowOrdinal).attr("id", tableId + "_GridSelectRecord_" + index);

        // change ordinal index for row completion status dropdown                
        $("#" + tableId + "_CompletionStatusDropDown_" + currentRowOrdinal).attr("name", $("#" + tableId + "_CompletionStatusDropDown_" + currentRowOrdinal).attr("name").replace(rowOrdinalRegEx, index));
        $("#" + tableId + "_CompletionStatusDropDown_" + currentRowOrdinal).attr("id", tableId + "_CompletionStatusDropDown_" + index);

        // change ordinal index for score textbox
        $("#" + tableId + "_ScoreTextBox_" + currentRowOrdinal).attr("name", $("#" + tableId + "_ScoreTextBox_" + currentRowOrdinal).attr("name").replace(rowOrdinalRegEx, index));
        $("#" + tableId + "_ScoreTextBox_" + currentRowOrdinal).attr("id", tableId + "_ScoreTextBox_" + index);

        // change ordinal index for promote/demote button
        if (hasPromoteButton) {
            $("#" + tableId + "_PromoteButton_" + currentRowOrdinal).attr("onclick", $("#" + tableId + "_PromoteButton_" + currentRowOrdinal).attr("onclick").replace(rowOrdinalRegEx, index));
            $("#" + tableId + "_PromoteButton_" + currentRowOrdinal).attr("id", tableId + "_PromoteButton_" + index);
        }
        else {
            $("#" + tableId + "_DemoteButton_" + currentRowOrdinal).attr("onclick", $("#" + tableId + "_DemoteButton_" + currentRowOrdinal).attr("onclick").replace(rowOrdinalRegEx, index));
            $("#" + tableId + "_DemoteButton_" + currentRowOrdinal).attr("id", tableId + "_DemoteButton_" + index);
        }

        // change ordinal index for drop button
        $("#" + tableId + "_DropButton_" + currentRowOrdinal).attr("onclick", $("#" + tableId + "_DropButton_" + currentRowOrdinal).attr("onclick").replace(rowOrdinalRegEx, index));
        $("#" + tableId + "_DropButton_" + currentRowOrdinal).attr("id", tableId + "_DropButton_" + index);
    });

    $('.RowOrdinalPanel').attr("style", "");
    var count = $('.' + tableId + 'Ordinal').length;

    //rearrange the labels of number order
    for (i = 0; i < count; i++) {    
        $('.' + tableId + 'Ordinal').eq(i).text(i + 1 + ". ");
    }
    $('.RowOrdinalPanel').attr("style", "display:none;");
}

// Function: MarkSelectedAs
// Marks the completion status drop downs for selected rows as "Complete", "Incomplete", or "Unknown"
function MarkSelectedAs(enrolledTableId) {
    // get the selected value
    var selectedValue = $("#MarkSelectedAsDropDown").val();

    if (selectedValue == "-1") {
        return;
    }

    // loop through each datarow of the enrolled grid and mark the completion status
    // of the selected rows as the selected value
    var gridRows = $("#" + enrolledTableId + "_HeaderRow").nextAll(".GridDataRow");
    var gridRowsCount = gridRows.length;

    for (var i = 0; i < gridRowsCount; i++) {
        // mark as selected value
        if ($("#" + enrolledTableId + "_GridSelectRecord_" + i).is(":checked")) {
            $("#" + enrolledTableId + "_CompletionStatusDropDown_" + i).val(selectedValue);
        }

        // un-check the box
        $("#" + enrolledTableId + "_GridSelectRecord_" + i).attr("checked", false);
        Grid.ToggleSelectRecord(enrolledTableId, $("#" + enrolledTableId + "_GridSelectRecord_" + i), i, false);        
    }

    // reset the selected value to -1
    $("#MarkSelectedAsDropDown").val("-1");
}

// Object: StandupTrainingToUserLinkDataObject
// JSON object structure for storing roster data.
function StandupTrainingToUserLinkDataObject() {
    this.OrdinalIndex = null;
    this.IdUser = null;
    this.RegistrantKey = null;  // this is an Int64, but needs to be treated as a string in JSON because JS does not have the concept of a long int
    this.RegistrantEmail = null;
    this.CompletionStatus = null;
    this.Score = null;    
    this.IsWaitList = null;
    this.ListStatus = null;
}

// Function: CompileJSONDataForSave
// Compiles the data from the roster grids into a JSON object and puts the object into a hidden field.
function CompileJSONDataForSave() {
    
    enrolledAndWaitListDataArray = new Array();
    var e = 1;
    var w = 1;
    
    $(".GridDataRow").each(function (index, obj) {
        
        // make sure this grid row is not in the formatting instructions table
        if ($(this).attr("id").indexOf("FormattingInstructions") == -1) {
            
            var dataRecord = new StandupTrainingToUserLinkDataObject();
            var rowIdOrdinal = $(this).attr("id").replace("EnrolledGrid", "").replace("WaitlistGrid", "").replace("_GridRow_", "");
            var gridId = $(this).attr("id").replace(rowIdOrdinal, "").replace("_GridRow_", "");
            var idUser = $("#" + gridId + "_GridSelectRecord_" + rowIdOrdinal).val();
            var registrantKey = $("#" + gridId + "_GridSelectRecord_" + rowIdOrdinal).attr("registrantkey");
            var registrantEmail = $("#" + gridId + "_GridSelectRecord_" + rowIdOrdinal).attr("registrantemail");
            var listStatus = $("#" + gridId + "_GridSelectRecord_" + rowIdOrdinal).attr("liststatus");
            dataRecord.IdUser = idUser;
            dataRecord.RegistrantKey = registrantKey;
            dataRecord.RegistrantEmail = registrantEmail;
            dataRecord.CompletionStatus = $("#" + gridId + "_CompletionStatusDropDown_" + rowIdOrdinal).val();
            dataRecord.Score = $("#" + gridId + "_ScoreTextBox_" + rowIdOrdinal).val();

            if (gridId == "EnrolledGrid") {
                dataRecord.OrdinalIndex = e;
                dataRecord.IsWaitList = false;

                e++;
            }
            else {
                dataRecord.OrdinalIndex = w;
                dataRecord.IsWaitList = true;

                w++;
            }

            dataRecord.ListStatus = listStatus;

            enrolledAndWaitListDataArray.push(dataRecord);
        }
    });
   
    var stringifiedData = JSON.stringify(enrolledAndWaitListDataArray);
    $("#EnrolledAndWaitListJsonData").val(stringifiedData);
   
}

// Function: SynchronizeWithWebMeetingApi
// Performs AJAX call to synchronize attendee information with the respective provider's API.
function SynchronizeWithWebMeetingApi(idStandupTrainingInstance, integratedObjectKey, integratedObjectType) {    
    
    // launch modal, show loading panel
    $("#HiddenButtonForSynchronizationSummaryModal").click();
    $("#SynchronizationSummaryModalModalPopupBody").hide();
    $("#SynchronizationSummaryModalModalPopupPostbackLoadingPlaceholder").show();
    SetReCenterOnShownForModal_SynchronizationSummaryModalModalPopupExtender();

    // call the service
    $.ajax({
        type: "POST",
        url: "ManageRoster.aspx/SynchronizeWithWebMeetingApi",
        data: "{idStandupTrainingInstance: " + idStandupTrainingInstance + ", integratedObjectKey: \"" + integratedObjectKey + "\", integratedObjectType: \"" + integratedObjectType + "\"}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSynchronizeSuccess,
        failure: function (response) {
            // show failed wrapper panel, hide others
            $("#SynchronizationSummaryModal_NoUsersWrapperPanel").hide();
            $("#SynchronizationSummaryModal_SuccessWrapperPanel").hide();
            $("#SynchronizationSummaryModal_FailedWrapperPanel").show();            

            // put the error message in the failed data panel
            $("#SynchronizationSummaryModal_FailedDataPanel").html(response.d);

            // hide loading, show body
            $("#SynchronizationSummaryModalModalPopupPostbackLoadingPlaceholder").hide();
            $("#SynchronizationSummaryModalModalPopupBody").show();
            SetReCenterOnShownForModal_SynchronizationSummaryModalModalPopupExtender();
        },
        error: function (response) {
            // show failed wrapper panel, hide others
            $("#SynchronizationSummaryModal_NoUsersWrapperPanel").hide();
            $("#SynchronizationSummaryModal_SuccessWrapperPanel").hide();
            $("#SynchronizationSummaryModal_FailedWrapperPanel").show();            

            // put the error message in the failed data panel
            $("#SynchronizationSummaryModal_FailedDataPanel").html(response.d);

            // hide loading, show body
            $("#SynchronizationSummaryModalModalPopupPostbackLoadingPlaceholder").hide();
            $("#SynchronizationSummaryModalModalPopupBody").show();
            SetReCenterOnShownForModal_SynchronizationSummaryModalModalPopupExtender();
        }
    });
}

// Function: OnSynchronizeSuccess
// Handles the return of the synchronize AJAX call. Loops through returned list of attendees and marks them as completed. 
function OnSynchronizeSuccess(response) {
    var responseObject = response.d;
    var completedSelectValue = 2; // 2 is the value of "completed"

    if (responseObject.actionSuccessful) {

        if (responseObject.attendees != null) {

            // go through the attendee list and find the attendees in the enrolled grid
            for (var i = 0; i < responseObject.attendees.length; i++) {
                // lets go through all the grid rows and match them up from the attendee list
                var gridRows = $("#EnrolledGrid_HeaderRow").nextAll(".GridDataRow");

                for (var j = 0; j < gridRows.length; j++) {
                    var recordCheckboxId = $(gridRows[j]).attr("id").replace("_GridRow_", "_GridSelectRecord_"); // the checkbox is where we store values we need in attributes
                    var recordCompletionDropDownId = $(gridRows[j]).attr("id").replace("_GridRow_", "_CompletionStatusDropDown_");
                    var recordRegistrantKey = $("#" + recordCheckboxId).attr("registrantkey");
                    var recordRegistrantEmail = $("#" + recordCheckboxId).attr("registrantemail");

                    // if either the key or email address matches in our attendee list, mark as complete and synched, then break this loop
                    if ((responseObject.attendees[i].key == recordRegistrantKey || responseObject.attendees[i].email == recordRegistrantEmail) && responseObject.attendees[i].allMeetingTimesAttended == true) {
                        if (responseObject.attendees[i].duration != null && responseObject.attendees[i].duration != "0") {
                            $("#" + recordCompletionDropDownId).val(completedSelectValue);
                        }
                        responseObject.attendees[i].isSynchronized = true;
                        break;
                    }
                }
            }

            // do interface stuff for synched attendees

            // html for synchronized attendees
            var htmlForSynchronized = "";

            for (var i = 0; i < responseObject.attendees.length; i++) {
                if (responseObject.attendees[i].isSynchronized) {
                    if (responseObject.attendees[i].duration != null && responseObject.attendees[i].duration != "0") {
                        htmlForSynchronized += "<div>" + responseObject.attendees[i].name + " (" + responseObject.attendees[i].email + ") - " + responseObject.attendees[i].duration + "</div>"
                    }                    
                }
            }

            // html for non-synchronized attendees
            var htmlForNonSynchronized = "";

            for (var i = 0; i < responseObject.attendees.length; i++) {
                if (!responseObject.attendees[i].isSynchronized) {
                    if (responseObject.attendees[i].duration != null && responseObject.attendees[i].duration != "0") {
                        htmlForNonSynchronized += "<div>" + responseObject.attendees[i].name + " (" + responseObject.attendees[i].email + ") - " + responseObject.attendees[i].duration + "</div>"
                    }                    
                }
            }

            // show success wrapper panel, hide others
            $("#SynchronizationSummaryModal_FailedWrapperPanel").hide();
            $("#SynchronizationSummaryModal_NoUsersWrapperPanel").hide();
            $("#SynchronizationSummaryModal_SuccessWrapperPanel").show();

            // put the data in the data panels
            $("#SynchronizationSummaryModal_SuccessMarkedDataPanel").html(htmlForSynchronized);
            $("#SynchronizationSummaryModal_SuccessNotMarkedDataPanel").html(htmlForNonSynchronized);

            // hide individual panels based on whether or not there is data
            if (htmlForSynchronized == "") {
                $("#SynchronizationSummaryModal_SuccessMarkedMessagePanel").hide();
                $("#SynchronizationSummaryModal_SuccessMarkedDataPanel").hide();
            }

            if (htmlForNonSynchronized == "") {
                $("#SynchronizationSummaryModal_SuccessNotMarkedMessagePanel").hide();
                $("#SynchronizationSummaryModal_SuccessNotMarkedDataPanel").hide();
            }
        }
        else { // there were no attendees to synchronize

            // show no users wrapper panel, hide others
            $("#SynchronizationSummaryModal_FailedWrapperPanel").hide();
            $("#SynchronizationSummaryModal_SuccessWrapperPanel").hide();
            $("#SynchronizationSummaryModal_NoUsersWrapperPanel").show();            
        }
    }
    else { // there is an exception, action was not successful

        // show failed wrapper panel, hide others
        $("#SynchronizationSummaryModal_NoUsersWrapperPanel").hide();
        $("#SynchronizationSummaryModal_SuccessWrapperPanel").hide();
        $("#SynchronizationSummaryModal_FailedWrapperPanel").show();

        // put the error message in the failed data panel
        $("#SynchronizationSummaryModal_FailedDataPanel").html(responseObject.exception);
    }

    // finally, show the modal body    
    $("#SynchronizationSummaryModalModalPopupPostbackLoadingPlaceholder").hide();
    $("#SynchronizationSummaryModalModalPopupBody").show();
    SetReCenterOnShownForModal_SynchronizationSummaryModalModalPopupExtender();
}

// function: BatchUserUpdate
function BatchUserUpdate(idStandupTrainingInstance) {
    // get the saved filename of the uploaded file
    var savedFilename = $("#BatchUpdateUploader_UploadHiddenField").val();

    // show modal loading panel
    $("#BatchUpdateModalPopupModalPopupBody").hide();
    $("#BatchUpdateModalPopupModalPopupPostbackLoadingPlaceholder").show();

    // call the service
    $.ajax({
        type: "POST",
        url: "ManageRoster.aspx/BatchUserUpdate",
        data: "{idStandupTrainingInstance: " + idStandupTrainingInstance + ", savedFilename: '" + savedFilename + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnBatchUpdateSuccess,
        failure: function (response) {
            $('#BatchUploadError').val(responseObject.exception);
            $('#HiddenFailureButton').click();
        },
        error: function (response) {
            $('#BatchUploadError').val(responseObject.exception);
            $('#HiddenFailureButton').click();
        }
    });
}

function OnBatchUpdateSuccess(response) {
    
    var responseObject = response.d;
    var unknownSelectValue = 0; // 0 is the value of "unknown"
    var completedSelectValue = 2; // 2 is the value of "completed"
    var incompleteSelectValue = 3; // 3 is the value of "incomplete"
        
    if (responseObject.actionSuccessful) {
        
        if (responseObject.attendees != null) {
            
            // go through the attendee list and find the attendees in the enrolled grid
            for (var i = 0; i < responseObject.attendees.length; i++) {
                var idUser = responseObject.attendees[i].idUser;
                var completionStatus = responseObject.attendees[i].completionStatus;
                var score = responseObject.attendees[i].score;
                var username = responseObject.attendees[i].username;
                    
                // if the user id = 0, that means we need to update the row in the enrollment table
                
                if (idUser == 0) {
              
                    // lets go through all the grid rows and match them up from the attendee list
                    var gridRows = $("#EnrolledGrid_HeaderRow").nextAll(".GridDataRow");
                
                    for (var j = 0; j < gridRows.length; j++) {
                        var recordCheckboxId = $(gridRows[j]).attr("id").replace("_GridRow_", "_GridSelectRecord_"); // the checkbox is where we store values we need in attributes
                        var recordCompletionDropDownId = $(gridRows[j]).attr("id").replace("_GridRow_", "_CompletionStatusDropDown_");
                        var scoreTextBoxId = $(gridRows[j]).attr("id").replace("_GridRow_", "_ScoreTextBox_");
                        var recordRegistrantUsername = $("#" + recordCheckboxId).attr("username");

                        // if the username matches in our attendee list, mark the completion status and score in the grid
                        if (username == recordRegistrantUsername) {
                            if (completionStatus == 'Completed') {
                                $("#" + recordCompletionDropDownId).val(completedSelectValue);
                            } else if (completionStatus == 'Incomplete') {
                                $("#" + recordCompletionDropDownId).val(incompleteSelectValue);
                            } else {
                                $("#" + recordCompletionDropDownId).val(unknownSelectValue);
                            }

                            if (score != "") {
                                $("#" + scoreTextBoxId).val(score);
                            }
                            break;
                        }
                    }
                } else {
                    
                    // add new users to the enrolled list
                    AddUsersToEnrolledListFromBatchUpdate(responseObject.hasSessionAlreadyOccurred, "EnrolledGrid", "WaitlistGrid", responseObject.attendees[i]);
                }
            }

        }
        $('#HiddenSuccessButton').click();
        
    }
    else { // there is an exception, action was not successful

        $('#BatchUploadError').val(responseObject.exception);
        $('#HiddenFailureButton').click();
            
    }
}

// Function AddUsersToEnrolledListFromBatchUpdate
// Adds user(s) to the enrolled grid from the batch update function.
function AddUsersToEnrolledListFromBatchUpdate(hasSessionAlreadyOccurred, enrolledTableId, waitlistTableId, attendee) {
    
    var idUser = attendee.idUser;
    var userName = attendee.username;
    var completionStatus = attendee.completionStatus;
    var score = attendee.score;
    var registrantEmail = attendee.email;
    var displayName = attendee.displayName;

    var rowsInTable = $("#" + enrolledTableId).find("tr").length - 1;
    var isGridEmpty = false;

    if (rowsInTable == 1 && $("#" + enrolledTableId + "_GridRow_0").hasClass("GridDataRowEmpty")) {
        rowsInTable = 0;
        isGridEmpty = true;
    }
    
    if (rowsInTable + 1 <= parseInt(AvailableSeatsForEnrollment)) {

        // get existing grid row information
        var gridRows = $("#" + enrolledTableId + "_HeaderRow").nextAll(".GridDataRow");
        var gridRowsCount = gridRows.length;

        var newRowOrdinalIndex = gridRowsCount;

        // if the grid is empty ("no records") we need to remove that row, and set the ordinal index to 0
        if (isGridEmpty) {
            RemoveUserFromList(enrolledTableId, 0, false); // this drops the "no records" row
            newRowOrdinalIndex = 0;
        }

        // select record checkbox
        var selectRecordCheckboxHtml = "<input id=\"" + enrolledTableId + "_GridSelectRecord_" + newRowOrdinalIndex + "\" type=\"checkbox\" name=\"Master$PageContentPlaceholder$" + enrolledTableId + "_GridSelectRecord_" + newRowOrdinalIndex + "\" onclick=\"Grid.ToggleSelectRecord('" + enrolledTableId + "', this, " + newRowOrdinalIndex + ", false);\" value=\"" + idUser + "\" registrantkey=\"\" registrantemail=\"" + registrantEmail + "\" liststatus=\"added\" tabindex=\"0\">";
                
        // completion status drop down html
        var completionStatusDropDownHtml = "<select name=\"Master$PageContentPlaceholder$" + enrolledTableId + "_CompletionStatusDropDown_" + newRowOrdinalIndex + "\" id=\"" + enrolledTableId + "_CompletionStatusDropDown_" + newRowOrdinalIndex + "\" tabindex \"0\">";
        if (completionStatus == "Completed") { // completed
            completionStatusDropDownHtml += "<option value=\"0\">" + LabelUnknown + "</option>";
            completionStatusDropDownHtml += "<option value=\"2\" selected=\"selected\">" + LabelCompleted + "</option>";
            completionStatusDropDownHtml += "<option value=\"3\">" + LabelIncomplete + "</option>";
        } else if (completionStatus == "Incomplete") { // incomplete
            completionStatusDropDownHtml += "<option value=\"0\">" + LabelUnknown + "</option>";
            completionStatusDropDownHtml += "<option value=\"2\">" + LabelCompleted + "</option>";
            completionStatusDropDownHtml += "<option value=\"3\" selected=\"selected\">" + LabelIncomplete + "</option>";
        } else { // unknown completion
            completionStatusDropDownHtml += "<option value=\"0\" selected=\"selected\">" + LabelUnknown + "</option>";
            completionStatusDropDownHtml += "<option value=\"2\">" + LabelCompleted + "</option>";
            completionStatusDropDownHtml += "<option value=\"3\">" + LabelIncomplete + "</option>";
        }
        completionStatusDropDownHtml += "</select>";
        // score text box html
        var scoreTextBoxHtml = "<input name=\"Master$PageContentPlaceholder$" + enrolledTableId + "_ScoreTextBox_" + newRowOrdinalIndex + "\" type=\"text\" id=\"" + enrolledTableId + "_ScoreTextBox_" + newRowOrdinalIndex + "\" class=\"InputXShort\" tabindex=\"0\" value=\"" + score + "\">";

        // demote button html
        var demoteButtonHtml = "<img id=\"" + enrolledTableId + "_DemoteButton_" + newRowOrdinalIndex + "\" class=\"SmallIcon\" onclick=\"DemoteUserFromEnrolledList(" + hasSessionAlreadyOccurred + ", '" + enrolledTableId + "', '" + waitlistTableId + "', " + newRowOrdinalIndex + ");\" src=\"" + DemoteImagePath + "\" style=\"cursor:pointer;\">";

        // drop button html
        var dropButtonHtml = "<img id=\"" + enrolledTableId + "_DropButton_" + newRowOrdinalIndex + "\" class=\"SmallIcon\" onclick=\"RemoveUserFromList('" + enrolledTableId + "', " + newRowOrdinalIndex + ", false);\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\">";

        // build row html
        rowClass = "GridDataRow";

        if (gridRowsCount > 0) {
            var lastRowClass = $(gridRows[gridRowsCount - 1]).attr("class");

            if (!$(gridRows[gridRowsCount - 1]).hasClass("GridDataRowAlternate")) {
                rowClass = "GridDataRow GridDataRowAlternate";
            }
        }

        var hideColumnsStyle = ">";

        if (!hasSessionAlreadyOccurred) {
            hideColumnsStyle = " style=\"display:none;\">";
        }

        var tableRowHtml = "<tr id=\"" + enrolledTableId + "_GridRow_" + newRowOrdinalIndex + "\" class=\"" + rowClass + "\" onmouseover=\"Grid.HoverRowOn(this);\" onmouseout=\"Grid.HoverRowOff('" + enrolledTableId + "', this, " + newRowOrdinalIndex + ");\" onclick=\"Grid.ToggleSelectRecord('" + enrolledTableId + "', document.getElementById('" + enrolledTableId + "_GridSelectRecord_" + newRowOrdinalIndex + "'), " + newRowOrdinalIndex + ", true);\" style=\"cursor:pointer;\">";
        tableRowHtml += "<td class=\"GridCheckboxColumn\">" + selectRecordCheckboxHtml + "</td>";
        tableRowHtml += "<td>" + displayName + " (" + userName + ")</td>";
        tableRowHtml += "<td" + hideColumnsStyle + completionStatusDropDownHtml + "</td>";
        tableRowHtml += "<td" + hideColumnsStyle + scoreTextBoxHtml + "</td>";
        tableRowHtml += "<td class=\"centered\">" + demoteButtonHtml + "</td>";
        tableRowHtml += "<td class=\"centered\">" + dropButtonHtml + "</td>";
        tableRowHtml += "</tr>";

        // attach the row
        if (gridRowsCount > 0) {
            $(gridRows[gridRowsCount - 1]).after(tableRowHtml);
            isGridEmpty = false;
        }
        else {
            $("#" + enrolledTableId + "_HeaderRow").after(tableRowHtml);
            isGridEmpty = false;
        }

        // re-seed row ordinals
        ReseedRowOrdinals(enrolledTableId, false);
    }
    else {
        ShowSeatsExceededMessage(true);
    }
    
}

function ShowFormattingInstructions()
{
    $('#HiddenFormattingInstructionsButton').click();
}

//open compose message modal and reset input fields accordingly
function OpenComposeModal(idInboxMessge) {
    $("#" + SendMessageToRosterButtonId).click();
    $("#" + UpdateMessagePopupFieldsButtonId).click();
}

function LoadSelectUsersModal() {
    $("#HiddenLoadUsersButton").click();
}