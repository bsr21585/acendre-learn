﻿function pageLoad() {
    ShowHideRemoveDateRowButton();
}

function SetOptionsForSelectedMeetingType()
{
    // get the selected meeting type
    var selectedMeetingTypeValue = $("#MeetingType_Field").find(":checked").val();

    // show/hide/change session options based on selected type
    if (selectedMeetingTypeValue == 0) { // Web-based
        // show "Online" options
        $("#SessionUrlRegistration_Container").show();
        $("#SessionUrlAttend_Container").show();        
        
        // hide "Classroom" options
        $("#SessionCity_Container").hide();
        $("#SessionProvince_Container").hide();
        $("#SessionPostalCode_Container").hide();
        $("#SessionCity_Field").val("");        
        $("#SessionProvince_Field").val("");
        $("#SessionPostalCode_Field").val("");


        // hide "Password" field
        $("#MeetingPassword_Container").hide();
        $("#MeetingPassword_Field").val("");

        // hide "organizer" field
        $("#OrganizerInformation_Container").hide();

        // re-enable options that may have been disabled by a web-meeting selection

        // seats
        if ($("#SessionSeats_Field").prop("disabled") == true) {
            $("#SessionSeats_Field").prop("disabled", false);
            $("#SessionSeats_Field").val("");
        }

        // url registration
        if ($("#SessionUrlRegistration_Field").prop("disabled") == true) {
            $("#SessionUrlRegistration_Field").prop("disabled", false);
            $("#SessionUrlRegistration_Field").val("");
        }
        
        // url attend
        if ($("#SessionUrlAttend_Field").prop("disabled") == true) {
            $("#SessionUrlAttend_Field").prop("disabled", false);
            $("#SessionUrlAttend_Field").val("");
        }

        // additional dates
        $("#SessionDatesInformationPanel").show();
        $("#AddDatesButtonContainer").show();
    }
    else if (selectedMeetingTypeValue == 1) { // Classroom
        // re-enable seats field
        if ($("#SessionSeats_Field").prop("disabled") == true) {
            $("#SessionSeats_Field").prop("disabled", false);
        }

        // show "Classroom" options
        $("#SessionCity_Container").show();
        $("#SessionProvince_Container").show();
        $("#SessionPostalCode_Container").show();

        // hide "Online" options        
        $("#SessionUrlRegistration_Container").hide();
        $("#SessionUrlAttend_Container").hide();
        $("#SessionUrlRegistration_Field").val("");
        $("#SessionUrlAttend_Field").val("");

        // hide "Password" field
        $("#MeetingPassword_Container").hide();
        $("#MeetingPassword_Field").val("");

        // hide "organizer" field
        $("#OrganizerInformation_Container").hide();
        
        // additional dates
        $("#SessionDatesInformationPanel").show();
        $("#AddDatesButtonContainer").show();
    }
    else if (selectedMeetingTypeValue == 2) { // GoToMeeting
        // show "Online" options
        $("#SessionUrlRegistration_Container").show();
        $("#SessionUrlAttend_Container").show();

        // hide "Classroom" options
        $("#SessionCity_Container").hide();
        $("#SessionProvince_Container").hide();
        $("#SessionPostalCode_Container").hide();
        $("#SessionCity_Field").val("");
        $("#SessionProvince_Field").val("");
        $("#SessionPostalCode_Field").val("");

        // hide "Password" field
        $("#MeetingPassword_Container").hide();
        $("#MeetingPassword_Field").val("");

        // show "organizer" field and set options
        $("#OrganizerInformation_Container").show();
        PopulateSelectOrganizerDropDownBasedOnType(2);

        // set options for GoToMeeting, which include disabling and pre-setting "Seats" and "URLs"

        // seats
        $("#SessionSeats_Field").prop("disabled", true);
        $("#SessionSeats_Field").val(GTMSeatsAllowed);

        // url registration
        $("#SessionUrlRegistration_Field").prop("disabled", true);        
        $("#SessionUrlRegistration_Field").val("[" + AutomaticallyGenerated + "]");

        // url attend
        $("#SessionUrlAttend_Field").prop("disabled", true);
        $("#SessionUrlAttend_Field").val("[" + AutomaticallyGenerated + "]");

        // additional dates
        $("#SessionDatesInformationPanel").hide();
        $("#AddDatesButtonContainer").hide();

        $(".DateRow").each(function () { if (this.id != "Date_1") { this.remove(); } }); // kill any additional dates as we can only set up one date/time for this type of meeting
    }
    else if (selectedMeetingTypeValue == 3) { // GoToWebinar
        // show "Online" options
        $("#SessionUrlRegistration_Container").show();
        $("#SessionUrlAttend_Container").show();

        // hide "Classroom" options
        $("#SessionCity_Container").hide();
        $("#SessionProvince_Container").hide();
        $("#SessionPostalCode_Container").hide();
        $("#SessionCity_Field").val("");
        $("#SessionProvince_Field").val("");
        $("#SessionPostalCode_Field").val("");

        // hide "Password" field
        $("#MeetingPassword_Container").hide();
        $("#MeetingPassword_Field").val("");

        // show "organizer" field and set options
        $("#OrganizerInformation_Container").show();
        PopulateSelectOrganizerDropDownBasedOnType(3);

        // set options for GoToMeeting, which include disabling and pre-setting "Seats" and "URLs"

        // seats
        $("#SessionSeats_Field").prop("disabled", true);
        $("#SessionSeats_Field").val(GTWSeatsAllowed);

        // url registration
        $("#SessionUrlRegistration_Field").prop("disabled", true);
        $("#SessionUrlRegistration_Field").val("[" + AutomaticallyGenerated + "]");

        // url attend
        $("#SessionUrlAttend_Field").prop("disabled", true);
        $("#SessionUrlAttend_Field").val("[" + AutomaticallyGenerated + "]");

        // additional dates
        $("#SessionDatesInformationPanel").hide();
        $("#AddDatesButtonContainer").hide();

        $(".DateRow").each(function () { if (this.id != "Date_1") { this.remove(); } }); // kill any additional dates as we can only set up one date/time for this type of meeting
    }
    else if (selectedMeetingTypeValue == 4) { // GoToTraining
        // show "Online" options
        $("#SessionUrlRegistration_Container").show();
        $("#SessionUrlAttend_Container").show();

        // hide "Classroom" options
        $("#SessionCity_Container").hide();
        $("#SessionProvince_Container").hide();
        $("#SessionPostalCode_Container").hide();
        $("#SessionCity_Field").val("");
        $("#SessionProvince_Field").val("");
        $("#SessionPostalCode_Field").val("");

        // hide "Password" field
        $("#MeetingPassword_Container").hide();
        $("#MeetingPassword_Field").val("");

        // show "organizer" field and set options
        $("#OrganizerInformation_Container").show();
        PopulateSelectOrganizerDropDownBasedOnType(4);

        // set options for GoToMeeting, which include disabling and pre-setting "Seats" and "URLs"

        // seats
        $("#SessionSeats_Field").prop("disabled", true);
        $("#SessionSeats_Field").val(GTTSeatsAllowed);

        // url registration
        $("#SessionUrlRegistration_Field").prop("disabled", true);
        $("#SessionUrlRegistration_Field").val("[" + AutomaticallyGenerated + "]");

        // url attend
        $("#SessionUrlAttend_Field").prop("disabled", true);
        $("#SessionUrlAttend_Field").val("[" + AutomaticallyGenerated + "]");

        // additional dates - GTT now allows additional dates
        $("#SessionDatesInformationPanel").show();
        $("#AddDatesButtonContainer").show();
    }
    else if (selectedMeetingTypeValue == 5) { // WebEx
        // show "Online" options
        $("#SessionUrlRegistration_Container").show();
        $("#SessionUrlAttend_Container").show();

        // hide "Classroom" options
        $("#SessionCity_Container").hide();
        $("#SessionProvince_Container").hide();
        $("#SessionPostalCode_Container").hide();
        $("#SessionCity_Field").val("");
        $("#SessionProvince_Field").val("");
        $("#SessionPostalCode_Field").val("");

        // show "Password" field
        $("#MeetingPassword_Container").show();

        // show "organizer" field and set options
        $("#OrganizerInformation_Container").show();
        PopulateSelectOrganizerDropDownBasedOnType(5);

        // set options for GoToMeeting, which include disabling and pre-setting "Seats" and "URLs"

        // seats
        $("#SessionSeats_Field").prop("disabled", true);
        $("#SessionSeats_Field").val(WebExSeatsAllowed);

        // url registration
        $("#SessionUrlRegistration_Field").prop("disabled", true);
        $("#SessionUrlRegistration_Field").val("[" + AutomaticallyGenerated + "]");

        // url attend
        $("#SessionUrlAttend_Field").prop("disabled", true);
        $("#SessionUrlAttend_Field").val("[" + AutomaticallyGenerated + "]");

        // additional dates
        $("#SessionDatesInformationPanel").hide();
        $("#AddDatesButtonContainer").hide();

        $(".DateRow").each(function () { if (this.id != "Date_1") { this.remove(); } }); // kill any additional dates as we can only set up one date/time for this type of meeting
    }
    else {
    }
}

function InitializeSelectOrganizerField() {
    var selectedOrganizerHiddenFieldValue = $("#SelectedWebMeetingOrganizer_Field").val();

    if (selectedOrganizerHiddenFieldValue != "NULL") {
        $("#SelectOrganizer_Field").val(selectedOrganizerHiddenFieldValue);
    }
    else {
        $("#SelectOrganizer_Field").val("NULL");
        $("#SelectOrganizer_Field").prop("disabled", true);
    }

    SelectOrganizerChange(false);
}

// Populates the "select organizer" select box with organizers for the selected meeting type
function PopulateSelectOrganizerDropDownBasedOnType(meetingType) {
    // clear all options from the select box
    $("#SelectOrganizer_Field").find("option").remove().end();

    for (var i = 0; i < OrganizerAccountsForDropDown.length; i++) {
        if (OrganizerAccountsForDropDown[i].OrganizerType == meetingType || OrganizerAccountsForDropDown[i].IdWebMeetingOrganizer == "0" || OrganizerAccountsForDropDown[i].IdWebMeetingOrganizer == "NULL") {
            $("#SelectOrganizer_Field").append($("<option>", { value: OrganizerAccountsForDropDown[i].IdWebMeetingOrganizer, text: OrganizerAccountsForDropDown[i].OrganizerUsername }));
        }
    }

    SelectOrganizerChange(false);
}

// Fires on "select organizer" change, to show/hide other options
function SelectOrganizerChange(changeHiddenFieldValue) {
    var selectedOrganizer = $("#SelectOrganizer_Field").val();

    // default organizer
    if (selectedOrganizer == "NULL") {
        $("#SelectOrganizerWrapperContainer .ToggleInput").hide();
        $("#OrganizerUsernameWrapperContainer").hide();
        $("#OrganizerPasswordWrapperContainer").show();
    }
    else if (selectedOrganizer == "0") {
        $("#SelectOrganizerWrapperContainer .ToggleInput").hide();
        $("#OrganizerUsernameWrapperContainer").show();
        $("#OrganizerPasswordWrapperContainer").show();
    }
    else {
        $("#SelectOrganizerWrapperContainer .ToggleInput").show();
        $("#OrganizerUsernameWrapperContainer").hide();
        $("#OrganizerPasswordWrapperContainer").hide();
    }

    if (changeHiddenFieldValue) {
        $("#SelectedWebMeetingOrganizer_Field").val(selectedOrganizer);
    }
}

// Fires on "modify organizer password" click to show/hide the password check box
function ModifyOrganizerPasswordClick() {
    if ($("#ChangeOrganizerPassword_Field").is(":checked")) {
        $("#OrganizerPasswordWrapperContainer").show();
    }
    else {
        $("#OrganizerPasswordWrapperContainer").hide();
    }
}

//Adds new instructor
function AddInstructorsForSession(idObject) {
    var instructorsListContainer = $("#" + SessionInstructorsList_Container);
    var selectedInstructors = $('select#SelectEligibleInstructorsListBox').val();       
    if (selectedInstructors != null) {
        for (var i = 0; i < selectedInstructors.length; i++) {

            //If the instructor is already present instructor list then just remove from it the modal and don't append in the instructor list
            if (!isNaN(Number(selectedInstructors[i])) && !$("#Instructor_" + selectedInstructors[i]).length) {
                // add the selected instructor to the instructor list container
                CheckInstructorAvailability(parseInt(idObject), parseInt(selectedInstructors[i]))
            }

        }
    }
}

//Removes existing instructor
function RemoveInstructorFromSession(instructorId) {
    //Append back the selected item into modal instructor list 
    var modalInstructorsListContainer = $('select#SelectEligibleInstructorListBox');
    var itemContainerDiv = $("<option value='" + instructorId + "'>" + $("#Instructor_" + instructorId).text() + "</option>");

    //If item is not present in the list then only append 
    if ($("select#SelectEligibleInstructorsListBox option[value='" + instructorId + "']").length == 0) {
        itemContainerDiv.appendTo(modalInstructorsListContainer);

        if ($("select#SelectEligibleInstructorsListBox option[value='" + NoRecordsFound + "']").length == 1) {
            // remove the instructor from the select list
            $("#SelectEligibleInstructorsListBox option[value='" + NoRecordsFound + "']").remove();
        }
    }

    //Remove from the instructor list
    $("#Instructor_" + instructorId).remove();
    $('#SelectInstructorsForSessionModalPopupFeedbackContainer').hide();
}

//Adds new resource
function AddResourcesToSession(idObject) {
    var resourcesListContainer = $("#" + SessionResourcesList_Container);
    var selectedResources = $('select#SelectEligibleResourcesListBox').val();

    if (selectedResources != null) {
        for (var i = 0; i < selectedResources.length; i++) {

            //If the resource is already present resource list then just remove from it the modal and don't append in the resource list
            if (!isNaN(Number(selectedResources[i])) && !$("#Resource_" + selectedResources[i]).length) {
                // add the selected resource to the resource list container
                CheckForResourceAvailability(parseInt(idObject), parseInt(selectedResources[i]))
            }

        }
    }
}

//Removes existing resource
function RemoveResourceFromSession(resourceId) {
    //Append back the selected item into modal resourse list 
    var modalResourcesListContainer = $('select#SelectEligibleResourcesListBox');
    var itemContainerDiv = $("<option value='" + resourceId + "'>" + $("#Resource_" + resourceId).text() + "</option>");

    //If item is not present in the list then only append 
    if ($("select#SelectEligibleResourcesListBox option[value='" + resourceId + "']").length == 0) {
        itemContainerDiv.appendTo(modalResourcesListContainer);

        if ($("select#SelectEligibleResourcesListBox option[value='" + NoRecordsFound + "']").length == 1) {
            // remove the user from the select list
            $("#SelectEligibleResourcesListBox option[value='" + NoRecordsFound + "']").remove();
        }
    }

    //Remove from the user list
    $("#Resource_" + resourceId).remove();
    $('#SelectResourcesForSessionModalPopupFeedbackContainer').hide();
}

//Remove display feedback message
function RemoveFeedBackMessage() {
    $('#SelectResourcesForSessionModalPopupFeedbackContainer').hide();
}

//Gets selected instructors
function GetSelectedInstructorsForHiddenField() {
    var instructorsListContainer = $("#" + SessionInstructorsList_Container);
    var selectedInstructorsField = $("#" + SessionSelectedInstructors_Field);
    var selectedInstructors = "";

    instructorsListContainer.children().each(function () {
        selectedInstructors = selectedInstructors + $(this).prop("id").replace("Instructor_", "") + ",";
    });

    if (selectedInstructors.length > 0)
    { selectedInstructors = selectedInstructors.substring(0, selectedInstructors.length - 1); }

    selectedInstructorsField.val(selectedInstructors);
}

//Gets selected resources
function GetSelectedResourcesForHiddenField() {
    var resourcesListContainer = $("#" + SessionResourcesList_Container);
    var selectedResourcesField = $("#" + SessionSelectedResources_Field);
    var selectedResources = "";

    resourcesListContainer.children().each(function () {
        selectedResources = selectedResources + $(this).prop("id").replace("Resource_", "") + ",";
    });

    if (selectedResources.length > 0)
    { selectedResources = selectedResources.substring(0, selectedResources.length - 1); }

    selectedResourcesField.val(selectedResources);
}

function PopulateHiddenFieldsForDynamicElements(elem, hfId) {
    GetSelectedInstructorsForHiddenField();
    GetSelectedResourcesForHiddenField();
    GetSelectedDates(elem, hfId);
    RemoveFeedBackMessage();
}

//Handles process of checking resource availability
function CheckForResourceAvailability(idObject, resourceId) {
    var timeZoneValue = $("#" + RuleSetEnrollmentTimezone_Field).val();
    var variables = "{'idObject': " + idObject + ",'idResource': " + resourceId + ",'idTimeZone': " + timeZoneValue + ",'selectedDatesForResources': " + $('#' + selectedDatesForResources).val() + "}";
    $.ajax(
   {
       type: "POST",
       url: "Modify.aspx/CheckForResourceAvailability",
       data: variables,
       contentType: "application/json;charset=utf-8",
       dataType: "json",
       async: "true",
       cache: "false",

       success: function (msg) {
           if (msg.d.ExceptionValue == "" || msg.d.ExceptionValue == null) {
               if (msg.d.IsAvailable) {
                   var resourcesListContainer = $("#" + SessionResourcesList_Container);
                   var itemContainerDiv = $("<div id=\"Resource_" + resourceId + "\">" + "<img class=\"SmallIcon\" onclick=\"javascript:RemoveResourceFromSession('" + resourceId + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" />" + $("#SelectEligibleResourcesListBox option[value='" + resourceId + "']").text() + "</div>");
                   itemContainerDiv.appendTo(resourcesListContainer);
                   $('#SelectResourcesForSessionModalPopupFeedbackContainer').hide();

                   // remove the user from the select list
                   $("#SelectEligibleResourcesListBox option[value='" + resourceId + "']").remove();
               }
               else {
                   //showing resource not available error message
                   $('#SelectResourcesForSessionModalPopupFeedbackContainer').show();
                   document.getElementById(SelectResourcesForSessionButton).click();
               }
           }

       },

       Error: function (x, e) {
       }
   });
}

//Handles process of checking instructor availability
function CheckInstructorAvailability(idStandupTrainingInstance, selectedInstructor) {
    var timezoneValue = $("#" + RuleSetEnrollmentTimezone_Field).val();
    var variables = "{'idInstructor': " + parseInt(selectedInstructor) + ",'idStandupTrainingInstance': " + idStandupTrainingInstance + ",'idTimezone': " + timezoneValue + ",'selectedDates': " + $('#' + selectedDatesForResources).val() + "}";

    $.ajax(
   {
       type: "POST",
       url: "Modify.aspx/CheckInstructorAvailability",
       data: variables,
       contentType: "application/json;charset=utf-8",
       dataType: "json",
       async: "true",
       cache: "false",

       success: function (msg) {
           if (msg.d.ExceptionValue == "" || msg.d.ExceptionValue == null) {
               if (msg.d.IsAvailable) {
                   var instructorsListContainer = $("#" + SessionInstructorsList_Container);
                   var itemContainerDiv = $("<div id=\"Instructor_" + selectedInstructor + "\">" + "<img class=\"SmallIcon\" onclick=\"javascript:RemoveInstructorFromSession('" + selectedInstructor + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" />" + $("#SelectEligibleInstructorsListBox option[value='" + selectedInstructor + "']").text() + "</div>");
                   itemContainerDiv.appendTo(instructorsListContainer);
                   $('#SelectInstructorsForSessionModalPopupFeedbackContainer').hide();

                   // remove the instructor from the select list
                   $("#SelectEligibleInstructorsListBox option[value='" + selectedInstructor + "']").remove();
               }
               else {
                   //showing instructor not available error message
                   $('#SelectInstructorsForSessionModalPopupFeedbackContainer').show();
                   document.getElementById(SelectInstructorsForSessionButton).click();
               }
           }


       },

       Error: function (x, e) {
       }
   });
}

//Remove date method
//Used to remove the date row
function RemoveDateRow(elem) {   
    var dateRows = $("tr.DateRow");
    var totalRows = dateRows.length;
    if (dateRows != null && totalRows > 1) {
        $(elem).parent().parent().remove();
    }

    //check if after deleting there is only one row then hide remove button
    var dateRowsAfterDeleting = $("tr.DateRow");
    if (dateRowsAfterDeleting.length == 1) {
        $(".RemoveDateButton").hide();
    }
}

//Get Dates method
//Used to fetch the dynamically added dates
function GetSelectedDates(elem, hfId) {

    var dates = new Array();

    $("tr.DateRow").each(function (index, obj) {

        var dateStartValue = $(obj).find(".DatePickerInput")[0];
        var amPmStartValue = $(obj).find(".DatePickerTimeAMPM")[0];
        var minuteStartValue = $(obj).find(".DatePickerTimeMinute")[0];
        var hourStartValue = $(obj).find(".DatePickerTimeHour")[0];
        var dateEndValue = $(obj).find(".DatePickerInput")[1];
        var minuteEndValue = $(obj).find(".DatePickerTimeMinute")[1];
        var amPmEndValue = $(obj).find(".DatePickerTimeAMPM")[1];
        var hourEndValue = $(obj).find(".DatePickerTimeHour")[1];
        var existingDateValue = $(obj).find(".IsExistingHidden")[0];
        if (dateStartValue != null && dateEndValue != null) {

            var date = new MeetingObject();
            date.DateStart = $(dateStartValue).val();
            date.HourStart = $(hourStartValue).val();
            date.MinuteStart = $(minuteStartValue).val();
            date.AmPmStart = $(amPmStartValue).val();
            date.HourEnd = $(hourEndValue).val();
            date.MinuteEnd = $(minuteEndValue).val();
            date.DateEnd = $(dateEndValue).val();
            date.AmPmEnd = $(amPmEndValue).val();
            date.ExistingDate = $(existingDateValue).text();
            dates.push(date);
        }
    });

    var stringifiedDates = JSON.stringify(dates);
    $("#" + hfId).val(stringifiedDates);
    $("#" + selectedDatesForResources).val(stringifiedDates);
}

//Add date method
//Used to add the date control row
function AddDateRow(elem) {

    var dateRows = $("tr.DateRow");

    if (dateRows != null) {

        var newDateRow = $(dateRows[dateRows.length - 1]).clone();

        if (newDateRow != null) {

            var dtStartValue = $(newDateRow).find(".StartDate")[0];
            var dtEndValue = $(newDateRow).find(".EndDate")[0];
            var removeDateButton = $(newDateRow).find(".RemoveDateButton")[0];

            if (dtStartValue != null && dtEndValue != null && removeDateButton != null) {

                var lastId = dtEndValue["id"].split("_")[1];
                var newId = parseInt(lastId) + 1;

                var clone = DateControlRowHtml.replace(/_1/g, '_' + newId);

                $("#Master_PageContentPlaceholder_DatesTable___Page tbody").append(clone);
                $("#Date_" + newId + " input").datepicker(
                    {
                        dateFormat: DateFormat,
                        changeMonth: true,
                        changeYear: true
                    });
                $("#Date_" + newId + " input").val("");

                $("#SessionDtEnd_" + newId + "_DatePickerImage").click(function () {
                    if ($("#SessionDtEnd_" + newId + "_DateInputControl").prop("disabled") == false) {
                        $("#SessionDtEnd_" + newId + "_DateInputControl").datepicker("show");
                    }
                });

                $("#SessionDtStart_" + newId + "_DatePickerImage").click(function () {
                    if ($("#SessionDtStart_" + newId + "_DateInputControl").prop("disabled") == false) {
                        $("#SessionDtStart_" + newId + "_DateInputControl").datepicker("show");
                    }
                });

                ShowHideRemoveDateRowButton();
            }
        }
    }
}

// class: MeetingObject
// used for populating the date information
function MeetingObject() {

    //represents the user field
    this.DateStart = null;

    //represents the user field
    this.HourStart = null;

    this.MinuteStart = null;
    this.AmPmStart = null;
    this.HourEnd = null;
    this.MinuteEnd = null;
    this.DateEnd = null;
    this.AmPmEnd = null;
    this.ExistingDate = null
}

//hides remove date row button if date has been passed away and showing other wise
function ShowHideRemoveDateRowButton() {

    var dateRows = $("tr.DateRow")
    dateRows.each(function (index, item) {
        if ($(item).find(".aspNetDisabled").length > 0 || dateRows.length == 1) {
            $(item).find(".RemoveDateButton").hide();
        }
        else {
            $(item).find(".RemoveDateButton").show();
            $(item).find(".RemoveDateButton").attr("onclick", "RemoveDateRow(this);return false;");
        }
    })
}
