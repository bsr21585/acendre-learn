﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using web = System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using System.Transactions;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.StandupTraining.Sessions
{
    public class ManageRoster : AsentiaAuthenticatedPage
    {
        #region Public Properties
        public Panel ObjectOptionsPanel;
        public Panel ManageRosterPropertiesFormContentWrapperContainer;
        public Panel StandupTrainingObjectMenuContainer;
        public Panel ManageRosterPropertiesWrapperContainer;
        public Panel ManageRosterPropertiesInstructionsPanel;
        public Panel ManageRosterPropertiesFeedbackContainer;
        public Panel ManageRosterPropertiesContainer;
        public Panel ManageRosterPropertiesActionsPanel;
        public Panel ManageRosterPropertiesTabPanelsContainer;        
        #endregion

        #region Private Properties
        private InboxMessage _InboxMessageObject;

        private Library.StandupTraining _StandupTrainingObject;
        private Library.StandupTrainingInstance _SessionObject;
        private Library.StandUpTrainingInstanceToUserLink _StandupTrainingInstanceToUserLinkObject;

        private Button _SaveButton;
        private Button _CancelButton;
        private Button _LaunchSeatsExceededButton;
        private Button _TargetButtonForComposeModal;
        private Button _SendButton;
        private Button _UpdateMessagePopupFieldsButton;

        private RadioButtonList _RecipientType;
        private TextBox _Subject = new TextBox();
        private TextBox _MessageBody = new TextBox();
        private TextBox _ReplyMessageBody = new TextBox();

        private ModalPopup _SelectUsersForEnrolledList;
        private ModalPopup _NumberOfSeatsExceeded;
        private ModalPopup _ComposeMessageModalPopup;

        private DynamicListBox _SelectAddEnrolledListUsers;

        private DataTable _ManageRosterUsersForSelectList;
        private DataTable _RosterUserList;

        private Table _EnrolledGrid;
        private Table _WaitlistGrid;

        private HiddenField _EnrolledAndWaitListJsonData;
        private HiddenField _DroppedRegistrantKeys;  

        private Panel _WaitlistSeatsExceededPanel;
        private Panel _EnrolledListSeatsExceededPanel;
        private Panel _MessageFormContainer = new Panel();

        private bool _HasSessionAlreadyOccurred;

        private ModalPopup _BatchUpdateModal;
        private ModalPopup _BatchUpdateFormattingInstructionsModal;
        private UploaderAsync _BatchUploader;
        private Button _HiddenFormattingInstructionsButton;
        private HiddenField _BatchUploadError;

        private Button _EnrolledUsersButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(ManageRoster), "Asentia.LMS.Pages.Administrator.StandupTraining.Sessions.ManageRoster.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Grid.js");

            // build global JS variables
            StringBuilder globalJS = new StringBuilder();
            globalJS.AppendLine("PromoteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_PROMOTE, ImageFiles.EXT_PNG) + "\";");
            globalJS.AppendLine("DemoteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DEMOTE, ImageFiles.EXT_PNG) + "\";");
            globalJS.AppendLine("DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            globalJS.AppendLine("AvailableSeatsForEnrollment = " + this._SessionObject.Seats.ToString() + ";");
            globalJS.AppendLine("AvailableSeatsForWaitingList = " + this._SessionObject.WaitingSeats.ToString() + ";");
            globalJS.AppendLine("LaunchSeatsExceededButtonId = \"" + this._LaunchSeatsExceededButton.ID + "\";");
            globalJS.AppendLine("SendMessageToRosterButtonId = \"" + this._TargetButtonForComposeModal.ID + "\";");
            globalJS.AppendLine("UpdateMessagePopupFieldsButtonId = \"" + this._UpdateMessagePopupFieldsButton.ID + "\";");
            globalJS.AppendLine("WaitlistSeatsExceededPanelId = \"" + this._WaitlistSeatsExceededPanel.ID + "\";");
            globalJS.AppendLine("EnrolledListSeatsExceededPanelId = \"" + this._EnrolledListSeatsExceededPanel.ID + "\";");
            globalJS.AppendLine("LabelUnknown = \"" + _GlobalResources.Unknown + "\";");
            globalJS.AppendLine("LabelCompleted = \"" + _GlobalResources.Completed + "\";");
            globalJS.AppendLine("LabelIncomplete = \"" + _GlobalResources.Incomplete + "\";");
            globalJS.AppendLine("LabelNoRecordsFound = \"" + _GlobalResources.NoRecordsFound + "\";");

            csm.RegisterClientScriptBlock(typeof(ManageRoster), "GlobalJS", globalJS.ToString(), true);

            // for multiple "start up" scripts, we need to build start up calls
            // and add them to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();
            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" InitializeSortableOnWaitlistGrid();");
            multipleStartUpCallsScript.AppendLine("});");
            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            
            // get the standup training and standup training instance objects
            this._GetStandupTrainingObjects();

            // determine whether the current session user is an instructor for this session
            DataTable sessionInstructors = this._SessionObject.GetInstructors(null);
            bool isUserAnInstructorForThisSession = false;

            foreach (DataRow row in sessionInstructors.Rows)
            {
                if (Convert.ToInt32(row["idInstructor"]) == AsentiaSessionState.IdSiteUser)
                {
                    isUserAnInstructorForThisSession = true;
                    break;
                }
            }

            // check permissions
            if (
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_InstructorLedTrainingManager) 
                && !AsentiaSessionState.IsUserAnILTInstructor 
                && !isUserAnInstructorForThisSession
               )
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/standuptraining/sessions/ManageRoster.css");

            // initialize the administrator menu
            this.InitializeAdminMenu();            

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // flag to set whether or not the status and score fields will be shown
            if (this._SessionObject.MeetingTimes.Count > 0)
            { this._HasSessionAlreadyOccurred = ((Library.StandupTrainingInstance.MeetingTimeProperty)(this._SessionObject.MeetingTimes[0])).DtStart < DateTime.UtcNow; }
            else
            { this._HasSessionAlreadyOccurred = true; }

            // build the page controls
            this._BuildControls();
        }
        #endregion        

        #region _GetStandupTrainingObjects
        /// <summary>
        /// Gets standup training and standup training instance objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetStandupTrainingObjects()
        {
            // get the id querystring parameter
            int qsSTId = this.QueryStringInt("stid", 0);
            int vsSTId = this.ViewStateInt(this.ViewState, "stid", 0);
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsSTId > 0 || vsSTId > 0)
            {
                int stid = 0;

                if (qsSTId > 0)
                { stid = qsSTId; }

                if (vsSTId > 0)
                { stid = vsSTId; }

                try
                {
                    if (stid > 0)
                    { this._StandupTrainingObject = new Library.StandupTraining(stid); }
                }
                catch
                { Response.Redirect("~/administrator/standuptraining"); }
            }
            else
            { Response.Redirect("~/administrator/standuptraining"); }

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._SessionObject = new Library.StandupTrainingInstance(id); }
                }
                catch
                { Response.Redirect("~/administrator/standuptraining/Dashboard.aspx?id=" + this._StandupTrainingObject.Id.ToString()); }
            }
            else
            { Response.Redirect("~/administrator/standuptraining/Dashboard.aspx?id=" + this._StandupTrainingObject.Id.ToString()); }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the object options panel
            this._BuildObjectOptionsPanel();

            this.ManageRosterPropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ManageRosterPropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the standup training object menu
            if (this._StandupTrainingObject != null)
            {
                StandupTrainingObjectMenu standupTrainingObjectMenu = new StandupTrainingObjectMenu(this._StandupTrainingObject);
                standupTrainingObjectMenu.SelectedItem = StandupTrainingObjectMenu.MenuObjectItem.Sessions;

                this.StandupTrainingObjectMenuContainer.Controls.Add(standupTrainingObjectMenu);
            }

            // build the standup training instance properties form actions panel
            this._BuildSessionPropertiesActionsPanel();

            // build the standup training instance properties form
            this._BuildManageRosterPropertiesForm();

            // builds the modal for composing the message.
            this._BuildComposeMessageActionModal();

            //add the input fields in compose message modal popup
            this._BuildInputControls();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get standup training title information
            string standupTrainingTitleInInterfaceLanguage = this._StandupTrainingObject.Title;
            string standupTrainingImagePath;
            string standupTrainingImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Library.StandupTraining.LanguageSpecificProperty standupTrainingLanguageSpecificProperty in this._StandupTrainingObject.LanguageSpecificProperties)
                {
                    if (standupTrainingLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { standupTrainingTitleInInterfaceLanguage = standupTrainingLanguageSpecificProperty.Title; }
                }
            }

            if (this._StandupTrainingObject.Avatar != null)
            {
                standupTrainingImagePath = SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + this._StandupTrainingObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                standupTrainingImageCssClass = "AvatarImage";
            }
            else
            {
                standupTrainingImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            }

            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;            
            string pageTitle;

            string sessionTitleInInterfaceLanguage = this._SessionObject.Title;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Library.StandupTrainingInstance.LanguageSpecificProperty sessionLanguageSpecificProperty in this._SessionObject.LanguageSpecificProperties)
                {
                    if (sessionLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { sessionTitleInInterfaceLanguage = sessionLanguageSpecificProperty.Title; }
                }
            }

            breadCrumbPageTitle = sessionTitleInInterfaceLanguage;
            pageTitle = sessionTitleInInterfaceLanguage;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.InstructorLedTrainingModules, "/administrator/standuptraining"));
            breadCrumbLinks.Add(new BreadcrumbLink(standupTrainingTitleInInterfaceLanguage, "/administrator/standuptraining/Dashboard.aspx?id=" + this._StandupTrainingObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Sessions, "/administrator/standuptraining/sessions/Default.aspx?stid=" + this._StandupTrainingObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, standupTrainingTitleInInterfaceLanguage, standupTrainingImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION, ImageFiles.EXT_PNG), standupTrainingImageCssClass);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // PRINT ROSTER
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("PrintRosterLink",
                                                null,
                                                "javascript:void(0);",
                                                "window.print();",
                                                _GlobalResources.PrintRoster,
                                                null,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_PRINT, ImageFiles.EXT_PNG))
                );


            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.MESSAGECENTER_ENABLE))
            {
                // COMPOSE MESSAGE
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("ComposeMessageLink",
                                                    null,
                                                   "javascript: void(0);",
                                                   "OpenComposeModal(0);return false;",
                                                    _GlobalResources.SendMessageToRoster,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL_CHECK, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                    );
            }

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildBatchUpdateModalPopup
        /// <summary>
        /// Builds the modal popup for batch updating the roster completions
        /// </summary>
        private void _BuildBatchUpdateModalPopup(string targetControlId, Control modalContainer)
        {
            this._BatchUpdateModal = new ModalPopup("BatchUpdateModalPopup");
            this._BatchUpdateModal.TargetControlID = targetControlId;
            this._BatchUpdateModal.Type = ModalPopupType.Form;
            this._BatchUpdateModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG);
            this._BatchUpdateModal.HeaderIconAlt = _GlobalResources.BatchUpdate;
            this._BatchUpdateModal.HeaderText = _GlobalResources.BatchUpdate;
            this._BatchUpdateModal.ShowLoadingPlaceholder = true;
            this._BatchUpdateModal.ShowCloseIcon = true;
            this._BatchUpdateModal.SubmitButton.OnClientClick = "BatchUserUpdate(" + this._SessionObject.Id + "); return false;";

            // information panel
            Panel batchUploadInformationPanel = new Panel();
            batchUploadInformationPanel.ID = "BatchUploadInformationPanel";
            this.FormatPageInformationPanel(batchUploadInformationPanel, _GlobalResources.PleaseUploadATabDelimitedTextFileToUpdateCompletionStatusForUsersOnTheRosterIfAUserIsNotOnTheRosterTheyWillBeAdded, true);
            this._BatchUpdateModal.AddControlToBody(batchUploadInformationPanel);

            // formatting instructions link
            HyperLink formattingInstructionsHyperLink = new HyperLink();
            formattingInstructionsHyperLink.ID = "FormattingInstructionsHyperLink";
            formattingInstructionsHyperLink.Text = _GlobalResources.FormattingInstructions;
            formattingInstructionsHyperLink.Attributes.Add("onclick", "ShowFormattingInstructions();");

            this._BatchUpdateModal.AddControlToBody(AsentiaPage.BuildFormField("FormattingInstructionsHyperLink_Field",
                                                                                null,
                                                                                null,
                                                                                formattingInstructionsHyperLink,
                                                                                false,
                                                                                false,
                                                                                false));

            this._BatchUploader = new UploaderAsync("BatchUpdateUploader", UploadType.RosterBatch, "Upload_BatchUpdate_Field_ErrorContainer");
            this._BatchUploader.CssClass = "FileUpload_Field";

            this._BatchUpdateModal.AddControlToBody(AsentiaPage.BuildFormField("Upload_BatchUpdate_Field",
                                                                                _GlobalResources.UploadFile,
                                                                                null,
                                                                                this._BatchUploader,
                                                                                true,
                                                                                true,
                                                                                false)
                );

            // hidden button for updating modal success message
            Button hiddenSuccessButton = new Button();
            hiddenSuccessButton.ID = "HiddenSuccessButton";
            hiddenSuccessButton.Style.Add("display", "none");
            hiddenSuccessButton.Command += this._HiddenSuccessButton_Command;
            this._BatchUpdateModal.AddControlToBody(hiddenSuccessButton);

            // hidden button for updating modal failure message
            Button hiddenFailureButton = new Button();
            hiddenFailureButton.ID = "HiddenFailureButton";
            hiddenFailureButton.Style.Add("display", "none");
            hiddenFailureButton.Command += this._HiddenFailureButton_Command;
            this._BatchUpdateModal.AddControlToBody(hiddenFailureButton);

            // hidden error value
            this._BatchUploadError = new HiddenField();
            this._BatchUploadError.ID = "BatchUploadError";
            this._BatchUpdateModal.AddControlToBody(this._BatchUploadError);

            modalContainer.Controls.Add(this._BatchUpdateModal);

            this._BuildBatchUpdateFormattingInstructionsModalPopup(this._HiddenFormattingInstructionsButton.ID, modalContainer);

        }
        #endregion

        #region _BuildBatchUpdateFormattingInstructionsModalPopup
        /// <summary>
        /// Builds the modal popup for displaying the batch update formatting instructions
        /// </summary>
        /// <param name="targetControlId"></param>
        private void _BuildBatchUpdateFormattingInstructionsModalPopup(string targetControlId, Control modalContainer)
        {
            this._BatchUpdateFormattingInstructionsModal = new ModalPopup("BatchUpdateFormattingInstructionsModalPopup");
            this._BatchUpdateFormattingInstructionsModal.TargetControlID = targetControlId;
            this._BatchUpdateFormattingInstructionsModal.Type = ModalPopupType.Information;
            this._BatchUpdateFormattingInstructionsModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION, ImageFiles.EXT_PNG);
            this._BatchUpdateFormattingInstructionsModal.HeaderIconAlt = _GlobalResources.FormattingInstructions;
            this._BatchUpdateFormattingInstructionsModal.HeaderText = _GlobalResources.FormattingInstructions;
            this._BatchUpdateFormattingInstructionsModal.ShowLoadingPlaceholder = true;
            this._BatchUpdateFormattingInstructionsModal.ShowCloseIcon = true;

            // information panel
            Panel formattingInstructionsInformationPanel = new Panel();
            formattingInstructionsInformationPanel.ID = "FormattingInstructionsInformationPanel";
            this.FormatPageInformationPanel(formattingInstructionsInformationPanel, _GlobalResources.UseTheTableBelowAsReferenceForCreatingTabDelimitedRosterFile, true);
            this._BatchUpdateFormattingInstructionsModal.AddControlToBody(formattingInstructionsInformationPanel);

            // column info panel
            Panel columnInfoPanel = new Panel();
            columnInfoPanel.ID = "ColumnInfoPanel";

            // apply css class to container
            columnInfoPanel.CssClass = "FormContentContainer";

            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.CssClass = "GridHeaderRow";
            TableHeaderCell columnOrder = new TableHeaderCell();
            TableHeaderCell dataHeader = new TableHeaderCell();
            TableHeaderCell requiredHeader = new TableHeaderCell();
            TableHeaderCell uniqueHeader = new TableHeaderCell();
            TableHeaderCell typeHeader = new TableHeaderCell();
            TableHeaderCell lengthHeader = new TableHeaderCell();
            TableHeaderCell formatHeader = new TableHeaderCell();

            columnOrder.Text = _GlobalResources.Column;
            
            dataHeader.Text = _GlobalResources.Data;
            requiredHeader.Text = _GlobalResources.Required;
            uniqueHeader.Text = _GlobalResources.Unique;
            typeHeader.Text = _GlobalResources.Type;
            lengthHeader.Text = _GlobalResources.Length;
            formatHeader.Text = _GlobalResources.Format;

            headerRow.Controls.Add(columnOrder);
            headerRow.Controls.Add(dataHeader);
            headerRow.Controls.Add(requiredHeader);
            headerRow.Controls.Add(uniqueHeader);
            headerRow.Controls.Add(typeHeader);
            headerRow.Controls.Add(lengthHeader);
            headerRow.Controls.Add(formatHeader);

            Table columnInfoTable = new Table();
            columnInfoTable.ID = "ColumnInfoTable";

            columnInfoTable.Controls.Add(headerRow);
            columnInfoTable.CssClass = "GridTable";

            // username row
            TableRow usernameRow = new TableRow();
            usernameRow.ID = "FormattingInstructions_Username";
            usernameRow.CssClass = "GridDataRow";

            TableCell usernameColumnCell = new TableCell();
            usernameColumnCell.Text = "Column A";
            usernameRow.Cells.Add(usernameColumnCell);

            TableCell usernameDataCell = new TableCell();
            usernameDataCell.Text = _GlobalResources.Username;
            usernameRow.Cells.Add(usernameDataCell);

            TableCell usernameRequiredCell = new TableCell();
            usernameRequiredCell.Text = _GlobalResources.Yes;
            usernameRow.Cells.Add(usernameRequiredCell);

            TableCell usernameUniqueCell = new TableCell();
            usernameUniqueCell.Text = _GlobalResources.Yes;
            usernameRow.Cells.Add(usernameUniqueCell);

            TableCell usernameTypeCell = new TableCell();
            usernameTypeCell.Text = _GlobalResources.Text;
            usernameRow.Cells.Add(usernameTypeCell);

            TableCell usernameLengthCell = new TableCell();
            usernameLengthCell.Text = "512";
            usernameRow.Cells.Add(usernameLengthCell);

            TableCell usernameFormatCell = new TableCell();
            usernameRow.Cells.Add(usernameFormatCell);

            columnInfoTable.Rows.Add(usernameRow);

            // completion status row
            TableRow completionStatusRow = new TableRow();
            completionStatusRow.ID = "FormattingInstructions_CompletionStatus";
            completionStatusRow.CssClass = "GridDataRow GridDataRowAlternate";

            TableCell completionStatusColumnCell = new TableCell();
            completionStatusColumnCell.Text = "Column B";
            completionStatusRow.Cells.Add(completionStatusColumnCell);

            TableCell completionStatusDataCell = new TableCell();
            completionStatusDataCell.Text = _GlobalResources.CompletionStatus;
            completionStatusRow.Cells.Add(completionStatusDataCell);

            TableCell completionStatusRequiredCell = new TableCell();
            completionStatusRequiredCell.Text = _GlobalResources.Yes;
            completionStatusRow.Cells.Add(completionStatusRequiredCell);

            TableCell completionStatusUniqueCell = new TableCell();
            completionStatusUniqueCell.Text = _GlobalResources.No;
            completionStatusRow.Cells.Add(completionStatusUniqueCell);

            TableCell completionStatusTypeCell = new TableCell();
            completionStatusTypeCell.Text = _GlobalResources.Text;
            completionStatusRow.Cells.Add(completionStatusTypeCell);

            TableCell completionStatusLengthCell = new TableCell();
            completionStatusLengthCell.Text = "10";
            completionStatusRow.Cells.Add(completionStatusLengthCell);

            TableCell completionStatusFormatCell = new TableCell();
            completionStatusFormatCell.Text = _GlobalResources.CompletedIncompleteOrUnknown;
            completionStatusRow.Cells.Add(completionStatusFormatCell);

            columnInfoTable.Rows.Add(completionStatusRow);

            // score row
            TableRow scoreRow = new TableRow();
            scoreRow.ID = "FormattingInstructions_Score";
            scoreRow.CssClass = "GridDataRow";

            TableCell scoreColumnCell = new TableCell();
            scoreColumnCell.Text = "Column C";
            scoreRow.Cells.Add(scoreColumnCell);

            TableCell scoreDataCell = new TableCell();
            scoreDataCell.Text = _GlobalResources.Score;
            scoreRow.Cells.Add(scoreDataCell);

            TableCell scoreRequiredCell = new TableCell();
            scoreRequiredCell.Text = _GlobalResources.No;
            scoreRow.Cells.Add(scoreRequiredCell);

            TableCell scoreUniqueCell = new TableCell();
            scoreUniqueCell.Text = _GlobalResources.No;
            scoreRow.Cells.Add(scoreUniqueCell);

            TableCell scoreTypeCell = new TableCell();
            scoreTypeCell.Text = _GlobalResources.Integer;
            scoreRow.Cells.Add(scoreTypeCell);

            TableCell scoreLengthCell = new TableCell();
            scoreLengthCell.Text = "-";
            scoreRow.Cells.Add(scoreLengthCell);

            TableCell scoreFormatCell = new TableCell();
            scoreRow.Cells.Add(scoreFormatCell);

            columnInfoTable.Rows.Add(scoreRow);

            // end row
            TableRow endRow = new TableRow();
            endRow.ID = "FormattingInstructions_End";
            endRow.CssClass = "GridDataRow GridDataRowAlternate";

            TableCell endColumnCell = new TableCell();
            endColumnCell.Text = "Column D";
            endRow.Cells.Add(endColumnCell);

            TableCell endDataCell = new TableCell();
            endDataCell.Text = _GlobalResources.ROWEND_UPPER;
            endRow.Cells.Add(endDataCell);

            TableCell endRequiredCell = new TableCell();
            endRequiredCell.Text = _GlobalResources.RowEndInfo;
            endRequiredCell.ColumnSpan = 5;
            endRow.Cells.Add(endRequiredCell);

            columnInfoTable.Rows.Add(endRow);

            columnInfoPanel.Controls.Add(columnInfoTable);
            this._BatchUpdateFormattingInstructionsModal.AddControlToBody(columnInfoPanel);


            modalContainer.Controls.Add(this._BatchUpdateFormattingInstructionsModal);


        }
        #endregion        

        #region _BuildManageRosterPropertiesForm
        /// <summary>
        /// Builds the manage roster properties form
        /// </summary>
        private void _BuildManageRosterPropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.ManageRosterPropertiesInstructionsPanel, _GlobalResources.ManageTheRosterForThisSessionUsingTheFormBelowForChangesToTakeEffectYouMustClickTheSaveChangesButton, true);

            // clear controls from container
            this.ManageRosterPropertiesContainer.Controls.Clear();

            // build standup training session form tabs
            this._BuildManageRosterFormTabs();

            this.ManageRosterPropertiesTabPanelsContainer = new Panel();
            this.ManageRosterPropertiesTabPanelsContainer.ID = "ManageRosterProperties_TabPanelsContainer";
            this.ManageRosterPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.ManageRosterPropertiesContainer.Controls.Add(this.ManageRosterPropertiesTabPanelsContainer);

            // initialize the "Enrolled List" and "Waiting List" grid tables
            this._EnrolledGrid = new Table();
            this._EnrolledGrid.ID = "EnrolledGrid";

            this._WaitlistGrid = new Table();
            this._WaitlistGrid.ID = "WaitlistGrid";

            // build the "Enrolled List" panel
            this._BuildRosterFormEnrolledListPanel();

            // build the "Waiting List" panel
            this._BuildRosterFormWaitingListPanel();
        }
        #endregion

        #region _BuildManageRosterFormTabs
        /// <summary>
        /// Builds manage roster form tabs
        /// </summary>
        private void _BuildManageRosterFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("EnrolledList", _GlobalResources.EnrolledList));
            tabs.Enqueue(new KeyValuePair<string, string>("WaitingList", _GlobalResources.WaitingList));

            // build and attach the tabs
            this.ManageRosterPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("ManageRosterProperties", tabs));
        }
        #endregion

        #region _BuildRosterFormEnrolledListPanel
        /// <summary>
        /// Builds the enrolled list panel.
        /// </summary>
        private void _BuildRosterFormEnrolledListPanel()
        {
            // populate datatables with lists of users who are not in enrolled list
            if (this._SessionObject != null)
            { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForManageRosterList(this._SessionObject.Id, null); }
            else
            { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForManageRosterList(0, null); }

            // "Enrolled List" is the default tab, so this is visible on page load.
            Panel enrolledListPanel = new Panel();
            enrolledListPanel.ID = "ManageRosterProperties_" + "EnrolledList" + "_TabPanel";
            enrolledListPanel.Attributes.Add("style", "display: block;");

            List<Control> enrolledListInputControls = new List<Control>();

            // add a panel for "mark selected as" - only if the session has already occurred
            if (this._HasSessionAlreadyOccurred)
            {
                Panel recordMarkingAndSynchronizationPanel = new Panel();
                recordMarkingAndSynchronizationPanel.ID = "RecordMarkingAndSynchronizationPanel";

                // mark selected as (left side)
                Panel markSelectedAsPanel = new Panel();
                markSelectedAsPanel.ID = "MarkSelectedAsPanel";
                recordMarkingAndSynchronizationPanel.Controls.Add(markSelectedAsPanel);

                Label markSelectedAsText = new Label();
                markSelectedAsText.Text = _GlobalResources.MarkSelectedRecord_sAs + ": ";
                markSelectedAsPanel.Controls.Add(markSelectedAsText);

                DropDownList markSelectedAsDropDown = new DropDownList();
                markSelectedAsDropDown.ID = "MarkSelectedAsDropDown";
                markSelectedAsDropDown.Items.Add(new ListItem("", "-1"));
                markSelectedAsDropDown.Items.Add(new ListItem(_GlobalResources.Unknown, Convert.ToInt32(Lesson.LessonCompletionStatus.Unknown).ToString()));
                markSelectedAsDropDown.Items.Add(new ListItem(_GlobalResources.Completed, Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString()));
                markSelectedAsDropDown.Items.Add(new ListItem(_GlobalResources.Incomplete, Convert.ToInt32(Lesson.LessonCompletionStatus.Incomplete).ToString()));
                markSelectedAsDropDown.SelectedValue = "-1";
                markSelectedAsDropDown.Attributes.Add("onchange", "MarkSelectedAs(\"" + this._EnrolledGrid.ID + "\");");
                markSelectedAsPanel.Controls.Add(markSelectedAsDropDown);

                // allow synchronization with integration provider if it is that type of session and
                // the session has been over for more than 15 minutes

                DateTime meetingEnd = DateTime.MinValue;

                foreach (StandupTrainingInstance.MeetingTimeProperty meetTimeProperty in this._SessionObject.MeetingTimes)
                { meetingEnd = meetTimeProperty.DtEnd;  }

                if (AsentiaSessionState.UtcNow >= meetingEnd.AddMinutes(15))
                {
                    // synchronize with GTM, GTW, GTT or batch upload for all others (right side)
                    Panel synchronizeWithProviderPanel = new Panel();
                    synchronizeWithProviderPanel.ID = "SynchronizeWithProviderPanel";
                    recordMarkingAndSynchronizationPanel.Controls.Add(synchronizeWithProviderPanel);

                    Image synchronizeWithImageForLink = new Image();
                    synchronizeWithImageForLink.ID = "SynchronizeWithImageForLink";
                    synchronizeWithImageForLink.CssClass = "SmallIcon";

                    Localize synchronizeWithTextForLink = new Localize();                    

                    HyperLink synchronizeWithLink = new HyperLink();
                    synchronizeWithLink.ID = "SynchronizeWithLink";
                    synchronizeWithLink.CssClass = "ImageLink";                    

                    synchronizeWithLink.Controls.Add(synchronizeWithImageForLink);
                    synchronizeWithLink.Controls.Add(synchronizeWithTextForLink);

                    if (this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToMeeting)
                    {
                        synchronizeWithImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOMEETING, ImageFiles.EXT_PNG);
                        synchronizeWithTextForLink.Text = _GlobalResources.SynchronizeWithGoToMeeting;
                        synchronizeWithLink.Attributes.Add("onclick", "SynchronizeWithWebMeetingApi(" + this._SessionObject.Id.ToString() + ", \"" + this._SessionObject.IntegratedObjectKey.ToString() + "\", \"gotomeeting\");");
                    }
                    else if (this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar)
                    {
                        synchronizeWithImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOWEBINAR, ImageFiles.EXT_PNG);
                        synchronizeWithTextForLink.Text = _GlobalResources.SynchronizeWithGoToWebinar;
                        synchronizeWithLink.Attributes.Add("onclick", "SynchronizeWithWebMeetingApi(" + this._SessionObject.Id.ToString() + ", \"" + this._SessionObject.IntegratedObjectKey.ToString() + "\", \"gotowebinar\");");
                    }
                    else if (this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToTraining)
                    {
                        synchronizeWithImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOTRAINING, ImageFiles.EXT_PNG);
                        synchronizeWithTextForLink.Text = _GlobalResources.SynchronizeWithGoToTraining;
                        synchronizeWithLink.Attributes.Add("onclick", "SynchronizeWithWebMeetingApi(" + this._SessionObject.Id.ToString() + ", \"" + this._SessionObject.IntegratedObjectKey.ToString() + "\", \"gototraining\");");
                    }
                    //else if (this._SessionObject.Type == StandupTrainingInstance.MeetingType.WebEx)
                    //{
                        //synchronizeWithImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_WEBEX, ImageFiles.EXT_PNG);
                        //synchronizeWithTextForLink.Text = _GlobalResources.SynchronizeWithWebEx;
                        //synchronizeWithLink.Attributes.Add("onclick", "SynchronizeWithWebMeetingApi(" + this._SessionObject.Id.ToString() + ", \"" + this._SessionObject.IntegratedObjectKey.ToString() + "\", \"webex\");");
                    //}
                    else
                    {
                        // batch upload
                        synchronizeWithImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG);
                        synchronizeWithTextForLink.Text = _GlobalResources.BatchUpdate;
                        synchronizeWithLink.Attributes.Add("onclick", "$('#BatchUpdateModalPopupModalPopupFeedbackContainer').hide();");

                        // hidden button for launching to the modal to view batch update formatting instructions
                        this._HiddenFormattingInstructionsButton = new Button();
                        this._HiddenFormattingInstructionsButton.ID = "HiddenFormattingInstructionsButton";
                        this._HiddenFormattingInstructionsButton.Style.Add("display", "none");
                        synchronizeWithProviderPanel.Controls.Add(this._HiddenFormattingInstructionsButton);

                        this._BuildBatchUpdateModalPopup(synchronizeWithLink.ID, synchronizeWithProviderPanel);
                    }

                    synchronizeWithProviderPanel.Controls.Add(synchronizeWithLink);

                    // build the modal and trigger for the synchronization summary
                    
                    // trigger
                    Button hiddenButtonForSynchronizationSummaryModal = new Button();
                    hiddenButtonForSynchronizationSummaryModal.ID = "HiddenButtonForSynchronizationSummaryModal";
                    hiddenButtonForSynchronizationSummaryModal.Style.Add("display", "none");
                    synchronizeWithProviderPanel.Controls.Add(hiddenButtonForSynchronizationSummaryModal);

                    // modal
                    ModalPopup synchronizationSummaryModal = new ModalPopup("SynchronizationSummaryModal");
                    synchronizationSummaryModal.Type = ModalPopupType.EmbeddedForm;
                    synchronizationSummaryModal.HeaderIconPath = synchronizeWithImageForLink.ImageUrl;
                    synchronizationSummaryModal.HeaderIconAlt = synchronizeWithTextForLink.Text;
                    synchronizationSummaryModal.HeaderText = synchronizeWithTextForLink.Text;
                    synchronizationSummaryModal.TargetControlID = hiddenButtonForSynchronizationSummaryModal.ID;

                    // build the modal body

                    // synchronization summary header
                    Panel synchronizationSummaryModalHeaderPanel = new Panel();
                    synchronizationSummaryModalHeaderPanel.ID = "SynchronizationSummaryModal_HeaderPanel";
                    synchronizationSummaryModal.AddControlToBody(synchronizationSummaryModalHeaderPanel);

                    Literal synchronizationSummaryText = new Literal();
                    synchronizationSummaryText.Text = _GlobalResources.SynchronizationSummary;
                    synchronizationSummaryModalHeaderPanel.Controls.Add(synchronizationSummaryText);

                    // information panel
                    Panel synchronizationSummaryModalInfoPanel = new Panel();
                    synchronizationSummaryModalInfoPanel.ID = "SynchronizationSummaryModal_InfomationPanel";
                    synchronizationSummaryModal.AddControlToBody(synchronizationSummaryModalInfoPanel);

                    this.FormatPageInformationPanel(synchronizationSummaryModalInfoPanel, _GlobalResources.AfterClosingThisWindowYouMustClickTheSaveChangesButtonToCommitTheStatusChanges, true);

                    // synchronization failed
                    Panel synchronizationSummaryModalFailedWrapperPanel = new Panel();
                    synchronizationSummaryModalFailedWrapperPanel.ID = "SynchronizationSummaryModal_FailedWrapperPanel";
                    synchronizationSummaryModalFailedWrapperPanel.Style.Add("display", "none");
                    synchronizationSummaryModal.AddControlToBody(synchronizationSummaryModalFailedWrapperPanel);

                    Panel synchronizationSummaryModalFailedMessagePanel = new Panel();
                    synchronizationSummaryModalFailedMessagePanel.ID = "SynchronizationSummaryModal_FailedMessagePanel";
                    synchronizationSummaryModalFailedWrapperPanel.Controls.Add(synchronizationSummaryModalFailedMessagePanel);

                    Literal synchronizationFailedMessage = new Literal();
                    synchronizationFailedMessage.Text = _GlobalResources.TheSynchronizationFailedWithTheErrorMessageBelow;
                    synchronizationSummaryModalFailedMessagePanel.Controls.Add(synchronizationFailedMessage);

                    Panel synchronizationSummaryModalFailedDataPanel = new Panel();
                    synchronizationSummaryModalFailedDataPanel.ID = "SynchronizationSummaryModal_FailedDataPanel";
                    synchronizationSummaryModalFailedWrapperPanel.Controls.Add(synchronizationSummaryModalFailedDataPanel);
                    
                    // synchronization no users
                    Panel synchronizationSummaryModalNoUsersWrapperPanel = new Panel();
                    synchronizationSummaryModalNoUsersWrapperPanel.ID = "SynchronizationSummaryModal_NoUsersWrapperPanel";
                    synchronizationSummaryModalNoUsersWrapperPanel.Style.Add("display", "none");
                    synchronizationSummaryModal.AddControlToBody(synchronizationSummaryModalNoUsersWrapperPanel);

                    Panel synchronizationSummaryModalNoUsersMessagePanel = new Panel();
                    synchronizationSummaryModalNoUsersMessagePanel.ID = "SynchronizationSummaryModal_NoUsersMessagePanel";
                    synchronizationSummaryModalNoUsersWrapperPanel.Controls.Add(synchronizationSummaryModalNoUsersMessagePanel);

                    Literal synchronizationNoUsersMessage = new Literal();
                    synchronizationNoUsersMessage.Text = _GlobalResources.ThereWereNoUsersFoundInTheAttendeeList;
                    synchronizationSummaryModalNoUsersMessagePanel.Controls.Add(synchronizationNoUsersMessage);

                    // synchronization success
                    Panel synchronizationSummaryModalSuccessWrapperPanel = new Panel();
                    synchronizationSummaryModalSuccessWrapperPanel.ID = "SynchronizationSummaryModal_SuccessWrapperPanel";
                    synchronizationSummaryModalSuccessWrapperPanel.Style.Add("display", "none");
                    synchronizationSummaryModal.AddControlToBody(synchronizationSummaryModalSuccessWrapperPanel);

                    Panel synchronizationSummaryModalSuccessMarkedMessagePanel = new Panel();
                    synchronizationSummaryModalSuccessMarkedMessagePanel.ID = "SynchronizationSummaryModal_SuccessMarkedMessagePanel";
                    synchronizationSummaryModalSuccessWrapperPanel.Controls.Add(synchronizationSummaryModalSuccessMarkedMessagePanel);

                    Literal synchronizationSuccessMarkedMessage = new Literal();
                    synchronizationSuccessMarkedMessage.Text = _GlobalResources.TheFollowingUser_sAndTheirAttendanceDurationsWereMarkedAsCompletedOnTheRosterForm;
                    synchronizationSummaryModalSuccessMarkedMessagePanel.Controls.Add(synchronizationSuccessMarkedMessage);

                    Panel synchronizationSummaryModalSuccessMarkedDataPanel = new Panel();
                    synchronizationSummaryModalSuccessMarkedDataPanel.ID = "SynchronizationSummaryModal_SuccessMarkedDataPanel";
                    synchronizationSummaryModalSuccessWrapperPanel.Controls.Add(synchronizationSummaryModalSuccessMarkedDataPanel);

                    Panel synchronizationSummaryModalSuccessNotMarkedMessagePanel = new Panel();
                    synchronizationSummaryModalSuccessNotMarkedMessagePanel.ID = "SynchronizationSummaryModal_SuccessNotMarkedMessagePanel";
                    synchronizationSummaryModalSuccessWrapperPanel.Controls.Add(synchronizationSummaryModalSuccessNotMarkedMessagePanel);

                    Literal synchronizationSuccessNotMarkedMessage = new Literal();
                    synchronizationSuccessNotMarkedMessage.Text = _GlobalResources.TheFollowingUser_sAndTheirAttendanceDurationsAttendedTheSessionButWereNotFoundOnTheRoster;
                    synchronizationSummaryModalSuccessNotMarkedMessagePanel.Controls.Add(synchronizationSuccessNotMarkedMessage);

                    Panel synchronizationSummaryModalSuccessNotMarkedDataPanel = new Panel();
                    synchronizationSummaryModalSuccessNotMarkedDataPanel.ID = "SynchronizationSummaryModal_SuccessNotMarkedDataPanel";
                    synchronizationSummaryModalSuccessWrapperPanel.Controls.Add(synchronizationSummaryModalSuccessNotMarkedDataPanel);

                    // add modal to container
                    synchronizeWithProviderPanel.Controls.Add(synchronizationSummaryModal);
                }

                // add the panel to the controls list
                enrolledListInputControls.Add(recordMarkingAndSynchronizationPanel);
            }

            // add a hidden panel for "Enrolled List" print label to be shown only on print
            Panel enrolledListLabelForPrintPanel = new Panel();
            enrolledListLabelForPrintPanel.ID = "EnrolledListLabelForPrintPanel";
            enrolledListLabelForPrintPanel.Style.Add("display", "none");

            Literal enrolledListLabelForPrint = new Literal();
            enrolledListLabelForPrint.Text = _GlobalResources.EnrolledList;

            enrolledListLabelForPrintPanel.Controls.Add(enrolledListLabelForPrint);
            enrolledListInputControls.Add(enrolledListLabelForPrintPanel);

            // build enrollment user grid using "_RosterGridForEnrollment" table
            this._BuildRosterGrid(false, this._EnrolledGrid, this._EnrolledGrid.ID);

            // add the grid to the panel
            enrolledListInputControls.Add(this._EnrolledGrid);

            Panel addUsersToEnrolledListButtonsPanel = new Panel();
            addUsersToEnrolledListButtonsPanel.ID = "SelectUsers_ButtonsPanel";

            // select users button

            // link to popup list of users to be added
            Image addUsersToEnrolledListImageForLink = new Image();
            addUsersToEnrolledListImageForLink.ID = "LaunchSelectUsersModalImage";
            addUsersToEnrolledListImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);
            addUsersToEnrolledListImageForLink.CssClass = "MediumIcon";

            Localize addUsersToEnrolledListTextForLink = new Localize();
            addUsersToEnrolledListTextForLink.Text = _GlobalResources.AddUser_s;

            LinkButton selectUsersLink = new LinkButton();
            selectUsersLink.ID = "LaunchSelectUsersModal";
            selectUsersLink.CssClass = "ImageLink";
            selectUsersLink.OnClientClick = "LoadSelectUsersModal(); return false;";

            selectUsersLink.Controls.Add(addUsersToEnrolledListImageForLink);
            selectUsersLink.Controls.Add(addUsersToEnrolledListTextForLink);

            addUsersToEnrolledListButtonsPanel.Controls.Add(selectUsersLink);

            // attach the buttons panel to the container
            enrolledListInputControls.Add(addUsersToEnrolledListButtonsPanel);

            // build modals for adding and removing users
            this._BuildSelectUsersForEnrolledListModal(selectUsersLink.ID);

            // build modals for displaying the available seat info
            this._BuildNumberOfSeatsExceededModalPopup();

            enrolledListPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Enrolled_List",
                                                                                          String.Empty,
                                                                                          enrolledListInputControls,
                                                                                          false,
                                                                                          true));

            this.ManageRosterPropertiesTabPanelsContainer.Controls.Add(enrolledListPanel);
        }
        #endregion

        #region _BuildComposeMessageActionModal
        /// <summary>
        /// Build's compose message action modal.
        /// </summary>
        private void _BuildComposeMessageActionModal()
        {
            //hidden button used to open compose madal popup
            this._TargetButtonForComposeModal = new Button();
            this._TargetButtonForComposeModal.ID = "TargetButtonForComposeModal";
            this._TargetButtonForComposeModal.Attributes.Add("style", "display:none;");

            this.ManageRosterPropertiesActionsPanel.Controls.Add(this._TargetButtonForComposeModal);
            this._BuildComposeMessageModal(this._TargetButtonForComposeModal.ID);
        }
        #endregion

        #region _BuildComposeMessageModal
        /// <summary>
        /// Builds the modal for composing the message.
        /// </summary>
        private void _BuildComposeMessageModal(string targetControlid)
        {
            this._ComposeMessageModalPopup = new ModalPopup("ComposeMessageModalPopup");

            // set modal properties
            this._ComposeMessageModalPopup.Type = ModalPopupType.Form;
            this._ComposeMessageModalPopup.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL_CHECK,
                                                                                   ImageFiles.EXT_PNG);
            this._ComposeMessageModalPopup.HeaderIconAlt = _GlobalResources.SendMessageToRoster;
            this._ComposeMessageModalPopup.HeaderText = _GlobalResources.SendMessageToRoster;

            this._ComposeMessageModalPopup.TargetControlID = targetControlid;
            this._ComposeMessageModalPopup.ReloadPageOnClose = false;
            this._ComposeMessageModalPopup.SubmitButton.Visible = false;
            this._ComposeMessageModalPopup.CloseButton.Visible = false;
            this._ComposeMessageModalPopup.DisableSubmitButton();

            this._UpdateMessagePopupFieldsButton = new Button();
            this._UpdateMessagePopupFieldsButton.ID = "InboxMessageButton";
            this._UpdateMessagePopupFieldsButton.Attributes.Add("style", "display:none;");
            this._UpdateMessagePopupFieldsButton.Click += this._UpdateMessagePopupFieldsButton_Click;

            Panel composeMailActionsPanel = new Panel();
            composeMailActionsPanel.ID = "ComposeMailActionsPanel";

            this._SendButton = new Button();
            this._SendButton.Text = _GlobalResources.Send;
            this._SendButton.Command += new CommandEventHandler(this._SendButton_Command);
            this._SendButton.CssClass = "Button ActionButton";
            composeMailActionsPanel.Controls.Add(this._SendButton);

            Panel innerBodyWrapper = new Panel();
            innerBodyWrapper.ID = "InnerBodyWrapper";

            innerBodyWrapper.Controls.Add(this._UpdateMessagePopupFieldsButton);
            innerBodyWrapper.Controls.Add(this._MessageFormContainer);
            innerBodyWrapper.Controls.Add(composeMailActionsPanel);

            // add controls to body
            this._ComposeMessageModalPopup.AddControlToBody(innerBodyWrapper);

            this.ManageRosterPropertiesActionsPanel.Controls.Add(this._ComposeMessageModalPopup);
        }
        #endregion

        #region _BuildInputControls
        /// <summary>
        /// Method to add the input fields in compose message modal popup
        /// </summary>
        /// <param name="idInboxMessage"></param>
        private void _BuildInputControls()
        {
            #region Message To
            this._MessageFormContainer.ID = "messageFormContainer";

            // Main Field Container
            Panel messageToFieldContainer = new Panel();
            messageToFieldContainer.ID = "ComposeMessageTo";
            messageToFieldContainer.CssClass = "FormFieldContainer";

            // Recipient Type -  All Learners, Rostered Learners, Waitlist Learners
            this._RecipientType = new RadioButtonList();
            this._RecipientType.ID = "RecipientType_Field";

            ListItem allLearners = new ListItem();
            allLearners.Text = _GlobalResources.AllLearners;
            allLearners.Value = "0";
            allLearners.Attributes.Add("onclick", "RecipientTypeSelected(\"default\");");

            ListItem rosteredLearners = new ListItem();
            rosteredLearners.Text = _GlobalResources.RosteredLearners;
            rosteredLearners.Value = "1";
            rosteredLearners.Attributes.Add("onclick", "RecipientTypeSelected(\"default\");");

            ListItem waitlistLearners = new ListItem();
            waitlistLearners.Text = _GlobalResources.WaitlistLearners;
            waitlistLearners.Value = "2";
            waitlistLearners.Attributes.Add("onclick", "RecipientTypeSelected(\"default\");");


            this._RecipientType.Items.Add(allLearners);
            this._RecipientType.Items.Add(rosteredLearners);
            this._RecipientType.Items.Add(waitlistLearners);
            this._RecipientType.SelectedIndex = 0;

            this._MessageFormContainer.Controls.Add(AsentiaPage.BuildFormField("ComposeMessageTo",
                                                                                _GlobalResources.To,
                                                                                this._RecipientType.ID,
                                                                                this._RecipientType,
                                                                                true,
                                                                                true,
                                                                                false));
            
            #endregion Message To

            #region Recipient AutoComplete

            Panel RecipientAutoCompleteContainer = new Panel();
            RecipientAutoCompleteContainer.ID = "RecipientAutoCompleteContainer";
            RecipientAutoCompleteContainer.CssClass = "RecipientAutoCompleteContainer";
            RecipientAutoCompleteContainer.Attributes.Add("hidden", "true");

            this._MessageFormContainer.Controls.Add(RecipientAutoCompleteContainer);

            #endregion Recipient AutoComplete

            #region Message Subject

            // Main Field Container
            Panel messageSubjectFieldContainer = new Panel();
            messageSubjectFieldContainer.ID = "ComposeMessageSubject";
            messageSubjectFieldContainer.CssClass = "FormFieldContainer";

            //Input Field
            this._Subject = new TextBox();
            this._Subject.ID = "ComposeMessageSubject" + "_Field";
            this._Subject.Text = String.Empty;
            this._Subject.CssClass = "InputLong";
            this._MessageFormContainer.Controls.Add(AsentiaPage.BuildFormField("ComposeMessageSubject",
                                                                               _GlobalResources.Subject,
                                                                               this._Subject.ID,
                                                                               this._Subject,
                                                                               true,
                                                                               true,
                                                                               false));
            this._Subject.Enabled = true;            
            #endregion Message Subject

            #region Message Body

            // Main Field Container
            Panel messageBodyFieldContainer = new Panel();
            messageBodyFieldContainer.ID = "ComposeMessageBody";
            messageBodyFieldContainer.CssClass = "FormFieldContainer";

            //Input Field
            this._MessageBody = new TextBox();
            this._MessageBody.ID = "ComposeMessageBody" + "_Field";
            this._MessageBody.Text = String.Empty;
            this._MessageBody.Columns = 80;
            this._MessageBody.Rows = 4;
            this._MessageBody.TextMode = TextBoxMode.MultiLine;
            this._MessageFormContainer.Controls.Add(AsentiaPage.BuildFormField("ComposeMessageBody",
                                                                               _GlobalResources.Message,
                                                                                this._MessageBody.ID,
                                                                                this._MessageBody,
                                                                                true,
                                                                                true,
                                                                                false));


            #endregion Message Body
        }
        #endregion

        #region _SendButton_Command
        /// <summary>
        /// Actually Sends the message to the recipient
        /// </summary>
        private void _SendButton_Command(object sender, EventArgs e)
        {
            try
            {
                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                //string recipientText = this._MessageTo.Text.ToString();

                //created dtRostedUserList datatable to fill all user in roster list and waiting list
                DataTable dtRostedUserList = new DataTable();
                dtRostedUserList = this._StandupTrainingInstanceToUserLinkObject._GetManageRosterUserList(this._SessionObject.Id, false);
                DataTable dtWaitingListUserList = new DataTable();
                dtWaitingListUserList = this._StandupTrainingInstanceToUserLinkObject._GetManageRosterUserList(this._SessionObject.Id, true);
                int idInboxMessage = 0;

                // send email to the rosted learners 
                if ((this._RecipientType.SelectedValue == "0" || this._RecipientType.SelectedValue == "1") && dtRostedUserList.Rows.Count > 0)
                {
                    // send mail to every rosted learners through the userid
                    foreach (DataRow drUser in dtRostedUserList.Rows)
                    {
                        idInboxMessage = 0;
                        this._InboxMessageObject = new InboxMessage();
                        this._InboxMessageObject.IdParentInboxMessage = null;
                        this._InboxMessageObject.IdRecipient = Convert.ToInt32(drUser["idUser"]);
                        this._InboxMessageObject.Subject = this._Subject.Text;
                        this._InboxMessageObject.Message = this._MessageBody.Text;
                        this._InboxMessageObject.IsDraft = false;
                        this._InboxMessageObject.IsSent = false;
                        this._InboxMessageObject.IsRead = false;
                        this._InboxMessageObject.IsRecipientDeleted = false;
                        this._InboxMessageObject.IsSenderDeleted = false;

                        // logged in user is the sender of the message
                        this._InboxMessageObject.IdSender = AsentiaSessionState.IdSiteUser;

                        // save message
                        idInboxMessage = this._InboxMessageObject.Save();

                        // send saved message
                        this._InboxMessageObject.Send(idInboxMessage);
                    }
                }

                // send email to the waiting list learners 
                if ((this._RecipientType.SelectedValue == "0" || this._RecipientType.SelectedValue == "2") && dtWaitingListUserList.Rows.Count > 0)
                {
                    // send mail to every rosted learners through the userid
                    foreach (DataRow drUser in dtWaitingListUserList.Rows)
                    {
                        idInboxMessage = 0;
                        this._InboxMessageObject = new InboxMessage();
                        this._InboxMessageObject.IdParentInboxMessage = null;
                        this._InboxMessageObject.IdRecipient = Convert.ToInt32(drUser["idUser"]);
                        this._InboxMessageObject.Subject = this._Subject.Text;
                        this._InboxMessageObject.Message = this._MessageBody.Text;
                        this._InboxMessageObject.IsDraft = false;
                        this._InboxMessageObject.IsSent = false;
                        this._InboxMessageObject.IsRead = false;
                        this._InboxMessageObject.IsRecipientDeleted = false;
                        this._InboxMessageObject.IsSenderDeleted = false;

                        // logged in user is the sender of the message
                        this._InboxMessageObject.IdSender = AsentiaSessionState.IdSiteUser;

                        // save message
                        idInboxMessage = this._InboxMessageObject.Save();

                        // send saved message
                        this._InboxMessageObject.Send(idInboxMessage);
                    }
                }

                if (idInboxMessage > 0)
                {
                    //this._EnableOrDisableTextBox(true, true, true);
                    this._ComposeMessageModalPopup.DisplayFeedback(_GlobalResources.MessageHasBeenSentSuccessfully, false);
                }
                else
                {
                    this._ComposeMessageModalPopup.DisplayFeedback(_GlobalResources.UnableToSendMessagePleaseTryAgain, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ComposeMessageModalPopup.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
            finally
            {

            }
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;

            // Subject Field
            if (String.IsNullOrWhiteSpace(this._Subject.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._MessageFormContainer, "ComposeMessageSubject", _GlobalResources.Subject + " " + _GlobalResources.IsRequired);
            }

            // Body Field
            if (String.IsNullOrWhiteSpace(this._MessageBody.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._MessageFormContainer, "ComposeMessageBody", _GlobalResources.Message + " " + _GlobalResources.IsRequired);
            }
            return isValid;
        }
        #endregion

        #region _UpdateMessagePopupFieldsButton_Click
        /// <summary>
        /// Update/Reset input field of message popup
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void _UpdateMessagePopupFieldsButton_Click(object sender, EventArgs e)
        {
            this._MessageFormContainer.Controls.Clear();
            this._ComposeMessageModalPopup.ClearFeedback();
            this._BuildInputControls();
        }
        #endregion

        #region _HiddenSuccessButton_Command
        /// <summary>
        /// Displays feedback in the batch update modal for success
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _HiddenSuccessButton_Command(object sender, CommandEventArgs e)
        {
            this._BatchUpdateModal.DisplayFeedback(_GlobalResources.TheRosterWasUpdatedSuccessfullyPleaseBeSureToClickSaveChangesOnTheManageRosterPageToSaveTheRosterUpdates, false);
        }

        #endregion

        #region _HiddenFailureButton_Command
        /// <summary>
        /// Displays feedback in the back update modal for failure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _HiddenFailureButton_Command(object sender, CommandEventArgs e)
        {
            this._BatchUpdateModal.DisplayFeedback(this._BatchUploadError.Value, true);
        }

        #endregion

        #region _BuildSelectUsersForEnrolledListModal
        /// <summary>
        /// Builds the modal for selecting users to add to the enrolled list.
        /// </summary>
        private void _BuildSelectUsersForEnrolledListModal(string targetControlId)
        {
            // set modal properties
            this._SelectUsersForEnrolledList = new ModalPopup("SelectUsersForEnrolledListModal");
            this._SelectUsersForEnrolledList.Type = ModalPopupType.Form;
            this._SelectUsersForEnrolledList.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR, ImageFiles.EXT_PNG);
            this._SelectUsersForEnrolledList.HeaderIconAlt = _GlobalResources.SelectUser_s;
            this._SelectUsersForEnrolledList.HeaderText = _GlobalResources.SelectUser_s;
            this._SelectUsersForEnrolledList.TargetControlID = targetControlId;
            this._SelectUsersForEnrolledList.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectUsersForEnrolledList.SubmitButtonCustomText = _GlobalResources.AddUser_s;

            this._SelectUsersForEnrolledList.SubmitButton.OnClientClick = "javascript:AddUsersToEnrolledList(" + this._HasSessionAlreadyOccurred.ToString().ToLower() + ", \"" + this._EnrolledGrid.ID + "\", \"" + this._WaitlistGrid.ID + "\"); return false;";
            this._SelectUsersForEnrolledList.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectUsersForEnrolledList.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectUsersForEnrolledList.ReloadPageOnClose = false;

            // build a container for the user listing
            this._SelectAddEnrolledListUsers = new DynamicListBox("SelectManageRosterUsers");
            this._SelectAddEnrolledListUsers.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectAddEnrolledListUsers.SearchButton.Command += new CommandEventHandler(this._SearchSelectUsersButton_Command);
            this._SelectAddEnrolledListUsers.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectUsersButton_Command);            
            this._SelectAddEnrolledListUsers.ListBoxControl.DataSource = this._ManageRosterUsersForSelectList;
            this._SelectAddEnrolledListUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectAddEnrolledListUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectAddEnrolledListUsers.ListBoxControl.DataBind();
            

            // bind an email attribute to the list of users, needed for web meeting provider integration
            for (int i = 0; i < this._SelectAddEnrolledListUsers.ListBoxControl.Items.Count; i++)
            { this._SelectAddEnrolledListUsers.ListBoxControl.Items[i].Attributes.Add("email", this._ManageRosterUsersForSelectList.Rows[i]["registrantEmail"].ToString()); }                

            // add controls to body
            this._SelectUsersForEnrolledList.AddControlToBody(this._SelectAddEnrolledListUsers);

            this._EnrolledUsersButton = new Button();
            this._EnrolledUsersButton.ID = "EnrolledUsersButton";
            this._EnrolledUsersButton.Text = _GlobalResources.EnrolledUsers;
            this._EnrolledUsersButton.ToolTip = _GlobalResources.ShowUsersEnrolledInCoursesWithThisILTAttached;
            this._EnrolledUsersButton.CssClass = "Button";
            this._EnrolledUsersButton.Click += this._EnrolledUsersButton_Click;
            this._SelectUsersForEnrolledList.AddControlToBody(this._EnrolledUsersButton);

            // hidden button to load the users when modal is launched
            Button hiddenLoadUsersButton = new Button();
            hiddenLoadUsersButton.ID = "HiddenLoadUsersButton";
            hiddenLoadUsersButton.Style.Add("display", "none");
            hiddenLoadUsersButton.Click += this._HiddenLoadUsersButton_Click;
            this._SelectUsersForEnrolledList.AddControlToBody(hiddenLoadUsersButton);


            // add modal to container
            this.ManageRosterPropertiesTabPanelsContainer.Controls.Add(this._SelectUsersForEnrolledList);
        }
        #endregion

        #region _HiddenLoadUsersButton_Click
        /// <summary>
        /// Click event for loading the select users modal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _HiddenLoadUsersButton_Click(object sender, EventArgs e)
        {
            if (this._SessionObject != null)
            { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForManageRosterList(this._SessionObject.Id, null); }
            else
            { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForManageRosterList(0, null); }
            this._SelectAddEnrolledListUsers.ListBoxControl.DataSource = this._ManageRosterUsersForSelectList;
            this._SelectAddEnrolledListUsers.ListBoxControl.DataBind();

            this._EnrolledUsersButton.Text = _GlobalResources.EnrolledUsers;
            this._EnrolledUsersButton.ToolTip = _GlobalResources.ShowUsersEnrolledInCoursesWithThisILTAttached;
            
        }

        #endregion

        #region _EnrolledUsersButton_Click
        /// <summary>
        /// Click event for showing enrolled users
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _EnrolledUsersButton_Click(object sender, EventArgs e)
        {
            

            if (this._EnrolledUsersButton.Text == _GlobalResources.AllUsers)
            {
                if (this._SessionObject != null)
                { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForManageRosterList(this._SessionObject.Id, null); }
                else
                { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForManageRosterList(0, null); }
                this._SelectAddEnrolledListUsers.ListBoxControl.DataSource = this._ManageRosterUsersForSelectList;
                this._SelectAddEnrolledListUsers.ListBoxControl.DataBind();
                this._EnrolledUsersButton.Text = _GlobalResources.EnrolledUsers;
                this._EnrolledUsersButton.ToolTip = _GlobalResources.ShowUsersEnrolledInCoursesWithThisILTAttached;
            }
            else
            {
                this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.EnrolledUsersIdsAndNamesForManageRosterList(this._SessionObject.IdStandupTraining, this._SessionObject.Id, null);
                this._SelectAddEnrolledListUsers.ListBoxControl.DataSource = this._ManageRosterUsersForSelectList;
                this._SelectAddEnrolledListUsers.ListBoxControl.DataBind();
                this._EnrolledUsersButton.Text = _GlobalResources.AllUsers;
                this._EnrolledUsersButton.ToolTip = _GlobalResources.ShowAllUsers;
            }
        }
        #endregion

        #region _SearchSelectUsersButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select User(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectUsersButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectUsersForEnrolledList.ClearFeedback();

            // clear the listbox control
            this._SelectAddEnrolledListUsers.ListBoxControl.Items.Clear();

            // do the search
            if (this._EnrolledUsersButton.Text == _GlobalResources.EnrolledUsers)
            {
                if (this._SessionObject != null)
                { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForManageRosterList(this._SessionObject.Id, this._SelectAddEnrolledListUsers.SearchTextBox.Text); }
                else
                { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForManageRosterList(0, this._SelectAddEnrolledListUsers.SearchTextBox.Text); }
            }
            else
            {
                if (this._SessionObject != null)
                { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.EnrolledUsersIdsAndNamesForManageRosterList(this._SessionObject.IdStandupTraining, this._SessionObject.Id, this._SelectAddEnrolledListUsers.SearchTextBox.Text); }
                else
                { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.EnrolledUsersIdsAndNamesForManageRosterList(0, 0, this._SelectAddEnrolledListUsers.SearchTextBox.Text); }
            }

            this._SelectAddEnrolledListUsers.ListBoxControl.DataSource = this._ManageRosterUsersForSelectList;
            this._SelectAddEnrolledListUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectAddEnrolledListUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectAddEnrolledListUsers.ListBoxControl.DataBind();

            // bind an email attribute to the list of users, needed for web meeting provider integration
            for (int i = 0; i < this._SelectAddEnrolledListUsers.ListBoxControl.Items.Count; i++)
            { this._SelectAddEnrolledListUsers.ListBoxControl.Items[i].Attributes.Add("email", this._ManageRosterUsersForSelectList.Rows[i]["registrantEmail"].ToString()); }                

            // if no records available then disable the list
            if (this._SelectAddEnrolledListUsers.ListBoxControl.Items.Count == 0)
            {
                this._SelectUsersForEnrolledList.SubmitButton.Enabled = false;
                this._SelectUsersForEnrolledList.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectUsersForEnrolledList.SubmitButton.Enabled = true;
                this._SelectUsersForEnrolledList.SubmitButton.CssClass = "Button ActionButton SaveButton";
            }
        }
        #endregion

        #region _ClearSearchSelectUsersButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Users" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectUsersButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectUsersForEnrolledList.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectAddEnrolledListUsers.ListBoxControl.Items.Clear();
            this._SelectAddEnrolledListUsers.SearchTextBox.Text = "";

            // clear the search
            if (this._EnrolledUsersButton.Text == _GlobalResources.EnrolledUsers)
            {
                if (this._SessionObject != null)
                { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForManageRosterList(this._SessionObject.Id, null); }
                else
                { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.IdsAndNamesForManageRosterList(0, null); }
            }
            else
            {
                if (this._SessionObject != null)
                { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.EnrolledUsersIdsAndNamesForManageRosterList(this._SessionObject.IdStandupTraining, this._SessionObject.Id, null); }
                else
                { this._ManageRosterUsersForSelectList = Asentia.UMS.Library.User.EnrolledUsersIdsAndNamesForManageRosterList(0, 0, null); }
            }

            this._SelectAddEnrolledListUsers.ListBoxControl.DataSource = this._ManageRosterUsersForSelectList;
            this._SelectAddEnrolledListUsers.ListBoxControl.DataTextField = "displayName";
            this._SelectAddEnrolledListUsers.ListBoxControl.DataValueField = "idUser";
            this._SelectAddEnrolledListUsers.ListBoxControl.DataBind();

            // bind an email attribute to the list of users, needed for web meeting provider integration
            for (int i = 0; i < this._SelectAddEnrolledListUsers.ListBoxControl.Items.Count; i++)
            { this._SelectAddEnrolledListUsers.ListBoxControl.Items[i].Attributes.Add("email", this._ManageRosterUsersForSelectList.Rows[i]["registrantEmail"].ToString()); }

            // if records available then enable the list
            if (this._SelectAddEnrolledListUsers.ListBoxControl.Items.Count > 0)
            {
                this._SelectUsersForEnrolledList.SubmitButton.Enabled = true;
                this._SelectUsersForEnrolledList.SubmitButton.CssClass = "Button ActionButton SaveButton";
            }
            else
            {
                this._SelectUsersForEnrolledList.SubmitButton.Enabled = false;
                this._SelectUsersForEnrolledList.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _BuildRosterFormWaitingListPanel
        /// <summary>
        /// Builds the waiting list panel.
        /// </summary>
        private void _BuildRosterFormWaitingListPanel()
        {
            Panel resourcesPanel = new Panel();
            resourcesPanel.ID = "ManageRosterProperties_" + "WaitingList" + "_TabPanel";
            resourcesPanel.Attributes.Add("style", "display: none;");

            List<Control> waitListInputControls = new List<Control>();

            // add a hidden panel for "Waiting List" print label to be shown only on print
            Panel waitingListLabelForPrintPanel = new Panel();
            waitingListLabelForPrintPanel.ID = "WaitingListLabelForPrintPanel";
            waitingListLabelForPrintPanel.Style.Add("display", "none");

            Literal waitingListLabelForPrint = new Literal();
            waitingListLabelForPrint.Text = _GlobalResources.WaitingList;

            waitingListLabelForPrintPanel.Controls.Add(waitingListLabelForPrint);
            waitListInputControls.Add(waitingListLabelForPrintPanel);

            // build wait list user grid using "_RosterGridForWaitList" table
            this._BuildRosterGrid(true, this._WaitlistGrid, this._WaitlistGrid.ID);

            //add the created "_RosterGridForEnrollment" grid to the panel
            waitListInputControls.Add(this._WaitlistGrid);

            resourcesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("WaitList_List",
                                                                                       String.Empty,
                                                                                       waitListInputControls,
                                                                                       false,
                                                                                       true));

            this.ManageRosterPropertiesTabPanelsContainer.Controls.Add(resourcesPanel);
        }
        #endregion

        #region _BuildRosterGrid
        /// <summary>
        /// Builds the roster grid for both enrolled and wait lists. 
        /// </summary>
        private void _BuildRosterGrid(bool isWaitListGrid, Table destinationTable, string destinationTableId)
        {
            // fill the data table of list with roster users 
            if (this._SessionObject != null)
            {
                this._StandupTrainingInstanceToUserLinkObject = new Library.StandUpTrainingInstanceToUserLink();
                this._RosterUserList = this._StandupTrainingInstanceToUserLinkObject._GetManageRosterUserList(this._SessionObject.Id, isWaitListGrid);
            }

            // apply grid class to table
            destinationTable.CssClass = "GridTable";

            // create the header row
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.ID = destinationTableId + "_HeaderRow";
            headerRow.CssClass = "GridHeaderRow";

            // select all header cell
            CheckBox selectAllRecords = new CheckBox();
            selectAllRecords.ID = destinationTableId + "_GridSelectAllRecords";
            selectAllRecords.Attributes.Add("onclick", "Grid.ToggleSelectAllRecords(\"" + destinationTableId + "\", this);");

            TableHeaderCell headerCellSelectAll = new TableHeaderCell();
            headerCellSelectAll.CssClass = "GridCheckboxColumn GridHeaderNotForPrint";
            headerCellSelectAll.Controls.Add(selectAllRecords);
            headerRow.Cells.Add(headerCellSelectAll);

            // name header cell
            TableHeaderCell headerCellName = new TableHeaderCell();
            headerCellName.CssClass = "GridHeaderNotForPrint";
            headerCellName.Text = _GlobalResources.Name;
            headerRow.Cells.Add(headerCellName);

            TableHeaderCell headerCellNumberForPrint = new TableHeaderCell();
            headerCellNumberForPrint.CssClass = "GridHeaderForPrint";
            headerCellNumberForPrint.Style.Add("display", "none");
            headerRow.Cells.Add(headerCellNumberForPrint);

            // hidden name header cell for print
            TableHeaderCell headerCellNameForPrint = new TableHeaderCell();
            headerCellNameForPrint.Text = _GlobalResources.LastName + ", " + _GlobalResources.FirstName + " (" + _GlobalResources.Username + ")";
            headerCellNameForPrint.CssClass = "GridHeaderForPrint";
            headerCellNameForPrint.Style.Add("display", "none");
            headerRow.Cells.Add(headerCellNameForPrint);

            // hidden email header cell for print
            TableHeaderCell headerCellEmailForPrint = new TableHeaderCell();
            headerCellEmailForPrint.Text = _GlobalResources.EmailAddress;
            headerCellEmailForPrint.CssClass = "GridHeaderForPrint";
            headerCellEmailForPrint.Style.Add("display", "none");
            headerRow.Cells.Add(headerCellEmailForPrint);

            TableHeaderCell headerCellSignatureForPrint = new TableHeaderCell();
            headerCellSignatureForPrint.Text = String.Empty;
            headerCellSignatureForPrint.CssClass = "GridHeaderForPrint";
            headerCellSignatureForPrint.Style.Add("display", "none");
            headerRow.Cells.Add(headerCellSignatureForPrint);

            // completion status header cell - show only if enrolled grid and session has already occurred
            TableHeaderCell headerCellCompletionStatus = new TableHeaderCell();
            headerCellCompletionStatus.Text = _GlobalResources.CompletionStatus;
            headerCellCompletionStatus.CssClass = "GridHeaderNotForPrint";

            if (!this._HasSessionAlreadyOccurred || isWaitListGrid)
            { headerCellCompletionStatus.Style.Add("display", "none"); }

            headerRow.Cells.Add(headerCellCompletionStatus);

            // score header cell - show only if enrolled grid and session has already occurred
            TableHeaderCell headerCellScore = new TableHeaderCell();
            headerCellScore.Text = _GlobalResources.Score;
            headerCellScore.CssClass = "GridHeaderNotForPrint";

            if (!this._HasSessionAlreadyOccurred || isWaitListGrid)
            { headerCellScore.Style.Add("display", "none"); }

            headerRow.Cells.Add(headerCellScore);

            // promote/demote header cell
            if (isWaitListGrid)
            {
                // promote header cell
                TableHeaderCell headerCellDemote = new TableHeaderCell();
                headerCellDemote.CssClass = "centered GridHeaderNotForPrint";
                headerCellDemote.Text = _GlobalResources.Promote;
                headerRow.Cells.Add(headerCellDemote);
            }
            else
            {
                // demote header cell
                TableHeaderCell headerCellPromote = new TableHeaderCell();
                headerCellPromote.CssClass = "centered GridHeaderNotForPrint";
                headerCellPromote.Text = _GlobalResources.Demote;
                headerRow.Cells.Add(headerCellPromote);
            }

            // drop header cell
            TableHeaderCell headerCellDrop = new TableHeaderCell();
            headerCellDrop.CssClass = "centered GridHeaderNotForPrint";
            headerCellDrop.Text = _GlobalResources.Drop;
            headerRow.Cells.Add(headerCellDrop);

            // attach header row to table
            destinationTable.Rows.Add(headerRow);

            // if there are no records, show the no records found message
            if (this._RosterUserList.Rows.Count == 0)
            {
                TableRow noRecordsFoundRow = new TableRow();
                noRecordsFoundRow.ID = destinationTableId + "_GridRow_0";
                noRecordsFoundRow.CssClass = "GridDataRowEmpty";

                TableCell noRecordsFoundCell = new TableCell();
                noRecordsFoundCell.Text = _GlobalResources.NoRecordsFound;

                if (!isWaitListGrid)
                {
                    if (this._HasSessionAlreadyOccurred)
                    { noRecordsFoundCell.ColumnSpan = 6; }
                    else
                    { noRecordsFoundCell.ColumnSpan = 4; }
                }
                else
                { noRecordsFoundCell.ColumnSpan = 4; }

                noRecordsFoundRow.Cells.Add(noRecordsFoundCell);

                // attach row to table
                destinationTable.Rows.Add(noRecordsFoundRow);
            }

            // loop through each user in the roster and add to table
            int i = 0;

            foreach (DataRow row in this._RosterUserList.Rows)
            {
                int idUser = Convert.ToInt32(row["idUser"]);

                TableRow dataRow = new TableRow();
                dataRow.ID = destinationTableId + "_GridRow_" + i.ToString();
                dataRow.Style.Add("cursor", "pointer");
                dataRow.Attributes.Add("onmouseover", "Grid.HoverRowOn(this);");
                dataRow.Attributes.Add("onmouseout", "Grid.HoverRowOff(\"" + destinationTableId + "\", this, " + i.ToString() + ");");
                dataRow.Attributes.Add("onclick", "Grid.ToggleSelectRecord(\"" + destinationTableId + "\", document.getElementById(\"" + destinationTableId + "_GridSelectRecord_" + i.ToString() + "\"), 0, true);");

                // apply css for alternating rows
                if (i % 2 == 0)
                { dataRow.CssClass = "GridDataRow"; }
                else
                { dataRow.CssClass = "GridDataRow GridDataRowAlternate"; }

                // select record cell
                CheckBox selectRecord = new CheckBox();
                selectRecord.ID = destinationTableId + "_GridSelectRecord_" + i.ToString();
                selectRecord.Attributes.Add("onclick", "Grid.ToggleSelectRecord(\"" + destinationTableId + "\", this, " + i.ToString() + ", false);");
                selectRecord.InputAttributes.Add("registrantkey", row["registrantKey"].ToString());
                selectRecord.InputAttributes.Add("registrantemail", row["registrantEmail"].ToString());
                selectRecord.InputAttributes.Add("liststatus", "existing");
                selectRecord.InputAttributes.Add("value", idUser.ToString());
                selectRecord.InputAttributes.Add("username", row["username"].ToString());

                TableCell cellSelect = new TableCell();
                cellSelect.CssClass = "GridCheckboxColumn";
                cellSelect.Controls.Add(selectRecord);
                dataRow.Cells.Add(cellSelect);

                // add a hidden cell for number and display name label to be shown only on print
                TableCell cellNumber = new TableCell();
                cellNumber.CssClass = "GridBodyForPrint RowOrdinalPanel";
                cellNumber.Style.Add("display", "none");
                Label labelNumber = new Label();
                labelNumber.CssClass = destinationTableId +"Ordinal";
                labelNumber.Text = (i + 1).ToString() + ". ";
                cellNumber.Controls.Add(labelNumber);
                dataRow.Cells.Add(cellNumber);

                // name cell
                TableCell cellName = new TableCell();
                cellNumber.CssClass = "GridBodyForPrint";
                cellName.Text = row["displayName"].ToString();
                dataRow.Cells.Add(cellName);

                // add a hidden cell for email label to be shown only on print
                TableCell cellEmail = new TableCell();
                cellEmail.CssClass = "GridBodyForPrint";
                cellEmail.Style.Add("display", "none");
                cellEmail.Text = row["email"].ToString();
                dataRow.Cells.Add(cellEmail);

                // add a hidden cell for signature label to be shown only on print
                TableCell cellSign = new TableCell();
                cellSign.CssClass = "GridBodyForPrint";
                cellSign.Style.Add("display", "none");
                cellSign.Text = "_____________________";
                dataRow.Cells.Add(cellSign);

                // completion status cell - show only if enrolled grid and session has already occurred
                TableCell cellCompletionStatus = new TableCell();

                if (!this._HasSessionAlreadyOccurred || isWaitListGrid)
                { cellCompletionStatus.Style.Add("display", "none"); }

                DropDownList completionStatusDropDown = new DropDownList();
                completionStatusDropDown.ID = destinationTableId + "_CompletionStatusDropDown_" + i.ToString();
                completionStatusDropDown.Items.Add(new ListItem(_GlobalResources.Unknown, Convert.ToInt32(Lesson.LessonCompletionStatus.Unknown).ToString()));
                completionStatusDropDown.Items.Add(new ListItem(_GlobalResources.Completed, Convert.ToInt32(Lesson.LessonCompletionStatus.Completed).ToString()));
                completionStatusDropDown.Items.Add(new ListItem(_GlobalResources.Incomplete, Convert.ToInt32(Lesson.LessonCompletionStatus.Incomplete).ToString()));
                completionStatusDropDown.SelectedValue = row["completionStatus"].ToString();

                cellCompletionStatus.Controls.Add(completionStatusDropDown);
                dataRow.Cells.Add(cellCompletionStatus);

                // score cell - show only if enrolled grid and session has already occurred
                TableCell cellScore = new TableCell();

                if (!this._HasSessionAlreadyOccurred || isWaitListGrid)
                { cellScore.Style.Add("display", "none"); }

                TextBox scoreTextBox = new TextBox();
                scoreTextBox.ID = destinationTableId + "_ScoreTextBox_" + i.ToString();
                scoreTextBox.CssClass = "InputXShort";
                scoreTextBox.Text = row["score"].ToString();
                cellScore.Controls.Add(scoreTextBox);

                dataRow.Cells.Add(cellScore);

                // promote/demote cell
                if (isWaitListGrid)
                {
                    TableCell cellPromote = new TableCell();
                    cellPromote.CssClass = "centered";

                    Image imgPromote = new Image();
                    imgPromote.ID = destinationTableId + "_PromoteButton_" + i.ToString();
                    imgPromote.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PROMOTE, ImageFiles.EXT_PNG);
                    imgPromote.Attributes.Add("onclick", "PromoteUserFromWaitList(" + this._HasSessionAlreadyOccurred.ToString().ToLower() + ", \"" + this._EnrolledGrid.ID + "\", \"" + this._WaitlistGrid.ID + "\", " + i.ToString() + ")");
                    imgPromote.Style.Add("cursor", "pointer");
                    imgPromote.CssClass = "SmallIcon";

                    cellPromote.Controls.Add(imgPromote);
                    dataRow.Cells.Add(cellPromote);
                }
                else
                {
                    TableCell cellDemote = new TableCell();
                    cellDemote.CssClass = "centered";

                    Image imgDemote = new Image();
                    imgDemote.ID = destinationTableId + "_DemoteButton_" + i.ToString();
                    imgDemote.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DEMOTE, ImageFiles.EXT_PNG);
                    imgDemote.Attributes.Add("onclick", "DemoteUserFromEnrolledList(" + this._HasSessionAlreadyOccurred.ToString().ToLower() + ", \"" + this._EnrolledGrid.ID + "\", \"" + this._WaitlistGrid.ID + "\", " + i.ToString() + ")");
                    imgDemote.Style.Add("cursor", "pointer");
                    imgDemote.CssClass = "SmallIcon";

                    cellDemote.Controls.Add(imgDemote);
                    dataRow.Cells.Add(cellDemote);
                }

                // drop cell
                TableCell cellDrop = new TableCell();
                cellDrop.CssClass = "centered";

                Image imgDrop = new Image();
                imgDrop.ID = destinationTableId + "_DropButton_" + i.ToString();
                imgDrop.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                imgDrop.Attributes.Add("onclick", "RemoveUserFromList(\"" + destinationTableId + "\", " + i.ToString() + ", true)");
                imgDrop.Style.Add("cursor", "pointer");
                imgDrop.CssClass = "SmallIcon";

                cellDrop.Controls.Add(imgDrop);
                dataRow.Cells.Add(cellDrop);

                // attach row to table
                destinationTable.Rows.Add(dataRow);

                // increment i
                i++;
            }
        }
        #endregion

        #region _BuildNumberOfSeatsExceededModalPopup
        /// <summary>
        /// Builds the modal indicating that the number of seats has been exceeded. 
        /// </summary>
        private void _BuildNumberOfSeatsExceededModalPopup()
        {
            // set modal properties
            this._NumberOfSeatsExceeded = new ModalPopup("NumberOfSeatsExceeded");
            this._NumberOfSeatsExceeded.Type = ModalPopupType.Information;
            this._NumberOfSeatsExceeded.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._NumberOfSeatsExceeded.HeaderIconAlt = _GlobalResources.Error;
            this._NumberOfSeatsExceeded.HeaderText = _GlobalResources.Error;

            this._LaunchSeatsExceededButton = new Button();
            this._LaunchSeatsExceededButton.ID = "LaunchSeatsExceededButton";
            this._LaunchSeatsExceededButton.Attributes.Add("style", "display:none;");

            this._NumberOfSeatsExceeded.TargetControlID = this._LaunchSeatsExceededButton.ID;

            this._EnrolledListSeatsExceededPanel = new Panel();
            this._EnrolledListSeatsExceededPanel.ID = "EnrolledListSeatsExceededPanel";
            this.FormatPageInformationPanel(this._EnrolledListSeatsExceededPanel, String.Format(_GlobalResources.OnlyXUser_sCanBeAddedOrMovedIntoTheActiveRosterList, this._SessionObject.Seats), true);

            this._WaitlistSeatsExceededPanel = new Panel();
            this._WaitlistSeatsExceededPanel.ID = "WaitlistSeatsExceededPanel";
            this.FormatPageInformationPanel(this._WaitlistSeatsExceededPanel, string.Format(_GlobalResources.OnlyXUser_sCanBeAddedOrMovedIntoTheWaitingList, this._SessionObject.WaitingSeats), true);

            // add controls to body
            this._NumberOfSeatsExceeded.AddControlToBody(this._LaunchSeatsExceededButton);
            this._NumberOfSeatsExceeded.AddControlToBody(this._EnrolledListSeatsExceededPanel);
            this._NumberOfSeatsExceeded.AddControlToBody(this._WaitlistSeatsExceededPanel);

            // add modal to container
            this.ManageRosterPropertiesTabPanelsContainer.Controls.Add(this._NumberOfSeatsExceeded);
        }
        #endregion

        #region _BuildSesssionPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for properties actions.
        /// </summary>
        private void _BuildSessionPropertiesActionsPanel()
        {
            // clear controls from container
            this.ManageRosterPropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.ManageRosterPropertiesActionsPanel.CssClass = "ActionsPanel";

            // add hidden field for JSON data
            this._EnrolledAndWaitListJsonData = new HiddenField();
            this._EnrolledAndWaitListJsonData.ID = "EnrolledAndWaitListJsonData";
            this.ManageRosterPropertiesActionsPanel.Controls.Add(this._EnrolledAndWaitListJsonData);

            // add hidden field for dropped registrant keys
            this._DroppedRegistrantKeys = new HiddenField();
            this._DroppedRegistrantKeys.ID = "DroppedRegistrantKeys";
            this.ManageRosterPropertiesActionsPanel.Controls.Add(this._DroppedRegistrantKeys);

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this._SaveButton.OnClientClick = "CompileJSONDataForSave();";
            this.ManageRosterPropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.ManageRosterPropertiesActionsPanel.Controls.Add(this._CancelButton);            
        }
        #endregion

        #region _StandupTrainingToUserLinkDataObject CLASS
        /// <summary>
        /// Used to receive the json stringified data with enrolled/wait list changes.
        /// </summary>    
        private class _StandupTrainingToUserLinkDataJSONObject
        {
            public int OrdinalIndex;
            public int IdUser;
            public string RegistrantKey;    // this is an Int64, but needs to be treated as a string in JSON because JS does not have the concept of a long int
            public string RegistrantEmail;
            public int CompletionStatus = 0;
            public int? Score;
            public bool IsWaitList;
            public string ListStatus;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            
            try
            {                
                string jsonStringifiedData = this._EnrolledAndWaitListJsonData.Value;
                string droppedRegistrantKeysData = this._DroppedRegistrantKeys.Value;

                // first, if the session has not already occurred and this is a GTT, GTW, or WebEx session, use the api to delete the registrant record
                if (
                    !this._HasSessionAlreadyOccurred
                    && !String.IsNullOrWhiteSpace(droppedRegistrantKeysData)
                    && ( 
                        this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToTraining
                        || this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar
                        || this._SessionObject.Type == StandupTrainingInstance.MeetingType.WebEx
                       )
                   )
                {
                    string[] droppedRegistrantKeys = droppedRegistrantKeysData.Split('|');

                    foreach (string registrantKey in droppedRegistrantKeys)
                    {
                        Int64 registrantKeyParsed;

                        // note that any errors thrown from this method will be caught by the method and buried, they arent showstopper errors
                        if (Int64.TryParse(registrantKey, out registrantKeyParsed))
                        { this._DeleteWebMeetingRegistrant(registrantKeyParsed); }
                    }
                }
                
                // now, start to compile the roster data and do actions such as creating registrants (API),
                // removing demoted registrants (API), and finally saving to database

                // declare a DataTable for collecting data from the JSON object
                DataTable sessionRosterDataTable = new DataTable();
                sessionRosterDataTable.Columns.Add("idUser", typeof(int));                
                sessionRosterDataTable.Columns.Add("completionStats", typeof(int));
                sessionRosterDataTable.Columns.Add("successStats", typeof(int));
                sessionRosterDataTable.Columns.Add("score", typeof(int));
                sessionRosterDataTable.Columns.Add("isWaitListRow", typeof(bool));
                sessionRosterDataTable.Columns.Add("registrantKey", typeof(Int64));
                sessionRosterDataTable.Columns.Add("registrantEmail", typeof(string));
                sessionRosterDataTable.Columns.Add("joinUrl", typeof(string));
                sessionRosterDataTable.Columns.Add("order", typeof(int));
                sessionRosterDataTable.Columns.Add("listStatus", typeof(string));
                
                // load the JSON data into an object
                List<_StandupTrainingToUserLinkDataJSONObject> enrolledAndWaitlistRosters = new JavaScriptSerializer().Deserialize<List<_StandupTrainingToUserLinkDataJSONObject>>(jsonStringifiedData);
                
                // loop through each object and perform appropriate actions
                foreach (_StandupTrainingToUserLinkDataJSONObject record in enrolledAndWaitlistRosters)
                {
                    
                    if (record.IdUser > 0)
                    {
                        // if the session has not yet occurred, we need to perform API actions in addition to prepping data for the database
                        if (!this._HasSessionAlreadyOccurred)
                        {
                            
                            if (!record.IsWaitList) // user is on enrolled list
                            {
                                // for added and promoted records, use API to add the registrant if it does not have a registrant key
                                // if this fails on any record inside the loop, the thrown exception will halt execution, errors are fatal here
                                string joinUrl = null;

                                if (
                                    (
                                    this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToTraining
                                    || this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar
                                    || this._SessionObject.Type == StandupTrainingInstance.MeetingType.WebEx
                                    )
                                    && (record.ListStatus == "added" || record.ListStatus == "promoted")
                                    && String.IsNullOrWhiteSpace(record.RegistrantKey)
                                   )
                                {                                    
                                    // if the user does not have an email address, we need to dummy one up for the API call
                                    if (String.IsNullOrWhiteSpace(record.RegistrantEmail))
                                    { record.RegistrantEmail = record.IdUser.ToString() + "@" + AsentiaSessionState.SiteHostname + "." + Config.AccountSettings.BaseDomain; }

                                    // add the registrant
                                    Tuple<Int64, string> returnedTuple = this._AddWebMeetingRegistrant(record.IdUser, record.RegistrantEmail);
                                    record.RegistrantKey = returnedTuple.Item1.ToString();

                                    if (!String.IsNullOrWhiteSpace(returnedTuple.Item2))
                                    { joinUrl = returnedTuple.Item2; }

                                    if (record.RegistrantKey == "0") // failure to retrieve registrant key
                                    { throw new AsentiaException(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator + " (0000)"); }
                                }

                                // if the email address is still empty, make it null; this is done to make it clean
                                if (String.IsNullOrWhiteSpace(record.RegistrantEmail))
                                { record.RegistrantEmail = null; }

                                // prep the record for database save
                                if (!String.IsNullOrWhiteSpace(record.RegistrantKey))
                                { sessionRosterDataTable.Rows.Add(record.IdUser, null, null, null, record.IsWaitList, Convert.ToInt64(record.RegistrantKey), record.RegistrantEmail, joinUrl, null, record.ListStatus); }
                                else
                                { sessionRosterDataTable.Rows.Add(record.IdUser, null, null, null, record.IsWaitList, null, record.RegistrantEmail, null, null, record.ListStatus); }
                            }
                            else // user is on waitlist
                            {
                                // for demoted records, use API to delete the registrant if it has a registrant key
                                if (
                                    (
                                    this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToTraining
                                    || this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar
                                    || this._SessionObject.Type == StandupTrainingInstance.MeetingType.WebEx
                                    )
                                    && record.ListStatus == "demoted" 
                                    && !String.IsNullOrWhiteSpace(record.RegistrantKey)
                                   )
                                {
                                    Int64 registrantKeyParsed;

                                    // note that any errors thrown from this method will be caught by the method and buried, they arent showstopper errors
                                    if (Int64.TryParse(record.RegistrantKey, out registrantKeyParsed))
                                    { this._DeleteWebMeetingRegistrant(registrantKeyParsed); }
                                }

                                // prep the record for database save
                                sessionRosterDataTable.Rows.Add(record.IdUser, null, null, null, record.IsWaitList, null, null, null, record.OrdinalIndex, record.ListStatus); 
                            }
                        }
                        else // the session has already occurred, do not perform any API actions, just do database stuff
                        {
                            if (!record.IsWaitList)
                            {
                                // if the email address is empty, make it null; this is done to make it clean
                                if (String.IsNullOrWhiteSpace(record.RegistrantEmail))
                                { record.RegistrantEmail = null; }

                                if (!String.IsNullOrWhiteSpace(record.RegistrantKey))
                                { sessionRosterDataTable.Rows.Add(record.IdUser, record.CompletionStatus, null, record.Score, record.IsWaitList, record.RegistrantKey, record.RegistrantEmail, null, null, record.ListStatus); }
                                else
                                { sessionRosterDataTable.Rows.Add(record.IdUser, record.CompletionStatus, null, record.Score, record.IsWaitList, null, record.RegistrantEmail, null, null, record.ListStatus); }
                            }
                            else
                            { sessionRosterDataTable.Rows.Add(record.IdUser, record.CompletionStatus, null, record.Score, record.IsWaitList, null, null, null, record.OrdinalIndex, record.ListStatus); }
                        }
                    }
                }
                
                // save the roster changes to database
                this._StandupTrainingInstanceToUserLinkObject = new Library.StandUpTrainingInstanceToUserLink();
                this._StandupTrainingInstanceToUserLinkObject.UpdateUsers(this._SessionObject.Id, sessionRosterDataTable);

                // clear the tab panels container
                this.ManageRosterPropertiesTabPanelsContainer.Controls.Clear();

                // clear the grids
                this._EnrolledGrid.Controls.Clear();
                this._WaitlistGrid.Controls.Clear();

                // re-build the enrolled grid
                this._BuildRosterFormEnrolledListPanel();

                // re-build the waiting list grid
                this._BuildRosterFormWaitingListPanel();

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.ManageRosterPropertiesFeedbackContainer, _GlobalResources.SessionRosterHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.ManageRosterPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.ManageRosterPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.ManageRosterPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.ManageRosterPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.ManageRosterPropertiesFeedbackContainer, ex.Message, true);
            }
        }
        #endregion 
        
        #region _AddWebMeetingRegistrant
        /// <summary>
        /// Adds a web meeting registrant from GTW, GTT, or WebEx by connecting to the appropriate API.
        /// </summary>
        /// <param name="idUser">the user id</param>
        private Tuple<Int64, string> _AddWebMeetingRegistrant(int idUser, string registrantEmail)
        {
            try
            {
                Asentia.UMS.Library.User userObject = new Asentia.UMS.Library.User(idUser);
                AsentiaAESEncryption cryptoProvider = new AsentiaAESEncryption();
                string organizerUsername = null;
                string organizerPassword = null;

                if (this._SessionObject.IdWebMeetingOrganizer != null && this._SessionObject.IdWebMeetingOrganizer > 0)
                {
                    WebMeetingOrganizer webMeetingOrganizer = new WebMeetingOrganizer((int)this._SessionObject.IdWebMeetingOrganizer);
                    organizerUsername = webMeetingOrganizer.Username;
                    organizerPassword = cryptoProvider.Decrypt(webMeetingOrganizer.Password);
                }

                if (this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToTraining) // GTT
                {
                    if (organizerUsername == null)
                    {                        
                        GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_USERNAME),
                                                                     cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PASSWORD)),
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                        GoToTrainingAPI.GTT_Registrant_Response response = gttApi.CreateRegistrant((Int64)this._SessionObject.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);
                        return Tuple.Create(response.Registrants[0].RegistrantKey, response.Registrants[0].JoinUrl);
                    }
                    else
                    {
                        GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                     organizerUsername,
                                                                     organizerPassword,
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                        GoToTrainingAPI.GTT_Registrant_Response response = gttApi.CreateRegistrant((Int64)this._SessionObject.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);
                        return Tuple.Create(response.Registrants[0].RegistrantKey, response.Registrants[0].JoinUrl);
                    }
                }
                else if (this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar) // GTW
                {
                    if (organizerUsername == null)
                    {
                        GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_USERNAME),
                                                                   cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PASSWORD)),
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                        GoToWebinarAPI.GTW_Registrant_Response response = gtwApi.CreateRegistrant((Int64)this._SessionObject.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);
                        return Tuple.Create(response.Registrants[0].RegistrantKey, response.Registrants[0].JoinUrl);
                    }
                    else
                    {
                        GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                   organizerUsername,
                                                                   organizerPassword,
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                        GoToWebinarAPI.GTW_Registrant_Response response = gtwApi.CreateRegistrant((Int64)this._SessionObject.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);
                        return Tuple.Create(response.Registrants[0].RegistrantKey, response.Registrants[0].JoinUrl);
                    }
                }
                else if (this._SessionObject.Type == StandupTrainingInstance.MeetingType.WebEx) // WebEx
                {
                    if (organizerUsername == null)
                    {
                        WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_USERNAME),
                                                       cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PASSWORD)));

                        WebExAPI.WebEx_Registrant_Response.Registrant response = wbxApi.CreateMeetingRegistrant((Int64)this._SessionObject.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);
                        return Tuple.Create(response.AttendeeId, wbxApi.GetMeetingJoinURL(response.MeetingKey));
                    }
                    else
                    {
                        WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                       organizerUsername,
                                                       organizerPassword);

                        WebExAPI.WebEx_Registrant_Response.Registrant response = wbxApi.CreateMeetingRegistrant((Int64)this._SessionObject.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);
                        return Tuple.Create(response.AttendeeId, wbxApi.GetMeetingJoinURL(response.MeetingKey));
                    }
                }
                else
                { return Tuple.Create(Convert.ToInt64(0), String.Empty); }
            }
            catch
            { throw; }
        }
        #endregion

        #region _DeleteWebMeetingRegistrant
        /// <summary>
        /// Deletes a web meeting registrant from GTW, GTT, or WebEx by connecting to the appropriate API.
        /// </summary>
        /// <param name="registrantKey">the registrant key of the user</param>
        private void _DeleteWebMeetingRegistrant(Int64 registrantKey)
        {
            try // this is wrapped in a try/catch because we dont want to die on error because an error here doesn't matter much
            {
                AsentiaAESEncryption cryptoProvider = new AsentiaAESEncryption();
                string organizerUsername = null;
                string organizerPassword = null;

                if (this._SessionObject.IdWebMeetingOrganizer != null && this._SessionObject.IdWebMeetingOrganizer > 0)
                {
                    WebMeetingOrganizer webMeetingOrganizer = new WebMeetingOrganizer((int)this._SessionObject.IdWebMeetingOrganizer);
                    organizerUsername = webMeetingOrganizer.Username;
                    organizerPassword = cryptoProvider.Decrypt(webMeetingOrganizer.Password);
                }

                if (this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToTraining) // GTT
                {
                    if (organizerUsername == null)
                    {
                        GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_USERNAME),
                                                                     cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PASSWORD)),
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                        gttApi.DeleteRegistrant(Convert.ToInt64(this._SessionObject.IntegratedObjectKey), registrantKey);
                    }
                    else
                    {
                        GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                     organizerUsername,
                                                                     organizerPassword,
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                        gttApi.DeleteRegistrant(Convert.ToInt64(this._SessionObject.IntegratedObjectKey), registrantKey);
                    }
                }
                else if (this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar) // GTW
                {
                    if (organizerUsername == null)
                    {
                        GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_USERNAME),
                                                                   cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PASSWORD)),
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                        gtwApi.DeleteRegistrant(Convert.ToInt64(this._SessionObject.IntegratedObjectKey), registrantKey);
                    }
                    else
                    {
                        GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                   organizerUsername,
                                                                   organizerPassword,
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                        gtwApi.DeleteRegistrant(Convert.ToInt64(this._SessionObject.IntegratedObjectKey), registrantKey);
                    }
                }
                else if (this._SessionObject.Type == StandupTrainingInstance.MeetingType.WebEx) // WebEx
                {
                    if (organizerUsername == null)
                    {
                        WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_USERNAME),
                                                       cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PASSWORD)));

                        wbxApi.DeleteMeetingRegistrant(registrantKey);
                    }
                    else
                    {
                        WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                       organizerUsername,
                                                       organizerPassword);

                        wbxApi.DeleteMeetingRegistrant(registrantKey);
                    }
                }
                else
                { }
            }
            catch // just bury the error
            { }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/standuptraining/sessions/Default.aspx?stid=" + this._StandupTrainingObject.Id.ToString());
        }
        #endregion        

        #region SynchronizeJsonDataStruct
        public struct SynchronizeJsonDataStruct
        {
            public bool actionSuccessful;
            public List<AttendeeJsonData> attendees;
            public string exception;            
        }

        public class AttendeeJsonData
        {
            public string key;
            public string email;
            public string name;
            public string duration;
            public bool allMeetingTimesAttended;
            public bool isSynchronized;
        }
        #endregion

        #region SynchronizeWithWebMeetingApi
        /// <summary>
        /// Synchronizes the attendee list from the web meeting provider API to mark rostered learners as completed.
        /// </summary>
        /// <param name="idStandupTrainingInstance"></param>
        /// <param name="integratedObjectKey"></param>
        /// <param name="integratedObjectType"></param>
        /// <returns>JSON Data Structure</returns>
        [WebMethod(EnableSession = true)]        
        public static SynchronizeJsonDataStruct SynchronizeWithWebMeetingApi(int idStandupTrainingInstance, string integratedObjectKey, string integratedObjectType)
        {
            SynchronizeJsonDataStruct jsonData = new SynchronizeJsonDataStruct();

            try
            {                
                jsonData.actionSuccessful = true;
                jsonData.attendees = null;
                jsonData.exception = String.Empty;

                StandupTrainingInstance stiObject = new StandupTrainingInstance(idStandupTrainingInstance);

                AsentiaAESEncryption cryptoProvider = new AsentiaAESEncryption();
                string organizerUsername = null;
                string organizerPassword = null;

                if (stiObject.IdWebMeetingOrganizer != null && stiObject.IdWebMeetingOrganizer > 0)
                {
                    WebMeetingOrganizer webMeetingOrganizer = new WebMeetingOrganizer((int)stiObject.IdWebMeetingOrganizer);
                    organizerUsername = webMeetingOrganizer.Username;
                    organizerPassword = cryptoProvider.Decrypt(webMeetingOrganizer.Password);
                }

                if (integratedObjectType == "gotomeeting") // GTM
                {
                    GoToMeetingAPI.GTM_Attendee_Response response;

                    if (organizerUsername == null)
                    {
                        GoToMeetingAPI gtmApi = new GoToMeetingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_APPLICATION),
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_USERNAME),
                                                                       cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PASSWORD)),
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN),
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_CONSUMERSECRET));

                        response = gtmApi.GetMeetingAttendees(Convert.ToInt64(integratedObjectKey));
                    }
                    else
                    {
                        GoToMeetingAPI gtmApi = new GoToMeetingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_APPLICATION),
                                                                       organizerUsername,
                                                                       organizerPassword,
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN),
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_CONSUMERSECRET));

                        response = gtmApi.GetMeetingAttendees(Convert.ToInt64(integratedObjectKey));
                    }                    

                    if (response.Attendees.Count > 0)
                    {
                        jsonData.attendees = new List<AttendeeJsonData>();

                        foreach (GoToMeetingAPI.GTM_Attendee_Response.Attendee attendee in response.Attendees)
                        {
                            // get the duration which comes in seconds, and translate that to text, i.e. "X hours and X minutes"                            
                            string durationString = DurationSecondsToStringHelper((int?)attendee.Duration);
                            
                            jsonData.attendees.Add(new AttendeeJsonData
                            {
                                key = attendee.MeetingId.ToString(),
                                email = attendee.Email,
                                name = attendee.FullName,                       
                                duration = durationString,
                                allMeetingTimesAttended = true,
                                isSynchronized = false
                            });
                        }
                    }
                }
                else if (integratedObjectType == "gototraining") // GTT
                {
                    GoToTrainingAPI.GTT_Attendee_Response response;

                    if (organizerUsername == null)
                    {
                        GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_USERNAME),
                                                                     cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PASSWORD)),
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                        response = gttApi.GetTrainingAttendees(Convert.ToInt64(integratedObjectKey));
                    }
                    else
                    {
                        GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                     organizerUsername,
                                                                     organizerPassword,
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                        response = gttApi.GetTrainingAttendees(Convert.ToInt64(integratedObjectKey));
                    }

                    if (response.Attendees.Count > 0)
                    {
                        jsonData.attendees = new List<AttendeeJsonData>();

                        foreach (GoToTrainingAPI.GTT_Attendee_Response.Attendee attendee in response.Attendees)
                        {
                            // get the duration which comes in seconds, and translate that to text, i.e. "X hours and X minutes"                            
                            string durationString = DurationSecondsToStringHelper(attendee.TotalDuration);                            

                            jsonData.attendees.Add(new AttendeeJsonData
                            {
                                key = String.Join("|", attendee.SessionKeys.ToArray()),
                                email = attendee.Email,
                                name = attendee.FirstName + " " + attendee.LastName,
                                duration = durationString,
                                allMeetingTimesAttended = attendee.AllMeetingTimesAttended,
                                isSynchronized = false
                            });
                        }
                    }
                }
                else if (integratedObjectType == "gotowebinar") // GTW
                {
                    GoToWebinarAPI.GTW_Attendee_Response response;

                    if (organizerUsername == null)
                    {
                        GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_USERNAME),
                                                                   cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PASSWORD)),
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                        response = gtwApi.GetWebinarAttendees(Convert.ToInt64(integratedObjectKey));
                    }
                    else
                    {
                        GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                   organizerUsername,
                                                                   organizerPassword,
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                        response = gtwApi.GetWebinarAttendees(Convert.ToInt64(integratedObjectKey));
                    }

                    if (response.Attendees.Count > 0)
                    {
                        jsonData.attendees = new List<AttendeeJsonData>();

                        foreach (GoToWebinarAPI.GTW_Attendee_Response.Attendee attendee in response.Attendees)
                        {
                            // get the duration which comes in seconds, and translate that to text, i.e. "X hours and X minutes"                            
                            string durationString = DurationSecondsToStringHelper(attendee.Duration);

                            jsonData.attendees.Add(new AttendeeJsonData
                            {
                                key = attendee.SessionKey.ToString(),
                                email = attendee.Email,
                                name = attendee.FirstName + " " + attendee.LastName,
                                duration = durationString,
                                allMeetingTimesAttended = true,
                                isSynchronized = false
                            });
                        }
                    }
                }
                else if (integratedObjectType == "webex") // WEBEX - note that as of Sept 2017, WebEx does not sync
                { }
                else
                { }

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.attendees = null;
                jsonData.exception = ex.Message;                

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region DurationSecondsToStringHelper
        /// <summary>
        /// Converts a duration in seconds to "X hours X minutes" string.
        /// </summary>
        /// <param name="duration">duration in seconds</param>
        /// <returns>string</returns>
        public static string DurationSecondsToStringHelper(int? durationSeconds)
        {
            string durationString = null;

            if (durationSeconds != null && durationSeconds > 0)
            {
                if (durationSeconds < 60)
                { durationString = String.Format(_GlobalResources.XSecond_s, durationSeconds.ToString()); }
                else
                {
                    int minutes = (int)durationSeconds / 60;

                    if (minutes < 60)
                    { durationString = String.Format(_GlobalResources.XMinute_s, minutes.ToString()); }
                    else
                    {
                        int hours = minutes / 60;
                        minutes = minutes - (hours * 60);

                        if (minutes == 0)
                        { durationString = String.Format(_GlobalResources.XHour_s, hours.ToString()); }
                        else
                        { durationString = String.Format(_GlobalResources.XHour_sAndXMinute_s, hours.ToString(), minutes.ToString()); }
                    }
                }
            }

            return durationString;
        }
        #endregion

        #region BatchUpdateDataStruct
        public struct BatchUpdateDataStruct
        {
            public bool actionSuccessful;
            public List<BatchAttendeeJsonData> attendees;
            public string exception;
            public bool hasSessionAlreadyOccurred; // only needed for new roster users
        }

        public class BatchAttendeeJsonData
        {
            public string username;
            public string completionStatus;
            public string score; // score can be empty (optional)
            public int idUser; // 0 indicates an "update"; non zero indicates a new user in the roster
            public string email; // only needed for new roster users
            public string displayName; // only needed for new roster users
        }

        #endregion

        #region BatchUserUpdate
        /// <summary>
        /// Used to update the user completion status for batch updates
        /// </summary>
        [WebMethod(EnableSession = true)]
        public static BatchUpdateDataStruct BatchUserUpdate(int idStandupTrainingInstance, string savedFilename)
        {
            // set the saved file path of the file
            string batchFilePath = "";
            if (!String.IsNullOrWhiteSpace(savedFilename))
            {
                batchFilePath = "/_upload/rosterbatch/" + savedFilename;
            }
            
            BatchUpdateDataStruct jsonData = new BatchUpdateDataStruct();
            
            jsonData.actionSuccessful = true;
            jsonData.exception = null;
            jsonData.attendees = new List<BatchAttendeeJsonData>();

            StandupTrainingInstance session = new StandupTrainingInstance(idStandupTrainingInstance);
            if (session.MeetingTimes.Count > 0)
            { jsonData.hasSessionAlreadyOccurred = ((Library.StandupTrainingInstance.MeetingTimeProperty)(session.MeetingTimes[0])).DtStart < DateTime.UtcNow; }
            else
            { jsonData.hasSessionAlreadyOccurred = true; }
            
            
            try
            {

                if (!String.IsNullOrWhiteSpace(batchFilePath) && File.Exists(web.HttpContext.Current.Server.MapPath(batchFilePath)))
                {

                    List<string> lines = File.ReadAllLines(web.HttpContext.Current.Server.MapPath(batchFilePath), Encoding.Default).ToList();

                    int lineNum = 0;
                    bool lineBreakFlag = false;
                    bool totalLineBreakFlag = false;
                    string lineBreakError = string.Empty;

                    if (lines.Count == 0)
                    {
                        jsonData.actionSuccessful = false;
                        jsonData.exception = _GlobalResources.ThereAreNoUsersInYourDataFile;
                    }
                    else
                    {

                        // read file line by line for line break checking
                        foreach (string line in lines)
                        {
                            lineNum++;
                            string firstItemWithValue = string.Empty;

                            string[] docColumns = line.Replace(Environment.NewLine, "").Split('\t');

                            for (int i = 0; i < docColumns.Length; i++)
                            {
                                if (!String.IsNullOrEmpty(docColumns[i].Trim()) && docColumns[i].Trim() != "\"")
                                {
                                    firstItemWithValue = docColumns[i];
                                    break;
                                }
                            }

                            if (lineBreakFlag)
                            {
                                lineBreakError += "\"" + firstItemWithValue + "\"";
                                lineBreakFlag = false;
                            }
                            else if (docColumns.Length != 4)
                            {
                                if (lineNum == 1)
                                {
                                    totalLineBreakFlag = true;
                                    throw new AsentiaException(_GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain);
                                }

                                if (docColumns[docColumns.Length - 1].ToUpper() != "END")
                                {
                                    lineBreakError += "<br>" + _GlobalResources.AtLine + " " + lineNum.ToString() + ":" + _GlobalResources.ThereAreLineBreakOrNewLineCharactersInYourDataBetween + " \"" + (string.IsNullOrEmpty(docColumns[docColumns.Length - 1]) ? docColumns[docColumns.Length - 2] : docColumns[docColumns.Length - 1]) + "\" " + _GlobalResources.and_lower + " ";
                                    lineBreakFlag = true;
                                    totalLineBreakFlag = true;
                                }
                                else
                                {
                                    lineBreakError += "<br>" + _GlobalResources.AtLine + lineNum.ToString() + ": " + _GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(lineBreakError))
                        {
                            jsonData.actionSuccessful = false;
                            if (totalLineBreakFlag)
                            { jsonData.exception = _GlobalResources.ThereAreErrorsInTheFollowingPlaces + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain + ":" + lineBreakError; }
                            else
                            { jsonData.exception = _GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain; }
                        }
                        else
                        {

                            //read file line by line
                            foreach (string line in lines)
                            {

                                string[] columns = line.Replace(Environment.NewLine, "").Split('\t');

                                try
                                {

                                    BatchAttendeeJsonData attendeeData = new BatchAttendeeJsonData();
                                    attendeeData.username = columns[0];
                                    attendeeData.completionStatus = columns[1];
                                    attendeeData.score = columns[2];

                                    // check if user exists in enrolled list table
                                    attendeeData.idUser = StandUpTrainingInstanceToUserLink.GetNewRosterUserId(idStandupTrainingInstance, columns[0]);

                                    // get the email address and display name if it is a new user
                                    if (attendeeData.idUser > 0)
                                    {
                                        User user = new User(attendeeData.idUser);
                                        attendeeData.email = user.Email;
                                        attendeeData.displayName = user.DisplayName;
                                    }

                                    jsonData.attendees.Add(attendeeData);

                                }
                                catch (Exception ex)
                                {
                                    jsonData.actionSuccessful = false;
                                    jsonData.exception = ex.Message;
                                }

                                if (columns.Length != 4)
                                {
                                    throw new AsentiaException(_GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain);
                                }

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // set json exception
                jsonData.actionSuccessful = false;
                jsonData.exception = ex.Message;
            }

            // make sure file exists in the upload folder and then delete it
            if(File.Exists(web.HttpContext.Current.Server.MapPath(batchFilePath)))
            { File.Delete(web.HttpContext.Current.Server.MapPath(batchFilePath)); }

            return jsonData;
            

        }
        #endregion
    }    
}
