﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;
//using user = Asentia.UMS.Library.User;

namespace Asentia.LMS.Pages.Administrator.StandupTraining.Sessions
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel SessionPropertiesFormContentWrapperContainer;
        public Panel StandupTrainingObjectMenuContainer;
        public Panel SessionPropertiesWrapperContainer;
        public Panel SessionPropertiesInstructionsPanel;
        public Panel SessionPropertiesFeedbackContainer;
        public Panel SessionPropertiesContainer;
        public Panel SessionPropertiesActionsPanel;
        public Panel SessionPropertiesTabPanelsContainer;
        public Panel ObjectOptionsPanel;
        #endregion

        #region Private Properties
        private Library.StandupTraining _StandupTrainingObject;
        private Library.StandupTrainingInstance _SessionObject;

        private TextBox _SessionTitle;
        private TextBox _Description;
        private TextBox _Seats;
        private TextBox _WaitingSeats;
        private TextBox _UrlRegistration;
        private TextBox _UrlAttend;
        private TextBox _City;
        private TextBox _Province;
        private TextBox _PostalCode;
        private TextBox _LocationDescription;
        private TextBox _MeetingPassword;
        
        private DropDownList _SelectOrganizer;
        private TextBox _OrganizerUsername;
        private TextBox _OrganizerPassword;
        private CheckBox _ChangeOrganizerPassword;
        private HiddenField _SelectedWebMeetingOrganizerId;

        private TimeZoneSelector _Timezone;
        private RadioButtonList _MeetingType;
        private RadioButtonList _IsClosed;

        private ModalPopup _SelectInstructorsForSessionModal;
        private ModalPopup _SelectResourcesForSessionModal;

        private Button _SelectInstructorsForSessionButton;
        private Button _SelectResourcesForSessionButton;
        
        private Button _SaveButton;
        private Button _CancelButton;

        private DynamicListBox _SelectEligibleInstructors;
        private DynamicListBox _SelectEligibleResources;        

        private Panel _MeetingDateControlPlaceHolder;
        private Panel _AddDatesButtonContainer;
        private Panel _InstructorsListContainer;
        private Panel _ResourcesListContainer;

        private DateTime? _DtStart;
        private DateTime? _DtEnd;

        private DataTable _InstructorsForSelectList;
        private DataTable _MeetingTimesToValidate;
        private DataTable _MeetingTimesToSave;
        private DataTable _MeetingTimesForModalResourceSearching;
        private DataTable _SessionInstructors;
        private DataTable _SessionResources;
        private DataTable _EligibleInstructorsForSelectList;
        private DataTable _EligibleResourcesForSelectList;

        private HiddenField _MeetingsJsonData = new HiddenField();
        private HiddenField _SelectedDatesForResources;
        private HiddenField _SelectedInstructors;
        private HiddenField _SelectedResources;

        private bool _IsExistingDate = false;
        private const string DateFormatDotNet = "yyyy-MM-dd";
        private string _TzDotNetName;
        private bool _IsDuplicatingSession = false;

        private Table _DatesTable;

        /// <summary>
        /// Used to populate session start and end dates from hidden field.
        /// </summary>
        private bool _IsHiddenFieldData = false;
        private bool _SessionDateValidationMessageShown = false;

        /// <summary>
        /// Web meeting integration.
        /// </summary>
        private bool _IsGoToMeetingOn = false;
        private bool _IsGoToWebinarOn = false;
        private bool _IsGoToTrainingOn = false;
        private bool _IsWebExOn = false;
        #endregion

        #region Page_Init
        /// <summary>
        /// Page_Init event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Init(object sender, EventArgs e)
        {
            this._MeetingDateControlPlaceHolder = new Panel();

            // Dates table
            this._DatesTable = new Table();
            this._DatesTable.ID = "DatesTable_" + this.ClientID;
            this._DatesTable.ClientIDMode = ClientIDMode.AutoID;

            this._MeetingDateControlPlaceHolder.Controls.Add(this._DatesTable);
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.StandupTraining.Sessions.Modify.js");

            // build start up call for MCE and add to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine(" SetOptionsForSelectedMeetingType();");
            multipleStartUpCallsScript.AppendLine(" InitializeSelectOrganizerField();");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);

            // build global JS variables for image elements
            StringBuilder imGlobalJS = new StringBuilder();
            imGlobalJS.AppendLine("DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            imGlobalJS.AppendLine("SessionInstructorsList_Container = \"" + this._InstructorsListContainer.ClientID + "\";");
            imGlobalJS.AppendLine("SessionResourcesList_Container = \"" + this._ResourcesListContainer.ClientID + "\";");
            imGlobalJS.AppendLine("SessionSelectedResources_Field = \"" + this._SelectedResources.ClientID + "\";");
            imGlobalJS.AppendLine("SessionSelectedInstructors_Field = \"" + this._SelectedInstructors.ClientID + "\";");
            imGlobalJS.AppendLine("selectedDatesForResources = \"" + _SelectedDatesForResources.ClientID + "\";");
            imGlobalJS.AppendLine("RuleSetEnrollmentTimezone_Field = \"" + this._Timezone.ClientID + "\";");
            imGlobalJS.AppendLine("SelectInstructorsForSessionButton = \"" + this._SelectInstructorsForSessionButton.ClientID + "\";");
            imGlobalJS.AppendLine("SelectResourcesForSessionButton = \"" + this._SelectResourcesForSessionButton.ClientID + "\";");
            imGlobalJS.AppendLine("NoRecordsFound = \"" + _GlobalResources.NoRecordsFound + "\";");
            imGlobalJS.AppendLine("AutomaticallyGenerated = \"" + _GlobalResources.AutomaticallyGenerated + "\";");
            imGlobalJS.AppendLine("DateFormat = \"" + DatePicker.DateFormatJS + "\";");
            imGlobalJS.AppendLine("DateControlRowHtml = '" + this._GetHTMLForCreatingDateControlRow() + "';");

            // set JS variables for seats allowed depending on web meeting plans

            imGlobalJS.AppendLine("GTMSeatsAllowed = \"" + this._GetMaxAttendeesForWebMeeting("GTM").ToString() + "\";");
            imGlobalJS.AppendLine("GTWSeatsAllowed = \"" + this._GetMaxAttendeesForWebMeeting("GTW").ToString() + "\";");
            imGlobalJS.AppendLine("GTTSeatsAllowed = \"" + this._GetMaxAttendeesForWebMeeting("GTT").ToString() + "\";");
            imGlobalJS.AppendLine("WebExSeatsAllowed = \"" + this._GetMaxAttendeesForWebMeeting("WebEx").ToString() + "\";");

            // set JS object variable for organizer accounts keyed to the calling user
            imGlobalJS.AppendLine("OrganizerAccountsForDropDown = [ ");
            imGlobalJS.AppendLine("{\"IdWebMeetingOrganizer\": \"NULL\", \"OrganizerType\": \"NULL\", \"OrganizerUsername\": \"" + _GlobalResources.DefaultOrganizer + "\" }, ");

            // get the organizer accounts tied to the calling user
            DataTable webMeetingOrganizers = WebMeetingOrganizer.IdsAndUsernamesForSelectList();

            foreach (DataRow row in webMeetingOrganizers.Rows)
            { imGlobalJS.AppendLine("{\"IdWebMeetingOrganizer\": \"" + row["idWebMeetingOrganizer"].ToString() + "\", \"OrganizerType\": \"" + row["organizerType"].ToString() + "\", \"OrganizerUsername\" : \"" + row["username"].ToString() + "\" },"); }

            // add the option for "Add New"
            imGlobalJS.AppendLine("{\"IdWebMeetingOrganizer\": \"0\", \"OrganizerType\": \"NULL\", \"OrganizerUsername\": \"" + _GlobalResources.AddNew + "\" } ");
            imGlobalJS.AppendLine(" ];");

            csm.RegisterClientScriptBlock(typeof(Modify), "ImageGlobalJS", imGlobalJS.ToString(), true);

            if (this._SessionObject != null)
            {
                // rebind the date control to maitain its values
                this._DatesTable.Rows.Clear();
                this._LoadDates(this._SessionObject.MeetingTimes);
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {            
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_InstructorLedTrainingManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/standuptraining/sessions/Modify.css");

            // determine if the session is being duplicated
            this._IsDuplicatingSession = this.ViewStateBool(this.ViewState, "IsDuplicatingSession", false);
            
            // get the standup training and standup training instance objects
            this._GetStandupTrainingObjects();
            
            // set the web meeting on/off settings
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE) && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ON))
            { this._IsGoToMeetingOn = true; }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE) && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ON))
            { this._IsGoToWebinarOn = true; }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE) && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ON))
            { this._IsGoToTrainingOn = true; }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE) && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ON))
            { this._IsWebExOn = true; }

            // initialize the administrator menu
            this.InitializeAdminMenu();            
            
            // build the controls for the page
            this._BuildControls();            
        }
        #endregion        

        #region _GetStandupTrainingObjects
        /// <summary>
        /// Gets standup training and standup training instance objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetStandupTrainingObjects()
        {
            // get the id querystring parameter
            int qsSTId = this.QueryStringInt("stid", 0);
            int vsSTId = this.ViewStateInt(this.ViewState, "stid", 0);
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsSTId > 0 || vsSTId > 0)
            {
                int stid = 0;

                if (qsSTId > 0)
                { stid = qsSTId; }

                if (vsSTId > 0)
                { stid = vsSTId; }
                
                try
                {
                    if (stid > 0)
                    { this._StandupTrainingObject = new Library.StandupTraining(stid); }
                }
                catch
                { Response.Redirect("~/administrator/standuptraining"); }
            }
            else
            { Response.Redirect("~/administrator/standuptraining"); }

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }
                
                try
                {
                    if (id > 0)
                    { this._SessionObject = new Library.StandupTrainingInstance(id); }
                }
                catch
                { Response.Redirect("~/administrator/standuptraining/Dashboard.aspx?id=" + this._StandupTrainingObject.Id.ToString()); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {            
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.SessionPropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.SessionPropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the standup training object menu
            if (this._StandupTrainingObject != null)
            {
                StandupTrainingObjectMenu standupTrainingObjectMenu = new StandupTrainingObjectMenu(this._StandupTrainingObject);
                standupTrainingObjectMenu.SelectedItem = StandupTrainingObjectMenu.MenuObjectItem.Sessions;

                this.StandupTrainingObjectMenuContainer.Controls.Add(standupTrainingObjectMenu);
            }            

            // build the standup training instance properties form
            this._BuildSessionPropertiesForm();
            
            // build the standup training instance properties form actions panel
            this._BuildSessionPropertiesActionsPanel();            
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get standup training module information
            string standupTrainingTitleInInterfaceLanguage = this._StandupTrainingObject.Title;
            string standupTrainingImagePath;
            string standupTrainingImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Library.StandupTraining.LanguageSpecificProperty standupTrainingLanguageSpecificProperty in this._StandupTrainingObject.LanguageSpecificProperties)
                {
                    if (standupTrainingLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { standupTrainingTitleInInterfaceLanguage = standupTrainingLanguageSpecificProperty.Title; }
                }
            }

            if (this._StandupTrainingObject.Avatar != null)
            {
                standupTrainingImagePath = SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + this._StandupTrainingObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                standupTrainingImageCssClass = "AvatarImage";
            }
            else
            {
                standupTrainingImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            }

            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;            
            string pageTitle;

            if (this._SessionObject != null && !this._IsDuplicatingSession)
            {
                string sessionTitleInInterfaceLanguage = this._SessionObject.Title;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (Library.StandupTrainingInstance.LanguageSpecificProperty sessionLanguageSpecificProperty in this._SessionObject.LanguageSpecificProperties)
                    {
                        if (sessionLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { sessionTitleInInterfaceLanguage = sessionLanguageSpecificProperty.Title; }
                    }
                }

                breadCrumbPageTitle = sessionTitleInInterfaceLanguage;
                pageTitle = sessionTitleInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewSession;
                pageTitle = _GlobalResources.NewSession;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.InstructorLedTrainingModules, "/administrator/standuptraining"));
            breadCrumbLinks.Add(new BreadcrumbLink(standupTrainingTitleInInterfaceLanguage, "/administrator/standuptraining/Dashboard.aspx?id=" + this._StandupTrainingObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Sessions, "/administrator/standuptraining/sessions/Default.aspx?stid=" + this._StandupTrainingObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, standupTrainingTitleInInterfaceLanguage, standupTrainingImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION, ImageFiles.EXT_PNG), standupTrainingImageCssClass);
        }
        #endregion        

        #region _BuildSessionPropertiesForm
        /// <summary>
        /// Builds session properties form with all required input controls
        /// </summary>
        private void _BuildSessionPropertiesForm()
        {
            if (this._SessionObject != null)
            { this._BuildObjectOptionsPanel(); }

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.SessionPropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisSession, true);

            // clear controls from container
            this.SessionPropertiesContainer.Controls.Clear();

            // build standup training session form tabs
            this._BuildSessionPropertiesFormTabs();

            this.SessionPropertiesTabPanelsContainer = new Panel();
            this.SessionPropertiesTabPanelsContainer.ID = "SessionProperties_TabPanelsContainer";
            this.SessionPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.SessionPropertiesContainer.Controls.Add(this.SessionPropertiesTabPanelsContainer);

            // build the "properties" panel of the standup training session properties form
            this._BuildSessionFormPropertiesPanel();

            // build multiple date control
            this._BuildMultipleDateControl();

            // build the "instructors" panel of the standup training session properties form
            this._BuildSessionPropertiesFormInstructorsPanel();

            // build the "resources" panel of the standup training session properties form
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE))
            {
                this._BuildSessionPropertiesFormResourcesPanel();
            }

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulateSessionPropertiesInputElements();
        }
        #endregion

        #region _GetMaxAttendeesForWebMeeting
        /// <summary>
        /// Gets the number of attendees allowed for a web meeting based on the provider.
        /// </summary>
        /// <param name="provider">valid values are GTM, GTW, GTT, or WebEx</param>
        /// <returns>number of allowed attendees based on the provider</returns>
        private int _GetMaxAttendeesForWebMeeting(string provider)
        {
            // GTM
            if (provider == "GTM")
            {
                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN) == GoToMeetingAPI.FREE_PLAN_NAME)
                { return Convert.ToInt32(GoToMeetingAPI.FREE_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN) == GoToMeetingAPI.STARTER_PLAN_NAME)
                { return Convert.ToInt32(GoToMeetingAPI.STARTER_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN) == GoToMeetingAPI.PRO_PLAN_NAME)
                { return Convert.ToInt32(GoToMeetingAPI.PRO_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN) == GoToMeetingAPI.PLUS_PLAN_NAME)
                { return Convert.ToInt32(GoToMeetingAPI.PLUS_PLAN_MAXATTENDEES); }
                else
                { return 0; }
            }

            // GTW
            if (provider == "GTW")
            {
                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PLAN) == GoToWebinarAPI.STARTER_PLAN_NAME)
                { return Convert.ToInt32(GoToWebinarAPI.STARTER_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PLAN) == GoToWebinarAPI.PRO_PLAN_NAME)
                { return Convert.ToInt32(GoToWebinarAPI.PRO_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PLAN) == GoToWebinarAPI.PLUS_PLAN_NAME)
                { return Convert.ToInt32(GoToWebinarAPI.PLUS_PLAN_MAXATTENDEES); }
                else
                { return 0; }
            }

            // GTT
            if (provider == "GTT")
            {
                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PLAN) == GoToTrainingAPI.STARTER_PLAN_NAME)
                { return Convert.ToInt32(GoToTrainingAPI.STARTER_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PLAN) == GoToTrainingAPI.PRO_PLAN_NAME)
                { return Convert.ToInt32(GoToTrainingAPI.PRO_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PLAN) == GoToTrainingAPI.PLUS_PLAN_NAME)
                { return Convert.ToInt32(GoToTrainingAPI.PLUS_PLAN_MAXATTENDEES); }
                else
                { return 0; }
            }

            // WebEx
            if (provider == "WebEx")
            {
                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PLAN) == WebExAPI.STARTER_PLAN_NAME)
                { return Convert.ToInt32(WebExAPI.STARTER_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PLAN) == WebExAPI.PLUS_PLAN_NAME)
                { return Convert.ToInt32(WebExAPI.PLUS_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PLAN) == WebExAPI.BUSINESS_PLAN_NAME)
                { return Convert.ToInt32(WebExAPI.BUSINESS_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PLAN) == WebExAPI.ENTERPRISE_PLAN_NAME)
                { return Convert.ToInt32(WebExAPI.ENTERPRISE_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PLAN) == WebExAPI.PREMIUM8_PLAN_NAME)
                { return Convert.ToInt32(WebExAPI.PREMIUM8_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PLAN) == WebExAPI.PREMIUM25_PLAN_NAME)
                { return Convert.ToInt32(WebExAPI.PREMIUM25_PLAN_MAXATTENDEES); }
                else if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PLAN) == WebExAPI.PREMIUM200_PLAN_NAME)
                { return Convert.ToInt32(WebExAPI.PREMIUM200_PLAN_MAXATTENDEES); }
                else
                { return 0; }
            }

            // if we get here, an invalid provider was passed as an argument, developer error
            return 0;
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {            
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";
            this.ObjectOptionsPanel.Controls.Clear();

            // Only create these buttons if this is not currently duplicating a session
            if (!this._IsDuplicatingSession)
            {
                Panel optionsPanelLinksContainer = new Panel();
                optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
                optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

                // MANAGE ROSTER
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("ManageRosterLink",
                                                    null,
                                                    "ManageRoster.aspx?stid=" + this._StandupTrainingObject.Id.ToString() + "&id=" + this._SessionObject.Id.ToString(),
                                                    null,
                                                    _GlobalResources.ManageRoster,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION, ImageFiles.EXT_PNG))
                    );
                
                // DUPLICATE SESSION
                LinkButton duplicateSessionLinkButton = new LinkButton();
                duplicateSessionLinkButton.Command += new CommandEventHandler(this._DuplicateSessionLinkButton_Command);
                duplicateSessionLinkButton.ID = "DuplicateSessionLink";
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink(null,
                                                    duplicateSessionLinkButton,
                                                    null,
                                                    null,
                                                    _GlobalResources.DuplicateSession,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION, ImageFiles.EXT_PNG))
                    );
                
                this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);                
            }
        }
        #endregion

        #region _DuplicateSessionLinkButton_Command
        /// <summary>
        /// Click event for the Duplicate Session Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DuplicateSessionLinkButton_Command(Object sender, CommandEventArgs e)
        {
            
            ViewState.Add("IsDuplicatingSession", true);
            this._IsDuplicatingSession = true;

            // clear controls for containers that have dynamically added elements
            this.StandupTrainingObjectMenuContainer.Controls.Clear();

            // build the page controls
            this._BuildControls();
            
        }
        #endregion

        #region _BuildSessionPropertiesFormTabs
        /// <summary>
        ///  Builds session properties form tabs
        /// </summary>
        private void _BuildSessionPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("Instructors", _GlobalResources.Instructors));
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE))
            {
                tabs.Enqueue(new KeyValuePair<string, string>("Resources", _GlobalResources.Resources));
            }

            // build and attach the tabs
            this.SessionPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("SessionProperties", tabs, null, this.Page, null));
        }
        #endregion

        #region _BuildSessionFormPropertiesPanel
        /// <summary>
        /// Builds the properties form.
        /// </summary>
        private void _BuildSessionFormPropertiesPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "SessionProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.Attributes.Add("style", "display: block;");

            // title field
            this._SessionTitle = new TextBox();
            this._SessionTitle.ID = "SessionTitle_Field";
            this._SessionTitle.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionTitle",
                                                              _GlobalResources.Title,
                                                              this._SessionTitle.ID,
                                                              this._SessionTitle,
                                                              true,
                                                              true,
                                                              true));

            // description field
            this._Description = new TextBox();
            this._Description.ID = "SessionDescription_Field";
            this._Description.CssClass = "ckeditor";
            this._Description.Style.Add("width", "98%");
            this._Description.TextMode = TextBoxMode.MultiLine;
            this._Description.Rows = 10;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionDescription",
                                                             _GlobalResources.Description,
                                                             this._Description.ID,
                                                             this._Description,
                                                             false,
                                                             true,
                                                             true));

            // meeting type            
            this._MeetingType = new RadioButtonList();
            this._MeetingType.ID = "MeetingType_Field";
            this._MeetingType.Items.Add(new ListItem(_GlobalResources.ClassroomBased, Convert.ToInt32(StandupTrainingInstance.MeetingType.Classroom).ToString()));
            this._MeetingType.Items.Add(new ListItem(_GlobalResources.WebBased, Convert.ToInt32(StandupTrainingInstance.MeetingType.Online).ToString()));

            if (this._IsGoToMeetingOn)
            { this._MeetingType.Items.Add(new ListItem(_GlobalResources.GoToMeeting, Convert.ToInt32(StandupTrainingInstance.MeetingType.GoToMeeting).ToString())); }

            if (this._IsGoToWebinarOn)
            { this._MeetingType.Items.Add(new ListItem(_GlobalResources.GoToWebinar, Convert.ToInt32(StandupTrainingInstance.MeetingType.GoToWebinar).ToString())); }

            if (this._IsGoToTrainingOn)
            { this._MeetingType.Items.Add(new ListItem(_GlobalResources.GoToTraining, Convert.ToInt32(StandupTrainingInstance.MeetingType.GoToTraining).ToString())); }

            if (this._IsWebExOn)
            { this._MeetingType.Items.Add(new ListItem(_GlobalResources.WebEx, Convert.ToInt32(StandupTrainingInstance.MeetingType.WebEx).ToString())); }

            this._MeetingType.SelectedIndex = 0;
            this._MeetingType.Attributes.Add("onclick", "SetOptionsForSelectedMeetingType();");

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("MeetingType",
                                                                    _GlobalResources.Type,
                                                                    this._MeetingType.ID,
                                                                    this._MeetingType,
                                                                    true,
                                                                    true,
                                                                    false));

            // open/closed registration
            this._IsClosed = new RadioButtonList();
            this._IsClosed.ID = "RegistrationClosed_Field";
            this._IsClosed.Items.Add(new ListItem(_GlobalResources.Open, "False"));
            this._IsClosed.Items.Add(new ListItem(_GlobalResources.Closed, "True"));
            this._IsClosed.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this._IsClosed.SelectedIndex = 0;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RegistrationClosed",
                                                           _GlobalResources.Registration,
                                                           this._IsClosed.ID,
                                                           this._IsClosed,
                                                           false,
                                                           true,
                                                           false));  


            // organizer information
            List<Control> organizerFieldContainer = new List<Control>();

            // select box
            Panel selectOrganizerWrapper = new Panel();
            selectOrganizerWrapper.ID = "SelectOrganizerWrapperContainer";

            this._SelectOrganizer = new DropDownList();
            this._SelectOrganizer.ID = "SelectOrganizer_Field";
            this._SelectOrganizer.Attributes.Add("onchange", "SelectOrganizerChange(true);");

            this._ChangeOrganizerPassword = new CheckBox();
            this._ChangeOrganizerPassword.ID = "ChangeOrganizerPassword_Field";
            this._ChangeOrganizerPassword.InputAttributes.Add("onclick", "ModifyOrganizerPasswordClick();");
            this._ChangeOrganizerPassword.Text = _GlobalResources.ModifyPassword;

            selectOrganizerWrapper.Controls.Add(this._SelectOrganizer);
            selectOrganizerWrapper.Controls.Add(this._ChangeOrganizerPassword);

            // username field
            Panel organizerUsernameWrapper = new Panel();
            organizerUsernameWrapper.ID = "OrganizerUsernameWrapperContainer";

            Label organizerUsernameLabel = new Label();
            organizerUsernameLabel.CssClass = "OrganizerFieldLabel";
            organizerUsernameLabel.Text = _GlobalResources.Username + ": ";

            this._OrganizerUsername = new TextBox();
            this._OrganizerUsername.ID = "OrganizerUsername_Field";
            this._OrganizerUsername.CssClass = "InputMedium";

            organizerUsernameWrapper.Controls.Add(organizerUsernameLabel);
            organizerUsernameWrapper.Controls.Add(this._OrganizerUsername);

            // password field
            Panel organizerPasswordWrapper = new Panel();
            organizerPasswordWrapper.ID = "OrganizerPasswordWrapperContainer";

            Label organizerPasswordLabel = new Label();
            organizerPasswordLabel.CssClass = "OrganizerFieldLabel";
            organizerPasswordLabel.Text = _GlobalResources.Password + ": ";

            this._OrganizerPassword = new TextBox();
            this._OrganizerPassword.ID = "OrganizerPassword_Field";
            this._OrganizerPassword.TextMode = TextBoxMode.Password;
            this._OrganizerPassword.CssClass = "InputMedium";

            organizerPasswordWrapper.Controls.Add(organizerPasswordLabel);
            organizerPasswordWrapper.Controls.Add(this._OrganizerPassword);

            // selected web meeting organizer id
            this._SelectedWebMeetingOrganizerId = new HiddenField();
            this._SelectedWebMeetingOrganizerId.ID = "SelectedWebMeetingOrganizer_Field";

            // add organizer controls to field container
            organizerFieldContainer.Add(selectOrganizerWrapper);
            organizerFieldContainer.Add(organizerUsernameWrapper);
            organizerFieldContainer.Add(organizerPasswordWrapper);
            organizerFieldContainer.Add(this._SelectedWebMeetingOrganizerId);

            // add controls to container
            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("OrganizerInformation",
                                                                               _GlobalResources.Organizer,
                                                                               organizerFieldContainer,
                                                                               true,
                                                                               true));

            // meeting password
            this._MeetingPassword = new TextBox();
            this._MeetingPassword.ID = "MeetingPassword_Field";
            this._MeetingPassword.CssClass = "InputShort";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("MeetingPassword",
                                                              _GlobalResources.MeetingPassword,
                                                              this._MeetingPassword.ID,
                                                              this._MeetingPassword,
                                                              true,
                                                              true,
                                                              false));

            // seats container
            this._Seats = new TextBox();
            this._Seats.ID = "SessionSeats_Field";
            this._Seats.CssClass = "InputXShort";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionSeats",
                                                              _GlobalResources.Seats,
                                                              this._Seats.ID,
                                                              this._Seats,
                                                              true,
                                                              true,
                                                              false));

            // waiting  seats container
            this._WaitingSeats = new TextBox();
            this._WaitingSeats.ID = "SessionWaitingSeats_Field";
            this._WaitingSeats.CssClass = "InputXShort";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionWaitingSeats",
                                                         _GlobalResources.WaitingSeats,
                                                         this._WaitingSeats.ID,
                                                         this._WaitingSeats,
                                                         true,
                                                         true,
                                                         false));            

            // url registration 
            this._UrlRegistration = new TextBox();
            this._UrlRegistration.ID = "SessionUrlRegistration_Field";
            this._UrlRegistration.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionUrlRegistration",
                                                       _GlobalResources.URLRegistration,
                                                       this._UrlRegistration.ID,
                                                       this._UrlRegistration,
                                                       false,
                                                       true,
                                                       false));


            // url attend 
            this._UrlAttend = new TextBox();
            this._UrlAttend.ID = "SessionUrlAttend_Field";
            this._UrlAttend.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionUrlAttend",
                                                     _GlobalResources.URLAttend,
                                                     this._UrlAttend.ID,
                                                     this._UrlAttend,
                                                     true,
                                                     true,
                                                     false));

            // city 
            this._City = new TextBox();
            this._City.ID = "SessionCity_Field";
            this._City.CssClass = "InputMedium";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionCity",
                                                    _GlobalResources.City,
                                                    this._City.ID,
                                                    this._City,
                                                    true,
                                                    true,
                                                    false));

            // province 
            this._Province = new TextBox();
            this._Province.ID = "SessionProvince_Field";
            this._Province.CssClass = "InputMedium";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionProvince",
                                                  _GlobalResources.StateProvince,
                                                  this._Province.ID,
                                                  this._Province,
                                                  true,
                                                  true,
                                                  false));

            // postal code
            this._PostalCode = new TextBox();
            this._PostalCode.ID = "PostalCode_Field";
            this._PostalCode.CssClass = "InputMedium";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionPostalCode",
                                                _GlobalResources.PostalCode,
                                                this._PostalCode.ID,
                                                this._PostalCode,
                                                false,
                                                false,
                                                false));

            // location description 
            this._LocationDescription = new TextBox();
            this._LocationDescription.ID = "SessionLocationDescription_Field";
            this._LocationDescription.Style.Add("width", "98%");
            this._LocationDescription.TextMode = TextBoxMode.MultiLine;
            this._LocationDescription.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionLocationDescription",
                                                  _GlobalResources.LocationDescription,
                                                  this._LocationDescription.ID,
                                                  this._LocationDescription,
                                                  false,
                                                  true,
                                                  true));

            // time zone
            this._Timezone = new TimeZoneSelector();
            this._Timezone.ID = "RuleSetEnrollmentTimezone_Field";
            this._Timezone.SelectedValue = AsentiaSessionState.GlobalSiteObject.IdTimezone.ToString();

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SessionTimezone",
                                                   _GlobalResources.Timezone,
                                                   this._Timezone.ID,
                                                   this._Timezone,
                                                   true,
                                                   true,
                                                   false));

            // add dates field
            List<Control> datesControls = new List<Control>();            

            // add the meeting dates placeholder wrapper
            datesControls.Add(this._MeetingDateControlPlaceHolder);

            Panel sessionDatesInformationPanel = new Panel();
            sessionDatesInformationPanel.ID = "SessionDatesInformationPanel";
            this.FormatFormInformationPanel(sessionDatesInformationPanel, _GlobalResources.AllMeetingDatesTimesAreForTHISSessionAddingAdditionalDatesTimesWillNotCreateAdditionalSessions);
            datesControls.Add(sessionDatesInformationPanel);

            this._AddDatesButtonContainer = new Panel();
            this._AddDatesButtonContainer.ID = "AddDatesButtonContainer";
            datesControls.Add(this._AddDatesButtonContainer);

            // add date  hyperlink
            HyperLink addDateImageLink = new HyperLink();
            addDateImageLink.ID = "Date_AddLinkImage";
            addDateImageLink.CssClass = "ImageLink";
            addDateImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            addDateImageLink.NavigateUrl = "javascript:AddDateRow(this);";
            this._AddDatesButtonContainer.Controls.Add(addDateImageLink);

            HyperLink addDateLink = new HyperLink();
            addDateLink.ID = "Date_AddLinkText";
            addDateLink.Text = _GlobalResources.AddDate_sToThisSession;
            addDateLink.NavigateUrl = "javascript:AddDateRow(this);";
            this._AddDatesButtonContainer.Controls.Add(addDateLink);

            // dates items JSON hidden field

            this._MeetingsJsonData.ID = "JsonData";
            datesControls.Add(this._MeetingsJsonData);

            //this will be used to restrict from selecting a resource which is alredy allocated in a selected date range
            _SelectedDatesForResources = new HiddenField();
            _SelectedDatesForResources.ID = "DatesForResources";
            _SelectedDatesForResources.Value = " ";
            datesControls.Add(_SelectedDatesForResources);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Dates",
                                                                               _GlobalResources.SessionMeetingDate_s,
                                                                               datesControls,
                                                                               false,
                                                                               true));

            //adding panel to container
            this.SessionPropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildSelectInstructorsModal
        /// <summary>
        /// Builds the modal for selecting the instructor for the standup training instance.
        /// </summary>
        private void _BuildSelectInstructorsModal(string targetControlId)
        {
            // set modal properties
            this._SelectInstructorsForSessionModal = new ModalPopup("SelectInstructorsForSession");
            this._SelectInstructorsForSessionModal.Type = ModalPopupType.Form;
            this._SelectInstructorsForSessionModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR, ImageFiles.EXT_PNG);
            this._SelectInstructorsForSessionModal.HeaderIconAlt = _GlobalResources.SelectInstructor_s;
            this._SelectInstructorsForSessionModal.HeaderText = _GlobalResources.SelectInstructor_s;
            this._SelectInstructorsForSessionModal.TargetControlID = targetControlId;
            this._SelectInstructorsForSessionModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectInstructorsForSessionModal.SubmitButtonCustomText = _GlobalResources.AddInstructor_s;

            if (this._SessionObject != null)
            {
                this._InstructorsForSelectList = UMS.Library.User.IdsAndNamesForInstructorSelectList(this._SessionObject.Id, null);
                this._SelectInstructorsForSessionModal.SubmitButton.OnClientClick = "javascript:AddInstructorsForSession(" + this._SessionObject.Id + "); return false;"; 
            }
            else
            {
                this._InstructorsForSelectList = UMS.Library.User.IdsAndNamesForInstructorSelectList(0, null);
                this._SelectInstructorsForSessionModal.SubmitButton.OnClientClick = "javascript:AddInstructorsForSession(0); return false;"; 
            }

            this._SelectInstructorsForSessionModal.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectInstructorsForSessionModal.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectInstructorsForSessionModal.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleInstructors = new DynamicListBox("SelectEligibleInstructors");
            this._SelectEligibleInstructors.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectEligibleInstructors.IncludeSelectAllNone = true;
            this._SelectEligibleInstructors.IsMultipleSelect = true;
            this._SelectEligibleInstructors.SearchButton.Command += new CommandEventHandler(this._SearchSelectInstructorButton_Command);
            this._SelectEligibleInstructors.SearchButton.OnClientClick = "javascript:GetSelectedDates(this,'" + this._MeetingsJsonData.ClientID + "');";
            this._SelectEligibleInstructors.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectInstructorButton_Command);
            this._SelectEligibleInstructors.ListBoxControl.DataSource = this._InstructorsForSelectList;
            this._SelectEligibleInstructors.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleInstructors.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleInstructors.ListBoxControl.DataBind();

            // add controls to body
            this._SelectInstructorsForSessionModal.AddControlToBody(this._SelectEligibleInstructors);

            //adding maintenance submit button to simulate button click to show error message.    
            this._SelectInstructorsForSessionButton = new Button();
            this._SelectInstructorsForSessionButton.ClientIDMode = ClientIDMode.Static;
            this._SelectInstructorsForSessionButton.ID = "SelectInstructorForSessionButton";
            this._SelectInstructorsForSessionButton.Command += new CommandEventHandler(this._SelectInstructorForSessionButton_Command);
            this._SelectInstructorsForSessionButton.Style.Add("display", "none");

            // add controls to body
            this._SelectInstructorsForSessionModal.AddControlToBody(this._SelectInstructorsForSessionButton);

            // add modal to container
            this.SessionPropertiesContainer.Controls.Add(this._SelectInstructorsForSessionModal);
        }
        #endregion

        #region _SearchSelectInstructorButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Instructor" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectInstructorButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // clear the modal's feedback container
                this._SelectInstructorsForSessionModal.ClearFeedback();

                // GET ALL SELECTED DATES
                this._GetSelectedDateRanges();

                // clear the listbox control
                this._SelectEligibleInstructors.ListBoxControl.Items.Clear();

                // do the search
                if (this._SessionObject != null)
                { this._InstructorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForInstructorSelectList(this._SessionObject.Id, this._SelectEligibleInstructors.SearchTextBox.Text); }
                else
                { this._InstructorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForInstructorSelectList(0, this._SelectEligibleInstructors.SearchTextBox.Text); }

                // weed out instructors that have scheduling conflicts
                int totalRows = this._InstructorsForSelectList.Rows.Count;
                
                for (int i = 0; i < totalRows; i++)
                {
                    int instructorId = Convert.ToInt32(this._InstructorsForSelectList.Rows[i]["idUser"]);

                    bool result;

                    if (this._SessionObject != null)
                    { result = StandupTrainingInstance.CheckInstructorAvailability(this._SessionObject.Id, instructorId, this._MeetingTimesForModalResourceSearching); }
                    else
                    { result = StandupTrainingInstance.CheckInstructorAvailability(0, instructorId, this._MeetingTimesForModalResourceSearching); }

                    if (!result)
                    {
                        DataRow[] rows = this._InstructorsForSelectList.Select("idUser =" + instructorId);
                        this._InstructorsForSelectList.Rows.Remove(rows[0]);
                        totalRows--;
                    }
                }

                _SelectInstructorListDataBind();
            }
            catch (Exception ex)
            {
                this._SelectInstructorsForSessionModal.DisplayFeedback(ex.Message + ex.StackTrace, true);
            }
        }
        #endregion

        #region _ClearSearchSelectInstructorButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Content Package" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectInstructorButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectInstructorsForSessionModal.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleInstructors.ListBoxControl.Items.Clear();
            this._SelectEligibleInstructors.SearchTextBox.Text = "";
            
            // clear the search
            if (this._SessionObject != null)
            { this._InstructorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForInstructorSelectList(this._SessionObject.Id, null); }
            else
            { this._InstructorsForSelectList = Asentia.UMS.Library.User.IdsAndNamesForInstructorSelectList(0, null); }

            _SelectInstructorListDataBind();
        }
        #endregion

        #region _SelectInstructorListDataBind
        /// <summary>
        /// DataBind for the Modal ListBox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SelectInstructorListDataBind()
        {
            this._SelectEligibleInstructors.ListBoxControl.DataSource = this._InstructorsForSelectList;
            this._SelectEligibleInstructors.ListBoxControl.DataTextField = "displayName";
            this._SelectEligibleInstructors.ListBoxControl.DataValueField = "idUser";
            this._SelectEligibleInstructors.ListBoxControl.DataBind();

            // if records available then enable the list
            if (this._SelectEligibleInstructors.ListBoxControl.Items.Count > 0)
            {
                this._SelectInstructorsForSessionModal.SubmitButton.Enabled = true;
                this._SelectInstructorsForSessionModal.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectInstructorsForSessionModal.SubmitButton.Enabled = false;
                this._SelectInstructorsForSessionModal.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _BuildSessionPropertiesFormInstructorsPanel
        /// <summary>
        /// Builds the instructors form.
        /// </summary>
        private void _BuildSessionPropertiesFormInstructorsPanel()
        {
            // populate datatables with lists of instructors that belong and do not belong to this standup training instance
            if (this._SessionObject != null)
            {
                this._SessionInstructors = this._SessionObject.GetInstructors(null);
                this._EligibleInstructorsForSelectList = UMS.Library.User.IdsAndNamesForInstructorSelectList(this._SessionObject.Id, null);
            }
            else
            {
                this._EligibleInstructorsForSelectList = UMS.Library.User.IdsAndNamesForInstructorSelectList(0, null);
            }

            Panel instructorsPanel = new Panel();
            instructorsPanel.ID = "SessionProperties_" + "Instructors" + "_TabPanel";
            instructorsPanel.Attributes.Add("style", "display: none;");

            // instructors container
            Panel instructorsFieldContainer = new Panel();
            instructorsFieldContainer.ID = "SessionInstructors_Container";
            instructorsFieldContainer.CssClass = "FormFieldContainer";

            // selected instructors hidden field
            this._SelectedInstructors = new HiddenField();
            this._SelectedInstructors.ID = "SessionSelectedInstructors_Field";
            instructorsFieldContainer.Controls.Add(this._SelectedInstructors);

            // session instructors error panel
            Panel sessionInstructorsErrorContainer = new Panel();
            sessionInstructorsErrorContainer.ID = "SessionInstructors_ErrorContainer";
            sessionInstructorsErrorContainer.CssClass = "FormFieldErrorContainer";
            instructorsFieldContainer.Controls.Add(sessionInstructorsErrorContainer);

            // build a container for the instructor listing
            this._InstructorsListContainer = new Panel();
            this._InstructorsListContainer.ID = "SessionInstructorsList_Container";
            this._InstructorsListContainer.CssClass = "ItemListingContainer";

            instructorsFieldContainer.Controls.Add(this._InstructorsListContainer);

            Panel instructorsButtonsPanel = new Panel();
            instructorsButtonsPanel.ID = "SessionInstructors_ButtonsPanel";

            // select instructors button

            // link
            Image selectInstructorsImageForLink = new Image();
            selectInstructorsImageForLink.ID = "LaunchSelectInstructorsModalImage";
            selectInstructorsImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INSTRUCTOR,
                                                                          ImageFiles.EXT_PNG);
            selectInstructorsImageForLink.CssClass = "MediumIcon";

            Localize selectInstructorsTextForLink = new Localize();
            selectInstructorsTextForLink.Text = _GlobalResources.SelectInstructor_s;

            LinkButton selectInstructorsLink = new LinkButton();
            selectInstructorsLink.ID = "LaunchSelectInstructorsModal";
            selectInstructorsLink.CssClass = "ImageLink";
            selectInstructorsLink.Controls.Add(selectInstructorsImageForLink);
            selectInstructorsLink.Controls.Add(selectInstructorsTextForLink);
            selectInstructorsLink.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicElements(this,'" + this._MeetingsJsonData.ClientID + "');");
            instructorsButtonsPanel.Controls.Add(selectInstructorsLink);

            // attach the buttons panel to the container
            instructorsFieldContainer.Controls.Add(instructorsButtonsPanel);

            // build modals for adding and removing instructors to/from the session
            this._BuildSelectInstructorsModal(selectInstructorsLink.ID);

            // add controls to container
            instructorsPanel.Controls.Add(instructorsFieldContainer);
            this.SessionPropertiesTabPanelsContainer.Controls.Add(instructorsPanel);
        }
        #endregion

        #region _BuildSessionPropertiesFormResourcesPanel
        /// <summary>
        /// Builds the resources form.
        /// </summary>
        private void _BuildSessionPropertiesFormResourcesPanel()
        {
            // populate datatables with lists of resources that belong and do not belong to this standup training instance
            if (this._SessionObject != null)
            {
                this._SessionResources = this._SessionObject.GetResources(null);
                this._EligibleResourcesForSelectList = Asentia.LMS.Library.Resource.IdsAndNamesForStandupTrainingInstanceSelectList(this._SessionObject.Id, null);
            }
            else
            {
                this._EligibleResourcesForSelectList = Asentia.LMS.Library.Resource.IdsAndNamesForStandupTrainingInstanceSelectList(0, null);
            }

            Panel resourcesPanel = new Panel();
            resourcesPanel.ID = "SessionProperties_" + "Resources" + "_TabPanel";
            resourcesPanel.Attributes.Add("style", "display: none;");

            // resources container
            Panel resourcesFieldContainer = new Panel();
            resourcesFieldContainer.ID = "SessionResources_Container";
            resourcesFieldContainer.CssClass = "FormFieldContainer";

            // selected resources hidden field
            this._SelectedResources = new HiddenField();
            this._SelectedResources.ID = "SessionSelectedResources_Field";
            resourcesFieldContainer.Controls.Add(this._SelectedResources);

            // session resources error panel
            Panel sessionResourcesErrorContainer = new Panel();
            sessionResourcesErrorContainer.ID = "SessionResources_ErrorContainer";
            sessionResourcesErrorContainer.CssClass = "FormFieldErrorContainer";
            resourcesFieldContainer.Controls.Add(sessionResourcesErrorContainer);

            // build a container for the resources listing
            this._ResourcesListContainer = new Panel();
            this._ResourcesListContainer.ID = "SessionResourcesList_Container";
            this._ResourcesListContainer.CssClass = "ItemListingContainer";

            resourcesFieldContainer.Controls.Add(this._ResourcesListContainer);

            Panel resourcesButtonsPanel = new Panel();
            resourcesButtonsPanel.ID = "SessionResources_ButtonsPanel";

            // select resources button

            // link
            Image selectResourcesImageForLink = new Image();
            selectResourcesImageForLink.ID = "LaunchSelectResourcesModalImage";
            selectResourcesImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSECATALOG,
                                                                          ImageFiles.EXT_PNG);
            selectResourcesImageForLink.CssClass = "MediumIcon";

            Localize selectResourcesTextForLink = new Localize();
            selectResourcesTextForLink.Text = _GlobalResources.SelectResource_s;

            LinkButton selectResourcesLink = new LinkButton();
            selectResourcesLink.ID = "LaunchSelectResourcesModal";
            selectResourcesLink.CssClass = "ImageLink";
            selectResourcesLink.Controls.Add(selectResourcesImageForLink);
            selectResourcesLink.Controls.Add(selectResourcesTextForLink);
            selectResourcesLink.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicElements(this,'" + this._MeetingsJsonData.ClientID + "');");
            resourcesButtonsPanel.Controls.Add(selectResourcesLink);

            // attach the buttons panel to the container
            resourcesFieldContainer.Controls.Add(resourcesButtonsPanel);

            // build modals for adding and removing resources to/from the course
            this._BuildSelectResourcesModal(selectResourcesLink.ID);

            // add controls to container
            resourcesPanel.Controls.Add(resourcesFieldContainer);
            this.SessionPropertiesTabPanelsContainer.Controls.Add(resourcesPanel);
        }
        #endregion

        #region _BuildSelectResourcesModal
        /// <summary>
        /// Builds the modal for selecting resource for session.
        /// </summary>
        private void _BuildSelectResourcesModal(string targetControlId)
        {
            // set modal properties
            this._SelectResourcesForSessionModal = new ModalPopup("SelectResourcesForSession");
            this._SelectResourcesForSessionModal.Type = ModalPopupType.Form;
            this._SelectResourcesForSessionModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSECATALOG,
                                                                                                    ImageFiles.EXT_PNG);
            this._SelectResourcesForSessionModal.HeaderIconAlt = _GlobalResources.SelectResource_s;
            this._SelectResourcesForSessionModal.HeaderText = _GlobalResources.SelectResource_s;
            this._SelectResourcesForSessionModal.TargetControlID = targetControlId;
            this._SelectResourcesForSessionModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectResourcesForSessionModal.SubmitButtonCustomText = _GlobalResources.AddResource_s;
            if (this._SessionObject != null)
            { this._SelectResourcesForSessionModal.SubmitButton.OnClientClick = "javascript:AddResourcesToSession(" + this._SessionObject.Id + "); return false;"; }
            else
            { this._SelectResourcesForSessionModal.SubmitButton.OnClientClick = "javascript:AddResourcesToSession(0); return false;"; }
            this._SelectResourcesForSessionModal.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectResourcesForSessionModal.CloseButtonCustomText = _GlobalResources.Done;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleResources = new DynamicListBox("SelectEligibleResources");
            this._SelectEligibleResources.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectEligibleResources.SearchButton.Command += new CommandEventHandler(this._SearchSelectResourcesButton_Command);
            this._SelectEligibleResources.SearchButton.OnClientClick = "javascript:GetSelectedDates(this,'" + this._MeetingsJsonData.ClientID + "');";
            this._SelectEligibleResources.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectResourcesButton_Command);
            this._SelectEligibleResources.ListBoxControl.DataSource = this._EligibleResourcesForSelectList;
            this._SelectEligibleResources.ListBoxControl.DataTextField = "name";
            this._SelectEligibleResources.ListBoxControl.DataValueField = "idResource";
            this._SelectEligibleResources.ListBoxControl.DataBind();

            //adding maintenance submit button to simulate button click to show error message.    
            this._SelectResourcesForSessionButton = new Button();
            this._SelectResourcesForSessionButton.ClientIDMode = ClientIDMode.Static;
            this._SelectResourcesForSessionButton.ID = "SelectResourcesForSessionButton";
            this._SelectResourcesForSessionButton.Command += new CommandEventHandler(this._SelectResourcesForSessionButton_Command);
            this._SelectResourcesForSessionButton.Style.Add("display", "none");

            // add controls to body
            this._SelectResourcesForSessionModal.AddControlToBody(this._SelectEligibleResources);
            this._SelectResourcesForSessionModal.AddControlToBody(this._SelectResourcesForSessionButton);

            // add modal to container
            this.SessionPropertiesContainer.Controls.Add(this._SelectResourcesForSessionModal);
        }
        #endregion

        #region _SearchSelectResourcesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Resources(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectResourcesButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // clear the modal's feedback container
                this._SelectResourcesForSessionModal.ClearFeedback();

                //GET ALL SELECTED DATES
                this._GetSelectedDateRanges();

                // clear the listbox control
                this._SelectEligibleResources.ListBoxControl.Items.Clear();

                // do the search
                if (this._SessionObject != null)
                { this._EligibleResourcesForSelectList = Asentia.LMS.Library.Resource.IdsAndNamesForStandupTrainingInstanceSelectList(this._SessionObject.Id, this._SelectEligibleResources.SearchTextBox.Text); }
                else
                { this._EligibleResourcesForSelectList = Asentia.LMS.Library.Resource.IdsAndNamesForStandupTrainingInstanceSelectList(0, this._SelectEligibleResources.SearchTextBox.Text); }

                //finding the results which are available for selected time intervals
                int totalRows = this._EligibleResourcesForSelectList.Rows.Count;
                for (int i = 0; i < totalRows; i++)
                {

                    int resourceId = Convert.ToInt32(this._EligibleResourcesForSelectList.Rows[i]["idResource"]);
                    bool result = Resource.CheckForResourceAvailability(this._SessionObject.Id, resourceId, this._MeetingTimesForModalResourceSearching);
                    if (!result)
                    {
                        DataRow[] rows = this._EligibleResourcesForSelectList.Select("idResource =" + resourceId);
                        this._EligibleResourcesForSelectList.Rows.Remove(rows[0]);
                    }
                }

                this._SelectEligibleResources.ListBoxControl.DataSource = this._EligibleResourcesForSelectList;
                this._SelectEligibleResources.ListBoxControl.DataTextField = "name";
                this._SelectEligibleResources.ListBoxControl.DataValueField = "idResource";
                this._SelectEligibleResources.ListBoxControl.DataBind();

                //If no records available then disable the list
                if (this._SelectEligibleResources.ListBoxControl.Items.Count == 0)
                {
                    this._SelectResourcesForSessionModal.SubmitButton.Enabled = false;
                    this._SelectResourcesForSessionModal.SubmitButton.CssClass = "Button ActionButton DisabledButton";
                }
                else
                {
                    this._SelectResourcesForSessionModal.SubmitButton.Enabled = true;
                    this._SelectResourcesForSessionModal.SubmitButton.CssClass = "Button ActionButton";
                }

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _ClearSearchSelectResourcesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Resource(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectResourcesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectResourcesForSessionModal.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleResources.ListBoxControl.Items.Clear();
            this._SelectEligibleResources.SearchTextBox.Text = "";

            // clear the search
            if (this._SessionObject != null)
            { this._EligibleResourcesForSelectList = Asentia.LMS.Library.Resource.IdsAndNamesForStandupTrainingInstanceSelectList(this._SessionObject.Id, null); }
            else
            { this._EligibleResourcesForSelectList = Asentia.LMS.Library.Resource.IdsAndNamesForStandupTrainingInstanceSelectList(0, null); }

            this._SelectEligibleResources.ListBoxControl.DataSource = this._EligibleResourcesForSelectList;
            this._SelectEligibleResources.ListBoxControl.DataTextField = "name";
            this._SelectEligibleResources.ListBoxControl.DataValueField = "idResource";
            this._SelectEligibleResources.ListBoxControl.DataBind();

            //If records available then enable the list
            if (this._SelectEligibleResources.ListBoxControl.Items.Count > 0)
            {
                this._SelectResourcesForSessionModal.SubmitButton.Enabled = true;
                this._SelectResourcesForSessionModal.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectResourcesForSessionModal.SubmitButton.Enabled = false;
                this._SelectResourcesForSessionModal.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _BuildSesssionPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for properties actions.
        /// </summary>
        private void _BuildSessionPropertiesActionsPanel()
        {            
            // clear controls from container
            this.SessionPropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.SessionPropertiesActionsPanel.CssClass = "ActionsPanel";

            //add hidden field which will be used to communicate with java script

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._SessionObject == null || this._IsDuplicatingSession)
            { this._SaveButton.Text = _GlobalResources.CreateSession; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this._SaveButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicElements(this,'" + this._MeetingsJsonData.ClientID + "');");
            this.SessionPropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.SessionPropertiesActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _PopulateSessionPropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateSessionPropertiesInputElements()
        {
            
            if (this._SessionObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES
                foreach (StandupTrainingInstance.LanguageSpecificProperty sessionLanguageSpecificProperty in this._SessionObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (sessionLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._SessionTitle.Text = sessionLanguageSpecificProperty.Title;
                        this._Description.Text = sessionLanguageSpecificProperty.Description;
                        this._LocationDescription.Text = sessionLanguageSpecificProperty.LocationDescription;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificCourseTitleTextBox = (TextBox)this.SessionPropertiesContainer.FindControl(this._SessionTitle.ID + "_" + sessionLanguageSpecificProperty.LangString);
                        TextBox languageSpecificCourseShortDescriptionTextBox = (TextBox)this.SessionPropertiesContainer.FindControl(this._Description.ID + "_" + sessionLanguageSpecificProperty.LangString);
                        TextBox languageSpecificCourseLongDescriptionTextBox = (TextBox)this.SessionPropertiesContainer.FindControl(this._LocationDescription.ID + "_" + sessionLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificCourseTitleTextBox != null)
                        { languageSpecificCourseTitleTextBox.Text = sessionLanguageSpecificProperty.Title; }

                        if (languageSpecificCourseShortDescriptionTextBox != null)
                        { languageSpecificCourseShortDescriptionTextBox.Text = sessionLanguageSpecificProperty.Description; }

                        if (languageSpecificCourseLongDescriptionTextBox != null)
                        { languageSpecificCourseLongDescriptionTextBox.Text = sessionLanguageSpecificProperty.LocationDescription; }

                    }
                }
                // NON-LANGUAGE SPECIFIC PROPERTIES

                // seats
                this._Seats.Text = this._SessionObject.Seats.ToString();

                // waiting seats
                this._WaitingSeats.Text = this._SessionObject.WaitingSeats.ToString();

                // type
                this._MeetingType.SelectedValue = Convert.ToInt32(this._SessionObject.Type).ToString();

                // if editing an existing meeting with any API connectivity, dont allow the type to change,
                // don't allow the organizer to change, and put the selected organizer's id into the hidden field
                if (
                    this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToMeeting
                    || this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToTraining
                    || this._SessionObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar
                    || this._SessionObject.Type == StandupTrainingInstance.MeetingType.WebEx
                   )
                { 
                    this._MeetingType.Enabled = false;
                    
                    if (this._SessionObject.IdWebMeetingOrganizer != null)
                    {
                        this._SelectOrganizer.SelectedValue = this._SessionObject.IdWebMeetingOrganizer.ToString();
                        this._SelectedWebMeetingOrganizerId.Value = this._SessionObject.IdWebMeetingOrganizer.ToString();
                    }
                    else
                    {
                        this._SelectOrganizer.SelectedValue = "NULL";
                        this._SelectedWebMeetingOrganizerId.Value = "NULL";
                    }
                }

                // meeting password
                this._MeetingPassword.Text = this._SessionObject.MeetingPassword;

                // url registration
                this._UrlRegistration.Text = this._SessionObject.UrlRegistration;

                // url attend
                this._UrlAttend.Text = this._SessionObject.UrlAttend;

                // registration closed
                if (this._SessionObject.IsClosed == true)
                { this._IsClosed.SelectedValue = "True"; }
                else
                { this._IsClosed.SelectedValue = "False"; }

                // city
                this._City.Text = this._SessionObject.City;

                // province
                this._Province.Text = this._SessionObject.Province;

                // postcode
                this._PostalCode.Text = this._SessionObject.PostalCode;

                /* INSTRUCTORS */

                // loop through the datatable and add each instructor to the listing container
                foreach (DataRow row in this._SessionInstructors.Rows)
                {
                    // container
                    Panel instructorNameContainer = new Panel();
                    instructorNameContainer.ID = "Instructor_" + row["idInstructor"].ToString();

                    // remove instructor button
                    Image removeInstructorImage = new Image();
                    removeInstructorImage.ID = "Instructor_" + row["idInstructor"].ToString() + "_RemoveImage";
                    removeInstructorImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                          ImageFiles.EXT_PNG);
                    removeInstructorImage.CssClass = "SmallIcon";
                    removeInstructorImage.Attributes.Add("onClick", "javascript:RemoveInstructorFromSession('" + row["idInstructor"].ToString() + "');");
                    removeInstructorImage.Style.Add("cursor", "pointer");

                    // instructor name
                    Literal instructorName = new Literal();
                    instructorName.Text = row["displayNameWithUsername"].ToString();

                    // add controls to container
                    instructorNameContainer.Controls.Add(removeInstructorImage);
                    instructorNameContainer.Controls.Add(instructorName);
                    this._InstructorsListContainer.Controls.Add(instructorNameContainer);
                }

                /* RESOURCES */

                // loop through the datatable and add each resource to the listing container
                foreach (DataRow row in this._SessionResources.Rows)
                {
                    // container
                    Panel resourceNameContainer = new Panel();
                    resourceNameContainer.ID = "Resource_" + row["idResource"].ToString();

                    // remove rsource button
                    Image removeResourceImage = new Image();
                    removeResourceImage.ID = "Resource_" + row["idResource"].ToString() + "_RemoveImage";
                    removeResourceImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                          ImageFiles.EXT_PNG);
                    removeResourceImage.CssClass = "SmallIcon";
                    removeResourceImage.Attributes.Add("onClick", "javascript:RemoveResourceFromSession('" + row["idResource"].ToString() + "');");
                    removeResourceImage.Style.Add("cursor", "pointer");

                    // resource name
                    Literal resourceName = new Literal();
                    resourceName.Text = row["name"].ToString();

                    // add controls to container
                    resourceNameContainer.Controls.Add(removeResourceImage);
                    resourceNameContainer.Controls.Add(resourceName);
                    this._ResourcesListContainer.Controls.Add(resourceNameContainer);
                }
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;
            bool instructorsTabHasErrors = false;
            bool resourcesTabHasErrors = false;

            // TITLE
            if (String.IsNullOrWhiteSpace(this._SessionTitle.Text))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionTitle", _GlobalResources.Title + " " + _GlobalResources.IsRequired);
            }

            // SEATS - REQUIRED IF CLASSROOM OR ONLINE & MUST BE INT
            if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.Classroom || (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.Online)
            {
                if (String.IsNullOrWhiteSpace(this._Seats.Text))
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionSeats", _GlobalResources.Seats + " " + _GlobalResources.IsRequired);
                }
                else
                {
                    int seats;
                    if (!int.TryParse(this._Seats.Text, out seats))
                    {
                        propertiesTabHasErrors = true;
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionSeats", _GlobalResources.Seats + " " + _GlobalResources.IsInvalid);
                    }
                    if (seats < 0)
                    {
                        propertiesTabHasErrors = true;
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionSeats", _GlobalResources.Seats + " " + _GlobalResources.IsInvalid);
                    }
                }
            }

            // WAITING SEATS - REQUIRED & MUST BE INT
            if (String.IsNullOrWhiteSpace(this._WaitingSeats.Text))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionWaitingSeats", _GlobalResources.WaitingSeats + " " + _GlobalResources.IsRequired);
            }
            else
            {
                int waitingSeats;

                if (!int.TryParse(this._WaitingSeats.Text, out waitingSeats))
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionWaitingSeats", _GlobalResources.WaitingSeats + " " + _GlobalResources.IsInvalid);
                }
                if (waitingSeats < 0)
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionWaitingSeats", _GlobalResources.WaitingSeats + " " + _GlobalResources.IsInvalid);
                }
            }

            // CHECK REQUIRED FIELDS BASED ON THE MEETING TYPE
            // WHEN "Classroom" IS SELECTED, "City" AND "Province" ARE REQUIRED
            // WHEN "Online" IS SELECTED, "URL Attend" IS REQUIRED
            // WHEN "WebEx" IS SELECTED, "Password" IS REQUIRED            
            if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.Classroom)
            {                
                if (String.IsNullOrWhiteSpace(this._City.Text))
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionCity", _GlobalResources.City + " " + _GlobalResources.IsRequired);
                }

                if (String.IsNullOrWhiteSpace(this._Province.Text))
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionProvince", _GlobalResources.Province + " " + _GlobalResources.IsRequired);
                }
            }
            else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.Online)
            {
                if (String.IsNullOrWhiteSpace(this._UrlAttend.Text))
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionUrlAttend", _GlobalResources.URLAttend + " " + _GlobalResources.IsRequired);
                }
            }
            else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.WebEx)
            {
                if (String.IsNullOrWhiteSpace(this._MeetingPassword.Text))
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "MeetingPassword", _GlobalResources.Password + " " + _GlobalResources.IsRequired);
                }
            }
            else
            { }

            // TIMEZONE - ALWAYS REQUIRED, AND MUST BE A NUMBER
            if (String.IsNullOrWhiteSpace(this._Timezone.SelectedValue))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionTimezone", _GlobalResources.Timezone + " " + _GlobalResources.IsRequired);
            }

            int idTimezone;

            if (!int.TryParse(this._Timezone.SelectedValue, out idTimezone))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionTimezone", _GlobalResources.Timezone + " " + _GlobalResources.IsInvalid);
            }

            // get the timezone's "dotnetname" from a timezone object
            this._TzDotNetName = new Timezone(Convert.ToInt32(idTimezone)).dotNetName;

            // MEETING DATE(S) - PASSED TO ANOTHER METHOD
            if (!this._ValidateAndGetMeetingsDetailsForSave())
            {
                isValid = false;
                propertiesTabHasErrors = true;
            }            

            // RESOURCES
            bool isResourceAvailable = true;

            if (!String.IsNullOrWhiteSpace(this._SelectedResources.Value) && this._MeetingTimesToValidate.Rows.Count > 0)
            {
                // split the "value" of the hidden field to get an array of resource ids
                string[] selectedResourceIds = this._SelectedResources.Value.Split(',');

                if (selectedResourceIds != null && selectedResourceIds.Length > 0)
                {
                    foreach (string resourceId in selectedResourceIds)
                    {
                        isResourceAvailable = Resource.CheckForResourceAvailability(this._SessionObject.Id, Convert.ToInt32(resourceId), this._MeetingTimesToSave);

                        if (selectedResourceIds.Length > 1 && !isResourceAvailable)
                        {this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionResources", _GlobalResources.SelectedResourceIsNotAvailableOnSelectedTimeInterval_s); }
                        else if (selectedResourceIds.Length == 1 && !isResourceAvailable)
                        {this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionResources", _GlobalResources.SelectedResourceIsNotAvailableOnSelectedTimeInterval_s); }

                        //if any one of the resource is not available then break the loop and show the message of not available
                        if (!isResourceAvailable)
                        {
                            resourcesTabHasErrors = true;
                            break;
                        }
                    }
                }
            }

            // INSTRUCTOR
            bool isInstructorAvailable = true;

            if (!String.IsNullOrWhiteSpace(this._SelectedInstructors.Value))
            {
                // split the "value" of the hidden field to get an array of resource ids
                string[] selectedInstructorIds = this._SelectedInstructors.Value.Split(',');

                if (selectedInstructorIds != null && selectedInstructorIds.Length > 0)
                {
                    foreach (string instructorId in selectedInstructorIds)
                    {
                        isInstructorAvailable = StandupTrainingInstance.CheckInstructorAvailability(this._SessionObject.Id, Convert.ToInt32(instructorId), this._MeetingTimesToSave);

                        if (selectedInstructorIds.Length > 1 && !isInstructorAvailable)
                        { this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionInstructors", _GlobalResources.TheSelectedInstructorIsNotAvailableForOneOrMoreOfTheSessionsMeetingTime_s); }
                        else if (selectedInstructorIds.Length == 1 && !isInstructorAvailable)
                        { this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "SessionInstructors", _GlobalResources.TheSelectedInstructorIsNotAvailableForOneOrMoreOfTheSessionsMeetingTime_s); }

                        //if any one of the instructor is not available then break the loop and show the message of not available
                        if (!isInstructorAvailable)
                        {
                            instructorsTabHasErrors = true;
                            break;
                        }
                    }
                }
            }

            // ORGANIZER
            bool isOrganizerAvailable = true;

            if (
                (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToMeeting
                || (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToTraining
                || (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToWebinar
                || (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.WebEx
               )
            {                
                if (!String.IsNullOrWhiteSpace(this._SelectedWebMeetingOrganizerId.Value) && this._MeetingTimesToValidate.Rows.Count > 0)
                {
                    int? selectedOrganizer = null;

                    if (this._SelectedWebMeetingOrganizerId.Value != "NULL") 
                    { selectedOrganizer = Convert.ToInt32(this._SelectedWebMeetingOrganizerId.Value); }

                    isOrganizerAvailable = StandupTrainingInstance.CheckOrganizerAvailability(this._SessionObject.Id, selectedOrganizer, Convert.ToInt32(this._MeetingType.SelectedValue), this._MeetingTimesToSave);

                    if (!isOrganizerAvailable)
                    {
                        propertiesTabHasErrors = true;
                        this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesContainer, "OrganizerInformation", _GlobalResources.TheSelectedOrganizerHasASessionWithMeetingTime_sThatConflictWithThisSessionsMeetingTime_s);
                    }
                }
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.SessionPropertiesContainer, "SessionProperties_Properties_TabLI"); }

            if (instructorsTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.SessionPropertiesContainer, "SessionProperties_Instructors_TabLI"); }

            if (resourcesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.SessionPropertiesContainer, "SessionProperties_Resources_TabLI"); }

            isValid = isResourceAvailable && isInstructorAvailable && isOrganizerAvailable && isValid;

            // if this is invalid, we need to pre-populate multi-select fields with the objects that were selected
            if (!isValid)
            {
                /* INSTRUCTOR */
                if (!String.IsNullOrWhiteSpace(this._SelectedInstructors.Value))
                {
                    // split the "value" of the hidden field to get an array of instructor ids
                    string[] selectedInstructors = this._SelectedInstructors.Value.Split(',');

                    // loop through the array and add each instructor to the listing container
                    foreach (string instructorId in selectedInstructors)
                    {
                        if (this._ResourcesListContainer.FindControl("Instructor_" + instructorId) == null)
                        {
                            // user object
                            User user = new User(Convert.ToInt32(instructorId));

                            // container
                            Panel instructorNameContainer = new Panel();
                            instructorNameContainer.ID = "Instructor_" + instructorId;

                            // remove instructor button
                            Image removeInstructorImage = new Image();
                            removeInstructorImage.ID = "Resource_" + instructorId + "_RemoveImage";
                            removeInstructorImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                  ImageFiles.EXT_PNG);
                            removeInstructorImage.CssClass = "SmallIcon";
                            removeInstructorImage.Attributes.Add("onClick", "javascript:RemoveResourceFromSession('" + instructorId + "');");
                            removeInstructorImage.Style.Add("cursor", "pointer");

                            // instructor name
                            Literal instructorName = new Literal();
                            instructorName.Text = user.DisplayName;

                            // add controls to container
                            instructorNameContainer.Controls.Add(removeInstructorImage);
                            instructorNameContainer.Controls.Add(instructorName);
                            this._InstructorsListContainer.Controls.Add(instructorNameContainer);
                        }
                    }
                }

                /* RESOURCES */
                if (!String.IsNullOrWhiteSpace(this._SelectedResources.Value))
                {
                    // split the "value" of the hidden field to get an array of course expert ids
                    string[] selectedResources = this._SelectedResources.Value.Split(',');

                    // loop through the array and add each resource to the listing container
                    foreach (string resourceId in selectedResources)
                    {
                        if (this._ResourcesListContainer.FindControl("Resource_" + resourceId) == null)
                        {
                            // user object
                            Library.Resource resourceObject = new Library.Resource(Convert.ToInt32(resourceId));

                            // container
                            Panel resourceNameContainer = new Panel();
                            resourceNameContainer.ID = "Resource_" + resourceObject.IdResource.ToString();

                            // remove resource button
                            Image removeResourceImage = new Image();
                            removeResourceImage.ID = "Resource_" + resourceObject.IdResource.ToString() + "_RemoveImage";
                            removeResourceImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                                  ImageFiles.EXT_PNG);
                            removeResourceImage.CssClass = "SmallIcon";
                            removeResourceImage.Attributes.Add("onClick", "javascript:RemoveResourceFromSession('" + resourceObject.IdResource.ToString() + "');");
                            removeResourceImage.Style.Add("cursor", "pointer");

                            // resource name
                            Literal resourceName = new Literal();
                            resourceName.Text = resourceObject.Name.ToString();

                            // add controls to container
                            resourceNameContainer.Controls.Add(removeResourceImage);
                            resourceNameContainer.Controls.Add(resourceName);
                            this._ResourcesListContainer.Controls.Add(resourceNameContainer);
                        }
                    }

                }
            }

            return isValid;
        }
        #endregion

        #region _ValidateMeetingsDates
        /// <summary>
        /// Validates dynamic date control selected dates
        /// </summary>
        /// <returns></returns>
        private bool _ValidateMeetingsDates()
        {
            bool isValid = true;

            // start date
            if (this._DtStart == null)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesTabPanelsContainer, "Dates", _GlobalResources.StartDate + " " + _GlobalResources.IsRequired);
                return isValid;
            }

            // end date
            if (this._DtEnd == null)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesTabPanelsContainer, "Dates", _GlobalResources.EndDate + " " + _GlobalResources.IsRequired);
                return isValid;
            }

            // only apply constraint validations if the other validations have passed
            if (isValid)
            {
                // date start - must be in future only if "GTM", "GTT", "GTW", or "WebEx", only validate if start has not passed
                DateTime dtStart;
                dtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtStart, TimeZoneInfo.FindSystemTimeZoneById(this._TzDotNetName));

                if ( 
                    (
                     (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToMeeting
                     || (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToTraining
                     || (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToWebinar
                     || (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.WebEx
                    )
                    && dtStart < AsentiaSessionState.UtcNow 
                    && !this._IsExistingDate
                   )
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesTabPanelsContainer, "Dates", _GlobalResources.StartDate + " " + _GlobalResources.MustBeInFuture);
                }

                // date end - if set, cannot be less than date start
                DateTime? dtEnd = null;
                dtEnd = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtEnd, TimeZoneInfo.FindSystemTimeZoneById(this._TzDotNetName));

                if (dtEnd <= dtStart)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesTabPanelsContainer, "Dates", _GlobalResources.EndDate + " " + _GlobalResources.CannotBeBeforeStartDate);
                }
            }

            return isValid;
        }

        #endregion

        #region _ValidateDatesOverlaping
        /// <summary>
        /// Checking for overlaping of date ranges
        /// </summary>
        /// <param name="ranges"></param>
        /// <returns></returns>
        private bool _ValidateDatesOverlaping(List<Tuple<DateTime, DateTime>> ranges)
        {
            bool isValid = true;

            for (int i = 0; i < ranges.Count; i++)
            {
                for (int j = i + 1; j < ranges.Count; j++)
                {
                    bool condition1 = ranges[i].Item1 <= ranges[j].Item2;
                    bool condition2 = ranges[i].Item2 >= ranges[j].Item1;

                    if ((condition1 && condition2))
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesTabPanelsContainer, "Dates", _GlobalResources.DatesShouldNotBeOverlappingForASession);
                    }
                }
            }

            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {            
            try
            {
                // if there is no standup training instance object, create one
                if (this._SessionObject == null || this._IsDuplicatingSession)
                { this._SessionObject = new Library.StandupTrainingInstance(); }                

                // validate the form
                if (!this._ValidatePropertiesForm())
                {
                    this._IsHiddenFieldData = true;
                    throw new AsentiaException();
                }
                else
                { this._IsHiddenFieldData = false; }

                // IF THIS IS A WEB-MEETING INTEGRATED SESSION, CREATE OR UPDATE (if exsiting) IT USING THE PLATFORM'S API (GTM, GTW, GTT, or WebEx)
                bool isNewSession = (this._SessionObject.Id == 0) ? true : false;
                Int64? webMeetingId = null;
                string hostUrl = null;
                string genericJoinUrl = null;
                int? idWebMeetingOrganizer = null;

                if (String.IsNullOrWhiteSpace(this._SelectedWebMeetingOrganizerId.Value))
                { this._SelectedWebMeetingOrganizerId.Value = "NULL"; }

                try
                {
                    if (
                        (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToMeeting
                        || (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToWebinar
                        || (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToTraining
                        || (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.WebEx
                    )
                    {
                        // before we start to do anything with web meeting integrated APIs, lets validate organizer information first
                        AsentiaAESEncryption cryptoProvider = new AsentiaAESEncryption();
                        string organizerUsername = String.Empty;
                        string organizerPassword = String.Empty;
                        bool isDefaultOrganizer = false;
                        bool isNewOrganizer = false;
                        bool modifyOrganizerPassword = false;

                        if (this._SelectedWebMeetingOrganizerId.Value == "NULL")
                        { isDefaultOrganizer = true; }
                        else if (this._SelectedWebMeetingOrganizerId.Value == "0")
                        { isNewOrganizer = true; }
                        else
                        {
                            if (isNewSession)
                            { idWebMeetingOrganizer = Convert.ToInt32(this._SelectedWebMeetingOrganizerId.Value); }
                            else
                            { idWebMeetingOrganizer = this._SessionObject.IdWebMeetingOrganizer; }
                        }

                        if (this._ChangeOrganizerPassword.Checked)
                        { modifyOrganizerPassword = true; }

                        // set organizer username and password based on selections made
                        if (!isDefaultOrganizer && isNewOrganizer)
                        {
                            organizerUsername = this._OrganizerUsername.Text.Trim();
                            organizerPassword = this._OrganizerPassword.Text.Trim();
                        }
                        else if (!isDefaultOrganizer && !isNewOrganizer)
                        {
                            WebMeetingOrganizer webMeetingOrganizer = new WebMeetingOrganizer((int)idWebMeetingOrganizer);
                            organizerUsername = webMeetingOrganizer.Username;

                            if (modifyOrganizerPassword) // do not modify the password here, wait until the API call succeeds
                            { organizerPassword = this._OrganizerPassword.Text.Trim(); }
                            else
                            { organizerPassword = cryptoProvider.Decrypt(webMeetingOrganizer.Password); }
                        }
                        else if (isDefaultOrganizer && !isNewOrganizer)
                        {
                            // validate the default organizer password and set based on selected meeting type
                            if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToMeeting)
                            {
                                organizerUsername = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_USERNAME);
                                organizerPassword = cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PASSWORD));

                                if (this._OrganizerPassword.Text.Trim() != organizerPassword)
                                { throw new AsentiaException(_GlobalResources.TheDefaultOrganizerPasswordIsIncorrect); }
                            }
                            else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToWebinar)
                            {
                                organizerUsername = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_USERNAME);
                                organizerPassword = cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PASSWORD));

                                if (this._OrganizerPassword.Text.Trim() != organizerPassword)
                                { throw new AsentiaException(_GlobalResources.TheDefaultOrganizerPasswordIsIncorrect); }
                            }
                            else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToTraining)
                            {
                                organizerUsername = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_USERNAME);
                                organizerPassword = cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PASSWORD));

                                if (this._OrganizerPassword.Text.Trim() != organizerPassword)
                                { throw new AsentiaException(_GlobalResources.TheDefaultOrganizerPasswordIsIncorrect); }
                            }
                            else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.WebEx)
                            {
                                organizerUsername = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_USERNAME);
                                organizerPassword = cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PASSWORD));

                                if (this._OrganizerPassword.Text.Trim() != organizerPassword)
                                { throw new AsentiaException(_GlobalResources.TheDefaultOrganizerPasswordIsIncorrect); }
                            }
                            else
                            { }
                        }

                        // now, proceed to do API work

                        // GTM
                        if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToMeeting)
                        {
                            // if this is a new session, create a new meeting; otherwise, update the existing one
                            if (isNewSession)
                            {
                                GoToMeetingAPI gtmApi = new GoToMeetingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_APPLICATION),
                                                                           organizerUsername,
                                                                           organizerPassword,
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN),
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_CONSUMERSECRET));

                                GoToMeetingAPI.GTM_Meeting_Response gtmMeetingResponse = gtmApi.CreateMeeting(this._SessionTitle.Text,
                                                                                                          Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtStart"]),
                                                                                                          Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtEnd"]),
                                                                                                          false,
                                                                                                          null);

                                webMeetingId = (Int64)gtmMeetingResponse.MeetingId;
                                hostUrl = gtmMeetingResponse.HostURL;
                                genericJoinUrl = gtmMeetingResponse.JoinURL;

                            }
                            else
                            {
                                GoToMeetingAPI gtmApi = new GoToMeetingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_APPLICATION),
                                                                           organizerUsername,
                                                                           organizerPassword,
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_PLAN),
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTM_CONSUMERSECRET));

                                gtmApi.UpdateMeeting((Int64)this._SessionObject.IntegratedObjectKey,
                                                     this._SessionTitle.Text,
                                                     Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtStart"]),
                                                     Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtEnd"]),
                                                     false,
                                                     null);
                            }
                        }
                        // GTW
                        else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToWebinar)
                        {
                            string tzDotNetName = new Timezone(Convert.ToInt32(this._Timezone.SelectedValue)).dotNetName;
                            string cleanedDescription = String.Empty;

                            if (!String.IsNullOrWhiteSpace(this._Description.Text))
                            {
                                HtmlAgilityPack.HtmlDocument descriptionHtml = new HtmlAgilityPack.HtmlDocument();
                                descriptionHtml.LoadHtml(this._Description.Text);
                                cleanedDescription = descriptionHtml.DocumentNode.InnerText;
                            }

                            if (isNewSession)
                            {
                                GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                           organizerUsername,
                                                                           organizerPassword,
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                                GoToWebinarAPI.GTW_Webinar_Response gtwMeetingResponse = gtwApi.CreateWebinar(this._SessionTitle.Text,
                                                                                                              cleanedDescription,
                                                                                                              Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtStart"]),
                                                                                                              Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtEnd"]),
                                                                                                              false,
                                                                                                              tzDotNetName);

                                webMeetingId = (Int64)gtwMeetingResponse.WebinarKey;
                                hostUrl = null;
                                genericJoinUrl = null;
                            }
                            else
                            {
                                GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                           organizerUsername,
                                                                           organizerPassword,
                                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                                gtwApi.UpdateWebinar((Int64)this._SessionObject.IntegratedObjectKey,
                                                     this._SessionTitle.Text,
                                                     cleanedDescription,
                                                     Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtStart"]),
                                                     Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtEnd"]),
                                                     false,
                                                     tzDotNetName);
                            }
                        }
                        // GTT
                        else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToTraining)
                        {
                            string tzDotNetName = new Timezone(Convert.ToInt32(this._Timezone.SelectedValue)).dotNetName;
                            string cleanedDescription = String.Empty;

                            if (!String.IsNullOrWhiteSpace(this._Description.Text))
                            {
                                HtmlAgilityPack.HtmlDocument descriptionHtml = new HtmlAgilityPack.HtmlDocument();
                                descriptionHtml.LoadHtml(this._Description.Text);
                                cleanedDescription = descriptionHtml.DocumentNode.InnerText;
                            }

                            if (isNewSession)
                            {
                                GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                             organizerUsername,
                                                                             organizerPassword,
                                                                             AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                                List<Tuple<DateTime, DateTime>> meetingTimesList = new List<Tuple<DateTime, DateTime>>();

                                foreach (DataRow meetingTimeRow in this._MeetingTimesToSave.Rows)
                                { meetingTimesList.Add(new Tuple<DateTime, DateTime>(Convert.ToDateTime(meetingTimeRow["dtStart"]), Convert.ToDateTime(meetingTimeRow["dtEnd"]))); }

                                GoToTrainingAPI.GTT_Training_Response gttMeetingResponse = gttApi.CreateTraining(this._SessionTitle.Text,
                                                                                                                 cleanedDescription,
                                                                                                                 meetingTimesList,
                                                                                                                 tzDotNetName);

                                webMeetingId = (Int64)gttMeetingResponse.TrainingKey;
                                hostUrl = gttMeetingResponse.StartUrl;
                                genericJoinUrl = null;
                            }
                            else
                            {
                                GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                             organizerUsername,
                                                                             organizerPassword,
                                                                             AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                                List<Tuple<DateTime, DateTime>> meetingTimesList = new List<Tuple<DateTime, DateTime>>();

                                foreach (DataRow meetingTimeRow in this._MeetingTimesToSave.Rows)
                                { meetingTimesList.Add(new Tuple<DateTime, DateTime>(Convert.ToDateTime(meetingTimeRow["dtStart"]), Convert.ToDateTime(meetingTimeRow["dtEnd"]))); }

                                gttApi.UpdateTraining((Int64)this._SessionObject.IntegratedObjectKey,
                                                      this._SessionTitle.Text,
                                                      cleanedDescription,
                                                      meetingTimesList,
                                                      tzDotNetName);
                            }
                        }
                        // WebEx
                        else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.WebEx)
                        {
                            string tzDotNetName = new Timezone(Convert.ToInt32(this._Timezone.SelectedValue)).dotNetName;

                            if (isNewSession)
                            {
                                WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                               organizerUsername,
                                                               organizerPassword);

                                WebExAPI.WebEx_Meeting_Response wbxMeetingResponse = wbxApi.CreateMeeting(this._SessionTitle.Text,
                                                                                                          this._MeetingPassword.Text,
                                                                                                          TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtStart"]), TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName)),
                                                                                                          TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtEnd"]), TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName)),
                                                                                                          tzDotNetName,
                                                                                                          this._GetMaxAttendeesForWebMeeting("WebEx"));

                                webMeetingId = (Int64)wbxMeetingResponse.MeetingKey;
                                hostUrl = wbxMeetingResponse.HostUrl;
                                genericJoinUrl = wbxMeetingResponse.JoinUrl;
                            }
                            else
                            {
                                WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                               organizerUsername,
                                                               organizerPassword);

                                wbxApi.UpdateMeeting((Int64)this._SessionObject.IntegratedObjectKey,
                                                     this._SessionTitle.Text,
                                                     this._MeetingPassword.Text,
                                                     TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtStart"]), TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName)),
                                                     TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(this._MeetingTimesToSave.Rows[0]["dtEnd"]), TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName)),
                                                     tzDotNetName,
                                                     this._GetMaxAttendeesForWebMeeting("WebEx"));
                            }
                        }
                        else
                        { }

                        // getting this far means we've succeeded in the API call, so we can save organizer credentials if new
                        // organizer, or update the password if we should update the password

                        if (isNewOrganizer)
                        {
                            WebMeetingOrganizer webMeetingOrganizer = new WebMeetingOrganizer();

                            webMeetingOrganizer.IdUser = AsentiaSessionState.IdSiteUser;
                            webMeetingOrganizer.OrganizerType = Convert.ToInt32(this._MeetingType.SelectedValue);
                            webMeetingOrganizer.Username = organizerUsername;
                            webMeetingOrganizer.Password = cryptoProvider.Encrypt(organizerPassword);

                            idWebMeetingOrganizer = webMeetingOrganizer.Save();
                        }
                        else
                        {
                            if (!isDefaultOrganizer && modifyOrganizerPassword && idWebMeetingOrganizer != null)
                            {
                                WebMeetingOrganizer webMeetingOrganizer = new WebMeetingOrganizer((int)idWebMeetingOrganizer);

                                webMeetingOrganizer.Password = cryptoProvider.Encrypt(organizerPassword);

                                idWebMeetingOrganizer = webMeetingOrganizer.Save();
                            }
                        }
                    }
                }
                catch (AsentiaException ex) // FAILURE TO CREATE AN INTEGRATED SESSION WITH THE PROVIDER'S API IS A FATAL ERROR; REPORT AND RETURN
                {
                    // display the failure message
                    this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, ex.Message, true);
                    return;
                }

                // SAVE THE OBJECT TO DATABASE

                // if this is an existing standup training instance, and not the default language, save language properties only
                // otherwise, save the whole object in its default language
                int id;

                // populate the object
                this._SessionObject.IdStandupTraining = this._StandupTrainingObject.Id;
                this._SessionObject.Title = this._SessionTitle.Text;

                if (!String.IsNullOrWhiteSpace(this._Description.Text))
                { this._SessionObject.Description = HttpUtility.HtmlDecode(this._Description.Text); }
                else
                { this._SessionObject.Description = null; }

                // web meeting organizer id
                this._SessionObject.IdWebMeetingOrganizer = idWebMeetingOrganizer;

                // meeting password
                if (!String.IsNullOrWhiteSpace(this._MeetingPassword.Text))
                { this._SessionObject.MeetingPassword = this._MeetingPassword.Text; }
                else
                { this._SessionObject.MeetingPassword = null; }

                this._SessionObject.IsClosed = Convert.ToBoolean(this._IsClosed.SelectedValue);

                

                // seats
                if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToMeeting)
                { this._SessionObject.Seats = this._GetMaxAttendeesForWebMeeting("GTM"); }
                else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToWebinar)
                { this._SessionObject.Seats = this._GetMaxAttendeesForWebMeeting("GTW"); }
                else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.GoToTraining)
                { this._SessionObject.Seats = this._GetMaxAttendeesForWebMeeting("GTT"); }
                else if ((StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue) == StandupTrainingInstance.MeetingType.WebEx)
                { this._SessionObject.Seats = this._GetMaxAttendeesForWebMeeting("WebEx"); }
                else
                { this._SessionObject.Seats = Convert.ToInt32(this._Seats.Text); }
                
                this._SessionObject.WaitingSeats = Convert.ToInt32(this._WaitingSeats.Text);                

                this._SessionObject.Type = (StandupTrainingInstance.MeetingType)Convert.ToInt32(this._MeetingType.SelectedValue);

                if (!String.IsNullOrWhiteSpace(this._UrlRegistration.Text))
                { this._SessionObject.UrlRegistration = this._UrlRegistration.Text; }
                else
                { this._SessionObject.UrlRegistration = null; }

                if (!String.IsNullOrWhiteSpace(this._UrlAttend.Text))
                { this._SessionObject.UrlAttend = this._UrlAttend.Text; }
                else
                { this._SessionObject.UrlAttend = null; }

                if (!String.IsNullOrWhiteSpace(this._City.Text))
                { this._SessionObject.City = this._City.Text; }
                else
                { this._SessionObject.City = null; }

                if (!String.IsNullOrWhiteSpace(this._Province.Text))
                { this._SessionObject.Province = this._Province.Text; }
                else
                { this._SessionObject.Province = null; }

                if(!String.IsNullOrWhiteSpace(this._PostalCode.Text))
                { this._SessionObject.PostalCode = this._PostalCode.Text; }
                else
                { this._SessionObject.PostalCode = null; }

                if (!String.IsNullOrWhiteSpace(this._LocationDescription.Text))
                { this._SessionObject.LocationDescription = this._LocationDescription.Text; }
                else
                { this._SessionObject.LocationDescription = null; }


                // web meeting integration properties, this only gets set if the session is new; does not change for existing sessions
                if (isNewSession)
                {
                    this._SessionObject.IntegratedObjectKey = webMeetingId;
                    this._SessionObject.HostUrl = hostUrl;
                    this._SessionObject.GenericJoinUrl = genericJoinUrl;
                }

                // save the standup training instance, save its returned id to viewstate, and 
                // instansiate a new user object with the id
                id = this._SessionObject.Save();
                this.ViewState["id"] = id;
                this._SessionObject.Id = id;

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string sessionTitle = null;
                        string sessionDescription = null;
                        string sessionLocationDescription = null;

                        // get text boxes
                        TextBox languageSpecificSessionTitleTextBox = (TextBox)this.SessionPropertiesContainer.FindControl(this._SessionTitle.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificSessionDescriptionTextBox = (TextBox)this.SessionPropertiesContainer.FindControl(this._Description.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificSessionLocationDescriptionTextBox = (TextBox)this.SessionPropertiesContainer.FindControl(this._LocationDescription.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificSessionTitleTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificSessionTitleTextBox.Text))
                            { sessionTitle = languageSpecificSessionTitleTextBox.Text; }
                        }

                        if (languageSpecificSessionDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificSessionDescriptionTextBox.Text))
                            { sessionDescription = languageSpecificSessionDescriptionTextBox.Text; }
                        }

                        if (languageSpecificSessionLocationDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificSessionLocationDescriptionTextBox.Text))
                            { sessionLocationDescription = languageSpecificSessionLocationDescriptionTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(sessionTitle) ||
                            !String.IsNullOrWhiteSpace(sessionDescription) ||
                            !String.IsNullOrWhiteSpace(sessionLocationDescription))
                        {
                            this._SessionObject.SaveLang(cultureInfo.Name,
                                                         sessionTitle,
                                                         HttpUtility.HtmlDecode(sessionDescription),
                                                         HttpUtility.HtmlDecode(sessionLocationDescription));
                        }
                    }
                }

                // save the meeting times
                this._SessionObject.SaveMeetingTimes(this._MeetingTimesToSave);

                // do instructors
                // delcare data table
                DataTable instructorsToSave = new DataTable(); ;
                instructorsToSave.Columns.Add("id", typeof(int));

                if (!String.IsNullOrWhiteSpace(this._SelectedInstructors.Value))
                {
                    // split the "value" of the hidden field to get an array of resource ids
                    string[] selectedInstructorsIds = this._SelectedInstructors.Value.Split(',');

                    // put ids into datatable 
                    foreach (string instructorsId in selectedInstructorsIds)
                    { instructorsToSave.Rows.Add(Convert.ToInt32(instructorsId)); }
                }

                // save the instructors  even when there will not be selected any instructor to delete the existing one(s).
                this._SessionObject.SaveInstructors(instructorsToSave);

                // do resources
                // delcare data table
                DataTable resourcesToSave = new DataTable(); ;
                resourcesToSave.Columns.Add("id", typeof(int));

                if (!String.IsNullOrWhiteSpace(this._SelectedResources.Value))
                {
                    // split the "value" of the hidden field to get an array of resource ids
                    string[] selectedResourceIds = this._SelectedResources.Value.Split(',');

                    // put ids into datatable 
                    foreach (string resourceId in selectedResourceIds)
                    { resourcesToSave.Rows.Add(Convert.ToInt32(resourceId)); }
                }

                // save the resources  even when there will not be selected any resource to delete the existing one(s).
                this._SessionObject.SaveResources(resourcesToSave);
                
                // load the saved object
                this._SessionObject = new Library.StandupTrainingInstance(id);

                // Session Duplication is complete, so set it to false
                this._IsDuplicatingSession = false;

                // clear controls for containers that have dynamically added elements
                this.StandupTrainingObjectMenuContainer.Controls.Clear();
                
                // build the page controls
                this._BuildControls();
                
                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, _GlobalResources.SessionHasBeenSavedSuccessfully, false);                
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SessionPropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }            
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/standuptraining/sessions/Default.aspx?stid=" + this._StandupTrainingObject.Id.ToString());
        }
        #endregion

        #region _ValidateAndGetMeetingsDetailsForSave
        /// <summary>
        /// Validates all the meetings time intervals.
        /// </summary>
        private bool _ValidateAndGetMeetingsDetailsForSave()
        {
            bool isValidDate = true; ;
            bool isValid = true;
            List<Tuple<DateTime, DateTime>> dateList = new List<Tuple<DateTime, DateTime>>();
            string jsonStringifiedData = this._MeetingsJsonData.Value;

            //datatable will be used for validating dates
            this._MeetingTimesToValidate = new DataTable();
            this._MeetingTimesToValidate.Columns.Add("dtStart", typeof(DateTime));
            this._MeetingTimesToValidate.Columns.Add("dtEnd", typeof(DateTime));
            this._MeetingTimesToValidate.Columns.Add("idTimezone", typeof(int));
            this._MeetingTimesToValidate.Columns.Add("isExisting", typeof(bool));

            //datatable will be used for saving dates
            this._MeetingTimesToSave = new DataTable();
            this._MeetingTimesToSave.Columns.Add("dtStart", typeof(DateTime));
            this._MeetingTimesToSave.Columns.Add("dtEnd", typeof(DateTime));
            this._MeetingTimesToSave.Columns.Add("idTimezone", typeof(int));

            List<MeetingObject> meetingData = new JavaScriptSerializer().Deserialize<List<MeetingObject>>(jsonStringifiedData);

            int idTimezone = Convert.ToInt32(this._Timezone.SelectedValue);
            if (meetingData != null && meetingData.Count > 0)
            {
                foreach (MeetingObject data in meetingData)
                {

                    //finding start date and end date
                    this._DtStart = _ConvertToDateTime(data.DateStart, data.HourStart, data.MinuteStart, data.AmPmStart);
                    this._DtEnd = _ConvertToDateTime(data.DateEnd, data.HourEnd, data.MinuteEnd, data.AmPmEnd);
                    _IsExistingDate = !String.IsNullOrWhiteSpace(data.ExistingDate) && Convert.ToInt32(data.ExistingDate) == 1 ? true : false;

                    //checking for valid dates
                    if (this._DtStart != null && this._DtEnd != null)
                    {
                        // put data into datatable 
                        this._MeetingTimesToValidate.Rows.Add(this._DtStart, this._DtEnd, idTimezone, this._IsExistingDate);

                        DateTime dtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtStart, TimeZoneInfo.FindSystemTimeZoneById(this._TzDotNetName));
                        DateTime dtEnd = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtEnd, TimeZoneInfo.FindSystemTimeZoneById(this._TzDotNetName));

                        this._MeetingTimesToSave.Rows.Add(dtStart, dtEnd, idTimezone);

                        //used to rebind the date control OR mantaining the current date control state
                        this._SessionObject.FillSessionMeetingList(this._MeetingTimesToValidate);

                        // validate the form
                        if (!this._SessionDateValidationMessageShown && !this._ValidateMeetingsDates())
                        {
                            //throw new AsentiaException();
                            isValidDate = false;

                            this._SessionDateValidationMessageShown = true;
                        }
                        else
                        {
                            //validates date time overlapping
                            Tuple<DateTime, DateTime> tuple = new Tuple<DateTime, DateTime>(dtStart, dtEnd);
                            dateList.Add(tuple);
                        }
                    }
                    else
                    {

                        DateTime tempDatedate = new DateTime();
                        this._MeetingTimesToValidate.Rows.Add(tempDatedate, tempDatedate, idTimezone, this._IsExistingDate);

                        //used to rebind the date control OR mantaining the current date control state when dates are empty
                        this._SessionObject.FillSessionMeetingList(this._MeetingTimesToValidate);

                        if (!this._SessionDateValidationMessageShown)
                        {
                            this.ApplyErrorMessageToFieldErrorPanel(this.SessionPropertiesTabPanelsContainer, "Dates", _GlobalResources.StartAndEndDatesAreRequired);
                        }

                        isValid = false;
                        this._SessionDateValidationMessageShown = true;
                    }
                }
                if (!isValid || !isValidDate || !this._ValidateDatesOverlaping(dateList))
                {
                    isValid = false;
                }
            }
            return isValid;
        }
        #endregion

        #region _GetSelectedDateRanges
        /// <summary>
        /// Gets the listing of selected date ranges
        /// </summary>
        private void _GetSelectedDateRanges()
        {
            string jsonStringifiedData = this._MeetingsJsonData.Value;

            this._MeetingTimesForModalResourceSearching = new DataTable();
            this._MeetingTimesForModalResourceSearching.Columns.Add("dtStart", typeof(DateTime));
            this._MeetingTimesForModalResourceSearching.Columns.Add("dtEnd", typeof(DateTime));
            this._MeetingTimesForModalResourceSearching.Columns.Add("idTimezone", typeof(int));
            List<MeetingObject> meetingData = new JavaScriptSerializer().Deserialize<List<MeetingObject>>(jsonStringifiedData);

            int idTimezone = Convert.ToInt32(this._Timezone.SelectedValue);
            if (meetingData != null && meetingData.Count > 0)
            {
                foreach (MeetingObject data in meetingData)
                {
                    //finding start date and end date
                    this._DtStart = _ConvertToDateTime(data.DateStart, data.HourStart, data.MinuteStart, data.AmPmStart);
                    this._DtEnd = _ConvertToDateTime(data.DateEnd, data.HourEnd, data.MinuteEnd, data.AmPmEnd);

                    if (this._DtStart != null && this._DtEnd != null)
                    {
                        // put data into datatable 
                        this._MeetingTimesForModalResourceSearching.Rows.Add(this._DtStart, this._DtEnd, idTimezone);
                    }
                }
            }
        }
        #endregion

        #region  ConvertToDateTime
        /// <summary>
        /// this will convert string date parts (returns in JSON) to a date time  just like date picker control
        /// </summary>
        /// <param name="meetingDate"></param>
        /// <param name="meetingHour"></param>
        /// <param name="meetingMinute"></param>
        /// <param name="ampm"></param>
        /// <returns></returns>
        private static DateTime? _ConvertToDateTime(string meetingDate, string meetingHour, string meetingMinute, string ampm)
        {
            if (String.IsNullOrWhiteSpace(meetingDate))
            {
                return null;
            }
            else
            {
                int hour = Convert.ToInt32(meetingHour);
                int minute = Convert.ToInt32(meetingMinute);

                if (ampm == "PM" && hour < 12)
                { hour += 12; }
                else if (ampm == "AM" && hour == 12)
                { hour = 0; }
                else
                { }

                string hourString = hour.ToString();
                string minuteString = minute.ToString();

                if (hourString.Length == 1)
                { hourString = "0" + hourString; }

                if (minuteString.Length == 1)
                { minuteString = "0" + minuteString; }

                return DateTime.ParseExact(meetingDate + " " + hourString + ":" + minuteString, DateFormatDotNet + " HH:mm", CultureInfo.InvariantCulture);
            }
        }
        #endregion

        #region _SelectResourcesForSessionButton_Command
        /// <summary>
        /// Shows select resources for session display feedback message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _SelectResourcesForSessionButton_Command(object sender, CommandEventArgs e)
        {
            this._SelectResourcesForSessionModal.DisplayFeedback(_GlobalResources.SelectedResourceIsNotAvailableOnSelectedTimeInterval_s, true);
        }
        #endregion

        #region _SelectInstructorForSessionButton_Command
        /// <summary>
        /// Shows select instructor for session display feedback message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _SelectInstructorForSessionButton_Command(object sender, CommandEventArgs e)
        {
            this._SelectInstructorsForSessionModal.DisplayFeedback(_GlobalResources.TheSelectedInstructorIsNotAvailableForOneOrMoreOfTheSessionsMeetingTime_s, true);
        }
        #endregion

        #region _GetHTMLForCreatingDateControlRow
        /// <summary>
        /// Gets Html of date control for creating a new date row using java script
        /// Chetu team has written this code and told Joe regarding this,he said for now its Fine
        /// </summary>
        private string _GetHTMLForCreatingDateControlRow()
        {
            string calenderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR,
                                                             ImageFiles.EXT_PNG);
            string deleteIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                           ImageFiles.EXT_PNG);

            string dateControlHTML = "<tr id=\"Date_1\" class=\"DateRow\">"
                                            + "<td><div id=\"SessionDtStart1_Container\" class=\"FormFieldContainer\">"
                                            + "       <div id=\"SessionDtStart1_LabelContainer\" class=\"FormFieldLabelContainer\">                             "
                                            + "              <label for=\"SessionDtStart_1\">Start Date: </label> <span class=\"RequiredAsterisk\">* </span>    "
                                            + "       </div><div id=\"SessionDtStart1_ErrorContainer\" class=\"FormFieldErrorContainer\">                       "
                                            + "                                                                                                            "
                                            + "       </div><div id=\"SessionDtStart1_InputContainer\" class=\"FormFieldInputContainer\">                       "
                                            + "              <div id=\"SessionDtStart_1\" class=\"StartDate\">                                                     "
                                            + "                    <input name=\"Master$PageContentPlaceholder$SessionDtStart_1_DateInputControl\" type=\"text\" value=\"2015-06-11\" id=\"SessionDtStart_1_DateInputControl\" class=\"DatePickerInput\" onChange=\"SessionDtStartChange(this);\"><img id=\"SessionDtStart_1_DatePickerImage\" class=\"DatePickerImage SmallIcon\" src=" + calenderIconPath + "><select name=\"Master$PageContentPlaceholder$SessionDtStart_1_HourInputControl\" id=\"SessionDtStart_1_HourInputControl\" class=\"DatePickerTimeHour\">"
                                            + "                           <option selected=\"selected\" value=\"12\">12</option>  "
                                            + "                           <option value=\"1\">1</option>                          "
                                            + "                           <option value=\"2\">2</option>                          "
                                            + "                           <option value=\"3\">3</option>                          "
                                            + "                           <option value=\"4\">4</option>                          "
                                            + "                           <option value=\"5\">5</option>                          "
                                            + "                           <option value=\"6\">6</option>                          "
                                            + "                           <option value=\"7\">7</option>                          "
                                            + "                           <option value=\"8\">8</option>                          "
                                            + "                           <option value=\"9\">9</option>                          "
                                            + "                           <option value=\"10\">10</option>                        "
                                            + "                           <option value=\"11\">11</option>                        "
                                            + "                                                                      "
                                            + "                    </select>:<select name=\"Master$PageContentPlaceholder$SessionDtStart_1_MinuteInputControl\" id=\"SessionDtStart_1_MinuteInputControl\" class=\"DatePickerTimeMinute\">"
                                            + "                    <option selected=\"selected\" value=\"00\">00</option>"
                                            + "                    <option value=\"01\">01</option>                "
                                            + "                    <option value=\"02\">02</option>                "
                                            + "                    <option value=\"03\">03</option>                "
                                            + "                    <option value=\"04\">04</option>                "
                                            + "                    <option value=\"05\">05</option>                "
                                            + "                    <option value=\"06\">06</option>                "
                                            + "                    <option value=\"07\">07</option>                "
                                            + "                    <option value=\"08\">08</option>                "
                                            + "                    <option value=\"09\">09</option>                "
                                            + "                    <option value=\"10\">10</option>                "
                                            + "                    <option value=\"11\">11</option>                "
                                            + "                    <option value=\"12\">12</option>                "
                                            + "                    <option value=\"13\">13</option>                "
                                            + "                    <option value=\"14\">14</option>                "
                                            + "                    <option value=\"15\">15</option>                "
                                            + "                    <option value=\"16\">16</option>                "
                                            + "                    <option value=\"17\">17</option>                "
                                            + "                    <option value=\"18\">18</option>                "
                                            + "                    <option value=\"19\">19</option>                "
                                            + "                    <option value=\"20\">20</option>                "
                                            + "                    <option value=\"21\">21</option>                "
                                            + "                    <option value=\"22\">22</option>                "
                                            + "                    <option value=\"23\">23</option>                "
                                            + "                    <option value=\"24\">24</option>                "
                                            + "                    <option value=\"25\">25</option>                "
                                            + "                    <option value=\"26\">26</option>                "
                                            + "                    <option value=\"27\">27</option>                "
                                            + "                    <option value=\"28\">28</option>                "
                                            + "                    <option value=\"29\">29</option>                "
                                            + "                    <option value=\"30\">30</option>                "
                                            + "                    <option value=\"31\">31</option>                "
                                            + "                    <option value=\"32\">32</option>                "
                                            + "                    <option value=\"33\">33</option>                "
                                            + "                    <option value=\"34\">34</option>                "
                                            + "                    <option value=\"35\">35</option>                "
                                            + "                    <option value=\"36\">36</option>                "
                                            + "                    <option value=\"37\">37</option>                "
                                            + "                    <option value=\"38\">38</option>                "
                                            + "                    <option value=\"39\">39</option>                "
                                            + "                    <option value=\"40\">40</option>                "
                                            + "                    <option value=\"41\">41</option>                "
                                            + "                    <option value=\"42\">42</option>                "
                                            + "                    <option value=\"43\">43</option>                "
                                            + "                    <option value=\"44\">44</option>                "
                                            + "                    <option value=\"45\">45</option>                "
                                            + "                    <option value=\"46\">46</option>                "
                                            + "                    <option value=\"47\">47</option>                "
                                            + "                    <option value=\"48\">48</option>                "
                                            + "                    <option value=\"49\">49</option>                "
                                            + "                    <option value=\"50\">50</option>                "
                                            + "                    <option value=\"51\">51</option>                "
                                            + "                    <option value=\"52\">52</option>                "
                                            + "                    <option value=\"53\">53</option>                "
                                            + "                    <option value=\"54\">54</option>                "
                                            + "                    <option value=\"55\">55</option>                "
                                            + "                    <option value=\"56\">56</option>                "
                                            + "                    <option value=\"57\">57</option>                "
                                            + "                    <option value=\"58\">58</option>                "
                                            + "                    <option value=\"59\">59</option>                "
                                            + "                                                                    <span class=\"RequiredAsterisk\">* </span>"
                                            + "                    </select><select name=\"Master$PageContentPlaceholder$SessionDtStart_1_AMPMInputControl\" id=\"SessionDtStart_1_AMPMInputControl\" class=\"DatePickerTimeAMPM\">"
                                            + "                           <option selected=\"selected\" value=\"AM\">AM</option>"
                                            + "                           <option value=\"PM\">PM</option>"
                                            + "                                                                                                     "
                                            + "                    </select>                                                                        "
                                            + "              </div>                                                                                 "
                                            + "       </div>                                                                                        "
                                            + "</div></td><td><div id=\"SessionDtEnd1_Container\" class=\"FormFieldContainer\">                     "
                                            + "       <div id=\"SessionDtEnd1_LabelContainer\" class=\"FormFieldLabelContainer\">                   "
                                            + "              <label for=\"SessionDtEnd_1\">End Date: </label> <span class=\"RequiredAsterisk\">* </span>"
                                            + "       </div><div id=\"SessionDtEnd1_ErrorContainer\" class=\"FormFieldErrorContainer\">                 "
                                            + "                                                                                                         "
                                            + "       </div><div id=\"SessionDtEnd1_InputContainer\" class=\"FormFieldInputContainer\">                 "
                                            + "              <div id=\"SessionDtEnd_1\" class=\"EndDate\">                                              "
                                            + "                    <input name=\"Master$PageContentPlaceholder$SessionDtEnd_1_DateInputControl\" type=\"text\" value=\"2015-06-13\" id=\"SessionDtEnd_1_DateInputControl\" class=\"DatePickerInput\"><img id=\"SessionDtEnd_1_DatePickerImage\" class=\"DatePickerImage SmallIcon\" src=" + calenderIconPath + "><select name=\"Master$PageContentPlaceholder$SessionDtEnd_1_HourInputControl\" id=\"SessionDtEnd_1_HourInputControl\" class=\"DatePickerTimeHour\">"
                                            + "                           <option selected=\"selected\" value=\"12\">12</option> "
                                            + "                           <option value=\"1\">1</option>                         "
                                            + "                           <option value=\"2\">2</option>                         "
                                            + "                           <option value=\"3\">3</option>                         "
                                            + "                           <option value=\"4\">4</option>                         "
                                            + "                           <option value=\"5\">5</option>                         "
                                            + "                           <option value=\"6\">6</option>                         "
                                            + "                           <option value=\"7\">7</option>                         "
                                            + "                           <option value=\"8\">8</option>                         "
                                            + "                           <option value=\"9\">9</option>                         "
                                            + "                           <option value=\"10\">10</option>                       "
                                            + "                           <option value=\"11\">11</option>                       "
                                            + "                                                                                  "
                                            + "                    </select>:<select name=\"Master$PageContentPlaceholder$SessionDtEnd_1_MinuteInputControl\" id=\"SessionDtEnd_1_MinuteInputControl\" class=\"DatePickerTimeMinute\">"
                                            + "                           <option selected=\"selected\" value=\"00\">00</option>"
                                            + "                           <option value=\"01\">01</option>                        "
                                            + "                           <option value=\"02\">02</option>                        "
                                            + "                           <option value=\"03\">03</option>                        "
                                            + "                           <option value=\"04\">04</option>                        "
                                            + "                           <option value=\"05\">05</option>                        "
                                            + "                           <option value=\"06\">06</option>                        "
                                            + "                           <option value=\"07\">07</option>                        "
                                            + "                           <option value=\"08\">08</option>                        "
                                            + "                           <option value=\"09\">09</option>                        "
                                            + "                           <option value=\"10\">10</option>                        "
                                            + "                           <option value=\"11\">11</option>                        "
                                            + "                           <option value=\"12\">12</option>                        "
                                            + "                           <option value=\"13\">13</option>                        "
                                            + "                           <option value=\"14\">14</option>                        "
                                            + "                           <option value=\"15\">15</option>                        "
                                            + "                           <option value=\"16\">16</option>                        "
                                            + "                           <option value=\"17\">17</option>                        "
                                            + "                           <option value=\"18\">18</option>                        "
                                            + "                           <option value=\"19\">19</option>                        "
                                            + "                           <option value=\"20\">20</option>                        "
                                            + "                           <option value=\"21\">21</option>                        "
                                            + "                           <option value=\"22\">22</option>                        "
                                            + "                           <option value=\"23\">23</option>                        "
                                            + "                           <option value=\"24\">24</option>                        "
                                            + "                           <option value=\"25\">25</option>                        "
                                            + "                           <option value=\"26\">26</option>                        "
                                            + "                           <option value=\"27\">27</option>                        "
                                            + "                           <option value=\"28\">28</option>                        "
                                            + "                           <option value=\"29\">29</option>                        "
                                            + "                           <option value=\"30\">30</option>                        "
                                            + "                           <option value=\"31\">31</option>                        "
                                            + "                           <option value=\"32\">32</option>                        "
                                            + "                           <option value=\"33\">33</option>                        "
                                            + "                           <option value=\"34\">34</option>                        "
                                            + "                           <option value=\"35\">35</option>                        "
                                            + "                           <option value=\"36\">36</option>                        "
                                            + "                           <option value=\"37\">37</option>                        "
                                            + "                           <option value=\"38\">38</option>                        "
                                            + "                           <option value=\"39\">39</option>                        "
                                            + "                           <option value=\"40\">40</option>                        "
                                            + "                           <option value=\"41\">41</option>                        "
                                            + "                           <option value=\"42\">42</option>                        "
                                            + "                           <option value=\"43\">43</option>                        "
                                            + "                           <option value=\"44\">44</option>                        "
                                            + "                           <option value=\"45\">45</option>                        "
                                            + "                           <option value=\"46\">46</option>                        "
                                            + "                           <option value=\"47\">47</option>                        "
                                            + "                           <option value=\"48\">48</option>                        "
                                            + "                           <option value=\"49\">49</option>                        "
                                            + "                           <option value=\"50\">50</option>                        "
                                            + "                           <option value=\"51\">51</option>                        "
                                            + "                           <option value=\"52\">52</option>                        "
                                            + "                           <option value=\"53\">53</option>                        "
                                            + "                           <option value=\"54\">54</option>                        "
                                            + "                           <option value=\"55\">55</option>                        "
                                            + "                           <option value=\"56\">56</option>                        "
                                            + "                           <option value=\"57\">57</option>                        "
                                            + "                           <option value=\"58\">58</option>                        "
                                            + "                           <option value=\"59\">59</option>                        "
                                            + ""
                                            + "                    </select><select name=\"Master$PageContentPlaceholder$SessionDtEnd_1_AMPMInputControl\" id=\"SessionDtEnd_1_AMPMInputControl\" class=\"DatePickerTimeAMPM\">"
                                            + "                           <option selected=\"selected\" value=\"AM\">AM</option>"
                                            + "                           <option value=\"PM\">PM</option>"
                                            + ""
                                            + "                    </select>                "
                                            + "              </div>                         "
                                            + "       </div>                                "
                                            + "</div><span id=\"IsExistingHidden_1\" class=\"IsExistingHidden\"  ></span></td><td><a onclick=\"RemoveDateRow(this);return false;\" id=\"RemoveDateButton_1\" class=\"RemoveDateButton\" href=\"javascript:__doPostBack(\"Master$PageContentPlaceholder$RemoveDateButton_1\",\"\")\" style=\"display: inline-block;\"><img class=\"SmallIcon\" src=" + deleteIconPath + "></a></td>"
                                        + "</tr>";
            return dateControlHTML;

        }
        #endregion

        #region Multiple dates control
        #region _BuildMultipleDateControl
        /// <summary>
        /// Builds multiple date control
        /// </summary>
        private void _BuildMultipleDateControl()
        {
            this._MeetingDateControlPlaceHolder.Controls.Clear();
            this._MeetingDateControlPlaceHolder.Controls.Add(this._DatesTable);

            if (this._SessionObject != null)
            {
                if (this._SessionObject.MeetingTimes.Count > 0)
                {
                    this._LoadDates(this._SessionObject.MeetingTimes);
                }
                else
                {
                    this._AddDate(1);
                }
            }
            else
            {
                this._AddDate(1);
            }
        }
        #endregion

        #region Add Date
        /// <summary>
        /// Adds date control row .
        /// </summary>
        private void _AddDate(int id)
        {
            //Create row and cells
            TableRow rowDate = new TableRow();
            rowDate.ID = "Date_" + id;
            rowDate.CssClass = "DateRow";
            TableCell cellStartDateField = new TableCell();
            TableCell cellEndDateField = new TableCell();
            TableCell cellRemoveButton = new TableCell();

            //Create and add user invisible field for start date
            DatePicker dtStart = new DatePicker("SessionDtStart_" + id, false, false, true);
            cellStartDateField.Controls.Add(AsentiaPage.BuildFormField("SessionDtStart" + id,
                                                 _GlobalResources.StartDate,
                                                 dtStart.ID,
                                                 dtStart,
                                                 true,
                                                 true,
                                                 false));
            dtStart.CssClass = "StartDate";

            //Create and add user invisible field for end date
            DatePicker dtEnd = new DatePicker("SessionDtEnd_" + id, false, false, true);
            cellEndDateField.Controls.Add(AsentiaPage.BuildFormField("SessionDtEnd" + id,
                                                 _GlobalResources.EndDate,
                                                 dtEnd.ID,
                                                 dtEnd,
                                                 true,
                                                 true,
                                                 false));
            dtEnd.CssClass = "EndDate";

            //used while applying the dates validation on existing values
            Label isExistingHidden = new Label();
            isExistingHidden.ID = "IsExistingHidden_" + id;
            cellEndDateField.Controls.Add(isExistingHidden);
            isExistingHidden.CssClass = "IsExistingHidden";
            //isExistingHidden.Attributes.Add("display", "none");

            //Create and add remove button
            LinkButton removeDateButton = new LinkButton();
            removeDateButton.ID = "RemoveDateButton_" + id;
            removeDateButton.Attributes.Add("ID", id.ToString());
            removeDateButton.CssClass = "RemoveDateButton";

            Image removeDateImageForLink = new Image();
            removeDateImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            removeDateImageForLink.CssClass = "SmallIcon";
            removeDateButton.Controls.Add(removeDateImageForLink);
            cellRemoveButton.Controls.Add(removeDateButton);


            //Add cells to row
            rowDate.Cells.Add(cellStartDateField);
            rowDate.Cells.Add(cellEndDateField);
            rowDate.Cells.Add(cellRemoveButton);

            //Add row to the table
            this._DatesTable.Rows.Add(rowDate);
        }
        #endregion

        #region Load provided Dates of for a session
        /// <summary>
        /// Loads provided Dates of for a session
        /// </summary>
        private void _LoadDates(ArrayList MeetingTimes)
        {
            this._DatesTable.Rows.Clear();
            if (MeetingTimes.Count > 0)
            {
                for (int i = 0; i < MeetingTimes.Count; i++)
                {
                    int id = i + 1;
                    //adding a new date control (date row)
                    this._AddDate(id);

                    //finding and populating the controls with existing dates
                    DatePicker dtStart = (DatePicker)this.SessionPropertiesTabPanelsContainer.FindControl("SessionDtStart_" + id);
                    DatePicker dtEnd = (DatePicker)this.SessionPropertiesTabPanelsContainer.FindControl("SessionDtEnd_" + id);
                    Label isExisting = (Label)this.SessionPropertiesTabPanelsContainer.FindControl("IsExistingHidden_" + id);
                    TimeZoneSelector timeZone = (TimeZoneSelector)this.SessionPropertiesTabPanelsContainer.FindControl("RuleSetEnrollmentTimezone_Field");


                    if (dtStart != null && dtEnd != null)
                    {
                        StandupTrainingInstance.MeetingTimeProperty meetingTimeProperty = (StandupTrainingInstance.MeetingTimeProperty)MeetingTimes[i];
                        DateTime tempDate = new DateTime();
                        DateTime startDate = DateTime.UtcNow;
                        DateTime endDate;
                        string tzDotNetName = new Timezone(meetingTimeProperty.IdTimezone).dotNetName;
                        if (meetingTimeProperty.DtStart != tempDate)
                        {
                            if (this._IsHiddenFieldData)
                            {
                                dtStart.Value = meetingTimeProperty.DtStart;
                            }
                            else
                            {
                                startDate = TimeZoneInfo.ConvertTimeFromUtc(meetingTimeProperty.DtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                                dtStart.Value = startDate;
                            }
                        }

                        if (meetingTimeProperty.DtEnd != tempDate)
                        {
                            if (this._IsHiddenFieldData)
                            {
                                dtEnd.Value = meetingTimeProperty.DtEnd;

                            }
                            else
                            {
                                endDate = TimeZoneInfo.ConvertTimeFromUtc(meetingTimeProperty.DtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                                dtEnd.Value = endDate;
                            }
                        }
                        if (meetingTimeProperty.IdTimezone > 0)
                        {
                            timeZone.SelectedValue = Convert.ToString(meetingTimeProperty.IdTimezone);
                        }
                        if (meetingTimeProperty.IsExistingDate)
                        {
                            DateTime currentTimeInUtc = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));

                            if (dtStart.Value < currentTimeInUtc && !this._IsDuplicatingSession)
                            {
                                dtStart.Enabled = false;
                                dtEnd.Enabled = false;

                                //for not applying any dates validation if date any existing date is passed away
                                isExisting.Text = "1";
                            }
                            else
                            {
                                //for not saving values of past for existing dates
                                isExisting.Text = "0";
                            }
                        }
                        else
                        {
                            //for not saving values of past for new dates (not existing in database)
                            isExisting.Text = "0";
                        }
                    }
                }
            }
            else
            {
                //adding a new date control (date row)
                this._AddDate(1);
            }
        }
        #endregion
        #endregion

        #region WebMethods
        #region CheckForResourceAvailability
        /// <summary>
        /// Checks For Resource Availability
        /// </summary>
        /// <param name="idObject"></param>
        /// <param name="idResource"></param>
        /// <param name="idTimeZone"></param>
        /// <param name="selectedDatesForResources"></param>
        /// <returns></returns>
        [WebMethod]
        public static JsonData CheckForResourceAvailability(int idObject, int idResource, int idTimeZone, object[] selectedDatesForResources)
        {
            bool isAvailable = true;
            JsonData jsonData = new JsonData();
            try
            {
                if (selectedDatesForResources.Length > 0 && idResource > 0)
                {
                    var json = JsonConvert.SerializeObject(selectedDatesForResources);
                    List<MeetingObject> meetingData = new JavaScriptSerializer().Deserialize<List<MeetingObject>>(json);
                    DataTable meetingTimesToSave = new DataTable();
                    meetingTimesToSave.Columns.Add("dtStart", typeof(DateTime));
                    meetingTimesToSave.Columns.Add("dtEnd", typeof(DateTime));
                    meetingTimesToSave.Columns.Add("idTimezone", typeof(int));
                    if (meetingData != null && meetingData.Count > 0)
                    {
                        foreach (MeetingObject data in meetingData)
                        {
                            //finding start date and end date
                            DateTime? dtStart = _ConvertToDateTime(data.DateStart, data.HourStart, data.MinuteStart, data.AmPmStart);
                            DateTime? dtEnd = _ConvertToDateTime(data.DateEnd, data.HourEnd, data.MinuteEnd, data.AmPmEnd);

                            if (dtStart != null && dtEnd != null)
                            {
                                // put data into datatable 
                                meetingTimesToSave.Rows.Add(dtStart, dtEnd, idTimeZone);
                            }
                        }
                    }

                    isAvailable = Resource.CheckForResourceAvailability(idObject, idResource, meetingTimesToSave);
                }

                jsonData.IsAvailable = isAvailable;
                jsonData.ExceptionValue = string.Empty;
                return jsonData;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                jsonData.ExceptionValue = dnfEx.Message;
                return jsonData;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                jsonData.ExceptionValue = cpeEx.Message;
                return jsonData;

            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                jsonData.ExceptionValue = dEx.Message;
                return jsonData;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                jsonData.ExceptionValue = ex.Message;
                return jsonData;
            }
            catch (Exception e)
            {
                jsonData.ExceptionValue = e.Message;
                return jsonData;
            }

        }
        #endregion

        #region CheckInstructorAvailability
        /// <summary>
        /// Web method called by JS to check an instructor's availability upon selection.
        /// </summary>
        /// <param name="idInstructor"></param>
        /// <param name="idObject"></param>
        /// <param name="idTimeZone"></param>
        /// <param name="selectedDates"></param>
        /// <returns></returns>
        [WebMethod]
        public static JsonData CheckInstructorAvailability(int idInstructor, int idStandupTrainingInstance, int idTimezone, object[] selectedDates)
        {
            bool isAvailable = true;
            JsonData jsonData = new JsonData();

            try
            {
                if (selectedDates.Length > 0 && idInstructor > 0)
                {
                    var json = JsonConvert.SerializeObject(selectedDates);
                    List<MeetingObject> meetingData = new JavaScriptSerializer().Deserialize<List<MeetingObject>>(json);
                    Timezone timeZone = new Timezone(idTimezone);

                    DataTable meetingTimes = new DataTable();
                    meetingTimes.Columns.Add("dtStart", typeof(DateTime));
                    meetingTimes.Columns.Add("dtEnd", typeof(DateTime));
                    meetingTimes.Columns.Add("idTimezone", typeof(int));

                    if (meetingData != null && meetingData.Count > 0)
                    {
                        foreach (MeetingObject data in meetingData)
                        {                            
                            DateTime? dtStart = _ConvertToDateTime(data.DateStart, data.HourStart, data.MinuteStart, data.AmPmStart);
                            DateTime? dtEnd = _ConvertToDateTime(data.DateEnd, data.HourEnd, data.MinuteEnd, data.AmPmEnd);

                            if (dtStart != null && dtEnd != null)
                            {
                                dtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)dtStart, TimeZoneInfo.FindSystemTimeZoneById(timeZone.dotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeToUtc((DateTime)dtEnd, TimeZoneInfo.FindSystemTimeZoneById(timeZone.dotNetName));

                                meetingTimes.Rows.Add(dtStart, dtEnd, idTimezone); 
                            }
                        }
                    }

                    isAvailable = StandupTrainingInstance.CheckInstructorAvailability(idStandupTrainingInstance, idInstructor, meetingTimes);
                }

                jsonData.IsAvailable = isAvailable;
                jsonData.ExceptionValue = string.Empty;
                return jsonData;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                jsonData.ExceptionValue = dnfEx.Message;
                return jsonData;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                jsonData.ExceptionValue = cpeEx.Message;
                return jsonData;

            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                jsonData.ExceptionValue = dEx.Message;
                return jsonData;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                jsonData.ExceptionValue = ex.Message;
                return jsonData;
            }
            catch (Exception e)
            {
                jsonData.ExceptionValue = e.Message;
                return jsonData;
            }

        }
        #endregion
        #endregion
    }

    #region JSON classes

    // class: MeetingObject
    // used for populating the date information
    public class MeetingObject
    {
        public string DateStart { get; set; }
        public string HourStart { get; set; }
        public string MinuteStart { get; set; }
        public string AmPmStart { get; set; }
        public string HourEnd { get; set; }
        public string MinuteEnd { get; set; }
        public string DateEnd { get; set; }
        public string AmPmEnd { get; set; }
        public string ExistingDate { get; set; }
    }

    //JsonData
    public struct JsonData
    {
        //resource available or not
        public bool IsAvailable;

        //Excepton value
        public string ExceptionValue;
    }
    #endregion
}