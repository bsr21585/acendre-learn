﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.StandupTraining
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel StandupTrainingPropertiesFormContentWrapperContainer;
        public Panel StandupTrainingObjectMenuContainer;
        public Panel StandupTrainingPropertiesWrapperContainer;        
        public Panel StandupTrainingPropertiesInstructionsPanel;
        public Panel StandupTrainingPropertiesFeedbackContainer;
        public Panel StandupTrainingPropertiesContainer;
        public Panel StandupTrainingPropertiesActionsPanel;        
        #endregion

        #region Private Properties
        private Library.StandupTraining _StandupTrainingObject;
        private EcommerceSettings _EcommerceSettings;

        private UploaderAsync _StandupTrainingAvatar;

        private TextBox _Title;
        private TextBox _Cost;
        private TextBox _Description;
        private TextBox _Objectives;
        private TextBox _SearchTags;

        private CheckBox _IsStandaloneEnroll;
        private CheckBox _IsRestrictedEnroll;
        private CheckBox _IsRestrictedDrop;

        private Button _SaveButton;
        private Button _CancelButton;

        private Image _AvatarImage;
        private HiddenField _ClearAvatar;

        private TextBox _Shortcode;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.StandupTraining.Modify.js");

            // build start up call for MCE and add to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);

            base.OnPreRender(e);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_InstructorLedTrainingManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/standuptraining/Modify.css");

            // get the standup training object
            this._GetStandupTrainingObject();

            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // initialize the administrator menu
            this.InitializeAdminMenu();            

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetStandupTrainingObject
        /// <summary>
        /// Gets a standup training object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetStandupTrainingObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._StandupTrainingObject = new Library.StandupTraining(id); }
                }
                catch
                { Response.Redirect("~/administrator/standuptraining"); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // set container classes
            this.StandupTrainingPropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";            
            this.StandupTrainingPropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the standup training object menu if we're editing a standup training
            if (this._StandupTrainingObject != null)
            {
                StandupTrainingObjectMenu standupTrainingObjectMenu = new StandupTrainingObjectMenu(this._StandupTrainingObject);
                standupTrainingObjectMenu.SelectedItem = StandupTrainingObjectMenu.MenuObjectItem.StandupTrainingProperties;

                this.StandupTrainingObjectMenuContainer.Controls.Add(standupTrainingObjectMenu);
            }

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.StandupTrainingPropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisInstructorLedTrainingModule, true);                        

            // clear controls from properties container
            this.StandupTrainingPropertiesContainer.Controls.Clear();

            // build the properties form
            this._BuildPropertiesForm();

            // build the properties form actions panel
            this._BuildPropertiesActionsPanel();        
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string standupTrainingImagePath;
            string imageCssClass = null;
            string pageTitle;

            if (this._StandupTrainingObject != null)
            {
                string standupTrainingTitleInInterfaceLanguage = this._StandupTrainingObject.Title;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (LMS.Library.StandupTraining.LanguageSpecificProperty standupTrainingLanguageSpecificProperty in this._StandupTrainingObject.LanguageSpecificProperties)
                    {
                        if (standupTrainingLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { standupTrainingTitleInInterfaceLanguage = standupTrainingLanguageSpecificProperty.Title; }
                    }
                }

                breadCrumbPageTitle = standupTrainingTitleInInterfaceLanguage;

                if (this._StandupTrainingObject.Avatar != null)
                {
                    standupTrainingImagePath = SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + this._StandupTrainingObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    imageCssClass = "AvatarImage";
                }
                else
                {
                    standupTrainingImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
                }

                pageTitle = standupTrainingTitleInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewInstructorLedTrainingModule;
                standupTrainingImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CLASSROOM, ImageFiles.EXT_PNG);
                pageTitle = _GlobalResources.NewInstructorLedTrainingModule;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.InstructorLedTrainingModules, "/administrator/standuptraining"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, pageTitle, standupTrainingImagePath, imageCssClass);
        }
        #endregion

        #region _BuildStandupTrainingPropertiesFormTabs
        /// <summary>
        /// 
        /// </summary>
        private void _BuildStandupTrainingPropertiesFormTabs()
        {
            // queue up the tabs
            //Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            //tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));            

            // build and attach the tabs
            //this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("StandupTrainingProperties", tabs));
        }
        #endregion

        #region _BuildPropertiesForm
        /// <summary>
        /// Builds the properties form.
        /// </summary>
        private void _BuildPropertiesForm()
        {
            // "Properties" is the default tab, so this is visible on page load.
            //Panel propertiesPanel = new Panel();
            //propertiesPanel.ID = "StandupTrainingProperties_" + "Properties" + "_TabPanel";
            //propertiesPanel.Attributes.Add("style", "display: block;");

            // title field
            this._Title = new TextBox();
            this._Title.ID = "Title_Field";
            this._Title.CssClass = "InputMedium";

            this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Title",
                                                             _GlobalResources.Name,
                                                             this._Title.ID,
                                                             this._Title,
                                                             true,
                                                             true,
                                                             true));

            // catalog link shortcode
            List<Control> shortcodeInputControls = new List<Control>();

            this._Shortcode = new TextBox();
            this._Shortcode.ID = "Shortcode_Field";
            this._Shortcode.CssClass = "InputShort";
            this._Shortcode.MaxLength = 10;
            shortcodeInputControls.Add(this._Shortcode);

            if (this._StandupTrainingObject != null)
            {
                if (!String.IsNullOrWhiteSpace(this._StandupTrainingObject.Shortcode) && (bool)this._StandupTrainingObject.IsStandaloneEnroll)
                {
                    // build the shortcode link
                    Panel shortcodeUrlLinkContainer = new Panel();
                    shortcodeUrlLinkContainer.ID = "ShortcodeUrlLinkContainer";

                    string shortcodeUrl = String.Empty;

                    shortcodeUrl = "https://" + AsentiaSessionState.GlobalSiteObject.Hostname + "." + Config.AccountSettings.BaseDomain + "/catalog/?type=ilt&sc=" + this._StandupTrainingObject.Shortcode;

                    HyperLink shortcodeLink = new HyperLink();
                    shortcodeLink.ID = "ShortcodeLink";
                    shortcodeLink.NavigateUrl = shortcodeUrl;
                    shortcodeLink.Text = shortcodeUrl;
                    shortcodeLink.Target = "_blank";

                    shortcodeUrlLinkContainer.Controls.Add(shortcodeLink);
                    shortcodeInputControls.Add(shortcodeUrlLinkContainer);

                    // attach an onchange event to the shortcode text box so the link gets updated as it is being changed
                    this._Shortcode.Attributes.Add("onkeyup", "UpdateShortcodeLink(this.id);");
                }
            }

            this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Shortcode",
                                                                                                         _GlobalResources.CatalogShortcode,
                                                                                                         shortcodeInputControls,
                                                                                                         false,
                                                                                                         true));

            // avatar field
            List<Control> avatarInputControls = new List<Control>();

            if (this._StandupTrainingObject != null)
            {
                if (this._StandupTrainingObject.Avatar != null)
                {
                    // standup training avatar image panel
                    Panel avatarImageContainer = new Panel();
                    avatarImageContainer.ID = "Avatar_Field_ImageContainer";
                    avatarImageContainer.CssClass = "AvatarImageContainer";

                    // avatar image
                    this._AvatarImage = new Image();
                    avatarImageContainer.Controls.Add(this._AvatarImage);
                    avatarInputControls.Add(avatarImageContainer);

                    Panel avatarImageDeleteButtonContainer = new Panel();
                    avatarImageDeleteButtonContainer.ID = "AvatarImageDeleteButtonContainer";
                    avatarImageDeleteButtonContainer.CssClass = "AvatarDeleteButtonContainer";
                    avatarImageContainer.Controls.Add(avatarImageDeleteButtonContainer);

                    // delete course avatar image
                    Image deleteAvatarImage = new Image();
                    deleteAvatarImage.ID = "AvatarImageDeleteButton";
                    deleteAvatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    deleteAvatarImage.CssClass = "SmallIcon";
                    deleteAvatarImage.Attributes.Add("onClick", "javascript:DeleteInstructorLedTrainingAvatar();");
                    deleteAvatarImage.Style.Add("cursor", "pointer");

                    avatarImageDeleteButtonContainer.Controls.Add(deleteAvatarImage);

                    // clear avatar hidden field
                    this._ClearAvatar = new HiddenField();
                    this._ClearAvatar.ID = "ClearAvatar_Field";
                    this._ClearAvatar.Value = "false";
                    avatarInputControls.Add(this._ClearAvatar);
                }
            }

            // avatar image upload
            this._StandupTrainingAvatar = new UploaderAsync("Avatar_Field", UploadType.Avatar, "Avatar_ErrorContainer");

            // set params to resize the avatar upon upload
            this._StandupTrainingAvatar.ResizeOnUpload = true;
            this._StandupTrainingAvatar.ResizeMaxWidth = 256;
            this._StandupTrainingAvatar.ResizeMaxHeight = 256;

            avatarInputControls.Add(this._StandupTrainingAvatar);

            this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Avatar",
                                                                                                         _GlobalResources.Avatar,
                                                                                                         avatarInputControls,
                                                                                                         false,
                                                                                                         true));

            // cost field
            if (this._EcommerceSettings.IsEcommerceSet)
            {
                List<Control> costInputControls = new List<Control>();

                Label costSymbolLabel = new Label();
                costSymbolLabel.Text = this._EcommerceSettings.CurrencySymbol + " ";
                costInputControls.Add(costSymbolLabel);

                this._Cost = new TextBox();
                this._Cost.ID = "Cost_Field";
                this._Cost.CssClass = "InputXShort";
                costInputControls.Add(this._Cost);

                Label costCodeLabel = new Label();
                costCodeLabel.Text = " (" + this._EcommerceSettings.CurrencyCode + ") ";
                costInputControls.Add(costCodeLabel);

                this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Cost",
                                                                                                             _GlobalResources.Cost,
                                                                                                             costInputControls,
                                                                                                             false,
                                                                                                             true));
            }
            
            // description field
            this._Description = new TextBox();
            this._Description.ID = "Description_Field";
            this._Description.CssClass = "ckeditor";
            this._Description.Style.Add("width", "98%");
            this._Description.TextMode = TextBoxMode.MultiLine;
            this._Description.Rows = 10;

            this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Description",
                                                             _GlobalResources.Description,
                                                             this._Description.ID,
                                                             this._Description,
                                                             true,
                                                             true,
                                                             true));

            // objectives field
            this._Objectives = new TextBox();
            this._Objectives.ID = "Objectives_Field";
            this._Objectives.CssClass = "ckeditor";
            this._Objectives.Style.Add("width", "98%");
            this._Objectives.TextMode = TextBoxMode.MultiLine;
            this._Objectives.Rows = 10;

            this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Objectives",
                                                             _GlobalResources.Objectives,
                                                             this._Objectives.ID,
                                                             this._Objectives,
                                                             false,
                                                             true,
                                                             true));

            // search tags field
            this._SearchTags = new TextBox();
            this._SearchTags.ID = "SearchTags_Field";
            this._SearchTags.CssClass = "ckeditor";
            this._SearchTags.Style.Add("width", "98%");
            this._SearchTags.TextMode = TextBoxMode.MultiLine;
            this._SearchTags.Rows = 5;

            this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("SearchTags",
                                                             _GlobalResources.SearchTags,
                                                             this._SearchTags.ID,
                                                             this._SearchTags,
                                                             false,
                                                             false,
                                                             true));

            // is standalone enroll field
            this._IsStandaloneEnroll = new CheckBox();
            this._IsStandaloneEnroll.ID = "IsStandaloneEnroll_Field";
            this._IsStandaloneEnroll.Text = _GlobalResources.AllowLearnersToSelfEnrollInThisInstructorLedTrainingIndependentOfAnyCourseEnrollment;
            this._IsStandaloneEnroll.Attributes.Add("onchange", "CheckStandaloneEnroll()");

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE))
            this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("IsStandaloneEnroll",
                                                             _GlobalResources.StandaloneEnrollment,
                                                             this._IsStandaloneEnroll.ID,
                                                             this._IsStandaloneEnroll,
                                                             false,
                                                             false,
                                                             false));

            

            // is restricted enroll field
            this._IsRestrictedEnroll = new CheckBox();
            this._IsRestrictedEnroll.ID = "IsRestrictedEnroll_Field";
            this._IsRestrictedEnroll.Text = _GlobalResources.LearnersMustBeEnrolledInThisInstructorLedTrainingByAnInstructorOrAdministrator;

            this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("IsRestrictedEnroll",
                                                             _GlobalResources.RestrictedEnrollment,
                                                             this._IsRestrictedEnroll.ID,
                                                             this._IsRestrictedEnroll,
                                                             false,
                                                             false,
                                                             false));

            

            // is restricted drop field
            this._IsRestrictedDrop = new CheckBox();
            this._IsRestrictedDrop.ID = "IsRestrictedDrop_Field";
            this._IsRestrictedDrop.Text = _GlobalResources.OnceEnrolledLearnersWhoWishToDropThisInstructorLedTrainingMustBeDroppedByAnInstructorOrAdministrator;

            this.StandupTrainingPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("IsRestrictedDrop",
                                                            _GlobalResources.RestrictedDrop,
                                                            this._IsRestrictedDrop.ID,
                                                            this._IsRestrictedDrop,
                                                            false,
                                                            false,
                                                            false));

            // this.StandupTrainingPropertiesContainer.Controls.Add(propertiesPanel);

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _BuildPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for properties actions.
        /// </summary>
        private void _BuildPropertiesActionsPanel()
        {
            // clear controls from container
            this.StandupTrainingPropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.StandupTrainingPropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton = new Button();
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._StandupTrainingObject == null)
            { this._SaveButton.Text = _GlobalResources.CreateInstructorLedTrainingModule; }
            else
            { this._SaveButton.Text = _GlobalResources.SaveChanges; }

            this._SaveButton.Command += new CommandEventHandler(this._SaveButton_Command);
            this.StandupTrainingPropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.StandupTrainingPropertiesActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            if (this._StandupTrainingObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // language specific properties - title, description
                bool isDefaultPopulated = false;

                foreach (Library.StandupTraining.LanguageSpecificProperty standupTrainingLanguageSpecificProperty in this._StandupTrainingObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (standupTrainingLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._Title.Text = standupTrainingLanguageSpecificProperty.Title;
                        this._Description.Text = standupTrainingLanguageSpecificProperty.Description;
                        this._Objectives.Text = standupTrainingLanguageSpecificProperty.Objectives;
                        this._SearchTags.Text = standupTrainingLanguageSpecificProperty.SearchTags;

                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificStandupTrainingTitleTextBox = (TextBox)this.StandupTrainingPropertiesContainer.FindControl(this._Title.ID + "_" + standupTrainingLanguageSpecificProperty.LangString);
                        TextBox languageSpecificStandupTrainingDescriptionTextBox = (TextBox)this.StandupTrainingPropertiesContainer.FindControl(this._Description.ID + "_" + standupTrainingLanguageSpecificProperty.LangString);
                        TextBox languageSpecificStandupTrainingObjectivesTextBox = (TextBox)this.StandupTrainingPropertiesContainer.FindControl(this._Objectives.ID + "_" + standupTrainingLanguageSpecificProperty.LangString);
                        TextBox languageSpecificStandupTrainingSearchTagsTextBox = (TextBox)this.StandupTrainingPropertiesContainer.FindControl(this._SearchTags.ID + "_" + standupTrainingLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificStandupTrainingTitleTextBox != null)
                        { languageSpecificStandupTrainingTitleTextBox.Text = standupTrainingLanguageSpecificProperty.Title; }

                        if (languageSpecificStandupTrainingDescriptionTextBox != null)
                        { languageSpecificStandupTrainingDescriptionTextBox.Text = standupTrainingLanguageSpecificProperty.Description; }

                        if (languageSpecificStandupTrainingObjectivesTextBox != null)
                        { languageSpecificStandupTrainingObjectivesTextBox.Text = standupTrainingLanguageSpecificProperty.Objectives; }

                        if (languageSpecificStandupTrainingSearchTagsTextBox != null)
                        { languageSpecificStandupTrainingSearchTagsTextBox.Text = standupTrainingLanguageSpecificProperty.SearchTags; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._Title.Text = this._StandupTrainingObject.Title;
                    this._Description.Text = this._StandupTrainingObject.Description;
                    this._Objectives.Text = this._StandupTrainingObject.Objectives;
                    this._SearchTags.Text = this._StandupTrainingObject.SearchTags;
                }

                // NON-LANGUAGE SPECIFIC PROPERTIES

                // catalog link shortcode
                this._Shortcode.Text = this._StandupTrainingObject.Shortcode;

                // avatar image
                if (this._StandupTrainingObject.Avatar != null)
                {
                    this._AvatarImage.ImageUrl = SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + this._StandupTrainingObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                }

                // cost
                if (this._EcommerceSettings.IsEcommerceSet)
                { this._Cost.Text = this._StandupTrainingObject.Cost.ToString(); }

                // is standalone enroll
                this._IsStandaloneEnroll.Checked = this._StandupTrainingObject.IsStandaloneEnroll;

                // is restricted enroll
                this._IsRestrictedEnroll.Checked = this._StandupTrainingObject.IsRestrictedEnroll;

                // is restricted drop
                this._IsRestrictedDrop.Checked = this._StandupTrainingObject.IsRestrictedDrop;
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;

            // TITLE - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._Title.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.StandupTrainingPropertiesContainer, "Title", _GlobalResources.Name + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // COST - MUST BE DOUBLE
            if (this._EcommerceSettings.IsEcommerceSet)
            {
                double cost;

                if ((!String.IsNullOrWhiteSpace(this._Cost.Text) && !double.TryParse(this._Cost.Text, out cost)))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.StandupTrainingPropertiesContainer, "Cost", _GlobalResources.Cost + " " + _GlobalResources.IsInvalid);
                }
            }

            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {                
                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                // if there is no standup training object, create one
                if (this._StandupTrainingObject == null)
                { this._StandupTrainingObject = new Library.StandupTraining(); }
                
                int id;

                // populate the object
                this._StandupTrainingObject.Title = this._Title.Text;
                
                if (!String.IsNullOrWhiteSpace(this._Shortcode.Text))
                { this._StandupTrainingObject.Shortcode = this._Shortcode.Text; }
                else
                { this._StandupTrainingObject.Shortcode = null; }

                if (this._EcommerceSettings.IsEcommerceSet)
                {
                    if (!String.IsNullOrWhiteSpace(this._Cost.Text))
                    { this._StandupTrainingObject.Cost = Convert.ToDouble(this._Cost.Text); }
                    else
                    { this._StandupTrainingObject.Cost = null; }
                }
                else
                { this._StandupTrainingObject.Cost = null; }
                
                if (!String.IsNullOrWhiteSpace(this._Description.Text))
                { this._StandupTrainingObject.Description = HttpUtility.HtmlDecode(this._Description.Text); }
                else
                { this._StandupTrainingObject.Description = null; }

                if (!String.IsNullOrWhiteSpace(this._Objectives.Text))
                { this._StandupTrainingObject.Objectives = HttpUtility.HtmlDecode(this._Objectives.Text); }
                else
                { this._StandupTrainingObject.Objectives = null; }
                
                if (!String.IsNullOrWhiteSpace(this._SearchTags.Text))
                { this._StandupTrainingObject.SearchTags = this._SearchTags.Text; }
                else
                { this._StandupTrainingObject.SearchTags = null; }

                this._StandupTrainingObject.IsStandaloneEnroll = this._IsStandaloneEnroll.Checked;
                this._StandupTrainingObject.IsRestrictedEnroll = this._IsRestrictedEnroll.Checked;
                this._StandupTrainingObject.IsRestrictedDrop = this._IsRestrictedDrop.Checked;

                // do avatar if existing standup training, if its a new standup training, we'll do it after initial save
                bool isNewStandupTraining = true;

                if (this._StandupTrainingObject.Id > 0)
                {
                    isNewStandupTraining = false;

                    if (this._StandupTrainingAvatar.SavedFilePath != null)
                    {
                        // check user folder existence and create if necessary
                        if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id)))
                        { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id)); }

                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._StandupTrainingAvatar.SavedFilePath)))
                        {
                            fullSavedFilePath = SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar" + Path.GetExtension(this._StandupTrainingAvatar.SavedFilePath);
                            fullSavedFilePathSmall = SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar.small" + Path.GetExtension(this._StandupTrainingAvatar.SavedFilePath);


                            // delete existing avatar images if any
                            if (this._StandupTrainingObject.Avatar != null)
                            {
                                if (File.Exists(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar" + Path.GetExtension(this._StandupTrainingObject.Avatar))))
                                { File.Delete(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar" + Path.GetExtension(this._StandupTrainingObject.Avatar))); }

                                if (File.Exists(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar.small" + Path.GetExtension(this._StandupTrainingObject.Avatar))))
                                { File.Delete(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar.small" + Path.GetExtension(this._StandupTrainingObject.Avatar))); }
                            }

                            // move the uploaded file into the standup training's folder
                            File.Copy(Server.MapPath(this._StandupTrainingAvatar.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._StandupTrainingAvatar.SavedFilePath));
                            this._StandupTrainingObject.Avatar = "avatar" + Path.GetExtension(this._StandupTrainingAvatar.SavedFilePath);

                            // create a smaller version of the avatar for use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            this._StandupTrainingObject.SaveAvatar();
                        }
                        else
                        { this._StandupTrainingObject.Avatar = null; }
                    }
                    else if (this._ClearAvatar != null)
                    {
                        if (this._ClearAvatar.Value == "true")
                        {
                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar" + Path.GetExtension(this._StandupTrainingObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar" + Path.GetExtension(this._StandupTrainingObject.Avatar))); }

                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar.small" + Path.GetExtension(this._StandupTrainingObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar.small" + Path.GetExtension(this._StandupTrainingObject.Avatar))); }

                            this._StandupTrainingObject.Avatar = null;

                            this._StandupTrainingObject.SaveAvatar();
                        }
                    }
                    else
                    { }
                }

                // clean up standalone enrollment option in lite version
                if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.STANDALONEENROLLMENT_ENABLE))
                {
                    this._StandupTrainingObject.IsStandaloneEnroll = false;
                }

                // save the standup training, save its returned id to viewstate, and set the current standup training object's id
                id = this._StandupTrainingObject.Save();
                this.ViewState["id"] = id;
                this._StandupTrainingObject.Id = id;

                // if this was a new standup training we just saved, we now have the id so that
                // we can create the standup training's folder and update the avatar if necessary.
                if (isNewStandupTraining)
                {
                    // check standup training folder existence and create if necessary
                    if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id)))
                    { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id)); }

                    // if there is an avatar, move it and update the database record
                    if (this._StandupTrainingAvatar.SavedFilePath != null)
                    {
                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._StandupTrainingAvatar.SavedFilePath)))
                        {
                            fullSavedFilePath = SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar" + Path.GetExtension(this._StandupTrainingAvatar.SavedFilePath);
                            fullSavedFilePathSmall = SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + "avatar.small" + Path.GetExtension(this._StandupTrainingAvatar.SavedFilePath);

                            // move the uploaded file into the user's folder
                            File.Copy(Server.MapPath(this._StandupTrainingAvatar.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._StandupTrainingAvatar.SavedFilePath));
                            this._StandupTrainingObject.Avatar = "avatar" + Path.GetExtension(this._StandupTrainingAvatar.SavedFilePath);

                            // create a smaller version of the avatar for use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            this._StandupTrainingObject.SaveAvatar();
                        }
                    }
                }

                // do standup training language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string standupTrainingTitle = null;
                        string standupTrainingDescription = null;
                        string standupTrainingObjectives = null;
                        string standupTrainingSearchTags = null;

                        // get text boxes
                        TextBox languageSpecificStandupTrainingTitleTextBox = (TextBox)this.StandupTrainingPropertiesContainer.FindControl(this._Title.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificStandupTrainingDescriptionTextBox = (TextBox)this.StandupTrainingPropertiesContainer.FindControl(this._Description.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificStandupTrainingObjectivesTextBox = (TextBox)this.StandupTrainingPropertiesContainer.FindControl(this._Objectives.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificStandupTrainingSearchTagsTextBox = (TextBox)this.StandupTrainingPropertiesContainer.FindControl(this._SearchTags.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificStandupTrainingTitleTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificStandupTrainingTitleTextBox.Text))
                            { standupTrainingTitle = languageSpecificStandupTrainingTitleTextBox.Text; }
                        }

                        if (languageSpecificStandupTrainingDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificStandupTrainingDescriptionTextBox.Text))
                            { standupTrainingDescription = languageSpecificStandupTrainingDescriptionTextBox.Text; }
                        }

                        if (languageSpecificStandupTrainingObjectivesTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificStandupTrainingObjectivesTextBox.Text))
                            { standupTrainingObjectives = languageSpecificStandupTrainingObjectivesTextBox.Text; }
                        }

                        if (languageSpecificStandupTrainingSearchTagsTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificStandupTrainingSearchTagsTextBox.Text))
                            { standupTrainingSearchTags = languageSpecificStandupTrainingSearchTagsTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(standupTrainingTitle) ||
                            !String.IsNullOrWhiteSpace(standupTrainingDescription) ||
                            !String.IsNullOrWhiteSpace(standupTrainingObjectives) ||
                            !String.IsNullOrWhiteSpace(standupTrainingSearchTags))
                        {
                            this._StandupTrainingObject.SaveLang(cultureInfo.Name,
                                                                 standupTrainingTitle,
                                                                 standupTrainingDescription,
                                                                 standupTrainingObjectives,
                                                                 standupTrainingSearchTags);
                        }
                    }
                }

                // load the saved standup training object
                this._StandupTrainingObject = new Library.StandupTraining(id);

                // clear controls for containers that have dynamically added elements
                this.StandupTrainingObjectMenuContainer.Controls.Clear();

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.StandupTrainingPropertiesFeedbackContainer, _GlobalResources.InstructorLedTrainingModulePropertiesHaveBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.StandupTrainingPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.StandupTrainingPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.StandupTrainingPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.StandupTrainingPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.StandupTrainingPropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/standuptraining");
        }
        #endregion
    }
}