﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;


namespace Asentia.LMS.Pages.Administrator.StandupTraining
{
    public class Dashboard : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel StandupTrainingDashboardFormContentWrapperContainer;
        public Panel StandupTrainingObjectMenuContainer;
        public Panel StandupTrainingDashboardWrapperContainer;
        public Panel StandupTrainingDashboardInstructionsPanel;
        public Panel StandupTrainingDashboardFeedbackContainer;
        #endregion

        #region Private Properties
        private Library.StandupTraining _StandupTrainingObject;
        private EcommerceSettings _EcommerceSettings;
        private ObjectDashboard _StandupTrainingObjectDashboard;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            //ClientScriptManager csm = this.Page.ClientScript;
            //csm.RegisterClientScriptResource(typeof(Dashboard), "Asentia.LMS.Pages.Administrator.StandupTraining.Dashboard.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_InstructorLedTrainingManager))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("ObjectDashboard.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/standuptraining/Dashboard.css");

            // get the standup training object
            this._GetStandupTrainingObject();

            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetStandupTrainingObject
        /// <summary>
        /// Gets a standup training object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetStandupTrainingObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._StandupTrainingObject = new Library.StandupTraining(id); }
                }
                catch
                { Response.Redirect("~/administrator/standuptraining"); }
            }
            else { Response.Redirect("~/administrator/standuptraining"); }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // set container classes
            this.StandupTrainingDashboardFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.StandupTrainingDashboardWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the standup training object menu
            if (this._StandupTrainingObject != null)
            {
                StandupTrainingObjectMenu standupTrainingObjectMenu = new StandupTrainingObjectMenu(this._StandupTrainingObject);
                standupTrainingObjectMenu.SelectedItem = StandupTrainingObjectMenu.MenuObjectItem.StandupTrainingDashboard;

                this.StandupTrainingObjectMenuContainer.Controls.Add(standupTrainingObjectMenu);
            }

            // build the standup training object dashboard
            this._StandupTrainingObjectDashboard = new ObjectDashboard("StandupTrainingObjectDashboard");

            this._BuildInformationWidget();
            this._BuildActionsWidget();
            this._BuildRecentSessionsWidget();
            this._BuildUpcomingSessionsWidget();            

            if (this._EcommerceSettings.IsEcommerceSetAndVerified)
            { this._BuildEcommerceWidget(); }

            // attach object widget to wrapper container
            this.StandupTrainingDashboardWrapperContainer.Controls.Add(this._StandupTrainingObjectDashboard);
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get standup training title information
            string standupTrainingTitleInInterfaceLanguage = this._StandupTrainingObject.Title;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Library.StandupTraining.LanguageSpecificProperty standupTrainingLanguageSpecificProperty in this._StandupTrainingObject.LanguageSpecificProperties)
                {
                    if (standupTrainingLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { standupTrainingTitleInInterfaceLanguage = standupTrainingLanguageSpecificProperty.Title; }
                }
            }

            // get standup training avatar information
            string standupTrainingImagePath;
            string imageCssClass = null;

            if (this._StandupTrainingObject.Avatar != null)
            {
                standupTrainingImagePath = SitePathConstants.SITE_STANDUPTRAINING_ROOT + this._StandupTrainingObject.Id + "/" + this._StandupTrainingObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                imageCssClass = "AvatarImage";
            }
            else
            {
                standupTrainingImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.InstructorLedTrainingModules, "/administrator/standuptraining"));
            breadCrumbLinks.Add(new BreadcrumbLink(standupTrainingTitleInInterfaceLanguage));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, standupTrainingTitleInInterfaceLanguage, standupTrainingImagePath, imageCssClass);
        }
        #endregion

        #region _BuildInformationWidget
        /// <summary>
        /// Builds the information widget.
        /// </summary>
        private void _BuildInformationWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> informationWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // CREATED DATE
            string formattedCreatedDate = TimeZoneInfo.ConvertTimeFromUtc(this._StandupTrainingObject.DtCreated, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", ""));
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("CreatedDate", _GlobalResources.Created + ":", formattedCreatedDate, ObjectDashboard.WidgetItemType.Text));

            // MODIFIED DATE
            string formattedModifiedDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._StandupTrainingObject.DtModified, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", ""));
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("LastModifiedDate", _GlobalResources.LastModified + ":", formattedModifiedDate, ObjectDashboard.WidgetItemType.Text));

            // add the widget
            this._StandupTrainingObjectDashboard.AddWidget("InformationWidget", _GlobalResources.Information, informationWidgetItems);
        }
        #endregion

        #region _BuildActionsWidget
        /// <summary>
        /// Builds the actions widget.
        /// </summary>
        private void _BuildActionsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> actionsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // NEW SESSION LINK

            Panel createSessionLinkWrapper = new Panel();

            HyperLink createSessionLink = new HyperLink();
            createSessionLink.NavigateUrl = "sessions/Modify.aspx?stid=" + this._StandupTrainingObject.Id.ToString();
            createSessionLinkWrapper.Controls.Add(createSessionLink);

            Image createSessionLinkImage = new Image();
            createSessionLinkImage.CssClass = "SmallIcon";
            createSessionLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SESSION, ImageFiles.EXT_PNG);
            createSessionLink.Controls.Add(createSessionLinkImage);

            Image createSessionOverlayLinkImage = new Image();
            createSessionOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
            createSessionOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            createSessionLink.Controls.Add(createSessionOverlayLinkImage);

            Literal createSessionLinkText = new Literal();
            createSessionLinkText.Text = _GlobalResources.CreateANewSession;
            createSessionLink.Controls.Add(createSessionLinkText);

            actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateSession", null, createSessionLinkWrapper, ObjectDashboard.WidgetItemType.Link));

            // NEW EMAIL NOTIFICATION LINK

            Panel createEmailNotificationLinkWrapper = new Panel();

            HyperLink createEmailNotificationLink = new HyperLink();
            createEmailNotificationLink.NavigateUrl = "emailnotifications/Modify.aspx?stid=" + this._StandupTrainingObject.Id.ToString();
            createEmailNotificationLinkWrapper.Controls.Add(createEmailNotificationLink);

            Image createEmailNotificationLinkImage = new Image();
            createEmailNotificationLinkImage.CssClass = "SmallIcon";
            createEmailNotificationLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
            createEmailNotificationLink.Controls.Add(createEmailNotificationLinkImage);

            Image createEmailNotificationOverlayLinkImage = new Image();
            createEmailNotificationOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
            createEmailNotificationOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            createEmailNotificationLink.Controls.Add(createEmailNotificationOverlayLinkImage);

            Literal createEmailNotificationLinkText = new Literal();
            createEmailNotificationLinkText.Text = _GlobalResources.CreateANewEmailNotification;
            createEmailNotificationLink.Controls.Add(createEmailNotificationLinkText);

            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE) ?? false)
            {
                actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateEmailNotification", null, createEmailNotificationLinkWrapper, ObjectDashboard.WidgetItemType.Link));
            }

            // add the widget
            this._StandupTrainingObjectDashboard.AddWidget("ActionsWidget", _GlobalResources.Actions, actionsWidgetItems);
        }
        #endregion        

        #region _BuildRecentSessionsWidget
        /// <summary>
        /// Builds the recent sessions widget.
        /// </summary>
        private void _BuildRecentSessionsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> recentSessionsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the datatable of recent sessions
            DataTable sessions = Library.StandupTraining.GetRecentSessions(this._StandupTrainingObject.Id);

            if (sessions.Rows.Count > 0)
            {
                foreach (DataRow row in sessions.Rows)
                {
                    int idStandupTrainingInstance = Convert.ToInt32(row["idStandupTrainingInstance"]);
                    string name = row["title"].ToString();
                    StandupTrainingInstance.MeetingType type = (StandupTrainingInstance.MeetingType)Convert.ToInt32(row["type"]);                    

                    Panel listItemWrapperContainer = new Panel();
                    listItemWrapperContainer.CssClass = "DashboardListItemWrapper";

                    // LIST ITEM IMAGE

                    Panel listItemContentPanel1 = new Panel();
                    listItemContentPanel1.CssClass = "DashboardListItemImage";
                    listItemWrapperContainer.Controls.Add(listItemContentPanel1);

                    string listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CLASSROOM, ImageFiles.EXT_PNG);                    

                    if (type == StandupTrainingInstance.MeetingType.Online)
                    { listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_WEBMEETING, ImageFiles.EXT_PNG); }
                    else if (type == StandupTrainingInstance.MeetingType.GoToMeeting)
                    { listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOMEETING, ImageFiles.EXT_PNG); }
                    else if (type == StandupTrainingInstance.MeetingType.GoToTraining)
                    { listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOTRAINING, ImageFiles.EXT_PNG); }
                    else if (type == StandupTrainingInstance.MeetingType.GoToWebinar)
                    { listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOWEBINAR, ImageFiles.EXT_PNG); }
                    else if (type == StandupTrainingInstance.MeetingType.WebEx)
                    { listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_WEBEX, ImageFiles.EXT_PNG); }
                    else
                    { }

                    Image listItemImage = new Image();
                    listItemImage.ImageUrl = listItemImagePath;
                    listItemImage.CssClass = "SmallIcon";
                    listItemImage.AlternateText = name;
                    listItemContentPanel1.Controls.Add(listItemImage);

                    // LIST ITEM TEXT

                    Panel listItemContentPanel2 = new Panel();
                    listItemContentPanel2.CssClass = "DashboardListItemText";
                    listItemWrapperContainer.Controls.Add(listItemContentPanel2);

                    HyperLink nameLink = new HyperLink();
                    nameLink.NavigateUrl = "sessions/Modify.aspx?stid=" + this._StandupTrainingObject.Id.ToString() + "&id=" + idStandupTrainingInstance.ToString();
                    nameLink.Text = name;
                    listItemContentPanel2.Controls.Add(nameLink);

                    // LIST ITEM OPTION LINKS

                    Panel listItemContentPanel3 = new Panel();
                    listItemContentPanel3.CssClass = "DashboardListItemOptionLinks";
                    listItemWrapperContainer.Controls.Add(listItemContentPanel3);

                    // manage roster link                    
                    HyperLink manageRosterLink = new HyperLink();
                    manageRosterLink.NavigateUrl = "sessions/ManageRoster.aspx?stid=" + this._StandupTrainingObject.Id.ToString() + "&id=" + idStandupTrainingInstance.ToString();
                    listItemContentPanel3.Controls.Add(manageRosterLink);

                    Image manageRosterIcon = new Image();
                    manageRosterIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SESSION_BUTTON, ImageFiles.EXT_PNG);
                    manageRosterIcon.CssClass = "SmallIcon";
                    manageRosterIcon.AlternateText = _GlobalResources.ManageRoster;
                    manageRosterIcon.ToolTip = _GlobalResources.ManageRoster;
                    manageRosterLink.Controls.Add(manageRosterIcon);

                    // add the list item to the control list
                    recentSessionsWidgetItems.Add(new ObjectDashboard.WidgetItem("RecentSession_" + idStandupTrainingInstance.ToString(), null, listItemWrapperContainer, ObjectDashboard.WidgetItemType.Object));
                }
            }
            else
            { recentSessionsWidgetItems.Add(new ObjectDashboard.WidgetItem("NoRecentSessionsFound", null, _GlobalResources.ThereAreNoRecentSessions, ObjectDashboard.WidgetItemType.Text)); }

            // add the widget
            this._StandupTrainingObjectDashboard.AddWidget("RecentSessionsWidget", _GlobalResources.RecentSessions, recentSessionsWidgetItems);
        }
        #endregion

        #region _BuildUpcomingSessionsWidget
        /// <summary>
        /// Builds the upcoming sessions widget.
        /// </summary>
        private void _BuildUpcomingSessionsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> upcomingSessionsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the datatable of upcoming sessions
            DataTable sessions = Library.StandupTraining.GetUpcomingSessions(this._StandupTrainingObject.Id);

            if (sessions.Rows.Count > 0)
            {
                foreach (DataRow row in sessions.Rows)
                {
                    int idStandupTrainingInstance = Convert.ToInt32(row["idStandupTrainingInstance"]);
                    string name = row["title"].ToString();
                    StandupTrainingInstance.MeetingType type = (StandupTrainingInstance.MeetingType)Convert.ToInt32(row["type"]);

                    Panel listItemWrapperContainer = new Panel();
                    listItemWrapperContainer.CssClass = "DashboardListItemWrapper";

                    // LIST ITEM IMAGE

                    Panel listItemContentPanel1 = new Panel();
                    listItemContentPanel1.CssClass = "DashboardListItemImage";
                    listItemWrapperContainer.Controls.Add(listItemContentPanel1);

                    string listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CLASSROOM, ImageFiles.EXT_PNG);

                    if (type == StandupTrainingInstance.MeetingType.Online)
                    { listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_WEBMEETING, ImageFiles.EXT_PNG); }
                    else if (type == StandupTrainingInstance.MeetingType.GoToMeeting)
                    { listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOMEETING, ImageFiles.EXT_PNG); }
                    else if (type == StandupTrainingInstance.MeetingType.GoToTraining)
                    { listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOTRAINING, ImageFiles.EXT_PNG); }
                    else if (type == StandupTrainingInstance.MeetingType.GoToWebinar)
                    { listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOWEBINAR, ImageFiles.EXT_PNG); }
                    else if (type == StandupTrainingInstance.MeetingType.WebEx)
                    { listItemImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_WEBEX, ImageFiles.EXT_PNG); }
                    else
                    { }

                    Image listItemImage = new Image();
                    listItemImage.ImageUrl = listItemImagePath;
                    listItemImage.CssClass = "SmallIcon";
                    listItemImage.AlternateText = name;
                    listItemContentPanel1.Controls.Add(listItemImage);

                    // LIST ITEM TEXT

                    Panel listItemContentPanel2 = new Panel();
                    listItemContentPanel2.CssClass = "DashboardListItemText";
                    listItemWrapperContainer.Controls.Add(listItemContentPanel2);

                    HyperLink nameLink = new HyperLink();
                    nameLink.NavigateUrl = "sessions/Modify.aspx?stid=" + this._StandupTrainingObject.Id.ToString() + "&id=" + idStandupTrainingInstance.ToString();
                    nameLink.Text = name;
                    listItemContentPanel2.Controls.Add(nameLink);

                    // LIST ITEM OPTION LINKS

                    Panel listItemContentPanel3 = new Panel();
                    listItemContentPanel3.CssClass = "DashboardListItemOptionLinks";
                    listItemWrapperContainer.Controls.Add(listItemContentPanel3);

                    // manage roster link                    
                    HyperLink manageRosterLink = new HyperLink();
                    manageRosterLink.NavigateUrl = "sessions/ManageRoster.aspx?stid=" + this._StandupTrainingObject.Id.ToString() + "&id=" + idStandupTrainingInstance.ToString();
                    listItemContentPanel3.Controls.Add(manageRosterLink);

                    Image manageRosterIcon = new Image();
                    manageRosterIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SESSION_BUTTON, ImageFiles.EXT_PNG);
                    manageRosterIcon.CssClass = "SmallIcon";
                    manageRosterIcon.AlternateText = _GlobalResources.ManageRoster;
                    manageRosterIcon.ToolTip = _GlobalResources.ManageRoster;
                    manageRosterLink.Controls.Add(manageRosterIcon);

                    // add the list item to the control list
                    upcomingSessionsWidgetItems.Add(new ObjectDashboard.WidgetItem("UpcomingSession_" + idStandupTrainingInstance.ToString(), null, listItemWrapperContainer, ObjectDashboard.WidgetItemType.Object));
                }
            }
            else
            { upcomingSessionsWidgetItems.Add(new ObjectDashboard.WidgetItem("NoUpcomingSessionsFound", null, _GlobalResources.ThereAreNoUpcomingSessions, ObjectDashboard.WidgetItemType.Text)); }

            // add the widget
            this._StandupTrainingObjectDashboard.AddWidget("UpcomingSessionsWidget", _GlobalResources.UpcomingSessions, upcomingSessionsWidgetItems);
        }
        #endregion        

        #region _BuildEcommerceWidget
        /// <summary>
        /// Builds the ecommerce widget.
        /// </summary>
        private void _BuildEcommerceWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> ecommerceWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // COST
            if (this._StandupTrainingObject.Cost > 0)
            {
                string formattedCourseCost = String.Format("{0}{1:N2} ({2})", this._EcommerceSettings.CurrencySymbol, this._StandupTrainingObject.Cost, this._EcommerceSettings.CurrencyCode);
                ecommerceWidgetItems.Add(new ObjectDashboard.WidgetItem("Cost", _GlobalResources.Cost + ":", formattedCourseCost, ObjectDashboard.WidgetItemType.Text));
            }
            else
            { ecommerceWidgetItems.Add(new ObjectDashboard.WidgetItem("Cost", _GlobalResources.Cost + ":", _GlobalResources.Free, ObjectDashboard.WidgetItemType.Text)); }

            // TOTAL EARNINGS

            // get the data and add it to the widget items            
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            double? totalEarnings = 0;

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idObject", this._StandupTrainingObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@itemType", PurchaseItemType.InstructorLedTraining, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@totalEarnings", null, SqlDbType.Float, 8, ParameterDirection.Output);

                databaseObject.ExecuteNonQuery("[TransactionItem.GetTotalEarningsForObject]", true);

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // format the data strings
                totalEarnings = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@totalEarnings"].Value);

                if (totalEarnings == null)
                { totalEarnings = 0; }
            }
            catch // on errors, just bury it
            { }
            finally
            { databaseObject.Dispose(); }

            // add the earnings to the widget items
            string formattedCourseEarnings = String.Format("{0}{1:N2} ({2})", this._EcommerceSettings.CurrencySymbol, totalEarnings, this._EcommerceSettings.CurrencyCode);
            ecommerceWidgetItems.Add(new ObjectDashboard.WidgetItem("TotalEarnings", _GlobalResources.TotalEarnings + ":", formattedCourseEarnings, ObjectDashboard.WidgetItemType.Text));

            // add the widget
            this._StandupTrainingObjectDashboard.AddWidget("EcommerceWidget", _GlobalResources.ECommerce, ecommerceWidgetItems);
        }
        #endregion
    }
}
