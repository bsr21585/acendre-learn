﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.StandupTraining
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel StandupTrainingFormContentWrapperContainer;        
        public UpdatePanel StandupTrainingModulesGridUpdatePanel;
        public Grid StandupTrainingModulesGrid;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmAction;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            //ClientScriptManager csm = this.Page.ClientScript;
            //csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.StandupTraining.Default.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_InstructorLedTrainingManager))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("page-specific/administrator/standuptraining/Default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.InstructorLedTrainingModules));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, _GlobalResources.InstructorLedTrainingModules, ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.StandupTrainingFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.StandupTrainingModulesGrid.BindData();
            }
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD STANDUP TRAINING MODULE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddStandupTrainingModuleLink",
                                                null,
                                                "Modify.aspx",
                                                null,
                                                _GlobalResources.NewInstructorLedTrainingModule,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.StandupTrainingModulesGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.StandupTrainingModulesGrid.StoredProcedure = Library.StandupTraining.GridProcedure;
            this.StandupTrainingModulesGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.StandupTrainingModulesGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.StandupTrainingModulesGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.StandupTrainingModulesGrid.IdentifierField = "idStandupTraining";
            this.StandupTrainingModulesGrid.DefaultSortColumn = "title";
            this.StandupTrainingModulesGrid.SearchBoxPlaceholderText = _GlobalResources.SearchInstructorLedTraining;

            // data key names
            this.StandupTrainingModulesGrid.DataKeyNames = new string[] { "idStandupTraining" };

            // columns
            GridColumn nameNumberSessions = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.Sessions, null, "title"); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.StandupTrainingModulesGrid.AddColumn(nameNumberSessions);
            this.StandupTrainingModulesGrid.AddColumn(options);

            // add row data bound event
            this.StandupTrainingModulesGrid.RowDataBound += new GridViewRowEventHandler(this._StandupTrainingModulesGrid_RowDataBound);
        }
        #endregion

        #region _StandupTrainingModulesGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _StandupTrainingModulesGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idStandUpTraining = Convert.ToInt32(rowView["idStandUpTraining"]);

                // AVATAR, NAME, NUMBER OF SESSIONS

                string avatar = rowView["avatar"].ToString();
                string title = Server.HtmlEncode(rowView["title"].ToString());
                int sessionCount = Convert.ToInt32(rowView["sessionCount"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                if (!String.IsNullOrWhiteSpace(avatar))
                {
                    avatarImagePath = SitePathConstants.SITE_STANDUPTRAINING_ROOT + idStandUpTraining.ToString() + "/" + avatar;
                    avatarImageClass += " AvatarImage";
                }

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = title;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink nameLink = new HyperLink();
                nameLink.NavigateUrl = "Dashboard.aspx?id=" + idStandUpTraining.ToString();
                nameLink.Text = title;
                nameLabel.Controls.Add(nameLink);

                // number of sessions
                Label numberSessionsLabel = new Label();
                numberSessionsLabel.CssClass = "GridSecondaryLine";
                numberSessionsLabel.Text = sessionCount + " " + ((sessionCount != 1) ? _GlobalResources.Sessions : _GlobalResources.Session);
                e.Row.Cells[1].Controls.Add(numberSessionsLabel);

                // OPTIONS COLUMN

                // modify
                Label modifySpan = new Label();
                modifySpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(modifySpan);

                HyperLink modifyLink = new HyperLink();
                modifyLink.NavigateUrl = "Modify.aspx?id=" + idStandUpTraining.ToString();
                modifySpan.Controls.Add(modifyLink);

                Image modifyIcon = new Image();
                modifyIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
                modifyIcon.AlternateText = _GlobalResources.Modify;
                modifyIcon.ToolTip = _GlobalResources.InstructorLedTrainingProperties;
                modifyLink.Controls.Add(modifyIcon);

                // sessions
                Label sessionsSpan = new Label();
                sessionsSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(sessionsSpan);

                HyperLink sessionsLink = new HyperLink();
                sessionsLink.NavigateUrl = "sessions/Default.aspx?stid=" + idStandUpTraining.ToString();
                sessionsSpan.Controls.Add(sessionsLink);

                Image sessionsIcon = new Image();
                sessionsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SESSION_BUTTON, ImageFiles.EXT_PNG);
                sessionsIcon.AlternateText = _GlobalResources.Sessions;
                sessionsIcon.ToolTip = _GlobalResources.Sessions;
                sessionsLink.Controls.Add(sessionsIcon);
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedInstructorLedTrainingModule_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedInstructorLedTrainingModule_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseInstructorLedTrainingModule_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.StandupTrainingModulesGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.StandupTrainingModulesGrid.Rows[i].FindControl(this.StandupTrainingModulesGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.StandupTraining.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedInstructorLedTrainingModule_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoInstructorLedTrainingModule_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.StandupTrainingModulesGrid.BindData();
            }
        }
        #endregion
    }
}
