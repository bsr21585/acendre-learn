﻿//Method to remove an instructor led training avatar
function DeleteInstructorLedTrainingAvatar() {
    $("#Avatar_Field_ImageContainer").remove();
    $("#ClearAvatar_Field").val("true");
}

//Updates shortcode link as you type
function UpdateShortcodeLink(shortcodeFieldId) {
    var shortcodeLinkText = $("#ShortcodeLink").text();
    var shortcodeLinkTextSplit = shortcodeLinkText.split("?");
    var shortcodeQSPrefix = "?type=ilt&sc=";

    shortcodeLinkText = shortcodeLinkTextSplit[0] + shortcodeQSPrefix + $("#Shortcode_Field").val();
    $("#ShortcodeLink").text(shortcodeLinkText);
}

function CheckStandaloneEnroll() {
    if ($("#IsStandaloneEnroll_Field").is(":checked")) {
        $("#IsRestrictedEnroll_Field").prop("checked", false);
        $("#IsRestrictedEnroll_Field").attr("disabled", true);
    } else {
        $("#IsRestrictedEnroll_Field").attr("disabled", false);
    }
}