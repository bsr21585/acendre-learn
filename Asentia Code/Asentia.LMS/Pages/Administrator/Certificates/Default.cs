﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.Certificates
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel = new Panel();
        public Panel CertificatesFormContentWrapperContainer = new Panel();

        public UpdatePanel _CertificatesGridUpdatePanel = new UpdatePanel();

        public Panel CertificatesPageWrapperContainer = new Panel();      
        public Panel CertificatesPropertiesContainer = new Panel();       
        #endregion

        #region Private Properties
        private ModalPopup _CertificateTemplatesGridConfirmAction = new ModalPopup();
        private ModalPopup _ObjectCertificatesGridConfirmAction = new ModalPopup();

        private LinkButton _CertificateTemplatesDeleteButton = new LinkButton();
        private LinkButton _ObjectCertificatesDeleteButton = new LinkButton();

        private Grid _CertificateTemplatesGrid = new Grid();
        private Grid _ObjectCertificatesGrid = new Grid();

        private Panel _CertificateTemplatesGridActionsPanel = new Panel();
        private Panel _ObjectCertificatesGridActionsPanel = new Panel();

        private Panel _CertificatesTabPanelsContainer = new Panel();
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CertificateTemplateManager))
            { Response.Redirect("/"); }

            // build the breadcrumb and page title.
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CertificatesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CertificatesPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildPageControls();

            // if not postback
            if (!IsPostBack)
            {
                // Certificate Templates grid bind data
                this._CertificateTemplatesGrid.BindData();

                // Object Certificates grid bind data
                this._ObjectCertificatesGrid.BindData();
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.CertificateTemplates));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, _GlobalResources.CertificateTemplates, ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE_TEMPLATE,
                                                                                                                                   ImageFiles.EXT_PNG));
        }
        #endregion

        #region _BuildPageControls
        private void _BuildPageControls()
        {
            // build certificates tabs
            this._BuildTabsList();

            this._CertificatesTabPanelsContainer = new Panel();
            this._CertificatesTabPanelsContainer.ID = "Certificates_TabPanelsContainer";
            this._CertificatesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.CertificatesPropertiesContainer.Controls.Add(this._CertificatesTabPanelsContainer);

            this._BuildCertificatesListPanel();

            this.CertificatesFormContentWrapperContainer.Controls.Add(this.CertificatesPropertiesContainer);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // add certificate link
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddCertificateLink",
                                                null,
                                                "Modify.aspx",
                                                null,
                                                _GlobalResources.NewCertificateTemplate,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE_TEMPLATE, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for the form.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("CertificateTemplates", _GlobalResources.CertificateTemplates));
            tabs.Enqueue(new KeyValuePair<string, string>("ObjectCertificates", _GlobalResources.ObjectCertificates));

            // build and attach the tabs
            this.CertificatesPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("Certificates", tabs));
        }
        #endregion

        #region _BuildCertificatesListPanel
        /// <summary>
        /// Builds the properties form.
        /// </summary>
        private void _BuildCertificatesListPanel()
        {
            #region Certificate Templates

            // "certificate templates" is the default tab, so this is visible on page load.
            Panel certificateTemplatesPanel = new Panel();
            certificateTemplatesPanel.ID = "Certificates_" + "CertificateTemplates" + "_TabPanel";
            certificateTemplatesPanel.Attributes.Add("style", "display: block;");

            // list of certificate templates tab controls
            List<Control> certificateTemplatesListsInputControls = new List<Control>();

            // build certificate templates grid
            this._BuildCertificateTemplatesGrid();
            UpdatePanel certificateTemplatesGridUpdatePanel = new UpdatePanel();
            certificateTemplatesGridUpdatePanel.ID = "CertificateTemplatesGridUpdatePanel";
            certificateTemplatesGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            certificateTemplatesGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._CertificateTemplatesGrid);

            // adding update panel to controls list
            certificateTemplatesListsInputControls.Add(certificateTemplatesGridUpdatePanel);

            // build certificate templates grid action panel 
            this._BuildCertificateTemplatesGridActionsPanel();
            certificateTemplatesListsInputControls.Add(this._CertificateTemplatesGridActionsPanel);

            // build certificate templates grid action modal popup
            this._BuildCertificateTemplatesGridActionsModal();
            certificateTemplatesListsInputControls.Add(this._CertificateTemplatesGridConfirmAction);


            //add controls to panel
            certificateTemplatesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CertificateTemplates_List",
                                                                                               String.Empty,
                                                                                               certificateTemplatesListsInputControls,
                                                                                               false,
                                                                                               true));

            #endregion

            #region Object Certificates

            Panel objectCertificatesPanel = new Panel();
            objectCertificatesPanel.ID = "Certificates_" + "ObjectCertificates" + "_TabPanel";
            objectCertificatesPanel.Attributes.Add("style", "display: none;");

            // list of object certificates tab controls
            List<Control> objectCertificatesListsInputControls = new List<Control>();

            // build object certificates grid
            this._BuildObjectCertificatesGrid();
            UpdatePanel objectCertificatesGridUpdatePanel = new UpdatePanel();
            objectCertificatesGridUpdatePanel.ID = "ObjectCertificatesGridUpdatePanel";
            objectCertificatesGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            objectCertificatesGridUpdatePanel.ContentTemplateContainer.Controls.Add(this._ObjectCertificatesGrid);

            // adding update panel to controls list
            objectCertificatesListsInputControls.Add(objectCertificatesGridUpdatePanel);

            // build object certificates grid action panel 
            this._BuildObjectCertificatesGridActionsPanel();
            objectCertificatesListsInputControls.Add(this._ObjectCertificatesGridActionsPanel);

            // build object certificates grid action modal popup
            this._BuildObjectCertificatesGridActionsModal();
            objectCertificatesListsInputControls.Add(this._ObjectCertificatesGridConfirmAction);

            // add controls to panel
            objectCertificatesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ObjectCertificates_List",
                                                                                                    String.Empty,
                                                                                                    objectCertificatesListsInputControls,
                                                                                                    false,
                                                                                                    true));

            #endregion

            //add controls to page
            this._CertificatesTabPanelsContainer.Controls.Add(certificateTemplatesPanel);
            this._CertificatesTabPanelsContainer.Controls.Add(objectCertificatesPanel);
        }
        #endregion

        #region Certificate Templates
        #region _BuildCertificateTemplatesEnrollmentsGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildCertificateTemplatesGrid()
        {
            this._CertificateTemplatesGrid.ID = "CertificateTemplatesGrid";
            this._CertificateTemplatesGrid.StoredProcedure = Library.Certificate.GridProcedure;
            this._CertificateTemplatesGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._CertificateTemplatesGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._CertificateTemplatesGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);

            this._CertificateTemplatesGrid.IdentifierField = "idCertificate";
            this._CertificateTemplatesGrid.DefaultSortColumn = "name";
            this._CertificateTemplatesGrid.SearchBoxPlaceholderText = _GlobalResources.SearchCertificates;

            // data key names
            this._CertificateTemplatesGrid.DataKeyNames = new string[] { "idCertificate" };

            // columns
            GridColumn nameIssuingCredits = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.Issuing + ", " + _GlobalResources.Credits + "", null, "name"); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this._CertificateTemplatesGrid.AddColumn(nameIssuingCredits);

            // add row data bound event
            this._CertificateTemplatesGrid.RowDataBound += new GridViewRowEventHandler(this._CertificateTemplatesGrid_RowDataBound);
        }
        #endregion

        #region _CertificateTemplatesGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the certificate templates grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _CertificateTemplatesGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idCertificate = Convert.ToInt32(rowView["idCertificate"]);

                // AVATAR, NAME, CREDITS

                string name = Server.HtmlEncode(rowView["name"].ToString());
                string issuing = Server.HtmlEncode(rowView["issuingOrganization"].ToString());
                int credits = Convert.ToInt32(rowView["credits"]);

                // avatar                
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = name;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink nameLink = new HyperLink();
                nameLink.NavigateUrl = "Modify.aspx?id=" + idCertificate.ToString();
                nameLink.Text = name;
                nameLabel.Controls.Add(nameLink);

                // issuing organization
                Label issuingLabel = new Label();
                issuingLabel.CssClass = "GridSecondaryLine";
                issuingLabel.Text = issuing;
                e.Row.Cells[1].Controls.Add(issuingLabel);

                // credits
                Label creditsLabel = new Label();
                creditsLabel.CssClass = "GridSecondaryLine";
                creditsLabel.Text = credits.ToString() + " " + ((credits != 1) ? _GlobalResources.Credits : _GlobalResources.Credit);
                e.Row.Cells[1].Controls.Add(creditsLabel);
            }
        }
        #endregion

        #region _BuildCertificateTemplatesGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildCertificateTemplatesGridActionsPanel()
        {
            this._CertificateTemplatesGridActionsPanel.ID = "CertificateTemplatesActionsPanel";
            this._CertificateTemplatesGridActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._CertificateTemplatesDeleteButton.ID = "CertificateTemplatesGridDeleteButton";
            this._CertificateTemplatesDeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "CertificateTemplatesGridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._CertificateTemplatesDeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedCertificateTemplate_s;
            this._CertificateTemplatesDeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this._CertificateTemplatesGridActionsPanel.Controls.Add(this._CertificateTemplatesDeleteButton);
        }
        #endregion

        #region _BuildCertificateTemplatesGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildCertificateTemplatesGridActionsModal()
        {
            this._CertificateTemplatesGridConfirmAction.ID = "CertificateTemplatesGridConfirmAction";

            // set modal properties
            this._CertificateTemplatesGridConfirmAction.Type = ModalPopupType.Confirm;
            this._CertificateTemplatesGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this._CertificateTemplatesGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._CertificateTemplatesGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedCertificateTemplate_s;
            this._CertificateTemplatesGridConfirmAction.TargetControlID = this._CertificateTemplatesDeleteButton.ClientID;
            this._CertificateTemplatesGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._CertificateTemplatesDeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "CertificateTemplatesGridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseCertificateTemplate_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._CertificateTemplatesGridConfirmAction.AddControlToBody(body1Wrapper);
        }
        #endregion

        #region _CertificateTemplatesDeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _CertificateTemplatesDeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._CertificateTemplatesGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._CertificateTemplatesGrid.Rows[i].FindControl(this._CertificateTemplatesGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.Certificate.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedCertificateTemplate_sHaveBeenDeletedSuccessfully, false);

                    // rebind the grid
                    this._CertificateTemplatesGrid.BindData();
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoCertificateTemplate_sSelectedForDeletion, true);
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this._CertificateTemplatesGrid.BindData();
            }
        }
        #endregion
        #endregion

        #region Object Certificates
        #region _BuildObjectCertificatesGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildObjectCertificatesGrid()
        {
            this._ObjectCertificatesGrid.ID = "ObjectCertificatesGrid";
            this._ObjectCertificatesGrid.StoredProcedure = Library.Certificate.ObjectCertificatesGridProcedure;
            this._ObjectCertificatesGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._ObjectCertificatesGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._ObjectCertificatesGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);

            this._ObjectCertificatesGrid.IdentifierField = "idCertificate";
            this._ObjectCertificatesGrid.DefaultSortColumn = "name";
            this._ObjectCertificatesGrid.SearchBoxPlaceholderText = _GlobalResources.SearchCertificates;

            // data key names
            this._ObjectCertificatesGrid.DataKeyNames = new string[] { "idCertificate" };

            // columns
            GridColumn nameObjectCredits = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.Object + ", " + _GlobalResources.Credits + "", null, "name"); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this._ObjectCertificatesGrid.AddColumn(nameObjectCredits);

            // add row data bound event
            this._ObjectCertificatesGrid.RowDataBound += new GridViewRowEventHandler(this._ObjectCertificatesGrid_RowDataBound);
        }
        #endregion

        #region _ObjectCertificatesGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the object certificates grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ObjectCertificatesGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idCertificate = Convert.ToInt32(rowView["idCertificate"]);
                int objectType = Convert.ToInt32(rowView["objectType"]);
                int idObject = Convert.ToInt32(rowView["idObject"]);

                // AVATAR, NAME, CREDITS

                string name = Server.HtmlEncode(rowView["name"].ToString());
                int credits = Convert.ToInt32(rowView["credits"]);

                // avatar                
                Image avatarImage = new Image();
                avatarImage.CssClass = "GridAvatarImage";
                avatarImage.AlternateText = name;

                switch (objectType)
                {
                    case 1:     // course
                        avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
                        break;
                    case 2:     // learning path
                        avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
                        break;
                    case 3:     // certification
                        avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
                        break;
                }   

                e.Row.Cells[1].Controls.Add(avatarImage);

                // name
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                
                // name label link
                HyperLink nameLink = new HyperLink();
                nameLink.Text = name;

                switch (objectType)
                {
                    case 1:     // course
                        nameLink.NavigateUrl = "~/administrator/courses/certificates/Modify.aspx?cid=" + idObject.ToString() + "&id=" + idCertificate.ToString();
                        break;
                    case 2:     // learning path
                        nameLink.NavigateUrl = "~/administrator/learningpaths/certificates/Modify.aspx?lpid=" + idObject.ToString() + "&id=" + idCertificate.ToString();
                        break;
                    case 3:     // certification
                        nameLink.NavigateUrl = "~/administrator/certifications/certificates/Modify.aspx?cid=" + idObject.ToString() + "&id=" + idCertificate.ToString();
                        break;
                }

                nameLabel.Controls.Add(nameLink);
                e.Row.Cells[1].Controls.Add(nameLabel);

                // credits
                Label creditsLabel = new Label();
                creditsLabel.CssClass = "GridSecondaryLine";
                creditsLabel.Text = credits.ToString() + " " + ((credits != 1) ? _GlobalResources.Credits : _GlobalResources.Credit);
                e.Row.Cells[1].Controls.Add(creditsLabel);
            }
        }
        #endregion

        #region _BuildObjectCertificatesGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildObjectCertificatesGridActionsPanel()
        {
            this._ObjectCertificatesGridActionsPanel.ID = "ObjectCertificatesGridActionsPanel";
            this._ObjectCertificatesGridActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._ObjectCertificatesDeleteButton.ID = "ObjectCertificatesGridDeleteButton";
            this._ObjectCertificatesDeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "ObjectCertificatesGridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._ObjectCertificatesDeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedCertificateTemplate_s;
            this._ObjectCertificatesDeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this._ObjectCertificatesGridActionsPanel.Controls.Add(this._ObjectCertificatesDeleteButton);
        }
        #endregion

        #region _BuildObjectCertificatesGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildObjectCertificatesGridActionsModal()
        {
            this._ObjectCertificatesGridConfirmAction.ID = "ObjectCertificatesGridConfirmAction";

            // set modal properties
            this._ObjectCertificatesGridConfirmAction.Type = ModalPopupType.Confirm;
            this._ObjectCertificatesGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this._ObjectCertificatesGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._ObjectCertificatesGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedCertificateTemplate_s;
            this._ObjectCertificatesGridConfirmAction.TargetControlID = this._ObjectCertificatesDeleteButton.ClientID;
            this._ObjectCertificatesGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._ObjectCertificatesDeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ObjectCertificatesGridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseCertificateTemplate_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._ObjectCertificatesGridConfirmAction.AddControlToBody(body1Wrapper);
        }
        #endregion

        #region _ObjectCertificatesDeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _ObjectCertificatesDeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._ObjectCertificatesGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._ObjectCertificatesGrid.Rows[i].FindControl(this._ObjectCertificatesGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.Certificate.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedCertificateTemplate_sHaveBeenDeletedSuccessfully, false);

                    // rebind the grid
                    this._ObjectCertificatesGrid.BindData();
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoCertificateTemplate_sSelectedForDeletion, true);
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this._ObjectCertificatesGrid.BindData();
            }
        }
        #endregion
        #endregion
    }
}
