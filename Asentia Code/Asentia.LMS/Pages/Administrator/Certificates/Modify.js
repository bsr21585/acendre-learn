﻿var id = 0;
var currentItemX = 0;
var currentItemY = 0;
var init = true;
var certificateElementsAdded = false;
//when save button is clicked then offset of the draggable elements will set according to 
//the full page height and the draggable elements goes down below the feedback panel
//so we are decreasing the top position as feedbackPanelHeight of the draggable elements.
var formatterHeight = 23;
var feedbackPanelHeight = 84;

var draggableItemHeight = 18; //Height minus border width

var certificateImageDefaultWidth = 800;
var certificateImageDefaultHeight = 600;

$(function () {
    init = false;
    Sys.Application.add_load(initialize);
});

$(window).load(function () {
    init = true;
    initialize();
});

function initialize() {
    if (!init) {
        return;
    }

    var userAgent = navigator.userAgent.toLowerCase();
    var isBrowserChrome = /chrome/.test(userAgent);
    if (!isBrowserChrome) {
        $("body").draggable({
            drag: function (event, ui) {
                return false;
            }
        });
    }

}

//Changes the certificate background image.
var newWidth;
var newHeight;
function changeBackgroundImage(uploadedFilePath) {
    
    $('#CertificateObject').css("background-image", "url('" + uploadedFilePath + "')");
    // iterate through all certificate background containers, there is one for each language
    $(".CertificateBackgroundContainer").each(function () {
        // if the certificate background is visible, populate that language's background image
        if ($(this).is(":visible")) {
            var language = this.id.substring(31);
            $('#CertificateBackgroundUploadFilepath_' + language).val(uploadedFilePath.substring(15));
            $(this).css("background-image", "url('" + uploadedFilePath + "')");
            $(this).css("filter", "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + uploadedFilePath + "',sizingMethod='scale')");
            var imageSrc = uploadedFilePath;
            var img = new Image();
            var isPortrait;
            img.src = imageSrc;
            img.onload = function () {
                isPortrait = (img.naturalWidth < img.naturalHeight) ? true : false;
                newWidth = (isPortrait) ? 600 : 800;
                newHeight = (isPortrait) ? 800 : 600;
                $(this).css("width", newWidth + "px");
                $(this).css("height", newHeight + "px");
                certificateImageDefaultWidth = newWidth;
                certificateImageDefaultHeight = newHeight;
            }
            return false;
        }
    });
}

//function to return the proportionate height and width 
function imageResize(imageWidth, imageHeight) {
    newWidth = imageWidth;
    newHeight = imageHeight;

    if (imageWidth != certificateImageDefaultWidth) {
        newWidth = certificateImageDefaultWidth;
        var percentChanged = (newWidth / imageWidth) * 100;
        newHeight = parseInt((imageHeight * percentChanged) / 100);
    }
}


// function to change the background image language
function changeBackgroundImageLanguage(language) {
    $('.CertificateBackgroundContainer').hide();
    if (language == '_DEFAULTLANGUAGE_') {
        $('#CertificateBackgroundContainer_en-US').show();
        $('#CertificateObject').css('background-image', $('#CertificateBackgroundContainer_en-US').css('background-image'));
    } else {
        $('#CertificateBackgroundContainer_' + language).show();
        $('#CertificateObject').css('background-image', $('#CertificateBackgroundContainer_' + language).css('background-image'));
    }
    
}


// function to reset uploader properties
function resetUploader() {
    $("#BackgroundImageUploader_CompletedPanel").html("");
    $("#BackgroundImageUploader_ErrorContainer").html("");
    $("#BackgroundImageUploader_UploadControl input").attr("style", "");
    $("#BackgroundImageUploader_UploadHiddenFieldOriginalFileName").val("");
    $("#BackgroundImageUploader_UploadHiddenField").val("");
    $("#BackgroundImageUploader_UploadHiddenFieldFileSize").val("");

    $('#PageFeedbackContainer').hide();
}
