﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.CouponCodes
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CouponCodeFormContentWrapperContainer;
        public Panel CouponCodeWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel CouponGridUpdatePanel;
        public Grid CouponGrid;
        public Panel ActionsPanel;
        public Panel GracePeriodPanel;
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup GridConfirmAction;
        #endregion

        #region Private Properties
        private EcommerceSettings _EcommerceSettings;

        private TextBox _GracePeriodDays;
        private Button _GracePeriodSaveButton;
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get e-commerce settings and check to ensure ecommerce is set on the portal
            this._EcommerceSettings = new EcommerceSettings();

            if (!this._EcommerceSettings.IsEcommerceSet)
            { Response.Redirect("/"); }

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_CouponCodes))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/couponcodes/Default.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CouponCodeFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CouponCodeWrapperContainer.CssClass = "FormContentContainer";

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGracePeriodPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {                
                // bind data grid
                this.CouponGrid.BindData();
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string configurationImagePath;
            string pageTitle;

            breadCrumbPageTitle = _GlobalResources.CouponCodes;

            configurationImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COUPONCODE, ImageFiles.EXT_PNG);

            pageTitle = _GlobalResources.CouponCodes;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, pageTitle, configurationImagePath);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD COUPON CODE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddCouponCodeLink",
                                                null,
                                                "Modify.aspx",
                                                null,
                                                _GlobalResources.NewCouponCode,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_COUPONCODE, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGracePeriodPanel
        /// <summary>
        /// Builds grace period panel.
        /// </summary>
        private void _BuildGracePeriodPanel()
        {
            this.GracePeriodPanel.Controls.Clear();

            this.GracePeriodPanel.CssClass = "CouponGracePeriodPanel";

            // grace period
            List<Control> gracePeriodControls = new List<Control>();

            // information
            Panel gracePeriodInformationPanel = new Panel();
            this.FormatFormInformationPanel(gracePeriodInformationPanel, _GlobalResources.LeavingThisValueBlankOrSettingItTo0WillReClaimCouponCodes, false);
            gracePeriodControls.Add(gracePeriodInformationPanel);

            // control label
            HtmlGenericControl gracePeriodControlLabel = new HtmlGenericControl();
            gracePeriodControlLabel.InnerHtml = _GlobalResources.CouponCodesWillBeReClaimedByTheSystemUnlessThePurchaseIsCompletedWithin + " ";
            gracePeriodControls.Add(gracePeriodControlLabel);

            // control
            this._GracePeriodDays = new TextBox();
            this._GracePeriodDays.ID = "GracePeriod_Field";
            this._GracePeriodDays.CssClass = "InputXShort";
            this._GracePeriodDays.MaxLength = 3;
            gracePeriodControls.Add(this._GracePeriodDays);

            // control days label
            HtmlGenericControl gracePeriodDaysLabel = new HtmlGenericControl();
            gracePeriodDaysLabel.InnerHtml = Asentia.Common._GlobalResources.day_s_lower;
            gracePeriodControls.Add(gracePeriodDaysLabel);

            this.GracePeriodPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("GracePeriod",
                                                                                       _GlobalResources.GracePeriod,
                                                                                       gracePeriodControls,
                                                                                       false,
                                                                                       true));        

            // save button
            this._GracePeriodSaveButton = new Button();
            this._GracePeriodSaveButton.ID = "GracePeriodSaveButton";
            this._GracePeriodSaveButton.CssClass = "Button ActionButton SaveButton";
            this._GracePeriodSaveButton.Text = _GlobalResources.Save;
            this._GracePeriodSaveButton.Command += new CommandEventHandler(this._GracePeriodSaveButton_Command);
            this.GracePeriodPanel.Controls.Add(this._GracePeriodSaveButton);

            // load the grace period
            this._GracePeriodDays.Text = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_COUPONCODE_GRACEPERIOD);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.CouponGrid.StoredProcedure = Library.CouponCode.GridProcedure;
            this.CouponGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.CouponGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.CouponGrid.IdentifierField = "idCouponCode";
            this.CouponGrid.DefaultSortColumn = "Code";

            // data key names
            this.CouponGrid.DataKeyNames = new string[] { "idCouponCode", "discountValue" };

            // columns
            GridColumn couponCode = new GridColumn(_GlobalResources.Code, "Code", "Code");
            
            GridColumn discountValue = new GridColumn(_GlobalResources.DiscountValue, "discountType");
            discountValue.AddProperty(new GridColumnProperty(Convert.ToInt32(DiscountType.Free).ToString(), _GlobalResources.Free));
            discountValue.AddProperty(new GridColumnProperty(Convert.ToInt32(DiscountType.NewPrice).ToString(), String.Format(_GlobalResources.FixedPriceX, this._EcommerceSettings.CurrencySymbol, "##discountValue##")));
            discountValue.AddProperty(new GridColumnProperty(Convert.ToInt32(DiscountType.AmountOff).ToString(), String.Format(_GlobalResources.XAmountOff, this._EcommerceSettings.CurrencySymbol, "##discountValue##")));
            discountValue.AddProperty(new GridColumnProperty(Convert.ToInt32(DiscountType.PercentOff).ToString(), String.Format(_GlobalResources.XPercentOff, "##discountValue##")));

            GridColumn startDate = new GridColumn(_GlobalResources.StartDate, "dtStart", "dtStart");
            GridColumn endDate = new GridColumn(_GlobalResources.EndDate, "dtEnd", "dtEnd");
            GridColumn usesAllowed = new GridColumn(_GlobalResources.UsesAllowed, "usesAllowed", "usesAllowed");

            GridColumn usesRemaining = new GridColumn(_GlobalResources.UsesRemaining, "usesRemaining", "usesRemaining");
            GridColumn usesPending = new GridColumn(_GlobalResources.UsesPending, "usesPending", "usesPending");

            GridColumn modify = new GridColumn(_GlobalResources.Modify, "isModifyOn", true);
            modify.AddProperty(new GridColumnProperty("True", "<a href=\"Modify.aspx?id=##idCouponCode##\">"
                                                                + "<img class=\"SmallIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.Modify + "\" />"
                                                                + "</a>"));
            modify.AddProperty(new GridColumnProperty("False", "<img class=\"SmallIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"" + _GlobalResources.ModifyDisabled + "\" />"));

            // add columns to data grid
            this.CouponGrid.AddColumn(couponCode);
            this.CouponGrid.AddColumn(discountValue);
            this.CouponGrid.AddColumn(startDate);
            this.CouponGrid.AddColumn(endDate);
            this.CouponGrid.AddColumn(usesAllowed);
            this.CouponGrid.AddColumn(usesRemaining);
            this.CouponGrid.AddColumn(usesPending);

            this.CouponGrid.AddColumn(modify);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedCouponCode_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this.DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedCouponCode_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseCouponCode_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.CouponGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.CouponGrid.Rows[i].FindControl(this.CouponGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.CouponCode.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedback(_GlobalResources.TheSelectedCouponCode_sHaveBeenDeletedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.CouponGrid.BindData();
            }
        }
        #endregion

        #region _GracePeriodSaveButton_Command
        /// <summary>
        /// Handles the "Save" button click for grace period.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _GracePeriodSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the grace period textbox value
                if (!String.IsNullOrWhiteSpace(this._GracePeriodDays.Text))
                {
                    int number;

                    if (!(int.TryParse(this._GracePeriodDays.Text, out number)))
                    {
                        this.ApplyErrorMessageToFieldErrorPanel(this.GracePeriodPanel, "GracePeriod", _GlobalResources.GracePeriod + " " + _GlobalResources.IsInvalid);
                        throw new AsentiaException();
                    }
                }

                // save the site param
                DataTable siteParams = new DataTable();
                siteParams.Columns.Add("key");
                siteParams.Columns.Add("value");

                DataRow rowGracePeriod = siteParams.NewRow();
                rowGracePeriod["key"] = SiteParamConstants.ECOMMERCE_COUPONCODE_GRACEPERIOD;
                rowGracePeriod["value"] = this._GracePeriodDays.Text;
                siteParams.Rows.Add(rowGracePeriod);

                AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite, siteParams);

                // re-load the global site object
                AsentiaSessionState.GlobalSiteObject = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite);

                // rebuild the grace period panel
                this._BuildGracePeriodPanel();

                // display the saved feedback
                this.DisplayFeedback(_GlobalResources.CouponCodeGracePeriodHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion
    }
}
