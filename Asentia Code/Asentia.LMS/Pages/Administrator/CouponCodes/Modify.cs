﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Administrator.CouponCodes
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CouponCodeFormContentWrapperContainer;
        public Panel CouponCodePropertiesWrapperContainer;
        public Panel CouponCodePropertiesInstructionsPanel;
        public Panel CouponCodePropertiesFeedbackContainer;
        public Panel CouponCodePropertiesContainer;        
        public Panel CouponCodePropertiesActionsPanel;
        public Panel CouponCodePropertiesTabPanelsContainer;
        #endregion

        #region Private Properties
        private CouponCode _CouponCodeObject;
        private EcommerceSettings _EcommerceSettings;

        private DataTable _CouponCodeCatalogs;
        private DataTable _EligibleCatalogsForSelectList;
        private DataTable _CouponCodeCourses;
        private DataTable _EligibleCoursesForSelectList;
        private DataTable _CouponCodeLearningPaths;
        private DataTable _EligibleLearningPathsForSelectList;
        private DataTable _CouponCodeInstructorLedTraining;
        private DataTable _EligibleInstructorLedTrainingForSelectList;
        
        private TextBox _CouponCode;
        private TextBox _CouponUsesAllowed;
        private TextBox _DiscountValue;
        private TextBox _CouponCodeComment;

        private CheckBox _SingleUsePerUser;

        private DatePicker _CouponLifeSpanDtStart;
        private DatePicker _CouponLifeSpanDtEnd;        

        private Button _CouponCodePropertiesSaveButton;
        private Button _CouponCodePropertiesCancelButton;

        private HiddenField _SelectedCatalogs;
        private HiddenField _SelectedCourses;
        private HiddenField _SelectedLearningPaths;
        private HiddenField _SelectedInstructorLedTraining;

        private Panel _CouponCodeCatalogsListContainer;
        private Panel _CouponCodeCoursesListContainer;
        private Panel _CouponCodeLearningPathsListContainer;
        private Panel _CouponCodeInstructorLedTrainingListContainer;

        private ModalPopup _SelectCatalogsForCouponCode;
        private ModalPopup _SelectCoursesForCouponCode;
        private ModalPopup _SelectLearningPathsForCouponCode;        
        private ModalPopup _SelectInstructorLedTrainingForCouponCode;

        private DynamicListBox _SelectEligibleCatalogs;
        private DynamicListBox _SelectEligibleCourses;        
        private DynamicListBox _SelectEligibleLearningPaths;
        private DynamicListBox _SelectEligibleInstructorLedTraining;

        private RadioButtonList _CatalogIsAny;
        private RadioButtonList _CourseIsAny;
        private RadioButtonList _LearningPathIsAny;
        private RadioButtonList _InstructorLedTrainingIsAny;
        private RadioButtonList _DiscountTypeList;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        /// 
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.CouponCodes.Modify.js");

            // build global JS variables for Social Media elements
            StringBuilder smGlobalJS = new StringBuilder();
            smGlobalJS.AppendLine("DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            csm.RegisterClientScriptBlock(typeof(Modify), "GlobalJS", smGlobalJS.ToString(), true);
        }
        #endregion

        #region Page Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get e-commerce settings and check to ensure ecommerce is set on the portal
            this._EcommerceSettings = new EcommerceSettings();

            if (!this._EcommerceSettings.IsEcommerceSet)
            { Response.Redirect("/"); }

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_CouponCodes))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/couponcodes/Modify.css");

            // get the coupon code object
            this._GetCouponCodeObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();            

            // build the controls for the page
            this._BuildControls();            
        }
        #endregion        

        #region _GetCouponCodeObject
        /// <summary>
        /// Gets a coupon code object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCouponCodeObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._CouponCodeObject = new CouponCode(id); }
                }
                catch
                { Response.Redirect("~/administrator/couponcodes"); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds page controls
        /// </summary>
        private void _BuildControls()
        {
            this.CouponCodeFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CouponCodePropertiesWrapperContainer.CssClass = "FormContentContainer";            

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // build the coupon code properties form
            this._BuildCouponCodePropertiesForm();

            // build the coupon code properties form actions panel
            this._BuildCouponCodePropertiesActionsPanel();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string courseImagePath;
            string pageTitle;

            if (this._CouponCodeObject != null)
            {
                breadCrumbPageTitle = this._CouponCodeObject.Code;
                courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COUPONCODE, ImageFiles.EXT_PNG);
                pageTitle = this._CouponCodeObject.Code;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewCouponCode;
                courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COUPONCODE, ImageFiles.EXT_PNG);
                pageTitle = _GlobalResources.NewCouponCode;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.CouponCodes, "/administrator/couponcodes"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, pageTitle, courseImagePath);
        }
        #endregion

        #region _BuildCouponCodePropertiesForm
        /// <summary>
        /// Builds the coupon code properties form.
        /// </summary>
        private void _BuildCouponCodePropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.CouponCodePropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisCouponCode, true);

            // clear controls from container
            this.CouponCodePropertiesContainer.Controls.Clear();

            // build the coupon code properties form tabs
            this._BuildCouponCodePropertiesFormTabs();

            this.CouponCodePropertiesTabPanelsContainer = new Panel();
            this.CouponCodePropertiesTabPanelsContainer.ID = "CouponCodeProperties_TabPanelsContainer";
            this.CouponCodePropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.CouponCodePropertiesContainer.Controls.Add(this.CouponCodePropertiesTabPanelsContainer);

            // build the "properties" panel of the course properties form
            this._BuildCouponCodeFormPropertiesPanel();

            // build the "catalogs" panel of the coupon codes properties form
            this._BuildCouponCodePropertiesFormCatalogsPanel();

            // build the "Courses" panel of the coupon codes properties form
            this._BuildCouponCodePropertiesFormCoursesPanel();

            // build the "Learning Paths" panel of the coupon codes properties form
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {  
                this._BuildCouponCodePropertiesFormLearningPathsPanel();
            }

            // build the "Instructor Led Training" panel of the coupon codes properties form
            this._BuildCouponCodePropertiesFormInstructorLedTrainingPanel();

            // populate the form input elements
            this._PopulateCouponCodePropertiesInputElements();
        }
        #endregion

        #region _BuildCouponCodePropertiesFormTabs
        /// <summary>
        /// Builds page tabs
        /// </summary>
        private void _BuildCouponCodePropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("Catalogs", _GlobalResources.Catalogs));
            tabs.Enqueue(new KeyValuePair<string, string>("Courses", _GlobalResources.Courses));

            // check to ensure learning paths are enabled on the portal
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                tabs.Enqueue(new KeyValuePair<string, string>("LearningPaths", _GlobalResources.LearningPaths));
            }

            tabs.Enqueue(new KeyValuePair<string, string>("ILT", _GlobalResources.ILT));

            // build and attach the tabs
            this.CouponCodePropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("CouponCodeProperties", tabs, null, this.Page, null));
        }
        #endregion

        #region _BuildCouponCodePropertiesFormPropertiesPanel
        /// <summary>
        /// Builds coupon code properties panel
        /// </summary>
        private void _BuildCouponCodeFormPropertiesPanel()
        {            
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "CouponCodeProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.CssClass = "TabPanelContainer";
            propertiesPanel.Attributes.Add("style", "display: block;");

            // coupon code field
            this._CouponCode = new TextBox();
            this._CouponCode.ID = "CouponCode_Field";
            this._CouponCode.CssClass = "InputShort";
            this._CouponCode.MaxLength = 10;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CouponCode",
                                                             _GlobalResources.Code,
                                                             this._CouponCode.ID,
                                                             this._CouponCode,
                                                             true,
                                                             true,
                                                             false));

            // coupon uses allowed field
            this._CouponUsesAllowed = new TextBox();
            this._CouponUsesAllowed.ID = "CouponUsesAllowed_Field";
            this._CouponUsesAllowed.CssClass = "InputXShort";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CouponUsesAllowed",
                                                             _GlobalResources.UsesAllowed,
                                                             this._CouponUsesAllowed.ID,
                                                             this._CouponUsesAllowed,
                                                             true,
                                                             true,
                                                             false));

            // single use per user selection field
            this._SingleUsePerUser = new CheckBox();
            this._SingleUsePerUser.ID = "SingleUsePerUser_Field";
            this._SingleUsePerUser.Text = _GlobalResources.AllowUsersToUseThisCouponCodeOnlyOneTime;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("SingleUsePerUser",
                                                             _GlobalResources.SingleUsePerUser,
                                                             this._SingleUsePerUser.ID,
                                                             this._SingleUsePerUser,
                                                             false,
                                                             true,
                                                             false));

            // coupon code lifespan start field
            this._CouponLifeSpanDtStart = new DatePicker("LifeSpanStart_Field", false, false, false);
            this._CouponLifeSpanDtStart.Value = AsentiaSessionState.UserTimezoneCurrentLocalTime;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LifeSpanStart",
                                                             _GlobalResources.LifespanStart,
                                                             this._CouponLifeSpanDtStart.ID,
                                                             this._CouponLifeSpanDtStart,
                                                             true,
                                                             true,
                                                             false));

            // coupon code lifespan end field
            this._CouponLifeSpanDtEnd = new DatePicker("LifeSpanEnd_Field", true, false, false);
            this._CouponLifeSpanDtEnd.NoneCheckboxText = _GlobalResources.CodeIsValidIndefinitely;
            this._CouponLifeSpanDtEnd.NoneCheckBoxChecked = true;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LifeSpanEnd",
                                                             _GlobalResources.LifespanEnd,
                                                             this._CouponLifeSpanDtEnd.ID,
                                                             this._CouponLifeSpanDtEnd,
                                                             true,
                                                             true,
                                                             false));

            // discount type field
            List<Control> discountTypeInputControls = new List<Control>();

            // types
            this._DiscountTypeList = new RadioButtonList();
            this._DiscountTypeList.ID = "DiscountType_Field";

            this._DiscountTypeList.Items.Add(new ListItem(_GlobalResources.Free, Convert.ToInt32(DiscountType.Free).ToString()));
            this._DiscountTypeList.Items.Add(new ListItem(String.Format(_GlobalResources.FixedPriceEGX25, this._EcommerceSettings.CurrencySymbol), Convert.ToInt32(DiscountType.NewPrice).ToString()));
            this._DiscountTypeList.Items.Add(new ListItem(String.Format(_GlobalResources.AmountOffEGX25, this._EcommerceSettings.CurrencySymbol), Convert.ToInt32(DiscountType.AmountOff).ToString()));
            this._DiscountTypeList.Items.Add(new ListItem(_GlobalResources.PercentOffEG30Off, Convert.ToInt32(DiscountType.PercentOff).ToString()));

            this._DiscountTypeList.Attributes.Add("onclick", "DiscountTypeClick();");
            
            discountTypeInputControls.Add(this._DiscountTypeList);

            // extended properties
            Panel discountTypeNewPriceContainer = new Panel();
            discountTypeNewPriceContainer.ID = "DiscountTypeExtendedPropertiesContainer";
            discountTypeNewPriceContainer.CssClass = "DiscountTypeExtendedPropertiesContainer";
            discountTypeInputControls.Add(discountTypeNewPriceContainer);

            // new price
            Label discountTypeNewPriceLabel = new Label();
            discountTypeNewPriceLabel.ID = "DiscountTypeNewPriceLabel";
            discountTypeNewPriceLabel.CssClass = "FormFieldLabelContainer";
            discountTypeNewPriceLabel.Style.Add("display", "none");
            discountTypeNewPriceLabel.Text = _GlobalResources.NewPrice + ": ";
            discountTypeNewPriceContainer.Controls.Add(discountTypeNewPriceLabel);

            // amount off
            Label discountTypeAmountOffLabel = new Label();
            discountTypeAmountOffLabel.ID = "DiscountTypeAmountOffLabel";
            discountTypeAmountOffLabel.CssClass = "FormFieldLabelContainer";
            discountTypeAmountOffLabel.Style.Add("display", "none");
            discountTypeAmountOffLabel.Text = _GlobalResources.AmountOff + ": ";
            discountTypeNewPriceContainer.Controls.Add(discountTypeAmountOffLabel);

            // percent off
            Label discountTypePercentOffLabel = new Label();
            discountTypePercentOffLabel.ID = "DiscountTypePercentOffLabel";
            discountTypePercentOffLabel.CssClass = "FormFieldLabelContainer";
            discountTypePercentOffLabel.Style.Add("display", "none");
            discountTypePercentOffLabel.Text = _GlobalResources.PercentOff + ": ";
            discountTypeNewPriceContainer.Controls.Add(discountTypePercentOffLabel);

            // currency prefix
            Label discountTypeCurrencyPrefix = new Label();
            discountTypeCurrencyPrefix.ID = "DiscountTypeCurrencyPrefix";
            discountTypeCurrencyPrefix.Style.Add("display", "none");
            discountTypeCurrencyPrefix.Text = this._EcommerceSettings.CurrencySymbol + " ";
            discountTypeNewPriceContainer.Controls.Add(discountTypeCurrencyPrefix);

            // value control
            this._DiscountValue = new TextBox();
            this._DiscountValue.ID = "DiscountValue_Field";
            this._DiscountValue.CssClass = "InputXShort";
            this._DiscountValue.MaxLength = 8;
            this._DiscountValue.Style.Add("display", "none");
            discountTypeNewPriceContainer.Controls.Add(this._DiscountValue);

            // currency suffix
            Label discountTypeCurrencySuffix = new Label();
            discountTypeCurrencySuffix.ID = "DiscountTypeCurrencySuffix";
            discountTypeCurrencySuffix.Style.Add("display", "none");
            discountTypeCurrencySuffix.Text = " (" + this._EcommerceSettings.CurrencyCode + ")";
            discountTypeNewPriceContainer.Controls.Add(discountTypeCurrencySuffix);

            // percent off suffix
            Label discountTypePercentOffSuffix = new Label();
            discountTypePercentOffSuffix.ID = "DiscountTypePercentOffSuffix";
            discountTypePercentOffSuffix.Style.Add("display", "none");
            discountTypePercentOffSuffix.Text = "%";
            discountTypeNewPriceContainer.Controls.Add(discountTypePercentOffSuffix);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("DiscountType",
                                                                                 _GlobalResources.DiscountType,
                                                                                 discountTypeInputControls,
                                                                                 true,
                                                                                 true));
                        
            // coupon code comment/notes field
            this._CouponCodeComment = new TextBox();
            this._CouponCodeComment.ID = "CouponCodeComment_Field";
            this._CouponCodeComment.Style.Add("width", "98%");
            this._CouponCodeComment.TextMode = TextBoxMode.MultiLine;
            this._CouponCodeComment.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("CouponCodeComment",
                                                             _GlobalResources.Comments,
                                                             this._CouponCodeComment.ID,
                                                             this._CouponCodeComment,
                                                             false,
                                                             true,
                                                             false));

            // attach panel to container
            this.CouponCodePropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildCouponCodePropertiesFormCatalogsPanel
        /// <summary>
        ///  Builds the coupon code properties form fields under catalogs tab.
        /// </summary>
        private void _BuildCouponCodePropertiesFormCatalogsPanel()
        {
            if (this._CouponCodeObject != null)
            {
                this._CouponCodeCatalogs = this._CouponCodeObject.GetCatalogs(null);
                this._EligibleCatalogsForSelectList = Asentia.LMS.Library.Catalog.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, null);
            }
            else
            {
                this._EligibleCatalogsForSelectList = Asentia.LMS.Library.Catalog.IdsAndNamesForCouponCodeSelectList(0, null);
            }

            Panel catalogsPanel = new Panel();
            catalogsPanel.ID = "CouponCodeProperties_" + "Catalogs" + "_TabPanel";
            catalogsPanel.CssClass = "TabPanelContainer";
            catalogsPanel.Attributes.Add("style", "display: none;");

            List<Control> couponCodeCatalogsInputControls = new List<Control>();

            // catalog is any, some, or none field
            this._CatalogIsAny = new RadioButtonList();
            this._CatalogIsAny.ID = "CatalogIsAny_Field";
            this._CatalogIsAny.Items.Add(new ListItem(_GlobalResources.NoCatalog_s, Convert.ToInt32(CouponCodeValidFor.No).ToString()));
            this._CatalogIsAny.Items.Add(new ListItem(_GlobalResources.AllCatalog_s, Convert.ToInt32(CouponCodeValidFor.All).ToString()));
            this._CatalogIsAny.Items.Add(new ListItem(_GlobalResources.Catalog_sListedBelow, Convert.ToInt32(CouponCodeValidFor.ListedBelow).ToString()));
            this._CatalogIsAny.Attributes.Add("onclick", "ObjectOptionTypeClick('Catalog');");
            this._CatalogIsAny.SelectedValue = Convert.ToInt32(CouponCodeValidFor.No).ToString(); // no selected by default
            couponCodeCatalogsInputControls.Add(this._CatalogIsAny);

            // selected catalogs hidden field
            this._SelectedCatalogs = new HiddenField();
            this._SelectedCatalogs.ID = "SelectedCatalogs_Field";
            couponCodeCatalogsInputControls.Add(this._SelectedCatalogs);

            // build a container for the catalogs listing
            this._CouponCodeCatalogsListContainer = new Panel();
            this._CouponCodeCatalogsListContainer.ID = "CouponCodeCatalogsList_Container";
            this._CouponCodeCatalogsListContainer.CssClass = "ItemListingContainer";
            couponCodeCatalogsInputControls.Add(this._CouponCodeCatalogsListContainer);

            Panel couponCodeCatalogsButtonsPanel = new Panel();
            couponCodeCatalogsButtonsPanel.ID = "CouponCodeCatalogs_ButtonsPanel";

            // select catalogs button

            // link
            Image selectCatalogsImageForLink = new Image();
            selectCatalogsImageForLink.ID = "LaunchSelectCatalogsModalImage";
            selectCatalogsImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG, ImageFiles.EXT_PNG);
            selectCatalogsImageForLink.CssClass = "MediumIcon";

            Localize selectCatalogsTextForLink = new Localize();
            selectCatalogsTextForLink.Text = _GlobalResources.SelectCatalog_s;

            LinkButton selectCatalogsLink = new LinkButton();
            selectCatalogsLink.ID = "LaunchSelectCatalogsModal";
            selectCatalogsLink.CssClass = "ImageLink";
            selectCatalogsLink.Controls.Add(selectCatalogsImageForLink);
            selectCatalogsLink.Controls.Add(selectCatalogsTextForLink);
            couponCodeCatalogsButtonsPanel.Controls.Add(selectCatalogsLink);            

            // attach the buttons panel to the container
            couponCodeCatalogsInputControls.Add(couponCodeCatalogsButtonsPanel);

            // build modals for adding and removing experts to/from the course
            this._BuildSelectCatalogsModal(selectCatalogsLink.ID);

            catalogsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CouponCodeCatalogs",
                                                                                      _GlobalResources.ThisCouponCodeIsValidFor,
                                                                                      couponCodeCatalogsInputControls,
                                                                                      false,
                                                                                      true));

            // attach panel to container
            this.CouponCodePropertiesTabPanelsContainer.Controls.Add(catalogsPanel);
        }
        #endregion

        #region _BuildSelectCatalogsModal
        /// <summary>
        /// Builds the modal for selecting catalogs to add to the coupon code.
        /// </summary>
        private void _BuildSelectCatalogsModal(string targetControlId)
        {
            // set modal properties
            this._SelectCatalogsForCouponCode = new ModalPopup("SelectCatalogsForCouponCodeModal");
            this._SelectCatalogsForCouponCode.Type = ModalPopupType.Form;
            this._SelectCatalogsForCouponCode.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG, ImageFiles.EXT_PNG);
            this._SelectCatalogsForCouponCode.HeaderIconAlt = _GlobalResources.SelectCatalog_s;
            this._SelectCatalogsForCouponCode.HeaderText = _GlobalResources.SelectCatalog_s;
            this._SelectCatalogsForCouponCode.TargetControlID = targetControlId;
            this._SelectCatalogsForCouponCode.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCatalogsForCouponCode.SubmitButtonCustomText = _GlobalResources.AddCatalog_s;
            this._SelectCatalogsForCouponCode.SubmitButton.OnClientClick = "javascript:AddCatalogsToCouponCode(); return false;";
            this._SelectCatalogsForCouponCode.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCatalogsForCouponCode.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectCatalogsForCouponCode.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleCatalogs = new DynamicListBox("SelectEligibleCatalogs");
            this._SelectEligibleCatalogs.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectEligibleCatalogs.SearchButton.Command += new CommandEventHandler(this._SearchSelectCatalogsButton_Command);
            this._SelectEligibleCatalogs.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectCatalogsButton_Command);
            this._SelectEligibleCatalogs.ListBoxControl.DataSource = this._EligibleCatalogsForSelectList;
            this._SelectEligibleCatalogs.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCatalogs.ListBoxControl.DataValueField = "idCatalog";
            this._SelectEligibleCatalogs.ListBoxControl.DataBind();

            // add controls to body
            this._SelectCatalogsForCouponCode.AddControlToBody(this._SelectEligibleCatalogs);

            // add modal to container
            this.CouponCodePropertiesTabPanelsContainer.Controls.Add(this._SelectCatalogsForCouponCode);
        }
        #endregion

        #region _SearchSelectCatalogsButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Catalog(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectCatalogsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCatalogsForCouponCode.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleCatalogs.ListBoxControl.Items.Clear();

            // do the search
            if (this._CouponCodeObject != null)
            { this._EligibleCatalogsForSelectList = Asentia.LMS.Library.Catalog.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, this._SelectEligibleCatalogs.SearchTextBox.Text); }
            else
            { this._EligibleCatalogsForSelectList = Asentia.LMS.Library.Catalog.IdsAndNamesForCouponCodeSelectList(0, this._SelectEligibleCatalogs.SearchTextBox.Text); }

            this._SelectEligibleCatalogs.ListBoxControl.DataSource = this._EligibleCatalogsForSelectList;
            this._SelectEligibleCatalogs.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCatalogs.ListBoxControl.DataValueField = "idCatalog";
            this._SelectEligibleCatalogs.ListBoxControl.DataBind();

            // if no records available then disable the list
            if (this._SelectEligibleCatalogs.ListBoxControl.Items.Count == 0)
            {
                this._SelectCatalogsForCouponCode.SubmitButton.Enabled = false;
                this._SelectCatalogsForCouponCode.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectCatalogsForCouponCode.SubmitButton.Enabled = true;
                this._SelectCatalogsForCouponCode.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _ClearSearchSelectCatalogsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Catalog(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectCatalogsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCatalogsForCouponCode.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleCatalogs.ListBoxControl.Items.Clear();
            this._SelectEligibleCatalogs.SearchTextBox.Text = "";

            // clear the search
            if (this._CouponCodeObject != null)
            { this._EligibleCatalogsForSelectList = Asentia.LMS.Library.Catalog.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, null); }
            else
            { this._EligibleCatalogsForSelectList = Asentia.LMS.Library.Catalog.IdsAndNamesForCouponCodeSelectList(0, null); }

            this._SelectEligibleCatalogs.ListBoxControl.DataSource = this._EligibleCatalogsForSelectList;
            this._SelectEligibleCatalogs.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCatalogs.ListBoxControl.DataValueField = "idCatalog";
            this._SelectEligibleCatalogs.ListBoxControl.DataBind();

            // if records available then enable the list
            if (this._SelectEligibleCatalogs.ListBoxControl.Items.Count > 0)
            {
                this._SelectCatalogsForCouponCode.SubmitButton.Enabled = true;
                this._SelectCatalogsForCouponCode.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectCatalogsForCouponCode.SubmitButton.Enabled = false;
                this._SelectCatalogsForCouponCode.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _BuildCouponCodePropertiesFormCoursesPanel
        /// <summary>
        ///  Builds the coupon code properties form fields under courses tab.
        /// </summary>
        private void _BuildCouponCodePropertiesFormCoursesPanel()
        {
            if (this._CouponCodeObject != null)
            {
                this._CouponCodeCourses = this._CouponCodeObject.GetCourses(null);
                this._EligibleCoursesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, null);
            }
            else
            {
                this._EligibleCoursesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCouponCodeSelectList(0, null);
            }

            Panel coursesPanel = new Panel();
            coursesPanel.ID = "CouponCodeProperties_" + "Courses" + "_TabPanel";
            coursesPanel.CssClass = "TabPanelContainer";
            coursesPanel.Attributes.Add("style", "display: none;");

            List<Control> couponCodeCoursesInputControls = new List<Control>();

            // course is any, some, or none field
            this._CourseIsAny = new RadioButtonList();
            this._CourseIsAny.ID = "CourseIsAny_Field";
            this._CourseIsAny.Items.Add(new ListItem(_GlobalResources.NoCourse_s, Convert.ToInt32(CouponCodeValidFor.No).ToString()));
            this._CourseIsAny.Items.Add(new ListItem(_GlobalResources.AllCourse_s, Convert.ToInt32(CouponCodeValidFor.All).ToString()));
            this._CourseIsAny.Items.Add(new ListItem(_GlobalResources.Course_sListedBelow, Convert.ToInt32(CouponCodeValidFor.ListedBelow).ToString()));
            this._CourseIsAny.Attributes.Add("onclick", "ObjectOptionTypeClick('Course');");
            this._CourseIsAny.SelectedValue = Convert.ToInt32(CouponCodeValidFor.No).ToString(); // no selected by default
            couponCodeCoursesInputControls.Add(this._CourseIsAny);

            // selected courses hidden field
            this._SelectedCourses = new HiddenField();
            this._SelectedCourses.ID = "SelectedCourses_Field";
            couponCodeCoursesInputControls.Add(this._SelectedCourses);

            // build a container for the courses listing
            this._CouponCodeCoursesListContainer = new Panel();
            this._CouponCodeCoursesListContainer.ID = "CouponCodeCoursesList_Container";
            this._CouponCodeCoursesListContainer.CssClass = "ItemListingContainer";
            couponCodeCoursesInputControls.Add(this._CouponCodeCoursesListContainer);

            Panel couponCodeCoursesButtonsPanel = new Panel();
            couponCodeCoursesButtonsPanel.ID = "CouponCodeCourses_ButtonsPanel";

            // select courses button

            // link
            Image selectCoursesImageForLink = new Image();
            selectCoursesImageForLink.ID = "LaunchSelectCoursesModalImage";
            selectCoursesImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            selectCoursesImageForLink.CssClass = "MediumIcon";

            Localize selectCoursesTextForLink = new Localize();
            selectCoursesTextForLink.Text = _GlobalResources.SelectCourse_s;

            LinkButton selectCoursesLink = new LinkButton();
            selectCoursesLink.ID = "LaunchSelectCoursesModal";
            selectCoursesLink.CssClass = "ImageLink";
            selectCoursesLink.Controls.Add(selectCoursesImageForLink);
            selectCoursesLink.Controls.Add(selectCoursesTextForLink);
            couponCodeCoursesButtonsPanel.Controls.Add(selectCoursesLink);

            // attach the buttons panel to the container
            couponCodeCoursesInputControls.Add(couponCodeCoursesButtonsPanel);

            // build modals for adding and removing experts to/from the course
            this._BuildSelectCoursesModal(selectCoursesLink.ID);

            coursesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CouponCodeCourses",
                                                                                     _GlobalResources.ThisCouponCodeIsValidFor,
                                                                                     couponCodeCoursesInputControls,
                                                                                     false,
                                                                                     true));

            // attach panel to container
            this.CouponCodePropertiesTabPanelsContainer.Controls.Add(coursesPanel);
        }
        #endregion

        #region _BuildSelectCoursesModal
        /// <summary>
        /// Builds the modal for selecting courses to add to the coupon code.
        /// </summary>
        private void _BuildSelectCoursesModal(string targetControlId)
        {
            // set modal properties
            this._SelectCoursesForCouponCode = new ModalPopup("SelectCoursesForCouponCodeModal");
            this._SelectCoursesForCouponCode.Type = ModalPopupType.Form;
            this._SelectCoursesForCouponCode.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            this._SelectCoursesForCouponCode.HeaderIconAlt = _GlobalResources.SelectCourse_s;
            this._SelectCoursesForCouponCode.HeaderText = _GlobalResources.SelectCourse_s;
            this._SelectCoursesForCouponCode.TargetControlID = targetControlId;
            this._SelectCoursesForCouponCode.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCoursesForCouponCode.SubmitButtonCustomText = _GlobalResources.AddCourse_s;
            this._SelectCoursesForCouponCode.SubmitButton.OnClientClick = "javascript:AddCoursesToCouponCode(); return false;";
            this._SelectCoursesForCouponCode.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCoursesForCouponCode.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectCoursesForCouponCode.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleCourses = new DynamicListBox("SelectEligibleCourses");
            this._SelectEligibleCourses.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectEligibleCourses.SearchButton.Command += new CommandEventHandler(this._SearchSelectCoursesButton_Command);
            this._SelectEligibleCourses.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectCoursesButton_Command);
            this._SelectEligibleCourses.ListBoxControl.DataSource = this._EligibleCoursesForSelectList;
            this._SelectEligibleCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleCourses.ListBoxControl.DataBind();

            // add controls to body
            this._SelectCoursesForCouponCode.AddControlToBody(this._SelectEligibleCourses);

            // add modal to container
            this.CouponCodePropertiesTabPanelsContainer.Controls.Add(this._SelectCoursesForCouponCode);
        }
        #endregion

        #region _SearchSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Course(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCoursesForCouponCode.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleCourses.ListBoxControl.Items.Clear();

            // do the search
            if (this._CouponCodeObject != null)
            { this._EligibleCoursesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, this._SelectEligibleCourses.SearchTextBox.Text); }
            else
            { this._EligibleCoursesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCouponCodeSelectList(0, this._SelectEligibleCourses.SearchTextBox.Text); }

            this._SelectEligibleCourses.ListBoxControl.DataSource = this._EligibleCoursesForSelectList;
            this._SelectEligibleCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleCourses.ListBoxControl.DataBind();

            // if no records available then disable the list
            if (this._SelectEligibleCourses.ListBoxControl.Items.Count == 0)
            {
                this._SelectCoursesForCouponCode.SubmitButton.Enabled = false;
                this._SelectCoursesForCouponCode.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectCoursesForCouponCode.SubmitButton.Enabled = true;
                this._SelectCoursesForCouponCode.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _ClearSearchSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Course(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCoursesForCouponCode.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleCourses.ListBoxControl.Items.Clear();
            this._SelectEligibleCourses.SearchTextBox.Text = "";

            // clear the search
            if (this._CouponCodeObject != null)
            { this._EligibleCoursesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, null); }
            else
            { this._EligibleCoursesForSelectList = Asentia.LMS.Library.Course.IdsAndNamesForCouponCodeSelectList(0, null); }

            this._SelectEligibleCourses.ListBoxControl.DataSource = this._EligibleCoursesForSelectList;
            this._SelectEligibleCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleCourses.ListBoxControl.DataBind();

            // if records available then enable the list
            if (this._SelectEligibleCourses.ListBoxControl.Items.Count > 0)
            {
                this._SelectCoursesForCouponCode.SubmitButton.Enabled = true;
                this._SelectCoursesForCouponCode.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectCoursesForCouponCode.SubmitButton.Enabled = false;
                this._SelectCoursesForCouponCode.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _BuildCouponCodePropertiesFormLearningPathsPanel
        /// <summary>
        ///  Builds the coupon code properties form fields under learning paths tab.
        /// </summary>
        private void _BuildCouponCodePropertiesFormLearningPathsPanel()
        {
            if (this._CouponCodeObject != null)
            {
                this._CouponCodeLearningPaths = this._CouponCodeObject.GetLearningPaths(null);
                this._EligibleLearningPathsForSelectList = Asentia.LMS.Library.LearningPath.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, null);
            }
            else
            {
                this._EligibleLearningPathsForSelectList = Asentia.LMS.Library.LearningPath.IdsAndNamesForCouponCodeSelectList(0, null);
            }

            Panel learningPathsPanel = new Panel();
            learningPathsPanel.ID = "CouponCodeProperties_" + "LearningPaths" + "_TabPanel";
            learningPathsPanel.CssClass = "TabPanelContainer";
            learningPathsPanel.Attributes.Add("style", "display: none;");

            List<Control> couponCodeLearningPathsInputControls = new List<Control>();

            // learning path is any, some, or none field
            this._LearningPathIsAny = new RadioButtonList();
            this._LearningPathIsAny.ID = "LearningPathIsAny_Field";
            this._LearningPathIsAny.Items.Add(new ListItem(_GlobalResources.NoLearningPath_s, Convert.ToInt32(CouponCodeValidFor.No).ToString()));
            this._LearningPathIsAny.Items.Add(new ListItem(_GlobalResources.AllLearningPath_s, Convert.ToInt32(CouponCodeValidFor.All).ToString()));
            this._LearningPathIsAny.Items.Add(new ListItem(_GlobalResources.LearningPath_sListedBelow, Convert.ToInt32(CouponCodeValidFor.ListedBelow).ToString()));
            this._LearningPathIsAny.Attributes.Add("onclick", "ObjectOptionTypeClick('LearningPath');");
            this._LearningPathIsAny.SelectedValue = Convert.ToInt32(CouponCodeValidFor.No).ToString(); // no selected by default
            couponCodeLearningPathsInputControls.Add(this._LearningPathIsAny);

            // selected learning paths hidden field
            this._SelectedLearningPaths = new HiddenField();
            this._SelectedLearningPaths.ID = "SelectedLearningPaths_Field";
            couponCodeLearningPathsInputControls.Add(this._SelectedLearningPaths);

            // build a container for the learning paths listing
            this._CouponCodeLearningPathsListContainer = new Panel();
            this._CouponCodeLearningPathsListContainer.ID = "CouponCodeLearningPathsList_Container";
            this._CouponCodeLearningPathsListContainer.CssClass = "ItemListingContainer";
            couponCodeLearningPathsInputControls.Add(this._CouponCodeLearningPathsListContainer);

            Panel couponCodeLearningPathsButtonsPanel = new Panel();
            couponCodeLearningPathsButtonsPanel.ID = "CouponCodeLearningPaths_ButtonsPanel";

            // select learning paths button

            // link
            Image selectLearningPathsImageForLink = new Image();
            selectLearningPathsImageForLink.ID = "LaunchSelectLearningPathsModalImage";
            selectLearningPathsImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
            selectLearningPathsImageForLink.CssClass = "MediumIcon";

            Localize selectLearningPathsTextForLink = new Localize();
            selectLearningPathsTextForLink.Text = _GlobalResources.SelectLearningPath_s;

            LinkButton selectLearningPathsLink = new LinkButton();
            selectLearningPathsLink.ID = "LaunchSelectLearningPathsModal";
            selectLearningPathsLink.CssClass = "ImageLink";
            selectLearningPathsLink.Controls.Add(selectLearningPathsImageForLink);
            selectLearningPathsLink.Controls.Add(selectLearningPathsTextForLink);
            couponCodeLearningPathsButtonsPanel.Controls.Add(selectLearningPathsLink);

            // attach the buttons panel to the container
            couponCodeLearningPathsInputControls.Add(couponCodeLearningPathsButtonsPanel);

            // build modals for adding and removing experts to/from the learning path
            this._BuildSelectLearningPathsModal(selectLearningPathsLink.ID);

            learningPathsPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CouponCodeLearningPaths",
                                                                                     _GlobalResources.ThisCouponCodeIsValidFor,
                                                                                     couponCodeLearningPathsInputControls,
                                                                                     false,
                                                                                     true));            

            // attach panel to container
            this.CouponCodePropertiesTabPanelsContainer.Controls.Add(learningPathsPanel);
        }
        #endregion

        #region _BuildSelectLearningPathsModal
        /// <summary>
        /// Builds the modal for selecting learning paths to add to the coupon code.
        /// </summary>
        private void _BuildSelectLearningPathsModal(string targetControlId)
        {
            // set modal properties
            this._SelectLearningPathsForCouponCode = new ModalPopup("SelectLearningPathsForCouponCodeModal");
            this._SelectLearningPathsForCouponCode.Type = ModalPopupType.Form;
            this._SelectLearningPathsForCouponCode.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
            this._SelectLearningPathsForCouponCode.HeaderIconAlt = _GlobalResources.SelectLearningPath_s;
            this._SelectLearningPathsForCouponCode.HeaderText = _GlobalResources.SelectLearningPath_s;
            this._SelectLearningPathsForCouponCode.TargetControlID = targetControlId;
            this._SelectLearningPathsForCouponCode.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectLearningPathsForCouponCode.SubmitButtonCustomText = _GlobalResources.AddLearningPath_s;
            this._SelectLearningPathsForCouponCode.SubmitButton.OnClientClick = "javascript:AddLearningPathsToCouponCode(); return false;";
            this._SelectLearningPathsForCouponCode.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectLearningPathsForCouponCode.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectLearningPathsForCouponCode.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleLearningPaths = new DynamicListBox("SelectEligibleLearningPaths");
            this._SelectEligibleLearningPaths.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectEligibleLearningPaths.SearchButton.Command += new CommandEventHandler(this._SearchSelectLearningPathsButton_Command);
            this._SelectEligibleLearningPaths.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectLearningPathsButton_Command);
            this._SelectEligibleLearningPaths.ListBoxControl.DataSource = this._EligibleLearningPathsForSelectList;
            this._SelectEligibleLearningPaths.ListBoxControl.DataTextField = "title";
            this._SelectEligibleLearningPaths.ListBoxControl.DataValueField = "idLearningPath";
            this._SelectEligibleLearningPaths.ListBoxControl.DataBind();

            // add controls to body
            this._SelectLearningPathsForCouponCode.AddControlToBody(this._SelectEligibleLearningPaths);

            // add modal to container
            this.CouponCodePropertiesTabPanelsContainer.Controls.Add(this._SelectLearningPathsForCouponCode);
        }
        #endregion

        #region _SearchSelectLearningPathsButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Learning Path(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectLearningPathsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectLearningPathsForCouponCode.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleLearningPaths.ListBoxControl.Items.Clear();

            // do the search
            if (this._CouponCodeObject != null)
            { this._EligibleLearningPathsForSelectList = Asentia.LMS.Library.LearningPath.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, this._SelectEligibleLearningPaths.SearchTextBox.Text); }
            else
            { this._EligibleLearningPathsForSelectList = Asentia.LMS.Library.LearningPath.IdsAndNamesForCouponCodeSelectList(0, this._SelectEligibleLearningPaths.SearchTextBox.Text); }

            this._SelectEligibleLearningPaths.ListBoxControl.DataSource = this._EligibleLearningPathsForSelectList;
            this._SelectEligibleLearningPaths.ListBoxControl.DataTextField = "title";
            this._SelectEligibleLearningPaths.ListBoxControl.DataValueField = "idLearningPath";
            this._SelectEligibleLearningPaths.ListBoxControl.DataBind();

            // if no records available then disable the list
            if (this._SelectEligibleLearningPaths.ListBoxControl.Items.Count == 0)
            {
                this._SelectLearningPathsForCouponCode.SubmitButton.Enabled = false;
                this._SelectLearningPathsForCouponCode.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectLearningPathsForCouponCode.SubmitButton.Enabled = true;
                this._SelectLearningPathsForCouponCode.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _ClearSearchSelectLearningPathsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Learning Path(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectLearningPathsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectLearningPathsForCouponCode.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleLearningPaths.ListBoxControl.Items.Clear();
            this._SelectEligibleLearningPaths.SearchTextBox.Text = "";

            // clear the search
            if (this._CouponCodeObject != null)
            { this._EligibleLearningPathsForSelectList = Asentia.LMS.Library.LearningPath.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, null); }
            else
            { this._EligibleLearningPathsForSelectList = Asentia.LMS.Library.LearningPath.IdsAndNamesForCouponCodeSelectList(0, null); }

            this._SelectEligibleLearningPaths.ListBoxControl.DataSource = this._EligibleLearningPathsForSelectList;
            this._SelectEligibleLearningPaths.ListBoxControl.DataTextField = "title";
            this._SelectEligibleLearningPaths.ListBoxControl.DataValueField = "idLearningPath";
            this._SelectEligibleLearningPaths.ListBoxControl.DataBind();

            // if records available then enable the list
            if (this._SelectEligibleLearningPaths.ListBoxControl.Items.Count > 0)
            {
                this._SelectLearningPathsForCouponCode.SubmitButton.Enabled = true;
                this._SelectLearningPathsForCouponCode.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectLearningPathsForCouponCode.SubmitButton.Enabled = false;
                this._SelectLearningPathsForCouponCode.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _BuildCouponCodePropertiesFormInstructorLedTrainingPanel
        /// <summary>
        ///  Builds the coupon code properties form fields under ilt tab.
        /// </summary>
        private void _BuildCouponCodePropertiesFormInstructorLedTrainingPanel()
        {
            if (this._CouponCodeObject != null)
            {
                this._CouponCodeInstructorLedTraining = this._CouponCodeObject.GetInstructorLedTraining(null);
                this._EligibleInstructorLedTrainingForSelectList = Asentia.LMS.Library.StandupTraining.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, null);
            }
            else
            {
                this._EligibleInstructorLedTrainingForSelectList = Asentia.LMS.Library.StandupTraining.IdsAndNamesForCouponCodeSelectList(0, null);
            }

            Panel iltPanel = new Panel();
            iltPanel.ID = "CouponCodeProperties_" + "ILT" + "_TabPanel";
            iltPanel.CssClass = "TabPanelContainer";
            iltPanel.Attributes.Add("style", "display: none;");

            List<Control> couponCodeInstructorLedTrainingInputControls = new List<Control>();

            // ilt is any, some, or none field
            this._InstructorLedTrainingIsAny = new RadioButtonList();
            this._InstructorLedTrainingIsAny.ID = "InstructorLedTrainingIsAny_Field";
            this._InstructorLedTrainingIsAny.Items.Add(new ListItem(_GlobalResources.NoInstructorLedTraining, Convert.ToInt32(CouponCodeValidFor.No).ToString()));
            this._InstructorLedTrainingIsAny.Items.Add(new ListItem(_GlobalResources.AllInstructorLedTraining, Convert.ToInt32(CouponCodeValidFor.All).ToString()));
            this._InstructorLedTrainingIsAny.Items.Add(new ListItem(_GlobalResources.InstructorLedTrainingListedBelow, Convert.ToInt32(CouponCodeValidFor.ListedBelow).ToString()));
            this._InstructorLedTrainingIsAny.Attributes.Add("onclick", "ObjectOptionTypeClick('InstructorLedTraining');");
            this._InstructorLedTrainingIsAny.SelectedValue = Convert.ToInt32(CouponCodeValidFor.No).ToString(); // no selected by default
            couponCodeInstructorLedTrainingInputControls.Add(this._InstructorLedTrainingIsAny);

            // selected ilt hidden field
            this._SelectedInstructorLedTraining = new HiddenField();
            this._SelectedInstructorLedTraining.ID = "SelectedInstructorLedTraining_Field";
            couponCodeInstructorLedTrainingInputControls.Add(this._SelectedInstructorLedTraining);

            // build a container for the ilt listing
            this._CouponCodeInstructorLedTrainingListContainer = new Panel();
            this._CouponCodeInstructorLedTrainingListContainer.ID = "CouponCodeInstructorLedTrainingList_Container";
            this._CouponCodeInstructorLedTrainingListContainer.CssClass = "ItemListingContainer";
            couponCodeInstructorLedTrainingInputControls.Add(this._CouponCodeInstructorLedTrainingListContainer);

            Panel couponCodeInstructorLedTrainingButtonsPanel = new Panel();
            couponCodeInstructorLedTrainingButtonsPanel.ID = "CouponCodeInstructorLedTraining_ButtonsPanel";

            // select ilt button

            // link
            Image selectInstructorLedTrainingImageForLink = new Image();
            selectInstructorLedTrainingImageForLink.ID = "LaunchSelectInstructorLedTrainingModalImage";
            selectInstructorLedTrainingImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            selectInstructorLedTrainingImageForLink.CssClass = "MediumIcon";

            Localize selectInstructorLedTrainingTextForLink = new Localize();
            selectInstructorLedTrainingTextForLink.Text = _GlobalResources.SelectInstructorLedTraining;

            LinkButton selectInstructorLedTrainingLink = new LinkButton();
            selectInstructorLedTrainingLink.ID = "LaunchSelectInstructorLedTrainingModal";
            selectInstructorLedTrainingLink.CssClass = "ImageLink";
            selectInstructorLedTrainingLink.Controls.Add(selectInstructorLedTrainingImageForLink);
            selectInstructorLedTrainingLink.Controls.Add(selectInstructorLedTrainingTextForLink);
            couponCodeInstructorLedTrainingButtonsPanel.Controls.Add(selectInstructorLedTrainingLink);

            // attach the buttons panel to the container
            couponCodeInstructorLedTrainingInputControls.Add(couponCodeInstructorLedTrainingButtonsPanel);

            // build modals for adding and removing experts to/from the ilt
            this._BuildSelectInstructorLedTrainingModal(selectInstructorLedTrainingLink.ID);

            iltPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CouponCodeInstructorLedTraining",
                                                                                 _GlobalResources.ThisCouponCodeIsValidFor,
                                                                                 couponCodeInstructorLedTrainingInputControls,
                                                                                 false,
                                                                                 true));            

            // attach panel to container
            this.CouponCodePropertiesTabPanelsContainer.Controls.Add(iltPanel);
        }
        #endregion

        #region _BuildSelectInstructorLedTrainingModal
        /// <summary>
        /// Builds the modal for selecting instructor led training to add to the coupon code.
        /// </summary>
        private void _BuildSelectInstructorLedTrainingModal(string targetControlId)
        {
            // set modal properties
            this._SelectInstructorLedTrainingForCouponCode = new ModalPopup("SelectInstructorLedTrainingForCouponCodeModal");
            this._SelectInstructorLedTrainingForCouponCode.Type = ModalPopupType.Form;
            this._SelectInstructorLedTrainingForCouponCode.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            this._SelectInstructorLedTrainingForCouponCode.HeaderIconAlt = _GlobalResources.SelectInstructorLedTraining;
            this._SelectInstructorLedTrainingForCouponCode.HeaderText = _GlobalResources.SelectInstructorLedTraining;
            this._SelectInstructorLedTrainingForCouponCode.TargetControlID = targetControlId;
            this._SelectInstructorLedTrainingForCouponCode.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectInstructorLedTrainingForCouponCode.SubmitButtonCustomText = _GlobalResources.AddInstructorLedTraining;
            this._SelectInstructorLedTrainingForCouponCode.SubmitButton.OnClientClick = "javascript:AddInstructorLedTrainingToCouponCode(); return false;";
            this._SelectInstructorLedTrainingForCouponCode.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectInstructorLedTrainingForCouponCode.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectInstructorLedTrainingForCouponCode.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleInstructorLedTraining = new DynamicListBox("SelectEligibleInstructorLedTraining");
            this._SelectEligibleInstructorLedTraining.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectEligibleInstructorLedTraining.SearchButton.Command += new CommandEventHandler(this._SearchSelectInstructorLedTrainingButton_Command);
            this._SelectEligibleInstructorLedTraining.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectInstructorLedTrainingButton_Command);
            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataSource = this._EligibleInstructorLedTrainingForSelectList;
            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataTextField = "title";
            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataValueField = "idStandupTraining";
            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataBind();

            // add controls to body
            this._SelectInstructorLedTrainingForCouponCode.AddControlToBody(this._SelectEligibleInstructorLedTraining);

            // add modal to container
            this.CouponCodePropertiesTabPanelsContainer.Controls.Add(this._SelectInstructorLedTrainingForCouponCode);
        }
        #endregion

        #region _SearchSelectInstructorLedTrainingButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Instructor Led Training" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectInstructorLedTrainingButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectInstructorLedTrainingForCouponCode.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleInstructorLedTraining.ListBoxControl.Items.Clear();

            // do the search
            if (this._CouponCodeObject != null)
            { this._EligibleInstructorLedTrainingForSelectList = Asentia.LMS.Library.StandupTraining.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, this._SelectEligibleInstructorLedTraining.SearchTextBox.Text); }
            else
            { this._EligibleInstructorLedTrainingForSelectList = Asentia.LMS.Library.StandupTraining.IdsAndNamesForCouponCodeSelectList(0, this._SelectEligibleInstructorLedTraining.SearchTextBox.Text); }

            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataSource = this._EligibleInstructorLedTrainingForSelectList;
            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataTextField = "title";
            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataValueField = "idStandupTraining";
            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataBind();

            // if no records available then disable the list
            if (this._SelectEligibleInstructorLedTraining.ListBoxControl.Items.Count == 0)
            {
                this._SelectInstructorLedTrainingForCouponCode.SubmitButton.Enabled = false;
                this._SelectInstructorLedTrainingForCouponCode.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectInstructorLedTrainingForCouponCode.SubmitButton.Enabled = true;
                this._SelectInstructorLedTrainingForCouponCode.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _ClearSearchSelectInstructorLedTrainingButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Instructor Led Training" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectInstructorLedTrainingButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectInstructorLedTrainingForCouponCode.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleInstructorLedTraining.ListBoxControl.Items.Clear();
            this._SelectEligibleInstructorLedTraining.SearchTextBox.Text = "";

            // clear the search
            if (this._CouponCodeObject != null)
            { this._EligibleInstructorLedTrainingForSelectList = Asentia.LMS.Library.StandupTraining.IdsAndNamesForCouponCodeSelectList(this._CouponCodeObject.Id, null); }
            else
            { this._EligibleInstructorLedTrainingForSelectList = Asentia.LMS.Library.StandupTraining.IdsAndNamesForCouponCodeSelectList(0, null); }

            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataSource = this._EligibleInstructorLedTrainingForSelectList;
            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataTextField = "title";
            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataValueField = "idStandupTraining";
            this._SelectEligibleInstructorLedTraining.ListBoxControl.DataBind();

            // if records available then enable the list
            if (this._SelectEligibleInstructorLedTraining.ListBoxControl.Items.Count > 0)
            {
                this._SelectInstructorLedTrainingForCouponCode.SubmitButton.Enabled = true;
                this._SelectInstructorLedTrainingForCouponCode.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectInstructorLedTrainingForCouponCode.SubmitButton.Enabled = false;
                this._SelectInstructorLedTrainingForCouponCode.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _BuildCouponCodePropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for Coupon Code Properties actions.
        /// </summary>
        private void _BuildCouponCodePropertiesActionsPanel()
        {
            // clear controls from container
            this.CouponCodePropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.CouponCodePropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._CouponCodePropertiesSaveButton = new Button();
            this._CouponCodePropertiesSaveButton.ID = "CouponCodeSaveButton";
            this._CouponCodePropertiesSaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._CouponCodeObject == null)
            { this._CouponCodePropertiesSaveButton.Text = _GlobalResources.CreateCouponCode; }
            else
            { this._CouponCodePropertiesSaveButton.Text = _GlobalResources.SaveChanges; }

            this._CouponCodePropertiesSaveButton.Command += new CommandEventHandler(this._CouponCodePropertiesSaveButton_Command);
            this._CouponCodePropertiesSaveButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicElements();");
            this.CouponCodePropertiesActionsPanel.Controls.Add(this._CouponCodePropertiesSaveButton);

            // cancel button
            this._CouponCodePropertiesCancelButton = new Button();
            this._CouponCodePropertiesCancelButton.ID = "CancelButton";
            this._CouponCodePropertiesCancelButton.CssClass = "Button NonActionButton";
            this._CouponCodePropertiesCancelButton.Text = _GlobalResources.Cancel;
            this._CouponCodePropertiesCancelButton.Command += new CommandEventHandler(this._CouponCodePropertiesCancelButton_Command);
            this.CouponCodePropertiesActionsPanel.Controls.Add(this._CouponCodePropertiesCancelButton);
        }
        #endregion

        #region _PopulateCouponCodePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateCouponCodePropertiesInputElements()
        {
            if (this._CouponCodeObject != null)
            {                
                // code
                this._CouponCode.Text = this._CouponCodeObject.Code;

                // uses allowed
                this._CouponUsesAllowed.Text = this._CouponCodeObject.UsesAllowed.ToString();

                // single use per user
                this._SingleUsePerUser.Checked = (bool)this._CouponCodeObject.IsSingleUsePerUser;

                // lifespan start
                this._CouponLifeSpanDtStart.Value = this._CouponCodeObject.DtStart;
                this._CouponLifeSpanDtStart.Value = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._CouponCodeObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(AsentiaSessionState.GlobalSiteObject.IdTimezone).dotNetName));

                // lifespan end
                if (this._CouponCodeObject.DtEnd != null)
                {
                    this._CouponLifeSpanDtEnd.NoneCheckBoxChecked = false;
                    this._CouponLifeSpanDtEnd.Value = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._CouponCodeObject.DtEnd, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(AsentiaSessionState.GlobalSiteObject.IdTimezone).dotNetName));
                }
                else
                {
                    this._CouponLifeSpanDtEnd.NoneCheckBoxChecked = true;
                    this._CouponLifeSpanDtEnd.Value = null;
                }

                // discount type
                this._DiscountTypeList.SelectedValue = this._CouponCodeObject.DiscountType.ToString();

                // discount value
                this._DiscountValue.Text = this._CouponCodeObject.Discount.ToString();

                // comments/notes
                this._CouponCodeComment.Text = this._CouponCodeObject.Comments;                                                                                

                /* CATALOGS */

                this._CatalogIsAny.SelectedValue = this._CouponCodeObject.ForCatalog.ToString();

                // loop through the datatable and add each catalog to the listing container
                foreach (DataRow row in this._CouponCodeCatalogs.Rows)
                {
                    // container
                    Panel catalogNameContainer = new Panel();
                    catalogNameContainer.ID = "Catalog_" + row["idCatalog"].ToString();

                    // remove catalog button
                    Image removeCatalogImage = new Image();
                    removeCatalogImage.ID = "Catalog_" + row["idCatalog"].ToString() + "_RemoveImage";
                    removeCatalogImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    removeCatalogImage.CssClass = "SmallIcon";
                    removeCatalogImage.Attributes.Add("onclick", "javascript:RemoveCatalogFromCouponCode('" + row["idCatalog"].ToString() + "');");
                    removeCatalogImage.Style.Add("cursor", "pointer");

                    // catalog name
                    Literal catalogName = new Literal();
                    catalogName.Text = row["title"].ToString();

                    // add controls to container
                    catalogNameContainer.Controls.Add(removeCatalogImage);
                    catalogNameContainer.Controls.Add(catalogName);
                    this._CouponCodeCatalogsListContainer.Controls.Add(catalogNameContainer);
                }

                /* COURSES */

                this._CourseIsAny.SelectedValue = this._CouponCodeObject.ForCourse.ToString();

                // loop through the datatable and add each course to the listing container
                foreach (DataRow row in this._CouponCodeCourses.Rows)
                {
                    // container
                    Panel courseNameContainer = new Panel();
                    courseNameContainer.ID = "Course_" + row["idCourse"].ToString();

                    // remove course button
                    Image removeCourseImage = new Image();
                    removeCourseImage.ID = "Course_" + row["idCourse"].ToString() + "_RemoveImage";
                    removeCourseImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    removeCourseImage.CssClass = "SmallIcon";
                    removeCourseImage.Attributes.Add("onclick", "javascript:RemoveCourseFromCouponCode('" + row["idCourse"].ToString() + "');");
                    removeCourseImage.Style.Add("cursor", "pointer");

                    // course name
                    Literal courseName = new Literal();
                    courseName.Text = row["title"].ToString();

                    // add controls to container
                    courseNameContainer.Controls.Add(removeCourseImage);
                    courseNameContainer.Controls.Add(courseName);
                    this._CouponCodeCoursesListContainer.Controls.Add(courseNameContainer);
                }

                /* LEARNING PATHS */

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    this._LearningPathIsAny.SelectedValue = this._CouponCodeObject.ForLearningPath.ToString();

                    // loop through the datatable and add each learning path to the listing container
                    foreach (DataRow row in this._CouponCodeLearningPaths.Rows)
                    {
                        // container
                        Panel learningPathNameContainer = new Panel();
                        learningPathNameContainer.ID = "LearningPath_" + row["idLearningPath"].ToString();

                        // remove learning path button
                        Image removeLearningPathImage = new Image();
                        removeLearningPathImage.ID = "LearningPath_" + row["idLearningPath"].ToString() + "_RemoveImage";
                        removeLearningPathImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        removeLearningPathImage.CssClass = "SmallIcon";
                        removeLearningPathImage.Attributes.Add("onclick", "javascript:RemoveLearningPathFromCouponCode('" + row["idLearningPath"].ToString() + "');");
                        removeLearningPathImage.Style.Add("cursor", "pointer");

                        // learning path name
                        Literal learningPathName = new Literal();
                        learningPathName.Text = row["title"].ToString();

                        // add controls to container
                        learningPathNameContainer.Controls.Add(removeLearningPathImage);
                        learningPathNameContainer.Controls.Add(learningPathName);
                        this._CouponCodeLearningPathsListContainer.Controls.Add(learningPathNameContainer);
                    }
                }

                /* ILTs */

                this._InstructorLedTrainingIsAny.SelectedValue = this._CouponCodeObject.ForStandupTraining.ToString();

                // loop through the datatable and add each instructor led training to the listing container
                foreach (DataRow row in this._CouponCodeInstructorLedTraining.Rows)
                {
                    // container
                    Panel instructorLedTrainingNameContainer = new Panel();
                    instructorLedTrainingNameContainer.ID = "InstructorLedTraining_" + row["idStandupTraining"].ToString();

                    // remove instructor led training button
                    Image removeInstructorLedTrainingImage = new Image();
                    removeInstructorLedTrainingImage.ID = "InstructorLedTraining_" + row["idStandupTraining"].ToString() + "_RemoveImage";
                    removeInstructorLedTrainingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    removeInstructorLedTrainingImage.CssClass = "SmallIcon";
                    removeInstructorLedTrainingImage.Attributes.Add("onclick", "javascript:RemoveInstructorLedTrainingFromCouponCode('" + row["idStandupTraining"].ToString() + "');");
                    removeInstructorLedTrainingImage.Style.Add("cursor", "pointer");

                    // instructor led training name
                    Literal instructorLedTrainingName = new Literal();
                    instructorLedTrainingName.Text = row["title"].ToString();

                    // add controls to container
                    instructorLedTrainingNameContainer.Controls.Add(removeInstructorLedTrainingImage);
                    instructorLedTrainingNameContainer.Controls.Add(instructorLedTrainingName);
                    this._CouponCodeInstructorLedTrainingListContainer.Controls.Add(instructorLedTrainingNameContainer);
                }
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Performs all server side validations
        /// </summary>
        /// <returns></returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;
            bool catalogsTabHasErrors = false;
            bool coursesTabHasErrors = false;
            bool learningPathsTabHasErrors = false;
            bool iltTabHasErrors = false;

            // code - required
            if (String.IsNullOrWhiteSpace(this._CouponCode.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponCode", _GlobalResources.Code + " " + _GlobalResources.IsRequired);
            }

            // uses allowed - required and must be int
            if (String.IsNullOrWhiteSpace(this._CouponUsesAllowed.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponUsesAllowed", _GlobalResources.UsesAllowed + " " + _GlobalResources.IsRequired);
            }

            if (!String.IsNullOrWhiteSpace(this._CouponUsesAllowed.Text))
            {
                int usesAllowed;

                if (!(int.TryParse(this._CouponUsesAllowed.Text, out usesAllowed)))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponUsesAllowed", _GlobalResources.UsesAllowed + " " + _GlobalResources.IsInvalid);
                }
            }

            // lifespan start date - required
            if (this._CouponLifeSpanDtStart.Value == null)
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "LifeSpanStart", _GlobalResources.LifespanStart + " " + _GlobalResources.IsRequired);
            }

            // lifespan end, if specified must occur after start
            if (!this._CouponLifeSpanDtEnd.NoneCheckBoxChecked)
            {
                if (this._CouponLifeSpanDtEnd.Value == null)
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "LifeSpanEnd", _GlobalResources.LifespanEnd + " " + _GlobalResources.IsInvalid);
                }
                else
                {
                    if (this._CouponLifeSpanDtStart.Value >= this._CouponLifeSpanDtEnd.Value)
                    {
                        isValid = false;
                        propertiesTabHasErrors = true;
                        this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "LifeSpanEnd", _GlobalResources.LifespanEndMustOccurAfterLifespanStart);
                    }
                }
            }

            // discount - required and must be double if "Free" discount type not selected
            if (this._DiscountTypeList.SelectedValue != Convert.ToInt32(DiscountType.Free).ToString())
            {
                if (String.IsNullOrWhiteSpace(this._DiscountValue.Text))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "DiscountType", _GlobalResources.DiscountType + " " + _GlobalResources.IsRequired);
                }
                else
                {
                    double discountValue;

                    if (!double.TryParse(this._DiscountValue.Text, out discountValue))
                    {
                        isValid = false;
                        propertiesTabHasErrors = true;
                        this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "DiscountType", _GlobalResources.DiscountType + " " + _GlobalResources.IsInvalid);
                    }
                   
                    // must be between 1 and 99 if the type is percent off
                    if (this._DiscountTypeList.SelectedValue == Convert.ToInt32(DiscountType.PercentOff).ToString() && (discountValue < 1 || discountValue > 99))
                    {
                        isValid = false;
                        propertiesTabHasErrors = true;
                        this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "DiscountType", _GlobalResources.DiscountType + " " + _GlobalResources.IsInvalid);
                    }
                }
            }

            // validate that the code at least appllies to something
            if (
                this._CatalogIsAny.SelectedValue == Convert.ToInt32(CouponCodeValidFor.No).ToString()
                && this._CourseIsAny.SelectedValue == Convert.ToInt32(CouponCodeValidFor.No).ToString()
                && this._LearningPathIsAny.SelectedValue == Convert.ToInt32(CouponCodeValidFor.No).ToString()
                && this._InstructorLedTrainingIsAny.SelectedValue == Convert.ToInt32(CouponCodeValidFor.No).ToString()
               )
            {
                isValid = false;
                catalogsTabHasErrors = true;
                coursesTabHasErrors = true;
                learningPathsTabHasErrors = true;
                iltTabHasErrors = true;

                this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponCodeCatalogs", _GlobalResources.AtLeastOneCatalogCourseLearningPathOrInstructorLedTrainingMustBeSelected);
                this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponCodeCourses", _GlobalResources.AtLeastOneCatalogCourseLearningPathOrInstructorLedTrainingMustBeSelected);

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponCodeLearningPaths", _GlobalResources.AtLeastOneCatalogCourseLearningPathOrInstructorLedTrainingMustBeSelected);
                }         
                
                this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponCodeInstructorLedTraining", _GlobalResources.AtLeastOneCatalogCourseLearningPathOrInstructorLedTrainingMustBeSelected);
            }
            else // for any of the "listed below" items, make sure at least one has been selected
            {
                // catalogs
                if (this._CatalogIsAny.SelectedValue == Convert.ToInt32(CouponCodeValidFor.ListedBelow).ToString() && String.IsNullOrWhiteSpace(this._SelectedCatalogs.Value))
                {
                    isValid = false;
                    catalogsTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponCodeCatalogs", _GlobalResources.AtLeastOneCatalogMustBeSelected);
                }

                // courses
                if (this._CourseIsAny.SelectedValue == Convert.ToInt32(CouponCodeValidFor.ListedBelow).ToString() && String.IsNullOrWhiteSpace(this._SelectedCourses.Value))
                {
                    isValid = false;
                    coursesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponCodeCourses", _GlobalResources.AtLeastOneCourseMustBeSelected);
                }

                // learning paths
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    if (this._LearningPathIsAny.SelectedValue == Convert.ToInt32(CouponCodeValidFor.ListedBelow).ToString() && String.IsNullOrWhiteSpace(this._SelectedLearningPaths.Value))
                    {
                        isValid = false;
                        learningPathsTabHasErrors = true;
                        this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponCodeLearningPaths", _GlobalResources.AtLeastOneLearningPathMustBeSelected);
                    }
                }

                // instructor led training
                if (this._InstructorLedTrainingIsAny.SelectedValue == Convert.ToInt32(CouponCodeValidFor.ListedBelow).ToString() && String.IsNullOrWhiteSpace(this._SelectedInstructorLedTraining.Value))
                {
                    isValid = false;
                    iltTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.CouponCodePropertiesContainer, "CouponCodeInstructorLedTraining", _GlobalResources.AtLeastOneInstructorLedTrainingMustBeSelected);
                }
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.CouponCodePropertiesContainer, "CouponCodeProperties_Properties_TabLI"); }

            if (catalogsTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.CouponCodePropertiesContainer, "CouponCodeProperties_Catalogs_TabLI"); }

            if (coursesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.CouponCodePropertiesContainer, "CouponCodeProperties_Courses_TabLI"); }

            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
            {
                if (learningPathsTabHasErrors)
                { this.ApplyErrorImageAndClassToTab(this.CouponCodePropertiesContainer, "CouponCodeProperties_LearningPaths_TabLI"); }
            }

            if (iltTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.CouponCodePropertiesContainer, "CouponCodeProperties_ILT_TabLI"); }

            // if this is invalid, we need to pre-populate multi-select fields with the objects that were selected
            if (!isValid)
            {
                /* CATALOGS */

                if (!String.IsNullOrWhiteSpace(this._SelectedCatalogs.Value))
                {
                    // split the "value" of the hidden field to get an array of catalog ids
                    string[] selectedCatalogs = this._SelectedCatalogs.Value.Split(',');

                    // loop through the array and add each catalog to the listing container
                    foreach (string catalogId in selectedCatalogs)
                    {
                        if (this._CouponCodeCatalogsListContainer.FindControl("Catalog_" + catalogId) == null)
                        {
                            // catalog object
                            Library.Catalog catalogObject = new Library.Catalog(Convert.ToInt32(catalogId));
                            
                            // container
                            Panel catalogNameContainer = new Panel();
                            catalogNameContainer.ID = "Catalog_" + catalogObject.IdCatalog.ToString();

                            // remove catalog button                            
                            Image removeCatalogImage = new Image();
                            removeCatalogImage.ID = "Catalog_" + catalogObject.IdCatalog.ToString() + "_RemoveImage";
                            removeCatalogImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                            removeCatalogImage.CssClass = "SmallIcon";
                            removeCatalogImage.Attributes.Add("onclick", "javascript:RemoveCatalogFromCouponCode('" + catalogObject.IdCatalog.ToString() + "');");
                            removeCatalogImage.Style.Add("cursor", "pointer");
                            
                            // catalog name
                            Literal catalogName = new Literal();
                            catalogName.Text = catalogObject.Title;

                            foreach (Library.Catalog.LanguageSpecificProperty catalogLanguageSpecificProperty in catalogObject.LanguageSpecificProperties)
                            {
                                if (catalogLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                                {
                                    catalogName.Text = catalogLanguageSpecificProperty.Title;
                                    break;
                                }
                            }

                            // add controls to container                            
                            catalogNameContainer.Controls.Add(removeCatalogImage);
                            catalogNameContainer.Controls.Add(catalogName);
                            this._CouponCodeCatalogsListContainer.Controls.Add(catalogNameContainer);
                        }
                    }
                }

                /* COURSES */

                if (!String.IsNullOrWhiteSpace(this._SelectedCourses.Value))
                {
                    // split the "value" of the hidden field to get an array of course ids
                    string[] selectedCourses = this._SelectedCourses.Value.Split(',');

                    // loop through the array and add each course to the listing container
                    foreach (string courseId in selectedCourses)
                    {
                        if (this._CouponCodeCoursesListContainer.FindControl("Course_" + courseId) == null)
                        {
                            // course object
                            Library.Course courseObject = new Library.Course(Convert.ToInt32(courseId));

                            // container
                            Panel courseNameContainer = new Panel();
                            courseNameContainer.ID = "Course_" + courseObject.Id.ToString();

                            // remove course button                            
                            Image removeCourseImage = new Image();
                            removeCourseImage.ID = "Course_" + courseObject.Id.ToString() + "_RemoveImage";
                            removeCourseImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                            removeCourseImage.CssClass = "SmallIcon";
                            removeCourseImage.Attributes.Add("onclick", "javascript:RemoveCourseFromCouponCode('" + courseObject.Id.ToString() + "');");
                            removeCourseImage.Style.Add("cursor", "pointer");

                            // course name
                            Literal courseName = new Literal();
                            courseName.Text = courseObject.Title;

                            foreach (Library.Course.LanguageSpecificProperty courseLanguageSpecificProperty in courseObject.LanguageSpecificProperties)
                            {
                                if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                                {
                                    courseName.Text = courseLanguageSpecificProperty.Title;
                                    break;
                                }
                            }
                            
                            // add controls to container                            
                            courseNameContainer.Controls.Add(removeCourseImage);
                            courseNameContainer.Controls.Add(courseName);
                            this._CouponCodeCoursesListContainer.Controls.Add(courseNameContainer);
                        }
                    }
                }

                /* LEARNING PATHS */

                // check to ensure learning paths are enabled on the portal
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    if (!String.IsNullOrWhiteSpace(this._SelectedLearningPaths.Value))
                    {
                        // split the "value" of the hidden field to get an array of learning path ids
                        string[] selectedLearningPaths = this._SelectedLearningPaths.Value.Split(',');

                        // loop through the array and add each learning path to the listing container
                        foreach (string learningPathId in selectedLearningPaths)
                        {
                            if (this._CouponCodeLearningPathsListContainer.FindControl("LearningPath_" + learningPathId) == null)
                            {
                                // learning path object
                                Library.LearningPath learningPathObject = new Library.LearningPath(Convert.ToInt32(learningPathId));

                                // container
                                Panel learningPathNameContainer = new Panel();
                                learningPathNameContainer.ID = "LearningPath_" + learningPathObject.Id.ToString();

                                // remove learning path button                            
                                Image removeLearningPathImage = new Image();
                                removeLearningPathImage.ID = "LearningPath_" + learningPathObject.Id.ToString() + "_RemoveImage";
                                removeLearningPathImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                                removeLearningPathImage.CssClass = "SmallIcon";
                                removeLearningPathImage.Attributes.Add("onclick", "javascript:RemoveLearningPathFromCouponCode('" + learningPathObject.Id.ToString() + "');");
                                removeLearningPathImage.Style.Add("cursor", "pointer");

                                // learning path name
                                Literal learningPathName = new Literal();
                                learningPathName.Text = learningPathObject.Name;

                                foreach (Library.LearningPath.LanguageSpecificProperty learningPathLanguageSpecificProperty in learningPathObject.LanguageSpecificProperties)
                                {
                                    if (learningPathLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                                    {
                                        learningPathName.Text = learningPathLanguageSpecificProperty.Name;
                                        break;
                                    }
                                }

                                // add controls to container                            
                                learningPathNameContainer.Controls.Add(removeLearningPathImage);
                                learningPathNameContainer.Controls.Add(learningPathName);
                                this._CouponCodeLearningPathsListContainer.Controls.Add(learningPathNameContainer);
                            }
                        }
                    }
                }

                /* INSTRUCTOR LED TRAINING */

                if (!String.IsNullOrWhiteSpace(this._SelectedInstructorLedTraining.Value))
                {
                    // split the "value" of the hidden field to get an array of instructor led training ids
                    string[] selectedInstructorLedTraining = this._SelectedInstructorLedTraining.Value.Split(',');

                    // loop through the array and add each instructor led training to the listing container
                    foreach (string instructorLedTrainingId in selectedInstructorLedTraining)
                    {
                        if (this._CouponCodeInstructorLedTrainingListContainer.FindControl("InstructorLedTraining_" + instructorLedTrainingId) == null)
                        {
                            // instructor led training object
                            Library.StandupTraining instructorLedTrainingObject = new Library.StandupTraining(Convert.ToInt32(instructorLedTrainingId));

                            // container
                            Panel instructorLedTrainingNameContainer = new Panel();
                            instructorLedTrainingNameContainer.ID = "InstructorLedTraining_" + instructorLedTrainingObject.Id.ToString();

                            // remove instructor led training button                            
                            Image removeInstructorLedTrainingImage = new Image();
                            removeInstructorLedTrainingImage.ID = "InstructorLedTraining_" + instructorLedTrainingObject.Id.ToString() + "_RemoveImage";
                            removeInstructorLedTrainingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                            removeInstructorLedTrainingImage.CssClass = "SmallIcon";
                            removeInstructorLedTrainingImage.Attributes.Add("onclick", "javascript:RemoveInstructorLedTrainingFromCouponCode('" + instructorLedTrainingObject.Id.ToString() + "');");
                            removeInstructorLedTrainingImage.Style.Add("cursor", "pointer");

                            // instructor led training name
                            Literal instructorLedTrainingName = new Literal();
                            instructorLedTrainingName.Text = instructorLedTrainingObject.Title;

                            foreach (Library.StandupTraining.LanguageSpecificProperty instructorLedTrainingLanguageSpecificProperty in instructorLedTrainingObject.LanguageSpecificProperties)
                            {
                                if (instructorLedTrainingLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                                {
                                    instructorLedTrainingName.Text = instructorLedTrainingLanguageSpecificProperty.Title;
                                    break;
                                }
                            }

                            // add controls to container                            
                            instructorLedTrainingNameContainer.Controls.Add(removeInstructorLedTrainingImage);
                            instructorLedTrainingNameContainer.Controls.Add(instructorLedTrainingName);
                            this._CouponCodeInstructorLedTrainingListContainer.Controls.Add(instructorLedTrainingNameContainer);
                        }
                    }
                }
            }

            return isValid;
        }
        #endregion

        #region _CouponCodePropertiesSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for coupon code Properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CouponCodePropertiesSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                // if there is no coupon code object, create one
                if (this._CouponCodeObject == null)
                { this._CouponCodeObject = new CouponCode(); }

                int id;

                // code
                this._CouponCodeObject.Code = this._CouponCode.Text.Trim();

                // uses allowed
                this._CouponCodeObject.UsesAllowed = Convert.ToInt32(this._CouponUsesAllowed.Text.Trim());

                // is single use per user
                this._CouponCodeObject.IsSingleUsePerUser = this._SingleUsePerUser.Checked;

                // lifespan start
                this._CouponCodeObject.DtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._CouponLifeSpanDtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(AsentiaSessionState.GlobalSiteObject.IdTimezone).dotNetName));

                // lifespan end
                if (this._CouponLifeSpanDtEnd.NoneCheckBoxChecked)
                { this._CouponCodeObject.DtEnd = null; }
                else
                { this._CouponCodeObject.DtEnd = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._CouponLifeSpanDtEnd.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(AsentiaSessionState.GlobalSiteObject.IdTimezone).dotNetName)); }
                
                // discount type
                this._CouponCodeObject.DiscountType = Convert.ToInt32(this._DiscountTypeList.SelectedValue);

                // discount value
                if (this._DiscountTypeList.SelectedValue != (Convert.ToInt32(DiscountType.Free).ToString()))
                { this._CouponCodeObject.Discount = Convert.ToDouble(this._DiscountValue.Text.Trim()); }
                else
                { this._CouponCodeObject.Discount = 0; }

                // comments/notes
                if (!String.IsNullOrWhiteSpace(this._CouponCodeComment.Text))
                { this._CouponCodeObject.Comments = this._CouponCodeComment.Text; }
                else
                { this._CouponCodeObject.Comments = null; }

                // coupon code is for catalog(s)
                this._CouponCodeObject.ForCatalog = Convert.ToInt32(this._CatalogIsAny.SelectedValue);

                // coupon code is for course(s)
                this._CouponCodeObject.ForCourse = Convert.ToInt32(this._CourseIsAny.SelectedValue);

                // coupon code is for learning path(s)
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    this._CouponCodeObject.ForLearningPath = Convert.ToInt32(this._LearningPathIsAny.SelectedValue);
                }
                
                // coupon code is for instructor led training
                this._CouponCodeObject.ForStandupTraining = Convert.ToInt32(this._InstructorLedTrainingIsAny.SelectedValue);

                // save the coupon code, save its returned id to viewstate, and set the current coupon code object's id
                id = this._CouponCodeObject.Save();
                this.ViewState["id"] = id;
                this._CouponCodeObject.Id = id;

                // do catalogs - "listed below"

                if ((CouponCodeValidFor)Convert.ToInt32(this._CatalogIsAny.SelectedValue) == CouponCodeValidFor.ListedBelow)
                {
                    // declare data table
                    DataTable catalogsToSave = new DataTable();
                    catalogsToSave.Columns.Add("id", typeof(int));

                    if (!String.IsNullOrWhiteSpace(this._SelectedCatalogs.Value))
                    {
                        // split the "value" of the hidden field to get an array of catalog ids
                        string[] selectedCatalogIds = this._SelectedCatalogs.Value.Split(',');

                        // put ids into datatable 
                        foreach (string catalogId in selectedCatalogIds)
                        { catalogsToSave.Rows.Add(Convert.ToInt32(catalogId)); }
                    }

                    // save catalogs for coupon code
                    this._CouponCodeObject.SaveCatalogs(catalogsToSave);
                }

                // do courses - "listed below"

                if ((CouponCodeValidFor)Convert.ToInt32(this._CourseIsAny.SelectedValue) == CouponCodeValidFor.ListedBelow)
                {
                    // declare data table
                    DataTable coursesToSave = new DataTable();
                    coursesToSave.Columns.Add("id", typeof(int));

                    if (!String.IsNullOrWhiteSpace(this._SelectedCourses.Value))
                    {
                        // split the "value" of the hidden field to get an array of course ids
                        string[] selectedCourseIds = this._SelectedCourses.Value.Split(',');

                        // put ids into datatable 
                        foreach (string courseId in selectedCourseIds)
                        { coursesToSave.Rows.Add(Convert.ToInt32(courseId)); }
                    }

                    // save courses for coupon code
                    this._CouponCodeObject.SaveCourses(coursesToSave);
                }

                // do learning paths - "listed below"
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                {
                    if ((CouponCodeValidFor)Convert.ToInt32(this._LearningPathIsAny.SelectedValue) == CouponCodeValidFor.ListedBelow)
                    {
                        // declare data table
                        DataTable learningPathsToSave = new DataTable();
                        learningPathsToSave.Columns.Add("id", typeof(int));

                        if (!String.IsNullOrWhiteSpace(this._SelectedLearningPaths.Value))
                        {
                            // split the "value" of the hidden field to get an array of learning path ids
                            string[] selectedLearningPathIds = this._SelectedLearningPaths.Value.Split(',');

                            // put ids into datatable 
                            foreach (string learningPathId in selectedLearningPathIds)
                            { learningPathsToSave.Rows.Add(Convert.ToInt32(learningPathId)); }
                        }

                        // save learning path for coupon code
                        this._CouponCodeObject.SaveLearningPaths(learningPathsToSave);
                    }
                }

                // do instructor led training - "listed below"

                if ((CouponCodeValidFor)Convert.ToInt32(this._InstructorLedTrainingIsAny.SelectedValue) == CouponCodeValidFor.ListedBelow)
                {
                    // declare data table
                    DataTable instructorLedTrainingToSave = new DataTable();
                    instructorLedTrainingToSave.Columns.Add("id", typeof(int));

                    if (!String.IsNullOrWhiteSpace(this._SelectedInstructorLedTraining.Value))
                    {
                        // split the "value" of the hidden field to get an array of instructor led training ids
                        string[] selectedInstructorLedTrainingIds = this._SelectedInstructorLedTraining.Value.Split(',');

                        // put ids into datatable 
                        foreach (string instructorLedTrainingId in selectedInstructorLedTrainingIds)
                        { instructorLedTrainingToSave.Rows.Add(Convert.ToInt32(instructorLedTrainingId)); }
                    }

                    // save instructor led training for coupon code
                    this._CouponCodeObject.SaveInstructorLedTraining(instructorLedTrainingToSave);
                }

                // load the saved coupon code object
                this._CouponCodeObject = new CouponCode(id);

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.CouponCodePropertiesFeedbackContainer, _GlobalResources.CouponCodeHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CouponCodePropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CouponCodePropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CouponCodePropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CouponCodePropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.CouponCodePropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CouponCodePropertiesCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for Coupon Code Properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CouponCodePropertiesCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/couponcodes");
        }
        #endregion
    }
}