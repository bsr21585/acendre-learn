﻿$(document).ready(function () {
    DiscountTypeClick();
    ObjectOptionTypeClick("Catalog");
    ObjectOptionTypeClick("Course");
    ObjectOptionTypeClick("LearningPath");
    ObjectOptionTypeClick("InstructorLedTraining");
});

// Function to handle when a discount type is clicked.
function DiscountTypeClick() {
    var discountType = $("#DiscountType_Field").find(":checked").val();

    if (discountType == 1) // Free
    {
        $("#DiscountValue_Field").val("");

        $("#DiscountTypeNewPriceLabel").hide();
        $("#DiscountTypeAmountOffLabel").hide();
        $("#DiscountTypePercentOffLabel").hide();
        $("#DiscountTypeCurrencyPrefix").hide();
        $("#DiscountValue_Field").hide();
        $("#DiscountTypeCurrencySuffix").hide();
        $("#DiscountTypePercentOffSuffix").hide();
    }
    else if (discountType == 2) // Fixed Price
    {
        $("#DiscountTypeNewPriceLabel").show();
        $("#DiscountTypeAmountOffLabel").hide();
        $("#DiscountTypePercentOffLabel").hide();
        $("#DiscountTypeCurrencyPrefix").show();
        $("#DiscountValue_Field").show();
        $("#DiscountTypeCurrencySuffix").show();
        $("#DiscountTypePercentOffSuffix").hide();
    }
    else if (discountType == 3) // Amount Off
    {
        $("#DiscountTypeNewPriceLabel").hide();
        $("#DiscountTypeAmountOffLabel").show();
        $("#DiscountTypePercentOffLabel").hide();
        $("#DiscountTypeCurrencyPrefix").show();
        $("#DiscountValue_Field").show();
        $("#DiscountTypeCurrencySuffix").show();
        $("#DiscountTypePercentOffSuffix").hide();
    }
    else if (discountType == 4) // Percent Off
    {
        $("#DiscountTypeNewPriceLabel").hide();
        $("#DiscountTypeAmountOffLabel").hide();
        $("#DiscountTypePercentOffLabel").show();
        $("#DiscountTypeCurrencyPrefix").hide();
        $("#DiscountValue_Field").show();
        $("#DiscountTypeCurrencySuffix").hide();
        $("#DiscountTypePercentOffSuffix").show();
    }
    else
    {}
}

// Function to handle when a object type option is clicked.
function ObjectOptionTypeClick(objectPrefix) {
    // setting a plural prefix is necessary because some elements are named with the object plural
    var objectPrefixPlural = objectPrefix;

    if (objectPrefix != "InstructorLedTraining") {
        objectPrefixPlural = objectPrefixPlural + "s";
    }

    // if "No" or "All" are selected, disable the option to selecte individual objects
    if ($("#" + objectPrefix + "IsAny_Field_0").prop("checked") || $("#" + objectPrefix + "IsAny_Field_1").prop("checked")) {
        $("#CouponCode" + objectPrefixPlural + "List_Container").css("opacity", "0.3");
        $("#CouponCode" + objectPrefixPlural + "List_Container").html("");
        $("#LaunchSelect" + objectPrefixPlural + "Modal").addClass("aspNetDisabled");
        $("#LaunchSelect" + objectPrefixPlural + "Modal").css("cursor", "default");
        $("#LaunchSelect" + objectPrefixPlural + "Modal").css("pointer-events", "none");
    }
    else {        
        $("#CouponCode" + objectPrefixPlural + "List_Container").css("opacity", "1.0");
        $("#LaunchSelect" + objectPrefixPlural + "Modal").removeClass("aspNetDisabled");
        $("#LaunchSelect" + objectPrefixPlural + "Modal").css("cursor", "pointer");
        $("#LaunchSelect" + objectPrefixPlural + "Modal").css("pointer-events", "inherit");
    }
}

// Method to add catalogs from modal popup to catalog listing container
function AddCatalogsToCouponCode() {
    var catalogListContainer = $("#CouponCodeCatalogsList_Container");
    var selectedCatalogs = $('select#SelectEligibleCatalogsListBox').val();

    if (selectedCatalogs != null) {
        for (var i = 0; i < selectedCatalogs.length; i++) {
            if (!$("#Catalog_" + selectedCatalogs[i]).length) {
                // add the selected catalog to the catalog list container
                var itemContainerDiv = $("<div id=\"Catalog_" + selectedCatalogs[i] + "\">" + "<img onclick=\"javascript:RemoveCatalogFromCouponCode('" + selectedCatalogs[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" class=\"SmallIcon\" />" + $("#SelectEligibleCatalogsListBox option[value='" + selectedCatalogs[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(catalogListContainer);
            }

            // remove the catalog from the select list
            $("#SelectEligibleCatalogsListBox option[value='" + selectedCatalogs[i] + "']").remove();
        }
    }
}

// Method to remove catalog from listing container
function RemoveCatalogFromCouponCode(catalogId) {
    $("#Catalog_" + catalogId).remove();
}

// Gets the selected catalogs
function GetSelectedCatalogsForHiddenField() {
    var catalogListContainer = $("#CouponCodeCatalogsList_Container");
    var selectedCatalogField = $("#SelectedCatalogs_Field");
    var selectedCatalog = "";

    catalogListContainer.children().each(function () {
        selectedCatalog = selectedCatalog + $(this).prop("id").replace("Catalog_", "") + ",";
    });

    if (selectedCatalog.length > 0)
    { selectedCatalog = selectedCatalog.substring(0, selectedCatalog.length - 1); }

    selectedCatalogField.val(selectedCatalog);
}

// Method to add courses from modal popup to course listing container
function AddCoursesToCouponCode() {
    var courseListContainer = $("#CouponCodeCoursesList_Container");
    var selectedCourses = $('select#SelectEligibleCoursesListBox').val();

    if (selectedCourses != null) {
        for (var i = 0; i < selectedCourses.length; i++) {
            if (!$("#Course_" + selectedCourses[i]).length) {
                // add the selected course to the course list container
                var itemContainerDiv = $("<div id=\"Course_" + selectedCourses[i] + "\">" + "<img onclick=\"javascript:RemoveCourseFromCouponCode('" + selectedCourses[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" class=\"SmallIcon\" />" + $("#SelectEligibleCoursesListBox option[value='" + selectedCourses[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(courseListContainer);
            }

            // remove the course from the select list
            $("#SelectEligibleCoursesListBox option[value='" + selectedCourses[i] + "']").remove();
        }
    }
}

// Method to remove course from listing container
function RemoveCourseFromCouponCode(courseId) {
    $("#Course_" + courseId).remove();
}

// Gets the selected courses
function GetSelectedCoursesForHiddenField() {
    var courseListContainer = $("#CouponCodeCoursesList_Container");
    var selectedCourseField = $("#SelectedCourses_Field");
    var selectedCourse = "";

    courseListContainer.children().each(function () {
        selectedCourse = selectedCourse + $(this).prop("id").replace("Course_", "") + ",";
    });

    if (selectedCourse.length > 0)
    { selectedCourse = selectedCourse.substring(0, selectedCourse.length - 1); }

    selectedCourseField.val(selectedCourse);
}

// Method to add learning paths from modal popup to learning path listing container
function AddLearningPathsToCouponCode() {
    var learningPathListContainer = $("#CouponCodeLearningPathsList_Container");
    var selectedLearningPaths = $('select#SelectEligibleLearningPathsListBox').val();

    if (selectedLearningPaths != null) {
        for (var i = 0; i < selectedLearningPaths.length; i++) {
            if (!$("#LearningPath_" + selectedLearningPaths[i]).length) {
                // add the selected learning path to the learning path list container
                var itemContainerDiv = $("<div id=\"LearningPath_" + selectedLearningPaths[i] + "\">" + "<img onclick=\"javascript:RemoveLearningPathFromCouponCode('" + selectedLearningPaths[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" class=\"SmallIcon\" />" + $("#SelectEligibleLearningPathsListBox option[value='" + selectedLearningPaths[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(learningPathListContainer);
            }

            // remove the learning path from the select list
            $("#SelectEligibleLearningPathsListBox option[value='" + selectedLearningPaths[i] + "']").remove();
        }
    }
}

// Method to remove learning path from listing container
function RemoveLearningPathFromCouponCode(learningPathId) {
    $("#LearningPath_" + learningPathId).remove();
}

// Gets the selected learning paths
function GetSelectedLearningPathsForHiddenField() {
    var learningPathListContainer = $("#CouponCodeLearningPathsList_Container");
    var selectedLearningPathField = $("#SelectedLearningPaths_Field");
    var selectedLearningPath = "";

    learningPathListContainer.children().each(function () {
        selectedLearningPath = selectedLearningPath + $(this).prop("id").replace("LearningPath_", "") + ",";
    });

    if (selectedLearningPath.length > 0)
    { selectedLearningPath = selectedLearningPath.substring(0, selectedLearningPath.length - 1); }

    selectedLearningPathField.val(selectedLearningPath);
}

// Method to add instructor led training from modal popup to instructor led training listing container
function AddInstructorLedTrainingToCouponCode() {
    var instructorLedTrainingListContainer = $("#CouponCodeInstructorLedTrainingList_Container");
    var selectedInstructorLedTraining = $('select#SelectEligibleInstructorLedTrainingListBox').val();

    if (selectedInstructorLedTraining != null) {
        for (var i = 0; i < selectedInstructorLedTraining.length; i++) {
            if (!$("#InstructorLedTraining_" + selectedInstructorLedTraining[i]).length) {
                // add the selected instructor led training to the instructor led training list container
                var itemContainerDiv = $("<div id=\"InstructorLedTraining_" + selectedInstructorLedTraining[i] + "\">" + "<img onclick=\"javascript:RemoveInstructorLedTrainingFromCouponCode('" + selectedInstructorLedTraining[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" class=\"SmallIcon\" />" + $("#SelectEligibleInstructorLedTrainingListBox option[value='" + selectedInstructorLedTraining[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(instructorLedTrainingListContainer);
            }

            // remove the instructor led training from the select list
            $("#SelectEligibleInstructorLedTrainingListBox option[value='" + selectedInstructorLedTraining[i] + "']").remove();
        }
    }
}

// Method to remove instructor led training from listing container
function RemoveInstructorLedTrainingFromCouponCode(instructorLedTrainingId) {
    $("#InstructorLedTraining_" + instructorLedTrainingId).remove();
}

// Gets the selected instructor led training
function GetSelectedInstructorLedTrainingForHiddenField() {
    var instructorLedTrainingListContainer = $("#CouponCodeInstructorLedTrainingList_Container");
    var selectedInstructorLedTrainingField = $("#SelectedInstructorLedTraining_Field");
    var selectedInstructorLedTraining = "";

    instructorLedTrainingListContainer.children().each(function () {
        selectedInstructorLedTraining = selectedInstructorLedTraining + $(this).prop("id").replace("InstructorLedTraining_", "") + ",";
    });

    if (selectedInstructorLedTraining.length > 0)
    { selectedInstructorLedTraining = selectedInstructorLedTraining.substring(0, selectedInstructorLedTraining.length - 1); }

    selectedInstructorLedTrainingField.val(selectedInstructorLedTraining);
}

// Method to populate the items
function PopulateHiddenFieldsForDynamicElements() {
    // get all the selected catalogs
    GetSelectedCatalogsForHiddenField();

    // get all the selected courses
    GetSelectedCoursesForHiddenField();

    // get all the selected learning paths
    GetSelectedLearningPathsForHiddenField();

    // get all the selected instructor led training
    GetSelectedInstructorLedTrainingForHiddenField();
}