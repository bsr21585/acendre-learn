﻿function LoadPublishQuizSurveyModalContent(idQuizSurvey) {
    // set the quiz/survey id to publish hidden field
    $("#QuizSurveyIdToPublish").val(idQuizSurvey);

    // show the modal
    $("#" + PublishQuizSurveyModalHiddenLaunchButtonId).click();
}