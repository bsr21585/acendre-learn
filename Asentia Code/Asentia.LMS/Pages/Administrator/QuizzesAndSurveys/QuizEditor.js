﻿/*

QuizEditor

*/

QuizEditor = function (
		guid,
		identifier,
		data,
		nodeRoot,
		language, // the language that we should be using to draw the interface.
		languages,
		dictionary,
		addImagePath,
		deleteImagePath,
		flagFlagImagePath,
		textEditorInitCommand,
		textEditorGetCommand,
		textEditorInitCommandReplacer
	) {

    this.GUID = guid;
    this.nodeRoot = nodeRoot;
    this.nodeRoot.className = "QuizContainer";
    this.Languages = languages.split(","); // list of languages available in the interface
    this.Questions = []; // array
    this.DefaultLanguageRadioBoxes = []; // array
    this.IncludedLanguageCheckBoxes = []; // array
    this.AddImagePath = addImagePath;
    this.DeleteImagePath = deleteImagePath;
    this.FlagImagePath = flagFlagImagePath;
    this.IncludedLanguages = []; // array of currently selected languages
    this.DICTIONARY = JSON.parse(dictionary);
    this.identifier = identifier;

    this.TextEditorInitCommand = textEditorInitCommand;
    this.TextEditorGetCommand = textEditorGetCommand;
    this.TextEditorInitCommandReplacer = textEditorInitCommandReplacer;

    this.TYPES = {
        QUIZ: "quiz",
        SURVEY: "survey"
    }

    this.counter = 0;

    try {
        this.Data = JSON.parse(data);
    } catch (e) { }

    //
    this.nodeRoot.className = this.nodeRoot.className + " " + this.Data.ObjectType;

    // set the interface language as told

    if (language == null) {
        this.InterfaceLanguage = "en-US";
    } else {
        this.InterfaceLanguage = language.toString();
    }

    // set all the selectors to the "file" language
    try {
        this.CurrentLanguage = this.Data.DefaultLanguage.toString();
    } catch (e) {
        this.CurrentLanguage = this.InterfaceLanguage.toString();
    }

    // default language should mimic the interface language unless otherwise set

    try {
        this.DefaultLanguage = this.Data.DefaultLanguage.toString();
    } catch (e) {
        this.DefaultLanguage = this.InterfaceLanguage.toString();
    }

    this.MAXQUESTIONS = 50;

    // included languages

    try {
        for (var i = 0; i < this.Data.Titles.length; i++) {
            this.IncludedLanguages.push(this.Data.Titles[i].Language);
        }
    } catch (e) {
        this.IncludedLanguages = this.Languages.join(",").split(",");
    }

    // Tabs
    this.TabsPanel = document.createElement("div");
    this.TabsPanel.className = "TabsPanel PageCategory_LearningAssets TabsContainer";

    this.TabsUL = document.createElement("ul")
    this.TabsUL.className = "TabbedList tabs";

    this.TabsUL.appendChild(new Tab("properties", this.DICTIONARY[this.InterfaceLanguage]["properties"], true, this));
    this.TabsUL.appendChild(new Tab("questions", this.DICTIONARY[this.InterfaceLanguage]["questions"], false, this));

    this.TabsPanel.appendChild(this.TabsUL);

    nodeRoot.appendChild(this.TabsPanel);

    // Tabs Content Container

    var TabPanelsContentContainer = document.createElement("div");
    TabPanelsContentContainer.className = "TabPanelsContentContainer";

    // Properties

    this.PropertiesTabPanel = document.createElement("div");
    this.PropertiesTabPanel.className = "TabPanelContainer";

    this.SelectLanguagesFormField = new FormField(this.PropertiesTabPanel, this.DICTIONARY[this.InterfaceLanguage]["languages"], this);
    for (var i = 0; i < this.Languages.length; i++) {
        this.SelectLanguagesFormField.SetAsLanguageChooser(new LanguageChooser(this.DefaultLanguage, this.Languages[i], this));
    }

    // append the language labels
    var inst = document.createElement("div");
    inst.className = "LanguageCheckLabels";
    inst.appendChild(document.createElement("div"));
    inst.appendChild(document.createElement("div"));
    inst.appendChild(document.createElement("div"));
    inst.childNodes[0].appendChild(document.createTextNode("- " + this.DICTIONARY[this.InterfaceLanguage]["default language"]));
    inst.childNodes[1].appendChild(document.createTextNode(" "));
    inst.childNodes[2].appendChild(document.createTextNode("- " + this.DICTIONARY[this.InterfaceLanguage]["check languages to include"]));

    this.SelectLanguagesFormField.ffc.appendChild(inst);

    // build the included language array

    this.SetIncludedLanguages();

    this.IdentifierFormField = new FormField(this.PropertiesTabPanel, this.DICTIONARY[this.InterfaceLanguage]["identifier"], this, true);
    this.IdentifierFormField.SetAsText(this.identifier, "", "", "normal", false, false);

    this.IdentifierFormField.IsValidShowErrors = function () {
        this.Errors.ClearAndRemove(); // clear previous
        if (this.input.value.replace(/ /g, '').length > 0) {
            return true;
        } else { this.Errors.AddAndShow(this.parent.DICTIONARY[this.parent.InterfaceLanguage]["identifier required"]); }
    }

    this.IdentifierFormField.input.onkeypress = function (e) {
        var disallowed = [];
        disallowed[47] = true; /* / */
        disallowed[92] = true; /* \ */
        disallowed[63] = true; /* ? */
        disallowed[36] = true; /* $ */
        disallowed[60] = true; /* < */
        disallowed[62] = true; /* > */
        disallowed[34] = true; /* " */
        disallowed[39] = true; /* ' */
        if (disallowed[e.key.charCodeAt(0)] == true) {
            return false;
        }
    }

    this.IdentifierFormField.input.onpaste = function (e) {
        return false; // no pasting allowed
    }

    // TITLE

    try {
        var tmp = this.Data.Titles;
    } catch (e) {
        var tmp = "";
    }

    this.TitleFormField = new FormField(this.PropertiesTabPanel, this.DICTIONARY[this.InterfaceLanguage]["title"], this, true);
    this.TitleFormField.SetAsText(tmp, "", "", "long", true, true, "", "Value");

    this.TitleFormField.IsValidShowErrors = function () {
        this.Errors.ClearAndRemove(); // clear previous
        for (var i = 0; i < this.ffic.childNodes.length; i++) {
            if ((this.ffic.childNodes[i].childNodes[0].nodeName == "INPUT") && (this.ffic.childNodes[i].childNodes[0].lang == this.parent.DefaultLanguage)) {
                if (this.ffic.childNodes[i].childNodes[0].value.replace(/ /g, '').length > 0) {
                    return true;
                } else { this.Errors.AddAndShow(this.parent.DICTIONARY[this.parent.InterfaceLanguage]["title required in default language"]); }
            }

        }

    }

    // TYPE

    /*

    try {
        var tmp = this.Data.ObjectType;
    } catch (e) {
        var tmp = "";
    }
    this.QuizTypeFormField = new FormField(this.PropertiesTabPanel, this.DICTIONARY[this.InterfaceLanguage]["type"], this);
    var qt = new QuizTypeSelector(this.Type, tmp);
    qt.onchange = function () {
        this.parent.parent.SetQuizType(this.options[this.selectedIndex].value);
    }
    this.QuizTypeFormField.SetAsSelect("", qt);
    */


    // SCORE (Quiz only)

    if (this.Data.ObjectType == "quiz") {

        if (this.Data.MinimumPassingScore) {

            try {
                var tmp = this.Data.MinimumPassingScore * 100;
            } catch (e) {
                var tmp = "";
            }

        } else {

            var tmp = "";

        }

        this.Score = new FormField(this.PropertiesTabPanel, this.DICTIONARY[this.InterfaceLanguage]["passing score"], this);
        this.Score.SetAsText(tmp, "", "", "xshort", false, false, "%");
        this.Score.input.setAttribute("maxlength", "3");

        try {
            if (this.Data.ObjectType == this.TYPES.SURVEY) {
                this.Score.input.disabled = true;
                this.Score.input.value = "";
            }
        } catch (e) { }


        this.Score.IsValidShowErrors = function () {
            this.Errors.ClearAndRemove(); // clear previous
            if (!this.input.disabled) {
                if (this.parent.isInteger(parseInt(this.input.value))) {
                    if (this.input.value >= 0) {
                        if (this.input.value <= 100) {
                            return true;
                        } else { this.Errors.AddAndShow(this.parent.DICTIONARY[this.parent.InterfaceLanguage]["passing score invalid"]); }
                    } else { this.Errors.AddAndShow(this.parent.DICTIONARY[this.parent.InterfaceLanguage]["passing score invalid"]); }
                } else { this.Errors.AddAndShow(this.parent.DICTIONARY[this.parent.InterfaceLanguage]["passing score invalid"]); }
            } else { return true; } // no validation when disabled
        }

    }

    try {
        var tmp = new Object;
        //tmp.RandomizeQuestionOrder 				= this.Data.RandomizeQuestionOrder;
        tmp.AllowBookmarking = this.Data.AllowBookmarking;
        tmp.AllowReview = this.Data.AllowReview;
        //tmp.SinglePage 							= this.Data.SinglePage;
        tmp.RequireAnswerToContinue = this.Data.RequireAnswerToContinue;
        //tmp.ShowCorrectAnswerAfterMaxAttempts 	= this.Data.ShowCorrectAnswerAfterMaxAttempts;
        tmp.ShowFinalScore = this.Data.ShowFinalScore;

    } catch (e) {
        var tmp = new Object;
        //tmp.RandomizeQuestionOrder 				= "";
        tmp.AllowBookmarking = "";
        tmp.AllowReview = "";
        //tmp.SinglePage 							= "";
        tmp.RequireAnswerToContinue = "";
        //tmp.ShowCorrectAnswerAfterMaxAttempts 	= "";
        tmp.ShowFinalScore = "";
    }

    // OPTIONS

    this.Options = new FormField(this.PropertiesTabPanel, this.DICTIONARY[this.InterfaceLanguage]["options"], this);
    //this.RandomizeCheckBox 		= this.Options.SetAsCheckBox(tmp.RandomizeQuestionOrder, 			"", "", "", this.DICTIONARY[this.InterfaceLanguage]["randomize questions"]);
    this.BookmarkingCheckBox = this.Options.SetAsCheckBox(tmp.AllowBookmarking, "", "", "", this.DICTIONARY[this.InterfaceLanguage]["allow bookmarking"]);
    this.AllowReviewCheckBox = this.Options.SetAsCheckBox(tmp.AllowReview, "", "", "", this.DICTIONARY[this.InterfaceLanguage]["allow user to review"]);
    //this.SinglePageCheckBox 	= this.Options.SetAsCheckBox(tmp.SinglePage, 						"", "", "", this.DICTIONARY[this.InterfaceLanguage]["display single page"]);

    if (this.Data.ObjectType == "quiz") {
        this.RequireAnswerCheckBox = this.Options.SetAsCheckBox(tmp.RequireAnswerToContinue, "", "", "", this.DICTIONARY[this.InterfaceLanguage]["require answers to continue"]);
    }

    //this.ShowCorrectCheckBox 	= this.Options.SetAsCheckBox(tmp.ShowCorrectAnswerAfterMaxAttempts, "", "", "", this.DICTIONARY[this.InterfaceLanguage]["display correct answer"]);

    if (this.Data.ObjectType == "quiz") {

        this.ShowScoreCheckBox = this.Options.SetAsCheckBox(tmp.ShowFinalScore, "", "", "", this.DICTIONARY[this.InterfaceLanguage]["show final score"]);

        this.ShowScoreCheckBox.onchange = function () {
            if (this.checked == true) {
                this.parent.parent.Summary.Disable(true);
            } else {
                this.parent.parent.Summary.Disable(false);
            }
        }
        try { // disabled?
            if (this.Data.ObjectType == this.TYPES.SURVEY) {
                this.ShowScoreCheckBox.disabled = true;
                this.ShowScoreCheckBox.checked = false;
            }
        } catch (e) { }
    }

    // SUMMARY

    try {
        var tmp = this.Data.Summaries;
    } catch (e) {
        var tmp = "";
    }

    this.Summary = new FormField(this.PropertiesTabPanel, this.DICTIONARY[this.InterfaceLanguage]["conclusion summary"], this);

    if (this.Data.ObjectType == "quiz") {

        if (this.ShowScoreCheckBox.checked == true) {
            this.Summary.SetAsText("", "", "", "long", true, true, "", "Value");
            this.Summary.Disable(true);
        } else {
            this.Summary.SetAsText(tmp, "", "", "long", true, true, "", "Value");
        }

    } else {

        this.Summary.SetAsText(tmp, "", "", "long", true, true, "", "Value");

    }

    TabPanelsContentContainer.appendChild(this.PropertiesTabPanel);

    // Questions

    this.QuestionsTabPanel = document.createElement("div");
    this.QuestionsTabPanel.className = "TabPanelContainer quiz-editor-hide";
    this.QuestionsTabPanel.id = "QuestionsTabPanel";

    var nqt = document.createElement("div");
    nqt.className = "NewQuestionButtonContainer";
    var nqb = document.createElement("div");
    nqb.className = "NewQuestionButtonContainer";

    this.NewQuestionTopButton = document.createElement("img");
    this.NewQuestionTopButton.className = "button";
    this.NewQuestionTopButton.setAttribute("src", this.AddImagePath);
    this.NewQuestionTopButton.Quiz = this;
    this.NewQuestionTopButton.onclick = function () {
        if (!this.disabled) {
            this.Quiz.NewQuestion(0);
        }
    }

    this.NewQuestionTopButton.disable = function () {
        this.disabled = true;
        this.className = "button quiz-editor-screen";
    }

    this.NewQuestionTopButton.enable = function () {
        this.disabled = false;
        this.className = "button";
    }

    this.NewQuestionBottomButton = document.createElement("img");
    this.NewQuestionBottomButton.className = "button";
    this.NewQuestionBottomButton.setAttribute("src", this.AddImagePath);
    this.NewQuestionBottomButton.Quiz = this;
    this.NewQuestionBottomButton.onclick = function () {
        if (!this.disabled) {
            this.Quiz.NewQuestion(1);
        }
    }

    this.NewQuestionBottomButton.disable = function () {
        this.disabled = true;
        this.className = "button quiz-editor-screen";
    }

    this.NewQuestionBottomButton.enable = function () {
        this.disabled = false;
        this.className = "button";
    }

    this.NewQuestionTopLink = document.createElement("a");
    this.NewQuestionTopLink.className = "quiz-editor";
    this.NewQuestionTopLink.appendChild(document.createTextNode(this.DICTIONARY[this.InterfaceLanguage]["new question"]));
    this.NewQuestionTopLink.Quiz = this;
    this.NewQuestionTopLink.onclick = function () {
        if (!this.disabled) {
            this.Quiz.NewQuestion(0);
        }
    }

    this.NewQuestionTopLink.disable = function () {
        this.className = "quiz-editor quiz-editor-disabled";
        this.disabled = true;
    }

    this.NewQuestionTopLink.enable = function () {
        this.className = "quiz-editor";
        this.disabled = false;
    }

    this.NewQuestionBottomLink = document.createElement("a");
    this.NewQuestionBottomLink.className = "quiz-editor";
    this.NewQuestionBottomLink.appendChild(document.createTextNode(this.DICTIONARY[this.InterfaceLanguage]["new question"]));
    this.NewQuestionBottomLink.Quiz = this;
    this.NewQuestionBottomLink.onclick = function () {
        if (!this.disabled) {
            this.Quiz.NewQuestion(1);
        }
    }

    this.NewQuestionBottomLink.disable = function () {
        this.className = "quiz-editor quiz-editor-disabled";
        this.disabled = true;
    }

    this.NewQuestionBottomLink.enable = function () {
        this.className = "quiz-editor";
        this.disabled = false;
    }

    nqt.appendChild(this.NewQuestionTopButton);
    nqt.appendChild(this.NewQuestionTopLink);

    nqb.appendChild(this.NewQuestionBottomButton);
    nqb.appendChild(this.NewQuestionBottomLink);

    this.QuestionsTabPanel.appendChild(nqt);
    this.QuestionsTabPanel.appendChild(nqb);

    try { // this will fail if there is no question data
        for (var i = 0; i < this.Data.Questions.length; i++) {
            switch (this.Data.Questions[i].QuestionType) {
                case "fill-in":  // do not show fill-in for surveys
                    switch (this.Data.ObjectType) {
                        case this.TYPES.SURVEY:
                            break;
                        default:
                            this.NewQuestion(1, this.Data.Questions[i]);
                            break;
                    }
                    break;
                case "long-fill-in": // do not show essays on quizzes
                    switch (this.Data.ObjectType) {
                        case this.TYPES.QUIZ:
                            break;
                        default:
                            this.NewQuestion(1, this.Data.Questions[i]);
                            break;
                    }
                    break;
                case "likert": // swap likert over to mc-sa on quizzes (no likerts on quizzes anymore)
                    if (this.Data.ObjectType == this.TYPES.QUIZ) {
                        this.Data.Questions[i].QuestionType = "multiple-choice-ss";
                    }
                    this.NewQuestion(1, this.Data.Questions[i]);
                    break;
                default:
                    this.NewQuestion(1, this.Data.Questions[i]);
                    break;
            }
        }
    } catch (e) {
        this.NewQuestion(0);
    }

    TabPanelsContentContainer.appendChild(this.QuestionsTabPanel);

    nodeRoot.appendChild(TabPanelsContentContainer);
}

QuizEditor.prototype.isInteger = function (num) {
    return (num ^ 0) === num;
}

QuizEditor.prototype.S4 = function () {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}

QuizEditor.prototype.S32 = function () {
    return (this.S4() + this.S4() + "-" + this.S4() + "-4" + this.S4().substr(0, 3) + "-" + this.S4() + "-" + this.S4() + this.S4() + this.S4()).toUpperCase();
}

QuizEditor.prototype.ToggleTab = function (tab) {
    switch (tab) {
        case "properties":
            this.TabsUL.childNodes[0].className = "TabbedListLI TabbedListLIOn on";
            this.TabsUL.childNodes[1].className = "TabbedListLI";
            this.PropertiesTabPanel.className = "TabPanelContainer";
            this.QuestionsTabPanel.className = "TabPanelContainer quiz-editor-hide";
            break;
        case "questions":
            this.TabsUL.childNodes[0].className = "TabbedListLI";
            this.TabsUL.childNodes[1].className = "TabbedListLI TabbedListLIOn on";
            this.PropertiesTabPanel.className = "TabPanelContainer quiz-editor-hide";
            this.QuestionsTabPanel.className = "TabPanelContainer";
            break;
    }
}



QuizEditor.prototype.NewQuestion = function (pos, data) {

    //disable button

    if (this.Questions.length >= this.MAXQUESTIONS - 1) {
        this.NewQuestionTopButton.disable();
        this.NewQuestionTopLink.disable();
        this.NewQuestionBottomButton.disable();
        this.NewQuestionBottomLink.disable();
    }

    this.NewQuestionContainer = document.createElement("div");

    if (pos == 1) {
        this.Questions.push(new QuestionEditor(data, this.NewQuestionContainer, this));
        this.QuestionsTabPanel.insertBefore(this.NewQuestionContainer, this.QuestionsTabPanel.lastChild);
    } else {
        this.Questions.unshift(new QuestionEditor(data, this.NewQuestionContainer, this));
        this.QuestionsTabPanel.insertBefore(this.NewQuestionContainer, this.QuestionsTabPanel.childNodes[1]);
    }

    // set delete question button states
    this.SetDeleteQuestionButtonsStates();

}

QuizEditor.prototype.SetDeleteQuestionButtonsStates = function (override) {

    // disable the delete button if only one question left

    if (this.Questions.length <= 1) {
        this.Questions[0].DeleteThisQuestionButton.disable();
    } else {
        for (var i = 0; i < this.Questions.length; i++) {
            this.Questions[i].DeleteThisQuestionButton.enable();
        }
    }

}

QuizEditor.prototype.DeleteQuestion = function (id) {
    //enable button
    if (this.Questions.length <= this.MAXQUESTIONS) {
        this.NewQuestionTopButton.enable();
        this.NewQuestionTopLink.enable();
        this.NewQuestionBottomButton.enable();
        this.NewQuestionBottomLink.enable();
    }
    for (var i = 0; i < this.Questions.length; i++) {
        if (id == this.Questions[i].GUID) {
            this.Questions[i].nodeRoot.className = this.Questions[i].nodeRoot.className + " QuestionAniOut";

            setTimeout("try{document.getElementById(\"" + this.QuestionsTabPanel.id + "\").removeChild(document.getElementById(\"" + this.Questions[i].nodeRoot.id + "\"));}catch(e){}", 500);

            this.Questions.splice(i, 1); // remove from the array
        }
    }

    // set delete question button states
    this.SetDeleteQuestionButtonsStates();

}

QuizEditor.prototype.GetQuestionIndex = function (key) {

    for (var i = 0; i < this.Questions.length; i++) {
        if (this.Questions.Key == key) {
            return i;
        }
    }

}

QuizEditor.prototype.GetLanguageIndex = function (l) {

    for (var i = 0; i < this.Languages.length; i++) {
        if (this.Languages[i] == l) {
            return i;
        }
    }
    return null;
}

QuizEditor.prototype.SetQuizType = function (s) {

    // alter the option inputs appropriately

    switch (s) {
        case this.TYPES.SURVEY:
            this.Score.input.disabled = true;
            this.Score.input.value = "";
            this.ShowScoreCheckBox.disabled = true;
            this.ShowScoreCheckBox.checked = false;
            break;
        case this.TYPES.QUIZ:
            this.Score.input.disabled = false;
            this.ShowScoreCheckBox.disabled = false;
            break;
    }

    // alter the conclusion inputs

    if (this.ShowScoreCheckBox.checked) {
        this.Summary.Disable(true);
    } else {
        this.Summary.Disable(false);
    }


}

QuizEditor.prototype.IsValidShowErrors = function () {

    var f = true; // valid

    //identifier
    if (!this.IdentifierFormField.IsValidShowErrors()) { f = false; }
    //title
    if (!this.TitleFormField.IsValidShowErrors()) { f = false; }
    // score

    if (this.ObjectType == this.TYPES.QUIZ) {

        if (!this.Score.IsValidShowErrors()) { f = false; }

        //questions
        for (var j = 0; j < this.Questions.length; j++) {
            if (!this.Questions[j].IsValidShowErrors()) { f = false; }
        }

    }

    return f;

}

QuizEditor.prototype.OutputGUID = function () {

    return this.GUID.OutputValue();

}

QuizEditor.prototype.OutputIdentifier = function () {

    return this.IdentifierFormField.OutputValue();

}

QuizEditor.prototype.OutputType = function () {
    return this.Data.ObjectType;
    //return this.QuizTypeFormField.OutputValue();	
}

QuizEditor.prototype.OutputJSON = function (h) {

    var s = "";

    s += "{";

    s += "\"Titles\": [";
    for (var i = 0; i < this.IncludedLanguages.length; i++) {

        if (i != 0) {
            s += ",";
        }
        s += "{";
        s += "\"Language\": \"" + this.IncludedLanguages[i] + "\",";
        s += "\"Value\": \"" + this.TitleFormField.OutputValue(this.IncludedLanguages[i]) + "\"";
        s += "}";
    }
    s += "],";

    s += "\"Summaries\": [";
    for (var i = 0; i < this.IncludedLanguages.length; i++) {

        if (i != 0) {
            s += ",";
        }
        s += "{";
        s += "\"Language\": \"" + this.IncludedLanguages[i] + "\",";
        s += "\"Value\": \"" + this.Summary.OutputValue(this.IncludedLanguages[i]) + "\"";
        s += "}";
    }
    s += "],";
    s += "\"ObjectId\": \"" + this.GUID + "\",";
    s += "\"ObjectType\": \"" + this.OutputType() + "\",";
    //s += "\"RandomizeQuestionOrder\": " + this.RandomizeCheckBox.checked + ",";
    try { s += "\"MinimumPassingScore\": \"" + this.Score.OutputValue() / 100 + "\","; } catch (e) { }
    s += "\"DefaultLanguage\": \"" + this.DefaultLanguage + "\",";
    //s += "\"SinglePage\": " + this.SinglePageCheckBox.checked + ",";
    s += "\"AllowBookmarking\": " + this.BookmarkingCheckBox.checked + ",";
    s += "\"AllowReview\": " + this.AllowReviewCheckBox.checked + ",";
    try { s += "\"RequireAnswerToContinue\": " + this.RequireAnswerCheckBox.checked + ","; } catch (e) { }
    //s += "\"ShowCorrectAnswerAfterMaxAttempts\": " + this.ShowCorrectCheckBox.checked + ",";
    try { s += "\"ShowFinalScore\": " + this.ShowScoreCheckBox.checked + ","; } catch (e) { }

    s += "\"Questions\": [";

    for (var i = 0; i < this.Questions.length; i++) {

        if (i != 0) {
            s += ",";
        }

        s += this.Questions[i].OutputJSON();

    }

    s += "]";

    s += "}";

    return s;
}

QuizEditor.prototype.SetIncludedLanguages = function () {

    // rebuild the array

    this.IncludedLanguages = [];

    for (var i = 0; i < this.IncludedLanguageCheckBoxes.length; i++) {
        if (this.IncludedLanguageCheckBoxes[i].checked == true) {
            this.IncludedLanguages.push(this.IncludedLanguageCheckBoxes[i].value);
        }
    }

    try {
        this.TitleFormField.RefreshInterfaceForLanguages();
        this.Summary.RefreshInterfaceForLanguages();

        for (var i = 0; i < this.Questions.length; i++) {
            this.Questions[i].RefreshInterfaceForLanguages();
        }
    } catch (e) { }

}

QuizEditor.prototype.SetDefaultLanguage = function (l) {

    this.DefaultLanguage = l;
    this.CurrentLanguage = l;

    for (var i = 0; i < this.IncludedLanguageCheckBoxes.length; i++) {
        if (this.IncludedLanguageCheckBoxes[i].value == this.DefaultLanguage) {
            this.IncludedLanguageCheckBoxes[i].checked = true;
            this.IncludedLanguageCheckBoxes[i].disabled = true;
        } else {
            this.IncludedLanguageCheckBoxes[i].disabled = false;
        }
    }

    this.SetIncludedLanguages();

}

//
//
// o	: the JSON object (should be an array that can be looped)
// l	: the language to search for
// v	: the key who's value gets returned for the found language

QuizEditor.GetLanguageValue = function (o, l, v) {

    try {
        for (var i = 0; i < o.length; i++) {

            if (o[i]["Language"] == l) {
                return o[i][v];
            }
        }
    } catch (e) {
        return "";
    }
    return "";

}


/*

LanguageChooser

*/

LanguageChooser = function (dl, l, parent) {

    this.parent = parent;
    this.FlagImagePath = this.parent.FlagImagePath;
    this.IncludedLanguages = this.parent.IncludedLanguages;

    // check that language is in the included list

    var idx = -1;
    for (var i = 0; i < this.IncludedLanguages.length; i++) {
        if (this.IncludedLanguages[i] == l) {
            idx = i;
        }
    }

    // draw the HTML

    var d = document.createElement("div");
    d.className = "QuizLanguageChooser";
    var r = document.createElement("input");
    r.setAttribute("type", "radio");
    r.id = "QuizDefaultLanguageChooser-" + l;
    r.name = "QuizDefaultLanguageChooser";
    r.value = l;
    r.parent = parent;
    if (dl == l) {
        r.checked = true;
    }
    r.onclick = function () {
        this.parent.SetDefaultLanguage(this.value);
    }
    var c = document.createElement("input");
    c.setAttribute("type", "checkbox");
    c.id = "QuizIncludedLanguageChooser-" + l;
    c.name = "QuizIncludedLanguageChooser";
    c.value = l;

    if (idx >= 0) {
        c.checked = true;
    }

    c.parent = parent;
    if (dl == l) {
        c.checked = true;
        c.disabled = true;
    }
    c.onclick = function () {
        this.parent.SetIncludedLanguages();
    }

    var img = document.createElement("img");
    img.setAttribute("src", this.FlagImagePath + "/" + l + ".png");

    this.parent.DefaultLanguageRadioBoxes.push(r);
    this.parent.IncludedLanguageCheckBoxes.push(c);

    d.appendChild(r);
    d.appendChild(img);
    d.appendChild(c);

    return d;
}


/*

FormField

*/

FormField = function (
		nodeRoot,
		labelName,
		parent,
		htmlRequiredElement
	) {

    this.parent = parent;
    this.nodeRoot = nodeRoot;
    this.InterfaceLanguage = parent.InterfaceLanguage;
    this.Languages = parent.Languages;
    this.inputs = []; // array of checkboxes if needed
    this.FlagImagePath = this.parent.FlagImagePath;
    this.CurrentLanguage = this.parent.CurrentLanguage.toString();

    this.Types = {
        "TEXT": "text",
        "TEXTAREA": "textarea",
        "CHECKBOX": "checkbox",
        "SELECTBOX": "seletbox"
    }

    this.ffc = document.createElement("div");
    this.ffc.className = "FormFieldContainer";

    this.nodeRoot.appendChild(this.ffc);

    // label

    if (labelName != null && labelName != "") {
        this.fflc = document.createElement("div");
        this.fflc.className = "FormFieldLabelContainer";
        this.ffc.appendChild(this.fflc);


        var label = document.createElement("label");
        label.appendChild(document.createTextNode(labelName + ":"));

        this.fflc.appendChild(label);

        if (htmlRequiredElement == true) {
            var req = document.createElement("span");
            req.className = "req RequiredAsterisk";
            req.appendChild(document.createTextNode(" *"));
            this.fflc.appendChild(req);
        }

    }

    // attach the error messager
    this.Errors = new ErrorAlerter(this, this.ffc);

    this.ffic = document.createElement("div");
    this.ffic.className = "FormFieldInputContainer";
    this.ffc.appendChild(this.ffic);

}

FormField.prototype.RefreshInterfaceForLanguages = function () {

    this.ffic.removeChild(this.LanguageSelector);

    // if the currently selected language is no longer in the available list, update the selector and the current input

    for (var i = 0; i < this.parent.IncludedLanguages.length; i++) {
        if (this.parent.IncludedLanguages[i] == this.CurrentLanguage) {

            // current language remains; keep it as selected. no need to change the inputs.
            this.LanguageSelector = new LanguageSelector(this, this.CurrentLanguage, this.parent.IncludedLanguages);
            this.ffic.appendChild(this.LanguageSelector);
            return true;


        }
    }

    // current language removed, update select and display to default
    this.SetLanguage(this.parent.DefaultLanguage);
    this.LanguageSelector = new LanguageSelector(this, this.CurrentLanguage, this.parent.IncludedLanguages);
    this.ffic.appendChild(this.LanguageSelector);

}

FormField.prototype.SetAsCheckBox = function (data, id, name, className, description) {

    this.InputType = this.Types.CHECKBOX;
    this.IsSingle = true;

    var d = document.createElement("div");
    d.className = "quiz-editor-option";

    var j = this.inputs.length;

    this.inputs[j] = document.createElement("input");
    this.inputs[j].parent = this;
    this.inputs[j].type = "checkbox";
    this.inputs[j].className = className;
    this.inputs[j].id = "prop_" + Math.random().toString(36).slice(-5);

    // insert the data

    if (data == true) {
        this.inputs[j].checked = true;
    }

    var desc = document.createElement("label");
    desc.setAttribute("for", this.inputs[j].id);
    desc.appendChild(document.createTextNode(description));

    this.ffic.appendChild(d);

    d.appendChild(this.inputs[j]);
    d.appendChild(desc);

    return this.inputs[j];
}

FormField.prototype.SetAsText = function (
		data,
		id,
		name,
		className,
		useLanguages,
		useLanguageSelector,
		description,
		nodeName
	) {

    this.InputType = this.Types.TEXT;

    if (useLanguages == true) {

        this.IsSingle = false;

        for (var i = 0; i < this.Languages.length; i++) {

            var v = QuizEditor.GetLanguageValue(data, this.Languages[i], nodeName);
            var g = this.parent.GUID + "_qt_" + this.Languages[i];

            if (this.Languages[i] == this.parent.CurrentLanguage) {
                var d = document.createElement("div");
                d.className = "inline-text";
                d.setAttribute("lang", this.Languages[i]);
                var h = new HTMLTextInput(g, "", "text", className, this.Languages[i], v, 255);	// hide
            } else {
                var d = document.createElement("div");
                d.className = "quiz-editor-hide";
                d.setAttribute("lang", this.Languages[i]);
                var h = new HTMLTextInput(g, "", "text", className, this.Languages[i], v, 255);
            }

            d.appendChild(h);
            this.ffic.appendChild(d);

        }

        if (description != null && description != "") {
            this.ffic.appendChild(document.createTextNode(description));
        }

        if (useLanguageSelector) {
            this.LanguageSelector = new LanguageSelector(this, this.CurrentLanguage, this.parent.IncludedLanguages);
            this.ffic.appendChild(this.LanguageSelector);
        }

    } else { // single input

        this.IsSingle = true;

        this.input = new HTMLTextInput(id, name, "text", className, "", "", 255);
        this.input.value = data;

        this.ffic.appendChild(this.input);
        if (description != null && description != "") {
            this.ffic.appendChild(document.createTextNode(description));
        }

    }



}

FormField.prototype.SetAsTextArea = function (
		data,
		id,
		name,
		className,
		useLanguages,
		useLanguageSelector,
		nodeName,
		isWYSIWYG,
		textEditorInitCommand,
		textEditorGetCommand,
		textEditorReplacer
		) {

    this.InputType = this.Types.TEXTAREA;
    this.WYSIWYG = isWYSIWYG;
    this.TextEditorInitCommand = textEditorInitCommand;
    this.TextEditorGetCommand = textEditorGetCommand;
    this.TextEditorReplacer = textEditorReplacer;

    if ((textEditorInitCommand == "") || (!textEditorInitCommand)) {
        //alert("nope!")
    }

    if (useLanguages == true) {

        this.IsSingle = false;

        for (var i = 0; i < this.Languages.length; i++) {

            var v = QuizEditor.GetLanguageValue(data, this.Languages[i], nodeName);
            var g = this.parent.GUID + "_qt_" + this.Languages[i];

            if (this.Languages[i] == this.parent.CurrentLanguage) {
                var d = document.createElement("div");
                d.className = "inline-block";
                d.setAttribute("lang", this.Languages[i]);
                var h = new HTMLTextInput(g, "", "textarea", className, this.Languages[i], v, 512);
            } else {
                var d = document.createElement("div");
                d.className = "quiz-editor-hide";
                d.setAttribute("lang", this.Languages[i]);
                var h = new HTMLTextInput(g, "", "textarea", className, this.Languages[i], v, 512);
            }

            d.appendChild(h);
            this.ffic.appendChild(d);

            if (this.WYSIWYG == true) {
                try {
                    // don't use the THIS. version of the vars here.
                    setTimeout(textEditorInitCommand.replace(new RegExp(textEditorReplacer, 'g'), g), 100);
                    h.className = "quiz-editor-hide"; // this MUST be second
                } catch (e) { };
            }

        }

        if (useLanguageSelector) {
            this.LanguageSelector = new LanguageSelector(this, this.CurrentLanguage, this.parent.IncludedLanguages);
            this.ffic.appendChild(this.LanguageSelector);
        }

    } else { // single input

        this.IsSingle = true;

        var g = this.parent.GUID + "_qt";

        this.input = new HTMLTextInput(g, name, "textarea", className, "", "", 512);
        this.input.value = data;
        this.ffic.appendChild(this.input);

        if (this.WYSIWYG == true) {
            try {
                setTimeout(textEditorInitCommand.replace(new RegExp(textEditorReplacer, 'g'), g), 100);
                h.className = "quiz-editor-hide"; // this MUST be second
            } catch (e) { };
        }

    }

}

FormField.prototype.SetAsSelect = function (data, HTMLSelector) {

    this.InputType = this.Types.SELECTBOX;
    this.IsSingle = true;
    this.ffic.appendChild(HTMLSelector);
}

FormField.prototype.SetAsLanguageChooser = function (HTMLDOM) {
    this.ffic.className = "FormFieldInputContainer LanguageChooserInputContainer"
    this.ffic.appendChild(HTMLDOM);
}

FormField.prototype.SetLanguage = function (l) {

    this.CurrentLanguage = l;

    // this.input object is only available when the form field is created 
    // with the 'useLanguages' flag set to false.

    if (this.input) {
        // no language options
        return true;
    } else {
        for (var i = 0; i < this.Languages.length; i++) {
            if (this.ffic.childNodes[i].getAttribute("lang") == l) {
                switch (this.InputType) {
                    case this.Types.TEXT:
                        this.ffic.childNodes[i].className = "inline-text";	// show
                        break;
                    case this.Types.TEXTAREA:
                        this.ffic.childNodes[i].className = "";	// show
                        break;
                }
            } else {
                this.ffic.childNodes[i].className = "quiz-editor-hide";	// hide
            }
        }
    }

}

FormField.prototype.Disable = function (bln) {

    if (this.IsSingle == true) {
        this.input.disabled = bln;
        if (bln == true)
            this.input.value = ""
    } else {
        for (var i = 0; i < this.ffic.childNodes.length; i++) {
            if (this.ffic.childNodes[i].childNodes[0].nodeName == "INPUT") {
                this.ffic.childNodes[i].childNodes[0].disabled = bln;
                if (bln == true)
                    this.ffic.childNodes[i].childNodes[0].value = ""
            }
        }
    }

    return true;

}

FormField.prototype.OutputValue = function (l) {

    var f;

    // single text input
    switch (this.InputType) {
        case this.Types.TEXT:
            if (this.IsSingle == true) {
                f = this.input.value;
            } else {
                for (var i = 0; i < this.ffic.childNodes.length; i++) {
                    if (this.ffic.childNodes[i].getAttribute("lang") == l) {
                        f = this.ffic.childNodes[i].childNodes[0].value;
                    }
                }
            }
            break;
        case this.Types.TEXTAREA:
            if (this.IsSingle == true) {
                f = this.input.value;
            } else {
                // extract wysiwyg values

                for (var i = 0; i < this.ffic.childNodes.length; i++) {
                    if (this.ffic.childNodes[i].getAttribute("lang") == l) {

                        if (this.WYSIWYG) {
                            for (var j = 0; j < this.ffic.childNodes[i].childNodes.length; j++) {
                                if (this.ffic.childNodes[i].childNodes[j].type == "textarea") {
                                    if (this.TextEditorGetCommand != null) {
                                        eval("document.getElementById(\"" + this.ffic.childNodes[i].childNodes[j].id + "\").value = " + this.TextEditorGetCommand.replace(new RegExp(this.TextEditorReplacer, 'g'), this.ffic.childNodes[i].childNodes[j].id));
                                    }
                                    f = (this.ffic.childNodes[i].childNodes[j].value);
                                }
                            }

                        } else {
                            f = this.ffic.childNodes[i].childNodes[0].childeNodes[0].value;
                        }
                    }
                }
            }
            //f = null;
            break;
        case this.Types.CHECKBOX:
            break;
        case this.Types.SELECTBOX:
            f = this.ffic.childNodes[0].options[this.ffic.childNodes[0].selectedIndex].value;
            break;

    }

    // escape for JSON
    if (f != null) {
        f = f.replace(/\\/g, "\\\\");
        f = f.replace(/"/g, "\\\"");
        f = f.replace(/\r/g, "");
        f = f.replace(/\n/g, "");
        f = f.replace(/\t/g, "");
    }
    return f;

}

/*-----------------------------------------

ErrorAlerter

*/

ErrorAlerter = function (parent, nodeRoot) {

    this.parent = parent;
    this.nodeRoot = nodeRoot; // the node we are attaching to (FFC)
    this.collection = [];

}

ErrorAlerter.prototype.AddAndShow = function (s) {
    this.Add(s);
    this.Show();
}

ErrorAlerter.prototype.ClearAndRemove = function (s) {
    this.Clear();
    this.Remove();
}

ErrorAlerter.prototype.Add = function (s) {
    this.collection.push(s);
    return this.collection.length;
}

ErrorAlerter.prototype.Clear = function (s) {
    this.collection = [];
    return this.collection.length;
}

ErrorAlerter.prototype.Show = function () {

    // remove previous
    this.Remove();

    this.ErrorsContainer = document.createElement("div");
    this.ErrorsContainer.className = "FormFieldErrorContainer";

    for (var i = 0; i < this.collection.length; i++) {
        var p = document.createElement("p");
        p.appendChild(document.createTextNode(this.collection[i]))
        this.ErrorsContainer.appendChild(p);
    }

    this.nodeRoot.insertBefore(this.ErrorsContainer, this.nodeRoot.childNodes[1]);

}

ErrorAlerter.prototype.Remove = function () {
    try {
        this.nodeRoot.removeChild(this.ErrorsContainer);
    } catch (e) { }
}

ErrorAlerter.prototype.count = function () {
    return this.collection.length;
}

/*-----------------------------------------

HTMLTextInput

*/

HTMLTextInput = function (
		id,
		name,
		type,
		className,
		language,
		value,
		max
	) {

    if (type == "textarea") {
        var t = document.createElement("textarea");
    } else {
        var t = document.createElement("input");
    }

    t.id = id;
    t.name = name;
    t.type = type;
    t.className = className;
    t.value = value;
    if (max) {
        t.maxLength = max;
    }

    if (language)
        t.setAttribute("lang", language);

    return t;
}

/*-----------------------------------------

Tab

*/

Tab = function (
		id,
		name,
		isOn,
		quiz
	) {

    var onClass = "";

    if (isOn)
        onClass = " TabbedListLIOn on";

    var li = document.createElement("li");
    li.className = "TabbedListLI" + onClass;
    li.Quiz = quiz;
    li.onclick = function () {
        this.Quiz.ToggleTab(id.toLowerCase());
    }

    var div = document.createElement("div");
    div.appendChild(document.createTextNode(name));

    var a = document.createElement("a");
    a.className = "TabLink";
    a.setAttribute("href", "javascript:void(0);");
    a.appendChild(document.createTextNode(name));


    li.appendChild(div);
    //div.appendChild(a);

    return li;

}

/*-----------------------------------------

QuestionEditor

*/

QuestionEditor = function (
		data,
		nodeRoot,
		parent
	) {

    this.Data = data;
    this.parent = parent;
    this.nodeRoot = nodeRoot;
    this.nodeRoot.className = "QuestionContainer QuestionAniIn";
    this.Choices = [];
    this.InterfaceLanguage = this.parent.InterfaceLanguage;
    this.Languages = this.parent.Languages;
    this.MAXCHOICES = 10;
    this.FlagImagePath = this.parent.FlagImagePath;
    this.AddImagePath = this.parent.AddImagePath;
    this.DeleteImagePath = this.parent.DeleteImagePath;
    this.CurrentLanguage = this.parent.CurrentLanguage.toString();

    this.QUESTION_TYPES = {
        FILL_IN: "fill-in",
        TRUE_FALSE: "true-false",
        MC_MULTIPLE: "multiple-choice-ms",
        MC_SINGLE: "multiple-choice-ss",
        LIKERT: "likert",
        ESSAY: "long-fill-in"
    }

    try {
        this.GUID = this.Data.QuestionId;
    }
    catch (e) {
        this.GUID = this.parent.S32();
    }

    this.nodeRoot.id = this.GUID;

    // Delete Question Button

    this.DeleteThisQuestionButton = document.createElement("img");
    this.DeleteThisQuestionButton.className = "button button-deletequestion";
    this.DeleteThisQuestionButton.setAttribute("src", this.DeleteImagePath);
    this.DeleteThisQuestionButton.FlagImagePath = this.FlagImagePath;
    this.DeleteThisQuestionButton.Question = this;
    this.DeleteThisQuestionButton.onclick = function () {
        if (this.enabled) {
            this.Question.parent.DeleteQuestion(this.Question.GUID);
        }
    }
    this.DeleteThisQuestionButton.disable = function () {
        this.enabled = false;
        this.className = "button button-deletequestion quiz-editor-screen";
    }
    this.DeleteThisQuestionButton.enable = function () {
        this.enabled = true;
        this.className = "button button-deletequestion";
    }

    // Language Selector

    this.LanguageSelector = new LanguageSelector(this, this.CurrentLanguage, this.parent.IncludedLanguages);
    this.LanguageSelector.onchange = function () {
        this.parent.SetLanguage(this.options[this.selectedIndex].value);
    }

    this.qi = document.createElement("div");
    this.qi.className = "QuestionInterfaceContainer";

    this.qi.appendChild(document.createTextNode(this.parent.DICTIONARY[this.InterfaceLanguage]["question"]));
    this.qi.appendChild(this.DeleteThisQuestionButton);
    this.qi.appendChild(this.LanguageSelector);

    this.nodeRoot.appendChild(this.qi);

    // Description
    try {
        var tmp = this.Data.QuestionDescription;
    } catch (e) {
        var tmp = "";
    }
    this.Description = new FormField(this.nodeRoot, this.parent.DICTIONARY[this.InterfaceLanguage]["description"], this, true);
    this.Description.SetAsText(tmp, "", "", "normal", false);

    this.Description.IsValidShowErrors = function () {
        this.Errors.ClearAndRemove(); // clear previous
        if (this.input.value.replace(/ /g, '').length > 0) {
            return true;
        } else { this.Errors.AddAndShow(this.parent.parent.DICTIONARY[this.parent.parent.InterfaceLanguage]["description required"]); }
    }

    // Question Type

    try {
        this.QuestionType = this.Data.QuestionType;
    } catch (e) {
        this.QuestionType = this.QUESTION_TYPES.MC_SINGLE;
    }

    this.QuestionTypeFormField = new FormField(this.nodeRoot, this.parent.DICTIONARY[this.InterfaceLanguage]["type"], this);
    var qt = new QuestionTypeSelector(this.QuestionTypeFormField, this.QuestionType);
    qt.onchange = function () {
        this.parent.parent.SetQuestionType(this.options[this.selectedIndex].value);
    }
    this.QuestionTypeFormField.SetAsSelect("", qt);

    // Question Text

    try {
        var qd = this.Data.QuestionDetails;
    } catch (e) {
        var qd = "";
    }
    this.QuestionText = new FormField(this.nodeRoot, this.parent.DICTIONARY[this.InterfaceLanguage]["text"], this, true);
    this.QuestionText.SetAsTextArea(qd, "", "", "", true, false, "QuestionHtml", true, this.parent.TextEditorInitCommand, this.parent.TextEditorGetCommand, this.parent.TextEditorInitCommandReplacer);

    this.QuestionText.IsValidShowErrors = function () {
        this.Errors.ClearAndRemove(); // clear previous
        if (this.parent.IsLanguageEntered(this.parent.parent.DefaultLanguage)) {
            return true;
        } else { this.Errors.AddAndShow(this.parent.parent.DICTIONARY[this.parent.parent.InterfaceLanguage]["question text required in default language"]); }
    }

    // Max Attempts

    if (this.parent.Data.ObjectType == this.parent.TYPES.QUIZ) {

        try {
            var tmp = this.Data.MaxAttempts;
        } catch (e) {
            var tmp = "";
        }

        this.MaxAttempts = new FormField(this.nodeRoot, this.parent.DICTIONARY[this.InterfaceLanguage]["maximum attempts"], this, false);
        //this.MaxAttempts.SetAsText(tmp, "", "maxAttempts", "xshort", false, false);
        this.MaxAttempts.SetAsSelect("", new QuizAttemptsSelector(this, tmp));

    }

    // choices
    this.ChoicesContainer = document.createElement("div");

    switch (this.QuestionType) {
        case this.QUESTION_TYPES.ESSAY:
            this.ChoicesContainer.className = "ChoicesContainer essay";
            break;
        default:
            this.ChoicesContainer.className = "ChoicesContainer";
            break;
    }

    // text
    this.fflc = document.createElement("div");
    this.fflc.className = "FormFieldLabelContainer";
    var ffl = document.createElement("label");
    ffl.appendChild(document.createTextNode(this.parent.DICTIONARY[this.InterfaceLanguage]["answer choices"] + ":"));
    this.fflc.appendChild(ffl);
    this.ChoicesContainer.appendChild(this.fflc);

    // Errors
    this.ChoicesErrors = new ErrorAlerter(this, this.fflc)

    // New Choice Button (create first so it exists before choices added, append immediately because new choices are inserted before lastNode [which is the new choice button])

    this.NewChoiceButton = document.createElement("img");
    this.NewChoiceButton.setAttribute("src", this.AddImagePath);
    this.NewChoiceButton.className = "button button-addquestion";
    this.NewChoiceButton.FlagImagePath = this.FlagImagePath;
    this.NewChoiceButton.Question = this;
    this.NewChoiceButton.enabled = true;
    this.NewChoiceButton.onclick = function () {
        if (this.enabled == true)
            this.Question.NewChoice();
    }
    this.NewChoiceButton.disable = function () {
        this.enabled = false;
        this.className = "button button-addquestion quiz-editor-screen";
    }
    this.NewChoiceButton.enable = function () {
        this.enabled = true;
        this.className = "button button-addquestion";
    }

    // append immediately

    var d = document.createElement("div");
    d.className = "ChoicesContainerButtons";
    d.appendChild(this.NewChoiceButton);

    //this.nodeRoot.appendChild(d);
    this.ChoicesContainer.appendChild(d);

    // Choices

    // qd was already extracted earlier
    // reformat data to pass to choice objects

    if (typeof qd == "object") {

        for (var i = 0; i < qd[0].Choices.length; i++) {  // for each choice (use the qd[0] object, loop its Choices simply so we do the correct number)

            var c = new Object;
            c.Strings = [];

            for (var j = 0; j < qd.length; j++) {
                c.IsCorrect = qd[j].Choices[i].IsCorrect;
                c.Strings[qd[j].Language] = { Text: qd[j].Choices[i].Text, Feedback: qd[j].Choices[i].Feedback };
            }
            this.NewChoice(c, this.QuestionType);
        }
    } else {
        this.NewChoice("", this.QuestionType);
        this.NewChoice("", this.QuestionType);
    }

    // append to screen
    nodeRoot.appendChild(this.ChoicesContainer)

    this.SetNewChoiceButtonState();

    // remove the class animation
    setTimeout("try{document.getElementById(\"" + this.nodeRoot.id + "\").className = \"QuestionContainer\"}catch(e){};", 500);

}

QuestionEditor.prototype.RefreshInterfaceForLanguages = function () {

    this.qi.removeChild(this.LanguageSelector);

    // loop through each included language

    for (var i = 0; i < this.parent.IncludedLanguages.length; i++) {

        // if the set language selected is still in the list

        if (this.parent.IncludedLanguages[i] == this.CurrentLanguage) {

            // current language remains; keep it as selected. no need to change the inputs.
            this.LanguageSelector = new LanguageSelector(this, this.CurrentLanguage, this.parent.IncludedLanguages);
            this.qi.appendChild(this.LanguageSelector);
            return true;


        }
    }

    // the set language for this question has been removed from the available list; set language for question back to the default (per the data).
    this.SetLanguage(this.parent.DefaultLanguage);

    // rebuild the language selector with the correct options
    this.LanguageSelector = new LanguageSelector(this, this.CurrentLanguage, this.parent.IncludedLanguages);
    // append the language selector
    this.qi.appendChild(this.LanguageSelector);
}

QuestionEditor.prototype.SetQuestionType = function (t) {

    this.QuestionType = t;

    switch (t) {
        case this.QUESTION_TYPES.TRUE_FALSE:
            // 

            this.ChoicesContainer.className = "ChoicesContainer true-false";

            // add inputs if we are short
            while (this.Choices.length < 2) {
                this.NewChoice();
            }
            // fill in the true-false verbiage for languages
            for (var i = 0; i < this.parent.Languages.length; i++) {
                this.Choices[0].nodeRoot.querySelectorAll("input[type=text]")[this.parent.GetLanguageIndex(this.parent.Languages[i])].value = this.parent.DICTIONARY[this.parent.Languages[i]]["true"];
                this.Choices[1].nodeRoot.querySelectorAll("input[type=text]")[this.parent.GetLanguageIndex(this.parent.Languages[i])].value = this.parent.DICTIONARY[this.parent.Languages[i]]["false"];
            }
            // disable all inputs
            for (var i = 0; i < this.Choices.length; i++) {
                for (var j = 0; j < this.Choices[i].nodeRoot.childNodes.length; j++) {
                    if (this.Choices[i].nodeRoot.childNodes[j].type == "text") {
                        this.Choices[i].nodeRoot.childNodes[j].disabled = true;
                    }
                }
            }
            // remove extra choices
            while (this.Choices.length > 2) {
                this.DeleteChoice(this.Choices[this.Choices.length - 1].id);
            }
            break;

        case this.QUESTION_TYPES.ESSAY:

            //
            this.ChoicesContainer.className = "ChoicesContainer essay";

            // remove all inputs
            while (this.Choices.length > 0) {
                this.DeleteChoice(this.Choices[this.Choices.length - 1].id);
            }

            break;

        default: // multiple-choice, likert
            //

            this.ChoicesContainer.className = "ChoicesContainer";

            // add inputs if we are short
            while (this.Choices.length < 2) {
                this.NewChoice();
            }

            // enable all inputs
            for (var i = 0; i < this.Choices.length; i++) {
                for (var j = 0; j < this.Choices[i].nodeRoot.childNodes.length; j++) {
                    if (this.Choices[i].nodeRoot.childNodes[j].type == "text") {
                        this.Choices[i].nodeRoot.childNodes[j].disabled = false;
                    }
                }
            }
            break;
    }
    this.SetCheckBoxesStates();
    this.SetNewChoiceButtonState();
    this.SetDeleteChoiceButtonStates();
}

QuestionEditor.prototype.SetCheckBoxesStates = function () {

    switch (this.QuestionTypeFormField.OutputValue()) {
        case this.QUESTION_TYPES.MC_SINGLE:
        case this.QUESTION_TYPES.TRUE_FALSE:
            // change type
            for (var i = 0; i < this.Choices.length; i++) {
                this.Choices[i].CheckBox.type = "radio";
            }
            // changed disabled
            for (var i = 0; i < this.Choices.length; i++) {
                this.Choices[i].CheckBox.disabled = false;
            }
            break;
        case this.QUESTION_TYPES.FILL_IN:
            //change type to checkboxes
            for (var i = 0; i < this.Choices.length; i++) {
                this.Choices[i].CheckBox.type = "checkbox";
            }
            // set all checks to checked/disabled
            for (var i = 0; i < this.Choices.length; i++) {
                this.Choices[i].CheckBox.disabled = true;
                this.Choices[i].CheckBox.checked = true;
            }
            break;
        default: // multiple-choice, likert
            for (var i = 0; i < this.Choices.length; i++) {
                this.Choices[i].CheckBox.type = "checkbox";
            }
            // changed disabled
            for (var i = 0; i < this.Choices.length; i++) {
                this.Choices[i].CheckBox.disabled = false;
            }
            break;
    }
}

QuestionEditor.prototype.SetNewChoiceButtonState = function (override) {

    switch (override) {
        case true:
            this.NewChoiceButton.enable();
            break;
        case false:
            this.NewChoiceButton.enable();
            break;
        default:
            switch (this.QuestionTypeFormField.OutputValue()) {
                case this.QUESTION_TYPES.TRUE_FALSE:
                    this.NewChoiceButton.disable();
                    break;
                default: // multiple-choice, likert, fill-in
                    if (this.Choices.length >= this.MAXCHOICES) {
                        this.NewChoiceButton.disable();
                    } else {
                        this.NewChoiceButton.enable();
                    }
                    break;
            }
            break;

    }
}

QuestionEditor.prototype.SetDeleteChoiceButtonStates = function (override) {

    switch (override) {
        case true:
            for (var i = 0; i < this.Choices.length; i++) {
                this.Choices[i].DeleteThisChoiceButton.enable();
            }
            break;
        case false:
            for (var i = 0; i < this.Choices.length; i++) {
                this.Choices[i].DeleteThisChoiceButton.disable();
            }
            break;
        default:
            switch (this.QuestionTypeFormField.OutputValue()) {
                case this.QUESTION_TYPES.FILL_IN:
                    var minChoices = 1;
                    break;
                default: // multiple-choice, likert, true-false
                    var minChoices = 2;
                    break;
            }

            if (this.Choices.length <= minChoices) {
                for (var i = 0; i < this.Choices.length; i++) {
                    this.Choices[i].DeleteThisChoiceButton.disable();
                }
            } else {
                for (var i = 0; i < this.Choices.length; i++) {
                    this.Choices[i].DeleteThisChoiceButton.enable();
                }
            }
            break;
    }
}

QuestionEditor.prototype.IsValidShowErrors = function () {

    var f = true; // valid

    //description
    if (!this.Description.IsValidShowErrors()) { f = false; }
    //question test
    if (!this.QuestionText.IsValidShowErrors()) { f = false; }

    //choices
    var g = true;
    this.ChoicesErrors.ClearAndRemove();
    for (var j = 0; j < this.Choices.length; j++) {
        if (!this.Choices[j].IsLanguageEntered(this.parent.DefaultLanguage)) {
            g = false;
        }
    }

    if (!g) {
        this.ChoicesErrors.AddAndShow(this.parent.DICTIONARY[this.parent.InterfaceLanguage]["all choices required in default language"]);
        f = false;
    }

    // correct selections (quiz only)

    if (this.parent.Data.ObjectType == this.parent.TYPES.QUIZ) {

        g = false; // set to false. when checks found, changed to true. 
        for (var j = 0; j < this.Choices.length; j++) {
            if (this.Choices[j].CheckBox.checked) {
                g = true;
            }
        }

        if (!g) {
            this.ChoicesErrors.AddAndShow(this.parent.DICTIONARY[this.parent.InterfaceLanguage]["at least one correct answer must be specified"]);
            f = false;
        }

    }

    return f;

}

QuestionEditor.prototype.NewChoice = function (
	data,
	type // multiple-choice, true-false, etc
	) {

    // update button state
    this.SetNewChoiceButtonState();

    var NewChoiceContainer = document.createElement("div");

    //this.ChoicesContainer.appendChild(NewChoiceContainer);
    this.ChoicesContainer.insertBefore(NewChoiceContainer, this.ChoicesContainer.lastChild);

    this.Choices.push(new ChoiceEditor(data, NewChoiceContainer, this, type));

    this.SetCheckBoxesStates();
    this.SetDeleteChoiceButtonStates();
}

QuestionEditor.prototype.DeleteChoice = function (id) {

    //enable/disable add choice button

    if (this.Choices.length <= this.MAXCHOICES) {
        this.NewChoiceButton.enable();
    }

    // delete the choice
    for (var i = 0; i < this.Choices.length; i++) {
        if (id == this.Choices[i].id) {
            this.ChoicesContainer.removeChild(this.Choices[i].nodeRoot); // remove the HTML element
            this.Choices.splice(i, 1); // remove from the array
        }
    }

    // update the button states
    this.SetDeleteChoiceButtonStates();

}

QuestionEditor.prototype.SetLanguage = function (l) {

    this.CurrentLanguage = l;

    this.QuestionText.SetLanguage(l);

    for (var i = 0; i < this.Choices.length; i++) {
        this.Choices[i].SetLanguage(l);
    }

}

QuestionEditor.prototype.IsLanguageEntered = function (l) {

    // check question text

    if (this.QuestionText.OutputValue(l) != null &&
		this.QuestionText.OutputValue(l) != "" &&
		this.QuestionText.OutputValue(l).replace(/ /g, '').length > 0) {
        return true;
    }

    // check choices
    // XXX
}

QuestionEditor.prototype.OutputJSON = function () {

    var s = "";

    // question data
    s += "{";
    s += "\"QuestionId\": \"" + this.GUID + "\",";
    s += "\"QuestionType\": \"" + this.QuestionTypeFormField.OutputValue() + "\",";
    s += "\"QuestionDescription\": \"" + this.Description.OutputValue() + "\",";
    s += "\"QuestionCorrectAnswer_sToMatch\": null,";
    try { s += "\"MaxAttempts\": \"" + this.MaxAttempts.OutputValue() + "\","; } catch (e) { }
    s += "\"ChoiceFeedbackMode\": \"final\",";

    // for each language

    s += "\"QuestionDetails\": [";

    try {
        var tmp = this.parent.Data.ObjectType;
    } catch (e) {
        var tmp = this.parent.TYPES.QUIZ;
    }

    for (var i = 0; i < this.parent.IncludedLanguages.length; i++) {

        if (i != 0) {
            s += ",";
        }

        s += "{";

        s += "\"Language\": \"" + this.parent.IncludedLanguages[i] + "\",";
        s += "\"QuestionHtml\": \"" + this.QuestionText.OutputValue(this.parent.IncludedLanguages[i]) + "\",";

        switch (tmp) { // default feedback only on quiz questions
            case this.parent.TYPES.QUIZ:
                s += "\"CorrectFeedback\": \"" + this.parent.DICTIONARY[this.parent.IncludedLanguages[i]]["correct"] + "\",";
                s += "\"WrongFeedback\": \"" + this.parent.DICTIONARY[this.parent.IncludedLanguages[i]]["wrong"] + "\",";
                break;
        }

        s += "\"TryAgainWrongFeedback\": \"" + this.parent.DICTIONARY[this.parent.IncludedLanguages[i]]["try again"] + "\",";

        //choices
        s += "\"Choices\": [";
        for (var j = 0; j < this.Choices.length; j++) {
            if (j != 0) {
                s += ",";
            }
            s += this.Choices[j].OutputJSON(this.parent.IncludedLanguages[i]);
        }
        s += "]";

        s += "}";

    }
    s += "]";

    s += "}";

    return s;

}

/*-----------------------------------------

ChoiceEditor

*/

ChoiceEditor = function (
		data,
		nodeRoot,
		parent,
		type
	) {


    this.id = Math.random().toString(36).slice(-5);
    this.parent = parent;
    this.nodeRoot = nodeRoot;
    this.nodeRoot.className = "ChoiceContainer";
    this.Languages = this.parent.Languages;
    this.FlagImagePath = this.parent.FlagImagePath;
    this.DeleteImagePath = this.parent.DeleteImagePath;
    this.AddImagePath = this.parent.AddImagePath;

    //checkbox

    this.CheckBox = document.createElement("input");
    this.CheckBox.name = "cb-" + this.parent.GUID; 1
    switch (type) {
        case this.parent.QUESTION_TYPES.MC_SINGLE:
        case this.parent.QUESTION_TYPES.TRUE_FALSE:
            this.CheckBox.type = "radio";
            break;
        default: // multiple-choice, likert, fill-in
            this.CheckBox.type = "checkbox";
            break;
    }

    try {
        this.CheckBox.checked = data.IsCorrect;
    } catch (e) { }

    // only attach to DOM if Quiz
    if (this.parent.parent.Data.ObjectType == this.parent.parent.TYPES.QUIZ) {

        nodeRoot.appendChild(this.CheckBox);

    }

    var o = 0;
    // create the inputs
    for (var i = 0; i < this.Languages.length; i++) {
        o++;
        try {
            var t = data.Strings[this.Languages[i]].Text;
        } catch (e) {
            var t = "";
        }
        if (this.Languages[i] == this.parent.CurrentLanguage) {
            var h = new HTMLTextInput(this.id + "_" + o, "", "text", "normal", this.Languages[i], t, 128);
        } else {
            var h = new HTMLTextInput(this.id + "_" + o, "", "text", "normal quiz-editor-hide", this.Languages[i], t, 128);
        }

        switch (type) {
            case this.parent.QUESTION_TYPES.TRUE_FALSE:
                h.disabled = true;
                break;
            default: // multiple-choice, likert, fill-in
                break;
        }

        this.nodeRoot.appendChild(h);

    }


    //delete this choice button
    this.DeleteThisChoiceButton = document.createElement("img");
    this.DeleteThisChoiceButton.className = "button";
    this.DeleteThisChoiceButton.setAttribute("src", this.DeleteImagePath);
    this.DeleteThisChoiceButton.Choice = this;
    this.DeleteThisChoiceButton.enabled = true;
    this.DeleteThisChoiceButton.FlagImagePath = this.FlagImagePath;
    this.DeleteThisChoiceButton.onclick = function () {
        if (this.enabled)
            this.Choice.parent.DeleteChoice(this.Choice.id);
    }
    this.DeleteThisChoiceButton.disable = function () {
        this.enabled = false;
        this.className = "button quiz-editor-screen";
    }
    this.DeleteThisChoiceButton.enable = function () {
        this.enabled = true;
        this.className = "button";
    }
    nodeRoot.appendChild(this.DeleteThisChoiceButton);

    // attach the error messager
    this.Errors = new ErrorAlerter(this, this.nodeRoot);


}


ChoiceEditor.prototype.IsLanguageEntered = function (l) {

    // check question text

    for (var i = 0; i < this.nodeRoot.childNodes.length; i++) {
        if ((this.nodeRoot.childNodes[i].nodeName == "INPUT") &&
			(this.nodeRoot.childNodes[i].type == "text") &&
			(this.nodeRoot.childNodes[i].lang == l)) {
            if (this.nodeRoot.childNodes[i].value.replace(/ /g, '').length > 0) {
                return true;
            }
        }
    }
    return false;
}


ChoiceEditor.prototype.SetLanguage = function (l) {

    for (var i = 0; i < this.nodeRoot.childNodes.length; i++) {
        if (this.nodeRoot.childNodes[i].type == "text") {
            if (this.nodeRoot.childNodes[i].getAttribute("lang") == l) {
                this.nodeRoot.childNodes[i].className = this.nodeRoot.childNodes[i].className.replace(" quiz-editor-hide", "");
            } else {
                this.nodeRoot.childNodes[i].className = this.nodeRoot.childNodes[i].className.replace(" quiz-editor-hide", "") + " quiz-editor-hide";
            }
        }
    }
}

ChoiceEditor.prototype.OutputJSON = function (l) {

    var s = "";
    var j;

    // find the index

    for (var i = 0; i < this.nodeRoot.childNodes.length; i++) {
        if (this.nodeRoot.childNodes[i].type == "text") {
            if (this.nodeRoot.childNodes[i].getAttribute("lang") == l) {
                j = i;
            }
        }
    }

    var f = this.nodeRoot.childNodes[j].value;

    // escape for JSON
    if (f != null) {
        f = f.replace(/\\/g, "\\\\");
        f = f.replace(/"/g, "\\\"");
        f = f.replace(/\r/g, "");
        f = f.replace(/\n/g, "");
        f = f.replace(/\t/g, "");
    }

    s += "{";
    s += "\"Text\": \"" + f + "\",";
    s += "\"Feedback\": null,";
    s += "\"IsCorrect\": " + this.CheckBox.checked;
    s += "}";

    return s;
}

/*-----------------------------------------

SELECTORS

*/

QuizTypeSelector = function (
		parent,
		selectedValue
	) {

    var s = document.createElement("select");
    var o;

    o = document.createElement("option");
    o.value = parent.parent.TYPES.QUIZ;
    o.text = parent.parent.DICTIONARY[parent.parent.InterfaceLanguage]["quiz"];
    s.appendChild(o);
    if (o.value == selectedValue) {
        s.selectedIndex = s.options.length - 1;
    }

    o = document.createElement("option");
    o.value = parent.parent.TYPES.SURVEY;
    o.text = parent.parent.DICTIONARY[parent.InterfaceLanguage]["survey"];
    s.appendChild(o);
    if (o.value == selectedValue) {
        s.selectedIndex = s.options.length - 1;
    }

    s.parent = parent;

    return s;
}

QuestionTypeSelector = function (
		parent, // type (formfield) object
		selectedValue
	) {

    this.parent = parent;

    var s = document.createElement("select");
    var o;

    o = document.createElement("option");
    o.value = this.parent.parent.QUESTION_TYPES.MC_SINGLE;
    o.text = this.parent.parent.parent.DICTIONARY[parent.parent.parent.InterfaceLanguage]["multiple choice single answer"];
    s.appendChild(o);
    if (o.value == selectedValue) {
        s.selectedIndex = s.options.length - 1;
    }

    o = document.createElement("option");
    o.value = this.parent.parent.QUESTION_TYPES.MC_MULTIPLE;
    o.text = this.parent.parent.parent.DICTIONARY[parent.parent.parent.InterfaceLanguage]["multiple choice multiple answer"];
    s.appendChild(o);
    if (o.value == selectedValue) {
        s.selectedIndex = s.options.length - 1;
    }

    o = document.createElement("option");
    o.value = this.parent.parent.QUESTION_TYPES.TRUE_FALSE;
    o.text = this.parent.parent.parent.DICTIONARY[parent.parent.parent.InterfaceLanguage]["true-false"];
    s.appendChild(o);
    if (o.value == selectedValue) {
        s.selectedIndex = s.options.length - 1;
    }

    if (this.parent.parent.parent.Data.ObjectType == this.parent.parent.parent.TYPES.SURVEY) {

        o = document.createElement("option");
        o.value = this.parent.parent.QUESTION_TYPES.LIKERT;
        o.text = this.parent.parent.parent.DICTIONARY[parent.parent.parent.InterfaceLanguage]["likert"];
        s.appendChild(o);
        if (o.value == selectedValue) {
            s.selectedIndex = s.options.length - 1;
        }

    }

    if (this.parent.parent.parent.Data.ObjectType == this.parent.parent.parent.TYPES.QUIZ) {

        o = document.createElement("option");
        o.value = this.parent.parent.QUESTION_TYPES.FILL_IN;
        o.text = this.parent.parent.parent.DICTIONARY[parent.parent.parent.InterfaceLanguage]["fill-in"];
        s.appendChild(o);
        if (o.value == selectedValue) {
            s.selectedIndex = s.options.length - 1;
        }

    }

    if (this.parent.parent.parent.Data.ObjectType == this.parent.parent.parent.TYPES.SURVEY) {

        o = document.createElement("option");
        o.value = this.parent.parent.QUESTION_TYPES.ESSAY;
        o.text = this.parent.parent.parent.DICTIONARY[parent.parent.parent.InterfaceLanguage]["essay"];
        s.appendChild(o);
        if (o.value == selectedValue) {
            s.selectedIndex = s.options.length - 1;
        }

    }

    s.parent = parent;

    return s;
}

QuizAttemptsSelector = function (
	parent,
	selectedValue
	) {

    this.parent = parent;
    this.Languages = this.parent.Languages;

    var s = document.createElement("select");
    s.id = "abc-123";

    for (var i = 1; i <= 10; i++) {
        var g = document.createElement("option");
        g.name = i;
        g.value = i;
        g.appendChild(document.createTextNode(i));
        s.appendChild(g);
        if (g.value == selectedValue) {
            s.selectedIndex = s.options.length - 1;
        }
    }

    var f = document.createElement("option");
    f.name = "0";
    f.value = "0";
    f.appendChild(document.createTextNode(this.parent.parent.DICTIONARY[this.parent.parent.InterfaceLanguage]["unlimited"]));
    s.appendChild(f);
    if (f.value == selectedValue) {
        s.selectedIndex = s.options.length - 1;
    }

    s.parent = parent;
    return s;

}

FeedbackTypeSelector = function (
		parent
	) {

    this.parent = parent;
    this.Languages = this.parent.Languages;

    var s = document.createElement("select");

    var f = document.createElement("option");
    f.name = ""
    f.value = "final"
    f.appendChild(document.createTextNode("Final"));

    var e = document.createElement("option");
    e.name = ""
    e.value = "every"
    e.appendChild(document.createTextNode("Every"));


    s.appendChild(f);
    s.appendChild(e);

    s.onchange = function () {
    }

    return s;

}

LanguageSelector = function (
		parent,
		sel, // the selected langauge code
		arr // array of included languages
	) {

    this.parent = parent;
    this.Languages = this.parent.Languages;
    this.FlagImagePath = this.parent.FlagImagePath;

    var div = document.createElement("div");
    div.parent = this;
    div.className = "FormFieldLanguageSelector FormFieldLanguageSelectorInlineField";
    //div.setAttribute("href", "javascript:void(0);");
    div.onclick = function () {
        this.parent.ToggleOpen();
    }
    var dl = document.createElement("dl");
    dl.parent = this;
    dl.className = "FormFieldLanguageSelectorDropDown FormFieldLanguageSelectorDropDownDL";
    var dd = document.createElement("dd");
    var dt = document.createElement("dt");
    this.OptionsUL = document.createElement("ul");
    this.OptionsUL.parent = this;

    this.SelectedFlag = document.createElement("img");
    this.SelectedFlag.setAttribute("src", this.FlagImagePath + "/" + sel + ".png");

    var arrow = document.createElement("div");
    arrow.className = "FormFieldLanguageSelectorDropDownArrow";

    // add options for each language in the array

    for (var i = 0; i < arr.length; i++) {
        this.OptionsUL.appendChild(this.SelectorOption(arr[i], true));
    }

    dd.appendChild(this.OptionsUL);
    dt.appendChild(this.SelectedFlag);
    dl.appendChild(dt);
    dl.appendChild(dd);
    div.appendChild(dl);
    div.appendChild(arrow);

    return div;

}

LanguageSelector.prototype.OpenOptions = function () {
    this.OptionsUL.style.display = "block";
    this.Open = true;
}

LanguageSelector.prototype.CloseOptions = function () {
    this.OptionsUL.style.display = "none";
    this.Open = false;
}

LanguageSelector.prototype.ToggleOpen = function () {
    if (this.Open == true) {
        this.CloseOptions();
    } else {
        this.OpenOptions();
    }
}

LanguageSelector.prototype.SetLanguage = function (l) {
    //this.CloseOptions();
    this.SelectedFlag.setAttribute("src", this.FlagImagePath + "/" + l + ".png");
    this.parent.SetLanguage(l);
}

LanguageSelector.prototype.SelectorOption = function (isoCode, isDefault) {

    var ul = document.createElement("ul");

    var i = document.createElement("img");
    i.setAttribute("src", this.FlagImagePath + "/" + isoCode + ".png");

    var li = document.createElement("li");

    if (isDefault) {
        li.setAttribute("language", "_DEFAULTLANGUAGE_");
    } else {
        li.setAttribute("language", isoCode);
    }

    li.parent = this;
    li.onclick = function () {
        this.parent.SetLanguage(isoCode)
    }

    li.appendChild(i);

    return li;
}



