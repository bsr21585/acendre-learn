﻿function PopulateHiddenFieldsForDynamicElements(commandName) {

    // hide feedback containers
    $("#PageClientSideFeedbackContainer").hide();
    $("#PageFeedbackContainer").hide();

    // validate, then populate
    if (QuizEditorObject.IsValidShowErrors()) {
        document.getElementById("QuizSurveyIdentifierField").value = QuizEditorObject.OutputIdentifier();
        document.getElementById("QuizSurveyTypeField").value = QuizEditorObject.OutputType();
        document.getElementById("QuizSurveyConfigurationJSONField").value = QuizEditorObject.OutputJSON();       

        // postback using the unique id of the command's submit button, so that the right commands are fired server-side
        if (commandName == "SaveAndPublish") {
            __doPostBack(SaveQuizSurveyAndPublishButtonUID, ''); // save and publish
        }
        else {
            __doPostBack(SaveQuizSurveyAsDraftButtonUID, ''); // save as draft, default
        }
    }
    else { // error
        window.scrollTo(0, 0);
        $("#PageClientSideFeedbackContainer").show();
    }
}