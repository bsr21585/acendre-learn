﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.QuizzesAndSurveys
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel QuizSurveyFormContentWrapperContainer;
        public Panel ObjectOptionsPanel;
        public UpdatePanel QuizSurveyGridUpdatePanel;
        public Grid QuizSurveyGrid;
        public Panel ActionsPanel;
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup GridConfirmAction;
        #endregion

        #region Private Properties
        // publish quiz/survey modal
        private ModalPopup _PublishQuizSurveyModal;
        private HiddenField _QuizSurveyIdToPublish;
        private Button _PublishQuizSurveyModalHiddenLaunchButton;
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (
                !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE)
                || !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_QuizAndSurveyManager)
               )
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/quizzesandsurveys/Default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.QuizzesAndSurveys));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, _GlobalResources.QuizzesAndSurveys, ImageFiles.GetIconPath(ImageFiles.ICON_QUIZ,
                                                                                                                                ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.QuizSurveyFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the grid, actions panel, and modals
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildPublishQuizSurveyModal();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.QuizSurveyGrid.BindData();
            }
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.QuizzesAndSurveys.Default.js");

            // set up javascript globals 
            StringBuilder globalJS = new StringBuilder();
            globalJS.AppendLine("var PublishQuizSurveyModalHiddenLaunchButtonId = \"" + this._PublishQuizSurveyModalHiddenLaunchButton.ID + "\";");

            csm.RegisterClientScriptBlock(typeof(Default), "QuizSurveyGlobalJs", globalJS.ToString(), true);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";            

            // NEW QUIZ
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddQuizLink",
                                                null,
                                                "Modify.aspx?type=quiz",
                                                null,
                                                _GlobalResources.NewQuiz,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_QUIZ, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            // NEW SURVEY
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddSurveyLink",
                                                null,
                                                "Modify.aspx?type=survey",
                                                null,
                                                _GlobalResources.NewSurvey,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_QUIZ, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.QuizSurveyGridUpdatePanel.Attributes.Add("class", "FormContentContainer");
            this.QuizSurveyGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            this.QuizSurveyGrid.StoredProcedure = Library.QuizSurvey.GridProcedure;
            this.QuizSurveyGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.QuizSurveyGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.QuizSurveyGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.QuizSurveyGrid.IdentifierField = "idQuizSurvey";
            this.QuizSurveyGrid.DefaultSortColumn = "identifier";
            this.QuizSurveyGrid.ShowTimeInDateStrings = true;
            this.QuizSurveyGrid.SearchBoxPlaceholderText = _GlobalResources.SearchQuizzesAndSurveys;

            // data key names
            this.QuizSurveyGrid.DataKeyNames = new string[] { "idQuizSurvey", "idContentPackage", "author" };

            // columns
            GridColumn nameDraftOwnerTypeLastModifiedAvgScore = new GridColumn(_GlobalResources.Name + " (" + _GlobalResources.Draft + "), " + _GlobalResources.Owner + ", " + _GlobalResources.Type + ", " + _GlobalResources.LastModified + ", " + _GlobalResources.AverageScore, null, "identifier"); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method            

            // add columns to data grid
            this.QuizSurveyGrid.AddColumn(nameDraftOwnerTypeLastModifiedAvgScore);
            this.QuizSurveyGrid.AddColumn(options);            

            // add row data bound event
            this.QuizSurveyGrid.RowDataBound += new GridViewRowEventHandler(this._QuizSurveyGrid_RowDataBound);
        }
        #endregion

        #region _QuizSurveyGrid_RowDataBound
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _QuizSurveyGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;

                int idQuizSurvey = Convert.ToInt32(rowView["idQuizSurvey"]);
                
                int idContentPackage = 0;
                if (rowView["idContentPackage"] != DBNull.Value)
                { idContentPackage = Convert.ToInt32(rowView["idContentPackage"]); }

                string name = Server.HtmlEncode(rowView["identifier"].ToString());
                string author = Server.HtmlEncode(rowView["author"].ToString());
                int type = Convert.ToInt32(rowView["type"]);
                DateTime dtModified = Convert.ToDateTime(rowView["dtModified"]);
                
                double? averageScore = null; // this needs to be able to be null because null meand no data, where 0 means average is 0
                if (rowView["averageScore"] != DBNull.Value)
                { averageScore = Convert.ToDouble(rowView["averageScore"]); }
                
                bool isDraft = Convert.ToBoolean(rowView["isDraft"]);
                bool isPublished = Convert.ToBoolean(rowView["isPublished"]);

                // AVATAR, NAME, (DRAFT), OWNER, TYPE, LAST MODIFIED, AVERAGE SCORE COLUMN

                // avatar
                Image avatarImage = new Image();
                avatarImage.CssClass = "GridAvatarImage";
                avatarImage.AlternateText = _GlobalResources.QuizSurvey;
                avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_QUIZ, ImageFiles.EXT_PNG);
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name                
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink nameLink = new HyperLink();
                nameLink.NavigateUrl = "Modify.aspx?id=" + idQuizSurvey.ToString();
                nameLink.Text = name;
                nameLabel.Controls.Add(nameLink);

                // draft
                if (isDraft)
                {
                    Label draftLabel = new Label();
                    draftLabel.CssClass = "GridSecondaryTitle DraftText";
                    draftLabel.Text = "(" + _GlobalResources.Draft + ")";
                    e.Row.Cells[1].Controls.Add(draftLabel);
                }

                // owner & type - they're on a single line, so they go into a wrapper
                Panel ownerTypeInlineWrapper = new Panel();
                ownerTypeInlineWrapper.CssClass = "GridSecondaryLineInlineWrapper";
                e.Row.Cells[1].Controls.Add(ownerTypeInlineWrapper);

                // owner
                Label ownerLabel = new Label();
                ownerLabel.CssClass = "GridSecondaryLineInline";
                ownerTypeInlineWrapper.Controls.Add(ownerLabel);

                Literal ownerLit = new Literal();

                if (author == "##Administrator##")
                { ownerLit.Text = _GlobalResources.Administrator; }
                else
                { ownerLit.Text = author; }

                ownerLabel.Controls.Add(ownerLit);

                // type                
                Label typeLabel = new Label();
                typeLabel.CssClass = "GridSecondaryLineInline";
                ownerTypeInlineWrapper.Controls.Add(typeLabel);

                Literal typeLit = new Literal();

                if (type == Convert.ToInt32(QuizSurvey.QuizSurveyType.Quiz))
                { typeLit.Text = _GlobalResources.Quiz; }
                else
                { typeLit.Text = _GlobalResources.Survey; }

                typeLabel.Controls.Add(typeLit);

                // dtModified & average score - they're on a single line, so they go into a wrapper
                Panel dtModifiedAverageScoreInlineWrapper = new Panel();
                dtModifiedAverageScoreInlineWrapper.CssClass = "GridSecondaryLineInlineWrapper";
                e.Row.Cells[1].Controls.Add(dtModifiedAverageScoreInlineWrapper);

                // dtModified
                dtModified = TimeZoneInfo.ConvertTimeFromUtc(dtModified, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                string dtModifiedText = dtModified.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern) + " @ " + dtModified.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern);

                Label dtModifiedLabel = new Label();
                dtModifiedLabel.CssClass = "GridSecondaryLineInline";
                dtModifiedLabel.Text = dtModifiedText;
                dtModifiedAverageScoreInlineWrapper.Controls.Add(dtModifiedLabel);

                // average score
                if (averageScore != null)
                {
                    Label averageScoreLabel = new Label();
                    averageScoreLabel.CssClass = "GridSecondaryLineInline";
                    dtModifiedAverageScoreInlineWrapper.Controls.Add(averageScoreLabel);

                    Literal averageScoreLit = new Literal();
                    averageScoreLit.Text = _GlobalResources.AverageScore + ": " + Convert.ToDouble(averageScore).ToString("N0") + "%";
                    averageScoreLabel.Controls.Add(averageScoreLit);
                }

                // OPTIONS COLUMN

                // package
                Label packageSpan = new Label();
                packageSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(packageSpan);

                if (isPublished)
                {
                    HyperLink packageLink = new HyperLink();
                    packageLink.NavigateUrl = "/administrator/packages/Modify.aspx?id=" + idContentPackage.ToString();
                    packageSpan.Controls.Add(packageLink);

                    Image packageLinkImage = new Image();
                    packageLinkImage.AlternateText = _GlobalResources.Package;
                    packageLinkImage.ToolTip = _GlobalResources.Published;
                    packageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE_BUTTON_ALT, ImageFiles.EXT_PNG);
                    packageLink.Controls.Add(packageLinkImage);
                }
                else
                {
                    Image packageLinkImage = new Image();
                    packageLinkImage.AlternateText = _GlobalResources.Package;
                    packageLinkImage.ToolTip = _GlobalResources.NotPublished;
                    packageLinkImage.CssClass = "DimIcon";
                    packageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE_BUTTON_ALT, ImageFiles.EXT_PNG);
                    packageSpan.Controls.Add(packageLinkImage);
                }

                // publish
                Label publishSpan = new Label();
                publishSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(publishSpan);

                Image publishQuizSurvey = new Image();
                publishQuizSurvey.ID = "PublishQuizSurveyLink_" + idQuizSurvey.ToString();
                publishQuizSurvey.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD_ALT, ImageFiles.EXT_PNG);
                publishSpan.Controls.Add(publishQuizSurvey);

                if (isDraft)
                {                    
                    publishQuizSurvey.ToolTip = _GlobalResources.Publish;
                    publishQuizSurvey.Attributes.Add("onclick", "LoadPublishQuizSurveyModalContent(" + idQuizSurvey.ToString() + ");");
                    publishQuizSurvey.Style.Add("cursor", "pointer");                    
                }
                else
                {
                    publishQuizSurvey.CssClass = "DimIcon";
                }
            }
        }
        #endregion

        #region _BuildPublishQuizSurveyModal
        /// <summary>
        /// Builds the confirmation modal for publishing a quiz/survey.
        /// </summary>
        private void _BuildPublishQuizSurveyModal()
        {
            // instansiate the trigger button
            this._PublishQuizSurveyModalHiddenLaunchButton = new Button();
            this._PublishQuizSurveyModalHiddenLaunchButton.ID = "PublishQuizSurveyModalHiddenLaunchButton";
            this._PublishQuizSurveyModalHiddenLaunchButton.Style.Add("display", "none");
            this.QuizSurveyFormContentWrapperContainer.Controls.Add(this._PublishQuizSurveyModalHiddenLaunchButton);

            // set modal properties
            this._PublishQuizSurveyModal = new ModalPopup("PublishQuizSurveyModal");
            this._PublishQuizSurveyModal.Type = ModalPopupType.Confirm;
            this._PublishQuizSurveyModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD_ALT, ImageFiles.EXT_PNG);
            this._PublishQuizSurveyModal.HeaderIconAlt = _GlobalResources.Publish;
            this._PublishQuizSurveyModal.HeaderText = _GlobalResources.PublishQuizSurvey;
            this._PublishQuizSurveyModal.TargetControlID = this._PublishQuizSurveyModalHiddenLaunchButton.ClientID;
            this._PublishQuizSurveyModal.SubmitButton.Command += new CommandEventHandler(this._PublishButton_Command);

            // hidden field for quiz/survey id to publish
            this._QuizSurveyIdToPublish = new HiddenField();
            this._QuizSurveyIdToPublish.ID = "QuizSurveyIdToPublish";
            this._PublishQuizSurveyModal.AddControlToBody(this._QuizSurveyIdToPublish);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "PublishQuizSurveyModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToPublishThisQuizSurvey;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._PublishQuizSurveyModal.AddControlToBody(body1Wrapper);

            this.QuizSurveyFormContentWrapperContainer.Controls.Add(this._PublishQuizSurveyModal);
        }
        #endregion

        #region _PublishButton_Command
        /// <summary>
        /// Performs the publish action for a quiz/survey.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _PublishButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get the value of the quiz/survey id to publish field and parse into integer
                int idQuizSurvey = 0;
                bool isIdParsed = Int32.TryParse(this._QuizSurveyIdToPublish.Value, out idQuizSurvey);

                if (!isIdParsed)
                { throw new AsentiaException(_GlobalResources.TheQuizSurveyCouldNotBePublishedPleaseContactAnAdministrator); }

                // load the quiz/survey object
                QuizSurvey quizSurveyObject = new QuizSurvey(idQuizSurvey);

                // publish the quiz/survey
                ContentPackage contentPackageObject;

                // if this quiz/survey has been published before, re-publish it by replacing the existing package
                // else, create a new package and link that to the quiz/survey
                if (quizSurveyObject.IdContentPackage > 0)
                {
                    // instansiate a content package object using the existing content package id
                    contentPackageObject = new ContentPackage((int)quizSurveyObject.IdContentPackage);

                    // copy the shell into the warehouse folder
                    Utility.CopyDirectory(Server.MapPath(SitePathConstants._BIN + "quizSurveyScormWrapperTemplate"), Server.MapPath(contentPackageObject.Path), true, true);

                    // open the template manifest file, replace the replacers, and overwrite the existing one
                    string imsManifestContent = String.Empty;

                    using (StreamReader reader = new StreamReader(Server.MapPath(contentPackageObject.Path + "/_imsmanifest.xml")))
                    { imsManifestContent = reader.ReadToEnd(); }

                    imsManifestContent = Regex.Replace(imsManifestContent, "##guid##", quizSurveyObject.GUIDString);
                    imsManifestContent = Regex.Replace(imsManifestContent, "##identifier##", quizSurveyObject.Identifier);

                    using (StreamWriter writer = new StreamWriter(Server.MapPath(contentPackageObject.Path + "/imsmanifest.xml"), false))
                    { writer.Write(imsManifestContent); }

                    // overwrite the Quiz.json file with the new JSON data
                    using (StreamWriter writer = new StreamWriter(Server.MapPath(contentPackageObject.Path + "/Quiz.json"), false))
                    { writer.Write(quizSurveyObject.JSONData); }

                    // update the draft status for the quiz/survey
                    quizSurveyObject.IsDraft = false;
                    quizSurveyObject.Save();
                }
                else
                {
                    // get some values so that we can help ensure a unique warehouse folder, and begin to set-up the package
                    string currentDateTimeFormatted = AsentiaSessionState.UtcNow.ToString("yyyy-MM-dd-hh-mm-ss");
                    string dateTimeDownToMilliseconds = AsentiaSessionState.UtcNow.ToString("o").Replace(":", "-");

                    string newWarehouseFolderName = AsentiaSessionState.IdAccount
                                                    + "-"
                                                    + AsentiaSessionState.IdSite
                                                    + "-"
                                                    + Cryptography.GetHash(quizSurveyObject.Identifier.Trim() + dateTimeDownToMilliseconds, Cryptography.HashType.MD5)
                                                    + "-"
                                                    + currentDateTimeFormatted;

                    // create the new folder and copy the quiz wrapper shell into it - CopyDirectory does both
                    Utility.CopyDirectory(Server.MapPath(SitePathConstants._BIN + "quizSurveyScormWrapperTemplate"), Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName));

                    // open the template manifest file, replace the replacers, and overwrite the existing one
                    string imsManifestContent = String.Empty;

                    using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/_imsmanifest.xml")))
                    { imsManifestContent = reader.ReadToEnd(); }

                    imsManifestContent = Regex.Replace(imsManifestContent, "##guid##", quizSurveyObject.GUIDString);
                    imsManifestContent = Regex.Replace(imsManifestContent, "##identifier##", quizSurveyObject.Identifier);

                    using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/imsmanifest.xml"), false))
                    { writer.Write(imsManifestContent); }

                    // overwrite the Quiz.json file with the new JSON data
                    using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/Quiz.json"), false))
                    { writer.Write(quizSurveyObject.JSONData); }

                    // instansiate a content package object
                    contentPackageObject = new ContentPackage();
                    contentPackageObject.IdSite = AsentiaSessionState.IdSite;
                    contentPackageObject.Path = SitePathConstants.WAREHOUSE + newWarehouseFolderName;
                    contentPackageObject.Name = quizSurveyObject.Identifier;
                    contentPackageObject.Kb = Utility.CalculateDirectorySizeInKb(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName));
                    contentPackageObject.Manifest = imsManifestContent;
                    contentPackageObject.IdContentPackageType = ContentPackage.ContentPackageType.SCORM;
                    contentPackageObject.IdSCORMPackageType = ContentPackage.SCORMPackageType.Content;
                    contentPackageObject.IdQuizSurvey = idQuizSurvey;

                    // save the content package
                    int idContentPackage = contentPackageObject.Save();

                    // update the draft status and content package id for the quiz/survey
                    quizSurveyObject.IsDraft = false;
                    quizSurveyObject.IdContentPackage = idContentPackage;
                    quizSurveyObject.Save();
                }

                // display the success message
                this.DisplayFeedback(String.Format(_GlobalResources.TheQuizSurveyHasBeenSavedAndPublishedSuccessfullyYouCanViewThePublishedPackage, quizSurveyObject.IdContentPackage), false);

                // rebind the grid
                this.QuizSurveyGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this.QuizSurveyGrid.BindData();
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedQuiz_zesSurvey_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this.DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);

            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedQuiz_zesSurvey_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseQuiz_zesSurvey_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.QuizSurveyGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.QuizSurveyGrid.Rows[i].FindControl(this.QuizSurveyGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.QuizSurvey.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedback(_GlobalResources.TheSelectedQuiz_zesSurvey_sHaveBeenDeletedSuccessfully, false);

                // rebind the grid
                this.QuizSurveyGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this.QuizSurveyGrid.BindData();
            }
        }
        #endregion
    }
}
