﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.QuizzesAndSurveys
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Public Properties
        public Panel PageInstructionsPanel;
        public Panel PageClientSideFeedbackContainer;
        public Panel QuizSurveyPropertiesFormWrapperContainer;
        public Panel QuizSurveyPropertiesWrapperContainer;
        public Panel QuizSurveyFormContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private QuizSurvey _QuizSurveyObject;

        private HiddenField _QuizSurveyGUID;
        private HiddenField _QuizSurveyIdentifier;
        private HiddenField _QuizSurveyType;
        private HiddenField _QuizSurveyConfigurationJSON;

        private Button _SaveQuizSurveyAsDraft;
        private Button _SaveQuizSurveyAndPublish;
        private Button _CancelQuizSurvey;
        #endregion

        #region Page Load
        /// <summary>
        /// Handles the Page Load Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (
                !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE)
                || !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_QuizAndSurveyManager)
               )
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("QuizEditor.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/quizzesandsurveys/Modify.css");

            // get the quiz/survey object
            this._GetQuizSurveyObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.CreateOrModifyThePropertiesOfTheQuizOrSurveyUsingTheFormBelow, true);

            // pre-format the client-side feedback container with its error mesage
            // this will be hidden by default, and only shown if the quiz editor object fails JS validation
            this.DisplayFeedbackInSpecifiedContainer(this.PageClientSideFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            this.PageClientSideFeedbackContainer.Style.Add("display", "none");

            // add formatting classes to form wrapper containers
            this.QuizSurveyPropertiesFormWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.QuizSurveyPropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;

            // QUIZ BUILDER OBJECT
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.QuizzesAndSurveys.QuizEditor.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.QuizzesAndSurveys.Modify.js");

            // build global JS variables
            StringBuilder globalJS = new StringBuilder();

            globalJS.AppendLine("var QuizEditorObject;");
            globalJS.AppendLine("var SaveQuizSurveyAsDraftButtonUID = \"" + this._SaveQuizSurveyAsDraft.UniqueID + "\";");
            globalJS.AppendLine("var SaveQuizSurveyAndPublishButtonUID = \"" + this._SaveQuizSurveyAndPublish.UniqueID + "\";");
            globalJS.AppendLine("var myCKEditors = [];");

            // AVAILABLE LANGUAGES

            // get the available languages by looping through each installed language based on the language folders contained in bin
            // append them to a comms-separated string for use in the Global JS
            string availableLanguages = "en-US"; // en-US is the default application language, so it's already installed            

            foreach (string directory in Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN)))
            {
                // loop through each language available to the site and add it to the array list for drop-down items
                foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
                {
                    if (language == Path.GetFileName(directory))
                    {
                        availableLanguages += "," + language;
                        break;
                    }
                }
            }

            globalJS.AppendLine("var AvailableLanguages = \"" + availableLanguages + "\";");
            globalJS.AppendLine("");

            // DICTIONARY OF TERMS

            globalJS.Append("var EditorTermsDictionary = ##quot##{");

            // en-US
            globalJS.Append("	\"en-US\":{");
            globalJS.Append("	\"identifier\":\"" + _GlobalResources.Identifier + "\",");
            globalJS.Append("	\"identifier required\":\"" + _GlobalResources.IdentifierIsRequired + "\",");
            globalJS.Append("	\"properties\":\"" + _GlobalResources.Properties + "\",");
            globalJS.Append("	\"questions\":\"" + _GlobalResources.Questions + "\",");
            globalJS.Append("	\"languages\":\"" + _GlobalResources.Languages + "\",");
            globalJS.Append("	\"title\":\"" + _GlobalResources.Title + "\",");
            globalJS.Append("	\"title required in default language\":\"" + _GlobalResources.TitleIsRequiredInTheDefaultLanguage + "\",");
            globalJS.Append("	\"type\":\"" + _GlobalResources.Type + "\",");
            globalJS.Append("	\"passing score\":\"" + _GlobalResources.PassingScore + "\",");
            globalJS.Append("	\"passing score invalid\":\"" + _GlobalResources.PassingScoreIsInvalid + "\",");
            globalJS.Append("	\"options\":\"" + _GlobalResources.Options + "\",");
            globalJS.Append("	\"conclusion summary\":\"" + _GlobalResources.ConclusionSummary + "\",");
            globalJS.Append("	\"new question\":\"" + _GlobalResources.NewQuestion + "\",");
            globalJS.Append("	\"question\":\"" + _GlobalResources.Question + "\",");
            globalJS.Append("	\"description\":\"" + _GlobalResources.Description + "\",");
            globalJS.Append("	\"description required\":\"" + _GlobalResources.DescriptionIsRequired + "\",");
            globalJS.Append("	\"text\":\"" + _GlobalResources.Text + "\",");
            globalJS.Append("	\"question text required in default language\":\"" + _GlobalResources.QuestionTextIsRequiredInTheDefaultLanguage + "\",");
            globalJS.Append("	\"maximum attempts\":\"" + _GlobalResources.MaximumAttempts + "\",");
            globalJS.Append("	\"answer choices\":\"" + _GlobalResources.AnswerChoices + "\",");
            globalJS.Append("	\"all choices required in default language\":\"" + _GlobalResources.AllAnswerChoicesAreRequiredInTheDefaultLanguage + "\",");
            globalJS.Append("	\"at least one correct answer must be specified\":\"" + _GlobalResources.AtLeastOneCorrectAnswerMustBeSpecified + "\",");
            globalJS.Append("	\"acceptable answers\":\"" + _GlobalResources.AcceptableAnswers + "\",");
            globalJS.Append("	\"multiple choice\":\"" + _GlobalResources.MultipleChoice + "\",");
            globalJS.Append("	\"multiple choice multiple answer\":\"" + _GlobalResources.MultipleChoiceMultipleAnswer + "\",");
            globalJS.Append("	\"multiple choice single answer\":\"" + _GlobalResources.MultipleChoiceSingleAnswer + "\",");
            globalJS.Append("	\"true-false\":\"" + _GlobalResources.True_False + "\",");
            globalJS.Append("	\"true\":\"" + _GlobalResources.True + "\",");
            globalJS.Append("	\"false\":\"" + _GlobalResources.False + "\",");
            globalJS.Append("	\"likert\":\"" + _GlobalResources.Likert + "\",");
            globalJS.Append("	\"fill-in\":\"" + _GlobalResources.FillIn + "\",");
            globalJS.Append("	\"essay\":\"" + _GlobalResources.Essay + "\",");            
            globalJS.Append("	\"quiz\":\"" + _GlobalResources.Quiz + "\",");
            globalJS.Append("	\"survey\":\"" + _GlobalResources.Survey + "\",");
            globalJS.Append("	\"randomize questions\":\"" + _GlobalResources.RandomizeQuestions + "\",");
            globalJS.Append("	\"allow bookmarking\":\"" + _GlobalResources.AllowBookmarking + "\",");
            globalJS.Append("	\"allow user to review\":\"" + _GlobalResources.AllowLearnerToReviewAnswersAfterCompletion + "\",");
            globalJS.Append("	\"display single page\":\"" + _GlobalResources.DisplayAllQuestionsOnSinglePage + "\",");
            globalJS.Append("	\"require answers to continue\":\"" + _GlobalResources.RequireResponsesToContinueToNextQuestion + "\",");
            globalJS.Append("	\"display correct answer\":\"" + _GlobalResources.DisplayCorrectAnswersToLearner + "\",");
            globalJS.Append("	\"show final score\":\"" + _GlobalResources.ShowFinalScore + "\",");
            globalJS.Append("	\"correct\":\"" + _GlobalResources.CorrectExclamation + "\",");
            globalJS.Append("	\"wrong\":\"" + _GlobalResources.SorryThatIsIncorrect + "\",");
            globalJS.Append("	\"try again\":\"" + _GlobalResources.IncorrectTryAgain + "\",");
            globalJS.Append("	\"default language\":\"" + _GlobalResources.DefaultLanguage + "\",");
            globalJS.Append("	\"check languages to include\":\"" + _GlobalResources.CheckLanguagesToInclude + "\",");
            globalJS.Append("	\"unlimited\":\"" + _GlobalResources.Unlimited + "\"");
            globalJS.Append("	}");

            // rest of the languages
            foreach (string directory in Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN)))
            {
                // loop through each language available to the site and add it to the array list for drop-down items
                foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
                {
                    if (language == Path.GetFileName(directory))
                    {
                        globalJS.Append(",	\"" + language + "\":{");
                        globalJS.Append("	\"identifier\":\"" + _GlobalResources.ResourceManager.GetString("Identifier", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"identifier required\":\"" + _GlobalResources.ResourceManager.GetString("IdentifierIsRequired", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"properties\":\"" + _GlobalResources.ResourceManager.GetString("Properties", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"questions\":\"" + _GlobalResources.ResourceManager.GetString("Questions", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"languages\":\"" + _GlobalResources.ResourceManager.GetString("Languages", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"title\":\"" + _GlobalResources.ResourceManager.GetString("Title", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"title required in default language\":\"" + _GlobalResources.ResourceManager.GetString("TitleIsRequiredInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"type\":\"" + _GlobalResources.ResourceManager.GetString("Type", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"passing score\":\"" + _GlobalResources.ResourceManager.GetString("PassingScore", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"passing score invalid\":\"" + _GlobalResources.ResourceManager.GetString("PassingScoreIsInvalid", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"options\":\"" + _GlobalResources.ResourceManager.GetString("Options", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"conclusion summary\":\"" + _GlobalResources.ResourceManager.GetString("ConclusionSummary", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"new question\":\"" + _GlobalResources.ResourceManager.GetString("NewQuestion", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"question\":\"" + _GlobalResources.ResourceManager.GetString("Question", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"description\":\"" + _GlobalResources.ResourceManager.GetString("Description", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"description required\":\"" + _GlobalResources.ResourceManager.GetString("DescriptionIsRequired", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"text\":\"" + _GlobalResources.ResourceManager.GetString("Text", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"question text required in default language\":\"" + _GlobalResources.ResourceManager.GetString("QuestionTextIsRequiredInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"maximum attempts\":\"" + _GlobalResources.ResourceManager.GetString("MaximumAttempts", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"answer choices\":\"" + _GlobalResources.ResourceManager.GetString("AnswerChoices", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"all choices required in default language\":\"" + _GlobalResources.ResourceManager.GetString("AllAnswerChoicesAreRequiredInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"at least one correct answer must be specified\":\"" + _GlobalResources.ResourceManager.GetString("AtLeastOneCorrectAnswerMustBeSpecified", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"acceptable answers\":\"" + _GlobalResources.ResourceManager.GetString("AcceptableAnswers", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"multiple choice\":\"" + _GlobalResources.ResourceManager.GetString("MultipleChoice", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"multiple choice multiple answer\":\"" + _GlobalResources.ResourceManager.GetString("MultipleChoiceMultipleAnswer", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"multiple choice single answer\":\"" + _GlobalResources.ResourceManager.GetString("MultipleChoiceSingleAnswer", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"true-false\":\"" + _GlobalResources.ResourceManager.GetString("True_False", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"true\":\"" + _GlobalResources.ResourceManager.GetString("True", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"false\":\"" + _GlobalResources.ResourceManager.GetString("False", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"likert\":\"" + _GlobalResources.ResourceManager.GetString("Likert", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"fill-in\":\"" + _GlobalResources.ResourceManager.GetString("FillIn", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"essay\":\"" + _GlobalResources.ResourceManager.GetString("Essay", new CultureInfo(language)) + "\",");                        
                        globalJS.Append("	\"quiz\":\"" + _GlobalResources.ResourceManager.GetString("Quiz", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"survey\":\"" + _GlobalResources.ResourceManager.GetString("Survey", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"randomize questions\":\"" + _GlobalResources.ResourceManager.GetString("RandomizeQuestions", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"allow bookmarking\":\"" + _GlobalResources.ResourceManager.GetString("AllowBookmarking", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"allow user to review\":\"" + _GlobalResources.ResourceManager.GetString("AllowLearnerToReviewAnswersAfterCompletion", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"display single page\":\"" + _GlobalResources.ResourceManager.GetString("DisplayAllQuestionsOnSinglePage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"require answers to continue\":\"" + _GlobalResources.ResourceManager.GetString("RequireResponsesToContinueToNextQuestion", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"display correct answer\":\"" + _GlobalResources.ResourceManager.GetString("DisplayCorrectAnswersToLearner", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"show final score\":\"" + _GlobalResources.ResourceManager.GetString("ShowFinalScore", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"correct\":\"" + _GlobalResources.ResourceManager.GetString("CorrectExclamation", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"wrong\":\"" + _GlobalResources.ResourceManager.GetString("SorryThatIsIncorrect", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"try again\":\"" + _GlobalResources.ResourceManager.GetString("IncorrectTryAgain", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"default language\":\"" + _GlobalResources.ResourceManager.GetString("DefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"check languages to include\":\"" + _GlobalResources.ResourceManager.GetString("CheckLanguagesToInclude", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"unlimited\":\"" + _GlobalResources.ResourceManager.GetString("Unlimited", new CultureInfo(language)) + "\"");
                        globalJS.Append("	}");
                        break;
                    }
                }
            }

            globalJS.Append("}##quot##;");
            globalJS.AppendLine("");

            // ESCAPE ' CHARACTERS FOR DICTIONARY OBJECT
            globalJS.Replace("'", "\\'");
            globalJS.Replace("##quot##", "'");

            // ADD IMAGE PATH
            globalJS.AppendLine("var QuizEditorAddImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG) + "\";");
            globalJS.AppendLine("");

            // DELETE IMAGE PATH
            globalJS.AppendLine("var QuizEditorDeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            globalJS.AppendLine("");
            
            // CURRENT INTERFACE LANGUAGE
            globalJS.AppendLine("var CurrentInterfaceLanguage = \"" + AsentiaSessionState.UserCulture + "\";");
            globalJS.AppendLine("");

            // CKEDITOR INIT COMMAND
            globalJS.AppendLine("var CKEditorInitCommand = \"myCKEditors[\\\"!id!\\\"] = CKEDITOR.appendTo(document.getElementById(\\\"!id!\\\").parentNode, {\\");
			globalJS.AppendLine("toolbar:[\\");
			globalJS.AppendLine("	{ \\");
			globalJS.AppendLine("		name: 'styles', \\");
			globalJS.AppendLine("		items: ['Font', 'FontSize'] \\");
			globalJS.AppendLine("	},\\");
			globalJS.AppendLine("	{ \\");
			globalJS.AppendLine("		name: 'colors', \\");
			globalJS.AppendLine("		items: ['TextColor', 'BGColor'] \\");
			globalJS.AppendLine("	},\\");
			globalJS.AppendLine("	{ \\");
			globalJS.AppendLine("		name: 'basicstyles', \\");
			globalJS.AppendLine("		items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] \\");
			globalJS.AppendLine("	},\\");
			globalJS.AppendLine("	{ \\");
			globalJS.AppendLine("		name: 'paragraph', \\");
			globalJS.AppendLine("		items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] \\");
			globalJS.AppendLine("	},\\");
			globalJS.AppendLine("	{ \\");
			globalJS.AppendLine("		name: 'links', \\");
			globalJS.AppendLine("		items: ['Link', 'Unlink'] \\");
			globalJS.AppendLine("	},\\");
			globalJS.AppendLine("	{ \\");
			globalJS.AppendLine("		name: 'document', \\");
			globalJS.AppendLine("		items: ['Source'] \\");
			globalJS.AppendLine("	}\\");
			globalJS.AppendLine("	]\\");
			globalJS.AppendLine("}, document.getElementById(\\\"!id!\\\").value);\";");
            globalJS.AppendLine("");

            // CKEDITOR GET COMMAND
            globalJS.AppendLine("var CKEditorGetCommand = \"myCKEditors[\\\"!id!\\\"].getData()\";");
            globalJS.AppendLine("");

            csm.RegisterClientScriptBlock(typeof(Modify), "GlobalJS", globalJS.ToString(), true);

            // BUILD STARTUP CALLS FOR CKEditor AND QuizEditor, AND ADD TO THE Page_Load            

            StringBuilder multipleStartUpCallsScript = new StringBuilder();
            multipleStartUpCallsScript.AppendLine("");

            // content stylesheet for CKEditor, path to the quiz wrapper's stylesheet
            if (File.Exists(MapPathSecure(SitePathConstants._BIN + "quizSurveyScormWrapperTemplate/style.css")))
            { multipleStartUpCallsScript.AppendLine("DefaultLayoutCSSFilePath = \"" + SitePathConstants._BIN + "quizSurveyScormWrapperTemplate/style.css\";"); }
            else
            { multipleStartUpCallsScript.AppendLine("DefaultLayoutCSSFilePath = \"\";"); }

            multipleStartUpCallsScript.AppendLine("SiteSpecificLayoutCSSFilePath = \"\";");
            multipleStartUpCallsScript.AppendLine("");

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine(" QuizEditorObject = new QuizEditor(");
            multipleStartUpCallsScript.AppendLine("     \"" + this._QuizSurveyGUID.Value + "\",");                                              // GUID
            multipleStartUpCallsScript.AppendLine("     \"" + this._QuizSurveyIdentifier.Value + "\",");                                        // IDENTIFIER
            multipleStartUpCallsScript.AppendLine("     document.getElementById(\"QuizSurveyConfigurationJSONField\").value,");                 // JSON DATA (for reading)
            multipleStartUpCallsScript.AppendLine("     document.getElementById(\"QuizSurveyFormContainer\"),");                                // CONTAINER TO ATTACH BUILDER TO
            multipleStartUpCallsScript.AppendLine("     CurrentInterfaceLanguage,");                                                            // THE CURRENT INERFACE LANGUAGE
            multipleStartUpCallsScript.AppendLine("     AvailableLanguages,");                                                                  // AVAILABLE LANGUAGES
            multipleStartUpCallsScript.AppendLine("     EditorTermsDictionary,");                                                               // DICTIONARY OF TERMS
            multipleStartUpCallsScript.AppendLine("     QuizEditorAddImagePath,");                                                              // ADD IMAGE
            multipleStartUpCallsScript.AppendLine("     QuizEditorDeleteImagePath,");                                                           // DELETE IMAGE
            multipleStartUpCallsScript.AppendLine("     \"/_images/flags\",");                                                                  // FLAG IMAGES FOLDER
            multipleStartUpCallsScript.AppendLine("     CKEditorInitCommand,");                                                                 // CKEDITOR INIT COMMAND (with replacer)
            multipleStartUpCallsScript.AppendLine("     CKEditorGetCommand,");                                                                  // CKEDITOR GET COMMAND (with replacer)
            multipleStartUpCallsScript.AppendLine("     \"!id!\"");                                                                             // CKEDITOR ID REPLACER
            multipleStartUpCallsScript.AppendLine("     );");
            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);
        }
        #endregion

        #region _GetQuizSurveyObject
        /// <summary>
        /// Gets a quiz/survey object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetQuizSurveyObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._QuizSurveyObject = new QuizSurvey(id); }
                }
                catch
                { Response.Redirect("~/administrator/quizzesandsurveys"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get page title
            string pageTitle = String.Empty;

            if (this._QuizSurveyObject != null)
            { pageTitle = this._QuizSurveyObject.Identifier; }
            else
            {
                pageTitle = _GlobalResources.NewQuiz;

                if (this.QueryStringString("type") == "survey")
                { pageTitle = _GlobalResources.NewSurvey; }
            }

            //Build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.QuizzesAndSurveys, "/administrator/quizzesandsurveys"));
            breadCrumbLinks.Add(new BreadcrumbLink(pageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_QUIZ, ImageFiles.EXT_PNG));
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb
            this._BuildBreadcrumbAndPageTitle();

            // clear the controls in the form container
            this.QuizSurveyFormContainer.Controls.Clear();

            // guid field
            this._QuizSurveyGUID = new HiddenField();
            this._QuizSurveyGUID.ID = "QuizSurveyGUIDField";
            this.QuizSurveyFormContainer.Controls.Add(this._QuizSurveyGUID);

            // identifer field
            this._QuizSurveyIdentifier = new HiddenField();
            this._QuizSurveyIdentifier.ID = "QuizSurveyIdentifierField";
            this.QuizSurveyFormContainer.Controls.Add(this._QuizSurveyIdentifier);

            // type field
            this._QuizSurveyType = new HiddenField();
            this._QuizSurveyType.ID = "QuizSurveyTypeField";
            this.QuizSurveyFormContainer.Controls.Add(this._QuizSurveyType);

            // configuration JSON
            this._QuizSurveyConfigurationJSON = new HiddenField();
            this._QuizSurveyConfigurationJSON.ID = "QuizSurveyConfigurationJSONField";
            this.QuizSurveyFormContainer.Controls.Add(this._QuizSurveyConfigurationJSON);

            // populate the fields
            this._PopulateQuizSurveyInputElements();

            // builds page action panel
            this._BuildActionsPanel();
        }
        #endregion

        #region _PopulateQuizSurveyInputElements
        /// <summary>
        /// Populates the input elements with values from the object.
        /// </summary>
        private void _PopulateQuizSurveyInputElements()
        {
            if (this._QuizSurveyObject != null)
            {
                // guid
                this._QuizSurveyGUID.Value = this._QuizSurveyObject.GUIDString;

                // identifier
                this._QuizSurveyIdentifier.Value = this._QuizSurveyObject.Identifier;

                // type
                switch (this._QuizSurveyObject.Type)
                {
                    case QuizSurvey.QuizSurveyType.Quiz:
                        this._QuizSurveyType.Value = "quiz";
                        break;
                    case QuizSurvey.QuizSurveyType.Survey:
                        this._QuizSurveyType.Value = "survey";
                        break;
                }

                // configuration JSON
                this._QuizSurveyConfigurationJSON.Value = this._QuizSurveyObject.JSONData;
            }
            else
            {
                // guid
                Guid newQuizSurveyGuid = Guid.NewGuid();
                this._QuizSurveyGUID.Value = newQuizSurveyGuid.ToString("D").ToUpper();

                // identifier
                this._QuizSurveyIdentifier.Value = String.Empty;

                // type
                this._QuizSurveyType.Value = String.Empty;

                // configuration JSON - DEFAULT TO QUIZ
                this._QuizSurveyConfigurationJSON.Value = "{\"ObjectType\": \"quiz\"}";

                if (this.QueryStringString("type") == "survey")
                { this._QuizSurveyConfigurationJSON.Value = "{\"ObjectType\": \"survey\"}"; }
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds actions panel.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // clear controls from container
            this.ActionsPanel.Controls.Clear();

            // style the actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // save as draft button
            this._SaveQuizSurveyAsDraft = new Button();
            this._SaveQuizSurveyAsDraft.ID = "SaveQuizSurveyAsDraft";
            this._SaveQuizSurveyAsDraft.CssClass = "Button ActionButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._QuizSurveyObject == null)
            { this._SaveQuizSurveyAsDraft.Text = _GlobalResources.CreateQuizSurvey; }
            else
            { this._SaveQuizSurveyAsDraft.Text = _GlobalResources.SaveChanges; }

            this._SaveQuizSurveyAsDraft.CommandName = "SaveAsDraft";
            this._SaveQuizSurveyAsDraft.Command += new CommandEventHandler(this._SaveQuizSurvey_Command);
            this._SaveQuizSurveyAsDraft.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicElements('SaveAsDraft');return false;");
            this.ActionsPanel.Controls.Add(this._SaveQuizSurveyAsDraft);

            // save and publish button
            this._SaveQuizSurveyAndPublish = new Button();
            this._SaveQuizSurveyAndPublish.ID = "SaveQuizSurveyAndPublish";
            this._SaveQuizSurveyAndPublish.CssClass = "Button ActionButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._QuizSurveyObject == null)
            { this._SaveQuizSurveyAndPublish.Text = _GlobalResources.CreateAndPublishQuizSurvey; }
            else
            { this._SaveQuizSurveyAndPublish.Text = _GlobalResources.SaveAndPublishChanges; }

            this._SaveQuizSurveyAndPublish.CommandName = "SaveAndPublish";
            this._SaveQuizSurveyAndPublish.Command += new CommandEventHandler(this._SaveQuizSurvey_Command);
            this._SaveQuizSurveyAndPublish.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicElements('SaveAndPublish');return false;");
            this.ActionsPanel.Controls.Add(this._SaveQuizSurveyAndPublish);

            // cancel button
            this._CancelQuizSurvey = new Button();
            this._CancelQuizSurvey.ID = "CancelButton";
            this._CancelQuizSurvey.CssClass = "Button NonActionButton";
            this._CancelQuizSurvey.Text = _GlobalResources.Cancel;
            this._CancelQuizSurvey.Command += new CommandEventHandler(this._CancelQuizSurvey_Command);
            this.ActionsPanel.Controls.Add(this._CancelQuizSurvey);
        }
        #endregion

        #region _SaveQuizSurvey_Command
        /// <summary>
        /// Handles Save Button Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _SaveQuizSurvey_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // if there is no quiz/survey object, create one
                if (this._QuizSurveyObject == null)
                { this._QuizSurveyObject = new QuizSurvey(); }

                this._QuizSurveyObject.GUIDString = this._QuizSurveyGUID.Value;
                this._QuizSurveyObject.Identifier = this._QuizSurveyIdentifier.Value;

                // kill characters in the identifier that can cause problems
                // these characters should already be prevented from being entered by JS, but do this here just in case
                this._QuizSurveyObject.Identifier = this._QuizSurveyObject.Identifier.Replace("\\", "");
                this._QuizSurveyObject.Identifier = this._QuizSurveyObject.Identifier.Replace("\"", "");
                this._QuizSurveyObject.Identifier = this._QuizSurveyObject.Identifier.Replace("<", "");
                this._QuizSurveyObject.Identifier = this._QuizSurveyObject.Identifier.Replace(">", "");

                this._QuizSurveyObject.IdAuthor = AsentiaSessionState.IdSiteUser;
                this._QuizSurveyObject.IsDraft = true;
                this._QuizSurveyObject.JSONData = this._QuizSurveyConfigurationJSON.Value;

                if (this._QuizSurveyType.Value == "quiz")
                { this._QuizSurveyObject.Type = QuizSurvey.QuizSurveyType.Quiz; }
                else
                { this._QuizSurveyObject.Type = QuizSurvey.QuizSurveyType.Survey; }

                // save the quiz/survey as draft
                int idQuizSurvey = this._QuizSurveyObject.Save();
                this.ViewState["id"] = idQuizSurvey;
                this._QuizSurveyObject.Id = idQuizSurvey;

                // load the saved quiz/survey object
                this._QuizSurveyObject = new QuizSurvey(idQuizSurvey);

                // if this is save and publish, publish it
                if (e.CommandName == "SaveAndPublish")
                {
                    ContentPackage contentPackageObject;

                    // if this quiz/survey has been published before, re-publish it by replacing the existing package
                    // else, create a new package and link that to the quiz/survey
                    if (this._QuizSurveyObject.IdContentPackage > 0)
                    {
                        // instansiate a content package object using the existing content package id
                        contentPackageObject = new ContentPackage((int)this._QuizSurveyObject.IdContentPackage);

                        // copy the shell into the warehouse folder
                        Utility.CopyDirectory(Server.MapPath(SitePathConstants._BIN + "quizSurveyScormWrapperTemplate"), Server.MapPath(contentPackageObject.Path), true, true);

                        // open the template manifest file, replace the replacers, and overwrite the existing one
                        string imsManifestContent = String.Empty;

                        using (StreamReader reader = new StreamReader(Server.MapPath(contentPackageObject.Path + "/_imsmanifest.xml")))
                        { imsManifestContent = reader.ReadToEnd(); }

                        imsManifestContent = Regex.Replace(imsManifestContent, "##guid##", this._QuizSurveyObject.GUIDString);
                        imsManifestContent = Regex.Replace(imsManifestContent, "##identifier##", this._QuizSurveyObject.Identifier);

                        using (StreamWriter writer = new StreamWriter(Server.MapPath(contentPackageObject.Path + "/imsmanifest.xml"), false))
                        { writer.Write(imsManifestContent); }

                        // overwrite the Quiz.json file with the new JSON data
                        using (StreamWriter writer = new StreamWriter(Server.MapPath(contentPackageObject.Path + "/Quiz.json"), false))
                        { writer.Write(this._QuizSurveyObject.JSONData); }

                        // update the draft status for the quiz/survey
                        this._QuizSurveyObject.IsDraft = false;
                        this._QuizSurveyObject.Save();
                    }
                    else
                    {
                        // get some values so that we can help ensure a unique warehouse folder, and begin to set-up the package
                        string currentDateTimeFormatted = AsentiaSessionState.UtcNow.ToString("yyyy-MM-dd-hh-mm-ss");
                        string dateTimeDownToMilliseconds = AsentiaSessionState.UtcNow.ToString("o").Replace(":", "-");

                        string newWarehouseFolderName = AsentiaSessionState.IdAccount
                                                        + "-"
                                                        + AsentiaSessionState.IdSite
                                                        + "-"
                                                        + Cryptography.GetHash(this._QuizSurveyObject.Identifier.Trim() + dateTimeDownToMilliseconds, Cryptography.HashType.MD5)
                                                        + "-"
                                                        + currentDateTimeFormatted;

                        // create the new folder and copy the quiz wrapper shell into it - CopyDirectory does both
                        Utility.CopyDirectory(Server.MapPath(SitePathConstants._BIN + "quizSurveyScormWrapperTemplate"), Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName));

                        // open the template manifest file, replace the replacers, and overwrite the existing one
                        string imsManifestContent = String.Empty;

                        using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/_imsmanifest.xml")))
                        { imsManifestContent = reader.ReadToEnd(); }

                        imsManifestContent = Regex.Replace(imsManifestContent, "##guid##", this._QuizSurveyObject.GUIDString);
                        imsManifestContent = Regex.Replace(imsManifestContent, "##identifier##", this._QuizSurveyObject.Identifier);

                        using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/imsmanifest.xml"), false))
                        { writer.Write(imsManifestContent); }

                        // overwrite the Quiz.json file with the new JSON data
                        using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/Quiz.json"), false))
                        { writer.Write(this._QuizSurveyObject.JSONData); }

                        // instansiate a content package object
                        contentPackageObject = new ContentPackage();
                        contentPackageObject.IdSite = AsentiaSessionState.IdSite;
                        contentPackageObject.Path = SitePathConstants.WAREHOUSE + newWarehouseFolderName;
                        contentPackageObject.Name = this._QuizSurveyObject.Identifier;
                        contentPackageObject.Kb = Utility.CalculateDirectorySizeInKb(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName));
                        contentPackageObject.Manifest = imsManifestContent;
                        contentPackageObject.IdContentPackageType = ContentPackage.ContentPackageType.SCORM;
                        contentPackageObject.IdSCORMPackageType = ContentPackage.SCORMPackageType.Content;
                        contentPackageObject.IdQuizSurvey = idQuizSurvey;

                        // save the content package
                        int idContentPackage = contentPackageObject.Save();

                        // update the draft status and content package id for the quiz/survey
                        this._QuizSurveyObject.IsDraft = false;
                        this._QuizSurveyObject.IdContentPackage = idContentPackage;
                        this._QuizSurveyObject.Save();
                    }
                }

                // rebuild the page controls
                this._BuildControls();

                // display the feedback
                if (e.CommandName == "SaveAndPublish")
                { this.DisplayFeedback(String.Format(_GlobalResources.TheQuizSurveyHasBeenSavedAndPublishedSuccessfullyYouCanViewThePublishedPackage, this._QuizSurveyObject.IdContentPackage), false); }
                else
                { this.DisplayFeedback(_GlobalResources.TheQuizSurveyHasBeenSavedSuccessfully, false); }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CancelQuizSurvey_Command
        /// <summary>
        /// Handles Cancel Button Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CancelQuizSurvey_Command(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        #endregion
    }
}
