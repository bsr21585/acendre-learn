﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.Packages
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel ContentPackagesFormContentWrapperContainer;
        public Panel GridAnalyticPanel;        
        public UpdatePanel PackageGridUpdatePanel;
        public Grid PackageGrid;
        public Panel ActionsPanel;
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup GridConfirmAction;
        #endregion

        #region Private Properties
        private LinkButton _UploadPackage;
        private LinkButton _UploadVideo;
        private LinkButton _UploadPowerPoint;
        private LinkButton _UploadPDF;

        private ModalPopup _UploadPackageModal;
        private ModalPopup _UploadVideoModal;
        private ModalPopup _UploadPowerPointModal;
        private ModalPopup _UploadPDFModal;
                       
        private UploaderAsync _PackageUploader;

        private RadioButtonList _VideoType;
        private TextBox _YouTubeEmbedCode;
        private TextBox _VimeoEmbedCode;
        private UploaderAsync _VideoUploader;
        private TextBox _VideoContentTitle;
        private CheckBox _EnableVideoAutoPlay;
        private CheckBox _EnableVideoRewind;
        private CheckBox _EnableVideoFastForward;
        private CheckBox _EnableVideoResume;
        private RadioButton _CompleteVideoOnStart;
        private RadioButton _CompleteVideoOnPercentage;
        private RadioButton _CompleteVideoOnEnd;
        private TextBox _VideoProgressForCompletionPercentage;

        private UploaderAsync _PowerPointUploader;
        private TextBox _PowerPointContentTitle;
        private CheckBox _EnablePowerPointAutoPlay;
        private CheckBox _EnablePowerPointLinearNavigation;
        private CheckBox _EnablePowerPointJumpNavigation;
        private CheckBox _EnablePowerPointBookmarking;
        private RadioButton _CompletePowerPointOnStart;
        private RadioButton _CompletePowerPointOnPercentage;
        private RadioButton _CompletePowerPointOnEnd;
        private TextBox _PowerPointProgressForCompletionPercentage;

        private UploaderAsync _PDFUploader;
        private TextBox _PDFContentTitle;
        private RadioButton _CompletePDFOnStart;
        private RadioButton _CompletePDFOnPercentage;
        private RadioButton _CompletePDFOnEnd;
        private TextBox _PDFProgressForCompletionPercentage;

        private const string _SCORM_MANIFEST_NAME = "imsmanifest.xml";
        private const string _XAPI_MANIFEST_NAME = "tincan.xml";

        private ContentPackage _PackageObject;

        // view content package utilization modal
        private ModalPopup _ViewContentPackageUtilizationModal;
        private Button _ViewContentPackageUtilizationModalHiddenLaunchButton;

        // download content package modal
        private ModalPopup _DownloadContentPackageModal;
        private Button _DownloadContentPackageModalHiddenLaunchButton;
        #endregion

        #region Page_Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // set the script timeout to 19 minutes (just shy of session timeout) to allow for larger uploads
            Server.ScriptTimeout = 1140;

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_ContentPackageManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("GridAnalytic.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/packages/Default.css");

            // clean-up old _download(s)
            Utility.EmptyOldFolderItems(Server.MapPath(SitePathConstants.DOWNLOAD), 60, true);

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ContentPackages));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, _GlobalResources.ContentPackages, ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.ContentPackagesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the grid, actions panel, and modals
            this._BuildObjectOptionsPanel();
            this._BuildUploadPackageModal();
            this._BuildUploadVideoModal();
            this._BuildUploadPowerPointModal();
            this._BuildUploadPDFModal();
            this._BuildViewContentPackageUtilizationModal();
            this._BuildDownloadContentPackageModal();
            this._BuildGridAnalytics();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.PackageGrid.BindData();
            }
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Packages.Default.js");

            // set up javascript globals 
            StringBuilder globalJS = new StringBuilder();
            globalJS.AppendLine("var ViewContentPackageUtilizationModalHiddenLaunchButtonId = \"" + this._ViewContentPackageUtilizationModalHiddenLaunchButton.ID + "\";");
            globalJS.AppendLine("var DownloadContentPackageModalHiddenLaunchButtonId = \"" + this._DownloadContentPackageModalHiddenLaunchButton.ID + "\";");            

            csm.RegisterClientScriptBlock(typeof(Default), "ContentPackagesGlobalJs", globalJS.ToString(), true);

            // register startup script to mark selected package type filter
            if (!String.IsNullOrWhiteSpace(this.QueryStringString("type")))
            {                
                StringBuilder markSelectedPackageTypeFilterJS = new StringBuilder();
                markSelectedPackageTypeFilterJS.AppendLine("$(\"#GridAnalyticPanel a[href*=\\\"?type=" + this.QueryStringString("type") + "\\\"]\").addClass(\"SelectedPackageTypeFilter\");");

                csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "MarkSelectedPackageTypeFilter", markSelectedPackageTypeFilterJS.ToString(), true);
            }
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // UPLOAD PACKAGE
            this._UploadPackage = new LinkButton();
            this._UploadPackage.ID = "UploadPackageButton";
            this._UploadPackage.OnClientClick = "InitializeUploadPackageModal(true)";

            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("UploadPackageLink",
                                                this._UploadPackage,
                                                null,
                                                null,
                                                _GlobalResources.UploadPackage,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG)) // ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG)
                );

            // UPLOAD VIDEO
            this._UploadVideo = new LinkButton();
            this._UploadVideo.ID = "UploadVideoButton";
            this._UploadVideo.OnClientClick = "InitializeUploadVideoModal(true)";

            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("UploadVideoLink",
                                                this._UploadVideo,
                                                null,
                                                null,
                                                _GlobalResources.ImportVideo,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_VIDEO, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_VIDEO, ImageFiles.EXT_PNG)) // ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG)
                );

            // UPLOAD POWERPOINT
            this._UploadPowerPoint = new LinkButton();
            this._UploadPowerPoint.ID = "UploadPowerPointButton";
            this._UploadPowerPoint.OnClientClick = "InitializeUploadPowerPointModal(true)";

            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("UploadPowerPointLink",
                                                this._UploadPowerPoint,
                                                null,
                                                null,
                                                _GlobalResources.ImportPowerPoint,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_POWERPOINT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_POWERPOINT, ImageFiles.EXT_PNG)) // ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG)
                );

            // UPLOAD PDF
            this._UploadPDF = new LinkButton();
            this._UploadPDF.ID = "UploadPDFButton";
            this._UploadPDF.OnClientClick = "InitializeUploadPDFModal(true)";

            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("UploadPDFLink",
                                                this._UploadPDF,
                                                null,
                                                null,
                                                _GlobalResources.ImportPDF,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_PDF, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_PDF, ImageFiles.EXT_PNG)) // ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG)
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGridAnalytics
        /// <summary>
        /// Builds the grid analytics for the page.
        /// </summary>
        private void _BuildGridAnalytics()
        {
            // get the analytic data
            DataTable analyticData = GridAnalyticData.ContentPackages();

            int total = Convert.ToInt32(analyticData.Rows[0]["total"]);
            int scorm = Convert.ToInt32(analyticData.Rows[0]["scorm"]);
            int xapi = Convert.ToInt32(analyticData.Rows[0]["xapi"]);
            int quizSurvey = Convert.ToInt32(analyticData.Rows[0]["quizSurvey"]);
            int video = Convert.ToInt32(analyticData.Rows[0]["video"]);
            int powerPoint = Convert.ToInt32(analyticData.Rows[0]["powerPoint"]);
            int pdf = Convert.ToInt32(analyticData.Rows[0]["pdf"]);
            int createdThisWeek = Convert.ToInt32(analyticData.Rows[0]["createdThisWeek"]);
            int createdThisMonth = Convert.ToInt32(analyticData.Rows[0]["createdThisMonth"]);
            int createdThisYear = Convert.ToInt32(analyticData.Rows[0]["createdThisYear"]);

            // build title for column 1
            GridAnalytic.DataBlock column1Title = new GridAnalytic.DataBlock(total.ToString("N0"), _GlobalResources.Packages, "/administrator/packages/");

            // build a DataBlock list for the column 1 data
            List<GridAnalytic.DataBlock> dataBlocksColumn1 = new List<GridAnalytic.DataBlock>();
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(scorm.ToString("N0"), _GlobalResources.SCORM.ToLower(), "?type=scorm"));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(xapi.ToString("N0"), _GlobalResources.xAPI.ToLower(), "?type=xapi"));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(quizSurvey.ToString("N0"), _GlobalResources.QuizSurvey.ToLower(), "?type=quizsurvey"));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(video.ToString("N0"), _GlobalResources.Video.ToLower(), "?type=video"));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(powerPoint.ToString("N0"), _GlobalResources.PowerPoint.ToLower(), "?type=powerpoint"));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(pdf.ToString("N0"), _GlobalResources.PDF.ToLower(), "?type=pdf"));
            
            GridAnalytic gridAnalyticObject = new GridAnalytic("ContentPackageStatistics", column1Title, dataBlocksColumn1);

            // attach the object to the container
            this.GridAnalyticPanel.Controls.Add(gridAnalyticObject);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.PackageGridUpdatePanel.Attributes.Add("class", "FormContentContainer");
            this.PackageGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;

            this.PackageGrid.StoredProcedure = Library.ContentPackage.GridProcedure;
            this.PackageGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.PackageGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.PackageGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);

            if (!String.IsNullOrWhiteSpace(this.QueryStringString("type")))
            { this.PackageGrid.AddFilter("@contentPackageOriginType", SqlDbType.NVarChar, 10, this.QueryStringString("type")); }

            this.PackageGrid.IdentifierField = "idContentPackage";
            this.PackageGrid.DefaultSortColumn = "name";            
            this.PackageGrid.ShowTimeInDateStrings = true;
            this.PackageGrid.SearchBoxPlaceholderText = _GlobalResources.SearchContentPackages;

            // data key names
            this.PackageGrid.DataKeyNames = new string[] { "idContentPackage" };

            // columns
            GridColumn nameSizeLastModifiedType = new GridColumn(_GlobalResources.Name + " (" + _GlobalResources.Size + "), " + _GlobalResources.LastModified + ", " + _GlobalResources.Type, null, "name"); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.PackageGrid.AddColumn(nameSizeLastModifiedType);
            this.PackageGrid.AddColumn(options); 

            // add row data bound event
            this.PackageGrid.RowDataBound += new GridViewRowEventHandler(this._PackageGrid_RowDataBound);
        }
        #endregion

        #region _PackageGrid_RowDataBound
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _PackageGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idContentPackage = Convert.ToInt32(rowView["idContentPackage"]);
                bool isPackageInUse = Convert.ToBoolean(rowView["isPackageInUse"]);

                bool isMediaUpload = false;
                if (!String.IsNullOrWhiteSpace(rowView["isMediaUpload"].ToString()))
                { isMediaUpload = Convert.ToBoolean(rowView["isMediaUpload"]); }
                
                ContentPackage.MediaType? idMediaType = null;
                if (!String.IsNullOrWhiteSpace(rowView["idMediaType"].ToString()))
                { idMediaType = (ContentPackage.MediaType)Convert.ToInt32(rowView["idMediaType"]); }

                bool isProcessed = false;
                if (!String.IsNullOrWhiteSpace(rowView["isProcessed"].ToString()))
                { isProcessed = Convert.ToBoolean(rowView["isProcessed"]); }

                int idQuizSurvey = 0;
                if (!String.IsNullOrWhiteSpace(rowView["idQuizSurvey"].ToString()))
                { idQuizSurvey = Convert.ToInt32(rowView["idQuizSurvey"]); }

                // if the package is in use, or is a media upload and not yet processed then disable the 
                // checkbox associated with it so that it cannot be deleted
                if (
                    isPackageInUse
                    || (isMediaUpload && (idMediaType == ContentPackage.MediaType.PowerPoint || idMediaType == ContentPackage.MediaType.PDF) && !isProcessed)
                   )
                {
                    string rowOrdinal = e.Row.DataItemIndex.ToString();
                    CheckBox rowCheckBox = (CheckBox)e.Row.Cells[0].FindControl(this.PackageGrid.ID + "_GridSelectRecord_" + rowOrdinal);

                    if (rowCheckBox != null)
                    { rowCheckBox.Enabled = false; }
                }

                // AVATAR, NAME, (SIZE), TYPE, LAST MODIFIED COLUMN

                string name = Server.HtmlEncode(rowView["name"].ToString());
                string size = Server.HtmlEncode(rowView["size"].ToString());
                string contentPackageType = Server.HtmlEncode(rowView["contentPackageType"].ToString());
                DateTime dtModified = Convert.ToDateTime(rowView["dtModified"]);                
                
                // avatar
                Image avatarImage = new Image();                
                avatarImage.CssClass = "GridAvatarImage";                
                e.Row.Cells[1].Controls.Add(avatarImage);

                // get the media/package type image
                if (isMediaUpload)
                {
                    // video
                    if (idMediaType == ContentPackage.MediaType.Video)
                    {                        
                        avatarImage.AlternateText = _GlobalResources.Video;
                        avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_VIDEO, ImageFiles.EXT_PNG);                      
                    }

                    // powerpoint
                    if (idMediaType == ContentPackage.MediaType.PowerPoint)
                    {                        
                        avatarImage.AlternateText = _GlobalResources.PowerPoint;
                        avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_POWERPOINT, ImageFiles.EXT_PNG);                     
                    }

                    // pdf
                    if (idMediaType == ContentPackage.MediaType.PDF)
                    {                        
                        avatarImage.AlternateText = _GlobalResources.PDF;
                        avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PDF, ImageFiles.EXT_PNG);                        
                    }
                }
                else // if its not a media upload, its a content package or a quiz/survey
                {
                    if (idQuizSurvey > 0)
                    {                        
                        avatarImage.AlternateText = _GlobalResources.QuizSurvey;
                        avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_QUIZ, ImageFiles.EXT_PNG);                     
                    }
                    else
                    {                        
                        avatarImage.AlternateText = _GlobalResources.ContentPackage;
                        avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG);                        
                    }
                }

                // name                
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                // if the package is a media upload and is not yet processed, just put the title in as plain text and replace the avatar with a loading icon
                if (isMediaUpload && (idMediaType == ContentPackage.MediaType.PowerPoint || idMediaType == ContentPackage.MediaType.PDF) && !isProcessed)
                {
                    Literal nameLit = new Literal();
                    nameLit.Text = name;
                    nameLabel.Controls.Add(nameLit);
                    
                    avatarImage.AlternateText = _GlobalResources.Processing;
                    avatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING, ImageFiles.EXT_GIF);                    
                }
                else // display name as modify link
                {                    
                    HyperLink nameLink = new HyperLink();
                    nameLink.NavigateUrl = "Modify.aspx?id=" + idContentPackage.ToString();
                    nameLink.Text = name;
                    nameLabel.Controls.Add(nameLink);
                }

                // size               
                Label sizeLabel = new Label();
                sizeLabel.CssClass = "GridSecondaryTitle";
                sizeLabel.Text = "(" + size + ")";
                e.Row.Cells[1].Controls.Add(sizeLabel);

                // dtModified
                dtModified = TimeZoneInfo.ConvertTimeFromUtc(dtModified, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                string dtModifiedText = dtModified.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern) + " @ " + dtModified.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern);

                Label dtModifiedLabel = new Label();
                dtModifiedLabel.CssClass = "GridSecondaryLine";
                dtModifiedLabel.Text = dtModifiedText;
                e.Row.Cells[1].Controls.Add(dtModifiedLabel);

                // type                
                Label typeLabel = new Label();
                typeLabel.CssClass = "GridSecondaryLine";
                e.Row.Cells[1].Controls.Add(typeLabel);

                // if the package is a quiz/survey, link to it; otherwise, just display text
                if (idQuizSurvey > 0)
                {
                    HyperLink typeLink = new HyperLink();
                    typeLink.NavigateUrl = "/administrator/quizzesandsurveys/Modify.aspx?id=" + idQuizSurvey.ToString();
                    typeLink.Text = _GlobalResources.QuizSurvey;
                    typeLabel.Controls.Add(typeLink);                    
                }
                else // display name as modify link
                {
                    Literal typeLit = new Literal();
                    typeLit.Text = contentPackageType;
                    typeLabel.Controls.Add(typeLit);
                }                

                // OPTIONS COLUMN

                // view package utilization
                Label viewPackageUtilizationSpan = new Label();
                viewPackageUtilizationSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(viewPackageUtilizationSpan);

                Image viewPackageUtilizationLink = new Image();
                viewPackageUtilizationLink.ID = "ViewPackageUtilizationLink_" + idContentPackage.ToString();
                viewPackageUtilizationLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);                
                viewPackageUtilizationSpan.Controls.Add(viewPackageUtilizationLink);

                // if the package is in use, make the "In Use" icon a clickable button that launches a modal
                // that shows the content that is using the package
                if (isPackageInUse)
                {
                    viewPackageUtilizationLink.AlternateText = _GlobalResources.ResourceIsBeingUsedByExistingModule_s;
                    viewPackageUtilizationLink.ToolTip = _GlobalResources.ClickToViewModulesThatUtilizeThisContentPackage;
                    viewPackageUtilizationLink.Attributes.Add("onclick", "LoadViewContentPackageUtilizationModalContent(" + idContentPackage.ToString() + ");");
                    viewPackageUtilizationLink.Style.Add("cursor", "pointer");   
                }
                else // package not in use
                {
                    viewPackageUtilizationLink.AlternateText = _GlobalResources.ResourceIsNotCurrentlyInUse;
                    viewPackageUtilizationLink.ToolTip = _GlobalResources.ResourceIsNotCurrentlyInUse;
                    viewPackageUtilizationLink.CssClass = "DimIcon";                    
                }

                // download package
                Label downloadPackageSpan = new Label();
                downloadPackageSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(downloadPackageSpan);

                Image downloadPackageLink = new Image();
                downloadPackageLink.ID = "DownloadPackageLink_" + idContentPackage.ToString();
                downloadPackageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE_BUTTON, ImageFiles.EXT_PNG);
                downloadPackageLink.AlternateText = _GlobalResources.Download;
                downloadPackageLink.ToolTip = _GlobalResources.DownloadPackage;
                downloadPackageLink.Attributes.Add("onclick", "LoadDownloadContentPackageModalContent(" + idContentPackage.ToString() + ");");
                downloadPackageLink.Style.Add("cursor", "pointer");
                downloadPackageSpan.Controls.Add(downloadPackageLink);
            }
        }
        #endregion

        #region _BuildViewContentPackageUtilizationModal
        /// <summary>
        /// 
        /// </summary>
        private void _BuildViewContentPackageUtilizationModal()
        {
            // instansiate the trigger button
            this._ViewContentPackageUtilizationModalHiddenLaunchButton = new Button();
            this._ViewContentPackageUtilizationModalHiddenLaunchButton.ID = "ViewContentPackageUtilizationModalHiddenLaunchButton";
            this._ViewContentPackageUtilizationModalHiddenLaunchButton.Style.Add("display", "none");
            this.ContentPackagesFormContentWrapperContainer.Controls.Add(this._ViewContentPackageUtilizationModalHiddenLaunchButton);

            // set modal properties
            this._ViewContentPackageUtilizationModal = new ModalPopup("ViewContentPackageUtilizationModal");
            this._ViewContentPackageUtilizationModal.Type = ModalPopupType.EmbeddedForm;
            this._ViewContentPackageUtilizationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG);
            this._ViewContentPackageUtilizationModal.HeaderIconAlt = _GlobalResources.Package;
            this._ViewContentPackageUtilizationModal.HeaderText = _GlobalResources.PackageUtilization;
            this._ViewContentPackageUtilizationModal.TargetControlID = this._ViewContentPackageUtilizationModalHiddenLaunchButton.ID;

            // hide the modal buttons

            // add modal to container
            this.ContentPackagesFormContentWrapperContainer.Controls.Add(this._ViewContentPackageUtilizationModal);
        }
        #endregion

        #region PackageUtilizationJsonData
        public struct PackageUtilizationJsonData
        {
            public bool actionSuccessful;
            public string html;
            public string exception;
            public int? idContentPackage;
            public string contentPackageTitle;
        }
        #endregion

        #region PopulateViewContentPackageUtilizationModal
        [WebMethod(EnableSession = true)]
        public static PackageUtilizationJsonData PopulateViewContentPackageUtilizationModal(int idContentPackage)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            PackageUtilizationJsonData jsonData = new PackageUtilizationJsonData();

            try
            {
                // get the content package object
                Asentia.LMS.Library.ContentPackage contentPackageObject = new Library.ContentPackage(idContentPackage);

                // get the lessons that are utilizing the content package
                DataTable utilizationInformation = contentPackageObject.GetUtilizationInformation();

                // CONTENT PACKAGE UTILIZATION - render control contentPackageUtilizationContainer

                Panel contentPackageUtilizationContainer = new Panel();
                contentPackageUtilizationContainer.ID = "ContentPackageUtilizationContainer";
                contentPackageUtilizationContainer.CssClass = "";

                // header paragraph
                Panel contentPackageUtilizationHeaderParagraphContainer = new Panel();
                contentPackageUtilizationHeaderParagraphContainer.ID = "ContentPackageUtilizationHeaderParagraphContainer";
                contentPackageUtilizationHeaderParagraphContainer.CssClass = "";
                contentPackageUtilizationContainer.Controls.Add(contentPackageUtilizationHeaderParagraphContainer);

                Literal contentPackageUtilizationHeaderParagraph = new Literal();
                contentPackageUtilizationHeaderParagraph.Text = String.Format(_GlobalResources.ContentPackage_CP_IsBeingUsedByTheFollowingModules, HttpContext.Current.Server.HtmlEncode(contentPackageObject.Name)) + ":";
                contentPackageUtilizationHeaderParagraphContainer.Controls.Add(contentPackageUtilizationHeaderParagraph);

                // lessons listing
                Panel contentPackageUtilizationLessonListingContainer = new Panel();
                contentPackageUtilizationLessonListingContainer.ID = "ContentPackageUtilizationLessonListingContainer";
                contentPackageUtilizationLessonListingContainer.CssClass = "ContentPackageUtilizationLessonListingContainer";
                contentPackageUtilizationContainer.Controls.Add(contentPackageUtilizationLessonListingContainer);

                HtmlGenericControl contentPackageUtilizationLessonListing = new HtmlGenericControl("ul");
                contentPackageUtilizationLessonListingContainer.Controls.Add(contentPackageUtilizationLessonListing);

                foreach (DataRow row in utilizationInformation.Rows)
                {
                    int idLesson = Convert.ToInt32(row["idLesson"]);
                    int idCourse = Convert.ToInt32(row["idCourse"]);
                    string lessonTitle = HttpContext.Current.Server.HtmlEncode(row["lessonTitle"].ToString());
                    string courseTitle = HttpContext.Current.Server.HtmlEncode(row["courseTitle"].ToString());

                    HtmlGenericControl contentPackageUtilizationLessonItem = new HtmlGenericControl("li");
                    contentPackageUtilizationLessonListing.Controls.Add(contentPackageUtilizationLessonItem);

                    HyperLink lessonModifyLink = new HyperLink();
                    lessonModifyLink.CssClass = "ContentPackageUtilizationLessonItemLessonTitle";
                    lessonModifyLink.NavigateUrl = "/administrator/courses/lessons/Modify.aspx?cid=" + idCourse + "&id=" + idLesson;
                    lessonModifyLink.Text = lessonTitle;
                    contentPackageUtilizationLessonItem.Controls.Add(lessonModifyLink);

                    Label courseParen = new Label();
                    courseParen.Text = " (" + _GlobalResources.Course + ": ";
                    courseParen.CssClass = "ContentPackageUtilizationLessonItemCourseTitle";
                    contentPackageUtilizationLessonItem.Controls.Add(courseParen);

                    HyperLink courseModifyLink = new HyperLink();
                    courseModifyLink.CssClass = "ContentPackageUtilizationLessonItemCourseTitle";
                    courseModifyLink.NavigateUrl = "/administrator/courses/Modify.aspx?id=" + idCourse;
                    courseModifyLink.Text = courseTitle;
                    contentPackageUtilizationLessonItem.Controls.Add(courseModifyLink);

                    Label closeParen = new Label();
                    closeParen.Text = ")";
                    closeParen.CssClass = "ContentPackageUtilizationLessonItemCourseTitle";
                    contentPackageUtilizationLessonItem.Controls.Add(closeParen);
                }

                // write the data to a text writer for json output
                TextWriter textWriter = new StringWriter();
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);

                contentPackageUtilizationContainer.RenderControl(htmlTextWriter);

                jsonData.actionSuccessful = true;
                jsonData.html += textWriter.ToString();
                jsonData.exception = String.Empty;
                jsonData.idContentPackage = (int)idContentPackage;
                jsonData.contentPackageTitle = HttpContext.Current.Server.HtmlEncode(contentPackageObject.Name);

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.idContentPackage = null;
                jsonData.contentPackageTitle = null;
            }

            // return jsonData
            return jsonData;
        }
        #endregion

        #region _BuildDownloadContentPackageModal
        /// <summary>
        /// 
        /// </summary>
        private void _BuildDownloadContentPackageModal()
        {
            // instansiate the trigger button
            this._DownloadContentPackageModalHiddenLaunchButton = new Button();
            this._DownloadContentPackageModalHiddenLaunchButton.ID = "DownloadContentPackageModalHiddenLaunchButton";
            this._DownloadContentPackageModalHiddenLaunchButton.Style.Add("display", "none");
            this.ContentPackagesFormContentWrapperContainer.Controls.Add(this._DownloadContentPackageModalHiddenLaunchButton);

            // set modal properties
            this._DownloadContentPackageModal = new ModalPopup("DownloadContentPackageModal");
            this._DownloadContentPackageModal.Type = ModalPopupType.EmbeddedForm;
            this._DownloadContentPackageModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DOWNLOAD, ImageFiles.EXT_PNG);
            this._DownloadContentPackageModal.HeaderIconAlt = _GlobalResources.Download;
            this._DownloadContentPackageModal.HeaderText = _GlobalResources.DownloadPackage;
            this._DownloadContentPackageModal.TargetControlID = this._DownloadContentPackageModalHiddenLaunchButton.ID;

            // hide the modal buttons

            // add modal to container
            this.ContentPackagesFormContentWrapperContainer.Controls.Add(this._DownloadContentPackageModal);
        }
        #endregion

        #region DownloadPackageJsonData
        public struct DownloadPackageJsonData
        {
            public bool actionSuccessful;
            public string html;
            public string exception;
            public int? idContentPackage;
            public string contentPackageTitle;
        }
        #endregion

        #region PopulateDownloadContentPackageModal
        [WebMethod(EnableSession = true)]
        public static DownloadPackageJsonData PopulateDownloadContentPackageModal(int idContentPackage)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            DownloadPackageJsonData jsonData = new DownloadPackageJsonData();

            try
            {
                // get the content package object
                Asentia.LMS.Library.ContentPackage contentPackageObject = new Library.ContentPackage(idContentPackage);

                // generate a string to be used as a folder for the download
                string downloadFolderName = Cryptography.GetHash(contentPackageObject.Name + DateTime.UtcNow.ToString("yyyy-MM-dd_HH-mm-ss"), Cryptography.HashType.MD5);

                // create the folder
                if (!Directory.Exists(SitePathConstants.DOWNLOAD + downloadFolderName))
                { Directory.CreateDirectory(HttpContext.Current.Server.MapPath(SitePathConstants.DOWNLOAD + downloadFolderName)); }

                // zip the content package into the folder, using its original name
                string zippedPackageFileName = contentPackageObject.Name;

                // append .zip to the name if its not already there
                if (!zippedPackageFileName.EndsWith(".zip"))
                { zippedPackageFileName += ".zip"; }

                ZipFile.CreateFromDirectory(HttpContext.Current.Server.MapPath(contentPackageObject.Path), HttpContext.Current.Server.MapPath(SitePathConstants.DOWNLOAD + downloadFolderName + "/" + zippedPackageFileName));

                // DOWNLOAD PACKAGE - render control downloadContentPackageLinkContainer

                Panel downloadContentPackageLinkContainer = new Panel();
                downloadContentPackageLinkContainer.ID = "DownloadContentPackageLinkContainer";
                downloadContentPackageLinkContainer.CssClass = "";

                // instructions
                Panel downloadContentPackageInstructionsContainer = new Panel();
                downloadContentPackageInstructionsContainer.ID = "DownloadContentPackageInstructionsContainer";
                downloadContentPackageLinkContainer.Controls.Add(downloadContentPackageInstructionsContainer);

                Literal downloadPackageInstructions = new Literal();
                downloadPackageInstructions.Text = _GlobalResources.ClickTheLinkBelowToDownloadYourPackage;
                downloadContentPackageInstructionsContainer.Controls.Add(downloadPackageInstructions);

                // link
                HyperLink downloadContentPackageLink = new HyperLink();
                downloadContentPackageLink.ID = "DownloadContentPackageLink";
                downloadContentPackageLink.NavigateUrl = SitePathConstants.DOWNLOAD + downloadFolderName + "/" + zippedPackageFileName;
                downloadContentPackageLink.Target = "_blank";
                downloadContentPackageLink.Text = zippedPackageFileName;
                downloadContentPackageLinkContainer.Controls.Add(downloadContentPackageLink);                

                // write the data to a text writer for json output
                TextWriter textWriter = new StringWriter();
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);

                downloadContentPackageLinkContainer.RenderControl(htmlTextWriter);

                jsonData.actionSuccessful = true;
                jsonData.html += textWriter.ToString();
                jsonData.exception = String.Empty;
                jsonData.idContentPackage = (int)idContentPackage;
                jsonData.contentPackageTitle = zippedPackageFileName;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                // DOWNLOAD PACKAGE ERROR - render control downloadContentPackageErrorContainer

                Panel downloadContentPackageErrorContainer = new Panel();
                downloadContentPackageErrorContainer.ID = "DownloadContentPackageErrorContainer";
                downloadContentPackageErrorContainer.CssClass = "";                

                Literal downloadPackageError = new Literal();
                downloadPackageError.Text = _GlobalResources.ThePackageCouldNotBePreparedForDownloadPleaseContactAnAdministrator;
                downloadContentPackageErrorContainer.Controls.Add(downloadPackageError);

                // write the data to a text writer for json output
                TextWriter textWriter = new StringWriter();
                HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);

                downloadPackageError.RenderControl(htmlTextWriter);

                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.idContentPackage = null;
                jsonData.contentPackageTitle = null;
            }

            // return jsonData
            return jsonData;
        }
        #endregion

        #region _BuildUploadPackageModal
        /// <summary>
        /// Builds the modal that contains the package uploader.
        /// </summary>
        private void _BuildUploadPackageModal()
        {
            // build the modal
            this._UploadPackageModal = new ModalPopup("UploadPackageModal");
            this._UploadPackageModal.Type = ModalPopupType.Form;
            this._UploadPackageModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE,
                                                                             ImageFiles.EXT_PNG);
            this._UploadPackageModal.HeaderIconAlt = _GlobalResources.Package;
            this._UploadPackageModal.HeaderText = _GlobalResources.UploadPackage;
            this._UploadPackageModal.TargetControlID = this._UploadPackage.ID;
            this._UploadPackageModal.SubmitButton.Command += new CommandEventHandler(this._SaveContentPackage_Package_Command);

            // build the modal body

            // file - file uploader
            List<Control> packageFileInputControls = new List<Control>();

            // file uploader
            this._PackageUploader = new UploaderAsync("PackageUploader", UploadType.ContentPackage, "PackageUploader_ErrorContainer");
            this._PackageUploader.ClientSideBeginJSMethod = "PackageFileUploadBegin";
            this._PackageUploader.ClientSideErrorJSMethod = "PackageFileUploadError";
            this._PackageUploader.ClientSideCompleteJSMethod = "PackageFileUploadComplete";
            packageFileInputControls.Add(this._PackageUploader);

            this._UploadPackageModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("PackageUploader",
                                                                                                     _GlobalResources.File,
                                                                                                     packageFileInputControls,
                                                                                                     false,
                                                                                                     true));

            // add modal to form content container
            this.PageContentContainer.Controls.Add(this._UploadPackageModal);
        }
        #endregion

        #region _SaveContentPackage_Package_Command
        /// <summary>
        /// Handles the "Submit" button click from the package uploader modal.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveContentPackage_Package_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // ensure that we have the uploaded package file
                if (String.IsNullOrWhiteSpace(this._PackageUploader.FileSavedName) || !File.Exists(Server.MapPath(this._PackageUploader.SavedFilePath)))
                {
                    // display feedback
                    this._UploadPackageModal.DisplayFeedback(_GlobalResources.PleaseSelectAFileToUpload, true);

                    // exit
                    return;
                }

                // get the package's original file name
                string originalFileName = this._PackageUploader.FileOriginalName;

                // get the date time down to ms
                string dateTimeDownToMilliseconds = AsentiaSessionState.UtcNow.ToString("o").Replace(":", "-");

                // get md5 hash of original file name and the date time down to ms
                string md5HashForPath = Cryptography.GetHash(originalFileName + dateTimeDownToMilliseconds, Cryptography.HashType.MD5);

                // get current date time formatted
                string currentDateTime = AsentiaSessionState.UtcNow.ToString("yyyy-MM-dd-hh-mm-ss");

                // build the folder path to extract the zip file to
                string destinationFolderPath = AsentiaSessionState.IdAccount
                                               + "-"
                                               + AsentiaSessionState.IdSite
                                               + "-"
                                               + md5HashForPath
                                               + "-"
                                               + currentDateTime;

                // Unzip the package to the destination folder in the content package upload folder
                this._ExtractZipArchive(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath),
                                        Server.MapPath(this._PackageUploader.SavedFilePath));

                // delete the uploaded zip file (it has been extracted and it is not needed now)
                File.Delete(Server.MapPath(this._PackageUploader.SavedFilePath));

                bool hasManifestFile = false;
                string manifestFileContent = String.Empty;
                string manifestNonFatalErrorDescriptions = String.Empty;
                ContentPackage.ContentPackageType contentPackageType = 0;
                ContentPackage.SCORMPackageType? scormPackageType = null;

                // look for tincan (xAPI) manifest
                if (File.Exists(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _XAPI_MANIFEST_NAME)))
                {
                    // identify the package as an xAPI package
                    contentPackageType = ContentPackage.ContentPackageType.xAPI;

                    // show that we found a manifest
                    hasManifestFile = true;

                    // NO VALIDATION IS NEEDED FOR A TINCAN PACKAGE

                    // read the entire manifest into the manifest file content string
                    using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _XAPI_MANIFEST_NAME)))
                    { manifestFileContent = reader.ReadToEnd(); }
                }
                // look for a scorm manifest
                else if (File.Exists(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _SCORM_MANIFEST_NAME)))
                {
                    // show that we found a manifest
                    hasManifestFile = true;

                    // identify the package as a SCORM package
                    contentPackageType = ContentPackage.ContentPackageType.SCORM;

                    // validate the SCORM manifest for fatal and non-fatal errors
                    Library.SCORM.ManifestValidator manifestValidator = new Library.SCORM.ManifestValidator();

                    // validate to fatal errors
                    ArrayList manifestFatalErrors = manifestValidator.ValidateManifestFileForFatalErrors(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _SCORM_MANIFEST_NAME));

                    // if there are errors, manifest is not valid AND FATAL, alert the user, then die
                    if (manifestFatalErrors != null && manifestFatalErrors.Count > 0)
                    {
                        string manifestFatalErrorDescriptions = String.Empty;

                        // provide feedback to the user for the invalid manifest                        
                        foreach (string error in manifestFatalErrors)
                        { manifestFatalErrorDescriptions += error + Environment.NewLine; }

                        // display feedback
                        this._UploadPackageModal.DisplayFeedback(String.Format(_GlobalResources.PackageUnableToBeSavedSuccessfullyDueToManifestErrors, originalFileName) + Environment.NewLine + Environment.NewLine + manifestFatalErrorDescriptions, true);

                        // since manifest validation failed, delete the directory.
                        Directory.Delete(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath), true);

                        // exit
                        return;
                    }

                    // validate to non-fatal errors
                    bool isResourcePackage;

                    ArrayList manifestNonFatalErrors = manifestValidator.ValidateManifestFileToXSD(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _SCORM_MANIFEST_NAME), out isResourcePackage);

                    // if there are errors, manifest is not valid, but the errors are not fatal, so we warn the user, and proceed
                    if (manifestNonFatalErrors != null && manifestNonFatalErrors.Count > 0)
                    {
                        // provide feedback to the user for the invalid manifest                        
                        foreach (Library.SCORM.ManifestValidator.ValidationError error in manifestNonFatalErrors)
                        { manifestNonFatalErrorDescriptions += "Line " + error.LineNumber + ": " + error.Desc + Environment.NewLine; }
                    }

                    // read the entire manifest into the manifest file content string
                    using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath + "\\" + _SCORM_MANIFEST_NAME)))
                    { manifestFileContent = reader.ReadToEnd(); }

                    // set the scorm package type
                    if (isResourcePackage)
                    { scormPackageType = ContentPackage.SCORMPackageType.Resource; }
                    else
                    { scormPackageType = ContentPackage.SCORMPackageType.Content; }
                }
                else
                { }

                // if there was no manifest file, fail
                if (!hasManifestFile)
                {
                    // display feedback
                    this._UploadPackageModal.DisplayFeedback(_GlobalResources.TheRequiredManifestIsMissing, true);

                    // since there is no manifest, delete the directory.
                    Directory.Delete(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath), true);

                    // exit
                    return;
                }

                // create a package object
                this._PackageObject = new ContentPackage();
                this._PackageObject.IdSite = AsentiaSessionState.IdSite;
                this._PackageObject.Name = originalFileName;
                this._PackageObject.Path = SitePathConstants.WAREHOUSE + destinationFolderPath;
                this._PackageObject.Manifest = manifestFileContent;
                this._PackageObject.IdContentPackageType = contentPackageType;
                this._PackageObject.HasManifestComplianceErrors = false;

                if (contentPackageType == ContentPackage.ContentPackageType.SCORM)
                { this._PackageObject.IdSCORMPackageType = scormPackageType; }
                else
                { this._PackageObject.IdSCORMPackageType = null; }

                // move the unzipped directory to its final destination in warehouse
                Directory.Move(Server.MapPath(SitePathConstants.UPLOAD_CONTENTPACKAGE + destinationFolderPath), Server.MapPath(SitePathConstants.WAREHOUSE + destinationFolderPath));

                // get the size in kb of the content directory
                this._PackageObject.Kb = Utility.CalculateDirectorySizeInKb(Server.MapPath(SitePathConstants.WAREHOUSE + destinationFolderPath));

                // if there were some "non-fatal" manifest errors, flag the package and record them
                if (!String.IsNullOrWhiteSpace(manifestNonFatalErrorDescriptions))
                {
                    this._PackageObject.HasManifestComplianceErrors = true;
                    this._PackageObject.ManifestComplianceErrors = manifestNonFatalErrorDescriptions;
                }

                // save the package to database
                int idPackage = _PackageObject.Save();

                // display feedback, with or without "non-fatal" errors
                if (String.IsNullOrWhiteSpace(manifestNonFatalErrorDescriptions))
                { this._UploadPackageModal.DisplayFeedback(String.Format(_GlobalResources.PackageSavedSuccessfully, originalFileName), false); }
                else
                {
                    string feedbackMessage = String.Format(_GlobalResources.PackageSavedSuccessfully, originalFileName);
                    feedbackMessage += Environment.NewLine;
                    feedbackMessage += _GlobalResources.HoweverThePackagesManifestIsNotSCORMCompliant;
                    feedbackMessage += Environment.NewLine + Environment.NewLine;
                    feedbackMessage += manifestNonFatalErrorDescriptions;

                    this._UploadPackageModal.DisplayFeedback(feedbackMessage, InformationStatusPanel_MessageType.Alert);
                }

                // rebind the grid
                this.PackageGrid.BindData();
                this.PackageGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._UploadPackageModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._UploadPackageModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._UploadPackageModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._UploadPackageModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._UploadPackageModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _ExtractZipArchive
        /// <summary>
        /// Unzips the content package zip file to a destination directory.
        /// </summary>
        /// <param name="unzipDestinationDirectory">unzip destination directory</param>
        /// <param name="zipToUnpack">path to zip file</param>
        private void _ExtractZipArchive(string unzipDestinationDirectory, string zipToUnpack)
        {            
            // new code, uses built in .NET object
            ZipFile.ExtractToDirectory(zipToUnpack, unzipDestinationDirectory);        
        }
        #endregion

        #region _BuildUploadVideoModal
        /// <summary>
        /// Builds the modal that contains the video uploader and options.
        /// </summary>
        private void _BuildUploadVideoModal()
        {
            // build the modal
            this._UploadVideoModal = new ModalPopup("UploadVideoModal");
            this._UploadVideoModal.Type = ModalPopupType.Form;
            this._UploadVideoModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_VIDEO,
                                                                           ImageFiles.EXT_PNG);
            this._UploadVideoModal.HeaderIconAlt = _GlobalResources.Video;
            this._UploadVideoModal.HeaderText = _GlobalResources.ImportVideo;
            this._UploadVideoModal.TargetControlID = this._UploadVideo.ID;
            this._UploadVideoModal.SubmitButton.Command += new CommandEventHandler(this._SaveContentPackage_Video_Command);

            // build the modal body

            // instructions panel
            Panel videoImportInstructionsPanel = new Panel();
            videoImportInstructionsPanel.ID = "VideoImportInstructionsPanel";
            this.FormatPageInformationPanel(videoImportInstructionsPanel, _GlobalResources.UseTheFormBelowToImportVideoImportedVideoWillBeConvertedIntoASCORMPackage, true);
            this._UploadVideoModal.AddControlToBody(videoImportInstructionsPanel);

            // type - file upload or vimeo
            this._VideoType = new RadioButtonList();
            this._VideoType.ID = "VideoType_Field";

            ListItem fileUpload = new ListItem();
            fileUpload.Text = _GlobalResources.FileUpload;
            fileUpload.Value = "default";
            fileUpload.Attributes.Add("onclick", "VideoTypeSelected(\"default\");");

            ListItem youTube = new ListItem();
            youTube.Text = _GlobalResources.YouTube;
            youTube.Value = "youtube";
            youTube.Attributes.Add("onclick", "VideoTypeSelected(\"youtube\");");

            ListItem vimeo = new ListItem();
            vimeo.Text = _GlobalResources.Vimeo;
            vimeo.Value = "vimeo";
            vimeo.Attributes.Add("onclick", "VideoTypeSelected(\"vimeo\");");

            this._VideoType.Items.Add(fileUpload);
            this._VideoType.Items.Add(youTube);
            this._VideoType.Items.Add(vimeo);
            this._VideoType.SelectedIndex = 0;

            this._UploadVideoModal.AddControlToBody(AsentiaPage.BuildFormField("VideoType",
                                                                               _GlobalResources.Type,
                                                                               this._VideoType.ID,
                                                                               this._VideoType,
                                                                               true,
                                                                               true,
                                                                               false));

            // file - file uploader or "embed code" textbox - based on type
            List<Control> videoFileInputControls = new List<Control>();

            // file uploader
            this._VideoUploader = new UploaderAsync("VideoUploader", UploadType.VideoFile, "VideoFile_ErrorContainer");
            this._VideoUploader.ClientSideBeginJSMethod = "VideoFileUploadBegin";
            this._VideoUploader.ClientSideErrorJSMethod = "VideoFileUploadError";
            this._VideoUploader.ClientSideCompleteJSMethod = "VideoFileUploadComplete";
            videoFileInputControls.Add(this._VideoUploader);

            // youtube embed code instructions
            Panel youTubeEmbedCodeInstructions = new Panel();
            youTubeEmbedCodeInstructions.ID = "YouTubeEmbedCodeInstructions";
            this.FormatFormInformationPanel(youTubeEmbedCodeInstructions, _GlobalResources.EnterTheFullYouTubeEmbedCodeInTheBoxBelow);
            videoFileInputControls.Add(youTubeEmbedCodeInstructions);

            // youtube embed code text box
            this._YouTubeEmbedCode = new TextBox();
            this._YouTubeEmbedCode.ID = "VideoFile_YouTubeEmbedCode_Field";
            this._YouTubeEmbedCode.Style.Add("width", "98%");
            this._YouTubeEmbedCode.TextMode = TextBoxMode.MultiLine;
            this._YouTubeEmbedCode.Rows = 5;
            videoFileInputControls.Add(this._YouTubeEmbedCode);

            // vimeo embed code instructions
            Panel vimeoEmbedCodeInstructions = new Panel();
            vimeoEmbedCodeInstructions.ID = "VimeoEmbedCodeInstructions";
            this.FormatFormInformationPanel(vimeoEmbedCodeInstructions, _GlobalResources.EnterTheFullVimeoEmbedCodeInTheBoxBelow);
            videoFileInputControls.Add(vimeoEmbedCodeInstructions);

            // vimeo embed code text box
            this._VimeoEmbedCode = new TextBox();
            this._VimeoEmbedCode.ID = "VideoFile_VimeoEmbedCode_Field";
            this._VimeoEmbedCode.Style.Add("width", "98%");
            this._VimeoEmbedCode.TextMode = TextBoxMode.MultiLine;
            this._VimeoEmbedCode.Rows = 5;
            videoFileInputControls.Add(this._VimeoEmbedCode);

            this._UploadVideoModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("VideoFile",
                                                                                                   _GlobalResources.Video,
                                                                                                   videoFileInputControls,
                                                                                                   true,
                                                                                                   true,
                                                                                                   true));

            // content title
            this._VideoContentTitle = new TextBox();
            this._VideoContentTitle.ID = "VideoContentTitle_Field";
            this._VideoContentTitle.CssClass = "InputLong";

            this._UploadVideoModal.AddControlToBody(AsentiaPage.BuildFormField("VideoContentTitle",
                                                                               _GlobalResources.ContentTitle,
                                                                               this._VideoContentTitle.ID,
                                                                               this._VideoContentTitle,
                                                                               true,
                                                                               true,
                                                                               false));

            // playback settings
            List<Control> playbackSettingsInputControls = new List<Control>();

            // autoplay
            this._EnableVideoAutoPlay = new CheckBox();
            this._EnableVideoAutoPlay.ID = "VideoPlaybackSettings_EnableVideoAutoplay_Field";
            this._EnableVideoAutoPlay.Text = _GlobalResources.AutomaticallyStartPlaybackWhenVideoIsLaunched;
            playbackSettingsInputControls.Add(this._EnableVideoAutoPlay);

            // rewind
            this._EnableVideoRewind = new CheckBox();
            this._EnableVideoRewind.ID = "VideoPlaybackSettings_EnableVideoRewind_Field";
            this._EnableVideoRewind.Text = _GlobalResources.AllowLearnerToJumpBackwardOrRewindVideo;
            playbackSettingsInputControls.Add(this._EnableVideoRewind);

            // fast forward
            this._EnableVideoFastForward = new CheckBox();
            this._EnableVideoFastForward.ID = "VideoPlaybackSettings_EnableVideoFastForward_Field";
            this._EnableVideoFastForward.Text = _GlobalResources.AllowLearnerToJumpForwardOrFastForwardVideo;
            playbackSettingsInputControls.Add(this._EnableVideoFastForward);

            // resume
            this._EnableVideoResume = new CheckBox();
            this._EnableVideoResume.ID = "VideoPlaybackSettings_EnableVideoResume_Field";
            this._EnableVideoResume.Text = _GlobalResources.AllowLearnerToResumePlaybackOnVideoReLaunch;
            playbackSettingsInputControls.Add(this._EnableVideoResume);

            this._UploadVideoModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("VideoPlaybackSettings",
                                                                                                   _GlobalResources.PlaybackSettings,
                                                                                                   playbackSettingsInputControls,
                                                                                                   false,
                                                                                                   true));
            
            // completion criteria
            List<Control> completionCriteriaInputControls = new List<Control>();

            // on start
            Panel onStartContainerPanel = new Panel();
            onStartContainerPanel.ID = "VideoCompletionCriteria_CompleteVideoOnStart_Container";

            this._CompleteVideoOnStart = new RadioButton();
            this._CompleteVideoOnStart.ID = "VideoCompletionCriteria_CompleteVideoOnStart_Field";
            this._CompleteVideoOnStart.GroupName = "VideoCompletionCriteria_VideoCompletionCriteria_Field";
            this._CompleteVideoOnStart.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onstart\", \"video\")");
            this._CompleteVideoOnStart.Text = _GlobalResources.SetCompletionOnVideoLaunch;

            onStartContainerPanel.Controls.Add(this._CompleteVideoOnStart);
            completionCriteriaInputControls.Add(onStartContainerPanel);

            // on percentage viewed
            Panel onPercentageContainerPanel = new Panel();
            onPercentageContainerPanel.ID = "VideoCompletionCriteria_CompleteVideoOnPercentage_Container";

            this._CompleteVideoOnPercentage = new RadioButton();
            this._CompleteVideoOnPercentage.ID = "VideoCompletionCriteria_CompleteVideoOnPercentage_Field";
            this._CompleteVideoOnPercentage.GroupName = "VideoCompletionCriteria_VideoCompletionCriteria_Field";
            this._CompleteVideoOnPercentage.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onpercentage\", \"video\")");
            onPercentageContainerPanel.Controls.Add(this._CompleteVideoOnPercentage);

            string[] splitDelimiter = new string[] {"[textbox]"};
            string[] completionOnPercentageLabelArray = _GlobalResources.SetCompletionWhenLearnerHasViewed__textbox__OfVideo.Split(splitDelimiter, StringSplitOptions.None);

            Label setCompletionWhenUserHasViewed = new Label();
            setCompletionWhenUserHasViewed.Text = completionOnPercentageLabelArray[0];
            setCompletionWhenUserHasViewed.AssociatedControlID = this._CompleteVideoOnPercentage.ID;
            onPercentageContainerPanel.Controls.Add(setCompletionWhenUserHasViewed);

            this._VideoProgressForCompletionPercentage = new TextBox();
            this._VideoProgressForCompletionPercentage.ID = "VideoCompletionCriteria_VideoProgressForCompletionPercentage_Field";
            this._VideoProgressForCompletionPercentage.CssClass = "InputXShort";
            onPercentageContainerPanel.Controls.Add(this._VideoProgressForCompletionPercentage);

            Label percentageOfVideo = new Label();
            percentageOfVideo.Text = completionOnPercentageLabelArray[1];
            percentageOfVideo.AssociatedControlID = this._CompleteVideoOnPercentage.ID;
            onPercentageContainerPanel.Controls.Add(percentageOfVideo);

            completionCriteriaInputControls.Add(onPercentageContainerPanel);

            // on end
            Panel onEndContainerPanel = new Panel();
            onEndContainerPanel.ID = "VideoCompletionCriteria_CompleteVideoOnEnd_Container";

            this._CompleteVideoOnEnd = new RadioButton();
            this._CompleteVideoOnEnd.ID = "VideoCompletionCriteria_CompleteVideoOnEnd_Field";
            this._CompleteVideoOnEnd.GroupName = "VideoCompletionCriteria_VideoCompletionCriteria_Field";
            this._CompleteVideoOnEnd.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onend\", \"video\")");
            this._CompleteVideoOnEnd.Text = _GlobalResources.SetCompletionWhenLearnerHasViewedTheEntireVideo;

            onEndContainerPanel.Controls.Add(this._CompleteVideoOnEnd);
            completionCriteriaInputControls.Add(onEndContainerPanel);

            this._UploadVideoModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("VideoCompletionCriteria",
                                                                                                   _GlobalResources.CompletionCriteria,
                                                                                                   completionCriteriaInputControls,
                                                                                                   true,
                                                                                                   true,
                                                                                                   false));

            // add modal to form content container
            this.PageContentContainer.Controls.Add(this._UploadVideoModal);
        }
        #endregion

        #region _ValidateVideoImportForm
        /// <summary>
        /// Validates the video import form based on the type selected.
        /// </summary>
        /// <param name="videoType"></param>
        /// <returns>true/false</returns>
        private bool _ValidateVideoImportForm(string videoType)
        {
            bool isValid = true;

            if (videoType == "default")
            { 
                // validate that a video was uploaded
                if (String.IsNullOrWhiteSpace(this._VideoUploader.FileSavedName) || !File.Exists(Server.MapPath(this._VideoUploader.SavedFilePath)))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoFile", _GlobalResources.PleaseSelectAFileToUpload);
                }

                // validate that a content title was entered
                if (String.IsNullOrWhiteSpace(this._VideoContentTitle.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoContentTitle", _GlobalResources.ContentTitle + " " + _GlobalResources.IsRequired);
                }

                // validate the completion criteria if it's a percentage
                if (this._CompleteVideoOnPercentage.Checked)
                {
                    int percentage;

                    if (
                        String.IsNullOrWhiteSpace(this._VideoProgressForCompletionPercentage.Text)
                        || !int.TryParse(this._VideoProgressForCompletionPercentage.Text, out percentage)
                        || (int.TryParse(this._VideoProgressForCompletionPercentage.Text, out percentage) && (percentage < 0 || percentage > 100))
                       )
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoCompletionCriteria", _GlobalResources.PercentageMustBeANumberBetween0And100);
                    }
                    else
                    {

                    }
                }
            }
            else if (videoType == "youtube")
            {
                // validate embed code
                if (String.IsNullOrWhiteSpace(this._YouTubeEmbedCode.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoFile", _GlobalResources.EmbedCodeIsRequired);
                }
                else // validate (the best we can) that the embed code is valid
                {
                    if (!this._YouTubeEmbedCode.Text.Contains("<iframe") && !this._YouTubeEmbedCode.Text.Contains("youtube"))
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoFile",  _GlobalResources.EmbedCodeDoesNotAppearToBeValidYouTubeEmbedCode);
                    }
                }

                // validate that a content title was entered
                if (String.IsNullOrWhiteSpace(this._VideoContentTitle.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoContentTitle", _GlobalResources.ContentTitle + " " + _GlobalResources.IsRequired);
                }
            }
            else if (videoType == "vimeo")
            {
                // validate embed code
                if (String.IsNullOrWhiteSpace(this._VimeoEmbedCode.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoFile", _GlobalResources.EmbedCodeIsRequired);
                }
                else // validate (the best we can) that the embed code is valid
                {
                    if (!this._VimeoEmbedCode.Text.Contains("<iframe") && !this._VimeoEmbedCode.Text.Contains("vimeo"))
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoFile", _GlobalResources.EmbedCodeDoesNotAppearToBeValidVimeoEmbedCode);
                    }
                }

                // validate that a content title was entered
                if (String.IsNullOrWhiteSpace(this._VideoContentTitle.Text))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoContentTitle", _GlobalResources.ContentTitle + " " + _GlobalResources.IsRequired);
                }

                // validate the completion criteria if it's a percentage
                if (this._CompleteVideoOnPercentage.Checked)
                {
                    int percentage;

                    if (
                        String.IsNullOrWhiteSpace(this._VideoProgressForCompletionPercentage.Text)
                        || !int.TryParse(this._VideoProgressForCompletionPercentage.Text, out percentage)
                        || (int.TryParse(this._VideoProgressForCompletionPercentage.Text, out percentage) && (percentage < 0 || percentage > 100))
                       )
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoCompletionCriteria", _GlobalResources.PercentageMustBeANumberBetween0And100);
                    }
                    else
                    {

                    }
                }
            }
            else
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoType", _GlobalResources.YouMustSelectAValidVideoType);
            }

            return isValid;
        }
        #endregion

        #region _SaveContentPackage_Video_Command
        /// <summary>
        /// Handles the "Submit" button click from the video uploader modal.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveContentPackage_Video_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!_ValidateVideoImportForm(this._VideoType.SelectedValue))
                {
                    // fire a startup script so that the hidden/shown states are retained
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Default), "RetainVideoTypeStateOnPostbackWithErrors", "RetainVideoTypeStateOnPostbackWithErrors();", true);

                    // display feedback and return
                    this._UploadVideoModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                    return;
                }

                // get some values so that we can help ensure a unique warehouse folder
                string currentDateTimeFormatted = AsentiaSessionState.UtcNow.ToString("yyyy-MM-dd-hh-mm-ss");
                string dateTimeDownToMilliseconds = AsentiaSessionState.UtcNow.ToString("o").Replace(":", "-");

                // get a new guid to serve as the manifest identifier, and begin to set-up the package                
                string manifestIdentifier = "_" + Guid.NewGuid().ToString();
                string newWarehouseFolderName = AsentiaSessionState.IdAccount
                                                + "-"
                                                + AsentiaSessionState.IdSite
                                                + "-"
                                                + Cryptography.GetHash(this._VideoContentTitle.Text.Trim() + dateTimeDownToMilliseconds, Cryptography.HashType.MD5)
                                                + "-"
                                                + currentDateTimeFormatted;

                // create the new folder and copy the video scorm wrapper shell into it - CopyDirectory does both
                Utility.CopyDirectory(Server.MapPath(SitePathConstants._BIN + "videoScormWrapperTemplate"), Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName));

                // instansiate a package object
                this._PackageObject = new ContentPackage();
                this._PackageObject.IdSite = AsentiaSessionState.IdSite;
                this._PackageObject.Path = SitePathConstants.WAREHOUSE + newWarehouseFolderName;
                this._PackageObject.IdContentPackageType = ContentPackage.ContentPackageType.SCORM;
                this._PackageObject.IdSCORMPackageType = ContentPackage.SCORMPackageType.Content;
                this._PackageObject.IsMediaUpload = true;
                this._PackageObject.IdMediaType = ContentPackage.MediaType.Video;
                this._PackageObject.ContentTitle = this._VideoContentTitle.Text;

                // do video type specific package properties
                string videoPathForConfigFile = String.Empty;

                if (this._VideoType.SelectedValue == "default")
                {
                    // name information
                    this._PackageObject.Name = this._VideoUploader.FileOriginalName;
                    this._PackageObject.OriginalMediaFilename = this._VideoUploader.FileOriginalName;
                    this._PackageObject.IsVideoMedia3rdParty = false;
                    videoPathForConfigFile = "video/" + this._VideoUploader.FileOriginalName;

                    // playback settings
                    this._PackageObject.EnableAutoplay = this._EnableVideoAutoPlay.Checked;
                    this._PackageObject.AllowFastForward = this._EnableVideoFastForward.Checked;
                    this._PackageObject.AllowRewind = this._EnableVideoRewind.Checked;
                    this._PackageObject.AllowResume = this._EnableVideoResume.Checked;
                    
                    // package completion criteria
                    if (this._CompleteVideoOnStart.Checked)
                    { this._PackageObject.MinProgressForCompletion = 0; }
                    else if (this._CompleteVideoOnPercentage.Checked)
                    { this._PackageObject.MinProgressForCompletion = Convert.ToDouble(this._VideoProgressForCompletionPercentage.Text) / 100; }
                    else if (this._CompleteVideoOnEnd.Checked)
                    { this._PackageObject.MinProgressForCompletion = 1; }
                    else
                    { this._PackageObject.MinProgressForCompletion = 0; }

                    // copy the uploaded file to the package's video directory and delete the uploaded file
                    string fullVideoFilePath = SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/video/" + this._VideoUploader.FileOriginalName;

                    File.Copy(Server.MapPath(this._VideoUploader.SavedFilePath), Server.MapPath(fullVideoFilePath), true);
                    File.Delete(Server.MapPath(this._VideoUploader.SavedFilePath));
                }
                else if (this._VideoType.SelectedValue == "youtube")
                {
                    // name information
                    this._PackageObject.Name = this._VideoContentTitle.Text;
                    this._PackageObject.IsVideoMedia3rdParty = true;
                    this._PackageObject.VideoMediaEmbedCode = Regex.Replace(this._YouTubeEmbedCode.Text, @"\r\n?|\n", "");
                    videoPathForConfigFile = this._PackageObject.VideoMediaEmbedCode;

                    // playback settings
                    this._PackageObject.EnableAutoplay = false;
                    this._PackageObject.AllowFastForward = false;
                    this._PackageObject.AllowRewind = false;
                    this._PackageObject.AllowResume = false;

                    // package completion criteria
                    this._PackageObject.MinProgressForCompletion = 0;
                }
                else if (this._VideoType.SelectedValue == "vimeo")
                {
                    // name information
                    this._PackageObject.Name = this._VideoContentTitle.Text;
                    this._PackageObject.IsVideoMedia3rdParty = true;
                    this._PackageObject.VideoMediaEmbedCode = Regex.Replace(this._VimeoEmbedCode.Text, @"\r\n?|\n", "");
                    videoPathForConfigFile = this._PackageObject.VideoMediaEmbedCode;

                    // playback settings
                    this._PackageObject.EnableAutoplay = false;
                    this._PackageObject.AllowFastForward = false;
                    this._PackageObject.AllowRewind = false;
                    this._PackageObject.AllowResume = this._EnableVideoResume.Checked;

                    // package completion criteria
                    if (this._CompleteVideoOnStart.Checked)
                    { this._PackageObject.MinProgressForCompletion = 0; }
                    else if (this._CompleteVideoOnPercentage.Checked)
                    { this._PackageObject.MinProgressForCompletion = Convert.ToDouble(this._VideoProgressForCompletionPercentage.Text) / 100; }
                    else if (this._CompleteVideoOnEnd.Checked)
                    { this._PackageObject.MinProgressForCompletion = 1; }
                    else
                    { this._PackageObject.MinProgressForCompletion = 0; }
                }

                // open the package's imsmanifest.xml, index.html, and /script/config.js files to replace placeholders with package specific information

                // imsmanifest.xml
                string imsManifestContent = String.Empty;

                using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/imsmanifest.xml")))
                { imsManifestContent = reader.ReadToEnd(); }

                imsManifestContent = Regex.Replace(imsManifestContent, "##manifestIdentifier##", manifestIdentifier);
                imsManifestContent = Regex.Replace(imsManifestContent, "##contentTitle##", this._PackageObject.ContentTitle);
				
                if (this._PackageObject.MinProgressForCompletion == 0)
                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>0</adlcp:completionThreshold>"); }
                else if (this._PackageObject.MinProgressForCompletion == 1)
                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>1</adlcp:completionThreshold>"); }
                else if (this._PackageObject.MinProgressForCompletion > 0 && this._PackageObject.MinProgressForCompletion < 1)
                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>" + String.Format("{0:0.0}", this._PackageObject.MinProgressForCompletion) + "</adlcp:completionThreshold>"); }
                else
                { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", ""); }

                File.Delete(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/imsmanifest.xml"));

                using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/imsmanifest.xml")))
                { writer.Write(imsManifestContent); }

                // index.html
                string indexContent = String.Empty;

                using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/index.html")))
                { indexContent = reader.ReadToEnd(); }

                indexContent = Regex.Replace(indexContent, "##contentTitle##", this._PackageObject.ContentTitle);

                File.Delete(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/index.html"));

                using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/index.html")))
                { writer.Write(indexContent); }

                // /script/config.js
                string configContent = String.Empty;

                using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/script/config.js")))
                { configContent = reader.ReadToEnd(); }

                configContent = Regex.Replace(configContent, "##videoPath##", videoPathForConfigFile.Replace("\"", "\\\""));
                configContent = Regex.Replace(configContent, "##videoType##", this._VideoType.SelectedValue.ToLower());
                configContent = Regex.Replace(configContent, "##allowFastForward##", this._PackageObject.AllowFastForward.ToString().ToLower());
                configContent = Regex.Replace(configContent, "##allowRewind##", this._PackageObject.AllowRewind.ToString().ToLower());
                configContent = Regex.Replace(configContent, "##allowResume##", this._PackageObject.AllowResume.ToString().ToLower());
                configContent = Regex.Replace(configContent, "##enableAutoplay##", this._PackageObject.EnableAutoplay.ToString().ToLower());
                configContent = Regex.Replace(configContent, "##minProgressForCompletion##", this._PackageObject.MinProgressForCompletion.ToString());

                File.Delete(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/script/config.js"));

                using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/script/config.js")))
                { writer.Write(configContent); }
                
                // write the package size and manifext text into the package object
                this._PackageObject.Kb = Utility.CalculateDirectorySizeInKb(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName));
                this._PackageObject.Manifest = imsManifestContent;

                // save the package to database
                int idPackage = this._PackageObject.Save();

                // display feedback
                this._UploadVideoModal.DisplayFeedback(String.Format(_GlobalResources.VideoHasBeenImportedSuccessfully, this._PackageObject.Name), false);

                // fire a startup script to re-initialize the form, so more content can be imported
                ScriptManager.RegisterStartupScript(this.Page, typeof(Default), "InitializeUploadVideoModalOnPostbackSuccess", "InitializeUploadVideoModal(false);", true);

                // rebind the grid
                this.PackageGrid.BindData();
                this.PackageGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._UploadVideoModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._UploadVideoModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._UploadVideoModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._UploadVideoModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._UploadVideoModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _BuildUploadPowerPointModal
        /// <summary>
        /// Builds the modal that contains the powerpoint uploader and options.
        /// </summary>
        private void _BuildUploadPowerPointModal()
        {
            // build the modal
            this._UploadPowerPointModal = new ModalPopup("UploadPowerPointModal");
            this._UploadPowerPointModal.Type = ModalPopupType.Form;
            this._UploadPowerPointModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_POWERPOINT,
                                                                                ImageFiles.EXT_PNG);
            this._UploadPowerPointModal.HeaderIconAlt = _GlobalResources.PowerPoint;
            this._UploadPowerPointModal.HeaderText = _GlobalResources.ImportPowerPoint;
            this._UploadPowerPointModal.TargetControlID = this._UploadPowerPoint.ID;
            this._UploadPowerPointModal.SubmitButton.Command += new CommandEventHandler(this._SaveContentPackage_PowerPoint_Command);

            // build the modal body

            // instructions panel
            Panel powerPointImportInstructionsPanel = new Panel();
            powerPointImportInstructionsPanel.ID = "PowerPointImportInstructionsPanel";
            this.FormatPageInformationPanel(powerPointImportInstructionsPanel, _GlobalResources.UseTheFormBelowToImportAPowerPointFileThePowerPointFileWillBeProcessedIntoSlideImages, true);
            this._UploadPowerPointModal.AddControlToBody(powerPointImportInstructionsPanel);

            // file - file uploader
            List<Control> powerPointFileInputControls = new List<Control>();

            // file uploader
            this._PowerPointUploader = new UploaderAsync("PowerPointUploader", UploadType.PowerPointFile, "PowerPointUploader_ErrorContainer");
            this._PowerPointUploader.ClientSideBeginJSMethod = "PowerPointFileUploadBegin";
            this._PowerPointUploader.ClientSideErrorJSMethod = "PowerPointFileUploadError";
            this._PowerPointUploader.ClientSideCompleteJSMethod = "PowerPointFileUploadComplete";
            powerPointFileInputControls.Add(this._PowerPointUploader);

            this._UploadPowerPointModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("PowerPointUploader",
                                                                                                        _GlobalResources.File,
                                                                                                        powerPointFileInputControls,
                                                                                                        false,
                                                                                                        true));

            // content title
            this._PowerPointContentTitle = new TextBox();
            this._PowerPointContentTitle.ID = "PowerPointContentTitle_Field";
            this._PowerPointContentTitle.CssClass = "InputLong";

            this._UploadPowerPointModal.AddControlToBody(AsentiaPage.BuildFormField("PowerPointContentTitle",
                                                                                    _GlobalResources.ContentTitle,
                                                                                    this._PowerPointContentTitle.ID,
                                                                                    this._PowerPointContentTitle,
                                                                                    true,
                                                                                    true,
                                                                                    false));

            // playback settings
            List<Control> playbackSettingsInputControls = new List<Control>();

            // autoplay
            this._EnablePowerPointAutoPlay = new CheckBox();
            this._EnablePowerPointAutoPlay.ID = "PowerPointPlaybackSettings_EnablePowerPointAutoplay_Field";
            this._EnablePowerPointAutoPlay.Text = _GlobalResources.AutomaticallyStartPlaybackWhenContentIsLaunched;
            playbackSettingsInputControls.Add(this._EnablePowerPointAutoPlay);

            // linear navigation
            this._EnablePowerPointLinearNavigation = new CheckBox();
            this._EnablePowerPointLinearNavigation.ID = "PowerPointPlaybackSettings_EnablePowerPointLinearNavigation_Field";
            this._EnablePowerPointLinearNavigation.Text = _GlobalResources.AllowLearnerToNavigateThroughSlidesInLinearOrder;
            playbackSettingsInputControls.Add(this._EnablePowerPointLinearNavigation);

            // jump navigation
            this._EnablePowerPointJumpNavigation = new CheckBox();
            this._EnablePowerPointJumpNavigation.ID = "PowerPointPlaybackSettings_EnablePowerPointJumpNavigation_Field";
            this._EnablePowerPointJumpNavigation.Text = _GlobalResources.AllowLearnerToNavigateThroughSlidesInAnyOrder;
            this._EnablePowerPointJumpNavigation.Attributes.Add("onclick", "TogglePowerPointJumpNavigation();");
            playbackSettingsInputControls.Add(this._EnablePowerPointJumpNavigation);

            // resume
            this._EnablePowerPointBookmarking = new CheckBox();
            this._EnablePowerPointBookmarking.ID = "PowerPointPlaybackSettings_EnablePowerPointBookmarking_Field";
            this._EnablePowerPointBookmarking.Text = _GlobalResources.AllowLearnerToResumePlaybackOnContentReLaunch;
            playbackSettingsInputControls.Add(this._EnablePowerPointBookmarking);

            this._UploadPowerPointModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("PowerPointPlaybackSettings",
                                                                                                        _GlobalResources.PlaybackSettings,
                                                                                                        playbackSettingsInputControls,
                                                                                                        false,
                                                                                                        true));

            // completion criteria
            List<Control> completionCriteriaInputControls = new List<Control>();

            // on start
            Panel onStartContainerPanel = new Panel();
            onStartContainerPanel.ID = "PowerPointCompletionCriteria_CompletePowerPointOnStart_Container";

            this._CompletePowerPointOnStart = new RadioButton();
            this._CompletePowerPointOnStart.ID = "PowerPointCompletionCriteria_CompletePowerPointOnStart_Field";
            this._CompletePowerPointOnStart.GroupName = "PowerPointCompletionCriteria_PowerPointCompletionCriteria_Field";
            this._CompletePowerPointOnStart.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onstart\", \"powerpoint\")");
            this._CompletePowerPointOnStart.Text = _GlobalResources.SetCompletionOnContentLaunch;

            onStartContainerPanel.Controls.Add(this._CompletePowerPointOnStart);
            completionCriteriaInputControls.Add(onStartContainerPanel);

            // on percentage viewed
            Panel onPercentageContainerPanel = new Panel();
            onPercentageContainerPanel.ID = "PowerPointCompletionCriteria_CompletePowerPointOnPercentage_Container";

            this._CompletePowerPointOnPercentage = new RadioButton();
            this._CompletePowerPointOnPercentage.ID = "PowerPointCompletionCriteria_CompletePowerPointOnPercentage_Field";
            this._CompletePowerPointOnPercentage.GroupName = "PowerPointCompletionCriteria_PowerPointCompletionCriteria_Field";
            this._CompletePowerPointOnPercentage.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onpercentage\", \"powerpoint\")");
            onPercentageContainerPanel.Controls.Add(this._CompletePowerPointOnPercentage);

            string[] splitDelimiter = new string[] { "[textbox]" };
            string[] completionOnPercentageLabelArray = _GlobalResources.SetCompletionWhenLearnerHasViewed__textbox__OfSlides.Split(splitDelimiter, StringSplitOptions.None);

            Label setCompletionWhenUserHasViewed = new Label();
            setCompletionWhenUserHasViewed.Text = completionOnPercentageLabelArray[0];
            setCompletionWhenUserHasViewed.AssociatedControlID = this._CompletePowerPointOnPercentage.ID;
            onPercentageContainerPanel.Controls.Add(setCompletionWhenUserHasViewed);

            this._PowerPointProgressForCompletionPercentage = new TextBox();
            this._PowerPointProgressForCompletionPercentage.ID = "PowerPointCompletionCriteria_PowerPointProgressForCompletionPercentage_Field";
            this._PowerPointProgressForCompletionPercentage.CssClass = "InputXShort";
            onPercentageContainerPanel.Controls.Add(this._PowerPointProgressForCompletionPercentage);

            Label percentageOfSlides = new Label();
            percentageOfSlides.Text = completionOnPercentageLabelArray[1];
            percentageOfSlides.AssociatedControlID = this._CompletePowerPointOnPercentage.ID;
            onPercentageContainerPanel.Controls.Add(percentageOfSlides);

            completionCriteriaInputControls.Add(onPercentageContainerPanel);

            // on end
            Panel onEndContainerPanel = new Panel();
            onEndContainerPanel.ID = "PowerPointCompletionCriteria_CompletePowerPointOnEnd_Container";

            this._CompletePowerPointOnEnd = new RadioButton();
            this._CompletePowerPointOnEnd.ID = "PowerPointCompletionCriteria_CompletePowerPointOnEnd_Field";
            this._CompletePowerPointOnEnd.GroupName = "PowerPointCompletionCriteria_PowerPointCompletionCriteria_Field";
            this._CompletePowerPointOnEnd.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onend\", \"powerpoint\")");
            this._CompletePowerPointOnEnd.Text = _GlobalResources.SetCompletionWhenLearnerHasViewedAllSlides;

            onEndContainerPanel.Controls.Add(this._CompletePowerPointOnEnd);
            completionCriteriaInputControls.Add(onEndContainerPanel);

            this._UploadPowerPointModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("PowerPointCompletionCriteria",
                                                                                                        _GlobalResources.CompletionCriteria,
                                                                                                        completionCriteriaInputControls,
                                                                                                        true,
                                                                                                        true,
                                                                                                        false));

            // add modal to form content container
            this.PageContentContainer.Controls.Add(this._UploadPowerPointModal);
        }
        #endregion

        #region _ValidatePowerPointImportForm
        /// <summary>
        /// Validates the powerpoint import form.
        /// </summary>
        /// <param name="videoType"></param>
        /// <returns>true/false</returns>
        private bool _ValidatePowerPointImportForm()
        {
            bool isValid = true;

            // validate that a powerpoint file was uploaded
            if (String.IsNullOrWhiteSpace(this._PowerPointUploader.FileSavedName) || !File.Exists(Server.MapPath(this._PowerPointUploader.SavedFilePath)))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "PowerPointUploader", _GlobalResources.PleaseSelectAFileToUpload);
            }

            // validate that a content title was entered
            if (String.IsNullOrWhiteSpace(this._PowerPointContentTitle.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "PowerPointContentTitle", _GlobalResources.ContentTitle + " " + _GlobalResources.IsRequired);
            }

            // validate the completion criteria if it's a percentage
            if (this._CompletePowerPointOnPercentage.Checked)
            {
                int percentage;

                if (
                    String.IsNullOrWhiteSpace(this._PowerPointProgressForCompletionPercentage.Text)
                    || !int.TryParse(this._PowerPointProgressForCompletionPercentage.Text, out percentage)
                    || (int.TryParse(this._PowerPointProgressForCompletionPercentage.Text, out percentage) && (percentage < 0 || percentage > 100))
                   )
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "PowerPointCompletionCriteria", _GlobalResources.PercentageMustBeANumberBetween0And100);
                }
                else
                {

                }
            }

            return isValid;
        }
        #endregion

        #region _SaveContentPackage_PowerPoint_Command
        /// <summary>
        /// Handles the "Submit" button click from the powerpoint uploader modal.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveContentPackage_PowerPoint_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!_ValidatePowerPointImportForm())
                {
                    // fire a startup script so that the hidden/shown states are retained
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Default), "RetainPowerPointStateOnPostbackWithErrors", "RetainPowerPointStateOnPostbackWithErrors();", true);

                    // display feedback and return
                    this._UploadPowerPointModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                    return;
                }

                // get some values so that we can help ensure a unique warehouse folder
                string currentDateTimeFormatted = AsentiaSessionState.UtcNow.ToString("yyyy-MM-dd-hh-mm-ss");
                string dateTimeDownToMilliseconds = AsentiaSessionState.UtcNow.ToString("o").Replace(":", "-");

                // begin to set-up the package by creating a warehouse folder
                string newWarehouseFolderName = AsentiaSessionState.IdAccount
                                                + "-"
                                                + AsentiaSessionState.IdSite
                                                + "-"
                                                + Cryptography.GetHash(this._PowerPointContentTitle.Text.Trim() + dateTimeDownToMilliseconds, Cryptography.HashType.MD5)
                                                + "-"
                                                + currentDateTimeFormatted;

                // create the new folder and copy the powerpoint scorm wrapper shell into it - CopyDirectory does both
                Utility.CopyDirectory(Server.MapPath(SitePathConstants._BIN + "powerPointScormWrapperTemplate"), Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName));

                // copy the uploaded file to the package's root directory and delete the uploaded file
                string fullPowerPointFilePath = SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/" + this._PowerPointUploader.FileOriginalName;

                File.Copy(Server.MapPath(this._PowerPointUploader.SavedFilePath), Server.MapPath(fullPowerPointFilePath), true);
                File.Delete(Server.MapPath(this._PowerPointUploader.SavedFilePath));

                // instansiate a package object
                this._PackageObject = new ContentPackage();
                this._PackageObject.IdSite = AsentiaSessionState.IdSite;
                this._PackageObject.IdContentPackageType = ContentPackage.ContentPackageType.SCORM;
                this._PackageObject.IdSCORMPackageType = ContentPackage.SCORMPackageType.Content;
                this._PackageObject.Name = this._PowerPointUploader.FileOriginalName;
                this._PackageObject.Path = SitePathConstants.WAREHOUSE + newWarehouseFolderName;
                this._PackageObject.Kb = Utility.CalculateDirectorySizeInKb(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName)); // this is a placeholder, it will change once the PowerPoint processor processes this package
                this._PackageObject.Manifest = "<manifest></manifest>"; // this is a placeholder, it will change once the PowerPoint processor processes this package
                this._PackageObject.IsMediaUpload = true;
                this._PackageObject.IdMediaType = ContentPackage.MediaType.PowerPoint;
                this._PackageObject.ContentTitle = this._PowerPointContentTitle.Text;
                this._PackageObject.OriginalMediaFilename = this._PowerPointUploader.FileOriginalName;

                // playback settings
                this._PackageObject.EnableAutoplay = this._EnablePowerPointAutoPlay.Checked;
                this._PackageObject.AllowFastForward = this._EnablePowerPointLinearNavigation.Checked;
                this._PackageObject.AllowRewind = this._EnablePowerPointLinearNavigation.Checked;
                this._PackageObject.AllowNavigation = this._EnablePowerPointJumpNavigation.Checked;
                this._PackageObject.AllowResume = this._EnablePowerPointBookmarking.Checked;

                // package completion criteria
                if (this._CompletePowerPointOnStart.Checked)
                { this._PackageObject.MinProgressForCompletion = 0; }
                else if (this._CompletePowerPointOnPercentage.Checked)
                { this._PackageObject.MinProgressForCompletion = Convert.ToDouble(this._PowerPointProgressForCompletionPercentage.Text) / 100; }
                else if (this._CompletePowerPointOnEnd.Checked)
                { this._PackageObject.MinProgressForCompletion = 1; }
                else
                { this._PackageObject.MinProgressForCompletion = 0; }

                // processing information
                this._PackageObject.IsProcessing = false;
                this._PackageObject.IsProcessed = false;

                // save the package to database
                int idPackage = this._PackageObject.Save();

                // display feedback
                this._UploadPowerPointModal.DisplayFeedback(String.Format(_GlobalResources.PowerPointFileHasBeenSubmittedForProcessingOnceProcessedYouWillBeAbleToAssignThePackageToModule_s, this._PackageObject.Name), false);

                // fire a startup script to re-initialize the form, so more content can be imported
                ScriptManager.RegisterStartupScript(this.Page, typeof(Default), "InitializeUploadPowerPointModalOnPostbackSuccess", "InitializeUploadPowerPointModal(false);", true);

                // rebind the grid
                this.PackageGrid.BindData();
                this.PackageGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._UploadPowerPointModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._UploadPowerPointModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._UploadPowerPointModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._UploadPowerPointModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._UploadPowerPointModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _BuildUploadPDFModal
        /// <summary>
        /// Builds the modal that contains the pdf uploader and options.
        /// </summary>
        private void _BuildUploadPDFModal()
        {
            // build the modal
            this._UploadPDFModal = new ModalPopup("UploadPDFModal");
            this._UploadPDFModal.Type = ModalPopupType.Form;
            this._UploadPDFModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PDF,
                                                                         ImageFiles.EXT_PNG);
            this._UploadPDFModal.HeaderIconAlt = _GlobalResources.PDF;
            this._UploadPDFModal.HeaderText = _GlobalResources.ImportPDF;
            this._UploadPDFModal.TargetControlID = this._UploadPDF.ID;
            this._UploadPDFModal.SubmitButton.Command += new CommandEventHandler(this._SaveContentPackage_PDF_Command);

            // build the modal body

            // instructions panel
            Panel pdfImportInstructionsPanel = new Panel();
            pdfImportInstructionsPanel.ID = "PDFImportInstructionsPanel";
            this.FormatPageInformationPanel(pdfImportInstructionsPanel, _GlobalResources.UseTheFormBelowToImportAPDFOrMicrosofWordFile, true);
            this._UploadPDFModal.AddControlToBody(pdfImportInstructionsPanel);

            // file - file uploader
            List<Control> pdfFileInputControls = new List<Control>();

            // file uploader
            this._PDFUploader = new UploaderAsync("PDFUploader", UploadType.PDF, "PDFUploader_ErrorContainer");
            this._PDFUploader.ClientSideBeginJSMethod = "PDFFileUploadBegin";
            this._PDFUploader.ClientSideErrorJSMethod = "PDFFileUploadError";
            this._PDFUploader.ClientSideCompleteJSMethod = "PDFFileUploadComplete";
            pdfFileInputControls.Add(this._PDFUploader);

            this._UploadPDFModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("PDFUploader",
                                                                                                 _GlobalResources.File,
                                                                                                 pdfFileInputControls,
                                                                                                 false,
                                                                                                 true));

            // content title
            this._PDFContentTitle = new TextBox();
            this._PDFContentTitle.ID = "PDFContentTitle_Field";
            this._PDFContentTitle.CssClass = "InputLong";

            this._UploadPDFModal.AddControlToBody(AsentiaPage.BuildFormField("PDFContentTitle",
                                                                             _GlobalResources.ContentTitle,
                                                                             this._PDFContentTitle.ID,
                                                                             this._PDFContentTitle,
                                                                             true,
                                                                             true,
                                                                             false));

            // completion criteria
            List<Control> completionCriteriaInputControls = new List<Control>();

            // on start
            Panel onStartContainerPanel = new Panel();
            onStartContainerPanel.ID = "PDFCompletionCriteria_CompletePDFOnStart_Container";

            this._CompletePDFOnStart = new RadioButton();
            this._CompletePDFOnStart.ID = "PDFCompletionCriteria_CompletePDFOnStart_Field";
            this._CompletePDFOnStart.GroupName = "PDFCompletionCriteria_PDFCompletionCriteria_Field";
            this._CompletePDFOnStart.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onstart\", \"pdf\")");
            this._CompletePDFOnStart.Text = _GlobalResources.SetCompletionOnContentLaunch;

            onStartContainerPanel.Controls.Add(this._CompletePDFOnStart);
            completionCriteriaInputControls.Add(onStartContainerPanel);

            // on percentage viewed
            Panel onPercentageContainerPanel = new Panel();
            onPercentageContainerPanel.ID = "PDFCompletionCriteria_CompletePDFOnPercentage_Container";

            this._CompletePDFOnPercentage = new RadioButton();
            this._CompletePDFOnPercentage.ID = "PDFCompletionCriteria_CompletePDFOnPercentage_Field";
            this._CompletePDFOnPercentage.GroupName = "PDFCompletionCriteria_PDFCompletionCriteria_Field";
            this._CompletePDFOnPercentage.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onpercentage\", \"pdf\")");
            onPercentageContainerPanel.Controls.Add(this._CompletePDFOnPercentage);

            string[] splitDelimiter = new string[] { "[textbox]" };
            string[] completionOnPercentageLabelArray = _GlobalResources.SetCompletionWhenLearnerHasViewed__textbox__OfPages.Split(splitDelimiter, StringSplitOptions.None);

            Label setCompletionWhenUserHasViewed = new Label();
            setCompletionWhenUserHasViewed.Text = completionOnPercentageLabelArray[0];
            setCompletionWhenUserHasViewed.AssociatedControlID = this._CompletePDFOnPercentage.ID;
            onPercentageContainerPanel.Controls.Add(setCompletionWhenUserHasViewed);

            this._PDFProgressForCompletionPercentage = new TextBox();
            this._PDFProgressForCompletionPercentage.ID = "PDFCompletionCriteria_PDFProgressForCompletionPercentage_Field";
            this._PDFProgressForCompletionPercentage.CssClass = "InputXShort";
            onPercentageContainerPanel.Controls.Add(this._PDFProgressForCompletionPercentage);

            Label percentageOfSlides = new Label();
            percentageOfSlides.Text = completionOnPercentageLabelArray[1];
            percentageOfSlides.AssociatedControlID = this._CompletePDFOnPercentage.ID;
            onPercentageContainerPanel.Controls.Add(percentageOfSlides);

            completionCriteriaInputControls.Add(onPercentageContainerPanel);

            // on end
            Panel onEndContainerPanel = new Panel();
            onEndContainerPanel.ID = "PDFCompletionCriteria_CompletePDFOnEnd_Container";

            this._CompletePDFOnEnd = new RadioButton();
            this._CompletePDFOnEnd.ID = "PDFCompletionCriteria_CompletePDFOnEnd_Field";
            this._CompletePDFOnEnd.GroupName = "PDFCompletionCriteria_PDFCompletionCriteria_Field";
            this._CompletePDFOnEnd.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onend\", \"pdf\")");
            this._CompletePDFOnEnd.Text = _GlobalResources.SetCompletionWhenLearnerHasViewedAllPages;

            onEndContainerPanel.Controls.Add(this._CompletePDFOnEnd);
            completionCriteriaInputControls.Add(onEndContainerPanel);

            this._UploadPDFModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("PDFCompletionCriteria",
                                                                                                 _GlobalResources.CompletionCriteria,
                                                                                                 completionCriteriaInputControls,
                                                                                                 true,
                                                                                                 true,
                                                                                                 false));

            // add modal to form content container
            this.PageContentContainer.Controls.Add(this._UploadPDFModal);
        }
        #endregion

        #region _ValidatePDFImportForm
        /// <summary>
        /// Validates the pdf import form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePDFImportForm()
        {
            bool isValid = true;

            // validate that a pdf (or word) file was uploaded
            if (String.IsNullOrWhiteSpace(this._PDFUploader.FileSavedName) || !File.Exists(Server.MapPath(this._PDFUploader.SavedFilePath)))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "PDFUploader", _GlobalResources.PleaseSelectAFileToUpload);
            }

            // validate that a content title was entered
            if (String.IsNullOrWhiteSpace(this._PDFContentTitle.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "PDFContentTitle", _GlobalResources.ContentTitle + " " + _GlobalResources.IsRequired);
            }

            // validate the completion criteria if it's a percentage
            if (this._CompletePDFOnPercentage.Checked)
            {
                int percentage;

                if (
                    String.IsNullOrWhiteSpace(this._PDFProgressForCompletionPercentage.Text)
                    || !int.TryParse(this._PDFProgressForCompletionPercentage.Text, out percentage)
                    || (int.TryParse(this._PDFProgressForCompletionPercentage.Text, out percentage) && (percentage < 0 || percentage > 100))
                   )
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "PDFCompletionCriteria", _GlobalResources.PercentageMustBeANumberBetween0And100);
                }
                else
                {

                }
            }

            return isValid;
        }
        #endregion

        #region _SaveContentPackage_PDF_Command
        /// <summary>
        /// Handles the "Submit" button click from the pdf uploader modal.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveContentPackage_PDF_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!_ValidatePDFImportForm())
                {
                    // fire a startup script so that the hidden/shown states are retained
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Default), "RetainPDFStateOnPostbackWithErrors", "RetainPDFStateOnPostbackWithErrors();", true);

                    // display feedback and return
                    this._UploadPDFModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
                    return;
                }

                // get some values so that we can help ensure a unique warehouse folder
                string currentDateTimeFormatted = AsentiaSessionState.UtcNow.ToString("yyyy-MM-dd-hh-mm-ss");
                string dateTimeDownToMilliseconds = AsentiaSessionState.UtcNow.ToString("o").Replace(":", "-");

                // begin to set-up the package by creating a warehouse folder
                string newWarehouseFolderName = AsentiaSessionState.IdAccount
                                                + "-"
                                                + AsentiaSessionState.IdSite
                                                + "-"
                                                + Cryptography.GetHash(this._PDFContentTitle.Text.Trim() + dateTimeDownToMilliseconds, Cryptography.HashType.MD5)
                                                + "-"
                                                + currentDateTimeFormatted;

                // create the new folder and copy the pdf scorm wrapper shell into it - CopyDirectory does both
                Utility.CopyDirectory(Server.MapPath(SitePathConstants._BIN + "pdfScormWrapperTemplate"), Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName));

                // copy the uploaded file to the package's root directory and delete the uploaded file
                string fullPDFFilePath = SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/" + this._PDFUploader.FileOriginalName;

                File.Copy(Server.MapPath(this._PDFUploader.SavedFilePath), Server.MapPath(fullPDFFilePath), true);
                File.Delete(Server.MapPath(this._PDFUploader.SavedFilePath));

                // get the extension of the uploaded file, id it's Word (.doc or .docx), we'll need to feed it to the processor
                string uploadedFileExtension = Path.GetExtension(fullPDFFilePath);

                // instansiate a package object
                this._PackageObject = new ContentPackage();
                this._PackageObject.IdSite = AsentiaSessionState.IdSite;
                this._PackageObject.IdContentPackageType = ContentPackage.ContentPackageType.SCORM;
                this._PackageObject.IdSCORMPackageType = ContentPackage.SCORMPackageType.Content;
                this._PackageObject.Name = this._PDFUploader.FileOriginalName;
                this._PackageObject.Path = SitePathConstants.WAREHOUSE + newWarehouseFolderName;                
                this._PackageObject.IsMediaUpload = true;
                this._PackageObject.IdMediaType = ContentPackage.MediaType.PDF;
                this._PackageObject.ContentTitle = this._PDFContentTitle.Text;
                this._PackageObject.OriginalMediaFilename = this._PDFUploader.FileOriginalName;

                // package completion criteria
                if (this._CompletePDFOnStart.Checked)
                { this._PackageObject.MinProgressForCompletion = 0; }
                else if (this._CompletePDFOnPercentage.Checked)
                { this._PackageObject.MinProgressForCompletion = Convert.ToDouble(this._PDFProgressForCompletionPercentage.Text) / 100; }
                else if (this._CompletePDFOnEnd.Checked)
                { this._PackageObject.MinProgressForCompletion = 1; }
                else
                { this._PackageObject.MinProgressForCompletion = 0; }

                // if the uploaded file was a word file, fill out the remaining properties, and let the processor do the work on the rest of the package
                // otherwise, we will do all the package work right here
                if (uploadedFileExtension == ".doc" || uploadedFileExtension == ".docx")
                {
                    // placeholders for kb and manifest
                    this._PackageObject.Kb = Utility.CalculateDirectorySizeInKb(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName)); // this is a placeholder, it will change once the Word To PDF processor processes this package
                    this._PackageObject.Manifest = "<manifest></manifest>"; // this is a placeholder, it will change once the Word To PDF processor processes this package

                    // processing information
                    this._PackageObject.IsProcessing = false;
                    this._PackageObject.IsProcessed = false;
                }
                else
                {
                    // open the package's imsmanifest.xml, viewer.html, and /script/config.js files to replace placeholders with package specific information

                    // imsmanifest.xml
                    string imsManifestContent = String.Empty;

                    // get a new guid to serve as the manifest identifier
                    string manifestIdentifier = "_" + Guid.NewGuid().ToString();                    

                    using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/imsmanifest.xml")))
                    { imsManifestContent = reader.ReadToEnd(); }

                    imsManifestContent = Regex.Replace(imsManifestContent, "##manifestIdentifier##", manifestIdentifier);
                    imsManifestContent = Regex.Replace(imsManifestContent, "##contentTitle##", this._PackageObject.ContentTitle);

                    if (this._PackageObject.MinProgressForCompletion == 0)
                    { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>0</adlcp:completionThreshold>"); }
                    else if (this._PackageObject.MinProgressForCompletion == 1)
                    { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>1</adlcp:completionThreshold>"); }
                    else if (this._PackageObject.MinProgressForCompletion > 0 && this._PackageObject.MinProgressForCompletion < 1)
                    { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", "<adlcp:completionThreshold>" + String.Format("{0:0.0}", this._PackageObject.MinProgressForCompletion) + "</adlcp:completionThreshold>"); }
                    else
                    { imsManifestContent = Regex.Replace(imsManifestContent, "##completionThreshold##", ""); }

                    File.Delete(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/imsmanifest.xml"));

                    using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/imsmanifest.xml")))
                    { writer.Write(imsManifestContent); }

                    // viewer.html
                    string viewerContent = String.Empty;

                    using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/viewer.html")))
                    { viewerContent = reader.ReadToEnd(); }

                    viewerContent = Regex.Replace(viewerContent, "##contentTitle##", this._PackageObject.ContentTitle);

                    File.Delete(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/viewer.html"));

                    using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/viewer.html")))
                    { writer.Write(viewerContent); }

                    // /script/config.js
                    string configContent = String.Empty;

                    using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/script/config.js")))
                    { configContent = reader.ReadToEnd(); }

                    configContent = Regex.Replace(configContent, "##pdfFilename##", this._PackageObject.OriginalMediaFilename);
                    configContent = Regex.Replace(configContent, "##minProgressForCompletion##", this._PackageObject.MinProgressForCompletion.ToString());

                    File.Delete(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/script/config.js"));

                    using (StreamWriter writer = new StreamWriter(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName + "/script/config.js")))
                    { writer.Write(configContent); }

                    // write the package size and manifext text into the package object
                    this._PackageObject.Kb = Utility.CalculateDirectorySizeInKb(Server.MapPath(SitePathConstants.WAREHOUSE + newWarehouseFolderName));
                    this._PackageObject.Manifest = imsManifestContent;

                    // processing information, this is so the processor can never try to pick this up for processing
                    this._PackageObject.IsProcessing = false;
                    this._PackageObject.IsProcessed = true;
                    this._PackageObject.DtProcessed = AsentiaSessionState.UtcNow;
                }

                // save the package to database
                int idPackage = this._PackageObject.Save();

                // display feedback
                if (uploadedFileExtension == ".doc" || uploadedFileExtension == ".docx")
                { this._UploadPDFModal.DisplayFeedback(String.Format(_GlobalResources.WordFileHasBeenSubmittedForProcessingToPDFOnceProcessedYouWillBeAbleToAssignThePackageToModule_s, this._PackageObject.Name), false); }
                else
                { this._UploadPDFModal.DisplayFeedback(String.Format(_GlobalResources.PDFFileHasBeenImportedSuccessfully, this._PackageObject.Name), false); }                

                // fire a startup script to re-initialize the form, so more content can be imported
                ScriptManager.RegisterStartupScript(this.Page, typeof(Default), "InitializeUploadPDFModalOnPostbackSuccess", "InitializeUploadPDFModal(false);", true);

                // rebind the grid
                this.PackageGrid.BindData();
                this.PackageGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._UploadPDFModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._UploadPDFModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._UploadPDFModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._UploadPDFModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._UploadPDFModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedPackage_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this.DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);

            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedPackage_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteThesePackage_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.PackageGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.PackageGrid.Rows[i].FindControl(this.PackageGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }
               
                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    // get the paths of the packages to be deleted
                    DataTable packagePaths = Library.ContentPackage.GetPaths(recordsToDelete);

                    Library.ContentPackage.Delete(recordsToDelete);

                    // delete packages from the disk if they exist
                    foreach (DataRow row in packagePaths.Rows)
                    {
                        string dirPath = Server.MapPath("~/" + row["path"].ToString());
                        if (Directory.Exists(dirPath))
                        { 
                            Directory.Delete(dirPath, true);
                        }
                    }

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedPackage_sHaveBeenDeletedSuccessfully, false);

                    // rebind the grid
                    this.PackageGrid.BindData();
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoContentPackage_sSelectedForDeletion, true);
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);

                // rebind the grid
                this.PackageGrid.BindData();
            }
        }
        #endregion
    }
}
