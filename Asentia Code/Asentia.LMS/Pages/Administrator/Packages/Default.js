﻿function InitializeUploadPackageModal(hideFeedbackPanel) {
    // reset the feedback panel
    if (hideFeedbackPanel) {
        $("#UploadPackageModalModalPopupFeedbackContainer").hide();
    }

    // do not reset the uploader, for now
}

function InitializeUploadVideoModal(hideFeedbackPanel) {
    // reset the feedback panel
    if (hideFeedbackPanel) {
        $("#UploadVideoModalModalPopupFeedbackContainer").hide();
    }

    // enable all video types
    $("#VideoType_Field_0").prop("disabled", false);
    $("#VideoType_Field_1").prop("disabled", false);
    $("#VideoType_Field_2").prop("disabled", false);

    // reset the uploader
    $("#VideoUploader_CompletedPanel").html("");
    $("#VideoFile_ErrorContainer").html("");
    $("#VideoUploader_UploadControl input").attr("style", "");
    $("#VideoUploader_UploadHiddenFieldOriginalFileName").val("");
    $("#VideoUploader_UploadHiddenField").val("");
    $("#VideoUploader_UploadHiddenFieldFileSize").val("");

    // reset the embed code fields
    $("#VideoFile_YouTubeEmbedCode_Field").val("");
    $("#VideoFile_VimeoEmbedCode_Field").val("");

    // reset the content title
    $("#VideoContentTitle_Field").val("");

    // reset playback settings
    $("#VideoPlaybackSettings_EnableVideoAutoplay_Field").prop("checked", false);
    $("#VideoPlaybackSettings_EnableVideoRewind_Field").prop("checked", false);
    $("#VideoPlaybackSettings_EnableVideoFastForward_Field").prop("checked", false);
    $("#VideoPlaybackSettings_EnableVideoResume_Field").prop("checked", false);

    // make the file upload type checked
    $("#VideoType_Field_0").prop("checked", true);
    VideoTypeSelected("default");

    // reset all other error panels
    $("#VideoContentTitle_ErrorContainer").html("");
    $("#VideoPlaybackSettings_ErrorContainer").html("");
    $("#VideoCompletionCriteria_ErrorContainer").html("");
}

function RetainVideoTypeStateOnPostbackWithErrors() {

    // file upload
    if ($("#VideoType_Field_0").prop("checked") == true) {

        // disable other video types
        $("#VideoType_Field_1").prop("disabled", true);
        $("#VideoType_Field_2").prop("disabled", true);

        // hide the fields for the other video types
        $("#YouTubeEmbedCodeInstructions_Container").hide();
        $("#VideoFile_YouTubeEmbedCode_Field_Container").hide();
        $("#VimeoEmbedCodeInstructions_Container").hide();
        $("#VideoFile_VimeoEmbedCode_Field_Container").hide();

        // if competion on percentage not checked, disable the percentage field
        if (!$("#VideoCompletionCriteria_CompleteVideoOnPercentage_Field").prop("checked")) {
            $("#VideoCompletionCriteria_VideoProgressForCompletionPercentage_Field").prop("disabled", true);
            $("#VideoCompletionCriteria_VideoProgressForCompletionPercentage_Field").val("");
        }

    }
    // youtube
    else if ($("#VideoType_Field_1").prop("checked") == true) {

        // disable other video types
        $("#VideoType_Field_0").prop("disabled", true);
        $("#VideoType_Field_2").prop("disabled", true);

        // hide the fields for the other video types
        $("#VimeoEmbedCodeInstructions_Container").hide();
        $("#VideoFile_VimeoEmbedCode_Field_Container").hide();
        $("#VideoUploader_Container").hide();

        // disable all playback options
        $("#VideoPlaybackSettings_EnableVideoAutoplay_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoAutoplay_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoRewind_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoRewind_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoFastForward_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoFastForward_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoResume_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoResume_Field").prop("checked", false);

        // set completion criteria to "on start" and disable other options
        $("#VideoCompletionCriteria_CompleteVideoOnStart_Field").prop("checked", true);
        CompletionCriteriaSelected("onstart", "video");

        $("#VideoCompletionCriteria_CompleteVideoOnPercentage_Field").prop("disabled", true);
        $("#VideoCompletionCriteria_CompleteVideoOnPercentage_Field").prop("checked", false);
        $("#VideoCompletionCriteria_CompleteVideoOnEnd_Field").prop("disabled", true);
        $("#VideoCompletionCriteria_CompleteVideoOnEnd_Field").prop("checked", false);

    }
    // vimeo
    else if ($("#VideoType_Field_2").prop("checked") == true) {

        // disable other video types
        $("#VideoType_Field_0").prop("disabled", true);
        $("#VideoType_Field_1").prop("disabled", true);

        // hide the fields for the other video types
        $("#YouTubeEmbedCodeInstructions_Container").hide();
        $("#VideoFile_YouTubeEmbedCode_Field_Container").hide();
        $("#VideoUploader_Container").hide();

        // disable all playback options but resume
        $("#VideoPlaybackSettings_EnableVideoAutoplay_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoAutoplay_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoRewind_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoRewind_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoFastForward_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoFastForward_Field").prop("checked", false);

        // if competion on percentage not checked, disable the percentage field
        if (!$("#VideoCompletionCriteria_CompleteVideoOnPercentage_Field").prop("checked")) {
            $("#VideoCompletionCriteria_VideoProgressForCompletionPercentage_Field").prop("disabled", true);
            $("#VideoCompletionCriteria_VideoProgressForCompletionPercentage_Field").val("");
        }

    }
    else
    { }

}

function InitializeUploadPowerPointModal(hideFeedbackPanel) {
    // reset the feedback panel
    if (hideFeedbackPanel) {
        $("#UploadPowerPointModalModalPopupFeedbackContainer").hide();
    }

    // reset the uploader
    $("#PowerPointUploader_CompletedPanel").html("");
    $("#PowerPointUploader_ErrorContainer").html("");
    $("#PowerPointUploader_UploadControl input").attr("style", "");
    $("#PowerPointUploader_UploadHiddenFieldOriginalFileName").val("");
    $("#PowerPointUploader_UploadHiddenField").val("");
    $("#PowerPointUploader_UploadHiddenFieldFileSize").val("");

    // reset the content title
    $("#PowerPointContentTitle_Field").val("");

    // reset playback settings
    $("#PowerPointPlaybackSettings_EnablePowerPointAutoplay_Field").prop("checked", false);
    $("#PowerPointPlaybackSettings_EnablePowerPointLinearNavigation_Field").prop("disabled", false);
    $("#PowerPointPlaybackSettings_EnablePowerPointLinearNavigation_Field").prop("checked", false);
    $("#PowerPointPlaybackSettings_EnablePowerPointJumpNavigation_Field").prop("checked", false);
    $("#PowerPointPlaybackSettings_EnablePowerPointBookmarking_Field").prop("checked", false);

    // set the completion criteria to "on start"
    $("#PowerPointCompletionCriteria_CompletePowerPointOnStart_Field").prop("checked", true);
    CompletionCriteriaSelected("onstart", "powerpoint");

    // reset all other error panels
    $("#PowerPointContentTitle_ErrorContainer").html("");
    $("#PowerPointPlaybackSettings_ErrorContainer").html("");
    $("#PowerPointCompletionCriteria_ErrorContainer").html("");
}

function RetainPowerPointStateOnPostbackWithErrors() {
    // retain state for navigation settings
    TogglePowerPointJumpNavigation();

    // if competion on percentage not checked, disable the percentage field
    if (!$("#PowerPointCompletionCriteria_CompletePowerPointOnPercentage_Field").prop("checked")) {
        $("#PowerPointCompletionCriteria_PowerPointProgressForCompletionPercentage_Field").prop("disabled", true);
        $("#PowerPointCompletionCriteria_PowerPointProgressForCompletionPercentage_Field").val("");
    }
}

function InitializeUploadPDFModal(hideFeedbackPanel) {
    // reset the feedback panel
    if (hideFeedbackPanel) {
        $("#UploadPDFModalModalPopupFeedbackContainer").hide();
    }

    // reset the uploader
    $("#PDFUploader_CompletedPanel").html("");
    $("#PDFUploader_ErrorContainer").html("");
    $("#PDFUploader_UploadControl input").attr("style", "");
    $("#PDFUploader_UploadHiddenFieldOriginalFileName").val("");
    $("#PDFUploader_UploadHiddenField").val("");
    $("#PDFUploader_UploadHiddenFieldFileSize").val("");

    // reset the content title
    $("#PDFContentTitle_Field").val("");    

    // set the completion criteria to "on start"
    $("#PDFCompletionCriteria_CompletePDFOnStart_Field").prop("checked", true);
    CompletionCriteriaSelected("onstart", "pdf");

    // reset all other error panels
    $("#PDFContentTitle_ErrorContainer").html("");    
    $("#PDFCompletionCriteria_ErrorContainer").html("");
}

function RetainPDFStateOnPostbackWithErrors() {    
    // if competion on percentage not checked, disable the percentage field
    if (!$("#PDFCompletionCriteria_CompletePDFOnPercentage_Field").prop("checked")) {
        $("#PDFCompletionCriteria_PDFProgressForCompletionPercentage_Field").prop("disabled", true);
        $("#PDFCompletionCriteria_PDFProgressForCompletionPercentage_Field").val("");
    }
}

function VideoTypeSelected(videoType) {

    if (videoType == "default") {

        // hide the fields for the other video types and show the file uploader
        $("#YouTubeEmbedCodeInstructions_Container").hide();
        $("#VideoFile_YouTubeEmbedCode_Field_Container").hide();
        $("#VimeoEmbedCodeInstructions_Container").hide();
        $("#VideoFile_VimeoEmbedCode_Field_Container").hide();
        $("#VideoUploader_Container").show();

        // enable/disable playback options
        $("#VideoPlaybackSettings_EnableVideoAutoplay_Field").prop("disabled", false);
        $("#VideoPlaybackSettings_EnableVideoRewind_Field").prop("disabled", false);
        $("#VideoPlaybackSettings_EnableVideoFastForward_Field").prop("disabled", false);
        $("#VideoPlaybackSettings_EnableVideoResume_Field").prop("disabled", false);

        // set completion criteria to "on start" and ensure other options are enabled
        $("#VideoCompletionCriteria_CompleteVideoOnStart_Field").prop("checked", true);
        CompletionCriteriaSelected("onstart", "video");

        $("#VideoCompletionCriteria_CompleteVideoOnPercentage_Field").prop("disabled", false);
        $("#VideoCompletionCriteria_CompleteVideoOnPercentage_Field").prop("checked", false);
        $("#VideoCompletionCriteria_CompleteVideoOnEnd_Field").prop("disabled", false);
        $("#VideoCompletionCriteria_CompleteVideoOnEnd_Field").prop("checked", false);
        
    }
    else if (videoType == "youtube") {

        // hide the fields for the other video types and show the embed code field for youtube
        $("#VimeoEmbedCodeInstructions_Container").hide();
        $("#VideoFile_VimeoEmbedCode_Field_Container").hide();
        $("#VideoUploader_Container").hide();
        $("#YouTubeEmbedCodeInstructions_Container").show();
        $("#VideoFile_YouTubeEmbedCode_Field_Container").show();

        // enable/disable playback options
        $("#VideoPlaybackSettings_EnableVideoAutoplay_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoAutoplay_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoRewind_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoRewind_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoFastForward_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoFastForward_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoResume_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoResume_Field").prop("checked", false);

        // set completion criteria to "on start" and disable other options
        $("#VideoCompletionCriteria_CompleteVideoOnStart_Field").prop("checked", true);
        CompletionCriteriaSelected("onstart", "video");

        $("#VideoCompletionCriteria_CompleteVideoOnPercentage_Field").prop("disabled", true);
        $("#VideoCompletionCriteria_CompleteVideoOnPercentage_Field").prop("checked", false);
        $("#VideoCompletionCriteria_CompleteVideoOnEnd_Field").prop("disabled", true);
        $("#VideoCompletionCriteria_CompleteVideoOnEnd_Field").prop("checked", false);

    }
    else if (videoType == "vimeo") {

        // hide the fields for the other video types and show the embed code field for vimeo
        $("#YouTubeEmbedCodeInstructions_Container").hide();
        $("#VideoFile_YouTubeEmbedCode_Field_Container").hide();
        $("#VideoUploader_Container").hide();
        $("#VimeoEmbedCodeInstructions_Container").show();
        $("#VideoFile_VimeoEmbedCode_Field_Container").show();

        // enable/disable playback options
        $("#VideoPlaybackSettings_EnableVideoAutoplay_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoAutoplay_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoRewind_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoRewind_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoFastForward_Field").prop("disabled", true);
        $("#VideoPlaybackSettings_EnableVideoFastForward_Field").prop("checked", false);
        $("#VideoPlaybackSettings_EnableVideoResume_Field").prop("disabled", false);

        // set completion criteria to "on start" and ensure other options are enabled
        $("#VideoCompletionCriteria_CompleteVideoOnStart_Field").prop("checked", true);
        CompletionCriteriaSelected("onstart", "video");

        $("#VideoCompletionCriteria_CompleteVideoOnPercentage_Field").prop("disabled", false);
        $("#VideoCompletionCriteria_CompleteVideoOnPercentage_Field").prop("checked", false);
        $("#VideoCompletionCriteria_CompleteVideoOnEnd_Field").prop("disabled", false);
        $("#VideoCompletionCriteria_CompleteVideoOnEnd_Field").prop("checked", false);

    }
    else {}
}

function PackageFileUploadBegin() {
    // reset the feedback panel
    $("#UploadPackageModalModalPopupFeedbackContainer").hide();

    $("#UploadPackageModalModalPopupSubmitButton").prop("disabled", true);
    $("#UploadPackageModalModalPopupSubmitButton").addClass("DisabledButton");
    $("#UploadPackageModalModalPopupSubmitButton").addClass("aspNetDisabled");

    $("#UploadPackageModalModalPopupCloseButton").prop("disabled", true);
    $("#UploadPackageModalModalPopupCloseButton").addClass("aspNetDisabled");
}

function PackageFileUploadError() {
    $("#UploadPackageModalModalPopupSubmitButton").prop("disabled", false);
    $("#UploadPackageModalModalPopupSubmitButton").removeClass("DisabledButton");
    $("#UploadPackageModalModalPopupSubmitButton").removeClass("aspNetDisabled");

    $("#UploadPackageModalModalPopupCloseButton").prop("disabled", false);
    $("#UploadPackageModalModalPopupCloseButton").removeClass("aspNetDisabled");
}

function PackageFileUploadComplete() {
    $("#UploadPackageModalModalPopupSubmitButton").prop("disabled", false);
    $("#UploadPackageModalModalPopupSubmitButton").removeClass("DisabledButton");
    $("#UploadPackageModalModalPopupSubmitButton").removeClass("aspNetDisabled");

    $("#UploadPackageModalModalPopupCloseButton").prop("disabled", false);
    $("#UploadPackageModalModalPopupCloseButton").removeClass("aspNetDisabled");
}

function VideoFileUploadBegin() {
    // reset the feedback panel
    $("#UploadVideoModalModalPopupFeedbackContainer").hide();

    $("#UploadVideoModalModalPopupSubmitButton").prop("disabled", true);
    $("#UploadVideoModalModalPopupSubmitButton").addClass("DisabledButton");
    $("#UploadVideoModalModalPopupSubmitButton").addClass("aspNetDisabled");

    $("#UploadVideoModalModalPopupCloseButton").prop("disabled", true);
    $("#UploadVideoModalModalPopupCloseButton").addClass("aspNetDisabled");
}

function VideoFileUploadError() {
    $("#UploadVideoModalModalPopupSubmitButton").prop("disabled", false);
    $("#UploadVideoModalModalPopupSubmitButton").removeClass("DisabledButton");
    $("#UploadVideoModalModalPopupSubmitButton").removeClass("aspNetDisabled");

    $("#UploadVideoModalModalPopupCloseButton").prop("disabled", false);
    $("#UploadVideoModalModalPopupCloseButton").removeClass("aspNetDisabled");
}

function VideoFileUploadComplete() {
    $("#UploadVideoModalModalPopupSubmitButton").prop("disabled", false);
    $("#UploadVideoModalModalPopupSubmitButton").removeClass("DisabledButton");
    $("#UploadVideoModalModalPopupSubmitButton").removeClass("aspNetDisabled");

    $("#UploadVideoModalModalPopupCloseButton").prop("disabled", false);
    $("#UploadVideoModalModalPopupCloseButton").removeClass("aspNetDisabled");
}

function PowerPointFileUploadBegin() {
    // reset the feedback panel
    $("#UploadPowerPointModalModalPopupFeedbackContainer").hide();

    $("#UploadPowerPointModalModalPopupSubmitButton").prop("disabled", true);
    $("#UploadPowerPointModalModalPopupSubmitButton").addClass("DisabledButton");
    $("#UploadPowerPointModalModalPopupSubmitButton").addClass("aspNetDisabled");

    $("#UploadPowerPointModalModalPopupCloseButton").prop("disabled", true);
    $("#UploadPowerPointModalModalPopupCloseButton").addClass("aspNetDisabled");
}

function PowerPointFileUploadError() {
    $("#UploadPowerPointModalModalPopupSubmitButton").prop("disabled", false);
    $("#UploadPowerPointModalModalPopupSubmitButton").removeClass("DisabledButton");
    $("#UploadPowerPointModalModalPopupSubmitButton").removeClass("aspNetDisabled");

    $("#UploadPowerPointModalModalPopupCloseButton").prop("disabled", false);
    $("#UploadPowerPointModalModalPopupCloseButton").removeClass("aspNetDisabled");
}

function PowerPointFileUploadComplete() {
    $("#UploadPowerPointModalModalPopupSubmitButton").prop("disabled", false);
    $("#UploadPowerPointModalModalPopupSubmitButton").removeClass("DisabledButton");
    $("#UploadPowerPointModalModalPopupSubmitButton").removeClass("aspNetDisabled");

    $("#UploadPowerPointModalModalPopupCloseButton").prop("disabled", false);
    $("#UploadPowerPointModalModalPopupCloseButton").removeClass("aspNetDisabled");
}

function PDFFileUploadBegin() {
    // reset the feedback panel
    $("#UploadPDFModalModalPopupFeedbackContainer").hide();

    $("#UploadPDFModalModalPopupSubmitButton").prop("disabled", true);
    $("#UploadPDFModalModalPopupSubmitButton").addClass("DisabledButton");
    $("#UploadPDFModalModalPopupSubmitButton").addClass("aspNetDisabled");

    $("#UploadPDFModalModalPopupCloseButton").prop("disabled", true);
    $("#UploadPDFModalModalPopupCloseButton").addClass("aspNetDisabled");
}

function PDFFileUploadError() {
    $("#UploadPDFModalModalPopupSubmitButton").prop("disabled", false);
    $("#UploadPDFModalModalPopupSubmitButton").removeClass("DisabledButton");
    $("#UploadPDFModalModalPopupSubmitButton").removeClass("aspNetDisabled");

    $("#UploadPDFModalModalPopupCloseButton").prop("disabled", false);
    $("#UploadPDFModalModalPopupCloseButton").removeClass("aspNetDisabled");
}

function PDFFileUploadComplete() {
    $("#UploadPDFModalModalPopupSubmitButton").prop("disabled", false);
    $("#UploadPDFModalModalPopupSubmitButton").removeClass("DisabledButton");
    $("#UploadPDFModalModalPopupSubmitButton").removeClass("aspNetDisabled");

    $("#UploadPDFModalModalPopupCloseButton").prop("disabled", false);
    $("#UploadPDFModalModalPopupCloseButton").removeClass("aspNetDisabled");
}

function CompletionCriteriaSelected(criteriaOption, contentType) {
    // set the identifier prefix based on contentType
    var identifierPrefix;

    if (contentType == "video") {
        identifierPrefix = "Video";
    }
    else if (contentType == "pdf") {
        identifierPrefix = "PDF";
    }
    else {
        identifierPrefix = "PowerPoint";
    }

    // enable/disable options based on criteriaOption
    if (criteriaOption == "onstart") {
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnPercentage_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnEnd_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_" + identifierPrefix + "ProgressForCompletionPercentage_Field").prop("disabled", true);
        $("#" + identifierPrefix + "CompletionCriteria_" + identifierPrefix + "ProgressForCompletionPercentage_Field").val("");
    }
    else if (criteriaOption == "onpercentage") {
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnStart_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnEnd_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_" + identifierPrefix + "ProgressForCompletionPercentage_Field").prop("disabled", false);
    }
    else if (criteriaOption == "onend") {
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnStart_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnPercentage_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_" + identifierPrefix + "ProgressForCompletionPercentage_Field").prop("disabled", true);
        $("#" + identifierPrefix + "CompletionCriteria_" + identifierPrefix + "ProgressForCompletionPercentage_Field").val("");
    }
    else {}
}

function TogglePowerPointJumpNavigation() {
    if ($("#PowerPointPlaybackSettings_EnablePowerPointJumpNavigation_Field").prop("checked")) {
        $("#PowerPointPlaybackSettings_EnablePowerPointLinearNavigation_Field").prop("checked", true);
        $("#PowerPointPlaybackSettings_EnablePowerPointLinearNavigation_Field").prop("disabled", true);
    }
    else {
        $("#PowerPointPlaybackSettings_EnablePowerPointLinearNavigation_Field").prop("disabled", false);
    }
}

function LoadViewContentPackageUtilizationModalContent(idContentPackage) {
    // empty the modal popup body
    $("#ViewContentPackageUtilizationModalModalPopupBody").empty();

    // show the modal
    $("#" + ViewContentPackageUtilizationModalHiddenLaunchButtonId).click();

    // show the loading placeholder
    $("#ViewContentPackageUtilizationModalModalPopupPostbackLoadingPlaceholder").show();

    // make the ajax call
    $.ajax({
        type: "POST",
        url: "Default.aspx/PopulateViewContentPackageUtilizationModal",
        data: "{idContentPackage: " + idContentPackage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            // get the response object
            var responseObject = response.d;

            // hide the loading placeholder
            $("#ViewContentPackageUtilizationModalModalPopupPostbackLoadingPlaceholder").hide();

            // put the html returned from ajax into the container
            $(responseObject.html).appendTo("#ViewContentPackageUtilizationModalModalPopupBody");

            // do the show modal click again so that it auto-centers the modal after content has been loaded
            $("#" + ViewContentPackageUtilizationModalHiddenLaunchButtonId).click();
        },
        failure: function (response) {
            //alert(response.d);
        },
        error: function (response) {
            //alert(response.d);
        }
    });
}

function LoadDownloadContentPackageModalContent(idContentPackage) {
    // empty the modal popup body
    $("#DownloadContentPackageModalModalPopupBody").empty();

    // show the modal
    $("#" + DownloadContentPackageModalHiddenLaunchButtonId).click();

    // show the loading placeholder
    $("#DownloadContentPackageModalModalPopupPostbackLoadingPlaceholder").show();

    // make the ajax call
    $.ajax({
        type: "POST",
        url: "Default.aspx/PopulateDownloadContentPackageModal",
        data: "{idContentPackage: " + idContentPackage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            // get the response object
            var responseObject = response.d;

            // hide the loading placeholder
            $("#DownloadContentPackageModalModalPopupPostbackLoadingPlaceholder").hide();

            // put the html returned from ajax into the container
            $(responseObject.html).appendTo("#DownloadContentPackageModalModalPopupBody");

            // do the show modal click again so that it auto-centers the modal after content has been loaded
            $("#" + DownloadContentPackageModalHiddenLaunchButtonId).click();
        },
        failure: function (response) {
            //alert(response.d);
        },
        error: function (response) {
            //alert(response.d);
        }
    });
}