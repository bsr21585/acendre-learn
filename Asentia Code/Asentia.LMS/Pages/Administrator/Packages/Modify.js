﻿function CompletionCriteriaSelected(criteriaOption, contentType) {
    // set the identifier prefix based on contentType
    var identifierPrefix;

    if (contentType == "video") {
        identifierPrefix = "Video";
    }
    else if (contentType == "pdf") {
        identifierPrefix = "PDF";
    }
    else {
        identifierPrefix = "PowerPoint";
    }

    // enable/disable options based on criteriaOption
    if (criteriaOption == "onstart") {
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnPercentage_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnEnd_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_" + identifierPrefix + "ProgressForCompletionPercentage_Field").prop("disabled", true);
        $("#" + identifierPrefix + "CompletionCriteria_" + identifierPrefix + "ProgressForCompletionPercentage_Field").val("");
    }
    else if (criteriaOption == "onpercentage") {
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnStart_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnEnd_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_" + identifierPrefix + "ProgressForCompletionPercentage_Field").prop("disabled", false);
    }
    else if (criteriaOption == "onend") {
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnStart_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_Complete" + identifierPrefix + "OnPercentage_Field").prop("checked", false);
        $("#" + identifierPrefix + "CompletionCriteria_" + identifierPrefix + "ProgressForCompletionPercentage_Field").prop("disabled", true);
        $("#" + identifierPrefix + "CompletionCriteria_" + identifierPrefix + "ProgressForCompletionPercentage_Field").val("");
    }
    else { }
}

function TogglePowerPointJumpNavigation() {
    if ($("#PowerPointPlaybackSettings_EnablePowerPointJumpNavigation_Field").prop("checked")) {
        $("#PowerPointPlaybackSettings_EnablePowerPointLinearNavigation_Field").prop("checked", true);
        $("#PowerPointPlaybackSettings_EnablePowerPointLinearNavigation_Field").prop("disabled", true);
    }
    else {
        $("#PowerPointPlaybackSettings_EnablePowerPointLinearNavigation_Field").prop("disabled", false);
    }
}

function EnableDisableManifestUpdate() {
    if ($('#PackageModify_Manifest_RevertCheckBox').is(':checked')) {
        $("#PackageModify_Manifest_Field").prop('disabled', true);
    }
    else {
        $("#PackageModify_Manifest_Field").prop('disabled', false);
    }
}