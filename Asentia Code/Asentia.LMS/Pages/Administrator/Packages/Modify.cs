﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Web;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.Packages
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Public Properties
        public Panel PageInstructionsPanel;
        public Panel PackageFormContainer;
        public Panel ActionsPanel;
        public Panel PackageFormTabPanelsContainer;
        public Panel ObjectOptionsPanel;
        public Panel PackagePropertiesFormWrapperContainer;
        public Panel PackagePropertiesWrapperContainer;
        #endregion

        #region Private Properties
        private ContentPackage _PackageObject;

        private LinkButton _ImportPackageLinkButton;
        private ModalPopup _ContentPackageImportConfirmationModal;

        private TextBox _PackageFileName;

        private CheckBox _EnableVideoAutoPlay;
        private CheckBox _EnableVideoRewind;
        private CheckBox _EnableVideoFastForward;
        private CheckBox _EnableVideoResume;
        private RadioButton _CompleteVideoOnStart;
        private RadioButton _CompleteVideoOnPercentage;
        private RadioButton _CompleteVideoOnEnd;
        private TextBox _VideoProgressForCompletionPercentage;

        private CheckBox _EnablePowerPointAutoPlay;
        private CheckBox _EnablePowerPointLinearNavigation;
        private CheckBox _EnablePowerPointJumpNavigation;
        private CheckBox _EnablePowerPointBookmarking;
        private RadioButton _CompletePowerPointOnStart;
        private RadioButton _CompletePowerPointOnPercentage;
        private RadioButton _CompletePowerPointOnEnd;
        private TextBox _PowerPointProgressForCompletionPercentage;

        private RadioButton _CompletePDFOnStart;
        private RadioButton _CompletePDFOnPercentage;
        private RadioButton _CompletePDFOnEnd;
        private TextBox _PDFProgressForCompletionPercentage;

        private CheckBox _RevertManifestToOriginal;
        private TextBox _ManifestText;

        private Button _SavePackage;
        private Button _CancelPackage;
        #endregion

        #region Page Load
        /// <summary>
        /// Handles the Page Load Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_ContentPackageManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/packages/Modify.css");

            // get the package object
            this._GetPackageObject();

            // build the breadcrumb
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // builds object options panel
            this._BuildObjectOptionsPanel();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.ModifyThePropertiesOfTheContentPackageUsingTheFormBelow, true);

            // builds tabs list
            this._BuildTabsList();

            this.PackagePropertiesFormWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.PackagePropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            this.PackageFormTabPanelsContainer = new Panel();
            this.PackageFormTabPanelsContainer.ID = "PackageForm_TabPanelsContainer";
            this.PackageFormTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.PackageFormContainer.Controls.Add(this.PackageFormTabPanelsContainer);

            // build the controls for the form
            this._BuildFormControls();

            // build the content package import confirmation modal
            this._BuildContentPackageImportConfirmationModal();

            // builds page action panel
            this._BuildActionsPanel();            
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Packages.Modify.js");
        }
        #endregion

        #region _GetPackageObject
        /// <summary>
        /// Gets a Pakcage object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetPackageObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._PackageObject = new ContentPackage(id); }

                    // if the package has not been processed, redirect
                    if (this._PackageObject.IsProcessed == false)
                    { Response.Redirect("~/administrator/packages"); }
                }
                catch
                { Response.Redirect("~/administrator/packages"); }
            }
            else
            { Response.Redirect("~/administrator/packages"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            //Build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Packages, "/administrator/packages"));
            breadCrumbLinks.Add(new BreadcrumbLink(this._PackageObject != null ? (this._PackageObject.Name) : (_GlobalResources.NewPackage)));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, this._PackageObject != null ? (this._PackageObject.Name) : (_GlobalResources.NewPackage), ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE,
                                                                                                                                                                                      ImageFiles.EXT_PNG));
        }
        #endregion

        #region _BuildFormControls
        /// <summary>
        /// Builds the form controls.
        /// </summary>
        private void _BuildFormControls()
        {
            this.PackageFormTabPanelsContainer.Controls.Clear();

            // builds properties tab panel
            this._BuildPropertiesTabPanel();

            // builds manifest tab panel
            this._BuildManifestTabPanel();
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds Action panel
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            this._SavePackage = new Button();
            this._SavePackage.Text = _GlobalResources.SaveChanges;
            this._SavePackage.Command += new CommandEventHandler(this._SavePackage_Command);
            this._SavePackage.CssClass = "Button ActionButton";
            ActionsPanel.Controls.Add(this._SavePackage);

            this._CancelPackage = new Button();
            this._CancelPackage.Text = _GlobalResources.Cancel;
            this._CancelPackage.Command += new CommandEventHandler(this._CancelPackage_Command);
            this._CancelPackage.CssClass = "Button NonActionButton";
            ActionsPanel.Controls.Add(this._CancelPackage);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // IMPORT PACKAGE
            this._ImportPackageLinkButton = new LinkButton();
            this._ImportPackageLinkButton.ID = "ImportPackageLinkButton";
            this._ImportPackageLinkButton.Command += new CommandEventHandler(this._ImportPackage_Command);

            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("ImportPackageLink",
                                                this._ImportPackageLinkButton,
                                                null,
                                                null,
                                                _GlobalResources.CreateCourseFromPackage,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildPropertiesTabPanel
        /// <summary>
        ///  Builds the Properties form.
        /// </summary>
        private void _BuildPropertiesTabPanel()
        {
            string contentType = "";

            if ((ContentPackage.ContentPackageType)this._PackageObject.IdContentPackageType == ContentPackage.ContentPackageType.SCORM)
            {
                if (this._PackageObject.IdQuizSurvey > 0)
                { contentType = String.Format("<a href=\"/administrator/quizzesandsurveys/Modify.aspx?id={0}\">{1}</a>", this._PackageObject.IdQuizSurvey.ToString(), _GlobalResources.QuizSurvey); }
                else
                { contentType = _GlobalResources.SCORM; }
            }
            else if ((ContentPackage.ContentPackageType)this._PackageObject.IdContentPackageType == ContentPackage.ContentPackageType.xAPI)
            { contentType = _GlobalResources.xAPI; }
            else
            { }

            string scormType = "";

            if (this._PackageObject.IdSCORMPackageType != null)
            {
                if ((ContentPackage.SCORMPackageType)this._PackageObject.IdSCORMPackageType == ContentPackage.SCORMPackageType.Content)
                { scormType = _GlobalResources.Content; }
                else if ((ContentPackage.SCORMPackageType)this._PackageObject.IdSCORMPackageType == ContentPackage.SCORMPackageType.Resource)
                { scormType = _GlobalResources.Resource; }
                else
                { }
            }

            // Package Modify Properties Tab            
            Panel tabContentPanel = new Panel();
            tabContentPanel.ID = "PackageModify_" + "Properties" + "_TabPanel";
            tabContentPanel.CssClass = "TabContentPanel";

            #region Name Field

            this._PackageFileName = new TextBox();
            this._PackageFileName.ID = "PackageModify_" + "Name" + "_Field";
            this._PackageFileName.CssClass = "InputLong";
            this._PackageFileName.Text = _PackageObject.Name;

            //Add to Main Field Panel
            tabContentPanel.Controls.Add(AsentiaPage.BuildFormField("PackageModify_Name",
                                                             _GlobalResources.Name,
                                                             this._PackageFileName.ID,
                                                             this._PackageFileName,
                                                             true,
                                                             true,
                                                             false));

            #endregion

            #region Package Type Field

            Label packageType = new Label();
            packageType.ID = "PackageModify_" + "PackageType" + "_Field";
            packageType.Text = Convert.ToString(contentType);

            //Add to Main Field Panel
            tabContentPanel.Controls.Add(AsentiaPage.BuildFormField("PackageModify_PackageType",
                                                           _GlobalResources.ContentPackageType,
                                                           packageType.ID,
                                                           packageType,
                                                           false,
                                                           false,
                                                           false));

            #endregion

            #region SCORMPType Field
            //Label scormpType = new Label();
            //scormpType.ID = identifierID + "_" + "SCORMPType" + "_Field";
            //scormpType.Text = Convert.ToString(scormType);

            //Add to Main Field Panel
            //tabContentPanel.Controls.Add(AsentiaPage.BuildFormField(identifierID + "_SCORMPType",
                                                    //_GlobalResources.SCORMPackageType,
                                                    //scormpType.ID,
                                                    //scormpType,
                                                    //false,
                                                    //false,
                                                    //false));
            #endregion

            if (this._PackageObject.IsMediaUpload == true)
            {
                #region MediaType Field
                Label mediaTypeLabel = new Label();
                mediaTypeLabel.ID = "ImportedMediaType_Field";

                switch (this._PackageObject.IdMediaType)
                {
                    case ContentPackage.MediaType.PowerPoint:
                        mediaTypeLabel.Text = _GlobalResources.PowerPoint;
                        break;
                    case ContentPackage.MediaType.Video:
                        mediaTypeLabel.Text = _GlobalResources.Video;
                        break;
                    case ContentPackage.MediaType.PDF:
                        mediaTypeLabel.Text = _GlobalResources.PDF;
                        break;
                    default:
                        break;
                }

                tabContentPanel.Controls.Add(AsentiaPage.BuildFormField("ImportedMediaType",
                                                    _GlobalResources.ImportedMediaType,
                                                    mediaTypeLabel.ID,
                                                    mediaTypeLabel,
                                                    false,
                                                    false,
                                                    false));
                #endregion

                #region Original Media Field
                if (this._PackageObject.IdMediaType == ContentPackage.MediaType.PowerPoint || this._PackageObject.IdMediaType == ContentPackage.MediaType.PDF)
                {
                    HyperLink fileLink = new HyperLink();
                    fileLink.ID = "OriginalMedia_Field";
                    fileLink.NavigateUrl = this._PackageObject.Path + "/" + this._PackageObject.OriginalMediaFilename;
                    fileLink.Target = "_blank";
                    fileLink.Text = this._PackageObject.OriginalMediaFilename;

                    tabContentPanel.Controls.Add(AsentiaPage.BuildFormField("OriginalMedia",
                                                 _GlobalResources.OriginalMedia,
                                                 fileLink.ID,
                                                 fileLink,
                                                 false,
                                                 false,
                                                 false));
                }
                else if (this._PackageObject.IdMediaType == ContentPackage.MediaType.Video)
                {
                    if (this._PackageObject.IsVideoMedia3rdParty == true)
                    {
                        TextBox videoMediaEmbedCode = new TextBox();
                        videoMediaEmbedCode.ID = "OriginalMedia_Field";
                        videoMediaEmbedCode.Style.Add("width", "98%");
                        videoMediaEmbedCode.TextMode = TextBoxMode.MultiLine;
                        videoMediaEmbedCode.Rows = 5;
                        videoMediaEmbedCode.Enabled = false;
                        videoMediaEmbedCode.Text = this._PackageObject.VideoMediaEmbedCode;

                        tabContentPanel.Controls.Add(AsentiaPage.BuildFormField("OriginalMedia",
                                                    _GlobalResources.OriginalMedia,
                                                    videoMediaEmbedCode.ID,
                                                    videoMediaEmbedCode,
                                                    false,
                                                    false,
                                                    false));
                    }
                    else
                    {
                        HyperLink videoFileLink = new HyperLink();
                        videoFileLink.ID = "OriginalMedia_Field";
                        videoFileLink.NavigateUrl = this._PackageObject.Path + "/video/" + this._PackageObject.OriginalMediaFilename;
                        videoFileLink.Target = "_blank";
                        videoFileLink.Text = this._PackageObject.OriginalMediaFilename;

                        tabContentPanel.Controls.Add(AsentiaPage.BuildFormField("OriginalMedia",
                                                        _GlobalResources.OriginalMedia,
                                                        videoFileLink.ID,
                                                        videoFileLink,
                                                        false,
                                                        false,
                                                        false));
                    }
                }
                else
                { }
                #endregion

                #region Playback Settings Field
                if (this._PackageObject.IdMediaType == ContentPackage.MediaType.PowerPoint)
                {
                    List<Control> playbackSettingsInputControls = new List<Control>();

                    // autoplay
                    this._EnablePowerPointAutoPlay = new CheckBox();
                    this._EnablePowerPointAutoPlay.ID = "PowerPointPlaybackSettings_EnablePowerPointAutoplay_Field";
                    this._EnablePowerPointAutoPlay.Text = _GlobalResources.AutomaticallyStartPlaybackWhenContentIsLaunched;
                    this._EnablePowerPointAutoPlay.Checked = (bool)this._PackageObject.EnableAutoplay;
                    playbackSettingsInputControls.Add(this._EnablePowerPointAutoPlay);

                    // linear navigation
                    this._EnablePowerPointLinearNavigation = new CheckBox();
                    this._EnablePowerPointLinearNavigation.ID = "PowerPointPlaybackSettings_EnablePowerPointLinearNavigation_Field";
                    this._EnablePowerPointLinearNavigation.Text = _GlobalResources.AllowLearnerToNavigateThroughSlidesInLinearOrder;
                    this._EnablePowerPointLinearNavigation.Checked = (bool)this._PackageObject.AllowFastForward;

                    if (this._PackageObject.AllowNavigation == true)
                    {
                        this._EnablePowerPointLinearNavigation.Checked = true;
                        this._EnablePowerPointLinearNavigation.Enabled = false;
                    }

                    playbackSettingsInputControls.Add(this._EnablePowerPointLinearNavigation);

                    // jump navigation
                    this._EnablePowerPointJumpNavigation = new CheckBox();
                    this._EnablePowerPointJumpNavigation.ID = "PowerPointPlaybackSettings_EnablePowerPointJumpNavigation_Field";
                    this._EnablePowerPointJumpNavigation.Text = _GlobalResources.AllowLearnerToNavigateThroughSlidesInAnyOrder;
                    this._EnablePowerPointJumpNavigation.Attributes.Add("onclick", "TogglePowerPointJumpNavigation();");
                    this._EnablePowerPointJumpNavigation.Checked = (bool)this._PackageObject.AllowNavigation;
                    playbackSettingsInputControls.Add(this._EnablePowerPointJumpNavigation);

                    // resume
                    this._EnablePowerPointBookmarking = new CheckBox();
                    this._EnablePowerPointBookmarking.ID = "PowerPointPlaybackSettings_EnablePowerPointBookmarking_Field";
                    this._EnablePowerPointBookmarking.Text = _GlobalResources.AllowLearnerToResumePlaybackOnContentReLaunch;
                    this._EnablePowerPointBookmarking.Checked = (bool)this._PackageObject.AllowResume;
                    playbackSettingsInputControls.Add(this._EnablePowerPointBookmarking);

                    tabContentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("PowerPointPlaybackSettings",
                                                                                                _GlobalResources.PlaybackSettings,
                                                                                                playbackSettingsInputControls,
                                                                                                false,
                                                                                                true));
                }
                else if (this._PackageObject.IdMediaType == ContentPackage.MediaType.Video)
                {
                    string videoType = String.Empty;

                    if (!String.IsNullOrWhiteSpace(this._PackageObject.OriginalMediaFilename))
                    { videoType = "default"; }
                    else
                    {
                        if (this._PackageObject.VideoMediaEmbedCode.Contains("vimeo"))
                        { videoType = "vimeo"; }
                        else
                        { videoType = "youtube"; }
                    }

                    List<Control> playbackSettingsInputControls = new List<Control>();

                    // autoplay
                    this._EnableVideoAutoPlay = new CheckBox();
                    this._EnableVideoAutoPlay.ID = "VideoPlaybackSettings_EnableVideoAutoplay_Field";
                    this._EnableVideoAutoPlay.Text = _GlobalResources.AutomaticallyStartPlaybackWhenVideoIsLaunched;
                    this._EnableVideoAutoPlay.Checked = (bool)this._PackageObject.EnableAutoplay;

                    if (videoType != "default")
                    { this._EnableVideoAutoPlay.Enabled = false; }

                    playbackSettingsInputControls.Add(this._EnableVideoAutoPlay);

                    // rewind
                    this._EnableVideoRewind = new CheckBox();
                    this._EnableVideoRewind.ID = "VideoPlaybackSettings_EnableVideoRewind_Field";
                    this._EnableVideoRewind.Text = _GlobalResources.AllowLearnerToJumpBackwardOrRewindVideo;
                    this._EnableVideoRewind.Checked = (bool)this._PackageObject.AllowRewind;

                    if (videoType != "default")
                    { this._EnableVideoRewind.Enabled = false; }

                    playbackSettingsInputControls.Add(this._EnableVideoRewind);

                    // fast forward
                    this._EnableVideoFastForward = new CheckBox();
                    this._EnableVideoFastForward.ID = "VideoPlaybackSettings_EnableVideoFastForward_Field";
                    this._EnableVideoFastForward.Text = _GlobalResources.AllowLearnerToJumpForwardOrFastForwardVideo;
                    this._EnableVideoFastForward.Checked = (bool)this._PackageObject.AllowFastForward;

                    if (videoType != "default")
                    { this._EnableVideoFastForward.Enabled = false; }

                    playbackSettingsInputControls.Add(this._EnableVideoFastForward);

                    // resume
                    this._EnableVideoResume = new CheckBox();
                    this._EnableVideoResume.ID = "VideoPlaybackSettings_EnableVideoResume_Field";
                    this._EnableVideoResume.Text = _GlobalResources.AllowLearnerToResumePlaybackOnVideoReLaunch;
                    this._EnableVideoResume.Checked = (bool)this._PackageObject.AllowResume;

                    if (videoType == "youtube")
                    { this._EnableVideoResume.Enabled = false; }

                    playbackSettingsInputControls.Add(this._EnableVideoResume);

                    tabContentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("VideoPlaybackSettings",
                                                                                                _GlobalResources.PlaybackSettings,
                                                                                                playbackSettingsInputControls,
                                                                                                false,
                                                                                                true));
                }
                else
                { }
                #endregion

                #region Completion Criteria Field
                if (this._PackageObject.IdMediaType == ContentPackage.MediaType.PowerPoint)
                {
                    List<Control> completionCriteriaInputControls = new List<Control>();

                    // on start
                    Panel onStartContainerPanel = new Panel();
                    onStartContainerPanel.ID = "PowerPointCompletionCriteria_CompletePowerPointOnStart_Container";

                    this._CompletePowerPointOnStart = new RadioButton();
                    this._CompletePowerPointOnStart.ID = "PowerPointCompletionCriteria_CompletePowerPointOnStart_Field";
                    this._CompletePowerPointOnStart.GroupName = "PowerPointCompletionCriteria_PowerPointCompletionCriteria_Field";
                    this._CompletePowerPointOnStart.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onstart\", \"powerpoint\")");
                    this._CompletePowerPointOnStart.Text = _GlobalResources.SetCompletionOnContentLaunch;

                    onStartContainerPanel.Controls.Add(this._CompletePowerPointOnStart);
                    completionCriteriaInputControls.Add(onStartContainerPanel);

                    // on percentage viewed
                    Panel onPercentageContainerPanel = new Panel();
                    onPercentageContainerPanel.ID = "PowerPointCompletionCriteria_CompletePowerPointOnPercentage_Container";

                    this._CompletePowerPointOnPercentage = new RadioButton();
                    this._CompletePowerPointOnPercentage.ID = "PowerPointCompletionCriteria_CompletePowerPointOnPercentage_Field";
                    this._CompletePowerPointOnPercentage.GroupName = "PowerPointCompletionCriteria_PowerPointCompletionCriteria_Field";
                    this._CompletePowerPointOnPercentage.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onpercentage\", \"powerpoint\")");
                    onPercentageContainerPanel.Controls.Add(this._CompletePowerPointOnPercentage);

                    string[] splitDelimiter = new string[] { "[textbox]" };
                    string[] completionOnPercentageLabelArray = _GlobalResources.SetCompletionWhenLearnerHasViewed__textbox__OfSlides.Split(splitDelimiter, StringSplitOptions.None);

                    Label setCompletionWhenUserHasViewed = new Label();
                    setCompletionWhenUserHasViewed.Text = completionOnPercentageLabelArray[0];
                    setCompletionWhenUserHasViewed.AssociatedControlID = this._CompletePowerPointOnPercentage.ID;
                    onPercentageContainerPanel.Controls.Add(setCompletionWhenUserHasViewed);

                    this._PowerPointProgressForCompletionPercentage = new TextBox();
                    this._PowerPointProgressForCompletionPercentage.ID = "PowerPointCompletionCriteria_PowerPointProgressForCompletionPercentage_Field";
                    this._PowerPointProgressForCompletionPercentage.CssClass = "InputXShort";
                    onPercentageContainerPanel.Controls.Add(this._PowerPointProgressForCompletionPercentage);

                    Label percentageOfSlides = new Label();
                    percentageOfSlides.Text = completionOnPercentageLabelArray[1];
                    percentageOfSlides.AssociatedControlID = this._CompletePowerPointOnPercentage.ID;
                    onPercentageContainerPanel.Controls.Add(percentageOfSlides);

                    completionCriteriaInputControls.Add(onPercentageContainerPanel);

                    // on end
                    Panel onEndContainerPanel = new Panel();
                    onEndContainerPanel.ID = "PowerPointCompletionCriteria_CompletePowerPointOnEnd_Container";

                    this._CompletePowerPointOnEnd = new RadioButton();
                    this._CompletePowerPointOnEnd.ID = "PowerPointCompletionCriteria_CompletePowerPointOnEnd_Field";
                    this._CompletePowerPointOnEnd.GroupName = "PowerPointCompletionCriteria_PowerPointCompletionCriteria_Field";
                    this._CompletePowerPointOnEnd.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onend\", \"powerpoint\")");
                    this._CompletePowerPointOnEnd.Text = _GlobalResources.SetCompletionWhenLearnerHasViewedAllSlides;

                    onEndContainerPanel.Controls.Add(this._CompletePowerPointOnEnd);
                    completionCriteriaInputControls.Add(onEndContainerPanel);

                    // set the completion criteria selected field
                    if (this._PackageObject.MinProgressForCompletion == 0)
                    { 
                        this._CompletePowerPointOnStart.Checked = true;
                        this._PowerPointProgressForCompletionPercentage.Text = String.Empty;
                        this._PowerPointProgressForCompletionPercentage.Enabled = false;
                    }
                    else if (this._PackageObject.MinProgressForCompletion == 1)
                    { 
                        this._CompletePowerPointOnEnd.Checked = true;
                        this._PowerPointProgressForCompletionPercentage.Text = String.Empty;
                        this._PowerPointProgressForCompletionPercentage.Enabled = false;
                    }
                    else
                    {
                        this._CompletePowerPointOnPercentage.Checked = true;
                        this._PowerPointProgressForCompletionPercentage.Text = (this._PackageObject.MinProgressForCompletion * 100).ToString();
                    }

                    tabContentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("PowerPointCompletionCriteria",
                                                                                                _GlobalResources.CompletionCriteria,
                                                                                                completionCriteriaInputControls,
                                                                                                true,
                                                                                                true,
                                                                                                false));
                }
                else if (this._PackageObject.IdMediaType == ContentPackage.MediaType.Video)
                {
                    string videoType = String.Empty;

                    if (!String.IsNullOrWhiteSpace(this._PackageObject.OriginalMediaFilename))
                    { videoType = "default"; }
                    else
                    {
                        if (this._PackageObject.VideoMediaEmbedCode.Contains("vimeo"))
                        { videoType = "vimeo"; }
                        else
                        { videoType = "youtube"; }
                    }

                    List<Control> completionCriteriaInputControls = new List<Control>();

                    // on start
                    Panel onStartContainerPanel = new Panel();
                    onStartContainerPanel.ID = "VideoCompletionCriteria_CompleteVideoOnStart_Container";

                    this._CompleteVideoOnStart = new RadioButton();
                    this._CompleteVideoOnStart.ID = "VideoCompletionCriteria_CompleteVideoOnStart_Field";
                    this._CompleteVideoOnStart.GroupName = "VideoCompletionCriteria_VideoCompletionCriteria_Field";
                    this._CompleteVideoOnStart.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onstart\", \"video\")");
                    this._CompleteVideoOnStart.Text = _GlobalResources.SetCompletionOnVideoLaunch;

                    onStartContainerPanel.Controls.Add(this._CompleteVideoOnStart);
                    completionCriteriaInputControls.Add(onStartContainerPanel);

                    // on percentage viewed
                    Panel onPercentageContainerPanel = new Panel();
                    onPercentageContainerPanel.ID = "VideoCompletionCriteria_CompleteVideoOnPercentage_Container";

                    this._CompleteVideoOnPercentage = new RadioButton();
                    this._CompleteVideoOnPercentage.ID = "VideoCompletionCriteria_CompleteVideoOnPercentage_Field";
                    this._CompleteVideoOnPercentage.GroupName = "VideoCompletionCriteria_VideoCompletionCriteria_Field";
                    this._CompleteVideoOnPercentage.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onpercentage\", \"video\")");
                    onPercentageContainerPanel.Controls.Add(this._CompleteVideoOnPercentage);

                    string[] splitDelimiter = new string[] { "[textbox]" };
                    string[] completionOnPercentageLabelArray = _GlobalResources.SetCompletionWhenLearnerHasViewed__textbox__OfVideo.Split(splitDelimiter, StringSplitOptions.None);

                    Label setCompletionWhenUserHasViewed = new Label();
                    setCompletionWhenUserHasViewed.Text = completionOnPercentageLabelArray[0];
                    setCompletionWhenUserHasViewed.AssociatedControlID = this._CompleteVideoOnPercentage.ID;
                    onPercentageContainerPanel.Controls.Add(setCompletionWhenUserHasViewed);

                    this._VideoProgressForCompletionPercentage = new TextBox();
                    this._VideoProgressForCompletionPercentage.ID = "VideoCompletionCriteria_VideoProgressForCompletionPercentage_Field";
                    this._VideoProgressForCompletionPercentage.CssClass = "InputXShort";
                    onPercentageContainerPanel.Controls.Add(this._VideoProgressForCompletionPercentage);

                    Label percentageOfVideo = new Label();
                    percentageOfVideo.Text = completionOnPercentageLabelArray[1];
                    percentageOfVideo.AssociatedControlID = this._CompleteVideoOnPercentage.ID;
                    onPercentageContainerPanel.Controls.Add(percentageOfVideo);

                    completionCriteriaInputControls.Add(onPercentageContainerPanel);

                    // on end
                    Panel onEndContainerPanel = new Panel();
                    onEndContainerPanel.ID = "VideoCompletionCriteria_CompleteVideoOnEnd_Container";

                    this._CompleteVideoOnEnd = new RadioButton();
                    this._CompleteVideoOnEnd.ID = "VideoCompletionCriteria_CompleteVideoOnEnd_Field";
                    this._CompleteVideoOnEnd.GroupName = "VideoCompletionCriteria_VideoCompletionCriteria_Field";
                    this._CompleteVideoOnEnd.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onend\", \"video\")");
                    this._CompleteVideoOnEnd.Text = _GlobalResources.SetCompletionWhenLearnerHasViewedTheEntireVideo;

                    onEndContainerPanel.Controls.Add(this._CompleteVideoOnEnd);
                    completionCriteriaInputControls.Add(onEndContainerPanel);

                    // set the completion criteria selected field
                    if (this._PackageObject.MinProgressForCompletion == 0)
                    { 
                        this._CompleteVideoOnStart.Checked = true;
                        this._VideoProgressForCompletionPercentage.Text = String.Empty;
                        this._VideoProgressForCompletionPercentage.Enabled = false;
                    }
                    else if (this._PackageObject.MinProgressForCompletion == 1)
                    { 
                        this._CompleteVideoOnEnd.Checked = true;
                        this._VideoProgressForCompletionPercentage.Text = String.Empty;
                        this._VideoProgressForCompletionPercentage.Enabled = false;
                    }
                    else
                    {
                        this._CompleteVideoOnPercentage.Checked = true;
                        this._VideoProgressForCompletionPercentage.Text = (this._PackageObject.MinProgressForCompletion * 100).ToString();
                    }

                    // if the video type is youtube, disable percentage and end options
                    if (videoType == "youtube")
                    {
                        this._CompleteVideoOnPercentage.Checked = false;
                        this._CompleteVideoOnPercentage.Enabled = false;
                        this._VideoProgressForCompletionPercentage.Text = String.Empty;
                        this._VideoProgressForCompletionPercentage.Enabled = false;

                        this._CompleteVideoOnEnd.Checked = false;
                        this._CompleteVideoOnEnd.Enabled = false;
                    }

                    tabContentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("VideoCompletionCriteria",
                                                                                                _GlobalResources.CompletionCriteria,
                                                                                                completionCriteriaInputControls,
                                                                                                false,
                                                                                                true,
                                                                                                false));                    
                }
                else if (this._PackageObject.IdMediaType == ContentPackage.MediaType.PDF)
                {
                    List<Control> completionCriteriaInputControls = new List<Control>();

                    // on start
                    Panel onStartContainerPanel = new Panel();
                    onStartContainerPanel.ID = "PDFCompletionCriteria_CompletePDFOnStart_Container";

                    this._CompletePDFOnStart = new RadioButton();
                    this._CompletePDFOnStart.ID = "PDFCompletionCriteria_CompletePDFOnStart_Field";
                    this._CompletePDFOnStart.GroupName = "PDFCompletionCriteria_PDFCompletionCriteria_Field";
                    this._CompletePDFOnStart.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onstart\", \"pdf\")");
                    this._CompletePDFOnStart.Text = _GlobalResources.SetCompletionOnContentLaunch;

                    onStartContainerPanel.Controls.Add(this._CompletePDFOnStart);
                    completionCriteriaInputControls.Add(onStartContainerPanel);

                    // on percentage viewed
                    Panel onPercentageContainerPanel = new Panel();
                    onPercentageContainerPanel.ID = "PDFCompletionCriteria_CompletePDFOnPercentage_Container";

                    this._CompletePDFOnPercentage = new RadioButton();
                    this._CompletePDFOnPercentage.ID = "PDFCompletionCriteria_CompletePDFOnPercentage_Field";
                    this._CompletePDFOnPercentage.GroupName = "PDFCompletionCriteria_PDFCompletionCriteria_Field";
                    this._CompletePDFOnPercentage.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onpercentage\", \"pdf\")");
                    onPercentageContainerPanel.Controls.Add(this._CompletePDFOnPercentage);

                    string[] splitDelimiter = new string[] { "[textbox]" };
                    string[] completionOnPercentageLabelArray = _GlobalResources.SetCompletionWhenLearnerHasViewed__textbox__OfPages.Split(splitDelimiter, StringSplitOptions.None);

                    Label setCompletionWhenUserHasViewed = new Label();
                    setCompletionWhenUserHasViewed.Text = completionOnPercentageLabelArray[0];
                    setCompletionWhenUserHasViewed.AssociatedControlID = this._CompletePDFOnPercentage.ID;
                    onPercentageContainerPanel.Controls.Add(setCompletionWhenUserHasViewed);

                    this._PDFProgressForCompletionPercentage = new TextBox();
                    this._PDFProgressForCompletionPercentage.ID = "PDFCompletionCriteria_PDFProgressForCompletionPercentage_Field";
                    this._PDFProgressForCompletionPercentage.CssClass = "InputXShort";
                    onPercentageContainerPanel.Controls.Add(this._PDFProgressForCompletionPercentage);

                    Label percentageOfSlides = new Label();
                    percentageOfSlides.Text = completionOnPercentageLabelArray[1];
                    percentageOfSlides.AssociatedControlID = this._CompletePDFOnPercentage.ID;
                    onPercentageContainerPanel.Controls.Add(percentageOfSlides);

                    completionCriteriaInputControls.Add(onPercentageContainerPanel);

                    // on end
                    Panel onEndContainerPanel = new Panel();
                    onEndContainerPanel.ID = "PDFCompletionCriteria_CompletePDFOnEnd_Container";

                    this._CompletePDFOnEnd = new RadioButton();
                    this._CompletePDFOnEnd.ID = "PDFCompletionCriteria_CompletePDFOnEnd_Field";
                    this._CompletePDFOnEnd.GroupName = "PDFCompletionCriteria_PDFCompletionCriteria_Field";
                    this._CompletePDFOnEnd.Attributes.Add("onclick", "CompletionCriteriaSelected(\"onend\", \"pdf\")");
                    this._CompletePDFOnEnd.Text = _GlobalResources.SetCompletionWhenLearnerHasViewedAllPages;

                    onEndContainerPanel.Controls.Add(this._CompletePDFOnEnd);
                    completionCriteriaInputControls.Add(onEndContainerPanel);

                    // set the completion criteria selected field
                    if (this._PackageObject.MinProgressForCompletion == 0)
                    {
                        this._CompletePDFOnStart.Checked = true;
                        this._PDFProgressForCompletionPercentage.Text = String.Empty;
                        this._PDFProgressForCompletionPercentage.Enabled = false;
                    }
                    else if (this._PackageObject.MinProgressForCompletion == 1)
                    {
                        this._CompletePDFOnEnd.Checked = true;
                        this._PDFProgressForCompletionPercentage.Text = String.Empty;
                        this._PDFProgressForCompletionPercentage.Enabled = false;
                    }
                    else
                    {
                        this._CompletePDFOnPercentage.Checked = true;
                        this._PDFProgressForCompletionPercentage.Text = (this._PackageObject.MinProgressForCompletion * 100).ToString();
                    }

                    tabContentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("PDFCompletionCriteria",
                                                                                                _GlobalResources.CompletionCriteria,
                                                                                                completionCriteriaInputControls,
                                                                                                true,
                                                                                                true,
                                                                                                false));
                }
                else
                { }
                #endregion
            }

            #region Size Field

            Label size = new Label();
            size.ID = "PackageModify_" + "Size" + "_Field";
            size.Text = Utility.GetSizeStringFromKB(this._PackageObject.Kb);

            //Add to Main Field Panel
            tabContentPanel.Controls.Add(AsentiaPage.BuildFormField("PackageModify_Size",
                                                _GlobalResources.Size,
                                                size.ID,
                                                size,
                                                false,
                                                false,
                                                false));

            #endregion

            #region Created Field

            Label createdField = new Label();
            createdField.ID = "PackageModify_" + "Created" + "_Field";
            createdField.Text = Convert.ToString(_PackageObject.DtCreated);

            //Add to Main Field Panel
            tabContentPanel.Controls.Add(AsentiaPage.BuildFormField("PackageModify_Created",
                                      _GlobalResources.Created,
                                      createdField.ID,
                                      createdField,
                                      false,
                                      false,
                                      false));

            #endregion

            #region Last Modified Field

            Label lastModified = new Label();
            lastModified.ID = "PackageModify_" + "LastModified" + "_Field";
            lastModified.Text = Convert.ToString(_PackageObject.DtModified);

            //Add to Main Field Panel
            tabContentPanel.Controls.Add(AsentiaPage.BuildFormField("PackageModify_LastModified",
                                    _GlobalResources.LastModified,
                                    lastModified.ID,
                                    lastModified,
                                    false,
                                    false,
                                    false));

            #endregion

            this.PackageFormTabPanelsContainer.Controls.Add(tabContentPanel);
        }
        #endregion

        #region _BuildManifestTabPanel
        /// <summary>
        /// Build Manifest Tab Panel
        /// </summary>
        private void _BuildManifestTabPanel()
        {
            // content panel
            Panel tabContentPanel = new Panel();
            tabContentPanel.ID = "PackageModify_" + "Manifest" + "_TabPanel";
            tabContentPanel.CssClass = "TabContentPanel";
            tabContentPanel.Style.Add("display", "none");

            List<Control> manifestControls = new List<Control>();

            // warning message for updating manifest
            Panel warningMessageForUpdatingManifest = new Panel();
            warningMessageForUpdatingManifest.ID = "WarningMessageForUpdatingManifest";
            this.FormatFormInformationPanel(warningMessageForUpdatingManifest, _GlobalResources.IfYouAreNotFamiliarWithXMLStructures);
            manifestControls.Add(warningMessageForUpdatingManifest);

            // manifest field
            this._ManifestText = new TextBox();
            this._ManifestText.ID = "PackageModify_Manifest_Field";
            this._ManifestText.Style.Add("width", "98%");
            this._ManifestText.Rows = 20;
            this._ManifestText.Text = this._PackageObject.Manifest;
            this._ManifestText.TextMode = TextBoxMode.MultiLine;
            this._ManifestText.Enabled = true;

            manifestControls.Add(this._ManifestText);

            // revert manifest to original checkbox
            this._RevertManifestToOriginal = new CheckBox();
            this._RevertManifestToOriginal.ID = "PackageModify_" + "Manifest" + "_RevertCheckBox";
            this._RevertManifestToOriginal.Text = _GlobalResources.RevertThePackagesManifestBackToOriginal;
            this._RevertManifestToOriginal.Attributes.Add("onclick", "EnableDisableManifestUpdate();");

            // if there is no imsmanifest.orig.xml file, disable the revert option
            if (!File.Exists(Server.MapPath(this._PackageObject.Path + "/imsmanifest.orig.xml")))
            { this._RevertManifestToOriginal.Enabled = false; }

            manifestControls.Add(this._RevertManifestToOriginal);

            tabContentPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("PackageModify_ManifestFields",
                                                                                        _GlobalResources.Manifest,
                                                                                        manifestControls,
                                                                                        false,
                                                                                        false));

            this.PackageFormTabPanelsContainer.Controls.Add(tabContentPanel);
        }
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for the form.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("Manifest", _GlobalResources.Manifest));

            // build and attach the tabs
            this.PackageFormContainer.Controls.Add(AsentiaPage.BuildTabListPanel("PackageModify", tabs));
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;

            // Package Name field
            if (String.IsNullOrWhiteSpace(this._PackageFileName.Text))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.PackageFormContainer, "PackageModify_Name", _GlobalResources.Name + " " + _GlobalResources.IsRequired);
            }

            // if this was a media upload, we need to validate playback and completion criteria properties
            if (this._PackageObject.IsMediaUpload == true)
            {
                // PowerPoint
                if (this._PackageObject.IdMediaType == ContentPackage.MediaType.PowerPoint)
                {
                    // validate the completion criteria if it's a percentage
                    if (this._CompletePowerPointOnPercentage.Checked)
                    {
                        int percentage;

                        if (
                            String.IsNullOrWhiteSpace(this._PowerPointProgressForCompletionPercentage.Text)
                            || !int.TryParse(this._PowerPointProgressForCompletionPercentage.Text, out percentage)
                            || (int.TryParse(this._PowerPointProgressForCompletionPercentage.Text, out percentage) && (percentage < 0 || percentage > 100))
                           )
                        {
                            propertiesTabHasErrors = true;
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "PowerPointCompletionCriteria", _GlobalResources.PercentageMustBeANumberBetween0And100);
                        }
                        else
                        {

                        }
                    }
                }
                // Video
                else if (this._PackageObject.IdMediaType == ContentPackage.MediaType.Video)
                {
                    // validate the completion criteria if it's a percentage
                    if (this._CompleteVideoOnPercentage.Checked)
                    {
                        int percentage;

                        if (
                            String.IsNullOrWhiteSpace(this._VideoProgressForCompletionPercentage.Text)
                            || !int.TryParse(this._VideoProgressForCompletionPercentage.Text, out percentage)
                            || (int.TryParse(this._VideoProgressForCompletionPercentage.Text, out percentage) && (percentage < 0 || percentage > 100))
                           )
                        {
                            propertiesTabHasErrors = true;
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "VideoCompletionCriteria", _GlobalResources.PercentageMustBeANumberBetween0And100);
                        }
                        else
                        {

                        }
                    }
                }
                // PDF
                if (this._PackageObject.IdMediaType == ContentPackage.MediaType.PDF)
                {
                    // validate the completion criteria if it's a percentage
                    if (this._CompletePDFOnPercentage.Checked)
                    {
                        int percentage;

                        if (
                            String.IsNullOrWhiteSpace(this._PDFProgressForCompletionPercentage.Text)
                            || !int.TryParse(this._PDFProgressForCompletionPercentage.Text, out percentage)
                            || (int.TryParse(this._PDFProgressForCompletionPercentage.Text, out percentage) && (percentage < 0 || percentage > 100))
                           )
                        {
                            propertiesTabHasErrors = true;
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.PageContentContainer, "PDFCompletionCriteria", _GlobalResources.PercentageMustBeANumberBetween0And100);
                        }
                        else
                        {

                        }
                    }
                }
                else
                { }
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.PackageFormContainer, "PackageModify_Tab1_TabLI"); }

            return isValid;
        }
        #endregion

        #region _SavePackage_Command
        /// <summary>
        /// Handles Save Button Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _SavePackage_Command(object sender, EventArgs e)
        {
            try
            {
                if (this.QueryStringInt("id", 0) == 0)
                {
                    this.DisplayFeedback(_GlobalResources.PackageIDNotFound, true);
                    return;
                }

                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                this._PackageObject.Name = this._PackageFileName.Text;

                // if this was a media upload, save the playback and completion criteria, and also write that information to the package's config files
                if (this._PackageObject.IsMediaUpload == true)
                {
                    // PowerPoint
                    if (this._PackageObject.IdMediaType == ContentPackage.MediaType.PowerPoint)
                    {
                        // playback settings
                        this._PackageObject.EnableAutoplay = this._EnablePowerPointAutoPlay.Checked;
                        this._PackageObject.AllowFastForward = this._EnablePowerPointLinearNavigation.Checked;
                        this._PackageObject.AllowRewind = this._EnablePowerPointLinearNavigation.Checked;
                        this._PackageObject.AllowNavigation = this._EnablePowerPointJumpNavigation.Checked;
                        this._PackageObject.AllowResume = this._EnablePowerPointBookmarking.Checked;

                        // package completion criteria
                        if (this._CompletePowerPointOnStart.Checked)
                        { this._PackageObject.MinProgressForCompletion = 0; }
                        else if (this._CompletePowerPointOnPercentage.Checked)
                        { this._PackageObject.MinProgressForCompletion = Convert.ToDouble(this._PowerPointProgressForCompletionPercentage.Text) / 100; }
                        else if (this._CompletePowerPointOnEnd.Checked)
                        { this._PackageObject.MinProgressForCompletion = 1; }
                        else
                        { this._PackageObject.MinProgressForCompletion = 0; }

                        // open the package's /script/config.js file to replace playback and completion criteria settings
                        if (File.Exists(Server.MapPath(this._PackageObject.Path + "/script/config.js"))) // let's make sure the config file exists first, just in case someone manages to access this prior to processing
                        {
                            StringBuilder configContent = new StringBuilder();
                            string fileLine = String.Empty;

                            using (StreamReader reader = new StreamReader(Server.MapPath(this._PackageObject.Path + "/script/config.js")))
                            {
                                // read file line by line and replace the variables
                                while ((fileLine = reader.ReadLine()) != null)
                                {
                                    if (fileLine.Contains("var autoplay ="))
                                    { configContent.AppendLine("var autoplay = " + this._PackageObject.EnableAutoplay.ToString().ToLower() + ";"); }
                                    else if (fileLine.Contains("var useBookmarking ="))
                                    { configContent.AppendLine("var useBookmarking = " + this._PackageObject.AllowResume.ToString().ToLower() + ";"); }
                                    else if (fileLine.Contains("var allowForward ="))
                                    { configContent.AppendLine("var allowForward = " + this._PackageObject.AllowFastForward.ToString().ToLower() + ";"); }
                                    else if (fileLine.Contains("var allowBackward ="))
                                    { configContent.AppendLine("var allowBackward = " + this._PackageObject.AllowRewind.ToString().ToLower() + ";"); }
                                    else if (fileLine.Contains("var allowNavigation ="))
                                    { configContent.AppendLine("var allowNavigation = " + this._PackageObject.AllowNavigation.ToString().ToLower() + ";"); }
                                    else if (fileLine.Contains("var completionThreshold ="))
                                    { configContent.AppendLine("var completionThreshold = " + this._PackageObject.MinProgressForCompletion.ToString() + ";"); }
                                    else
                                    { configContent.AppendLine(fileLine); }
                                }

                                // close the reader
                                reader.Close();
                            }

                            // delete the original file and write the new one
                            File.Delete(Server.MapPath(this._PackageObject.Path + "/script/config.js"));

                            using (StreamWriter writer = new StreamWriter(Server.MapPath(this._PackageObject.Path + "/script/config.js")))
                            {
                                writer.Write(configContent);
                                writer.Close();
                            }
                        }
                    }
                    // Video
                    else if (this._PackageObject.IdMediaType == ContentPackage.MediaType.Video)
                    {
                        // determine the video type
                        string videoType = String.Empty;

                        if (!String.IsNullOrWhiteSpace(this._PackageObject.OriginalMediaFilename))
                        { videoType = "default"; }
                        else
                        {
                            if (this._PackageObject.VideoMediaEmbedCode.Contains("vimeo"))
                            { videoType = "vimeo"; }
                            else
                            { videoType = "youtube"; }
                        }

                        // set playback and completion criteria settings based on video type
                        if (videoType == "default")
                        {
                            // playback settings
                            this._PackageObject.EnableAutoplay = this._EnableVideoAutoPlay.Checked;
                            this._PackageObject.AllowFastForward = this._EnableVideoFastForward.Checked;
                            this._PackageObject.AllowRewind = this._EnableVideoRewind.Checked;
                            this._PackageObject.AllowResume = this._EnableVideoResume.Checked;

                            // package completion criteria
                            if (this._CompleteVideoOnStart.Checked)
                            { this._PackageObject.MinProgressForCompletion = 0; }
                            else if (this._CompleteVideoOnPercentage.Checked)
                            { this._PackageObject.MinProgressForCompletion = Convert.ToDouble(this._VideoProgressForCompletionPercentage.Text) / 100; }
                            else if (this._CompleteVideoOnEnd.Checked)
                            { this._PackageObject.MinProgressForCompletion = 1; }
                            else
                            { this._PackageObject.MinProgressForCompletion = 0; }
                        }
                        else if (videoType == "youtube")
                        {                            
                            // playback settings
                            this._PackageObject.EnableAutoplay = false;
                            this._PackageObject.AllowFastForward = false;
                            this._PackageObject.AllowRewind = false;
                            this._PackageObject.AllowResume = false;

                            // package completion criteria
                            this._PackageObject.MinProgressForCompletion = 0;
                        }
                        else if (videoType == "vimeo")
                        {
                            // playback settings
                            this._PackageObject.EnableAutoplay = false;
                            this._PackageObject.AllowFastForward = false;
                            this._PackageObject.AllowRewind = false;
                            this._PackageObject.AllowResume = this._EnableVideoResume.Checked;

                            // package completion criteria
                            if (this._CompleteVideoOnStart.Checked)
                            { this._PackageObject.MinProgressForCompletion = 0; }
                            else if (this._CompleteVideoOnPercentage.Checked)
                            { this._PackageObject.MinProgressForCompletion = Convert.ToDouble(this._VideoProgressForCompletionPercentage.Text) / 100; }
                            else if (this._CompleteVideoOnEnd.Checked)
                            { this._PackageObject.MinProgressForCompletion = 1; }
                            else
                            { this._PackageObject.MinProgressForCompletion = 0; }
                        }
                        else
                        { }

                        // open the package's /script/config.js file to replace playback and completion criteria settings
                        StringBuilder configContent = new StringBuilder();
                        string fileLine = String.Empty;

                        using (StreamReader reader = new StreamReader(Server.MapPath(this._PackageObject.Path + "/script/config.js")))
                        {
                            // read file line by line and replace the variables
                            while ((fileLine = reader.ReadLine()) != null)
                            {
                                if (fileLine.Contains("var allowSeekForward ="))
                                { configContent.AppendLine("var allowSeekForward = " + this._PackageObject.AllowFastForward.ToString().ToLower() + ";"); }
                                else if (fileLine.Contains("var allowSeekBackward ="))
                                { configContent.AppendLine("var allowSeekBackward = " + this._PackageObject.AllowRewind.ToString().ToLower() + ";"); }
                                else if (fileLine.Contains("var useBookmarking ="))
                                { configContent.AppendLine("var useBookmarking = " + this._PackageObject.AllowResume.ToString().ToLower() + ";"); }
                                else if (fileLine.Contains("var autoplay ="))
                                { configContent.AppendLine("var autoplay = " + this._PackageObject.EnableAutoplay.ToString().ToLower() + ";"); }
                                else if (fileLine.Contains("var completionThreshold ="))
                                { configContent.AppendLine("var completionThreshold = " + this._PackageObject.MinProgressForCompletion.ToString() + ";"); }
                                else
                                { configContent.AppendLine(fileLine); }
                            }

                            // close the reader
                            reader.Close();
                        }

                        // delete the original file and write the new one
                        File.Delete(Server.MapPath(this._PackageObject.Path + "/script/config.js"));

                        using (StreamWriter writer = new StreamWriter(Server.MapPath(this._PackageObject.Path + "/script/config.js")))
                        {
                            writer.Write(configContent);
                            writer.Close();
                        }
                    }
                    // PDF
                    else if (this._PackageObject.IdMediaType == ContentPackage.MediaType.PDF)
                    {
                        // package completion criteria
                        if (this._CompletePDFOnStart.Checked)
                        { this._PackageObject.MinProgressForCompletion = 0; }
                        else if (this._CompletePDFOnPercentage.Checked)
                        { this._PackageObject.MinProgressForCompletion = Convert.ToDouble(this._PDFProgressForCompletionPercentage.Text) / 100; }
                        else if (this._CompletePDFOnEnd.Checked)
                        { this._PackageObject.MinProgressForCompletion = 1; }
                        else
                        { this._PackageObject.MinProgressForCompletion = 0; }

                        // open the package's /script/config.js file to replace completion criteria settings
                        if (File.Exists(Server.MapPath(this._PackageObject.Path + "/script/config.js"))) // let's make sure the config file exists first, just in case someone manages to access this prior to processing
                        {
                            StringBuilder configContent = new StringBuilder();
                            string fileLine = String.Empty;

                            using (StreamReader reader = new StreamReader(Server.MapPath(this._PackageObject.Path + "/script/config.js")))
                            {
                                // read file line by line and replace the variables
                                while ((fileLine = reader.ReadLine()) != null)
                                {                                    
                                    if (fileLine.Contains("var completionThreshold ="))
                                    { configContent.AppendLine("var completionThreshold = " + this._PackageObject.MinProgressForCompletion.ToString() + ";"); }
                                    else
                                    { configContent.AppendLine(fileLine); }
                                }

                                // close the reader
                                reader.Close();
                            }

                            // delete the original file and write the new one
                            File.Delete(Server.MapPath(this._PackageObject.Path + "/script/config.js"));

                            using (StreamWriter writer = new StreamWriter(Server.MapPath(this._PackageObject.Path + "/script/config.js")))
                            {
                                writer.Write(configContent);
                                writer.Close();
                            }
                        }
                    }
                    else
                    { }

                    // edit the manifest to update the completion threshold, simpler to just rewrite than to evaluate if it needs to be rewritten                    
                    if (File.Exists(Server.MapPath(this._PackageObject.Path + "/imsmanifest.xml"))) // let's make sure the manifest file exists first, just in case someone manages to access this prior to processing
                    {
                        StringBuilder manifestContent = new StringBuilder();
                        string fileLine = String.Empty;

                        using (StreamReader reader = new StreamReader(Server.MapPath(this._PackageObject.Path + "/imsmanifest.xml")))
                        {
                            // read file line by line and replace the variables
                            while ((fileLine = reader.ReadLine()) != null)
                            {
                                if (fileLine.Contains("<adlcp:completionThreshold>"))
                                {
                                    string adlcpCompletionThresholdNode = String.Empty;

                                    if (this._PackageObject.MinProgressForCompletion == 0)
                                    { adlcpCompletionThresholdNode = "<adlcp:completionThreshold>0</adlcp:completionThreshold>"; }
                                    else if (this._PackageObject.MinProgressForCompletion == 1)
                                    { adlcpCompletionThresholdNode = "<adlcp:completionThreshold>1</adlcp:completionThreshold>"; }
                                    else if (this._PackageObject.MinProgressForCompletion > 0 && this._PackageObject.MinProgressForCompletion < 1)
                                    { adlcpCompletionThresholdNode = "<adlcp:completionThreshold>" + String.Format("{0:0.0}", this._PackageObject.MinProgressForCompletion) + "</adlcp:completionThreshold>"; }

                                    manifestContent.AppendLine(adlcpCompletionThresholdNode);
                                }
                                else
                                { manifestContent.AppendLine(fileLine); }
                            }

                            // close the reader
                            reader.Close();
                        }

                        // delete the original file and write the new one
                        File.Delete(Server.MapPath(this._PackageObject.Path + "/imsmanifest.xml"));

                        using (StreamWriter writer = new StreamWriter(Server.MapPath(this._PackageObject.Path + "/imsmanifest.xml")))
                        {
                            writer.Write(manifestContent);
                            writer.Close();
                        }
                    }                    
                }
                
                // if we are reverting the manifest to the original manifest, set the manifest property to the original manifest and
                // delete the .orig file, if it exists
                if (this._RevertManifestToOriginal.Checked && File.Exists(Server.MapPath(this._PackageObject.Path + "/imsmanifest.orig.xml")))
                {
                    StringBuilder originalManifest = new StringBuilder();
                    string originalManifestLine = String.Empty;

                    // read the content from the .orig file
                    using (StreamReader reader = new StreamReader(Server.MapPath(this._PackageObject.Path + "/imsmanifest.orig.xml")))
                    {
                        while((originalManifestLine = reader.ReadLine()) != null)
                        {
                            originalManifest.AppendLine(originalManifestLine);
                        }

                        reader.Close();
                    }

                    // delete the .orig file
                    File.Delete(Server.MapPath(this._PackageObject.Path + "/imsmanifest.orig.xml"));

                    // write the contents to the imsmanifest file
                    using (StreamWriter writer = new StreamWriter(Server.MapPath(this._PackageObject.Path + "/imsmanifest.xml")))
                    {
                        writer.Write(originalManifest);
                        writer.Close();
                    }

                    this._PackageObject.Manifest = originalManifest.ToString();
                    this._ManifestText.Text = originalManifest.ToString();                    
                }
                else
                {
                    // if the orig manifest file does not exist and it has been edited, create one and copy the manifest contents to it
                    if (!File.Exists(Server.MapPath(this._PackageObject.Path + "/imsmanifest.orig.xml")) && this._ManifestText.Text != this._PackageObject.Manifest)
                    {
                        StringBuilder manifest = new StringBuilder();
                        String manifestLine = String.Empty;

                        // read the content from the manifest file
                        using (StreamReader reader = new StreamReader(Server.MapPath(this._PackageObject.Path + "/imsmanifest.xml")))
                        {
                            while ((manifestLine = reader.ReadLine()) != null)
                            {
                                manifest.AppendLine(manifestLine);
                            }

                            reader.Close();
                        }

                        using (StreamWriter writer = new StreamWriter(Server.MapPath(this._PackageObject.Path + "/imsmanifest.orig.xml")))
                        {
                            writer.Write(manifest.ToString());
                            writer.Close();
                        }
                    }

                    // write the contents to the imsmanifest file
                    using (StreamWriter writer = new StreamWriter(Server.MapPath(this._PackageObject.Path + "/imsmanifest.xml")))
                    {
                        writer.Write(this._ManifestText.Text);
                        writer.Close();
                    }

                    // set the manifest text (this is necessary in case a user updates the manifest)
                    this._PackageObject.Manifest = this._ManifestText.Text;
                }
                
                // save the content package
                int idContentPackage = this._PackageObject.Save();

                // rebuild the form controls
                this._BuildFormControls();

                // display the feedback
                this.DisplayFeedback(_GlobalResources.ThePackageHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CancelPackage_Command
        /// <summary>
        /// Handles Cancel Button Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CancelPackage_Command(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        #endregion

        #region _ImportPackage_Command
        /// <summary>
        /// Creates a course from the package, and uses manifest metadata for course information.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _ImportPackage_Command(object sender, EventArgs e)
        {
            try
            {                
                string title = String.Empty;
                string description = String.Empty;
                int durationInMinutes = 0;

                string metadataFileName = String.Empty;                

                if (this._PackageObject != null)
                {
                    // load mainfest file to a xml document
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(this._PackageObject.Manifest);

                    if ((ContentPackage.ContentPackageType)this._PackageObject.IdContentPackageType == ContentPackage.ContentPackageType.SCORM)
                    {                        
                        // if the package contains metadata, extract it from there, either via file or via the metadata node itself
                        if (xmlDoc.GetElementsByTagName("metadata") != null && xmlDoc.GetElementsByTagName("metadata").Count > 0)
                        {
                            XmlNode metadataNode = xmlDoc.GetElementsByTagName("metadata")[0];

                            // look for a metadata file
                            for (int i = 0; i < metadataNode.ChildNodes.Count; i++)
                            {
                                if (metadataNode.ChildNodes[i].Name.Contains("location"))
                                { metadataFileName = metadataNode.ChildNodes[i].InnerText; }
                            }

                            // if there is a metadata file, try to use it to extract title and location
                            if (!String.IsNullOrWhiteSpace(metadataFileName))
                            {                                
                                // get the metadata file path
                                string metadataFile = this._PackageObject.Path + "/" + metadataFileName;

                                // if we have a metadata file, try to extract from it
                                if (File.Exists(Server.MapPath(metadataFile)))
                                {
                                    XmlDocument xmlMetadata = new XmlDocument();
                                    xmlMetadata.Load(Server.MapPath(metadataFile));

                                    XmlNodeList metadataTitleNodes = xmlMetadata.GetElementsByTagName("title");
                                    XmlNodeList metadataDescriptionNodes = xmlMetadata.GetElementsByTagName("description");
                                    XmlNodeList metadataDurationNodes = xmlMetadata.GetElementsByTagName("duration");

                                    // title
                                    if (metadataTitleNodes != null && metadataTitleNodes.Count > 0)
                                    {
                                        title = metadataTitleNodes[0].InnerText;

                                        // if the title is still empty, look in sub-nodes
                                        if (String.IsNullOrWhiteSpace(title))
                                        {
                                            // look for "langstring" or "string" sub-nodes
                                            foreach (XmlNode titleChildNode in metadataTitleNodes[0].ChildNodes)
                                            {
                                                if (titleChildNode.Name.Contains("langstring") || titleChildNode.Name.Contains("string"))
                                                {
                                                    title = titleChildNode.InnerText;
                                                    break;
                                                }
                                            }                                            
                                        }                                                                                
                                    }

                                    // description
                                    if (metadataDescriptionNodes != null && metadataDescriptionNodes.Count > 0)
                                    {
                                        description = metadataDescriptionNodes[0].InnerText;

                                        // if the description is still empty, look in sub-nodes
                                        if (String.IsNullOrWhiteSpace(description))
                                        {                                            
                                            // look for "langstring" or "string" sub-nodes
                                            foreach (XmlNode descriptionChildNode in metadataDescriptionNodes[0].ChildNodes)
                                            {
                                                if (descriptionChildNode.Name.Contains("langstring") || descriptionChildNode.Name.Contains("string"))
                                                {
                                                    description = descriptionChildNode.InnerText;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    // duration - duration should only be found within lom metadata
                                    if (metadataDurationNodes != null && metadataDurationNodes.Count > 0)
                                    {
                                        // look for "datetime" sub-node                                        
                                        foreach (XmlNode durationChildNode in metadataDurationNodes[0].ChildNodes)
                                        {
                                            if (durationChildNode.Name.Contains("datetime"))
                                            {
                                                try
                                                {
                                                    TimeSpan durationTS = XmlConvert.ToTimeSpan(durationChildNode.InnerText);
                                                    durationInMinutes = (int)durationTS.TotalMinutes;
                                                }
                                                catch (Exception ex)
                                                { durationInMinutes = 0; }

                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            // if title or description are still empty here, look for them inside of the metadata nodes of the manifest
                            if (String.IsNullOrWhiteSpace(title) || String.IsNullOrWhiteSpace(description))
                            {
                                for (int i = 0; i < metadataNode.ChildNodes.Count; i++)
                                {                                    
                                    // title - if title is not already populated
                                    if (metadataNode.ChildNodes[i].Name.Contains("title") && String.IsNullOrWhiteSpace(title))
                                    {
                                        title = metadataNode.ChildNodes[i].InnerText;

                                        // if the title is still empty, look in sub-nodes
                                        if (String.IsNullOrWhiteSpace(title))
                                        {
                                            // look for "langstring" or "string" sub-nodes
                                            foreach (XmlNode titleChildNode in metadataNode.ChildNodes[i].ChildNodes)
                                            {
                                                if (titleChildNode.Name.Contains("langstring") || titleChildNode.Name.Contains("string"))
                                                {
                                                    title = titleChildNode.InnerText;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    // description - if description is not already populated
                                    if (metadataNode.ChildNodes[i].Name.Contains("description") && String.IsNullOrWhiteSpace(description))
                                    {
                                        description = metadataNode.ChildNodes[i].InnerText;

                                        // if the description is still empty, look in sub-nodes
                                        if (String.IsNullOrWhiteSpace(description))
                                        {
                                            // look for "langstring" or "string" sub-nodes
                                            foreach (XmlNode descriptionChildNode in metadataNode.ChildNodes[i].ChildNodes)
                                            {
                                                if (descriptionChildNode.Name.Contains("langstring") || descriptionChildNode.Name.Contains("string"))
                                                {
                                                    description = descriptionChildNode.InnerText;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    // lom - if either one is still not populated, look inside "lom" for the title and/or description
                                    if (metadataNode.ChildNodes[i].Name.Contains("lom") && (String.IsNullOrWhiteSpace(title) || String.IsNullOrWhiteSpace(description)))
                                    {
                                        // loop through sub nodes for "general" and "technical", that is where the information is
                                        for (int j = 0; j < metadataNode.ChildNodes[i].ChildNodes.Count; j++)
                                        {
                                            // title and description can be found under "general"
                                            if (metadataNode.ChildNodes[i].ChildNodes[j].Name.Contains("general"))
                                            {
                                                for (int k = 0; k < metadataNode.ChildNodes[i].ChildNodes[j].ChildNodes.Count; k++)
                                                {
                                                    // title - if title is not already populated
                                                    if (metadataNode.ChildNodes[i].ChildNodes[j].ChildNodes[k].Name.Contains("title") && String.IsNullOrWhiteSpace(title))
                                                    {
                                                        title = metadataNode.ChildNodes[i].ChildNodes[j].ChildNodes[k].InnerText;

                                                        // if the title is still empty, look in sub-nodes
                                                        if (String.IsNullOrWhiteSpace(title))
                                                        {
                                                            // look for "langstring" or "string" sub-nodes
                                                            foreach (XmlNode titleChildNode in metadataNode.ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes)
                                                            {
                                                                if (titleChildNode.Name.Contains("langstring") || titleChildNode.Name.Contains("string"))
                                                                {
                                                                    title = titleChildNode.InnerText;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }

                                                    // description - if description is not already populated
                                                    if (metadataNode.ChildNodes[i].ChildNodes[j].ChildNodes[k].Name.Contains("description") && String.IsNullOrWhiteSpace(description))
                                                    {
                                                        description = metadataNode.ChildNodes[i].ChildNodes[j].ChildNodes[k].InnerText;

                                                        // if the description is still empty, look in sub-nodes
                                                        if (String.IsNullOrWhiteSpace(description))
                                                        {
                                                            // look for "langstring" or "string" sub-nodes
                                                            foreach (XmlNode descriptionChildNode in metadataNode.ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes)
                                                            {
                                                                if (descriptionChildNode.Name.Contains("langstring") || descriptionChildNode.Name.Contains("string"))
                                                                {
                                                                    description = descriptionChildNode.InnerText;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            // duration can be found under "technical"
                                            if (metadataNode.ChildNodes[i].ChildNodes[j].Name.Contains("technical"))
                                            {
                                                for (int k = 0; k < metadataNode.ChildNodes[i].ChildNodes[j].ChildNodes.Count; k++)
                                                {
                                                    // duration - duration should only be found within lom metadata
                                                    if (metadataNode.ChildNodes[i].ChildNodes[j].ChildNodes[k].Name.Contains("duration") && durationInMinutes == 0)
                                                    {
                                                        // look for "datetime" sub-node
                                                        foreach (XmlNode durationChildNode in metadataNode.ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes)
                                                        {                                                                                                                        
                                                            if (durationChildNode.Name.Contains("datetime"))
                                                            {
                                                                try
                                                                {
                                                                    TimeSpan durationTS = XmlConvert.ToTimeSpan(durationChildNode.InnerText);
                                                                    durationInMinutes = (int)durationTS.TotalMinutes;
                                                                }
                                                                catch (Exception ex)
                                                                { durationInMinutes = 0; }

                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // if by this point we still don't have a title, we'll need to extract the title from the organization                        
                        if (String.IsNullOrWhiteSpace(title))
                        {
                            XmlNodeList organizationNodes = xmlDoc.GetElementsByTagName("organization");

                            if (organizationNodes != null && organizationNodes.Count > 0)
                            {
                                XmlNode organizationNode = organizationNodes[0];

                                for (int i = 0; i < organizationNode.ChildNodes.Count; i++)
                                {
                                    if (organizationNode.ChildNodes[i].Name.Contains("title"))
                                    { title = organizationNode.ChildNodes[i].InnerText; }                                    
                                }
                            }
                        }

                        // if after everything there is still not a description, make it the title
                        if (String.IsNullOrWhiteSpace(description))
                        { description = title; }
                    }
                    else if ((ContentPackage.ContentPackageType)this._PackageObject.IdContentPackageType == ContentPackage.ContentPackageType.xAPI)
                    {
                        // title should be embedded in the root
                        if (xmlDoc.GetElementsByTagName("title") != null && xmlDoc.GetElementsByTagName("title").Count > 0)
                        { title = xmlDoc.GetElementsByTagName("title")[0].InnerText; }

                        // description should be embedded in the root
                        if (xmlDoc.GetElementsByTagName("description") != null && xmlDoc.GetElementsByTagName("description").Count > 0)
                        { description = xmlDoc.GetElementsByTagName("description")[0].InnerText; }

                        // if description is empty, use title
                        if (String.IsNullOrWhiteSpace(description))
                        { description = title; }
                    }

                    // create course
                    Course course = new Course();

                    course.Title = title;
                    course.ShortDescription = description;
                    course.LongDescription = description;

                    if (durationInMinutes > 0)
                    { course.EstimatedLengthInMinutes = durationInMinutes; }

                    course.IsClosed = false;
                    course.IsLocked = false;
                    course.IsPublished = false;

                    int courseId = course.Save();

                    course = new Course(courseId);
                    course.SaveLang(AsentiaSessionState.GlobalSiteObject.LanguageString, title, description, description, null, null);

                    // create lesson
                    Lesson lesson = new Lesson();

                    lesson.IdCourse = courseId;
                    lesson.Title = title;
                    lesson.ShortDescription = description;

                    int lessonId = lesson.Save();

                    lesson = new Lesson(lessonId);
                    lesson.SaveLang(AsentiaSessionState.GlobalSiteObject.LanguageString, title, description, description, null);

                    // link content package to lesson
                    lesson = new Lesson(lessonId);
                    lesson.SaveContent(this._PackageObject.Id, Lesson.LessonContentType.ContentPackage, null, null, null, null);

                    // display feedback
                    this.DisplayFeedback(_GlobalResources.PackageHasBeenImportedSuccessfully, false);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.Error_sHaveOccurredWhileImportingThisPackage, true);
            }
        }
        #endregion

        #region _BuildContentPackageImportConfirmationModal
        /// <summary>
        ///builds content package import confirmation modal.
        /// </summary>
        private void _BuildContentPackageImportConfirmationModal()
        {
            this._ContentPackageImportConfirmationModal = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._ContentPackageImportConfirmationModal.Type = ModalPopupType.Confirm;
            this._ContentPackageImportConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE,
                                                                           ImageFiles.EXT_PNG);

            this._ContentPackageImportConfirmationModal.HeaderIconAlt = _GlobalResources.CreateCourseFromPackage;
            this._ContentPackageImportConfirmationModal.HeaderText = _GlobalResources.CreateCourseFromPackage;
            this._ContentPackageImportConfirmationModal.TargetControlID = this._ImportPackageLinkButton.ClientID;
            this._ContentPackageImportConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._ImportPackage_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ContentPackageImportConfirmationModalBody";
            body1.Text = _GlobalResources.AreYouSureYouWantToCreateCourseFromThisPackage;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._ContentPackageImportConfirmationModal.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._ContentPackageImportConfirmationModal);
        }
        #endregion
    }
}
