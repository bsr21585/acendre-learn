﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Services;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.LMS.Pages.Administrator.RulesEngine
{    
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel RulesEngineFormContentWrapperContainer;
        public Panel GridAnalyticPanel;

        public UpdatePanel RulesEngineGridUpdatePanel;
        public Grid RulesEngineGrid;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmAction;

        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            /*ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.UMS.Pages.Administrator.Users.Default.js");*/
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            /*if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserCreator)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserDeleter)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserEditor)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserImpersonator)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
            { Response.Redirect("/"); }*/

            // include page-specific css files
            this.IncludePageSpecificCssFile("GridAnalytic.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/users/Default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.RulesEngine));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.System, _GlobalResources.RulesEngine, ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.RulesEngineFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the grid, actions panel, and modals
            this._BuildObjectOptionsPanel();
            this._BuildGridAnalytics();
            this._BuildGrid();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.RulesEngineGrid.BindData();
            }
        }
        #endregion        

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGridAnalytics
        /// <summary>
        /// Builds the grid analytics for the page.
        /// </summary>
        private void _BuildGridAnalytics()
        {            
            // get the analytic data
            DataTable analyticData = GridAnalyticData.Rulesets();

            int total = Convert.ToInt32(analyticData.Rows[0]["total"]);
            int course = Convert.ToInt32(analyticData.Rows[0]["course"]);
            int learningpath = Convert.ToInt32(analyticData.Rows[0]["learningpath"]);
            int certification = Convert.ToInt32(analyticData.Rows[0]["certification"]);
            int group = Convert.ToInt32(analyticData.Rows[0]["group"]);
            int role = Convert.ToInt32(analyticData.Rows[0]["role"]);

            // build title for column 1
            GridAnalytic.DataBlock column1Title = new GridAnalytic.DataBlock(total.ToString("N0"), _GlobalResources.Rulesets);

            // build a DataBlock list for the column 1 data
            List<GridAnalytic.DataBlock> dataBlocksColumn1 = new List<GridAnalytic.DataBlock>();
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(course.ToString("N0"), _GlobalResources.Course.ToLower()));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(learningpath.ToString("N0"), _GlobalResources.LearningPath.ToLower()));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(certification.ToString("N0"), _GlobalResources.Certification.ToLower()));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(group.ToString("N0"), _GlobalResources.Group.ToLower()));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(role.ToString("N0"), _GlobalResources.Role.ToLower()));


            // build the GridAnalytic object
            GridAnalytic gridAnalyticObject = new GridAnalytic("UserStatistics", column1Title, dataBlocksColumn1);
            
            // attach the object to the container
            this.GridAnalyticPanel.Controls.Add(gridAnalyticObject);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // apply css class to container
            this.RulesEngineGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.RulesEngineGrid.StoredProcedure = "[RulesEngine.GetGrid]";
            this.RulesEngineGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.RulesEngineGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.RulesEngineGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.RulesEngineGrid.IdentifierField = "idRuleset";
            this.RulesEngineGrid.DefaultSortColumn = "displayName";
            //this.RulesEngineGrid.SearchBoxPlaceholderText = _GlobalResources.SearchUsers;

            // data key names
            this.RulesEngineGrid.DataKeyNames = new string[] { "idRuleset" };

            // columns
            GridColumn ruleSet = new GridColumn(_GlobalResources.Rulesets + ", " + _GlobalResources.Object, null, "label"); // this is calculated dynamically in the RowDataBound method

            GridColumn fields = new GridColumn(_GlobalResources.Field, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.RulesEngineGrid.AddColumn(ruleSet);
            this.RulesEngineGrid.AddColumn(fields);
            
            // add row data bound event
            this.RulesEngineGrid.RowDataBound += new GridViewRowEventHandler(this._RulesEngineGrid_RowDataBound);
        }
        #endregion

        #region _RulesEngineGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RulesEngineGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idRuleSet = Convert.ToInt32(rowView["idRuleSet"]);
                int idObject = Convert.ToInt32(rowView["idObject"]);
                string objectName = rowView["objectName"].ToString();
                int idRulesetEnrollment;

                // AVATAR, NAME, OBJECT, FIELDS

                string avatar = rowView["avatar"].ToString();
                string label = Server.HtmlEncode(rowView["label"].ToString());
                string rulesetObject = Server.HtmlEncode(rowView["object"].ToString());
                string fields = rowView["fields"].ToString();

                // avatar
                string avatarImagePath;
                
                if (rulesetObject == "group")
                {
                    avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG);
                }
                else if (rulesetObject == "learningpath")
                {
                    avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
                }
                else if (rulesetObject == "role")
                {
                    avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG);
                }
                else if (rulesetObject == "certification")
                {
                    avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
                }
                else
                {
                    avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
                }
                string avatarImageClass = "GridAvatarImage";

                if (!String.IsNullOrWhiteSpace(avatar))
                {
                    if (rulesetObject == "course") // course avatar
                    {
                        avatarImagePath = SitePathConstants.SITE_COURSES_ROOT + idObject.ToString() + "/" + avatar;
                    }
                    else if (rulesetObject == "learningpath") // learning path avatar
                    {
                        avatarImagePath = SitePathConstants.SITE_LEARNINGPATHS_ROOT + idObject.ToString() + "/" + avatar;
                    }
                    else if (rulesetObject == "group") // group avatar
                    {
                        avatarImagePath = SitePathConstants.SITE_GROUPS_ROOT + idObject.ToString() + "/" + avatar;
                    }
                    avatarImageClass += " AvatarImage";
                }

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = label;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name - check permissions for modify link
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);
                  
                HyperLink nameLink = new HyperLink();
                
                nameLink.Text = label;
                nameLabel.Controls.Add(nameLink);
                
                if (rulesetObject == "course")
                {
                    idRulesetEnrollment = Convert.ToInt32(rowView["idRulesetEnrollment"]);
                    nameLink.NavigateUrl = @"/administrator/courses/rulesetenrollments/Modify.aspx?cid=" + idObject.ToString() + @"&id=" + idRulesetEnrollment + "&rsid=" + idRuleSet.ToString();
                }
                else if (rulesetObject == "group")
                {
                    nameLink.NavigateUrl = @"/administrator/groups/AutoJoinRules.aspx?gid=" + idObject.ToString() + @"&action=ModifyRuleset&rsid=" + idRuleSet.ToString();
                }
                else if (rulesetObject == "role")
                {
                    nameLink.NavigateUrl = @"/administrator/roles/Modify.aspx?id=" + idObject.ToString() + @"&rsid=" + idRuleSet.ToString();
                }
                else if (rulesetObject == "certification")
                {
                    nameLink.NavigateUrl = @"/administrator/certifications/AutoJoinRules.aspx?cid=" + idObject.ToString() + @"&action=ModifyRuleset&rsid=" + idRuleSet.ToString();
                }
                else if (rulesetObject == "learningpath")
                {
                    idRulesetEnrollment = Convert.ToInt32(rowView["idRulesetEnrollment"]);
                    nameLink.NavigateUrl = @"/administrator/learningpaths/rulesetenrollments/Modify.aspx?lpid=" + idObject.ToString() + @"&id=" + idRulesetEnrollment.ToString() + @"&rsid=" + idRuleSet.ToString();
                }
               

                // object
                Label objectLabel = new Label();
                objectLabel.CssClass = "GridSecondaryLine";
                objectLabel.Text = rulesetObject + " - " + objectName;
                e.Row.Cells[1].Controls.Add(objectLabel);

                // fields
                Label fieldsLabel = new Label();
                fieldsLabel.CssClass = "GridBaseTitle";
                if (fields.Length < 2)
                {
                    fieldsLabel.Text = fields;
                }
                else
                {
                    fieldsLabel.Text = fields.Remove(fields.Length - 2, 2);
                }
                e.Row.Cells[2].Controls.Add(fieldsLabel);
                


            }
        }
        #endregion


    }
}
