﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;
using Asentia.UMS.Controls;

namespace Asentia.LMS.Pages.Administrator.CertificateImport
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CertificateImportFormContentWrapperContainer;
        public Panel ObjectOptionsPanel;
        public Panel UserObjectMenuContainer;
        public Panel CertificateImportPageWrapperContainer;
        public Panel PanelMsgTable = new Panel();
        public Panel TabContainer, TabContentWrapperPanel;
        public UpdatePanel CertificateImportUpdatePanel, CertificateImportGridUpdatePanel;
        public Panel TabUploadContentPanel, TabManageContentPanel;
        public Panel ErrorTablePanel, ManageCertificateFeedbackContainer;
        public Panel ManagementInstructionPanel;

        public Grid CertificateImportGrid;
        public LinkButton DeleteButton = new LinkButton();
        public Panel ActionsPanel;
        public ModalPopup GridConfirmAction = new ModalPopup();

        /// <summary>
        /// Collection of columns for Certificate import table
        /// These are the system imposed constraints on each field.
        /// </summary>
        public List<Library.CertificateImport> CertificateImportColumns;
        #endregion

        #region Private Properties
        private User _UserObject;

        private int _ErrorNum = 0;
        private int _RowNum = 0;
        private const int _MAX_RECORDS_FOR_IMPORT = 10000;
        private string _ErrorList = String.Empty;
        private string _UsernameImported = String.Empty;

        private Panel _UploadTotalPanel = new Panel();
        private Panel _UploadDetailedInstruction = new Panel();
        private LinkButton _UploadCertificateButton;

        private Table _CertificateColumnsInfoTable = new Table();
        private Table _CertificateImportTable = new Table();

        private Button _PreviousErrorButton = new Button();
        private Button _NextErrorButton = new Button();

        private TableHeaderRow _CertificateDataTableHeader = new TableHeaderRow();
        private List<string> _UsernameNotExistList = new List<string>();
        private List<string> _CodeNotExistList = new List<string>();
        private UploaderAsync _CertificateUploader;
        private ModalPopup _CertificateFileModal;
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            StringBuilder globalJs = new StringBuilder();
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.CertificateImport.Default.js");
            globalJs.AppendLine("     Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler); ");
            globalJs.AppendLine("         function EndRequestHandler(sender, args){ ");
            globalJs.AppendLine("         if (args.get_error() != undefined){ ");
            globalJs.AppendLine("          var url = window.location.href; ");
            globalJs.AppendLine("           if (url.indexOf('?') > -1){  ");
            globalJs.AppendLine("              url += '&timeout=1'  ");
            globalJs.AppendLine("              }else{  ");
            globalJs.AppendLine("               url += '?timeout=1'  ");
            globalJs.AppendLine("              }  ");
            globalJs.AppendLine("           window.location.href = url;  ");
            globalJs.AppendLine("             } ");
            globalJs.AppendLine("         } ");

            // add start up script
            csm.RegisterStartupScript(typeof(Default), "GlobalJs", globalJs.ToString(), true);

        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // set timeout for the certificate import
            Server.ScriptTimeout = 600;

            // get the usernameImported if there is an assigned userID
            this._UsernameImported = _GetUserNameImported();

            // check permissions
            if (
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_CertificateImport)
                && (!String.IsNullOrWhiteSpace(this._UsernameImported) && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
                )
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/certificateimport/Default.css");            

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.CertificateDataImport));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, _GlobalResources.CertificateDataImport, ImageFiles.GetIconPath(ImageFiles.ICON_IMPORTCERTIFICATEDATA, ImageFiles.EXT_PNG));

            // build the object options panel
            this._BuildObjectOptionsPanel();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CertificateImportFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CertificateImportPageWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the user object menu container
            if (this._UserObject != null)
            {
                UserObjectMenu userObjectMenu = new UserObjectMenu(this._UserObject);
                userObjectMenu.SelectedItem = UserObjectMenu.MenuObjectItem.ImportCertificateData;

                this.UserObjectMenuContainer.Controls.Add(userObjectMenu);
            }

            // load Certificate table column info
            this.CertificateImportColumns = Library.CertificateImport.GetCertificateImportColumnsInfo();

            // load tab content
            if (String.IsNullOrWhiteSpace(this._UsernameImported))
            { this._BuildTabsList(); }

            // load the page controls
            this._LoadPageControls();   
            
            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.CertificateImportGrid.BindData();
            }
        }
        #endregion

        #region _LoadPageControls
        /// <summary>
        /// Setup the Tags/Controls on the page
        /// </summary>
        private void _LoadPageControls()
        {
            this.TabContentWrapperPanel.ID = this.ID + "_" + "TabContentWrapperPanel";
            this.TabContentWrapperPanel.CssClass = "TabPanelsContentContainer";
            this._BuildCertificateTableColumn();

            // build the table
            this._BuildCertificateContent();
            this._BuildCertificateFileUploadModal();
        }
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for the form.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Upload", _GlobalResources.UploadCertificateData));
            tabs.Enqueue(new KeyValuePair<string, string>("Manage", _GlobalResources.ManageCertificateData));

            this.TabContainer.Controls.Add(AsentiaPage.BuildTabListPanel("CertificateData", tabs));
        }
        #endregion

        #region _GetUserImported
        /// <summary>
        /// Gets Object User Imported
        /// </summary>
        private string _GetUserNameImported()
        {
            string usernameImported = String.Empty;
            User userImported;
            // get the id querystring parameter
            int qsId = this.QueryStringInt("uid", 0);
            int vsId = this.ViewStateInt(this.ViewState, "uid", 0);
            int id = 0;

            if (qsId > 0 || vsId > 0)
            {
                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }
            }
            if (id > 0)
            {
                this._UserObject = new User(id);
                userImported = new User(id);
                usernameImported = userImported.Username;
            }

            return usernameImported;
        }
        #endregion

        #region _BuildCertificateContent
        /// <summary>
        /// Builds the tab content container for specific purpose of each tab
        /// </summary>
        private void _BuildCertificateContent()
        {
            this.CertificateImportUpdatePanel.ID = "CertificateData_" + "Upload" + "_TabPanel";
            this.CertificateImportUpdatePanel.Attributes.Add("style", "display: block;");
            _BuildUploadTabContent(this.CertificateImportUpdatePanel.ContentTemplateContainer);

            this.CertificateImportGridUpdatePanel.ID = "CertificateData_" + "Manage" + "_TabPanel";
            this.CertificateImportGridUpdatePanel.Attributes.Add("style", "display: none;");
            _BuildManageTabContent();
        }
        #endregion
                
        #region _BuildUploadTabContent
        /// <summary>
        /// Builds the tab content container for Upload tab
        /// </summary>
        private void _BuildUploadTabContent(Control UploadControls)
        {
            _BuildUploadPanel(UploadControls);
            _BuildCertificateColumnsTable();
        }
        #endregion

        #region _BuildManageTabContent
        /// <summary>
        /// Builds the tab content container for Manage tab
        /// </summary>
        private void _BuildManageTabContent()
        {
            ManagementInstructionPanel.CssClass = "ObjectOptionsPanel";
            this.FormatPageInformationPanel(ManagementInstructionPanel, _GlobalResources.AllCertificateDataImportsAreListedInTheTableBelow, true);

            this.CertificateImportGridUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the Manage Certificate Tab.
        /// </summary>
        private void _BuildGrid()
        {
            this.CertificateImportGrid.StoredProcedure = Library.CertificateImport.GridProcedure;
            this.CertificateImportGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.CertificateImportGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.CertificateImportGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.CertificateImportGrid.IdentifierField = "idCertificateImport";
            this.CertificateImportGrid.DefaultSortColumn = "importFileName";
            this.CertificateImportGrid.ShowTimeInDateStrings = true;
            this.CertificateImportGrid.ShowSearchBox = false;
            this.CertificateImportGrid.ShowRecordsPerPageSelectbox = false;
            this.CertificateImportGrid.PagerSettings.Position = PagerPosition.Bottom;
            this.CertificateImportGrid.ID = "CertificateImportGrid";

            // data key names
            this.CertificateImportGrid.DataKeyNames = new string[] { "idCertificateImport", "importFileName" };

            // columns
            GridColumn fileName = new GridColumn(_GlobalResources.ImportFileName, "importFileName", "importFileName");
            GridColumn importTime = new GridColumn(_GlobalResources.ImportDateTime, "timestamp", "timestamp");
            GridColumn performedBy = new GridColumn(_GlobalResources.ImportPerformedBy, "performedBy", "performedBy");
            GridColumn performedFor = new GridColumn(_GlobalResources.ImportPerformedFor, "performedFor", "performedFor");  

            // add columns to data grid
            this.CertificateImportGrid.AddColumn(fileName);
            this.CertificateImportGrid.AddColumn(importTime);
            this.CertificateImportGrid.AddColumn(performedBy);
            this.CertificateImportGrid.AddColumn(performedFor);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedCertificate_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this.DeleteButton);
        }
        #endregion

        #region _BuildCertificateColumnsTable
        /// <summary>
        /// Builds the CertificateColumnsTable for the page.
        /// </summary>
        private void _BuildCertificateColumnsTable()
        {
            TableHeaderRow headerRow = new TableHeaderRow();
            headerRow.CssClass = "GridHeaderRow";
            TableHeaderCell columnOrder = new TableHeaderCell();
            TableHeaderCell dataHeader = new TableHeaderCell();
            TableHeaderCell requiredHeader = new TableHeaderCell();
            TableHeaderCell typeHeader = new TableHeaderCell();
            TableHeaderCell lengthHeader = new TableHeaderCell();
            TableHeaderCell formatHeader = new TableHeaderCell();

            int columnNum = 0;
            columnOrder.Text = _GlobalResources.Column;
            dataHeader.Text = _GlobalResources.Data;
            requiredHeader.Text = _GlobalResources.Required;
            typeHeader.Text = _GlobalResources.Type;
            lengthHeader.Text = _GlobalResources.Length;
            formatHeader.Text = _GlobalResources.Format;

            headerRow.Controls.Add(columnOrder);
            headerRow.Controls.Add(dataHeader);
            headerRow.Controls.Add(requiredHeader);
            headerRow.Controls.Add(typeHeader);
            headerRow.Controls.Add(lengthHeader);
            headerRow.Controls.Add(formatHeader);

            this._CertificateColumnsInfoTable.CssClass = "GridTable";
            this._CertificateColumnsInfoTable.Controls.Add(headerRow);

            foreach (Library.CertificateImport CertificateColumn in this.CertificateImportColumns)
            {
                columnNum++;
                string reference = String.Empty;
                
                string columnOrderText = "Column " + ((columnNum > 26) ? ((Char)(64 + (columnNum - 1) / 26)).ToString() : String.Empty) + ((Char)(64 + columnNum % 26)).ToString();
                TableRow certificateColumnTableRow = new TableRow();
                certificateColumnTableRow.CssClass = ((columnNum % 2) == 0) ? "GridDataRow GridDataRowAlternate" : "GridDataRow";

                // Column Order
                TableCell columnTableCell = new TableCell();
                columnTableCell.Text = columnOrderText.Replace('@', 'Z');
                certificateColumnTableRow.Controls.Add(columnTableCell);

                // Column Identifier
                TableCell labelTableCell = new TableCell();
                labelTableCell.Text = CertificateColumn.ColumnName;
                labelTableCell.CssClass = "ColumnDataName";
                certificateColumnTableRow.Controls.Add(labelTableCell);

                // Column Always Required
                TableCell requiredTableCell = new TableCell();
                requiredTableCell.Text = ((CertificateColumn.AlwaysRequired) ? _GlobalResources.Yes : _GlobalResources.No) + reference;
                certificateColumnTableRow.Controls.Add(requiredTableCell);

                if (CertificateColumn.Identifier != _GlobalResources.ROWEND_UPPER)
                {
                    // Field Type
                    TableCell typeTableCell = new TableCell();
                    switch (CertificateColumn.DataType)
                    {
                        case (UserAccountData.DataType.String):
                            typeTableCell.Text = _GlobalResources.Text;
                            break;
                        case (UserAccountData.DataType.Date):
                            typeTableCell.Text = _GlobalResources.DateTime;
                            break;
                        case (UserAccountData.DataType.Boolean):
                            typeTableCell.Text = _GlobalResources.Boolean;
                            break;
                        case (UserAccountData.DataType.Integer):
                            typeTableCell.Text = _GlobalResources.Integer;
                            break;
                        default:
                            typeTableCell.Text = _GlobalResources.Text;
                            break;
                    }
                    certificateColumnTableRow.Controls.Add(typeTableCell);

                    // Field Length
                    TableCell lengthTableCell = new TableCell();
                    lengthTableCell.Text = (CertificateColumn.DataType == UserAccountData.DataType.String && CertificateColumn.MaxLength > 0) ? CertificateColumn.MaxLength.ToString() : " -";
                    certificateColumnTableRow.Controls.Add(lengthTableCell);

                    // Field Format
                    TableCell formatTableCell = new TableCell();
                    formatTableCell.Text = CertificateColumn.Format;
                    certificateColumnTableRow.Controls.Add(formatTableCell);
                }
                else
                {
                    // Field Format specified for Row End
                    TableCell formatTableCell = new TableCell();
                    formatTableCell.Text = CertificateColumn.Format;
                    formatTableCell.ColumnSpan = 3;
                    certificateColumnTableRow.Controls.Add(formatTableCell);
                }
                this._CertificateColumnsInfoTable.Controls.Add(certificateColumnTableRow);

            }
                 this._UploadTotalPanel.Controls.Add(this._CertificateColumnsInfoTable);

                 this.CertificateImportUpdatePanel.ContentTemplateContainer.Controls.Add(this._UploadTotalPanel);
        }
        #endregion

        #region _BuildCertificateTableColumn
        /// <summary>
        /// Builds datatable columns for 
        /// </summary>
        private void _BuildCertificateTableColumn()
        {
           
            int columnNum = 0;

            TableHeaderCell cellColumnRowHeader = new TableHeaderCell();
            _CertificateDataTableHeader.Controls.Add(cellColumnRowHeader);

            foreach (Library.CertificateImport CertificateColumn in this.CertificateImportColumns)
            {
                    columnNum++;
                    string columnOrderText = ((columnNum > 26) ? ((Char)(64 + (columnNum - 1) / 26)).ToString() : String.Empty) + ((Char)(64 + columnNum % 26)).ToString();

                    //Create header row for the _CertificateDataTableHeader
                    TableHeaderCell cellColumnHeader = new TableHeaderCell();
                    cellColumnHeader.Text = columnOrderText.Replace('@', 'Z'); ;
                    _CertificateDataTableHeader.Controls.Add(cellColumnHeader);
            }

             this._CertificateImportTable.CssClass = "ExcelTableFormat";
             this._CertificateImportTable.Controls.Add(_CertificateDataTableHeader);
        }

        #endregion

        #region _BuildUploadPanel
        /// <summary>
        /// Builds upload icon for the Certificate batch file
        /// </summary>
        private void _BuildUploadPanel(Control uploadControls)
        {
            Panel certificateFileUploadInstructionsPanel = new Panel();
            this.FormatPageInformationPanel(certificateFileUploadInstructionsPanel, _GlobalResources.UseTheTableBelowAsReferenceForCreatingTabDelimitedCertificateFile, true);
            uploadControls.Controls.Add(certificateFileUploadInstructionsPanel);

            #region Detail Instruction Panel

            //Detail Instruction Panel
            _UploadDetailedInstruction.ID = "CertificateImportExpandableParagraphContainer";
            _UploadDetailedInstruction.CssClass = "ExpandableParagraphContainer";

            Panel sectionTitlePanel1 = new Panel();
            HtmlGenericControl sectionTitle1 = new HtmlGenericControl("h2");
            sectionTitle1.InnerText = _GlobalResources.Important + ":";
            sectionTitlePanel1.Controls.Add(sectionTitle1);
            _UploadDetailedInstruction.Controls.Add(sectionTitlePanel1);

            Panel sectionDetail1 = new Panel();
            this.FormatFormInformationPanel(sectionDetail1, _GlobalResources.UsernameColumnCommentForCertificateImport, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            Panel sectionDetail2 = new Panel();
            this.FormatFormInformationPanel(sectionDetail2, _GlobalResources.YourDataWillBeValidatedAgainstAllTheDataTypes, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            Panel sectionDetail3 = new Panel();
            this.FormatFormInformationPanel(sectionDetail3, _GlobalResources.AMaximumOf10000RecordsCertificate, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            _UploadDetailedInstruction.Controls.Add(sectionDetail1);
            _UploadDetailedInstruction.Controls.Add(sectionDetail2);
            _UploadDetailedInstruction.Controls.Add(sectionDetail3);

            Panel sectionTitlePanel2 = new Panel();
            HtmlGenericControl sectionTitle2 = new HtmlGenericControl("h2");
            sectionTitle2.InnerText = _GlobalResources.ColumnDetails + ":";
            sectionTitlePanel2.Controls.Add(sectionTitle2);
            _UploadDetailedInstruction.Controls.Add(sectionTitlePanel2);

            Panel sectionDetailPanel2 = new Panel();
            HtmlGenericControl ul2 = new HtmlGenericControl("ul");
            HtmlGenericControl li12 = new HtmlGenericControl("li");
            li12.InnerHtml = "<span class=\"bold\">" + _GlobalResources.Required + "</span> - " + _GlobalResources.YESIndicatesThatAllRowsMustContainData;
            ul2.Controls.Add(li12);

            HtmlGenericControl li22 = new HtmlGenericControl("li");
            li22.InnerHtml = "<span class=\"bold\">" + _GlobalResources.Length + "</span> - " + _GlobalResources.TheMaximumNumberOfCharacters;
            ul2.Controls.Add(li22);

            HtmlGenericControl li33 = new HtmlGenericControl("li");
            li33.InnerHtml = "<span class=\"bold\">" + _GlobalResources.Format + "</span> - " + _GlobalResources.DisplaysFormattingInformationForTheColumn;
            ul2.Controls.Add(li33);
            sectionDetailPanel2.Controls.Add(ul2);
            _UploadDetailedInstruction.Controls.Add(sectionDetailPanel2);

            Panel sectionTitlePanel4 = new Panel();
            HtmlGenericControl sectionTitle4 = new HtmlGenericControl("h2");
            sectionTitle4.InnerText = _GlobalResources.ImportantFormattingNotes + ":";
            sectionTitlePanel4.Controls.Add(sectionTitle4);
            _UploadDetailedInstruction.Controls.Add(sectionTitlePanel4);

            Panel sectionDetail4 = new Panel();
            this.FormatFormInformationPanel(sectionDetail4, _GlobalResources.NoteTheFollowingBeforeCreatingYourDataFile, false, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));

            HtmlGenericControl ul = new HtmlGenericControl("ul");
            HtmlGenericControl li1 = new HtmlGenericControl("li");
            li1.InnerHtml = _GlobalResources.UploadedFileMustBeATabDelimitedTextFileWithExtensionTXT;
            ul.Controls.Add(li1);

            HtmlGenericControl li2 = new HtmlGenericControl("li");
            li2.InnerHtml = _GlobalResources.IncludeAllDataColumns;
            ul.Controls.Add(li2);

            HtmlGenericControl li3 = new HtmlGenericControl("li");
            li3.InnerHtml = _GlobalResources.AllRequiredFieldsMust;
            ul.Controls.Add(li3);

            HtmlGenericControl li4 = new HtmlGenericControl("li");
            li4.InnerHtml = _GlobalResources.BeSureThatNoExtraEmptyLines;
            ul.Controls.Add(li4);

            HtmlGenericControl li5 = new HtmlGenericControl("li");
            li5.InnerHtml = _GlobalResources.DoNotIncludeColumnHeaders;
            ul.Controls.Add(li5);

            HtmlGenericControl li6 = new HtmlGenericControl("li");
            li6.InnerHtml = _GlobalResources.DoNotEncloseCellValuesInQuotations;
            ul.Controls.Add(li6);

            HtmlGenericControl li7 = new HtmlGenericControl("li");
            li7.InnerHtml = _GlobalResources.DoNotIncludeTabsInCellValues;
            ul.Controls.Add(li7);

            sectionDetail4.Controls.Add(ul);
            _UploadDetailedInstruction.Controls.Add(sectionDetail4);

            uploadControls.Controls.Add(_UploadDetailedInstruction);

            #endregion
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {

            //Upload Image Panel
            ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";
            if (Session["Imported"] != null)
            {
                if (Session["Imported"].ToString() == "Success")
                    this.DisplayFeedback(_GlobalResources.CertificateDataHasBeenImportedSuccessfully, false);
                else if (Session["Imported"].ToString() == "NotMatched")
                    this.DisplayFeedback(_GlobalResources.TheNumberOfColumnsDoesNotMatchPleaseTryAgain, true);
                else
                    this.DisplayFeedback(Session["Imported"].ToString(), true);
                Session.Remove("Imported");
            }

            // UPLOAD CERTIFICATE IMPORT
            this._UploadCertificateButton = new LinkButton();
            this._UploadCertificateButton.ID = "CertificateUploadButton";

            ObjectOptionsPanel.Controls.Add(
                this.BuildOptionsPanelImageLink("UploadCertificateImportLink",
                                                this._UploadCertificateButton,
                                                null,
                                                "$('#PageFeedbackContainer').hide();",
                                                _GlobalResources.UploadCertificateDataFile,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_UPLOAD, ImageFiles.EXT_PNG)) // ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG)
                );
            string qsTimeOut = this.QueryStringString("timeout", String.Empty);

            if (!String.IsNullOrWhiteSpace(qsTimeOut))
                this.DisplayFeedback(_GlobalResources.ThePageHasReachedItsExecutionTimeoutLimitBecauseTheFileYouAreUploadingContainsTooManyRecords, true);
        }
        #endregion

        #region _BuildCertificateFileUploadModal
        /// <summary>
        /// Pop up window for Certificate to upload batch file
        /// </summary>
        private void _BuildCertificateFileUploadModal()
        {
            this._CertificateFileModal = new ModalPopup("CertificateFileModal");
            this._CertificateFileModal.Type = ModalPopupType.Form;
            this._CertificateFileModal.HeaderText = _GlobalResources.UploadCertificateDataFile;
            this._CertificateFileModal.TargetControlID = this._UploadCertificateButton.ID;

            this._CertificateFileModal.SubmitButtonCustomText = _GlobalResources.Submit;
            this._CertificateFileModal.SubmitButton.ID = "CertificateFileModalSubmitButton";
            this._CertificateFileModal.SubmitButton.OnClientClick = "javascript:  document.getElementById('CertificateFileModalModalPopupHeader').innerHTML = '" + _GlobalResources.ProcessingCertificateDataFile + "';";
            this._CertificateFileModal.SubmitButton.Command += new CommandEventHandler(this._CertificateUploadButton_Command);

            // build uploader controls
            List<Control> certificateFileInputControls = new List<Control>();

            this._CertificateUploader = new UploaderAsync("CertificateUploader", UploadType.UserCertificateData, "CertificateUploader_ErrorContainer");

            // add controls to body
            certificateFileInputControls.Add(this._CertificateUploader);

            this._CertificateFileModal.AddControlToBody(AsentiaPage.BuildMultipleInputControlFormField("CertificateUploader",
                                                                 _GlobalResources.File,
                                                                 certificateFileInputControls,
                                                                 false,
                                                                 true));

            this.PageContentContainer.Controls.Add(this._CertificateFileModal);
        }
        #endregion

        #region _CertificateUploadButton_Command
        /// <summary>
        /// Handles the event initiated by the Save Changes Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CertificateUploadButton_Command(object sender, CommandEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(this._CertificateUploader.SavedFilePath) && File.Exists(Server.MapPath(this._CertificateUploader.SavedFilePath)))
            {
                bool columnMatched = true;

                List<string> lines = File.ReadAllLines(Server.MapPath(this._CertificateUploader.SavedFilePath), Encoding.Default).ToList();

                if (lines.Count > _MAX_RECORDS_FOR_IMPORT)
                {
                    this.DisplayFeedback(_GlobalResources.YourCertificateDataImportFileContainsXrecordsWhichExceedsTheaximumNumberOfLRecordsAllowedPleaseBreakYourCertificateDataImportIntoMultipleFiles.Replace("##X##", lines.Count.ToString()).Replace("##L##", _MAX_RECORDS_FOR_IMPORT.ToString()), true);
                }
                else
                {
                    try
                    {
                        this.PanelMsgTable.ID = "PanelErrorMsgTable";
                        this.PanelMsgTable.CssClass = "ExcelTableWrapper";
                        this.PanelMsgTable.ScrollBars = ScrollBars.Auto;

                        DataTable certificateDataTable = new DataTable();
                        DataTable usernameDT = new DataTable();
                        usernameDT.Columns.Add("username", typeof(string));
                        DataTable codeDT = new DataTable();
                        codeDT.Columns.Add("code", typeof(string));
                        foreach (string line in lines)
                        {
                            DataRow usernameRow = usernameDT.NewRow();
                            usernameRow[0] = line.Replace(Environment.NewLine, "").Split('\t')[0];
                            usernameDT.Rows.Add(usernameRow);

                            DataRow codeRow = codeDT.NewRow();
                            codeRow[0] = line.Replace(Environment.NewLine, "").Split('\t')[1];
                            codeDT.Rows.Add(codeRow);
                        }

                        this._UsernameNotExistList = Asentia.UMS.Library.User.UserListNotExistForCurrentSite(usernameDT).AsEnumerable().Select(u => u.Field<string>("username")).ToList();

                        string[] codeListExist = Library.CertificateImport.CodeListExistForCurrentSite();

                        //build datatable columns
                        foreach (Library.CertificateImport CertificateColumn in this.CertificateImportColumns)
                        {
                            DataColumn dcCertificate;

                            Type dcType;
                            switch (CertificateColumn.DataType.ToString())
                            {
                                case "String":
                                    dcType = typeof(string);
                                    break;
                                case "Integer":
                                    dcType = typeof(int);
                                    break;
                                case "Boolean":
                                    dcType = typeof(bool);
                                    break;
                                case "Date":
                                    dcType = typeof(DateTime);
                                    break;
                                default:
                                    dcType = typeof(string);
                                    break;
                            }
                            dcCertificate = new DataColumn(CertificateColumn.ColumnName, dcType);
                            certificateDataTable.Columns.Add(dcCertificate);
                        }

                        int lineNum = 0;
                        bool lineBreakFlag = false;
                        bool totalLineBreakFlag = false;
                        string lineBreakError = string.Empty;

                        //read file line by line for line break checking
                        foreach (string line in lines)
                        {
                            lineNum++;
                            string firstItemWithValue = string.Empty;

                            string[] docColumns = line.Replace(Environment.NewLine, "").Split('\t');

                            for (int i = 0; i < docColumns.Length; i++)
                            {
                                if (!String.IsNullOrEmpty(docColumns[i].Trim()) && docColumns[i].Trim() != "\"")
                                {
                                    firstItemWithValue = docColumns[i];
                                    break;
                                }
                            }

                            if (lineBreakFlag)
                            {
                                lineBreakError += "\"" + firstItemWithValue + "\"";
                                lineBreakFlag = false;
                            }
                            else if (docColumns.Length != certificateDataTable.Columns.Count)
                            {
                                if (lineNum == 1)
                                {
                                    totalLineBreakFlag = true;
                                    columnMatched = false;
                                    Session["Imported"] = _GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain;
                                    Response.Redirect(Request.RawUrl);
                                }

                                if (docColumns[docColumns.Length - 1].ToUpper() != _GlobalResources.END_UPPER)
                                {
                                    lineBreakError += "<br>" + _GlobalResources.AtLine + " " + lineNum.ToString() + ":" + _GlobalResources.ThereAreLineBreakOrNewLineCharactersInYourDataBetween + " \"" + (string.IsNullOrEmpty(docColumns[docColumns.Length - 1]) ? docColumns[docColumns.Length - 2] : docColumns[docColumns.Length - 1]) + "\" " + _GlobalResources.and_lower + " ";
                                    lineBreakFlag = true;
                                    totalLineBreakFlag = true;
                                }
                                else
                                {
                                    lineBreakError += "<br>" + _GlobalResources.AtLine + lineNum.ToString() + ": " + _GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(lineBreakError))
                        {
                            if (totalLineBreakFlag)
                            {
                                Session["Imported"] = _GlobalResources.ThereAreErrorsInTheFollowingPlaces + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain + ":" + lineBreakError;
                            }
                            else
                            {                                
                                Session["Imported"] = _GlobalResources.TheNumberOfColumnsInYourDataFileDoesNotMatchTheNumberOfColumnsExpectedByTheBatchTemplate + " " + _GlobalResources.PleaseCheckYourUploadFileAndTryAgain;
                            }
                            Response.Redirect(Request.RawUrl);
                        }

                        //read file line by line
                        foreach (string line in lines)
                        {
                            bool differentAwardDate = false;
                            string[] columns = line.Replace(Environment.NewLine, "").Split('\t');

                                DataRow certificateRow = certificateDataTable.NewRow();
                                //#datatype conversion
                                for (int i = 0; i < columns.Length; i++)
                                {
                                    if (String.IsNullOrWhiteSpace(columns[i]) || columns[i].ToLower() == "null")
                                        certificateRow[i] = DBNull.Value;
                                    else if (certificateDataTable.Columns[i].DataType == typeof(DateTime))
                                        certificateRow[i] = Convert.ToDateTime(columns[i]);
                                    else
                                        certificateRow[i] = columns[i];
                                }

                                certificateDataTable.Rows.Add(certificateRow);

                                //make sure only one certificate code/award date combo exists for each user
                                string selectString = string.Format("([{0}]='{1}')", certificateDataTable.Columns[0].ColumnName, columns[0]);
                                selectString += string.Format(" AND ([{0}]='{1}')", certificateDataTable.Columns[1].ColumnName, columns[1]);
                                differentAwardDate = (certificateDataTable.Select(selectString).Count() > 1) ? true : false;

                            this._ColumnValueValidation(columns, differentAwardDate, codeListExist);

                            this.PanelMsgTable.Controls.Add(this._CertificateImportTable);
                        }

                        if (_ErrorNum > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeExpandableParagraphs", "Helper.ExpandableParagraphContainer();$('#Upload_TabLink').click();", true);
                            this.DisplayFeedback(_GlobalResources.ThisFileCannotBeImportedDueToMistakesPleaseTryAgain.Replace("NUMBER", _ErrorNum.ToString()), true);
                            this._PreviousErrorButton.ID = "PreviousErrorButton";
                            this._PreviousErrorButton.CssClass = "Button ActionButton";
                            this._PreviousErrorButton.Attributes.Add("onclick", "PreviousError();return false;");
                            this._PreviousErrorButton.Text = _GlobalResources.PreviousError;

                            this._NextErrorButton.ID = "NextErrorButton";
                            this._NextErrorButton.CssClass = "Button ActionButton";
                            this._NextErrorButton.Attributes.Add("onclick", "NextError();return false;");
                            this._NextErrorButton.Text = _GlobalResources.NextError;

                            this.ErrorTablePanel.Controls.Add(this._PreviousErrorButton);
                            this.ErrorTablePanel.Controls.Add(this._NextErrorButton);

                            this.ErrorTablePanel.Controls.Add(this.PanelMsgTable);
                        }
                        else if (columnMatched)
                        {
                            using (TransactionScope scope = new TransactionScope())
                            {
                                int idCertificateImport = Library.CertificateImport.ImportRecord((this._UserObject != null) ? this._UserObject.Id.ToString() : AsentiaSessionState.IdSiteUser.ToString(), this._CertificateUploader.FileOriginalName);
                                if (idCertificateImport > 0)
                                {
                                    certificateDataTable.Columns.RemoveAt(certificateDataTable.Columns.Count - 1);
                                    Library.CertificateImport.Import(idCertificateImport, certificateDataTable);
                                }
                                scope.Complete();
                                Session["Imported"] = "Success";
                                Response.Redirect(Request.RawUrl);
                            }

                            File.Delete(Server.MapPath(this._CertificateUploader.SavedFilePath));
                        }
                    }
                    catch (Exception)
                    { ;}                  
                }
            }
            this._CertificateFileModal.HideModal();
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction.ID = "GridConfirmAction";

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedCertificate_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseCertificate_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);
            this.ActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.CertificateImportGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.CertificateImportGrid.Rows[i].FindControl(this.CertificateImportGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.CertificateImport.Delete(recordsToDelete);

                    // rebind the grid
                    this.CertificateImportGrid.BindData();

                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this.ManageCertificateFeedbackContainer, _GlobalResources.TheSelectedCertificatesHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedbackInSpecifiedContainer(this.ManageCertificateFeedbackContainer, _GlobalResources.NoCertificateImportFile_sSelectedForDeletion, true);
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.ManageCertificateFeedbackContainer, ex.Message, true);

                // rebind the grid
                this.CertificateImportGrid.BindData();
            }
        }
        #endregion

        #region _ColumnValueValidation
        /// <summary>
        /// Validation for each Certificate field based on the datatype or its special format
        /// </summary>
        private void  _ColumnValueValidation(string[] columns, bool differentAwardDate, string[] codeListExist)
        {
            _RowNum++;
            string originalData = String.Empty;
            int n;

            DateTime dateValue;
            TableRow rowCertificateData = new TableRow();

            TableCell rowNum = new TableCell();
            rowNum.Text = _RowNum.ToString();
            rowNum.CssClass = "ExcelTableRowHeader";
            rowCertificateData.Controls.Add(rowNum);

            for (int i = 0; i < columns.Length; i++)
            {
                TableCell cellCertificateData = new TableCell();
                cellCertificateData.CssClass = "ExcelTableData";
                string cellText = _CheckNullValue(columns[i]);
                string columnIdentifier = this._CertificateColumnsInfoTable.Rows[i+1].Cells[1].Text;
                string errorText = "<a href='#' id='ErrorPositionError#'><font color=red>" + cellText + "<sup>Error#</sup></font></ a>";

                //Check if the columns allow null value, if not, check the import value for that column.
                if (this._CertificateColumnsInfoTable.Rows[i + 1].Cells[2].Text.Contains(_GlobalResources.Yes) && String.IsNullOrWhiteSpace(cellText))
                {
                    _ErrorNum++;
                    cellCertificateData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellCertificateData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellCertificateData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_IsARequiredFieldThisFieldMustContainAValue.Replace("##FIELD##", columnIdentifier);
                }
                //Check the String length
                else if (this._CertificateColumnsInfoTable.Rows[i + 1].Cells[3].Text.Contains(_GlobalResources.Text) && int.TryParse(this._CertificateColumnsInfoTable.Rows[i + 1].Cells[4].Text, out n) && cellText.Length > Convert.ToInt16(this._CertificateColumnsInfoTable.Rows[i + 1].Cells[4].Text))
                {
                    _ErrorNum++;
                    cellCertificateData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellCertificateData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellCertificateData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.TheLengthOfThisEntryIsExeedingItsMaxLength.Replace("##X##", this._CertificateColumnsInfoTable.Rows[i + 1].Cells[4].Text);
                }
                //Check the Datetime format
                else if (this._CertificateColumnsInfoTable.Rows[i + 1].Cells[3].Text.Contains(_GlobalResources.DateTime) && !String.IsNullOrWhiteSpace(cellText) && !
                    DateTime.TryParse(cellText, out dateValue))
                {
                    _ErrorNum++;
                    cellCertificateData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellCertificateData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellCertificateData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_MustBeAValidDateInTheISODateTimeFormat.Replace("##FIELD##", columnIdentifier);
                }
                //Check the userImported
                else if (columnIdentifier == _GlobalResources.Username && !String.IsNullOrWhiteSpace(cellText) && !String.IsNullOrWhiteSpace(_UsernameImported) && (_UsernameImported != cellText))
                {
                    _ErrorNum++;
                    cellCertificateData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellCertificateData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellCertificateData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.OnlyDataForTheUsername_username_CanBeImported.Replace("##username##", _UsernameImported);
                }
                //Check if the username existed
                else if (columnIdentifier == _GlobalResources.Username && !String.IsNullOrWhiteSpace(cellText) && _UsernameNotExistList.Contains(cellText))
                {
                    _ErrorNum++;
                    cellCertificateData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellCertificateData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellCertificateData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.TheUserWithTheUsername_username_DoesNotExistInThePortal.Replace("##username##", cellText);
                }
                //Check if the code existed
                else if (columnIdentifier == _GlobalResources.CertificateCode && !String.IsNullOrWhiteSpace(cellText) && !codeListExist.Contains(cellText))
                {
                    _ErrorNum++;
                    cellCertificateData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellCertificateData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellCertificateData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.ACertificateWithTheCodeDoesNotExistInThePortal.Replace("##certificate_code##", cellText);
                }
                //Check the endRow
                else if (columnIdentifier == _GlobalResources.ROWEND_UPPER && !String.IsNullOrWhiteSpace(cellText) && (cellText.ToUpper() != _GlobalResources.END_UPPER))
                {
                    _ErrorNum++;
                    cellCertificateData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellCertificateData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellCertificateData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources._FIELD_MustContainTheTextEND.Replace("##FIELD##", columnIdentifier);
                }

                //Check different awardDate
                else if (differentAwardDate && (columnIdentifier == _GlobalResources.CertificateCode))
                {
                    _ErrorNum++;
                    cellCertificateData.ID = "ErrorTdPosition" + _ErrorNum.ToString();
                    cellCertificateData.Text = errorText.Replace("Error#", _ErrorNum.ToString());
                    cellCertificateData.ToolTip = "(" + columnIdentifier + ") " + _GlobalResources.TheSameCertificateCannotBeAwardedForAUserMultipleTimesInASingleCertificateImport;
                }
                else
                {
                    cellCertificateData.Text = columns[i];
                }

                rowCertificateData.Controls.Add(cellCertificateData);
            }

            this._CertificateImportTable.Controls.Add(rowCertificateData);

        }
        #endregion

        #region _CheckNullValue
        /// <summary>
        /// Check if an input is null value or not.
        /// </summary>
        private string _CheckNullValue(string value)
        {
            string returnValue = value;
            if (value.ToLower() == "null" || String.IsNullOrWhiteSpace(value) || String.IsNullOrWhiteSpace(value))
                returnValue = null;
            return returnValue;
        }
        #endregion
    }
}
