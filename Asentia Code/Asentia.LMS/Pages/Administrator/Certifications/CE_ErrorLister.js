//Stores information of all the errors in the Certification that a user must correct
CE_ErrorLister = function(
	parent
	){
		
	this.Parent = parent;
	this.Interface = this.Parent;
	this.ErrorList = new Array();
	
	//create the list of possible errors
	this.PROPERTIES_TITLE_REQUIRED = "PROPERTIES_TITLE_REQUIRED";
	this.PROPERTIES_DESCRIPTION_REQUIRED = "PROPERTIES_DESCRIPTION_REQUIRED";
	
	this.INITIAL_UNIT_TITLE_REQUIRED = "INITIAL_UNIT_TITLE_REQUIRED";
	this.INITIAL_UNIT_DESCRIPTION_REQUIRED = "INITIAL_UNIT_DESCRIPTION_REQUIRED";
	this.INITIAL_SEGMENT_LABEL_REQUIRED = "INITIAL_SEGMENT_LABEL_REQUIRED";
	this.INITIAL_SEGMENT_NO_REQUIREMENTS = "INITIAL_SEGMENT_NO_REQUIREMENTS";
	this.INITIAL_UNIT_NO_SEGMENTS = "INITIAL_UNIT_NO_SEGMENTS";
	this.INITIAL_NO_UNITS = "INITIAL_NO_UNITS";
	
	this.RENEWAL_UNIT_TITLE_REQUIRED = "RENEWAL_UNIT_TITLE_REQUIRED";
	this.RENEWAL_UNIT_DESCRIPTION_REQUIRED = "RENEWAL_UNIT_DESCRIPTION_REQUIRED";
	this.RENEWAL_SEGMENT_LABEL_REQUIRED = "RENEWAL_SEGMENT_LABEL_REQUIRED";
	this.RENEWAL_SEGMENT_NO_REQUIREMENTS = "RENEWAL_SEGMENT_NO_REQUIREMENTS";
	this.RENEWAL_UNIT_NO_SEGMENTS = "RENEWAL_UNIT_NO_SEGMENTS";
	this.RENEWAL_NO_UNITS = "RENEWAL_NO_UNITS";
	
	this.COURSE_DOES_NOT_EXIST = "COURSE_DOES_NOT_EXIST";
	this.GETCOURSELISTING_ERROR = "GETCOURSELISTING_ERROR";

}

CE_ErrorLister.prototype.AddError = function(
	tabName, 		//name of tab where error occurred
	targetObject, 	//specific item that error occurred on
	errorName,		//name of the error based on the defined constants
	unitObject,		//optional: used when error occurs on a unit
	segmentObject	//optional: used when error occurs on a segment
)
{
	var thisError = new ErrorListItem(this, tabName, targetObject, errorName, unitObject, segmentObject);
	this.ErrorList.push(thisError);
}

CE_ErrorLister.prototype.ClearAllErrors = function()
{
	this.ErrorList.splice(0, this.ErrorList.length);
}

CE_ErrorLister.prototype.GetFriendlyErrorString = function(errorConstant)
{
	//return the error in a human-readable format
	switch(errorConstant)
	{
		case "PROPERTIES_TITLE_REQUIRED":
			return this.Interface.GetDictionaryTerm("The Properties Tab requires a Title in the default language");
			//return "The Properties Tab requires a Title in the default language.";//ThePropertiesTabRequiresATitleInTheDefaultLanguage
			break;
		case "PROPERTIES_DESCRIPTION_REQUIRED":
			return this.Interface.GetDictionaryTerm("The Properties Tab requires a Description in the default language");
			//return "The Properties Tab requires a Description in the default language.";//ThePropertiesTabRequiresADescriptionInTheDefaultLanguage
			break;
		case "INITIAL_UNIT_TITLE_REQUIRED":
			return this.Interface.GetDictionaryTerm("Unit x on the Initial Requirements Tab requires a Title in the default language");
			//return "Unit ##UnitNumber## on the Initial Requirements Tab requires a Title in the default language.";//UnitXOnTheInitialRequirementsTabRequiresATitleInTheDefaultLanguage
			break;
		case "INITIAL_UNIT_DESCRIPTION_REQUIRED":
			return this.Interface.GetDictionaryTerm("Unit x on the Initial Requirements Tab requires a Description in the default language");
			//return "Unit ##UnitNumber## on the Initial Requirements Tab requires a Description in the default language.";//UnitXOnTheInitialRequirementsTabRequiresADescriptionInTheDefaultLanguage
			break;
		case "INITIAL_SEGMENT_LABEL_REQUIRED":
			return this.Interface.GetDictionaryTerm("Segment x on Unit x on the Initial Requirements Tab requires a Label in the default language");
			//return "Segment ##SegmentNumber## on Unit ##UnitNumber## on the Initial Requirements Tab requires a Label in the default language.";//SegmentXOnUnitXOnTheInitialRequirementsTabRequiresALabelInTheDefaultLanguage
			break;
		case "INITIAL_SEGMENT_NO_REQUIREMENTS":
			return this.Interface.GetDictionaryTerm("Segment x on Unit x on the Initial Requirements Tab must have at least one Requirement");
			//return "Segment ##SegmentNumber## on Unit ##UnitNumber## on the Initial Requirements Tab must have at least one Requirement.";//SegmentXOnUnitXOnTheInitialRequirementsTabMustHaveAtLeastOneRequirement
			break;
		case "INITIAL_UNIT_NO_SEGMENTS":
			return this.Interface.GetDictionaryTerm("Unit x on the Initial Requirements Tab must have at least one Segment");
			//return "Unit ##UnitNumber## on the Initial Requirements Tab must have at least one Segment.";//UnitXOnTheInitialRequirementsTabMustHaveAtLeastOneSegment
			break;
		case "INITIAL_NO_UNITS":
			return this.Interface.GetDictionaryTerm("there are no units on the initial tab");//ThereAreNoUnitsOnTheInitialTab
			break;
		case "RENEWAL_UNIT_TITLE_REQUIRED":
			return this.Interface.GetDictionaryTerm("Unit x on the Renewal Requirements Tab requires a Title in the default language");
			//return "Unit ##UnitNumber## on the Renewal Requirements Tab requires a Title in the default language.";//UnitXOnTheRenewalRequirementsTabRequiresATitleInTheDefaultLanguage
			break;
		case "RENEWAL_UNIT_DESCRIPTION_REQUIRED":
			return this.Interface.GetDictionaryTerm("Unit x on the Renewal Requirements Tab requires a Description in the default language");
			//return "Unit ##UnitNumber## on the Renewal Requirements Tab requires a Description in the default language.";//UnitXOnTheRenewalRequirementsTabRequiresADescriptionInTheDefaultLanguage
			break;
		case "RENEWAL_SEGMENT_LABEL_REQUIRED":
			return this.Interface.GetDictionaryTerm("Segment x on Unit x on the Renewal Requirements Tab requires a Label in the default language");
			//return "Segment ##SegmentNumber## on Unit ##UnitNumber## on the Renewal Requirements Tab requires a Label in the default language.";//SegmentXOnUnitXOnTheRenewalRequirementsTabRequiresALabelInTheDefaultLanguage
			break;
		case "RENEWAL_SEGMENT_NO_REQUIREMENTS":
			return this.Interface.GetDictionaryTerm("Segment x on Unit x on the Renewal Requirements Tab must have at least one Requirement");
			//return "Segment ##SegmentNumber## on Unit ##UnitNumber## on the Renewal Requirements Tab must have at least one Requirement.";//SegmentXOnUnitXOnTheRenewalRequirementsTabMustHaveAtLeastOneRequirement
			break;
		case "RENEWAL_UNIT_NO_SEGMENTS":
			return this.Interface.GetDictionaryTerm("Unit x on the Renewal Requirements Tab must have at least one Segment");
			//return "Unit ##UnitNumber## on the Renewal Requirements Tab must have at least one Segment.";//UnitXOnTheRenewalRequirementsTabMustHaveAtLeastOneSegment
			break;
		case "RENEWAL_NO_UNITS":
			return this.Interface.GetDictionaryTerm("there are no units on the renewal tab");//ThereAreNoUnitsOnTheRenewalTab
			break;
		case "COURSE_DOES_NOT_EXIST":
			return this.Interface.GetDictionaryTerm("The course titled x no longer exists in the system");
			//return "The course titled ##CourseName## no longer exists in the system.";//TheCourseTitledXNoLongerExistsInTheSystem
		case "GETCOURSELISTING_ERROR":
			return this.Interface.GetDictionaryTerm("error occurred retrieving course listing. please try again or contact administrator");
			//return "Error occurred retrieving course listing. Please try again or contact administrator."
		default:
			return this.Interface.GetDictionaryTerm("General Error");
			//return "General Error.";//GeneralError
			break;
	}
	
	
}

//Holds all info about the specific error
ErrorListItem = function(
	parent,
	tabName,
	targetObject,
	errorName,
	unitObject,
	segmentObject
)
{
	this.Parent = parent;
	this.Interface = this.Parent.Interface;
	
	this.TabName = tabName;
	this.TargetObject = targetObject;
	this.ErrorName = errorName;//string error description constant
	this.ErrorNameFriendly = this.Parent.GetFriendlyErrorString(this.ErrorName);
	
	//if Error is that course doesn't exist, add the specific name of the course in the error
	if (errorName == "COURSE_DOES_NOT_EXIST")
	{
		var missingCourseName = this.Interface.GetCourseNameFromPool(targetObject.Identifier);
		
		//if a course name cannot be found, change to a generic message
		if (missingCourseName)
			this.ErrorNameFriendly = this.ErrorNameFriendly.split("##CourseName##").join(missingCourseName);
		else
			this.ErrorNameFriendly = this.Interface.GetDictionaryTerm("This course no longer exists in the system");
	}

	//handler for if there's an error in the Unit section
	if (unitObject)
	{
		this.UnitObject = unitObject;
		this.UnitObjectIndexInCarousel;
		
		//the object's Identifier will match the 'id' of the UI element
		this.UnitObjectIndexInCarousel = this.GetUnitIndexInCarousel();
		
		//update the friendly error text for the Unit Number
		this.ErrorNameFriendly = this.ErrorNameFriendly.split("##UnitNumber##").join(this.UnitObjectIndexInCarousel + 1);
	}
	
	if (segmentObject)
	{
		this.SegmentObject = segmentObject;
		this.SegmentObjectIndexInCarousel;
		
		this.SegmentObjectIndexInCarousel = this.GetSegmentIndexInCarousel();
		//console.log("GetSegmentIndexInCarousel: " + this.GetSegmentIndexInCarousel());
		
		//update the friendly error text for the Unit Number (divide by 2 to make up for AND/OR buttons)
		var calculatedSegmentNumber = (this.SegmentObjectIndexInCarousel / 2) + 1;
		this.ErrorNameFriendly = this.ErrorNameFriendly.split("##SegmentNumber##").join(calculatedSegmentNumber);
	}
	
	//unique Id
	this.Id = this.Parent.ErrorList.length;
}

ErrorListItem.prototype.GetUnitIndexInCarousel = function()
{
	//find the index of the Unit in question in AsentiaCarousel
	//console.log(this.Interface.GetTabFormObjectByName(this.TabName).UnitsCarousel);
	var thisUnitsCarousel = this.Interface.GetTabFormObjectByName(this.TabName).UnitsCarousel;
	var carouselItemIndex;
	
	console.log(thisUnitsCarousel.Items[0]);
	
	for (var i = 0; i < thisUnitsCarousel.Items.length; i++)
	{
		var currCarouselItemId = thisUnitsCarousel.Items[i].children[0].id;//UnitsCarousel-Item0 > myCertification-Unit0
		//console.log("currCarouselItemId: " + currCarouselItemId);
		
		//match the Identifier of this Unit with the Carousel item id name
		if (String(this.UnitObject.Identifier) == String(currCarouselItemId))
		{
			carouselItemIndex = i;
			break;
		}
	}
	
	return carouselItemIndex;
}

ErrorListItem.prototype.GetSegmentIndexInCarousel = function()
{
	//find the index of the Segment in question in AsentiaCarousel
	
	//Unit must exist for there to be a Segment
	if (!this.UnitObject)
		return;
	
	var thisSegmentsCarousel = this.UnitObject.SegmentsCarousel;
	var carouselItemIndex;
	
	//console.log(thisSegmentsCarousel);
	
	for (var i = 0; i < thisSegmentsCarousel.Items.length; i++)
	{
		var currCarouselItemId = thisSegmentsCarousel.Items[i].children[0].id;
		//console.log("Segments currCarouselItemId: " + currCarouselItemId);
		
		//match the Identifier of this Segment with the Segment item id name
		if (String(this.SegmentObject.Identifier) == String(currCarouselItemId))
		{
			carouselItemIndex = i;
			break;
		}
	}
	
	return carouselItemIndex;
}

