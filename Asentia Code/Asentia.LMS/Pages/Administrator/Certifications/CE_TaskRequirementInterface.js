CE_TaskRequirementInterface = function(
	parent, 
	modal,
	identifier,
	languages,
	initialLanguage,
	flagImagePath,
	deleteImagePath,
	addImagePath,
	data
	){
		
	this.TypeName = "TaskRequirementInterface";
	
	this.Parent 			= parent;
	this.ModalWindow		= modal;
	this.Identifier 		= identifier; 
	this.Languages 			= languages;
	this.InitialLanguage 	= initialLanguage;
	this.FlagImagePath 		= flagImagePath;
	this.DeleteImagePath 	= deleteImagePath;
	this.AddImagePath 		= addImagePath;
	
	this.Interface 			= this.Parent.Interface;
	
	this.Body = document.createElement("div");
		this.Body.className = "ModalPopupBody";
		
	if (typeof data == "object") {
	    this.Data = data;
	    this.DataIdentifier = this.Data.id;
	}else{
	    this.Data = new Object;
	    this.DataIdentifier = this.Parent.GetNextDataIdentifier();
		this.Data.Labels = [];
		this.Data.Task = new Object;
			this.Data.Task.Signoff = new Object;
	}

	// content here
	
	this.Task = new FormInputField(
		this, 					//object
		this.Body,				// DOM node to append to
		"input",				// type (input|textarea)
		this.Interface.GetDictionaryTerm("Describe the External Task"),  				// text
		true, 					// bit
		this.Languages, 		// array
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		this.Data.Labels, 					// JSON
		"InputMedium",						// className to add to the input
		this.Interface.GetDictionaryTerm("New Task")				// default label if unable to extract
		);

	this.RequireDocumentation = new FormInputField(
		this, 					//object
		this.Body,				// DOM node to append to
		"custom",				// type (input|textarea)
		this.Interface.GetDictionaryTerm("Require Documentation"),// text: "Require Documentation?"
		false, 					// bit
		this.Languages, 		// array
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		"", 					// JSON
		"",						// className to add to the input
		this.Interface.GetDictionaryTerm("New Task")				// default label if unable to extract
		);
		
	this.RequireDocumentationOptionYes = new HTMLRadioOptionWithLabel(
		"RequireDocumentation", 
		"RequireDocumentation_Yes", 
		"true", 
		this.Interface.GetDictionaryTerm("yes learner must upload documentation verifying completion of task"), //"Yes, learner must upload documentation verifying completion of task."
		(this.Data.Task.RequireDocumentation == true),
		false,
		"ToggleInput"
		);
	
	this.RequireDocumentationOptionNo = new HTMLRadioOptionWithLabel(
		"RequireDocumentation", 
		"RequireDocumentation_No", 
		"false", 
		this.Interface.GetDictionaryTerm("No") + ".", 
		(this.Data.Task.RequireDocumentation != true),
		false,
		"ToggleInput"
		);
		
	this.RequireDocumentation.FormFieldInputContainer.append(this.RequireDocumentationOptionYes.Container);
	this.RequireDocumentation.FormFieldInputContainer.append(this.RequireDocumentationOptionNo.Container);
	
	this.Signoff = new FormInputField(
		this, 					//object
		this.Body,				// DOM node to append to
		"custom",				// type (input|textarea)
		this.Interface.GetDictionaryTerm("Signoff"),// text
		false, 					// bit
		this.Languages, 		// array
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		"", 					// JSON
		"",						// className to add to the input
		this.Interface.GetDictionaryTerm("New Task")				// default label if unable to extract
		);
	
	//this.Interface.GetDictionaryTerm("Signoff is required by either")
	this.SignOffOptionYes = new HTMLCheckBoxOptionWithLabel(
		"SignOffRequired", 
		"SignOffRequired_Yes", 
		"1", 
		this.Interface.GetDictionaryTerm("Signoff is required by any of the following"), 
		(this.Data.Task.Signoff.Required == true),
		false
		);
		
	this.SignOffOptionYes.Parent = this;
	
	this.SignOffOptionYes.Input.onclick = function(){
			this.Parent.Parent.UpdateForm();
		}

	this.SignOffByAdministrators = new HTMLCheckBoxOptionWithLabel(
		"SignOffBy", 
		"SignOffBy_Administrators", 
		"administrators", 
		this.Interface.GetDictionaryTerm("administrators"), 
		(this.Data.Task.Signoff.ByAdministrators == true),
		true
		);
			
	this.SignOffBySupervisor = new HTMLCheckBoxOptionWithLabel(
		"SignOffBy", 
		"SignOffBy_Supervisor", 
		"supervisor", 
		this.Interface.GetDictionaryTerm("Learners Supervisors"), 
		(this.Data.Task.Signoff.BySupervisor == true),
		false//SignOffOptionYes.Input.disabled
		);
	
	//Course Expert option deprecated
	/*this.SignOffByCourseExpert = new HTMLCheckBoxOptionWithLabel(
		"SignOffBy", 
		"SignOffBy_CourseExpert", 
		"courseexpert", 
		this.Interface.GetDictionaryTerm("Course Expert"), 
		(this.Data.Task.Signoff.ByExpert == true),
		false//SignOffOptionYes.Input.disabled
		);*/
	
	this.SignOffByContainer = document.createElement("div");
		this.SignOffByContainer.className = "FormFieldInputContainer";
	this.SignOffByContainer.append(this.SignOffByAdministrators.Container);
	this.SignOffByContainer.append(this.SignOffBySupervisor.Container);
	//this.SignOffByContainer.append(this.SignOffByCourseExpert.Container);
	
	this.Signoff.FormFieldInputContainer.append(this.SignOffOptionYes.Container);
	this.Signoff.FormFieldInputContainer.append(this.SignOffByContainer);
	
	// do not attach. 
	this.Separator = document.createElement("div");
		this.Separator.className = "ModalPopupSeparator";
		
	// do not attach
	this.ButtonContainer = document.createElement("div");
		this.ButtonContainer.className = "ModalPopupButtons";
	
	this.OKButton = document.createElement("input");
		this.OKButton.Parent = this;
		this.OKButton.type = "button";
		this.OKButton.className = "Button ActionButton";
		this.OKButton.value = this.Interface.GetDictionaryTerm("OK");
		
		this.OKButton.onclick = function(){
			if(this.Parent.IsValidOrShowErrors()){
				this.Parent.ModalWindow.Parent.PushRequirementInterface(this.Parent.GetDataAsObject());
				//this.Parent.ModalWindow.Panel.style.visibility = "hidden";
				this.Parent.ModalWindow.Close();
				
				//hide the containing window
				this.Parent.Interface.ShowAsentiaModalContainer(false);
			}
		}
		
	this.CancelButton = document.createElement("input");
		this.CancelButton.Parent = this;
		this.CancelButton.type = "button";
		this.CancelButton.className = "Button NonActionButton";
		this.CancelButton.value = this.Interface.GetDictionaryTerm("Cancel");
		
		this.CancelButton.onclick = function(){
		
			//this.Parent.ModalWindow.Panel.style.visibility = "hidden";
			this.Parent.ModalWindow.Close();
			
			//hide the containing window
			this.Parent.Interface.ShowAsentiaModalContainer(false);
		}
		
	this.ButtonContainer.append(this.OKButton);
	this.ButtonContainer.append(this.CancelButton);
	
	this.UpdateForm();
	
}

CE_TaskRequirementInterface.prototype.UpdateForm = function(){
	
	this.SignOffBySupervisor.Input.disabled = !this.SignOffOptionYes.Input.checked;
	if (this.SignOffBySupervisor.Input.disabled)
	{
		this.SignOffBySupervisor.Input.checked = false;
	}
	
	//also automatically update the Administrators box (this box will ALWAYS be checked)
	this.SignOffByAdministrators.Input.checked = this.SignOffOptionYes.Input.checked;
	
	//this.SignOffByCourseExpert.Input.disabled = !this.SignOffOptionYes.Input.checked;
	//if (this.SignOffByCourseExpert.Input.disabled) {this.SignOffByCourseExpert.Input.checked = false;}
	
}

CE_TaskRequirementInterface.prototype.GetDataAsString = function(){
	
	var s = "";
	
	s = s + "{";
	s = s + "	\"id\":\"" + this.DataIdentifier + "\",";
	s = s + "	\"Labels\":" + this.Task.GetData() + ",";
	s = s + "	\"Type\":\"task\",";
    s = s + "   \"Course\": {\"Items\": [], \"isAny\": null },";
	s = s + "   \"Credit\": {\"Items\": [], \"Minimum\": null, \"isAny\": null },";    
	s = s + "	\"Task\":{";
	s = s + "		\"RequireDocumentation\":" + this.RequireDocumentationOptionYes.Input.checked + ",";
	s = s + "		\"Signoff\":{";
	s = s + "			\"Required\":" + this.SignOffOptionYes.Input.checked + ",";
	s = s + "			\"ByAdministrators\":" + this.SignOffByAdministrators.Input.checked + ",";
	s = s + "			\"BySupervisor\":" + this.SignOffBySupervisor.Input.checked;
	//s = s + "			\"ByExpert\":" + this.SignOffByCourseExpert.Input.checked;
	s = s + "		}";
	s = s + "	}";
	s = s + "}";
	
	return s;
	
}

CE_TaskRequirementInterface.prototype.GetDataAsObject = function(){
	
	//console.log(this.GetDataAsString());
	return JSON.parse(this.GetDataAsString());
	
}

CE_TaskRequirementInterface.prototype.IsValidOrShowErrors = function(){
	
	this.InErrorState = false;
	
	this.Task.ClearError();
	this.RequireDocumentation.ClearError();
	this.Signoff.ClearError();
	
	/* Title */
	if (this.Task.Inputs[0].value == "")
	{
		this.Task.ShowError(this.Interface.GetDictionaryTerm("Description is required"));
		this.InErrorState = true;
	}
	
	/* Documentation */
	
	if(this.RequireDocumentationOptionYes.Input.checked == false && this.RequireDocumentationOptionNo.Input.checked == false){
		this.RequireDocumentation.ShowError(this.Interface.GetDictionaryTerm("Require Documentation is required"));//"Require Documentation is required."
		this.InErrorState = true;
	}
	
	/* Signoff */
	
	//technically, Administrators will always be checked, but do the error check just for good measure
	if (this.SignOffOptionYes.Input.checked == true)
	{
		if (this.SignOffBySupervisor.Input.checked == false && this.SignOffByAdministrators.Input.checked == false)
		{
			this.Signoff.ShowError(this.Interface.GetDictionaryTerm("signoff is required please select one or both options"));//"Signoff is required; please select one or both options."
			this.InErrorState = true;
		}
	}
	
	//Deprecated
	/*if (this.SignOffOptionYes.Input.checked == true){
		if(this.SignOffBySupervisor.Input.checked == false && this.SignOffByCourseExpert.Input.checked == false){
			this.Signoff.ShowError(this.Interface.GetDictionaryTerm("signoff is required please select one or both options"));//"Signoff is required; please select one or both options."
			this.InErrorState = true;
		}
	}*/
	
	return !this.InErrorState;
	
}