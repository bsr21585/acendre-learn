CE_CreditRequirementInterface = function(
	parent, 
	modal,
	identifier,
	languages,
	initialLanguage,
	flagImagePath,
	deleteImagePath,
	addImagePath,
	data
	){
		
	this.TypeName = "CreditRequirementInterface";
	
	this.Parent 			= parent;
	this.ModalWindow		= modal;
	this.Identifier 		= identifier; 
	this.Languages 			= languages;
	this.InitialLanguage 	= initialLanguage;
	this.FlagImagePath 		= flagImagePath;
	this.DeleteImagePath 	= deleteImagePath;
	this.AddImagePath 		= addImagePath;
	
	this.Interface 			= this.Parent.Interface;
	
	this.Body = document.createElement("div");
		this.Body.className = "ModalPopupBody";
		
	if (typeof data == "object") {
	    this.Data = data;
	    this.DataIdentifier = this.Data.id;
	}else{
	    this.Data = new Object;
	    this.DataIdentifier = this.Parent.GetNextDataIdentifier();
		this.Data.Labels = [];
		this.Data.Credit = new Object;
			this.Data.Credit.Items = [];
			this.Data.Credit.Minimum = "";
	}
	
	// content here
	
	this.Label = new FormInputField(
		this, 					//object
		this.Body,				// DOM node to append to
		"input",				// type (input|textarea)
		this.Interface.GetDictionaryTerm("Description"),  				// text
		true, 					// bit
		this.Languages, 		// array
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		this.Data.Labels, 					// JSON
		"InputMedium",						// className to add to the input
		this.Interface.GetDictionaryTerm("New Requirement")		// default label if unable to extract
		);
		
	this.Credit = new FormInputField(
		this, 					//object
		this.Body,				// DOM node to append to
		"input",				// type (input|textarea)
		this.Interface.GetDictionaryTerm("Credits Required"),  	// text
		true, 					// bit
		this.InitialLanguage.split(","), 		// array
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		this.Data.Credit.Minimum.toString(), 					// JSON
		"InputXShort",						// className to add to the input
		"1"						// default label if unable to extract
		);

	this.Courses = new FormInputField(
		this, 					//object
		this.Body,				// DOM node to append to
		"custom",				// type (input|textarea)
		this.Interface.GetDictionaryTerm("From Course(s)"),  		// text
		true, 					// bit
		this.InitialLanguage.split(","), 		// array
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		"", 					// JSON
		"InputXShort",			// className to add to the input
		""						// default label if unable to extract
		);
		
	
	this.AnyCourseSelector = new HTMLCheckBoxOptionWithLabel(
		"", 
		this.Identifier + "-SelectAnyCourse", 
		"0", 
		this.Interface.GetDictionaryTerm("Any Course"), 
		(this.Data.Credit.isAny == true),
		false
		);
		
	try {this.SelectedCoursesData = this.Data.Credit.Items}catch(e){this.CoursesData = "";}
	
	this.HTMLCoursesList = new HTMLListOfCheckBoxWithLabel(
		this.Identifier + "-CreditList",
		this.Identifier + "-CreditList",
		"", 
		"CheckBoxList",
		this
		);
	
	// target the list
	this.AnyCourseSelector.Input.Target = this.HTMLCoursesList;
	this.AnyCourseSelector.Input.onchange = function(){
		if (this.checked){
			this.Target.SelectAll();
			this.Target.DisableAll();
		}else{
			this.Target.DisableNone();
		}
	}
		
	this.Courses.FormFieldInputContainer.append(this.AnyCourseSelector.Container);
	
	//accumulate the selected Ids
	var ai = [];
	for (var i = 0; i < this.Data.Credit.Items.length; i++){
		ai[this.Data.Credit.Items[i].id] = true;
	}
	
	// process the course list
	var ac = this.Interface.Data.ApplicableCourses;
	for (var i = 0; i < ac.length; i++) {
		//first, process the number of credits string
		var numCreditsStr = this.Interface.GetDictionaryTerm("x credits");//i.e. ##NumberOfCredits## credits
		numCreditsStr = numCreditsStr.split("##NumberOfCredits##").join(ac[i].Credits);
		
		//ac[i].Name + " (" + ac[i].Credits + " credits)",
		this.HTMLCoursesList.AddItem(
			this.Identifier + "-CreditList-Items", 
			this.Identifier + "-CreditList-Item" + ac[i].id,
			ac[i].id,  
			ac[i].Name + " (" + numCreditsStr + ")", 
			(ai[ac[i].id] == true),
			false
		);
	}
	
	// do the "any"
	if (this.Data.Credit.isAny == true){
		this.HTMLCoursesList.SelectAll();
		this.HTMLCoursesList.DisableAll();
	}
	
	this.Courses.FormFieldInputContainer.append(this.HTMLCoursesList.Panel);
	
	// do not attach. 
	this.Separator = document.createElement("div");
		this.Separator.className = "ModalPopupSeparator";
		
	// do not attach
	this.ButtonContainer = document.createElement("div");
		this.ButtonContainer.className = "ModalPopupButtons";
	
	this.OKButton = document.createElement("input");
		this.OKButton.Parent = this;
		this.OKButton.type = "button";
		this.OKButton.className = "Button ActionButton";
		this.OKButton.value = this.Interface.GetDictionaryTerm("OK");
		
		this.OKButton.onclick = function(){
			if(this.Parent.IsValidOrShowErrors()){

				//console.log(this.Parent.Data.Credit);
				this.Parent.ModalWindow.Parent.PushRequirementInterface(this.Parent.GetDataAsObject());
				//this.Parent.ModalWindow.Panel.style.visibility = "hidden";
				this.Parent.ModalWindow.Close();
				//hide the containing window
				this.Parent.Interface.ShowAsentiaModalContainer(false);
				//console.log(this.Parent.Data.Credit);
				//console.log("++++++++++++++++++++");
				//console.log(this.Parent.Interface.Data);
				//console.log("********************");
				//console.log(this.Parent.GetDataAsObject());
			}
		}
		
	this.CancelButton = document.createElement("input");
		this.CancelButton.Parent = this;
		this.CancelButton.type = "button";
		this.CancelButton.className = "Button NonActionButton";
		this.CancelButton.value = this.Interface.GetDictionaryTerm("Cancel");
		
		this.CancelButton.onclick = function(){
			//this.Parent.ModalWindow.Panel.style.visibility = "hidden";
			this.Parent.ModalWindow.Close();
			
			//hide the containing window
			this.Parent.Interface.ShowAsentiaModalContainer(false);
		}
		
	this.ButtonContainer.append(this.OKButton);
	this.ButtonContainer.append(this.CancelButton);
	
	this.UpdateForm();
	
}

CE_CreditRequirementInterface.prototype.UpdateForm = function(){
	
	
}

CE_CreditRequirementInterface.prototype.IsValidOrShowErrors = function(){
	
	//validate whether the total credits for the courses selected meets the minimum amount of required credits the learner needs to earn
	this.InErrorState = false;
	
	var totalCreditsOfIncludedCourses = 0;
	
	for (var i = 0; i < this.HTMLCoursesList.Items.length; i++)
	{	
		//if "Any Course" box is checked, include all courses in the credits count; if "Any Course" box unchecked, only include individual courses that are checked
		if (this.AnyCourseSelector.Input.checked || (!this.AnyCourseSelector.Input.checked && this.HTMLCoursesList.Items[i].Input.checked))
		{
			var thisCourseId = this.HTMLCoursesList.Items[i].Input.value;
			var thisCourseNumCredits = this.Interface.GetCourseCreditsFromPool(thisCourseId);
			
			if (thisCourseNumCredits)
				totalCreditsOfIncludedCourses += thisCourseNumCredits;
		}
	}
	
	//console.log("TOTAL CREDS: " + totalCreditsOfIncludedCourses);
	//console.log("REQ'D CREDS: " + this.Credit.Inputs[0].value);
	//compare the total reported credits with the number in the "Credits Required:" box
	var creditsRequired = this.Credit.Inputs[0].value;
	
	if (creditsRequired == undefined || creditsRequired == "")
	{
		creditsRequired = 0;
		this.Credit.Inputs[0].value = 0;
	}
	
	this.Credit.ClearError();
	if (this.Credit.Inputs[0].value == 0 || isNaN(this.Credit.Inputs[0].value))
	{
		this.Credit.ShowError(this.Interface.GetDictionaryTerm("Please enter a valid number of Required Credits"));//Please enter a valid number of Required Credits.
		
		/*this.Parent.Parent.Parent.Parent.Parent.ShowAlertModal(
			"Invalid Entry",
			"Please enter a valid number of Credits Required.",
			"OK",
			""
		);*/
		
		this.InErrorState = true;
	}
	
	if (totalCreditsOfIncludedCourses < this.Credit.Inputs[0].value)
	{
		this.Credit.ShowError(this.Interface.GetDictionaryTerm("Value exceeds total selected course credits"));//"Value exceeds total selected course credits."
		//this.Parent.Parent.Parent.Parent.Parent.ShowAlertModal(
		/*this.Interface.ShowAlertModal(
			"Not Enough Credits",
			"The courses selected do not meet the minimum number of required credits.",
			"OK",
			""
		);*/

		this.InErrorState = true;	
	}
	
	//validate that there is a Description entered
	if (this.Label.Inputs[0].value == "")
	{
		this.Label.ShowError(this.Interface.GetDictionaryTerm("Description is required"));
		this.InErrorState = true;
	}
	
	//console.log(this.HTMLCoursesList.Items);
	//window.foo = this;
	//console.log(this.HTMLCoursesList.Items[0].Container);
	//console.log(this.HTMLCoursesList.Items[0].Label);
	//console.log(this.HTMLCoursesList.Items[0].Input);
	//console.log(this.HTMLCoursesList.Items[0].Input.checked);
	
	//this.Data.Credit.Minimum;
	//this.Data.Credit.isAny == true;
	//this.AnyCourseSelector.Input.checked
	//this.HTMLCoursesList.GetData(); [{"id":123},{"id":987},{"id":456}]
	//Input.value is the course id
	
	//this.HTMLCoursesList.Items
	
	return !this.InErrorState;
	
}

CE_CreditRequirementInterface.prototype.GetDataAsString = function(){
	
	var s = "";
	
	s = s + "{";
	s = s + "	\"id\":\"" + this.DataIdentifier + "\",";
	s = s + "	\"Labels\":";
	s = s + 	this.Label.GetData();
	s = s + "	,";
	s = s + "	\"Type\":\"credit\",";
	s = s + "   \"Course\": {\"Items\": [], \"isAny\": null },";	
    s = s + "   \"Task\": { \"RequireDocumentation\": null, \"Signoff\": { \"ByAdministrators\": null, \"BySupervisor\": null, \"Required\": null } },";
	s = s + "	\"Credit\":{";
	s = s + "		\"isAny\":" + this.AnyCourseSelector.Input.checked + ",";
	s = s + "		\"Minimum\":" + this.Credit.Inputs[0].value + ",";
	s = s + "		\"Items\":";
	
	if (!this.AnyCourseSelector.Input.checked){
		s = s + 		this.HTMLCoursesList.GetData();
	} else {
		s = s + "[]";
	}
	
	s = s + "		";
	s = s + "	}";
	s = s + "}";
	
	return s;
	
}
CE_CreditRequirementInterface.prototype.GetDataAsObject = function(){
	
	return JSON.parse(this.GetDataAsString());
	
}
