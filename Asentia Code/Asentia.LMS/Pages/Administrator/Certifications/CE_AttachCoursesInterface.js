CE_AttachCoursesInterface = function(
	modal,
	parent
	){
	
	this.Modal = modal;
	this.TypeName = "AttachCoursesInterface";
	this.Parent = parent;
	this.Interface = this.Parent.Parent;
	
	//create body container that holds the search field, buttons, results window, and Select All/None
	this.Body = document.createElement("div");
	this.Body.className = "ModalPopupBody";
	
	//create the Select area inside the body
	this.SelectAttachedCoursesSearch = HTML.c(
		this,
		"div",
		"SelectAttachedCoursesSearch",
		"SelectAttachedCoursesSearch", 
		"", 
		this.Body
	);
	
	//create the Search Container (will contain search field, Search/Clear buttons)
	this.SelectAttachedCoursesSearchContainer = HTML.c(
		this,
		"div",
		"SelectAttachedCoursesSearchContainer",
		"SelectAttachedCoursesSearchContainer", 
		"ListBoxSearchContainer", 
		this.SelectAttachedCoursesSearch
	);
	
	//create the search input field 
	this.SelectAttachedCoursesSearchTextBox = HTML.c(
		this,
		"input",
		"SelectAttachedCoursesSearchTextBox",
		"SelectAttachedCoursesSearchTextBox", 
		"InputText InputMedium", 
		this.SelectAttachedCoursesSearchContainer
	);
	
	this.SelectAttachedCoursesSearchTextBox.setAttribute("type","text");
	this.SelectAttachedCoursesSearchTextBox.addEventListener("keyup", function(event) {
		event.preventDefault();
		if (event.keyCode == 13)
			this.Parent.SelectAttachedCoursesSearchButton.click();
	});
	
	//Search button
	this.SelectAttachedCoursesSearchButton = HTML.c(
		this,
		"input",
		"SelectAttachedCoursesSearchButton",
		"SelectAttachedCoursesSearchButton", 
		"Button ActionButton SearchButton", 
		this.SelectAttachedCoursesSearchContainer
	);
	
	this.SelectAttachedCoursesSearchButton.setAttribute("type","button");
	this.SelectAttachedCoursesSearchButton.setAttribute("value",this.Interface.GetDictionaryTerm("Search"));
	
	this.SelectAttachedCoursesSearchButton.onclick = function()
	{
		this.Parent.DoAttachCoursesSearch();
	}
	
	//Clear button
	this.SelectAttachedCoursesClearSearchButton = HTML.c(
		this,
		"input",
		"SelectAttachedCoursesClearSearchButton",
		"SelectAttachedCoursesClearSearchButton", 
		"Button NonActionButton", 
		this.SelectAttachedCoursesSearchContainer
	);
	
	this.SelectAttachedCoursesClearSearchButton.setAttribute("type","button");
	this.SelectAttachedCoursesClearSearchButton.setAttribute("value",this.Interface.GetDictionaryTerm("Clear"));
	
	this.SelectAttachedCoursesClearSearchButton.onclick = function()
	{
		this.Parent.ClearSearch();
	}
	
	//Select/options list box container
	this.SelectAttachedCoursesListBoxContainer = HTML.c(
		this,
		"div",
		"SelectAttachedCoursesListBoxContainer",
		"SelectAttachedCoursesListBoxContainer", 
		"", 
		this.SelectAttachedCoursesSearch
	);
	
	//Select options list box
	this.SelectAttachedCoursesListBox = HTML.c(
		this,
		"select",
		"SelectAttachedCoursesListBox",
		"SelectAttachedCoursesListBox", 
		"ListBox", 
		this.SelectAttachedCoursesListBoxContainer
	);
	
	this.SelectAttachedCoursesListBox.setAttribute("multiple","multiple");
	
	//add Select All/None container (ListBoxSelectAllNoneContainer)
	this.SelectAttachedCoursesListBoxSelectAllNoneContainer = HTML.c(
		this,
		"div",
		"SelectAttachedCoursesListBoxSelectAllNoneContainer",
		"SelectAttachedCoursesListBoxSelectAllNoneContainer", 
		"ListBoxSelectAllNoneContainer", 
		this.SelectAttachedCoursesListBoxContainer
	);
	
	//add Select All|None links
	this.SelectAttachedCoursesListBoxSelectAllNoneContainer.append(this.Interface.GetDictionaryTerm("Select") + " ");
	
	this.SelectAttachedCoursesListBoxSelectAllLink = HTML.c(
		this,
		"a",
		"SelectAttachedCoursesListBoxSelectAllLink",
		"SelectAttachedCoursesListBoxSelectAllLink", 
		"", 
		this.SelectAttachedCoursesListBoxSelectAllNoneContainer,
		this.Interface.GetDictionaryTerm("All")
	);
	
	this.SelectAttachedCoursesListBoxSelectAllLink.onclick = function()
	{
		for (var i = 0; i < this.Parent.SelectAttachedCoursesListBox.options.length; i++)
		{
			var thisCourseItemSelect = this.Parent.SelectAttachedCoursesListBox.options[i];
			thisCourseItemSelect.selected = true;
		}
	}
	
	//add the separator between All | None
	this.SelectAttachedCoursesListBoxSelectAllNoneContainer.append(" | ");
	
	this.SelectAttachedCoursesListBoxSelectNoneLink = HTML.c(
		this,
		"a",
		"SelectAttachedCoursesListBoxSelectNoneLink",
		"SelectAttachedCoursesListBoxSelectNoneLink", 
		"", 
		this.SelectAttachedCoursesListBoxSelectAllNoneContainer,
		this.Interface.GetDictionaryTerm("None")
	);
	
	this.SelectAttachedCoursesListBoxSelectNoneLink.onclick = function()
	{
		for (var i = 0; i < this.Parent.SelectAttachedCoursesListBox.options.length; i++)
		{
			var thisCourseItemSelect = this.Parent.SelectAttachedCoursesListBox.options[i];
			thisCourseItemSelect.selected = false;
		}
	}
	
	//create the container for the buttons at the bottom
	this.ButtonContainer = document.createElement("div");
	this.ButtonContainer.className = "ModalPopupButtons";
	
	//Attach CoursesButton
	this.AttachCoursesModalAttachCoursesButton = HTML.c(
			this, 							//parent
			"input", 						//type
			"AttachCoursesModalAttachCoursesButton", //name
			"placeHolder", 					//id
			"Button ActionButton", 		//class
			this.ButtonContainer 			//appendTo
		);
		
		//manually add the 'type' and 'value' attributes
		this.AttachCoursesModalAttachCoursesButton.setAttribute("type","button");
		this.AttachCoursesModalAttachCoursesButton.setAttribute("value",this.Interface.GetDictionaryTerm("Attach Course(s)"));
		
		this.AttachCoursesModalAttachCoursesButton.onclick = function()
		{
			this.Parent.AddSelectedCoursesToAttachedCourses();
		}
	
	//Done Button
	this.AttachCoursesModalDoneButton = HTML.c(
			this, 							//parent
			"input", 						//type
			"AttachCoursesModalDoneButton", //name
			"placeHolder", 					//id
			"Button NonActionButton", 		//class
			this.ButtonContainer 			//appendTo
		);
		
		//manually add the 'type' and 'value' attributes
		this.AttachCoursesModalDoneButton.setAttribute("type","button");
		this.AttachCoursesModalDoneButton.setAttribute("value",this.Interface.GetDictionaryTerm("Done"));
	
	this.AttachCoursesModalDoneButton.onclick = function()
	{
		//this.Parent.Modal.Panel.style.visibility = "hidden";
		this.Parent.Modal.Close();
		
		//hide the modal container
		this.Parent.Parent.Interface.ShowAsentiaModalContainer(false);
	}
	
	//this.Parent.ModalWindow.Panel.append(this.Body);
	//this.Parent.ModalWindow.Panel.append(this.ButtonContainer);
	//this.Parent.ModalWindow.Reposition();
	
	//document.getElementById(this.Parent.Interface.ModalDOMId).style.position = "fixed";
		
}

CE_AttachCoursesInterface.prototype.PopulateListBox = function(data)
{
	//use the ApplicableCourses data on the page to populate the list box
	this.Items = [];
	
	if (typeof data == "object"){ 
	
		this.Data = data;
		
		for (var i = 0; i < this.Data.length; i++){
			
			//don't add items that are already displayed in the Attached Courses box
			if (this.DoesCourseIdExistInAttachedCourses(this.Data[i].Id))
				continue;
			
			//if Credits is null/undefined, set Credits to 0
			var credits = this.Data[i].Credits;
			if (credits == null || credits == undefined)
				credits = 0;
			
			this.AddListBoxItem(
				this.Data[i].Id, 
				this.Data[i].Title, 
				credits
			);
			
			this.AddListBoxItemElement(
				this.Data[i].Id, 
				this.Data[i].Title, 
				credits
			);
		}
		
	}
	
	//fill the list elements
}

CE_AttachCoursesInterface.prototype.PopulateListBoxWithSearchResults = function(data)
{
	//use with search results; this version of PopulateListBox preserves the original list of courses
	if (typeof data == "object"){ 
		
		for (var i = 0; i < data.length; i++){
			
			//don't add items that are already displayed in the Attached Courses box
			if (this.DoesCourseIdExistInAttachedCourses(data[i].Id))
				continue;
			
			//if Credits is null/undefined, set Credits to 0
			var credits = data[i].Credits;
			if (credits == null || credits == undefined)
				credits = 0;
			
			this.AddListBoxItemElement(
				data[i].Id, 
				data[i].Title, 
				credits
			);
		}
		
	}
	
	//if courses have been found in the global list but they've all been added to the main Attached Courses list, do the 'no courses found' action
	if (this.SelectAttachedCoursesListBox.getElementsByTagName("OPTION").length == 0)
	{
		this.ShowNoSearchResultsFound();
	}
}

CE_AttachCoursesInterface.prototype.AddListBoxItem = function(
	id,
	title,
	credits
)
{
	//attach the item option tag
	this.Items[this.Items.length] = new CE_AttachCoursesInterfaceListItem(
		id,
		title,
		credits
	);

}

CE_AttachCoursesInterface.prototype.RemoveListBoxItem = function(id)
{
	var foundIndex;
	
	//find the index of the matching course
	for (var i = 0; i < this.Items.length; i++)
	{
		var thisId = this.Items[i].Identifier;
		
		if (thisId == id)
		{
			//once found, store the index where the object lies
			foundIndex = i;
			break;
		}
	}
	
	if (foundIndex != null && foundIndex != undefined)
	{
		//remove the item from the array of objects
		this.Items.splice(foundIndex, 1);
	}
	
}

CE_AttachCoursesInterface.prototype.ClearListBox = function()
{
	//clear the items from the Attach Courses list box
	this.SelectAttachedCoursesListBox.innerHTML = "";
}

//add list item object to the array
CE_AttachCoursesInterfaceListItem = function(
	id,
	title,
	credits
)
{	
	this.Identifier = id;
	this.Title = title;
	this.Credits = credits;
}

//add list box element to the page select element
CE_AttachCoursesInterface.prototype.AddListBoxItemElement = function(
	id,
	title,
	credits
)
{
	var thisItemElement = HTML.c(
			this, 							//parent
			"option", 						//type
			null, 							//name
			null, 							//id
			null, 							//class
			this.SelectAttachedCoursesListBox, 			//appendTo
			title + " (" + String(credits) + ")"										//text content
		);
	
	thisItemElement.setAttribute("value",id);
}

CE_AttachCoursesInterface.prototype.AddSelectedCoursesToAttachedCourses = function()
{
	//move the selected items from the modal window to the main "Attached Courses" box on the main tab
	var selectedCourses = this.GetSelectedCourseElementsFromAttachCoursesList();

	if (selectedCourses.length > 0)
	{
		for (var i = 0; i < selectedCourses.length; i++)
		{
			//be sure there's a valid Id and that the course doesn't already exist in the Attached Courses list
			if (!isNaN(Number(selectedCourses[i].value)) && !(document.getElementById("AttachCourse_" + selectedCourses[i].value))) {
                // add the selected course to the Attached Courses list container
				//console.log(selectedCourses[i]);
				
				//get the course object associated with this id
				var thisId = selectedCourses[i].value;
				
				var thisCourseObject = this.GetMatchingCourseObjectById(thisId);
				//returns i.e.: CE_AttachCoursesInterfaceListItem {Identifier: 10558, Title: "5678 New Test Course 3/21/2016", Credits: null}
				//but HTMLCourseListBoxWithCredits uses id, Name, Credits; need to remedy swap id and Name for "Identifier" and "Title"
				this.AddCourseToAttachedCourses(thisCourseObject);

            }
		}
	}

}

CE_AttachCoursesInterface.prototype.GetSelectedCourseElementsFromAttachCoursesList = function()
{
	//return an array of selected objects in the Attach Courses modal window select list
	var selectedCourses = new Array();
	var attachCoursesListContainer = this.SelectAttachedCoursesListBox;
	
	for (var i = 0; i < attachCoursesListContainer.options.length; i++)
	{
		var thisOption = attachCoursesListContainer.options[i];
		
		if (thisOption.selected)
			selectedCourses.push(thisOption);
	}
	
	return selectedCourses;
}

CE_AttachCoursesInterface.prototype.AddCourseToAttachedCourses = function(courseObject)
{
	//add a course to the "Attached Courses:" box on the main page, leaving all other information already in the box as is
	//courseObject: CE_AttachCoursesInterfaceListItem {Identifier: 12345, Title: "My Course", Credits: null}

	//create an object to match the id, Name, Credits scheme
	var newCourseObject = new Object();
	newCourseObject.id = courseObject.Identifier;
	newCourseObject.Name = courseObject.Title;
	newCourseObject.Credits = courseObject.Credits;
	
	this.Parent.AttachedCoursesList.AddItem(
		newCourseObject.id,
		newCourseObject.Name,
		newCourseObject.Credits
	);
	
	//add the item to the ApplicableCourses object
	this.Parent.Parent.AddCourseToPool(newCourseObject.id, newCourseObject.Name, newCourseObject.Credits);
	
	//remove the item from the listbox
	this.RemoveCourseFromListWindow(newCourseObject.id);
	
	//remove the reference of the item from the Items internal object
	this.RemoveListBoxItem(newCourseObject.id);
}

CE_AttachCoursesInterface.prototype.GetMatchingCourseObjectById = function(id)
{
	//use passed Id to grab the matching course object from the listed objects
	for (var i = 0; i < this.Items.length; i++)
	{
		var currId = this.Items[i].Identifier;
		
		if (currId == id)
			return this.Items[i];
	}
	
	return;
}

CE_AttachCoursesInterface.prototype.DoesCourseIdExistInAttachedCourses = function(courseId)
{
	//check the "Attached Courses" box on the main page for currently attached courses
	var foundMatch = false;
	
	var attachedCoursesList = this.Parent.AttachedCoursesList.Container.getElementsByTagName("DIV");
	
	for (var i = 0; i < attachedCoursesList.length; i++)
	{
		var thisIdFull = attachedCoursesList[i].getAttribute("id");
		
		//if no id found, skip this item
		if (!thisIdFull)
			continue;
		
		//if this id isn't an attached course, skip this item
		if (thisIdFull.indexOf("AttachedCourse_") == -1)
			continue;
		
		//extract out the id number
		var thisId = thisIdFull.split("AttachedCourse_")[1];
		
		if (String(thisId) == String(courseId))
		{
			foundMatch = true;
			break;
		}
	}
	
	return foundMatch;
}

CE_AttachCoursesInterface.prototype.DoAttachCoursesSearch = function()
{
	//get what they searched for (non case-sensitive)
	var searchStr = this.SelectAttachedCoursesSearchTextBox.value;
	searchStr = String(searchStr).toLowerCase();
	
	this.SearchList = [];
	
	//check the active items for matching items 
	for (var i = 0; i < this.Data.length; i++)
	{
		var thisCourseName = String(this.Data[i].Title).toLowerCase();
		
		if (thisCourseName.indexOf(searchStr) > -1)
		{
			this.SearchList[this.SearchList.length] = this.Data[i];
		}
	}
	
	//update the modal dialog list box
	if (this.SearchList.length > 0)
	{
		this.ClearListBox();
		this.PopulateListBoxWithSearchResults(this.SearchList);
		
		//enable the list window
		this.EnableListWindow();
	}
	else
	{
		this.ShowNoSearchResultsFound();
	}
}

CE_AttachCoursesInterface.prototype.ClearSearch = function()
{
	//clear the current list box
	this.ClearListBox();
	
	//repopulate all the courses
	this.GetCourseListing();
	
	//clear the search field
	this.SelectAttachedCoursesSearchTextBox.value = "";
	
	//enable the list window
	this.EnableListWindow();
	
}

CE_AttachCoursesInterface.prototype.ShowNoSearchResultsFound = function()
{
	//clear list & insert a new list item
	this.ClearListBox();
	
	this.NoCoursesFoundOption = HTML.c(
			this, 							//parent
			"option", 						//type
			"NoCoursesFoundOption", 							//name
			null, 							//id
			null, 							//class
			this.SelectAttachedCoursesListBox, 			//appendTo
			"No courses found."										//text content
		);
	
	this.NoCoursesFoundOption.setAttribute("value", this.Interface.GetDictionaryTerm("No Courses Found"));

	//disable attaching any courses
	this.DisableListWindow();
}

CE_AttachCoursesInterface.prototype.DisableListWindow = function()
{
	//add a class to disable the listbox
	this.AddClassToElement(this.SelectAttachedCoursesListBox, "aspNetDisabled");
	
	//disable the "Attach Courses" button
	this.AddClassToElement(this.AttachCoursesModalAttachCoursesButton, "aspNetDisabled");
	this.AddClassToElement(this.AttachCoursesModalAttachCoursesButton, "DisabledButton");
	this.AttachCoursesModalAttachCoursesButton.setAttribute("disabled", "");
}

CE_AttachCoursesInterface.prototype.EnableListWindow = function()
{
	//remove the class to re-enable the listbox
	this.RemoveClassFromElement(this.SelectAttachedCoursesListBox, "aspNetDisabled");
	
	//enable the "Attach Courses" button
	this.RemoveClassFromElement(this.AttachCoursesModalAttachCoursesButton, "aspNetDisabled");
	this.RemoveClassFromElement(this.AttachCoursesModalAttachCoursesButton, "DisabledButton");
	this.AttachCoursesModalAttachCoursesButton.removeAttribute("disabled");
		
}

CE_AttachCoursesInterface.prototype.RemoveCourseFromListWindow = function(id)
{
	//at the moment a course is attached, it should disappear from the Attach Courses choosable listbox
	var optionElementList = this.SelectAttachedCoursesListBox.getElementsByTagName("OPTION");
	
	for (var i = 0; i < optionElementList.length; i++)
	{
		var thisElement = optionElementList[i];
		var thisId = thisElement.getAttribute("value");
			
		if (thisId == id)
		{
			//must remove the element by referencing the parent node
			thisElement.parentNode.removeChild(thisElement);
			break;
		}
	}
}

CE_AttachCoursesInterface.prototype.AddClassToElement = function(elem,value)
{
	//adds specified class to passed element
	this.Interface.AddClassToElement(elem,value);
}

CE_AttachCoursesInterface.prototype.RemoveClassFromElement = function(elem, value)
{
	//removes specified class from passed element and leaves any other class names as-is
	this.Interface.RemoveClassFromElement(elem,value);

}

CE_AttachCoursesInterface.prototype.GetCourseListing = function()
{
	var targetInterfaceObject = this;
	
	$.ajax({
        type: "POST",
        url: this.Interface.GetCourseListingServiceURL(),
        data: "{ searchParam: null }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
			var responseObject = response.d;
			
			//populate the list
			targetInterfaceObject.PopulateListBox(responseObject.courses);
			
			if (targetInterfaceObject.Interface.DebugMode)
			{
				console.log("Debug - GetCourseListing:");
				console.log(JSON.stringify(responseObject.courses));
			}
	},
        failure: function (response) {
            //alert(response.d);
        },
        error: function (response) {
            //alert(response.d);
        }
    });

}
