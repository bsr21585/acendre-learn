//Interface for Error Explorer; allows user to actively click thru and correct errors
CE_ErrorListerInterface = function(
	parent
	){
		
	this.Parent = parent;
	this.Interface = this.Parent;
	this.ErrorList = this.Parent.ErrorLister.ErrorList;
	
	//create body container that holds the search field, buttons, results window, and Select All/None
	this.Container = document.createElement("div");
	this.Container.className = "ErrorListerInterfaceContainer";
	
	//create the Body
	this.Body = HTML.c(
		this,						//parent
		"div",						//type
		"ErrorListerInterfaceBody",	//name
		"ErrorListerInterfaceBody", //id
		"", 						//className
		this.Container				//appendTo
	);
	
	//create the instruction text/title
	this.Instruction = HTML.c(
		this,								//parent
		"div",								//type
		"ErrorListerInterfaceInstruction",	//name
		"ErrorListerInterfaceInstruction", 	//id
		"FormFieldErrorContainer", 			//className
		this.Body		//appendTo
	);
	
	//field that holds the error list
	this.ErrorListBox = HTML.c(
		this,								//parent
		"div",								//type
		"ErrorListerInterfaceErrorListBox",	//name
		"ErrorListerInterfaceErrorListBox", //id
		"", 								//className
		this.Body							//appendTo
	);

	//Create an Update/Refresh button so they can actively update the error list
	this.RefreshButton = HTML.c(
		this,										//parent
		"input",									//type
		"ErrorListerInterfaceRefreshButton",		//name
		"ErrorListerInterfaceRefreshButton", 		//id
		"Button ActionButton", 						//classname
		this.Body									//appendTo
	);
	
	this.RefreshButton.setAttribute("type","button");
	this.RefreshButton.setAttribute("value",this.Interface.GetDictionaryTerm("Refresh"));
	
	this.RefreshButton.onclick = function()
	{
		//recheck for any errors the were/weren't corrected
		this.Parent.Interface.ValidateEntireForm();
	}
	
}

//updates the text in the header/instruction area
CE_ErrorListerInterface.prototype.UpdateHeaderText = function(headerText)
{
	this.Instruction.innerHTML = headerText;
}

//populates the on-screen UI error list from ErrorListItem objects
CE_ErrorListerInterface.prototype.PopulateErrorList = function()
{
	
	for (var i = 0; i < this.ErrorList.length; i++)
	{
		//create a new error object for each error
		var thisErrorItem = this.ErrorList[i];
		
		//the new item will append itself to the list
		var thisErrorListItem = new CE_ErrorListerInterfaceListItem(this, thisErrorItem, i);
		
		this.ErrorListBox.append(thisErrorListItem.Body);

	}
}

//clear all elements out of the error list box display
CE_ErrorListerInterface.prototype.ClearErrorList = function()
{
	this.ErrorListBox.innerHTML = "";
}

CE_ErrorListerInterface.prototype.Show = function()
{
	this.Container.style.display = "block";
}

CE_ErrorListerInterface.prototype.Hide = function()
{
	this.Container.style.display = "none";
}





//represents a list item in the ErrorListBox
CE_ErrorListerInterfaceListItem = function(
	parent,
	errorListItemObject,
	itemIndex
)
{
	this.Parent = parent;
	this.Interface = this.Parent.Interface;
	this.ErrorListItemObject = errorListItemObject;
	this.ItemIndex = itemIndex;
	this.BodyText = "";
	
	//add a number for the list item
	this.BodyText +=  String(this.ItemIndex + 1) + ") ";
	
	//add the description
	this.BodyText += this.ErrorListItemObject.ErrorNameFriendly;
	
	//create the Body
	this.Body = HTML.c(
		this,								//parent
		"div",								//type
		"ErrorListerInterfaceListItemBody",	//name
		"", //id
		"ErrorListerInterfaceListItem",		//className
		"",									//appendTo this.Parent.ErrorListBox
		this.BodyText
	);
	
	//create a click action for the div
	this.Body.onclick = function()
	{
		this.Parent.NavigateToError(this.Parent.ErrorListItemObject);
	}
}

CE_ErrorListerInterfaceListItem.prototype.NavigateToError = function(errorListItemObject)
{
	//nav to the proper tab
	var thisTabName = this.ErrorListItemObject.TabName;
	
	this.Interface.Tabs.SetTab(thisTabName);
	
	//if a UnitObject exists, navigate to the Unit in question in the AsentiaCarousel
	if (this.ErrorListItemObject.UnitObject)
	{
		//get the target AsentiaCarousel
		var thisUnitsCarousel = this.Interface.GetTabFormObjectByName(thisTabName).UnitsCarousel
		
		//navigate to the index of the Unit in the carousel
		try
		{
			var thisUnitIndexInCarousel = this.ErrorListItemObject.UnitObjectIndexInCarousel;
			thisUnitsCarousel.GoToItemAtIndex(thisUnitIndexInCarousel);
		}
		catch(e){};
	}
	
	//if a SegmentObject exists, navigate to the Segment in question in the AsentiaCarousel
	if (this.ErrorListItemObject.SegmentObject)
	{
		//get this target AsentiaCarousel
		var thisSegmentsCarousel = this.ErrorListItemObject.UnitObject.SegmentsCarousel;
		
		//navigate to the index of the Segment in the AsentiaCarousel
		try
		{
			var thisSegmentIndexInCarousel = this.ErrorListItemObject.SegmentObjectIndexInCarousel;
			thisSegmentsCarousel.GoToItemAtIndex(this.ErrorListItemObject.SegmentObjectIndexInCarousel);
			
			//scroll to the bottom of the page (where segments are located)
			window.scrollTo(0,document.body.scrollHeight);
		}catch(e){};
	}
	
	//if this is a missing course error, navigate to the position of the object
	if (this.ErrorListItemObject.ErrorName == "COURSE_DOES_NOT_EXIST")
	{
		//find the top position of the element in the Attached Courses box so we can auto-scroll to it
		try
		{
			//get the container (contains page html element) of the target object
			var courseListElement = errorListItemObject.TargetObject.Container;
			
			//get the position of the element in its containing div
			var scrollPosition = courseListElement.offsetTop;
			//console.log(scrollPosition);
			
			//target the Attach Courses scrolling div and scroll it accordingly
			this.Interface.GetTabFormObjectByName("properties").AttachedCoursesList.Container.scrollTop = scrollPosition;
		}
		catch(e){};
		
		//scroll to the bottom of the page (where Attached Courses are located)
		window.scrollTo(0,document.body.scrollHeight);
	}
}
