CE_UnitInterface = function(
	parent,
	identifier,
	node,
	languages,
	initialLanguage,
	flagImagePath,
	deleteImagePath,
	addImagePath,
	data
	){
	
	this.Parent		 		= parent;
	this.Identifier			= identifier,
	this.Node				= node;
	this.Languages			= languages;
	this.InitialLanguage	= initialLanguage;
	this.FlagImagePath		= flagImagePath;
	this.DeleteImagePath 	= deleteImagePath;
	this.AddImagePath		= addImagePath;
	this.Data				= data;
	
	this.Interface = this.Parent.Parent;
	
	this.TypeName 			= "UnitInterface";
	
	try{
		this.DBID = this.Data.id;
	} catch(e){	}
	
	this.SegmentCounter = 0;
	
	this.Panel = document.createElement("div");
		this.Panel.className = "UnitContainer";		
		this.Panel.id = this.Identifier;
		
	/* Delete Button */
	this.DeleteButton = new IconButton(
		this,
		this.Panel,
		this.DeleteImagePath,
		this.Interface.GetDictionaryTerm("Delete Unit"),
		true, 
		"DeleteUnitButton"
		)
		
	this.DeleteButton.onclick = function(){
		if (!this.Disabled){
			this.Parent.Parent.PromptToDeleteUnit(this.Parent.Panel);
		}
	}
	
	/* Heading */
	
	this.Heading = document.createElement("div");
		this.Heading.className = "Title";
		this.Heading.append(HTML.text(this.Interface.GetDictionaryTerm("Unit")));
	
	this.HidePanelButton = document.createElement("input");
		this.HidePanelButton.Parent = this;
		this.HidePanelButton.className = "HidePanelButton Open";
		this.HidePanelButton.setAttribute("type", "button");
		this.HidePanelButton.onclick = function(){
			this.Parent.ToggleHideablePanel();
		}
	
	this.Heading.append(this.HidePanelButton);
	this.Panel.append(this.Heading);
	
	this.HideablePanel = document.createElement("div");
		this.HideablePanel.className = "UnitHidePanel"
		
	try{ var localTitle = this.Data.Titles;	}catch(e){ var localTitle = "";}
	 
	/* INPUT Fields */
	
	this.Title = new FormInputField(
		this, 					// parent
		this.HideablePanel,				// attach to
		"input",				// type
		this.Interface.GetDictionaryTerm("Unit Title"),  		// label
		true, 					// isRequired
		this.Languages, 		// languages
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		localTitle,				// JSON data
		"InputLong",			// input class
		this.Interface.GetDictionaryTerm("New Unit")			// default label
	)
	
	try{ var localDescriptions = this.Data.Descriptions;	}catch(e){ var localDescriptions = "";}
	
	this.Description = new FormInputField(
		this, 					// parent
		this.HideablePanel,	// attach to
		"textarea",				// type
		this.Interface.GetDictionaryTerm("Description"),  		// label
		true, 					// isRequired
		this.Languages, 		// languages
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		localDescriptions,		// JSON data or string
		"",						// input class
		""						// default label
	)
	
	this.Panel.append(this.HideablePanel);
	
	/* SEGMENTS */
	
	try {this.SegmentsIsAny = this.Data.Segments.isAny;}catch(e){this.SegmentsIsAny = false}
	
	this.Segments = [];
	this.SegmentsContainer = document.createElement("div");
		this.SegmentsContainer.className = "FormFieldContainer";
		
		this.SegmentsLabelContainer = document.createElement("div");
			this.SegmentsLabelContainer.className = "FormFieldLabelContainer";
		
		this.SegmentsInputContainer = document.createElement("div");
			//this.SegmentsInputContainer.className = "FormFieldInputContainer";
			
	// Segments Title
	
	try{ var localSegmentCount = this.Data.Segments.Items.length;	}catch(e){ var localSegmentCount = "0";}
	
	this.SegmentsPanelTitle = document.createElement("div");
		this.SegmentsPanelTitle.className = "Title";
	this.SegmentsCount = document.createElement("span");
		this.SegmentsCount.className = "SegmentCount";
		this.SegmentsCount.append(HTML.text(localSegmentCount));
		
	this.SegmentsPanelTitle.append(this.SegmentsCount);
	//this.SegmentsPanelTitle.append(HTML.text(" Segment(s):"));
	this.SegmentsPanelTitle.append(HTML.text(" " + this.Interface.GetDictionaryTerm("Segment(s)")));
	
	this.SegmentsLabelContainer.append(this.SegmentsPanelTitle);
	
	// attach label and input 
	this.SegmentsContainer.append(this.SegmentsLabelContainer);
	this.SegmentsContainer.append(this.SegmentsInputContainer);
	
		/* ADD SEGMENT Button */
	
		this.AddSegmentButton = new IconButton(
			this,
			this.SegmentsPanelTitle,
			this.AddImagePath,
			this.Interface.GetDictionaryTerm("Add a Segment"),
			true, 
			"AddItemButton"
			)
		
		this.AddSegmentButton.onclick = function(){
			this.Parent.NewSegmentInterface("");
			this.Parent.SegmentsCarousel.GoToLastItem();
			
			//update any Delete buttons that need to be enabled/disabled
			this.Parent.UpdateDeleteButtonEnabledOnAllSegments();
		}
	
		/* SEGMENTS Navigation / Dots */
	
		this.SegmentsNavigationContainer = document.createElement("div");
			this.SegmentsNavigationContainer.className = "SegmentNavigationContainer"
			this.SegmentsInputContainer.append(this.SegmentsNavigationContainer);
		
		/* Carousel */
		
		this.SegmentsCarousel = new AsentiaCarousel(
			this.Identifier + "-Segment-" + this.Segments.length,  
			this.SegmentsInputContainer
			);
		
		this.SegmentsCarousel.Panel.style.width = "calc(100% - 120px)";
		this.SegmentsCarousel.Panel.style.marginLeft = "60px";
		this.SegmentsCarousel.Panel.style.marginRight = "60px";
		
		// Add Segments from Data
		try{
			for (var i = 0; i < this.Data.Segments.Items.length; i++){
				this.NewSegmentInterface(this.Data.Segments.Items[i]);
			}
		} catch(e){} // do not create empty segments
		
		// Configure the Carousel Navigation
		
		var pButtonContainer = document.createElement("div");
			pButtonContainer.className = "SegmentCarouselButtonContainer";
			pButtonContainer.style.float = "left";
			pButtonContainer.append(this.SegmentsCarousel.PreviousItemButton.Button);
			this.SegmentsCarousel.PreviousItemButton.Button.style.left = "2px";
			
		var nButtonContainer = document.createElement("div");
			nButtonContainer.className = "SegmentCarouselButtonContainer";
			nButtonContainer.style.float = "right";
			nButtonContainer.append(this.SegmentsCarousel.NextItemButton.Button);
			this.SegmentsCarousel.NextItemButton.Button.style.right = "2px";
			
		this.SegmentsNavigationContainer.append(pButtonContainer);
		this.SegmentsNavigationContainer.append(nButtonContainer);
		//this.SegmentsNavigationContainer.append(this.SegmentsCarousel.Dots.Panel); no dots
		
		this.Panel.append(this.SegmentsContainer);
	
	// finally append the whole sh-bang.
	
	try{
		node.append(this.Panel);
	} catch(e){}
	
	// add a final clearfix element
	var clearfix = document.createElement("div");
		clearfix.className = "clearfix";
	
	this.SegmentsContainer.append(clearfix);
	
	
	//update any Delete buttons that need to be enabled/disabled
	this.UpdateDeleteButtonEnabledOnAllSegments();
}

CE_UnitInterface.prototype.ToggleSegmentsAndOr = function(){
	
	this.SegmentsIsAny = !this.SegmentsIsAny;
	
	for (var i = 0; i < this.SegmentsCarousel.Items.length; i++){
		if(this.SegmentsCarousel.Items[i].childNodes[0].className == "AndOr"){
			var ao = this.SegmentsCarousel.Items[i].childNodes[0];
			ao.removeChild(ao.childNodes[0]);
			ao.append(HTML.text(  (this.SegmentsIsAny) ? this.Interface.GetDictionaryTerm("OR") : this.Interface.GetDictionaryTerm("AND") ));
		}
	}
	
}



CE_UnitInterface.prototype.ToggleHideablePanel = function(){
	
	if (!this.HideablePanel.TrueHeight){
		this.HideablePanel.TrueHeight = this.HideablePanel.offsetHeight;
		this.HideablePanel.style.height = this.HideablePanel.TrueHeight.toString() + "px";
	}
	
	this.HideablePanel.style.overflowY = "hidden";
	
	if (this.HideablePanel.offsetHeight == 0){
		this.HideablePanel.style.height = this.HideablePanel.TrueHeight.toString() + "px";
		this.HidePanelButton.className = "HidePanelButton Open";
	}else{
		this.HideablePanel.style.height = "0px";
		this.HidePanelButton.className = "HidePanelButton Closed";
	}
	
}

CE_UnitInterface.prototype.UpdateSegmentsTitleCount = function(){
	
	// update the counter on the page
	this.SegmentsCount.removeChild(this.SegmentsCount.childNodes[0]);
	this.SegmentsCount.append(HTML.text(this.Segments.length));
	
}

CE_UnitInterface.prototype.NewSegmentInterface = function(
	data
	){
	
	this.Segments.push(new CE_SegmentInterface(
		this,
		this.Identifier + "-Segment" + this.SegmentCounter,
		null, 
		this.Languages,						// languages
		this.InitialLanguage,				// initialLanguage
		this.FlagImagePath,					// path to flag images
		this.DeleteImagePath,				// path to delete image
		this.AddImagePath,					// path to delete image
		data								// JSON data
		));

	this.UpdateSegmentsTitleCount();
	
	this.SegmentCounter++;
	
	if (this.SegmentsCarousel.Items.length != 0){
		this.SegmentsCarousel.AttachItem(new CE_SegmentAndOr(this).Container, 100/14, true);
		this.SegmentsCarousel.AttachItem(this.Segments[this.Segments.length - 1].Panel, 100/3.5);
	}else{
		this.SegmentsCarousel.AttachItem(this.Segments[this.Segments.length - 1].Panel, 100/3.5);
	}
	
	return this.Segments[this.Segments.length - 1].Panel;
	
}

CE_UnitInterface.prototype.DeleteSegment = function(segmentContainerElement){

	var index = -1;
	
	// find the segment UI on the page
	for (var i = 0; i < this.Segments.length; i++){
		if (this.Segments[i].Panel.id == segmentContainerElement.id) {index = i;}
	}
	
	// not found, exit
	if (index == -1)
		return true;
	
	// remove from the carousel; because of and/or buttons, the segment on the carousel will be index * 2
	this.SegmentsCarousel.RemoveItemAtIndex(index * 2);
	
	//if removing any other segment other than first, also remove the and/or button preceding it
	if (index > 0)
		this.SegmentsCarousel.RemoveItemAtIndex((index * 2) - 1);
		
	//if removing the first segment, also remove the and/or button trailing it
	else if (index == 0)
		this.SegmentsCarousel.RemoveItemAtIndex(0);
	
	//this.SegmentsCarousel.RemoveSegmentAtIndex(index);
	
	// remove from the array
	this.Segments.splice(index, 1);
	
	this.UpdateSegmentsTitleCount();
	
	//update any Delete buttons that need to be enabled/disabled
	this.UpdateDeleteButtonEnabledOnAllSegments();
	
	this.FixSegmentsCarouselAfterSegmentDelete();
	
	
	// disable the delete unit button from index 0 if its the last unit remaining
	//alert("still needs work - DeleteSegment");
	//if (this.GetEffectiveUnitCount() == 1){
	//	for (var i = 0; i < this.Segments.length; i++){
	//		this.Segments[i].DeleteButton.Enable(false);
	//	}
	//}
	
	
}

CE_UnitInterface.prototype.FixSegmentsCarouselAfterSegmentDelete = function()
{
	//correct any inconsistencies in the Segments carousel
	
	var totalSegmentCarouselItems = ((this.Segments.length - 1) * 2) + 1;
	this.SegmentsCarousel.UpdateLastViewableIndex();
	this.SegmentsCarousel.PreviousItemButton.Enable(this.SegmentsCarousel.GetEffectiveItemIndex() > 0 || this.SegmentsCarousel.ActualItemIndex > 0);
	if (this.SegmentsCarousel.ItemIndexQueuedForDelete == 0)
		this.SegmentsCarousel.PreviousItemButton.Enable(false);
	this.SegmentsCarousel.NextItemButton.Enable(this.SegmentsCarousel.ActualItemIndex + 5 < totalSegmentCarouselItems);
	
	//console.log(this.SegmentsCarousel.GetEffectiveItemIndex());
	//console.log(this.SegmentsCarousel);
	
}

CE_UnitInterface.prototype.PromptToDeleteSegment = function(segmentContainerElement){
	/*
	headerText,
	messageText,
	confirmButtonText,
	cancelButtonText,
	confirmButtonActionObject,
	cancelButtonActionObject
	*/
	this.Parent.Parent.ShowAlertModal(
	 this.Interface.GetDictionaryTerm("Delete Segment"),
	 this.Interface.GetDictionaryTerm("Are you sure you want to delete this segment"),//"Are you sure you want to delete this segment?"
	 this.Interface.GetDictionaryTerm("Yes"),
	 this.Interface.GetDictionaryTerm("No"),
	 {targetObject: this, functionName: "DeleteSegment", functionParam: segmentContainerElement}
	);
}

CE_UnitInterface.prototype.DeleteRequirementsWithNoCoursesOnAllSegments = function()
{
	for (var i = 0; i < this.Segments.length; i++)
	{
		this.Segments[i].DeleteRequirementsWithNoCourses();
	}
}

CE_UnitInterface.prototype.DeleteAllSegmentsWithNoRequirements = function()
{
	//deletes any Segment that has no current Requirements on it
	var segmentIdsToDelete = new Array();
	
	//get the segment Ids to be deleted
	for (var i = 0; i < this.Segments.length; i++)
	{
		var thisSegmentRequirementList = this.Segments[i].GetRequirementsList();
		//console.log("***THIS SEGMENTS REQMT LIST***");
		//console.log(thisSegmentRequirementList);
		
		if (thisSegmentRequirementList.length == 0)
			segmentIdsToDelete.push(this.Segments[i].GetId());
	}
	
	//console.log("***SEGMENT IDS TO REMOVE***");
	//console.log(segmentIdsToDelete[0]);
	
	//delete the segments
	for (var i = 0; i < segmentIdsToDelete.length; i++)
	{
		var currentSegmentId = segmentIdsToDelete[i];
		var segmentToDelete = this.GetSegmentById(currentSegmentId);
		
		this.DeleteSegment(segmentToDelete.Panel);
	}
}

CE_UnitInterface.prototype.GetSegmentById = function(id)
{
	//returns a CE_SegmentInterface object by id
	for (var i = 0; i < this.Segments.length; i++)
	{
		if (this.Segments[i].GetId() == id)
			return this.Segments[i];
	}
	
	//return undefined if nothing found
	return;
}

CE_UnitInterface.prototype.UpdateDeleteButtonEnabled = function()
{
	//Delete button should be disabled if this is the only Unit left on a Tab
	if (this.Parent.Units)
	{
		if (this.Parent.Units.length < 2)
		{
			this.DeleteButton.Enable(false);
		}
		else
		{
			this.DeleteButton.Enable(true);
		}
	}
}

CE_UnitInterface.prototype.UpdateDeleteButtonEnabledOnAllSegments = function()
{
	//goes thru each Segment to determine if its Delete button (X) should be enabled/disabled
	for (var i = 0; i < this.Segments.length; i++)
	{
		this.Segments[i].UpdateDeleteButtonEnabled();
	}
}

CE_UnitInterface.prototype.IsValidOrShowErrors = function()
{
	//validate the required fields on this tab
	this.InErrorState = false;
	
	//hide any previous errors
	this.Title.ClearError();
	this.Description.ClearError();
	
	var tabName = this.Parent.TabName;
	
	if (this.Title.IsInitialLanguageFieldEmpty())
	{
		this.Title.ShowError(this.Interface.GetDictionaryTerm("Title is required in the default language"));
		this.InErrorState = true;
		
		//store info about the error to the error lister
		this.Interface.AddToErrorList(
			tabName,
			this.Title,
			this.Interface.ErrorLister[tabName.toUpperCase() + "_UNIT_TITLE_REQUIRED"],
			this
		);
	}
	
	if (this.Description.IsInitialLanguageFieldEmpty())
	{
		this.Description.ShowError(this.Interface.GetDictionaryTerm("Description is required in the default language"));
		this.InErrorState = true;
		
		//store info about the error to the error lister
		this.Interface.AddToErrorList(
			tabName,
			this.Description,
			this.Interface.ErrorLister[tabName.toUpperCase() + "_UNIT_DESCRIPTION_REQUIRED"],
			this
		);
	}
	
	//Unit must have at least one segment
	this.ClearError();
	if (this.Segments.length == 0)
	{
		this.ShowError(this.Interface.GetDictionaryTerm("at least one segment is required"));
		this.InErrorState = true;
		
		//store info about the error to the error lister
		this.Interface.AddToErrorList(
			tabName,
			this,
			this.Interface.ErrorLister[tabName.toUpperCase() + "_UNIT_NO_SEGMENTS"],
			this //Unit
		);
	}
	
	//call down to all existing Segments to check that the Label field is filled for default language
	for (var i = 0; i < this.Segments.length; i++)
	{
		if (!this.Segments[i].IsValidOrShowErrors())
			this.InErrorState = true;
	}
	
	return !this.InErrorState;
}


CE_UnitInterface.prototype.ShowError = function(s){
	
	this.ClearError();
	
	this.FormFieldErrorContainer = document.createElement("div");
		this.FormFieldErrorContainer.className = "FormFieldErrorContainer";
	var p = document.createElement("p");
		p.append(document.createTextNode(s));
	
	this.FormFieldErrorContainer.append(p);
	this.SegmentsContainer.insertBefore(this.FormFieldErrorContainer, this.SegmentsInputContainer);
	
}

CE_UnitInterface.prototype.ClearError = function(){

	// remove the existing error
	try{
		this.SegmentsContainer.removeChild(this.FormFieldErrorContainer);
	} catch(e){}

}

CE_UnitInterface.prototype.GetDataAsString = function()
{
	var s = "";
	
	s += "{";
	
    //Id
	if (this.Data.id != null) {
	    s += "\"id\":" + this.Data.id + ",";
	}
	else {
	    s += "\"id\":0,";
	}
		
	//titles & descriptions
	s += "\"Titles\":" + this.Title.GetData() + ",";
	s += "\"Descriptions\":" + this.Description.GetData() + ",";

	//Segments
	s += "\"Segments\":{";

	if (this.SegmentsIsAny != null) {
	    s += "\"isAny\":" + this.SegmentsIsAny + ",";
	}
	else {
	    s += "\"isAny\":false,";
	}
	
	s += "\"Items\":[";
	
	//get Segment data
	for (var i = 0; i < this.Segments.length; i++)
	{
		//add separator for subsequent items
		if (i > 0)
			s += ",";
			
		s += this.Segments[i].GetDataAsString();
	}
	
	//close Items
	s += "]";
	
	//close Segments
	s += "}";
	
	//close the entire Object
	s += "}"
	
	return s;
}

CE_UnitAndOr = function(
	parent
	){
		
	this.Parent = parent;
	this.TypeName = "UnitAndOr";
	
	this.Container = document.createElement("div");
		this.Container.TypeName = "TheUnitContainer";
		this.Container.Parent = this;
		this.Container.className = "UnitAndOr";
	
	if (this.Parent.UnitsIsAny == true){
		this.Container.append(HTML.text(this.Parent.Parent.GetDictionaryTerm("OR")));
	}else {
		this.Container.append(HTML.text(this.Parent.Parent.GetDictionaryTerm("AND")));
	}
	
	this.Container.onclick = function(){
		this.Parent.Parent.ToggleUnitsAndOr();
	}
	
	return this.Container;
}