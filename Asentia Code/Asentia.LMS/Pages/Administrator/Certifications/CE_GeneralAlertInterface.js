CE_GeneralAlertInterface = function(
	modal,
	parent,
	messageText,
	confirmButtonText,				//OPTIONAL (default: "OK")
	cancelButtonText,				//OPTIONAL (default: no cancel button appears)
	confirmButtonActionObject,		//OPTIONAL (default: closes the modal)
	cancelButtonActionObject		//OPTIONAL (default: closes the modal)
	){
	
	this.Modal = modal;
	this.TypeName = "GeneralAlertInterface";
	this.Parent = parent;
	this.MessageText = messageText;
	this.ConfirmButtonText = confirmButtonText ? confirmButtonText : this.Parent.Parent.GetDictionaryTerm("OK");
	this.CancelButtonText = cancelButtonText;
	//this.CancelButtonText = cancelButtonText ? cancelButtonText : "Cancel";
	this.ConfirmButtonActionObject = confirmButtonActionObject;
	this.CancelButtonActionObject = cancelButtonActionObject;
	
	//create body container that holds the search field, buttons, results window, and Select All/None
	this.Body = document.createElement("div");
	this.Body.className = "ModalPopupBody";
	
	//paragraph that will display the message for the alert
	this.GeneralAlertMessageModalPopupParagraph = HTML.c(
		this,
		"div",
		"GeneralAlertMessageModalPopupParagraph",
		"GeneralAlertMessageModalPopupParagraph",
		null,
		this.Body,
		this.MessageText
	);
	
	//bottom separator for the text
	this.GeneralAlertMessageModalPopupSeparator = HTML.c(
		this,
		"div",
		"GeneralAlertMessageModalPopupSeparator",
		"GeneralAlertMessageModalPopupSeparator",
		"ModalPopupSeparator",
		this.Body
	);
	this.GeneralAlertMessageModalPopupSeparator.innerHTML = "&nbsp;";
	
	//button holder
	this.GeneralAlertMessageModalPopupButtonHolder = HTML.c(
		this,
		"div",
		"GeneralAlertMessageModalPopupButtonHolder",
		"GeneralAlertMessageModalPopupButtonHolder",
		"ModalPopupButtons",
		this.Body
	);
	
	//'Confirm' button
	this.GeneralAlertMessageModalPopupConfirmButton = HTML.c(
		this,
		"input",
		"GeneralAlertMessageModalPopupConfirmButton",
		"GeneralAlertMessageModalPopupConfirmButton", 
		"Button ActionButton", 
		this.GeneralAlertMessageModalPopupButtonHolder
	);
	
	this.GeneralAlertMessageModalPopupConfirmButton.setAttribute("type","button");
	this.GeneralAlertMessageModalPopupConfirmButton.setAttribute("value", this.ConfirmButtonText);
	
	this.GeneralAlertMessageModalPopupConfirmButton.onclick = function()
	{
		try
		{
			var targetObject = this.Parent.ConfirmButtonActionObject.targetObject;
			var functionName = this.Parent.ConfirmButtonActionObject.functionName;
			var functionParam = this.Parent.ConfirmButtonActionObject.functionParam;
			
			if (functionParam)
				targetObject[functionName](functionParam);
			else
				targetObject[functionName]();
		}
		catch(e)
		{
			//No custom action defined
			//console.log(e);
		}
		
		//this.Parent.Modal.Panel.style.visibility = "hidden";
		this.Parent.Modal.Close();
		
		//hide the container div
		this.Parent.Parent.ShowAsentiaModalAlertContainer(false);
	}
	
	//'Cancel' button; If Cancel button text is empty string or null, don't add this button. Suitable for when creating an alert that only needs one button, i.e. OK
	if (this.CancelButtonText != "" && this.CancelButtonText != null && this.CancelButtonText != undefined)
	{
		this.GeneralAlertMessageModalPopupCancelButton = HTML.c(
			this,
			"input",
			"GeneralAlertMessageModalPopupCancelButton",
			"GeneralAlertMessageModalPopupCancelButton", 
			"Button NonActionButton", 
			this.GeneralAlertMessageModalPopupButtonHolder
		);
		
		this.GeneralAlertMessageModalPopupCancelButton.setAttribute("type","button");
		this.GeneralAlertMessageModalPopupCancelButton.setAttribute("value", this.CancelButtonText);
		
		this.GeneralAlertMessageModalPopupCancelButton.onclick = function()
		{
			if (this.Parent.CancelButtonActionObject)
			{
				try
				{
					var targetObject = this.Parent.CancelButtonActionObject.targetObject;
					var functionName = this.Parent.CancelButtonActionObject.functionName;
					var functionParam = this.Parent.CancelButtonActionObject.functionParam;
					
					if (functionParam)
						targetObject[functionName](functionParam);
					else
						targetObject[functionName]();
				}
				catch(e)
				{
					//General Alert: Confirm Button Action Error
					//console.log(e);
				}
				
				//this.Parent.Modal.Panel.style.visibility = "hidden";
				this.Parent.Modal.Close();
				
				//hide the container div
				this.Parent.Parent.ShowAsentiaModalAlertContainer(false);
			}
			else
			{
				//default 'cancel' action if none is defined
				//this.Parent.Modal.Panel.style.visibility = "hidden";
				this.Parent.Modal.Close();
				
				//hide the container div
				this.Parent.Parent.ShowAsentiaModalAlertContainer(false);
			}
		}
	}
	
	//show the container div
	this.Parent.ShowAsentiaModalAlertContainer(true);
		
}
