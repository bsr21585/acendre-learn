﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.LMS.Pages.Administrator.Certifications
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel CertificationsFormContentWrapperContainer;
        public UpdatePanel CertificationGridUpdatePanel;
        public Grid CertificationGrid;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmAction;
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (
                !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE)
                || !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CertificationsManager)
               )
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/certifications/Default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Certifications));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, _GlobalResources.Certifications, ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CertificationsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.CertificationGrid.BindData();
            }
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD CERTIFICATION
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddCertificationLink",
                                                null,
                                                "Modify.aspx",
                                                null,
                                                _GlobalResources.NewCertification,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the certification grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // apply css class to container
            this.CertificationGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.CertificationGrid.StoredProcedure = Library.Certification.GridProcedure;
            this.CertificationGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.CertificationGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.CertificationGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.CertificationGrid.IdentifierField = "idCertification";
            this.CertificationGrid.DefaultSortColumn = "title";
            this.CertificationGrid.SearchBoxPlaceholderText = _GlobalResources.SearchCertifications;

            // data key names
            this.CertificationGrid.DataKeyNames = new string[] { "idCertification" };

            // columns
            GridColumn name = new GridColumn(_GlobalResources.Name + "", null, "title"); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.CertificationGrid.AddColumn(name);
            this.CertificationGrid.AddColumn(options);

            // add row data bound event
            this.CertificationGrid.RowDataBound += _CertificationGrid_RowDataBound;
        }
        #endregion

        #region _CertificationGrid_RowDataBound
        private void _CertificationGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idCertification = Convert.ToInt32(rowView["idCertification"]);      

                // AVATAR, NAME

                string name = Server.HtmlEncode(rowView["title"].ToString());

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = name;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink nameLink = new HyperLink();
                nameLink.NavigateUrl = "Dashboard.aspx?id=" + idCertification.ToString();
                nameLink.Text = name;
                nameLabel.Controls.Add(nameLink);

                // OPTIONS COLUMN

                // modify
                Label modifySpan = new Label();
                modifySpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(modifySpan);

                HyperLink modifyLink = new HyperLink();
                modifyLink.NavigateUrl = "Modify.aspx?id=" + idCertification.ToString();
                modifySpan.Controls.Add(modifyLink);

                Image modifyIcon = new Image();
                modifyIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
                modifyIcon.AlternateText = _GlobalResources.Modify;
                modifyIcon.ToolTip = _GlobalResources.CertificationProperties;
                modifyLink.Controls.Add(modifyIcon);

                // auto-join rules
                Label autojoinRulesSpan = new Label();
                autojoinRulesSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(autojoinRulesSpan);

                HyperLink autojoinRulesLink = new HyperLink();
                autojoinRulesLink.NavigateUrl = "/administrator/certifications/AutoJoinRules.aspx?gid=" + idCertification.ToString();
                autojoinRulesSpan.Controls.Add(autojoinRulesLink);

                Image autojoinRulesIcon = new Image();
                autojoinRulesIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYNCHRONIZE_BUTTON, ImageFiles.EXT_PNG);
                autojoinRulesIcon.AlternateText = _GlobalResources.AutoJoinRules;
                autojoinRulesIcon.ToolTip = _GlobalResources.AutoJoinRules;
                autojoinRulesLink.Controls.Add(autojoinRulesIcon);

                // email notifications
                Label emailNotificationsSpan = new Label();
                emailNotificationsSpan.CssClass = "GridImageLink";
                e.Row.Cells[2].Controls.Add(emailNotificationsSpan);

                HyperLink emailNotificationsLink = new HyperLink();
                emailNotificationsLink.NavigateUrl = "/administrator/certifications/emailnotifications/Default.aspx?cid=" + idCertification.ToString();
                emailNotificationsSpan.Controls.Add(emailNotificationsLink);

                Image emailNotificationsIcon = new Image();
                emailNotificationsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL_BUTTON, ImageFiles.EXT_PNG);
                emailNotificationsIcon.AlternateText = _GlobalResources.EmailNotifications;
                emailNotificationsIcon.ToolTip = _GlobalResources.EmailNotifications;
                emailNotificationsLink.Controls.Add(emailNotificationsIcon);
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedCertification_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedCertification_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseCertification_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.CertificationGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.CertificationGrid.Rows[i].FindControl(this.CertificationGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.Certification.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedCertification_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoCertification_sSelectedForDeletion, true);
                }                
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.CertificationGrid.BindData();
            }
        }
        #endregion
    }
}
