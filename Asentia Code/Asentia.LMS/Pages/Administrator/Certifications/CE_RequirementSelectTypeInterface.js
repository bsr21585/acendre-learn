CE_RequirementSelectTypeInterface = function(
	modal
	){
	
	this.Modal = modal;
	this.TypeName = "RequirementSelectTypeInterface";
	
	this.Body = document.createElement("div");
		this.Body.className = "ModalPopupBody";
		
	this.SelectTypeContainer = new FormInputField(
		this, 			//object
		this.Body,				// DOM node to append to
		"custom",				// type (input|textarea)
		this.Modal.Parent.Interface.GetDictionaryTerm("Select the type of requirement"),  			// text
		true, 		// bit
		[], 			// array
		"",	// initialLanguage
		"",		// path to flag images
		"", 				// JSON
		"",			// className to add to the input
		""		// default label if unable to extract
		)
	
	this.Form = HTML.c(this, "form");
		
	this.CourseChoice = new HTMLRadioOptionWithLabel(
		"RequirementType", 
		"RequirementType_Course",  
		"course", 
		this.Modal.Parent.Interface.GetDictionaryTerm("The learner must complete specific course(s)"), //"The learner must complete specific course(s)."
		true,
		false,
		"ToggleInput"
		);
		
	this.CreditsChoice = new HTMLRadioOptionWithLabel(
		"RequirementType", 
		"RequirementType_Credits",  
		"credit", 
		this.Modal.Parent.Interface.GetDictionaryTerm("The learner must earn enough course credits"), //"The learner must earn enough course credits."
		false,
		false,
		"ToggleInput"
		);
		
	this.TaskChoice = new HTMLRadioOptionWithLabel(
		"RequirementType", 
		"RequirementType_Task",  
		"task", 
		this.Modal.Parent.Interface.GetDictionaryTerm("The learner must complete an external task"), //"The learner must complete an external task."
		false,
		false,
		"ToggleInput"
		);
	
	this.Form.append(this.CourseChoice.Container);
	this.Form.append(this.CreditsChoice.Container);
	this.Form.append(this.TaskChoice.Container);
	
	this.SelectTypeContainer.FormFieldInputContainer.append(this.Form);
		
	// do not attach. 
	this.Separator = document.createElement("div");
		this.Separator.className = "ModalPopupSeparator";
		
	// do not attach
	this.ButtonContainer = document.createElement("div");
		this.ButtonContainer.className = "ModalPopupButtons";
	
	this.OKButton = document.createElement("input");
		this.OKButton.Parent = this;
		this.OKButton.type = "button";
		this.OKButton.className = "Button ActionButton";
		this.OKButton.value = this.Modal.Parent.Interface.GetDictionaryTerm("Continue");
		
		this.OKButton.onclick = function(){
			switch(this.Parent.Form.elements["RequirementType"].value){
				case "course":
					this.Parent.Modal.Parent.ChooseCourse();
					break;
				case "credit":
					this.Parent.Modal.Parent.ChooseCredit();
					break;
				case "task":
					this.Parent.Modal.Parent.ChooseTask();
					break;
			}
		}
		
	this.CancelButton = document.createElement("input");
		this.CancelButton.Parent = this;
		this.CancelButton.type = "button";
		this.CancelButton.className = "Button NonActionButton";
		this.CancelButton.value = this.Modal.Parent.Interface.GetDictionaryTerm("Cancel");
		
		this.CancelButton.onclick = function(){
			//this.Parent.Modal.Panel.style.visibility = "hidden";
			this.Parent.Modal.Close();
			
			//hide the containing window
			this.Parent.Modal.Parent.Interface.ShowAsentiaModalContainer(false);
		}
		
	this.ButtonContainer.append(this.OKButton);
	this.ButtonContainer.append(this.CancelButton);
	

	}
