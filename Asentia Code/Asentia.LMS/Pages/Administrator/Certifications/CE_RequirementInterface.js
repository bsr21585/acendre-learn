CE_RequirementInterface = function(
	parent,
	identifier,
	node,
	languages,
	initialLanguage,
	flagImagePath,
	deleteImagePath,
	addImagePath, 
	data
	
	){
	
	this.TypeName = "RequirementInterface";
	
	if (typeof data == "object"){
		this.Data = data; 
	}else{
	}
	
	
	this.Parent = parent;
	this.Identifier = identifier;
	this.Languages = languages;
	this.InitialLanguage = initialLanguage;
	this.FlagImagePath = flagImagePath;
	this.DeleteImagePath = deleteImagePath;
	this.AddImagePath = addImagePath;
	this.Interface = this.Parent.Parent.Parent.Parent; // root interface
	this.Index = -1; // will be set when appropriate
	
	this.Container = HTML.c(this, "div");
	
	//this.SwapContainer = HTML.c(this, "div");
		//this.SwapContainer.className = "SwapContainer";
		
		this.Container.Parent = this;
		this.Container.className = "RequirementClickContainer";
		this.Container.id = this.Data.id;
		this.Container.onclick = function(){
			
			//show the modal container
			this.Parent.Interface.ShowAsentiaModalContainer(true);
			
			// launch the modal
			this.Parent.ModalWindow = new Asentia_ModalWindow(
				this.Parent.Parent,
				this.Parent.Interface.ModalDOMId, 		//attach to node
				this.Parent.Interface.GetDictionaryTerm("New Requirement"), 
				"asentia-modal-container", //this.Parent.Panel.id,  				// containment
				this.Parent.DeleteImagePath									// close button image path
				)
			
			//override the close button function to add extra functionality to it	
			this.Parent.ModalWindow.CloseButton.onclick = function()
			{
				this.Parent.Close();
				
				//hide the containing window
				this.Parent.Parent.Interface.ShowAsentiaModalContainer(false);
			}
			
			// create the interface
			
			switch(this.Parent.Data.Type){
				case "credit":
					this.Parent.ModalWindow.SetTitle(this.Parent.Interface.GetDictionaryTerm("Requirement Earn Credit(s)"));//"Requirement: Earn Credit(s)"
					this.Parent.EditWindowInterface = new CE_CreditRequirementInterface(
						this.Parent,
						this.Parent.ModalWindow,
						this.Parent.Identifier + "-CreditWindow",
						this.Parent.Languages,
						this.Parent.InitialLanguage,
						this.Parent.FlagImagePath,
						this.Parent.DeleteImagePath,
						this.Parent.AddImagePath,
						this.Parent.Data
						);
					break;
				case "course":
					this.Parent.ModalWindow.SetTitle(this.Parent.Interface.GetDictionaryTerm("Requirement Complete Course(s)"));//"Requirement: Complete Course(s)"
					this.Parent.EditWindowInterface = new CE_CourseRequirementInterface(
						this.Parent,
						this.Parent.ModalWindow,
						this.Parent.Identifier + "-CourseWindow",
						this.Parent.Languages,
						this.Parent.InitialLanguage,
						this.Parent.FlagImagePath,
						this.Parent.DeleteImagePath,
						this.Parent.AddImagePath,
						this.Parent.Data
						);
					break;
				case "task":
					this.Parent.ModalWindow.SetTitle(this.Parent.Interface.GetDictionaryTerm("Requirement Complete Task"));//"Requirement: Complete Task"
					this.Parent.EditWindowInterface = new CE_TaskRequirementInterface(
						this.Parent,
						this.Parent.ModalWindow,
						this.Parent.Identifier + "-TaskWindow",
						this.Parent.Languages,
						this.Parent.InitialLanguage,
						this.Parent.FlagImagePath,
						this.Parent.DeleteImagePath,
						this.Parent.AddImagePath,
						this.Parent.Data
						);
					break;
			}
			
			// attach the interface
			this.Parent.ModalWindow.Panel.append(this.Parent.EditWindowInterface.Body);
			this.Parent.ModalWindow.Panel.append(this.Parent.EditWindowInterface.Separator);
			this.Parent.ModalWindow.Panel.append(this.Parent.EditWindowInterface.ButtonContainer);
			this.Parent.ModalWindow.Reposition();
			
		}
	
	this.CreateCompleteEarnAndTaskInterfaces();
	
	this.Container.append(this.ReqItem);
	
}

CE_RequirementInterface.prototype.CreateCompleteEarnAndTaskInterfaces = function()
{
	this.ReqItem = HTML.c(this, "div");
	
	//this.DeleteButtonHolder = HTML.c(this, "div", "", "", "", this.Container);
	//this.DeleteButtonHolder.style.width = "100%";
	//this.DeleteButtonHolder.style.height = "17px";
	//this.DeleteButtonHolder.style.display = "block";

	try
	{
		//attach the Delete button
		this.DeleteButton = new IconButton(
			this,
			this.ReqItem,
			this.DeleteImagePath,
			"",
			true, 
			"DeleteRequirementButton"
		);
		
		//add onclick event to button Panel (div)
		this.DeleteButton.Panel.onclick = function(event)
		{
			//stop event propagation so click event doesn't bubble up to the RequirementClicker and open the 'edit requirement' modal
			event.stopPropagation();
		};
		
		//onclick function on object opens the confirm/cancel modal
		this.DeleteButton.onclick = function()
		{	
			this.Parent.Parent.PromptToDeleteRequirement(this.Parent);
		};

	}
	catch(e)
	{
		console.log(e);
	}

	switch(this.Data.Type){
		case "course":
			//if English, any/all text gets special bold highlighing
			if (this.InitialLanguage == "en-US")
			{
				this.ReqItem.append(HTML.text("Complete "));
				if (this.Data.Course.isAny == true){
					this.ReqItem.append(HTML.c(this, "span", "", "", "RequirementStrong", null, "any "));
				}else{
					this.ReqItem.append(HTML.c(this, "span", "", "", "RequirementStrong", null, "all "));
				}
				this.ReqItem.append(HTML.text("of these course(s):"));
			}
			else
			{
				//otherwise, translate phrase as full string since other languages may not take the same format and the any/all text placement is arbitrary
				if (this.Data.Course.isAny == true)
					this.ReqItem.append(HTML.text(this.Interface.GetDictionaryTerm("Complete any of these course(s)")));
				else
					this.ReqItem.append(HTML.text(this.Interface.GetDictionaryTerm("Complete all of these course(s)")));
			}
			
			this.List = HTML.c(this, "ul");
			
			for (var i = 0; i < this.Data.Course.Items.length; i++){
					this.List.append(HTML.c(
						this, 
						"li", 
						"", 
						"", 
						"", 
						"",
						this.Interface.GetCourseNameFromPool(this.Data.Course.Items[i].id) + " (" + this.Interface.GetCourseCreditsFromPool(this.Data.Course.Items[i].id) + ")"
						));
				}
				
			this.ReqItem.append(this.List);
			break;
		case "credit":
			//English get special bolded text on the number of credits
			if (this.InitialLanguage == "en-US")
			{
				this.ReqItem.append(HTML.text("Earn "));
				this.ReqItem.append(HTML.c(this, "span", "", "", "RequirementStrong", null, this.Data.Credit.Minimum.toString() + " "));
				this.ReqItem.append(HTML.text("credits from "));
				
				if (this.Data.Credit.isAny == true){
					this.ReqItem.append(HTML.text("any course."));
				}
				else
				{
					this.ReqItem.append(HTML.text("these courses:"));
					
					this.List = HTML.c(this, "ul");
					
					//this.Parent.Parent.Parent.Parent.GetCourseNameFromPool(this.Data.Credit.Items[i].id)
					
					for (var i = 0; i < this.Data.Credit.Items.length; i++){
						this.List.append(HTML.c(
							this, 
							"li", 
							"", 
							"", 
							"", 
							"",
							this.Interface.GetCourseNameFromPool(this.Data.Credit.Items[i].id) + " (" + this.Interface.GetCourseCreditsFromPool(this.Data.Credit.Items[i].id) + ")"
							));
					}
	
					this.ReqItem.append(this.List);
					
				}
			}
			//other languages get more general setup since bolded text could change positions
			else
			{
				if (this.Data.Credit.isAny == true)
				{
					//translate the string and insert the proper number of credits
					var earnStr = this.Interface.GetDictionaryTerm("Earn x credits from any course");
					earnStr = earnStr.split("##NumberOfCredits##").join(this.Data.Credit.Minimum.toString());
					this.ReqItem.append(HTML.text(earnStr));
				}
				else
				{
					var earnStr = this.Interface.GetDictionaryTerm("Earn x credits from these courses");
					earnStr = earnStr.split("##NumberOfCredits##").join(this.Data.Credit.Minimum.toString());
					this.ReqItem.append(HTML.text(earnStr));
					
					this.List = HTML.c(this, "ul");
					
					for (var i = 0; i < this.Data.Credit.Items.length; i++){
						this.List.append(HTML.c(
							this, 
							"li", 
							"", 
							"", 
							"", 
							"",
							this.Interface.GetCourseNameFromPool(this.Data.Credit.Items[i].id) + " (" + this.Interface.GetCourseCreditsFromPool(this.Data.Credit.Items[i].id) + ")"
							));
					}
	
					this.ReqItem.append(this.List);
				}
			}
			
			break;
		case "task":
			this.ReqItem.append(HTML.text(this.Interface.GetDictionaryTerm("Task") + ": "));
			this.ReqItem.append(HTML.c(this, "span", "", "", "RequirementStrong", null, unescape(this.Data.Labels[0].Value)));
			
			if(this.Data.Task.RequireDocumentation == true || this.Data.Task.Signoff.Required == true){
				this.ReqItem.append(HTML.text("; "));
			}
			
			if(this.Data.Task.RequireDocumentation == true){
				this.ReqItem.append(HTML.text(this.Interface.GetDictionaryTerm("Documentation is Required")));
				if(this.Data.Task.Signoff.Required == true){
					this.ReqItem.append(HTML.text("; "));
				}else{
					//this.ReqItem.append(HTML.text("."));
				}
			}
			if(this.Data.Task.Signoff.Required == true){
				
				var signoffText = "";
				//Supervisor or Expert DEPRECATED
				//Update to Administators and Learner's Supervisor(s)
				if (this.Data.Task.Signoff.ByAdministrators == true && this.Data.Task.Signoff.BySupervisor == true)
				{
					signoffText = this.Interface.GetDictionaryTerm("Signoff is Required by Administrators or Learners Supervisors");//Signoff is Required by Administrators or Learner's Supervisor(s)
				}
				//just Administrators
				else if (this.Data.Task.Signoff.ByAdministrators == true && this.Data.Task.Signoff.BySupervisor == false)
				{
					signoffText = this.Interface.GetDictionaryTerm("Signoff is Required by Administrators");
				}
				
				/*if (this.Data.Task.Signoff.BySupervisor == true && this.Data.Task.Signoff.ByExpert == true)
				{
					signoffText = this.Interface.GetDictionaryTerm("Signoff is Required by Supervisor or Expert");
				}
				//just Supervisor
				else if (this.Data.Task.Signoff.BySupervisor == true && this.Data.Task.Signoff.ByExpert == false)
				{
					signoffText = this.Interface.GetDictionaryTerm("Signoff is Required by Supervisor");
				}
				//just Expert
				else if (this.Data.Task.Signoff.BySupervisor == false && this.Data.Task.Signoff.ByExpert == true)
				{
					signoffText = this.Interface.GetDictionaryTerm("Signoff is Required by Expert");
				}*/
				
				this.ReqItem.append(HTML.text(signoffText));
				
				/*this.ReqItem.append(HTML.text("Signoff is Required by "));
				
				if(this.Data.Task.Signoff.BySupervisor == true){
					this.ReqItem.append(HTML.text("Supervisor"));
					if(this.Data.Task.Signoff.ByExpert == true){
						this.ReqItem.append(HTML.text(" or "));
					}
				}
				if(this.Data.Task.Signoff.ByExpert == true){
					this.ReqItem.append(HTML.text("Expert"));
				}
				this.ReqItem.append(HTML.text("."));*/
			}
			break;
			
	}
}

CE_RequirementInterface.prototype.UpdateRequirementInterface = function()
{
	//this will update the entire list (list items don't contain a course id)
	
	this.CreateCompleteEarnAndTaskInterfaces();
	
	this.Container.replaceChild(this.ReqItem, this.Container.childNodes[0]);
}

CE_RequirementInterface.prototype.UpdateAvailableCourses = function()
{
	//after a course delete, update the interface to match the change
	try
	{
		var deletedCoursesArray = new Array();
		var requirementType;
		
		if (this.Data.Type == "course")
			requirementType = "Course";
		else if (this.Data.Type == "credit")
			requirementType = "Credit";
		
		//if not either type, exit function
		if (!requirementType)
			return;
		
		for (var i = 0; i < this.Data[requirementType].Items.length; i++)
		{
			var currentCourseId = this.Data[requirementType].Items[i].id;
			
			if (!this.Interface.DoesCourseExistInPool(currentCourseId))
			{
				//if course doesn't exist in the Attached Courses pool, remove the item from this requirement
				//console.log("Course does not exist in pool anymore: " + currentCourseId);
				deletedCoursesArray.push(currentCourseId);
			}
		}
		
		//remove the appropriate courses from this Requirement
		this.RemoveCoursesFromThisRequirementById(deletedCoursesArray);
		
	}
	catch(e)
	{
		//error updating Requirement Interface
	}
	
}

CE_RequirementInterface.prototype.RemoveCoursesFromThisRequirementById = function(courseIdsToRemoveArray)
{
	//remove the specified courses from this Requirement data object
	var idList = courseIdsToRemoveArray;
	
	for (var i = 0; i < idList.length; i++)
	{
		var currentId = idList[i];
		var courseIndex;
		var requirementType;
		
		if (this.Data.Type == "course")
			requirementType = "Course";
		else if (this.Data.Type == "credit")
			requirementType = "Credit";
		
		//if not either type, exit function
		if (!requirementType)
			return;
		
		for (var j = 0; j < this.Data[requirementType].Items.length; j++)
		{
			//if matching course is found, store the index and break from for loop
			if (this.Data[requirementType].Items[j].id == currentId)
			{
				courseIndex = j;
				break;
			}
		}
		
		//use the found index to splice the item from the Requirement
		if (courseIndex != null && courseIndex != undefined)
		{
			//console.log("CE_RequirementInterface used RemoveCoursesFromThisRequirementById to splice id: " + this.Data[requirementType].Items[j].id);
			
			this.Data[requirementType].Items.splice(courseIndex, 1);
						
			//console.log(this.Data[requirementType].Items);
		}

	}
}

CE_RequirementInterface.prototype.RemoveFromUI = function()
{
	//removes this UI element from the Segment
	this.Parent.RequirementsContainer.removeChild(this.Container);
}

CE_RequirementInterface.prototype.GetTotalCreditsOfAllCoursesInCreditRequirement = function()
{
	//if this is a Credit Requirement, return the total number of credits that all included courses add up to
	if (this.Data.Type == "credit")
	{
		var totalCredits = 0;
		
		if (this.Data.Credit.isAny == false)
		{
			for (var i = 0; i < this.Data.Credit.Items.length; i++)
			{
				//get the id of the current course
				var currentCourseId = this.Data.Credit.Items[i].id;
				var currentCourseCredits = this.Interface.GetCourseCreditsFromPool(currentCourseId);
				
				//grab the number of credits that the current course is worth
				if (typeof currentCourseCredits == "number")
					totalCredits += currentCourseCredits;
				
			}
		}
		else if (this.Data.Credit.isAny == true)
		{
			//tally credits from all available courses if the setting is "Any Course"
			var courseIds = this.Interface.GetCourseIdListFromPool();
			
			totalCredits = this.GetTotalCreditsFromCourseIdList(courseIds);

		}
		
		return totalCredits;
	}
	
	return;	
}

CE_RequirementInterface.prototype.GetTotalCreditsFromCourseIdList = function(courseIdList)
{
	//use the id's in the list to tally the total number of credits the courses add up to
	var totalCredits = 0;
	
	for (var i = 0; i < courseIdList.length; i++)
	{
		var currentCourseCredits = this.Interface.GetCourseCreditsFromPool(courseIdList[i]);
		
		//grab the number of credits that the current course is worth
		if (typeof currentCourseCredits == "number")
			totalCredits += currentCourseCredits;
	}
	
	return totalCredits;
}

CE_RequirementInterface.prototype.GetCourseIdList = function()
{
	//requirements with a Type of "credit" or "course" will have a list of associated courses
	var courseIdList = new Array();
	
	if (this.GetType() == "credit")
	{
		if (this.GetIsAny() == true)
		{
			//get id's from all available courses if the setting is "Any Course"
			courseIdList = this.Interface.GetCourseIdListFromPool();
		
			return courseIdList;
		}
		else if (this.GetIsAny() == false)
		{
			//if "Any Course" not checked, course Id's will be members of the Item object
			for (var i = 0; i < this.Data.Credit.Items.length; i++)
			{
				//get the id of the current course and push it to the array
				var currentCourseId = this.Data.Credit.Items[i].id;
				courseIdList.push(currentCourseId);
			}
			
			return courseIdList;
		}
	}
	else if (this.GetType() == "course")
	{
		for (var i = 0; i < this.Data.Course.Items.length; i++)
		{
			//get the id of the current course and push it to the array
			var currentCourseId = this.Data.Course.Items[i].id;
			courseIdList.push(currentCourseId);
		}
		
		return courseIdList;
	}
	
	//return undefined if a Type of "course" or "credit"
	return;

}

CE_RequirementInterface.prototype.UpdateDeleteButtonEnabled = function()
{
	//Delete button should be disabled if this is the only Requirement left on a Segment
	if (this.Parent.Requirements)
	{
		if (this.Parent.Requirements.length < 2)
		{
			this.DeleteButton.Enable(false);
		}
		else
		{
			this.DeleteButton.Enable(true);
		}
	}
}

CE_RequirementInterface.prototype.GetType = function()
{
	//return the type of Requirement this is: "credit", "course", "task"
	return this.Data.Type;
}

CE_RequirementInterface.prototype.GetIsAny = function()
{
	//return whether isAny is true or false
	if (this.GetType() == "credit")
		return this.Data.Credit.isAny;
	else if (this.GetType() == "course")
		return this.Data.Course.isAny;
	
	//if not applicable, return undefined
	return;
}

CE_RequirementInterface.prototype.GetId = function()
{
	return this.Data.id;
}

CE_RequirementInterface.prototype.GetDataAsString = function()
{
    var s = "";
    
	s += JSON.stringify(this.Data);
	
	return s;
}

CE_RequirementAndOr = function(
	parent
	){
		
	this.Parent = parent;
	this.TypeName = "RequirementAndOr";
	
	this.Container = document.createElement("div");
		this.Container.TypeName = "TheRequirementContainer";
		this.Container.Parent = this;
		this.Container.className = "AndOr";
	
	if (this.Parent.RequirementsIsAny == true){
		this.Container.append(HTML.text(this.Parent.Interface.GetDictionaryTerm("OR")));
	}else {
		this.Container.append(HTML.text(this.Parent.Interface.GetDictionaryTerm("AND")));
	}
	
	this.Container.onclick = function(){
		this.Parent.Parent.ToggleRequirementsAndOr();
	}
	
}