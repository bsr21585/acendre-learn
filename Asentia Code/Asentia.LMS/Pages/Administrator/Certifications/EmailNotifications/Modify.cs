﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml;

namespace Asentia.LMS.Pages.Administrator.Certifications.EmailNotifications
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel EmailNotificationPropertiesFormContentWrapperContainer;
        public Panel CertificationObjectMenuContainer;
        public Panel EmailNotificationPropertiesWrapperContainer;
        public Panel EmailNotificationPropertiesFeedbackContainer;
        public Panel EmailNotificationPropertiesInstructionsPanel;
        public Panel EmailNotificationPropertiesContainer;
        public Panel EmailNotificationPropertiesActionsPanel;        
        #endregion

        #region Private Properties
        private Certification _CertificationObject;
        private EventEmailNotification _EventEmailNotificationObject;
        private EventEmailNotificationForm _EventEmailNotificationForm;

        private DataTable _EventTypesDataTable;
        private Button _SaveButton = new Button();
        private Button _CancelButton = new Button();

        private string _SiteLanguage;
        #endregion

        #region Page Load
        /// <summary>
        /// Handles the Page Load Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CertificationsManager))
            { Response.Redirect("/"); }

            // check to ensure Object Specific Email Notifications is enabled on the portal
            if (!AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE) ?? false)
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/certifications/emailnotifications/Modify.css");

            // get the certification object
            this._GetCertificationObject();

            // get the email notification object
            this._GetEmailNotificationObject();

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.EmailNotificationPropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.EmailNotificationPropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the certification object menu
            if (this._CertificationObject != null)
            {
                CertificationObjectMenu certificationObjectMenu = new CertificationObjectMenu(this._CertificationObject);
                certificationObjectMenu.SelectedItem = CertificationObjectMenu.MenuObjectItem.EmailNotifications;

                this.CertificationObjectMenuContainer.Controls.Add(certificationObjectMenu);
            }

            // assign site language
            this._SiteLanguage = AsentiaSessionState.GlobalSiteObject.LanguageString;

            // build the properties form
            this._BuildPropertiesForm();

            // build the properties form actions panel
            this._BuildPropertiesActionsPanel();
        }
        #endregion

        #region _GetCertificationObject
        /// <summary>
        /// Gets a certification object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCertificationObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("cid", 0);

            if (qsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                try
                {
                    if (id > 0)
                    { this._CertificationObject = new Certification(id); }
                }
                catch
                { Response.Redirect("~/administrator/certifications"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get certification title information
            string certificationTitleInInterfaceLanguage = this._CertificationObject.Title;
            string certificationImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
            string certificationImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Certification.LanguageSpecificProperty certificationLanguageSpecificProperty in this._CertificationObject.LanguageSpecificProperties)
                {
                    if (certificationLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { certificationTitleInInterfaceLanguage = certificationLanguageSpecificProperty.Title; }
                }
            }

            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;            
            string pageTitle;

            if (this._EventEmailNotificationObject != null)
            {
                string eventEmailNotificationTitleInInterfaceLanguage = this._EventEmailNotificationObject.Name;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (EventEmailNotification.LanguageSpecificProperty eventEmailNotificationLanguageSpecificProperty in this._EventEmailNotificationObject.LanguageSpecificProperties)
                    {
                        if (eventEmailNotificationLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { eventEmailNotificationTitleInInterfaceLanguage = eventEmailNotificationLanguageSpecificProperty.Name; }
                    }
                }

                breadCrumbPageTitle = eventEmailNotificationTitleInInterfaceLanguage;
                pageTitle = eventEmailNotificationTitleInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewEmailNotification;
                pageTitle = _GlobalResources.NewEmailNotification;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Certifications, "/administrator/certifications"));
            breadCrumbLinks.Add(new BreadcrumbLink(certificationTitleInInterfaceLanguage, "/administrator/certifications/Dashboard.aspx?id=" + this._CertificationObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.EmailNotifications, "/administrator/certifications/emailnotifications/Default.aspx?cid=" + this._CertificationObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, certificationTitleInInterfaceLanguage, certificationImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG), certificationImageCssClass);
        }
        #endregion

        #region _GetEmailNotificationObject
        /// <summary>
        /// Gets an EmailNotification object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetEmailNotificationObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);
            string lang = this.QueryStringString("lang", this._SiteLanguage);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    {
                        this._EventEmailNotificationObject = new EventEmailNotification(id, lang);
                    }
                }
                catch
                { Response.Redirect("/administrator/certifications/emailnotifications/Default.aspx?cid=" + this._CertificationObject.Id.ToString()); }
            }
        }
        #endregion

        #region _BuildPropertiesForm
        /// <summary>
        /// Builds the properties form.
        /// </summary>
        private void _BuildPropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.EmailNotificationPropertiesInstructionsPanel, _GlobalResources.CreateOrConfigureAnEmailNotification, true);

            // clear controls from container
            this.EmailNotificationPropertiesContainer.Controls.Clear();

            this._EventTypesDataTable = EventType.GetEventTypesInfo(6);

            // build the event email notification form
            this._EventEmailNotificationForm = new EventEmailNotificationForm("EventEmailNotificationModify", this._SiteLanguage, this._EventTypesDataTable, this._EventEmailNotificationObject, this.Page);
            this.EmailNotificationPropertiesContainer.Controls.Add(this._EventEmailNotificationForm);

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildPropertiesActionsPanel()
        {
            // style actions panel
            this.EmailNotificationPropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SaveButton.ID = "SaveButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";
            this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this.EmailNotificationPropertiesActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.EmailNotificationPropertiesActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion        

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;
            bool messageTabHasErrors = false;

            // title field
            if (String.IsNullOrWhiteSpace(this._EventEmailNotificationForm.Title))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.EmailNotificationPropertiesContainer, "TitleText", _GlobalResources.Name + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // built-in regex validation for email addresses
            if (!String.IsNullOrWhiteSpace(this._EventEmailNotificationForm.SpecificEmailAddress) && !RegExValidation.ValidateEmailAddress(this._EventEmailNotificationForm.SpecificEmailAddress))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.EmailNotificationPropertiesContainer, "Recipient", _GlobalResources.EmailAddress + " " + _GlobalResources.IsInvalid);
            }

            // check if there is a specific email address assigned
            if (this._EventEmailNotificationForm.Recipient == Convert.ToString((int)EventEmailNotificationForm.EventTypeRecipient.SpecificEmailAddress) && String.IsNullOrWhiteSpace(this._EventEmailNotificationForm.SpecificEmailAddress))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.EmailNotificationPropertiesContainer, "Recipient", _GlobalResources.AValidEmailAddressIsRequiredWhenSpecificEmailAddressIsTheRecipientType);
            }

            // interval field, integer required
            int value;
            if (!String.IsNullOrWhiteSpace(this._EventEmailNotificationForm.Interval) && (!int.TryParse(this._EventEmailNotificationForm.Interval, out value) || Convert.ToInt32(this._EventEmailNotificationForm.Interval) < 1))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.EmailNotificationPropertiesContainer, "Send", _GlobalResources.Interval + " " + _GlobalResources.IsInvalid);
            }

            // subject field
            if (String.IsNullOrWhiteSpace(this._EventEmailNotificationForm.Subject))
            {
                messageTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.EmailNotificationPropertiesContainer, "SubjectText", _GlobalResources.Subject + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // body field
            if (this._EventEmailNotificationForm.IsHTMLBased == true)
            {
                if (String.IsNullOrWhiteSpace(this._EventEmailNotificationForm.BodyHTML))
                {
                    messageTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EmailNotificationPropertiesContainer, "BodyHTML", _GlobalResources.Body + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
                }
            }
            else
            {
                if (String.IsNullOrWhiteSpace(this._EventEmailNotificationForm.Body))
                {
                    messageTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.EmailNotificationPropertiesContainer, "BodyText", _GlobalResources.Body + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
                }
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.EmailNotificationPropertiesContainer, "EventEmailNotificationModify_Options_TabLI"); }

            if (messageTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.EmailNotificationPropertiesContainer, "EventEmailNotificationModify_Message_TabLI"); }

            return isValid;
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // if there is no Email Notification object, create one
                if (this._EventEmailNotificationObject == null)
                { this._EventEmailNotificationObject = new EventEmailNotification(); }

                // validate the form
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                int id;

                this._EventEmailNotificationObject.IdSite = AsentiaSessionState.IdSite;
                this._EventEmailNotificationObject.IdUser = AsentiaSessionState.IdSiteUser;
                this._EventEmailNotificationObject.Name = this._EventEmailNotificationForm.Title;
                this._EventEmailNotificationObject.IsActive = Convert.ToBoolean(this._EventEmailNotificationForm.IsActive);
                this._EventEmailNotificationObject.IdEventType = Convert.ToInt32(this._EventEmailNotificationForm.EventType);
                this._EventEmailNotificationObject.IdEventTypeRecipient = Convert.ToInt32(this._EventEmailNotificationForm.Recipient);
                this._EventEmailNotificationObject.From = this._EventEmailNotificationForm.From;
                this._EventEmailNotificationObject.CopyTo = this._EventEmailNotificationForm.CopyTo;
                this._EventEmailNotificationObject.SpecificEmailAddress = this._EventEmailNotificationForm.SpecificEmailAddress;
                this._EventEmailNotificationObject.IdObject = this._CertificationObject.Id;

                if (this._EventEmailNotificationForm.CurrentTime || this._EventEmailNotificationForm.Interval.Length < 1)
                {
                    this._EventEmailNotificationObject.Interval = null;
                    this._EventEmailNotificationObject.TimeFrame = null;
                }
                else
                {
                    this._EventEmailNotificationObject.Interval = (this._EventEmailNotificationForm.BeforeAfter == "before") ? Convert.ToInt32(this._EventEmailNotificationForm.Interval) * -1 : Convert.ToInt32(this._EventEmailNotificationForm.Interval);
                    this._EventEmailNotificationObject.TimeFrame = this._EventEmailNotificationForm.TimeFrame;
                }

                this._EventEmailNotificationObject.Priority = Convert.ToInt32(this._EventEmailNotificationForm.Priority);
                this._EventEmailNotificationObject.IsHTMLBased = Convert.ToBoolean(this._EventEmailNotificationForm.IsHTMLBased);
                this._EventEmailNotificationObject.AttachmentType = this._EventEmailNotificationForm.AttachmentType;

                id = this._EventEmailNotificationObject.Save();
                this.ViewState["id"] = id;
                this._EventEmailNotificationObject.IdEventEmailNotification = id;

                // do email notification language-specific properties

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in this.GetArrayListOfSiteAvailableInstalledLanguages())
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string notificationName = null;

                        // get text boxes
                        TextBox languageSpecificNotificationNameTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.TitleID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificNotificationNameTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificNotificationNameTextBox.Text))
                            { notificationName = languageSpecificNotificationNameTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(notificationName))
                        {
                            this._EventEmailNotificationObject.SaveLang(cultureInfo.Name,
                                                                        notificationName);
                        }
                    }
                }

                this._SaveXMLFile(this._EventEmailNotificationObject.IdEventEmailNotification);

                // load the saved email notification object
                this._EventEmailNotificationObject = new EventEmailNotification(id, this._SiteLanguage);                

                // build the page title controls
                this._BuildBreadcrumbAndPageTitle();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.EmailNotificationPropertiesFeedbackContainer, _GlobalResources.EmailNotificationSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.EmailNotificationPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.EmailNotificationPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.EmailNotificationPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.EmailNotificationPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.EmailNotificationPropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _SaveXMLFile
        /// <summary>
        /// Handles save data "subject and email body template into xml file
        /// </summary>
        private void _SaveXMLFile(int IdEventEmailNotification)
        {
            string fullFilePath = Server.MapPath(SitePathConstants.SITE_EMAILNOTIFICATIONS_ROOT + IdEventEmailNotification.ToString() + "/EmailNotification.xml");

            try
            {
                TextBox languageSpecificNotificationSubjectTextBox, languageSpecificNotificationBodyTextBox;
                XmlDocument emailNotificationXml = new XmlDocument();
                XmlNode newEmailNotificationNode;

                // BEGIN BUILDING XML

                // Root Node
                newEmailNotificationNode = emailNotificationXml.CreateNode(XmlNodeType.Element, "emailNotification", null);

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in this.GetArrayListOfSiteAvailableInstalledLanguages())
                {
                    // EmailNotification node and lang attribute
                    XmlNode languageNode = emailNotificationXml.CreateNode(XmlNodeType.Element, "language", null);
                    XmlAttribute newLangAttribute = emailNotificationXml.CreateAttribute("code");
                    newLangAttribute.Value = availableLanguage;
                    languageNode.Attributes.Append(newLangAttribute);

                    if (availableLanguage == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        languageSpecificNotificationSubjectTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.SubjectID);

                        if (this._EventEmailNotificationForm.IsHTMLBased == true)
                        {
                            languageSpecificNotificationBodyTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.BodyHTMLID);
                        }
                        else
                        {
                            languageSpecificNotificationBodyTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.BodyID);
                        }

                        XmlNode defaultLlanguageNode = emailNotificationXml.CreateNode(XmlNodeType.Element, "language", null);
                        XmlAttribute defaultLanAttribute = emailNotificationXml.CreateAttribute("code");
                        defaultLanAttribute.Value = "default";
                        defaultLlanguageNode.Attributes.Append(defaultLanAttribute);

                        // subject node
                        XmlNode defaultSubjectNode = emailNotificationXml.CreateNode(XmlNodeType.Element, "subject", null);
                        XmlCDataSection defaultSubjectNodeCData = emailNotificationXml.CreateCDataSection(languageSpecificNotificationSubjectTextBox.Text);
                        defaultSubjectNode.AppendChild(defaultSubjectNodeCData);
                        defaultLlanguageNode.AppendChild(defaultSubjectNode);

                        // body node
                        XmlNode defaultBodyNode = emailNotificationXml.CreateNode(XmlNodeType.Element, "body", null);
                        XmlCDataSection defaultMobileHeadlineNodeCData = emailNotificationXml.CreateCDataSection(languageSpecificNotificationBodyTextBox.Text);
                        defaultBodyNode.AppendChild(defaultMobileHeadlineNodeCData);
                        defaultLlanguageNode.AppendChild(defaultBodyNode);

                        newEmailNotificationNode.AppendChild(defaultLlanguageNode);
                    }
                    else
                    {
                        languageSpecificNotificationSubjectTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.SubjectID + "_" + availableLanguage);

                        if (this._EventEmailNotificationForm.IsHTMLBased == true)
                        {
                            languageSpecificNotificationBodyTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.BodyHTMLID + "_" + availableLanguage);
                        }
                        else
                        {
                            languageSpecificNotificationBodyTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.BodyID + "_" + availableLanguage);
                        }
                    }

                    // subject node
                    XmlNode newSubjectNode = emailNotificationXml.CreateNode(XmlNodeType.Element, "subject", null);
                    XmlCDataSection newSubjectNodeCData = emailNotificationXml.CreateCDataSection(languageSpecificNotificationSubjectTextBox.Text);
                    newSubjectNode.AppendChild(newSubjectNodeCData);
                    languageNode.AppendChild(newSubjectNode);

                    // body node
                    XmlNode newBodyNode = emailNotificationXml.CreateNode(XmlNodeType.Element, "body", null);
                    XmlCDataSection newMobileHeadlineNodeCData = emailNotificationXml.CreateCDataSection(languageSpecificNotificationBodyTextBox.Text);
                    newBodyNode.AppendChild(newMobileHeadlineNodeCData);
                    languageNode.AppendChild(newBodyNode);

                    newEmailNotificationNode.AppendChild(languageNode);
                }

                // append EmailNotifications to document
                emailNotificationXml.AppendChild(newEmailNotificationNode);

                // END BUILDING XML

                // BEGIN WRITE FILE

                // if the directory does not exist, create it
                if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_EMAILNOTIFICATIONS_ROOT + IdEventEmailNotification)))
                { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_EMAILNOTIFICATIONS_ROOT + IdEventEmailNotification)); }

                // write the new file
                using (XmlTextWriter xmlWriter = new XmlTextWriter(fullFilePath, Encoding.UTF8))
                {
                    xmlWriter.Formatting = Formatting.Indented;
                    emailNotificationXml.Save(xmlWriter);
                    xmlWriter.Close();
                }
            }
            catch (Exception ex)
            {
                throw new AsentiaException(_GlobalResources.UnableToSaveEmailnotificationContent);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("Default.aspx?cid=" + this._CertificationObject.Id.ToString());
        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            if (this._EventEmailNotificationObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                bool isDefaultPopulated = false;

                foreach (EventEmailNotification.LanguageSpecificProperty eventEmailNotificationLanguageSpecificProperty in this._EventEmailNotificationObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (eventEmailNotificationLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._EventEmailNotificationObject.Name = eventEmailNotificationLanguageSpecificProperty.Name;
                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificNotificationNameTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.TitleID + "_" + eventEmailNotificationLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificNotificationNameTextBox != null)
                        { languageSpecificNotificationNameTextBox.Text = eventEmailNotificationLanguageSpecificProperty.Name; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._EventEmailNotificationObject.Name = this._EventEmailNotificationForm.Title;
                }

                string xmlFilePath = MapPath(SitePathConstants.SITE_EMAILNOTIFICATIONS_ROOT + _EventEmailNotificationObject.IdEventEmailNotification.ToString() + "/EmailNotification.xml");
                XmlDocument emailNotificationXml = new XmlDocument();
                emailNotificationXml.Load(xmlFilePath);

                XmlNodeList languageNodes = emailNotificationXml.GetElementsByTagName("language");

                foreach (string availableLanguage in this.GetArrayListOfSiteAvailableInstalledLanguages())
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    TextBox languageSpecificNotificationSubjectTextBox, languageSpecificNotificationBodyTextBox;

                    if (availableLanguage == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        languageSpecificNotificationSubjectTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.SubjectID);

                        if (this._EventEmailNotificationForm.IsHTMLBased == true)
                        { languageSpecificNotificationBodyTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.BodyHTMLID); }
                        else
                        { languageSpecificNotificationBodyTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.BodyID); }
                    }
                    else
                    {
                        languageSpecificNotificationSubjectTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.SubjectID + "_" + availableLanguage);

                        if (this._EventEmailNotificationForm.IsHTMLBased == true)
                        { languageSpecificNotificationBodyTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.BodyHTMLID + "_" + availableLanguage); }
                        else
                        { languageSpecificNotificationBodyTextBox = (TextBox)this.EmailNotificationPropertiesContainer.FindControl(this._EventEmailNotificationForm.BodyID + "_" + availableLanguage); }
                    }

                    if (languageSpecificNotificationSubjectTextBox != null)
                    {
                        languageSpecificNotificationSubjectTextBox.Text = emailNotificationXml.SelectSingleNode("/emailNotification/language[@code='" + availableLanguage + "']/subject").InnerText;
                    }

                    if (languageSpecificNotificationBodyTextBox != null)
                    {
                        languageSpecificNotificationBodyTextBox.Text = emailNotificationXml.SelectSingleNode("/emailNotification/language[@code='" + availableLanguage + "']/body").InnerText;
                    }
                }
            }
        }
        #endregion
    }
}
