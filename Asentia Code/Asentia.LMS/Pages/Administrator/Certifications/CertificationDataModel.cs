using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

#region CertificationDataModel

// KnownTypes for JSON Serialization
[KnownType(typeof(CertificationLanguageValueDataModel))]
[KnownType(typeof(CertificationAccreditorDataModel))]
[KnownType(typeof(CertificationExpertDataModel))]
[KnownType(typeof(CertificationUnitsDataModel))]
[KnownType(typeof(CertificationRequirementsTabDataModel))]
[KnownType(typeof(CertificationUnitsExpirationDataModel))]
[KnownType(typeof(CertificationUnitsItemsDataModel))]
[KnownType(typeof(CertificationApplicableCourseDataModel))]
[KnownType(typeof(CertificationSegmentsDataModel))]
[KnownType(typeof(CertificationSegmentsItemsDataModel))]
[KnownType(typeof(CertificationRequirementsDataModel))]
//[KnownType(typeof(CertificationCourseRequirementDataModel))]
//[KnownType(typeof(CertificationCreditRequirementDataModel))]
//[KnownType(typeof(CertificationTaskRequirementDataModel))]
[KnownType(typeof(CertificationRequirementItemDataModel))]
[KnownType(typeof(CertificationCourseRequirementDetailsDataModel))]
[KnownType(typeof(CertificationRequirementCourseIdDataModel))]
[KnownType(typeof(CertificationCreditRequirementDetailsDataModel))]
[KnownType(typeof(CertificationTaskRequirementDetailsDataModel))]
[KnownType(typeof(CertificationTaskRequirementDetailsSignoffDataModel))]

public class CertificationDataModel
{
    // Properties
    public List<CertificationLanguageValueDataModel>        Titles;					// List of CertificationLanguageValueDataModel objects
    public List<CertificationLanguageValueDataModel>        Descriptions;			// List of CertificationLanguageValueDataModel objects
	public CertificationAccreditorDataModel		            Accreditor;				// CertificationAccreditorDataModel object
	public string								            Tags;					// Tags associated with the Certification
	public List<CertificationExpertDataModel>				Experts;				// List of CertificationExpertDataModel objects
	public bool? 								            isPublished;			// whether the Certification is published or not
	public bool?								            isClosed;				// whether the Certification is closed or open
	public CertificationUnitsDataModel			            Units;					// CertificationUnitsDataModel object
	public List<CertificationApplicableCourseDataModel>     ApplicableCourses;		// List of CertificationApplicableCourseDataModel objects

    // Default Constructor
    public CertificationDataModel()
    {		
        this.Titles 				= new List<CertificationLanguageValueDataModel>();
        this.Descriptions           = new List<CertificationLanguageValueDataModel>();
        this.Accreditor				= new CertificationAccreditorDataModel();
        this.Tags					= null;
        this.Experts                = new List<CertificationExpertDataModel>();
        this.isPublished			= true;
        this.isClosed				= false;
        this.Units					= new CertificationUnitsDataModel();
        this.ApplicableCourses      = new List<CertificationApplicableCourseDataModel>();
    }
}
#endregion

#region CertificationLanguageValueDataModel
public class CertificationLanguageValueDataModel
{
	// Properties    
	public string Language;	// language string, i.e. "en-US" or "es-ES"
	public string Value;	// the text in the given language
	
	// Default Constructor
	public CertificationLanguageValueDataModel()
	{
		this.Language   = null;
		this.Value      = null;
	}
}
#endregion

#region CertificationAccreditorDataModel
public class CertificationAccreditorDataModel
{
	// Properties
	public string Company;	// company name
	public string Name;		// Accreditor's name
	public string Email;	// Accreditor's email
	public string Phone;	// Accreditor's number
	
	// Default Constructor
	public CertificationAccreditorDataModel()
	{
		this.Company    = null;
		this.Name		= null;
		this.Email 		= null;
		this.Phone		= null;
	}
}
#endregion

#region CertificationExpertDataModel
public class CertificationExpertDataModel
{
	// Properties
	public string id;		// id of Expert
	public string Name;     // name of Expert
	
	// Default Constructor
	public CertificationExpertDataModel()
	{
		this.id 	= null;
		this.Name   = null;
	}
}
#endregion

#region CertificationUnitsDataModel
public class CertificationUnitsDataModel
{
	// Properties
	public CertificationRequirementsTabDataModel Initial;	// CertificationRequirementsTabDataModel object containing data for the Initial Requirements tab
	public CertificationRequirementsTabDataModel Renewal;	// CertificationRequirementsTabDataModel object containing data for the Renewal Requirements tab
	
	// Default Constructor
	public CertificationUnitsDataModel()
	{
		this.Initial    = new CertificationRequirementsTabDataModel();
		this.Renewal	= new CertificationRequirementsTabDataModel();
	}
}
#endregion

#region CertificationRequirementsTabDataModel
public class CertificationRequirementsTabDataModel
{
	// Properties
	public bool?                                        isAny;		//corresponds to Units AND/OR button
	public CertificationUnitsExpirationDataModel		Expiration;	//CertificationUnitsExpirationDataModel object
	public List<CertificationUnitsItemsDataModel>		Items;		//List of CertificationUnitsItemsDataModel object that contains the info for each Unit
	
	// Default Constructor
	public CertificationRequirementsTabDataModel()
	{
		this.isAny          = null;
		this.Expiration     = new CertificationUnitsExpirationDataModel();
		this.Items			= new List<CertificationUnitsItemsDataModel>();
	}
}
#endregion

#region CertificationUnitsExpirationDataModel
public class CertificationUnitsExpirationDataModel
{
	// Properties
	public string Interval;
	public string Timeframe;
	
	// Default Constructor
	public CertificationUnitsExpirationDataModel()
	{
		this.Interval   = null;
		this.Timeframe  = null;
	}
}
#endregion

#region CertificationUnitsItemsDataModel
public class CertificationUnitsItemsDataModel
{
	// Properties
	public int? 							            id;				// Unit id
	public List<CertificationLanguageValueDataModel> 	Titles;			// List of CertificationLanguageValueDataModel objects
	public List<CertificationLanguageValueDataModel> 	Descriptions;	// List of CertificationLanguageValueDataModel objects
	public CertificationSegmentsDataModel 	            Segments;		// CertificationSegmentsDataModel object that contains all info for a segment
	
	// Default Constructor
	public CertificationUnitsItemsDataModel()
	{
		this.id				= 0;
		this.Titles			= new List<CertificationLanguageValueDataModel>();
		this.Descriptions 	= new List<CertificationLanguageValueDataModel>();
		this.Segments 		= new CertificationSegmentsDataModel();
	}
}
#endregion

#region CertificationSegmentsDataModel
public class CertificationSegmentsDataModel
{
	// Properties
	public bool? 		                                isAny;	// corresponds to Segments AND/OR button
	public List<CertificationSegmentsItemsDataModel>	Items;	// List of CertificationSegmentsItemsDataModel objects that hold data for each Segment
	
	// Default Constructor
	public CertificationSegmentsDataModel()
	{
		this.isAny  = null;
		this.Items	= new List<CertificationSegmentsItemsDataModel>();
	}
}
#endregion

#region CertificationSegmentsItemsDataModel
public class CertificationSegmentsItemsDataModel
{
	// Properties
	public int? 								        id;				// the Segment id
	public List<CertificationLanguageValueDataModel>    Labels;			// List of CertificationLanguageValueDataModel objects
	public CertificationRequirementsDataModel 	        Requirements;	// CertificationRequirementsDataModel object that contains all requirements for the segment
	
	// Default Constructor
	public CertificationSegmentsItemsDataModel()
	{
		this.id				= 0;
		this.Labels			= new List<CertificationLanguageValueDataModel>();
		this.Requirements	= new CertificationRequirementsDataModel();
	}
}
#endregion

#region CertificationRequirementsDataModel
public class CertificationRequirementsDataModel
{
	// Properties
	public bool? 		isAny;	// corresponds to the Requirements AND/OR button
	//public ArrayList	Items;	// ArrayList of CertificationCourseRequirementDataModel, CertificationCreditRequirementDataModel, and CertificationTaskRequirementDataModel objects that make up a Segment's Requirements
    public List<CertificationRequirementItemDataModel> Items;	// ArrayList of CertificationCourseRequirementDataModel, CertificationCreditRequirementDataModel, and CertificationTaskRequirementDataModel objects that make up a Segment's Requirements
	
	// Default Constructor
	public CertificationRequirementsDataModel()
	{
		this.isAny	= null;
        this.Items = new List<CertificationRequirementItemDataModel>();
	}
}
#endregion

#region CertificationRequirementItemDataModel
public class CertificationRequirementItemDataModel
{
	// Properties
	public string 											id;		// Requirement id -- string for JSON purposes because we prefix new requirement ids with a letter, 
                                                                    //                   but needs to be converted to Int32 server-side; basically if it can be converted
                                                                    //                   it's existing, if not it's new
	public List<CertificationLanguageValueDataModel> 		Labels;	// List of CertificationLanguageValueDataModel objects that determine Requirement Label for each language
	public string 											Type;	// the Type of Requirement (course, credit, task)
	public CertificationCourseRequirementDetailsDataModel 	Course;	// CertificationCourseRequirementDetailsDataModel object that stores the Course Requirement details
    public CertificationCreditRequirementDetailsDataModel   Credit;	// CertificationCreditRequirementDetailsDataModel object that stores the Credit Requirement details
    public CertificationTaskRequirementDetailsDataModel     Task;	// CertificationTaskRequirementDetailsDataModel object that stores the Task Requirement details
	
	// Default Constructor
    public CertificationRequirementItemDataModel()
	{
		this.id		    = ""; // empty string, not null
		this.Labels     = new List<CertificationLanguageValueDataModel>();
		this.Type 	    = null; // this will always be "course" for this Type
		this.Course     = new CertificationCourseRequirementDetailsDataModel();
        this.Credit     = new CertificationCreditRequirementDetailsDataModel();
        this.Task       = new CertificationTaskRequirementDetailsDataModel();
	}
}
#endregion

#region CertificationCourseRequirementDetailsDataModel
public class CertificationCourseRequirementDetailsDataModel
{
	// Properties
	public bool? 			                                    isAny;	// corresponds to if learner must complete any course or all courses
	public List<CertificationRequirementCourseIdDataModel> 		Items;	// List of CertificationRequirementCourseIdDataModel objects that contain course id
	
	// Default Constructor
	public CertificationCourseRequirementDetailsDataModel()
	{
		this.isAny  = null;
		this.Items	= new List<CertificationRequirementCourseIdDataModel>();
	}
}
#endregion

#region CertificationRequirementCourseIdDataModel
public class CertificationRequirementCourseIdDataModel
{
	// Properties
	public int? id;	// Course Id
	
	// Default Constructor
	public CertificationRequirementCourseIdDataModel()
	{
		this.id	= null;
	}
}
#endregion

#region CertificationCreditRequirementDataModel
/*
public class CertificationCreditRequirementDataModel
{
	// Properties
	public int? 											id;		// Requirement id
	public List<CertificationLanguageValueDataModel> 		Labels;	// List of CertificationLanguageValueDataModel objects that determine Requirement Label for each language
	public string 											Type;	// the Type of Requirement (course, credit, task)
	public CertificationCreditRequirementDetailsDataModel 	Credit;	// CertificationCreditRequirementDetailsDataModel object that stores the Credit Requirement details
	
	// Default Constructor
	public CertificationCreditRequirementDataModel()
	{
		this.id		    = 0;
		this.Labels     = new List<CertificationLanguageValueDataModel>();
		this.Type 	    = null; // this will always be "credit" for this Type
		this.Credit     = new CertificationCreditRequirementDetailsDataModel();
	}
}
*/
#endregion

#region CertificationCreditRequirementDetailsDataModel
public class CertificationCreditRequirementDetailsDataModel
{
	// Properties
	public bool? 			                                    isAny;	 // corresponds to if learner must complete any course or all courses
	public double?			                                    Minimum; // Minimum credits required for the requirement to be met
	public List<CertificationRequirementCourseIdDataModel> 		Items;	 // List of CertificationRequirementCourseIdDataModel objects that contain course id
	
	// Default Constructor
	public CertificationCreditRequirementDetailsDataModel()
	{
		this.isAny		= null;
		this.Minimum 	= null;
		this.Items		= new List<CertificationRequirementCourseIdDataModel>();
	}
}
#endregion

#region CertificationTaskRequirementDataModel
/*
public class CertificationTaskRequirementDataModel
{
	// Properties
	public int? 											id;		// Requirement id
	public List<CertificationLanguageValueDataModel> 		Labels;	// List of CertificationLanguageValueDataModel objects that determine Requirement Label for each language
	public string 											Type;	// the Type of Requirement (course, credit, task)
	public CertificationTaskRequirementDetailsDataModel 	Task;	// CertificationTaskRequirementDetailsDataModel object that stores the Task Requirement details
	
	// Default Constructor
	public CertificationTaskRequirementDataModel()
	{
		this.id		= 0;
		this.Labels	= new List<CertificationLanguageValueDataModel>();
		this.Type 	= null; // this will always be "task" for this Type
		this.Task 	= new CertificationTaskRequirementDetailsDataModel();
	}
}
*/
#endregion

#region CertificationTaskRequirementDetailsDataModel
public class CertificationTaskRequirementDetailsDataModel
{
	// Properties
	public bool? 													RequireDocumentation;	// whether documentation is required for the task
	public CertificationTaskRequirementDetailsSignoffDataModel		Signoff;				// CertificationTaskRequirementDetailsSignoffDataModel object that contains details of Signoff
	
	// Default Constructor
	public CertificationTaskRequirementDetailsDataModel()
	{
		this.RequireDocumentation	= null;
		this.Signoff 				= new CertificationTaskRequirementDetailsSignoffDataModel();
	}
}
#endregion

#region CertificationTaskRequirementDetailsSignoffDataModel
public class CertificationTaskRequirementDetailsSignoffDataModel
{
	// Properties
	public bool? 	Required;		    // whether signoff is required
	public bool? 	ByAdministrators;	// whether signoff is required by Administrators
	public bool? 	BySupervisor;	    // whether signoff is required by learner's supervisor(s)
	//public bool? 	ByExpert;		    // whether signoff is required by an expert
	
	// Default Constructor
	public CertificationTaskRequirementDetailsSignoffDataModel()
	{
		this.Required			= null;
		this.ByAdministrators 	= null;
		this.BySupervisor 		= null;
		//this.ByExpert			= null;
	}
}
#endregion

#region CertificationApplicableCourseDataModel
public class CertificationApplicableCourseDataModel
{
	// Properties
	public int? 		id;				// id of the course
	public string 		Name;			// Display name of the course
	public double? 		Credits;		// Number of credits the course is worth
	
	// Default Constructor
	public CertificationApplicableCourseDataModel()
	{
		this.id				= 0;
		this.Name			= null;
		this.Credits 		= null;
	}
}
#endregion