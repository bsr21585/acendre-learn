﻿/*
METHOD: OnRuleSetModifyClick
Method that is used to launch the modal popup for adding/modifying a ruleset.
*/
function OnRuleSetModifyClick(idRuleset) {

    $("#IdRulesetModalHidden").val(idRuleset);

    // launch the modal
    document.getElementById("AddModifyRuleSetHiddenButton").click();

    // change the modal header
    if ($("#" + IdRulesetModalHidden).val() == "" || $("#" + IdRulesetModalHidden).val() == "0") {
        $("#AddModifyRuleSetModalModalPopupHeaderText").text(AddRuleSetModalHeader);
    }

    // clear the name field
    $("input[id^='" + RuleSetLabel_Field + "']").val("");

    // load the modal
    document.getElementById(ModifyRuleSetModalButton).click();

    var ruleRows = $("tr.RuleRow");

    if (ruleRows != null && ruleRows.length <= 1) {
        $(".RemoveRuleButton").hide();
    }

    return false;
}

/*
METHOD: HideModalPopup
Method that is used to hide the ruleset modal popup.
*/
function HideModalPopup() {
    $("#AddModifyRuleSetModal").hide();
    $("#AddModifyRuleSetModalModalPopupExtender_backgroundElement").css("display", "none");
    return false;
}

function pageLoad() {

    var idRuleset = $("#IdRulesetModalHidden").val();

    if (idRuleset == "" || idRuleset == "0") {
        $("#AddModifyRuleSetModalModalPopupHeaderText").html(AddRuleSetModalHeader);
    }
}