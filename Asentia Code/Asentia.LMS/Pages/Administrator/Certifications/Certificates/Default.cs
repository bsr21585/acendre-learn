﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SysDrawing = System.Drawing;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.Certifications.Certificates
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Public Properties
        public Panel CertificatesFormContentWrapperContainer;
        public Panel CertificationObjectMenuContainer;
        public Panel CertificatesWrapperContainer;
        public Panel ObjectOptionsPanel;
        public Panel ActionsPanel;
        public UpdatePanel CertificateGridUpdatePanel;
        public Grid CertificateGrid;
        #endregion

        #region Private Properties
        private Certification _CertificationObject;

        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmAction;

        /* NEW CERTIFICATE FROM TEMPLATE */

        private ModalPopup _CertificateTemplatesModal;
        private DynamicListBox _CertificateTemplates;
        private Button _PreviewCertificateTemplateButton;
        private Button _UseCertificateTemplateButton;

        private ModalPopup _PreviewCertificateTemplateModal;
        private Button _HiddenCertificateTemplatePreviewButton;
        #endregion 

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.Certificates.Default.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CertificationsManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("Certificate.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/certifications/certificates/Default.css");

            // get the certification object
            this._GetCertificationObject();

            // build the breadcrumb and page title.
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // set container classes
            this.CertificatesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CertificatesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the certification object menu
            if (this._CertificationObject != null)
            {
                CertificationObjectMenu certificationObjectMenu = new CertificationObjectMenu(this._CertificationObject);
                certificationObjectMenu.SelectedItem = CertificationObjectMenu.MenuObjectItem.Certificates;

                this.CertificationObjectMenuContainer.Controls.Add(certificationObjectMenu);
            }

            // build the grid, actions panel, and modals
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.CertificateGrid.BindData();
            }
        }
        #endregion

        #region _GetCertificationObject
        /// <summary>
        /// Gets a certification object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCertificationObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("cid", 0);

            if (qsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                try
                {
                    if (id > 0)
                    { this._CertificationObject = new Certification(id); }
                }
                catch
                { Response.Redirect("~/administrator/certifications"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get certification title information
            string certificationNameInInterfaceLanguage = this._CertificationObject.Title;
            string certificationImagePath;
            string certificationImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Certification.LanguageSpecificProperty certificationLanguageSpecificProperty in this._CertificationObject.LanguageSpecificProperties)
                {
                    if (certificationLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { certificationNameInInterfaceLanguage = certificationLanguageSpecificProperty.Title; }
                }
            }

            certificationImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Certifications, "/administrator/certifications"));
            breadCrumbLinks.Add(new BreadcrumbLink(certificationNameInInterfaceLanguage, "/administrator/certifications/Dashboard.aspx?id=" + this._CertificationObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Certificates));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, certificationNameInInterfaceLanguage, certificationImagePath, _GlobalResources.Certificates, ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG), certificationImageCssClass);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD CERTIFICATE
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddCertificateLink",
                                                null,
                                                "Modify.aspx?cid=" + this._CertificationObject.Id.ToString(),
                                                null,
                                                _GlobalResources.NewCertificate,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            // ADD CERTIFICATE FROM TEMPLATE
            LinkButton newCertificateFromTemplateButton = new LinkButton();
            newCertificateFromTemplateButton.ID = "NewCertificateFromTemplateButton";

            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("NewCertificateFromTemplateLink",
                                                newCertificateFromTemplateButton,
                                                null,
                                                null,
                                                _GlobalResources.NewCertificateFromTemplate,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);

            // build modals for certificate template selection and preview            
            this._HiddenCertificateTemplatePreviewButton = new Button();
            this._HiddenCertificateTemplatePreviewButton.Style.Add("display", "none");
            this._HiddenCertificateTemplatePreviewButton.ID = "HiddenCertificateTemplatePreviewButton";
            this.ActionsPanel.Controls.Add(this._HiddenCertificateTemplatePreviewButton);

            this._BuildCertificateTemplatesModal(newCertificateFromTemplateButton.ID);
            this._BuildPreviewCertificateTemplateModal();            
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the certificate grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.CertificateGrid.StoredProcedure = Library.Certificate.GridProcedure;
            this.CertificateGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.CertificateGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.CertificateGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.CertificateGrid.AddFilter("@idObject", SqlDbType.Int, 4, this._CertificationObject.Id);
            this.CertificateGrid.AddFilter("@objectType", SqlDbType.Int, 4, 3);
            this.CertificateGrid.IdentifierField = "idCertificate";
            this.CertificateGrid.DefaultSortColumn = "name";
            this.CertificateGrid.SearchBoxPlaceholderText = _GlobalResources.SearchCertificates;

            // data key names
            this.CertificateGrid.DataKeyNames = new string[] { "idCertificate" };

            // columns
            GridColumn nameIssuingCredits = new GridColumn(_GlobalResources.Name + ", " + " (" + _GlobalResources.Code + "), " + _GlobalResources.Issuing + ", " + _GlobalResources.Credits + "", null, "name"); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.CertificateGrid.AddColumn(nameIssuingCredits);

            // add row data bound event
            this.CertificateGrid.RowDataBound += this._CertificateGrid_RowDataBound;
        }
        #endregion

        #region _CertificateGrid_RowDataBound
        private void _CertificateGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idCertificate = Convert.ToInt32(rowView["idCertificate"]);                

                // AVATAR, NAME, (CODE), CREDITS

                string name = Server.HtmlEncode(rowView["name"].ToString());
                string code = Server.HtmlEncode(rowView["code"].ToString());
                string issuing = Server.HtmlEncode(rowView["issuingOrganization"].ToString());
                int credits = Convert.ToInt32(rowView["credits"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = name;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // name
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink nameLink = new HyperLink();
                nameLink.NavigateUrl = "Modify.aspx?cid=" + this._CertificationObject.Id.ToString() + "&id=" + idCertificate.ToString();
                nameLink.Text = name;
                nameLabel.Controls.Add(nameLink);

                // code
                if (!String.IsNullOrWhiteSpace(code))
                {
                    Label codeLabel = new Label();
                    codeLabel.CssClass = "GridSecondaryTitle";
                    codeLabel.Text = "(" + code + ")";
                    e.Row.Cells[1].Controls.Add(codeLabel);
                }

                // issuing organization
                Label issuingLabel = new Label();
                issuingLabel.CssClass = "GridSecondaryLine";
                issuingLabel.Text = issuing;
                e.Row.Cells[1].Controls.Add(issuingLabel);

                // credits
                Label creditsLabel = new Label();
                creditsLabel.CssClass = "GridSecondaryLine";
                creditsLabel.Text = credits.ToString() + " " + ((credits != 1) ? _GlobalResources.Credits : _GlobalResources.Credit);
                e.Row.Cells[1].Controls.Add(creditsLabel);
            }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedCertificate_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedCertificate_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseCertificate_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            // add modal to container
            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.CertificateGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.CertificateGrid.Rows[i].FindControl(this.CertificateGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.Certificate.Delete(recordsToDelete);                    

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedCertificatesHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoCertificate_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.CertificateGrid.BindData();
            }
        }
        #endregion

        #region New Certificate From Template
        #region _BuildCertificateTemplatesModal
        /// <summary>
        /// Builds the modal for selecting a certificate template.
        /// </summary>
        /// <param name="targetControlId">the control that triggers modal launch</param>
        private void _BuildCertificateTemplatesModal(string targetControlId)
        {
            this._CertificateTemplatesModal = new ModalPopup("CertificateTemplatesModal");
            this._CertificateTemplatesModal.Type = ModalPopupType.Form;
            this._CertificateTemplatesModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
            this._CertificateTemplatesModal.HeaderIconAlt = _GlobalResources.CertificateTemplates;
            this._CertificateTemplatesModal.HeaderText = _GlobalResources.CertificateTemplates;
            this._CertificateTemplatesModal.TargetControlID = targetControlId;
            this._CertificateTemplatesModal.ReloadPageOnClose = false;
            this._CertificateTemplatesModal.CloseButton.Visible = false;
            this._CertificateTemplatesModal.SubmitButton.Visible = false;
            this._CertificateTemplatesModal.DisableSubmitButton();

            // build modal body

            // build a container for the certificate listing
            this._CertificateTemplates = new DynamicListBox("CertificateTemplateListing");
            this._CertificateTemplates.NoRecordsFoundMessage = _GlobalResources.NoCertificateTemplatesFound;
            this._CertificateTemplates.IncludeSelectAllNone = false;
            this._CertificateTemplates.IsMultipleSelect = false;
            this._CertificateTemplates.IsSearchable = true;
            this._CertificateTemplates.SearchButton.Command += new CommandEventHandler(this._SearchCertificateTemplatesButton_Command);
            this._CertificateTemplates.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchCertificateTemplatesButton_Command);
            this._CertificateTemplates.ListBoxControl.DataSource = Certificate.IdsAndNamesForObject(null, null);
            this._CertificateTemplates.ListBoxControl.DataTextField = "name";
            this._CertificateTemplates.ListBoxControl.DataValueField = "idCertificate";
            this._CertificateTemplates.ListBoxControl.DataBind();

            Panel certificateTemplatesModalActionsPanel = new Panel();
            certificateTemplatesModalActionsPanel.ID = "CertificateTemplatesModalActionsPanel";

            this._PreviewCertificateTemplateButton = new Button();
            this._PreviewCertificateTemplateButton.ID = "PreviewCertificateTemplateButton";
            this._PreviewCertificateTemplateButton.CssClass = "Button ActionButton";
            this._PreviewCertificateTemplateButton.Text = _GlobalResources.Preview;
            this._PreviewCertificateTemplateButton.Click += new EventHandler(this._PreviewCertificateTemplateButton_Click);

            this._UseCertificateTemplateButton = new Button();
            this._UseCertificateTemplateButton.ID = "UseCertificateTemplateButton";
            this._UseCertificateTemplateButton.CssClass = "Button ActionButton";
            this._UseCertificateTemplateButton.Text = _GlobalResources.UseTemplate;
            this._UseCertificateTemplateButton.OnClientClick = "RedirectToCertificateTemplate(" + this._CertificationObject.Id.ToString() + "); return false;";

            certificateTemplatesModalActionsPanel.Controls.Add(this._PreviewCertificateTemplateButton);
            certificateTemplatesModalActionsPanel.Controls.Add(this._UseCertificateTemplateButton);

            // add controls to modal body
            this._CertificateTemplatesModal.AddControlToBody(this._CertificateTemplates);
            this._CertificateTemplatesModal.AddControlToBody(certificateTemplatesModalActionsPanel);

            // add modal to container
            this.ActionsPanel.Controls.Add(this._CertificateTemplatesModal);
        }
        #endregion

        #region _SearchCertificateTemplatesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Certificate Templates" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchCertificateTemplatesButton_Command(object sender, CommandEventArgs e)
        {            
            // do the search and bind the data
            this._QueryAndBindCertificateTemplateList(this._CertificateTemplates.SearchTextBox.Text);
        }

        #endregion

        #region _ClearSearchCertificateTemplatesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Certificate Templates" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchCertificateTemplatesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the search text box
            this._CertificateTemplates.SearchTextBox.Text = "";

            // clear and bind the data
            this._QueryAndBindCertificateTemplateList(null);
        }
        #endregion

        #region _QueryAndBindCertificateTemplateList
        /// <summary>
        /// Method to perform the query and bind the listbox with certificate templates.
        /// </summary>
        /// <param name="searchParam">search param</param>
        private void _QueryAndBindCertificateTemplateList(string searchParam)
        {
            // clear the modal's feedback panel
            this._CertificateTemplatesModal.ClearFeedback();

            // clear the listbox control
            this._CertificateTemplates.ListBoxControl.Items.Clear();

            // perform the search
            this._CertificateTemplates.ListBoxControl.DataSource = Certificate.IdsAndNamesForObject(null, searchParam);
            this._CertificateTemplates.ListBoxControl.DataTextField = "name";
            this._CertificateTemplates.ListBoxControl.DataValueField = "idCertificate";
            this._CertificateTemplates.ListBoxControl.DataBind();

            // if no records available then disable the buttons
            if (this._CertificateTemplates.ListBoxControl.Items.Count == 0)
            {
                this._PreviewCertificateTemplateButton.Enabled = false;
                this._PreviewCertificateTemplateButton.CssClass = "Button ActionButton DisabledButton";

                this._UseCertificateTemplateButton.Enabled = false;
                this._UseCertificateTemplateButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._PreviewCertificateTemplateButton.Enabled = true;
                this._PreviewCertificateTemplateButton.CssClass = "Button ActionButton";

                this._UseCertificateTemplateButton.Enabled = true;
                this._UseCertificateTemplateButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _BuildPreviewCertificateTemplateModal
        /// <summary>
        /// Builds the modal for previewing a certificate template.
        /// </summary>
        private void _BuildPreviewCertificateTemplateModal()
        {
            this._PreviewCertificateTemplateModal = new ModalPopup("PreviewCertificateTemplateModal");
            this._PreviewCertificateTemplateModal.Type = ModalPopupType.Form;
            this._PreviewCertificateTemplateModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
            this._PreviewCertificateTemplateModal.HeaderIconAlt = _GlobalResources.CertificateTemplate;
            this._PreviewCertificateTemplateModal.HeaderText = _GlobalResources.CertificateTemplate;
            this._PreviewCertificateTemplateModal.ReloadPageOnClose = false;
            this._PreviewCertificateTemplateModal.TargetControlID = this._HiddenCertificateTemplatePreviewButton.ID;
            this._PreviewCertificateTemplateModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._PreviewCertificateTemplateModal.SubmitButtonCustomText = _GlobalResources.UseTemplate;
            this._PreviewCertificateTemplateModal.SubmitButton.OnClientClick = "RedirectToCertificateTemplate(" + this._CertificationObject.Id.ToString() + "); return false;";

            // build the body
            Panel previewCertificateTemplateCertificateContainer = new Panel();
            previewCertificateTemplateCertificateContainer.ID = "PreviewCertificateTemplateModal_CertificateContainer";
            previewCertificateTemplateCertificateContainer.CssClass = "PreviewCertificateTemplateCertificateContainer";

            HtmlGenericControl certificateTemplateImageWrapper = new HtmlGenericControl("div");
            certificateTemplateImageWrapper.ID = "PreviewCertificateTemplateModal_CertificateTemplateImageWrapper";

            Image certificateTemplateBackgroundImage = new Image();
            certificateTemplateBackgroundImage.ID = "PreviewCertificateTemplateModal_CertificateTemplateBackgroundImage";

            certificateTemplateImageWrapper.Controls.Add(certificateTemplateBackgroundImage);
            previewCertificateTemplateCertificateContainer.Controls.Add(certificateTemplateImageWrapper);

            // add controls to modal body
            this._PreviewCertificateTemplateModal.AddControlToBody(previewCertificateTemplateCertificateContainer);

            // add modal to container
            this.ActionsPanel.Controls.Add(this._PreviewCertificateTemplateModal);
        }
        #endregion        

        #region _PreviewCertificateTemplateButton_Click
        /// <summary>
        /// Loads the certificate template preview into the modal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _PreviewCertificateTemplateButton_Click(object sender, EventArgs e)
        {            
            if (!String.IsNullOrWhiteSpace(this._CertificateTemplates.ListBoxControl.SelectedValue))
            {
                // clear the modal's feedback container.
                this._CertificateTemplatesModal.ClearFeedback();

                // load the certificate object
                int idCertificate = Convert.ToInt32(this._CertificateTemplates.ListBoxControl.SelectedValue);
                Certificate certificateObject = new Certificate(idCertificate);
                
                // draw the certificate template
                Image certificateTemplateBackgroundImagePlaceholder = (Image)this._PreviewCertificateTemplateModal.FindControl("PreviewCertificateTemplateModal_CertificateTemplateBackgroundImage");
                string backgroundImageName = String.Empty;
                int backgroundImageHeight = 0;
                int backgroundImageWidth = 0;
                SysDrawing::Image imageObject = null;

                if (certificateObject != null && File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + certificateObject.Id + "/" + certificateObject.FileName)))
                {
                    backgroundImageName = SitePathConstants.SITE_CERTIFICATES_ROOT + certificateObject.Id + "/" + certificateObject.FileName;
                }
                else if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "default.jpg")))
                {
                    backgroundImageName = SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "default.jpg";
                }

                if (File.Exists(HttpContext.Current.Server.MapPath(backgroundImageName)))
                {
                    imageObject = SysDrawing::Image.FromFile(HttpContext.Current.Server.MapPath(backgroundImageName));
                    Certificate.GetImageHeightWidth(imageObject.Width, imageObject.Height, Certificate.CertificateImageDefaultWidth, Certificate.CertificateImageDefaultHeight, out backgroundImageWidth, out backgroundImageHeight);

                    if (certificateTemplateBackgroundImagePlaceholder != null)
                    {
                        certificateTemplateBackgroundImagePlaceholder.ImageUrl = backgroundImageName + "?" + Guid.NewGuid();
                        certificateTemplateBackgroundImagePlaceholder.Width = Unit.Pixel(backgroundImageWidth);
                        certificateTemplateBackgroundImagePlaceholder.Height = Unit.Pixel(backgroundImageHeight);                        
                    }
                }                

                // initialize startup scripts
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "PreviewCertificateTemplateModalInitialize", "$('#HiddenCertificateTemplatePreviewButton').click();", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "PreviewCertificateTemplateModalLoad", "$('#PreviewCertificateTemplateModalModalPopupHeaderWrapper span').text(\"" + this._CertificateTemplates.ListBoxControl.SelectedItem.Text + "\");", true);
            }
            else
            { this._CertificateTemplatesModal.DisplayFeedback(_GlobalResources.PleaseSelectACertificateTemplateForPreview, true); }
        }
        #endregion
        #endregion
    }
}
