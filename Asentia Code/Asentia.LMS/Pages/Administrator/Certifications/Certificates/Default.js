﻿function RedirectToCertificateTemplate(idCertification) {
    var idCertificate = $('#CertificateTemplateListingListBox :selected').val();

    window.location.replace("/administrator/certifications/certificates/Modify.aspx?cid=" + idCertification + "&id=" + idCertificate);
}