CE_SegmentInterface = function(
	parent,
	identifier,
	node,
	languages,
	initialLanguage,
	flagImagePath,
	deleteImagePath,
	addImagePath, 
	data
	){
	
	this.Parent 			= parent;
	this.Identifier			= identifier;
	this.Data 				= data;
	this.Languages 			= languages;
	this.FlagImagePath 		= flagImagePath;
	this.DeleteImagePath 	= deleteImagePath;
	this.AddImagePath 		= addImagePath;
	this.InitialLanguage	= initialLanguage;
	
	this.Interface = this.Parent.Parent.Parent;
	
	if (typeof data == "object") {
		this.Data = data;
	}else{
		this.Data = new Object;
		this.Data.Labels = [];
		this.Data.Requirements = new Object;
		this.Data.Requirements.Items = [];
	}
	
	this.TypeName			= "SegmentInterface";
	this.Requirements = [];
	this.AndOrs = [];
	
	this.RequirementCounter = 0;
	
	this.Panel = document.createElement("div");
		this.Panel.className = "SegmentContainer";
		this.Panel.id = this.Identifier;//Added
	
	// Delete Segment Button
	this.DeleteButton = new IconButton(
		this,
		this.Panel,
		this.DeleteImagePath,
		this.Interface.GetDictionaryTerm("Delete Segment"),
		true, 
		"DeleteSegmentButton"
		)
		
	this.DeleteButton.onclick = function(){
		if (!this.Disabled){
			//this.Parent.Parent.DeleteSegment(this.Parent.Panel);
			this.Parent.Parent.PromptToDeleteSegment(this.Parent.Panel);
		}
	}
	
	// Heading
	
	//this.Heading = document.createElement("div");
	//	this.Heading.className = "title";
	//	this.Heading.append(HTML.text("Segment"));
		
	//this.Panel.append(this.Heading);
	
	this.Label = new FormInputField(
		this, 					// parent
		this.Panel,				// attach to
		"input",				// type
		this.Interface.GetDictionaryTerm("Label"),  				// label
		true, 					// isRequired
		this.Languages, 		// languages
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		this.Data.Labels,			// JSON data
		"InputMedium",			// input class
		this.Interface.GetDictionaryTerm("New Segment")			// default label
	)
	
	// REQUIREMENTS
	
	try {this.RequirementsIsAny = this.Data.Requirements.isAny;}catch(e){this.RequirementsIsAny = false}
	
	this.RequirementsContainer = document.createElement("div");
		this.RequirementsContainer.className = "FormFieldContainer";
		
		var requirementsLabel = document.createElement("div");
			requirementsLabel.className = "FormFieldLabelContainer";
			requirementsLabel.append(HTML.text(this.Interface.GetDictionaryTerm("Requirements") + ":"));
		
	this.Panel.append(this.RequirementsContainer);
	
	/* New Requirement Button */
	
	this.NewRequirementClicker = new CE_NewRequirementClicker(this);
	this.RequirementsContainer.append(this.NewRequirementClicker.Container);
	
	/* EACH REQUIREMENT */

	try{
		for (var i = 0; i < this.Data.Requirements.Items.length; i++){
			this.PushRequirementInterface(this.Data.Requirements.Items[i]);
		}
	} catch(e){} // do not create empty requirements
		
	
		
	try{
		node.append(this.Panel);
	}catch(e){}
}

CE_SegmentInterface.prototype.ToggleRequirementsAndOr = function(){
	
	this.RequirementsIsAny = !this.RequirementsIsAny;
	
	for (var i = 0; i < this.AndOrs.length; i++){
		if(this.AndOrs[i].className == "RequirementAndOr"){
			var ao = this.AndOrs[i];
			ao.removeChild(ao.childNodes[0]);
			ao.append(HTML.text(  (this.RequirementsIsAny) ? this.Interface.GetDictionaryTerm("OR") : this.Interface.GetDictionaryTerm("AND") ));
		}
	}
	
}

CE_SegmentInterface.prototype.PushRequirementInterface = function(
	data // data should always be an object
	){
	//find the existing id in the array and replace if necessary, otherwise, use a letter ID
	
	var id = data.id;
	var R = new CE_RequirementInterface(
			this,
			this.Identifier + "-Requirement" + this.RequirementCounter,
			null,
			this.Languages,						// languages
			this.InitialLanguage,				// initialLanguage
			this.FlagImagePath,					// path to flag images
			this.DeleteImagePath,				// path to delete image
			this.AddImagePath,					// path to delete image
			data								// JSON data
			);
	
	var index = -1;

	// only for updates, find and replace the existing
	
	for (var i = 0; i < this.Requirements.length; i++){
		if(this.Requirements[i].Data.id == id){
			
			this.RequirementsContainer.insertBefore(R.Container, this.Requirements[i].Container);
			this.RequirementsContainer.removeChild(this.Requirements[i].Container);
			
			this.Requirements[i] = R; // replace in the array
			return true;
		}
	}
	
	// only new additions should go beyond here
	
	this.Requirements.push(R);

	this.RequirementCounter++; 
	
	this.Requirements[this.Requirements.length - 1].Index = this.Requirements.length - 1; 
	
	if(this.Requirements.length != 1){ // do not add an "and/or" for the first addition
		this.AndOrs.push(new CE_RequirementAndOr(this).Container);
		this.AndOrs[this.AndOrs.length - 1].className = "RequirementAndOr";
		this.RequirementsContainer.insertBefore(this.AndOrs[this.AndOrs.length - 1], this.NewRequirementClicker.Container);
	}
	
	this.RequirementsContainer.insertBefore(this.Requirements[this.Requirements.length - 1].Container, this.NewRequirementClicker.Container);
	//this.RequirementsContainer.append(this.Requirements[this.Requirements.length - 1].Container);
	
	//check if any Delete buttons need to be re-enabled
	this.UpdateDeleteButtonEnabledOnAllRequirements();
}

CE_SegmentInterface.prototype.UpdateAllRequirementInterfaces = function()
{
	//Requirement interfaces need updating when number of credits in a course changes
	//console.log(this.Parent.Parent.Parent.Data.Units.Initial.Items);
	for (var i = 0;  i < this.Requirements.length; i++)
	{
		this.Requirements[i].UpdateAvailableCourses();
		this.Requirements[i].UpdateRequirementInterface();
	}
	
}

CE_SegmentInterface.prototype.DeleteRequirementsWithNoCourses = function()
{
	//after a course is deleted, Requirements that had that course as the only item are now left hanging and will need to be removed
	var requirementIdsToRemove = new Array();
	
	//get the id's of the Requirements to be deleted
	for (var i = 0; i < this.Requirements.length; i++)
	{
		if (this.Requirements[i].GetType() == "credit" || this.Requirements[i].GetType() == "course")
		{
			var courseIds = new Array();
			courseIds = this.Requirements[i].GetCourseIdList();
			
			if (!courseIds || courseIds.length == 0)
				requirementIdsToRemove.push(this.Requirements[i].GetId());

		}
	}
	
	//console.log("***REQUIREMENTS TO REMOVE***");
	//console.log(requirementIdsToRemove);
	
	//delete the requirements by Id
	for (var i = 0; i < requirementIdsToRemove.length; i++)
	{
		var currentRequirementId = requirementIdsToRemove[i];
		var currentRequirementObject = this.GetRequirementById(currentRequirementId);
		
		//console.log('**********REMOVE THIS REQMNT*************');
		//console.log(currentRequirementObject);
		this.DeleteRequirement(currentRequirementObject);
	}
	
}

CE_SegmentInterface.prototype.ValidateCourseCreditChange = function(courseId, updatedNumberOfCredits)
{
	try{
		//console.log(this.Requirements[1].Data.Credit.Minimum);
		//console.log(this.Requirements);
	}catch(e){};
	//validate that the # of credits change does not break any current rules when user changes it in the "Attached Courses" item list
	for (var i = 0; i < this.Requirements.length; i++)
	{
		//needs to check only "credit" requirement types
		if (this.Requirements[i].GetType() == "credit")
		{
			//get the minimum credits
			var thisCreditMinimum = this.Requirements[i].Data.Credit.Minimum;
			
			//track the new number of credits after the impending change
			var totalCreditsAfterChange = 0;
			
			//get the active course Ids currently in the Requirements list
			var courseIds = new Array();
			
			//if isAny, pull all available Id's directly from the pool
			if (this.Requirements[i].GetIsAny())
				courseIds = this.Interface.GetCourseIdListFromPool();
			else
				courseIds = this.GetCreditRequirementCourseIdList(this.Requirements[i].Data.Credit);
			//console.log("************************COURSE IDS LENGTH: " + courseIds.length);
			
			//console.log("Updated number of creds: " + updatedNumberOfCredits);
			//calculate what the new total of credits would be
			var totalCreditsAfterChange = this.GetTotalRequirementCreditsAfterPendingCreditsChange(courseIds, courseId, updatedNumberOfCredits);
			
			//compare new amount to mimimum number of credits
			//console.log("Credit minimum: " + thisCreditMinimum);
			//console.log("Credits after change: " + totalCreditsAfterChange);
			if (totalCreditsAfterChange < thisCreditMinimum)
				return false;
			
		}
	}
	
	/*for (var i = 0; i < this.Data.Requirements.Items.length; i++)
	{
		//needs to check only "credit" requirement types
		if (this.Data.Requirements.Items[i].Type == "credit")
		{
			//get the minimum credits
			var thisCreditMinimum = this.Data.Requirements.Items[i].Credit.Minimum;
			
			//track the new number of credits after the impending change
			var totalCreditsAfterChange = 0;
			
			//get the active course Ids currently in the Requirements list
			var courseIds = new Array();
			courseIds = this.GetCreditRequirementCourseIdList(this.Data.Requirements.Items[i].Credit);
			
			//calculate what the new total of credits would be
			var totalCreditsAfterChange = this.GetTotalRequirementCreditsAfterPendingCreditsChange(courseIds, courseId, updatedNumberOfCredits);
			
			//compare new amount to mimimum number of credits
			console.log("Credit minimum: " + thisCreditMinimum);
			console.log("Credits after change: " + totalCreditsAfterChange);
			if (totalCreditsAfterChange < thisCreditMinimum)
				return false;
			
		}
	}*/

	return true;	
}

CE_SegmentInterface.prototype.GetCreditRequirementCourseIdList = function(creditObject)
{
	//uses the Credit object (this.Data.Requirements.Items[x].Credit) to grab the Id's of the courses listed for this requirement
	var courseIds = new Array();
	
	for (var i = 0; i < creditObject.Items.length; i++)
	{
		var thisCourseId = creditObject.Items[i].id;
		
		courseIds.push(thisCourseId);
	}
	
	return courseIds;
}

CE_SegmentInterface.prototype.GetTotalRequirementCreditsAfterPendingCreditsChange = function(courseIdList, targetCourseId, updatedNumberOfCredits)
{
	//uses courseIdList array to tally the number of credits total for the courses in the list if the user-prompted change was to be made (for error-checking to block any changes that invalidate any requirements in place)
	//calculate what the new total of credits would be
	var totalCreditsAfterChange = 0;
	
	for (var i = 0; i < courseIdList.length; i++)
	{
		var currentCourseId = courseIdList[i];
		var currentCourseCredits = this.Interface.GetCourseCreditsFromPool(currentCourseId);
		
		//when adding credits for the course being updated, use the new value of credits instead of the current number
		if (typeof currentCourseCredits == "number")
		{
			if (currentCourseId != targetCourseId)
				totalCreditsAfterChange += currentCourseCredits;
			else if (currentCourseId == targetCourseId)
				totalCreditsAfterChange += Number(updatedNumberOfCredits);
		}
	}
	
	return totalCreditsAfterChange;
}

CE_SegmentInterface.prototype.GetTotalRequirementCreditsNotIncludingCourse = function(courseIdList, targetCourseId)
{
	//uses courseIdList array to tally the number of credits total for the courses in the list, not including the course with the specified courseId
	var totalCreditsNotIncludingCourse = 0;
	
	for (var i = 0; i < courseIdList.length; i++)
	{
		var currentCourseId = courseIdList[i];
		
		//when adding credits for the course being updated, use the new value of credits instead of the current number
		if (currentCourseId != targetCourseId)
		{
			var currentCourseCredits = this.Interface.GetCourseCreditsFromPool(currentCourseId);
			
			if (typeof currentCourseCredits == "number")
				totalCreditsNotIncludingCourse += currentCourseCredits;
		}
	}
	
	return totalCreditsNotIncludingCourse;
}

CE_SegmentInterface.prototype.GetTotalRequirementCredits = function(courseIdList)
{
	//uses courseIdList array to tally the number of credits total for the courses in the list
	var totalCredits = 0;
	
	for (var i = 0; i < courseIdList.length; i++)
	{
		var currentCourseId = courseIdList[i];
		var currentCourseCredits = this.Interface.GetCourseCreditsFromPool(currentCourseId);
		
		//when adding credits for the course being updated, use the new value of credits instead of the current number
		if (typeof currentCourseCredits == "number")
			totalCredits += currentCourseCredits;
	}
	
	return totalCredits;
}

CE_SegmentInterface.prototype.GetRequirementById = function(id)
{
	//search all requirements in this segment by id and return the matching requirement
	for (var i = 0; i < this.Requirements.length; i++)
	{
		if (this.Requirements[i].GetId() == id)
			return this.Requirements[i];
	}
	
	//return undefined if nothing found
	return;
}

CE_SegmentInterface.prototype.ValidateSegmentCourseDeleteById = function(courseId)
{
	//check if there's a Requirement dependency on the course id provided
	var allSegmentsValid = true;
	
	for (var i = 0; i < this.Requirements.length; i++)
	{
		var currentRequirement = this.Requirements[i];

		if (!this.ValidateSegmentRequirementCourseDeleteById(courseId, currentRequirement))
			allSegmentsValid = false;
		
	}
	
	return allSegmentsValid;
}

CE_SegmentInterface.prototype.ValidateSegmentRequirementCourseDeleteById = function(courseId, requirementInterfaceObject)
{
	//check the requirement object to validate whether the courseId can be deleted from it without breaking any rules
	var rqObject = requirementInterfaceObject;
	
	if (rqObject.GetType() == "credit")
	{	
		//console.log("THIS IS ID " + rqObject.Data.id);
		
		//get the minimum credits
		var thisCreditMinimum = rqObject.Data.Credit.Minimum;
		
		//see if "Any Course" is checked
		//console.log("!!!");
		//console.log(rqObject.GetIsAny() == false ? "IT'S FALSE" : "IT'S TRUE");
		//console.log(rqObject.GetIsAny() == true);
		//console.log("!!!");
		var isAny = rqObject.GetIsAny();
		//console.log("Credit Type segment; isAny:");
		//console.log(typeof isAny);
		//console.log(isAny);
		//console.log(rqObject);
		//console.log("ID: " + rqObject.Data.id);
		
		//track the number of credits available not including the course in question
		var totalCreditsNotIncludingThisCourse = 0;
		var courseIds = new Array();
		
		if (isAny)//"Any Course" box is checked
		{
			//console.log("Any Course Checked");
			//if "Any Course" is checked, the courses in the this Requirement's Credit list are cleared out; must get full list of courses directly from the Attached Courses object
			courseIds = this.Interface.GetCourseIdListFromPool();
			//console.log(courseIds);
		}
		else//"Any Course" box is NOT checked
		{
			//console.log("Any Course NOT Checked");
			//grab listing of course Ids included in this Requirement
			courseIds = this.GetCreditRequirementCourseIdList(rqObject.Data.Credit);
			//console.log(courseIds);
		}
		
		//get the total number of credits all courses add up to, not including the course to be deleted
		totalCreditsNotIncludingThisCourse = this.GetTotalRequirementCreditsNotIncludingCourse(courseIds, courseId);
		//console.log(totalCreditsNotIncludingThisCourse);
		
		//return true or false depending if there are enough credits left to meet the minimum
		if (totalCreditsNotIncludingThisCourse >= thisCreditMinimum)
			return true;
		else if (totalCreditsNotIncludingThisCourse < thisCreditMinimum)
			return false;
		
	}
	else if (rqObject.GetType() == "course")
	{
		//console.log("found a course requirement:");
		//console.log(rqObject);
		
		//grab listing of course Ids included in this Requirement
		courseIds = this.GetCreditRequirementCourseIdList(rqObject.Data.Course);
		//console.log(courseIds);
		
		//console.log("Course is used in this reqmt?: " + this.DoesCourseIdExistInArray(courseId, courseIds));
		
		//If course is the only one in the list, allow option to delete the course and remove the Requirement altogether
		//regardless of whether "any selected course" or "all selected courses" is checked, allow the delete to happen as long as this is not the only course in this requirement
		if (this.DoesCourseIdExistInArray(courseId, courseIds) && courseIds.length > 1)
		{
			return true;
		}
		else if (this.DoesCourseIdExistInArray(courseId, courseIds) && courseIds.length == 1)
		{
			return false;
		}
		else if (!this.DoesCourseIdExistInArray(courseId, courseIds))
		{
			return true;
		}
		
	}
	else
	{
		//if it's of Type "task" or anything else, return true
		return true;
	}
}

CE_SegmentInterface.prototype.ReduceMinCreditsRequiredOnCourseDelete = function()
{
	//iterate thru all Requirements in this Segment and reduce the min required credits on any Credit Requirement where needed
	for (var i = 0; i < this.Requirements.length; i++)
	{
		var thisRequirement =  this.Requirements[i];

		//check any Requirement with a Type that is Credit
		if (thisRequirement.GetType() == "credit")
		{
			//check if the current credit minimum exceeds the total number of available credits
			var thisCreditMinimum = thisRequirement.Data.Credit.Minimum;
			//console.log("CE_SegmentInterface.prototype.ReduceMinCreditsRequiredOnCourseDelete. Credit Min: " + thisCreditMinimum);
			
			//get the number of total available credits
			var totalCreditsAvailable = thisRequirement.GetTotalCreditsOfAllCoursesInCreditRequirement();
			//console.log("CE_SegmentInterface.prototype.ReduceMinCreditsRequiredOnCourseDelete. Total Credits Avail: " + totalCreditsAvailable);
			
			if (totalCreditsAvailable != undefined)
			{
				//compare the values; if the current credit minimum is higher than the available credits, reduce the minimum to meet the total credits available
				if (thisCreditMinimum > totalCreditsAvailable)
				{
					thisRequirement.Data.Credit.Minimum = totalCreditsAvailable;
				}
			}
			
			//console.log("CE_SegmentInterface.prototype.ReduceMinCreditsRequiredOnCourseDelete. Final Min: " + thisRequirement.Data.Credit.Minimum);
		}
	}
}

CE_SegmentInterface.prototype.DoesCourseIdExistInArray = function(courseId, courseIdArray)
{
	//returns true/false on whether courseId exists in courseIdArray
	for (var i = 0; i < courseIdArray.length; i++)
	{
		if (courseId == courseIdArray[i])
			return true;
	}
	
	return false;
}

CE_SegmentInterface.prototype.PromptToDeleteRequirement = function(requirementObject)
{

	this.Interface.ShowAlertModal(
	 this.Interface.GetDictionaryTerm("Delete Requirement"),
	 this.Interface.GetDictionaryTerm("Are you sure you want to delete this requirement"),//"Are you sure you want to delete this requirement?"
	 this.Interface.GetDictionaryTerm("Yes"),
	 this.Interface.GetDictionaryTerm("No"),
	 {targetObject: this, functionName: "DeleteRequirement", functionParam: requirementObject}
	);
}

CE_SegmentInterface.prototype.DeleteRequirement = function(requirementObject)
{
	//get the id of the Requirement object to be deleted
	var targetRequirementId = requirementObject.GetId();
	var targetIndex = -1;
	
	//find the index of the Requirement object in the current list
	for (var i = 0; i < this.Requirements.length; i++)
	{
		if (this.Requirements[i].GetId() == targetRequirementId)
		{
			targetIndex = i;
			break;
		}
	}
	
	//remove the item at the found index
	if (targetIndex > -1)
	{
		//an AND/OR button will also need to be removed in certain cases if there is more than one Requirement in this segment
		if (this.Requirements.length > 1)
		{
			//if this is not the last requirement in the list, remove the AND/OR button sitting directly after the Requirement
			if (targetIndex < this.Requirements.length - 1)
			{
				//remove the AND/OR button from the array
				this.DeleteRequirementAndOrByIndex(targetIndex);
			}
			//if this is the last item in the list, remove the AND/OR button that directly precedes it
			else if (targetIndex == this.Requirements.length - 1)
			{
				//remove the AND/OR button from the array
				this.DeleteRequirementAndOrByIndex(targetIndex - 1);
			}
		}
		
		//remove the Requirement box from the Segment UI
		this.Requirements[i].RemoveFromUI();
		
		//splice out the requirement object
		this.Requirements.splice(targetIndex, 1);

	}
	
	//if only 1 requirement left, the Delete icon on that requirement should be disabled
	if (this.Requirements.length == 1)
		this.Requirements[0].UpdateDeleteButtonEnabled();
	
	//console.log(this.Requirements);
}

CE_SegmentInterface.prototype.DeleteRequirementAndOrByIndex = function(index)
{
	//remove the element from the UI
	var targetAndOrButtonElement = this.AndOrs[index];
	this.RequirementsContainer.removeChild(targetAndOrButtonElement);
	
	//remove the AND/OR element from the array
	this.AndOrs.splice(index, 1);
}

CE_SegmentInterface.prototype.UpdateDeleteButtonEnabled = function()
{
	//Delete button should be disabled if this is the only Segment left on a Unit
	if (this.Parent.Segments)
	{
		if (this.Parent.Segments.length < 2)
		{
			this.DeleteButton.Enable(false);
		}
		else
		{
			this.DeleteButton.Enable(true);
		}
	}
}

CE_SegmentInterface.prototype.UpdateDeleteButtonEnabledOnAllRequirements = function()
{
	//goes thru each Requirement to determine if its Delete button (X) should be enabled/disabled
	for (var i = 0; i < this.Requirements.length; i++)
	{
		this.Requirements[i].UpdateDeleteButtonEnabled();
	}
}

CE_SegmentInterface.prototype.GetRequirementsList = function()
{
	//returns a list of the CE_RequirementInterface objects on this segment
	return this.Requirements;
}

CE_SegmentInterface.prototype.GetId = function()
{
	//returns a list of the CE_RequirementInterface objects on this segment
	return this.Data.id;
}

CE_SegmentInterface.prototype.IsValidOrShowErrors = function()
{
	//check that the Label field is filled for default language
	this.InErrorState = false;
	
	//hide any previous errors
	this.Label.ClearError();
	
	var tabName = this.Parent.Parent.TabName;
	
	if (this.Label.IsInitialLanguageFieldEmpty())
	{
		this.Label.ShowError(this.Interface.GetDictionaryTerm("Label is required in the default language"));
		this.InErrorState = true;
		
		//store info about the error to the error lister
		this.Interface.AddToErrorList(
			tabName,
			this.Label,
			this.Interface.ErrorLister[tabName.toUpperCase() + "_SEGMENT_LABEL_REQUIRED"],
			this.Parent, //Unit
			this //Segment
		);
	}
	
	//segment must have at least 1 requirement to be valid
	this.ClearError();
	if (this.Requirements.length == 0)
	{
		this.ShowError(this.Interface.GetDictionaryTerm("segment must contain at least one requirement"));
		this.InErrorState = true;
		
		//store info about the error to the error lister
		this.Interface.AddToErrorList(
			tabName,
			this,
			this.Interface.ErrorLister[tabName.toUpperCase() + "_SEGMENT_NO_REQUIREMENTS"],
			this.Parent, //Unit
			this //Segment
		);
	}
	
	return !this.InErrorState;
}

CE_SegmentInterface.prototype.ShowError = function(s){
	
	this.ClearError();
	
	this.FormFieldErrorContainer = document.createElement("div");
		this.FormFieldErrorContainer.className = "FormFieldErrorContainer";
	var p = document.createElement("p");
		p.append(document.createTextNode(s));
	
	this.FormFieldErrorContainer.append(p);
	this.Panel.insertBefore(this.FormFieldErrorContainer, this.RequirementsContainer);
	
}

CE_SegmentInterface.prototype.ClearError = function(){

	// remove the existing error
	try{
		this.Panel.removeChild(this.FormFieldErrorContainer);
	} catch(e){}

}

CE_SegmentInterface.prototype.GetDataAsString = function()
{
	var s = "";
	
	//open this Segment item
	s += "{";
	
    // Id	
	if (this.Data.id != null) {
	    s += "\"id\":" + this.Data.id + ",";
	}
	else {
	    s += "\"id\":0,";
	}
	
	s += "\"Labels\":";
	s += this.Label.GetData() + ",";
	
	s += "\"Requirements\":{";

	if (this.RequirementsIsAny != null) {
	    s += "\"isAny\":" + this.RequirementsIsAny + ",";
	}
	else {
	    s += "\"isAny\":false,";
	}

	s += "\"Items\":[";
	for (var i = 0; i < this.Requirements.length; i++)
	{
		if (i > 0)
			s += ",";
		
		s += this.Requirements[i].GetDataAsString();
	}
	//close Requirement Items
	s += "]";
	//close Requirements
	s += "}";
	
	//Close this Segment item
	s += "}";
	
	return s;
}


CE_SegmentAndOr = function(
	parent
	){
		
	this.Parent = parent;
	this.TypeName = "SegmentAndOr";
	
	this.Container = document.createElement("div");
		this.Container.TypeName = "TheSegmentContainer";
		this.Container.Parent = this;
		this.Container.className = "AndOr";
	
	if (this.Parent.SegmentsIsAny == true){
		this.Container.append(HTML.text(this.Parent.Interface.GetDictionaryTerm("OR")));
	}else {
		this.Container.append(HTML.text(this.Parent.Interface.GetDictionaryTerm("AND")));
	}
	
	this.Container.onclick = function(){
		this.Parent.Parent.ToggleSegmentsAndOr();
	}
	
}