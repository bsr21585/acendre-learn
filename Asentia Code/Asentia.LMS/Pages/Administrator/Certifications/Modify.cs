﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Newtonsoft;
using Newtonsoft.Json;

namespace Asentia.LMS.Pages.Administrator.Certifications
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CertificationPropertiesFormContentWrapperContainer;
        public Panel CertificationObjectMenuContainer;
        public Panel CertificationPropertiesWrapperContainer;
        public Panel CertificationPropertiesInstructionsPanel;
        public Panel CertificationPropertiesFeedbackContainer;
        public Panel CertificationPropertiesContainer;
        public Panel CertificationFormContainer;
        public Panel CertificationAsentiaModalContainer;
        public Panel CertificationAsentiaJsModal;
        public Panel CertificationAsentiaModalAlertContainer;
        public Panel CertificationAsentiaJsModalAlert;
        public Panel CertificationPropertiesActionsPanel;
        public Panel CertificationPropertiesTabPanelsContainer;
        #endregion

        #region Private Properties
        private Certification _CertificationObject;

        private HiddenField _CertificationJSONData;
        private CertificationDataModel _CertificationDataModel;

        private Button _CertificationPropertiesSaveButton;
        private Button _CertificationPropertiesCancelButton;
        #endregion

        #region Page Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (
                !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE)
                || !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CertificationsManager)
               )
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("AsentiaCarousel.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/certifications/Modify.css");            

            // get the certification object
            this._GetCertificationObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;            

            // COMMON JS INTERFACE ITEMS
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.JSInterface.AsentiaCarousel.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.JSInterface.AsentiaModalWindow.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.JSInterface.FormInputField.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.JSInterface.HTMLCourseListBoxWithCredits.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.JSInterface.HTMLLibrary.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.JSInterface.IconButton.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.JSInterface.LanguageSelector.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.JSInterface.Tab.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.JSInterface.Tabs.js");

            // CERTIFICATION-SPECIFIC ITEMS
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_AttachCoursesInterface.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_CourseRequirementInterface.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_CreditRequirementInterface.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_ErrorLister.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_ErrorListerInterface.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_GeneralAlertInterface.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_RequirementInterface.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_RequirementSelectTypeInterface.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_SegmentInterface.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_TaskRequirementInterface.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CE_UnitInterface.js");
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Certifications.CertificationEditor.js");

            // build global JS variables
            StringBuilder globalJS = new StringBuilder();

            globalJS.AppendLine("var CertificationEditorObject;");
            globalJS.AppendLine("var CertificationPropertiesSaveButtonUID = \"" + this._CertificationPropertiesSaveButton.ID + "\";");

            // AVAILABLE LANGUAGES

            // get the available languages by looping through each installed language based on the language folders contained in bin
            // append them to a comms-separated string for use in the Global JS
            string availableLanguages = "en-US"; // en-US is the default application language, so it's already installed            

            foreach (string directory in Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN)))
            {
                // loop through each language available to the site and add it to the array list for drop-down items
                foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
                {
                    if (language == Path.GetFileName(directory))
                    {
                        availableLanguages += "," + language;
                        break;
                    }
                }
            }

            globalJS.AppendLine("var AvailableLanguages = \"" + availableLanguages + "\";");
            globalJS.AppendLine("");

            // DICTIONARY OF TERMS

            globalJS.Append("var EditorTermsDictionary = ##quot##{");

            // en-US
            globalJS.Append("	\"en-US\":{");
            
            //Previously existing terms
            globalJS.Append("	\"and\":\"" + _GlobalResources.AND_UPPER + "\",");
            globalJS.Append("	\"all\":\"" + _GlobalResources.All + "\",");
            globalJS.Append("	\"are you sure you want to delete this course\":\"" + _GlobalResources.AreYouSureYouWantToDeleteThisCourse + "\",");
            globalJS.Append("	\"at least one course must be selected\":\"" + _GlobalResources.AtLeastOneCourseMustBeSelected + "\",");
            globalJS.Append("	\"cancel\":\"" + _GlobalResources.Cancel + "\",");
            globalJS.Append("	\"clear\":\"" + _GlobalResources.Clear + "\",");
            globalJS.Append("	\"company\":\"" + _GlobalResources.Company + "\",");
            globalJS.Append("	\"continue\":\"" + _GlobalResources.Continue + "\",");
            globalJS.Append("	\"description\":\"" + _GlobalResources.Description + "\",");
            globalJS.Append("	\"day_s\":\"" + _GlobalResources.Day_s + "\",");
            globalJS.Append("	\"done\":\"" + _GlobalResources.Done + "\",");
            globalJS.Append("	\"email\":\"" + _GlobalResources.Email + "\",");
            globalJS.Append("	\"label\":\"" + _GlobalResources.Label + "\",");
            globalJS.Append("	\"month_s\":\"" + _GlobalResources.Month_s + "\",");
            globalJS.Append("	\"name\":\"" + _GlobalResources.Name + "\",");
            globalJS.Append("	\"no\":\"" + _GlobalResources.No + "\",");
            globalJS.Append("	\"no courses found\":\"" + _GlobalResources.NoCoursesFound + "\",");
            globalJS.Append("	\"none\":\"" + _GlobalResources.None + "\",");
            globalJS.Append("	\"ok\":\"" + _GlobalResources.OK + "\",");
            globalJS.Append("	\"or\":\"" + _GlobalResources.OR_UPPER + "\",");
            globalJS.Append("	\"other\":\"" + _GlobalResources.Other + "\",");
            globalJS.Append("	\"requirements\":\"" + _GlobalResources.Requirements + "\",");
            globalJS.Append("	\"search\":\"" + _GlobalResources.Search + "\",");
            globalJS.Append("	\"search tags\":\"" + _GlobalResources.SearchTags + "\",");
            globalJS.Append("	\"select\":\"" + _GlobalResources.Select + "\",");
            globalJS.Append("	\"task\":\"" + _GlobalResources.Task + "\",");
            globalJS.Append("	\"title\":\"" + _GlobalResources.Title + "\",");
            globalJS.Append("	\"user\":\"" + _GlobalResources.User + "\",");
            globalJS.Append("	\"week_s\":\"" + _GlobalResources.Week_s + "\",");
            globalJS.Append("	\"year_s\":\"" + _GlobalResources.Year_s + "\",");
            globalJS.Append("	\"yes\":\"" + _GlobalResources.Yes + "\",");

            //Certification Specific
            globalJS.Append("	\"add a segment\":\"" + _GlobalResources.AddASegment + "\",");
            globalJS.Append("	\"add new requirement\":\"" + _GlobalResources.AddNewRequirement + "\",");
            globalJS.Append("	\"administrators\":\"" + _GlobalResources.Administrators + "\",");
            globalJS.Append("	\"all selected course(s)\":\"" + _GlobalResources.AllSelectedCourse_s + "\",");
            globalJS.Append("	\"any selected course\":\"" + _GlobalResources.AnySelectedCourse + "\",");
            globalJS.Append("	\"any course\":\"" + _GlobalResources.AnyCourse + "\",");
            globalJS.Append("	\"are you sure you want to delete this requirement\":\"" + _GlobalResources.AreYouSureYouWantToDeleteThisRequirement + "\",");
            globalJS.Append("	\"are you sure you want to delete this segment\":\"" + _GlobalResources.AreYouSureYouWantToDeleteThisSegment + "\",");
            globalJS.Append("	\"are you sure you want to delete this unit\":\"" + _GlobalResources.AreYouSureYouWantToDeleteThisUnit + "\",");
            globalJS.Append("	\"at least one segment is required\":\"" + _GlobalResources.AtLeastOneSegmentIsRequired + "\",");
            globalJS.Append("	\"attach course(s)\":\"" + _GlobalResources.AttachCourse_s + "\",");
            globalJS.Append("	\"attached courses\":\"" + _GlobalResources.AttachedCourses + "\",");
            globalJS.Append("	\"accrediting organization\":\"" + _GlobalResources.AccreditingOrganization + "\",");
            globalJS.Append("	\"certification cannot be saved until all errors are corrected\":\"" + _GlobalResources.CertificationCannotBeSavedUntilAllErrorsAreCorrected + "\",");
            globalJS.Append("	\"change credits\":\"" + _GlobalResources.ChangeCredits + "\",");
            globalJS.Append("	\"changing this courses credit value will affect one or more requirements\":\"" + _GlobalResources.ChangingThisCoursesCreditValueWillAffectOneOrMoreRequirements + "\",");
            globalJS.Append("	\"complete all of these course(s)\":\"" + _GlobalResources.CompleteAllOfTheseCourse_s + "\",");
            globalJS.Append("	\"complete any of these course(s)\":\"" + _GlobalResources.CompleteAnyOfTheseCourse_s + "\",");
            globalJS.Append("	\"contact\":\"" + _GlobalResources.Contact + "\",");
            globalJS.Append("	\"course expert\":\"" + _GlobalResources.CourseExpert + "\",");
            globalJS.Append("	\"credits required\":\"" + _GlobalResources.CreditsRequired + "\",");
            globalJS.Append("	\"delete requirement\":\"" + _GlobalResources.DeleteRequirement + "\",");
            globalJS.Append("	\"delete course\":\"" + _GlobalResources.DeleteCourse + "\",");
            globalJS.Append("	\"delete segment\":\"" + _GlobalResources.DeleteSegment + "\",");
            globalJS.Append("	\"delete unit\":\"" + _GlobalResources.DeleteUnit + "\",");
            globalJS.Append("	\"deleting this course will affect one or more requirements\":\"" + _GlobalResources.DeletingThisCourseWillAffectOneOrMoreRequirements + "\",");
            globalJS.Append("	\"describe the external task\":\"" + _GlobalResources.DescribeTheExternalTask + "\",");
            globalJS.Append("	\"description is required in the default language\":\"" + _GlobalResources.DescriptionIsRequiredInTheDefaultLanguage + "\",");
            globalJS.Append("	\"documentation is required\":\"" + _GlobalResources.DocumentationIsRequired + "\",");
            globalJS.Append("	\"earn x credits from any course\":\"" + _GlobalResources.EarnXCreditsFromAnyCourse + "\",");
            globalJS.Append("	\"earn x credits from these courses\":\"" + _GlobalResources.EarnXCreditsFromTheseCourses + "\",");
            globalJS.Append("	\"from course(s)\":\"" + _GlobalResources.FromCourse_s + "\",");
            globalJS.Append("	\"initial\":\"" + _GlobalResources.Initial + "\",");
            globalJS.Append("	\"initial award expiration\":\"" + _GlobalResources.InitialAwardExpiration + "\",");
            globalJS.Append("	\"initial requirements\":\"" + _GlobalResources.InitialRequirements + "\",");
            globalJS.Append("	\"label is required in the default language\":\"" + _GlobalResources.LabelIsRequiredInTheDefaultLanguage + "\",");
            globalJS.Append("	\"learner must complete\":\"" + _GlobalResources.LearnerMustComplete + "\",");
            globalJS.Append("	\"learners supervisor\":\"" + _GlobalResources.LearnersSupervisor + "\",");
            globalJS.Append("	\"learners supervisors\":\"" + _GlobalResources.LearnersSupervisor_s + "\",");
            globalJS.Append("	\"new requirement\":\"" + _GlobalResources.NewRequirement + "\",");
            globalJS.Append("	\"new segment\":\"" + _GlobalResources.NewSegment + "\",");
            globalJS.Append("	\"new task\":\"" + _GlobalResources.NewTask + "\",");
            globalJS.Append("	\"new unit\":\"" + _GlobalResources.NewUnit + "\",");
            globalJS.Append("	\"phone\":\"" + _GlobalResources.Phone + "\",");
            globalJS.Append("	\"please enter a valid number of required credits\":\"" + _GlobalResources.PleaseEnterAValidNumberOfRequiredCredits + "\",");
            globalJS.Append("	\"refresh\":\"" + _GlobalResources.Refresh + "\",");
            globalJS.Append("	\"renewal\":\"" + _GlobalResources.Renewal + "\",");
            globalJS.Append("	\"renewal expiration\":\"" + _GlobalResources.RenewalExpiration + "\",");
            globalJS.Append("	\"renewal requirements\":\"" + _GlobalResources.RenewalRequirements + "\",");
            globalJS.Append("	\"require documentation is required\":\"" + _GlobalResources.RequireDocumentationIsRequired + "\",");
            globalJS.Append("	\"require documentation\":\"" + _GlobalResources.RequireDocumentation + "\",");
            globalJS.Append("	\"requirement complete course(s)\":\"" + _GlobalResources.RequirementCompleteCourse_s + "\",");
            globalJS.Append("	\"requirement complete task\":\"" + _GlobalResources.RequirementCompleteTask + "\",");
            globalJS.Append("	\"requirement earn credit(s)\":\"" + _GlobalResources.RequirementEarnCredit_s + "\",");
            globalJS.Append("	\"save certification\":\"" + _GlobalResources.SaveCertification + "\",");
            globalJS.Append("	\"segment(s)\":\"" + _GlobalResources.Segment_s + "\",");
            globalJS.Append("	\"segment must contain at least one requirement\":\"" + _GlobalResources.SegmentMustContainAtLeastOneRequirement + "\",");
            globalJS.Append("	\"select the type of requirement\":\"" + _GlobalResources.SelectTheTypeOfRequirement + "\",");
            globalJS.Append("	\"signoff\":\"" + _GlobalResources.Signoff + "\",");
            globalJS.Append("	\"signoff is required by administrators\":\"" + _GlobalResources.SignoffIsRequiredByAdministrators + "\",");
            globalJS.Append("	\"signoff is required by administrators or learners supervisors\":\"" + _GlobalResources.SignoffIsRequiredByAdministratorsOrLearnersSupervisor_s + "\",");
            globalJS.Append("	\"signoff is required by any of the following\":\"" + _GlobalResources.SignoffIsRequiredByAnyOfTheFollowing + "\",");
            globalJS.Append("	\"signoff is required by either\":\"" + _GlobalResources.SignoffIsRequiredByEither + "\",");
            globalJS.Append("	\"signoff is required by expert\":\"" + _GlobalResources.SignoffIsRequiredByExpert + "\",");
            globalJS.Append("	\"signoff is required by supervisor\":\"" + _GlobalResources.SignoffIsRequiredBySupervisor + "\",");
            globalJS.Append("	\"signoff is required by supervisor or expert\":\"" + _GlobalResources.SignoffIsRequiredBySupervisorOrExpert + "\",");
            globalJS.Append("	\"signoff is required please select one or both options\":\"" + _GlobalResources.SignoffIsRequiredPleaseSelectOneOrBothOptions + "\",");
            globalJS.Append("	\"the learner must complete an external task\":\"" + _GlobalResources.TheLearnerMustCompleteAnExternalTask + "\",");
            globalJS.Append("	\"the learner must complete specific course(s)\":\"" + _GlobalResources.TheLearnerMustCompleteSpecificCourse_s + "\",");
            globalJS.Append("	\"the learner must earn enough course credits\":\"" + _GlobalResources.TheLearnerMustEarnEnoughCourseCredits + "\",");
            globalJS.Append("	\"unit\":\"" + _GlobalResources.Unit + "\",");
            globalJS.Append("	\"unit(s)\":\"" + _GlobalResources.Unit_s + "\",");
            globalJS.Append("	\"unit title\":\"" + _GlobalResources.UnitTitle + "\",");
            globalJS.Append("	\"use initial requirements\":\"" + _GlobalResources.UseInitialRequirements + "\",");
            globalJS.Append("	\"value exceeds total selected course credits\":\"" + _GlobalResources.ValueExceedsTotalSelectedCourseCredits + "\",");
            globalJS.Append("	\"yes learner must upload documentation verifying completion of task\":\"" + _GlobalResources.YesLearnerMustUploadDocumentationVerifyingCompletionOfTask + "\",");
            globalJS.Append("	\"x credits\":\"" + _GlobalResources.XCredits + "\",");

            //Certification Specific: New terms for Error-Lister Interface
            globalJS.Append("	\"click each error below to auto navigate to its location\":\"" + _GlobalResources.ClickEachErrorBelowToAutoNavigateToItsLocation + "\",");
            globalJS.Append("	\"error occurred retrieving course listing. please try again or contact administrator\":\"" + _GlobalResources.ErrorOccurredRetrievingCourseListingPleaseTryAgainOrContactAdministrator + "\",");
            globalJS.Append("	\"general error\":\"" + _GlobalResources.GeneralError + "\",");
            globalJS.Append("	\"segment x on unit x on the initial requirements tab must have at least one requirement\":\"" + _GlobalResources.SegmentXOnUnitXOnTheInitialRequirementsTabMustHaveAtLeastOneRequirement + "\",");
            globalJS.Append("	\"segment x on unit x on the initial requirements tab requires a label in the default language\":\"" + _GlobalResources.SegmentXOnUnitXOnTheInitialRequirementsTabRequiresALabelInTheDefaultLanguage + "\",");
            globalJS.Append("	\"segment x on unit x on the renewal requirements tab must have at least one requirement\":\"" + _GlobalResources.SegmentXOnUnitXOnTheRenewalRequirementsTabMustHaveAtLeastOneRequirement + "\",");
            globalJS.Append("	\"segment x on unit x on the renewal requirements tab requires a label in the default language\":\"" + _GlobalResources.SegmentXOnUnitXOnTheRenewalRequirementsTabRequiresALabelInTheDefaultLanguage + "\",");
            globalJS.Append("	\"the course titled x no longer exists in the system\":\"" + _GlobalResources.TheCourseTitledXNoLongerExistsInTheSystem + "\",");
            globalJS.Append("	\"there are no units on the initial tab\":\"" + _GlobalResources.ThereAreNoUnitsOnTheInitialTab + "\",");
            globalJS.Append("	\"there are no units on the renewal tab\":\"" + _GlobalResources.ThereAreNoUnitsOnTheRenewalTab + "\",");
            globalJS.Append("	\"this course no longer exists in the system\":\"" + _GlobalResources.ThisCourseNoLongerExistsInTheSystem + "\",");
            globalJS.Append("	\"the properties tab requires a description in the default language\":\"" + _GlobalResources.ThePropertiesTabRequiresADescriptionInTheDefaultLanguage + "\",");
            globalJS.Append("	\"the properties tab requires a title in the default language\":\"" + _GlobalResources.ThePropertiesTabRequiresATitleInTheDefaultLanguage + "\",");
            globalJS.Append("	\"unit x on the initial requirements tab must have at least one segment\":\"" + _GlobalResources.UnitXOnTheInitialRequirementsTabMustHaveAtLeastOneSegment + "\",");
            globalJS.Append("	\"unit x on the initial requirements tab requires a description in the default language\":\"" + _GlobalResources.UnitXOnTheInitialRequirementsTabRequiresADescriptionInTheDefaultLanguage + "\",");
            globalJS.Append("	\"unit x on the initial requirements tab requires a title in the default language\":\"" + _GlobalResources.UnitXOnTheInitialRequirementsTabRequiresATitleInTheDefaultLanguage + "\",");
            globalJS.Append("	\"unit x on the renewal requirements tab must have at least one segment\":\"" + _GlobalResources.UnitXOnTheRenewalRequirementsTabMustHaveAtLeastOneSegment + "\",");
            globalJS.Append("	\"unit x on the renewal requirements tab requires a description in the default language\":\"" + _GlobalResources.UnitXOnTheRenewalRequirementsTabRequiresADescriptionInTheDefaultLanguage + "\",");
            globalJS.Append("	\"unit x on the renewal requirements tab requires a title in the default language\":\"" + _GlobalResources.UnitXOnTheRenewalRequirementsTabRequiresATitleInTheDefaultLanguage + "\"");

            globalJS.Append("	}");

            // rest of the languages
            foreach (string directory in Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN)))
            {
                // loop through each language available to the site and add it to the array list for drop-down items
                foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
                {
                    if (language == Path.GetFileName(directory))
                    {
                        globalJS.Append(",	\"" + language + "\":{");                        

                        //Previously existing terms
                        globalJS.Append("	\"and\":\"" + _GlobalResources.ResourceManager.GetString("AND_UPPER", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"all\":\"" + _GlobalResources.ResourceManager.GetString("All", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"are you sure you want to delete this course\":\"" + _GlobalResources.ResourceManager.GetString("AreYouSureYouWantToDeleteThisCourse", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"at least one course must be selected\":\"" + _GlobalResources.ResourceManager.GetString("AtLeastOneCourseMustBeSelected", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"cancel\":\"" + _GlobalResources.ResourceManager.GetString("Cancel", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"clear\":\"" + _GlobalResources.ResourceManager.GetString("Clear", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"company\":\"" + _GlobalResources.ResourceManager.GetString("Company", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"continue\":\"" + _GlobalResources.ResourceManager.GetString("Continue", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"description\":\"" + _GlobalResources.ResourceManager.GetString("Description", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"day_s\":\"" + _GlobalResources.ResourceManager.GetString("Day_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"done\":\"" + _GlobalResources.ResourceManager.GetString("Done", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"email\":\"" + _GlobalResources.ResourceManager.GetString("Email", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"label\":\"" + _GlobalResources.ResourceManager.GetString("Label", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"name\":\"" + _GlobalResources.ResourceManager.GetString("Name", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"no\":\"" + _GlobalResources.ResourceManager.GetString("No", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"no courses found\":\"" + _GlobalResources.ResourceManager.GetString("NoCoursesFound", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"month_s\":\"" + _GlobalResources.ResourceManager.GetString("Month_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"none\":\"" + _GlobalResources.ResourceManager.GetString("None", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"ok\":\"" + _GlobalResources.ResourceManager.GetString("OK", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"or\":\"" + _GlobalResources.ResourceManager.GetString("OR_UPPER", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"other\":\"" + _GlobalResources.ResourceManager.GetString("Other", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"requirements\":\"" + _GlobalResources.ResourceManager.GetString("Requirements", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"search\":\"" + _GlobalResources.ResourceManager.GetString("Search", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"search tags\":\"" + _GlobalResources.ResourceManager.GetString("SearchTags", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"select\":\"" + _GlobalResources.ResourceManager.GetString("Select", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"task\":\"" + _GlobalResources.ResourceManager.GetString("Task", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"title\":\"" + _GlobalResources.ResourceManager.GetString("Title", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"user\":\"" + _GlobalResources.ResourceManager.GetString("User", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"week_s\":\"" + _GlobalResources.ResourceManager.GetString("Week_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"year_s\":\"" + _GlobalResources.ResourceManager.GetString("Year_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"yes\":\"" + _GlobalResources.ResourceManager.GetString("Yes", new CultureInfo(language)) + "\",");

                        //Certification Specific
                        globalJS.Append("	\"add a segment\":\"" + _GlobalResources.ResourceManager.GetString("AddASegment", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"add new requirement\":\"" + _GlobalResources.ResourceManager.GetString("AddNewRequirement", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"administrators\":\"" + _GlobalResources.ResourceManager.GetString("Administrators", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"all selected course(s)\":\"" + _GlobalResources.ResourceManager.GetString("AllSelectedCourse_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"any selected course\":\"" + _GlobalResources.ResourceManager.GetString("AnySelectedCourse", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"any course\":\"" + _GlobalResources.ResourceManager.GetString("AnyCourse", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"are you sure you want to delete this requirement\":\"" + _GlobalResources.ResourceManager.GetString("AreYouSureYouWantToDeleteThisRequirement", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"are you sure you want to delete this segment\":\"" + _GlobalResources.ResourceManager.GetString("AreYouSureYouWantToDeleteThisSegment", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"are you sure you want to delete this unit\":\"" + _GlobalResources.ResourceManager.GetString("AreYouSureYouWantToDeleteThisUnit", new CultureInfo(language)) + "\",");                        
                        globalJS.Append("	\"at least one segment is required\":\"" + _GlobalResources.ResourceManager.GetString("AtLeastOneSegmentIsRequired", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"attach course(s)\":\"" + _GlobalResources.ResourceManager.GetString("AttachCourse_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"attached courses\":\"" + _GlobalResources.ResourceManager.GetString("AttachedCourses", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"accrediting organization\":\"" + _GlobalResources.ResourceManager.GetString("AccreditingOrganization", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"certification cannot be saved until all errors are corrected\":\"" + _GlobalResources.ResourceManager.GetString("CertificationCannotBeSavedUntilAllErrorsAreCorrected", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"change credits\":\"" + _GlobalResources.ResourceManager.GetString("ChangeCredits", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"changing this courses credit value will affect one or more requirements\":\"" + _GlobalResources.ResourceManager.GetString("ChangingThisCoursesCreditValueWillAffectOneOrMoreRequirements", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"complete all of these course(s)\":\"" + _GlobalResources.ResourceManager.GetString("CompleteAllOfTheseCourse_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"complete any of these course(s)\":\"" + _GlobalResources.ResourceManager.GetString("CompleteAnyOfTheseCourse_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"contact\":\"" + _GlobalResources.ResourceManager.GetString("Contact", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"course expert\":\"" + _GlobalResources.ResourceManager.GetString("CourseExpert", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"credits required\":\"" + _GlobalResources.ResourceManager.GetString("CreditsRequired", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"delete requirement\":\"" + _GlobalResources.ResourceManager.GetString("DeleteRequirement", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"delete course\":\"" + _GlobalResources.ResourceManager.GetString("DeleteCourse", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"delete segment\":\"" + _GlobalResources.ResourceManager.GetString("DeleteSegment", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"delete unit\":\"" + _GlobalResources.ResourceManager.GetString("DeleteUnit", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"deleting this course will affect one or more requirements\":\"" + _GlobalResources.ResourceManager.GetString("DeletingThisCourseWillAffectOneOrMoreRequirements", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"describe the external task\":\"" + _GlobalResources.ResourceManager.GetString("DescribeTheExternalTask", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"description is required in the default language\":\"" + _GlobalResources.ResourceManager.GetString("DescriptionIsRequiredInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"documentation is required\":\"" + _GlobalResources.ResourceManager.GetString("DocumentationIsRequired", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"earn x credits from any course\":\"" + _GlobalResources.ResourceManager.GetString("EarnXCreditsFromAnyCourse", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"earn x credits from these courses\":\"" + _GlobalResources.ResourceManager.GetString("EarnXCreditsFromTheseCourses", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"from course(s)\":\"" + _GlobalResources.ResourceManager.GetString("FromCourse_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"initial\":\"" + _GlobalResources.ResourceManager.GetString("Initial", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"initial award expiration\":\"" + _GlobalResources.ResourceManager.GetString("InitialAwardExpiration", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"initial requirements\":\"" + _GlobalResources.ResourceManager.GetString("InitialRequirements", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"label is required in the default language\":\"" + _GlobalResources.ResourceManager.GetString("LabelIsRequiredInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"learner must complete\":\"" + _GlobalResources.ResourceManager.GetString("LearnerMustComplete", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"learners supervisor\":\"" + _GlobalResources.ResourceManager.GetString("LearnersSupervisor", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"learners supervisors\":\"" + _GlobalResources.ResourceManager.GetString("LearnersSupervisor_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"new requirement\":\"" + _GlobalResources.ResourceManager.GetString("NewRequirement", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"new segment\":\"" + _GlobalResources.ResourceManager.GetString("NewSegment", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"new task\":\"" + _GlobalResources.ResourceManager.GetString("NewTask", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"new unit\":\"" + _GlobalResources.ResourceManager.GetString("NewUnit", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"phone\":\"" + _GlobalResources.ResourceManager.GetString("Phone", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"please enter a valid number of required credits\":\"" + _GlobalResources.ResourceManager.GetString("PleaseEnterAValidNumberOfRequiredCredits", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"refresh\":\"" + _GlobalResources.ResourceManager.GetString("Refresh", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"renewal\":\"" + _GlobalResources.ResourceManager.GetString("Renewal", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"renewal expiration\":\"" + _GlobalResources.ResourceManager.GetString("RenewalExpiration", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"renewal requirements\":\"" + _GlobalResources.ResourceManager.GetString("RenewalRequirements", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"require documentation is required\":\"" + _GlobalResources.ResourceManager.GetString("RequireDocumentationIsRequired", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"require documentation\":\"" + _GlobalResources.ResourceManager.GetString("RequireDocumentation", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"requirement complete course(s)\":\"" + _GlobalResources.ResourceManager.GetString("RequirementCompleteCourse_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"requirement complete task\":\"" + _GlobalResources.ResourceManager.GetString("RequirementCompleteTask", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"requirement earn credit(s)\":\"" + _GlobalResources.ResourceManager.GetString("RequirementEarnCredit_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"save certification\":\"" + _GlobalResources.ResourceManager.GetString("SaveCertification", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"segment(s)\":\"" + _GlobalResources.ResourceManager.GetString("Segment_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"segment must contain at least one requirement\":\"" + _GlobalResources.ResourceManager.GetString("SegmentMustContainAtLeastOneRequirement", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"select the type of requirement\":\"" + _GlobalResources.ResourceManager.GetString("SelectTheTypeOfRequirement", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"signoff\":\"" + _GlobalResources.ResourceManager.GetString("Signoff", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"signoff is required by administrators\":\"" + _GlobalResources.ResourceManager.GetString("SignoffIsRequiredByAdministrators", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"signoff is required by administrators or learners supervisors\":\"" + _GlobalResources.ResourceManager.GetString("SignoffIsRequiredByAdministratorsOrLearnersSupervisor_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"signoff is required by any of the following\":\"" + _GlobalResources.ResourceManager.GetString("SignoffIsRequiredByAnyOfTheFollowing", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"signoff is required by either\":\"" + _GlobalResources.ResourceManager.GetString("SignoffIsRequiredByEither", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"signoff is required by expert\":\"" + _GlobalResources.ResourceManager.GetString("SignoffIsRequiredByExpert", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"signoff is required by supervisor\":\"" + _GlobalResources.ResourceManager.GetString("SignoffIsRequiredBySupervisor", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"signoff is required by supervisor or expert\":\"" + _GlobalResources.ResourceManager.GetString("SignoffIsRequiredBySupervisorOrExpert", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"signoff is required please select one or both options\":\"" + _GlobalResources.ResourceManager.GetString("SignoffIsRequiredPleaseSelectOneOrBothOptions", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"the learner must complete an external task\":\"" + _GlobalResources.ResourceManager.GetString("TheLearnerMustCompleteAnExternalTask", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"the learner must complete specific course(s)\":\"" + _GlobalResources.ResourceManager.GetString("TheLearnerMustCompleteSpecificCourse_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"the learner must earn enough course credits\":\"" + _GlobalResources.ResourceManager.GetString("TheLearnerMustEarnEnoughCourseCredits", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"unit\":\"" + _GlobalResources.ResourceManager.GetString("Unit", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"unit(s)\":\"" + _GlobalResources.ResourceManager.GetString("Unit_s", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"unit title\":\"" + _GlobalResources.ResourceManager.GetString("UnitTitle", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"use initial requirements\":\"" + _GlobalResources.ResourceManager.GetString("UseInitialRequirements", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"value exceeds total selected course credits\":\"" + _GlobalResources.ResourceManager.GetString("ValueExceedsTotalSelectedCourseCredits", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"yes learner must upload documentation verifying completion of task\":\"" + _GlobalResources.ResourceManager.GetString("YesLearnerMustUploadDocumentationVerifyingCompletionOfTask", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"x credits\":\"" + _GlobalResources.ResourceManager.GetString("XCredits", new CultureInfo(language)) + "\",");

                        //Certification Specific: New terms for Error-Lister Interface
                        globalJS.Append("	\"click each error below to auto navigate to its location\":\"" + _GlobalResources.ResourceManager.GetString("ClickEachErrorBelowToAutoNavigateToItsLocation", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"error occurred retrieving course listing. please try again or contact administrator\":\"" + _GlobalResources.ResourceManager.GetString("ErrorOccurredRetrievingCourseListingPleaseTryAgainOrContactAdministrator", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"general error\":\"" + _GlobalResources.ResourceManager.GetString("GeneralError", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"segment x on unit x on the initial requirements tab must have at least one requirement\":\"" + _GlobalResources.ResourceManager.GetString("SegmentXOnUnitXOnTheInitialRequirementsTabMustHaveAtLeastOneRequirement", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"segment x on unit x on the initial requirements tab requires a label in the default language\":\"" + _GlobalResources.ResourceManager.GetString("SegmentXOnUnitXOnTheInitialRequirementsTabRequiresALabelInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"segment x on unit x on the renewal requirements tab must have at least one requirement\":\"" + _GlobalResources.ResourceManager.GetString("SegmentXOnUnitXOnTheRenewalRequirementsTabMustHaveAtLeastOneRequirement", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"segment x on unit x on the renewal requirements tab requires a label in the default language\":\"" + _GlobalResources.ResourceManager.GetString("SegmentXOnUnitXOnTheRenewalRequirementsTabRequiresALabelInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"the course titled x no longer exists in the system\":\"" + _GlobalResources.ResourceManager.GetString("TheCourseTitledXNoLongerExistsInTheSystem", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"there are no units on the initial tab\":\"" + _GlobalResources.ResourceManager.GetString("ThereAreNoUnitsOnTheInitialTab", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"there are no units on the renewal tab\":\"" + _GlobalResources.ResourceManager.GetString("ThereAreNoUnitsOnTheRenewalTab", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"this course no longer exists in the system\":\"" + _GlobalResources.ResourceManager.GetString("ThisCourseNoLongerExistsInTheSystem", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"the properties tab requires a description in the default language\":\"" + _GlobalResources.ResourceManager.GetString("ThePropertiesTabRequiresADescriptionInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"the properties tab requires a title in the default language\":\"" + _GlobalResources.ResourceManager.GetString("ThePropertiesTabRequiresATitleInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"unit x on the initial requirements tab must have at least one segment\":\"" + _GlobalResources.ResourceManager.GetString("UnitXOnTheInitialRequirementsTabMustHaveAtLeastOneSegment", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"unit x on the initial requirements tab requires a description in the default language\":\"" + _GlobalResources.ResourceManager.GetString("UnitXOnTheInitialRequirementsTabRequiresADescriptionInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"unit x on the initial requirements tab requires a title in the default language\":\"" + _GlobalResources.ResourceManager.GetString("UnitXOnTheInitialRequirementsTabRequiresATitleInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"unit x on the renewal requirements tab must have at least one segment\":\"" + _GlobalResources.ResourceManager.GetString("UnitXOnTheRenewalRequirementsTabMustHaveAtLeastOneSegment", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"unit x on the renewal requirements tab requires a description in the default language\":\"" + _GlobalResources.ResourceManager.GetString("UnitXOnTheRenewalRequirementsTabRequiresADescriptionInTheDefaultLanguage", new CultureInfo(language)) + "\",");
                        globalJS.Append("	\"unit x on the renewal requirements tab requires a title in the default language\":\"" + _GlobalResources.ResourceManager.GetString("UnitXOnTheRenewalRequirementsTabRequiresATitleInTheDefaultLanguage", new CultureInfo(language)) + "\"");

                        globalJS.Append("	}");
                        break;
                    }
                }
            }

            globalJS.Append("}##quot##;");
            globalJS.AppendLine("");

            // ESCAPE ' CHARACTERS FOR DICTIONARY OBJECT
            globalJS.Replace("'", "\\'");
            globalJS.Replace("##quot##", "'");

            // ADD IMAGE PATHS
            globalJS.AppendLine("var CertificationsAddImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG) + "\";");
            globalJS.AppendLine("var CertificationsDeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            globalJS.AppendLine("var CertificationsYellowWarningImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG) + "\";");
            globalJS.AppendLine("var CertificationsCourseImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG) + "\";");            

            // ADD CURRENT INTERFACE LANGUAGE
            globalJS.AppendLine("var CurrentInterfaceLanguage = \"" + AsentiaSessionState.UserCulture + "\";");            
                        
            csm.RegisterClientScriptBlock(typeof(Modify), "GlobalJS", globalJS.ToString(), true);

            // BUILD STARTUP CALLS FOR CERTIFICATIONS INTERFACE           
            StringBuilder multipleStartUpCallsScript = new StringBuilder();            
            
            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");                        
            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine(" CertificationEditorObject = new CE_CertificationInterface(");
            multipleStartUpCallsScript.AppendLine("     \"CertificationIdentifier\",");                                         // IDENTIFIER
            multipleStartUpCallsScript.AppendLine("     document.getElementById(\"certification-editor\"),");                   // NODE TO ATTACH TO
            multipleStartUpCallsScript.AppendLine("     AvailableLanguages,");                                                  // AVAILABLE LANGUAGES
            multipleStartUpCallsScript.AppendLine("     CurrentInterfaceLanguage,");                                            // THE CURRENT INTERFACE LANGUAGE
            multipleStartUpCallsScript.AppendLine("     \"/_images/flags/\",");                                                 // FLAG IMAGES FOLDER
            multipleStartUpCallsScript.AppendLine("     CertificationsAddImagePath,");                                          // ADD IMAGE
            multipleStartUpCallsScript.AppendLine("     CertificationsDeleteImagePath,");                                       // DELETE IMAGE
            multipleStartUpCallsScript.AppendLine("     CertificationsYellowWarningImagePath,");                                // YELLOW WARNING IMAGE
            multipleStartUpCallsScript.AppendLine("     document.getElementById(\"CertificationJSONField\").value,");           // DATA GRAB            
            multipleStartUpCallsScript.AppendLine("     \"\",");                                                                // ACTIVE PANEL - ID OF THE PANEL TO BE DISPLAYED
            multipleStartUpCallsScript.AppendLine("     \"asentia-js-modal\",");                                                // DOM ITEMS TO USE FOR MODALS
            multipleStartUpCallsScript.AppendLine("     EditorTermsDictionary");                                                // DICTIONARY TO USE FOR TRANSLATIONS
            multipleStartUpCallsScript.AppendLine("     );");
            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);
        }
        #endregion

        #region _GetCertificationObject
        /// <summary>
        /// Gets a certification object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCertificationObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._CertificationObject = new Certification(id); }
                }
                catch
                { Response.Redirect("~/administrator/certifications"); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {            
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.CertificationPropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CertificationPropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the object menu if we have a certification object
            this._BuildCertificationObjectMenu();

            // certification JSON Data HiddenField          
            this._CertificationJSONData = new HiddenField();
            this._CertificationJSONData.ID = "CertificationJSONField";
            this.CertificationFormContainer.Controls.Add(this._CertificationJSONData);

            // populate the hidden field with an empty representation of the data model
            CertificationDataModel certDM = new CertificationDataModel();
            this._CertificationJSONData.Value = JsonHelper.JsonSerializer<CertificationDataModel>(certDM);

            // build the certification properties form
            this._BuildCertificationPropertiesForm();            

            // build the certification properties form actions panel
            this._BuildCertificationPropertiesActionsPanel();

            // populate the certification form - only executes if this is an existing certification
            this._PopulateCertificationDataModelForJSONObject();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string certificationImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
            string imageCssClass = null;
            string pageTitle;

            if (this._CertificationObject != null)
            {
                string certificationTitleInInterfaceLanguage = this._CertificationObject.Title;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (Certification.LanguageSpecificProperty certificationLanguageSpecificProperty in this._CertificationObject.LanguageSpecificProperties)
                    {
                        if (certificationLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { certificationTitleInInterfaceLanguage = certificationLanguageSpecificProperty.Title; }
                    }
                }

                breadCrumbPageTitle = certificationTitleInInterfaceLanguage;
                pageTitle = certificationTitleInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewCertification;
                pageTitle = _GlobalResources.NewCertification;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Certifications, "/administrator/certifications"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, pageTitle, certificationImagePath, imageCssClass);
        }
        #endregion

        #region _BuildCertificationObjectMenu
        /// <summary>
        /// Builds the certification object menu if there is a Certification object.
        /// </summary>
        private void _BuildCertificationObjectMenu()
        {
            this.CertificationObjectMenuContainer.Controls.Clear();

            if (this._CertificationObject != null)
            {                
                CertificationObjectMenu certificationObjectMenu = new CertificationObjectMenu(this._CertificationObject);
                certificationObjectMenu.SelectedItem = CertificationObjectMenu.MenuObjectItem.CertificationProperties;

                this.CertificationObjectMenuContainer.Controls.Add(certificationObjectMenu);  
            }            
        }
        #endregion

        #region _BuildCertificationPropertiesForm
        /// <summary>
        /// Builds the certification properties form.
        /// </summary>
        private void _BuildCertificationPropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.CertificationPropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisCertification, true);

            // clear controls from container
            this.CertificationPropertiesContainer.Controls.Clear();

            // set up the certification container elements
            this.CertificationFormContainer.ID = "certification-editor";
            this.CertificationFormContainer.CssClass = "CertificationEditor";
            this.CertificationPropertiesContainer.Controls.Add(this.CertificationFormContainer);

            this.CertificationAsentiaModalContainer.ID = "asentia-modal-container";
            this.CertificationFormContainer.Controls.Add(this.CertificationAsentiaModalContainer);

            this.CertificationAsentiaJsModal.ID = "asentia-js-modal";
            this.CertificationAsentiaModalContainer.Controls.Add(this.CertificationAsentiaJsModal);

            this.CertificationAsentiaModalAlertContainer.ID = "asentia-modal-alert-container";
            this.CertificationFormContainer.Controls.Add(this.CertificationAsentiaModalAlertContainer);

            this.CertificationAsentiaJsModalAlert.ID = "asentia-js-modal-alert";
            this.CertificationAsentiaModalAlertContainer.Controls.Add(this.CertificationAsentiaJsModalAlert);
        }
        #endregion

        #region _BuildCertificationPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for certification properties actions.
        /// </summary>
        private void _BuildCertificationPropertiesActionsPanel()
        {
            // clear controls from container
            this.CertificationPropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.CertificationPropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._CertificationPropertiesSaveButton = new Button();
            this._CertificationPropertiesSaveButton.ID = "CertificationSaveButton";
            this._CertificationPropertiesSaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._CertificationObject == null)
            { this._CertificationPropertiesSaveButton.Text = _GlobalResources.CreateCertification; }
            else
            { this._CertificationPropertiesSaveButton.Text = _GlobalResources.SaveChanges; }
            
            this._CertificationPropertiesSaveButton.Command += new CommandEventHandler(this._CertificationPropertiesSaveButton_Command);
            this._CertificationPropertiesSaveButton.Attributes.Add("onclick", "CertificationEditorObject.ValidateAndDoSave(); return false;");
            this.CertificationPropertiesActionsPanel.Controls.Add(this._CertificationPropertiesSaveButton);

            // set the default button for the certification properties container to the id of the properties save button
            this.CertificationPropertiesContainer.DefaultButton = this._CertificationPropertiesSaveButton.ID;

            // cancel button
            this._CertificationPropertiesCancelButton = new Button();
            this._CertificationPropertiesCancelButton.ID = "CancelButton";
            this._CertificationPropertiesCancelButton.CssClass = "Button NonActionButton";
            this._CertificationPropertiesCancelButton.Text = _GlobalResources.Cancel;
            this._CertificationPropertiesCancelButton.Command += new CommandEventHandler(this._CertificationPropertiesCancelButton_Command);
            this.CertificationPropertiesActionsPanel.Controls.Add(this._CertificationPropertiesCancelButton);
        }
        #endregion

        #region _PopulateCertificationDataModelForJSONObject
        /// <summary>
        /// Populates a certification data model with data from the certification, then serializes into the hidden field.
        /// </summary>
        private void _PopulateCertificationDataModelForJSONObject()
        {
            if (this._CertificationObject != null)
            {
                CertificationDataModel certificationDataModel = new CertificationDataModel();

                // certification titles and descriptions
                foreach (Certification.LanguageSpecificProperty certLSP in this._CertificationObject.LanguageSpecificProperties)
                {
                    certificationDataModel.Titles.Add(new CertificationLanguageValueDataModel(){ Language = certLSP.LangString, Value = certLSP.Title });
                    certificationDataModel.Descriptions.Add(new CertificationLanguageValueDataModel() { Language = certLSP.LangString, Value = certLSP.ShortDescription });
                }

                // certification accreditor                
                certificationDataModel.Accreditor.Company = this._CertificationObject.AccreditingOrganization;
                certificationDataModel.Accreditor.Name = this._CertificationObject.CertificationContactName;
                certificationDataModel.Accreditor.Email = this._CertificationObject.CertificationContactEmail;
                certificationDataModel.Accreditor.Phone = this._CertificationObject.CertificationContactPhoneNumber;

                // certification tags, and publish and closed settings
                certificationDataModel.Tags = this._CertificationObject.SearchTags;
                certificationDataModel.isPublished = this._CertificationObject.IsPublished;
                certificationDataModel.isClosed = this._CertificationObject.IsClosed;

                // do expiration intervals
                certificationDataModel.Units.Initial.Expiration.Interval = this._CertificationObject.InitialAwardExpiresInterval.ToString();
                certificationDataModel.Units.Initial.Expiration.Timeframe = this._CertificationObject.InitialAwardExpiresTimeframe;
                certificationDataModel.Units.Renewal.Expiration.Interval = this._CertificationObject.RenewalExpiresInterval.ToString();
                certificationDataModel.Units.Renewal.Expiration.Timeframe = this._CertificationObject.RenewalExpiresTimeframe;

                // certification attached course/credit links
                DataTable certificationAttachedCourses = this._CertificationObject.GetAttachedCourses();

                foreach (DataRow row in certificationAttachedCourses.Rows)
                {
                    certificationDataModel.ApplicableCourses.Add(new CertificationApplicableCourseDataModel()
                    {
                        id = Convert.ToInt32(row["idCourse"]),
                        Name = row["title"].ToString(),
                        Credits = Convert.ToDouble(row["credits"])
                    });
                }

                // certification units
                DataTable certificationUnits = this._CertificationObject.GetModuleIds();

                foreach (DataRow row in certificationUnits.Rows)
                {
                    int idCertificationModule = Convert.ToInt32(row["idCertificationModule"]);
                    bool isInitialRequirement = Convert.ToBoolean(row["isInitialRequirement"]);
                    CertificationModule certificationModule = new CertificationModule(idCertificationModule);

                    if (isInitialRequirement)
                    {
                        // is any requirement set
                        certificationDataModel.Units.Initial.isAny = this._CertificationObject.IsAnyModule;

                        // add "items" for this module/unit
                        CertificationUnitsItemsDataModel certificationUnitItems = new CertificationUnitsItemsDataModel();
                        certificationUnitItems.id = certificationModule.Id;

                        // certification module/unit titles and descriptions
                        foreach (CertificationModule.LanguageSpecificProperty certModuleLSP in certificationModule.LanguageSpecificProperties)
                        {
                            certificationUnitItems.Titles.Add(new CertificationLanguageValueDataModel() { Language = certModuleLSP.LangString, Value = certModuleLSP.Title });
                            certificationUnitItems.Descriptions.Add(new CertificationLanguageValueDataModel() { Language = certModuleLSP.LangString, Value = certModuleLSP.ShortDescription });
                        }

                        // certification segments
                        DataTable certificationUnitSegments = certificationModule.GetRequirementSetIds();

                        foreach (DataRow segmentRow in certificationUnitSegments.Rows)
                        {
                            int idCertificationModuleRequirementSet = Convert.ToInt32(segmentRow["idCertificationModuleRequirementSet"]);
                            CertificationModuleRequirementSet certificationModuleRequirementSet = new CertificationModuleRequirementSet(idCertificationModuleRequirementSet);

                            // is any requirement set
                            certificationUnitItems.Segments.isAny = certificationModule.IsAnyRequirementSet;

                            // add "items" for this module requirement set/segment
                            CertificationSegmentsItemsDataModel certificationSegmentItems = new CertificationSegmentsItemsDataModel();
                            certificationSegmentItems.id = certificationModuleRequirementSet.Id;

                            // certification module requirement set/segment labels
                            foreach (CertificationModuleRequirementSet.LanguageSpecificProperty certModuleRSLSP in certificationModuleRequirementSet.LanguageSpecificProperties)
                            {
                                certificationSegmentItems.Labels.Add(new CertificationLanguageValueDataModel() { Language = certModuleRSLSP.LangString, Value = certModuleRSLSP.Label });
                            }

                            // certification requirements
                            DataTable certificationRequirements = certificationModuleRequirementSet.GetRequirementIds();

                            foreach (DataRow requirementRow in certificationRequirements.Rows)
                            {
                                int idCertificationModuleRequirement = Convert.ToInt32(requirementRow["idCertificationModuleRequirement"]);
                                CertificationModuleRequirement certificationModuleRequirement = new CertificationModuleRequirement(idCertificationModuleRequirement);

                                // is any requirement
                                certificationSegmentItems.Requirements.isAny = certificationModuleRequirementSet.IsAny;

                                // add "items" for this module requirement
                                if (certificationModuleRequirement.RequirementType == 0) // course(s) requirement
                                {
                                    CertificationRequirementItemDataModel certificationRequirementItems = new CertificationRequirementItemDataModel();
                                    certificationRequirementItems.id = certificationModuleRequirement.Id.ToString();
                                    certificationRequirementItems.Type = "course";
                                    certificationRequirementItems.Course.isAny = certificationModuleRequirement.CourseCompletionIsAny;

                                    // certification module requirement labels
                                    foreach (CertificationModuleRequirement.LanguageSpecificProperty certModuleRLSP in certificationModuleRequirement.LanguageSpecificProperties)
                                    {
                                        certificationRequirementItems.Labels.Add(new CertificationLanguageValueDataModel() { Language = certModuleRLSP.LangString, Value = certModuleRLSP.Label });
                                    }

                                    // course items
                                    DataTable courseRequirementCourses = certificationModuleRequirement.GetCourseRequirementCourseIds();

                                    foreach (DataRow courseRow in courseRequirementCourses.Rows)
                                    {
                                        certificationRequirementItems.Course.Items.Add(new CertificationRequirementCourseIdDataModel() { id = Convert.ToInt32(courseRow["idCourse"]) });
                                    }
                                    
                                    // attach the requirement
                                    certificationSegmentItems.Requirements.Items.Add(certificationRequirementItems);
                                }
                                else if (certificationModuleRequirement.RequirementType == 1) // credit(s) requirement
                                {
                                    CertificationRequirementItemDataModel certificationRequirementItems = new CertificationRequirementItemDataModel();
                                    certificationRequirementItems.id = certificationModuleRequirement.Id.ToString();
                                    certificationRequirementItems.Type = "credit";
                                    certificationRequirementItems.Credit.isAny = certificationModuleRequirement.CourseCreditEligibleCoursesIsAll;
                                    certificationRequirementItems.Credit.Minimum = certificationModuleRequirement.NumberCreditsRequired;

                                    // certification module requirement labels
                                    foreach (CertificationModuleRequirement.LanguageSpecificProperty certModuleRLSP in certificationModuleRequirement.LanguageSpecificProperties)
                                    {
                                        certificationRequirementItems.Labels.Add(new CertificationLanguageValueDataModel() { Language = certModuleRLSP.LangString, Value = certModuleRLSP.Label });
                                    }

                                    // course items
                                    DataTable courseRequirementCourses = certificationModuleRequirement.GetCourseRequirementCourseIds();

                                    foreach (DataRow courseRow in courseRequirementCourses.Rows)
                                    {
                                        certificationRequirementItems.Credit.Items.Add(new CertificationRequirementCourseIdDataModel() { id = Convert.ToInt32(courseRow["idCourse"]) });
                                    }

                                    // attach the requirement
                                    certificationSegmentItems.Requirements.Items.Add(certificationRequirementItems);
                                }
                                else if (certificationModuleRequirement.RequirementType == 2) // task requirement
                                {
                                    CertificationRequirementItemDataModel certificationRequirementItems = new CertificationRequirementItemDataModel();
                                    certificationRequirementItems.id = certificationModuleRequirement.Id.ToString();
                                    certificationRequirementItems.Type = "task";
                                    certificationRequirementItems.Task.RequireDocumentation = certificationModuleRequirement.DocumentationApprovalRequired;
                                    certificationRequirementItems.Task.Signoff.ByAdministrators = true;
                                    certificationRequirementItems.Task.Signoff.BySupervisor = certificationModuleRequirement.AllowSupervisorsToApproveDocumentation;

                                    // certification module requirement labels
                                    foreach (CertificationModuleRequirement.LanguageSpecificProperty certModuleRLSP in certificationModuleRequirement.LanguageSpecificProperties)
                                    {
                                        certificationRequirementItems.Labels.Add(new CertificationLanguageValueDataModel() { Language = certModuleRLSP.LangString, Value = certModuleRLSP.Label });
                                    }                                   

                                    // attach the requirement
                                    certificationSegmentItems.Requirements.Items.Add(certificationRequirementItems);
                                }
                                else
                                { }                                
                            }

                            // attach the segment
                            certificationUnitItems.Segments.Items.Add(certificationSegmentItems);
                        }

                        // attach the unit
                        certificationDataModel.Units.Initial.Items.Add(certificationUnitItems);
                    }
                    else
                    {
                        // is any requirement set
                        certificationDataModel.Units.Renewal.isAny = this._CertificationObject.IsRenewalAnyModule;

                        // add "items" for this module/unit
                        CertificationUnitsItemsDataModel certificationUnitItems = new CertificationUnitsItemsDataModel();
                        certificationUnitItems.id = certificationModule.Id;

                        // certification module/unit titles and descriptions
                        foreach (CertificationModule.LanguageSpecificProperty certModuleLSP in certificationModule.LanguageSpecificProperties)
                        {
                            certificationUnitItems.Titles.Add(new CertificationLanguageValueDataModel() { Language = certModuleLSP.LangString, Value = certModuleLSP.Title });
                            certificationUnitItems.Descriptions.Add(new CertificationLanguageValueDataModel() { Language = certModuleLSP.LangString, Value = certModuleLSP.ShortDescription });
                        }

                        // certification segments
                        DataTable certificationUnitSegments = certificationModule.GetRequirementSetIds();

                        foreach (DataRow segmentRow in certificationUnitSegments.Rows)
                        {
                            int idCertificationModuleRequirementSet = Convert.ToInt32(segmentRow["idCertificationModuleRequirementSet"]);
                            CertificationModuleRequirementSet certificationModuleRequirementSet = new CertificationModuleRequirementSet(idCertificationModuleRequirementSet);

                            // is any requirement set
                            certificationUnitItems.Segments.isAny = certificationModule.IsAnyRequirementSet;

                            // add "items" for this module requirement set/segment
                            CertificationSegmentsItemsDataModel certificationSegmentItems = new CertificationSegmentsItemsDataModel();
                            certificationSegmentItems.id = certificationModuleRequirementSet.Id;

                            // certification module requirement set/segment labels
                            foreach (CertificationModuleRequirementSet.LanguageSpecificProperty certModuleRSLSP in certificationModuleRequirementSet.LanguageSpecificProperties)
                            {
                                certificationSegmentItems.Labels.Add(new CertificationLanguageValueDataModel() { Language = certModuleRSLSP.LangString, Value = certModuleRSLSP.Label });
                            }

                            // certification requirements
                            DataTable certificationRequirements = certificationModuleRequirementSet.GetRequirementIds();

                            foreach (DataRow requirementRow in certificationRequirements.Rows)
                            {
                                int idCertificationModuleRequirement = Convert.ToInt32(requirementRow["idCertificationModuleRequirement"]);
                                CertificationModuleRequirement certificationModuleRequirement = new CertificationModuleRequirement(idCertificationModuleRequirement);

                                // is any requirement
                                certificationSegmentItems.Requirements.isAny = certificationModuleRequirementSet.IsAny;

                                // add "items" for this module requirement
                                if (certificationModuleRequirement.RequirementType == 0) // course(s) requirement
                                {
                                    CertificationRequirementItemDataModel certificationRequirementItems = new CertificationRequirementItemDataModel();
                                    certificationRequirementItems.id = certificationModuleRequirement.Id.ToString();
                                    certificationRequirementItems.Type = "course";
                                    certificationRequirementItems.Course.isAny = certificationModuleRequirement.CourseCompletionIsAny;

                                    // certification module requirement labels
                                    foreach (CertificationModuleRequirement.LanguageSpecificProperty certModuleRLSP in certificationModuleRequirement.LanguageSpecificProperties)
                                    {
                                        certificationRequirementItems.Labels.Add(new CertificationLanguageValueDataModel() { Language = certModuleRLSP.LangString, Value = certModuleRLSP.Label });
                                    }

                                    // course items
                                    DataTable courseRequirementCourses = certificationModuleRequirement.GetCourseRequirementCourseIds();

                                    foreach (DataRow courseRow in courseRequirementCourses.Rows)
                                    {
                                        certificationRequirementItems.Course.Items.Add(new CertificationRequirementCourseIdDataModel() { id = Convert.ToInt32(courseRow["idCourse"]) });
                                    }

                                    // attach the requirement
                                    certificationSegmentItems.Requirements.Items.Add(certificationRequirementItems);
                                }
                                else if (certificationModuleRequirement.RequirementType == 1) // credit(s) requirement
                                {
                                    CertificationRequirementItemDataModel certificationRequirementItems = new CertificationRequirementItemDataModel();
                                    certificationRequirementItems.id = certificationModuleRequirement.Id.ToString();
                                    certificationRequirementItems.Type = "credit";
                                    certificationRequirementItems.Credit.isAny = certificationModuleRequirement.CourseCreditEligibleCoursesIsAll;
                                    certificationRequirementItems.Credit.Minimum = certificationModuleRequirement.NumberCreditsRequired;

                                    // certification module requirement labels
                                    foreach (CertificationModuleRequirement.LanguageSpecificProperty certModuleRLSP in certificationModuleRequirement.LanguageSpecificProperties)
                                    {
                                        certificationRequirementItems.Labels.Add(new CertificationLanguageValueDataModel() { Language = certModuleRLSP.LangString, Value = certModuleRLSP.Label });
                                    }

                                    // course items
                                    DataTable courseRequirementCourses = certificationModuleRequirement.GetCourseRequirementCourseIds();

                                    foreach (DataRow courseRow in courseRequirementCourses.Rows)
                                    {
                                        certificationRequirementItems.Credit.Items.Add(new CertificationRequirementCourseIdDataModel() { id = Convert.ToInt32(courseRow["idCourse"]) });
                                    }

                                    // attach the requirement
                                    certificationSegmentItems.Requirements.Items.Add(certificationRequirementItems);
                                }
                                else if (certificationModuleRequirement.RequirementType == 2) // task requirement
                                {
                                    CertificationRequirementItemDataModel certificationRequirementItems = new CertificationRequirementItemDataModel();
                                    certificationRequirementItems.id = certificationModuleRequirement.Id.ToString();
                                    certificationRequirementItems.Type = "task";
                                    certificationRequirementItems.Task.RequireDocumentation = certificationModuleRequirement.DocumentationApprovalRequired;
                                    certificationRequirementItems.Task.Signoff.ByAdministrators = true;
                                    certificationRequirementItems.Task.Signoff.BySupervisor = certificationModuleRequirement.AllowSupervisorsToApproveDocumentation;

                                    // certification module requirement labels
                                    foreach (CertificationModuleRequirement.LanguageSpecificProperty certModuleRLSP in certificationModuleRequirement.LanguageSpecificProperties)
                                    {
                                        certificationRequirementItems.Labels.Add(new CertificationLanguageValueDataModel() { Language = certModuleRLSP.LangString, Value = certModuleRLSP.Label });
                                    }

                                    // attach the requirement
                                    certificationSegmentItems.Requirements.Items.Add(certificationRequirementItems);
                                }
                                else
                                { }
                            }

                            // attach the segment
                            certificationUnitItems.Segments.Items.Add(certificationSegmentItems);
                        }

                        // attach the unit
                        certificationDataModel.Units.Renewal.Items.Add(certificationUnitItems);
                    }
                }

                // serialize it
                this._CertificationJSONData.Value = JsonHelper.JsonSerializer<CertificationDataModel>(certificationDataModel);
            }
        }
        #endregion

        #region _CertificationPropertiesSaveButton_Command
        /// <summary>
        /// Handles Save Button Event
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CertificationPropertiesSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get the data from the form
                this._WriteCertificationDataHiddenFieldToCertificationDataModel();

                // declare module, module requirement set, and module requirement data tables to record existing and new records
                // this is so we can delete ones that were deleted from the interface as the final step in this save process
                DataTable savedCertificationModules = new DataTable();
                savedCertificationModules.Columns.Add("id", typeof(int));

                DataTable savedCertificationModuleRequirementSets = new DataTable();
                savedCertificationModuleRequirementSets.Columns.Add("id", typeof(int));

                DataTable savedCertificationModuleRequirements = new DataTable();
                savedCertificationModuleRequirements.Columns.Add("id", typeof(int));

                // create a new certification object if we do not already have one
                if (this._CertificationObject == null)
                { this._CertificationObject = new Certification(); }

                /* CERTIFICATION BASE */

                // do certification title for base language
                foreach (CertificationLanguageValueDataModel titleItem in this._CertificationDataModel.Titles)
                {
                    if (titleItem.Language == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._CertificationObject.Title = titleItem.Value;
                        break;
                    }
                }

                // do certification short description for base language
                foreach (CertificationLanguageValueDataModel shortDescriptionItem in this._CertificationDataModel.Descriptions)
                {
                    if (shortDescriptionItem.Language == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._CertificationObject.ShortDescription = shortDescriptionItem.Value;
                        break;
                    }
                }

                // do all other certification properties
                this._CertificationObject.SearchTags = this._CertificationDataModel.Tags;
                this._CertificationObject.AccreditingOrganization = this._CertificationDataModel.Accreditor.Company;
                this._CertificationObject.CertificationContactName = this._CertificationDataModel.Accreditor.Name;
                this._CertificationObject.CertificationContactEmail = this._CertificationDataModel.Accreditor.Email;
                this._CertificationObject.CertificationContactPhoneNumber = this._CertificationDataModel.Accreditor.Phone;
                this._CertificationObject.IsPublished = this._CertificationDataModel.isPublished;
                this._CertificationObject.IsClosed = this._CertificationDataModel.isClosed;
                this._CertificationObject.InitialAwardExpiresInterval = Convert.ToInt32(this._CertificationDataModel.Units.Initial.Expiration.Interval);
                this._CertificationObject.InitialAwardExpiresTimeframe = this._CertificationDataModel.Units.Initial.Expiration.Timeframe;
                this._CertificationObject.RenewalExpiresInterval = Convert.ToInt32(this._CertificationDataModel.Units.Renewal.Expiration.Interval);
                this._CertificationObject.RenewalExpiresTimeframe = this._CertificationDataModel.Units.Renewal.Expiration.Timeframe;
                this._CertificationObject.IsAnyModule = (bool)this._CertificationDataModel.Units.Initial.isAny;
                this._CertificationObject.IsRenewalAnyModule = (bool)this._CertificationDataModel.Units.Renewal.isAny;

                // save the certification
                int idCertification = this._CertificationObject.Save();

                /* CERTIFICATION LANGUAGE-SPECIFIC PROPERTIES */

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string certificationTitleInLanguage = null;
                        string certificationShortDescriptionInLanguage = null;

                        // title
                        foreach (CertificationLanguageValueDataModel titleItem in this._CertificationDataModel.Titles)
                        {
                            if (titleItem.Language == cultureInfo.Name)
                            {
                                certificationTitleInLanguage = titleItem.Value;
                                break;
                            }
                        }

                        // short description
                        foreach (CertificationLanguageValueDataModel shortDescriptionItem in this._CertificationDataModel.Descriptions)
                        {
                            if (shortDescriptionItem.Language == cultureInfo.Name)
                            {
                                certificationShortDescriptionInLanguage = shortDescriptionItem.Value;
                                break;
                            }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(certificationTitleInLanguage) ||
                            !String.IsNullOrWhiteSpace(certificationShortDescriptionInLanguage))
                        {
                            this._CertificationObject.SaveLang(cultureInfo.Name,
                                                                certificationTitleInLanguage,
                                                                certificationShortDescriptionInLanguage,
                                                                null);
                        }
                    }
                }

                /* CERTIFICATION ATTACHED COURSES */

                DataTable certificationAttachedCourses = new DataTable();
                certificationAttachedCourses.Columns.Add("idCourse", typeof(int));
                certificationAttachedCourses.Columns.Add("credits", typeof(double));

                foreach (CertificationApplicableCourseDataModel attachedCourse in this._CertificationDataModel.ApplicableCourses)
                { certificationAttachedCourses.Rows.Add(attachedCourse.id, attachedCourse.Credits); }

                this._CertificationObject.SaveAttachedCourses(certificationAttachedCourses);

                /* CERTIFICATION MODULES/UNITS - INITIAL */

                foreach (CertificationUnitsItemsDataModel certificationUnitItem in this._CertificationDataModel.Units.Initial.Items)
                {
                    // get the module if it is existing, or create a new one
                    int idCertificationModule = (int)certificationUnitItem.id;
                    CertificationModule certificationModuleObject;

                    if (idCertificationModule > 0)
                    { certificationModuleObject = new CertificationModule(idCertificationModule); }
                    else
                    { certificationModuleObject = new CertificationModule(); }

                    // do certification module title for base language
                    foreach (CertificationLanguageValueDataModel titleItem in certificationUnitItem.Titles)
                    {
                        if (titleItem.Language == AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            certificationModuleObject.Title = titleItem.Value;
                            break;
                        }
                    }

                    // do certification module short description for base language
                    foreach (CertificationLanguageValueDataModel shortDescriptionItem in certificationUnitItem.Descriptions)
                    {
                        if (shortDescriptionItem.Language == AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            certificationModuleObject.ShortDescription = shortDescriptionItem.Value;
                            break;
                        }
                    }

                    // do all other certification module properties
                    certificationModuleObject.IdCertification = idCertification;
                    certificationModuleObject.IsInitialRequirement = true;
                    certificationModuleObject.IsAnyRequirementSet = (bool)certificationUnitItem.Segments.isAny;

                    // save the certification module
                    idCertificationModule = certificationModuleObject.Save();
                    savedCertificationModules.Rows.Add(idCertificationModule);

                    /* CERTIFICATION MODULE LANGUAGE-SPECIFIC PROPERTIES */

                    // loop through languages, grab values from inputs, and populate language specific properties
                    foreach (string availableLanguage in availableLanguages)
                    {
                        // get the culture of the info for the language
                        CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                        // if this is the not the default language, get values from the language-specific text boxes
                        // and save the language-specific properties; default language is already taken care of in the
                        // object's Save procedure
                        if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            string certificationModuleTitleInLanguage = null;
                            string certificationModuleShortDescriptionInLanguage = null;

                            // title
                            foreach (CertificationLanguageValueDataModel titleItem in certificationUnitItem.Titles)
                            {
                                if (titleItem.Language == cultureInfo.Name)
                                {
                                    certificationModuleTitleInLanguage = titleItem.Value;
                                    break;
                                }
                            }

                            // short description
                            foreach (CertificationLanguageValueDataModel shortDescriptionItem in certificationUnitItem.Descriptions)
                            {
                                if (shortDescriptionItem.Language == cultureInfo.Name)
                                {
                                    certificationModuleShortDescriptionInLanguage = shortDescriptionItem.Value;
                                    break;
                                }
                            }

                            // save the properties if at least one property is populated
                            if (!String.IsNullOrWhiteSpace(certificationModuleTitleInLanguage) ||
                                !String.IsNullOrWhiteSpace(certificationModuleShortDescriptionInLanguage))
                            {
                                certificationModuleObject.SaveLang(cultureInfo.Name,
                                                                   certificationModuleTitleInLanguage,
                                                                   certificationModuleShortDescriptionInLanguage);
                            }
                        }
                    }

                    /* CERTIFICATION UNIT/MODULE SEGMENTS/REQUIREMENT SETS */

                    foreach (CertificationSegmentsItemsDataModel certificationSegmentItem in certificationUnitItem.Segments.Items)
                    {
                        // get the module requirement set if it is existing, or create a new one
                        int idCertificationModuleRequirementSet = (int)certificationSegmentItem.id;
                        CertificationModuleRequirementSet certificationModuleRequirementSetObject;

                        if (idCertificationModuleRequirementSet > 0)
                        { certificationModuleRequirementSetObject = new CertificationModuleRequirementSet(idCertificationModuleRequirementSet); }
                        else
                        { certificationModuleRequirementSetObject = new CertificationModuleRequirementSet(); }

                        // do certification module requirement set label for base language
                        foreach (CertificationLanguageValueDataModel labelItem in certificationSegmentItem.Labels)
                        {
                            if (labelItem.Language == AsentiaSessionState.GlobalSiteObject.LanguageString)
                            {
                                certificationModuleRequirementSetObject.Label = labelItem.Value;
                                break;
                            }
                        }

                        // do all other certification module requirement set properties
                        certificationModuleRequirementSetObject.IdCertificationModule = idCertificationModule;
                        certificationModuleRequirementSetObject.IsAny = (bool)certificationSegmentItem.Requirements.isAny;

                        // save the certification module requirement set
                        idCertificationModuleRequirementSet = certificationModuleRequirementSetObject.Save();
                        savedCertificationModuleRequirementSets.Rows.Add(idCertificationModuleRequirementSet);

                        /* CERTIFICATION MODULE REQUIREMENT SET LANGUAGE-SPECIFIC PROPERTIES */

                        // loop through languages, grab values from inputs, and populate language specific properties
                        foreach (string availableLanguage in availableLanguages)
                        {
                            // get the culture of the info for the language
                            CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                            // if this is the not the default language, get values from the language-specific text boxes
                            // and save the language-specific properties; default language is already taken care of in the
                            // object's Save procedure
                            if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                            {
                                string certificationModuleRequirementSetLabelInLanguage = null;

                                // label
                                foreach (CertificationLanguageValueDataModel labelItem in certificationSegmentItem.Labels)
                                {
                                    if (labelItem.Language == cultureInfo.Name)
                                    {
                                        certificationModuleRequirementSetLabelInLanguage = labelItem.Value;
                                        break;
                                    }
                                }

                                // save the properties if at least one property is populated
                                if (!String.IsNullOrWhiteSpace(certificationModuleRequirementSetLabelInLanguage))
                                {
                                    certificationModuleRequirementSetObject.SaveLang(cultureInfo.Name,
                                                                                     certificationModuleRequirementSetLabelInLanguage);
                                }
                            }
                        }

                        /* CERTIFICATION UNIT/MODULE SEGMENT/REQUIREMENT SET REQUIREMENTS */

                        foreach (CertificationRequirementItemDataModel certificationRequirementItem in certificationSegmentItem.Requirements.Items)
                        {
                            // get the module requirement if it is existing, or create a new one
                            int idCertificationModuleRequirement;
                            CertificationModuleRequirement certificationModuleRequirementObject;

                            if (!Int32.TryParse(certificationRequirementItem.id, out idCertificationModuleRequirement))
                            { idCertificationModuleRequirement = 0; }

                            if (idCertificationModuleRequirement > 0)
                            { certificationModuleRequirementObject = new CertificationModuleRequirement(idCertificationModuleRequirement); }
                            else
                            { certificationModuleRequirementObject = new CertificationModuleRequirement(); }

                            // do certification module requirement label for base language
                            foreach (CertificationLanguageValueDataModel labelItem in certificationRequirementItem.Labels)
                            {
                                if (labelItem.Language == AsentiaSessionState.GlobalSiteObject.LanguageString)
                                {
                                    certificationModuleRequirementObject.Label = labelItem.Value;
                                    break;
                                }
                            }

                            // do all other certification module requirement properties based on the type
                            certificationModuleRequirementObject.IdCertificationModuleRequirementSet = idCertificationModuleRequirementSet;
                            certificationModuleRequirementObject.ShortDescription = null;

                            if (certificationRequirementItem.Type == "course")
                            {
                                certificationModuleRequirementObject.RequirementType = 0;
                                certificationModuleRequirementObject.CourseCompletionIsAny = certificationRequirementItem.Course.isAny;
                                certificationModuleRequirementObject.NumberCreditsRequired = null;
                                certificationModuleRequirementObject.CourseCreditEligibleCoursesIsAll = null;
                                certificationModuleRequirementObject.DocumentationApprovalRequired = null;
                                certificationModuleRequirementObject.AllowSupervisorsToApproveDocumentation = null;
                            }
                            else if (certificationRequirementItem.Type == "credit")
                            {
                                certificationModuleRequirementObject.RequirementType = 1;
                                certificationModuleRequirementObject.CourseCompletionIsAny = null;
                                certificationModuleRequirementObject.NumberCreditsRequired = certificationRequirementItem.Credit.Minimum;
                                certificationModuleRequirementObject.CourseCreditEligibleCoursesIsAll = certificationRequirementItem.Credit.isAny;
                                certificationModuleRequirementObject.DocumentationApprovalRequired = null;
                                certificationModuleRequirementObject.AllowSupervisorsToApproveDocumentation = null;
                            }
                            else if (certificationRequirementItem.Type == "task")
                            {
                                certificationModuleRequirementObject.RequirementType = 2;
                                certificationModuleRequirementObject.CourseCompletionIsAny = null;
                                certificationModuleRequirementObject.NumberCreditsRequired = null;
                                certificationModuleRequirementObject.CourseCreditEligibleCoursesIsAll = null;
                                certificationModuleRequirementObject.DocumentationApprovalRequired = certificationRequirementItem.Task.RequireDocumentation;
                                certificationModuleRequirementObject.AllowSupervisorsToApproveDocumentation = certificationRequirementItem.Task.Signoff.BySupervisor;
                            }
                            else
                            { }

                            // save the certification module requirement
                            idCertificationModuleRequirement = certificationModuleRequirementObject.Save();
                            savedCertificationModuleRequirements.Rows.Add(idCertificationModuleRequirement);

                            /* CERTIFICATION MODULE REQUIREMENT LANGUAGE-SPECIFIC PROPERTIES */

                            // loop through languages, grab values from inputs, and populate language specific properties
                            foreach (string availableLanguage in availableLanguages)
                            {
                                // get the culture of the info for the language
                                CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                                // if this is the not the default language, get values from the language-specific text boxes
                                // and save the language-specific properties; default language is already taken care of in the
                                // object's Save procedure
                                if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                                {
                                    string certificationModuleRequirementLabelInLanguage = null;

                                    // label
                                    foreach (CertificationLanguageValueDataModel labelItem in certificationRequirementItem.Labels)
                                    {
                                        if (labelItem.Language == cultureInfo.Name)
                                        {
                                            certificationModuleRequirementLabelInLanguage = labelItem.Value;
                                            break;
                                        }
                                    }

                                    // save the properties if at least one property is populated
                                    if (!String.IsNullOrWhiteSpace(certificationModuleRequirementLabelInLanguage))
                                    {
                                        certificationModuleRequirementObject.SaveLang(cultureInfo.Name,
                                                                                      certificationModuleRequirementLabelInLanguage,
                                                                                      null);
                                    }
                                }
                            }

                            /* CERTIFICATION MODULE REQUIREMENT TO COURSE LINKS */
                            DataTable certificationRequirementCourses = new DataTable();
                            certificationRequirementCourses.Columns.Add("id", typeof(int));
                            certificationRequirementCourses.Columns.Add("order", typeof(int));

                            if (certificationRequirementItem.Type == "course")
                            {
                                int i = 1;

                                foreach (CertificationRequirementCourseIdDataModel requirementCourse in certificationRequirementItem.Course.Items)
                                { certificationRequirementCourses.Rows.Add(requirementCourse.id, i); }
                            }
                            else if (certificationRequirementItem.Type == "credit")
                            {
                                int i = 1;

                                foreach (CertificationRequirementCourseIdDataModel requirementCourse in certificationRequirementItem.Credit.Items)
                                { certificationRequirementCourses.Rows.Add(requirementCourse.id, i); }
                            }
                            else
                            { }

                            certificationModuleRequirementObject.SaveRequirementCourses(certificationRequirementCourses);
                        }
                    }
                }

                /* CERTIFICATION MODULES/UNITS - RENEWAL */

                foreach (CertificationUnitsItemsDataModel certificationUnitItem in this._CertificationDataModel.Units.Renewal.Items)
                {
                    // get the module if it is existing, or create a new one
                    int idCertificationModule = (int)certificationUnitItem.id;
                    CertificationModule certificationModuleObject;

                    if (idCertificationModule > 0)
                    { certificationModuleObject = new CertificationModule(idCertificationModule); }
                    else
                    { certificationModuleObject = new CertificationModule(); }

                    // do certification module title for base language
                    foreach (CertificationLanguageValueDataModel titleItem in certificationUnitItem.Titles)
                    {
                        if (titleItem.Language == AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            certificationModuleObject.Title = titleItem.Value;
                            break;
                        }
                    }

                    // do certification module short description for base language
                    foreach (CertificationLanguageValueDataModel shortDescriptionItem in certificationUnitItem.Descriptions)
                    {
                        if (shortDescriptionItem.Language == AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            certificationModuleObject.ShortDescription = shortDescriptionItem.Value;
                            break;
                        }
                    }

                    // do all other certification module properties
                    certificationModuleObject.IdCertification = idCertification;
                    certificationModuleObject.IsInitialRequirement = false;
                    certificationModuleObject.IsAnyRequirementSet = (bool)certificationUnitItem.Segments.isAny;

                    // save the certification module
                    idCertificationModule = certificationModuleObject.Save();
                    savedCertificationModules.Rows.Add(idCertificationModule);

                    /* CERTIFICATION MODULE LANGUAGE-SPECIFIC PROPERTIES */

                    // loop through languages, grab values from inputs, and populate language specific properties
                    foreach (string availableLanguage in availableLanguages)
                    {
                        // get the culture of the info for the language
                        CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                        // if this is the not the default language, get values from the language-specific text boxes
                        // and save the language-specific properties; default language is already taken care of in the
                        // object's Save procedure
                        if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            string certificationModuleTitleInLanguage = null;
                            string certificationModuleShortDescriptionInLanguage = null;

                            // title
                            foreach (CertificationLanguageValueDataModel titleItem in certificationUnitItem.Titles)
                            {
                                if (titleItem.Language == cultureInfo.Name)
                                {
                                    certificationModuleTitleInLanguage = titleItem.Value;
                                    break;
                                }
                            }

                            // short description
                            foreach (CertificationLanguageValueDataModel shortDescriptionItem in certificationUnitItem.Descriptions)
                            {
                                if (shortDescriptionItem.Language == cultureInfo.Name)
                                {
                                    certificationModuleShortDescriptionInLanguage = shortDescriptionItem.Value;
                                    break;
                                }
                            }

                            // save the properties if at least one property is populated
                            if (!String.IsNullOrWhiteSpace(certificationModuleTitleInLanguage) ||
                                !String.IsNullOrWhiteSpace(certificationModuleShortDescriptionInLanguage))
                            {
                                certificationModuleObject.SaveLang(cultureInfo.Name,
                                                                   certificationModuleTitleInLanguage,
                                                                   certificationModuleShortDescriptionInLanguage);
                            }
                        }
                    }

                    /* CERTIFICATION UNIT/MODULE SEGMENTS/REQUIREMENT SETS */

                    foreach (CertificationSegmentsItemsDataModel certificationSegmentItem in certificationUnitItem.Segments.Items)
                    {
                        // get the module requirement set if it is existing, or create a new one
                        int idCertificationModuleRequirementSet = (int)certificationSegmentItem.id;
                        CertificationModuleRequirementSet certificationModuleRequirementSetObject;

                        if (idCertificationModuleRequirementSet > 0)
                        { certificationModuleRequirementSetObject = new CertificationModuleRequirementSet(idCertificationModuleRequirementSet); }
                        else
                        { certificationModuleRequirementSetObject = new CertificationModuleRequirementSet(); }

                        // do certification module requirement set label for base language
                        foreach (CertificationLanguageValueDataModel labelItem in certificationSegmentItem.Labels)
                        {
                            if (labelItem.Language == AsentiaSessionState.GlobalSiteObject.LanguageString)
                            {
                                certificationModuleRequirementSetObject.Label = labelItem.Value;
                                break;
                            }
                        }

                        // do all other certification module requirement set properties
                        certificationModuleRequirementSetObject.IdCertificationModule = idCertificationModule;
                        certificationModuleRequirementSetObject.IsAny = (bool)certificationSegmentItem.Requirements.isAny;

                        // save the certification module requirement set
                        idCertificationModuleRequirementSet = certificationModuleRequirementSetObject.Save();
                        savedCertificationModuleRequirementSets.Rows.Add(idCertificationModuleRequirementSet);

                        /* CERTIFICATION MODULE REQUIREMENT SET LANGUAGE-SPECIFIC PROPERTIES */

                        // loop through languages, grab values from inputs, and populate language specific properties
                        foreach (string availableLanguage in availableLanguages)
                        {
                            // get the culture of the info for the language
                            CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                            // if this is the not the default language, get values from the language-specific text boxes
                            // and save the language-specific properties; default language is already taken care of in the
                            // object's Save procedure
                            if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                            {
                                string certificationModuleRequirementSetLabelInLanguage = null;

                                // label
                                foreach (CertificationLanguageValueDataModel labelItem in certificationSegmentItem.Labels)
                                {
                                    if (labelItem.Language == cultureInfo.Name)
                                    {
                                        certificationModuleRequirementSetLabelInLanguage = labelItem.Value;
                                        break;
                                    }
                                }

                                // save the properties if at least one property is populated
                                if (!String.IsNullOrWhiteSpace(certificationModuleRequirementSetLabelInLanguage))
                                {
                                    certificationModuleRequirementSetObject.SaveLang(cultureInfo.Name,
                                                                                     certificationModuleRequirementSetLabelInLanguage);
                                }
                            }
                        }

                        /* CERTIFICATION UNIT/MODULE SEGMENT/REQUIREMENT SET REQUIREMENTS */

                        foreach (CertificationRequirementItemDataModel certificationRequirementItem in certificationSegmentItem.Requirements.Items)
                        {
                            // get the module requirement if it is existing, or create a new one
                            int idCertificationModuleRequirement;
                            CertificationModuleRequirement certificationModuleRequirementObject;

                            if (!Int32.TryParse(certificationRequirementItem.id, out idCertificationModuleRequirement))
                            { idCertificationModuleRequirement = 0; }

                            if (idCertificationModuleRequirement > 0)
                            { certificationModuleRequirementObject = new CertificationModuleRequirement(idCertificationModuleRequirement); }
                            else
                            { certificationModuleRequirementObject = new CertificationModuleRequirement(); }

                            // do certification module requirement label for base language
                            foreach (CertificationLanguageValueDataModel labelItem in certificationRequirementItem.Labels)
                            {
                                if (labelItem.Language == AsentiaSessionState.GlobalSiteObject.LanguageString)
                                {
                                    certificationModuleRequirementObject.Label = labelItem.Value;
                                    break;
                                }
                            }

                            // do all other certification module requirement properties based on the type
                            certificationModuleRequirementObject.IdCertificationModuleRequirementSet = idCertificationModuleRequirementSet;
                            certificationModuleRequirementObject.ShortDescription = null;

                            if (certificationRequirementItem.Type == "course")
                            {
                                certificationModuleRequirementObject.RequirementType = 0;
                                certificationModuleRequirementObject.CourseCompletionIsAny = certificationRequirementItem.Course.isAny;
                                certificationModuleRequirementObject.NumberCreditsRequired = null;
                                certificationModuleRequirementObject.CourseCreditEligibleCoursesIsAll = null;
                                certificationModuleRequirementObject.DocumentationApprovalRequired = null;
                                certificationModuleRequirementObject.AllowSupervisorsToApproveDocumentation = null;
                            }
                            else if (certificationRequirementItem.Type == "credit")
                            {
                                certificationModuleRequirementObject.RequirementType = 1;
                                certificationModuleRequirementObject.CourseCompletionIsAny = null;
                                certificationModuleRequirementObject.NumberCreditsRequired = certificationRequirementItem.Credit.Minimum;
                                certificationModuleRequirementObject.CourseCreditEligibleCoursesIsAll = certificationRequirementItem.Credit.isAny;
                                certificationModuleRequirementObject.DocumentationApprovalRequired = null;
                                certificationModuleRequirementObject.AllowSupervisorsToApproveDocumentation = null;
                            }
                            else if (certificationRequirementItem.Type == "task")
                            {
                                certificationModuleRequirementObject.RequirementType = 2;
                                certificationModuleRequirementObject.CourseCompletionIsAny = null;
                                certificationModuleRequirementObject.NumberCreditsRequired = null;
                                certificationModuleRequirementObject.CourseCreditEligibleCoursesIsAll = null;
                                certificationModuleRequirementObject.DocumentationApprovalRequired = certificationRequirementItem.Task.RequireDocumentation;
                                certificationModuleRequirementObject.AllowSupervisorsToApproveDocumentation = certificationRequirementItem.Task.Signoff.BySupervisor;
                            }
                            else
                            { }

                            // save the certification module requirement
                            idCertificationModuleRequirement = certificationModuleRequirementObject.Save();
                            savedCertificationModuleRequirements.Rows.Add(idCertificationModuleRequirement);

                            /* CERTIFICATION MODULE REQUIREMENT LANGUAGE-SPECIFIC PROPERTIES */

                            // loop through languages, grab values from inputs, and populate language specific properties
                            foreach (string availableLanguage in availableLanguages)
                            {
                                // get the culture of the info for the language
                                CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                                // if this is the not the default language, get values from the language-specific text boxes
                                // and save the language-specific properties; default language is already taken care of in the
                                // object's Save procedure
                                if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                                {
                                    string certificationModuleRequirementLabelInLanguage = null;

                                    // label
                                    foreach (CertificationLanguageValueDataModel labelItem in certificationRequirementItem.Labels)
                                    {
                                        if (labelItem.Language == cultureInfo.Name)
                                        {
                                            certificationModuleRequirementLabelInLanguage = labelItem.Value;
                                            break;
                                        }
                                    }

                                    // save the properties if at least one property is populated
                                    if (!String.IsNullOrWhiteSpace(certificationModuleRequirementLabelInLanguage))
                                    {
                                        certificationModuleRequirementObject.SaveLang(cultureInfo.Name,
                                                                                      certificationModuleRequirementLabelInLanguage,
                                                                                      null);
                                    }
                                }
                            }

                            /* CERTIFICATION MODULE REQUIREMENT TO COURSE LINKS */
                            DataTable certificationRequirementCourses = new DataTable();
                            certificationRequirementCourses.Columns.Add("id", typeof(int));
                            certificationRequirementCourses.Columns.Add("order", typeof(int));

                            if (certificationRequirementItem.Type == "course")
                            {
                                int i = 1;

                                foreach (CertificationRequirementCourseIdDataModel requirementCourse in certificationRequirementItem.Course.Items)
                                { certificationRequirementCourses.Rows.Add(requirementCourse.id, i); }
                            }
                            else if (certificationRequirementItem.Type == "credit")
                            {
                                int i = 1;

                                foreach (CertificationRequirementCourseIdDataModel requirementCourse in certificationRequirementItem.Credit.Items)
                                { certificationRequirementCourses.Rows.Add(requirementCourse.id, i); }
                            }
                            else
                            { }

                            certificationModuleRequirementObject.SaveRequirementCourses(certificationRequirementCourses);
                        }
                    }
                }

                // DELETE MODULES, REQUIREMENT SETS, AND REQUIREMENTS THAT WERE NOT SAVED - DELETED IN INTERFACE
                this._CertificationObject.DeleteRemovedObjects(savedCertificationModules, savedCertificationModuleRequirementSets, savedCertificationModuleRequirements);     
           
                // re-build the certification object menu
                this._BuildCertificationObjectMenu();

                // DISPLAY FEEDBACK
                this.DisplayFeedback(_GlobalResources.TheCertificationHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _WriteCertificationDataHiddenFieldToCertificationDataModel
        /// <summary>
        /// Transfers the JSON Data from the _CertificationJSONData HiddenField to the _CertificationDataModel object.
        /// </summary>
        private void _WriteCertificationDataHiddenFieldToCertificationDataModel()
        {
            // convert string in _CertificationJSONData to a CertificationDataModel object
            string certificationJSONDataInHiddenField = _CertificationJSONData.Value.ToString();
            this._CertificationDataModel = new CertificationDataModel();

            // deserialize the data string to the data model
            this._CertificationDataModel = JsonHelper.JsonDeserialize<CertificationDataModel>(certificationJSONDataInHiddenField);
        }
        #endregion

        #region _CertificationPropertiesCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for certification properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CertificationPropertiesCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/administrator/certifications");
        }
        #endregion        
    }
}
