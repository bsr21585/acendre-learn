﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.Certifications
{
    public class Dashboard : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CertificationDashboardFormContentWrapperContainer;
        public Panel CertificationObjectMenuContainer;
        public Panel CertificationDashboardWrapperContainer;
        public Panel CertificationDashboardInstructionsPanel;
        public Panel CertificationDashboardFeedbackContainer;
        #endregion

        #region Private Properties
        private Certification _CertificationObject;
        private ObjectDashboard _CertificationObjectDashboard;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            //csm.RegisterClientScriptResource(typeof(Dashboard), "Asentia.LMS.Pages.Administrator.Certifications.Dashboard.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CertificationsManager))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("ObjectDashboard.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/certifications/Dashboard.css");

            // get the certification object
            this._GetCertificationObject();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetCertificationObject
        /// <summary>
        /// Gets a certification object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCertificationObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._CertificationObject = new Certification(id); }
                }
                catch
                { Response.Redirect("~/administrator/certifications"); }
            }
            else { Response.Redirect("~/administrator/certifications"); }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // set container classes
            this.CertificationDashboardFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CertificationDashboardWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the certification object menu
            if (this._CertificationObject != null)
            {
                CertificationObjectMenu certificationObjectMenu = new CertificationObjectMenu(this._CertificationObject);
                certificationObjectMenu.SelectedItem = CertificationObjectMenu.MenuObjectItem.CertificationDashboard;

                this.CertificationObjectMenuContainer.Controls.Add(certificationObjectMenu);
            }

            // build the certification object dashboard
            this._CertificationObjectDashboard = new ObjectDashboard("CertificationObjectDashboard");

            this._BuildInformationWidget();
            this._BuildActionsWidget();
            this._BuildStatsWidget();            

            // attach object widget to wrapper container
            this.CertificationDashboardWrapperContainer.Controls.Add(this._CertificationObjectDashboard);
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get certification title information
            string certificationTitleInInterfaceLanguage = this._CertificationObject.Title;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Certification.LanguageSpecificProperty certificationLanguageSpecificProperty in this._CertificationObject.LanguageSpecificProperties)
                {
                    if (certificationLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { certificationTitleInInterfaceLanguage = certificationLanguageSpecificProperty.Title; }
                }
            }            

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Certifications, "/administrator/certifications"));
            breadCrumbLinks.Add(new BreadcrumbLink(certificationTitleInInterfaceLanguage));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, certificationTitleInInterfaceLanguage, ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD, ImageFiles.EXT_PNG), null);
        }
        #endregion

        #region _BuildInformationWidget
        /// <summary>
        /// Builds the information widget.
        /// </summary>
        private void _BuildInformationWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> informationWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // CREATED DATE
            string formattedCreatedDate = TimeZoneInfo.ConvertTimeFromUtc(this._CertificationObject.DtCreated, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", ""));
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("CreatedDate", _GlobalResources.Created + ":", formattedCreatedDate, ObjectDashboard.WidgetItemType.Text));

            // MODIFIED DATE
            string formattedModifiedDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._CertificationObject.DtModified, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", ""));
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("LastModifiedDate", _GlobalResources.LastModified + ":", formattedModifiedDate, ObjectDashboard.WidgetItemType.Text));

            // ACCREDITING ORGANIZATION
            if (!String.IsNullOrWhiteSpace(this._CertificationObject.AccreditingOrganization))
            { informationWidgetItems.Add(new ObjectDashboard.WidgetItem("AccreditingOrganization", _GlobalResources.AccreditingOrganization + ":", this._CertificationObject.AccreditingOrganization.ToString(), ObjectDashboard.WidgetItemType.Text)); }
            else
            { informationWidgetItems.Add(new ObjectDashboard.WidgetItem("AccreditingOrganization", _GlobalResources.AccreditingOrganization + ":", "-", ObjectDashboard.WidgetItemType.Text)); }

            // INITIAL AWARD EXPIRATION
            if (this._CertificationObject.InitialAwardExpiresInterval != null)
            { 
                string initialExpiresTimeframe = String.Empty;

                switch (this._CertificationObject.InitialAwardExpiresTimeframe)
                {
                    case "yyyy":
                        initialExpiresTimeframe = _GlobalResources.Year_s;
                        break;
                    case "m":
                        initialExpiresTimeframe = _GlobalResources.Month_s;
                        break;
                    case "ww":
                        initialExpiresTimeframe = _GlobalResources.Week_s;
                        break;
                    case "d":
                        initialExpiresTimeframe = _GlobalResources.Day_s;
                        break;
                    default:
                        break;
                }

                informationWidgetItems.Add(new ObjectDashboard.WidgetItem("InitialAwardExpiration", _GlobalResources.InitialAwardExpiration + ":", this._CertificationObject.InitialAwardExpiresInterval.ToString() + " " + initialExpiresTimeframe, ObjectDashboard.WidgetItemType.Text));
            }
            else
            { informationWidgetItems.Add(new ObjectDashboard.WidgetItem("InitialAwardExpiration", _GlobalResources.InitialAwardExpiration + ":", "-", ObjectDashboard.WidgetItemType.Text)); }

            // RENEWAL EXPIRATION
            if (this._CertificationObject.RenewalExpiresInterval != null)
            {
                string renewalExpiresTimeframe = String.Empty;

                switch (this._CertificationObject.RenewalExpiresTimeframe)
                {
                    case "yyyy":
                        renewalExpiresTimeframe = _GlobalResources.Year_s;
                        break;
                    case "m":
                        renewalExpiresTimeframe = _GlobalResources.Month_s;
                        break;
                    case "ww":
                        renewalExpiresTimeframe = _GlobalResources.Week_s;
                        break;
                    case "d":
                        renewalExpiresTimeframe = _GlobalResources.Day_s;
                        break;
                    default:
                        break;
                }

                informationWidgetItems.Add(new ObjectDashboard.WidgetItem("RenewalExpiration", _GlobalResources.RenewalExpiration + ":", this._CertificationObject.RenewalExpiresInterval.ToString() + " " + renewalExpiresTimeframe, ObjectDashboard.WidgetItemType.Text));
            }
            else
            { informationWidgetItems.Add(new ObjectDashboard.WidgetItem("RenewalExpiration", _GlobalResources.RenewalExpiration + ":", "-", ObjectDashboard.WidgetItemType.Text)); }

            // add the widget
            this._CertificationObjectDashboard.AddWidget("InformationWidget", _GlobalResources.Information, informationWidgetItems);
        }
        #endregion

        #region _BuildActionsWidget
        /// <summary>
        /// Builds the actions widget.
        /// </summary>
        private void _BuildActionsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> actionsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // NEW CERTIFICATE LINK

            Panel createCertificateLinkWrapper = new Panel();

            HyperLink createCertificateLink = new HyperLink();
            createCertificateLink.NavigateUrl = "certificates/Modify.aspx?cid=" + this._CertificationObject.Id.ToString();
            createCertificateLinkWrapper.Controls.Add(createCertificateLink);

            Image createCertificateLinkImage = new Image();
            createCertificateLinkImage.CssClass = "SmallIcon";
            createCertificateLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
            createCertificateLink.Controls.Add(createCertificateLinkImage);

            Image createCertificateOverlayLinkImage = new Image();
            createCertificateOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
            createCertificateOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            createCertificateLink.Controls.Add(createCertificateOverlayLinkImage);

            Literal createCertificateLinkText = new Literal();
            createCertificateLinkText.Text = _GlobalResources.CreateANewCertificate;
            createCertificateLink.Controls.Add(createCertificateLinkText);

            actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateCertificate", null, createCertificateLinkWrapper, ObjectDashboard.WidgetItemType.Link));

            // NEW EMAIL NOTIFICATION LINK

            Panel createEmailNotificationLinkWrapper = new Panel();

            HyperLink createEmailNotificationLink = new HyperLink();
            createEmailNotificationLink.NavigateUrl = "emailnotifications/Modify.aspx?cid=" + this._CertificationObject.Id.ToString();
            createEmailNotificationLinkWrapper.Controls.Add(createEmailNotificationLink);

            Image createEmailNotificationLinkImage = new Image();
            createEmailNotificationLinkImage.CssClass = "SmallIcon";
            createEmailNotificationLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
            createEmailNotificationLink.Controls.Add(createEmailNotificationLinkImage);

            Image createEmailNotificationOverlayLinkImage = new Image();
            createEmailNotificationOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
            createEmailNotificationOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            createEmailNotificationLink.Controls.Add(createEmailNotificationOverlayLinkImage);

            Literal createEmailNotificationLinkText = new Literal();
            createEmailNotificationLinkText.Text = _GlobalResources.CreateANewEmailNotification;
            createEmailNotificationLink.Controls.Add(createEmailNotificationLinkText);

            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE) ?? false)
            {
                actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateEmailNotification", null, createEmailNotificationLinkWrapper, ObjectDashboard.WidgetItemType.Link));
            }

            // NEW AUTO-JOIN RULESET LINK

            Panel createRulesetLinkWrapper = new Panel();

            HyperLink createRulesetLink = new HyperLink();
            createRulesetLink.NavigateUrl = "AutoJoinRules.aspx?cid=" + this._CertificationObject.Id.ToString() + "&action=NewRuleset";
            createRulesetLinkWrapper.Controls.Add(createRulesetLink);

            Image createRulesetLinkImage = new Image();
            createRulesetLinkImage.CssClass = "SmallIcon";
            createRulesetLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYNCHRONIZE, ImageFiles.EXT_PNG);
            createRulesetLink.Controls.Add(createRulesetLinkImage);

            Image createRulesetOverlayLinkImage = new Image();
            createRulesetOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
            createRulesetOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
            createRulesetLink.Controls.Add(createRulesetOverlayLinkImage);

            Literal createRulesetLinkText = new Literal();
            createRulesetLinkText.Text = _GlobalResources.CreateANewAutoJoinRuleset;
            createRulesetLink.Controls.Add(createRulesetLinkText);

            actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateRuleset", null, createRulesetLinkWrapper, ObjectDashboard.WidgetItemType.Link));            

            // add the widget
            this._CertificationObjectDashboard.AddWidget("ActionsWidget", _GlobalResources.Actions, actionsWidgetItems);
        }
        #endregion        

        #region _BuildStatsWidget
        /// <summary>
        /// Builds the stats widget.
        /// </summary>
        private void _BuildStatsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> statsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the data and build a chart
            DataTable certificationsStatsDt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            Chart certificationStatsPieChart;

            // chart colors
            List<string> certificationStatsPieChartColors = new List<string>();
            certificationStatsPieChartColors.Add("#2E7DBD");
            certificationStatsPieChartColors.Add("#55BD86");
            certificationStatsPieChartColors.Add("#FED155");
            certificationStatsPieChartColors.Add("#EC6E61");

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idCertification", this._CertificationObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certification.EnrollmentStatistics]", true);

                // load recordset
                certificationsStatsDt.Load(sdr);

                sdr.Close();

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // draw the chart
                certificationStatsPieChart = new Chart("CertificationStatsPieChart", ChartType.Doughnut, certificationsStatsDt, certificationStatsPieChartColors, _GlobalResources.Stats, null);
                certificationStatsPieChart.ShowTotalInTitle = true;
                certificationStatsPieChart.ShowTotalAndPercentageInLegend = true;
                certificationStatsPieChart.IsResponsive = false;
                certificationStatsPieChart.CanvasWidth = 125;
                certificationStatsPieChart.CanvasHeight = 125;
            }
            catch // on errors, just draw an empty chart
            {
                // draw an empty chart
                DataTable emptyDataTable = new DataTable();
                emptyDataTable.Columns.Add("_Total_", typeof(int));
                emptyDataTable.Rows.Add(0);

                certificationStatsPieChart = new Chart("CertificationStatsPieChart", ChartType.Doughnut, emptyDataTable, certificationStatsPieChartColors, _GlobalResources.Stats, null);
                certificationStatsPieChart.ShowTotalInTitle = true;
                certificationStatsPieChart.ShowTotalAndPercentageInLegend = false;
                certificationStatsPieChart.IsResponsive = false;
                certificationStatsPieChart.CanvasWidth = 125;
                certificationStatsPieChart.CanvasHeight = 125;
            }
            finally
            { databaseObject.Dispose(); }

            // add the chart to the widget item list
            statsWidgetItems.Add(new ObjectDashboard.WidgetItem("CertificationStats", null, certificationStatsPieChart, ObjectDashboard.WidgetItemType.Object));

            // add the widget
            this._CertificationObjectDashboard.AddWidget("StatsWidget", _GlobalResources.Stats, statsWidgetItems);
        }
        #endregion        
    }
}
